﻿#region Update
/*
    26/02/2021 [DITA/SIER] New Apps, tambah processind draft/ final
    16/03/2021 [ICA/SIER] menambah field Total Estimated Price Based On parameter IsMRShowTotalEstimatedPrice
    28/05/2021 [IQA/SIER] Menambah kolom baru Specification dan parameter IsMRUseSpecification
    02/06/2021 [BRI/SIER] menambah filed Division berdasarkan parameter IsMRUseDivision
    03/06/2021 [ICA+RDA/SIER] penyesuaian printout
    10/06/2021 [ICA+RDA/SIER] feedback penyesuaian printout
    08/09/2021 [VIN/SIER]Bug Designer: Button Upload&Download File + Event Hilang
    21/09/2021 [MYA/SIER] tambah field status disebelah item name pada detail
    27/09/2021 [RDA/SIER] penyesuaian printout Material Request
    29/09/2021 [TYO/SIER] memindah parameter mIsMRShowTotalEstimatedPrice ke internal
    06/10/2021 [VIN/SIER] ComputeAvailableBudget tambah parameter IsBudgetCalculateFromEstimatedPrice --> saat ambil amount MR 
    08/10/2021 [RDA/SIER] penyesuaian printout material request field after used mengambil dari available budget transaksi sebelumnya
    14/10/2021 [IBL/SIER] Feedback : Approval berdasarkan starting amount doc approval setting. Melihat dari Total Price bukan dari Estimated Price
    02/11/2021 [RDA/SIER] penyesuaian printout material request untuk menampilkan mata uang, merubah format menjadi landscape, penyesuaian field tanda tangan dan footer
    09/11/2021 [TYO/SIER] hide kolom Minimum stock, Reorder point, Dropping Request's Amount
    09/11/2021 [TYO/SIER] tambah kolom location diantara Item's name dan Specification
    09/11/2021 [TYO/SIER] estimated price di edit state not editable berdasarkan parameter IsMREstimatedPriceNotEditable
    17/11/2021 [TYO/SIER] estimated price di insert state not editable berdasarkan parameter IsMREstimatedPriceNotEditable
    19/11/2021 [MYA/SIER] Penyesuaian menu BOQ pada Detail (0103990104) Material Request SPPJB saat di loop
    19/11/2021 [SET/SIER] Penyesuaian Print out document Material Request SPPJB dan Permintaan Printout document BOQ pada Detail Material Request SPPJB
    03/01/2021 [VIN/SIER] BUG IsMREstimatedPriceNotEditable hanya untuk MR SPPJB      
    04/01/2022 [TYO/SIER] Menambah gambar ttd pada printout SIER
    05/01/2022 [ISD/SIER] Penyesuaian ttd printout pada printout MR SPPJB dan BOQ
    12/01/2022 [ISD/SIER] Bug muncul kolom location
    03/02/2022 [VIN/SIER] Bug ComputeTotalEstPrice tidak menjumlahkan item yg cancel
    04/02/2022 [VIN/SIER] Bug ComputeAvailableBudget tidak menjumlahkan item yg cancel
    10/02/2022 [VIN/SIER] Feedback Penyesuaian Budget di Printout
    02/03/2022 [SET/SIER] Membuat approval setting berdasar group & amount - Material Request
    02/03/2022 [SET/SIER] Membuat approval setting berdasar group & amount - Material Request SPPJB
    28/03/2022 [IBL/SIER] BUG : Dokumen MR dan SPPJB tidak bisa nyantol ketika approval settingnya tidak mempunyai DAG.
    07/04/2022 [VIN/SIER] BUG : Event ChakedChanged hilang
    14/04/2022 [TRI/SIER] BUG validasi ketika type budget tetapi tidak memilih budget category
    20/04/2022 [VIN/SIER] BUG PIC tidak terotorisasi berdasarkan user yang login
    18/07/2022 [RIS/SIER] Merubah informasi approval dengan param IsMaterialRequestUseDetailApprovalInformation
    20/08/2022 [DITA/SIER] tambah department for budget untuk pemilihan budget
    21/07/2022 [RIS/SIER] Menambahkan copy data
    22/07/2022 [DITA/SIER] tambah param : BudgetCategoryCodeForCrossDepartment  untuk menampung budget category yg bisa dipilih
    25/07/2022 [MYA/SIER] Approval desktop bisa melihat print out yang akan di-approve di menu Material Request
    25/07/2022 [MYA/SIER] Approval desktop bisa melihat print out yang akan di-approve di menu Material Request SPPJB
    27/07/2022 [DITA/SIER] saat copy data, docdt teteap pake current date
    01/08/2022 [BRI/SIER] merubah informasi approval
    03/08/2022 [DITA/SIER] ketika item dihapus,nilai pada Total Estimated Price tidak berkurang sesuai dengan item yang dicancel
    05/08/2022 [DITA/SIER] jika department for budget yg dipilih merupakan department itu sendiri maka budgetcategory si department itu akan otomatis muncul tanpa di set diparameter
    05/08/2022 [BRI/SIER] penyesuaian informasi approval & copy data
    08/08/2022 [BRI/SIER] BUG PIC tidak bisa refresh data
    10/08/2022 [BRI/SIER] BUG Approval information
    11/08/2022 [BRI/SIER] penyesuaian copy data
    12/08/2022 [BRI/SIER] BUG pada jabatan
    12/08/2022 [DITA/SIER] perubahan printoutbagian budget karena terkena dampak adanya penambahan dept for budget
    12/08/2022 [DITA/SIER] based on param : IsBudgetUseMRProcessInd, availbale budget hanya mengurangi MR yg sudah final saja
    15/08/2022 [MYA/SIER] BUG : Material Request ketika edit mau ubah divisi, deptnya tidak hilang
    15/08/2022 [MYA/SIER] BUG : Material Request SPPJB ketika edit mau ubah divisi, deptnya tidak hilang
    18/08/2022 [DITA/SIER] Saat insert atau copy data pada MR dan MR SPPJB, jika ada perubahan pada Dept dan Dept for Budget maka list item pada detail hilang, namun Total Estimated Price di header belum 0 
    22/08/2022 [BRI/SIER] BUG : copy data (division)
    22/08/2022 [ICA/SIER] BUG : Field Status tidak muncul ketika loop dari docapproval
    24/08/2022 [DITA/SIER] Penyesuaian printout MR & MRSPPJB terkait budget
    25/08/2022 [RDA/SIER] feedback typo di grid dan copy data
    29/08/2022 [BRI/SIER] bug approval level 5 tidak muncul di mr sppjb
    30/08/2022 [BRI/SIER] local docno 
    31/08/2022 [MYA/SIER] menambahkan source MR  yang di copy di database dan nambahin validasi boq harus ada baru bisa di save
    31/08/2022 [VIN/SIER] Setelah Download nama menu = menudesc , cancel reason hrsnya muncul setelah find 
    01/09/2022 [SET/SIER] Menyesuaikan validasi DAG di Save Doc Approval
    01/09/2022 [MYA/SIER] BUG : Tidak bisa Save Material Request
    01/09/2022 [MAU/SIER] tambah parameter ItemManagedByWhsInd 
    01/09/2022 [MYA/SIER] Saat copy data field Department tidak editable pada MR dan MR SPPJB.
    01/09/2022 [BRI/SIER] Saat insert biasa dan copy data pada MR dan MR SPPJB, field Department for Budget akan muncul sesuai group setting dan tidak tervalidasi atas Site dan Division.
    06/09/2022 [SET/PRODUCT] tambah extension file upload
    07/09/2022 [ICA/SIER] menonactive kan param IsShowForeignName, ForeignName dihide tidak berdasarkan param
    07/09/2022 [SET/PRODUCT] add extension file download
    19/09/2022 [MAU/SIER] Feedback Penyesuaian MR SPPJB ketika copy data dan mengganti header, detail tidak hilang ketika itemmanagedbywhs = N
    21/09/2022 [ICA/SIER] Penyesuaian printout, tanggal di footer ambil dari last approver (sekalian ubah yg SPPJB biar sumbernya dari tbldocapproval, awalnya dari MaterialRequestDtl)
    21/09/2022 [ICA/SIER] Bug: Penyesuaian printout SPPJB, total budget dan after used belum sesuai (disamakan dengan MR)
    09/11/2022 [RDA/SIER] mempercepat load saat membuka frm (ubah bentuk parameter)
    14/02/2023 [DITA/SIER] based on param IsUploadFileRenamed, upload file dapat di rename file nya --> ngerubah ambil method upload file dari stdmtd
    17/02/2023 [DITA/SIER] Bug ke 3 file jadi mandatory setelah ubah method upload ke stdmtd
    09/03/2023 [BRI/SIER] Bug validasi file exists
    10/04/2023 [WED/SIER] bisa edit LocalDocNo dan UploadFile dengan parameter IsLocalDocMRDraftEditable
                          bisa edit LocalDocNo dengan parameter IsLocalDocMRSPPJBDraftEditable
    18/04/2023 [SET/SIER] menyesuaikan SFD.Filter berdasar extension file download (Btn_Download)
    27/04/2023 [SET/SIER] kurang source ext (.jpg) ketika download file
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Reflection;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest5 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, IsProcFormat = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            mDroppingRequestBCCode = string.Empty,
            mReqTypeForNonBudget = string.Empty,
            mItGrpCodeNotShowOnMaterialRequest = string.Empty,
            mInitProcessInd = string.Empty,
            mBOQDlgForMRFormat = string.Empty,
            mBudgetCategoryCodeForCrossDepartment = string.Empty,
            CopyDataDocNo = string.Empty,
            mFileName1 = string.Empty,
            mFileName2 = string.Empty,
            mFileName3 = string.Empty
            ;
        internal FrmMaterialRequest5Find FrmFind;
        private bool
            mIsUseItemConsumption = false,
            mIsRemarkForApprovalMandatory = false,
            IsAutoGeneratePurchaseLocalDocNo = false,
            mIsApprovalBySiteMandatory = false,
            mIsDocNoWithDeptShortCode = false,
            mIsDeptFilterBySite = false,
            mIsShowSeqNoInMaterialRequest = false,
            mIsMaterialRequestMaxStockValidationEnabled = false,
            mIsBudgetCalculateFromEstimatedPrice = false,
            mIsMaterialRequestDocNoUseDifferentAbbr = false,
            mIsMaterialRequestSPPJBDocNoUseDifferentAbbr = false,
            mIsUsageDateMaterialRequestMandatory = false,
            mIsMaterialRequestShowReorderPoint = false,
            mIsMaterialRequestShowMinimumStock = false,
            mIsMRDurationMandatory = false,
            mIsMRApprovalByAmount = false,
            mIsCASUsedForBudget = false,
            mIsMRUseSpecification = false, 
            IsOk = false, IsOk2 = false, IsOk3 = false, 
            mIsInsert = false,
            mIsMREstimatedPriceNotEditable = false,
            mIsMaterialRequestUseDetailApprovalInformation = false,
            mIsBudgetUseMRProcessInd = false
            ;

        internal bool
            mIsSiteMandatory = false,
            mIsPICInMRMandatory = false,
            mIsFilterBySite = false,
            mIsFilterByDept = false,
            mIsFilterByItCt = false,
            mIsShowForeignName = false,
            mIsBudgetActive = false,
            mIsDORequestNeedStockValidation = false,
            mIsDORequestUseItemIssued = false,
            mIsMRAllowToUploadFile = false,
            mIsMaterialRequestForDroppingRequestEnabled = false,
            mIsBOMShowSpecifications = false,
            mIsMRSPPJBEnabled = false,
            mIsMRSPPJB = false,
            mIsMRAutomaticGetLastVendorQuotation = false,
            mIsMRBudgetBasedOnBudgetCategory = false,
            mIsRemarkMRNotMandatory = false,
            mIsMRShowEstimatedPrice = false,
            mIsMRShowTotalPrice = false,
            mIsMRShowTotalEstimatedPrice = false,
            mIsMRQuotationNotShow = false,
            mIsPICGrouped = false,
            mIsMRItemGroupNotShow = false,
            mIsMRUseDivision = false,
            mIsBudgetTransactionCanUseOtherDepartmentBudget = false,
            mIsBudget2YearlyFormat = false,
            mIsPltCheckboxNotBasedOnPosition = false,
            mItemManagedByWhsInd = false,
            mIsUploadFileRenamed = false,
            mIsLocalDocMRDraftEditable = false,
            mIsLocalDocMRSPPJBDraftEditable = false
            ;

        private string
            mBudgetBasedOn = "1",
            mIsPrintOutUseDigitalSignature = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mMRAvailableBudgetSubtraction = string.Empty,
            mMRApprovalGroupValidation = string.Empty,
            mFormatFTPClient = string.Empty;

        private string mFormPrintOutMaterialRequestWithoutConsumption = string.Empty;
        private string mFormPrintOutMaterialRequestWithConsumption = string.Empty;
        private string mFormPrintOutMaterialRequestSPPJB = string.Empty;
        private string mFormPrintOutMaterialRequestSPPJB2 = string.Empty;
        private string Doctitle = Sm.GetParameter("Doctitle");

        private iGCopyPasteManager fCopyPasteManager;

        iGCell fCell;
        bool fAccept;

        private byte[] downloadedData;
        internal List<Job> mlJob;
        internal List<Job2> mlJob2;

        #endregion

        #region Constructor

        public FrmMaterialRequest5(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0)
            {
                if (Doctitle == "IMS")
                    this.Text = "Purchase Request";
                else
                    this.Text = "Material Request";
            }
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                ExecQuery();
                GetParameter();
                if (!mIsMRUseDivision) {
                    label23.Visible = false;
                    LueDivCode.Visible = false;
                } 
                if (mIsSiteMandatory) LblSiteCode.ForeColor = Color.Red;
                if (mIsPICInMRMandatory) LblPICCode.ForeColor = Color.Red;
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo, TxtPOQtyCancelDocNo, TxtRemainingBudget, TxtDORequestDocNo }, true);
                SetLueCurCode(ref LueCurCode);
                Sl.SetLueOption(ref LueDurationUom, "DurationUom");
                SetGrd();
                LueCurCode.Visible = false;
                LueDurationUom.Visible = false;
                SetFormControl(mState.View);
                SetLuePICCode(ref LuePICCode);
                if (!mIsDeptFilterBySite) Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
                if (!mIsDeptFilterBySite) Sl.SetLueDeptCode(ref LueDeptCode2, string.Empty, mIsFilterByDept ? "Y" : "N");
                //Sl.SetLueDivisionCode(ref LueDivCode);
                SetLueDivisionCode(ref LueDivCode);
                SetLueProcessInd(ref LueProcessInd);
                Tp2.PageVisible = mIsMaterialRequestForDroppingRequestEnabled;

                mlJob = new List<Job>();
                mlJob2 = new List<Job2>();

                if (!mIsBudgetTransactionCanUseOtherDepartmentBudget)
                {
                    LblDeptCode2.Visible = LueDeptCode2.Visible = BtnBCCode.Visible = false;
                }
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = false;
                    if (Doctitle != "SIER")
                    {
                        BtnPrint.Visible = false;
                    }
                }

                if (Doctitle == "IMS")
                    label19.Text = "PR\'s Total Amount";

                if (mIsMRShowEstimatedPrice && mIsMRShowTotalEstimatedPrice) label22.Visible = TxtTotalEstPrice.Visible = true;
                else label22.Visible = TxtTotalEstPrice.Visible = false;

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Old Code
        /*
        private void GetParameter()
        {
            mMRApprovalGroupValidation = Sm.GetParameter("MRApprovalGroupValidation");
            IsProcFormat = Sm.GetParameter("ProcFormatDocNo");
            mIsRemarkForApprovalMandatory = Sm.GetParameterBoo("IsMaterialRequestRemarkForApprovalMandatory");
            IsAutoGeneratePurchaseLocalDocNo = Sm.GetParameterBoo("IsAutoGeneratePurchaseLocalDocNo");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsApprovalBySiteMandatory = Sm.GetParameterBoo("IsApprovalBySiteMandatory");
            mBudgetBasedOn = Sm.GetParameter("BudgetBasedOn");
            mIsBudgetActive = Sm.GetParameterBoo("IsBudgetActive");
            mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            mIsDocNoWithDeptShortCode = Sm.GetParameterBoo("IsDocNoWithDeptShortCode");
            mIsDORequestNeedStockValidation = Sm.GetParameterBoo("IsDORequestNeedStockValidation");
            mIsDORequestUseItemIssued = Sm.GetParameterBoo("IsDORequestUseItemIssued");
            mIsMRShowEstimatedPrice = Sm.GetParameterBoo("IsMRShowEstimatedPrice");
            mIsUseItemConsumption = Sm.GetParameterBoo("IsUseItemConsumption");
            mIsPICInMRMandatory = Sm.GetParameterBoo("IsPICInMRMandatory");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsMRAutomaticGetLastVendorQuotation = Sm.GetParameterBoo("IsMRAutomaticGetLastVendorQuotation");
            mIsMaterialRequestShowReorderPoint = Sm.GetParameterBoo("IsMaterialRequestShowReorderPoint");
            mIsMaterialRequestShowMinimumStock = Sm.GetParameterBoo("IsMaterialRequestShowMinimumStock");
            mIsBudget2YearlyFormat = Sm.GetParameterBoo("IsBudget2YearlyFormat");
            mIsRemarkMRNotMandatory = Sm.GetParameterBoo("IsRemarkMRNotMandatory");
            mIsMRQuotationNotShow = Sm.GetParameterBoo("IsMRQuotationNotShow");
            mIsMRItemGroupNotShow = Sm.GetParameterBoo("IsMRItemGroupNotShow");
            mIsMRUseSpecification = Sm.GetParameterBoo("IsMRUseSpecification");
            mIsUsageDateMaterialRequestMandatory = Sm.GetParameterBoo("IsUsageDateMaterialRequestMandatory");
            mIsPICGrouped = Sm.GetParameterBoo("IsPICGrouped");
            mIsMREstimatedPriceNotEditable = Sm.GetParameterBoo("IsMREstimatedPriceNotEditable");
            mIsMaterialRequestUseDetailApprovalInformation = Sm.GetParameterBoo("IsMaterialRequestUseDetailApprovalInformation");

            if (mIsSiteMandatory)
            {
                mIsDeptFilterBySite = Sm.IsDataExist(
                    "Select 1 From TblDepartment A, TblDepartmentBudgetSite B " +
                    "Where A.DeptCode=B.DeptCode And A.ActInd='Y' " +
                    "And Exists(Select 1 from TblParameter " +
                    "Where ParCode='IsBudgetActive' And ParValue='Y') " +
                    "Limit 1;");
            }

            mIsPrintOutUseDigitalSignature = Sm.GetParameter("IsPrintOutUseDigitalSignature");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mIsMRAllowToUploadFile = Sm.GetParameterBoo("IsMRAllowToUploadFile");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsShowSeqNoInMaterialRequest = Sm.GetParameterBoo("IsShowSeqNoInMaterialRequest");
            mIsMaterialRequestMaxStockValidationEnabled = Sm.GetParameterBoo("IsMaterialRequestMaxStockValidationEnabled");
            mMRAvailableBudgetSubtraction = Sm.GetParameter("MRAvailableBudgetSubtraction");
            mIsMaterialRequestForDroppingRequestEnabled = Sm.GetParameterBoo("IsMaterialRequestForDroppingRequestEnabled");
            mIsMRSPPJBEnabled = Sm.GetParameterBoo("IsMRSPPJBEnabled");
            if (mIsMRSPPJBEnabled) mIsMRSPPJB = Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForMRSPPJB"));
            mIsMRBudgetBasedOnBudgetCategory = Sm.GetParameterBoo("IsMRBudgetBasedOnBudgetCategory");
            mIsBudgetCalculateFromEstimatedPrice = Sm.GetParameterBoo("IsBudgetCalculateFromEstimatedPrice");
            mIsMaterialRequestDocNoUseDifferentAbbr = Sm.GetParameterBoo("IsMaterialRequestDocNoUseDifferentAbbr");
            mIsMaterialRequestSPPJBDocNoUseDifferentAbbr = Sm.GetParameterBoo("IsMaterialRequestSPPJBDocNoUseDifferentAbbr");
            mIsMRShowTotalPrice = Sm.GetParameterBoo("IsMRShowTotalPrice");

            mItGrpCodeNotShowOnMaterialRequest = Sm.GetParameter("ItGrpCodeNotShowOnMaterialRequest");

            mFormPrintOutMaterialRequestWithoutConsumption = Sm.GetParameter("FormPrintOutMaterialRequestWithoutConsumption");
            mFormPrintOutMaterialRequestWithConsumption = Sm.GetParameter("FormPrintOutMaterialRequestWithConsumption");
            mFormPrintOutMaterialRequestSPPJB = Sm.GetParameter("FormPrintOutMaterialRequestSPPJB");
            mFormPrintOutMaterialRequestSPPJB2 = Sm.GetParameter("FormPrintOutMaterialRequestSPPJB2");
            mIsMRDurationMandatory = Sm.GetParameterBoo("IsMRDurationMandatory");
            mIsMRApprovalByAmount = Sm.GetParameterBoo("IsMRApprovalByAmount");

            mIsCASUsedForBudget = Sm.GetParameterBoo("IsCASUsedForBudget");
            mIsMRShowTotalEstimatedPrice = Sm.GetParameterBoo("IsMRShowTotalEstimatedPrice");
            mIsMRUseDivision = Sm.GetParameterBoo("IsMRUseDivision");
            mBOQDlgForMRFormat = Sm.GetParameter("BOQDlgForMRFormat");
            mIsBudgetTransactionCanUseOtherDepartmentBudget = Sm.GetParameterBoo("IsBudgetTransactionCanUseOtherDepartmentBudget");
            mBudgetCategoryCodeForCrossDepartment = Sm.GetParameter("BudgetCategoryCodeForCrossDepartment");
            mIsPltCheckboxNotBasedOnPosition = Sm.GetParameterBoo("IsPltCheckboxNotBasedOnPosition");
            mIsBudgetUseMRProcessInd = Sm.GetParameterBoo("IsBudgetUseMRProcessInd");

            if (mFormPrintOutMaterialRequestWithoutConsumption.Length == 0) mFormPrintOutMaterialRequestWithoutConsumption = "MaterialRequest";
            if (mFormPrintOutMaterialRequestWithConsumption.Length == 0) mFormPrintOutMaterialRequestWithConsumption = "MaterialRequest2";

            mItemManagedByWhsInd = Sm.GetParameterBoo("ItemManagedByWhsInd");
        }
        */
        #endregion

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'MRApprovalGroupValidation','ProcFormatDocNo','ReqTypeForNonBudget','BudgetBasedOn','IsPrintOutUseDigitalSignature', ");
            SQL.AppendLine("'HostAddrForFTPClient','SharedFolderForFTPClient','UsernameForFTPClient','PasswordForFTPClient','PortForFTPClient', ");
            SQL.AppendLine("'FileSizeMaxUploadFTPClient','MRAvailableBudgetSubtraction','ItGrpCodeNotShowOnMaterialRequest','FormPrintOutMaterialRequestWithoutConsumption','FormPrintOutMaterialRequestWithConsumption', ");
            SQL.AppendLine("'FormPrintOutMaterialRequestSPPJB','FormPrintOutMaterialRequestSPPJB2','BOQDlgForMRFormat','BudgetCategoryCodeForCrossDepartment','IsMaterialRequestRemarkForApprovalMandatory', ");
            SQL.AppendLine("'IsAutoGeneratePurchaseLocalDocNo','IsFilterByItCt','IsFilterByDept','IsFilterBySite','IsSiteMandatory', ");
            SQL.AppendLine("'IsShowForeignName','IsApprovalBySiteMandatory','IsBudgetActive','IsDocNoWithDeptShortCode','IsDORequestNeedStockValidation', ");
            SQL.AppendLine("'IsDORequestUseItemIssued','IsMRShowEstimatedPrice','IsUseItemConsumption','IsPICInMRMandatory','IsBOMShowSpecifications', ");
            SQL.AppendLine("'IsMRAutomaticGetLastVendorQuotation','IsMaterialRequestShowReorderPoint','IsMaterialRequestShowMinimumStock','IsBudget2YearlyFormat','IsRemarkMRNotMandatory', ");
            SQL.AppendLine("'IsMRQuotationNotShow','IsMRItemGroupNotShow','IsMRUseSpecification','IsUsageDateMaterialRequestMandatory','IsPICGrouped', ");
            SQL.AppendLine("'IsMREstimatedPriceNotEditable','IsMaterialRequestUseDetailApprovalInformation','IsMRAllowToUploadFile','IsShowSeqNoInMaterialRequest','IsMaterialRequestMaxStockValidationEnabled', ");
            SQL.AppendLine("'IsMaterialRequestForDroppingRequestEnabled','IsMRSPPJBEnabled','IsMRBudgetBasedOnBudgetCategory','IsBudgetCalculateFromEstimatedPrice','IsMaterialRequestDocNoUseDifferentAbbr', ");
            SQL.AppendLine("'IsMaterialRequestSPPJBDocNoUseDifferentAbbr','IsMRShowTotalPrice','IsMRDurationMandatory','IsMRApprovalByAmount','IsCASUsedForBudget', ");
            SQL.AppendLine("'IsMRShowTotalEstimatedPrice','IsMRUseDivision','IsBudgetTransactionCanUseOtherDepartmentBudget','IsPltCheckboxNotBasedOnPosition','IsBudgetUseMRProcessInd','ItemManagedByWhsInd', 'IsUploadFileRenamed', 'FormatFTPClient', 'IsLocalDocMRDraftEditable', 'IsLocalDocMRSPPJBDraftEditable' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsMaterialRequestRemarkForApprovalMandatory": mIsRemarkForApprovalMandatory = ParValue == "Y"; break;
                            case "IsAutoGeneratePurchaseLocalDocNo": IsAutoGeneratePurchaseLocalDocNo = ParValue == "Y"; break;
                            case "IsFilterByItCt": mIsFilterByItCt = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsSiteMandatory": mIsSiteMandatory = ParValue == "Y"; break;
                            case "IsShowForeignName": mIsShowForeignName = ParValue == "Y"; break;
                            case "IsApprovalBySiteMandatory": mIsApprovalBySiteMandatory = ParValue == "Y"; break;
                            case "IsBudgetActive": mIsBudgetActive = ParValue == "Y"; break;
                            case "IsDocNoWithDeptShortCode": mIsDocNoWithDeptShortCode = ParValue == "Y"; break;
                            case "IsDORequestNeedStockValidation": mIsDORequestNeedStockValidation = ParValue == "Y"; break;
                            case "IsDORequestUseItemIssued": mIsDORequestUseItemIssued = ParValue == "Y"; break;
                            case "IsMRShowEstimatedPrice": mIsMRShowEstimatedPrice = ParValue == "Y"; break;
                            case "IsUseItemConsumption": mIsUseItemConsumption = ParValue == "Y"; break;
                            case "IsPICInMRMandatory": mIsPICInMRMandatory = ParValue == "Y"; break;
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsMRAutomaticGetLastVendorQuotation": mIsMRAutomaticGetLastVendorQuotation = ParValue == "Y"; break;
                            case "IsMaterialRequestShowReorderPoint": mIsMaterialRequestShowReorderPoint = ParValue == "Y"; break;
                            case "IsMaterialRequestShowMinimumStock": mIsMaterialRequestShowMinimumStock = ParValue == "Y"; break;
                            case "IsBudget2YearlyFormat": mIsBudget2YearlyFormat = ParValue == "Y"; break;
                            case "IsRemarkMRNotMandatory": mIsRemarkMRNotMandatory = ParValue == "Y"; break;
                            case "IsMRQuotationNotShow": mIsMRQuotationNotShow = ParValue == "Y"; break;
                            case "IsMRItemGroupNotShow": mIsMRItemGroupNotShow = ParValue == "Y"; break;
                            case "IsMRUseSpecification": mIsMRUseSpecification = ParValue == "Y"; break;
                            case "IsUsageDateMaterialRequestMandatory": mIsUsageDateMaterialRequestMandatory = ParValue == "Y"; break;
                            case "IsPICGrouped": mIsPICGrouped = ParValue == "Y"; break;
                            case "IsMREstimatedPriceNotEditable": mIsMREstimatedPriceNotEditable = ParValue == "Y"; break;
                            case "IsMaterialRequestUseDetailApprovalInformation": mIsMaterialRequestUseDetailApprovalInformation = ParValue == "Y"; break;
                            case "IsMRAllowToUploadFile": mIsMRAllowToUploadFile = ParValue == "Y"; break;
                            case "IsShowSeqNoInMaterialRequest": mIsShowSeqNoInMaterialRequest = ParValue == "Y"; break;
                            case "IsMaterialRequestMaxStockValidationEnabled": mIsMaterialRequestMaxStockValidationEnabled = ParValue == "Y"; break;
                            case "IsMaterialRequestForDroppingRequestEnabled": mIsMaterialRequestForDroppingRequestEnabled = ParValue == "Y"; break;
                            case "IsMRSPPJBEnabled": mIsMRSPPJBEnabled = ParValue == "Y"; break;
                            case "IsMRBudgetBasedOnBudgetCategory": mIsMRBudgetBasedOnBudgetCategory = ParValue == "Y"; break;
                            case "IsBudgetCalculateFromEstimatedPrice": mIsBudgetCalculateFromEstimatedPrice = ParValue == "Y"; break;
                            case "IsMaterialRequestDocNoUseDifferentAbbr": mIsMaterialRequestDocNoUseDifferentAbbr = ParValue == "Y"; break;
                            case "IsMaterialRequestSPPJBDocNoUseDifferentAbbr": mIsMaterialRequestSPPJBDocNoUseDifferentAbbr = ParValue == "Y"; break;
                            case "IsMRShowTotalPrice": mIsMRShowTotalPrice = ParValue == "Y"; break;
                            case "IsMRDurationMandatory": mIsMRDurationMandatory = ParValue == "Y"; break;
                            case "IsMRApprovalByAmount": mIsMRApprovalByAmount = ParValue == "Y"; break;
                            case "IsCASUsedForBudget": mIsCASUsedForBudget = ParValue == "Y"; break;
                            case "IsMRShowTotalEstimatedPrice": mIsMRShowTotalEstimatedPrice = ParValue == "Y"; break;
                            case "IsMRUseDivision": mIsMRUseDivision = ParValue == "Y"; break;
                            case "IsBudgetTransactionCanUseOtherDepartmentBudget": mIsBudgetTransactionCanUseOtherDepartmentBudget = ParValue == "Y"; break;
                            case "IsPltCheckboxNotBasedOnPosition": mIsPltCheckboxNotBasedOnPosition = ParValue == "Y"; break;
                            case "IsBudgetUseMRProcessInd": mIsBudgetUseMRProcessInd = ParValue == "Y"; break;
                            case "ItemManagedByWhsInd": mItemManagedByWhsInd = ParValue == "Y"; break;
                            case "IsUploadFileRenamed": mIsUploadFileRenamed = ParValue == "Y"; break;
                            case "IsLocalDocMRDraftEditable": mIsLocalDocMRDraftEditable = ParValue == "Y"; break;
                            case "IsLocalDocMRSPPJBDraftEditable": mIsLocalDocMRSPPJBDraftEditable = ParValue == "Y"; break;

                            //string
                            case "MRApprovalGroupValidation": mMRApprovalGroupValidation = ParValue; break;
                            case "ProcFormatDocNo": IsProcFormat = ParValue; break;
                            case "ReqTypeForNonBudget": mReqTypeForNonBudget = ParValue; break;
                            case "BudgetBasedOn": mBudgetBasedOn = ParValue; break;
                            case "IsPrintOutUseDigitalSignature": mIsPrintOutUseDigitalSignature = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "MRAvailableBudgetSubtraction": mMRAvailableBudgetSubtraction = ParValue; break;
                            case "ItGrpCodeNotShowOnMaterialRequest": mItGrpCodeNotShowOnMaterialRequest = ParValue; break;
                            case "FormPrintOutMaterialRequestWithoutConsumption": mFormPrintOutMaterialRequestWithoutConsumption = ParValue; break;
                            case "FormPrintOutMaterialRequestWithConsumption": mFormPrintOutMaterialRequestWithConsumption = ParValue; break;
                            case "FormPrintOutMaterialRequestSPPJB": mFormPrintOutMaterialRequestSPPJB = ParValue; break;
                            case "FormPrintOutMaterialRequestSPPJB2": mFormPrintOutMaterialRequestSPPJB2 = ParValue; break;
                            case "BOQDlgForMRFormat": mBOQDlgForMRFormat = ParValue; break;
                            case "BudgetCategoryCodeForCrossDepartment": mBudgetCategoryCodeForCrossDepartment = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }

            if (mIsSiteMandatory)
            {
                mIsDeptFilterBySite = Sm.IsDataExist(
                    "Select 1 From TblDepartment A, TblDepartmentBudgetSite B " +
                    "Where A.DeptCode=B.DeptCode And A.ActInd='Y' " +
                    "And Exists(Select 1 from TblParameter " +
                    "Where ParCode='IsBudgetActive' And ParValue='Y') " +
                    "Limit 1;");
            }

            if (mIsMRSPPJBEnabled) mIsMRSPPJB = Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForMRSPPJB"));
            if (mFormPrintOutMaterialRequestWithoutConsumption.Length == 0) mFormPrintOutMaterialRequestWithoutConsumption = "MaterialRequest";
            if (mFormPrintOutMaterialRequestWithConsumption.Length == 0) mFormPrintOutMaterialRequestWithConsumption = "MaterialRequest2";
            if (mFormatFTPClient.Length == 0) mFormatFTPClient = "1";
        }

        private void SetGrd()
        {
            string Doctitle = Sm.GetParameter("DocTitle");

            Grd1.Cols.Count = 41;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                       "Cancel",
                        "Old Cancel",
                        "Cancel Reason",
                        "",
                        "Status",
                        
                        
                        //6-10
                        "Checked by",
                        "",
                        "Item's Code",
                        "Local Code",
                        "",
                        
                        //11-15
                        "Item's Name",
                        "Foreign Name",
                        "Item Sub Category code",
                        "Sub Category",
                        "Minimum Stock",
                       
                        //16-20
                        "Reoder Point",
                        "Quantity",
                        "UoM",
                        "Usage Date",
                        "Quotation#",
                        
                        //21-25
                        "Quotation Dno",
                        "Quotation Date",
                        "Price",
                        "Total",
                        "Remark",

                        //26-30
                        "DORequestDocNo",
                        "DORequestDNo",
                        "Currency",
                        "Estimated Price",
                        "No.",

                        //31-35
                        "Dropping Request Quantity",
                        "Dropping Request's"+Environment.NewLine+"Amount",
                        "Specification",
                        "BOQ",
                        "Duration",

                        //36-40
                        "Duration UOM Code",
                        "Duration UOM",
                        "Total Price",
                        "Specification of Item",
                        "Location"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        50, 50, 200, 20, 80, 
                        
                        //6-10
                        100, 20, 100, 150, 20,  
                        
                        //11-15
                        230, 200, 80, 100, 100, 
                        
                        //16-20
                        100, 120, 100, 100, 130,  
                        
                        //21-25
                        80, 100, 150, 150, 400,

                        //26-30
                        0, 0, 100, 120, 50,

                        //31-35
                        0, 130, 300, 50, 80,

                        //36-40
                        0, 150, 150, 150, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 15, 16, 17, 23, 24, 29, 31, 32, 35, 38 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 4, 7, 10, 34 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 19, 22 });
            Sm.GrdColInvisible(Grd1, new int[] {  15, 16, 32, 36 }, false);

            if (!mIsMRShowTotalPrice)
                Sm.GrdColInvisible(Grd1, new int[] { 38 });

            if (!mIsDORequestNeedStockValidation && !mIsDORequestUseItemIssued)
                Sm.GrdColInvisible(Grd1, new int[] { 7 });

            if (!mIsMRShowEstimatedPrice)
                Sm.GrdColInvisible(Grd1, new int[] { 28, 29 });

            #region Use Param IsShowForeignName - di non active kan
            //if (mIsShowForeignName)
            //{
            //    if (IsProcFormat == "1")
            //        Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 6, 8, 9, 10, 21, 22, 23, 24, 13 }, false);
            //    else
            //        Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 6, 8, 9, 10, 21, 22, 23, 24, 13, 14 }, false);
            //}
            //else
            //{
            //    if (IsProcFormat == "1")
            //        Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 6, 8, 9, 10, 12, 13, 21, 22, 23, 24 }, false);
            //    else
            //        Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 6, 8, 9, 10, 12, 13, 14, 21, 22, 23, 24 }, false);
            //}
            #endregion

            if (IsProcFormat == "1")
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 6, 8, 9, 10, 12, 13, 21, 22, 23, 24 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 6, 8, 9, 10, 12, 13, 14, 21, 22, 23, 24 }, false);

            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 30, 31, 32, 33, 36, 38 });
            Sm.GrdColInvisible(Grd1, new int[] { 30 }, mIsShowSeqNoInMaterialRequest);
            fCopyPasteManager = new iGCopyPasteManager(Grd1);
            Grd1.Cols[28].Move(25);
            Grd1.Cols[29].Move(26);
            Grd1.Cols[38].Move(27);
            Grd1.Cols[30].Move(0);
            Grd1.Cols[5].Move(7);
            Grd1.Cols[40].Move(13);
            Grd1.Cols[33].Move(14);
            


            if (Doctitle == "SIER")
            {
                if (!mIsMaterialRequestForDroppingRequestEnabled && !mIsMRSPPJB)
                    Sm.GrdColInvisible(Grd1, new int[] { 32 });

            }
            else
            {
                if (mIsMaterialRequestForDroppingRequestEnabled)
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 23, 24, 32 }, true);
                    Grd1.Cols[32].Move(26);
                }
            }
            if (!mIsMRSPPJB)
            {
                if (!mIsMaterialRequestShowMinimumStock)
                    Sm.GrdColInvisible(Grd1, new int[] { 15 });
                if (!mIsMaterialRequestShowReorderPoint)
                    Sm.GrdColInvisible(Grd1, new int[] { 16 });
            }
            if (!mIsBOMShowSpecifications) 
                Sm.GrdColInvisible(Grd1, new int[] { 33 });

            if (!mIsMRSPPJB)
                Sm.GrdColInvisible(Grd1, new int[] { 34, 40 }, false);
            else
                Grd1.Cols[34].Move(28);

            if (mIsMRQuotationNotShow)
                Sm.GrdColInvisible(Grd1, new int[] { 20 });

            if (!mIsUsageDateMaterialRequestMandatory)
                Grd1.Cols[19].Visible = false;

            if (!mIsMRUseSpecification)
                Sm.GrdColInvisible(Grd1, new int[] { 39 });
            else
                Grd1.Cols[39].Move(14);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 8, 9, 10, 15, 16, 20, 22, 32 }, !ChkHideInfoInGrd.Checked);
            if (!mIsUsageDateMaterialRequestMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 19 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtLocalDocNo, DteDocDt, LueReqType, LueDeptCode, LueBCCode, 
                       LueSiteCode, MeeRemark, TxtFile, TxtFile2, TxtFile3, LuePICCode, LueProcessInd, TxtTotalEstPrice, LueDivCode,
                       LueDeptCode2
                    }, true);
                    BtnPOQtyCancelDocNo.Enabled = false;
                    BtnDORequestDocNo.Enabled = false;
                    BtnDroppingRequestDocNo.Enabled = false;
                    if (!mIsDORequestNeedStockValidation)
                        LblDORequestDocNo.ForeColor = Color.Red;
                    else
                        LblDORequestDocNo.ForeColor = Color.Black;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 28, 29, 33, 35, 37, 39, 40 });
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    BtnFile2.Enabled = false;
                    BtnDownload2.Enabled = false;
                    BtnFile3.Enabled = false;
                    BtnDownload3.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsMRAllowToUploadFile)
                        BtnDownload.Enabled = true;
                    ChkFile.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsMRAllowToUploadFile)
                        BtnDownload2.Enabled = true;
                    ChkFile2.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsMRAllowToUploadFile)
                        BtnDownload3.Enabled = true;
                    ChkFile3.Enabled = false;
                    BtnBCCode.Enabled = false;
                    LblCopyGeneral.Visible = false;
                    BtnCopyGeneral.Visible = false;
                    BtnCopyGeneral.Enabled = false;
                    if (TxtDocNo.Text.Length > 0)
                        Sm.GrdColInvisible(Grd1, new int[] { 3 }, true);
                    TxtDocNo.Focus();
                    
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDeptCode, LueDeptCode2 }, mIsDeptFilterBySite);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueReqType, LueSiteCode, MeeRemark, LuePICCode }, false);
                    if (!IsAutoGeneratePurchaseLocalDocNo) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);
                    BtnPOQtyCancelDocNo.Enabled = true;
                    if (!mIsDORequestNeedStockValidation && !mIsDORequestUseItemIssued)
                        LblDORequestDocNo.ForeColor = Color.Red;
                    else
                        LblDORequestDocNo.ForeColor = Color.Black;
                    if (!mIsDORequestNeedStockValidation) BtnDORequestDocNo.Enabled = true;
                    Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 7, 17, 19, 25, 28, 29, 35, 37, 39, 40 });
                    if (mIsMREstimatedPriceNotEditable && mIsMRSPPJB)
                        Sm.GrdColReadOnly(true, true, Grd1, new int[] { 29 });
                    if (mIsMRAllowToUploadFile)
                    {
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = true;
                        BtnFile2.Enabled = true;
                        BtnDownload2.Enabled = true;
                        BtnFile3.Enabled = true;
                        BtnDownload3.Enabled = true;
                    }
                    ChkFile.Enabled = true;
                    ChkFile2.Enabled = true;
                    ChkFile3.Enabled = true;
                    BtnDroppingRequestDocNo.Enabled = true;
                    BtnBCCode.Enabled = true;
                    LblCopyGeneral.Visible = true;
                    BtnCopyGeneral.Visible = true;
                    BtnCopyGeneral.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    if (Sm.IsDataExist("Select 1 From TblMaterialRequestHdr Where DocNo = @Param And ProcessInd = 'D';", TxtDocNo.Text))
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDeptCode, LueDeptCode2 }, mIsDeptFilterBySite);
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueReqType, LueSiteCode, MeeRemark, LuePICCode, LueProcessInd, LueDivCode }, false);
                        BtnPOQtyCancelDocNo.Enabled = true;
                        if (!mIsDORequestNeedStockValidation && !mIsDORequestUseItemIssued)
                            LblDORequestDocNo.ForeColor = Color.Red;
                        else
                            LblDORequestDocNo.ForeColor = Color.Black;
                        if (!mIsDORequestNeedStockValidation) BtnDORequestDocNo.Enabled = true;
                        Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 7, 17, 19, 25, 28, 29, 35, 37, 39, 40 });
                        if (mIsMRAllowToUploadFile)
                        {
                            BtnFile.Enabled = true;
                            BtnDownload.Enabled = true;
                            BtnFile2.Enabled = true;
                            BtnDownload2.Enabled = true;
                            BtnFile3.Enabled = true;
                            BtnDownload3.Enabled = true;
                        }
                        ChkFile.Enabled = true;
                        ChkFile2.Enabled = true;
                        ChkFile3.Enabled = true;
                        BtnDroppingRequestDocNo.Enabled = true;
                        BtnBCCode.Enabled = true;
                        if (TxtDocNo.Text.Length > 0)
                        {
                            string DivCode = Sm.GetLue(LueDivCode);
                            SetLueDivisionCode(ref LueDivCode);
                            Sm.SetLue(LueDivCode, DivCode);
                        }

                        if (mIsMRSPPJB)
                        {
                            if (mIsLocalDocMRSPPJBDraftEditable)
                                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);
                        }
                        else
                        {
                            if (mIsLocalDocMRDraftEditable)
                            {
                                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);
                            }   
                        }

                    }
                    else
                    {

                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3 });
                        Sm.GrdColInvisible(Grd1, new int[] { 3 }, true);
                        if (!mIsDORequestNeedStockValidation)
                            LblDORequestDocNo.ForeColor = Color.Red;
                        else
                            LblDORequestDocNo.ForeColor = Color.Black;
                    }
                    if (mIsMREstimatedPriceNotEditable && mIsMRSPPJB)
                        Sm.GrdColReadOnly(true, true, Grd1, new int[] { 29 });
                    LblCopyGeneral.Visible = false;
                    BtnCopyGeneral.Visible = false;
                    BtnCopyGeneral.Enabled = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            if (mIsMRSPPJB)
            {
                mlJob.Clear();
                mlJob2.Clear();
            }
            mDroppingRequestBCCode = string.Empty;
            mInitProcessInd = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtLocalDocNo, DteDocDt, LueReqType, LueDeptCode, TxtFile,
                LueSiteCode, TxtPOQtyCancelDocNo, LueBCCode, MeeRemark, TxtDORequestDocNo, 
                LuePICCode, TxtDroppingRequestDocNo, TxtDR_DeptCode, TxtDR_Yr, TxtDR_Mth, 
                TxtDR_PRJIDocNo, TxtDR_BCCode, MeeDR_Remark, TxtFile2, TxtFile3, LueProcessInd, LueDivCode,
                LueDeptCode2
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtRemainingBudget, TxtDR_DroppingRequestAmt, TxtDR_MRAmt, TxtDR_Balance, TxtTotalEstPrice
            }, 0);
            ClearGrd();
            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ChkFile2.Checked = false;
            PbUpload2.Value = 0;
            ChkFile3.Checked = false;
            PbUpload3.Value = 0;
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 15, 16, 17, 23, 24, 29, 31, 32, 35 });
            if (mIsBudgetActive) Grd1.Cells[0, 11].ForeColor = Color.Black;
            SetSeqNo();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmMaterialRequest5Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            mIsInsert = true;
            SetLuePICCode(ref LuePICCode);
            InsertData();
        }

        private void InsertData()
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueProcessInd, "D");
                SetLueReqType(ref LueReqType, string.Empty);
                if (mReqTypeForNonBudget.Length > 0) Sm.SetLue(LueReqType, mReqTypeForNonBudget);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySite ? "Y" : "N");
                if (!mIsDeptFilterBySite) Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
                if(mIsBudgetTransactionCanUseOtherDepartmentBudget)
                    if (!mIsDeptFilterBySite) Sl.SetLueDeptCode(ref LueDeptCode2, string.Empty, mIsFilterByDept ? "Y" : "N");
                //SetLueSiteCode(ref LueSiteCode, string.Empty);
                //if (DeptCode.Length>0) 
                //    Sm.SetLue(LueDeptCode, DeptCode);
                //else
                //{
                //    if (!mIsDeptFilterBySite)
                //        Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
                //}
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            mIsInsert = true;
            SetFormControl(mState.Edit);
            mInitProcessInd = Sm.GetValue("Select ProcessInd From TblMaterialRequestHdr Where DocNo = @Param;", TxtDocNo.Text);
            mFileName1 = TxtFile.Text;
            mFileName2 = TxtFile2.Text;
            mFileName3 = TxtFile3.Text;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || (Sm.GetParameter("DocTitle") == "KIM" && ShowPrintApproval()) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            ParPrint(TxtDocNo.Text);
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            FileInfo toDownload = null;
            string FileName = TxtFile.Text;
            toDownload = new FileInfo(string.Format(@"{0}", FileName));
            string fExt = Path.GetExtension(toDownload.Name);
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;
            if (fExt == ".pdf")
                SFD.Filter = "PDF files(*.pdf) | *.pdf";
            else if (fExt == ".rar" || fExt == ".zip")
                SFD.Filter = "rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
            else if (fExt == ".PNG" || fExt == ".JPG" || fExt == ".JPEG" || fExt == ".jpg" || fExt == ".png" || fExt == ".jpeg")
                SFD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG";
            else if (fExt == ".doc" || fExt == ".docx")
                SFD.Filter = "Word files (*.doc;*docx)|*.doc;*docx";
            else if (fExt == ".doc" || fExt == ".docx")
                SFD.Filter = "Word files (*.doc;*docx)|*.doc;*docx";
            else if (fExt == ".xls" || fExt == ".xlsx" || fExt == ".csv")
                SFD.Filter = "Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv";
            else if (fExt == ".txt")
                SFD.Filter = "Text files (*.txt)|*.txt";
            else if (fExt == ".ppt" || fExt == ".pptx")
                SFD.Filter = "Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
            //SFD.Filter = "PDF files(*.pdf) | *.pdf" +
            //        "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
            //        "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
            //        "|Word files (*.doc;*docx)|*.doc;*docx" +
            //        "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
            //        "|Text files (*.txt)|*.txt" +
            //        "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            FileInfo toDownload = null;
            string FileName = TxtFile.Text;
            toDownload = new FileInfo(string.Format(@"{0}", FileName));
            string fExt = Path.GetExtension(toDownload.Name);
            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;
            if (fExt == ".pdf")
                SFD.Filter = "PDF files(*.pdf) | *.pdf";
            else if (fExt == ".rar" || fExt == ".zip")
                SFD.Filter = "rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
            else if (fExt == ".PNG" || fExt == ".JPG" || fExt == ".JPEG")
                SFD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG";
            else if (fExt == ".doc" || fExt == ".docx")
                SFD.Filter = "Word files (*.doc;*docx)|*.doc;*docx";
            else if (fExt == ".doc" || fExt == ".docx")
                SFD.Filter = "Word files (*.doc;*docx)|*.doc;*docx";
            else if (fExt == ".xls" || fExt == ".xlsx" || fExt == ".csv")
                SFD.Filter = "Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv";
            else if (fExt == ".txt")
                SFD.Filter = "Text files (*.txt)|*.txt";
            else if (fExt == ".ppt" || fExt == ".pptx")
                SFD.Filter = "Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
            //SFD.Filter = "PDF files(*.pdf) | *.pdf" +
            //        "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
            //        "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
            //        "|Word files (*.doc;*docx)|*.doc;*docx" +
            //        "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
            //        "|Text files (*.txt)|*.txt" +
            //        "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {
            FileInfo toDownload = null;
            string FileName = TxtFile.Text;
            toDownload = new FileInfo(string.Format(@"{0}", FileName));
            string fExt = Path.GetExtension(toDownload.Name);
            DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;
            if (fExt == ".pdf")
                SFD.Filter = "PDF files(*.pdf) | *.pdf";
            else if (fExt == ".rar" || fExt == ".zip")
                SFD.Filter = "rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
            else if (fExt == ".PNG" || fExt == ".JPG" || fExt == ".JPEG")
                SFD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG";
            else if (fExt == ".doc" || fExt == ".docx")
                SFD.Filter = "Word files (*.doc;*docx)|*.doc;*docx";
            else if (fExt == ".doc" || fExt == ".docx")
                SFD.Filter = "Word files (*.doc;*docx)|*.doc;*docx";
            else if (fExt == ".xls" || fExt == ".xlsx" || fExt == ".csv")
                SFD.Filter = "Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv";
            else if (fExt == ".txt")
                SFD.Filter = "Text files (*.txt)|*.txt";
            else if (fExt == ".ppt" || fExt == ".pptx")
                SFD.Filter = "Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
            //SFD.Filter = "PDF files(*.pdf) | *.pdf" +
            //        "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
            //        "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
            //        "|Word files (*.doc;*docx)|*.doc;*docx" +
            //        "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
            //        "|Text files (*.txt)|*.txt" +
            //        "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0 || (TxtDocNo.Text.Length != 0 && mInitProcessInd == "D"))
                {
                    if (mIsMRSPPJB && e.RowIndex != 0)
                    {
                        e.DoDefault = false;
                        return;
                    }
                    if (e.ColIndex == 7 && !Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                            Sm.FormShowDialog(new FrmMaterialRequest5Dlg(this, Sm.GetLue(LueReqType), Sm.GetLue(LueDeptCode)));
                    }

                    if (e.ColIndex == 17 && TxtDroppingRequestDocNo.Text.Length > 0)
                        e.DoDefault = false;

                    if (Sm.IsGrdColSelected(new int[] { 3, 17, 19, 25 }, e.ColIndex))
                    {
                        if (e.ColIndex == 19) Sm.DteRequestEdit(Grd1, DteUsageDt, ref fCell, ref fAccept, e);
                        if (mIsDORequestNeedStockValidation)
                        {
                            if (!(mIsMRSPPJB && Grd1.Rows.Count > 1))
                                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        }
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 17, 23, 24, 29, 31, 32 });
                    }

                    if (e.ColIndex == 28)
                    {
                        LueRequestEdit(Grd1, LueCurCode, ref fCell, ref fAccept, e);
                        if (mIsDORequestNeedStockValidation)
                        {
                            if (!(mIsMRSPPJB && Grd1.Rows.Count > 1))
                                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        }
                    }

                    if (e.ColIndex == 37)
                    {
                        LueRequestEdit(Grd1, LueDurationUom, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);

                    }

                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0) ShowMRApprovalInfo(e.RowIndex);
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
            if (e.ColIndex == 34 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                if (mBOQDlgForMRFormat == "2")
                {
                    Sm.FormShowDialog(new FrmMaterialRequest5Dlg7(
                        this,
                        e.RowIndex,
                        (TxtDocNo.Text.Length == 0 || (TxtDocNo.Text.Length != 0 && mInitProcessInd == "D")) && BtnSave.Enabled
                        ));
                }
                else
                {
                    Sm.FormShowDialog(new FrmMaterialRequest5Dlg5(
                        this,
                        e.RowIndex,
                        (TxtDocNo.Text.Length == 0 || (TxtDocNo.Text.Length != 0 && mInitProcessInd == "D")) && BtnSave.Enabled
                        ));
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            //diremark oleh TKG on 14/8/2018
            //if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            //if (mIsDORequestNeedStockValidation)
            //{
            if (TxtDocNo.Text.Length == 0 || (TxtDocNo.Text.Length != 0 && mInitProcessInd == "D"))
            {
                if (mIsMRSPPJB)
                {
                    var ItCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
                    mlJob.RemoveAll(x => Sm.CompareStr(x.ItCode, ItCode));
                    mlJob2.RemoveAll(x => Sm.CompareStr(x.ItCode, ItCode));
                }
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeRemainingBudget();
                ComputeDR_Balance();
                ComputeTotalEstPrice();
                SetSeqNo();
            }
            //}
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e) //
        {
            if (mIsMRSPPJB && e.RowIndex != 0) return;
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0) ShowMRApprovalInfo(e.RowIndex);
            if (e.ColIndex == 7 && (TxtDocNo.Text.Length == 0 || (TxtDocNo.Text.Length != 0 && mInitProcessInd == "D")) && !Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg(this, Sm.GetLue(LueReqType), Sm.GetLue(LueDeptCode)));

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
            if (e.ColIndex == 34 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                if (mBOQDlgForMRFormat == "2") 
                {
                    Sm.FormShowDialog(new FrmMaterialRequest5Dlg7(
                        this,
                        e.RowIndex,
                        (TxtDocNo.Text.Length == 0 || (TxtDocNo.Text.Length != 0 && mInitProcessInd == "D")) && BtnSave.Enabled
                        ));
                }
                else
                {
                    Sm.FormShowDialog(new FrmMaterialRequest5Dlg5(
                        this,
                        e.RowIndex,
                        (TxtDocNo.Text.Length == 0 || (TxtDocNo.Text.Length != 0 && mInitProcessInd == "D")) && BtnSave.Enabled
                        ));
                }
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 17 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 25 }, e);
            if (e.ColIndex == 17 || e.ColIndex == 29)
            {
                ComputeTotal(e.RowIndex);
                ComputeDR_Balance();
            }
            if (e.ColIndex == 17 || e.ColIndex == 29)
            {
                ComputeTotalPrice(e.RowIndex);
                ComputeTotalEstPrice();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 19)
            {
                if (Sm.GetGrdDate(Grd1, 0, 19).Length != 0)
                {
                    var UsageDt = Sm.ConvertDate(Sm.GetGrdDate(Grd1, 0, 19));
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0) Grd1.Cells[Row, 19].Value = UsageDt;
                }
            }

            if (e.ColIndex == 25)
            {
                var Remark = Sm.GetGrdStr(Grd1, 0, 25);
                if (Remark.Length != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0) Grd1.Cells[Row, 25].Value = Remark;
                }
            }

            if (e.ColIndex == 28)
            {
                var CurCode = Sm.GetGrdStr(Grd1, 0, 28);
                if (CurCode.Length != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                            Grd1.Cells[Row, 28].Value = CurCode;
                }
            }

            if (e.ColIndex == 37)
            {
                var DurationUOM = Sm.GetGrdStr(Grd1, 0, 37);
                if (DurationUOM.Length != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                            Grd1.Cells[Row, 37].Value = DurationUOM;
                }
            }

            if (e.ColIndex == 29)
            {
                decimal Total = 0m;

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 29).Length != 0)
                        Total += Sm.GetGrdDec(Grd1, Row, 29);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        override protected void GrdRequestColHdrToolTipText(object sender, iGRequestColHdrToolTipTextEventArgs e)
        {
            if (e.ColIndex == 19)
                e.Text = "Double click title to copy data based on the first row's value.";
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            string LocalDocNo = string.Empty;
            if (Sm.StdMsgYN("Question",
                    "Department : " + LueDeptCode.GetColumnValue("Col2") + Environment.NewLine +
                    "Do you want to save this data ?"
                    ) == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DeptCode = Sm.GetLue(LueDeptCode);
            string SubCategory = Sm.GetGrdStr(Grd1, 0, 13);
            string DocNo = string.Empty;

            if (mIsMRSPPJB)
            {
                if (mIsMaterialRequestSPPJBDocNoUseDifferentAbbr)
                    DocNo = GenerateDocNo2(Sm.GetDte(DteDocDt), "MaterialRequestSPPJB", "TblMaterialRequestHdr");
                else
                    DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "MaterialRequestSPPJB", "TblMaterialRequestHdr", SubCategory);
            }
            else
            {
                if (mIsMaterialRequestDocNoUseDifferentAbbr)
                    DocNo = GenerateDocNo2(Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr");
                else
                    DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr", SubCategory);
            }

            if (IsAutoGeneratePurchaseLocalDocNo)
                LocalDocNo = GenerateLocalDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr", SubCategory);
            else
                LocalDocNo = TxtLocalDocNo.Text;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveMaterialRequestHdr(DocNo, LocalDocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 8).Length > 0)
                {
                    cml.Add(SaveMaterialRequestDtl(
                        DocNo,
                        Row,
                        IsDocApprovalSettingNotExisted()
                        ));
                }
            }

            if (mIsMRSPPJB)
            {
                if (mBOQDlgForMRFormat == "2")
                {
                    cml.Add(SaveMaterialRequestDtl3(DocNo));
                }
                else
                {
                    cml.Add(SaveMaterialRequestDtl2(DocNo));
                }
            }

            Sm.ExecCommands(cml);

            ProcessUploadFile(DocNo);


            if (Sm.StdMsgYN("Print", "") == DialogResult.No)
                BtnInsertClick(sender, e);
            else
            {
                ShowData(DocNo);
                ParPrint(DocNo);
            }
        }

        private void ProcessUploadFile(string DocNo)
        {           
            if (mIsMRAllowToUploadFile)
            {
                if (TxtFile.Text.Length == 0 && TxtFile2.Text.Length == 0 && TxtFile3.Text.Length == 0) return;

                var l = new List<Sm.UploadFileClass>();
                string RenameTo = string.Empty, RenameTo2 = string.Empty, RenameTo3 = string.Empty;
                bool isSuccessFile1 = true, isSuccessFile2 = true, isSuccessFile3 = true;
                FileInfo toUpload = null,
                 toUpload2 = null,
                 toUpload3 = null;
                #region init data FileName1
                if (TxtFile.Text.Length > 0 && mFileName1 != TxtFile.Text)
                {
                    string FileName = TxtFile.Text;
                    toUpload = new FileInfo(string.Format(@"{0}", FileName));
                    string fExt = Path.GetExtension(toUpload.Name);
                    string fName = Path.GetFileNameWithoutExtension(toUpload.Name);
                    RenameTo =  string.Concat(fName, "_", DocNo.Replace('/', '-'), fExt);
                    isSuccessFile1 = Sm.UploadFile(mIsMRAllowToUploadFile, FileName, mFormatFTPClient, RenameTo, mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, mUsernameForFTPClient, mPasswordForFTPClient, mIsUploadFileRenamed);
                }
                #endregion

                #region init data FileName2
                if (TxtFile2.Text.Length > 0 && mFileName2 != TxtFile2.Text)
                {
                    string FileName2 = TxtFile2.Text;
                    toUpload2 = new FileInfo(string.Format(@"{0}", FileName2));
                    string fExt2 = Path.GetExtension(toUpload2.Name);
                    string fName2 = Path.GetFileNameWithoutExtension(toUpload2.Name);
                    RenameTo2 = string.Concat(fName2, "_", DocNo.Replace('/', '-'), fExt2);
                    isSuccessFile2 = Sm.UploadFile(mIsMRAllowToUploadFile, FileName2, mFormatFTPClient, RenameTo2, mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, mUsernameForFTPClient, mPasswordForFTPClient, mIsUploadFileRenamed);
                }
                #endregion

                #region init data FileName3
                if (TxtFile3.Text.Length > 0 && mFileName3 != TxtFile3.Text)
                {
                    string FileName3 = TxtFile3.Text;
                    toUpload3 = new FileInfo(string.Format(@"{0}", FileName3));
                    string fExt3 = Path.GetExtension(toUpload3.Name);
                    string fName3 = Path.GetFileNameWithoutExtension(toUpload3.Name);
                    RenameTo3 = string.Concat(fName3, "_", DocNo.Replace('/', '-'), fExt3);
                    isSuccessFile3 = Sm.UploadFile(mIsMRAllowToUploadFile, FileName3, mFormatFTPClient, RenameTo3, mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, mUsernameForFTPClient, mPasswordForFTPClient, mIsUploadFileRenamed);
                }
                #endregion

                if (isSuccessFile1 && isSuccessFile2 && isSuccessFile3)
                {
                    l.Add(new Sm.UploadFileClass()
                    {
                        DocNo = DocNo,
                        FileName = mIsUploadFileRenamed ? RenameTo : toUpload != null ? toUpload.Name : string.Empty ,
                        FileName2 = mIsUploadFileRenamed ? RenameTo2 : toUpload2 != null ? toUpload2.Name : string.Empty,
                        FileName3 = mIsUploadFileRenamed ? RenameTo3 : toUpload3 != null ? toUpload3.Name : string.Empty,
                    });
                    Sm.UpdateUploadedFile(mIsMRAllowToUploadFile, ref l, "TblMaterialRequesthdr");
                }

                l.Clear();
            }
        }
        private bool IsInsertedDataNotValid()
        {
            IsSubCategoryNull();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueReqType, "Request type") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                (mIsBudgetTransactionCanUseOtherDepartmentBudget && Sm.IsLueEmpty(LueDeptCode2, "Department for Budget")) ||
                (Sm.GetLue(LueReqType) == "1" && Sm.IsLueEmpty(LueBCCode, "Budget Category")) ||
                (mIsPICInMRMandatory && Sm.IsLueEmpty(LuePICCode, "PIC")) ||
                (!mIsDORequestNeedStockValidation && !mIsDORequestUseItemIssued && Sm.IsTxtEmpty(TxtDORequestDocNo, "DO Request#", false)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsRemainingBudgetNotValid() ||
                IsSubcategoryDifferent() ||
                IsSubCategoryXXX() ||
                IsPOQtyCancelDocNoNotValid() ||
                (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                //(mIsMRAllowToUploadFile && IsUploadFileNotValid()) ||
                IsMaxStockInvalid() ||
                IsDR_QtyInvalid() ||
                IsDR_AmtInvalid() ||
                IsDR_BalanceInvalid() ||
                (mIsMRSPPJB && IsJobCurCodeInvalid()) ||
                (mIsMRSPPJB && IsBOQEmpty()) ||
                 TxtFile.Text.Length != 0 && IsUploadFileInvalid()||
                 TxtFile2.Text.Length != 0 && IsUploadFileInvalid2()||
                 TxtFile3.Text.Length != 0 && IsUploadFileInvalid3()
                 ;
            ;
        }

        private bool IsUploadFileInvalid()
        {
            if (!mIsMRAllowToUploadFile) return false;

            string FileName = TxtFile.Text;

            FileInfo f = new FileInfo(FileName);

            if (Sm.IsFTPClientDataNotValid(mIsMRAllowToUploadFile, FileName, mHostAddrForFTPClient, mSharedFolderForFTPClient, mUsernameForFTPClient, mPortForFTPClient) ||
                Sm.IsFileSizeNotValid(mIsMRAllowToUploadFile, FileName, false, 0, ref f, mFileSizeMaxUploadFTPClient) ||
                Sm.IsFileNameAlreadyExisted(mIsMRAllowToUploadFile, FileName, "TblMaterialRequesthdr", "FileName"))
            {
                return true;
            }

            return false;
        }

        private bool IsUploadFileInvalid2()
        {
            if (!mIsMRAllowToUploadFile) return false;

            string FileName = TxtFile2.Text;


            FileInfo f = new FileInfo(FileName);

            if (Sm.IsFTPClientDataNotValid(mIsMRAllowToUploadFile, FileName, mHostAddrForFTPClient, mSharedFolderForFTPClient, mUsernameForFTPClient, mPortForFTPClient) ||
                Sm.IsFileSizeNotValid(mIsMRAllowToUploadFile, FileName, false, 0, ref f, mFileSizeMaxUploadFTPClient) ||
                Sm.IsFileNameAlreadyExisted(mIsMRAllowToUploadFile, FileName, "TblMaterialRequesthdr", "FileName2"))
            {
                return true;
            }

            return false;
        }

        private bool IsUploadFileInvalid3()
        {
            if (!mIsMRAllowToUploadFile) return false;

            string FileName = TxtFile3.Text;


            FileInfo f = new FileInfo(FileName);

            if (Sm.IsFTPClientDataNotValid(mIsMRAllowToUploadFile, FileName, mHostAddrForFTPClient, mSharedFolderForFTPClient, mUsernameForFTPClient, mPortForFTPClient) ||
                Sm.IsFileSizeNotValid(mIsMRAllowToUploadFile, FileName, false, 0, ref f, mFileSizeMaxUploadFTPClient) ||
                Sm.IsFileNameAlreadyExisted(mIsMRAllowToUploadFile, FileName, "TblMaterialRequesthdr", "FileName3"))
            {
                return true;
            }

            return false;
        }
        private bool IsBOQEmpty()
        {
            if (mlJob2.Count > 0)
                return false;
            else if (mlJob.Count > 0)
                return false;
            else
            {
                Sm.StdMsg(mMsgType.Warning, "Bill of Quantity is Empty");
                return true;
            }
        }

        private bool IsJobCurCodeInvalid()
        {
            string ItCode = string.Empty;
            string CurCode = string.Empty;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 8).Length > 0)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 28, false, "Estimated currency is empty.") ||
                        Sm.IsGrdValueEmpty(Grd1, r, 29, true, "Estimated price should be bigger than 0.00."))
                        return true;
                    ItCode = Sm.GetGrdStr(Grd1, r, 8);
                    CurCode = Sm.GetGrdStr(Grd1, r, 28);
                    foreach (var i in mlJob)
                    {
                        if (Sm.CompareStr(ItCode, i.ItCode))
                        {
                            if (!Sm.CompareStr(CurCode, i.CurCode))
                            {
                                Sm.StdMsg(mMsgType.Warning,
                                    "Item's Code : " + ItCode + Environment.NewLine +
                                    "Item's Name : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                                    "Job's Code : " + i.JobCode + Environment.NewLine +
                                    "Job's Name : " + i.JobName + Environment.NewLine +
                                    "Job's Currency : " + i.CurCode + Environment.NewLine +
                                    "Estimated Currency : " + CurCode + Environment.NewLine + Environment.NewLine +
                                    "Invalid job's currency."
                                    );
                                return true;
                            }
                        }
                    }
                }

            }
            return false;
        }

        private bool IsDR_AlreadyProcessed1()
        {
            if (!mIsMaterialRequestForDroppingRequestEnabled) return false;

            if (TxtDR_PRJIDocNo.Text.Length > 0)
            {
                if (Sm.IsDataExist("Select 1 From TblDroppingRequestDtl Where MRDocNo Is Not Null And DocNo=@Param Limit 1;", TxtDroppingRequestDocNo.Text))
                {
                    Sm.StdMsg(mMsgType.Warning, "This dropping request# already processed.");
                    return true;
                }
            }
            if (mDroppingRequestBCCode.Length > 0)
            {
                if (Sm.IsDataExist(
                    "Select 1 From TblDroppingRequestDtl2 Where MRDocNo Is Not Null And DocNo=@Param1 And BCCode=@Param2 Limit 1;", TxtDroppingRequestDocNo.Text, mDroppingRequestBCCode, string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning, "This dropping request# already processed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDR_QtyInvalid()
        {
            if (TxtDroppingRequestDocNo.Text.Length <= 0) return false;

            decimal Qty1 = 0m, Qty2 = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Qty1 = 0m;
                Qty2 = 0m;
                if (Sm.GetGrdStr(Grd1, r, 18).Length != 0) Qty1 = Sm.GetGrdDec(Grd1, r, 18);
                if (Sm.GetGrdStr(Grd1, r, 32).Length != 0) Qty2 = Sm.GetGrdDec(Grd1, r, 32);

                if (Qty1 > Qty2)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                        "Requested Quantity : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 18), 0) + Environment.NewLine +
                        "Dropping Request's Quantity : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 32), 0) + Environment.NewLine + Environment.NewLine +
                        "Requested Quantity is bigger than Dropping Request's Quantity.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDR_AmtInvalid()
        {
            if (TxtDroppingRequestDocNo.Text.Length <= 0) return false;

            decimal Amt1 = 0m, Amt2 = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Amt1 = 0m;
                Amt2 = 0m;
                if (Sm.GetGrdStr(Grd1, r, 24).Length != 0) Amt1 = Sm.GetGrdDec(Grd1, r, 24);
                if (Sm.GetGrdStr(Grd1, r, 32).Length != 0) Amt2 = Sm.GetGrdDec(Grd1, r, 32);

                if (Amt1 > Amt2)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                        "Requested Amount : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 24), 0) + Environment.NewLine +
                        "Dropping Request's Amount : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 32), 0) + Environment.NewLine + Environment.NewLine +
                        "Requested Amount is bigger than Dropping Request's Amount.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDR_BalanceInvalid()
        {
            if (TxtDroppingRequestDocNo.Text.Length <= 0) return false;

            //ComputeDR_OtherMRAmt();
            ComputeDR_Balance();
            if (decimal.Parse(TxtDR_Balance.Text) < 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                        "Dropping Request's Balance : " + TxtDR_Balance.Text + Environment.NewLine +
                        "Dropping Request's balance is less than 0.00.");
                return true;
            }
            return false;
        }

        //private void ComputeDR_OtherMRAmt()
        //{
        //    if (TxtDR_PRJIDocNo.Text.Length == 0) return;

        //    var SQL = new StringBuilder();
        //    decimal DR_OtherMRAmt = 0m;

        //    SQL.AppendLine("Select IfNull(Sum(T2.Qty*T2.UPrice), 0.00) As Amt ");
        //    SQL.AppendLine("From TblMaterialRequestHdr T1 ");
        //    SQL.AppendLine("Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo ");
        //    SQL.AppendLine("Where T1.CancelInd='N' ");
        //    SQL.AppendLine("And T1.Status In ('O', 'A') ");
        //    if (TxtDocNo.Text.Length>0) SQL.AppendLine("And T1.DocNo<>@Param2 ");
        //    SQL.AppendLine("And T1.DroppingRequestDocNo Is Not Null ");
        //    SQL.AppendLine("And T1.DroppingRequestDocNo=@Param1;");

        //    DR_OtherMRAmt = Sm.GetValueDec(SQL.ToString(), TxtDroppingRequestDocNo.Text, TxtDocNo.Text, string.Empty, string.Empty);

        //    //SQL.AppendLine("Select IfNull(Sum(T2.Qty*T2.UPrice), 0.00) As Amt ");
        //    //SQL.AppendLine("From TblMaterialRequestHdr T1 ");
        //    //SQL.AppendLine("Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo ");
        //    //SQL.AppendLine("Where T1.CancelInd='N' ");
        //    //SQL.AppendLine("And T1.Status In ('O', 'A') ");
        //    //if (TxtDocNo.Text.Length > 0) SQL.AppendLine("And T1.DocNo<>@Param3 ");
        //    //SQL.AppendLine("And T1.DroppingRequestDocNo Is Not Null ");
        //    //SQL.AppendLine("And T1.DroppingRequestDocNo=@Param1 ");
        //    //SQL.AppendLine("And T1.DroppingRequestBCCode=@Param2;");

        //    //DR_OtherMRAmt = Sm.GetValueDec(
        //    //    SQL.ToString(), 
        //    //    TxtDroppingRequestDocNo.Text, 
        //    //    mDroppingRequestBCCode, 
        //    //    TxtDocNo.Text, 
        //    //    string.Empty);

        //    TxtDR_OtherMRAmt.EditValue = Sm.FormatNum(DR_OtherMRAmt, 0);
        //}

        private bool IsMaxStockInvalid()
        {
            //membandingkan maximum stock dengan quantity yg diminta + stock semua warehouse + outang MR yg belum di-PO-kan.
            //MR yg belum di-received inidikator belum ada/Uom bisa beda/proses bisa lama
            //Hanya yg maximum stocknya diisi dan uom purchase dan inventory-nya sama

            if (!mIsMaterialRequestMaxStockValidationEnabled) return false;

            string ItCode = string.Empty, Filter = string.Empty, Filter2 = string.Empty;
            var cm = new MySqlCommand();

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 8);
                if (ItCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.ItCode=@ItCode0" + r.ToString() + ") ";

                    if (Filter2.Length > 0) Filter2 += " Union All ";
                    Filter2 += " Select @ItCode0" + r.ToString() + " As ItCode, @Qty0" + r.ToString() + " As Qty ";

                    Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                    Sm.CmParam<Decimal>(ref cm, "@Qty0" + r.ToString(), Sm.GetGrdDec(Grd1, r, 17));
                }
            }

            if (Filter.Length == 0)
                return false;
            else
                Filter = " And ( " + Filter + " ) ";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.MaxStock, IfNull(B.Qty, 0.00) As Stock, IfNull(C.Qty, 0.00) As OutstandingMR, IfNull(D.Qty, 0.00) As MRQty  ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select A.ItCode, Sum(A.Qty) As Qty  ");
            SQL.AppendLine("    From TblStockSummary A ");
            SQL.AppendLine("    Where A.Qty>0.00 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("    Group By A.ItCode ");
            SQL.AppendLine("    ) B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select T2.ItCode, Sum(T2.Qty) As Qty  ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.Status In ('O', 'A') And T2.ProcessInd In ('O', 'P') ");
            SQL.AppendLine(Filter.Replace("A.", "T2."));
            SQL.AppendLine("    Where T1.CancelInd='N' And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    Group By T2.ItCode ");
            SQL.AppendLine("    ) C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("    ) D On A.ItCode=D.ItCode ");
            SQL.AppendLine("Where A.MaxStock>0.00 ");
            SQL.AppendLine("And A.PurchaseUomCode=A.InventoryUomCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("And A.MaxStock<IfNull(B.Qty, 0.00)+IfNull(C.Qty, 0.00)+IfNull(D.Qty, 0.00) ");
            SQL.AppendLine("Limit 1; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        "ItCode", 
                        "ItName", "MaxStock", "Stock", "OutstandingMR", "MRQty"
                    });

                if (dr.HasRows)
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sm.StdMsg(mMsgType.Warning,
                            "Item's Code : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Item's Name : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Maximum Stock : " + Sm.FormatNum(Sm.DrDec(dr, 2), 0) + Environment.NewLine +
                            "Current Stock : " + Sm.FormatNum(Sm.DrDec(dr, 3), 0) + Environment.NewLine +
                            ((Doctitle == "IMS") ? "Outstanding Purchase Request : " : "Outstanding Material Request : ") + Sm.FormatNum(Sm.DrDec(dr, 4), 0) + Environment.NewLine +
                            "Requested Quantity : " + Sm.FormatNum(Sm.DrDec(dr, 5), 0) + Environment.NewLine + Environment.NewLine +
                            "Invalid maximum stock."
                            );
                            return true;
                        }
                    }
                }
                dr.Close();
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var DocDt = Sm.GetDte(DteDocDt);
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 8, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 17, true, "Quantity should be bigger than 0.00.") ||
                    (mIsUsageDateMaterialRequestMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 19, false, "Usage date is empty.")) ||
                    (mIsUsageDateMaterialRequestMandatory && IsUsageDtNotValid(DocDt, Row)) ||
                    (Sm.GetLue(LueReqType) == "1" && mIsMRAutomaticGetLastVendorQuotation && Sm.IsGrdValueEmpty(Grd1, Row, 23, true, "Quotation's price is 0.")) ||
                    (!mIsRemarkMRNotMandatory && mIsRemarkForApprovalMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 25, false, "Remark is empty.")) ||
                    (mIsMRDurationMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 35, true, "Duration is 0.")) ||
                    (Sm.GetGrdDec(Grd1, Row, 35) != 0 && Sm.IsGrdValueEmpty(Grd1, Row, 37, false, "Duration UOM is empty."))
                    )
                    return true;

            }
            return false;
        }

        private bool IsUsageDtNotValid(string DocDt, int Row)
        {
            var UsageDt = Sm.GetGrdDate(Grd1, Row, 19);
            if (Sm.CompareDtTm(UsageDt, DocDt) < 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Document Date : " + Sm.FormatDate2("dd/MMM/yyyy", DocDt) + Environment.NewLine +
                    "Usage Date : " + Sm.FormatDate2("dd/MMM/yyyy", UsageDt) + Environment.NewLine + Environment.NewLine +
                    "Usage date is earlier than document date.");
                Sm.FocusGrd(Grd1, Row, 19);
                return true;
            }
            return false;
        }

        private bool IsRemainingBudgetNotValid()
        {
            decimal RemainingBudget = 0m;

            if (TxtRemainingBudget.Text.Length != 0) RemainingBudget = decimal.Parse(TxtRemainingBudget.Text);
            //harusnya <0
            if (RemainingBudget < 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid remaining budget.");
                return true;
            }
            return false;
        }

        private bool IsDocApprovalSettingNotExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where UserCode Is Not Null ");
            if (mIsMRSPPJB)
                SQL.AppendLine("And DocType = 'MaterialRequestSPPJB' ");
            else
                SQL.AppendLine("And DocType = 'MaterialRequest' ");
            SQL.AppendLine("And DeptCode Is Not Null ");
            SQL.AppendLine("And DeptCode = @Param1 ");
            if (mIsApprovalBySiteMandatory)
            {
                SQL.AppendLine("And SiteCode Is Not Null ");
                SQL.AppendLine("And SiteCode=@Param2 ");
            }

            SQL.AppendLine("Limit 1; ");

            if (!Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueDeptCode), Sm.GetLue(LueSiteCode), string.Empty))
                return true;
            else
                return false;
        }

        private void IsSubCategoryNull()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 13).Length == 0)
                {
                    Grd1.Cells[Row, 13].Value = Grd1.Cells[Row, 14].Value = "XXX";
                }
            }
        }

        private bool IsSubCategoryXXX()
        {
            if (IsProcFormat == "1")
            {
                string Msg = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 14) == "XXX")
                    {
                        Msg =
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine;

                        Sm.StdMsg(mMsgType.Warning, Msg + "doesn't have Sub-Category.");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsSubcategoryDifferent()
        {
            if (IsProcFormat == "1")
            {
                string SubCat = Sm.GetGrdStr(Grd1, 0, 13);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (SubCat != Sm.GetGrdStr(Grd1, Row, 13))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Item have different subcategory ");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsPOQtyCancelDocNoNotValid()
        {
            if (TxtPOQtyCancelDocNo.Text.Length == 0) return false;

            var cm = new MySqlCommand()
            {
                CommandText =
                "Select DocNo From TblPOQtyCancel " +
                "Where DocNo=@Param And CancelInd='N' And ProcessInd='O' And NewMRInd='Y';"
            };
            Sm.CmParam<String>(ref cm, "@Param", TxtPOQtyCancelDocNo.Text);
            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Cancellation PO# is invalid.");
                return true;
            }
            return false;
        }

        private bool IsCancelReasonEmpty()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 3).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                        "Cancel reason still empty.");
                    Sm.FocusGrd(Grd1, Row, 3);
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveMaterialRequestHdr(string DocNo, string LocalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialRequestHdr(DocNo, LocalDocNo, ProcessInd, POQtyCancelDocNo, DORequestDocNo, DocDt, SiteCode, DivisionCode, DeptCode, PICCode, ReqType, BCCode, SeqNo, ItScCode, Mth, Yr, Revision, ");
            if (TxtDroppingRequestDocNo.Text.Length > 0)
                SQL.AppendLine("DroppingRequestDocNo, DroppingRequestBCCode, ");
            if (mIsMRSPPJBEnabled) SQL.AppendLine("SPPJBInd, ");
            SQL.AppendLine("AvailableBudget, DeptCode2, Remark, CopyDataDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @LocalDocNo, @ProcessInd, @POQtyCancelDocNo, @DORequestDocNo, @DocDt, @SiteCode, @DivisionCode, @DeptCode, @PICCode, @ReqType, @BCCode, @SeqNo, @ItScCode, @Mth, @Yr, @Revision, ");
            if (TxtDroppingRequestDocNo.Text.Length > 0)
                SQL.AppendLine("@DroppingRequestDocNo, @DroppingRequestBCCode, ");
            if (mIsMRSPPJBEnabled)
            {
                if (mIsMRSPPJB)
                    SQL.AppendLine("'Y', ");
                else
                    SQL.AppendLine("'N', ");
            }
            SQL.AppendLine("@AvailableBudget, @DeptCode2, @Remark, @CopyDataDocNo, @CreateBy, CurrentDateTime());");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<String>(ref cm, "@POQtyCancelDocNo", TxtPOQtyCancelDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DORequestDocNo", TxtDORequestDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DivisionCode", Sm.GetLue(LueDivCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PICCode", Sm.GetLue(LuePICCode));
            Sm.CmParam<String>(ref cm, "@ReqType", Sm.GetLue(LueReqType));
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
            if (IsAutoGeneratePurchaseLocalDocNo)
            {
                Sm.CmParam<String>(ref cm, "@SeqNo", Sm.Left(LocalDocNo, 6));
                Sm.CmParam<String>(ref cm, "@ItScCode", Sm.GetGrdStr(Grd1, 0, 13));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.Left(Sm.GetDte(DteDocDt), 4));
                Sm.CmParam<String>(ref cm, "@Revision", "");
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@SeqNo", "");
                Sm.CmParam<String>(ref cm, "@ItScCode", "");
                Sm.CmParam<String>(ref cm, "@Mth", "");
                Sm.CmParam<String>(ref cm, "@Yr", "");
                Sm.CmParam<String>(ref cm, "@Revision", "");
            }
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mDroppingRequestBCCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@AvailableBudget", (Decimal.Parse(TxtRemainingBudget.Text) + Decimal.Parse(TxtTotalEstPrice.Text)));
            Sm.CmParam<String>(ref cm, "@DeptCode2", Sm.GetLue(LueDeptCode2));
            Sm.CmParam<String>(ref cm, "@CopyDataDocNo", CopyDataDocNo);
            return cm;
        }

        private MySqlCommand SaveMaterialRequestDtl(string DocNo, int Row, bool NoNeedApproval)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialRequestDtl(DocNo, DNo, CancelInd, CancelReason, Status, Location, ItCode, Qty, UsageDt, QtDocNo, QtDNo, DORequestDocNo, DORequestDNo, UPrice, CurCode, EstPrice, TotalPrice, Remark, Duration, DurationUom, Specification,CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @CancelReason, 'O', @Location, @ItCode,  @Qty, @UsageDt, @QtDocNo, @QtDNo, @DORequestDocNo, @DORequestDNo, @UPrice, @CurCode, @EstPrice, @TotalPrice, @Remark, @Duration, @DurationUom, @Specification, @CreateBy, CurrentDateTime()); ");

            if (TxtDroppingRequestDocNo.Text.Length > 0)
            {
                if (TxtDR_PRJIDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("Update TblDroppingRequestDtl A ");
                    SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr B On A.PRBPDocNo=B.DocNo ");
                    SQL.AppendLine("Inner Join TblMaterialRequestDtl C On B.ResourceItCode=C.ItCode And C.DocNo=@DocNo ");
                    SQL.AppendLine("Set A.MRDocNo=C.DocNo, A.MRDNo=C.DNo ");
                    SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("And A.MRDocNo Is Null; ");
                }
                if (TxtDR_BCCode.Text.Length > 0)
                {
                    SQL.AppendLine("Update TblDroppingRequestDtl2 A ");
                    SQL.AppendLine("Inner Join TblMaterialRequestHdr B On A.BCCode=B.DroppingRequestBCCode And B.DocNo=@DocNo ");
                    SQL.AppendLine("Inner Join TblMaterialRequestDtl C On A.ItCode=C.ItCode And B.DocNo=C.DocNo ");
                    SQL.AppendLine("Set A.MRDocNo=C.DocNo, A.MRDNo=C.DNo ");
                    SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("And A.ItCode=@ItCode ");
                    SQL.AppendLine("And A.MRDocNo Is Null; ");
                }
            }

            if (Sm.GetLue(LueProcessInd) == "F")
            {
                if (!NoNeedApproval)
                {
                    SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
                    SQL.AppendLine("From TblDocApprovalSetting T ");
                    SQL.AppendLine("Where T.DeptCode=@DeptCode ");
                    if (mIsApprovalBySiteMandatory)
                        SQL.AppendLine("And IfNull(T.SiteCode, '')=@SiteCode ");
                    if (mMRApprovalGroupValidation == "1")
                    {
                        SQL.AppendLine("And ");
                        SQL.AppendLine("(T.DAGCode Is Null Or");
                        SQL.AppendLine("(T.DAGCode Is Not Null ");
                        SQL.AppendLine("And T.DAGCode In ( ");
                        SQL.AppendLine("    Select A.DAGCode ");
                        SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                        SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                        SQL.AppendLine("    And A.ActInd='Y' ");
                        SQL.AppendLine("    And B.EmpCode IN ( ");
                        SQL.AppendLine("        SELECT B2.EmpCode ");
                        SQL.AppendLine("        FROM tblmaterialrequesthdr B1 ");
                        SQL.AppendLine("        Inner Join tblemployee B2 On B1.PICCode = B2.UserCode ");
                        SQL.AppendLine("        WHERE B1.DocNo = @DocNo ) ");
                        SQL.AppendLine("))) ");
                    }
                    if(mMRApprovalGroupValidation == "2")
                    {
                        SQL.AppendLine("And ");
                        SQL.AppendLine("(T.DAGCode Is Null Or");
                        SQL.AppendLine("(T.DAGCode Is Not Null ");
                        SQL.AppendLine("And T.DAGCode In ( ");
                        SQL.AppendLine("    Select A.DAGCode ");
                        SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                        SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                        SQL.AppendLine("    And A.ActInd='Y' ");
                        SQL.AppendLine("    And B.EmpCode IN ( ");
                        SQL.AppendLine("        SELECT B2.EmpCode ");
                        SQL.AppendLine("        FROM tblmaterialrequesthdr B1 ");
                        SQL.AppendLine("        Inner Join tblemployee B2 On B1.CreateBy = B2.UserCode ");
                        SQL.AppendLine("        WHERE B1.DocNo = @DocNo ) ");
                        SQL.AppendLine("))) ");
                    }
                    if (mIsMRApprovalByAmount)
                    {
                        SQL.AppendLine("And (T.StartAmt = 0 ");
                        SQL.AppendLine("OR T.StartAmt<=IfNull(( ");
                        SQL.AppendLine("    Select IfNull(TotalPrice, 0) ");
                        SQL.AppendLine("    From TblMaterialRequestDtl ");
                        SQL.AppendLine("    Where DocNo=@DocNo And DNo=@DNo ");
                        SQL.AppendLine("), 0)) ");
                        SQL.AppendLine("AND (T.EndAmt = 0 ");
                        SQL.AppendLine("OR ");
                        SQL.AppendLine("T.EndAmt>=IfNull(( ");
                        SQL.AppendLine("    Select IfNull(TotalPrice, 0) ");
                        SQL.AppendLine("    From TblMaterialRequestDtl ");
                        SQL.AppendLine("    Where DocNo=@DocNo And DNo=@DNo ");
                        SQL.AppendLine("), 0)) ");
                    }
                    if (mIsMRSPPJB)
                        SQL.AppendLine("And T.DocType = 'MaterialRequestSPPJB' ");
                    else
                        SQL.AppendLine("And T.DocType='MaterialRequest' ");
                    SQL.AppendLine("; ");
                }

                SQL.AppendLine("Update TblMaterialRequestDtl ");
                SQL.AppendLine("Set Status = 'A' ");
                SQL.AppendLine("Where DocNo = @DocNo And DNo=@DNo ");
                SQL.AppendLine("And Not Exists( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblDocApproval ");
                if (mIsMRSPPJB)
                    SQL.AppendLine("    Where DocType = 'MaterialRequestSPPJB' ");
                else
                    SQL.AppendLine("    Where DocType = 'MaterialRequest' ");
                SQL.AppendLine("    And DocNo=@DocNo And DNo=@DNo ");
                SQL.AppendLine(") ");
            }


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetGrdDate(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@QtDocNo", Sm.GetGrdStr(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@QtDNo", Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@DORequestDocNo", Sm.GetGrdStr(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@DORequestDNo", Sm.GetGrdStr(Grd1, Row, 27));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, Row, 28));
            Sm.CmParam<Decimal>(ref cm, "@EstPrice", Sm.GetGrdDec(Grd1, Row, 29));
            Sm.CmParam<Decimal>(ref cm, "@TotalPrice", Sm.GetGrdDec(Grd1, Row, 38));
            Sm.CmParam<String>(ref cm, "@Specification", Sm.GetGrdStr(Grd1, Row, 39));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DurationUom", Sm.GetGrdStr(Grd1, Row, 36));
            Sm.CmParam<Decimal>(ref cm, "@Duration", Sm.GetGrdDec(Grd1, Row, 35));
            Sm.CmParam<String>(ref cm, "@Location", Sm.GetGrdStr(Grd1, Row, 40));

            return cm;
        }

        private MySqlCommand SaveMaterialRequestDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int i = 0;
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            foreach (var x in mlJob)
            {
                SQL.AppendLine("Insert Into TblMaterialRequestDtl2 ");
                SQL.AppendLine("(DocNo, ItCode, JobCode, PrevCurCode, PrevUPrice, CurCode, UPrice, Qty, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values ");
                SQL.AppendLine("(@DocNo, @ItCode" + i + ", @JobCode" + i + ", @PrevCurCode" + i + ", @PrevUPrice" + i + ", @CurCode" + i + ", @UPrice" + i + ", @Qty" + i + ", @Remark" + i + ", @UserCode, @Dt); ");

                SQL.AppendLine("Update TblJob Set ");
                SQL.AppendLine("    CurCode=@CurCode" + i + ", UPrice=@UPrice" + i);
                SQL.AppendLine(" Where JobCode=@JobCode" + i + ";");

                Sm.CmParam<String>(ref cm, "@ItCode" + i, x.ItCode);
                Sm.CmParam<String>(ref cm, "@JobCode" + i, x.JobCode);
                Sm.CmParam<String>(ref cm, "@PrevCurCode" + i, x.PrevCurCode);
                Sm.CmParam<Decimal>(ref cm, "@PrevUPrice" + i, x.PrevUPrice);
                Sm.CmParam<String>(ref cm, "@CurCode" + i, x.CurCode);
                Sm.CmParam<Decimal>(ref cm, "@UPrice" + i, x.UPrice);
                Sm.CmParam<Decimal>(ref cm, "@Qty" + i, x.Qty);
                Sm.CmParam<String>(ref cm, "@Remark" + i, x.Remark);

                i++;
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveMaterialRequestDtl3(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int i = 0;
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            foreach (var x in mlJob2)
            {
                SQL.AppendLine("Insert Into TblMaterialRequestDtl3 ");
                SQL.AppendLine("(DocNo, DNo, GrpType, GrpName, ItCode, LocName, JobCtCode, JobName, Qty, UomCode, CurCode, Price, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values ");
                SQL.AppendLine("(@DocNo, @DNo" + i + ", @GrpType" + i +", @GrpName"+ i +", @ItCode" + i + ", @LocName" + i + ", @JobCtCode" + i + ", @JobName" + i + ", @Qty" + i + ", @UomCode" + i + ", @CurCode" + i + ", @Price" + i + ", @Remark" + i + ", @UserCode, @Dt); ");

                //SQL.AppendLine("Update TblJob Set ");
                //SQL.AppendLine("    CurCode=@CurCode" + i + ", UPrice=@UPrice" + i);
                //SQL.AppendLine(" Where JobCode=@JobCode" + i + ";");
                Sm.CmParam<String>(ref cm, "@DNo" + i, x.DNo);
                Sm.CmParam<String>(ref cm, "@GrpType" + i, x.GrpType);
                Sm.CmParam<String>(ref cm, "@GrpName" + i, x.GrpName);
                Sm.CmParam<String>(ref cm, "@ItCode" + i, x.ItCode);
                Sm.CmParam<String>(ref cm, "@JobCtCode" + i, x.JobCtCode);
                Sm.CmParam<String>(ref cm, "@JobName" + i, x.JobName);
                Sm.CmParam<Decimal>(ref cm, "@Qty" + i, x.Qty);
                Sm.CmParam<String>(ref cm, "@UomCode" + i, x.UomCode);
                Sm.CmParam<String>(ref cm, "@CurCode" + i, x.CurCode);
                Sm.CmParam<Decimal>(ref cm, "@Price" + i, x.Price);
                Sm.CmParam<String>(ref cm, "@Remark" + i, x.Remark);

                i++;
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //Sm.CmParam<String>(ref cm, "@LocName", Sm.GetGrdStr(Grd1, Row, 40));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData(object sender, EventArgs e)
        {
            UpdateCancelledItem();

            IsOk = CheckFile(1);
            IsOk2 = CheckFile(2);
            IsOk3 = CheckFile(3);

            string DNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 8).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            if (mInitProcessInd == "D")
            {
                cml.Add(EditMaterialRequestHdr(TxtDocNo.Text));
                cml.Add(DeleteDtl(TxtDocNo.Text));
                if (mIsMRSPPJB)
                {
                    if (mBOQDlgForMRFormat == "2")
                    {
                        cml.Add(SaveMaterialRequestDtl3(TxtDocNo.Text));
                    }
                    else
                    { 
                        cml.Add(SaveMaterialRequestDtl2(TxtDocNo.Text));
                    }
                }
            }
            else
                cml.Add(CancelMaterialRequestDtl(TxtDocNo.Text, DNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (mInitProcessInd == "D")
                {
                    if (Sm.GetGrdStr(Grd1, Row, 8).Length > 0)
                    {
                        cml.Add(SaveMaterialRequestDtl(
                            TxtDocNo.Text,
                            Row,
                            IsDocApprovalSettingNotExisted()
                            ));


                    }
                }
                else
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 8).Length > 0)
                        cml.Add(CancelMaterialRequestDtl2(TxtDocNo.Text, Row));
                }
            }

            Sm.ExecCommands(cml);

            if (mInitProcessInd == "D")
            {
                ProcessUploadFile(TxtDocNo.Text);

                if (Sm.StdMsgYN("Print", "") == DialogResult.No)
                    BtnInsertClick(sender, e);
                else
                {
                    ShowData(TxtDocNo.Text);
                    ParPrint(TxtDocNo.Text);
                }
            }
            else
            {
                ShowData(TxtDocNo.Text);
            }
        }

        private MySqlCommand EditMaterialRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequestHdr Set ProcessInd=@ProcessInd, POQtyCancelDocNo=@POQtyCancelDocNo, DORequestDocNo=@DORequestDocNo, SiteCode=@SiteCode, ");
            SQL.AppendLine("DeptCode=@DeptCode, PICCode=@PICCode, ReqType=@ReqType, BCCode=@BCCode, DeptCode2=@DeptCode2, AvailableBudget=@AvailableBudget, ");
            SQL.AppendLine("LocalDocNo = @LocalDocNo, ");
            SQL.AppendLine("Remark=@Remark, LastUpBy=@LastUpBy, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            if (TxtPOQtyCancelDocNo.Text.Length > 0 && Sm.GetLue(LueProcessInd) == "F")
                SQL.AppendLine("Update TblPOQtyCancel Set ProcessInd='F' Where DocNo=@POQtyCancelDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<String>(ref cm, "@POQtyCancelDocNo", TxtPOQtyCancelDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DORequestDocNo", TxtDORequestDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@DeptCode2", Sm.GetLue(LueDeptCode2));
            Sm.CmParam<String>(ref cm, "@PICCode", Sm.GetLue(LuePICCode));
            Sm.CmParam<String>(ref cm, "@ReqType", Sm.GetLue(LueReqType));
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
            Sm.CmParam<Decimal>(ref cm, "@AvailableBudget", (Decimal.Parse(TxtRemainingBudget.Text) + Decimal.Parse(TxtTotalEstPrice.Text)));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@LastUpBy", Gv.CurrentUserCode);
            return cm;
        }


        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblMaterialRequestDtl " +
                        "Where DocNo=@DocNo Order By DNo"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsEditedDataNotValid(string DNo)
        {
            if (mInitProcessInd == "D")
            {
                IsSubCategoryNull();
                return
                    Sm.IsLueEmpty(LueProcessInd, "Process") ||
                    Sm.IsLueEmpty(LueReqType, "Request type") ||
                    Sm.IsLueEmpty(LueDeptCode, "Department") ||
                    (mIsBudgetTransactionCanUseOtherDepartmentBudget && Sm.IsLueEmpty(LueDeptCode2, "Department")) ||
                    (mIsPICInMRMandatory && Sm.IsLueEmpty(LuePICCode, "PIC")) ||
                    (!mIsDORequestNeedStockValidation && !mIsDORequestUseItemIssued && Sm.IsTxtEmpty(TxtDORequestDocNo, "DO Request#", false)) ||
                    IsGrdEmpty() ||
                    IsGrdValueNotValid() ||
                    IsGrdExceedMaxRecords() ||
                    IsRemainingBudgetNotValid() ||
                    IsSubcategoryDifferent() ||
                    IsSubCategoryXXX() ||
                    IsPOQtyCancelDocNoNotValid() ||
                    (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                    //(mIsMRAllowToUploadFile && IsUploadFileNotValid()) ||
                    IsMaxStockInvalid() ||
                    IsDR_QtyInvalid() ||
                    IsDR_AmtInvalid() ||
                    IsDR_BalanceInvalid() ||
                    (mIsMRSPPJB && IsJobCurCodeInvalid())||
                     TxtFile.Text.Length != 0 && mFileName1 != TxtFile.Text && IsUploadFileInvalid() ||
                     TxtFile2.Text.Length != 0 && mFileName2 != TxtFile2.Text && IsUploadFileInvalid2() ||
                     TxtFile3.Text.Length != 0 && mFileName3 != TxtFile3.Text && IsUploadFileInvalid3()
                    ;
            }
            else
            {
                return
                IsCancelledItemNotExisted(DNo) ||
                IsCancelledItemCheckedAlready(DNo) ||
                IsCancelReasonEmpty();
            }
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (mInitProcessInd == "D") return false;
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsCancelledItemCheckedAlready(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblPORequestDtl ");
            SQL.AppendLine("Where MaterialRequestDocNo=@DocNo ");
            SQL.AppendLine("And (CancelInd='N' And IfNull(Status, 'O')<>'C') ");
            SQL.AppendLine("And MaterialRequestDNo In (" + DNo + ") ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document has been processed.");
                return true;
            }
            return false;
        }

        private MySqlCommand DeleteDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblMaterialRequestDtl ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblMaterialRequestHdr  ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            //SQL.AppendLine("    And ProcessInd = 'D' ");
            SQL.AppendLine("); ");

            if (mIsMRSPPJB)
            {
                if (mBOQDlgForMRFormat == "2")
                {
                    SQL.AppendLine("Delete From TblMaterialRequestDtl3 ");
                    SQL.AppendLine("Where DocNo = @DocNo ");
                    SQL.AppendLine("And Exists ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select 1 ");
                    SQL.AppendLine("    From TblMaterialRequestHdr  ");
                    SQL.AppendLine("    Where DocNo = @DocNo ");
                    //SQL.AppendLine("    And ProcessInd = 'D' ");
                    SQL.AppendLine("); ");
                }
                else 
                { 
                    SQL.AppendLine("Delete From TblMaterialRequestDtl2 ");
                    SQL.AppendLine("Where DocNo = @DocNo ");
                    SQL.AppendLine("And Exists ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select 1 ");
                    SQL.AppendLine("    From TblMaterialRequestHdr  ");
                    SQL.AppendLine("    Where DocNo = @DocNo ");
                    //SQL.AppendLine("    And ProcessInd = 'D' ");
                    SQL.AppendLine("); ");
                }
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }


        private MySqlCommand CancelMaterialRequestDtl(string DocNo, string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequestDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And (CancelInd='N' Or Status='C') And DNo In (" + DNo + "); ");

            if (!mIsDORequestNeedStockValidation)
            {
                SQL.AppendLine("Update TblMaterialRequestDtl Set ");
                SQL.AppendLine("  DORequestDocNo = null, ");
                SQL.AppendLine("  DORequestDNo = null ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And DORequestDocNo Is Not Null ");
                SQL.AppendLine("And DORequestDNo Is Not Null ");
                SQL.AppendLine("And DNo In (" + DNo + "); ");
            }

            if (TxtPOQtyCancelDocNo.Text.Length > 0)
                SQL.AppendLine("Update TblPOQtyCancel Set ProcessInd='O' Where DocNo=@POQtyCancelDocNo;");

            SQL.AppendLine("Update TblPRDtl A ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr B ");
            SQL.AppendLine("    On A.DocNo=IfNull(B.PRDocNo, '') ");
            SQL.AppendLine("    And B.DocNo=@DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl C ");
            SQL.AppendLine("    On B.DocNo=C.DocNo ");
            SQL.AppendLine("    And A.DNo=IfNull(C.PRDNo, '') ");
            SQL.AppendLine("    And (C.CancelInd='Y' Or C.Status='C') ");
            SQL.AppendLine("    And C.DNo In (" + DNo + ") ");
            SQL.AppendLine("Set A.CancelInd='Y', A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Update TblMakeToStockDtl A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.MakeToStockDocNo As DocNo, T2.MakeToStockDNo As DNo, ");
            SQL.AppendLine("    Sum(Case When T2.CancelInd='N' Then T2.Qty1 Else 0 End) As Qty1 ");
            SQL.AppendLine("    From TblPRHdr T1 ");
            SQL.AppendLine("    Inner Join TblPRDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("            Select Distinct X1.PRDocNo As DocNo, X2.PRDNo As DNo ");
            SQL.AppendLine("            From TblMaterialRequestHdr X1 ");
            SQL.AppendLine("            Inner Join TblMaterialRequestDtl X2 ");
            SQL.AppendLine("                On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("                And X2.PRDNo Is Not Null ");
            SQL.AppendLine("                And X2.DNo In (" + DNo + ") ");
            SQL.AppendLine("            Where X1.DocNo=@DocNo ");
            SQL.AppendLine("            And X1.PRDocNo Is Not Null ");
            SQL.AppendLine("    ) T3 On T1.DocNo=T3.DocNo And T2.DNo=T3.DNo ");
            SQL.AppendLine("    Group By T1.MakeToStockDocNo, T2.MakeToStockDNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("Set A.ProcessInd2 = ");
            SQL.AppendLine("    Case When A.Qty<>0 Then ");
            SQL.AppendLine("        Case When IfNull(B.Qty1, 0)=0 Then 'O' ");
            SQL.AppendLine("        Else ");
            SQL.AppendLine("            Case When A.Qty>IfNull(B.Qty1, 0) Then 'P' Else 'F' End ");
            SQL.AppendLine("        End ");
            SQL.AppendLine("    Else 'F' End;");

            if (TxtDroppingRequestDocNo.Text.Length > 0)
            {
                if (TxtDR_PRJIDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("Update TblDroppingRequestDtl A ");
                    SQL.AppendLine("Inner Join TblMaterialRequestDtl B ");
                    SQL.AppendLine("    On A.MRDocNo=B.DocNo ");
                    SQL.AppendLine("    And A.MRDNo=B.DNo ");
                    SQL.AppendLine("    And (B.CancelInd='Y' Or B.Status='C') ");
                    SQL.AppendLine("Set A.MRDocNo=Null, A.MRDNo=Null ");
                    SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("And A.MRDocNo Is Not Null ");
                    SQL.AppendLine("And A.MRDocNo=@DocNo;");
                }
                if (TxtDR_BCCode.Text.Length > 0)
                {
                    SQL.AppendLine("Update TblDroppingRequestDtl2 A ");
                    SQL.AppendLine("Inner Join TblMaterialRequestDtl B ");
                    SQL.AppendLine("    On A.MRDocNo=B.DocNo ");
                    SQL.AppendLine("    And A.MRDNo=B.DNo ");
                    SQL.AppendLine("    And (B.CancelInd='Y' Or B.Status='C') ");
                    SQL.AppendLine("Set A.MRDocNo=Null, A.MRDNo=Null ");
                    SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("And A.MRDocNo Is Not Null ");
                    SQL.AppendLine("And A.MRDocNo=@DocNo;");
                }
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (TxtPOQtyCancelDocNo.Text.Length > 0) Sm.CmParam<String>(ref cm, "@POQtyCancelDocNo", TxtPOQtyCancelDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand CancelMaterialRequestDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequestDtl Set ");
            SQL.AppendLine("    CancelReason=@CancelReason ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@Dno ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Dno", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));

            return cm;
        }

        private MySqlCommand UpdateMRFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequesthdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateMRFile2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequesthdr Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        private MySqlCommand UpdateMRFile3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequesthdr Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                mIsInsert = false;
                mFileName1 = string.Empty;
                mFileName2 = string.Empty;
                mFileName3 = string.Empty;
                ClearData();
                ShowMaterialRequestHdr(DocNo, "N");
                ShowDroppingRequestInfo();
                ShowMaterialRequestDtl(DocNo);
                if (mIsMRSPPJB)
                {
                    if (mBOQDlgForMRFormat == "2")
                    {
                        ShowMaterialRequestDtl3(DocNo);
                    }
                    else
                    {
                        ShowMaterialRequestDtl2(DocNo);
                    }
                }
                ComputeRemainingBudget();
                ComputeDR_Balance();
                ComputeTotalEstPrice();
                SetSeqNo();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowMaterialRequestHdr(string DocNo, string IsCopyData)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, LocalDocNo, DocDt, POQtyCancelDocNo, " +
                    "SiteCode, DeptCode, PICCode, ReqType, DORequestDocNo, BCCode, FileName, filename2, filename3, Remark, DroppingRequestDocNo, DroppingRequestBCCode, ProcessInd, DivisionCode, DeptCode2  " +
                    "From TblMaterialRequestHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", 
                        "LocalDocNo", "DocDt", "POQtyCancelDocNo", "SiteCode", "DeptCode",  
                        "ReqType", "BCCode", "FileName", "Remark", "DORequestDocNo",
                        "PICCode", "DroppingRequestDocNo", "DroppingRequestBCCode", "filename2", "filename3",
                        "ProcessInd", "DivisionCode", "DeptCode2"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if(IsCopyData == "N") TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        TxtPOQtyCancelDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[4]), IsCopyData);
                        Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[5]), IsCopyData);
                        SetLueReqType(ref LueReqType, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueReqType, Sm.DrStr(dr, c[6]));
                        Sl.SetLueBCCode(ref LueBCCode, Sm.DrStr(dr, c[7]), string.Empty);
                        TxtFile.EditValue = Sm.DrStr(dr, c[8]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                        TxtDORequestDocNo.EditValue = Sm.DrStr(dr, c[10]);
                        //Sm.SetLue(LuePICCode, Sm.DrStr(dr, c[11]));
                        TxtDroppingRequestDocNo.EditValue = Sm.DrStr(dr, c[12]);
                        mDroppingRequestBCCode = Sm.DrStr(dr, c[13]);
                        TxtFile2.EditValue = Sm.DrStr(dr, c[14]);
                        TxtFile3.EditValue = Sm.DrStr(dr, c[15]);
                        Sl.SetLueUserCode(ref LuePICCode, Sm.DrStr(dr, c[11]));
                        Sm.SetLue(LueProcessInd, Sm.DrStr(dr, c[16]));
                        Sm.SetLue(LueDivCode, Sm.DrStr(dr, c[17]));
                        //Sl.SetLueDeptCode(ref LueDeptCode2, Sm.DrStr(dr, c[18]), IsCopyData);
                        Sl.SetLueDeptCode(ref LueDeptCode2, string.Empty, IsCopyData);
                        Sm.SetLue(LueDeptCode2, Sm.DrStr(dr, c[18]));
                    }, true
                );
        }

        private void ShowDroppingRequestInfo()
        {
            if (!mIsMaterialRequestForDroppingRequestEnabled || TxtDroppingRequestDocNo.Text.Length == 0) return;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.DeptName, B.Yr, B.Mth, B.PRJIDocNo, D.BCName, B.Remark, ");
            SQL.AppendLine("Case When A.DroppingRequestBCCode Is Not Null Then ");
            SQL.AppendLine("(Select Sum(Amt) From TblDroppingRequestDtl2 Where DocNo=A.DroppingRequestDocNo And BCCode=A.DroppingRequestBCCode) ");
            SQL.AppendLine("Else B.Amt End As Amt ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Left Join TblDroppingRequestHdr B On A.DroppingRequestDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblBudgetCategory D On A.DroppingRequestBCCode=D.BCCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "DeptName", 
                        "Yr", "Mth", "PRJIDocNo", "BCName", "Remark",
                        "Amt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDR_DeptCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtDR_Yr.EditValue = Sm.DrStr(dr, c[1]);
                        TxtDR_Mth.EditValue = Sm.DrStr(dr, c[2]);
                        TxtDR_PRJIDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtDR_BCCode.EditValue = Sm.DrStr(dr, c[4]);
                        MeeDR_Remark.EditValue = Sm.DrStr(dr, c[5]);
                        TxtDR_DroppingRequestAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    }, true
                );
        }

        private void ShowMaterialRequestDtl(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.CancelReason , A.Specification as Specification2, ");
            SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As StatusDesc, ");
            SQL.AppendLine("(   Select T2.UserName From TblDocApproval T1, TblUser T2 ");
            if (mIsMRSPPJB)
                SQL.AppendLine("    Where T1.DocType = 'MaterialRequestSPPJB' ");
            else
                SQL.AppendLine("    Where T1.DocType='MaterialRequest' ");
            SQL.AppendLine("    And T1.DocNo=@DocNo And T1.DNo=A.DNo And T1.UserCode=T2.UserCode And T1.UserCode Is Not Null ");
            SQL.AppendLine("    Order By ApprovalDNo Desc Limit 1");
            SQL.AppendLine(") As UserName, A.DORequestDocNo, A.DORequestDNo,  ");
            SQL.AppendLine("A.ItCode, B.ItCodeInternal, B.ItName, B.ForeignName,  B.ItScCode, D.ItScName, B.MinStock, B.ReorderStock, A.Qty, B.PurchaseUomCode, B.Specification, ");
            SQL.AppendLine("A.Location, A.UsageDt, A.QtDocNo, A.QtDNo, C.DocDt, A.UPrice, (A.Qty*A.UPrice) As Total, TotalPrice, A.Remark, A.CurCode, A.EstPrice, A.Duration, A.DurationUom As DurationUomCode, E.OptDesc DurationUom,  ");

            if (mIsMaterialRequestForDroppingRequestEnabled)
            {
                Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);

                if (TxtDR_PRJIDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("(Select IfNull(T3.Amt, 0.00) As Amt ");
                    SQL.AppendLine("From TblDroppingRequestDtl T1, TblProjectImplementationRBPHdr T2, TblProjectImplementationDtl2 T3 ");
                    SQL.AppendLine("Where T1.PRBPDocNo=T2.DocNo And T2.ResourceItCode=A.ItCode And T2.PRJIDocNo=T3.DocNo And T2.ResourceItCode=T3.ResourceItCode ");
                    SQL.AppendLine("And T1.DocNo=@DroppingRequestDocNo) As DroppingRequestAmt ");
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mDroppingRequestBCCode);

                    SQL.AppendLine("(Select Amt ");
                    SQL.AppendLine("From TblDroppingRequestDtl2 ");
                    SQL.AppendLine("Where DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("And BCCode=@DroppingRequestBCCode ");
                    SQL.AppendLine("And ItCode=A.ItCode ");
                    SQL.AppendLine(") As DroppingRequestAmt ");
                }
            }
            else
            {
                SQL.AppendLine("0.00 As DroppingRequestAmt ");
            }

            SQL.AppendLine("From TblMaterialRequestDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblQtHdr C On A.QtDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblItemSubCategory D On B.ItScCode = D.ItScCode ");
            SQL.AppendLine("Left Join TblOption E On E.OptCode = A.DurationUom And E.OptCat = 'DurationUom' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ItCode");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "CancelInd", "CancelReason", "StatusDesc", "UserName", "ItCode",  
                    
                    //6-10
                    "ItCodeInternal", "ItName", "ForeignName", "ItScCode", "ItScName",  
                    
                    //11-15
                    "MinStock", "ReorderStock", "Qty", "PurchaseUomCode", "UsageDt", 
                    
                    //16-20
                    "QtDocNo", "QtDNo", "DocDt", "UPrice", "Total", 
                    
                    //21-25
                    "Remark", "DORequestDocNo", "DORequestDNo", "CurCode", "EstPrice",
                    
                    //26-30
                    "DroppingRequestAmt", "Specification", "Duration", "DurationUom", "TotalPrice",

                    //31-33
                    "DurationUomCode", "Specification2", "Location"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 17);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 23, 19);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 24, 20);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 27, 23);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 28, 24);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 29, 25);
                    Grd.Cells[Row, 31].Value = 0m;
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 32, 26);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 33, 27);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 35, 28);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 36, 31);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 37, 29);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 38, 30);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 39, 32);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 40, 33);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 15, 16, 17, 29, 31, 32, 35 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowMaterialRequestDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ItCode, A.JobCode, B.JobName, C.JobCtName, B.UomCode, ");
            SQL.AppendLine("A.PrevCurCode, A.PrevUPrice, A.CurCode, A.UPrice, A.Qty, A.Remark ");
            SQL.AppendLine("From TblMaterialRequestDtl2 A ");
            SQL.AppendLine("Inner Join TblJob B On A.JobCode=B.JobCode ");
            SQL.AppendLine("Inner Join TblJobCategory C On B.JobCtCode=C.JobCtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By C.JobCtName, B.JobName;");

            cm.CommandTimeout = 600;
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            cm.CommandText = SQL.ToString();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                int No = 0;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode", 
                    //1-5
                    "JobCode", "JobName", "JobCtName", "UomCode", "PrevCurCode", 
                    //6-10
                    "PrevUPrice", "CurCode", "UPrice", "Qty", "Remark"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        No = No + 1;
                        mlJob.Add(new Job()
                        {
                            No = No,
                            ItCode = Sm.DrStr(dr, c[0]),
                            JobCode = Sm.DrStr(dr, c[1]),
                            JobName = Sm.DrStr(dr, c[2]),
                            JobCtName = Sm.DrStr(dr, c[3]),
                            UomCode = Sm.DrStr(dr, c[4]),
                            PrevCurCode = Sm.DrStr(dr, c[5]),
                            PrevUPrice = Sm.DrDec(dr, c[6]),
                            CurCode = Sm.DrStr(dr, c[7]),
                            UPrice = Sm.DrDec(dr, c[8]),
                            Qty = Sm.DrDec(dr, c[9]),
                            Remark = Sm.DrStr(dr, c[10]),
                            Total = Sm.DrDec(dr, c[9]) * Sm.DrDec(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ShowMaterialRequestDtl3(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.GrpType, A.GrpName, A.ItCode, A.LocName, A.JobCtCode, ");
            SQL.AppendLine("B.JobCtName, A.JobName, A.Qty, A.UomCode, A.CurCode, A.Price, A.Remark ");
            SQL.AppendLine("From TblMaterialRequestDtl3 A ");
            //SQL.AppendLine("Inner Join TblJob B On A.JobCode=B.JobCode ");
            SQL.AppendLine("Inner Join TblJobCategory B On A.JobCtCode = B.JobCtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            //SQL.AppendLine("Order By C.JobCtName, B.JobName;");

            cm.CommandTimeout = 600;
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            cm.CommandText = SQL.ToString();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                int No = 0;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "GrpType", "GrpName", "ItCode", "LocName", "JobCtCode", 
                    //6-10
                    "JobCtName", "JobName", "Qty", "UomCode", "CurCode",
                    //11
                    "Price","Remark"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mlJob2.Add(new Job2()
                        {
                            DNo = Sm.DrStr(dr, c[0]),
                            GrpType = Sm.DrStr(dr, c[1]),
                            GrpName = Sm.DrStr(dr, c[2]),
                            ItCode = Sm.DrStr(dr, c[3]),
                            LocName = Sm.DrStr(dr, c[4]),
                            JobCtCode = Sm.DrStr(dr, c[5]),
                            JobCtName = Sm.DrStr(dr, c[6]),
                            JobName = Sm.DrStr(dr, c[7]),
                            Qty = Sm.DrDec(dr, c[8]),
                            UomCode = Sm.DrStr(dr, c[9]),
                            CurCode = Sm.DrStr(dr, c[10]),
                            Price = Sm.DrDec(dr, c[11]),
                            Remark = Sm.DrStr(dr, c[12]),
                            //Total = Sm.DrDec(dr, c[9]) * Sm.DrDec(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }
        }


        #endregion

        #region Additional Method

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Ignore Into TblParameter(ParCode, ParDesc, Parvalue, Customize, CreateBy, CreateDt) Values('IsLocalDocMRDraftEditable', 'Apakah Local Docs dan Upload file bisa dirubah ketika edit MR Draft ?', 'N', 'SIER', 'WEDHA', '202304101530'); ");
            SQL.AppendLine("Insert Ignore Into TblParameter(ParCode, ParDesc, Parvalue, Customize, CreateBy, CreateDt) Values('IsLocalDocMRSPPJBDraftEditable', 'Apakah Local Docs dan Upload file bisa dirubah ketika edit MR SPPJB Draft ?', 'N', 'SIER', 'WEDHA', '202304101530'); ");

            Sm.ExecQuery(SQL.ToString());
        }

        private bool CheckFile(byte Type)
        {
            bool IsTheSameFile = false;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");

            if (Type == 1)
            {
                if (TxtFile.Text.Length == 0) return true;
                SQL.AppendLine("FileName ");
            }

            if (Type == 2)
            {
                if (TxtFile2.Text.Length == 0) return true;
                SQL.AppendLine("FileName2 ");
            }

            if (Type == 3)
            {
                if (TxtFile3.Text.Length == 0) return true;
                SQL.AppendLine("FileName3 ");
            }

            SQL.AppendLine("From TblMaterialRequestHdr ");
            SQL.AppendLine("Where DocNo = @Param; ");

            string FileName = Sm.GetValue(SQL.ToString(), TxtDocNo.Text);

            if (Type == 1) IsTheSameFile = FileName == TxtFile.Text;
            if (Type == 2) IsTheSameFile = FileName == TxtFile2.Text;
            if (Type == 3) IsTheSameFile = FileName == TxtFile3.Text;

            if (IsTheSameFile)
            {
                return false;
            }

            return true;
        }

        private void SetLueProcessInd(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'D' As Col1, 'Draft' As Col2 ");
            SQL.AppendLine("Union All Select 'F' As Col1, 'Final' As Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.SetLue2(
              ref Lue, ref cm,
              0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal void SetSeqNo()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
                Grd1.Cells[r, 30].Value = r + 1;
        }

        public static string GetNumber(string Dno)
        {
            string number = string.Empty;
            for (int ind = 0; ind < Dno.Length; ind++)
            {
                if (Char.IsNumber(Dno[ind]) == true)
                {
                    number = number + Dno[ind];
                }

            }
            return number;
        }

        private string GenerateDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                ShortCode = string.Empty,
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            bool IsDocNoFormatUseFullYear = Sm.GetParameter("IsDocNoFormatUseFullYear") == "Y";

            if (mIsDocNoWithDeptShortCode)
                ShortCode = Sm.GetValue("Select ShortCode From TblDepartment Where DeptCode='" + Sm.GetLue(LueDeptCode) + "';");

            var SQL = new StringBuilder();

            if (IsDocNoFormatUseFullYear)
            {
                Yr = Sm.Left(DocDt, 4);

                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', ");
                if (mIsDocNoWithDeptShortCode && ShortCode.Length > 0)
                    SQL.Append("'" + ShortCode + "', '/', ");
                SQL.Append("'" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                if (IsProcFormat == "1")
                {
                    SQL.Append("Select Concat('" + SubCategory + "', '/', ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("         Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("      Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
                else
                {
                    SQL.Append("Select Concat( ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', ");
                    if (mIsDocNoWithDeptShortCode && ShortCode.Length > 0)
                        SQL.Append("'" + ShortCode + "', '/', ");
                    SQL.Append("'" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
            }

            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateDocNo2(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mDocNo = string.Empty;
            string Yr = Sm.Left(DocDt, 4);
            string Mth = DocDt.Substring(4, 2);
            string DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType = @Param Limit 1; ", DocType);
            string ShortCode = Sm.GetValue("Select B.ShortCode From TblDepartment A Inner Join TblDivision B ON A.DivisionCode = B.DivisionCode WHERE A.DeptCode = @Param; ", Sm.GetLue(LueDeptCode));

            SQL.AppendLine("Select Concat(  ");
            SQL.AppendLine("IfNull((  ");
            SQL.AppendLine("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From (  ");
            SQL.AppendLine("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.AppendLine("       Where Right(DocNo, LENGTH(CONCAT(@DocAbbr, '/', IfNull(@ShortCode, ''), '/', @Mth,'/', @Yr)))=Concat(@DocAbbr, '/', IfNull(@ShortCode, ''), '/', @Mth,'/', @Yr)  ");
            SQL.AppendLine("       Order By Left(DocNo, 4) Desc Limit 1  ");
            SQL.AppendLine("       ) As Temp  ");
            SQL.AppendLine("   ), '0001')  ");
            SQL.AppendLine(", '/', @DocAbbr, '/', IfNull(@ShortCode, ''), '/', @Mth,'/', @Yr ");
            SQL.AppendLine(") As DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocAbbr", DocAbbr);
                Sm.CmParam<String>(ref cm, "@ShortCode", ShortCode);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mDocNo = Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }

            return mDocNo;
        }

        private string GenerateLocalDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                Yr = DocDt.Substring(0, 4),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DeptCode = Sm.GetValue("Select ShortCode From TblDepartment Where DeptCode = '" + Sm.GetLue(LueDeptCode) + "' ");


            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('000000', Convert(LocalDocNo+1, Char)), 6) From ( ");
            SQL.Append("       Select Convert(ifnull(Max(LocalDocNo), 0), Decimal) As LocalDocNo From " + Tbl);
            SQL.Append("       Order By Convert(ifnull(Max(LocalDocNo), 0), Decimal) Desc Limit 1");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '000001'), ");
            SQL.Append(" '/', '" + DocAbbr + "', '/', '" + DeptCode + "', '/', '" + SubCategory + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As LocalDocNo");

            return Sm.GetValue(SQL.ToString());
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 8) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ComputeTotal(int Row)
        {
            decimal Qty = 0m, UPrice = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 17).Length != 0) Qty = Sm.GetGrdDec(Grd1, Row, 17);
            if (Sm.GetGrdStr(Grd1, Row, 23).Length != 0) UPrice = Sm.GetGrdDec(Grd1, Row, 23);

            Grd1.Cells[Row, 24].Value = Qty * UPrice;

            ComputeRemainingBudget();
            ComputeDR_Balance();
        }

        private decimal ComputeAvailableBudget()
        {
            string AvailableBudget = "0";

            if (((mIsBudgetTransactionCanUseOtherDepartmentBudget && Sm.GetLue(LueDeptCode2).Length != 0 )|| (!mIsBudgetTransactionCanUseOtherDepartmentBudget && Sm.GetLue(LueDeptCode).Length != 0)) && Sm.GetDte(DteDocDt).Length != 0 && Sm.CompareStr(Sm.GetLue(LueReqType), "1"))
            {
                var SQL = new StringBuilder();

                if (mIsMRBudgetBasedOnBudgetCategory)
                {
                    SQL.AppendLine("Select ");
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select  ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        Amt2 ");
                    else
                        SQL.AppendLine("        SUM(Amt2) ");
                    SQL.AppendLine("        From TblBudgetSummary A");
                    SQL.AppendLine("        Where A.DeptCode=@DeptCode AND A.BCCode = @BCCode ");
                    SQL.AppendLine("        And A.Yr=Left(@DocDt, 4) ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And A.Mth=Substring(@DocDt, 5, 2) ");
                    SQL.AppendLine("    ), 0.00)- ");
                }
                else
                {

                    SQL.AppendLine("Select ");
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Amt From TblBudget ");
                    SQL.AppendLine("        Where DeptCode=@DeptCode ");
                    SQL.AppendLine("        And Yr=Left(@DocDt, 4) ");
                    SQL.AppendLine("        And Mth=Substring(@DocDt, 5, 2) ");
                    //SQL.AppendLine("        And UserCode Is Not Null ");
                    SQL.AppendLine("    ), 0.00)- ");

                }

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(Amt) ");
                SQL.AppendLine("        From TblVoucherRequestHdr ");
                SQL.AppendLine("        Where DocNo <> @DocNo ");
                SQL.AppendLine("        And Find_In_Set(DocType, ");
                SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And Parcode = 'VoucherDocTypeBudget'), '')) ");
                SQL.AppendLine("        And CancelInd = 'N' ");
                SQL.AppendLine("        And Status In ('O', 'A') ");
                SQL.AppendLine("        And ReqType Is Not Null ");
                SQL.AppendLine("        And ReqType <> @ReqTypeForNonBudget ");
                SQL.AppendLine("        And DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(DocDt, 4) = Left(@DocDt, 4) ");
                SQL.AppendLine("        And IfNull(BCCode, '') = IfNull(@BCCode, '') ");
                SQL.AppendLine("    ), 0.00) - ");

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) ");
                SQL.AppendLine("        From TblTravelRequestHdr A ");
                SQL.AppendLine("        Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
                SQL.AppendLine("        Where A.DocNo <> @DocNo ");
                SQL.AppendLine("        And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.Status In ('O', 'A') ");
                SQL.AppendLine("        And C.DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                SQL.AppendLine("        And IfNull(B.BCCode, '') = IfNull(@BCCode, '') ");
                SQL.AppendLine("    ), 0.00) - ");

                if (mMRAvailableBudgetSubtraction == "1")
                {
                    SQL.AppendLine("    IfNull(( ");
                    if (mIsBudgetCalculateFromEstimatedPrice && mIsMRShowEstimatedPrice)
                        SQL.AppendLine("        Select Sum(B.Qty*B.EstPrice) ");
                    else
                        SQL.AppendLine("        Select Sum(B.Qty*B.UPrice) ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A, TblMaterialRequestDtl B ");
                    SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                    if (mBudgetBasedOn == "1")
                        SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    if (mBudgetBasedOn == "2")
                        SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    if (mIsMRBudgetBasedOnBudgetCategory)
                    {
                        SQL.AppendLine("        And A.BCCode=@BCCode ");
                    }
                    SQL.AppendLine("        And A.ReqType='1' ");
                    SQL.AppendLine("        And B.CancelInd='N' ");
                    SQL.AppendLine("        And B.Status<>'C' ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("        And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                    SQL.AppendLine("       And A.DocNo<>@DocNo ");
                    SQL.AppendLine("    ),0.00)+");
                }

                if (mMRAvailableBudgetSubtraction == "2")
                {
                    SQL.AppendLine("    IfNull(( ");

                    #region Old Code

                    //SQL.AppendLine("        Select Sum(E.Qty*D.UPrice)  ");
                    //SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                    //SQL.AppendLine("        Inner Join TblMaterialRequestDtl B on A.Docno = B.DocNo ");
                    //SQL.AppendLine("        Inner Join TblPORequestDtl C  On B.DocNo=C.MaterialRequestDocNo And B.DNo = C.MaterialRequestDNo ");
                    //SQL.AppendLine("        Inner Join TblQTDtl D On D.DocNo = C.QtDocNo And D.DNo = C.QtDNo ");
                    //SQL.AppendLine("        Inner Join TblPODtl E on E.PORequestDocNo = C.DocNo And E.PORequestDNo = C.DNo And E.CancelInd = 'N' ");

                    //if (mBudgetBasedOn == "1")
                    //    SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    //if (mBudgetBasedOn == "2")
                    //    SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    //SQL.AppendLine("        And A.ReqType='1' ");
                    //SQL.AppendLine("        And B.CancelInd='N' ");
                    //SQL.AppendLine("        And B.Status<>'C' ");
                    //SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    //SQL.AppendLine("       And A.DocNo<>@DocNo ");

                    #endregion

                    SQL.AppendLine("    Select Sum( ");
                    SQL.AppendLine("    X2.Amt -  ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("      IfNull( X1.DiscountAmt *  ");
                    SQL.AppendLine("          Case When X1.CurCode = @MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("          IfNull(( ");
                    SQL.AppendLine("              Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("              Where RateDt<=X1.DocDt And CurCode1=X1.CurCode And CurCode2=@MainCurCode  ");
                    SQL.AppendLine("              Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End ");
                    SQL.AppendLine("      , 0.00) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("    From TblPOHdr X1 ");
                    SQL.AppendLine("    Inner Join ( ");
                    SQL.AppendLine("        Select A.DeptCode, E.DocNo, Sum(  ");
                    SQL.AppendLine("        ((((100.00-D.Discount)*0.01)*(D.Qty*G.UPrice))-D.DiscountAmt+D.RoundingValue)  ");
                    SQL.AppendLine("        * Case When F.CurCode=@MainCurCode Then 1.00 Else  ");
                    SQL.AppendLine("        IfNull((  ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode  ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End  ");
                    SQL.AppendLine("        ) As Amt  ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                    SQL.AppendLine("        Inner Join TblMaterialRequestDtl B  ");
                    SQL.AppendLine("            On A.DocNo=B.DocNo ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("            And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("            And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                    SQL.AppendLine("            And A.DocNo <> @DocNo ");
                    SQL.AppendLine("            And B.CancelInd='N'  ");
                    SQL.AppendLine("            And B.Status In ('A', 'O')  ");
                    //SQL.AppendLine("            And A.Status='A'  ");
                    SQL.AppendLine("            And A.Reqtype='1'   ");

                    if (mBudgetBasedOn == "1")
                        SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    if (mBudgetBasedOn == "2")
                        SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    if (mIsMRBudgetBasedOnBudgetCategory)
                        SQL.AppendLine("        And A.BCCode=@BCCode ");

                    SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N'  ");
                    SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo=F.DocNo  ");
                    SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo  ");
                    SQL.AppendLine("        Group By A.DeptCode, E.DocNo ");
                    SQL.AppendLine("    ) X2 On X1.DocNo = X2.DocNo ");
                    SQL.AppendLine("    Group By X2.DeptCode ");

                    SQL.AppendLine("    ),0.00)+");
                }

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(A.Qty*D.UPrice) ");
                SQL.AppendLine("        From TblPOQtyCancel A ");
                SQL.AppendLine("        Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestHdr E ");
                SQL.AppendLine("            On D.DocNo=E.DocNo  ");
                if (mBudgetBasedOn == "1")
                    SQL.AppendLine("        And E.DeptCode=@DeptCode ");
                if (mBudgetBasedOn == "2")
                    SQL.AppendLine("        And E.SiteCode=@DeptCode ");
                if (mIsMRBudgetBasedOnBudgetCategory)
                    SQL.AppendLine("        And E.BCCode=@BCCode ");

                SQL.AppendLine("            And E.ReqType='1' ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("            And Left(E.DocDt, 6)=Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("            And Left(E.DocDt, 4)=Left(@DocDt, 4) ");
                SQL.AppendLine("            And E.DocNo<>@DocNo ");
                SQL.AppendLine("        Where A.CancelInd='N' ");
                SQL.AppendLine("    ),0.00) + ");

                SQL.AppendLine("IfNull(( ");
                SQL.AppendLine("    Select Sum(T.Amt) Amt From ( ");
                SQL.AppendLine("        Select Case when A.AcType = 'D' Then IfNull(A.Amt, 0.00) Else IfNull(A.Amt*-1, 0.00) END As Amt ");
                SQL.AppendLine("        From TblVoucherHdr A  ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select X1.DocNo, X1.VoucherRequestDocNo   ");
                SQL.AppendLine("            From TblVoucherHdr X1  ");
                SQL.AppendLine("            Inner Join TblVoucherRequestHdr X2 ON X2.DocNo = X1.VoucherRequestDocNo  ");
                SQL.AppendLine("                And X1.DocType = '58' ");
                SQL.AppendLine("                AND X1.CancelInd = 'N'  ");
                SQL.AppendLine("                AND X2.Status In ('O', 'A')  ");
                SQL.AppendLine("        ) B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("        Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
                SQL.AppendLine("            From TblVoucherHdr X1  ");
                SQL.AppendLine("            Inner Join TblCashAdvanceSettlementDtl X2 ON X1.DocNo = X2.VoucherDocNo ");
                SQL.AppendLine("            INNER JOIN TblVoucherRequestHdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
                SQL.AppendLine("                AND X1.DocType = '56' ");
                SQL.AppendLine("                AND X1.CancelInd = 'N' ");
                SQL.AppendLine("                AND X3.Status In ('O', 'A') ");
                SQL.AppendLine("        ) D ON C.DocNo = D.DocNo ");
                SQL.AppendLine("        Where A.DocNo <> @DocNo ");
                SQL.AppendLine("        And D.DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                SQL.AppendLine("        And IfNull(D.BCCode, '') = IfNull(@BCCode, '')  ");
                SQL.AppendLine("    ) T ");
                SQL.AppendLine("), 0.00 ) ");

                if (mIsCASUsedForBudget)
                {
                    SQL.AppendLine(" - IfNull(( ");
                    SQL.AppendLine("    Select Sum(T.Amt) Amt From ( ");
                    SQL.AppendLine("        Select Left(A.DocDt, 4) Yr, Substring(A.DocDt, 5, 2) As Mth, C.BCCode, C.DeptCode, B.Amt ");
                    SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                    SQL.AppendLine("        Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("            And A.`Status` = 'A' And A.CancelInd = 'N' ");
                    SQL.AppendLine("            And A.DocStatus = 'F' ");
                    SQL.AppendLine("            And B.CCtCode Is Not Null ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("            And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("            And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    SQL.AppendLine("        Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                    SQL.AppendLine("            And C.CCtCode Is Not Null ");
                    SQL.AppendLine("            And C.BCCode = @BCCode ");
                    SQL.AppendLine("            And C.DeptCOde = @DeptCode ");
                    SQL.AppendLine("    ) T ");
                    SQL.AppendLine("), 0.00) ");
                }

                SQL.AppendLine("As RemainingBudget");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (mBudgetBasedOn == "1")
                {
                    if(mIsBudgetTransactionCanUseOtherDepartmentBudget)
                        Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode2));
                    else 
                        Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                }
                if (mBudgetBasedOn == "2")
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueSiteCode));

                Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                Sm.CmParam<String>(ref cm, "@DocNo", (TxtDocNo.Text.Length != 0) ? TxtDocNo.Text : "XXX");
                Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<String>(ref cm, "@MainCurCode", Sm.GetParameter("MainCurCode"));

                AvailableBudget = Sm.GetValue(cm);
            }

            return decimal.Parse(AvailableBudget);
        }

        public void ComputeRemainingBudget()
        {
            decimal AvailableBudget = 0m, RequestedBudget = 0m;
            try
            {
                if (Sm.CompareStr(Sm.GetLue(LueReqType), "1"))
                {
                    //AvailableBudget = ComputeAvailableBudget();
                    AvailableBudget = Sm.ComputeAvailableBudget(
                       (TxtDocNo.Text.Length > 0 ? TxtDocNo.Text : "XXX"), Sm.GetDte(DteDocDt),
                       Sm.GetLue(LueSiteCode), mIsBudgetTransactionCanUseOtherDepartmentBudget ? Sm.GetLue(LueDeptCode2) : Sm.GetLue(LueDeptCode), Sm.GetLue(LueBCCode)
                       );
                    if ((mIsBudgetUseMRProcessInd && Sm.GetLue(LueProcessInd) == "F") || !mIsBudgetUseMRProcessInd)
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 24).Length != 0 && !Sm.GetGrdBool(Grd1, Row, 1))
                            {
                                if (mIsBudgetCalculateFromEstimatedPrice && mIsMRShowEstimatedPrice)
                                    RequestedBudget += (Sm.GetGrdDec(Grd1, Row, 17) * Sm.GetGrdDec(Grd1, Row, 29));
                                else
                                    RequestedBudget += Sm.GetGrdDec(Grd1, Row, 24);
                            }
                        }
                    }

                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            TxtRemainingBudget.Text = Sm.FormatNum(AvailableBudget - RequestedBudget, 0);
        }

        public void ComputeDR_Balance()
        {
            if (!mIsMaterialRequestForDroppingRequestEnabled) return;
            decimal
                DR_DroppingRequestAmt = decimal.Parse(TxtDR_DroppingRequestAmt.Text),
                //DR_OtherMRAmt = decimal.Parse(TxtDR_OtherMRAmt.Text),
                DR_MRAmt = 0m,
                Qty = 0m,
                UPrice = 0m;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Qty = 0m;
                UPrice = 0m;

                if (Sm.GetGrdStr(Grd1, r, 17).Length != 0) Qty = Sm.GetGrdDec(Grd1, r, 17);
                if (Sm.GetGrdStr(Grd1, r, 23).Length != 0) UPrice = Sm.GetGrdDec(Grd1, r, 23);

                DR_MRAmt += (Qty * UPrice);
            }
            TxtDR_MRAmt.Text = Sm.FormatNum(DR_MRAmt, 0);
            //TxtDR_Balance.Text = Sm.FormatNum(DR_DroppingRequestAmt - DR_OtherMRAmt - DR_MRAmt, 0);
            TxtDR_Balance.Text = Sm.FormatNum(DR_DroppingRequestAmt - DR_MRAmt, 0);
        }

        internal void ComputeTotalPrice(int Row)
        {
            decimal Qty = 0m, EstPrice = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 17).Length != 0) Qty = Sm.GetGrdDec(Grd1, Row, 17);
            if (Sm.GetGrdStr(Grd1, Row, 29).Length != 0) EstPrice = Sm.GetGrdDec(Grd1, Row, 29);

            Grd1.Cells[Row, 38].Value = Qty * EstPrice;
        }

        internal void ComputeTotalEstPrice()
        {
            decimal TotEstPrice = 0m;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if(!Sm.GetGrdBool(Grd1, Row, 1))
                    TotEstPrice += Sm.GetGrdDec(Grd1, Row, 38);
            }

            TxtTotalEstPrice.EditValue = Sm.FormatNum(TotEstPrice, 0);
        }

        private void ParPrint(string DocNo)
        {
            string Doctitle = Sm.GetParameter("DocTitle");

            var l = new List<MatReq>();
            var l1 = new List<MatReq1>();
            var l2 = new List<MatReq2>();
            var ldtl = new List<MatReqDtl>();
            var ldtl2 = new List<MatReqDtl2>();
            var ldtl3 = new List<MatReqDtl3>();
            var ldtl4 = new List<MatReqDtl4>();
            var l3 = new List<MatReqSier>();
            var ldtl5 = new List<MatReqSPPJB>();
            var lsign = new List<SignatureSIER>();
            var l4 = new List<MRIMS>();
            var ldtl6 = new List<MRIMSDtl>();
            var lsign2 = new List<MRIMSSign>();
            var lsign3 = new List<MRIMSSign2>();
            var lsign4 = new List<MRSPPJBSignSIER>();
            var l5 = new List<MRSPPJBBOQDtl>();
            var l6 = new List<MRSPPJBBOQHdr>();

            string[] TableName = { "MatReq", "MatReqDtl", "MatReqDtl2", "MatReq1", "MatReq2", "MatReqDtl3", "MatReqDtl4", "MatReqSPPJB", "MatReqSier", "SignatureSIER", "MRIMS", "MRIMSDtl", "MRIMSSign", "MRIMSSign2", "MRSPPJBSignSIER", "MRSPPJBBOQDtl", "MRSPPJBBOQHdr", "Job" };
            List<IList> myLists = new List<IList>();
            int Nomor = 1;
            decimal mUsageAmt = 0m;
            decimal mUsageAmt2 = 0m;
            var cm = new MySqlCommand();
            DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);

            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d/%m/%Y') As DocDt, E.SiteName, B.DeptName, P.PosName, C.OptDesc, A.Remark, D.UserName As CreateBy, ");
            SQL.AppendLine("I.Amt1, I.Amt2, (IfNull(J.Amt3, 0.00) + IfNull(K.Amt, 0.00) + IfNull(L.Amt, 0.00)+ IfNull(M.Amt, 0.00) ");
            if (mIsCASUsedForBudget)
                SQL.AppendLine(" + IfNull(N.Amt, 0.00) ");
            SQL.AppendLine(") As Amt3, ");
            SQL.AppendLine("(Select parvalue from tblparameter where parcode='isfilterbysite') As SiteInd, A.LocalDocNo, G.BCName, H.DivisionName, Case IfNull(A.ProcessInd, '') When 'F' Then 'Final' When 'D' Then 'Draft' Else '' End As ProcessInd, ");
            SQL.AppendLine("DATE_FORMAT(LEFT(IFNULL(Q.LastUpDt, IfNull(A.LastUpDt, A.CreateDt)), 8), '%d/%m/%Y') AS LastUpDt, TIME_FORMAT(concat(IFNULL(Q.LastUpDt, IfNull(A.LastUpDt, A.CreateDt)),'00'), '%H:%i') AS LastUpTime ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("Inner Join TblOption C On A.ReqType = C.OptCode ");
            SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode ");
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select distinct A.DocNo, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                SQL.AppendLine("    From TblMaterialRequesthdr A  ");
                SQL.AppendLine("    Inner Join TblSite B On A.SiteCode = B.SiteCode  ");
                SQL.AppendLine("    Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode  ");
                SQL.AppendLine("    Inner Join TblEntity D On C.EntCode = D.EntCode  ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine(")F On A.DocNo = F.DocNo ");
            }
            SQL.AppendLine("Left Join TblBudgetCategory G On A.BCCode=G.BCCode ");
            SQL.AppendLine("Left Join TblDivision H On A.DivisionCode = H.DivisionCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.Yr, A.DeptCode, A.BCCode, ");
            SQL.AppendLine("    Sum(A.Amt1) Amt1, Sum(A.Amt2) Amt2 ");
            SQL.AppendLine("    From TblBudgetSummary A ");
            SQL.AppendLine("    Group By A.Yr, A.DeptCode, A.BCCode ");
            SQL.AppendLine(")I On I.Yr = Left(A.DocDt, 4) And I.DeptCode = A.DeptCode And I.BCCode = A.BCCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select BCCode, DeptCode, Left(DocDt, 4) As Yr, ");
            if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                SQL.AppendLine("    Sum(B.Qty * B.EstPrice) As Amt3 ");
            else
                SQL.AppendLine("    Sum(B.Qty*B.UPrice) As Amt3  ");
            SQL.AppendLine("    From TblMaterialRequestHdr A ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Where B.cancelind = 'N'  ");
            SQL.AppendLine("    Group By A.BCCode, A.DeptCode, Left(A.DocDt, 4) ");
            SQL.AppendLine(")J On A.BCCode=J.BCCode And I.DeptCode=J.DeptCode And I.Yr=J.Yr ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DeptCode, BCCode, Left(DocDt, 4) Yr, ");
            SQL.AppendLine("    Sum(Amt) Amt ");
            SQL.AppendLine("    From TblVoucherRequestHdr ");
            SQL.AppendLine("    Where ReqType Is Not Null ");
            SQL.AppendLine("    And Reqtype <> (Select Parvalue From TblParameter Where Parcode = 'ReqTypeForNonBudget') ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    And Status In ('O', 'A') ");
            SQL.AppendLine("    And Find_In_Set(DocType, ");
            SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
            SQL.AppendLine("    Group By DeptCode, BCCode, Left(DocDt, 4) ");
            SQL.AppendLine(")K On I.BCCode=K.BCCode And I.DeptCode=K.DeptCode And I.Yr=K.Yr ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select B.BCCode, C.DeptCode, Left(A.DocDt, 4) As Yr, ");
            SQL.AppendLine("    Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) As Amt  ");
            SQL.AppendLine("    From TblTravelRequestHdr A ");
            SQL.AppendLine("    Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
            SQL.AppendLine("    Where A.Cancelind = 'N'  ");
            SQL.AppendLine("    Group By B.BCCode, C.DeptCode, Left(A.DocDt, 4) ");
            SQL.AppendLine(")L On I.BCCode=L.BCCode And I.DeptCode=L.DeptCode And I.Yr=L.Yr ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine(" Select T.BcCode, T.DeptCode, T.Yr, SUM(T.Amt) As Amt From( ");
            SQL.AppendLine("    Select D.BCCode, D.DeptCode, A.DocDt ,Left(A.DocDt, 4) As Yr, ");
            SQL.AppendLine("    Case when A.AcType = 'D' Then IFNULL((A.Amt)*-1, 0.00) ELSE IFNULL((A.Amt), 0.00) END As Amt  ");
            SQL.AppendLine("        From tblvoucherhdr A ");
            SQL.AppendLine("        Inner Join ");
            SQL.AppendLine("	    (");
            SQL.AppendLine("			SELECT X1.DocNo, X1.VoucherRequestDocNo  ");
            SQL.AppendLine("		  	From tblvoucherhdr X1 ");
            SQL.AppendLine("			INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
            SQL.AppendLine("			AND X1.DocType = '58' ");
            SQL.AppendLine("            AND X1.CancelInd = 'N' ");
            SQL.AppendLine("            AND X2.Status In ('O', 'A') ");
            SQL.AppendLine("		  ) B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("       Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
            SQL.AppendLine("       Inner Join ");
            SQL.AppendLine("		  ( ");
            SQL.AppendLine("		  	SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
            SQL.AppendLine("		  	From tblvoucherhdr X1  ");
            SQL.AppendLine("		  	Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
            SQL.AppendLine("		  	INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
            SQL.AppendLine("			AND X1.DocType = '56' ");
            SQL.AppendLine("		    AND X1.CancelInd = 'N'  ");
            SQL.AppendLine("            AND X3.Status In ('O', 'A')  ");
            SQL.AppendLine("		  ) D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("		  ) T ");
            SQL.AppendLine("    Group By T.BCCode, T.DeptCode, Left(T.DocDt, 4) ");
            SQL.AppendLine(")M On I.BCCode=M.BCCode And I.DeptCode=M.DeptCode And I.Yr=M.Yr ");
            if (mIsCASUsedForBudget)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Left(A.DocDt, 4) Yr, '00' As Mth, C.BCCode, C.DeptCode, Sum(B.Amt) Amt ");
                SQL.AppendLine("    From TblCashAdvanceSettlementHdr A ");
                SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        And A.`Status` = 'A' And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.DocStatus = 'F' ");
                SQL.AppendLine("        And B.CCtCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                SQL.AppendLine("        And C.CCtCode Is Not Null ");
                SQL.AppendLine("    Group By Left(A.DocDt, 4), C.BCCode, C.DeptCode ");
                SQL.AppendLine(")N On I.BCCode = N.BCCode And I.DeptCode = N.DeptCode And I.Yr = N.Yr ");
            }
            SQL.AppendLine("Left Join TblEmployee O On D.UserCode = O.UserCode ");
            SQL.AppendLine("Left Join TblPosition P On O.PosCode = P.PosCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select DocNo, LastUpDt ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    Order By LastUpDt Desc Limit 1 ");
            SQL.AppendLine(")Q On Q.DocNo = A.DocNo ");

            SQL.AppendLine("Where A.DocNo=@DocNo And C.OptCat = 'ReqType' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
             

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyAddressCity",
                         "CompanyFax",

                         //6-10
                         "DocNo",
                         "DocDt",
                         "SiteName",
                         "DeptName",
                         "DivisionName",

                         //11-15
                         "OptDesc",
                         "Remark",
                         "CreateBy",
                         "SiteInd",
                         "LocalDocNo",

                         //16-20
                         "BCName", 
                         "Amt2", 
                         "Amt3",
                         "ProcessInd",
                         "LastUpDt",

                        //21-22
                        "LastUpTime",
                        "PosName",

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new MatReq()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyAddressCity = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),

                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            SiteName = Sm.DrStr(dr, c[8]),
                            DeptName = Sm.DrStr(dr, c[9]),
                            Division = Sm.DrStr(dr, c[10]),

                            OptDesc = Sm.DrStr(dr, c[11]),
                            HRemark = Sm.DrStr(dr, c[12]),
                            CreateBy = Sm.DrStr(dr, c[13]),
                            SiteInd = Sm.DrStr(dr, c[14]),
                            LocalDocNo = Sm.DrStr(dr, c[15]),

                            BCName = Sm.DrStr(dr, c[16]),
                            BudgetAmt = Sm.DrDec(dr, c[17]),
                            AfterUsed = Sm.DrDec(dr, c[18]),
                            EstPrice = Decimal.Parse(TxtTotalEstPrice.Text),
                            AvailableBudget = Decimal.Parse(TxtRemainingBudget.Text),
                            ProcessInd = Sm.DrStr(dr, c[19]),
                            LastUpDt = Sm.DrStr(dr, c[20]),
                            LastUpTime = Sm.DrStr(dr, c[21]),
                            PosName = Sm.DrStr(dr, c[22]),
                            FooterImage = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\FooterImage.png",

                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.ItCode, B.ItName, A.Qty, B.PurchaseUomCode, A.Location, ");
                SQLDtl.AppendLine("DATE_FORMAT(A.UsageDt,'%d-%m-%Y') As UsageDt, ");
                if (mIsUseItemConsumption)
                {
                    SQLDtl.AppendLine("IfNull(C.Qty01, 0.00) As Mth01, ");
                    SQLDtl.AppendLine("IfNull(C.Qty03, 0.00) As Mth03, ");
                    SQLDtl.AppendLine("IfNull(C.Qty06, 0.00) As Mth06, ");
                    SQLDtl.AppendLine("IfNull(C.Qty09, 0.00) As Mth09, ");
                    SQLDtl.AppendLine("IfNull(C.Qty12, 0.00) As Mth12, ");
                }
                else
                {
                    SQLDtl.AppendLine("0.00 As Mth01, ");
                    SQLDtl.AppendLine("0.00 As Mth03, ");
                    SQLDtl.AppendLine("0.00 As Mth06, ");
                    SQLDtl.AppendLine("0.00 As Mth09, ");
                    SQLDtl.AppendLine("0.00 As Mth12, ");
                }
                SQLDtl.AppendLine("A.Remark, ");
                SQLDtl.AppendLine("B.ForeignName, A.EstPrice, A.CurCode, A.Duration, D.OptDesc DurationUOM, B.Specification, Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As StatusDesc, "); //spec disini belum ada value muncul
                SQLDtl.AppendLine("DATE_FORMAT(LEFT(A.LastUpDt, 8), '%d/%m/%Y') AS LastUpDt, TIME_FORMAT(concat(A.LastUpDt,'00'), '%H:%i') AS LastUpTime ");
                SQLDtl.AppendLine("From TblMaterialRequestDtl A ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                if (mIsUseItemConsumption)
                {
                    SQLDtl.AppendLine("Left Join ( ");
                    SQLDtl.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                    SQLDtl.AppendLine("        From ( ");
                    SQLDtl.AppendLine("        select Z1.itCode, ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                    SQLDtl.AppendLine("            From ");
                    SQLDtl.AppendLine("            ( ");
                    SQLDtl.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                    SQLDtl.AppendLine("                From ");
                    SQLDtl.AppendLine("                ( ");
                    SQLDtl.AppendLine("                    Select convert('01' using latin1) As Mth Union All ");
                    SQLDtl.AppendLine("                    Select convert('03' using latin1) As Mth Union All ");
                    SQLDtl.AppendLine("                    Select convert('06' using latin1) As Mth Union All ");
                    SQLDtl.AppendLine("                    Select convert('09' using latin1) As Mth Union All ");
                    SQLDtl.AppendLine("                    Select convert('12' using latin1) As Mth  ");
                    SQLDtl.AppendLine("                )T1 ");
                    SQLDtl.AppendLine("                Inner Join ");
                    SQLDtl.AppendLine("                ( ");
                    SQLDtl.AppendLine("                    Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt And last_day(@MthDocDt) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode  ");
                    SQLDtl.AppendLine("                    Union ALL ");
                    SQLDtl.AppendLine("                    Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt2 And last_day(@MthDocDt3) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    Union All ");
                    SQLDtl.AppendLine("                    Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt4 And last_day(@MthDocDt3) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    Union All ");
                    SQLDtl.AppendLine("                    Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt5 And last_day(@MthDocDt3) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    Union All ");
                    SQLDtl.AppendLine("                    Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt6 And last_day(@MthDocDt3) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                    SQLDtl.AppendLine("            Group By T1.mth, T2.ItCode ");
                    SQLDtl.AppendLine("        )Z1 ");
                    SQLDtl.AppendLine("   )Z2 Group By Z2.ItCode ");
                    SQLDtl.AppendLine(" ) C On C.ItCode = A.ItCode ");
                }
                SQLDtl.AppendLine("Left Join TblOption D On  D.OptCode = A.DurationUom And D.OptCat = 'DurationUOM' ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");
                SQLDtl.AppendLine("And A.CancelInd = 'N' ");
                SQLDtl.AppendLine("Order By A.ItCode;");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                Sm.CmParamDt(ref cmDtl, "@MthDocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "ItCode" ,

                     //1-5
                     "ItName" ,
                     "Qty",
                     "PurchaseUomCode",
                     "UsageDt",
                     "Mth01",
                     //6-10
                     "Mth03",
                     "Mth06",
                     "Mth09",
                     "Mth12",
                     "Remark",
                     //11-15
                     "ForeignName",
                     "EstPrice",
                     "Curcode",
                     "Duration",
                     "DurationUOM",
                     //16-17
                     "Specification",
                     "StatusDesc",
                     "Location",
                     "LastUpDt",
                     "LastUpTime",
                    });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new MatReqDtl()
                        {
                            nomor = nomor,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            Qty = Sm.DrDec(drDtl, cDtl[2]),
                            PurchaseUomCode = Sm.DrStr(drDtl, cDtl[3]),
                            UsageDt = Sm.DrStr(drDtl, cDtl[4]),
                            Mth01 = Sm.DrDec(drDtl, cDtl[5]),
                            Mth03 = Sm.DrDec(drDtl, cDtl[6]),
                            Mth06 = Sm.DrDec(drDtl, cDtl[7]),
                            Mth09 = Sm.DrDec(drDtl, cDtl[8]),
                            Mth12 = Sm.DrDec(drDtl, cDtl[9]),
                            DRemark = Sm.DrStr(drDtl, cDtl[10]),
                            ForeignName = Sm.DrStr(drDtl, cDtl[11]),
                            EstPrice = Sm.DrDec(drDtl, cDtl[12]),
                            Curcode = Sm.DrStr(drDtl, cDtl[13]),
                            Duration = Sm.DrStr(drDtl, cDtl[14]),
                            DurationUom = Sm.DrStr(drDtl, cDtl[15]),
                            Specification = Sm.DrStr(drDtl, cDtl[16]),
                            TotalPrice = Sm.DrDec(drDtl, cDtl[2]) * Sm.DrDec(drDtl, cDtl[12]),
                            Status = Sm.DrStr(drDtl, cDtl[17]),
                            Location = Sm.DrStr(drDtl, cDtl[18]),
                            LastUpDt = Sm.DrStr(drDtl, cDtl[19]),
                            LastUpTime = Sm.DrStr(drDtl, cDtl[20]),

                        });
                        mUsageAmt += Sm.DrDec(drDtl, cDtl[12]);
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail Signature
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                SQLDtl2.AppendLine("Select A.CreateBy As UserCode, B.UserName, ");
                SQLDtl2.AppendLine("Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict, '" + Doctitle + "' As SignInd, E.Posname ");
                SQLDtl2.AppendLine("From TblMaterialRequestHdr A ");
                SQLDtl2.AppendLine("Inner Join TblUser B On A.CreateBy = B.UserCode ");
                SQLDtl2.AppendLine("Left Join TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQLDtl2.AppendLine("Left Join tblemployee D On A.CreateBy=D.UserCode ");
                SQLDtl2.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
                SQLDtl2.AppendLine("Where DocNo=@DocNo ");
                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0-1
                         "UserCode" ,
                         "UserName",
                         "EmpPict",
                         "SignInd",
                         "Posname"
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new MatReqDtl2()
                        {
                            UserCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            EmpPict = Sm.DrStr(drDtl2, cDtl2[2]),
                            SignInd = Sm.DrStr(drDtl2, cDtl2[3]),
                            PosName = Sm.DrStr(drDtl2, cDtl2[4]),

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Approve1
            var cm1 = new MySqlCommand();
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
            SQL1.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict, '" + Doctitle + "' As SignInd, E.PosName, ");
            SQL1.AppendLine("DATE_FORMAT(SUBSTRING(A.LastUpDt, 1, 8),'%d %M %Y') As LastUpDt ");
            SQL1.AppendLine("from TblDocApproval A ");
            SQL1.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL1.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL1.AppendLine("Left Join tblemployee D On A.UserCode=D.UserCode ");
            SQL1.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
            if (mIsMRSPPJB)
                SQL1.AppendLine("Where DocType = 'MaterialRequestSPPJB' ");
            else
                SQL1.AppendLine("Where DocType = 'MaterialRequest' ");
            SQL1.AppendLine("And DocNo =@DocNo ");
            SQL1.AppendLine("Group by ApprovalDno limit 1");

            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandText = SQL1.ToString();
                Sm.CmParam<String>(ref cm1, "@DocNo", TxtDocNo.Text);
                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-5
                         "UserCode",
                         "UserName",
                         "EmpPict",
                         "SignInd",
                         "PosName",
                         //6
                         "LastUpDt"

                        
                        });
                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        l1.Add(new MatReq1()
                        {
                            ApprovalDno = Sm.DrStr(dr1, c1[0]),
                            UserCode = Sm.DrStr(dr1, c1[1]),
                            UserName = Sm.DrStr(dr1, c1[2]),
                            EmpPict = Sm.DrStr(dr1, c1[3]),
                            SignInd = Sm.DrStr(dr1, c1[4]),
                            PosName = Sm.DrStr(dr1, c1[5]),
                            LastUpDt = Sm.DrStr(dr1, c1[6]),
                        });
                    }
                }

                dr1.Close();
            }
            myLists.Add(l1);
            #endregion

            #region Approve2

            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
            SQL2.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict,  '" + Doctitle + "' As SignInd, E.PosName, ");
            SQL2.AppendLine("DATE_FORMAT(SUBSTRING(A.LastUpDt, 1, 8),'%d %M %Y') As LastUpDt ");
            SQL2.AppendLine("from TblDocApproval A ");
            SQL2.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL2.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL2.AppendLine("Left Join tblemployee D On A.UserCode=D.UserCode ");
            SQL2.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
            if (mIsMRSPPJB)
                SQL2.AppendLine("Where DocType = 'MaterialRequestSPPJB' ");
            else
                SQL2.AppendLine("Where DocType = 'MaterialRequest' ");
            SQL2.AppendLine("And DocNo =@DocNo ");
            SQL2.AppendLine("Group by ApprovalDno Order by ApprovalDno Desc limit 1");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-5
                         "UserCode",
                         "UserName",
                         "EmpPict",
                         "SignInd",
                         "PosName",

                         "LastUpDt"

                        
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new MatReq2()
                        {
                            ApprovalDno = Sm.DrStr(dr2, c2[0]),
                            UserCode = Sm.DrStr(dr2, c2[1]),
                            UserName = Sm.DrStr(dr2, c2[2]),
                            EmpPict = Sm.DrStr(dr2, c2[3]),
                            SignInd = Sm.DrStr(dr2, c2[4]),
                            PosName = Sm.DrStr(dr2, c2[5]),
                            LastUpDt = Sm.DrStr(dr2, c2[6]),
                        });
                    }
                }

                dr2.Close();
            }
            myLists.Add(l2);
            #endregion

            #region Detail Signature KIM
            // level 2 tidak ditampilkan

            var cmDtl3 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;

                SQLDtl3.AppendLine("Select Distinct Concat(T4.ParValue, T1.UserCode, '.JPG') As Signature, ");
                SQLDtl3.AppendLine("T5.UserName, T3.PosName, T1.DNo, ");
                SQLDtl3.AppendLine("Case T1.Title When 'P' Then 'Prepared By' When 'A' Then 'Approved By' End As Title, ");
                SQLDtl3.AppendLine("Date_Format(T1.LastUpDt,'%d %M %Y') As LastUpDt ");
                SQLDtl3.AppendLine("From ( ");
                SQLDtl3.AppendLine("    Select Title, UserCode, Min(DNo) As DNo, Min(LastUpDt) LastUpDt ");
                SQLDtl3.AppendLine("    From ( ");
                SQLDtl3.AppendLine("        Select Distinct 'A' As Title, B.UserCode, B.ApprovalDNo As DNo, Left(B.LastUpDt, 8) As LastUpDt ");
                SQLDtl3.AppendLine("        From TblMaterialRequestDtl A ");
                SQLDtl3.AppendLine("        Inner Join TblDocApproval B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
                if (mIsMRSPPJB)
                    SQLDtl3.AppendLine("            And B.DocType='MaterialRequestSPPJB' ");
                else
                    SQLDtl3.AppendLine("            And B.DocType='MaterialRequest' ");
                SQLDtl3.AppendLine("        Where A.DocNo=@DocNo And A.CancelInd='N' And A.Status='A' ");
                SQLDtl3.AppendLine("        Union All ");
                SQLDtl3.AppendLine("        Select 'P' As Title, CreateBy As UserCode, '00' As DNo, Left(CreateDt, 8) As LastUpDt ");
                SQLDtl3.AppendLine("        From TblMaterialRequestDtl Where DocNo=@DocNo And CancelInd='N' And Status='A' ");
                SQLDtl3.AppendLine("    ) T Group By Title, UserCode ");
                SQLDtl3.AppendLine(") T1 ");
                SQLDtl3.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl3.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl3.AppendLine("Inner Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' And T4.ParValue Is Not Null ");
                SQLDtl3.AppendLine("Inner Join TblUser T5 On T1.UserCode=T5.UserCode ");
                SQLDtl3.AppendLine("Order By T1.DNo; ");

                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", DocNo);
                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                {
                    //0
                    "Signature" ,

                    //1-5
                    "Username", "PosName", "DNo", "Title", "LastupDt"
                });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        ldtl3.Add(new MatReqDtl3()
                        {
                            Signature = Sm.DrStr(drDtl3, cDtl3[0]),
                            UserName = Sm.DrStr(drDtl3, cDtl3[1]),
                            PosName = Sm.DrStr(drDtl3, cDtl3[2]),
                            DNo = Sm.DrStr(drDtl3, cDtl3[3]),
                            Title = Sm.DrStr(drDtl3, cDtl3[4]),
                            LastUpDt = Sm.DrStr(drDtl3, cDtl3[5]),
                            Space = "-------------------------",
                            IsPrintOutUseDigitalSignature = mIsPrintOutUseDigitalSignature
                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(ldtl3);

            #endregion

            #region Detail Signature TWC

            var cmDtl4 = new MySqlCommand();

            var SQLDtl4 = new StringBuilder();
            using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl4.Open();
                cmDtl4.Connection = cnDtl4;

                SQLDtl4.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtl4.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtl4.AppendLine("From ( ");
                SQLDtl4.AppendLine("    Select Distinct ");
                SQLDtl4.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                SQLDtl4.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                SQLDtl4.AppendLine("    From TblMaterialRequestDTL A ");
                SQLDtl4.AppendLine("    Inner Join TblDocApproval B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
                if (mIsMRSPPJB)
                    SQLDtl4.AppendLine("        And B.DocType='MaterialRequestSPPJB' ");
                else
                    SQLDtl4.AppendLine("        And B.DocType='MaterialRequest' ");
                SQLDtl4.AppendLine("    Left Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl4.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = B.DocType ");
                SQLDtl4.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtl4.AppendLine("    Union All ");
                SQLDtl4.AppendLine("    Select Distinct ");
                SQLDtl4.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, ");
                SQLDtl4.AppendLine("    '1' As DNo, 0 As Level, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                SQLDtl4.AppendLine("    From TblMaterialRequestDTL A ");
                SQLDtl4.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtl4.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtl4.AppendLine(") T1 ");
                SQLDtl4.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl4.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl4.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtl4.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.level ");
                SQLDtl4.AppendLine("Order By T1.Level; ");

                cmDtl4.CommandText = SQLDtl4.ToString();
                Sm.CmParam<String>(ref cmDtl4, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtl4, "@DocNo", DocNo);
                var drDtl4 = cmDtl4.ExecuteReader();
                var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                if (drDtl4.HasRows)
                {
                    while (drDtl4.Read())
                    {

                        ldtl4.Add(new MatReqDtl4()
                        {
                            Signature = Sm.DrStr(drDtl4, cDtl4[0]),
                            UserName = Sm.DrStr(drDtl4, cDtl4[1]),
                            PosName = Sm.DrStr(drDtl4, cDtl4[2]),
                            Space = Sm.DrStr(drDtl4, cDtl4[3]),
                            DNo = Sm.DrStr(drDtl4, cDtl4[4]),
                            Title = Sm.DrStr(drDtl4, cDtl4[5]),
                            LastUpDt = Sm.DrStr(drDtl4, cDtl4[6]),
                            IsPrintOutUseDigitalSignature = mIsPrintOutUseDigitalSignature
                        });
                    }
                }
                drDtl4.Close();
            }
            myLists.Add(ldtl4);
            #endregion

            #region Header SPPJB

            #region Old Code
            /*
            int No = 0;
            string mJobCtName = string.Empty;
            var cm4 = new MySqlCommand();
            var SQL4 = new StringBuilder();

            SQL4.AppendLine(" SELECT A.Qty, A.UPrice, B.JobName, C.JobCtName ");
            SQL4.AppendLine(" FROM TblMaterialRequestDtl2 A ");
            SQL4.AppendLine(" LEFT JOIN TblJob B ON A.JobCode = b.JobCode ");
            SQL4.AppendLine(" INNER JOIN TblJobCategory C ON B.JobCtCode = C.JobCtCode ");
            SQL4.AppendLine(" WHERE DocNo = @DocNo Order By C.JobCtName");

            using (var cn4 = new MySqlConnection(Gv.ConnectionString))
            {
                cn4.Open();
                cm4.Connection = cn4;
                cm4.CommandText = SQL4.ToString();
                Sm.CmParam<String>(ref cm4, "@DocNo", DocNo);
                var dr4 = cm4.ExecuteReader();
                var c4 = Sm.GetOrdinal(dr4, new string[] 
                        {
                        //0
                         "JobName",

                        // 1-2
                         "Qty",
                         "UPrice",

                        });
                if (dr4.HasRows)
                {
                    while (dr4.Read())
                    {
                        No = No + 1;

                        ldtl5.Add(new MatReqSPPJB()
                        {
                            No = No,
                            ItName = Sm.GetGrdStr(Grd1, 1, 11),
                            JobName = Sm.DrStr(dr4, c4[0]),
                            Qty = Sm.DrDec(dr4, c4[1]),
                            UPrice = Sm.DrDec(dr4, c4[2]),
                            Total = Sm.DrDec(dr4, c4[1]) * Sm.DrDec(dr4, c4[2])

                        });
                        //mJobCtName = Sm.DrStr(dr4, c4[1]);
                        //mUsageAmt2 += Sm.DrDec(dr4, c4[2]);
                    }

                }
                dr4.Close();
            }
            myLists.Add(ldtl5);
            */
            #endregion

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (!Sm.GetGrdBool(Grd1, i, 1)) // tidak di cancel
                {
                    ldtl5.Add(new MatReqSPPJB()
                    {
                        No = Nomor,
                        ItName = Sm.GetGrdStr(Grd1, i, 11),

                    });
                    Nomor += 1;
                }
            }
            myLists.Add(ldtl);

            #endregion

            #region HeaderSier

            var cm3 = new MySqlCommand();
            var SQL3 = new StringBuilder();

            //SQL3.AppendLine("Select DISTINCT B.DocNo, A.Amt2 BudgetAmt, (A.Amt2-SUM(C.Totalprice)) RemainingBudgetAmt, ");
            //SQL3.AppendLine("SUM(C.TotalPrice) UsageAmt, ");
            //SQL3.AppendLine("IFNULL ");
            //SQL3.AppendLine("((SELECT F.Amt2 - SUM(B.TotalPrice) ");
            //SQL3.AppendLine("FROM TblMaterialRequestHdr A ");
            //SQL3.AppendLine("INNER JOIN TblMaterialRequestDtl B ON A.DocNo = B.DocNo  ");
            //SQL3.AppendLine("INNER JOIN TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            //SQL3.AppendLine("INNER JOIN TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            //SQL3.AppendLine("INNER Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            //SQL3.AppendLine("INNER JOIN TblBudgetSummary F ON A.BCCode = F.BCCode ");
            //SQL3.AppendLine("Where A.DeptCode=@DeptCode  ");
            //SQL3.AppendLine("AND F.BCCode = @BCCode  ");
            //SQL3.AppendLine("AND F.Yr=LEFT(@DocDt, 4)  ");
            //SQL3.AppendLine("AND B.DocNo=@DocNo  ");
            //SQL3.AppendLine("GROUP BY A.DocNo) ");
            //SQL3.AppendLine(", A.Amt2 ");
            //SQL3.AppendLine(") AfterUsageAmt ");
            //SQL3.AppendLine("From TblBudgetSummary A  ");
            //SQL3.AppendLine("INNER JOIN TblMaterialRequestHdr B ON A.BCCode=B.BCCode  ");
            //SQL3.AppendLine("INNER JOIN TblMaterialRequestDtl C ON B.DocNo=C.DocNo  ");


            SQL3.AppendLine("SELECT A.Amt2 BudgetAmt, (B.AvailableBudget - B.UsageAmt) RemainingBudgetAmt, B.UsageAmt, B.AvailableBudget AfterUsageAmt, B.CurCode ");
            SQL3.AppendLine("FROM TblBudgetSummary A  ");
            SQL3.AppendLine("INNER JOIN ( ");
	        SQL3.AppendLine("    SELECT A.DocNo, A.AvailableBudget, A.BCCode, SUM(B.TotalPrice) UsageAmt, B.CurCode ");
	        SQL3.AppendLine("    FROM TblMaterialRequestHdr A ");
	        SQL3.AppendLine("    INNER JOIN TblMaterialRequestDtl B ON A.DocNo = B.DocNo ");
	        SQL3.AppendLine("    WHERE A.DocNo = @DocNo And B.CancelInd = 'N' ");
	        SQL3.AppendLine("    GROUP BY A.DocNo, A.BCCode, A.AvailableBudget ");
            SQL3.AppendLine(")B ON A.BCCode = B.BCCode ");
            SQL3.AppendLine("Where A.DeptCode=@DeptCode ");
            SQL3.AppendLine(" AND A.BCCode = @BCCode  ");
            SQL3.AppendLine(" AND A.Yr=LEFT(@DocDt, 4)  ");
            if (!mIsBudget2YearlyFormat)
                SQL3.AppendLine("   And A.Mth=SUBSTRING(@DocDt, 5, 2) ");
            SQL3.AppendLine("   AND B.DocNo=@DocNo ");

            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cm3.Connection = cn3;
                cm3.CommandText = SQL3.ToString();
                Sm.CmParam<String>(ref cm3, "@DeptCode", Sm.GetLue(LueDeptCode2).Length> 0 ? Sm.GetLue(LueDeptCode2): Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm3, "@BCCode", Sm.GetLue(LueBCCode));
                Sm.CmParam<String>(ref cm3, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<String>(ref cm3, "@DocNo", TxtDocNo.Text);

                var dr3 = cm3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[] 
                        {
                        //0
                        "BudgetAmt",

                        //1-4
                        "RemainingBudgetAmt",
                        "UsageAmt",
                        "AfterUsageAmt",
                        "CurCode"
                        });
                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        l3.Add(new MatReqSier()
                        {
                            Budget = Sm.DrDec(dr3, c3[0]),
                            AvailableBudget = decimal.Parse(TxtRemainingBudget.Text),
                            UsageAmt = decimal.Parse(TxtTotalEstPrice.Text),
                            BudgetCategory = LueBCCode.Text,
                            AfterUsageAmt = Sm.GetLue(LueProcessInd) == "D" ? decimal.Parse(TxtRemainingBudget.Text) : decimal.Parse(TxtRemainingBudget.Text) + decimal.Parse(TxtTotalEstPrice.Text),
                            CurCode = Sm.DrStr(dr3, c3[4])
                        });
                    }
                }
                dr3.Close();
            }
            myLists.Add(l3);
            #endregion

            #region Detail Signature SIER

            var cmDtl6 = new MySqlCommand();

            var SQLDtl6 = new StringBuilder();
            using (var cnDtl6 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl6.Open();
                cmDtl6.Connection = cnDtl6;

                SQLDtl6.AppendLine("Select B.EmpName, C.PosName, A.Seq, A.Title, DATE_FORMAT(A.LastUpDt, '%d/%m/%Y') as LastUpDt, A.EmpPict ");
                SQLDtl6.AppendLine("From (  ");
                SQLDtl6.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By ,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict  ");
                SQLDtl6.AppendLine("    FROM TblDocApproval A  ");
                SQLDtl6.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level > 1  ");
                SQLDtl6.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                
                SQLDtl6.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl6.AppendLine("    UNION ALL  ");
                SQLDtl6.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By ,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict  ");
                SQLDtl6.AppendLine("    FROM TblDocApproval A  ");
                SQLDtl6.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 1  ");
                SQLDtl6.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl6.AppendLine("	 Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                
                SQLDtl6.AppendLine("    UNION ALL  ");
                SQLDtl6.AppendLine("    Select A.CreateBy As UserCode, 0 As Seq, 'Created By,' As Title, Left(A.CreateDt, 8) As LastUpDt, Concat(IfNull(C.ParValue, ''), A.CreateBy, '.JPG') AS EmpPict   ");
                SQLDtl6.AppendLine("    From TblMaterialRequestHdr A  ");
                SQLDtl6.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode  ");
                SQLDtl6.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl6.AppendLine("	 Where A.Status='A' And A.DocNo = @DocNo ");
                
                SQLDtl6.AppendLine(")A  ");
                SQLDtl6.AppendLine("Left Join TblEmployee B On A.UserCode = B.UserCode And (ResignDt Is Null Or ResignDt > Left(currentdatetime(), 8))  ");
                SQLDtl6.AppendLine("Left Join TblPosition C On B.PosCode = C.PosCode  ");
                SQLDtl6.AppendLine("Group By A.UserCode, B.EmpCode, C.PosCode, A.Seq, A.Title, A.LastUpDt  ");
                SQLDtl6.AppendLine("Order By A.Seq Desc  ");

                cmDtl6.CommandText = SQLDtl6.ToString();
                Sm.CmParam<String>(ref cmDtl6, "@DocNo", DocNo);
                var drDtl6 = cmDtl6.ExecuteReader();
                var cDtl6 = Sm.GetOrdinal(drDtl6, new string[] 
                        {
                         //0
                         "EmpName" ,

                         //1-5
                         "PosName" ,
                         "LastUpDt",
                         "Seq", 
                         "Title",
                         "EmpPict"
                        });
                if (drDtl6.HasRows)
                {
                    while (drDtl6.Read())
                    {

                        lsign.Add(new SignatureSIER()
                        {
                            EmpName = Sm.DrStr(drDtl6, cDtl6[0]),
                            Position = Sm.DrStr(drDtl6, cDtl6[1]),
                            Date = Sm.DrStr(drDtl6, cDtl6[2]),
                            Sequence = Sm.DrStr(drDtl6, cDtl6[3]),
                            Title = Sm.DrStr(drDtl6, cDtl6[4]),
                            EmpPict = Sm.DrStr(drDtl6, cDtl6[5]),
                            Space = "                       ",
                            LabelName = "", //Name       : 
                            LabelPos = "", //Position    : 
                            LabelDt =   "Date : ",
                        });
                    }
                }
                drDtl6.Close();
            }
            myLists.Add(lsign);
            #endregion

            #region Header MRIMS

            var SQLH = new StringBuilder();
            var cmh = new MySqlCommand();

            SQLH.AppendLine("SELECT Distinct @CompanyLogo As CompanyLogo, A.DocNo, ");
            SQLH.AppendLine("DATE_FORMAT(A.DocDt, '%d-%m-%Y') DocDt, ");
            SQLH.AppendLine("B.SiteName, C.ProjectCode, ");
            SQLH.AppendLine("C.ProjectName, GROUP_CONCAT(Distinct T9.FinishedGood SEPARATOR ' \n') DocName  ");
            SQLH.AppendLine("FROM TblMaterialRequestHdr A  ");
            SQLH.AppendLine("LEFT JOIN TblSite B ON A.SiteCode = B.SiteCode ");
            SQLH.AppendLine("INNER JOIN tblmaterialrequestdtl T9 ON A.DocNo=T9.DocNo ");
            SQLH.AppendLine("LEFT JOIN TblProjectGroup C ON A.PGCode = C.PGCOde ");
            SQLH.AppendLine("WHERE A.DocNo = @DocNo; ");
            //SQL.AppendLine("And Exists(Select 1 From TblMaterialRequestDtl Where DocNo = @DocNo And ItCode In (Select ItCode From TblItem Where InventoryItemInd ='Y')); ");

            using (var cnh = new MySqlConnection(Gv.ConnectionString))
            {
                cnh.Open();
                cmh.Connection = cnh;
                cmh.CommandText = SQLH.ToString();
                Sm.CmParam<String>(ref cmh, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cmh, "@CompanyLogo", @Sm.CompanyLogo());

                var dr = cmh.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DocDt",
                        "SiteName",
                        "ProjectCode",
                        "ProjectName",
                        "DocName",
                        //6
                        "CompanyLogo"
                    });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new MRIMS()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            SiteName = Sm.DrStr(dr, c[2]),
                            ProjectCode = Sm.DrStr(dr, c[3]),
                            ProjectName = Sm.DrStr(dr, c[4]),
                            DocName = Sm.DrStr(dr, c[5]),
                            CompanyLogo = Sm.DrStr(dr, c[6])
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l4);

            #endregion

            #region Detail MRIMS

            var SQLD = new StringBuilder();
            var cmd = new MySqlCommand();

            SQLD.AppendLine("SELECT A.DocNo, A.DNo, B.ItCodeInternal, B.ItName, B.Specification, A.Qty, B.InventoryUomCode Uom, ");
            SQLD.AppendLine("C.ProjectCode, DATE_FORMAT(A.UsageDt, '%d-%m-%Y') UsageDt, A.Remark ");
            SQLD.AppendLine("FROM TblMaterialRequestDtl A ");
            SQLD.AppendLine("INNER JOIN TblItem B ON A.ItCode = B.ItCode ");
            SQLD.AppendLine("    AND A.DocNo = @DocNo ");
            SQLD.AppendLine("LEFT JOIN ");
            SQLD.AppendLine("( ");
            SQLD.AppendLine("    SELECT T1.DocNo, T1.DNo, GROUP_CONCAT(DISTINCT T5.ProjectCode) ProjectCode ");
            SQLD.AppendLine("    FROM TblMaterialRequestDtl T1 ");
            SQLD.AppendLine("    INNER JOIN TblBOMRevisionHdr T2 ON T1.BOMRDocNo = T2.DocNo ");
            SQLD.AppendLine("        AND T1.DocNo = @DocNo ");
            SQLD.AppendLine("    INNER JOIN TblBOQHdr T3 ON T2.BOQDocNo = T3.DocNo ");
            SQLD.AppendLine("    INNER JOIN TblLOPHdr T4 ON T3.LOPDocNo = T4.DocNo ");
            SQLD.AppendLine("    LEFT JOIN TblProjectGroup T5 ON T4.PGCode = T5.PGCode ");
            SQLD.AppendLine("    GROUP BY T1.DocNo, T1.DNo ");
            SQLD.AppendLine(") C ON A.DocNo = C.DocNo AND A.DNo = C.DNo ");
            SQLD.AppendLine("Where A.CancelInd = 'N' ");
            SQLD.AppendLine("Order by A.ItCode; ");

            using (var cnd = new MySqlConnection(Gv.ConnectionString))
            {
                cnd.Open();
                cmd.Connection = cnd;
                cmd.CommandText = SQLD.ToString();
                Sm.CmParam<String>(ref cmd, "@DocNo", DocNo);

                var drd = cmd.ExecuteReader();
                var cd = Sm.GetOrdinal(drd, new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DNo",
                        "ItCodeInternal",
                        "ItName",
                        "Specification",
                        "Qty",
                        //6-9
                        "Uom",
                        "ProjectCode",
                        "UsageDt",
                        "Remark"
                    });

                if (drd.HasRows)
                {
                    int mNo = 1;
                    while (drd.Read())
                    {
                        ldtl6.Add(new MRIMSDtl()
                        {
                            No = mNo,
                            DocNo = Sm.DrStr(drd, cd[0]),
                            DNo = Sm.DrStr(drd, cd[1]),
                            ItCodeInternal = Sm.DrStr(drd, cd[2]),
                            ItName = Sm.DrStr(drd, cd[3]),
                            Specification = Sm.DrStr(drd, cd[4]),
                            Qty = Sm.DrDec(drd, cd[5]),
                            Uom = Sm.DrStr(drd, cd[6]),
                            ProjectCode = Sm.DrStr(drd, cd[7]),
                            UsageDt = Sm.DrStr(drd, cd[8]),
                            Remark = Sm.DrStr(drd, cd[9])
                        });
                        mNo += 1;
                    }
                }
                drd.Close();
            }
            myLists.Add(ldtl6);

            #endregion

            #region Detail Signature MRIMS
            var SQLS = new StringBuilder();
            var cms = new MySqlCommand();

            SQLS.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
            SQLS.AppendLine("T1.UserCode, T1.UserName, T3.PosName, ");
            SQLS.AppendLine("T1.DNo, T1.Seq As Level, @Space As Space, ");
            SQLS.AppendLine("T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
            SQLS.AppendLine("From ( ");

            SQLS.AppendLine("    Select UserCode, UserName, DNo, Seq, Title, LastUpDt From ( ");
            SQLS.AppendLine("        Select Distinct B.UserCode, C.UserName, ");
            SQLS.AppendLine("        B.ApprovalDNo As DNo, 900+D.Level As Seq, ");
            SQLS.AppendLine("        'Approved By' As Title, ");
            SQLS.AppendLine("        Left(B.LastUpDt, 8) As LastUpDt ");
            SQLS.AppendLine("        From TblMaterialRequestDtl A ");
            SQLS.AppendLine("        Inner Join TblDocApproval B On A.DocNo=B.DocNo AND A.DocNo = @DocNo ");
            SQLS.AppendLine("        Inner Join TblUser C On B.UserCode=C.UserCode ");
            SQLS.AppendLine("        Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType='MaterialRequest' ");
            SQLS.AppendLine("        Where A.Status='A' ");
            SQLS.AppendLine("        And A.DocNo=@DocNo ");
            SQLS.AppendLine("        And B.UserCode Not In (Select CreateBy From TblMaterialRequestHdr Where DocNo = @DocNo) ");
            SQLS.AppendLine("        Order By D.Level Desc ");
            SQLS.AppendLine("    ) Tbl ");

            SQLS.AppendLine("    Union All ");
            SQLS.AppendLine("    Select Distinct ");
            SQLS.AppendLine("    A.CreateBy As UserCode, B.UserName, ");
            SQLS.AppendLine("    '1' As DNo, 0 As Seq, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
            SQLS.AppendLine("    From TblMaterialRequestHdr A ");
            SQLS.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode And A.DocNo=@DocNo ");
            SQLS.AppendLine(") T1 ");
            SQLS.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
            SQLS.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
            SQLS.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
            SQLS.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
            SQLS.AppendLine("Order By T1.Seq; ");

            using (var cns = new MySqlConnection(Gv.ConnectionString))
            {
                cns.Open();
                cms.Connection = cns;
                cms.CommandText = SQLS.ToString();
                Sm.CmParam<String>(ref cms, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cms, "@DocNo", DocNo);

                var drs = cms.ExecuteReader();
                var cs = Sm.GetOrdinal(drs, new string[]
                {
                    //0
                     "Signature",
                     //1-5
                     "Username",
                     "PosName",
                     "Space",
                     "Level",
                     "Title",
                     "LastupDt"
                });

                if (drs.HasRows)
                {
                    while (drs.Read())
                    {
                        lsign2.Add(new MRIMSSign()
                        {
                            Signature = Sm.DrStr(drs, cs[0]),
                            UserName = Sm.DrStr(drs, cs[1]),
                            PosName = Sm.DrStr(drs, cs[2]),
                            Space = Sm.DrStr(drs, cs[3]),
                            DNo = Sm.DrStr(drs, cs[4]),
                            Title = Sm.DrStr(drs, cs[5]),
                            LastUpDt = Sm.DrStr(drs, cs[6])
                        });
                    }
                }
                drs.Close();
            }
            myLists.Add(lsign2);
            #endregion

            #region Detail Signature2 MRIMS

            var SQLS2 = new StringBuilder();
            var cms2 = new MySqlCommand();

            SQLS2.AppendLine("SELECT * ");
            SQLS2.AppendLine("FROM  ");
            SQLS2.AppendLine("( ");
            SQLS2.AppendLine("    SELECT UPPER(B.UserName) CreateUserName, DATE_FORMAT(LEFT(A.CreateDt, 8), '%d-%m-%Y') CreateDocDt ");
            SQLS2.AppendLine("    FROM TblMaterialRequestHdr A ");
            SQLS2.AppendLine("    INNER JOIN TblUser B ON A.CreateBy = B.UserCode ");
            SQLS2.AppendLine("        AND A.DocNo = @DocNo ");
            SQLS2.AppendLine(") A ");
            SQLS2.AppendLine("LEFT JOIN ");
            SQLS2.AppendLine("( ");
            SQLS2.AppendLine("    SELECT Distinct UPPER(T3.UserName) ApproveUserName, IFNULL(DATE_FORMAT(LEFT(T1.LastUpDt, 8), '%d-%m-%Y'), '') ApproveDocDt ");
            SQLS2.AppendLine("    FROM TblDocApproval T1 ");
            SQLS2.AppendLine("    INNER JOIN TblDocApprovalSetting T2 ON T1.DocType = T2.DocType ");
            SQLS2.AppendLine("        AND T1.DocNo = @DocNo ");
            SQLS2.AppendLine("        AND T1.ApprovalDNo = T2.DNo ");
            SQLS2.AppendLine("        AND T2.Level IN  ");
            SQLS2.AppendLine("        ( ");
            SQLS2.AppendLine("	            SELECT MAX(B.Level) MaxLevel ");
            SQLS2.AppendLine("	            FROM TblDocApproval A ");
            SQLS2.AppendLine("	            INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
            SQLS2.AppendLine("	                AND A.ApprovalDNo = B.DNo ");
            SQLS2.AppendLine("	                AND A.DocNo = @DocNo ");
            SQLS2.AppendLine("        ) ");
            SQLS2.AppendLine("    INNER JOIN TblUser T3 ON T1.UserCode = T3.UserCode AND T1.Status = 'A' ");
            SQLS2.AppendLine("        AND T3.UserCode NOT IN (SELECT CreateBy FROM TblMaterialRequestHdr WHERE DocNo = @DocNo) ");
            SQLS2.AppendLine(") B ON 0 = 0 ");
            SQLS2.AppendLine("LEFT JOIN  ");
            SQLS2.AppendLine("( ");
            SQLS2.AppendLine("    SELECT Distinct UPPER(T3.UserName) Approve2UserName, IFNULL(DATE_FORMAT(LEFT(T1.LastUpDt, 8), '%d-%m-%Y'), '') Approve2DocDt ");
            SQLS2.AppendLine("    FROM TblDocApproval T1 ");
            SQLS2.AppendLine("    INNER JOIN TblDocApprovalSetting T2 ON T1.DocType = T2.DocType ");
            SQLS2.AppendLine("        AND T1.DocNo = @DocNo ");
            SQLS2.AppendLine("        AND T1.ApprovalDNo = T2.DNo ");
            SQLS2.AppendLine("        AND T2.Level != 1 ");
            SQLS2.AppendLine("        AND T2.Level IN  ");
            SQLS2.AppendLine("        ( ");
            SQLS2.AppendLine("	            SELECT MAX(B.Level) - 1 MaxLevel ");
            SQLS2.AppendLine("	            FROM TblDocApproval A ");
            SQLS2.AppendLine("	            INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
            SQLS2.AppendLine("	                AND A.ApprovalDNo = B.DNo ");
            SQLS2.AppendLine("	                AND A.DocNo = @DocNo ");
            SQLS2.AppendLine("        ) ");
            SQLS2.AppendLine("    INNER JOIN TblUser T3 ON T1.UserCode = T3.UserCode AND T1.Status = 'A' ");
            SQLS2.AppendLine("        AND T3.UserCode NOT IN (SELECT CreateBy FROM TblMaterialRequestHdr WHERE DocNo = @DocNo) ");
            SQLS2.AppendLine(") C ON 0 = 0 ");
            SQLS2.AppendLine("LEFT JOIN  ");
            SQLS2.AppendLine("( ");
            SQLS2.AppendLine("    SELECT Distinct UPPER(T3.UserName) Approve3UserName, IFNULL(DATE_FORMAT(LEFT(T1.LastUpDt, 8), '%d-%m-%Y'), '') Approve3DocDt ");
            SQLS2.AppendLine("    FROM TblDocApproval T1 ");
            SQLS2.AppendLine("    INNER JOIN TblDocApprovalSetting T2 ON T1.DocType = T2.DocType ");
            SQLS2.AppendLine("        AND T1.DocNo = @DocNo ");
            SQLS2.AppendLine("        AND T1.ApprovalDNo = T2.DNo ");
            SQLS2.AppendLine("        AND T2.Level IN  ");
            SQLS2.AppendLine("        ( ");
            SQLS2.AppendLine("                SELECT MAX(B.Level) - 2 MaxLevel ");
            SQLS2.AppendLine("                FROM TblDocApproval A ");
            SQLS2.AppendLine("                INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
            SQLS2.AppendLine("                    AND A.ApprovalDNo = B.DNo ");
            SQLS2.AppendLine("                    AND A.DocNo = @DocNo ");
            SQLS2.AppendLine("        ) ");
            SQLS2.AppendLine("    INNER JOIN TblUser T3 ON T1.UserCode = T3.UserCode AND T1.Status = 'A' ");
            SQLS2.AppendLine("        AND T3.UserCode NOT IN (SELECT CreateBy FROM TblMaterialRequestHdr WHERE DocNo = @DocNo) ");
            SQLS2.AppendLine(") D ON 0 = 0 ");

            using (var cns2 = new MySqlConnection(Gv.ConnectionString))
            {
                cns2.Open();
                cms2.Connection = cns2;
                cms2.CommandText = SQLS2.ToString();
                Sm.CmParam<String>(ref cms2, "@DocNo", DocNo);

                var drs2 = cms2.ExecuteReader();
                var cs2 = Sm.GetOrdinal(drs2, new string[]
                {
                    //0
                     "CreateUserName",
                     //1-5
                     "CreateDocDt",
                     "ApproveUserName",
                     "ApproveDocDt",
                     "Approve2UserName",
                     "Approve2DocDt",
                     //6-7
                     "Approve3UserName",
                     "Approve3DocDt"
                });

                if (drs2.HasRows)
                {
                    while (drs2.Read())
                    {
                        lsign3.Add(new MRIMSSign2()
                        {
                            CreateUserName = Sm.DrStr(drs2, cs2[0]),
                            CreateDocDt = Sm.DrStr(drs2, cs2[1]),
                            ApproveUserName = Sm.DrStr(drs2, cs2[2]),
                            ApproveDocDt = Sm.DrStr(drs2, cs2[3]),
                            Approve2UserName = Sm.DrStr(drs2, cs2[4]),
                            Approve2DocDt = Sm.DrStr(drs2, cs2[5]),
                            Approve3UserName = Sm.DrStr(drs2, cs2[6]),
                            Approve3DocDt = Sm.DrStr(drs2, cs2[7])
                        });
                    }
                }
                drs2.Close();
            }

            myLists.Add(lsign3);

            #endregion

            #region Signature MR SPPJB SIER

            var cmDtl10 = new MySqlCommand();
            var SQLDtl10 = new StringBuilder();

            SQLDtl10.AppendLine("Select B.EmpName, C.PosName, A.Seq, A.Title, DATE_FORMAT(A.LastUpDt, '%d/%m/%Y') as LastUpDt,  A.EmpPict  ");
            SQLDtl10.AppendLine("FROM ( ");
            SQLDtl10.AppendLine("SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
            SQLDtl10.AppendLine("FROM TblDocApproval A ");
            SQLDtl10.AppendLine("INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND (B.Level = 4 OR B.Level = 5) ");
            SQLDtl10.AppendLine("LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQLDtl10.AppendLine("Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
            SQLDtl10.AppendLine("UNION ALL ");
            SQLDtl10.AppendLine("SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
            SQLDtl10.AppendLine("FROM TblDocApproval A ");
            SQLDtl10.AppendLine("INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 3 ");
            SQLDtl10.AppendLine("LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQLDtl10.AppendLine("Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
            SQLDtl10.AppendLine("UNION ALL ");
            SQLDtl10.AppendLine("SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
            SQLDtl10.AppendLine("FROM TblDocApproval A ");
            SQLDtl10.AppendLine("INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 2 ");
            SQLDtl10.AppendLine("LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQLDtl10.AppendLine("Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
            SQLDtl10.AppendLine("UNION ALL ");
            SQLDtl10.AppendLine("SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
            SQLDtl10.AppendLine("FROM TblDocApproval A ");
            SQLDtl10.AppendLine("INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 1 ");
            SQLDtl10.AppendLine("LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQLDtl10.AppendLine("Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
            SQLDtl10.AppendLine("UNION ALL ");
            SQLDtl10.AppendLine("Select A.CreateBy As UserCode, 0 As Seq, 'Created By,' As Title, Left(A.CreateDt, 8) As LastUpDt, Concat(IfNull(C.ParValue, ''), A.CreateBy, '.JPG') AS EmpPict ");
            SQLDtl10.AppendLine("From TblMaterialRequestHdr A ");
            SQLDtl10.AppendLine("Inner Join TblUser B On A.CreateBy=B.UserCode ");
            SQLDtl10.AppendLine("LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQLDtl10.AppendLine("Where A.DocNo = @DocNo ");
            SQLDtl10.AppendLine(") A ");
            SQLDtl10.AppendLine("Left Join TblEmployee B On A.UserCode = B.UserCode And (ResignDt Is Null Or ResignDt > Left(currentdatetime(), 8)) ");
            SQLDtl10.AppendLine("Left Join TblPosition C On B.PosCode = C.PosCode ");
            SQLDtl10.AppendLine("Group By A.UserCode, B.EmpCode, C.PosCode, A.Seq, A.Title, A.LastUpDt ");
            SQLDtl10.AppendLine("Order By A.Seq Desc ");

            using (var cnDtl10 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl10.Open();
                cmDtl10.Connection = cnDtl10;
                cmDtl10.CommandText = SQLDtl10.ToString();
                Sm.CmParam<String>(ref cmDtl10, "@DocNo", TxtDocNo.Text);
                var drDtl10 = cmDtl10.ExecuteReader();
                var cDtl10 = Sm.GetOrdinal(drDtl10, new string[]
                {
                    //0
                    "EmpName" ,

                    //1-4
                    "PosName" ,
                    "LastUpDt",
                    "Seq",
                    "Title",
                    "EmpPict",
                });
                if (drDtl10.HasRows)
                {
                    while (drDtl10.Read())
                    {
                        lsign4.Add(new MRSPPJBSignSIER()
                        {
                            EmpName = Sm.DrStr(drDtl10, cDtl10[0]),
                            Position = Sm.DrStr(drDtl10, cDtl10[1]),
                            Date = Sm.DrStr(drDtl10, cDtl10[2]),
                            Sequence = Sm.DrStr(drDtl10, cDtl10[3]),
                            Title = Sm.DrStr(drDtl10, cDtl10[4]),
                            EmpPict = Sm.DrStr(drDtl10, cDtl10[5]),
                            Space = "                       ",
                            LabelName = "", //Name       : 
                            LabelPos = "", //Position    : 
                            LabelDt = "Date : ",
                        });
                    }
                }
                drDtl10.Close();
            }
            myLists.Add(lsign4);

            #endregion

            #region MRSPPJBBOQDtl

            var cmDtl11 = new MySqlCommand();
            var SQLDtl11 = new StringBuilder();

            SQLDtl11.AppendLine("SELECT DocNo, DNo, GrpType, GrpName, JobName, Qty, UoMCode, Price, Remark ");
            SQLDtl11.AppendLine("FROM tblmaterialrequestdtl3 ");
            SQLDtl11.AppendLine("WHERE DocNo = @DocNo ");
            SQLDtl11.AppendLine("ORDER BY CAST(grptype AS int), DNo; ");

            using (var cnDtl11 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl11.Open();
                cmDtl11.Connection = cnDtl11;
                cmDtl11.CommandText = SQLDtl11.ToString();
                Sm.CmParam<String>(ref cmDtl11, "@DocNo", TxtDocNo.Text);
                var drDtl11 = cmDtl11.ExecuteReader();
                var cDtl11 = Sm.GetOrdinal(drDtl11, new string[]
                {
                    //0
                    "GrpType",

                    //1-5
                    "GrpName",
                    "JobName",
                    "Qty",
                    "UoMCode",
                    "Price",

                    //6-8
                    "Remark",
                });
                if (drDtl11.HasRows)
                {
                    int No = 0;
                    int i = 1;
                    while (drDtl11.Read())
                    {
                        if (i == Sm.DrDec(drDtl11, cDtl11[0]))
                        {
                            No = No + 1;
                        }
                        else
                        {
                            No = 1;
                            i++;
                        }
                        l5.Add(new MRSPPJBBOQDtl()
                        {
                            //0
                            No = No,

                            //1-5
                            GrpType = Sm.DrStr(drDtl11, cDtl11[0]),
                            GrpName = Sm.DrStr(drDtl11, cDtl11[1]),
                            JobName = Sm.DrStr(drDtl11, cDtl11[2]),
                            Qty = Sm.DrDec(drDtl11, cDtl11[3]),
                            UoMCode = Sm.DrStr(drDtl11, cDtl11[4]),
                            Price = Sm.DrDec(drDtl11, cDtl11[5]),

                            //6-10
                            Total1 = Sm.DrDec(drDtl11, cDtl11[3]) * Sm.DrDec(drDtl11, cDtl11[5]),
                            Remark = Sm.DrStr(drDtl11, cDtl11[6]),
                        });
                    }
                }
                drDtl11.Close();
            }
            myLists.Add(l5);

            #endregion

            #region MRSPPJBBOQHdr

            var cmDtl12 = new MySqlCommand();
            var SQLDtl12 = new StringBuilder();

            SQLDtl12.AppendLine("SELECT SUM(Qty*Price) as Total, SUM(Qty*Price)*10/100 as Tax ");
            SQLDtl12.AppendLine("FROM tblmaterialrequestdtl3 ");
            SQLDtl12.AppendLine("WHERE DocNo = @DocNo ");

            using (var cnDtl12 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl12.Open();
                cmDtl12.Connection = cnDtl12;
                cmDtl12.CommandText = SQLDtl12.ToString();
                Sm.CmParam<String>(ref cmDtl12, "@DocNo", TxtDocNo.Text);
                var drDtl12 = cmDtl12.ExecuteReader();
                var cDtl12 = Sm.GetOrdinal(drDtl12, new string[]
                {
                    //0
                    "Total",

                    //1
                    "Tax",
                });
                if (drDtl12.HasRows)
                {
                    while (drDtl12.Read())
                    {
                        l6.Add(new MRSPPJBBOQHdr()
                        {
                            //0
                            Total = Sm.Round((Sm.DrDec(drDtl12, cDtl12[0]) + Sm.DrDec(drDtl12, cDtl12[1])),0),

                            //1
                            Terbilang = Sm.Terbilang(Sm.DrDec(drDtl12, cDtl12[0])+ Sm.DrDec(drDtl12, cDtl12[1])),
                        });
                    }
                }
                drDtl12.Close();
            }
            myLists.Add(l6);

            #endregion

            myLists.Add(mlJob);

            #region Old Code by wed
            //if (!mIsUseItemConsumption)
            //{
            //    if (Doctitle == "TWC") 
            //        Sm.PrintReport("MaterialRequestTWC", myLists, TableName, false);
            //    else if (Doctitle == "SIER")
            //    {
            //        if (mIsMRSPPJBEnabled)
            //        {
            //            if (mIsMRSPPJB)
            //            {
            //                Sm.PrintReport("MaterialRequestSPPJB", myLists, TableName, false);
            //                Sm.PrintReport("MaterialRequestSPPJB_2", myLists, TableName, false);
            //            }

            //            else
            //                Sm.PrintReport("MaterialRequestSIER", myLists, TableName, false);
            //        }
            //    }
            //    else if (Doctitle == "SRN")
            //    {
            //        Sm.PrintReport("MaterialRequest2TWC", myLists, TableName, false);
            //    }
            //    else
            //        Sm.PrintReport("MaterialRequest", myLists, TableName, false);
            //}
            //else
            //{
            //    if (Sm.GetParameter("DocTitle") == "KIM")
            //        Sm.PrintReport("MaterialRequestKIM", myLists, TableName, false);
            //    else
            //        Sm.PrintReport("MaterialRequest2", myLists, TableName, false);
            //}
            #endregion

            if (!mIsUseItemConsumption)
            {
                if (mIsMRSPPJBEnabled)
                {
                    if (mIsMRSPPJB)
                    {
                         Sm.PrintReport("MaterialRequestSPPJBSIER", myLists, TableName, false);
                        //Sm.PrintReport(mFormPrintOutMaterialRequestSPPJB, myLists, TableName, false);
                        //Sm.PrintReport(mFormPrintOutMaterialRequestSPPJB2, myLists, TableName, false);
                    }
                    else
                        Sm.PrintReport(mFormPrintOutMaterialRequestWithoutConsumption, myLists, TableName, false);
                }
                else
                {
                    if (Doctitle == "IMS")
                    {
                        Sm.PrintReport(mFormPrintOutMaterialRequestWithoutConsumption, myLists, TableName, false);
                    }
                    else
                    {
                        Sm.PrintReport(mFormPrintOutMaterialRequestWithoutConsumption, myLists, TableName, false);
                    }
                }
            }
            else
            {
                Sm.PrintReport(mFormPrintOutMaterialRequestWithConsumption, myLists, TableName, false);
            }
        }

        internal void ShowPOQtyCancelInfo(string DocNo)
        {
            TxtRemainingBudget.EditValue = Sm.FormatNum(0m, 0);
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, D.DocDt, D.ReqType, D.DeptCode, Concat('Replacement for ', D.DocNo) As RemarkH, ");
            SQL.AppendLine("E.ItCode, F.ItName, A.Qty, F.PurchaseUomCode, E.UsageDt, E.Remark As RemarkD, ");
            SQL.AppendLine("F.ItSCCode, H.ItSCName ");
            SQL.AppendLine("From TblPOQtyCancel A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr D On C.MaterialRequestDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On C.MaterialRequestDocNo=E.DocNo And C.MaterialRequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblItem F On E.ItCode=F.ItCode ");
            SQL.AppendLine("Inner Join TblDepartment G On D.DeptCode=G.DeptCode ");
            SQL.AppendLine("Left Join TblItemSubCategory H On F.ItSCCode=H.ItSCCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "ReqType", "DeptCode", "RemarkH", "ItCode", 
                        //6-10
                        "ItName", "Qty", "PurchaseUomCode", "UsageDt", "RemarkD",
                        //11-12
                        "ItSCCode", "ItSCName"
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        TxtPOQtyCancelDocNo.EditValue = Sm.DrStr(dr, 0);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueReqType, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 8, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 11, 6);
                        Sm.SetGrdValue("N", Grd1, dr, c, 0, 17, 7);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 18, 8);
                        Sm.SetGrdValue("D", Grd1, dr, c, 0, 19, 9);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 25, 10);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 13, 11);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 14, 12);
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            Grd1.Rows.Add();
            Sm.SetGrdBoolValueFalse(ref Grd1, 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 1, new int[] { 17, 23, 24, 31, 32 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private string ItemSelection()
        {
            var SQL = new StringBuilder();

            if (Sm.CompareStr(Sm.GetLue(LueReqType), "2"))
            {
                SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, Null As DocNo, Null As DNo, Null As DocDt, 0 As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("A.ItName, A.ForeignName, ");
                SQL.AppendLine("ifnull(C.Qty01, 0) As Mth01, ifnull(C.Qty03, 0) As Mth03, ifnull(C.Qty06, 0) As Mth06, ifnull(C.Qty09, 0) As Mth09, ifnull(C.Qty12, 0) As Mth12, ");
                SQL.AppendLine("A.ItScCode, D.ItScName, A.ItCodeInternal, A.ItGrpCode, E.ItGrpName ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" ) C On C.ItCode = A.ItCode ");
                SQL.AppendLine("Left Join TblItemSubCategory D On A.ItScCode = D.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup E On A.ItGrpCode=E.ItGrpCode ");
            }
            else
            {
                SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, C.DocNo, C.DNo, C.DocDt, ");
                SQL.AppendLine("A.ItName, A.ForeignName, ");
                SQL.AppendLine("C.UPrice*");
                SQL.AppendLine("    Case When IfNull(C.CurCode, '')=D.ParValue Then 1 ");
                SQL.AppendLine("    Else IfNull(E.Amt, 0) ");
                SQL.AppendLine("    End As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("ifnull(F.Qty01, 0) As Mth01, ifnull(F.Qty03, 0) As Mth03, ifnull(F.Qty06, 0) As Mth06, ifnull(F.Qty09, 0) As Mth09, ifnull(F.Qty12, 0) As Mth12, ");
                SQL.AppendLine("A.ItScCode, G.ItScName, A.ItCodeInternal, A.ItGrpCode, H.ItGrpName ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, T2.DNo, T1.DocDt, T2.ItCode, T1.CurCode, T2.UPrice ");
                SQL.AppendLine("    From TblQtHdr T1 ");
                SQL.AppendLine("    Inner Join TblQtDtl T2 On T1.DocNo=T2.DocNo And T2.ActInd='Y' ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select T3b.ItCode, Max(T3a.SystemNo) As Key1 ");
                SQL.AppendLine("        From TblQtHdr T3a, TblQtDtl T3b ");
                SQL.AppendLine("        Where T3a.DocNo=T3b.DocNo And T3b.ActInd='Y' ");
                SQL.AppendLine("        Group By T3b.ItCode ");
                SQL.AppendLine("    ) T3 On T1.SystemNo=T3.Key1 And T2.ItCode=T3.ItCode ");
                SQL.AppendLine(") C On A.ItCode=C.ItCode ");
                SQL.AppendLine("Left Join TblParameter D On D.ParCode='MainCurCode' ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.CurCode CurCode1, T2.CurCode As CurCode2, IfNull(T3.Amt, 0) As Amt ");
                SQL.AppendLine("    From TblCurrency T1 ");
                SQL.AppendLine("    Inner Join TblCurrency T2 On T1.CurCode<>T2.CurCode ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select T3a.RateDt, T3a.CurCode1, T3a.CurCode2, T3a.Amt ");
                SQL.AppendLine("        From TblCurrencyRate T3a  ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select CurCode1, CurCode2, Max(RateDt) As RateDt  ");
                SQL.AppendLine("            From TblCurrencyRate Group By CurCode1, CurCode2 ");
                SQL.AppendLine("        ) T3b On T3a.CurCode1=T3b.CurCode1 And T3a.CurCode2=T3b.CurCode2 And T3a.RateDt=T3b.RateDt ");
                SQL.AppendLine("    ) T3 On T1.CurCode=T3.CurCode1 And T2.CurCode=T3.CurCode2 ");
                SQL.AppendLine(") E On  E.CurCode1=C.CurCode And E.CurCode2=D.ParValue ");
                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" ) F On F.ItCode = A.ItCode ");
                SQL.AppendLine("Left Join TblItemSubCategory G On A.ItScCode=G.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup H On A.ItGrpCode=H.ItGrpCode ");

            }

            SQL.AppendLine("Where A.ActInd = 'Y' ");

            if (Sm.IsDataExist("Select ParCode From TblParameter Where ParCode='ItemManagedByWhsInd' And ParValue='Y' Limit 1 "))
                SQL.AppendLine("And (IfNull(A.ControlByDeptCode, '')='' Or (IfNull(A.ControlByDeptCode, '')<>'' And A.ControlByDeptCode=@DeptCode)) ");

            return SQL.ToString();
        }

        internal void ShowDORequestItem(string DocNo)
        {
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo As DORequestDocNo, B.DNo As DORequestDNo, B.ItCode, ");
            SQL.AppendLine("X.ItCtName, X.PurchaseUomCode, X.DocNo, X.DNo, X.DocDt, X.UPrice, X.MinStock, X.ReorderStock, ");
            SQL.AppendLine("X.ItName, X.ForeignName, ");
            SQL.AppendLine("X.Mth01, X.Mth03, X.Mth06, X.Mth09, X.Mth12, ");
            SQL.AppendLine("X.ItScCode, X.ItScName, X.ItCodeInternal, X.ItGrpCode, X.ItGrpName, ");
            SQL.AppendLine("(IfNull(B.Qty, 0.00)-IfNull(D.Qty, 0.00)) As MRQty ");
            SQL.AppendLine("From TblDORequestDeptHdr A ");
            SQL.AppendLine("Inner Join TblDORequestDeptDtl B On A.DocNo = B.DocNo And B.ProcessInd = 'O' ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine(ItemSelection());

            SQL.AppendLine(")X On B.ItCode = X.ItCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  Select ItCode, Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
            SQL.AppendLine("  From TblStockSummary ");
            SQL.AppendLine("  Where WhsCode = @WhsCode ");
            SQL.AppendLine("  And Qty>0 ");
            SQL.AppendLine("  Group By ItCode ");
            SQL.AppendLine(") D On B.ItCode = D.ItCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And IfNull(B.Qty, 0.00)>IfNull(D.Qty, 0.00) ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetValue("Select WhsCode From TblDORequestDeptHdr Where DocNo = '" + DocNo + "'; "));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@DocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",

                    //1-5
                    "ItCodeInternal", "ItGrpName", "ItName", "ForeignName",  "ItCtName",   
                    
                    //6-7
                    "ItScCode", "ItScName", "PurchaseUomCode", "DocNo", "DNo", 

                    //11-15
                    "DocDt", "UPrice", "MinStock", "ReorderStock", "Mth01",   

                    //16-20
                    "Mth03", "Mth06", "Mth09", "Mth12", "DORequestDocNo",
                    
                    //21-22
                    "DORequestDNo", "MRQty"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 10);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 22);
                    Grd1.Cells[Row, 29].Value = 0;

                    if (mIsBudgetActive)
                        Grd1.Cells[Row, 11].ForeColor = Sm.GetGrdStr(Grd1, Row, 20).Length > 0 ? Color.Black : Color.Red;
                    ComputeTotal(Row);
                }, false, false, true, false
            );

            Sm.FocusGrd(Grd1, 0, 1);

            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 15, 16, 17, 23, 24, 29, 31, 32 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void SetLueDeptCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");
            SQL.AppendLine("Where 1=1 ");
            SQL.AppendLine("And T.ActInd = 'Y' ");
            if (mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsDeptFilterBySite)
            {
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select DeptCode From TblDepartmentBudgetSite ");
                SQL.AppendLine("    Where SiteCode=@SiteCode ");
                SQL.AppendLine(") ");
            }
            if (mIsMRUseDivision && Sm.GetLue(LueDivCode).Length > 0)
            {
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select DeptCode From TblDepartment ");
                SQL.AppendLine("    Where DivisionCode=@DivisionCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DivisionCode", Sm.GetLue(LueDivCode));

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue, string SiteCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                if (SiteCode.Length == 0)
                    SQL.AppendLine("Where T.ActInd='Y' ");
                else
                    SQL.AppendLine("Where T.SiteCode=@SiteCode ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By SiteName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueReqType(ref DXE.LookUpEdit Lue, string ReqType)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
                SQL.AppendLine("From TblOption Where OptCat='ReqType' ");
                if (ReqType.Length > 0)
                    SQL.AppendLine("And OptCode=@ReqType ");
                else
                {
                    if (!mIsMaterialRequestForDroppingRequestEnabled)
                    {
                        if (mIsBudgetActive && mReqTypeForNonBudget.Length > 0)
                            SQL.AppendLine("And OptCode<>@ReqTypeForNonBudget ");
                    }
                }
                SQL.AppendLine("Order By OptDesc;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                Sm.CmParam<String>(ref cm, "@ReqType", ReqType);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Type", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueDivisionCode(ref LookUpEdit Lue)
        {

            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select DivisionCode As Col1, DivisionName As Col2 ");
                SQL.AppendLine("From TblDivision");
                if(Sm.GetLue(LueDeptCode).Length > 0)
                {
                    SQL.AppendLine("Where DivisionCode =  ");
                    SQL.AppendLine("( SELECT DivisionCode FROM tbldepartment WHERE DeptCode = @DeptCode )");
                }
                SQL.AppendLine("Order By DivisionName;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Type", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetBudgetCategory()
        {
            LueBCCode.EditValue = null;
            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, true);

            if (!mIsBudgetTransactionCanUseOtherDepartmentBudget)
            {
                var ReqType = Sm.GetLue(LueReqType);
                var DeptCode = Sm.GetLue(LueDeptCode);

                if (
                    mReqTypeForNonBudget.Length == 0 ||
                    ReqType.Length == 0 ||
                    Sm.CompareStr(ReqType, mReqTypeForNonBudget) ||
                    DeptCode.Length == 0
                    ) return;

                Sl.SetLueBCCode(ref LueBCCode, string.Empty, DeptCode);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, false);
            }
            else return;
        }

        private bool ShowPrintApproval()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select Status From TblMaterialRequestDtl " +
                    "Where DocNo=@DocNo And Status ='O';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Can't print. This document has not been approved.");
                return true;
            }
            return false;
        }

        private void ShowMRApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            //SQL.AppendLine("Select B.UserName, Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc, ");
            //SQL.AppendLine("Case When A.LastUpDt Is Not Null Then  ");
            //SQL.AppendLine("Concat(Substring(A.LastUpDt, 7, 2), '/', Substring(A.LastUpDt, 5, 2), '/', Left(A.LastUpDt, 4))  ");
            //SQL.AppendLine("Else Null End As LastUpDt, A.Remark, ");
            //SQL.AppendLine("case D.ActingOfficialInd when 'Y' then CONCAT(D.PosName, ' (PLT)') when 'N' then D.PosName end AS Position  ");
            //SQL.AppendLine("From TblDocApproval A ");
            //SQL.AppendLine("Left JOIN tbluser B ON A.UserCode = B.UserCode ");
            //SQL.AppendLine("Left JOIN tblemployee C ON B.UserCode = C.UserCode");
            //SQL.AppendLine("Left JOIN tblposition D ON C.PosCode = D.PosCode");
            //if (mIsMRSPPJB)
            //    SQL.AppendLine("Where DocType = 'MaterialRequestSPPJB' ");
            //else
            //    SQL.AppendLine("Where DocType='MaterialRequest' ");
            //SQL.AppendLine("And Status In ('A', 'C') ");
            //SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo ");
            //SQL.AppendLine("Order By ApprovalDNo;");

            SQL.AppendLine("SELECT Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' ELSE 'Outstanding' End As StatusDesc, ");
            if (mIsMaterialRequestUseDetailApprovalInformation)
                SQL.AppendLine("IFNULL(F.UserName, GROUP_CONCAT(C.UserName Separator '/')) as UserName,");
            else
                SQL.AppendLine("A.UserCode as UserName,");
            if (mIsPltCheckboxNotBasedOnPosition)
            {
                SQL.AppendLine("Case When A.UserCode Is Not Null Then IF(G.ActingOfficialInd = 'Y', CONCAT(H.PosName,' (PLT)'), H.PosName) ");
                SQL.AppendLine("Else GROUP_CONCAT(IF(D.ActingOfficialInd = 'Y', CONCAT(E.PosName,' (PLT)'), E.PosName)SEPARATOR '/') End AS Position, ");
            }
            else
            {
                SQL.AppendLine("Case When A.UserCode Is Not Null Then IF(H.ActingOfficialInd = 'Y', CONCAT(H.PosName,' (PLT)'), H.PosName) ");
                SQL.AppendLine("Else GROUP_CONCAT(IF(E.ActingOfficialInd = 'Y', CONCAT(E.PosName,' (PLT)'), E.PosName)SEPARATOR '/') End AS Position, ");
            }
                SQL.AppendLine("GROUP_CONCAT(IF(IFNULL(H.ActingOfficialInd, E.ActingOfficialInd) = 'Y', CONCAT(ifnull(H.PosName, E.PosName), ' (PLT)'), IFNULL(H.PosName, E.PosName))Separator '/') AS Position, ");
            SQL.AppendLine("Case When A.LastUpDt Is Not Null Then ");
            SQL.AppendLine("Concat(Substring(A.LastUpDt, 7, 2), '/', Substring(A.LastUpDt, 5, 2), '/', Left(A.LastUpDt, 4)) ");
            SQL.AppendLine("Else Null End As LastUpDt, A.Remark, B.DNo ");
            SQL.AppendLine("FROM tbldocapproval A ");
            SQL.AppendLine("Left JOIN tbldocapprovalsetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo ");
            SQL.AppendLine("Left JOIN tbluser C ON B.UserCode like CONCAT('%', C.UserCode, '%') ");
            SQL.AppendLine("Left JOIN tblemployee D ON C.UserCode = D.UserCode ");
            SQL.AppendLine("Left JOIN tblposition E ON D.PosCode = E.PosCode ");
            SQL.AppendLine("Left JOIN tbluser F ON A.UserCode = F.UserCode ");
            SQL.AppendLine("Left JOIN tblemployee G ON F.UserCode = G.UserCode ");
            SQL.AppendLine("Left JOIN tblposition H ON G.PosCode = H.PosCode ");
            if (mIsMRSPPJB)
                SQL.AppendLine("Where A.DocType = 'MaterialRequestSPPJB' ");
            else
                SQL.AppendLine("Where A.DocType='MaterialRequest' ");
            if(!mIsMaterialRequestUseDetailApprovalInformation)
                SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And A.DocNo=@DocNo And A.DNo=@DNo ");
            SQL.AppendLine("GROUP BY B.DNo ");
            SQL.AppendLine("ORDER BY A.ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, new string[] { "UserName", "Position", "LastUpDt", "Remark", "StatusDesc", "DNo" });
                    
                    if(mIsMaterialRequestUseDetailApprovalInformation)
                    {
                        while (dr.Read())
                        {
                            Index++;
                            Msg +=
                                "No : " + Sm.DrStr(dr, c[5]) + Environment.NewLine +
                                "User : " + Sm.DrStr(dr, c[0]) + Environment.NewLine +
                                "Jabatan : " + Sm.DrStr(dr, c[1]) + Environment.NewLine +
                                "Status : " + Sm.DrStr(dr, c[4]) + Environment.NewLine +
                                "Date : " + Sm.DrStr(dr, c[2]) + Environment.NewLine +
                                "Remark : " + Sm.DrStr(dr, c[3]) + Environment.NewLine + Environment.NewLine;
                        }
                    }
                    else
                    {
                        while (dr.Read())
                        {
                            Index++;
                            Msg +=
                                "No : " + Index.ToString() + Environment.NewLine +
                                "User : " + Sm.DrStr(dr, c[0]) + Environment.NewLine +
                                "Status : " + Sm.DrStr(dr, c[4]) + Environment.NewLine +
                                "Date : " + Sm.DrStr(dr, c[2]) + Environment.NewLine +
                                "Remark : " + Sm.DrStr(dr, c[3]) + Environment.NewLine + Environment.NewLine;
                        }
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile2(string DocNo)
        {
            if (IsUploadFileNotValid2()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile3(string DocNo)
        {
            if (IsUploadFileNotValid3()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile3(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsUploadFileNotValid2()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsUploadFileNotValid3()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid3() ||
                IsFileSizeNotvalid3() ||
                IsFileNameAlreadyExisted3();
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFTPClientDataNotValid2()
        {

            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid3()
        {

            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }


        private bool IsFileSizeNotvalid()
        {
            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid3()
        {
            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2()
        {
            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName2=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted3()
        {
            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName3=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
                this.Text = Sm.GetValue("Select Menudesc From TblMenu Where Menucode = @Param ", mMenuCode);

            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
                this.Text = Sm.GetValue("Select Menudesc From TblMenu Where Menucode = @Param ", mMenuCode);

            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
                this.Text = Sm.GetValue("Select Menudesc From TblMenu Where Menucode = @Param ", mMenuCode);

            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        internal void SetDroppingReqProjImplItQty()
        {
            if (TxtDR_PRJIDocNo.Text.Length == 0) return;

            var SQL = new StringBuilder();
            string ItCode = string.Empty;
            decimal Qty = 0m, Amt = 0m;

            SQL.AppendLine("Select T2.ResourceItCode, T1.Qty, IfNull(T3.Amt, 0.00) As Amt ");
            SQL.AppendLine("From TblDroppingRequestDtl T1 ");
            SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr T2 On T1.PRBPDocNo=T2.DocNo ");
            SQL.AppendLine("Left Join TblProjectImplementationDtl2 T3 On T2.PRJIDocNo=T3.DocNo And T2.ResourceItCode=T3.ResourceItCode ");
            SQL.AppendLine("Where T1.DocNo=@DroppingRequestDocNo And T1.MRDocNo Is Null ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ResourceItCode", "Qty", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        Qty = Sm.DrDec(dr, 1);
                        Amt = Sm.DrDec(dr, 2);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), ItCode) && Sm.GetGrdDec(Grd1, r, 31) == 0m)
                            {
                                Grd1.Cells[r, 17].Value = Qty;
                                Grd1.Cells[r, 31].Value = Qty;
                                Grd1.Cells[r, 32].Value = Amt;
                                Grd1.Cells[r, 24].Value = Sm.GetGrdDec(Grd1, r, 17) * Sm.GetGrdDec(Grd1, r, 23);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            ComputeDR_Balance();
        }

        internal void SetDroppingReqBCItQty()
        {
            if (mDroppingRequestBCCode.Length == 0) return;

            var SQL = new StringBuilder();
            string ItCode = string.Empty;
            decimal Qty = 0m, Amt = 0m;

            SQL.AppendLine("Select ItCode, Qty, Amt ");
            SQL.AppendLine("From TblDroppingRequestDtl2 ");
            SQL.AppendLine("Where DocNo=@DroppingRequestDocNo ");
            SQL.AppendLine("And BCCode=@DroppingRequestBCCode ");
            SQL.AppendLine("And MRDocNo Is Null ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mDroppingRequestBCCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "Qty", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        Qty = Sm.DrDec(dr, 1);
                        Amt = Sm.DrDec(dr, 2);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), ItCode) &&
                                Sm.GetGrdDec(Grd1, r, 31) == 0m
                                )
                            {
                                Grd1.Cells[r, 17].Value = Qty;
                                Grd1.Cells[r, 31].Value = Qty;
                                Grd1.Cells[r, 32].Value = Amt;
                                Grd1.Cells[r, 24].Value = Sm.GetGrdDec(Grd1, r, 17) * Sm.GetGrdDec(Grd1, r, 23);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            ComputeDR_Balance();
        }

        internal void CopyData(string DocNo)
        {
            try
            {
                ClearData();
                ShowMaterialRequestHdr(DocNo, "Y");
                if (LuePICCode.Text == "[Empty]") SetLuePICCode(ref LuePICCode);
                TxtDocNo.EditValue = null;
                LueProcessInd.EditValue = "D";
                Sm.SetDteCurrentDate(DteDocDt);
                ShowDroppingRequestInfo();
                ShowMaterialRequestDtl(DocNo);
                for (int r=0; r < Grd1.Rows.Count - 1; r++)
                {
                    Sm.SetGrdStringValueEmpty(Grd1, r, new int[] { 3, 5, 6 });
                    Sm.SetGrdBoolValueFalse(ref Grd1, r, new int[] { 1, 2 });
                }
                if (mIsMRSPPJB)
                {
                    if (mBOQDlgForMRFormat == "2")
                    {
                        ShowMaterialRequestDtl3(DocNo);
                    }
                    else
                    {
                        ShowMaterialRequestDtl2(DocNo);
                    }
                }
                ComputeRemainingBudget();
                ComputeDR_Balance();
                ComputeTotalEstPrice();
                SetSeqNo();
                CopyDataDocNo = DocNo;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueReqType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueReqType, new Sm.RefreshLue2(SetLueReqType), string.Empty);
                if (mIsMaterialRequestForDroppingRequestEnabled)
                {
                    mDroppingRequestBCCode = string.Empty;
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        TxtDroppingRequestDocNo, TxtDR_DeptCode, TxtDR_Yr, TxtDR_Mth, TxtDR_PRJIDocNo, 
                        TxtDR_BCCode, MeeDR_Remark
                    });
                    Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                    { 
                        TxtDR_DroppingRequestAmt, TxtDR_MRAmt, TxtDR_Balance 
                    }, 0);
                }
                else
                    SetBudgetCategory();
                if (!LueReqType.Properties.ReadOnly) ClearGrd();
                ComputeTotalEstPrice();
                ComputeRemainingBudget();
                ComputeDR_Balance();
            }
        }

        private void DteUsageDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteUsageDt, ref fCell, ref fAccept);
        }

        private void DteUsageDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteDocDt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsMaterialRequestForDroppingRequestEnabled)
                {
                    mDroppingRequestBCCode = string.Empty;
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        TxtDroppingRequestDocNo, TxtDR_DeptCode, TxtDR_Yr, TxtDR_Mth, TxtDR_PRJIDocNo, 
                        TxtDR_BCCode, MeeDR_Remark
                    });
                    Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                    { 
                        TxtDR_DroppingRequestAmt, TxtDR_MRAmt, TxtDR_Balance 
                    }, 0);
                    ClearGrd();
                }
                ComputeTotalEstPrice();
                ComputeRemainingBudget();
                ComputeDR_Balance();
            }
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ComputeRemainingBudget();
                ComputeDR_Balance();
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite ? "Y" : "N");
                if (mIsDeptFilterBySite && mIsInsert && TxtDocNo.Text.Length == 0)
                {
                    if (Sm.GetLue(LueSiteCode).Length > 0)
                    {
                        Sm.SetControlReadOnly(LueDeptCode, false);
                        LueDeptCode.EditValue = "<Refresh>";
                        Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
                        Sm.SetControlReadOnly(LueDeptCode2, false);
                        LueDeptCode2.EditValue = "<Refresh>";
                        Sm.RefreshLookUpEdit(LueDeptCode2, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
                    }
                    else
                    {
                        LueDeptCode.EditValue = null;
                        Sm.SetControlReadOnly(LueDeptCode, true);
                        LueDeptCode2.EditValue = null;
                        Sm.SetControlReadOnly(LueDeptCode2, true);
                    }
                }
                if (mIsMRSPPJB && mIsFilterBySite)
                {
                    if(mItemManagedByWhsInd) ClearGrd();
                    mlJob.Clear();
                    ComputeTotalEstPrice();
                    ComputeRemainingBudget();
                    ComputeDR_Balance();
                }
            }
        }

        private void LueDivCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDivCode, new Sm.RefreshLue1(SetLueDivisionCode));
        }

        private void LueSiteCode_Validated(object sender, EventArgs e)
        {
            
        }

        private void LueDivCode_Validated(object sender, EventArgs e)
        {
            
        }
        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
                if (Sm.GetLue(LueDeptCode).Length > 0)
                {
                    Sm.SetControlReadOnly(LueDivCode, false);
                    LueDivCode.EditValue = "<Refresh>";
                    SetLueDivisionCode(ref LueDivCode);
                }
                else
                {
                    Sm.SetControlReadOnly(LueDivCode, true);
                    LueDivCode.EditValue = "<Refresh>";
                    SetLueDivisionCode(ref LueDivCode);
                }
            }
        }

        private void LueDeptCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode2, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
        }

        private void LueDeptCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsDeptFilterBySite)
                    Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
                else
                    Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
                if (!mIsMaterialRequestForDroppingRequestEnabled && !LueDeptCode.Properties.ReadOnly) SetBudgetCategory();
                mDroppingRequestBCCode = string.Empty;
                if (mIsMaterialRequestForDroppingRequestEnabled)
                {
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        TxtDroppingRequestDocNo, TxtDR_DeptCode, TxtDR_Yr, TxtDR_Mth, TxtDR_PRJIDocNo, 
                        TxtDR_BCCode, MeeDR_Remark
                    });
                    Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                    { 
                        TxtDR_DroppingRequestAmt, TxtDR_MRAmt, TxtDR_Balance 
                    }, 0);
                }
                if (!LueDeptCode.Properties.ReadOnly)
                {
                    if(mItemManagedByWhsInd) ClearGrd();
                    ComputeTotalEstPrice();
                    ComputeRemainingBudget();
                    ComputeDR_Balance();
                }
            }
        }

        private void LueDeptCode2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode2, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
                if (!mIsMaterialRequestForDroppingRequestEnabled && !LueDeptCode2.Properties.ReadOnly) SetBudgetCategory();
                mDroppingRequestBCCode = string.Empty;
                if (mIsMaterialRequestForDroppingRequestEnabled)
                {
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
                    {
                        TxtDroppingRequestDocNo, TxtDR_DeptCode, TxtDR_Yr, TxtDR_Mth, TxtDR_PRJIDocNo,
                        TxtDR_BCCode, MeeDR_Remark
                    });
                    Sm.SetControlNumValueZero(new List<DXE.TextEdit>
                    {
                        TxtDR_DroppingRequestAmt, TxtDR_MRAmt, TxtDR_Balance
                    }, 0);
                }
                if (!LueDeptCode2.Properties.ReadOnly)
                {
                    if(mItemManagedByWhsInd) ClearGrd();
                    ComputeTotalEstPrice();
                    ComputeRemainingBudget();
                    ComputeDR_Balance();
                }
            }
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue3(Sl.SetLueBCCode), string.Empty, Sm.GetLue(LueDeptCode));
            ComputeRemainingBudget();
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        public static void SetLueCurCode(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue, "Select CurCode As Col1 From tblCurrency ", "Currency");
        }

        //public static void SetLuePICCode(ref LookUpEdit Lue)
        //{
        //    Sm.SetLue2(ref Lue, "Select UserCode As Col1, UserName As Col2 From TblUser Order By UserName", 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //}

        private void SetLuePICCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select UserCode As Col1, UserName As Col2 ");
                SQL.AppendLine("From TblUser ");
                if (mIsInsert && mIsPICGrouped)
                    SQL.AppendLine("where UserCode=@UserCode ");
                SQL.AppendLine("Order By UserName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode_Leave(object sender, EventArgs e)
        {
            if (LueCurCode.Visible && fAccept && fCell.ColIndex == 28)
            {
                if (Sm.GetLue(LueCurCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 28].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 28].Value = Sm.GetLue(LueCurCode);
                }
                LueCurCode.Visible = false;
            }
        }

        private void LueCurCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }
        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }
        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }
        private void LuePICCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LuePICCode, new Sm.RefreshLue1(SetLuePICCode));
        }

        private void LueDurationUom_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDurationUom, new Sm.RefreshLue2(Sl.SetLueOption), "DurationUom");
        }

        private void LueDurationUom_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }
        private void LueDurationUom_Leave(object sender, EventArgs e)
        {
            if (LueDurationUom.Visible && fAccept && fCell.ColIndex == 37)
            {
                if (Sm.GetLue(LueDurationUom).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 36].Value =
                    Grd1.Cells[fCell.RowIndex, 37].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 36].Value = Sm.GetLue(LueDurationUom);
                    Grd1.Cells[fCell.RowIndex, 37].Value = LueDurationUom.GetColumnValue("Col2");
                }
                LueDurationUom.Visible = false;
            }
        }

        private void LueProcessInd_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProcessInd, new Sm.RefreshLue1(SetLueProcessInd));
                ComputeRemainingBudget();
            }
        }

        #endregion

        #region Button  Event

        private void BtnPOQtyCancelDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMaterialRequest5Dlg2(this));
        }

        private void BtnPOQtyCancelDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPOQtyCancelDocNo, "Cancellation PO#", false))
            {
                var f = new FrmPOQtyCancel(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPOQtyCancelDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnDORequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueReqType, "Request Type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg3(this, Sm.GetLue(LueDeptCode)));
        }

        private void BtnDORequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDORequestDocNo, "DO Request#", false))
            {
                var f = new FrmDORequestDept(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDORequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnDroppingRequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDroppingRequestDocNo, "Dropping request#", false))
            {
                var f = new FrmDroppingRequest(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDroppingRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnDroppingRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg4(this, Sm.GetDte(DteDocDt), Sm.GetLue(LueDeptCode)));
        }

        private void BtnBCCode_Click(object sender, EventArgs e)
        {
            string mYr = Sm.Left(Sm.GetDte(DteDocDt), 4), mMth = Sm.GetDte(DteDocDt).Substring(4, 2);
            
            if (!Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode2, "Department for Budget") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg9(this, mYr, mMth, Sm.GetLue(LueDeptCode), Sm.GetLue(LueDeptCode2)));
        }

        private void BtnCopyGeneral_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMaterialRequest5Dlg10(this));
        }

        #endregion

        #endregion

        #region Class

        internal class Job
        {
            public string ItCode { get; set; }
            public string JobCode { get; set; }
            public string JobName { get; set; }
            public string JobCtName { get; set; }
            public string UomCode { get; set; }
            public string PrevCurCode { get; set; }
            public decimal PrevUPrice { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice { get; set; }
            public decimal Qty { get; set; }
            public string Remark { get; set; }
            public decimal Total { get; set; }
            public int No { get; set; }


        }

        internal class Job2
        {
            public string DNo { get; set; }
            public string GrpType { get; set; }
            public string GrpName { get; set; }
            public string ItCode { get; set; }
            public string LocName { get; set; }
            public string JobCtCode { get; set; }
            public string JobCtName { get; set; }
            public string JobName { get; set; }
            public decimal Qty { get; set; }
            public string UomCode { get; set; }
            public string CurCode { get; set; }
            public decimal Price { get; set; }
            public string Remark { get; set; }
            public decimal Total { get; set; }
            public int No { get; set; }


        }

        #region Report Class

        private class MatReq
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string SiteName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string OptDesc { get; set; }
            public string HRemark { get; set; }
            public string CreateBy { get; set; }
            public string PrintBy { get; set; }
            public string SiteInd { get; set; }
            public string LocalDocNo { get; set; }
            public string BCName { get; set; }
            public string Division { get; set; }
            public decimal BudgetAmt { get; set; }
            public decimal AfterUsed { get; set; }
            public decimal EstPrice { get; set; }
            public decimal AvailableBudget { get; set; }
            public string ProcessInd { get; set; }
            public string LastUpDt { get; set; }
            public string LastUpTime { get; set; }
            public string FooterImage { get; set; }
            public decimal TotalBudget { get; set; }
        }

        class MatReq1
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
            public string PosName { get; set; }
            public string LastUpDt { get; set; }
        }

        private class MatReq2
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
            public string PosName { get; set; }
            public string LastUpDt { get; set; }
        }

        private class MatReqDtl
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUomCode { get; set; }
            public string UsageDt { get; set; }
            public decimal Mth01 { get; set; }
            public decimal Mth03 { get; set; }
            public decimal Mth06 { get; set; }
            public decimal Mth09 { get; set; }
            public decimal Mth12 { get; set; }
            public string DRemark { get; set; }
            public string ForeignName { get; set; }
            public decimal EstPrice { get; set; }
            public string Curcode { get; set; }
            public string Duration { get; set; }
            public string DurationUom { get; set; }
            public string Specification { get; set; }
            public decimal TotalPrice { get; set; }
            public string Status { get; set; }
            public string Location { get; set; }
            public string LastUpDt { get; set; }
            public string LastUpTime { get; set; }
        }

        private class MatReqDtl2
        {
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
            public string PosName { get; set; }

        }

        private class MatReqDtl3
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            public string IsPrintOutUseDigitalSignature { get; set; }
        }

        private class MatReqDtl4
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            public string IsPrintOutUseDigitalSignature { get; set; }
        }

        private class MatReqSier
        {
            public string BudgetCategory { get; set; }
            public decimal Budget { get; set; }
            public decimal AfterUsageAmt { get; set; }
            public decimal UsageAmt { get; set; }
            public decimal BalanceAmt { get; set; }
            public decimal UsageAmt2 { get; set; }
            public decimal AvailableBudget { get; set; }
            public decimal Amt { get; set; }
            public string CurCode { get; set; }

        }

        private class MatReqSPPJB
        {
            public int No { get; set; }
            public string JobName { get; set; }
            public string JobCtName { get; set; }
            public decimal UPrice { get; set; }
            public decimal Qty { get; set; }
            public string ItName { get; set; }
            public decimal Total { get; set; }
        }

        private class SignatureSIER
        {
            public string EmpName { get; set; }
            public string EmpPict { get; set; }
            public string Position { get; set; }
            public string Date { get; set; }
            public string Sequence { get; set; }
            public string Title { get; set; }
            public string Space { get; set; }
            public string LabelName { get; set; }
            public string LabelPos { get; set; }
            public string LabelDt { get; set; }
        }

        private class MRIMS
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string SiteName { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string DocName { get; set; }
            public string CompanyLogo { get; set; }
        }

        private class MRIMSDtl
        {
            public int No { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItName { get; set; }
            public string Specification { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }
            public string ProjectCode { get; set; }
            public string UsageDt { get; set; }
            public string Remark { get; set; }
        }

        private class MRIMSSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class MRIMSSign2
        {
            public string CreateUserName { get; set; }
            public string CreateDocDt { get; set; }
            public string ApproveUserName { get; set; }
            public string ApproveDocDt { get; set; }
            public string Approve2UserName { get; set; }
            public string Approve2DocDt { get; set; }
            public string Approve3UserName { get; set; }
            public string Approve3DocDt { get; set; }
        }

        private class MRSPPJBSignSIER
        {
            public string EmpName { get; set; }
            public string EmpPict { get; set; }
            public string Position { get; set; }
            public string Date { get; set; }
            public string Sequence { get; set; }
            public string Title { get; set; }
            public string Space { get; set; }
            public string LabelName { get; set; }
            public string LabelPos { get; set; }
            public string LabelDt { get; set; }
        }
        private class MRSPPJBBOQHdr
        {
            public decimal Total { get; set; }
            public string Terbilang { get; set; }
        }
        private class MRSPPJBBOQDtl
        {
            public int No { get; set; }
            public string GrpType { get; set; }
            public string GrpName { get; set; }
            public string JobName { get; set; }
            public decimal Qty { get; set; }
            public string UoMCode { get; set; }
            public string CurCode { get; set; }
            public decimal Price { get; set; }
            public decimal Total1 { get; set; }
            public string SubTotal { get; set; }
            public decimal Total2 { get; set; }
            public string Remark { get; set; }
            public string Terbilang { get; set; }
        }
        #endregion

        #endregion
    }
}
