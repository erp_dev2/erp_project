﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRecvWhs3Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmRecvWhs3 mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsInventoryShowTotalQty = false;

        #endregion

        #region Constructor

        public FrmRecvWhs3Find(FrmRecvWhs3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                mIsInventoryShowTotalQty = (Sm.GetParameter("IsInventoryShowTotalQty") == "Y");
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueWhsCode(ref LueWhsCode2);
                Sl.SetLueItCtCode(ref LueItCtCode);
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.WhsName, E.WhsName As WhsName2, B.CancelInd, ");
            SQL.AppendLine("B.ItCode, D.ItName, D.ItCodeInternal, D.ItCtCode, B.PropCode, F.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("D.InventoryUomCode, D.InventoryUomCode2, D.InventoryUomCode3,  ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, D.ItGrpCode ");
            SQL.AppendLine("From TblRecvWhs3Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select WhsCode From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=C.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblWarehouse E On A.WhsCode2=E.WhsCode ");
            SQL.AppendLine("Left Join TblProperty F On B.PropCode=F.PropCode ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel", 
                        "From",
                        "To",
                        
                        //6-10
                        "Item's Code",
                        "",
                        "Item's"+Environment.NewLine+"Local Code",
                        "Item's Name",
                        "Property",
                        
                        //11-15
                        "Batch#",
                        "Source",
                        "Lot",
                        "Bin",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "Quantity 2",
                        "Uom 2",
                        "Quantity 3",
                        "Uom 3",
                        
                        //21-25
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 

                        //26-27
                        "Last"+Environment.NewLine+"Updated Time",
                        "Group"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 50, 150, 150, 
                        
                        //6-10
                        80, 20, 80, 250, 100,
                        
                        //11-15
                        200, 170, 60, 80, 80,  

                        //16-20
                        80, 80, 80, 80, 80, 

                        //21-25
                        100, 80, 80, 100, 80, 
                        
                        //26-27
                        80, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 7 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 15, 17, 19 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 22, 25 });
            Sm.GrdFormatTime(Grd1, new int[] { 23, 26 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 12, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 }, false);
            ShowInventoryUomCode();
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[27].Visible = true;
                Grd1.Cols[27].Move(9);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 12, 21, 22, 23, 24, 25, 26 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " Where B.CancelInd='N' ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode2), "A.WhsCode2", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "D.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "D.ItCodeInternal", "D.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "B.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "B.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "B.Bin", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "WhsName2", "WhsName", "ItCode", 
                            
                            //6-10
                            "ItCodeInternal", "ItName", "PropName", "BatchNo", "Source",   
                            
                            //11-15
                            "Lot", "Bin", "Qty", "InventoryUomCode", "Qty2",   
                            
                            //16-20
                            "InventoryUomCode2", "Qty3", "InventoryUomCode3", "CreateBy", "CreateDt",  
                            
                            //21-23
                            "LastUpBy", "LastUpDt", "ItGrpCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 26, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 23);
                        }, true, mIsInventoryShowTotalQty, false, false
                    );
                if (mIsInventoryShowTotalQty)
                {
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 15, 17, 19 });
                    Grd1.Rows.ExpandAll();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                if (Grd1.Rows.Count <= 1)
                    Sm.FocusGrd(Grd1, 0, 0);
                else
                    Sm.FocusGrd(Grd1, 1, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Event

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "To");
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "From");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Category");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        #endregion

        #endregion

    }
}
