﻿#region Update
/*
    10/06/2020 [VIN/IMS] new apps
    30/06/2020 [VIN/IMS] memunculkan journal# 
    12/08/2020 [VIN/IMS] bug - lup insert OT verification tidak bisa insert
    03/11/2020 [TKG/IMS] tambah payroll group
    17/01/2021 [TKG/PHT] ubah GenerateDocNo*, GenerateVoucherRequestDocNo
    11/02/2021 [WED/IMS] save ke banyak voucher request sesuai cost center nya
    28/07/2021 [TKG/IMS] journal vr payroll ims ditambah validasi ActInd ketika join dengan TblCostCenter
    23/09/2021 [VIN/IMS] VR AcType hardcode Credit
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestSpecial : RunSystem.FrmBase3
    {
        #region Field, Property
        internal FrmVoucherRequestSpecialFind FrmFind;
        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty //if this application is called from other application
            ;

        internal bool
            mIsVRSOT = false,
            mIsVRSRHA = false,
            mIsVRSIncentive = false;

        #endregion

        #region Constructor

        public FrmVoucherRequestSpecial(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Voucher Request Special";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                GetParameter();
                
                SetGrd();
                SetFormControl(mState.View);
                //if this application is called from other application
                
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        
                override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion 

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Document#",
                    "",
                    "Notes",
                    "Amount",
                    "Remark", 

                    //6-7
                    "DNo",
                    "Payroll's Group"
                },
                new int[] { 20, 150, 20, 200, 150, 200, 100, 200 }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0, 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 3, 4, 5, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 6 }, false);
            Grd1.Cols[6].Move(1);

        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtStatus, MeeRemark,
                        TxtAmt
                    }, true);
                    Grd1.ReadOnly = true;
                    BtnVoucherRequest.Enabled = BtnVoucherDocNo.Enabled = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark
                    }, false);
                    ChkCancelInd.Enabled = false;
                    BtnVoucherRequest.Enabled = BtnVoucherDocNo.Enabled = false;
                    DteDocDt.Focus();
                    Grd1.ReadOnly = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason,  TxtStatus, MeeRemark   
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion 

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVoucherRequestSpecialFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            string LocalDocNo = string.Empty;
            //decimal TotalAmt = decimal.Parse(TxtAmt.Text);

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = string.Empty;
            //string VoucherRequestDocNo = string.Empty;

            if(mIsVRSRHA)
                DocNo = GenerateDocNo(Sm.GetDte(DteDocDt), "VRRHA", "TblVoucherRequestSpecialHdr");
            else if (mIsVRSOT)
                DocNo = GenerateDocNo(Sm.GetDte(DteDocDt), "VROT", "TblVoucherRequestSpecialHdr");
            else
                DocNo = GenerateDocNo(Sm.GetDte(DteDocDt), "VRINC", "TblVoucherRequestSpecialHdr");

            //VoucherRequestDocNo = GenerateVoucherRequestDocNo("1");

            var cml = new List<MySqlCommand>();
            var l = new List<VoucherRequestHdr>();
            var l2 = new List<VoucherRequestDtl>();
            var l3 = new List<DtlCostCenter>();
            var l4 = new List<DtlDept>();

            cml.Add(SaveVoucherRequestSpecialHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SaveVoucherRequestSpecialDtl(DocNo, Row));
                }
            }

            PrepDetailData(ref l4);
            if (l4.Count > 0)
            {
                SumByCostCenter(ref l4, ref l3);
                GetVoucherRequestHdr(ref l3, ref l);
                GetVoucherRequestDtl(ref l4, ref l2);
                GetDocNoVoucherRequestDtl(ref l, ref l2);

                foreach (var x in l) cml.Add(SaveVoucherRequestHdr(x, DocNo));
                foreach (var x in l2) cml.Add(SaveVoucherRequestDtl(x));
            }

            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
            //        cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo, Row));

            Sm.ExecCommands(cml);
            ShowData(DocNo);

            l.Clear(); l2.Clear(); l3.Clear(); l4.Clear();
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 document#.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var DocDt = Sm.GetDte(DteDocDt);
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Document# is empty.")
                    ) return true;
            }
            return false;
        }


        private MySqlCommand SaveVoucherRequestSpecialHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            string mDocType = string.Empty;
            string mVoucherDocType = string.Empty;

            if (mIsVRSRHA) mDocType = "VRRHA";
            if (mIsVRSIncentive) mDocType = "VRINC";
            if (mIsVRSOT) mDocType = "VROT";

            if (mIsVRSRHA) mVoucherDocType = "66";
            if (mIsVRSIncentive) mVoucherDocType = "67";
            if (mIsVRSOT) mVoucherDocType = "65";

            SQL.AppendLine("Insert Into TblVoucherRequestSpecialHdr(DocNo, DocDt, DocType, Status, CancelInd, ");
            SQL.AppendLine("Amt, Remark, CreateBy, CreateDt)  ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @DocType, 'O', 'N', @Amt, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            //SQL.AppendLine("INSERT INTO tblvoucherrequesthdr  ");
            //SQL.AppendLine("(DocNo, DocDt, CancelInd, STATUS, DocType, CurCode, Amt, AcType, BankAcCode, Remark, DeptCode, CreateBy, CreateDt) ");
            //SQL.AppendLine("Values(@VoucherRequestDocNo, @DocDt, 'N', 'O', @VoucherDocType, 'IDR', @Amt, 'D',  ");
            //SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='BankAcCodeForVRSpecial'), @Remark, (Select ParValue From TblParameter Where ParCode='VoucherRequestSpecialDeptCode'), @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType=@DocType ");

            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblVoucherRequestSpecialHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("); ");

            //SQL.AppendLine("Update tblvoucherrequesthdr ");
            //SQL.AppendLine("Set Status = 'A' ");
            //SQL.AppendLine("Where DocNo = @VoucherRequestDocNo ");
            //SQL.AppendLine("And Not Exists ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select 1 ");
            //SQL.AppendLine("    From TblDocApproval ");
            //SQL.AppendLine("    Where DocNo = @DocNo ");
            //SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@VoucherDocType", mVoucherDocType);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            //Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestSpecialDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestSpecialDtl(DocNo, DNo, DocNo2, Amt, CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @DNo, @DocNo2, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DocNo2", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestHdr(VoucherRequestHdr x, string DocNo)
        {
            var SQL = new StringBuilder();
            string mDocType = string.Empty;
            string mVoucherDocType = string.Empty;

            if (mIsVRSRHA) mDocType = "VRRHA";
            if (mIsVRSIncentive) mDocType = "VRINC";
            if (mIsVRSOT) mDocType = "VROT";

            if (mIsVRSRHA) mVoucherDocType = "66";
            if (mIsVRSIncentive) mVoucherDocType = "67";
            if (mIsVRSOT) mVoucherDocType = "65";

            SQL.AppendLine("INSERT INTO tblvoucherrequesthdr  ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, STATUS, DocType, PIC, CurCode, Amt, AcType, BankAcCode, Remark, DeptCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@VoucherRequestDocNo, @DocDt, 'N', 'O', @VoucherDocType, @CreateBy, 'IDR', @Amt, 'C',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='BankAcCodeForVRSpecial'), @Remark, @DeptCode, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update tblvoucherrequesthdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @VoucherRequestDocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Insert Into TblVoucherRequestSpecialDtl2(DocNo, VoucherRequestDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @VoucherRequestDocNo, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@VoucherDocType", mVoucherDocType);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", x.DocNo);
            Sm.CmParam<Decimal>(ref cm, "@Amt", x.Amt);
            Sm.CmParam<String>(ref cm, "@DeptCode", x.DeptCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(VoucherRequestDtl x)
        {
            var SQL = new StringBuilder();

            SQL.Append("Insert Into TblVoucherRequestDtl ");
            SQL.Append("(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.Append("Values (@DocNo, @DNo, @Description, @Amt, ");
            SQL.Append("@Remark, @CreateBy, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", x.DNo);
            Sm.CmParam<String>(ref cm, "@Description", string.Concat(x.DocNo2, "-", x.Notes));
            Sm.CmParam<Decimal>(ref cm, "@Amt", x.Amt);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveVoucherRequestDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.Append("Insert Into TblVoucherRequestDtl ");
        //    SQL.Append("(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
        //    SQL.Append("Values (@DocNo, @DNo, @Description, @Amt, ");
        //    SQL.Append("@Remark, @CreateBy, CurrentDateTime());");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@Description", String.Concat(Sm.GetGrdStr(Grd1, Row, 1),'-', Sm.GetGrdStr(Grd1, Row, 3)));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditVoucherRequestSpecialHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Voucher Request Special#", false) ||
                IsDataProcessedAlready()||
                IsDocumentAlreadyCancel();
        }

        private bool IsDocumentAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select 1 From TblVoucherRequestSpecialHdr Where DocNo=@DocNo And (CancelInd='Y' Or Status = 'C'); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblVoucherRequestSpecialDtl2 A ");
            SQL.AppendLine("Inner Join TblVoucherHdr B On A.VoucherRequestDocno = B.VoucherRequestDocNo ");
            SQL.AppendLine("Where A.DocNo = @Param ");
            SQL.AppendLine("And B.CancelInd = 'N' ");
            SQL.AppendLine("Limit 1; ");

            return Sm.IsDataExist(
                SQL.ToString(),
                TxtDocNo.Text,
                "Data already processed into voucher");
        }

        private MySqlCommand EditVoucherRequestSpecialHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestSpecialHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason = @CancelReason,");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            //SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            //SQL.AppendLine("    CancelInd=@CancelInd, CancelReason = @CancelReason,");
            //SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            //SQL.AppendLine("Where DocNo In (Select VoucherRequestDocNo From TblVoucherRequestSpecialHdr Where DocNo = @DocNo); ");

            SQL.AppendLine("Update TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestSpecialDtl2 B On A.DocNo = B.VoucherRequestDocNo ");
            SQL.AppendLine("    Set ");
            SQL.AppendLine("    A.CancelInd=@CancelInd, A.CancelReason = @CancelReason,");
            SQL.AppendLine("    A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where B.DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }
        #endregion 

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowVoucherRequestSpecialHdr(DocNo);
                ShowVoucherRequestSpecialDtl(DocNo);
               
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowVoucherRequestSpecialHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As Status, ");
            SQL.AppendLine("A.Amt, A.Remark ");
            SQL.AppendLine("FROM tblvoucherrequestspecialhdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "Status", "Amt", 
                    
                    //6
                    "Remark"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                     ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                     TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[6]);                     
                 }, true
             );
        }

        private void ShowVoucherRequestSpecialDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            if (mIsVRSRHA)
            {
                SQL.AppendLine("SELECT DISTINCT A.DNo, A.DocNo2, A.Amt, B.Remark, ");
                SQL.AppendLine("Concat(GROUP_CONCAT(Distinct E.DeptName SEPARATOR ', '),  (DATE_FORMAT(B.HolidayDt, ' %d %M %Y'))) Notes, F.PGName ");
                SQL.AppendLine("From tblvoucherrequestspecialdtl A ");
                SQL.AppendLine("INNER JOIN tblrhahdr B ON A.DocNo2=B.DocNo ");
                SQL.AppendLine("INNER Join TblRHADtl C ON B.DocNo=C.DocNo ");
                SQL.AppendLine("INNER Join TblEmployee D ON C.EmpCode=D.EmpCode ");
                SQL.AppendLine("INNER Join TblDepartment E ON D.DeptCode=E.DeptCode ");
                SQL.AppendLine("Left Join TblPayrollGrpHdr F ON B.PGCode=F.PGCode ");
            }
            if (mIsVRSIncentive)
            {
                SQL.AppendLine("SELECT DISTINCT A.DNo, A.DocNo2, A.Amt, B.Remark, ");
                SQL.AppendLine("Concat((DATE_FORMAT(B.DocDt, '%M %Y ')), GROUP_CONCAT(Distinct E.DeptName SEPARATOR ', ')) Notes, F.PGName ");
                SQL.AppendLine("From tblvoucherrequestspecialdtl A ");
                SQL.AppendLine("INNER JOIN tblempincentivehdr B ON A.DocNo2=B.DocNo ");
                SQL.AppendLine("INNER Join tblempincentivedtl C ON B.DocNo=C.DocNo ");
                SQL.AppendLine("INNER Join TblEmployee D ON C.EmpCode=D.EmpCode ");
                SQL.AppendLine("INNER Join TblDepartment E ON D.DeptCode=E.DeptCode ");
                SQL.AppendLine("Left Join TblPayrollGrpHdr F ON B.PGCode=F.PGCode ");
            }
            if (mIsVRSOT)
            {
                SQL.AppendLine("SELECT DISTINCT A.DNo, A.DocNo2, A.Amt, B.Remark, ");
                SQL.AppendLine("Concat( GROUP_CONCAT(Distinct E.DeptName SEPARATOR ', '), (DATE_FORMAT(C.OTDt, ' %d %M %Y'))) Notes, F.PGName "); 
                SQL.AppendLine("From tblvoucherrequestspecialdtl A "); 
                SQL.AppendLine("INNER JOIN tblotverificationhdr B ON A.DocNo2=B.DocNo ");
                SQL.AppendLine("INNER Join tblotverificationdtl C ON B.DocNo=C.DocNo ");
                SQL.AppendLine("INNER Join TblEmployee D ON C.EmpCode=D.EmpCode ");
                SQL.AppendLine("INNER Join TblDepartment E ON D.DeptCode=E.DeptCode ");
                SQL.AppendLine("Left Join TblPayrollGrpHdr F ON B.PGCode=F.PGCode ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo GROUP BY A.DNo Order By Dno");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo",
                    "DocNo2", "Amt", "Notes", "Remark", "PGName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region additional method

        #region generate voucher request

        private void PrepDetailData(ref List<DtlDept> l4)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string DocNo2 = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (DocNo2.Length > 0) DocNo2 += ",";
                    DocNo2 += Sm.GetGrdStr(Grd1, i, 1);
                }
            }

            GenerateSQL(ref SQL, mIsVRSRHA, mIsVRSIncentive, mIsVRSOT);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo2", DocNo2);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DeptCode", "CCCode", "Amt", "Notes", "Remark" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new DtlDept()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DeptCode = Sm.DrStr(dr, c[1]),
                            CCCode = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            Notes = Sm.DrStr(dr, c[4]),
                            Remark = Sm.DrStr(dr, c[5])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GenerateSQL(ref StringBuilder SQL, bool IsVRSRHA, bool IsVRSIncentive, bool IsVRSOT)
        {
            if (IsVRSRHA)
            {
                SQL.AppendLine("Select A.DocNo, D.DeptCode, E.CCCode, B.Amt, Concat(D.DeptName, ', ', Date_Format(A.HolidayDt, '%d %M %Y')) Notes, A.Remark ");
                SQL.AppendLine("From TblRHAHdr A ");
                SQL.AppendLine("Inner Join TblRHADtl B ON A.DocNo=B.DocNo ");
                SQL.AppendLine("    And Find_In_Set(A.DocNo, @DocNo2) ");
                SQL.AppendLine("Inner Join TblEmployee C ON B.EmpCode=C.EmpCode ");
                SQL.AppendLine("Inner Join TblDepartment D ON C.DeptCode=D.DeptCode ");
                SQL.AppendLine("Inner Join TblCostCenter E On D.DeptCode = E.DeptCode And E.DeptCode Is Not Null And E.ActInd='Y' ");
            }

            if (IsVRSIncentive)
            {
                SQL.AppendLine("Select A.DocNo, D.DeptCode, E.CCCode, B.Amount As Amt, Concat(Date_Format(A.DocDt, '%M %Y'), ', ', D.DeptName) Notes, A.Remark ");
                SQL.AppendLine("FROM tblempincentivehdr A ");
                SQL.AppendLine("Inner Join tblempincentivedtl B ON A.DocNo=B.DocNo ");
                SQL.AppendLine("    And Find_In_Set(A.DocNo, @DocNo2) ");
                SQL.AppendLine("Inner Join TblEmployee C ON B.EmpCode=C.EmpCode ");
                SQL.AppendLine("Inner Join TblDepartment D ON C.DeptCode=D.DeptCode ");
                SQL.AppendLine("Inner Join TblCostCenter E On D.DeptCode = E.DeptCode And E.DeptCode Is Not Null And E.ActInd='Y' ");
            }

            if (IsVRSOT)
            {
                SQL.AppendLine("Select A.DocNo, D.DeptCode, E.CCCode, B.Amt, Concat(D.DeptName, ', ', Date_Format(B.OTDt, '%d %M %Y')) Notes, A.Remark ");
                SQL.AppendLine("FROM tblotverificationhdr A  ");
                SQL.AppendLine("Inner Join Tblotverificationdtl B ON A.DocNo=B.DocNo ");
                SQL.AppendLine("    And Find_In_Set(A.DocNo, @DocNo2) ");
                SQL.AppendLine("Inner Join TblEmployee C ON B.EmpCode=C.EmpCode ");
                SQL.AppendLine("Inner Join TblDepartment D ON C.DeptCode=D.DeptCode ");
                SQL.AppendLine("Inner Join TblCostCenter E On D.DeptCode = E.DeptCode And E.DeptCode Is Not Null And E.ActInd='Y' ");
            }
        }

        private void SumByCostCenter(ref List<DtlDept> l4, ref List<DtlCostCenter> l3)
        {
            l3 = l4.GroupBy(g => g.CCCode)
                        .Select
                        (
                            s => new DtlCostCenter()
                            {
                                CCCode = s.Key,
                                DeptCode = s.First().DeptCode,
                                DocNo = string.Join(",", s.Select(ss => ss.DocNo).ToArray()),
                                Amt = s.Sum(p => p.Amt)
                            }
                        ).ToList();
        }

        private void GetVoucherRequestHdr(ref List<DtlCostCenter> l3, ref List<VoucherRequestHdr> l)
        {
            int index = 1;
            foreach (var x in l3)
            {
                l.Add(new VoucherRequestHdr()
                {
                    DocNo = GenerateVoucherRequestDocNo(index.ToString()),
                    DeptCode = x.DeptCode,
                    CCCode = x.CCCode,
                    DocNo2 = x.DocNo,
                    Amt = x.Amt
                });

                index += 1;
            }
        }

        private void GetVoucherRequestDtl(ref List<DtlDept> l4, ref List<VoucherRequestDtl> l2)
        {
            foreach(var x in l4)
            {
                l2.Add(new VoucherRequestDtl() 
                {
                    DocNo = string.Empty,
                    DocNo2 = x.DocNo,
                    DNo = string.Empty,
                    CCCode = x.CCCode,
                    DeptCode = x.DeptCode,
                    Amt = x.Amt,
                    Notes = x.Notes,
                    Remark = x.Remark
                });
            }
        }

        private void GetDocNoVoucherRequestDtl(ref List<VoucherRequestHdr> l, ref List<VoucherRequestDtl> l2)
        {
            int DNo = 1;

            foreach (var x in l)
            {
                DNo = 1;
                foreach (var y in l2
                    .OrderBy(o => o.CCCode)
                    .ThenBy(o => o.DeptCode)
                    .Where(w => w.DocNo.Length == 0 && 
                    w.CCCode == x.CCCode && 
                    w.DeptCode == x.DeptCode))
                {
                    if (x.DocNo2.Contains(y.DocNo2))
                    {
                        y.DocNo = x.DocNo;
                        y.DNo = Sm.Right(string.Concat("00000000", DNo.ToString()), 8);

                        DNo += 1;
                    }
                }
            }
        }

        #endregion

        private string GenerateDocNo(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From " + Tbl);
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       And DocType = '" + DocType + "' ");
            SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
            //SQL.Append("   ), '0001') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateVoucherRequestDocNo(string Val)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            //SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
            //SQL.Append("    (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+"+Val+", Char)), " + DocSeqNo + ") As Numb From ( ");
            //SQL.Append("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
            //SQL.Append("    From TblVoucherRequestHdr ");
            ////SQL.Append("(Select Right(Concat('0000', Convert(DocNo+" + Val + ", Char)), 4) As Numb From ( ");
            ////SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
            //SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
            //SQL.Append("Order By SUBSTRING(DocNo,7,"+DocSeqNo+") Desc Limit 1) As temp ");
            //SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '"+Val.ToString() +"'), " + DocSeqNo + ") ");
            //SQL.Append("), '000" + Val + "' ");
            //SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");

            SQL.Append("Select Concat( ");
            SQL.Append("'" + Yr + "', '/', '" + Mth + "', '/', ");
            SQL.Append("    ( ");
            SQL.Append("        Select IfNull( ");
            SQL.Append("            ( ");
            SQL.Append("                Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+" + Val + ", Char)), " + DocSeqNo + ") As Numb ");
            SQL.Append("                From ");
            SQL.Append("                ( ");
            SQL.Append("                    Select Convert(SUBSTRING(DocNo, 7, " + DocSeqNo + "), Decimal) As DocNoTemp ");
            SQL.Append("                    From TblVoucherRequestHdr ");
            SQL.Append("                    Where Left(DocNo, 5)= Concat('" + Yr + "', '/', '" + Mth + "')  ");
            SQL.Append("                    Order By SUBSTRING(DocNo, 7, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("                ) ");
            SQL.Append("                As Temp ");
            SQL.Append("            ) ");
            SQL.Append("        , Right(Concat(Repeat('0', " + DocSeqNo + "), '" + Val + "'), " + DocSeqNo + ")) ");
            SQL.Append("    ) ");
            SQL.Append(", '/', '" + DocAbbr + "' ");
            SQL.Append(") As DocNo; ");            

            return Sm.GetValue(SQL.ToString());
        }

        internal void ComputeTotalAmt()
        {
            decimal TotalAmt = 0m;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    TotalAmt += Sm.GetGrdDec(Grd1, r, 4);
                }
               
            }

            TxtAmt.EditValue = Sm.FormatNum(TotalAmt, 0);
        }

        private void GetParameter()
        {

            mIsVRSOT = Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForVRSOT"));
            mIsVRSRHA = Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForVRSRHA"));
            mIsVRSIncentive = Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForVRSIncentive"));

        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmVoucherRequestSpecialDlg(this));
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            ComputeTotalAmt();
        }

        internal void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmVoucherRequestSpecialDlg(this));
            if (mIsVRSRHA)
            {
                if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                {
                    var f = new FrmRHA16(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
            if (mIsVRSIncentive)
            {
                if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                {
                    var f = new FrmEmpIncentive2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
            if (mIsVRSOT)
            {
                if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                {
                    var f = new FrmOTVerification(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
        }

        #endregion 
      
        #endregion

        #region Event

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #region Button Click

        private void BtnVoucherRequest_Click(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length > 0) Sm.FormShowDialog(new FrmVoucherRequestSpecialDlg2(this, 1));
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length > 0) Sm.FormShowDialog(new FrmVoucherRequestSpecialDlg2(this, 2));
        }

        #endregion

        #endregion

        #region Class

        private class VoucherRequestHdr
        {
            public string DocNo { get; set; }
            public string DocNo2 { get; set; }
            public string DeptCode { get; set; }
            public string CCCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class VoucherRequestDtl
        {
            public string DocNo { get; set; }
            public string DocNo2 { get; set; }
            public string CCCode { get; set; }
            public string DeptCode { get; set; }
            public string DNo { get; set; }
            public string Notes { get; set; }
            public string Remark { get; set; }
            public decimal Amt { get; set; }
        }

        private class DtlDept
        {
            public string DocNo { get; set; }
            public string DeptCode { get; set; }
            public string CCCode { get; set; }
            public string Notes { get; set; }
            public string Remark { get; set; }
            public decimal Amt { get; set; }
        }

        private class DtlCostCenter
        {
            public string DocNo { get; set; }
            public string CCCode { get; set; }
            public string DeptCode { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion

    }
}
