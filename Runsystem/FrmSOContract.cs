﻿#region Update
/*
    25/06/2018 [WED] nggak boleh cancel kalau sudah di proses ke Project Implementation
    04/07/2018 [HAR] bug amount, Nilai amount nya ambil dari item BOQ yang tidak ada parent
    10/07/2018 [HAR] bug sum multiple parent
    18/07/2018 [HAR] bisa input amount di detail update ke header, otomatis kebikin COA berdasrkan parameter
    27/07/2018 [HAR] tambah field buat deskripsi project
    03/08/2018 [HAR] COA otomatis yang tadinya berasal dari parameter diganti berdasarkan option
    07/08/2018 [HAR] tambah kolom kode project untuk menampung kode project untuk keperluan COA voucher
    09/08/2018 [HAR] HO indicator waktu generate COA dihilangkan, Project Resources belum  masuk ke susunan COA
    21/08/2018 [WED] Cek ke ProjectImplementation yang sudah Approved
    24/09/2018 [HAR] tambah informasi project name dari BOQ
    18/05/2019 [TKG] memperpanjang DNo karena tidak cukup
    19/06/2019 [WED] COA dibuat agar tidak duplikat ketika SO Contract dibuat ulang
    20/06/2019 [TKG] panjang local document tambah menjadi 80 karakter
    04/07/2019 [DITA] menambahkan informasi project code untuk keperluan di project implementation
    16/07/2019 [DITA] tambah validasi, LOP yang lose dan cancelled tidak boleh di SO Contract
    24/07/2019 [DITA] COA otomatis disesuaikan dengan kode proyek (berdasarkan parameter baru)
    30/07/2019 [WED] tambah tab Joint Operation
    01/08/2019 [DITA] Info Bank Guarantee Indictor di BOQ di SO Contract
    07/08/2019 [WED] tambah save ke SO Contract Revision saat save pertama kali
    24/09/2019 [WED/IMS] BOQ yang SO Contract nya masih aktif maupun outstanding, tidak boleh di save
    25/09/2019 [WED/YK] SOContract yang sudah dibuat PRJI nya, tidak boleh di cancel
    05/10/2019 [WED/YK] tambah SOCDocNo di COA
    16/01/2020 [VIN/YK] Ganti pemisah project code dari - menjadi .
    08/05/2020 [DITA/YK] ubah amount ke amount2 dan sebaliknya, tambah amt2 -> contract amount
    05/06/2020 [WED/YK] tax diaktifkan. amt2 --> item; amt --> item + tax
    01/09/2020 [WED/VIR] rumus COA format baru, berdasarkan parameter SOContractCOAFormat
    27/12/2021 [IBL/YK] menambah kolom Portion dan NPWP pada tab Joint Operation
    03/01/2022 [DITA/VIR] project code2 urutan disesuaikan scope-resource-type
    03/06/2022 [TRI/VIR] BUG acno yang tergenerate belum ada CCCodenya (setelah sitecode)
    16/06/2022 [TYO/VIR] Validasi untuk KSO dan non KSO
    20/06/2022 [RIS/VIR] Menambah cost center pada project code
    20/06/2022 [RIS/VIR] COA mengambil project code dengan rumus terbaru
    15/07/2022 [RDA/PRODUCT] Pengisian Contact Person bisa dilakukan pada menu SO Contract tanpa harus mengisi Contact Person di BOQ
    18/07/2022 [IBL/PRODUCT] Joint Operation mandatory berdasarkan parameter IsSOContractJointOperationMandatory
    18/07/2022 [IBL/YK] Saat save data proses insert coa ketika memilih NON KSO coa yg terbentuk adl kepala 4.00, ketika memilih KSO Lead/KSO Member terbentuk adl kepala 4.10.
                        Ketika tidak memilih Joint Operation maka coa kepala 4.00 dan 4.10 akan terbentuk, berdasarkan parameter SOContractCOAFormat
    22/07/2022 [RDA/PRODUCT] Perubahan fungsi mandatory dari param IsCustomerContactPersonNonMandatory untuk lup Contact Person
    14/10/2022 [VIN/YK] Insert COA tambah sitecode untuk parent ProjectAcNoFormula = 2
    19/01/2022 [RDA/MNET] tambah param IsShippingNameValidatedContactPerson
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;


#endregion

namespace RunSystem
{
    public partial class FrmSOContract : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mCity = string.Empty, 
            mCnt = string.Empty, 
            mSADNo = string.Empty,
            mJOType = string.Empty;
        internal bool
            mIsFilterBySite = false,
            mIsCustomerItemNameMandatory = false,
            mIsSOUseARDPValidated =false,
            mIsSOContractUseTax = false,
            mIsCustomerContactPersonNonMandatory = false,
            mIsSOContractJointOperationMandatory = false,
            mIsShippingNameValidatedContactPerson = false;
        private bool IsInsert = false;
        internal string mIsSoUseDefaultPrintout,
            mProjectAcNoFormula = string.Empty,
            mGenerateCustomerCOAFormat = string.Empty;
        internal int mNumberOfSalesUomCode = 1;
        internal FrmSOContractFind FrmFind;
        private string mJointOperationKSOType = string.Empty,
            mSOContractCOAFormat = string.Empty;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmSOContract(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "SO Contract";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetNumberOfSalesUomCode();
                GetParameter();
                if (mJointOperationKSOType.Length <= 0) mJointOperationKSOType = "1";
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCtCode(ref LueCtCode);
                SetLueStatus(ref LueStatus);
                Sl.SetLueOption(ref LueJOType, "JointOperationType");
                if (mIsSOContractJointOperationMandatory)
                    label24.ForeColor = Color.Red;

                TcOutgoingPayment.SelectedTabPage = TpBankGuarantee;
                Sl.SetLueBankCode(ref LueBankCode);

                TcOutgoingPayment.SelectedTabPage = TpJO;
                SetLueJOCode(ref LueJOCode, string.Empty);
                LueJOCode.Visible = false;
                DteJODocDt.Visible = false;

                TcOutgoingPayment.SelectedTabPage = TpItem;
                TcOutgoingPayment.SelectedTabPage = TpBOQ;

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (mIsCustomerContactPersonNonMandatory)
                {
                    BtnContact.Visible = true;
                    BtnContact.Enabled = true;
                }
                else
                {
                    BtnContact.Visible = false;
                    BtnContact.Enabled = false;
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Set Grid

        private void SetGrd()
        {

            #region Grid 3 - Item

            Grd3.Cols.Count = 30;
            Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd3, new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Agent Code",
                        "Agent",
                        "Item's"+Environment.NewLine+"Code",
                        "",

                        //6-10
                        "Item's Name",
                        "Packaging",
                        "Quantity"+Environment.NewLine+"Packaging",
                        "Quantity",
                        "UoM",

                        //11-15
                        "Price"+Environment.NewLine+"(Price List)",
                        "Discount"+Environment.NewLine+"%",
                        "Discount"+Environment.NewLine+"Amount",
                        "Price After"+Environment.NewLine+"Discount",                        
                        "Promo"+Environment.NewLine+"%",

                        //16-20
                        "Price"+Environment.NewLine+"Before Tax",
                        "Tax"+Environment.NewLine+"%",
                        "Tax"+Environment.NewLine+"Amount",
                        "Price"+Environment.NewLine+"After Tax",                        
                        "Total",

                        //21-25
                        "Delivery"+Environment.NewLine+"Date",
                        "Remark",
                        "CtQtDNo",
                        "Specification",
                        "Customer's"+Environment.NewLine+"Item Code",

                        //26-29
                        "Customer's"+Environment.NewLine+"Item Name",
                        "Volume",
                        "Total Volume",
                        "Uom"+Environment.NewLine+"Volume"
                    },
                    new int[] 
                    {
                        //0
                        40,

                        //1-5
                        20, 0, 180, 80, 20, 
                        
                        //6-10
                        200, 100, 100, 80, 80, 
                        
                        //11-15
                        100, 80, 100, 100, 80,
                        
                        //16-20
                        100, 80, 100, 100, 120,   
                        
                        //21-25
                        100, 400, 0, 120, 120,

                        //26-29
                        250, 100, 100, 100
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 1, 5 });
            Sm.GrdFormatDate(Grd3, new int[] { 21 });
            Sm.GrdFormatDec(Grd3, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 }, 0);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 });
            Grd3.Cols[24].Move(7);
            Grd3.Cols[26].Move(7);
            Grd3.Cols[25].Move(7);
            Grd3.Cols[27].Move(14);
            Grd3.Cols[28].Move(15);
            Grd3.Cols[29].Move(16);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2, 3, 4, 5, 12, 14, 15, 17, 18, 23, 24, 25, 27, 28, 29 }, false);
            if (!mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd3, new int[] { 26 });

            #endregion

            #region Grid 1 - ARDP

            Grd1.Cols.Count = 8;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "AR Downpayment#", 
                        
                        //1-5
                        "Date",
                        "Currency",
                        "Amount",
                        "Person In Charge",
                        "Voucher Request#",

                        //6-7
                        "Voucher#",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        150,
 
                        //1-5
                        80, 80, 100, 200, 150, 
                        
                        //6-7
                        150, 250 
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });

            #endregion

            #region Grid 2 - BOQ

            Grd2.Cols.Count = 11;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "BOQ Document", 
                        
                        //1-5
                        "BOQ Date",
                        "LOP Document",
                        "Project",
                        "BOQ Item Code",
                        "BOQ Item Name",
                        //6-10
                        "BOQ Amount",
                        "Amount",
                        "Allow",
                        "Amt",
                        "parentInd"
                    },
                    new int[] 
                    {
                        //0
                        150,
 
                        //1-5
                        120, 150, 250, 100, 250,
                        //6-8
                        150, 150, 80, 150, 80
                    }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 1 });
            Sm.GrdFormatDec(Grd2, new int[] { 6, 7, 9 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd2, new int[] { 8, 9, 10 });

            #endregion

            #region Grid 5 - Joint Operation

            Grd5.Cols.Count = 6;

            Sm.GrdHdrWithColWidth(
                Grd5,
                new string[] 
                {
                    //0
                    "JOCode", 
                    
                    //1-5
                    "Joint Operation",
                    "Joint Operation's" + Environment.NewLine + "Date",
                    "Joint Operation's" + Environment.NewLine + "Document#",
                    "Portion (%)",
                    "NPWP"
                },
                new int[] 
                {
                    //0
                    0,
                    //1-3
                    120, 150, 150, 120, 120
                }
            );
            Sm.GrdFormatDate(Grd5, new int[] { 2 });
            Sm.GrdFormatDec(Grd5, new int[] { 4 }, 0);
            Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0 });
            Sm.GrdColInvisible(Grd5, new int[] { 0 });

            #endregion

        }
        #endregion

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd3, new int[] { 3, 4, 5, 12, 14, 15, 17, 18, 24, 25, 27, 28, 29 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtBOQDocNo, TxtLocalDocNo, LueCtCode, MeeCancelReason,
                        LueCustomerContactperson, MeeRemark,  ChkCancelInd, LueStatus, LueSPCode, 
                        LueShpMCode, TxtProjectName, MeeProjectDesc, LueJOType, TxtBankGuaranteeNo,
                        LueBankCode, DteBankGuaranteeDt, LueJOCode, DteJODocDt
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnCustomerShipAddress.Enabled = false;
                    BtnContact.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 7 });
                    Grd5.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtLocalDocNo, DteDocDt, LueCtCode, LueCustomerContactperson, LueShpMCode, LueSPCode, 
                       MeeRemark, MeeProjectDesc, LueJOCode, DteJODocDt, LueJOType, TxtBankGuaranteeNo,
                        LueBankCode, DteBankGuaranteeDt,
                    }, false);
                    BtnCustomerShipAddress.Enabled = true;
                    BtnContact.Enabled = true;
                    DteDocDt.Focus();
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 7 });
                    Grd5.ReadOnly = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    LueStatus.Focus();
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mSADNo = string.Empty;
            mCnt = string.Empty;
            mCity = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, DteDocDt, TxtBOQDocNo, LueStatus, TxtLocalDocNo,
                  TxtSAName, TxtAddress, TxtCity, TxtCountry, TxtPostalCd, 
                  TxtPhone, TxtFax, TxtEmail, TxtMobile, MeeCancelReason,
                  LueCtCode, LueCustomerContactperson, MeeRemark, LueShpMCode, LueSPCode,
                  TxtProjectName, MeeProjectDesc, LueJOType, TxtBankGuaranteeNo,
                  LueBankCode, DteBankGuaranteeDt, LueJOCode, DteJODocDt
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt, TxtContractAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd5, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 8, 9, 11, 12, 13, 14, 16, 17, 18, 19, 20 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3 });
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 4 });
        }

        private void ClearData2()
        {
            mSADNo = string.Empty;
            mCity = string.Empty;
            mCnt = string.Empty;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtAddress, TxtCity, TxtCountry, TxtPostalCd, TxtPhone, 
                TxtFax, TxtEmail, TxtMobile
            });
        }


        #endregion
    
        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSOContractFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueStatus, Sm.GetValue("Select 'O' "));
                IsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 &&
                       !Sm.IsLueEmpty(LueCtCode, "Customer") &&
                       !Sm.IsTxtEmpty(TxtBOQDocNo, "BOQ Document", false))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                    Sm.FormShowDialog(new FrmSOContractDlg3(
                        this,
                        e.RowIndex,
                        mNumberOfSalesUomCode > 1,
                        Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length != 0,
                        !BtnSave.Enabled,
                        Sm.GetLue(LueCtCode),
                        Sm.GetGrdStr(Grd3, e.RowIndex, 4)));
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd3, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
                {
                    Sm.GrdRemoveRow(Grd3, e, BtnSave);
                }
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && //BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
                    !Sm.IsLueEmpty(LueCtCode, "Customer") &&
                    !Sm.IsTxtEmpty(TxtBOQDocNo, "BOQ Document", false))
                Sm.FormShowDialog(
                new FrmSOContractDlg3(
                    this,
                    e.RowIndex,
                    mNumberOfSalesUomCode > 1,
                    Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length != 0,
                    !BtnSave.Enabled,
                    Sm.GetLue(LueCtCode),
                    Sm.GetGrdStr(Grd3, e.RowIndex, 4))
                );

                if (e.ColIndex == 5 && Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length != 0)
                {
                    var f = new FrmItem(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = Sm.GetGrdStr(Grd3, e.RowIndex, 4);
                    f.ShowDialog();
                }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 7}, e.ColIndex))
            {
                InputAmount(e.RowIndex, e.ColIndex);
                ComputeTotalBOQ();
                //ComputeItem();
            }
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd5.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 1, 2 }, e.ColIndex))
                {
                    //e.DoDefault = false;
                    if (e.ColIndex == 1) LueRequestEdit(Grd5, LueJOCode, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 2) Sm.DteRequestEdit(Grd5, DteJODocDt, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd5, e.RowIndex);
                }
            }
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd5.ReadOnly)
            {
                Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd5, e, BtnSave);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SOContract", "TblSOContractHdr");
            string mLOPDocNo = Sm.GetValue("Select LOPDocNo From TblBOQHdr Where DocNo = '"+TxtBOQDocNo.Text+"' ");
            string mSiteCode = Sm.GetValue("Select SiteCode From TblLOPHdr Where DocNo = @Param ",mLOPDocNo);
            string mProjectScope = Sm.GetValue("Select ProjectScope From TblLOPHdr  Where DocNo = @Param ", mLOPDocNo);
            string ProjectSequenceNo = GenerateProjectSequenceNo(mSiteCode, mProjectScope);
            string ProjectCode = string.Concat(mSiteCode, ".", ProjectSequenceNo, mProjectScope);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSOContractHdr(DocNo, ProjectSequenceNo, ProjectCode, mSiteCode));
            // save SO Contract Revision
            cml.Add(SaveSOContractRevisionHdr(DocNo));
            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0)
                {
                    cml.Add(SaveSOContractDtl(DocNo, Row));
                    cml.Add(SaveSOContractRevisionDtl(DocNo, Row));
                }
            }

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0)
                {
                    cml.Add(SaveSOContractDtl2(DocNo, Row));
                    cml.Add(SaveSOContractRevisionDtl2(DocNo, Row));
                }
            }

            if (Grd5.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                        cml.Add(SaveSOContractDtl3(DocNo, Row));
                }
            }

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueStatus, "Status") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueCustomerContactperson, "Contact Person") ||
                Sm.IsTxtEmpty(TxtSAName, "Shipping name", false) ||
                Sm.IsTxtEmpty(TxtAddress, "Address", false) ||
                Sm.IsTxtEmpty(TxtBOQDocNo, "Bill Of Quantity", false) ||
                IsBOQAlreadyProceed() ||
                IsDocumentLOPProcessIndInvalid()||
                (mIsSOContractJointOperationMandatory && Sm.IsLueEmpty(LueJOType, "Joint Operation")) ||
                Sm.IsLueEmpty(LueSPCode, "Sales Person") ||
                Sm.IsLueEmpty(LueShpMCode, "Shipment Method") ||
                IsGrdEmpty() ||
                IsJointOperationEmpty() ||
                IsJointOperationInvalid() ||
                IsBankGuaranteeEmpty()||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid()||
                IsCOASalesEmpty()||
                IsCOAOptionEmpty();
        }

        private bool IsBOQAlreadyProceed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblSOContractHdr ");
            SQL.AppendLine("Where BOQDocNo = @Param ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("And Status In ('O', 'A') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtBOQDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This BOQ data already proceed in SOC#" + Sm.GetValue(SQL.ToString(), TxtBOQDocNo.Text));
                return true;
            }

            return false;
        }

        private bool IsBankGuaranteeEmpty()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblBOQHdr A ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And BankGuaranteeInd = 'Y' ");
            SQL.AppendLine("limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtBOQDocNo.Text))
            {
                if (Sm.IsTxtEmpty(TxtBankGuaranteeNo, "Bank Guarantee#", false)) { TcOutgoingPayment.SelectedTabPage = TpBankGuarantee; return true; }
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) { TcOutgoingPayment.SelectedTabPage = TpBankGuarantee; return true; }
                if (Sm.IsDteEmpty(DteBankGuaranteeDt, "Date")) { TcOutgoingPayment.SelectedTabPage = TpBankGuarantee; return true; }
                //Sm.StdMsg(mMsgType.Warning, "You need to input Bank Guarantee data. " );
                //return true;
            }

            //if (TxtBankGuaranteeNo.Text.Length == 0)
            //{
            //    Sm.StdMsg(mMsgType.Warning, "Bank Guarantee# is Empty. ");
            //    return true;
            //}

            //if ( Sm.GetLue(LueBankCode).Length == 0)
            //{
            //    Sm.StdMsg(mMsgType.Warning, "Bank Name is Empty. ");
            //    return true;
            //}

            return false;
        }

        private bool IsJointOperationEmpty()
        {
            if (Sm.GetLue(LueJOType) == mJointOperationKSOType)
            {
                if(Grd5.Rows.Count <= 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input Joint Operation data.");
                    TcOutgoingPayment.SelectedTabPage = TpJO;
                    Sm.FocusGrd(Grd5, 0, 1);
                    return true;
                }
            }

            return false;
        }

        private bool IsJointOperationInvalid()
        {
            if (Sm.GetLue(LueJOType) == mJointOperationKSOType)
            {
                for (int i = 0; i < Grd5.Rows.Count - 1; i++)
                {
                    if (Sm.IsGrdValueEmpty(Grd5, i, 1, false, "Joint Operation is empty.")) { TcOutgoingPayment.SelectedTabPage = TpJO; return true; }
                    if (Sm.IsGrdValueEmpty(Grd5, i, 2, false, "Joint Operation's Date is empty.")) { TcOutgoingPayment.SelectedTabPage = TpJO; return true; }
                    if (Sm.IsGrdValueEmpty(Grd5, i, 3, false, "Joint Operation's Number is empty.")) { TcOutgoingPayment.SelectedTabPage = TpJO; return true; }
                }
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd3, Row, 4, false, "Item is empty.")) return true;

            return false;
        }

        private bool IsDocumentLOPProcessIndInvalid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.LOPDocNo From TblBOQHdr A ");
            SQL.AppendLine("Inner Join TblLOPHdr B On A.LOPDocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @Param ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("B.ProcessInd In ('S', 'C') ");
            SQL.AppendLine("Or B.CancelInd = 'Y' ");
            SQL.AppendLine("Or B.Status = 'C' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtBOQDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This LOP data is either Lose or Cancelled. LOP# : " + Sm.GetValue(SQL.ToString(), TxtBOQDocNo.Text));
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd3.Rows.Count-1 > 1)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item entered (" + (Grd3.Rows.Count - 1).ToString() + ") exceeds the maximum 1.");
                return true;
            }
            return false;
        }

        private bool IsCOASalesEmpty()
        {
            string AcNo  = Sm.GetValue("Select B.AcNo4 From tblItem A "+
                            "Inner Join TblItemcategory B On A.ItCtCode = B.ItCtCode "+
                            "Where A.ItCode = '" + Sm.GetGrdStr(Grd3, 0, 4) + "'"); 
            if (AcNo.Length<=0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Account sales on master item category, "+Environment.NewLine+" for item (" + Sm.GetGrdStr(Grd3, 0, 6) + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsCOAOptionEmpty()
        {
            string AcNo = Sm.GetValue("Select A.OptCode From TblOption A "+
                "Inner Join TblItemcategory B On A.OptCode = B.Acno4 "+
                "Inner Join TblItem C on B.ItCtCode = C.itCtCode " +
                "Where A.Optcat = 'AccountSet' And C.ItCode = '" + Sm.GetGrdStr(Grd3, 0, 4) + "'");

            if (AcNo.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Account sales on master item category, " + Environment.NewLine + " for item (" + Sm.GetGrdStr(Grd3, 0, 6) + ") "+Environment.NewLine+" should be listed on system option menu.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd3.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveSOContractHdr(string DocNo, string ProjectSequenceNo, string ProjectCode, string SiteCode)
        {
            //string HOInd = Sm.GetValue("Select if(B.HOInd = 'Y', '0', '1') As HoInd From tblLOPhdr A "+
            //                                                     "Inner Join TblSite B On A.SiteCode = b.SiteCode "+
            //                                                     "Where DocNo = (Select LopDocNo From tblBOQHdr Where DocNO = '"+TxtBOQDocNo.Text+"')");
            mJOType = Sm.GetLue(LueJOType);
            string ProfitCode =  Sm.GetValue("Select B.profitCenterCode From tblLOPhdr A "+
                                                                  "Inner Join TblSite B On A.SiteCode = b.SiteCode "+
                                                                  "Where DocNo = (Select LopDocNo From tblBOQHdr Where DocNO = '"+TxtBOQDocNo.Text+"')");
           
            string CCCode = Sm.GetValue("Select A.CCCode From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '"+TxtBOQDocNo.Text+"'  ) ");
            string ProjectScope =  Sm.GetValue("Select A.ProjectScope From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '"+TxtBOQDocNo.Text+"'  ) ");
            string ProjectResource = Sm.GetValue("Select A.projectresource From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '"+TxtBOQDocNo.Text+"'  ) ");
            string ProjectType = Sm.GetValue("Select A.ProjectType From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '"+TxtBOQDocNo.Text+"'  ) ");
            string ProjectName = Sm.GetValue("Select A.ProjectName From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '" + TxtBOQDocNo.Text + "'  ) ");
            string CoaOption = Sm.GetValue("Select OptCode From tblOption Where Optcat = 'AccountSet' limit 1 ");
            string Yr =  Sm.Right(Sm.GetDte(DteDocDt).Substring(0, 4), 2);

            string Acno = string.Concat(ProfitCode, CCCode, ProjectScope, ProjectResource, ProjectType, Yr);
            
            //ngeceknya ke salah satu account saja buat dapetin 2 digit terakhir
            string lastDigit = GetDigitAcno(string.Concat(CoaOption,'.', Acno));
            
            // new COA format
            string AcNo2 = string.Empty;
            string SequenceNo = string.Empty;

            if (mSOContractCOAFormat == "2") // VIR
            {
                if (ProjectResource.Length == 0) ProjectResource = "0";
                if (ProjectType.Length == 0) ProjectType = "0";
                if (ProjectScope.Length == 0) ProjectScope = "0";

                if (SiteCode.Length == 0) SiteCode = "000"; else SiteCode = Sm.Left(string.Concat(SiteCode, "0"), 3);

                SequenceNo = GetSequenceNo(ProjectResource, ProjectType, ProjectScope, CCCode, Yr);
                if (SequenceNo.Length == 0) SequenceNo = "01"; else SequenceNo = Sm.Right(string.Concat("00", SequenceNo), 2);

                //command by dita 03/01/2022
                // AcNo2 = string.Concat(ProjectResource, ProjectType, ProjectScope, ".", Yr, SequenceNo);
                AcNo2 = string.Concat(CCCode, ".", ProjectScope, ProjectResource, ProjectType, ".", Yr, SequenceNo);
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractHdr(DocNo, LocalDocNo, DocDt, Status, CancelInd, ");
            SQL.AppendLine("CtCode, CtContactPersonName, BOQDocNo, CurCode, Amt, Amt2, ");
            SQL.AppendLine("SADNo, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, ");
            SQL.AppendLine("SAPhone, SAFax, SAEmail, SAMobile, JOType, ");
            SQL.AppendLine("SpCode, ShpMCode, Remark, ProjectDesc, ProjectCode, ProjectSequenceNo,ProjectCode2, CreateBy, CreateDt, BankGuaranteeNo, BankCode, BankGuaranteeDt  ) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @LocalDocNo, @DocDt, 'A', @CancelInd, ");
            SQL.AppendLine("@CtCode, @CtContactPersonName, @BOQDocNo, ");
            SQL.AppendLine("(Select CurCode From TblBOQHdr Where DocNo=@BOQDocNo), @Amt, @Amt2, ");
            SQL.AppendLine("@SADNo, @SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCd, ");
            SQL.AppendLine("@SAPhone, @SAFax, @SAEmail, @SAMobile, @JOType, ");
            SQL.AppendLine("@SpCode, @ShpMCode, @Remark, @ProjectDesc, @AcNo, @ProjectSequenceNo, @ProjectCode2, @CreateBy, CurrentDateTime(), @BankGuaranteeNo, @BankCode, @BankGuaranteeDt); ");

          
            if (IsInsert && mGenerateCustomerCOAFormat == "2")
            {
                if (mSOContractCOAFormat == "1")
                {
                    SQL.AppendLine("Insert into TblCoa(AcNo, AcDesc, ActInd, Parent, Level, AcType, EntCode, SOCDocNo, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
                    SQL.AppendLine("Select Concat(OptCode,'.', @AcNo) As AcNo, Concat(OptDesc, ' ', @ProjectName) As AcDesc, ");
                    SQL.AppendLine("'Y' ActiveInd,  ");
                    if(mProjectAcNoFormula == "2")
                        SQL.AppendLine("Concat(OptCode, '.', '"+ SiteCode +"') As Parent,  ");
                    else 
                        SQL.AppendLine("OptCode As Parent,  ");
                    SQL.AppendLine("(Length(Concat(OptCode,'.', @AcNo)) - Length(Replace(Concat(OptCode,'.', @AcNo), '.', ''))+1)   As Lvl, ");
                    SQL.AppendLine("B.Actype, null, @DocNo, 'Sys', CurrentDatetime() , null, null  ");
                    SQL.AppendLine("From tblOption A ");
                    SQL.AppendLine("Inner Join TblCoa B On A.OptCode = B.Acno  ");
                    SQL.AppendLine("Where A.OptCat = 'AccountSet'  ");

                    SQL.AppendLine("On Duplicate Key Update LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
                    SQL.AppendLine("; ");
                }
                else
                {
                    SQL.AppendLine("Insert into TblCoa(AcNo, AcDesc, ActInd, Parent, Level, AcType, EntCode, SOCDocNo, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
                    SQL.AppendLine("Select Concat(OptCode,'.', @AcNo) As AcNo, Concat(OptDesc, ' ', @ProjectName) As AcDesc, ");
                    SQL.AppendLine("'Y' ActiveInd, OptCode As Parent,  (Length(Concat(OptCode,'.', @AcNo2)) - Length(Replace(Concat(OptCode,'.', @AcNo2), '.', ''))+1)   As Lvl, ");
                    SQL.AppendLine("B.Actype, null, @DocNo, 'Sys', CurrentDatetime() , null, null  ");
                    SQL.AppendLine("From tblOption A ");
                    SQL.AppendLine("Inner Join TblCoa B On A.OptCode = B.Acno  ");
                    SQL.AppendLine("Where A.OptCat = 'AccountSet'  ");
                    if (mJOType.Length > 0)
                    {
                        SQL.AppendLine("And ( ");
                        SQL.AppendLine("    A.OptCode Not In ");
                        SQL.AppendLine("    ( ");
                        SQL.AppendLine("        Select Distinct(Property1) From TblOption ");
                        SQL.AppendLine("        Where OptCat = 'JointOperationType' ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine("    Or A.OptCode In ");
                        SQL.AppendLine("    ( ");
                        SQL.AppendLine("        Select Distinct(Property1) From TblOption ");
                        SQL.AppendLine("        Where OptCat = 'JointOperationType' ");
                        SQL.AppendLine("        And OptCode = @JOType ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }

                    #region Old
                    /*
                    if (mJOType == "1")
                        SQL.AppendLine("And A.OptCode != '4.0.0.0' ");
                    else if(mJOType == "2")
                        SQL.AppendLine("And A.OptCode != '4.1.0.0' ");
                    */
                    #endregion

                    SQL.AppendLine("On Duplicate Key Update LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
                    SQL.AppendLine("; ");
                }
            }
                
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CtContactPersonName", Sm.GetLue(LueCustomerContactperson));
            Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtBOQDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtContractAmt.Text));
            Sm.CmParam<String>(ref cm, "@SADNo", mSADNo);
            Sm.CmParam<String>(ref cm, "@SAName", TxtSAName.Text);
            Sm.CmParam<String>(ref cm, "@SAAddress", TxtAddress.Text);
            Sm.CmParam<String>(ref cm, "@SACityCode", mCity);
            Sm.CmParam<String>(ref cm, "@SACntCode", mCnt);
            Sm.CmParam<String>(ref cm, "@SAPostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@SAPhone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@SAFax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@SAEmail", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@SAMobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@SPCode", Sm.GetLue(LueSPCode));
            Sm.CmParam<String>(ref cm, "@ShpMCode", Sm.GetLue(LueShpMCode));
            Sm.CmParam<String>(ref cm, "@JOType", Sm.GetLue(LueJOType));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@ProjectDesc", MeeProjectDesc.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@ProjectName", ProjectName);

            if (mProjectAcNoFormula == "2") Sm.CmParam<String>(ref cm, "@AcNo", ProjectCode);
            else
            {
                if (mSOContractCOAFormat == "1")
                    Sm.CmParam<String>(ref cm, "@AcNo", String.Concat(Acno, lastDigit));
                else
                    Sm.CmParam<String>(ref cm, "@AcNo", AcNo2);
            }
            
            if (mSOContractCOAFormat == "2")
                Sm.CmParam<String>(ref cm, "@AcNo2", string.Concat(SiteCode,".",CCCode, ".", AcNo2));

            Sm.CmParam<String>(ref cm, "@ProjectSequenceNo", ProjectSequenceNo);

            if (mProjectAcNoFormula == "2")
                Sm.CmParam<String>(ref cm, "@ProjectCode2", ProjectCode);
            else
                Sm.CmParam<String>(ref cm, "@ProjectCode2", AcNo2);
            
            Sm.CmParam<String>(ref cm, "@BankGuaranteeNo", TxtBankGuaranteeNo.Text);
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParamDt(ref cm, "@BankGuaranteeDt", Sm.GetDte(DteBankGuaranteeDt));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt.Text));

            return cm;
        }

        private MySqlCommand SaveSOContractDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblSOContractDtl(DocNo, DNo, ItCode, PackagingUnitUomCode, QtyPackagingUnit, Qty, TaxRate, TaxAmt, UPrice, UPriceBefTax, UPriceAfTax, Amt, DeliveryDt,  Remark, CreateBy, CreateDt) " +
                "Values(@DocNo, @DNo, @ItCode, @PackagingUnitUomCode, @QtyPackagingUnit, @Qty, @TaxRate, @TaxAmt, @UPrice, @UPriceBefTax, @UPriceAfTax, @Amt, @DeliveryDt,  @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd3, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@TaxRate", Sm.GetGrdDec(Grd3, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Sm.GetGrdDec(Grd3, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd3, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@UPriceBefTax", Sm.GetGrdDec(Grd3, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@UPriceAfTax", Sm.GetGrdDec(Grd3, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 20));
            Sm.CmParamDt(ref cm, "@DeliveryDt", Sm.GetGrdDate(Grd3, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 22));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractDtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblSOContractDtl2(DocNo, DNo, BOQDocNo, ItBOQCode, Amt, CreateBy, CreateDt) " +
                "Values(@DocNo, @DNo, @BOQDocNo, @ItBOQCode, @Amt, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<String>(ref cm, "@BOQDocNo", Sm.GetGrdStr(Grd2, Row, 0));
            Sm.CmParam<String>(ref cm, "@ItBOQCode", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractDtl3(string DocNo, int Row)
        {
            var SQLDtl3 = new StringBuilder();

            SQLDtl3.AppendLine("Insert Into TblSOContractDtl3(DocNo, DNo, JOCode, JODocDt, JODocNo, PortionPercentage, NPWP, CreateBy, CreateDt) ");
            SQLDtl3.AppendLine("Values(@DocNo, @DNo, @JOCode, @JODocDt, @JODocNo, @PortionPercentage, @NPWP, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQLDtl3.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<String>(ref cm, "@JOCode", Sm.GetGrdStr(Grd5, Row, 0));
            Sm.CmParamDt(ref cm, "@JODocDt", Sm.GetGrdDate(Grd5, Row, 2));
            Sm.CmParam<String>(ref cm, "@JODocNo", Sm.GetGrdStr(Grd5, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@PortionPercentage", Sm.GetGrdDec(Grd5, Row, 4));
            Sm.CmParam<String>(ref cm, "@NPWP", Sm.GetGrdStr(Grd5, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractRevisionHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionHdr(DocNo, SOCDocNo, DocDt, Status, Amt, PrevAmt, Amt2,  ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @SOCDocNo, @DocDt, 'O', @Amt, @Amt, @Amt2, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            if (IsRevisionNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting ");
                SQL.AppendLine("Where DocType='SOCRev'; ");
            }

            SQL.AppendLine("Update TblSOContractRevisionHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='SOCRev' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo, "/0001"));
            Sm.CmParam<String>(ref cm, "@SOCDocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtContractAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractRevisionDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionDtl(DocNo, SOCDocNo, DNo, Amt, PrevAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @SOCDocNo, @DNo, @Amt, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo, "/0001"));
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<String>(ref cm, "@SOCDocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 20));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractRevisionDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionDtl2(DocNo, SOCDocNo, DNo, Amt, PrevAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @SOCDocNo, @DNo, @Amt, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo, "/0001"));
            Sm.CmParam<String>(ref cm, "@SOCDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        public static string GetDigitAcno(string AcNo)
        {            
            string Lastdigit = Sm.GetValue("Select MAX(Right(AcNo, 2)) From TblCoa Where Acno like '"+AcNo+"%'");

            if (Lastdigit.Length > 0)
            {
                Lastdigit = Sm.Right(Convert.ToString(string.Concat("00",(decimal.Parse(Lastdigit) + 1))), 2);
            }
            else
            {
                Lastdigit = "01";
            }

            return Lastdigit;
        }

        private string GetSequenceNo(string ProjectResource, string ProjectType, string ProjectScope, string CCCOde, string Yr)
        {
            string seq = string.Empty;
            string sampleAccountSet = Sm.GetValue("Select OptCode From TblOption Where OptCat = 'AccountSet' Limit 1; ");
            string param = string.Concat(sampleAccountSet, ".", CCCOde, ".", ProjectScope, ProjectResource, ProjectType, ".", Yr);
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Count(*) ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Where AcNo Like @Param; ");

            seq = Sm.GetValue(SQL.ToString(), string.Concat(param, "%"));

            if (seq == "0") seq = string.Empty;
            else
            {
                if (Int32.Parse(seq) > 0) seq = (Int32.Parse(seq) + 1).ToString();
            }

            return seq;
        }

        private void InputAmount(int RowIndex, int ColIndex)
        {
            if (ColIndex == 7)
            {
                decimal Amt = Sm.GetGrdDec(Grd2, RowIndex, 9);
                if (Sm.GetGrdStr(Grd2, RowIndex, 8) == "Y") // allowed to edit & required
                {
                    string Ac = Sm.GetGrdStr(Grd2, RowIndex, 4);
                    string ccc = string.Empty;
                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int y = 0; y < RowIndex; y++)
                            {
                                if (Sm.GetGrdStr(Grd2, y, 4) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 7));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, y, 7));
                                    decimal Qty = QtyRow + QtyRow2 - Amt;
                                    Grd2.Cells[y, 7].Value = Qty;
                                    Grd2.Cells[y, 9].Value = Qty;
                                    Grd2.Cells[RowIndex, 9].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Sm.StdMsg(mMsgType.Info, "Can't input amount for this BOQ item");
                }
            }

           
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (ChkCancelInd.Checked == true)
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;
            }
            else
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid2()) return;
            }

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateSOContractHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueStatus, "Status") ||
                IsSOContractAlreadyCancelled() ||
                IsSOContractAlreadyFullfiled() ||
                IsSOContractAlreadyProcessedToProjectImplementation() ||
                IsSOContractAlreadyProcessedToARDP()
                ;
        }

        private bool IsSOContractAlreadyProcessedToProjectImplementation()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo = B.DocNo ");
            SQL.AppendLine("Where B.SOCDocNo = @Param ");
            SQL.AppendLine("And (A.CancelInd = 'N' And A.Status <> 'C') ");
            SQL.AppendLine("Limit 1; ");

            if(Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document has already processed to Project Implementation : " + Sm.GetValue(SQL.ToString(), TxtDocNo.Text) + ".");
                return true;
            }

            return false;
        }

        private bool IsCancelledDataNotValid2()
        {
            return
                Sm.IsLueEmpty(LueStatus, "Status") ||
                IsSOContractAlreadyCancelled() ||
                IsSOContractAlreadyFullfiled() ||
                IsSOContractAlreadyProcessedToARDP();
        }

        private MySqlCommand UpdateSOContractHdr()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsSOContractAlreadyCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblSOContractHdr " +
                    "Where DocNo=@DocNo And (CancelInd='Y' Or Status = 'C') ;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This Document already cancelled.");
                ChkCancelInd.Checked = true;
                return true;
            }
            return false;
        }

        private bool IsSOContractAlreadyFullfiled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblSOContractDtl " +
                    "Where DocNo=@DocNo And processInd = 'F';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This Document already fullfiled.");
                Sm.SetLue(LueStatus, "F");
                return true;
            }
            return false;
        }

     
        private bool IsSOContractAlreadyProcessedToARDP()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SODocNo ");
            SQL.AppendLine("From TblARDownpayment ");
            SQL.AppendLine("Where SODocNo=@SODocNo And CancelInd = 'N' And Status = 'A' And CtCode=@CtCode ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SODocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));

            if (Sm.IsDataExist(cm) && mIsSOUseARDPValidated)
            {
                Sm.StdMsg(mMsgType.Warning, "This document already use to AR DownPayment.");
                return true;
            }
            return false;


        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSOContractHdr(DocNo);
                ShowSOContractDtl(DocNo);
                ShowSOContractDtl2(DocNo);
                ShowSOContractDtl3(DocNo);
                ShowARDownPayment(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSOContractHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.Status, A.CancelInd, ");
            SQL.AppendLine("A.CtCode, A.CtContactPersonName, ");
            SQL.AppendLine("A.BOQDocNo, A.Amt, A.Amt2, A.SAName, ifnull(A.SpCode, B.SpCode) As SpCode, ");
            SQL.AppendLine("A.ShpMCode, A.Remark, A.SADNo, ");
            SQL.AppendLine("IfNull(A.SAAddress, E.Address) As SAAddress, ");
            SQL.AppendLine("IfNull(A.SACityCode, E.CityCode) As SACityCode, ");
            SQL.AppendLine("IfNull(C.CityName, F.CityName) As CityName, ");
            SQL.AppendLine("IfNull(A.SACntCode, E.CntCode) As SACntCode, ");
            SQL.AppendLine("IfNull(D.CntName, G.CntName) As CntName, ");
            SQL.AppendLine("IfNull(A.SAPostalCD, E.PostalCD) As SAPostalCode, ");
            SQL.AppendLine("IfNull(A.SAPhone, E.Phone) As SAPhone, ");
            SQL.AppendLine("IfNull(A.SAFax, E.Fax) As SAFax, ");
            SQL.AppendLine("IfNull(A.SAEmail, E.Email) As SAEmail, A.JOType, ");
            SQL.AppendLine("IfNull(A.SAMobile, E.Mobile) As SAMobile, A.CancelReason, A.ProjectDesc, H.Projectname,  A.BankGuaranteeNo, A.BankCode, A.BankGuaranteeDt ");
            SQL.AppendLine("From TblSOContractHdr A  ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblCity C On A.SACityCode=C.CityCode ");
            SQL.AppendLine("Left Join TblCountry D On A.SACntCode=D.CntCode ");
            SQL.AppendLine("Left Join TblCustomerShipAddress E ");
            SQL.AppendLine("    On A.CtCode=E.CtCode ");
            SQL.AppendLine("    And E.DNo=( ");
            SQL.AppendLine("        Select T1.DNo ");
            SQL.AppendLine("        From TblCustomerShipAddress T1 ");
            SQL.AppendLine("        Inner Join TblSOContractHdr T2 On T2.DocNo=@DocNo And T1.CtCode=T2.CtCode And Upper(T1.Name)=Upper(T2.SAName) And T1.Address=T2.SAAddress And T2.SAAddress Is Not Null ");
            SQL.AppendLine("        Where T1.Address Is Not Null ");
            SQL.AppendLine("        Limit 1 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblCity F On E.CityCode=F.CityCode ");
            SQL.AppendLine("Left Join TblCountry G On E.CntCode=G.CntCode ");
            SQL.AppendLine("Inner Join TblLOphdr H On B.LOPDocNo = H.DocNO ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "LocalDocNo", "DocDt", "Status", "CancelInd","CtCode",    

                        //6-10
                        "CtContactPersonName", "BOQDocNo", "Amt", "SAName", "SpCode", 
                        
                        //11-15
                       "ShpMCode", "Remark", "SADNo", "SAAddress", "SACityCode", 
                        
                        //16-20
                        "CityName", "SACntCode", "CntName", "SAPostalCode", "SAPhone", 
                        
                        //21-25
                        "SAFax", "SAEmail", "SAMobile", "CancelReason", "ProjectDesc",
                        
                        //26-30
                        "ProjectName", "JOType",  "BankGuaranteeNo", "BankCode", "BankGuaranteeDt",

                        //31
                        "Amt2"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueStatus, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                        SetLueCtPersonCode(ref LueCustomerContactperson, Sm.DrStr(dr, c[5]), Sm.DrStr(dr, c[6]));
                        TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[7]);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[31]), 0);
                        TxtSAName.EditValue = Sm.DrStr(dr, c[9]);
                        SetLueSPCode(ref LueSPCode);
                        Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[10]));
                        SetLueDTCode(ref LueShpMCode);
                        Sm.SetLue(LueShpMCode, Sm.DrStr(dr, c[11]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                        mSADNo = Sm.DrStr(dr, c[13]);
                        TxtAddress.EditValue = Sm.DrStr(dr, c[14]);
                        mCity = Sm.DrStr(dr, c[15]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[16]);
                        mCnt = Sm.DrStr(dr, c[17]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[18]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[19]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[20]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[21]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[22]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[23]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[24]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        MeeProjectDesc.EditValue = Sm.DrStr(dr, c[25]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[26]);
                        Sm.SetLue(LueJOType, Sm.DrStr(dr, c[27]));
                        TxtBankGuaranteeNo.EditValue = Sm.DrStr(dr, c[28]);
                        Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[29]));
                        Sm.SetDte(DteBankGuaranteeDt, Sm.DrStr(dr, c[30]));
                        TxtContractAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    }, true
                );
        }

        private void ShowSOContractDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From (  ");
            SQL.AppendLine("   Select B.Dno, B.ItCode, G.ItName, G.Specification, I.CtItCode, I.CtItName, ");
            SQL.AppendLine("   B.PackagingUnitUomCode, B.QtyPackagingUnit, B.Qty, ");
            SQL.AppendLine("   B.Uprice, 0 As Discount, 0 As DiscountAmt, ");
            SQL.AppendLine("   B.UPriceBefTax As UPriceAfterDiscount, ");
            SQL.AppendLine("   0 As PromoRate, ");
            SQL.AppendLine("   B.UPriceBefTax As UPriceBefore, ");
            SQL.AppendLine("   B.TaxRate, B.TaxAmt, B.UPriceAfTax UPriceAfterTax, B.Amt Total, ");
            SQL.AppendLine("   B.DeliveryDt, B.Remark, J.Volume, (B.QtyPackagingUnit * J.Volume) As TotalVolume,  "); 
            SQL.AppendLine("   (Select parValue From tblparameter Where parCode = 'ItemVolumeUom') As VolUom  ");
            SQL.AppendLine("   From TblSOContractHdr A    ");
            SQL.AppendLine("   Inner Join TblSOContractDtl B On A.DocNo=B.DocNo   ");
            SQL.AppendLine("   Inner Join TblItem G On B.ItCode=G.ItCode  ");
            SQL.AppendLine("   Left Join TblCustomerItem I On B.ItCode=I.ItCode And A.CtCode=I.CtCode  ");
            SQL.AppendLine("   Left Join tblitempackagingunit J On G.ItCode = J.ItCode And G.SalesUomCode = J.UomCode  ");
            SQL.AppendLine("   Where A.DocNo=@DocNo  ");
            SQL.AppendLine(") T Order By DNo; ");
           
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "ItCode", "ItName", "PackagingUnitUomCode",  "QtyPackagingUnit", "Qty", 
                    //6-10
                    "UPrice", "Discount", "DiscountAmt", "UPriceAfterDiscount", "UPriceBefore",
                    //11-15
                    "TaxRate" , "TaxAmt", "UPriceAfterTax", "Total", "DeliveryDt",
                    //16-20
                    "Remark", "Specification", "CtItCode", "CtItName", "Volume",
                    //21-22
                    "TotalVolume", "VolUom"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 });
            Sm.FocusGrd(Grd3, 0, 1);

        }

        private void ShowSOContractDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Docno AS BOQDocNo, A.DocDt, B.DocNo As LOpDocNo, B.ProjectName,  ");
            SQL.AppendLine("A2.ItBoqCode, D.ItBOQName, A2.AMt, ifnull(E.AllowEdit, 'N') As Allow, if(D.parent is null, 'Y', 'N') ParentInd  ");
            SQL.AppendLine("From tblSOContracthdr A1 ");
            SQL.AppendLine("Inner Join TblSOContractDtl2 A2 On A1.DocNo = A2.Docno ");
            SQL.AppendLine("Inner Join tblBOQhdr A On A1.BOQDocno = A.DocNo ");
            SQL.AppendLine("Inner Join TblLopHdr B On A.LOPDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblBOQDtl C On A.DocNo = C.DocNo  And A2.ItBoqCode = C.ItBOQCode ");
            SQL.AppendLine("Inner Join TblItemBoQ D On C.ItBOQCode = D.ItBOQCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select ItBOQCode, 'Y' As AllowEdit  ");
            SQL.AppendLine("    From TblItemBOQ ");
            SQL.AppendLine("    Where ItBOQCode not in ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Parent From TblItemBOQ	 ");
            SQL.AppendLine("        Where parent is not null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(")E On C.ItBOQCode = E.ItBOQCode  ");
            SQL.AppendLine("Where A1.DocNo=@DocNo ");
            SQL.AppendLine("Order By D.ItBOQCode ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
              ref Grd2, ref cm, SQL.ToString(),
              new string[] 
                { 
                    //0
                    "BOQDocNo", 

                    //1-5
                    "DocDt", "LOpDocNo", "ProjectName", "ItBoqCode", "ItBOQName",
 
                    //6-8
                    "Amt", "Allow", "parentInd"
                },
              (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
              {
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                  Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);

              }, false, false, true, false
          );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowSOContractDtl3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.JOCode, B.JOName, A.JODocDt, A.JODocNo, IfNull(A.PortionPercentage, 0.00) As PortionPercentage, A.NPWP ");
            SQL.AppendLine("FROM TblSOContractDtl3 A ");
            SQL.AppendLine("INNER JOIN TblJointOperationHdr B ON A.JOCode = B.JOCode ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo ");
            SQL.AppendLine("ORDER BY A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd5, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "JOCode", 

                    //1-5
                    "JOName", "JODocDt", "JODocNo", "PortionPercentage", "NPWP"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 0);
        }

        private void ShowARDownPayment(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CurCode, A.Amt, ");
            SQL.AppendLine("A.PIC, A.VoucherRequestDocNo, B.VoucherDocNo, A.Remark ");
            SQL.AppendLine("From TblARDownPayment A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.SODocNo=@DocNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CurCode", "Amt", "PIC", "VoucherRequestDocNo",  
                    
                    //6-7
                    "VoucherDocNo", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        public void ShowBOQ(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.Docno AS BOQDocNo, A.DocDt, B.DocNo As LOpDocNo, B.ProjectName,  ");
            SQL.AppendLine("C.ItBoqCode, D.ItBOQName, C.totalAMt, ifnull(E.AllowEdit, 'N') As Allow, if(D.parent is null, 'Y', 'N') ParentInd  ");
            SQL.AppendLine("from tblBOQhdr A ");
            SQL.AppendLine("Inner Join TblLopHdr B On A.LOPDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblBOQDtl C On A.DocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblItemBoQ D On C.ItBOQCode = D.ItBOQCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select ItBOQCode, 'Y' As AllowEdit  ");
	        SQL.AppendLine("    From TblItemBOQ ");
	        SQL.AppendLine("    Where ItBOQCode not in ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select Parent From TblItemBOQ	 ");
		    SQL.AppendLine("        Where parent is not null ");
	        SQL.AppendLine("    ) ");
            SQL.AppendLine(")E On C.ItBOQCode = E.ItBOQCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo, C.ItBOQCode ;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "BOQDocNo", 

                    //1-5
                    "DocDt", "LOpDocNo", "ProjectName", "ItBoqCode", "ItBOQName",
 
                    //6-8
                    "TotalAmt", "Allow", "parentInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    if (Sm.GetGrdStr(Grd2, Row, 8) == "N")
                    {
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 7 });
                        Grd2.Cells[Row, 7].ReadOnly = iGBool.True;
                        Grd2.Cells[Row, 7].BackColor = Color.FromArgb(224, 224, 224);
                        Grd2.Cells[Row, 7].Value = Sm.GetGrdDec(Grd2, Row, 6);
                    }
                    else
                    {
                        Grd2.Cells[Row, 7].Value = Sm.GetGrdDec(Grd2, Row, 6);
                    }

                    
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void SetLueCtPersonCode(ref DXE.LookUpEdit Lue, string CtCode, string ContactPersonName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Col1 From ( ");
            SQL.AppendLine("Select ContactPersonName As Col1 From TblCustomerContactPerson Where CtCode=@CtCode ");
            if (ContactPersonName.Length > 0)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select @ContactPersonName As Col1 ");
            }
            SQL.AppendLine(") T Order By Col1;");

            var cm = new MySqlCommand();
            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@ContactPersonName", ContactPersonName);

            Sm.SetLue1(ref Lue, ref cm, "Contact Person");
            if (ContactPersonName.Length > 0) Sm.SetLue(Lue, ContactPersonName);

        }

        private bool IsRevisionNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType = 'SOCRev' Limit 1; ");
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueJOCode(ref LookUpEdit Lue, string JOCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select JOCode As Col1, JOName As Col2 ");
            SQL.AppendLine("From TblJointOperationHdr ");
            SQL.AppendLine("Where 0 = 0 ");
            
            if (JOCode.Length > 0)
                SQL.AppendLine("And JOCode = @JOCode ");
            else
                SQL.AppendLine("And ActInd = 'Y' ");
            
            SQL.AppendLine("Order By JOName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@JOCode", JOCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal void ComputeTotalBOQ()
        {
            decimal Amt = 0m;
            string BOQDocNo = TxtBOQDocNo.Text;
            //Amt = Decimal.Parse(Sm.GetValue("Select SUM(A.totalAmt) From TblBoqDtl A "+
            //      "Inner Join TblItemBOQ B On A.ItBOQCode = B.ItBOQCode And B.Parent Is null " +
            //      "Where A.DocNo = '"+BOQDocNo+"' ; "));

            for (int i = 0; i < Grd2.Rows.Count-1; i++)
            {
                if (Sm.GetGrdStr(Grd2, i, 10).Length > 0 && Sm.GetGrdStr(Grd2, i, 10) == "Y")
                {
                    Amt += Sm.GetGrdDec(Grd2, i, 9);
                }
            }
        
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
            ComputeItem();
        }

        internal void ComputeItem()
        {
            decimal Qty = 0m, TotalAmt = 0m, PriceGrid3 = 0m, TaxAmt = 0m, TaxPercentage = 0m;
            Qty = Sm.GetGrdDec(Grd3, 0, 8);
            TaxPercentage = Sm.GetGrdDec(Grd3, 0, 17);
            TotalAmt = Decimal.Parse(TxtAmt.Text);

            if (TotalAmt > 0 && Qty>0)
            {
                PriceGrid3 = TotalAmt * Qty;
                TaxAmt = PriceGrid3 * TaxPercentage * 0.01m;
                TotalAmt = PriceGrid3;
            }

            Grd3.Cells[0, 11].Value = PriceGrid3; // Price List
            Grd3.Cells[0, 14].Value = PriceGrid3; // Price After Discount
            Grd3.Cells[0, 16].Value = PriceGrid3; // Price Before Tax
            Grd3.Cells[0, 18].Value = TaxAmt; // Tax Amount
            Grd3.Cells[0, 19].Value = PriceGrid3 + TaxAmt; // Price After Tax
            Grd3.Cells[0, 20].Value = PriceGrid3 + TaxAmt; // Total

            TxtAmt.EditValue = Sm.FormatNum(TotalAmt, 0);
            TxtContractAmt.EditValue = Sm.FormatNum((PriceGrid3 + TaxAmt), 0);
        }

        private void SetNumberOfSalesUomCode()
        {
            string NumberOfSalesUomCode = Sm.GetParameter("NumberOfSalesUomCode");
            if (NumberOfSalesUomCode.Length == 0)
                mNumberOfSalesUomCode = 1;
            else
                mNumberOfSalesUomCode = int.Parse(NumberOfSalesUomCode);
        }

        private void GetParameter()
        {
            mIsSoUseDefaultPrintout = Sm.GetParameter("IsSoUseDefaultPrintout");
            mIsCustomerItemNameMandatory = Sm.GetParameterBoo("IsCustomerItemNameMandatory");
            mIsSOUseARDPValidated = Sm.GetParameterBoo("IsSOUseARDPValidated");
            mGenerateCustomerCOAFormat = Sm.GetParameter("GenerateCustomerCOAFormat");
            mJointOperationKSOType = Sm.GetParameter("JointOperationKSOType");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mProjectAcNoFormula = Sm.GetParameter("ProjectAcNoFormula");
            mIsSOContractUseTax = Sm.GetParameterBoo("IsSOContractUseTax");
            mSOContractCOAFormat = Sm.GetParameter("SOContractCOAFormat");
            mIsCustomerContactPersonNonMandatory = Sm.GetParameterBoo("IsCustomerContactPersonNonMandatory");
            mIsSOContractJointOperationMandatory = Sm.GetParameterBoo("IsSOContractJointOperationMandatory");
            mIsShippingNameValidatedContactPerson = Sm.GetParameterBoo("IsShippingNameValidatedContactPerson");

            if (mSOContractCOAFormat.Length == 0) mSOContractCOAFormat = "1";
        }

        private void SetBOQDocNo(string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblBOQHdr ");
            SQL.AppendLine("Where ActInd='Y' And Status='A' And CtCode=@CtCode ");
            SQL.AppendLine("Order By DocDt Desc, DocNo Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            TxtBOQDocNo.EditValue = Sm.GetValue(cm);

            string AllowEditSalesPerson = Sm.GetValue("Select IF( EXISTS(Select B.Spname from TblBOQhdr A " +
            "Inner Join TblSalesPerson B On A.SpCode=B.SpCode " +
            "Where A.DocNo = '" + Sm.GetValue(cm) + "'), 1, 0)");

            string AllowEditShipmentMethod = Sm.GetValue("Select IF( EXISTS(Select B.Dtname from TblBOQhdr A  " +
            "Inner Join TblDeliveryType B On A.ShpmCode=B.DtCode " +
            "Where A.DocNo = '" + Sm.GetValue(cm) + "'), 1, 0)");


            if (AllowEditSalesPerson == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueSPCode    
                }, false);
                SetLueSPCode(ref LueSPCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueSPCode    
                }, true);
                SetLueSPCode(ref LueSPCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }


            if (AllowEditShipmentMethod == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueShpMCode   
                }, false);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueShpMCode    
                }, true);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
        }

        private string GenerateProjectSequenceNo(string SiteCode, string ProjectScope)
        {
            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('000', Convert(ProjectSequenceNo+1, Char)), 3) From ( ");
            SQL.Append("       Select Convert(Left(A.ProjectSequenceNo, 3), Decimal) As ProjectSequenceNo  ");
            SQL.Append("       From TblSOContractHdr A ");
            SQL.Append("       Inner Join TblBOQHdr B On A.BOQDocNo= B.DocNo ");
            SQL.Append("       Inner Join TblLOPHdr C On B.LOPDocNo= C.DocNo ");
            SQL.Append("       Where C.SiteCode='" + SiteCode + "' And C.ProjectScope='" + ProjectScope + "' ");
            SQL.Append("       Order By Left(ProjectSequenceNo, 3) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '001') ");
            SQL.Append(") As ProjectSequenceNo");

            return Sm.GetValue(SQL.ToString());
           
        }

        #endregion

        #region SetLue

        public static void SetLueDTCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DTCode As Col1, DTName As Col2 From TblDeliveryType " +
                 "Union ALL Select 'ALL' As Col1, 'ALL' As Col2 Order By Col2;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union All Select 'ALL' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }



        public void SetLueCtPersonCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblCustomerContactPerson " +
                "Where CtCode= '" + CtCode + "' Order By ContactPersonName;",
                "Contact Person");
        }

        public static void SetLueCtPersonCodeShow(ref LookUpEdit Lue, string DocNo)
        {
            Sm.SetLue1(
                ref Lue,
                "Select CtContactPersonName As Col1 From TblSOContractHdr " +
                "Where DocNo= '" + DocNo + "' Order By CtContactPersonName;",
                "Contact Person");
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocType As Col1, T.Status As Col2 From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 'A' As Doctype, 'Approve' As Status ");
            SQL.AppendLine("    Union ALl ");
            SQL.AppendLine("    Select 'O' As Doctype, 'Outstanding' As Status ");
            SQL.AppendLine(")T ");


            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCustomerContactperson_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueCustomerContactperson, new Sm.RefreshLue3(SetLueCtPersonCode), Sm.GetLue(LueCtCode), string.Empty);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                TxtBOQDocNo.EditValue = null;
                ClearData2();
                Sm.ClearGrd(Grd1, true);
                var CtCode = Sm.GetLue(LueCtCode);
                if (CtCode.Length != 0 && BtnSave.Enabled)
                {
                    SetBOQDocNo(CtCode);
                    TxtSAName.EditValue = null;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void LueJOCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueJOCode, new Sm.RefreshLue2(SetLueJOCode), string.Empty);
            }
        }

        private void LueJOCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.LueKeyDown(Grd5, ref fAccept, e);
            }
        }

        private void LueJOCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueJOCode.Visible && fAccept && fCell.ColIndex == 1)
                {
                    if (Sm.GetLue(LueJOCode).Length == 0)
                        Grd5.Cells[fCell.RowIndex, 0].Value =
                        Grd5.Cells[fCell.RowIndex, 1].Value = null;
                    else
                    {
                        Grd5.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueJOCode);
                        Grd5.Cells[fCell.RowIndex, 1].Value = LueJOCode.GetColumnValue("Col2");
                    }
                    LueJOCode.Visible = false;
                }
            }
        }

        private void DteJODocDt_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.DteKeyDown(Grd5, ref fAccept, e);
            }
        }

        private void DteJODocDt_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.DteLeave(DteJODocDt, ref fCell, ref fAccept);
            }
        }

        #endregion

        #region Button Event

      
        private void BtnContact_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    //var f = new FrmCustomer(mMenuCode);
                    //f.Tag = mMenuCode;
                    //f.WindowState = FormWindowState.Normal;
                    //f.StartPosition = FormStartPosition.CenterScreen;
                    //f.mCtCode = Sm.GetLue(LueCtCode);
                    //f.ShowDialog();
                        
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mMenuCode = "RSYYNNN";
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                    SetLueCtPersonCode(ref LueCustomerContactperson, Sm.GetLue(LueCtCode), string.Empty);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

      
      
        private void BtnSOMaster_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    {
                        if (TxtBOQDocNo.EditValue != null && TxtBOQDocNo.Text.Length != 0)
                        {
                            var f = new FrmBOQ(mMenuCode);
                            f.Tag = "XXX";
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mDocNo = TxtBOQDocNo.Text;
                            f.ShowDialog();
                        }
                        else
                        {
                            var f = new FrmBOQ(mMenuCode);
                            f.Tag = "XXX";
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mCtCode = Sm.GetLue(LueCtCode);
                            f.ShowDialog();
                            TxtBOQDocNo.EditValue = Sm.GetValue("Select MAX(DocNo) From TblBOQhdr Where CtCode = '" + Sm.GetLue(LueCtCode) + "' ");
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnCtShippingAddress_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LueShpMCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(SetLueDTCode));
        }


        private void BtnBOQDocNo_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    var f = new FrmSOContractDlg(this, Sm.GetLue(LueCtCode));
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.ShowDialog();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnCustomerShipAddress_Click_1(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmSOContractDlg2(this, Sm.GetLue(LueCtCode), Sm.GetValue("Select CtContactPersonName From tblBOQhdr Where DocNo ='" + TxtBOQDocNo.Text + "'")));
        }

        private void BtnCtShippingAddress_Click_1(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

        private void LueSPCode_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LueShpMCode_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(SetLueDTCode));
        }

        private void LueCtCode_EditValueChanged_1(object sender, EventArgs e)
        {
            try
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                if (Sm.GetLue(LueCtCode).Length == 0)
                {
                    LueCustomerContactperson.EditValue = null;
                    Sm.SetControlReadOnly(LueCustomerContactperson, true);
                }
                else
                {
                    SetLueCtPersonCode(ref LueCustomerContactperson, Sm.GetLue(LueCtCode), string.Empty);
                    Sm.SetControlReadOnly(LueCustomerContactperson, false);
                }

                TxtBOQDocNo.EditValue = null;
                ClearData2();
                Sm.ClearGrd(Grd1, true);
                var CtCode = Sm.GetLue(LueCtCode);
                if (CtCode.Length != 0 && BtnSave.Enabled)
                {
                    SetLueSPCode(ref LueSPCode);
                    SetLueDTCode(ref LueShpMCode);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueStatus_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        #endregion

        #endregion

    }
}
