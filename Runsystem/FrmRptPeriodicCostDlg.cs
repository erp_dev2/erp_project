﻿#region Update
/*
    18/06/2019 [TKG] parameter document date harus cuma diambil yyyyMMdd
*/ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
#endregion

namespace RunSystem
{
    public partial class FrmRptPeriodicCostDlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptPeriodicCost mFrmParent;
        private string 
            mSQL = string.Empty, 
            mDocDt1 = string.Empty,
            mDocDt2 = string.Empty,
            mAcNo = string.Empty,
            mCCtCode= string.Empty,
            mCCCode= string.Empty
            ;

        #endregion

        #region Constructor

        public FrmRptPeriodicCostDlg(FrmRptPeriodicCost FrmParent, String CCCode, String CCtCode, String DocDt1, String DocDt2)
        {
            try
            {
                InitializeComponent();
                mFrmParent = FrmParent;
                mAcNo = Sm.GetValue("Select AcNo From TblCostCategory Where CCCode=@Param1 And CCtCode=@Param2;", CCCode, CCtCode, string.Empty);
                mDocDt1 = DocDt1;
                mDocDt2 = DocDt2;
                mCCtCode= CCtCode;
                mCCCode = CCCode;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            
            base.FrmLoad(sender, e);
            SetGrd(); 
            ShowData();
        }


        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "Month",

                        //1-5
                        "Document#", 
                        "Date",
                        "Department",
                        "Item's Code",
                        "Item's Name",

                        //6-8
                        "Quantity",
                        "Value",
                        "Remark"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7 }, 0);
            Sm.SetGrdProperty(Grd1, true);
        }

        #endregion

        #region Show data

        private void ShowData()
        {
            Cursor.Current = Cursors.WaitCursor;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var l = new List<Result>();
            var Filter = string.Empty;
            int i = 0;

            Sm.CmParam<String>(ref cm, "@DocDt1", mDocDt1.Substring(0, 8));
            Sm.CmParam<String>(ref cm, "@DocDt2", mDocDt2.Substring(0, 8));
            Sm.CmParam<String>(ref cm, "@AcNo", mAcNo);
            Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
            Sm.CmParam<String>(ref cm, "@CCtCode", mCCtCode);

            SQL.AppendLine("Select Mth, DocNo, DocDt, DeptName, ");
            SQL.AppendLine("ItCode, ItName, Qty, Value, Remark ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select SubString(E.DocDt, 5, 2) As Mth, ");
            SQL.AppendLine("    A.DocNo, E.DocDt, H.DeptName, ");
            SQL.AppendLine("    B.ItCode, G.ItName, B.Qty, (B.Qty*F.ExcRate*F.UPrice) As Value, A.Remark ");
            SQL.AppendLine("    From TblDODeptHdr A ");
            SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.AcNo Is Not Null And B.AcNo=@AcNo ");
            SQL.AppendLine("    Inner Join TblJournalHdr E On A.JournalDocNo=E.DocNo "); 
            SQL.AppendLine("    Inner Join TblStockPrice F On B.Source=F.Source ");
            SQL.AppendLine("    Inner Join TblItem G On B.ItCode=G.ItCode ");
            SQL.AppendLine("    Inner Join TblDepartment H On A.DeptCode=H.DeptCode ");
            SQL.AppendLine("    Where A.JournalDocNo Is Not Null ");
            SQL.AppendLine("    And A.CCCode=@CCCode ");
            SQL.AppendLine("    And A.JournalDocNo In ( ");
            SQL.AppendLine("        Select DocNo from TblJournalHdr T ");
            SQL.AppendLine("        Where (T.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        And Exists(Select 1 From TblJournalDtl Where DocNo=T.DocNo And AcNo=@AcNo) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select SubString(A.DocDt, 5, 2) As Mth, ");
            SQL.AppendLine("    'Miscellaneous Transactions' As DocNo, A.DocDt, Null As DeptName, ");
            SQL.AppendLine("    Null As ItCode, Null As ItName, 0.00 As Qty, ");
            SQL.AppendLine("    Case C.AcType  ");
            SQL.AppendLine("        When 'D' Then B.DAmt-B.CAmt ");
            SQL.AppendLine("        When 'C' Then B.CAmt-B.DAmt ");
            SQL.AppendLine("    End As Value, ");
            SQL.AppendLine("    JnDesc As Remark ");
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B On A.DocNo=B.DocNo And B.AcNo=@AcNo ");
            SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo ");
            SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And A.DocNo Not In ( ");
            SQL.AppendLine("        Select T1.DocNo ");
            SQL.AppendLine("        From TblJournalHdr T1 ");
            SQL.AppendLine("        Inner Join TblDODeptHdr T2 On T2.JournalDocNo Is Not Null And T1.DocNo=T2.JournalDocNo And T2.CCCode=@CCCode ");
            SQL.AppendLine("        Inner Join TblDODeptDtl T3 On T2.DocNo=T3.DocNo And T3.CancelInd='N' And T3.AcNo Is Not Null And T3.AcNo=@AcNo ");
            SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") T Order By T.Mth, T.DocDt, T.DocNo; ");

            try
            {
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SQL.ToString(),
                        new string[]
                        {
                            //0
                            "Mth",

                            //1-5
                            "DocNo", "DocDt", "DeptName", "ItCode", "ItName",

                            //6-8
                            "Qty", "Value", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                        }, false, false, false, true
                    );
                Grd1.GroupObject.Add(0);
                Grd1.GroupObject.Add(1);
                Grd1.Group();
                Grd1.Rows.CollapseAll();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7 });
                Grd1.Cols.AutoWidth();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void GetJournalDocNo(ref List<Result> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocDt1", mDocDt1);
            Sm.CmParam<String>(ref cm, "@DocDt2", mDocDt2);
            Sm.CmParam<String>(ref cm, "@AcNo", mAcNo);
            Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
            Sm.CmParam<String>(ref cm, "@CCtCode", mCCtCode);

            SQL.AppendLine("Select Distinct E.DocNo ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblItemCostCategory C On B.ItCode=C.ItCode And A.CCCode=C.CCCode ");
            SQL.AppendLine("Inner Join TblCostCategory D On C.CCtCode=D.CCtCode And D.AcNo=@AcNo And D.CCCode=@CCCode And D.CCtCode=@CCtCode ");
            SQL.AppendLine("Inner Join TblJournalHdr E On A.JournalDocNo=E.DocNo And (E.DocDt Between @DocDt1 And @DocDt2) And Exists(Select 1 From TblJournalDtl Where DocNo=E.DocNo And AcNo=@AcNo);");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result(){ DocNo = Sm.DrStr(dr, c[0]) });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string DocNo { get; set; }
        }

        #endregion
    }
}
