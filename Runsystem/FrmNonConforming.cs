﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmNonConforming : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmNonConformingFind FrmFind;

        #endregion

        #region Constructor

        public FrmNonConforming(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
            SetGrd1();
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }



        #endregion

        #region Standard Method

        #region Set Grid
        private void SetGrd1()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "",
                        //1-4
                        "Item"+Environment.NewLine+"Code",
                        "Item"+Environment.NewLine+"Name",
                        "Quantity",
                        "Remark"
                    },
                    new int[] 
                    {
                        20,
                        100, 100, 
                        100, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }


        #endregion

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtPic, TxtShopFloor, TxtShopFloorDno, ChkCancelInd, MeeRemark
                    }, true);
                    Grd1.ReadOnly = true; ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         DteDocDt, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtDocNo, true);
                    Grd1.ReadOnly = true;
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, DteDocDt, TxtPic, TxtShopFloor, TxtShopFloorDno, MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmNonConformingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            Sm.SetDteCurrentDate(DteDocDt);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method
        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 )
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmNonConformingDlg3(this));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 )
                Sm.FormShowDialog(new FrmNonConformingDlg3(this));
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "NonConforming", "TblNonConformingHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveNCHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveNCDtl(DocNo, Row));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document date") ||
                Sm.IsTxtEmpty(TxtPic, "PIC", false) ||
                Sm.IsTxtEmpty(TxtShopFloor, "ShopFloor Control", false) ||
                Sm.IsTxtEmpty(TxtShopFloorDno, "ShopFloor Control Dno", false) ||
                IsGrdEmpty()||
                IsGrdValueNotValid()||
                IsShopFloorProcessedAlreadyYes();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 3, true, "Quantity should be greater than 0.")) return true;
            }
            return false;
        }


        private MySqlCommand SaveNCHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblNonConformingHdr(DocNo, DocDt, PIC, ShopFloorControlDocNo, ShopFloorControlDNo, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @PIC, @ShopFloorControlDocNo, @ShopFloorControlDNo, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            string PIC = Sm.GetValue("Select EmpCode From TblEmployee Where EmpName = '" + TxtPic.Text + "' ");
            Sm.CmParam<String>(ref cm, "@PIC", PIC);
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", TxtShopFloor.Text);
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDNo", TxtShopFloorDno.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveNCDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblNonConformingDtl(DocNo, DNo, " +
                "ItCode, Qty, Remark, CreateBy, CreateDt) " +
                "Values(@DocNo, @DNo, " +
                "@ItCode, @Qty, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 2));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data


        private void CancelData()
        {
            try
            {
                string DocNo = TxtDocNo.Text;
                if (ChkCancelInd.Checked == true)
                {
                    if (IsCancelledProcessedAlreadyYes(DocNo)) return;
                    Cursor.Current = Cursors.WaitCursor;

                   var cml = new List<MySqlCommand>();
                   cml.Add(CancelInd(DocNo));

                   Sm.ExecCommands(cml);

                    ShowData(DocNo);
                }
                else
                {
                    if (IsCancelledProcessedAlreadyYes(DocNo)) return;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private MySqlCommand CancelInd(string DocNo)
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblNonConformingHdr Set ");
            SQL.AppendLine("CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsCancelledProcessedAlreadyYes(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblNonConformingHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd = 'Y' ");
            SQL.AppendLine("Limit 1 ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is processed already.");
                ChkCancelInd.Checked = true;
                return true;
            }
            return false;
        }

        private bool IsShopFloorProcessedAlreadyYes()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ShopFloorControlDocNo, ShopFloorControlDNo From TblNonConformingHdr ");
            SQL.AppendLine("Where ShopFloorControlDocNo=@ShopDocNo And ShopFloorControlDNo = @ShopDNo ");
            SQL.AppendLine("Limit 1 ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ShopDocNo", TxtShopFloor.Text);
            Sm.CmParam<String>(ref cm, "@ShopDNo", TxtShopFloorDno.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document ShopFloor Control is processed already.");
                return true;
            }
            return false;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowNCHdr(DocNo);
                ShowNCDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowNCHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.CancelInd, " +
                    "B.EmpName, A.ShopFloorControlDocNo, A.ShopFloorControlDNo, A.Remark " +
                    "From TblNonConformingHdr A "+ 
                    "Inner Join TblEmployee B On A.PIC = B.EmpCode Where A.DocNo=@DocNo ",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "CancelInd", "EmpName", "ShopFloorControlDocNo",
                        "ShopFloorControlDNo", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtPic.EditValue = Sm.DrStr(dr, c[3]);
                        TxtShopFloor.EditValue = Sm.DrStr(dr, c[4]);
                        TxtShopFloorDno.EditValue = Sm.DrStr(dr, c[5]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        private void ShowNCDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                "Select A.ItCode, B.ItName, A.Qty, A.Remark " +
                "From TblNonConformingDtl A " +
                "Inner Join TblItem B On A.ItCode = B.ItCode Where A.DocNo = '"+DocNo+"' ",
                    new string[] 
                    { 
                        //0
                        "ItCode", 
                        //1-3
                        "ItName", "Qty", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    }, false, false, true, false
            );
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method
        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 1)+
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }
        #endregion

        #endregion

        #region Event
        private void BtnPic_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmNonConformingDlg(this));
        }


        private void BtnShopFloor_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmNonConformingDlg2(this));
        }
        #endregion

    }
}
