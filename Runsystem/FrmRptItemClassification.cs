﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptItemClassification : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty,
            mAccessInd = string.Empty, 
            mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptItemClassification(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                string CurrentDateTime = Sm.ServerCurrentDateTime();

                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                
                Sl.SetLueSPCode(ref LueSalesPerson);
                SetGrd();
                SetSQL();

                iGCellStyle myPercentBarStyle = new iGCellStyle();
                myPercentBarStyle.CustomDrawFlags = TenTec.Windows.iGridLib.iGCustomDrawFlags.Foreground;
                myPercentBarStyle.Flags = ((TenTec.Windows.iGridLib.iGCellFlags)((TenTec.Windows.iGridLib.iGCellFlags.DisplayText | TenTec.Windows.iGridLib.iGCellFlags.DisplayImage)));
                Grd1.Cols[10].CellStyle = myPercentBarStyle;
                Grd1.Cols[13].CellStyle = myPercentBarStyle;

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Y.SPCode, Y.SPname, Y.Yr, Y.Mth, Y.Ind, SUM(Y.AMT) As Amt1, SUM(Y.Amt2) As AMt2, ifnull(Y2.Amt, 0) As AmtTarget, ");
            SQL.AppendLine("ifNUll(Y3.FastMoving, 0) As Fastmoving, ifnull(Y4.SlowMoving, 0) SlowMoving ");
            SQL.AppendLine("From ( ");
            //sales invoice cbd
	        SQL.AppendLine("    Select X1.DOcNO, X1.DocDt, X1.SPCode,  X1.SPname, X1.Yr, X1.Mth, X1.AMT, X1.Amt2, 'CBD' As Ind  ");
	        SQL.AppendLine("    from (  ");
		    SQL.AppendLine("        Select A.DocNo, A.DocDt,  ifnull(D.SpCode, B.SpCode) As SpCode, ifNull(A.Salesname, C.SpName) As SpName ,  ");
		    SQL.AppendLine("        Left(A.DocDt, 4) As Yr, Substring(A.DocDt, 5, 2) As Mth, A.Amt, A.Amt-(ifnull(E.DAmt,0)+ ifnull(E.CAmt, 0)) As Amt2   ");
		    SQL.AppendLine("        From TblsalesInvoicehdr A ");
		    SQL.AppendLine("        Inner Join TblSOhdr B On A.SODocNo = B.DocNo ");
		    SQL.AppendLine("        Left Join TblsalesPerson C On B.SpCode = C.SpCode ");
		    SQL.AppendLine("        Left Join TblsalesPerson D On A.salesname = D.SpName ");
		    SQL.AppendLine("        Left Join TblSalesInvoiceDtl2 E On A.DocNo = E.DocNo And E.OptAcDesc  = '1' ");
		    SQL.AppendLine("        Where A.CancelInd = 'N' And A.SoDocno is not null And left(A.DocDt, 6) = @MonthFilter ");
	        SQL.AppendLine("    )X1 ");
	        SQL.AppendLine("    Union All ");
            //sales invoice biasa lokal
	        SQL.AppendLine("    Select X1.DOcNO, X1.DocDt, X1.SPCode,  X1.SPname, X1.Yr, X1.Mth, X1.AMT, X1.Amt2, 'TOP' As Ind-- , ifnull(X2.Amt, 0) AmtTarget  ");
	        SQL.AppendLine("    from (  ");
		    SQL.AppendLine("        Select Distinct ");
		    SQL.AppendLine("        A.DocNo, A.DocDt,  ifnull(I.SpCode, G.SpCode) As SpCode, ifNull(A.Salesname, H.SpName) As SpName , ");
		    SQL.AppendLine("        Left(A.DocDt, 4) As Yr, Substring(A.DocDt, 5, 2) As Mth, A.Amt, A.Amt-(ifnull(J.DAmt,0)+ ifnull(J.CAmt, 0)) As Amt2   ");
		    SQL.AppendLine("        From TblsalesInvoicehdr A ");
		    SQL.AppendLine("        Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo  ");
		    SQL.AppendLine("        Inner Join TblDOCt2hdr C On B.DOCtDocNo = C.DocnO ");
		    SQL.AppendLine("        Inner Join TblDoCt2Dtl2 D On B.DOCtDocNo = D.Docno And B.DOCtDno = D.Dno ");
		    SQL.AppendLine("        Inner Join TblDrHdr E On C.DRDocno = E.DocNo And C.DRDocNo Is not null ");
		    SQL.AppendLine("        Inner Join TblDRDtl F On C.DRDocno = F.DocNo And D.DrDno = F.Dno ");
		    SQL.AppendLine("        Inner Join TblSOHdr G On F.SODocNo=G.DocNo  And G.CancelInd = 'N' ");
		    SQL.AppendLine("        Left Join TblsalesPerson H On G.SpCode = H.SpCode ");
		    SQL.AppendLine("        Left Join TblsalesPerson I On A.salesname = I.SpName ");
		    SQL.AppendLine("        Left Join TblSalesInvoiceDtl2 J On A.DocNo = J.DocNo And J.OptAcDesc  = '1' ");
            SQL.AppendLine("        Where A.CancelInd = 'N' And A.SoDocno is null And left(A.DocDt, 6) =  @MonthFilter ");
	        SQL.AppendLine("    )X1 ");
	        SQL.AppendLine("    Where X1.Amt2>0 ");
	        SQL.AppendLine("    UNION All ");
            //sales invoice biasa  export
	        SQL.AppendLine("    Select X1.DOcNO, X1.DocDt, X1.SPCode,  X1.SPname, X1.Yr, X1.Mth, X1.AMT, X1.Amt2, 'EXP' As Ind -- , ifnull(X2.Amt, 0) AmtTarget  ");
	        SQL.AppendLine("    from (  ");
		    SQL.AppendLine("        Select Distinct ");
		    SQL.AppendLine("        A.DocNo, A.DocDt,  ifnull(I.SpCode, G.SpCode) As SpCode, ifNull(A.Salesname, H.SpName) As SpName , ");
		    SQL.AppendLine("        Left(A.DocDt, 4) As Yr, Substring(A.DocDt, 5, 2) As Mth, A.Amt, A.Amt-(ifnull(J.DAmt,0)+ ifnull(J.CAmt, 0)) As Amt2   ");
		    SQL.AppendLine("        From TblsalesInvoicehdr A ");
		    SQL.AppendLine("        Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo  ");
		    SQL.AppendLine("        Inner Join TblDOCt2hdr C On B.DOCtDocNo = C.DocnO  And C.PLDocNo Is not null ");
		    SQL.AppendLine("        Inner Join TblDoCt2Dtl3 D On B.DOCtDocNo = D.Docno And B.DOCtDno = D.Dno ");
		    SQL.AppendLine("        Inner Join  ");
		    SQL.AppendLine("        ( ");
			SQL.AppendLine("            Select DocNo, SODOcNO From TblPLDtl ");
			SQL.AppendLine("            Group By DocNO, SoDocNo ");
		    SQL.AppendLine("        )E On C.PlDocNO = E.DocNO ");
		    SQL.AppendLine("        Inner Join TblSOHdr G On E.SODocNo=G.DocNo  And G.CancelInd = 'N' ");
		    SQL.AppendLine("        Left Join TblsalesPerson H On G.SpCode = H.SpCode ");
		    SQL.AppendLine("        Left Join TblsalesPerson I On A.salesname = I.SpName ");
		    SQL.AppendLine("        Left Join TblSalesInvoiceDtl2 J On A.DocNo = J.DocNo And J.OptAcDesc  = '1' ");
            SQL.AppendLine("        Where A.CancelInd = 'N' And A.SoDocno is null And left(A.DocDt, 6) =  @MonthFilter ");
	        SQL.AppendLine("    )X1 ");
	        SQL.AppendLine("    Where X1.Amt2>0 ");
            //ambil dari POS
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    Select X1.TrnNo, X1.BSDate, X1.SpCode, X1.SPName, X1.Yr, X1.mth, X1.PayAmtNett, X1.PayAmtNett2, X1.TypeDoc ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.TrnNo, A.BSDate, C.SpCode, C.Spname, Left(A.BSDate, 4) Yr, Substring(A.BSDate, 5, 2) Mth, B.PayAmtNett, B.PayAmtNett As PayAmtNett2, 'POS' As TypeDoc   ");
	        SQL.AppendLine("        From TblPostrnhdr A  ");
	        SQL.AppendLine("        Inner Join TblPosTrnPay B On A.TrnNo=B.TrnNo And A.BsDate = B.BSDate And A.PosNo = B.PosNo ");
	        SQL.AppendLine("        Inner Join TblSalesPerson C On A.UserCode = C.UserCode ");
	        SQL.AppendLine("    )X1 Where X1.PayAmtNett >0 ");
            SQL.AppendLine(")Y "); 
            SQL.AppendLine("Left Join TblSalestarget Y2 On Y.SpCode = Y2.SpCode And Y.Yr = Y2.Yr And Y.Mth = Y2.Mth And Y2.CancelInd = 'N' ");
            //join ke Fast moving
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
 	        SQL.AppendLine("    Select D.SPCode, SUM(B.Qty * UPriceAfterTax) As FastMoving   ");
	        SQL.AppendLine("    From TblsalesInvoicehdr A  ");
	        SQL.AppendLine("    Inner Join tblsalesInvoiceDtl B On A.DocNo = B.DocNO ");
	        SQL.AppendLine("    Inner Join  ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select B.ItCode  ");
            SQL.AppendLine("        From tblItemClassificationHdr A  ");
            SQL.AppendLine("        Inner Join tblItemClassificationDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("        Inner Join   ");
            SQL.AppendLine("        (  ");
	        SQL.AppendLine("            Select B.ItCode,   ");
	        SQL.AppendLine("            Max(Concat(A.DocDt, A.DocNo, B.DNo)) As Key1    ");
	        SQL.AppendLine("            from TblItemClassificationhdr A, TblItemClassificationDtl B  ");
	        SQL.AppendLine("            Where A.DocNo = B.DocNo And A.Status = 'A'  ");
	        SQL.AppendLine("            Group By B.ItCode  ");
            SQL.AppendLine("        )C On Concat(A.DocDt, A.DocNo, B.DNo) = C.Key1  ");
            SQL.AppendLine("        Where B.ItClass = '1'  ");
	        SQL.AppendLine("    )C On B.ItCode = C.itCode ");
	        SQL.AppendLine("    Inner Join TblSalesPerson D On A.SalesName = D.SpName ");
            SQL.AppendLine("    Where A.cancelInd = 'N' And left(A.DocDt, 6) =  @MonthFilter ");
	        SQL.AppendLine("    Group By A.Salesname ");
            SQL.AppendLine(")Y3 On Y.SpCode = Y3.SpCode ");
            //Join ke Slow Moving
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
 	        SQL.AppendLine("    Select D.SPCode, SUM(B.Qty * UPriceAfterTax) As SlowMoving   ");
	        SQL.AppendLine("    From TblsalesInvoicehdr A  ");
	        SQL.AppendLine("    Inner Join tblsalesInvoiceDtl B On A.DocNo = B.DocNO ");
	        SQL.AppendLine("    Inner Join  ");
	        SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select B.ItCode  ");
            SQL.AppendLine("        From tblItemClassificationHdr A  ");
            SQL.AppendLine("        Inner Join tblItemClassificationDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("        Inner Join   ");
            SQL.AppendLine("        (  ");
            SQL.AppendLine("            Select B.ItCode,   ");
            SQL.AppendLine("            Max(Concat(A.DocDt, A.DocNo, B.DNo)) As Key1    ");
            SQL.AppendLine("            from TblItemClassificationhdr A, TblItemClassificationDtl B  ");
            SQL.AppendLine("            Where A.DocNo = B.DocNo And A.Status = 'A'  ");
            SQL.AppendLine("            Group By B.ItCode  ");
            SQL.AppendLine("        )C On Concat(A.DocDt, A.DocNo, B.DNo) = C.Key1  ");
            SQL.AppendLine("        Where B.ItClass = '2'  ");
	        SQL.AppendLine("    )C On B.ItCode = C.itCode ");
	        SQL.AppendLine("    Inner Join TblSalesPerson D On A.SalesName = D.SpName ");
            SQL.AppendLine("    Where A.cancelInd = 'N' And left(A.DocDt, 6) =  @MonthFilter ");
	        SQL.AppendLine("    Group By A.Salesname ");
            SQL.AppendLine(")Y4 On Y.SpCode = Y4.SpCode ");
            SQL.AppendLine("Where Y.Yr = @Yr And Y.Mth = @Mth ");
            //SQL.AppendLine("Group By Y.SPCode,  Y.SPname, Y.Yr, Y.Mth, Y.Ind ");

           

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "SPCode",
                        "Sales Person",
                        "Year",
                        "Month",
                        "Detail",
                        //6-10
                        "Type",
                        "Invoice Amount",
                        "Invoice Amount "+Environment.NewLine+" Without Freight",
                        "Monthly Target Amount",
                        "Prosentase Invoice "+Environment.NewLine+" with Monthly Target",
                        //11-13
                        "Fast Moving Amount",
                        "Slow Moving Amount",
                        "Prosentase Based "+Environment.NewLine+"On Fast And Slow Moving"
                    },
                    new int[] 
                    {
                        //0
                        30,
                        //1-5
                        150, 200, 80, 80, 40,    
                        //6-10
                        120, 150, 150, 150, 150,
                        //11-13
                        150, 150, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] {  7, 8, 9, 10, 11, 12, 13 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 6, 7 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }      

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Month = Sm.GetLue(LueMth);
           
            if (Year == string.Empty && Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month And Year is Empty.");
            }
            else if (Year == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
            }
            else if (Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
            }
            else
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = " And 0=0 ";

                    var cm = new MySqlCommand();

                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSalesPerson), "Y.SPCode", true);
                    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                    Sm.CmParam<String>(ref cm, "@MonthFilter", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));

                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter + " Group By Y.SPCode,  Y.SPname, Y.Yr, Y.Mth ",
                            new string[]
                        {
                            //0
                            "SPCode", 
                            //1-5
                            "SpName", "Yr", "Mth", "Ind", "Amt1", 
                            //6-9
                            "Amt2", "AmtTarget", "FastMoving", "SlowMoving"
                        },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd1.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 5);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 6);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 7);
                                if (Sm.GetGrdDec(Grd1, Row, 9) != 0)
                                {
                                    Grd1.Cells[Row, 10].Value = Convert.ToDouble(Sm.GetGrdDec(Grd1, Row, 8) / Sm.GetGrdDec(Grd1, Row, 9));
                                }
                                else
                                {
                                     Grd1.Cells[Row, 10].Value = Convert.ToDouble(0);
                                }
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 8);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 9);
                                if (Sm.GetGrdDec(Grd1, Row, 8) != 0)
                                {
                                    Grd1.Cells[Row, 13].Value = Convert.ToDouble((Sm.GetGrdDec(Grd1, Row, 11)+Sm.GetGrdDec(Grd1, Row, 12)) / Sm.GetGrdDec(Grd1, Row, 8));
                                }
                                else
                                {
                                    Grd1.Cells[Row, 13].Value = Convert.ToDouble(0);
                                }
                            }, true, false, false, false
                        );
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Sm.FocusGrd(Grd1, 0, 1);
                    Cursor.Current = Cursors.Default;
                }
            }

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void ChkSalesPerson_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Sales Person");
        }

        private void LueSalesPerson_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSalesPerson, new Sm.RefreshLue1(Sl.SetLueSPCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }


        #endregion

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            

          
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
           

        }
       
        private void Grd1_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            if (e.ColIndex == 10 || e.ColIndex == 13)
            {
                object myObjValue = Grd1.Cells[e.RowIndex, e.ColIndex].Value;
                if (myObjValue == null)
                    return;

                Rectangle myBounds = e.Bounds;
                myBounds.Inflate(-2, -2);
                myBounds.Width = myBounds.Width - 1;
                myBounds.Height = myBounds.Height - 1;
                if (myBounds.Width > 0)
                {
                    e.Graphics.FillRectangle(Brushes.Bisque, myBounds);
                    double myValue = (double)myObjValue;
                    int myWidth = (int)(myBounds.Width * myValue);
                    e.Graphics.FillRectangle(Brushes.SandyBrown, myBounds.X, myBounds.Y, myWidth, myBounds.Height);

                    e.Graphics.DrawRectangle(Pens.SaddleBrown, myBounds);

                    StringFormat myStringFormat = new StringFormat();
                    myStringFormat.Alignment = StringAlignment.Center;
                    myStringFormat.LineAlignment = StringAlignment.Center;
                    e.Graphics.DrawString(string.Format("{0:F2}%", myValue * 100), Font, SystemBrushes.ControlText, new RectangleF(myBounds.X, myBounds.Y, myBounds.Width, myBounds.Height), myStringFormat);
                }
            }

        }

        

        #endregion


    }
}
