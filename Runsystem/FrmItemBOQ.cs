﻿#region Update
/*
    18/01/2019 [HAR/VIR] validasi saat ini jika ada itemboqcode, name, parent, level yang sama dia tidak bisa save, 
                      diganti jika ada item boq saja  yang sama dia tdk bisa save
    19/02/2019 [MEY/YK] active indikator diaktifkan
    22/07/2019 [WED/YK] Parent dan Level dibuat otomatis
    08/11/2019 [WED/YK] Validasi item tidak bisa non-aktif jika masih digunakan di BOQ, diubah menjadi warning saja
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemBOQ : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmItemBOQFind FrmFind;

        #endregion

        #region Constructor

        public FrmItemBOQ(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length <= 0) this.Text = Sm.GetMenuDesc("FrmItemBOQ");
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
            SetLueItBOQCode(ref LueParent);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItBOQCode,TxtItBOQName, LueParent, TxtLevel, ChkActInd
                    }, true);
                    TxtItBOQCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItBOQCode,TxtItBOQName
                    }, false);
                    TxtItBOQCode.Focus();
                    ChkActInd.Checked = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtItBOQCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtItBOQName, ChkActInd
                    }, false);
                    TxtItBOQName.Focus();
                    ChkActInd.Properties.ReadOnly = false;
                    ChkActInd.Focus();
                    break;
                default:
                    break;
            }
        }
        #endregion

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtItBOQCode,TxtItBOQName, LueParent, TxtLevel
            });
            ChkActInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtLevel }, 2);
        }

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemBOQFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
            TxtLevel.Text = Sm.FormatNum(1, 2);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItBOQCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            //if (Sm.IsTxtEmpty(TxtItBOQCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            //try
            //{
            //    if (IsJournalExisted()) return;
            //    Cursor.Current = Cursors.WaitCursor;

            //    var cml = new List<MySqlCommand>();

            //    cml.Add(DeleteCOA(TxtItBOQCode.Text));

            //    Sm.ExecCommands(cml);

            //    BtnCancelClick(sender, e);

            //}
            //catch (Exception Exc)
            //{
            //    Sm.StdMsg(mMsgType.Warning, Exc.Message);
            //}
            //finally
            //{
            //    Cursor.Current = Cursors.Default;
            //}
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                GenerateLevel();
                if (IsDataNotValid()) return;
                string mBOQDocNo = IsDataStillUsed();

                if (mBOQDocNo.Length > 0)
                {
                    if (Sm.StdMsgYN("Question", "This Item is used in BOQ# " + mBOQDocNo + ". Do you still want to deactivate this item ?") == DialogResult.No) return;
                }

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveItemBOQ());

                Sm.ExecCommands(cml);

                ShowData(TxtItBOQCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ItBOQCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                SetLueItBOQCode(ref LueParent);
                ShowItemBOQ(ItBOQCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowItemBOQ(string ItBOQCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ItBOQCode", ItBOQCode);

            Sm.ShowDataInCtrl(
                ref cm,
                "SELECT ItBOQCode, ItBOQName, ActInd, Parent, Level FROM TblItemBOQ WHERE ItBOQCode=@ItBOQCode;",
                new string[] { "ItBOQCode", "ItBOQName", "ActInd", "Parent", "Level" },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtItBOQCode.EditValue = Sm.DrStr(dr, c[0]);
                    TxtItBOQName.EditValue = Sm.DrStr(dr, c[1]);
                    ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                    Sm.SetLue(LueParent, Sm.DrStr(dr, c[3]));
                    TxtLevel.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 2);
                }, true
            );
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtItBOQCode, "Item BOQ#", false) ||
                Sm.IsTxtEmpty(TxtItBOQName, "Item BOQ Name", false) ||
                Sm.IsTxtEmpty(TxtLevel, "Level", true) ||
                InvalidItBOQCode() ||
                IsItBOQCodeExisted() ||
                //IsDataStillUsed() ||
                IsParentNotExists() ||
                (TxtItBOQCode.Text.Length > 0 && IsDataAlreadyInactived());
        }

        private bool InvalidItBOQCode()
        {
            var mItBOQCode = TxtItBOQCode.Text;
            var mCount = mItBOQCode.Split('.');

            if (mCount[mCount.Count() - 1] == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "There should be a number / character after the last '.' separator");
                TxtItBOQCode.Focus();
                return true;
            }

            return false;
        }

        private bool IsParentNotExists()
        {
            var mItBOQCode = TxtItBOQCode.Text;
            var mCount = mItBOQCode.Split('.');
            var c = mCount.Count();
            if (c > 1)
            {
                var SQL = new StringBuilder();
                string mParent = string.Empty;

                for (int x = 0; x < c - 1; x++)
                {
                    if (mParent.Length > 0) mParent += ".";
                    mParent += mCount[x];
                }

                SQL.AppendLine("Select 1 ");
                SQL.AppendLine("From TblItemBOQ ");
                SQL.AppendLine("Where ItBOQCode = @Param ");
                SQL.AppendLine("And ActInd = 'Y'; ");

                if (!Sm.IsDataExist(SQL.ToString(), mParent))
                {
                    Sm.StdMsg(mMsgType.Warning, "There is no active parent of this item : " + mParent);
                    TxtItBOQCode.Focus();
                    return true;
                }
                
            }

            return false;
        }

        private string IsDataStillUsed()
        {
            string mBOQDocNo = string.Empty;
            if (!ChkActInd.Checked)
            {         
                var SQL = new StringBuilder();
                SQL.AppendLine("Select B.DocNo From (Select * From TblBOQDtl Where RequiredInd = 'Y' And Amt != 0) A  ");
                SQL.AppendLine("Inner Join TblBOQHdr B ON B.DocNo=A.DocNo And B.Status In ('O','A') ");
                SQL.AppendLine("And A.ItBOQCode= @ItBOQCode; ");
                
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ItBOQCode", TxtItBOQCode.Text);
                mBOQDocNo = Sm.GetValue(cm);
                //if (Sm.IsDataExist(cm))
                //{
                //    Sm.StdMsg(mMsgType.Warning, "This Item is used in BOQ# "+DocNo+" ");
                //    return true;
                //}
            }
            return mBOQDocNo;
        }
        private bool IsDataAlreadyInactived()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select * From TblItemBOQ Where ItBOQCode = @Param And ActInd = 'N'; "
            };
            Sm.CmParam<String>(ref cm, "@Param", TxtItBOQCode.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already inactive.");
                return true;
            }
            return false;
        }

        private bool IsItBOQCodeExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT ItBOQCode FROM TblItemBOQ ");
            SQL.AppendLine("WHERE ItBOQCode = @ItBOQCode ");
            //SQL.AppendLine("  AND ItBOQName = @ItBOQName ");
            //SQL.AppendLine("  AND Parent = @Parent ");
            //SQL.AppendLine("  AND Level = @Level ");
            SQL.AppendLine("LIMIT 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItBOQCode", TxtItBOQCode.Text);
            Sm.CmParam<String>(ref cm, "@ItBOQName", TxtItBOQName.Text);
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
            Sm.CmParam<String>(ref cm, "@Level", TxtLevel.Text);

            if (!TxtItBOQCode.Properties.ReadOnly && Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Item BOQ# already existed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveItemBOQ()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT INTO TblItemBOQ(ItBOQCode, ItBOQName, ActInd, Parent, Level, CreateBy, CreateDt) ");
            SQL.AppendLine("VALUES(@ItBOQCode, @ItBOQName, @ActInd, NULL, @Level, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("ON DUPLICATE KEY ");
            SQL.AppendLine("   UPDATE ItBOQName=@ItBOQName, ActInd=@ActInd, Parent=NULL,Level=@Level, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Update TblItemBOQ ");
            SQL.AppendLine("  Set Parent = NULL ");
            SQL.AppendLine("Where ItBOQCode = @ItBOQCode And Level = 1; ");

            SQL.AppendLine("Update TblItemBOQ ");
            SQL.AppendLine("  Set Parent = Substring_Index(ItBOQCode, '.', Level-1) ");
            SQL.AppendLine("Where ItBOQCode = @ItBOQCode And Level > 1; ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@ItBOQCode", TxtItBOQCode.Text);
            Sm.CmParam<String>(ref cm, "@ItBOQName", TxtItBOQName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
            Sm.CmParam<Decimal>(ref cm, "@Level", decimal.Parse(TxtLevel.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Methods

        private void GenerateLevel()
        {
            int Lvl = 1;
            var mItemBOQCode = TxtItBOQCode.Text;
            var countLvl = mItemBOQCode.Split('.');
            Lvl = countLvl.Count() == 0 ? 1 : countLvl.Count();

            TxtLevel.EditValue = Sm.FormatNum(Lvl, 0);
        }

        private void SetLueItBOQCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ItBOQCode As Col1, Concat(ItBOQCode, ' [ ', ItBOQName, ' ]') As Col2 ");
            SQL.AppendLine("From TblItemBOQ ");
            SQL.AppendLine("Order By ItBOQCode; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtItBOQCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItBOQCode);
        }

        private void TxtItBOQName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItBOQName);
        }

        private void TxtLevel_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLevel, 2);
        }

        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue1(SetLueItBOQCode));
                TxtLevel.Text = Sm.FormatNum(1, 2);
                if(Sm.GetLue(LueParent).Length > 0)
                {
                    decimal mLevel = Decimal.Parse(Sm.GetValue("Select (IfNull(Level, 0) + 1) as Level From TblItemBOQ Where ItBOQCode = @Param; ", Sm.GetLue(LueParent)));
                    TxtLevel.Text = Sm.FormatNum(mLevel, 2);
                }
            }
        }

        #endregion

        #endregion

    }
}
