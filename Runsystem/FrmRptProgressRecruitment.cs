﻿#region Update
/*
    09/05/2022 [RIS/HIN] new reporting
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProgressRecruitment : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProgressRecruitment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueDivisionCode(ref LueDiv);
                Sl.SetLueDeptCode(ref LueDept);
                Sl.SetLuePosCode(ref LuePos);
                Sl.SetLueLevelCode(ref LueLvl);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        { 

        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.EmployeeRequestDocNo, A.DocDt, E.DivisionName, F.DeptName, G.PosName, H.LevelName, A.EmpName, D.OptDesc as Progress, B.Date, B.Remark ");
            SQL.AppendLine("FROM tblemployeerecruitment A ");
            SQL.AppendLine("LEFT JOIN tblemployeestagerecruitment B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("LEFT JOIN tblemployeerequest C ON A.EmployeeRequestDocNo = C.DocNo ");
            SQL.AppendLine("LEFT JOIN tbloption D ON B.ProgressCode = D.OptCode AND D.OptCat = 'PersonalInformationProgress' ");
            SQL.AppendLine("LEFT JOIN tbldivision E ON A.DivisionCode = E.DivisionCode ");
            SQL.AppendLine("LEFT JOIN tbldepartment F ON A.DeptCode = F.DeptCode ");
            SQL.AppendLine("LEFT JOIN tblposition G ON A.PosCode = G.PosCode ");
            SQL.AppendLine("LEFT JOIN tbllevelhdr H ON C.LevelCode = H.LevelCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Personal Information#", 
                    "Employee Request#",
                    "Date",
                    "Division",
                    "Department",

                    //6-10
                    "Position",
                    "Level",
                    "Employee Name",
                    "Progress",
                    "Date",
                    
                    //11-1
                    "Remark"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 180, 100, 180, 180, 
                    
                    //6-10
                    180, 180, 180, 180, 100,  
                    
                    //11-13
                    200
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 3, 10 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpName", "A.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDiv), "A.DivisionCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDept), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePos), "A.PosCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLvl), "C.LevelCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " ORDER BY A.DocDt DESC, A.DocNo, B.Date;",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "EmployeeRequestDocno", "DocDt", "DivisionName", "DeptName", "PosName", 

                            //6-10
                            "LevelName", "EmpName", "Progress", "Date", "Remark", 
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void LueDiv_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDiv, new Sm.RefreshLue1(Sl.SetLueDivisionCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueDept_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDept, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LuePos_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePos, new Sm.RefreshLue1(Sl.SetLuePosCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueLvl_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLvl, new Sm.RefreshLue1(Sl.SetLueLevelCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDiv_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Division");
        }

        private void ChkDept_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkPos_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Position");
        }

        private void ChkLvl_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Level");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        #endregion

        #endregion
    }
}
