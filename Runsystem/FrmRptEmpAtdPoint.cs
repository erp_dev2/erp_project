﻿#region Update
    //31/01/2018 [HAR] Bug fixing filter department 
    //06/06/2018 [HAR] tambah kolom penambah point leave  
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpAtdPoint : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
          private bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false;
        #endregion

        #region Constructor

        public FrmRptEmpAtdPoint(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueYr(LueYr, "");
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.EmpName, C.Deptname, D.posname, E.Sitename, B.Yr, ");
            SQL.AppendLine("(ifnull(B.Point, 0)+ifnull(H.AddLeavePoint, 0)) As Point, ifNUll(F.Wd, 0) As Wd, ifnull(G.LeavePoint, 0) As LeavePoint,  ifnull(H.AddLeavePoint, 0) AddleavePoint, ");
            SQL.AppendLine("(ifNUll(F.Wd, 0)-ifnull(G.LeavePoint, 0)+((ifnull(B.Point, 0)+ifnull(H.AddLeavePoint, 0)) - ifnull(G.LeavePoint, 0))) / ifNUll(F.Wd, 0) As PTK  ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select B.EmpCode, A.Yr, A.Point From tblEmpAtdPointHdr A ");
	        SQL.AppendLine("    Inner Join TblEmpAtdPointDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Where A.CancelInd = 'N' And A.Yr = @Yr  ");
            SQL.AppendLine(")B on A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Inner Join TblDepartment C on A.DeptCode = C.DeptCode ");
            SQL.AppendLine("left Join tblPosition D on  A.posCode  =D.posCode ");
            SQL.AppendLine("Left Join TblSite E On A.SiteCode = E.SiteCode ");
            SQL.AppendLine("left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    select EmpCode, Sum(workingday) As Wd ");
	        SQL.AppendLine("    From tblpayrollprocess1 ");
            SQL.AppendLine("    Where Left(payrunCode, 4)=@Yr ");
	        SQL.AppendLine("    Group By EmpCode ");
            SQL.AppendLine(")F On A.EmpCode = F.EmpCode  ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Y.EmpCode, SUM(LeavePoint) As LeavePoint");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.EmpCode, A.leaveCode,(Count(B.Docno)* C.Point)  LeavePoint ");
	        SQL.AppendLine("        From TblEmpLeaveHdr A ");
	        SQL.AppendLine("        Inner Join TblEmpleaveDtl B On A.Docno  = B.DocnO ");
	        SQL.AppendLine("        Inner Join tblleave C On A.LeaveCode = C.leaveCode ");
            SQL.AppendLine("        Where A.CancelInd = 'N' And A.Status = 'A' And Left(B.leavedt, 4)=@Yr ");
	        SQL.AppendLine("        Group by A.EmpCode, A.leaveCode ");
            SQL.AppendLine("    )Y Group BY EmpCode ");
            SQL.AppendLine(")G ON A.EmpCode = G.EmpCode  ");

            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Y.EmpCode, SUM(LeavePoint) As AddLeavePoint  ");
            SQL.AppendLine("    From (   ");
            SQL.AppendLine("        Select A.EmpCode, A.leaveCode,(Count(B.Docno)* C.Point)  LeavePoint   ");
            SQL.AppendLine("        From TblEmpLeaveHdr A   ");
            SQL.AppendLine("        Inner Join TblEmpleaveDtl B On A.Docno  = B.DocnO   ");
            SQL.AppendLine("        Inner Join tblleave C On A.LeaveCode = C.leaveCode   ");
            SQL.AppendLine("        Where A.CancelInd = 'N' And A.Status = 'A' And Left(B.leavedt, 4)=@Yr   ");
            SQL.AppendLine("        And Find_In_Set(   ");
            SQL.AppendLine("        IfNull(A.LeaveCode, ''),   ");
            SQL.AppendLine("            (Select ParValue From TblParameter Where ParCode Is Not Null And ParCode='LeaveCodeAdditionalPoint')   ");
            SQL.AppendLine("        )   ");
            SQL.AppendLine("        Group by A.EmpCode, A.leaveCode   ");
            SQL.AppendLine("    )Y Group BY EmpCode   ");
            SQL.AppendLine(")H ON A.EmpCode = H.EmpCode  ");

            SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And ResignDt>@CurrentDate)) ");

            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And A.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Employee"+Environment.NewLine+"Code",
                        "",
                        "Old"+Environment.NewLine+"Code",
                        "Employee",
                        "Department",
                        
                        //6-10
                        "Position",
                        "Site",
                        "Period",
                        "Yearly"+Environment.NewLine+"Point",
                        "Working"+Environment.NewLine+"Days",
                        
                        //11-14
                        "Leave"+Environment.NewLine+"Point",
                        "Additional"+Environment.NewLine+"Point",
                        "",
                        "Percentage",
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        120, 20, 120, 200, 150, 
                        
                        //6-10
                        150, 150, 100,  120, 120,  
                        
                        //11-13
                        120, 120, 20, 120, 
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14 });
            Sm.GrdFormatDec(Grd1, new int[] { 8,9,10,11,12,14 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 2, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                if (Sm.IsLueEmpty(LueYr, "Year"))
                {
                    return;
                }

                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode); 
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpCodeOld", "A.EmpName" });
                

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL.ToString() + Filter + " Order By A.EmpCode, A.EmpName;",
                    new string[] 
                        { 
                            //0
                            "EmpCode",  

                            //1-5
                            "EmpCodeOld", "EmpName", "Deptname", "posname", "Sitename",  
                            
                            //6-10
                            "Yr", "Point", "Wd", "LeavePoint", "AddleavePoint", 
                            //11
                            "PTK",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            if (Sm.DrDec(dr, 11) >= 0)
                            {
                                Grd1.Cells[Row, 14].Value = Sm.FormatNum(Sm.DrDec(dr, 11)*100, 0); 
                            }
                        }, false, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 13 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRptEmpAtdPointDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 1));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 13 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmRptEmpAtdPointDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 1));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var c = new FrmEmployee(mMenuCode);
                c.Tag = mMenuCode;
                c.WindowState = FormWindowState.Normal;
                c.StartPosition = FormStartPosition.CenterScreen;
                c.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                c.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method


        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }


        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion
        
    }
}
