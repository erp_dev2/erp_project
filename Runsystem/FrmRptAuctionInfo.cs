﻿#region Update
/*
    13/08/2019 [TKG] New reporting
    18/06/2019 [TKG] Old contract jadi carry over
    13/03/2020 [WED/YK] perubahan sumber data dan kolom
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAuctionInfo : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mSiteCodeForAuctionInfo = string.Empty,
            mPhaseCodeForUnprocessedAmt = string.Empty,
            mPhaseCodeForPQProcessedAmt = string.Empty,
            mPhaseCodeForAuctionProcessedAmt = string.Empty,
            mPhaseCodeForWinAmt = string.Empty;

        #endregion

        #region Constructor

        public FrmRptAuctionInfo(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mSiteCodeForAuctionInfo = Sm.GetParameter("SiteCodeForAuctionInfo");
            mPhaseCodeForUnprocessedAmt = Sm.GetParameter("PhaseCodeForUnprocessedAmt");
            mPhaseCodeForPQProcessedAmt = Sm.GetParameter("PhaseCodeForPQProcessedAmt");
            mPhaseCodeForAuctionProcessedAmt = Sm.GetParameter("PhaseCodeForAuctionProcessedAmt");
            mPhaseCodeForWinAmt = Sm.GetParameter("PhaseCodeForWinAmt");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.SiteCode, A.SiteName, IfNull(B.OCAmt, 0.00) OCAmt, IfNull(B.NCAmt, 0.00) NCAmt, ");
            SQL.AppendLine("IfNull(B.CPQ1Amt, 0.00) CPQ1Amt, IfNull(B.CPQ2Amt, 0.00) CPQ2Amt, IfNull(B.CPQ3Amt, 0.00) CPQ3Amt, ");
            SQL.AppendLine("IfNull(B.CPQ4Amt, 0.00) CPQ4Amt, IfNull(C.Amt, 0.00) SOCAmt, ");
            SQL.AppendLine("IfNull(D.Amt, 0.00) LOPAmt, IfNull(E.UnprocessedAmt, 0.00) UnprocessedAmt, IfNull(E.PQAmt, 0.00) PQAmt, ");
            SQL.AppendLine("IfNull(E.AuctionAmt, 0.00) AuctionAmt, IfNull(E.WinAmt, 0.00) WinAmt, IfNull(F.Amt, 0.00) LoseAmt ");
            SQL.AppendLine("FROM -- site ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT SiteCode, SiteName ");
            SQL.AppendLine("    FROM TblSite ");
            SQL.AppendLine("    WHERE FIND_IN_SET(SiteCode, @SiteCodeForAuctionInfo) ");
            SQL.AppendLine("    AND ActInd = 'Y' ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("LEFT JOIN -- auction info (old contract, new contract, contract plan per quartal) ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT T.SiteCode, SUM(T.OCAmt) OCAmt, SUM(T.NCAmt) NCAmt, SUM(T.CPQ1Amt) CPQ1Amt, ");
            SQL.AppendLine("    SUM(T.CPQ2Amt) CPQ2Amt, SUM(T.CPQ3Amt) CPQ3Amt, SUM(T.CPQ4Amt) CPQ4Amt ");
            SQL.AppendLine("    FROM ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T2.SiteCode, T2.Amt OCAmt, ");
            SQL.AppendLine("        0.00 NCAmt, 0.00 CPQ1Amt, 0.00 CPQ2Amt, 0.00 CPQ3Amt, 0.00 CPQ4Amt ");
            SQL.AppendLine("        FROM TblAuctionHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblAuctionDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            AND T1.CancelInd = 'N' ");
            SQL.AppendLine("            AND T1.Yr = LEFT(@DocDt1, 4) ");
            SQL.AppendLine("            AND T2.AuctionType = 'OC' ");
            SQL.AppendLine("        INNER JOIN TblOption T3 ON T3.Optcat = 'AuctionType' AND T2.Auctiontype = T3.OptCode ");
            SQL.AppendLine("        UNION ALL ");
            SQL.AppendLine("        SELECT T2.SiteCode, 0.00 OCAmt, ");
            SQL.AppendLine("        T2.Amt NCAmt, 0.00 CPQ1Amt, 0.00 CPQ2Amt, 0.00 CPQ3Amt, 0.00 CPQ4Amt ");
            SQL.AppendLine("        FROM TblAuctionHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblAuctionDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            AND T1.CancelInd = 'N' ");
            SQL.AppendLine("            AND T1.Yr = LEFT(@DocDt1, 4) ");
            SQL.AppendLine("            AND T2.AuctionType = 'NC' ");
            SQL.AppendLine("        INNER JOIN TblOption T3 ON T3.Optcat = 'AuctionType' AND T2.Auctiontype = T3.OptCode ");
            SQL.AppendLine("        UNION ALL ");
            SQL.AppendLine("        SELECT T2.SiteCode, 0.00 OCAmt, ");
            SQL.AppendLine("        0.00 NCAmt, T2.Amt CPQ1Amt, 0.00 CPQ2Amt, 0.00 CPQ3Amt, 0.00 CPQ4Amt ");
            SQL.AppendLine("        FROM TblAuctionHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblAuctionDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            AND T1.CancelInd = 'N' ");
            SQL.AppendLine("            AND T1.Yr = LEFT(@DocDt1, 4) ");
            SQL.AppendLine("            AND T2.AuctionType = 'CPQ1' ");
            SQL.AppendLine("        INNER JOIN TblOption T3 ON T3.Optcat = 'AuctionType' AND T2.Auctiontype = T3.OptCode ");
            SQL.AppendLine("        UNION ALL ");
            SQL.AppendLine("        SELECT T2.SiteCode, 0.00 OCAmt, ");
            SQL.AppendLine("        0.00 NCAmt, 0.00 CPQ1Amt, T2.Amt CPQ2Amt, 0.00 CPQ3Amt, 0.00 CPQ4Amt ");
            SQL.AppendLine("        FROM TblAuctionHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblAuctionDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            AND T1.CancelInd = 'N' ");
            SQL.AppendLine("            AND T1.Yr = LEFT(@DocDt1, 4) ");
            SQL.AppendLine("            AND T2.AuctionType = 'CPQ2' ");
            SQL.AppendLine("        INNER JOIN TblOption T3 ON T3.Optcat = 'AuctionType' AND T2.Auctiontype = T3.OptCode ");
            SQL.AppendLine("        UNION ALL ");
            SQL.AppendLine("        SELECT T2.SiteCode, 0.00 OCAmt, ");
            SQL.AppendLine("        0.00 NCAmt, 0.00 CPQ1Amt, 0.00 CPQ2Amt, T2.Amt CPQ3Amt, 0.00 CPQ4Amt ");
            SQL.AppendLine("        FROM TblAuctionHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblAuctionDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            AND T1.CancelInd = 'N' ");
            SQL.AppendLine("            AND T1.Yr = LEFT(@DocDt1, 4) ");
            SQL.AppendLine("            AND T2.AuctionType = 'CPQ3' ");
            SQL.AppendLine("        INNER JOIN TblOption T3 ON T3.Optcat = 'AuctionType' AND T2.Auctiontype = T3.OptCode ");
            SQL.AppendLine("        UNION ALL ");
            SQL.AppendLine("        SELECT T2.SiteCode, 0.00 OCAmt, ");
            SQL.AppendLine("        0.00 NCAmt, 0.00 CPQ1Amt, 0.00 CPQ2Amt, 0.00 CPQ3Amt, T2.Amt CPQ4Amt ");
            SQL.AppendLine("        FROM TblAuctionHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblAuctionDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            AND T1.CancelInd = 'N' ");
            SQL.AppendLine("            AND T1.Yr = LEFT(@DocDt1, 4) ");
            SQL.AppendLine("            AND T2.AuctionType = 'CPQ4' ");
            SQL.AppendLine("        INNER JOIN TblOption T3 ON T3.Optcat = 'AuctionType' AND T2.Auctiontype = T3.OptCode ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    GROUP BY T.SiteCode ");
            SQL.AppendLine(") B ON A.SiteCode = B.SiteCode ");
            SQL.AppendLine("LEFT JOIN -- so contract ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT T3.SiteCode, SUM(T1.Amt) Amt ");
            SQL.AppendLine("    FROM TblSOContractHdr T1 ");
            SQL.AppendLine("    INNER JOIN TblBOQHdr T2 ON T1.BOQDocNo = T2.DocNo ");
            SQL.AppendLine("        AND (T1.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            SQL.AppendLine("        AND T1.CancelInd = 'N' ");
            SQL.AppendLine("        AND T1.Status = 'A' ");
            SQL.AppendLine("    INNER JOIN TblLOPHdr T3 ON T2.LOPDocNo = T3.DocNo ");
            SQL.AppendLine("        AND FIND_IN_SET(T3.SiteCode, @SiteCodeForAuctionInfo) ");
            SQL.AppendLine("    GROUP BY T3.SiteCode ");
            SQL.AppendLine(") C ON A.SiteCode = C.SiteCode ");
            SQL.AppendLine("LEFT JOIN -- lop ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT SiteCode, SUM(EstValue) Amt ");
            SQL.AppendLine("    FROM TblLOPHdr ");
            SQL.AppendLine("    WHERE FIND_IN_SET(SiteCode, @SiteCodeForAuctionInfo) ");
            SQL.AppendLine("        AND (DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            SQL.AppendLine("        AND CancelInd = 'N' ");
            SQL.AppendLine("        AND STATUS = 'A' ");
            SQL.AppendLine(") D ON A.SiteCode = D.SiteCode ");
            SQL.AppendLine("LEFT JOIN -- lop info awal, PQ, lelang, pemenang ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT T.SiteCode, SUM(T.UnprocessedAmt) UnprocessedAmt, SUM(T.PQAmt) PQAmt, SUM(T.AuctionAmt) AuctionAmt, SUM(T.WinAmt) WinAmt ");
            SQL.AppendLine("    FROM ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT SiteCode, EstValue UnprocessedAmt, 0.00 PQAmt, 0.00 AuctionAmt, 0.00 WinAmt ");
            SQL.AppendLine("        FROM TblLOPHdr T1 ");
            SQL.AppendLine("        WHERE (T1.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            SQL.AppendLine("        AND FIND_IN_SET(SiteCode, @SiteCodeForAuctionInfo) ");
            SQL.AppendLine("        AND T1.CancelInd = 'N' ");
            SQL.AppendLine("        AND T1.Status = 'A' ");
            SQL.AppendLine("        AND DocNo IN (SELECT DISTINCT DocNo FROM TblLOPDtl WHERE PhaseCode = @PhaseCodeForUnprocessedAmt AND Status = 'F') ");
            SQL.AppendLine("        UNION ALL ");
            SQL.AppendLine("        SELECT SiteCode, 0.00 UnprocessedAmt, EstValue PQAmt, 0.00 AuctionAmt, 0.00 WinAmt ");
            SQL.AppendLine("        FROM TblLOPHdr T1 ");
            SQL.AppendLine("        WHERE (T1.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            SQL.AppendLine("        AND FIND_IN_SET(SiteCode, @SiteCodeForAuctionInfo) ");
            SQL.AppendLine("        AND T1.CancelInd = 'N' ");
            SQL.AppendLine("        AND T1.Status = 'A' ");
            SQL.AppendLine("        AND DocNo IN (SELECT DISTINCT DocNo FROM TblLOPDtl WHERE PhaseCode = @PhaseCodeForPQProcessedAmt AND Status = 'F') ");
            SQL.AppendLine("        UNION ALL ");
            SQL.AppendLine("        SELECT SiteCode, 0.00 UnprocessedAmt, 0.00 PQAmt, EstValue AuctionAmt, 0.00 WinAmt ");
            SQL.AppendLine("        FROM TblLOPHdr T1 ");
            SQL.AppendLine("        WHERE (T1.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            SQL.AppendLine("        AND FIND_IN_SET(SiteCode, @SiteCodeForAuctionInfo) ");
            SQL.AppendLine("        AND T1.CancelInd = 'N' ");
            SQL.AppendLine("        AND T1.Status = 'A' ");
            SQL.AppendLine("        AND DocNo IN (SELECT DISTINCT DocNo FROM TblLOPDtl WHERE PhaseCode = @PhaseCodeForAuctionProcessedAmt AND Status = 'F') ");
            SQL.AppendLine("        UNION ALL ");
            SQL.AppendLine("        SELECT SiteCode, 0.00 UnprocessedAmt, 0.00 PQAmt, 0.00 AuctionAmt, EstValue WinAmt ");
            SQL.AppendLine("        FROM TblLOPHdr T1 ");
            SQL.AppendLine("        WHERE (T1.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            SQL.AppendLine("        AND FIND_IN_SET(SiteCode, @SiteCodeForAuctionInfo) ");
            SQL.AppendLine("        AND T1.CancelInd = 'N' ");
            SQL.AppendLine("        AND T1.Status = 'A' ");
            SQL.AppendLine("        AND DocNo IN (SELECT DISTINCT DocNo FROM TblLOPDtl WHERE PhaseCode = @PhaseCodeForWinAmt AND Status = 'F') ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    GROUP BY T.SiteCode ");
            SQL.AppendLine(") E ON A.SiteCode = E.SiteCode ");
            SQL.AppendLine("LEFT JOIN -- lop kalah ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT SiteCode, SUM(EstValue) Amt ");
            SQL.AppendLine("    FROM TblLOPHdr ");
            SQL.AppendLine("    WHERE (DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            SQL.AppendLine("    AND FIND_IN_SET(SiteCode, @SiteCodeForAuctionInfo) ");
            SQL.AppendLine("    AND CancelInd = 'N' ");
            SQL.AppendLine("    AND STATUS = 'A' ");
            SQL.AppendLine("    AND ProcessInd = 'S' ");
            SQL.AppendLine("    GROUP BY SiteCode ");
            SQL.AppendLine(") F ON A.SiteCode = F.SiteCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",
                    
                    //1-5
                    "Production"+Environment.NewLine+"Unit",
                    "Carry Over"+Environment.NewLine+"(Old Contract)",
                    "New Contract",
                    "Total",
                    "Contract"+Environment.NewLine+"Plan Q I",

                    //6-10
                    "Contract"+Environment.NewLine+"Plan Q II",
                    "Contract"+Environment.NewLine+"Plan Q III",
                    "Contract"+Environment.NewLine+"Plan Q IV",
                    "New Contract"+Environment.NewLine+"Realization",
                    "Win"+Environment.NewLine+"(New Contract Plan)",

                    //11-15
                    "Total"+Environment.NewLine+"(New Contract)", 
                    "%",
                    "Total"+Environment.NewLine+"(Contract)",
                    "%",
                    "Market"+Environment.NewLine+"Info",

                    //16-20
                    "Balance",
                    "Unprocessed",
                    "PQ"+Environment.NewLine+"Process",
                    "Auction"+Environment.NewLine+"Process",
                    "Win",

                    //21
                    "Lose"
                },
                new int[] 
                {
                    //0
                    50,
                    //1-5
                    250, 150, 150, 150, 150,
                    //6-10
                    150, 150, 150, 150, 150, 
                    //11-15
                    150, 150, 150, 150, 150, 
                    //16-20
                    150, 150, 150, 150, 150, 
                    //21
                    150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 }, 2);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<AuctionInfo>();
                var l2 = new List<AuctionBranchToSite>();

                Process1(ref l);
                if (l.Count > 0)
                {
                    Process2(ref l);
                    Process3(ref l2);
                    if (l2.Count > 0)
                        Process4(ref l, ref l2);
                    Process5(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                l.Clear(); l2.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional

        private void Process1(ref List<AuctionInfo> l)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = mSQL;
                
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@SiteCodeForAuctionInfo", mSiteCodeForAuctionInfo);
                Sm.CmParam<String>(ref cm, "@PhaseCodeForUnprocessedAmt", mPhaseCodeForUnprocessedAmt);
                Sm.CmParam<String>(ref cm, "@PhaseCodeForPQProcessedAmt", mPhaseCodeForPQProcessedAmt);
                Sm.CmParam<String>(ref cm, "@PhaseCodeForAuctionProcessedAmt", mPhaseCodeForAuctionProcessedAmt);
                Sm.CmParam<String>(ref cm, "@PhaseCodeForWinAmt", mPhaseCodeForWinAmt);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "SiteCode",
                    //1-5
                    "SiteName",
                    "OCAmt",
                    "NCAmt",
                    "CPQ1Amt",
                    "CPQ2Amt",
                    //6-10
                    "CPQ3Amt",
                    "CPQ4Amt",
                    "SOCAmt",
                    "LOPAmt",
                    "UnprocessedAmt",
                    //11-14
                    "PQAmt",
                    "AuctionAmt",
                    "WinAmt",
                    "LoseAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AuctionInfo()
                        {
                            SiteCode = Sm.DrStr(dr, c[0]),
                            SiteName = Sm.DrStr(dr, c[1]),
                            OCAmt = Sm.DrDec(dr, c[2]),
                            NCAmt = Sm.DrDec(dr, c[3]),
                            Total = Sm.DrDec(dr, c[2]) + Sm.DrDec(dr, c[3]),
                            CPQ1Amt = Sm.DrDec(dr, c[4]),
                            CPQ2Amt = Sm.DrDec(dr, c[5]),
                            CPQ3Amt = Sm.DrDec(dr, c[6]),
                            CPQ4Amt = Sm.DrDec(dr, c[7]),
                            SOCAmt = Sm.DrDec(dr, c[8]),
                            TotalNCAmt = Sm.DrDec(dr, c[8]) + Sm.DrDec(dr, c[13]),
                            PercentageNC = 0m,
                            TotalContractAmt = Sm.DrDec(dr, c[2]) + Sm.DrDec(dr, c[8]) + Sm.DrDec(dr, c[13]),
                            PercentageContractAmt = 0m, 
                            LOPAmt = Sm.DrDec(dr, c[9]),
                            BalanceAmt = Sm.DrDec(dr, c[3]) - Sm.DrDec(dr, c[9]),
                            UnprocessedAmt = Sm.DrDec(dr, c[10]),
                            PQAmt = Sm.DrDec(dr, c[11]),
                            AuctionAmt = Sm.DrDec(dr, c[12]),
                            WinAmt = Sm.DrDec(dr, c[13]),
                            LoseAmt = Sm.DrDec(dr, c[14])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<AuctionInfo> l)
        {
            decimal mQAmt = 0m; string mQLocation = string.Empty;

            foreach(var x in l)
            {
                x.PercentageContractAmt = (x.Total == 0m) ? 0m : (x.TotalContractAmt / x.Total) * 100m;
                
                string mEndMth = Sm.Left(Sm.GetDte(DteDocDt2), 8).Substring(4, 2);

                if (mEndMth == "01" || mEndMth == "02" || mEndMth == "03") mQLocation = "1";
                if (mEndMth == "04" || mEndMth == "05" || mEndMth == "06") mQLocation = "2";
                if (mEndMth == "07" || mEndMth == "08" || mEndMth == "09") mQLocation = "3";
                if (mEndMth == "10" || mEndMth == "11" || mEndMth == "12") mQLocation = "4";

                if (mQLocation.Length == 0) mQLocation = "4";

                if (mQLocation == "1") mQAmt = x.CPQ1Amt;
                if (mQLocation == "2") mQAmt = x.CPQ1Amt + x.CPQ2Amt;
                if (mQLocation == "3") mQAmt = x.CPQ1Amt = x.CPQ2Amt + x.CPQ3Amt;
                if (mQLocation == "4") mQAmt = x.CPQ1Amt + x.CPQ2Amt + x.CPQ3Amt + x.CPQ4Amt;

                if (mQAmt != 0m) x.PercentageNC = (x.TotalNCAmt / mQAmt) * 100m;
            }
        }

        private void Process3(ref List<AuctionBranchToSite> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.OptCode BranchCode, A.OptDesc SiteCode, B.SiteName ");
            SQL.AppendLine("From TblOption A ");
            SQL.AppendLine("Inner Join TblSite B ON A.OptDesc = B.SiteCode ");
            SQL.AppendLine("Where A.OptCat = 'AuctionBranchToSite'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BranchCode", "SiteCode", "SiteName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AuctionBranchToSite()
                        {
                            BranchCode = Sm.DrStr(dr, c[0]),
                            SiteCode = Sm.DrStr(dr, c[1]),
                            SiteName = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process4(ref List<AuctionInfo> l, ref List<AuctionBranchToSite> l2)
        {
            foreach(var x in l)
            {
                foreach(var y in l2)
                {
                    if (x.SiteCode == y.BranchCode)
                    {
                        x.SiteCode = y.SiteCode;
                        x.SiteName = y.SiteName;
                        break;
                    }
                }
            }
        }

        private void Process5(ref List<AuctionInfo> l)
        {
            var lx = l.GroupBy(x => x.SiteCode)
                .Select(t => new
                {
                    SiteCode = t.Key,
                    SiteName = t.First().SiteName,
                    OCAmt = t.Sum(s => s.OCAmt),
                    NCAmt = t.Sum(s => s.NCAmt),
                    Total = t.Sum(s => s.Total),
                    CPQ1Amt = t.Sum(s => s.CPQ1Amt),
                    CPQ2Amt = t.Sum(s => s.CPQ2Amt),
                    CPQ3Amt = t.Sum(s => s.CPQ3Amt),
                    CPQ4Amt = t.Sum(s => s.CPQ4Amt),
                    SOCAmt = t.Sum(s => s.SOCAmt),
                    TotalNCAmt = t.Sum(s => s.TotalNCAmt),
                    PercentageNC = t.Sum(s => s.PercentageNC),
                    TotalContractAmt = t.Sum(s => s.TotalContractAmt),
                    PercentageContractAmt = t.Sum(s => s.PercentageContractAmt),
                    LOPAmt = t.Sum(s => s.LOPAmt),
                    BalanceAmt = t.Sum(s => s.BalanceAmt),
                    UnprocessedAmt = t.Sum(s => s.UnprocessedAmt),
                    PQAmt = t.Sum(s => s.PQAmt),
                    AuctionAmt = t.Sum(s => s.AuctionAmt),
                    WinAmt = t.Sum(s => s.WinAmt),
                    LoseAmt = t.Sum(s => s.LoseAmt)
                }).ToList();

            int Row = 0;

            foreach(var x in lx)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Row, 0].Value = Row + 1;
                Grd1.Cells[Row, 1].Value = x.SiteName;
                Grd1.Cells[Row, 2].Value = x.OCAmt;
                Grd1.Cells[Row, 3].Value = x.NCAmt;
                Grd1.Cells[Row, 4].Value = x.Total;
                Grd1.Cells[Row, 5].Value = x.CPQ1Amt;
                Grd1.Cells[Row, 6].Value = x.CPQ2Amt;
                Grd1.Cells[Row, 7].Value = x.CPQ3Amt;
                Grd1.Cells[Row, 8].Value = x.CPQ4Amt;
                Grd1.Cells[Row, 9].Value = x.SOCAmt;
                Grd1.Cells[Row, 10].Value = x.WinAmt;
                Grd1.Cells[Row, 11].Value = x.TotalNCAmt;
                Grd1.Cells[Row, 12].Value = x.PercentageNC;
                Grd1.Cells[Row, 13].Value = x.TotalContractAmt;
                Grd1.Cells[Row, 14].Value = x.PercentageContractAmt;
                Grd1.Cells[Row, 15].Value = x.LOPAmt;
                Grd1.Cells[Row, 16].Value = x.BalanceAmt;
                Grd1.Cells[Row, 17].Value = x.UnprocessedAmt;
                Grd1.Cells[Row, 18].Value = x.PQAmt;
                Grd1.Cells[Row, 19].Value = x.AuctionAmt;
                Grd1.Cells[Row, 20].Value = x.WinAmt;
                Grd1.Cells[Row, 21].Value = x.LoseAmt;

                Row += 1;
            }

            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 16, 17, 18, 19, 20, 21 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Class

        private class AuctionInfo
        {
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
            public decimal OCAmt { get; set; }
            public decimal NCAmt { get; set; }
            public decimal Total { get; set; }
            public decimal CPQ1Amt { get; set; }
            public decimal CPQ2Amt { get; set; }
            public decimal CPQ3Amt { get; set; }
            public decimal CPQ4Amt { get; set; }
            public decimal SOCAmt { get; set; }
            public decimal TotalNCAmt { get; set; }
            public decimal PercentageNC { get; set; }
            public decimal TotalContractAmt { get; set; }
            public decimal PercentageContractAmt { get; set; }
            public decimal LOPAmt { get; set; }
            public decimal BalanceAmt { get; set; }
            public decimal UnprocessedAmt { get; set; }
            public decimal PQAmt { get; set; }
            public decimal AuctionAmt { get; set; }
            public decimal WinAmt { get; set; }
            public decimal LoseAmt { get; set; }
        }

        private class AuctionBranchToSite
        {
            public string BranchCode { get; set; }
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
        }

        #endregion

        #endregion
    }
}
