﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmDevice : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDeviceCode = string.Empty;
        internal FrmDeviceFind FrmFind;

        #endregion

        #region Constructor

        public FrmDevice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Device";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                //if this application is called from other application
                if (mDeviceCode.Length != 0)
                {
                    ShowData(mDeviceCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-4
                        "Code",
                        "Access Point",
                        "",
                        "Active"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-4
                        150, 300, 20, 80
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0, 3 });
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { 
                        TxtDeviceCode, TxtDeviceName, ChkActInd, LueBrCode, LueBTSCode, 
                        LueAPModelCode 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtDeviceCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { 
                        TxtDeviceCode, TxtDeviceName, LueBrCode, LueBTSCode, LueAPModelCode 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtDeviceCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { 
                        TxtDeviceName, ChkActInd, LueBrCode, LueBTSCode, LueAPModelCode 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtDeviceName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtDeviceCode, TxtDeviceName, LueBrCode, LueBTSCode, LueAPModelCode });
            ChkActInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 4 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDeviceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                Sl.SetLueBrCode(ref LueBrCode, string.Empty);
                Sl.SetLueBTSCode(ref LueBTSCode, string.Empty);
                Sl.SetLueAPModelCode(ref LueAPModelCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDeviceCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Delete", "") == DialogResult.No || Sm.IsTxtEmpty(TxtDeviceCode, "", false)) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand()
                {
                    CommandText =
                        "Delete From TblDeviceHdr Where DeviceCode=@DeviceCode;" +
                        "Delete From TblDeviceDtl Where DeviceCode=@DeviceCode;"
                };
                Sm.CmParam<String>(ref cm, "@DeviceCode", TxtDeviceCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveDeviceHdr());
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        cml.Add(SaveDeviceDtl(Row));

                Sm.ExecCommands(cml);
                ShowData(TxtDeviceCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDeviceDlg(this));
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAPCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmDeviceDlg(this));
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmAP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAPCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string DeviceCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowDeviceHdr(DeviceCode);
                ShowDeviceDtl(DeviceCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDeviceHdr(string DeviceCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DeviceCode, DeviceName, ActInd, BrCode, BTSCode, APModelCode ");
            SQL.AppendLine("From TblDeviceHdr Where DeviceCode=@DeviceCode; ");

            Sm.CmParam<String>(ref cm, "@DeviceCode", DeviceCode);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "DeviceCode", 
                        "DeviceName", "ActInd", "BrCode", "BTSCode", "APModelCode" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDeviceCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtDeviceName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        Sl.SetLueBrCode(ref LueBrCode, Sm.DrStr(dr, c[3]));
                        Sl.SetLueBTSCode(ref LueBTSCode, Sm.DrStr(dr, c[4]));
                        Sl.SetLueAPModelCode(ref LueAPModelCode, Sm.DrStr(dr, c[5]));
                    }, true
                );
        }

        private void ShowDeviceDtl(string DeviceCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DeviceCode", DeviceCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.APCode, B.APName, B.ActInd ");
            SQL.AppendLine("From TblDeviceDtl A ");
            SQL.AppendLine("Inner Join TblAPHdr B On A.APCode=B.APCode ");
            SQL.AppendLine("Where A.DeviceCode=@DeviceCode Order By B.APName;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { "APCode", "APName", "ActInd" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 2);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDeviceCode, "Device code", false) ||
                Sm.IsTxtEmpty(TxtDeviceName, "Device name", false) ||
                Sm.IsLueEmpty(LueBrCode, "Brand") ||
                Sm.IsLueEmpty(LueBTSCode, "BTS") ||
                Sm.IsLueEmpty(LueAPModelCode, "AP model") ||
                IsCodeAlreadyExisted() ||
                IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 access point.");
                return true;
            }
            return false;
        }


        private bool IsCodeAlreadyExisted()
        {
            return
                !TxtDeviceCode.Properties.ReadOnly &&
                Sm.IsDataExist(
                    "Select DeviceCode From TblDeviceHdr Where DeviceCode=@Param;",
                    TxtDeviceCode.Text,
                    "Device code ( " + TxtDeviceCode.Text + " ) already existed."
                    );
        }

        
        private MySqlCommand SaveDeviceHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDeviceHdr(DeviceCode, DeviceName, ActInd, ");
            SQL.AppendLine("BrCode, BTSCode, APModelCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DeviceCode, @DeviceName, @ActInd, ");
            SQL.AppendLine("@BrCode, @BTSCode, @APModelCode, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update DeviceName=@DeviceName, ActInd=@ActInd, ");
            SQL.AppendLine("   BrCode=@BrCode, BTSCode=@BTSCode, APModelCode=@APModelCode, ");
            SQL.AppendLine("   LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            if (TxtDeviceCode.Properties.ReadOnly)
                SQL.AppendLine("Delete From TblDeviceDtl Where DeviceCode=@DeviceCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DeviceCode", TxtDeviceCode.Text);
            Sm.CmParam<String>(ref cm, "@DeviceName", TxtDeviceName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@BrCode", Sm.GetLue(LueBrCode));
            Sm.CmParam<String>(ref cm, "@BTSCode", Sm.GetLue(LueBTSCode));
            Sm.CmParam<String>(ref cm, "@APModelCode", Sm.GetLue(LueAPModelCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDeviceDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDeviceDtl(DeviceCode, APCode, CreateBy, CreateDt, LastUpBy, LastUpDt)");
            SQL.AppendLine("Select DeviceCode, @APCode, CreateBy, CreateDt, LastUpBy, LastUpDt ");
            SQL.AppendLine("From TblDeviceHdr Where DeviceCode=@DeviceCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DeviceCode", TxtDeviceCode.Text);
            Sm.CmParam<String>(ref cm, "@APCode", Sm.GetGrdStr(Grd1, Row, 1));
            return cm;
        }

        #endregion

        internal string GetSelectedData()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDeviceCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDeviceCode);
        }

        private void TxtDeviceName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDeviceName);
        }

        private void LueBrCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBrCode, new Sm.RefreshLue2(Sl.SetLueBrCode), string.Empty);
        }

        private void LueBTSCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBTSCode, new Sm.RefreshLue2(Sl.SetLueBTSCode), string.Empty);
        }

        private void LueAPModelCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAPModelCode, new Sm.RefreshLue2(Sl.SetLueAPModelCode), string.Empty);
        }

        #endregion

        #endregion
    }
}
