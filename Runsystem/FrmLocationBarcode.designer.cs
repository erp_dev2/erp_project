﻿namespace RunSystem
{
    partial class FrmLocationBarcode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChkLocCode = new DevExpress.XtraEditors.CheckEdit();
            this.TxtLocCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.ChkLocName = new DevExpress.XtraEditors.CheckEdit();
            this.TxtLocName = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLocCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLocName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkLocName);
            this.panel2.Controls.Add(this.TxtLocName);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.ChkLocCode);
            this.panel2.Controls.Add(this.TxtLocCode);
            this.panel2.Controls.Add(this.label3);
            // 
            // ChkLocCode
            // 
            this.ChkLocCode.Location = new System.Drawing.Point(590, 77);
            this.ChkLocCode.Name = "ChkLocCode";
            this.ChkLocCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkLocCode.Properties.Appearance.Options.UseFont = true;
            this.ChkLocCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkLocCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkLocCode.Properties.Caption = " ";
            this.ChkLocCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkLocCode.Size = new System.Drawing.Size(20, 22);
            this.ChkLocCode.TabIndex = 37;
            this.ChkLocCode.ToolTip = "Remove filter";
            this.ChkLocCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkLocCode.ToolTipTitle = "Run System";
            this.ChkLocCode.CheckedChanged += new System.EventHandler(this.ChkLocCode_CheckedChanged);
            // 
            // TxtLocCode
            // 
            this.TxtLocCode.EnterMoveNextControl = true;
            this.TxtLocCode.Location = new System.Drawing.Point(263, 79);
            this.TxtLocCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocCode.Name = "TxtLocCode";
            this.TxtLocCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocCode.Properties.Appearance.Options.UseFont = true;
            this.TxtLocCode.Properties.MaxLength = 400;
            this.TxtLocCode.Size = new System.Drawing.Size(319, 20);
            this.TxtLocCode.TabIndex = 36;
            this.TxtLocCode.Validated += new System.EventHandler(this.TxtLocCode_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(171, 80);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 14);
            this.label3.TabIndex = 35;
            this.label3.Text = "Location Code";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkLocName
            // 
            this.ChkLocName.Location = new System.Drawing.Point(590, 104);
            this.ChkLocName.Name = "ChkLocName";
            this.ChkLocName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkLocName.Properties.Appearance.Options.UseFont = true;
            this.ChkLocName.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkLocName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkLocName.Properties.Caption = " ";
            this.ChkLocName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkLocName.Size = new System.Drawing.Size(20, 22);
            this.ChkLocName.TabIndex = 40;
            this.ChkLocName.ToolTip = "Remove filter";
            this.ChkLocName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkLocName.ToolTipTitle = "Run System";
            this.ChkLocName.CheckedChanged += new System.EventHandler(this.ChkLocName_CheckedChanged);
            // 
            // TxtLocName
            // 
            this.TxtLocName.EnterMoveNextControl = true;
            this.TxtLocName.Location = new System.Drawing.Point(263, 104);
            this.TxtLocName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocName.Name = "TxtLocName";
            this.TxtLocName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocName.Properties.Appearance.Options.UseFont = true;
            this.TxtLocName.Properties.MaxLength = 400;
            this.TxtLocName.Size = new System.Drawing.Size(319, 20);
            this.TxtLocName.TabIndex = 39;
            this.TxtLocName.Validated += new System.EventHandler(this.TxtLocName_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(168, 106);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 14);
            this.label1.TabIndex = 38;
            this.label1.Text = "Location Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmLocationBarcode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmLocationBarcode";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLocCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLocName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkLocName;
        private DevExpress.XtraEditors.TextEdit TxtLocName;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkLocCode;
        private DevExpress.XtraEditors.TextEdit TxtLocCode;
        private System.Windows.Forms.Label label3;
    }
}