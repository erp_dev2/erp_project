﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWOPIC : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmWOPICFind FrmFind;

        #endregion

        #region Constructor

        public FrmWOPIC(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
            Sl.SetLueCompetenceCode(ref LueCompetenceCode);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPicCode, TxtPicName, LueCompetenceCode
                    }, true);
                    TxtPicCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPicCode, TxtPicName, LueCompetenceCode
                    }, false);
                    TxtPicCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPicName, LueCompetenceCode
                    }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPicCode, 
                    }, true);
                    TxtPicName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtPicCode, TxtPicName, LueCompetenceCode
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWOPICFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPicCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

   
        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;
                Cursor.Current = Cursors.WaitCursor;
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblWOPIC(WOPICCode, WOPICName, CompetenceCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@WOPICCode, @WOPICName, @CompetenceCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");

                SQL.AppendLine("Update WOPICName=@WOPICName, CompetenceCode=@CompetenceCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@WOPICCode", TxtPicCode.Text);
                Sm.CmParam<String>(ref cm, "@WOPICName", TxtPicName.Text);
                Sm.CmParam<String>(ref cm, "@CompetenceCode", Sm.GetLue(LueCompetenceCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtPicCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPicCode, "PIC Code", false)||
                Sm.IsTxtEmpty(TxtPicName, "PIC Name", false) ||
                Sm.IsLueEmpty(LueCompetenceCode, "Competence Name")||
                IsPICCodeExisted();
        }

        private bool IsPICCodeExisted()
        {
            if (!TxtPicCode.Properties.ReadOnly && Sm.IsDataExist("Select WOPICCode From TblWOPIC Where WOPICCode='" + TxtPicCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "PIC code ( " + TxtPicCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion


        #endregion

        #region  Show Data

        public void ShowData(string PICCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@PICCode", PICCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select  WOPICCode, WOPICName, competencecode From TblWOPIC Where WOPICCode=@PICCode",
                        new string[] 
                        {
                            "WOPICCode", "WOPICName", "competencecode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPicCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPicName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetLue(LueCompetenceCode, Sm.DrStr(dr, c[2]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }


        #endregion

        #endregion

        #region Event
        private void LueCompetenceCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCompetenceCode, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }
        #endregion
    }
}
