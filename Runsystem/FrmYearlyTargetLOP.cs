﻿#region update

/*
 *  29/09/2022 [SET/VIR] Menu Baru : Yearly Target LOP
 */ 

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmYearlyTargetLOP : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmYearlyTargetLOPFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmYearlyTargetLOP(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        protected override void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            LueSiteCode.Visible = false;
            //GetParameter();
            SetGrd();
            SetFormControl(mState.View);
            Sl.SetLueSiteCode(ref LueSiteCode);
            Sl.SetLueYr(LueYr, "");
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, LueSiteCode, LueYr, DteDocDt, ChkCancelInd
                    }, true);
                    LblGetAllSite.Visible = false;
                    Grd1.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                       LueYr, LueSiteCode, DteDocDt
                    }, false);
                    LblGetAllSite.Visible = true;
                    Sm.SetDteCurrentDate(DteDocDt);
                    Grd1.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                       ChkCancelInd
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { });
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 3;
            Grd1.ReadOnly = false;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "SiteCode",

                        //1-2
                        "Site", "Amount", 
                    },
                    new int[]
                    {
                       //0
                       50, 

                       //1-5
                       150, 150, 
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 0 });
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, LueSiteCode, LueYr, DteDocDt
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmYearlyTargetLOPFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            string DocNo = string.Empty;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "YearlyTargetLOP", "TblSiteTargetLOPHdr");

            cml.Add(SaveYearlyTargetLOPHdr(DocNo));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsYearlyTargetLOPInvalid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 detail.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1) == Sm.GetGrdStr(Grd1, Row + 1, 1))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Sites can't be the same");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsYearlyTargetLOPInvalid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblSiteTargetLOPHdr Where CancelInd = 'N' ");
            SQL.AppendLine("And Yr = '" + Sm.GetLue(LueYr) + "' Limit 1 ");

            var Yr = Sm.GetLue(LueYr);

            if (Sm.IsDataExist(SQL.ToString()))
            {
                Sm.StdMsg(mMsgType.Warning, "Document in " + Yr + " Already Exists.");
                return true;
            }

            return false;
        }

        private MySqlCommand SaveYearlyTargetLOPHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            bool IsFirst = true;
            var SQLDtl = new StringBuilder();
            var cm = new MySqlCommand();

            //TblHdr
            SQL.AppendLine("Insert Into TblSiteTargetLOPHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Yr, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @Yr, @CreateBy, CurrentDateTime()); ");

            //TblDtl
            SQLDtl.AppendLine("Insert Into TblSiteTargetLOPDtl ");
            SQLDtl.AppendLine("(DocNo, DNo, SiteCode, Amt, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values ");
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (IsFirst)
                    IsFirst = false;
                else
                    SQLDtl.AppendLine(", ");

                SQLDtl.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", @SiteCode_" + r.ToString() + ", @Amt_" + r.ToString() + ", ");
                SQLDtl.AppendLine("@CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@SiteCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 2));
            }
            SQLDtl.AppendLine("; ");

            cm.CommandText = SQL.ToString() + SQLDtl.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData(string DocNo)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            cml.Add(EditYearlyTargetLOPHdr(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentAlreadyCancel();
        }

        private bool IsDocumentAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select 1 From TblSiteTargetLOPHdr Where CancelInd = 'Y' And DocNo = @DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancel");
                return true;
            }
            return false;
        }

        private MySqlCommand EditYearlyTargetLOPHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSiteTargetLOPHdr ");
            SQL.AppendLine("Set CancelInd=@CancelInd, ");
            SQL.AppendLine("LastUpBy=@LastUpBy, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LastUpBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowYearlyTargetLOPHdr(DocNo);
                ShowYearlyTargetLOPDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowYearlyTargetLOPHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                 ref cm,
                  "Select DocNo, CancelInd, DocDt, Yr " +
                  "From TblSiteTargetLOPHdr Where DocNo=@DocNo",
                 new string[]
                   {
                      //0
                      "DocNo",

                      //1-3
                      "CancelInd", "DocDt", "Yr",

                   },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[1]), "Y");
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                     Sm.SetLue(LueYr, Sm.DrStr(dr, c[3]));
                 }, true
             );
        }

        private void ShowYearlyTargetLOPDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.SiteCode, B.SiteName, A.Amt ");
            SQL.AppendLine("FROM tblsitetargetLOPdtl A ");
            SQL.AppendLine("INNER JOIN tblsite B ON A.SiteCode = B.SiteCode ");
            SQL.AppendLine("Where DocNo=@DocNo ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[]
                       { 
                           //0
                           "SiteCode", 

                           //1-2
                           "SiteName",
                           "Amt",
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 2);
        }

        #endregion

        #region Grid Control Events

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                if (e.ColIndex == 1) Sm.LueRequestEdit(ref Grd1, ref LueSiteCode, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd1, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2 });
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] {  }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] {  }, e);
        }

        #endregion

        #endregion

        #region Event

        private void LueSiteCode_Validated(object sender, EventArgs e)
        {
            LueSiteCode.Visible = false;
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            //if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, ChkCancelInd);
        }

        private void LblGetAllSite_Click(object sender, EventArgs e)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.SiteCode, A.SiteName, 0.00 As Amt ");
            SQL.AppendLine("FROM TblSite A ");

            var cm = new MySqlCommand();

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[]
                       { 
                           //0
                           "SiteCode", 

                           //1-2
                           "SiteName",
                           "Amt"
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 2);
        }

        private void LueSiteCode_Leave(object sender, EventArgs e)
        {
            if (LueSiteCode.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (LueSiteCode.Text.Trim().Length == 0)
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueSiteCode).Trim();
                    Grd1.Cells[fCell.RowIndex, 1].Value = LueSiteCode.GetColumnValue("Col2");
                }
            }
        }

        #endregion
    }
}
