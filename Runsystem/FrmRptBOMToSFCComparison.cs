﻿#region Update
/*
    06/02/2019 [TKG] New application
    09/07/2019 [WED] masih muncul yang selain sequence terkecil dan terbesar
    06/08/2019 [TKG] menampilkan work center name
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptBOMToSFCComparison : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mRptWarpingtoPackingComparisonWC1 = string.Empty, mRptWarpingtoPackingComparisonWC2 = string.Empty;
        
        #endregion

        #region Constructor

        public FrmRptBOMToSFCComparison(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueItCtCode(ref LueItCtCode);
                Sl.SetLueUomCode(ref LueInventoryUomCode);
                Sl.SetLueUomCode(ref LuePlanningUomCode);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            #region Old Code

            //SQL.AppendLine("Select ");
            //SQL.AppendLine("Tbl1.DocNo, Tbl2.PPDocNo, Tbl5.DocName, Tbl2.WorkcenterDocNo, Tbl2.DocDt, ");
            //SQL.AppendLine("Tbl1.ItCode, Tbl3.ItName, Tbl4.ItCtName, Tbl1.BatchNo, Tbl3.InventoryUomCode, Tbl3.PlanningUomCode, ");
            //SQL.AppendLine("Tbl1.Qty_4, Tbl1.Qty_5, Tbl1.Qty_6, ");
            //SQL.AppendLine("IfNull((Tbl1.Qty_5/Tbl1.Qty_6)*Tbl1.Qty_4, 0.00000000) As Value2, ");
            //SQL.AppendLine("Tbl1.Qty_1, Tbl1.Qty_2, Tbl1.Qty_3, "); 
            //SQL.AppendLine("IfNull((Tbl1.Qty_2/Tbl1.Qty_3)*Tbl1.Qty_1, 0.00000000) As Value1, ");
            //SQL.AppendLine("Tbl2.CreateBy, Tbl2.Remark ");
            //SQL.AppendLine("From ( ");
            //SQL.AppendLine("    Select DocNo, ProductionRoutingDocNo, ItCode, BatchNo, "); 
            //SQL.AppendLine("    Sum(Qty_1) As Qty_1, "); 
            //SQL.AppendLine("    Sum(Qty_2) As Qty_2, "); 
            //SQL.AppendLine("    Sum(Qty_3) As Qty_3, "); 
            //SQL.AppendLine("    Sum(Qty_4) As Qty_4, "); 
            //SQL.AppendLine("    Sum(Qty_5) As Qty_5, "); 
            //SQL.AppendLine("    Sum(Qty_6) As Qty_6 ");
            //SQL.AppendLine("    From ( ");
            //SQL.AppendLine("        Select A.DocNo, E.ProductionRoutingDocNo, ");
            //SQL.AppendLine("        B.ItCode, B.BatchNo, "); 
            //SQL.AppendLine("        B.Qty As Qty_1, F.Qty As Qty_2, G.Qty As Qty_3, ");  
            //SQL.AppendLine("        0.00000000 As Qty_4, 0.00000000 As Qty_5, 0.00000000 As Qty_6 ");
            //SQL.AppendLine("        From TblShopFloorControlHdr A ");
            //SQL.AppendLine("        Inner Join TblShopFloorControlDtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("        Inner Join TblPPHdr C On A.PPDocNo=C.DocNo ");
            //SQL.AppendLine("        Inner Join TblPPDtl D On C.DocNo=D.DocNo ");
            //SQL.AppendLine("        Inner Join TblProductionOrderHdr E On D.ProductionOrderDocNo=E.DocNo ");
            //SQL.AppendLine("        Inner Join ( ");
            //SQL.AppendLine("            Select T1.DocNo, T6.ItCode, Sum(T6.Qty) Qty ");
            //SQL.AppendLine("            From TblProductionOrderHdr T1 ");
            //SQL.AppendLine("            Inner Join TblProductionOrderDtl T2 On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("            Inner Join TblProductionRoutingHdr T3 On T1.ProductionRoutingDocNo=T3.DocNo ");
            //SQL.AppendLine("            Inner Join TblProductionRoutingDtl T4 On T3.DocNo=T4.DocNo And T2.ProductionRoutingDNo=T4.DNo ");
            //SQL.AppendLine("            Inner Join TblBOMHdr T5 On T2.BOMDocNo=T5.DocNo "); 
            //SQL.AppendLine("            Inner Join TblBOMDtl2 T6 On T5.DocNo=T6.DocNo ");
		    
            ////SQL.AppendLine("            Inner Join ( ");
            ////SQL.AppendLine("                Select DocNo, Max(DNo) DNo From TblProductionRoutingDtl Group By DocNo ");
            ////SQL.AppendLine("            ) T7 On T4.DocNo=T7.DocNo And T4.DNo=T7.DNo ");

            //SQL.AppendLine("            Inner Join ( ");
            //SQL.AppendLine("                Select DocNo, Max(Sequence) Sequence From TblProductionRoutingDtl Group By DocNo ");
            //SQL.AppendLine("            ) T7 On T4.DocNo=T7.DocNo And T4.Sequence=T7.Sequence ");
		    

            //SQL.AppendLine("            Where T1.CancelInd='N' ");
            //SQL.AppendLine("            And T1.Status<>'C' ");
            //SQL.AppendLine("            And T1.DocNo In ( ");
            //SQL.AppendLine("                Select Distinct X3.ProductionOrderDocNo ");
            //SQL.AppendLine("                From TblShopFloorControlHdr X1 ");
            //SQL.AppendLine("                Inner Join TblPPHdr X2 On X1.PPDocNo=X2.DocNo ");
            //SQL.AppendLine("                Inner Join TblPPDtl X3 On X3.DocNo=X3.DocNo ");
            //SQL.AppendLine("                Where X1.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("            ) ");
            //SQL.AppendLine("            Group By T1.DocNo, T6.ItCode ");
            //SQL.AppendLine("        ) F On E.DocNo=F.DocNo And B.ItCode=F.ItCode ");
            //SQL.AppendLine("        Inner Join ( ");
            //SQL.AppendLine("            Select T1.DocNo, T6.ItCode, Sum(T6.Qty) Qty ");
            //SQL.AppendLine("            From TblProductionOrderHdr T1 ");
            //SQL.AppendLine("            Inner Join TblProductionOrderDtl T2 On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("            Inner Join TblProductionRoutingHdr T3 On T1.ProductionRoutingDocNo=T3.DocNo ");
            //SQL.AppendLine("            Inner Join TblProductionRoutingDtl T4 On T3.DocNo=T4.DocNo And T2.ProductionRoutingDNo=T4.DNo ");
            //SQL.AppendLine("            Inner Join TblBOMHdr T5 On T2.BOMDocNo=T5.DocNo "); 
            //SQL.AppendLine("            Inner Join TblBOMDtl2 T6 On T5.DocNo=T6.DocNo ");

            //SQL.AppendLine("            Inner Join ( ");
            //SQL.AppendLine("                Select DocNo, Max(Sequence) Sequence From TblProductionRoutingDtl Group By DocNo ");
            //SQL.AppendLine("            ) T7 On T4.DocNo=T7.DocNo And T4.Sequence=T7.Sequence ");

            //SQL.AppendLine("            Where T1.CancelInd='N' And T1.Status<>'C' ");
            //SQL.AppendLine("            And T1.DocNo In ( ");
            //SQL.AppendLine("                Select Distinct X3.ProductionOrderDocNo ");
            //SQL.AppendLine("                From TblShopFloorControlHdr X1 ");
            //SQL.AppendLine("                Inner Join TblPPHdr X2 On X1.PPDocNo=X2.DocNo ");
            //SQL.AppendLine("                Inner Join TblPPDtl X3 On X2.DocNo=X3.DocNo ");
            //SQL.AppendLine("                Where X1.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("            ) ");
            //SQL.AppendLine("            Group By T1.DocNo, T6.ItCode ");
            //SQL.AppendLine("        ) G On E.DocNo=G.DocNo And B.ItCode=G.ItCode ");
            //SQL.AppendLine("        Where A.CancelInd='N' ");
            //SQL.AppendLine("        And A.DocDt Between @DocDt1 And @DocDt2 ");

            //SQL.AppendLine("        Union All ");

            //SQL.AppendLine("        Select A.DocNo, E.ProductionRoutingDocNo, B.ItCode, B.BatchNo, "); 
            //SQL.AppendLine("        0.00000000 As Qty_1, 0.00000000 As Qty_2, 0.00000000 As Qty_3, ");  
            //SQL.AppendLine("        B.Qty As Qty_4, F.Qty As Qty_5, G.Qty As Qty_6 ");
            //SQL.AppendLine("        From TblShopFloorControlHdr A ");
            //SQL.AppendLine("        Inner Join TblShopFloorControl3Dtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("        Inner Join TblPPHdr C On A.PPDocNo=C.DocNo ");
            //SQL.AppendLine("        Inner Join TblPPDtl D On C.DocNo=D.DocNo ");
            //SQL.AppendLine("        Inner Join TblProductionOrderHdr E On D.ProductionOrderDocNo=E.DocNo ");
            //SQL.AppendLine("        Inner Join ( ");
            //SQL.AppendLine("            Select T1.DocNo, T6.ItCode, Sum(T6.Qty) Qty ");
            //SQL.AppendLine("            From TblProductionOrderHdr T1 ");
            //SQL.AppendLine("            Inner Join TblProductionOrderDtl T2 On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("            Inner Join TblProductionRoutingHdr T3 On T1.ProductionRoutingDocNo=T3.DocNo ");
            //SQL.AppendLine("            Inner Join TblProductionRoutingDtl T4 On T3.DocNo=T4.DocNo And T2.ProductionRoutingDNo=T4.DNo ");
            //SQL.AppendLine("            Inner Join TblBOMHdr T5 On T2.BOMDocNo=T5.DocNo "); 
            //SQL.AppendLine("            Inner Join TblBOMDtl2 T6 On T5.DocNo=T6.DocNo ");
            
            ////SQL.AppendLine("            Inner Join ( ");
            ////SQL.AppendLine("                Select DocNo, Min(DNo) DNo From TblProductionRoutingDtl Group By DocNo ");
            ////SQL.AppendLine("            ) T7 On T4.DocNo=T7.DocNo And T4.DNo=T7.DNo ");

            //SQL.AppendLine("            Inner Join ( ");
            //SQL.AppendLine("                Select DocNo, Min(Sequence) Sequence From TblProductionRoutingDtl Group By DocNo ");
            //SQL.AppendLine("            ) T7 On T4.DocNo=T7.DocNo And T4.Sequence=T7.Sequence ");


            //SQL.AppendLine("            Where T1.CancelInd='N' And T1.Status<>'C' ");
            //SQL.AppendLine("            And T1.DocNo In ( ");
            //SQL.AppendLine("                Select Distinct X3.ProductionOrderDocNo ");
            //SQL.AppendLine("                From TblShopFloorControlHdr X1 ");
            //SQL.AppendLine("                Inner Join TblPPHdr X2 On X1.PPDocNo=X2.DocNo ");
            //SQL.AppendLine("                Inner Join TblPPDtl X3 On X2.DocNo=X3.DocNo ");
            //SQL.AppendLine("                Where X1.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("            ) ");
            //SQL.AppendLine("            Group By T1.DocNo, T6.ItCode ");
            //SQL.AppendLine("        ) F On E.DocNo=F.DocNo And B.ItCode=F.ItCode ");
            //SQL.AppendLine("        Inner Join ( ");
            //SQL.AppendLine("            Select T1.DocNo, T6.ItCode, Sum(T6.Qty) Qty ");
            //SQL.AppendLine("            From TblProductionOrderHdr T1 ");
            //SQL.AppendLine("            Inner Join TblProductionOrderDtl T2 On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("            Inner Join TblProductionRoutingHdr T3 On T1.ProductionRoutingDocNo=T3.DocNo ");
            //SQL.AppendLine("            Inner Join TblProductionRoutingDtl T4 On T3.DocNo=T4.DocNo And T2.ProductionRoutingDNo=T4.DNo ");
            //SQL.AppendLine("            Inner Join TblBOMHdr T5 On T2.BOMDocNo=T5.DocNo ");
            //SQL.AppendLine("            Inner Join TblBOMDtl2 T6 On T5.DocNo=T6.DocNo ");
            //SQL.AppendLine("            Inner Join ( ");
            //SQL.AppendLine("                Select DocNo, Min(Sequence) Sequence From TblProductionRoutingDtl Group By DocNo ");
            //SQL.AppendLine("            ) T7 On T4.DocNo=T7.DocNo And T4.Sequence=T7.Sequence ");
            //SQL.AppendLine("            Where T1.CancelInd='N' ");
            //SQL.AppendLine("            And T1.Status<>'C' ");
            //SQL.AppendLine("            And T1.DocNo In ( ");
            //SQL.AppendLine("                Select Distinct X3.ProductionOrderDocNo ");
            //SQL.AppendLine("                From TblShopFloorControlHdr X1 ");
            //SQL.AppendLine("                Inner Join TblPPHdr X2 On X1.PPDocNo=X2.DocNo ");
            //SQL.AppendLine("                Inner Join TblPPDtl X3 On X2.DocNo=X3.DocNo ");
            //SQL.AppendLine("                Where X1.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("            ) ");
            //SQL.AppendLine("            Group By T1.DocNo, T6.ItCode ");
            //SQL.AppendLine("        ) G On E.DocNo=G.DocNo And B.ItCode=G.ItCode ");
            //SQL.AppendLine("        Where A.CancelInd='N' ");
            //SQL.AppendLine("        And A.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("    ) T ");
            //SQL.AppendLine("    Group By DocNo, ProductionRoutingDocNo, ItCode, BatchNo ");
            //SQL.AppendLine(") Tbl1 ");
            //SQL.AppendLine("Inner Join TblShopFloorControlHdr Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");
            //SQL.AppendLine("Inner Join TblItem Tbl3 On Tbl1.ItCode=Tbl3.ItCode ");
            //SQL.AppendLine("Inner Join TblItemCategory Tbl4 On Tbl3.ItCtCode=Tbl4.ItCtCode ");
            //SQL.AppendLine("Inner Join TblProductionRoutingHdr Tbl5 On Tbl1.ProductionRoutingDocNo=Tbl5.DocNo ");

            #endregion

            SQL.AppendLine("Select T.SFCDocNo As DocNo, T.PPDocNo, T.RoutingDocNo, T.DocName, V.DocName As WorkCenterDocName, T.DocDt, T.ItCode, U.ItName, T.BatchNo, ");
            SQL.AppendLine("U1.ItCtName, U.InventoryUomCode, U.PlanningUomCode, T.CreateBy, T.Remark, ");
            SQL.AppendLine("IfNull(Sum(T.Qty_1), 0.00000000) Qty_1, IfNull(Sum(T.Qty_2), 0.00000000) Qty_2, IfNull(Sum(T.Qty_3), 0.00000000) Qty_3, ");
            SQL.AppendLine("IfNull(Sum(T.Qty_4), 0.00000000) Qty_4, IfNull(Sum(T.Qty_5), 0.00000000) Qty_5, IfNull(Sum(T.Qty_6), 0.00000000) Qty_6, ");
            SQL.AppendLine("0.00000000 As Value1, 0.00000000 Value2 ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select T2.DocNo SFCDocNo, T2.PPDocNo, T1.DocNo As RoutingDocNo, T1.DocName, T1.WorkCenterDocNo, T2.DocDt, T3.ItCode, ");
	        SQL.AppendLine("    T3.BatchNo, T3.Qty Qty_1, T5.Qty Qty_2, 0.00000000 As Qty_3, 0.00000000 As Qty_4, 0.00000000 As Qty_5, 0.00000000 As Qty_6, ");
            SQL.AppendLine("    T1.BOMDocNo, T2.CreateBy, T2.Remark ");
	        SQL.AppendLine("    From ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select A.DocNo, A.DocName, C.WorkCenterDocNo, C.Sequence, E.BOMDocNo ");
		    SQL.AppendLine("        From TblProductionRoutingHdr A ");
		    SQL.AppendLine("        Inner Join ");
		    SQL.AppendLine("        ( ");
			SQL.AppendLine("            Select X5.Docno, min(X5.sequence) minseq, max(X5.sequence) maxseq ");
			SQL.AppendLine("            from TblShopFloorControlHdr X1 ");
			SQL.AppendLine("            Inner Join TblPPDtl X2 On X1.PPDocNo = X2.DocNo And (X1.DocDt Between @DocDt1 And @DocDt2) And X1.CancelInd = 'N' ");
			SQL.AppendLine("            Inner Join TblProductionOrderHdr X3 On X2.ProductionOrderDocNo = X3.DocNo And X3.CancelInd = 'N' ");
			SQL.AppendLine("            Inner Join TblProductionOrderDtl X4 On X3.DocNo = X4.DocNo ");
			SQL.AppendLine("            Inner Join TblProductionRoutingDtl X5 On X3.ProductionRoutingDocNo = X5.DocNo And X4.ProductionRoutingDNo = X5.DNo ");
			SQL.AppendLine("            group by X5.Docno ");
		    SQL.AppendLine("        ) B On A.DocNo = B.DocNo And A.ActiveInd = 'Y' ");
            if (TxtProductionRoutingDocNo.Text.Length > 0)
                SQL.AppendLine("            And (A.DocNo Like @RoutingDocNo Or A.DocName Like @RoutingDocNo) ");
		    SQL.AppendLine("        Inner Join TblProductionRoutingDtl C On B.DocNo = C.DocNo And (C.Sequence = B.MinSeq) ");
		    SQL.AppendLine("        Inner Join TblProductionOrderHdr D On D.ProductionRoutingDocNo = A.DocNo ");
		    SQL.AppendLine("        Inner Join TblProductionOrderDtl E On D.DocNo = E.DocNo And E.ProductionRoutingDNo = C.DNo ");
	        SQL.AppendLine("    ) T1 ");
	        SQL.AppendLine("    Inner Join TblShopFloorControlHdr T2 On T1.WorkCenterDocNo = T2.WorkCenterDocNo And (T2.DocDt Between @DocDt1 And @DocDt2) ");
	        SQL.AppendLine("        And T2.CancelInd = 'N' ");
	        SQL.AppendLine("    Inner Join TblShopFloorControl3Dtl T3 On T2.DocNo = T3.DocNo ");
	        SQL.AppendLine("    Inner Join TblBOMHdr T4 On T1.BOMDocNo = T4.DocNo And T4.ActiveInd = 'Y' ");
	        SQL.AppendLine("    Inner Join TblBOMDtl T5 On T4.DocNo = T5.DocNo And T5.DocType = '1' And T3.ItCode = T5.DocCode ");
            	
	        SQL.AppendLine("    Union All ");

	        SQL.AppendLine("    Select T2.DocNo SFCDocNo, T2.PPDocNo, T1.DocNo As RoutingDocNo, T1.DocName, T1.WorkCenterDocNo, T2.DocDt, T3.ItCode, ");
	        SQL.AppendLine("    T3.BatchNo, 0.00000000 As Qty_1, 0.00000000 As Qty_2, 0.00000000 As Qty_3, T3.Qty Qty_4, T5.Qty Qty_5, 0.00000000 As Qty_6, ");
            SQL.AppendLine("    T1.BOMDocNo, T2.CreateBy, T2.Remark ");
	        SQL.AppendLine("    From ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select A.DocNo, A.DocName, C.WorkCenterDocNo, C.Sequence, E.BOMDocNo ");
		    SQL.AppendLine("        From TblProductionRoutingHdr A ");
		    SQL.AppendLine("        Inner Join ");
		    SQL.AppendLine("        ( ");
			SQL.AppendLine("            Select X5.Docno, min(X5.sequence) minseq, max(X5.sequence) maxseq ");
			SQL.AppendLine("            from TblShopFloorControlHdr X1 ");
			SQL.AppendLine("            Inner Join TblPPDtl X2 On X1.PPDocNo = X2.DocNo And (X1.DocDt Between @DocDt1 And @DocDt2) And X1.CancelInd = 'N' ");
			SQL.AppendLine("            Inner Join TblProductionOrderHdr X3 On X2.ProductionOrderDocNo = X3.DocNo And X3.CancelInd = 'N' ");
			SQL.AppendLine("            Inner Join TblProductionOrderDtl X4 On X3.DocNo = X4.DocNo ");
			SQL.AppendLine("            Inner Join TblProductionRoutingDtl X5 On X3.ProductionRoutingDocNo = X5.DocNo And X4.ProductionRoutingDNo = X5.DNo ");
			SQL.AppendLine("            group by X5.Docno ");
		    SQL.AppendLine("        ) B On A.DocNo = B.DocNo And A.ActiveInd = 'Y' ");
            if (TxtProductionRoutingDocNo.Text.Length > 0)
                SQL.AppendLine("            And (A.DocNo Like @RoutingDocNo Or A.DocName Like @RoutingDocNo) ");
		    SQL.AppendLine("        Inner Join TblProductionRoutingDtl C On B.DocNo = C.DocNo And (C.Sequence = B.MaxSeq) ");
		    SQL.AppendLine("        Inner Join TblProductionOrderHdr D On D.ProductionRoutingDocNo = A.DocNo ");
		    SQL.AppendLine("        Inner Join TblProductionOrderDtl E On D.DocNo = E.DocNo And E.ProductionRoutingDNo = C.DNo ");
            SQL.AppendLine("    ) T1 ");
	        SQL.AppendLine("    Inner Join TblShopFloorControlHdr T2 On T1.WorkCenterDocNo = T2.WorkCenterDocNo And (T2.DocDt Between @DocDt1 And @DocDt2) ");
	        SQL.AppendLine("        And T2.CancelInd = 'N' ");
	        SQL.AppendLine("    Inner Join TblShopFloorControlDtl T3 On T2.DocNo = T3.DocNo ");
	        SQL.AppendLine("    Inner Join TblBOMHdr T4 On T1.BOMDocNo = T4.DocNo And T4.ActiveInd = 'Y' ");
	        SQL.AppendLine("    Inner Join TblBOMDtl2 T5 On T4.DocNo = T5.DocNo And T3.ItCode = T5.ItCode ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Inner Join TblItem U On T.ItCode = U.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory U1 On U.ItCtCode = U1.ItCtCode ");
            SQL.AppendLine("Inner Join TblWorkCenterHdr V On T.WorkCenterDocNo=V.DocNo ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "SFC#", 
                    "Planned#",
                    "Routing",
                    "Work Center",
                    "Date",

                    //6-10
                    "Item's"+Environment.NewLine+"Code",
                    "Item's Name",
                    "Category",
                    "Batch#", 
                    "UoM" + Environment.NewLine+"(Inventory)",

                    //11-15
                    "UoM" + Environment.NewLine+"(Planned)",
                    "Material"+Environment.NewLine+"(BOM)",
                    "Material"+Environment.NewLine+"(SFC)",
                    "Result"+Environment.NewLine+"(BOM)",
                    "Result"+Environment.NewLine+"(SFC)",

                    //16-17
                    "Remark",
                    "Created By"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 150, 150, 200, 80, 
                    
                    //6-10
                    80, 200, 180, 180, 100, 

                    //11-15
                    100, 120, 120, 120, 120, 
                    
                    //16-17
                    200, 120
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14, 15 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 6, 8, 16, 17 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 8, 16, 17 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@RoutingDocNo", string.Concat("%", TxtProductionRoutingDocNo.Text, "%"));

                Sm.FilterStr(ref Filter, ref cm, TxtShopFloorControlDocNo.Text, "T.SFCDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtPPDocNo.Text, "T.PPDocNo", false);
                //Sm.FilterStr(ref Filter, ref cm, TxtProductionRoutingDocNo.Text, new string[] { "T.RoutingDocNo", "T.DocName" });
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "U.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "U.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueInventoryUomCode), "U.InventoryUomCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePlanningUomCode), "U.PlanningUomCode", true);
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + " Group By T.SFCDocNo, T.PPDocNo, T.RoutingDocNo, T.DocName, V.DocName, T.DocDt, T.ItCode, U.ItName, T.BatchNo, U1.ItCtName, U.InventoryUomCode, U.PlanningUomCode, T.CreateBy, T.Remark Order By T.DocDt, T.SFCDocNo; ",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "PPDocNo", "DocName", "WorkCenterDocName", "DocDt", "ItCode", 

                        //6-10
                        "ItName", "ItCtName", "BatchNo", "InventoryUomCode", "PlanningUomCode", 

                        //11-15
                        "Qty_1", "Qty_2", "Qty_3", "Qty_4", "Remark", 

                        //16
                        "CreateBy"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[]{ 12, 13, 14, 15 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true); 
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtShopFloorControlDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkShopFloorControlDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SFC#");
        }

        private void TxtPPDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPPDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Planned#");
        }

        private void TxtProductionRoutingDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProductionRoutingDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Routing#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        private void LueInventoryUomCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueInventoryUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkInventoryUomCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "UoM (Inventory)");
        }

        private void LuePlanningUomCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePlanningUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPlanningUomCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "UoM (Planned)");
        }
      
        #endregion

        #endregion
    }
}
