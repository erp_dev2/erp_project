﻿#region Update
/*
    04/10/2021 [WED/PHT] new apps
    21/10/2021 [IBL/PHT] Dokumen CBP yg sudah ditarik di CBP Revision yg masih Outstanding tidak dimunculkan.
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCompanyBudgetPlan2RevDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmCompanyBudgetPlan2Rev mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmCompanyBudgetPlan2RevDlg3(FrmCompanyBudgetPlan2Rev FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                mFrmParent.SetLueCCCode(ref LueCCCode, string.Empty);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Document#", 
                    "Date", 
                    "Year",
                    "Cost Center Code",
                    "Cost Center",

                    //6-8
                    "Type",
                    "Budget Request#",
                    "Origin Document#"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 80, 100, 120, 200, 

                    //6-8
                    0, 180, 180
                }
            );
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.Yr, A.CCCode, B.CCName, A.DocType, A.BudgetRequestDocNo, ");
            SQL.AppendLine("Substring_Index(A.DocNo, '/', 5) As OriginDocNo ");
            SQL.AppendLine("From TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
            SQL.AppendLine("Where A.CompletedInd = 'Y' ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("And A.Status = 'A' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsCompanyBudgetPlanBalanceSheet) SQL.AppendLine("And A.DocType = '3' ");
            else SQL.AppendLine(" And A.DocType = '2' ");
            SQL.AppendLine("And A.DocNo Not In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct(OriginDocNo) ");
            SQL.AppendLine("    From TblCompanyBudgetPlanHdr ");
            SQL.AppendLine("    Where CompletedInd = 'Y' ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    And IfNull(Status, 'O') = 'O' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblCompanyBudgetPlanHdr ");
            SQL.AppendLine("    Where CompletedInd = 'Y' ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    And IfNull(Status, 'O') = 'O' ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "A.CCCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL.ToString() + Filter + " Order By A.CreateDt Desc;",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "Yr", "CCCode", "CCName", "DocType", 
                        "BudgetRequestDocNo", "OriginDocNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtOriginDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 8);
                Sm.SetLue(mFrmParent.LueYr, Sm.GetGrdStr(Grd1, Row, 3));
                Sm.SetLue(mFrmParent.LueCCCode, Sm.GetGrdStr(Grd1, Row, 4));
                mFrmParent.TxtBudgetRequestDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                mFrmParent.GetDetailData(Sm.GetGrdStr(Grd1, Row, 1));
                mFrmParent.mDocType = Sm.GetGrdStr(Grd1, Row, 6);
                
                this.Close();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion        

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(mFrmParent.SetLueCCCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");
        }

        #endregion

        #endregion

    }
}
