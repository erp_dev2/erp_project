﻿namespace RunSystem
{
    partial class FrmTO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTO));
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtDisplayName = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.LueInfo = new DevExpress.XtraEditors.LookUpEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.BtnAsset = new DevExpress.XtraEditors.SimpleButton();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteEnd = new DevExpress.XtraEditors.DateEdit();
            this.label96 = new System.Windows.Forms.Label();
            this.DteStart = new DevExpress.XtraEditors.DateEdit();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.TxtAssetCtCode = new DevExpress.XtraEditors.TextEdit();
            this.label66 = new System.Windows.Forms.Label();
            this.TxtAssetName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtAssetCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TpgOrganization = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.TxtCatalog = new DevExpress.XtraEditors.TextEdit();
            this.label122 = new System.Windows.Forms.Label();
            this.TxtWorkcenter = new DevExpress.XtraEditors.TextEdit();
            this.label116 = new System.Windows.Forms.Label();
            this.TxtPlannerGroup = new DevExpress.XtraEditors.TextEdit();
            this.label117 = new System.Windows.Forms.Label();
            this.TxtPlanningPlant = new DevExpress.XtraEditors.TextEdit();
            this.label118 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.TxtWbs = new DevExpress.XtraEditors.TextEdit();
            this.label115 = new System.Windows.Forms.Label();
            this.LueCCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label113 = new System.Windows.Forms.Label();
            this.TxtAsset = new DevExpress.XtraEditors.TextEdit();
            this.label119 = new System.Windows.Forms.Label();
            this.TxtArea = new DevExpress.XtraEditors.TextEdit();
            this.label120 = new System.Windows.Forms.Label();
            this.TxtCompany = new DevExpress.XtraEditors.TextEdit();
            this.label121 = new System.Windows.Forms.Label();
            this.TpLocation = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label112 = new System.Windows.Forms.Label();
            this.LueLocationCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgGeneral = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.TxtConstMth = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt = new DevExpress.XtraEditors.TextEdit();
            this.label111 = new System.Windows.Forms.Label();
            this.TxtConstYr = new DevExpress.XtraEditors.TextEdit();
            this.TxtSerial = new DevExpress.XtraEditors.TextEdit();
            this.label110 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.TxtPartNo = new DevExpress.XtraEditors.TextEdit();
            this.label107 = new System.Windows.Forms.Label();
            this.TxtModel = new DevExpress.XtraEditors.TextEdit();
            this.label108 = new System.Windows.Forms.Label();
            this.TxtManufacture = new DevExpress.XtraEditors.TextEdit();
            this.label109 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.DteAcquisitionDt = new DevExpress.XtraEditors.DateEdit();
            this.label106 = new System.Windows.Forms.Label();
            this.TxtAcquisition = new DevExpress.XtraEditors.TextEdit();
            this.label105 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.TxtInventoryNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtShift = new DevExpress.XtraEditors.TextEdit();
            this.label104 = new System.Windows.Forms.Label();
            this.DteStartUpDt = new DevExpress.XtraEditors.DateEdit();
            this.label103 = new System.Windows.Forms.Label();
            this.TxtSize = new DevExpress.XtraEditors.TextEdit();
            this.label97 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.LueWeightUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtWeight = new DevExpress.XtraEditors.TextEdit();
            this.label98 = new System.Windows.Forms.Label();
            this.TxtAuthorize = new DevExpress.XtraEditors.TextEdit();
            this.label99 = new System.Windows.Forms.Label();
            this.TxtObjectType = new DevExpress.XtraEditors.TextEdit();
            this.label100 = new System.Windows.Forms.Label();
            this.TxtClass = new DevExpress.XtraEditors.TextEdit();
            this.label101 = new System.Windows.Forms.Label();
            this.TpgTO = new System.Windows.Forms.TabControl();
            this.TpgStructure = new System.Windows.Forms.TabPage();
            this.panel11 = new System.Windows.Forms.Panel();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.LueCapacityUom = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtCapacity = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.MeeDesc2 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeDesc = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtConsType = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtTechnical = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtPosition = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtSuperorder = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtFunctional = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInfo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEnd.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStart.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetCode.Properties)).BeginInit();
            this.TpgOrganization.SuspendLayout();
            this.panel10.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCatalog.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkcenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlannerGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlanningPlant.Properties)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWbs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtArea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompany.Properties)).BeginInit();
            this.TpLocation.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLocationCode.Properties)).BeginInit();
            this.TpgGeneral.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtConstMth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtConstYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSerial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPartNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtManufacture.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteAcquisitionDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteAcquisitionDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcquisition.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShift.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartUpDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartUpDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWeightUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAuthorize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtObjectType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtClass.Properties)).BeginInit();
            this.TpgTO.SuspendLayout();
            this.TpgStructure.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCapacityUom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCapacity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDesc2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtConsType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTechnical.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSuperorder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFunctional.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 514);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TpgTO);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 514);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.TxtDisplayName);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.LueInfo);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.LueStatus);
            this.panel3.Controls.Add(this.MeeRemark);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.BtnAsset);
            this.panel3.Controls.Add(this.ChkActInd);
            this.panel3.Controls.Add(this.DteEnd);
            this.panel3.Controls.Add(this.label96);
            this.panel3.Controls.Add(this.DteStart);
            this.panel3.Controls.Add(this.label95);
            this.panel3.Controls.Add(this.label94);
            this.panel3.Controls.Add(this.TxtAssetCtCode);
            this.panel3.Controls.Add(this.label66);
            this.panel3.Controls.Add(this.TxtAssetName);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtAssetCode);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 180);
            this.panel3.TabIndex = 12;
            // 
            // TxtDisplayName
            // 
            this.TxtDisplayName.EnterMoveNextControl = true;
            this.TxtDisplayName.Location = new System.Drawing.Point(128, 47);
            this.TxtDisplayName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDisplayName.Name = "TxtDisplayName";
            this.TxtDisplayName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDisplayName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDisplayName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDisplayName.Properties.Appearance.Options.UseFont = true;
            this.TxtDisplayName.Properties.MaxLength = 16;
            this.TxtDisplayName.Size = new System.Drawing.Size(517, 20);
            this.TxtDisplayName.TabIndex = 20;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(10, 50);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(112, 14);
            this.label14.TabIndex = 19;
            this.label14.Text = "Asset Display Name";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInfo
            // 
            this.LueInfo.EnterMoveNextControl = true;
            this.LueInfo.Location = new System.Drawing.Point(128, 131);
            this.LueInfo.Margin = new System.Windows.Forms.Padding(5);
            this.LueInfo.Name = "LueInfo";
            this.LueInfo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInfo.Properties.Appearance.Options.UseFont = true;
            this.LueInfo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInfo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInfo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInfo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInfo.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInfo.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInfo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInfo.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInfo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInfo.Properties.DropDownRows = 20;
            this.LueInfo.Properties.NullText = "[Empty]";
            this.LueInfo.Properties.PopupWidth = 500;
            this.LueInfo.Size = new System.Drawing.Size(106, 20);
            this.LueInfo.TabIndex = 30;
            this.LueInfo.ToolTip = "F4 : Show/hide list";
            this.LueInfo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueInfo.EditValueChanged += new System.EventHandler(this.LueInfo_EditValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(93, 134);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 14);
            this.label12.TabIndex = 29;
            this.label12.Text = "Info";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueStatus
            // 
            this.LueStatus.EnterMoveNextControl = true;
            this.LueStatus.Location = new System.Drawing.Point(128, 89);
            this.LueStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueStatus.Name = "LueStatus";
            this.LueStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.Appearance.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStatus.Properties.DropDownRows = 20;
            this.LueStatus.Properties.NullText = "[Empty]";
            this.LueStatus.Properties.PopupWidth = 500;
            this.LueStatus.Size = new System.Drawing.Size(278, 20);
            this.LueStatus.TabIndex = 24;
            this.LueStatus.ToolTip = "F4 : Show/hide list";
            this.LueStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStatus.EditValueChanged += new System.EventHandler(this.LueStatus_EditValueChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(128, 152);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(517, 20);
            this.MeeRemark.TabIndex = 28;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(75, 155);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 14);
            this.label11.TabIndex = 27;
            this.label11.Text = "Remark";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAsset
            // 
            this.BtnAsset.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAsset.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAsset.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAsset.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAsset.Appearance.Options.UseBackColor = true;
            this.BtnAsset.Appearance.Options.UseFont = true;
            this.BtnAsset.Appearance.Options.UseForeColor = true;
            this.BtnAsset.Appearance.Options.UseTextOptions = true;
            this.BtnAsset.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAsset.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAsset.Image = ((System.Drawing.Image)(resources.GetObject("BtnAsset.Image")));
            this.BtnAsset.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAsset.Location = new System.Drawing.Point(413, 5);
            this.BtnAsset.Name = "BtnAsset";
            this.BtnAsset.Size = new System.Drawing.Size(24, 21);
            this.BtnAsset.TabIndex = 15;
            this.BtnAsset.ToolTip = "Find Asset";
            this.BtnAsset.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAsset.ToolTipTitle = "Run System";
            this.BtnAsset.Click += new System.EventHandler(this.BtnEquip_Click);
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(441, 4);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(76, 22);
            this.ChkActInd.TabIndex = 16;
            // 
            // DteEnd
            // 
            this.DteEnd.EditValue = null;
            this.DteEnd.EnterMoveNextControl = true;
            this.DteEnd.Location = new System.Drawing.Point(263, 109);
            this.DteEnd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEnd.Name = "DteEnd";
            this.DteEnd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEnd.Properties.Appearance.Options.UseFont = true;
            this.DteEnd.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEnd.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEnd.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEnd.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEnd.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEnd.Properties.MaxLength = 16;
            this.DteEnd.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEnd.Size = new System.Drawing.Size(105, 20);
            this.DteEnd.TabIndex = 28;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.Black;
            this.label96.Location = new System.Drawing.Point(238, 111);
            this.label96.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(22, 14);
            this.label96.TabIndex = 27;
            this.label96.Text = "To";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteStart
            // 
            this.DteStart.EditValue = null;
            this.DteStart.EnterMoveNextControl = true;
            this.DteStart.Location = new System.Drawing.Point(128, 110);
            this.DteStart.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStart.Name = "DteStart";
            this.DteStart.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStart.Properties.Appearance.Options.UseFont = true;
            this.DteStart.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStart.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStart.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStart.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStart.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStart.Properties.MaxLength = 16;
            this.DteStart.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStart.Size = new System.Drawing.Size(105, 20);
            this.DteStart.TabIndex = 26;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.Red;
            this.label95.Location = new System.Drawing.Point(59, 112);
            this.label95.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(63, 14);
            this.label95.TabIndex = 25;
            this.label95.Text = "Valid From";
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Red;
            this.label94.Location = new System.Drawing.Point(80, 91);
            this.label94.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(42, 14);
            this.label94.TabIndex = 23;
            this.label94.Text = "Status";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAssetCtCode
            // 
            this.TxtAssetCtCode.EnterMoveNextControl = true;
            this.TxtAssetCtCode.Location = new System.Drawing.Point(128, 68);
            this.TxtAssetCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAssetCtCode.Name = "TxtAssetCtCode";
            this.TxtAssetCtCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAssetCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAssetCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAssetCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtAssetCtCode.Properties.MaxLength = 13;
            this.TxtAssetCtCode.Size = new System.Drawing.Size(278, 20);
            this.TxtAssetCtCode.TabIndex = 22;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(66, 69);
            this.label66.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(56, 14);
            this.label66.TabIndex = 21;
            this.label66.Text = "Category";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAssetName
            // 
            this.TxtAssetName.EnterMoveNextControl = true;
            this.TxtAssetName.Location = new System.Drawing.Point(128, 26);
            this.TxtAssetName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAssetName.Name = "TxtAssetName";
            this.TxtAssetName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAssetName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAssetName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAssetName.Properties.Appearance.Options.UseFont = true;
            this.TxtAssetName.Properties.MaxLength = 40;
            this.TxtAssetName.Size = new System.Drawing.Size(517, 20);
            this.TxtAssetName.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(55, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 14);
            this.label2.TabIndex = 17;
            this.label2.Text = "Description";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAssetCode
            // 
            this.TxtAssetCode.EnterMoveNextControl = true;
            this.TxtAssetCode.Location = new System.Drawing.Point(128, 5);
            this.TxtAssetCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAssetCode.Name = "TxtAssetCode";
            this.TxtAssetCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAssetCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAssetCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAssetCode.Properties.Appearance.Options.UseFont = true;
            this.TxtAssetCode.Properties.MaxLength = 16;
            this.TxtAssetCode.Size = new System.Drawing.Size(278, 20);
            this.TxtAssetCode.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(53, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "Asset Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgOrganization
            // 
            this.TpgOrganization.Controls.Add(this.panel10);
            this.TpgOrganization.Location = new System.Drawing.Point(4, 26);
            this.TpgOrganization.Name = "TpgOrganization";
            this.TpgOrganization.Padding = new System.Windows.Forms.Padding(3);
            this.TpgOrganization.Size = new System.Drawing.Size(764, 23);
            this.TpgOrganization.TabIndex = 2;
            this.TpgOrganization.Text = "Organization";
            this.TpgOrganization.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel10.Controls.Add(this.groupBox5);
            this.panel10.Controls.Add(this.groupBox4);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel10.Location = new System.Drawing.Point(3, 3);
            this.panel10.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(758, 17);
            this.panel10.TabIndex = 12;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.TxtCatalog);
            this.groupBox5.Controls.Add(this.label122);
            this.groupBox5.Controls.Add(this.TxtWorkcenter);
            this.groupBox5.Controls.Add(this.label116);
            this.groupBox5.Controls.Add(this.TxtPlannerGroup);
            this.groupBox5.Controls.Add(this.label117);
            this.groupBox5.Controls.Add(this.TxtPlanningPlant);
            this.groupBox5.Controls.Add(this.label118);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(0, 145);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(754, 0);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Responsibilities";
            // 
            // TxtCatalog
            // 
            this.TxtCatalog.EnterMoveNextControl = true;
            this.TxtCatalog.Location = new System.Drawing.Point(115, 86);
            this.TxtCatalog.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCatalog.Name = "TxtCatalog";
            this.TxtCatalog.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCatalog.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCatalog.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCatalog.Properties.Appearance.Options.UseFont = true;
            this.TxtCatalog.Properties.MaxLength = 16;
            this.TxtCatalog.Size = new System.Drawing.Size(133, 20);
            this.TxtCatalog.TabIndex = 82;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.ForeColor = System.Drawing.Color.Black;
            this.label122.Location = new System.Drawing.Point(23, 89);
            this.label122.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(84, 14);
            this.label122.TabIndex = 81;
            this.label122.Text = "Catalog Profile";
            this.label122.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWorkcenter
            // 
            this.TxtWorkcenter.EnterMoveNextControl = true;
            this.TxtWorkcenter.Location = new System.Drawing.Point(115, 65);
            this.TxtWorkcenter.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWorkcenter.Name = "TxtWorkcenter";
            this.TxtWorkcenter.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWorkcenter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWorkcenter.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWorkcenter.Properties.Appearance.Options.UseFont = true;
            this.TxtWorkcenter.Properties.MaxLength = 16;
            this.TxtWorkcenter.Size = new System.Drawing.Size(133, 20);
            this.TxtWorkcenter.TabIndex = 80;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.ForeColor = System.Drawing.Color.Black;
            this.label116.Location = new System.Drawing.Point(7, 68);
            this.label116.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(100, 14);
            this.label116.TabIndex = 79;
            this.label116.Text = "Main Workcenter";
            this.label116.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPlannerGroup
            // 
            this.TxtPlannerGroup.EnterMoveNextControl = true;
            this.TxtPlannerGroup.Location = new System.Drawing.Point(115, 44);
            this.TxtPlannerGroup.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPlannerGroup.Name = "TxtPlannerGroup";
            this.TxtPlannerGroup.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPlannerGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPlannerGroup.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPlannerGroup.Properties.Appearance.Options.UseFont = true;
            this.TxtPlannerGroup.Properties.MaxLength = 16;
            this.TxtPlannerGroup.Size = new System.Drawing.Size(133, 20);
            this.TxtPlannerGroup.TabIndex = 78;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.ForeColor = System.Drawing.Color.Black;
            this.label117.Location = new System.Drawing.Point(23, 46);
            this.label117.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(84, 14);
            this.label117.TabIndex = 77;
            this.label117.Text = "Planner Group";
            this.label117.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPlanningPlant
            // 
            this.TxtPlanningPlant.EnterMoveNextControl = true;
            this.TxtPlanningPlant.Location = new System.Drawing.Point(115, 23);
            this.TxtPlanningPlant.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPlanningPlant.Name = "TxtPlanningPlant";
            this.TxtPlanningPlant.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPlanningPlant.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPlanningPlant.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPlanningPlant.Properties.Appearance.Options.UseFont = true;
            this.TxtPlanningPlant.Properties.MaxLength = 16;
            this.TxtPlanningPlant.Size = new System.Drawing.Size(133, 20);
            this.TxtPlanningPlant.TabIndex = 76;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.ForeColor = System.Drawing.Color.Black;
            this.label118.Location = new System.Drawing.Point(24, 26);
            this.label118.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(83, 14);
            this.label118.TabIndex = 75;
            this.label118.Text = "Planning Plant";
            this.label118.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.TxtWbs);
            this.groupBox4.Controls.Add(this.label115);
            this.groupBox4.Controls.Add(this.LueCCCode);
            this.groupBox4.Controls.Add(this.label113);
            this.groupBox4.Controls.Add(this.TxtAsset);
            this.groupBox4.Controls.Add(this.label119);
            this.groupBox4.Controls.Add(this.TxtArea);
            this.groupBox4.Controls.Add(this.label120);
            this.groupBox4.Controls.Add(this.TxtCompany);
            this.groupBox4.Controls.Add(this.label121);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(754, 145);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Account Assignment";
            // 
            // TxtWbs
            // 
            this.TxtWbs.EnterMoveNextControl = true;
            this.TxtWbs.Location = new System.Drawing.Point(115, 109);
            this.TxtWbs.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWbs.Name = "TxtWbs";
            this.TxtWbs.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWbs.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWbs.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWbs.Properties.Appearance.Options.UseFont = true;
            this.TxtWbs.Properties.MaxLength = 16;
            this.TxtWbs.Size = new System.Drawing.Size(257, 20);
            this.TxtWbs.TabIndex = 74;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.ForeColor = System.Drawing.Color.Black;
            this.label115.Location = new System.Drawing.Point(25, 110);
            this.label115.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(82, 14);
            this.label115.TabIndex = 73;
            this.label115.Text = "WBS Element";
            this.label115.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCCCode
            // 
            this.LueCCCode.EnterMoveNextControl = true;
            this.LueCCCode.Location = new System.Drawing.Point(115, 88);
            this.LueCCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCCCode.Name = "LueCCCode";
            this.LueCCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.Appearance.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCCCode.Properties.DropDownRows = 20;
            this.LueCCCode.Properties.NullText = "[Empty]";
            this.LueCCCode.Properties.PopupWidth = 500;
            this.LueCCCode.Size = new System.Drawing.Size(257, 20);
            this.LueCCCode.TabIndex = 72;
            this.LueCCCode.ToolTip = "F4 : Show/hide list";
            this.LueCCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCCCode.EditValueChanged += new System.EventHandler(this.LueCCCode_EditValueChanged);
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.Black;
            this.label113.Location = new System.Drawing.Point(40, 91);
            this.label113.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(67, 14);
            this.label113.TabIndex = 71;
            this.label113.Text = "Costcenter";
            this.label113.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAsset
            // 
            this.TxtAsset.EnterMoveNextControl = true;
            this.TxtAsset.Location = new System.Drawing.Point(115, 67);
            this.TxtAsset.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAsset.Name = "TxtAsset";
            this.TxtAsset.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAsset.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAsset.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAsset.Properties.Appearance.Options.UseFont = true;
            this.TxtAsset.Properties.MaxLength = 16;
            this.TxtAsset.Size = new System.Drawing.Size(257, 20);
            this.TxtAsset.TabIndex = 70;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.ForeColor = System.Drawing.Color.Black;
            this.label119.Location = new System.Drawing.Point(70, 70);
            this.label119.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(37, 14);
            this.label119.TabIndex = 69;
            this.label119.Text = "Asset";
            this.label119.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtArea
            // 
            this.TxtArea.EnterMoveNextControl = true;
            this.TxtArea.Location = new System.Drawing.Point(115, 46);
            this.TxtArea.Margin = new System.Windows.Forms.Padding(5);
            this.TxtArea.Name = "TxtArea";
            this.TxtArea.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtArea.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtArea.Properties.Appearance.Options.UseBackColor = true;
            this.TxtArea.Properties.Appearance.Options.UseFont = true;
            this.TxtArea.Properties.MaxLength = 16;
            this.TxtArea.Size = new System.Drawing.Size(257, 20);
            this.TxtArea.TabIndex = 68;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.ForeColor = System.Drawing.Color.Black;
            this.label120.Location = new System.Drawing.Point(26, 48);
            this.label120.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(81, 14);
            this.label120.TabIndex = 67;
            this.label120.Text = "Bussines Area";
            this.label120.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCompany
            // 
            this.TxtCompany.EnterMoveNextControl = true;
            this.TxtCompany.Location = new System.Drawing.Point(115, 25);
            this.TxtCompany.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCompany.Name = "TxtCompany";
            this.TxtCompany.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCompany.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCompany.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCompany.Properties.Appearance.Options.UseFont = true;
            this.TxtCompany.Properties.MaxLength = 16;
            this.TxtCompany.Size = new System.Drawing.Size(257, 20);
            this.TxtCompany.TabIndex = 66;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.ForeColor = System.Drawing.Color.Black;
            this.label121.Location = new System.Drawing.Point(50, 28);
            this.label121.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(57, 14);
            this.label121.TabIndex = 65;
            this.label121.Text = "Company";
            this.label121.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpLocation
            // 
            this.TpLocation.Controls.Add(this.panel9);
            this.TpLocation.Location = new System.Drawing.Point(4, 26);
            this.TpLocation.Name = "TpLocation";
            this.TpLocation.Padding = new System.Windows.Forms.Padding(3);
            this.TpLocation.Size = new System.Drawing.Size(764, 23);
            this.TpLocation.TabIndex = 1;
            this.TpLocation.Text = "Location";
            this.TpLocation.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.label112);
            this.panel9.Controls.Add(this.LueLocationCode);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(758, 17);
            this.panel9.TabIndex = 11;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.ForeColor = System.Drawing.Color.Black;
            this.label112.Location = new System.Drawing.Point(14, 17);
            this.label112.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(53, 14);
            this.label112.TabIndex = 63;
            this.label112.Text = "Location";
            this.label112.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLocationCode
            // 
            this.LueLocationCode.EnterMoveNextControl = true;
            this.LueLocationCode.Location = new System.Drawing.Point(73, 15);
            this.LueLocationCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueLocationCode.Name = "LueLocationCode";
            this.LueLocationCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocationCode.Properties.Appearance.Options.UseFont = true;
            this.LueLocationCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocationCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLocationCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocationCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLocationCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocationCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLocationCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocationCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLocationCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLocationCode.Properties.DropDownRows = 20;
            this.LueLocationCode.Properties.NullText = "[Empty]";
            this.LueLocationCode.Properties.PopupWidth = 500;
            this.LueLocationCode.Size = new System.Drawing.Size(319, 20);
            this.LueLocationCode.TabIndex = 64;
            this.LueLocationCode.ToolTip = "F4 : Show/hide list";
            this.LueLocationCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLocationCode.EditValueChanged += new System.EventHandler(this.LueLocationCode_EditValueChanged);
            // 
            // TpgGeneral
            // 
            this.TpgGeneral.Controls.Add(this.panel6);
            this.TpgGeneral.Location = new System.Drawing.Point(4, 26);
            this.TpgGeneral.Name = "TpgGeneral";
            this.TpgGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.TpgGeneral.Size = new System.Drawing.Size(764, 304);
            this.TpgGeneral.TabIndex = 0;
            this.TpgGeneral.Text = "General";
            this.TpgGeneral.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.groupBox3);
            this.panel6.Controls.Add(this.groupBox2);
            this.panel6.Controls.Add(this.groupBox1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(758, 298);
            this.panel6.TabIndex = 11;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.panel8);
            this.groupBox3.Controls.Add(this.TxtPartNo);
            this.groupBox3.Controls.Add(this.label107);
            this.groupBox3.Controls.Add(this.TxtModel);
            this.groupBox3.Controls.Add(this.label108);
            this.groupBox3.Controls.Add(this.TxtManufacture);
            this.groupBox3.Controls.Add(this.label109);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(0, 198);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(754, 96);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Manufacture Data";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.TxtConstMth);
            this.panel8.Controls.Add(this.TxtCnt);
            this.panel8.Controls.Add(this.label111);
            this.panel8.Controls.Add(this.TxtConstYr);
            this.panel8.Controls.Add(this.TxtSerial);
            this.panel8.Controls.Add(this.label110);
            this.panel8.Controls.Add(this.label114);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(367, 18);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(384, 75);
            this.panel8.TabIndex = 59;
            // 
            // TxtConstMth
            // 
            this.TxtConstMth.EnterMoveNextControl = true;
            this.TxtConstMth.Location = new System.Drawing.Point(185, 46);
            this.TxtConstMth.Margin = new System.Windows.Forms.Padding(5);
            this.TxtConstMth.Name = "TxtConstMth";
            this.TxtConstMth.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtConstMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtConstMth.Properties.Appearance.Options.UseBackColor = true;
            this.TxtConstMth.Properties.Appearance.Options.UseFont = true;
            this.TxtConstMth.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtConstMth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtConstMth.Properties.MaxLength = 2;
            this.TxtConstMth.Size = new System.Drawing.Size(40, 20);
            this.TxtConstMth.TabIndex = 62;
            // 
            // TxtCnt
            // 
            this.TxtCnt.EnterMoveNextControl = true;
            this.TxtCnt.Location = new System.Drawing.Point(120, 25);
            this.TxtCnt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt.Name = "TxtCnt";
            this.TxtCnt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt.Properties.MaxLength = 16;
            this.TxtCnt.Size = new System.Drawing.Size(257, 20);
            this.TxtCnt.TabIndex = 59;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.ForeColor = System.Drawing.Color.Black;
            this.label111.Location = new System.Drawing.Point(65, 27);
            this.label111.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(50, 14);
            this.label111.TabIndex = 58;
            this.label111.Text = "Country";
            this.label111.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtConstYr
            // 
            this.TxtConstYr.EnterMoveNextControl = true;
            this.TxtConstYr.Location = new System.Drawing.Point(120, 46);
            this.TxtConstYr.Margin = new System.Windows.Forms.Padding(5);
            this.TxtConstYr.Name = "TxtConstYr";
            this.TxtConstYr.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtConstYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtConstYr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtConstYr.Properties.Appearance.Options.UseFont = true;
            this.TxtConstYr.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtConstYr.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtConstYr.Properties.MaxLength = 4;
            this.TxtConstYr.Size = new System.Drawing.Size(60, 20);
            this.TxtConstYr.TabIndex = 61;
            // 
            // TxtSerial
            // 
            this.TxtSerial.EnterMoveNextControl = true;
            this.TxtSerial.Location = new System.Drawing.Point(120, 4);
            this.TxtSerial.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSerial.Name = "TxtSerial";
            this.TxtSerial.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSerial.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSerial.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSerial.Properties.Appearance.Options.UseFont = true;
            this.TxtSerial.Properties.MaxLength = 16;
            this.TxtSerial.Size = new System.Drawing.Size(257, 20);
            this.TxtSerial.TabIndex = 57;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.ForeColor = System.Drawing.Color.Black;
            this.label110.Location = new System.Drawing.Point(80, 6);
            this.label110.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(35, 14);
            this.label110.TabIndex = 56;
            this.label110.Text = "Serial";
            this.label110.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.ForeColor = System.Drawing.Color.Black;
            this.label114.Location = new System.Drawing.Point(1, 50);
            this.label114.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(114, 14);
            this.label114.TabIndex = 60;
            this.label114.Text = "Const year / month";
            this.label114.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPartNo
            // 
            this.TxtPartNo.EnterMoveNextControl = true;
            this.TxtPartNo.Location = new System.Drawing.Point(106, 64);
            this.TxtPartNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPartNo.Name = "TxtPartNo";
            this.TxtPartNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPartNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPartNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPartNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPartNo.Properties.MaxLength = 16;
            this.TxtPartNo.Size = new System.Drawing.Size(257, 20);
            this.TxtPartNo.TabIndex = 55;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.Black;
            this.label107.Location = new System.Drawing.Point(49, 67);
            this.label107.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(48, 14);
            this.label107.TabIndex = 54;
            this.label107.Text = "Part No";
            this.label107.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtModel
            // 
            this.TxtModel.EnterMoveNextControl = true;
            this.TxtModel.Location = new System.Drawing.Point(106, 43);
            this.TxtModel.Margin = new System.Windows.Forms.Padding(5);
            this.TxtModel.Name = "TxtModel";
            this.TxtModel.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtModel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtModel.Properties.Appearance.Options.UseBackColor = true;
            this.TxtModel.Properties.Appearance.Options.UseFont = true;
            this.TxtModel.Properties.MaxLength = 16;
            this.TxtModel.Size = new System.Drawing.Size(257, 20);
            this.TxtModel.TabIndex = 53;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.ForeColor = System.Drawing.Color.Black;
            this.label108.Location = new System.Drawing.Point(11, 45);
            this.label108.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(86, 14);
            this.label108.TabIndex = 52;
            this.label108.Text = "Model Number";
            this.label108.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtManufacture
            // 
            this.TxtManufacture.EnterMoveNextControl = true;
            this.TxtManufacture.Location = new System.Drawing.Point(106, 22);
            this.TxtManufacture.Margin = new System.Windows.Forms.Padding(5);
            this.TxtManufacture.Name = "TxtManufacture";
            this.TxtManufacture.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtManufacture.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtManufacture.Properties.Appearance.Options.UseBackColor = true;
            this.TxtManufacture.Properties.Appearance.Options.UseFont = true;
            this.TxtManufacture.Properties.MaxLength = 16;
            this.TxtManufacture.Size = new System.Drawing.Size(257, 20);
            this.TxtManufacture.TabIndex = 51;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.ForeColor = System.Drawing.Color.Black;
            this.label109.Location = new System.Drawing.Point(22, 24);
            this.label109.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(75, 14);
            this.label109.TabIndex = 50;
            this.label109.Text = "Manufacture";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.DteAcquisitionDt);
            this.groupBox2.Controls.Add(this.label106);
            this.groupBox2.Controls.Add(this.TxtAcquisition);
            this.groupBox2.Controls.Add(this.label105);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(0, 118);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(754, 80);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Refference Data";
            // 
            // DteAcquisitionDt
            // 
            this.DteAcquisitionDt.EditValue = null;
            this.DteAcquisitionDt.EnterMoveNextControl = true;
            this.DteAcquisitionDt.Location = new System.Drawing.Point(108, 44);
            this.DteAcquisitionDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteAcquisitionDt.Name = "DteAcquisitionDt";
            this.DteAcquisitionDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteAcquisitionDt.Properties.Appearance.Options.UseFont = true;
            this.DteAcquisitionDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteAcquisitionDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteAcquisitionDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteAcquisitionDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteAcquisitionDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteAcquisitionDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteAcquisitionDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteAcquisitionDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteAcquisitionDt.Properties.MaxLength = 16;
            this.DteAcquisitionDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteAcquisitionDt.Size = new System.Drawing.Size(105, 20);
            this.DteAcquisitionDt.TabIndex = 49;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.Black;
            this.label106.Location = new System.Drawing.Point(10, 47);
            this.label106.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(95, 14);
            this.label106.TabIndex = 48;
            this.label106.Text = "Acquisition Date";
            this.label106.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcquisition
            // 
            this.TxtAcquisition.EnterMoveNextControl = true;
            this.TxtAcquisition.Location = new System.Drawing.Point(108, 23);
            this.TxtAcquisition.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcquisition.Name = "TxtAcquisition";
            this.TxtAcquisition.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcquisition.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcquisition.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcquisition.Properties.Appearance.Options.UseFont = true;
            this.TxtAcquisition.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAcquisition.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAcquisition.Size = new System.Drawing.Size(151, 20);
            this.TxtAcquisition.TabIndex = 47;
            this.TxtAcquisition.Validated += new System.EventHandler(this.TxtAcquisition_Validated);
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Black;
            this.label105.Location = new System.Drawing.Point(7, 26);
            this.label105.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(99, 14);
            this.label105.TabIndex = 46;
            this.label105.Text = "Acquisition Value";
            this.label105.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel7);
            this.groupBox1.Controls.Add(this.LueWeightUomCode);
            this.groupBox1.Controls.Add(this.TxtWeight);
            this.groupBox1.Controls.Add(this.label98);
            this.groupBox1.Controls.Add(this.TxtAuthorize);
            this.groupBox1.Controls.Add(this.label99);
            this.groupBox1.Controls.Add(this.TxtObjectType);
            this.groupBox1.Controls.Add(this.label100);
            this.groupBox1.Controls.Add(this.TxtClass);
            this.groupBox1.Controls.Add(this.label101);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(754, 118);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General Data";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.TxtInventoryNo);
            this.panel7.Controls.Add(this.TxtShift);
            this.panel7.Controls.Add(this.label104);
            this.panel7.Controls.Add(this.DteStartUpDt);
            this.panel7.Controls.Add(this.label103);
            this.panel7.Controls.Add(this.TxtSize);
            this.panel7.Controls.Add(this.label97);
            this.panel7.Controls.Add(this.label102);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(367, 18);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(384, 97);
            this.panel7.TabIndex = 58;
            // 
            // TxtInventoryNo
            // 
            this.TxtInventoryNo.EnterMoveNextControl = true;
            this.TxtInventoryNo.Location = new System.Drawing.Point(102, 25);
            this.TxtInventoryNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInventoryNo.Name = "TxtInventoryNo";
            this.TxtInventoryNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInventoryNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInventoryNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInventoryNo.Properties.Appearance.Options.UseFont = true;
            this.TxtInventoryNo.Properties.MaxLength = 16;
            this.TxtInventoryNo.Size = new System.Drawing.Size(225, 20);
            this.TxtInventoryNo.TabIndex = 41;
            // 
            // TxtShift
            // 
            this.TxtShift.EnterMoveNextControl = true;
            this.TxtShift.Location = new System.Drawing.Point(102, 67);
            this.TxtShift.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShift.Name = "TxtShift";
            this.TxtShift.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShift.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShift.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShift.Properties.Appearance.Options.UseFont = true;
            this.TxtShift.Properties.MaxLength = 16;
            this.TxtShift.Size = new System.Drawing.Size(225, 20);
            this.TxtShift.TabIndex = 45;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.Black;
            this.label104.Location = new System.Drawing.Point(4, 69);
            this.label104.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(95, 14);
            this.label104.TabIndex = 44;
            this.label104.Text = "Shift Note Type";
            this.label104.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteStartUpDt
            // 
            this.DteStartUpDt.EditValue = null;
            this.DteStartUpDt.EnterMoveNextControl = true;
            this.DteStartUpDt.Location = new System.Drawing.Point(102, 46);
            this.DteStartUpDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartUpDt.Name = "DteStartUpDt";
            this.DteStartUpDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartUpDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartUpDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartUpDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartUpDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartUpDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartUpDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartUpDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartUpDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartUpDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartUpDt.Properties.MaxLength = 16;
            this.DteStartUpDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartUpDt.Size = new System.Drawing.Size(105, 20);
            this.DteStartUpDt.TabIndex = 43;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.ForeColor = System.Drawing.Color.Black;
            this.label103.Location = new System.Drawing.Point(18, 49);
            this.label103.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(81, 14);
            this.label103.TabIndex = 42;
            this.label103.Text = "Start-up date";
            this.label103.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSize
            // 
            this.TxtSize.EnterMoveNextControl = true;
            this.TxtSize.Location = new System.Drawing.Point(102, 4);
            this.TxtSize.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSize.Name = "TxtSize";
            this.TxtSize.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSize.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSize.Properties.Appearance.Options.UseFont = true;
            this.TxtSize.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSize.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSize.Size = new System.Drawing.Size(148, 20);
            this.TxtSize.TabIndex = 39;
            this.TxtSize.Validated += new System.EventHandler(this.TxtSize_Validated);
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.Black;
            this.label97.Location = new System.Drawing.Point(20, 28);
            this.label97.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(79, 14);
            this.label97.TabIndex = 40;
            this.label97.Text = "Inventory No";
            this.label97.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Black;
            this.label102.Location = new System.Drawing.Point(71, 7);
            this.label102.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(28, 14);
            this.label102.TabIndex = 38;
            this.label102.Text = "Size";
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWeightUomCode
            // 
            this.LueWeightUomCode.EnterMoveNextControl = true;
            this.LueWeightUomCode.Location = new System.Drawing.Point(263, 86);
            this.LueWeightUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWeightUomCode.Name = "LueWeightUomCode";
            this.LueWeightUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWeightUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueWeightUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWeightUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWeightUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWeightUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWeightUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWeightUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWeightUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWeightUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWeightUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWeightUomCode.Properties.DropDownRows = 20;
            this.LueWeightUomCode.Properties.NullText = "[Empty]";
            this.LueWeightUomCode.Properties.PopupWidth = 500;
            this.LueWeightUomCode.Size = new System.Drawing.Size(101, 20);
            this.LueWeightUomCode.TabIndex = 37;
            this.LueWeightUomCode.ToolTip = "F4 : Show/hide list";
            this.LueWeightUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWeightUomCode.EditValueChanged += new System.EventHandler(this.LueWeightUomCode_EditValueChanged);
            // 
            // TxtWeight
            // 
            this.TxtWeight.EnterMoveNextControl = true;
            this.TxtWeight.Location = new System.Drawing.Point(107, 86);
            this.TxtWeight.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWeight.Name = "TxtWeight";
            this.TxtWeight.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWeight.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeight.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWeight.Properties.Appearance.Options.UseFont = true;
            this.TxtWeight.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtWeight.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtWeight.Size = new System.Drawing.Size(151, 20);
            this.TxtWeight.TabIndex = 36;
            this.TxtWeight.Validated += new System.EventHandler(this.TxtWeight_Validated);
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.Black;
            this.label98.Location = new System.Drawing.Point(55, 88);
            this.label98.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(47, 14);
            this.label98.TabIndex = 35;
            this.label98.Text = "Weight";
            this.label98.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAuthorize
            // 
            this.TxtAuthorize.EnterMoveNextControl = true;
            this.TxtAuthorize.Location = new System.Drawing.Point(107, 65);
            this.TxtAuthorize.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAuthorize.Name = "TxtAuthorize";
            this.TxtAuthorize.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAuthorize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAuthorize.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAuthorize.Properties.Appearance.Options.UseFont = true;
            this.TxtAuthorize.Properties.MaxLength = 16;
            this.TxtAuthorize.Size = new System.Drawing.Size(257, 20);
            this.TxtAuthorize.TabIndex = 34;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.Black;
            this.label99.Location = new System.Drawing.Point(6, 68);
            this.label99.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(96, 14);
            this.label99.TabIndex = 33;
            this.label99.Text = "Authorize Group";
            this.label99.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtObjectType
            // 
            this.TxtObjectType.EnterMoveNextControl = true;
            this.TxtObjectType.Location = new System.Drawing.Point(107, 44);
            this.TxtObjectType.Margin = new System.Windows.Forms.Padding(5);
            this.TxtObjectType.Name = "TxtObjectType";
            this.TxtObjectType.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtObjectType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtObjectType.Properties.Appearance.Options.UseBackColor = true;
            this.TxtObjectType.Properties.Appearance.Options.UseFont = true;
            this.TxtObjectType.Properties.MaxLength = 16;
            this.TxtObjectType.Size = new System.Drawing.Size(257, 20);
            this.TxtObjectType.TabIndex = 32;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Black;
            this.label100.Location = new System.Drawing.Point(26, 46);
            this.label100.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(76, 14);
            this.label100.TabIndex = 31;
            this.label100.Text = "Object Type";
            this.label100.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtClass
            // 
            this.TxtClass.EnterMoveNextControl = true;
            this.TxtClass.Location = new System.Drawing.Point(107, 23);
            this.TxtClass.Margin = new System.Windows.Forms.Padding(5);
            this.TxtClass.Name = "TxtClass";
            this.TxtClass.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtClass.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtClass.Properties.Appearance.Options.UseBackColor = true;
            this.TxtClass.Properties.Appearance.Options.UseFont = true;
            this.TxtClass.Properties.MaxLength = 16;
            this.TxtClass.Size = new System.Drawing.Size(257, 20);
            this.TxtClass.TabIndex = 30;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Black;
            this.label101.Location = new System.Drawing.Point(70, 26);
            this.label101.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(32, 14);
            this.label101.TabIndex = 29;
            this.label101.Text = "Class";
            this.label101.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgTO
            // 
            this.TpgTO.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.TpgTO.Controls.Add(this.TpgGeneral);
            this.TpgTO.Controls.Add(this.TpLocation);
            this.TpgTO.Controls.Add(this.TpgOrganization);
            this.TpgTO.Controls.Add(this.TpgStructure);
            this.TpgTO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TpgTO.Location = new System.Drawing.Point(0, 180);
            this.TpgTO.Multiline = true;
            this.TpgTO.Name = "TpgTO";
            this.TpgTO.SelectedIndex = 0;
            this.TpgTO.ShowToolTips = true;
            this.TpgTO.Size = new System.Drawing.Size(772, 334);
            this.TpgTO.TabIndex = 17;
            // 
            // TpgStructure
            // 
            this.TpgStructure.Controls.Add(this.panel11);
            this.TpgStructure.Location = new System.Drawing.Point(4, 26);
            this.TpgStructure.Name = "TpgStructure";
            this.TpgStructure.Padding = new System.Windows.Forms.Padding(3);
            this.TpgStructure.Size = new System.Drawing.Size(764, 23);
            this.TpgStructure.TabIndex = 3;
            this.TpgStructure.Text = "Structure";
            this.TpgStructure.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel11.Controls.Add(this.Grd1);
            this.panel11.Controls.Add(this.button1);
            this.panel11.Controls.Add(this.groupBox7);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel11.Location = new System.Drawing.Point(3, 3);
            this.panel11.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(758, 17);
            this.panel11.TabIndex = 13;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 219);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(754, 0);
            this.Grd1.TabIndex = 100;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.SteelBlue;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.AliceBlue;
            this.button1.Location = new System.Drawing.Point(0, 194);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(754, 25);
            this.button1.TabIndex = 38;
            this.button1.Text = "Equipment";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.LueCapacityUom);
            this.groupBox7.Controls.Add(this.TxtCapacity);
            this.groupBox7.Controls.Add(this.label3);
            this.groupBox7.Controls.Add(this.MeeDesc2);
            this.groupBox7.Controls.Add(this.MeeDesc);
            this.groupBox7.Controls.Add(this.TxtConsType);
            this.groupBox7.Controls.Add(this.label4);
            this.groupBox7.Controls.Add(this.TxtTechnical);
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Controls.Add(this.TxtPosition);
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Controls.Add(this.label7);
            this.groupBox7.Controls.Add(this.TxtSuperorder);
            this.groupBox7.Controls.Add(this.label8);
            this.groupBox7.Controls.Add(this.label9);
            this.groupBox7.Controls.Add(this.TxtFunctional);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(0, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(754, 194);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Structuring";
            // 
            // LueCapacityUom
            // 
            this.LueCapacityUom.EnterMoveNextControl = true;
            this.LueCapacityUom.Location = new System.Drawing.Point(297, 167);
            this.LueCapacityUom.Margin = new System.Windows.Forms.Padding(5);
            this.LueCapacityUom.Name = "LueCapacityUom";
            this.LueCapacityUom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCapacityUom.Properties.Appearance.Options.UseFont = true;
            this.LueCapacityUom.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCapacityUom.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCapacityUom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCapacityUom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCapacityUom.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCapacityUom.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCapacityUom.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCapacityUom.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCapacityUom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCapacityUom.Properties.DropDownRows = 20;
            this.LueCapacityUom.Properties.NullText = "[Empty]";
            this.LueCapacityUom.Properties.PopupWidth = 500;
            this.LueCapacityUom.Size = new System.Drawing.Size(101, 20);
            this.LueCapacityUom.TabIndex = 99;
            this.LueCapacityUom.ToolTip = "F4 : Show/hide list";
            this.LueCapacityUom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCapacityUom.EditValueChanged += new System.EventHandler(this.LueCapacityUom_EditValueChanged);
            // 
            // TxtCapacity
            // 
            this.TxtCapacity.EnterMoveNextControl = true;
            this.TxtCapacity.Location = new System.Drawing.Point(141, 167);
            this.TxtCapacity.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCapacity.Name = "TxtCapacity";
            this.TxtCapacity.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCapacity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCapacity.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCapacity.Properties.Appearance.Options.UseFont = true;
            this.TxtCapacity.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCapacity.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCapacity.Properties.MaxLength = 16;
            this.TxtCapacity.Size = new System.Drawing.Size(151, 20);
            this.TxtCapacity.TabIndex = 98;
            this.TxtCapacity.Validated += new System.EventHandler(this.TxtCapacity_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(85, 169);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 14);
            this.label3.TabIndex = 97;
            this.label3.Text = "Capacity";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeDesc2
            // 
            this.MeeDesc2.EnterMoveNextControl = true;
            this.MeeDesc2.Location = new System.Drawing.Point(141, 83);
            this.MeeDesc2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDesc2.Name = "MeeDesc2";
            this.MeeDesc2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc2.Properties.Appearance.Options.UseFont = true;
            this.MeeDesc2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDesc2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDesc2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDesc2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDesc2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDesc2.Properties.MaxLength = 400;
            this.MeeDesc2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeDesc2.Properties.ShowIcon = false;
            this.MeeDesc2.Size = new System.Drawing.Size(356, 20);
            this.MeeDesc2.TabIndex = 90;
            this.MeeDesc2.ToolTip = "F4 : Show/hide text";
            this.MeeDesc2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDesc2.ToolTipTitle = "Run System";
            // 
            // MeeDesc
            // 
            this.MeeDesc.EnterMoveNextControl = true;
            this.MeeDesc.Location = new System.Drawing.Point(141, 41);
            this.MeeDesc.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDesc.Name = "MeeDesc";
            this.MeeDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc.Properties.Appearance.Options.UseFont = true;
            this.MeeDesc.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDesc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDesc.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDesc.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDesc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDesc.Properties.MaxLength = 400;
            this.MeeDesc.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeDesc.Properties.ShowIcon = false;
            this.MeeDesc.Size = new System.Drawing.Size(356, 20);
            this.MeeDesc.TabIndex = 86;
            this.MeeDesc.ToolTip = "F4 : Show/hide text";
            this.MeeDesc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDesc.ToolTipTitle = "Run System";
            // 
            // TxtConsType
            // 
            this.TxtConsType.EnterMoveNextControl = true;
            this.TxtConsType.Location = new System.Drawing.Point(141, 146);
            this.TxtConsType.Margin = new System.Windows.Forms.Padding(5);
            this.TxtConsType.Name = "TxtConsType";
            this.TxtConsType.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtConsType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtConsType.Properties.Appearance.Options.UseBackColor = true;
            this.TxtConsType.Properties.Appearance.Options.UseFont = true;
            this.TxtConsType.Properties.MaxLength = 16;
            this.TxtConsType.Size = new System.Drawing.Size(257, 20);
            this.TxtConsType.TabIndex = 96;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(67, 147);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 14);
            this.label4.TabIndex = 95;
            this.label4.Text = "Const Type";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTechnical
            // 
            this.TxtTechnical.EnterMoveNextControl = true;
            this.TxtTechnical.Location = new System.Drawing.Point(141, 125);
            this.TxtTechnical.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTechnical.Name = "TxtTechnical";
            this.TxtTechnical.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTechnical.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTechnical.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTechnical.Properties.Appearance.Options.UseFont = true;
            this.TxtTechnical.Properties.MaxLength = 16;
            this.TxtTechnical.Size = new System.Drawing.Size(257, 20);
            this.TxtTechnical.TabIndex = 94;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(60, 126);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 14);
            this.label5.TabIndex = 93;
            this.label5.Text = "Technical No";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPosition
            // 
            this.TxtPosition.EnterMoveNextControl = true;
            this.TxtPosition.Location = new System.Drawing.Point(141, 104);
            this.TxtPosition.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosition.Name = "TxtPosition";
            this.TxtPosition.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPosition.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosition.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosition.Properties.Appearance.Options.UseFont = true;
            this.TxtPosition.Properties.MaxLength = 16;
            this.TxtPosition.Size = new System.Drawing.Size(257, 20);
            this.TxtPosition.TabIndex = 92;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(88, 105);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 14);
            this.label6.TabIndex = 91;
            this.label6.Text = "Position";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(5, 86);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 14);
            this.label7.TabIndex = 89;
            this.label7.Text = "Superorder Description";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSuperorder
            // 
            this.TxtSuperorder.EnterMoveNextControl = true;
            this.TxtSuperorder.Location = new System.Drawing.Point(141, 62);
            this.TxtSuperorder.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSuperorder.Name = "TxtSuperorder";
            this.TxtSuperorder.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSuperorder.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSuperorder.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSuperorder.Properties.Appearance.Options.UseFont = true;
            this.TxtSuperorder.Properties.MaxLength = 16;
            this.TxtSuperorder.Size = new System.Drawing.Size(257, 20);
            this.TxtSuperorder.TabIndex = 88;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(35, 65);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 14);
            this.label8.TabIndex = 87;
            this.label8.Text = "Superorder Equip";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(11, 43);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(126, 14);
            this.label9.TabIndex = 85;
            this.label9.Text = "Functional Description";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFunctional
            // 
            this.TxtFunctional.EnterMoveNextControl = true;
            this.TxtFunctional.Location = new System.Drawing.Point(141, 20);
            this.TxtFunctional.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFunctional.Name = "TxtFunctional";
            this.TxtFunctional.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFunctional.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFunctional.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFunctional.Properties.Appearance.Options.UseFont = true;
            this.TxtFunctional.Properties.MaxLength = 16;
            this.TxtFunctional.Size = new System.Drawing.Size(356, 20);
            this.TxtFunctional.TabIndex = 84;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(25, 23);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(112, 14);
            this.label10.TabIndex = 83;
            this.label10.Text = "Functional Location";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmTO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 514);
            this.Name = "FrmTO";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInfo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEnd.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStart.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetCode.Properties)).EndInit();
            this.TpgOrganization.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCatalog.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkcenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlannerGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlanningPlant.Properties)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWbs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtArea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompany.Properties)).EndInit();
            this.TpLocation.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLocationCode.Properties)).EndInit();
            this.TpgGeneral.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtConstMth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtConstYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSerial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPartNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtManufacture.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteAcquisitionDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteAcquisitionDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcquisition.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShift.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartUpDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartUpDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWeightUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAuthorize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtObjectType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtClass.Properties)).EndInit();
            this.TpgTO.ResumeLayout(false);
            this.TpgStructure.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCapacityUom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCapacity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDesc2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtConsType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTechnical.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSuperorder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFunctional.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label11;
        public DevExpress.XtraEditors.SimpleButton BtnAsset;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        internal DevExpress.XtraEditors.DateEdit DteEnd;
        private System.Windows.Forms.Label label96;
        internal DevExpress.XtraEditors.DateEdit DteStart;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl TpgTO;
        private System.Windows.Forms.TabPage TpgGeneral;
        protected System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox groupBox3;
        protected System.Windows.Forms.Panel panel8;
        internal DevExpress.XtraEditors.TextEdit TxtConstMth;
        internal DevExpress.XtraEditors.TextEdit TxtCnt;
        private System.Windows.Forms.Label label111;
        internal DevExpress.XtraEditors.TextEdit TxtConstYr;
        internal DevExpress.XtraEditors.TextEdit TxtSerial;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label114;
        internal DevExpress.XtraEditors.TextEdit TxtPartNo;
        private System.Windows.Forms.Label label107;
        internal DevExpress.XtraEditors.TextEdit TxtModel;
        private System.Windows.Forms.Label label108;
        internal DevExpress.XtraEditors.TextEdit TxtManufacture;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.GroupBox groupBox1;
        protected System.Windows.Forms.Panel panel7;
        internal DevExpress.XtraEditors.TextEdit TxtInventoryNo;
        internal DevExpress.XtraEditors.TextEdit TxtShift;
        private System.Windows.Forms.Label label104;
        internal DevExpress.XtraEditors.DateEdit DteStartUpDt;
        private System.Windows.Forms.Label label103;
        internal DevExpress.XtraEditors.TextEdit TxtSize;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label102;
        private DevExpress.XtraEditors.LookUpEdit LueWeightUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtWeight;
        private System.Windows.Forms.Label label98;
        internal DevExpress.XtraEditors.TextEdit TxtAuthorize;
        private System.Windows.Forms.Label label99;
        internal DevExpress.XtraEditors.TextEdit TxtObjectType;
        private System.Windows.Forms.Label label100;
        internal DevExpress.XtraEditors.TextEdit TxtClass;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TabPage TpLocation;
        protected System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label112;
        private DevExpress.XtraEditors.LookUpEdit LueLocationCode;
        private System.Windows.Forms.TabPage TpgOrganization;
        protected System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.GroupBox groupBox5;
        internal DevExpress.XtraEditors.TextEdit TxtCatalog;
        private System.Windows.Forms.Label label122;
        internal DevExpress.XtraEditors.TextEdit TxtWorkcenter;
        private System.Windows.Forms.Label label116;
        internal DevExpress.XtraEditors.TextEdit TxtPlannerGroup;
        private System.Windows.Forms.Label label117;
        internal DevExpress.XtraEditors.TextEdit TxtPlanningPlant;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.GroupBox groupBox4;
        internal DevExpress.XtraEditors.TextEdit TxtWbs;
        private System.Windows.Forms.Label label115;
        private DevExpress.XtraEditors.LookUpEdit LueCCCode;
        private System.Windows.Forms.Label label113;
        internal DevExpress.XtraEditors.TextEdit TxtAsset;
        private System.Windows.Forms.Label label119;
        internal DevExpress.XtraEditors.TextEdit TxtArea;
        private System.Windows.Forms.Label label120;
        internal DevExpress.XtraEditors.TextEdit TxtCompany;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.TabPage TpgStructure;
        protected System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.GroupBox groupBox7;
        private DevExpress.XtraEditors.LookUpEdit LueCapacityUom;
        internal DevExpress.XtraEditors.TextEdit TxtCapacity;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.MemoExEdit MeeDesc2;
        private DevExpress.XtraEditors.MemoExEdit MeeDesc;
        internal DevExpress.XtraEditors.TextEdit TxtConsType;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtTechnical;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtPosition;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtSuperorder;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtFunctional;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        public DevExpress.XtraEditors.TextEdit TxtAssetName;
        public DevExpress.XtraEditors.TextEdit TxtAssetCode;
        public DevExpress.XtraEditors.TextEdit TxtAssetCtCode;
        public DevExpress.XtraEditors.TextEdit TxtAcquisition;
        public DevExpress.XtraEditors.DateEdit DteAcquisitionDt;
        private DevExpress.XtraEditors.LookUpEdit LueStatus;
        private DevExpress.XtraEditors.LookUpEdit LueInfo;
        private System.Windows.Forms.Label label12;
        public DevExpress.XtraEditors.TextEdit TxtDisplayName;
        private System.Windows.Forms.Label label14;
    }
}