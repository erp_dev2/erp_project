﻿#region update 
/*
    17/06/2021 [IQA/BRI/KSM] Buat Menu Baru buat Report Weekly OutStanding Do To Customer dan Query dibuat oleh mas bri
    06/09/2021 [TKG/KSM] tambah filter item category dan sedikit ubah query (cuma masih perlu konfirmasi balik ke ksm)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptWeeklyOutStandingDOCt : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptWeeklyOutStandingDOCt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnPrint.Visible = false;
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -6);
                Sl.SetLueItCtCodeActive(ref LueItCtCode, string.Empty);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ DteDocDt2 }, true);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            
            //SQL.AppendLine("SELECT C.CtName, D.ItName, E.LocalDocNo, B.Qty, B.UPrice, (B.Qty*B.Uprice) AS Total, SUM((B.Qty*B.Uprice))/SUM(B.Qty) AS `Rata-rata`, Keluar,  ");
            //SQL.AppendLine("IF(Keluar > 0,(B.Qty-Keluar),B.Qty) AS `Saldo Akhir`, (Keluar*B.UPrice) AS `Total Keluar`, SUM(Keluar*B.UPrice)/SUM(Keluar) AS `Rata-rata Keluar`, ");
            //SQL.AppendLine("Week(E.DocDt) As Weekly, H. ItCtName AS `Item Category`, E.DocDt as `Document Date` ");
            //SQL.AppendLine("FROM tblsalesmemohdr A ");
            //SQL.AppendLine("INNER JOIN tblsalesmemodtl B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("INNER JOIN tblcustomer C ON A.CtCode = C.CtCode ");
            //SQL.AppendLine("INNER JOIN tblitem D ON B.ItCode = D.ItCode ");
            //SQL.AppendLine("INNER JOIN tblsalescontract E ON A.DocNo = E.SalesMemoDocNo ");
            //SQL.AppendLine(" AND A.Status = 'A' ");
            //SQL.AppendLine(" AND A.CancelInd = 'N' ");
            //SQL.AppendLine(" And (A.DocDt Between @DocDt1 And @DocDt2) ");
            //SQL.AppendLine("INNER JOIN tbldrdtl F ON E.DocNo = F.SCDocNo ");
            //SQL.AppendLine("");
            //SQL.AppendLine("LEFT JOIN ");
            //SQL.AppendLine("(");
            //SQL.AppendLine("SELECT T1.DRDocNo, T1.DocDt, T2.Qty AS Keluar ");
            //SQL.AppendLine("FROM tbldoct2hdr T1 ");
            //SQL.AppendLine("LEFT JOIN tbldoct2dtl T2 ON T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("GROUP BY T1.DocNo ");
            //SQL.AppendLine(") G ON F.DocNo = G.DRDocNo ");
            //SQL.AppendLine("INNER JOIN TblItemCategory H On D.ItCtCode = H.ItCtCode ");

            SQL.AppendLine("SELECT C.CtName, B.ItName, A.*, D.ItCtName ");
            SQL.AppendLine("FROM ( ");
            SQL.AppendLine("    SELECT T1.CtCode, T1.ItCode, T1.LocalDocNo, ");
            SQL.AppendLine("    IF(T3.QtyDOCt2 > 0,(T1.Qty-T3.QtyDOCt2),T1.Qty) AS Qty, ");
            SQL.AppendLine("    T1.UPrice AS Price, ");
            SQL.AppendLine("    IF(T3.QtyDOCt2 > 0,(T1.Qty-T3.QtyDOCt2),T1.Qty)*T1.UPrice AS Total, ");
            SQL.AppendLine("    (T1.Qty*T1.UPrice)/T1.Qty AS Average, ");
            SQL.AppendLine("    T2.QtyDOCt, ");
            SQL.AppendLine("    IF(T3.QtyDOCt2 > 0,(T1.Qty-T3.QtyDOCt2),T1.Qty)-T2.QtyDOCt AS Balance, ");
            SQL.AppendLine("    T2.QtyDOCt*T1.UPrice AS Total2, ");
            SQL.AppendLine("    (T2.QtyDOCt*T1.UPrice)/T2.QtyDOCt AS Average2, ");
            SQL.AppendLine("    WEEK(T1.DocDt) AS Weekly, T1.DocDt ");
            SQL.AppendLine("    FROM ( ");
            SQL.AppendLine("        SELECT B.CtCode, C.ItCode, A.LocalDocNo, SUM(IFNULL(C.Qty, 0)) Qty, SUM(IFNULL(C.UPriceAfterTax, 0)) UPrice, SUM(IFNULL(D.Qty, 0)) Qty2, A.DocDt ");
            SQL.AppendLine("        FROM tblsalescontract A ");
		    SQL.AppendLine("        INNER JOIN tblsalesmemohdr B ON A.SalesMemoDocNo=B.DocNo AND B.CancelInd = 'N' ");
		    SQL.AppendLine("        INNER JOIN tblsalesmemodtl C ON B.DocNo=C.DocNo ");
		    SQL.AppendLine("        INNER JOIN tblstocksummary D ON C.ItCode=D.ItCode ");
		    SQL.AppendLine("        INNER JOIN tblwarehouse E ON D.WhsCode=E.WhsCode AND FIND_IN_SET(E.WhsCode,'G.SPN,GBJ-K') ");
            SQL.AppendLine("        WHERE (A.DocDt BETWEEN @DocDt1 AND @DocDt2) AND A.CancelInd = 'N' ");
            SQL.AppendLine("        Group By B.CtCode, C.ItCode, A.LocalDocNo, A.DocDt ");
            //SQL.AppendLine("        GROUP BY C.ItCode "); 
            SQL.AppendLine("    )T1 ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("    SELECT B.ItCode, SUM(IFNULL(B.Qty, 0)) QtyDOCt ");
            SQL.AppendLine("    FROM tbldoct2hdr A ");
            SQL.AppendLine("    INNER JOIN tbldoct2dtl B ON A.DocNo = B.DocNo AND B.CancelInd = 'N' ");
            SQL.AppendLine("    INNER JOIN tbldrhdr C ON A.DRDocNo = C.DocNo AND C.CancelInd = 'N' ");
            SQL.AppendLine("    INNER JOIN tbldrdtl D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("    INNER JOIN tblsalescontract E ON D.SCDocNo = E.DOcNo "); 
            //SQL.AppendLine("    WHERE A.DocDt BETWEEN @DocDt1 AND @DocDt2 ");
            SQL.AppendLine("        And E.DocDt BETWEEN @DocDt1 AND @DocDt2 ");
            SQL.AppendLine("    GROUP BY B.ItCode ");
            SQL.AppendLine("    )T2 ON T1.ItCode = T2.ItCode ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("    SELECT B.ItCode, SUM(IFNULL(B.Qty, 0)) QtyDOCt2 ");
            SQL.AppendLine("    FROM tbldoct2hdr A ");
            SQL.AppendLine("    INNER JOIN tbldoct2dtl B ON A.DocNo = B.DocNo AND B.CancelInd = 'N' ");
            SQL.AppendLine("    INNER JOIN tbldrhdr C ON A.DRDocNo = C.DocNo AND C.CancelInd = 'N' ");
            SQL.AppendLine("    INNER JOIN tbldrdtl D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("    INNER JOIN tblsalescontract E ON D.SCDocNo = E.DOcNo ");
            SQL.AppendLine("    WHERE A.DocDt < @DocDt1 "); // ???
            SQL.AppendLine("    GROUP BY B.ItCode ");
            SQL.AppendLine("    )T3 ON T1.ItCode = T3.ItCode ");
            SQL.AppendLine(")A ");
            SQL.AppendLine("INNER JOIN tblitem B ON A.ItCode = B.ItCode ");
            SQL.AppendLine("INNER JOIN tblcustomer C ON A.CtCode=C.CtCode ");
            SQL.AppendLine("INNER JOIN tblitemcategory D ON B.ItCtCode=D.ItCtCode ");
         
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Customer Name",//CtName
                    "Item Name",//ItName
                    "No Contract", //LocalDocNo
                    "Initial balance",//Qty
                    "Price",//Price

                    //6-10
                    "Total",//Total
                    "Average Price",//average
                    "Balance"+Environment.NewLine+"In Periode", //QtyDOCt 
                    "Balance"+Environment.NewLine+"End",//balance 
                    "Total"+Environment.NewLine+"Exit",//total2
                    

                    //11-13
                    "Average"+Environment.NewLine+"Exit",//average2
                    "Weekly",//Weekly
                    "Item's Category",//ItCtName
                    "Date" //DocDt
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 150, 200, 150, 150,

                    //6-10
                    180, 120, 120, 120, 120,

                    //11-13
                    120, 100, 150, 100
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 14 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[14].Move(3);
            Grd1.GroupObject.Add(12);
            Grd1.GroupObject.Add(13);
            Grd1.GroupObject.Add(2);
            Grd1.Group();
        }

        override protected void HideInfoInGrd()
        {
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Grd1.Rows.Count > 0)
                Grd1.Rows[0].BackColor = Color.White;
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 1 = 1 ";
                DateTime d = DateTime.Now;
                String
                    CurrentDt = Sm.ServerCurrentDate(),
                    Dt = string.Empty;
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "A.LocalDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode","B.ItName"});
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "D.ItCtCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter,
                new string[]
                    {
                        //0
                        "CtName", 

                        //1-5
                        "ItName", 
                        "LocalDocNo", 
                        "Qty",
                        "Price", 
                        "Total", 

                        //6-10
                        "Average",
                        "QtyDOCt",
                        "Balance", 
                        "Total2",
                        "Average2",
                        
                        //11-13
                        "Weekly",
                        "ItCtName",
                        "DocDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("I", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 13);
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 6, 7, 9, 11 });
                Grd1.GroupObject.Add(12);
                Grd1.GroupObject.Add(13);
                Grd1.GroupObject.Add(2);
                Grd1.Group();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local Document");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            DteDocDt2.EditValue = DteDocDt1.DateTime.AddDays(6);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCodeActive), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion

    }
}
