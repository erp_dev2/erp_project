﻿#region Update
/*
    07/09/2017 [ARI] tambah Domicile s.d Email
    10/09/2017 [TKG] Berdasarkan parameter IsAbleToAmendResigneeInfo, informasi karyawan yg sudah resign tidak bisa diubah informasinya.
    22/12/2017 [TKG] tambah filter dept+site berdasarkan group
    15/11/2018 [HAR] tambah bisa edit employee name, display name, employee code old]
    18/12/2018 [HAR] tambah training graduation
    20/02/2019 [HAR] tambah section
    17/09/2019 [HAR] tidak boelh edit employeeoldcode di SIER berdasarkan parameter IsEmpOldCodeEditable 
    16/03/2023 [HAR/PHT] Menghilangkan field Biological Mother berdasarkan parameter
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmployeeAmendment : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        public string DeptCodeOld, PosCodeOld, GrdLvlCodeOld, SiteCodeOld, CityOld, SectionCodeOld;
        internal bool 
            mIsSiteMandatory = false,
            mIsNotAbleToAmendResigneeInfo = false,
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false,
            mIsEmpOldCodeEditable = false,
            mIsEmployeeUseBiologicalMother =false;
        //internal bool mIsNotFilterByAuthorization = false;
        internal FrmEmployeeAmendmentFind FrmFind;

        #endregion

        #region Constructor

        public FrmEmployeeAmendment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Information Amendment";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                //Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLuePosCode(ref LuePosCode);
                Sl.SetLueCityCode(ref LueCity);

                Sl.SetLueGender(ref LueGenderOld);
                Sl.SetLueReligion(ref LueReligionOld);
                Sl.SetLueOption(ref LueMaritalStatusOld, "MaritalStatus");
                Sl.SetLueOption(ref LueBloodTypeOld, "BloodType");
                Sl.SetLuePTKP(ref LuePTKPOld);
                Sl.SetLueBankCode(ref LueBankCodeOld);

                Sl.SetLueGender(ref LueGender);
                Sl.SetLueReligion(ref LueReligion);
                Sl.SetLueOption(ref LueMaritalStatus, "MaritalStatus");
                Sl.SetLueOption(ref LueBloodType, "BloodType");
                Sl.SetLuePTKP(ref LuePTKP);
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueSectionCode(ref LueSection);
                if (!mIsEmployeeUseBiologicalMother)
                {
                    LblMotherOld.Visible =  LblMotherNew.Visible =  false;
                    TxtMotherOld.Visible = TxtMother.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
           
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, DteResignDt, MeeRemark, MeeAddress, TxtVillage, 
                        LueCity, TxtSubDistrict, TxtRTRW, TxtPostalCode,
                        MeeDomicileOld, TxtIdNumberOld, LueMaritalStatusOld, DteWeddingDtOld, 
                        LueGenderOld, LueReligionOld, TxtBirthPlaceOld, DteBirthDtOld, LueBloodTypeOld,
                        TxtMotherOld, TxtNPWPOld, LuePTKPOld, LueBankCodeOld, TxtBankBranchOld,
                        TxtBankAcNameOld, TxtBankAcNoOld, TxtPhoneOld, TxtMobileOld, TxtEmailOld,
                        MeeDomicile, TxtIdNumber, LueMaritalStatus, DteWeddingDt, 
                        LueGender, LueReligion, TxtBirthPlace, DteBirthDt, LueBloodType,
                        TxtMother, TxtNPWP, LuePTKP, LueBankCode, TxtBankBranch,
                        TxtBankAcName, TxtBankAcNo, TxtPhone, TxtMobile, TxtEmail, TxtEmpCodeOld, TxtEmpCodeOldOld,
                        TxtDisplayNameOld, TxtDisplayName, TxtEmpName, DteTGDtOld, DteTGDt, TxtSectionOld, LueSection
                    }, true);
                    BtnEmpCode.Enabled = false;
                    BtnEmpCode2.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, DteResignDt, MeeRemark, MeeAddress, TxtVillage, 
                        LueCity, TxtSubDistrict, TxtRTRW, TxtPostalCode,
                        //MeeDomicileOld, TxtIdNumberOld, LueMaritalStatusOld, DteWeddingDtOld, 
                        //LueGenderOld, LueReligionOld, TxtBirthPlaceOld, DteBirthDtOld, LueBloodTypeOld,
                        //TxtMotherOld, TxtNPWPOld, LuePTKPOld, LueBankCodeOld, TxtBankBranchOld,
                        //TxtBankAcNameOld, TxtBankAcNoOld, TxtPhoneOld, TxtMobileOld, TxtEmailOld,
                        MeeDomicile, TxtIdNumber, LueMaritalStatus, DteWeddingDt, 
                        LueGender, LueReligion, TxtBirthPlace, DteBirthDt, LueBloodType,
                        TxtMother, TxtNPWP, LuePTKP, LueBankCode, TxtBankBranch,
                        TxtBankAcName, TxtBankAcNo, TxtPhone, TxtMobile, TxtEmail,TxtEmpCodeOld, 
                        TxtDisplayName, TxtEmpName, DteTGDt, LueSection
                    }, false);
                    if (!mIsEmpOldCodeEditable)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                            { 
                                TxtEmpCodeOld
                            }, true);
                    }
                    BtnEmpCode.Enabled = true;
                    BtnEmpCode2.Enabled = true;
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
              TxtDocNo, DteDocDt, TxtEmpCode, TxtEmpNameOld, TxtDeptCodeOld, 
              TxtPosCodeOld, DteResignDt, TxtGrdLvlCodeOld, TxtSiteCodeOld, LueDeptCode, 
              LueGrdLvlCode, LuePosCode, LueSiteCode, MeeRemark,
              MeeAddressOld, TxtVillageOld, TxtCityOld, TxtSubDistrictOld, TxtRTRWOld, TxtPostalCodeOld,
              MeeAddress, TxtVillage, LueCity, TxtSubDistrict, TxtRTRW, TxtPostalCode,
              MeeDomicileOld, TxtIdNumberOld, LueMaritalStatusOld, DteWeddingDtOld, LueGenderOld, LueReligionOld, 
              TxtBirthPlaceOld, DteBirthDtOld, LueBloodTypeOld, TxtMotherOld, TxtNPWPOld, LuePTKPOld, LueBankCodeOld,
              TxtBankBranchOld, TxtBankAcNameOld, TxtBankAcNoOld, TxtPhoneOld, TxtMobileOld, TxtEmailOld,
              MeeDomicile, TxtIdNumber, LueMaritalStatus, DteWeddingDt, LueGender, LueReligion, TxtBirthPlace,
              DteBirthDt, LueBloodType, TxtMother, TxtNPWP, LuePTKP, LueBankCode, TxtBankBranch, TxtBankAcName,
              TxtBankAcNo, TxtPhone, TxtMobile, TxtEmail, TxtEmpCodeOld, TxtEmpCodeOldOld, TxtDisplayNameOld, TxtDisplayName, 
              TxtEmpName, DteTGDtOld, DteTGDt, TxtSectionOld, LueSection
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmployeeAmendmentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueGrdLvlCode(ref LueGrdLvlCode, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmployeeAmendment", "TblEmployeeAmendment");
            
            var cml = new List<MySqlCommand>();
            cml.Add(SaveEmployeeAmendment(DocNo));
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee", false) ||
                (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "New Site")) ||
                IsResigneeAmendmentInValid()
                ;
        }

        private bool IsResigneeAmendmentInValid()
        {
            bool IsResigned = !Sm.IsDataExist(
                "Select 1 From TblEmployee " +
                "Where EmpCode=@Param1 " + 
                "And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>@Param2)); ",
                TxtEmpCode.Text, Sm.ServerCurrentDateTime(), string.Empty
                );

            if (IsResigned)
            {
                if (mIsNotAbleToAmendResigneeInfo)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "You can't edit this data." + Environment.NewLine +
                        "This employee already resign.");
                    return true;
                }
                else
                {
                    if (Sm.StdMsgYN(
                        "Question",
                        "This employee already resign." + Environment.NewLine +
                        "Do you still want to edit this data ?"
                        )==DialogResult.No)
                        return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveEmployeeAmendment(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmployeeAmendment ");
            SQL.AppendLine("(DocNo, DocDt, EmpCode, ");
            SQL.AppendLine("DeptCodeOld, PosCodeOld, GrdLvlCodeOld, SiteCodeOld, AddressOld, VillageOld, ");
            SQL.AppendLine("CityCodeOld, SubDistrictOld, RTRWOld, PostalCodeOld, DeptCode, PosCode, ");
            SQL.AppendLine("GrdLvlCode, SiteCode, Address, Village, CityCode, SubDistrict, RTRW, PostalCode, ");

            SQL.AppendLine("DomicileOld, IDNumberOld, MaritalStatusOld, WeddingDtOld, GenderOld, ReligionOld, BirthPlaceOld, ");
            SQL.AppendLine("BirthDtOld, BloodTypeOld, MotherOld, NPWPOld, PTKPOld, BankCodeOld, BankBranchOld, BankAcNameOld, BankACNoOld, ");
            SQL.AppendLine("PhoneOld, MobileOld, EmailOld, EmpCodeOldOld, DisplayNameOld, EmpNameOld, TGDtOld, SectionCodeOld, ");
           
            SQL.AppendLine("Domicile, IDNumber, MaritalStatus, WeddingDt, Gender, Religion, BirthPlace, BirthDt, ");
            SQL.AppendLine("BloodType, Mother, NPWP, PTKP, BankCode, BankBranch, BankAcName, BankACNo, ");
            SQL.AppendLine("Phone, Mobile, Email, EmpCodeOld, DisplayName, EmpName, TGDt, SectionCode, ResignDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @EmpCode, ");
            SQL.AppendLine("@DeptCodeOld, @PosCodeOld, @GrdLvlCodeOld, @SiteCodeOld, @AddressOld, @VillageOld, ");
            SQL.AppendLine("@CityOld, @SubDistrictOld, @RTRWOld, @PostalCodeOld, @DeptCode, @PosCode, ");
            SQL.AppendLine("@GrdLvlCode, @SiteCode,@Address, @Village, @CityCode, @SubDistrict, @RTRW, @PostalCode, ");

            SQL.AppendLine("@DomicileOld, @IDNumberOld, @MaritalStatusOld, @WeddingDtOld, @GenderOld, @ReligionOld, @BirthPlaceOld, @BirthDtOld, ");
            SQL.AppendLine("@BloodTypeOld, @MotherOld, @NPWPOld, @PTKPOld, @BankCodeOld, @BankBranchOld, @BankAcNameOld, @BankACNoOld, ");
            SQL.AppendLine("@PhoneOld, @MobileOld, @EmailOld, @EmpCodeOldOld, @DisplayNameOld, @EmpNameOld, @TGDtOld, @SectionCodeOld, ");

            SQL.AppendLine("@Domicile, @IDNumber, @MaritalStatus, @WeddingDt, @Gender, @Religion, @BirthPlace, @BirthDt, ");
            SQL.AppendLine("@BloodType, @Mother, @NPWP, @PTKP, @BankCode, @BankBranch, @BankAcName, @BankACNo, ");
            SQL.AppendLine("@Phone, @Mobile, @Email, @EmpCodeOld, @DisplayName, @EmpName, @TGDt, @SectionCode, @ResignDt,  @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblEmployee Set ");
            SQL.AppendLine("    DeptCode=@DeptCode, PosCode=@PosCode, GrdLvlCode=@GrdLvlCode, SiteCode=@SiteCode, ");
            SQL.AppendLine("    Address=@Address, Village=@Village, CityCode=@CityCode, SubDistrict=@SubDistrict, RTRW=@RTRW, PostalCode=@PostalCode, ");

            //SQL.AppendLine("    DomicileOld=@DomicileOld, IDNumberOld=@IDNumberOld, MaritalStatusOld=@MaritalStatusOld, WeddingDtOld=@WeddingDtOld, GenderOld=@GenderOld, ");
            //SQL.AppendLine("    ReligionOld=@ReligionOld, BirthPlaceOld=@BirthPlaceOld, BirthDtOld=@BirthDtOld, BloodTypeOld=@BloodTypeOld, MotherOld=@MotherOld, NPWPOld=@NPWPOld, ");
            //SQL.AppendLine("    PTKPOld=@PTKPOld, BankCodeOld=@BankCodeOld, BankBranchOld=@BankBranchOld, BankAcNameOld=@BankAcNameOld, BankACNoOld=@BankACNoOld, ");
            //SQL.AppendLine("    PhoneOld=@PhoneOld, MobileOld=@MobileOld, EmailOld=@EmailOld,");

            SQL.AppendLine("    Domicile=@Domicile, IDNumber=@IDNumber, MaritalStatus=@MaritalStatus, WeddingDt=@WeddingDt, Gender=@Gender, ");
            SQL.AppendLine("    Religion=@Religion, BirthPlace=@BirthPlace, BirthDt=@BirthDt, BloodType=@BloodType, Mother=@Mother, NPWP=@NPWP, ");
            SQL.AppendLine("    PTKP=@PTKP, BankCode=@BankCode, BankBranch=@BankBranch, BankAcName=@BankAcName, BankACNo=@BankACNo, ");
            SQL.AppendLine("    Phone=@Phone, Mobile=@Mobile, Email=@Email, ResignDt=@ResignDt, EmpCodeOld=@EmpCodeOld, DisplayName=@DisplayName, EmpName=@EmpName, ");
            SQL.AppendLine("    TGDt=@TGDt, SectionCode=@SectionCode, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where EmpCode=@EmpCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@DeptCodeOld", DeptCodeOld);
            Sm.CmParam<String>(ref cm, "@PosCodeOld", PosCodeOld);
            Sm.CmParam<String>(ref cm, "@GrdLvlCodeOld", GrdLvlCodeOld);
            Sm.CmParam<String>(ref cm, "@SiteCodeOld", SiteCodeOld);
            Sm.CmParam<String>(ref cm, "@AddressOld", MeeAddressOld.Text);
            Sm.CmParam<String>(ref cm, "@VillageOld", TxtVillageOld.Text); 
            Sm.CmParam<String>(ref cm, "@CityOld", CityOld);
            Sm.CmParam<String>(ref cm, "@SubDistrictOld", TxtSubDistrictOld.Text);
            Sm.CmParam<String>(ref cm, "@RTRWOld", TxtRTRWOld.Text);
            Sm.CmParam<String>(ref cm, "@PostalCodeOld", TxtPostalCodeOld.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetLue(LueGrdLvlCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Address", MeeAddress.Text);
            Sm.CmParam<String>(ref cm, "@Village", TxtVillage.Text);
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCity));
            Sm.CmParam<String>(ref cm, "@SubDistrict", TxtSubDistrict.Text);
            Sm.CmParam<String>(ref cm, "@RTRW", TxtRTRW.Text);
            Sm.CmParam<String>(ref cm, "@PostalCode", TxtPostalCode.Text);
            Sm.CmParam<String>(ref cm, "@DomicileOld", MeeDomicileOld.Text);
            Sm.CmParam<String>(ref cm, "@IdNumberOld", TxtIdNumberOld.Text);
            Sm.CmParam<String>(ref cm, "@MaritalStatusOld", Sm.GetLue(LueMaritalStatusOld));
            Sm.CmParamDt(ref cm, "@WeddingDtOld", Sm.GetDte(DteWeddingDtOld));
            Sm.CmParam<String>(ref cm, "@GenderOld", Sm.GetLue(LueGenderOld));
            Sm.CmParam<String>(ref cm, "@ReligionOld", Sm.GetLue(LueReligionOld));
            Sm.CmParam<String>(ref cm, "@BirthPlaceOld", TxtBirthPlaceOld.Text);
            Sm.CmParamDt(ref cm, "@BirthDtOld", Sm.GetDte(DteBirthDtOld));
            Sm.CmParam<String>(ref cm, "@BloodTypeOld", Sm.GetLue(LueBloodTypeOld));
            Sm.CmParam<String>(ref cm, "@MotherOld", TxtMotherOld.Text);
            Sm.CmParam<String>(ref cm, "@NPWPOld", TxtNPWPOld.Text);
            Sm.CmParam<String>(ref cm, "@PTKPOld", Sm.GetLue(LuePTKPOld));
            Sm.CmParam<String>(ref cm, "@BankCodeOld", Sm.GetLue(LueBankCodeOld));
            Sm.CmParam<String>(ref cm, "@BankBranchOld", TxtBankBranchOld.Text);
            Sm.CmParam<String>(ref cm, "@BankAcNameOld", TxtBankAcNameOld.Text);
            Sm.CmParam<String>(ref cm, "@BankAcNoOld", TxtBankAcNoOld.Text);
            Sm.CmParam<String>(ref cm, "@PhoneOld", TxtPhoneOld.Text);
            Sm.CmParam<String>(ref cm, "@MobileOld", TxtMobileOld.Text);
            Sm.CmParam<String>(ref cm, "@EmailOld", TxtEmailOld.Text);
            Sm.CmParam<String>(ref cm, "@EmpCodeOldOld", TxtEmpCodeOldOld.Text);
            Sm.CmParam<String>(ref cm, "@DisplayNameOld", TxtDisplayNameOld.Text);
            Sm.CmParam<String>(ref cm, "@EmpNameOld", TxtEmpNameOld.Text);
            Sm.CmParamDt(ref cm, "@TGDtOld", Sm.GetDte(DteTGDtOld));
            Sm.CmParam<String>(ref cm, "@SectionCodeOld", SectionCodeOld);

            Sm.CmParam<String>(ref cm, "@Domicile", MeeDomicile.Text);
            Sm.CmParam<String>(ref cm, "@IdNumber", TxtIdNumber.Text);
            Sm.CmParam<String>(ref cm, "@MaritalStatus", Sm.GetLue(LueMaritalStatus));
            Sm.CmParamDt(ref cm, "@WeddingDt", Sm.GetDte(DteWeddingDt));
            Sm.CmParam<String>(ref cm, "@Gender", Sm.GetLue(LueGender));
            Sm.CmParam<String>(ref cm, "@Religion", Sm.GetLue(LueReligion));
            Sm.CmParam<String>(ref cm, "@BirthPlace", TxtBirthPlace.Text);
            Sm.CmParamDt(ref cm, "@BirthDt", Sm.GetDte(DteBirthDt));
            Sm.CmParam<String>(ref cm, "@BloodType", Sm.GetLue(LueBloodType));
            Sm.CmParam<String>(ref cm, "@Mother", TxtMother.Text);
            Sm.CmParam<String>(ref cm, "@NPWP", TxtNPWP.Text);
            Sm.CmParam<String>(ref cm, "@PTKP", Sm.GetLue(LuePTKP));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@BankBranch", TxtBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@BankAcName", TxtBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@BankAcNo", TxtBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@Phone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@Mobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@Email", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@EmpCodeOld", TxtEmpCodeOld.Text);
            Sm.CmParam<String>(ref cm, "@DisplayName", TxtDisplayName.Text);
            Sm.CmParam<String>(ref cm, "@EmpName", TxtEmpName.Text);
            Sm.CmParamDt(ref cm, "@TGDt", Sm.GetDte(DteTGDt));
            Sm.CmParamDt(ref cm, "@ResignDt", Sm.GetDte(DteResignDt));
            Sm.CmParam<String>(ref cm, "@SectionCode", Sm.GetLue(LueSection));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowEmployeeAmendment(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmployeeAmendment(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.EmpCode, C.DeptName As DeptNameOld, D.PosName As PosNameOld, ");
            SQL.AppendLine("E.GrdLvlName As GrdLvlNameOld, F.SiteName As SiteNameOld, A.AddressOld, A.VillageOld, ");
            SQL.AppendLine("G.CityName As CityCOdeOld, A.RTRWOld, A.SUbDistrictOld, A.PostalCOdeOld, A.DeptCode, A.PosCode, ");
            SQL.AppendLine("A.GrdLvlCode, A.SiteCode, A.Address, A.Village, A.CityCode, A.RTRW, A.SUbDistrict, A.PostalCOde, A.ResignDt, A.Remark, ");
          
            SQL.AppendLine("A.DomicileOld, A.IDNumberOld, A.MaritalStatusOld, A.WeddingDtOld, A.GenderOld, A.ReligionOld, A.BirthPlaceOld, A.BirthDtOld, ");
            SQL.AppendLine("A.BloodTypeOld, A.MotherOld, A.NPWPOld, A.PTKPOld, A.BankCodeOld, A.BankBranchOld, A.BankAcNameOld, A.BankACNoOld, ");
            SQL.AppendLine("A.PhoneOld, A.MobileOld, A.EmailOld, ");
           
            SQL.AppendLine("A.Domicile, A.IDNumber, A.MaritalStatus, A.WeddingDt, A.Gender, A.Religion, A.BirthPlace, A.BirthDt, ");
            SQL.AppendLine("A.BloodType, A.Mother, A.NPWP, A.PTKP, A.BankCode, A.BankBranch, A.BankAcName, A.BankACNo, ");
            SQL.AppendLine("A.Phone, A.Mobile, A.Email, A.EmpCodeOldOld, A.EmpCodeOld, A.EmpnameOld, A.EmpName, A.DisplaynameOld, A.DisplayName, A.TGDtOld, A.TGDt, ");
            SQL.AppendLine("A.SectionCodeOld, A.SectionCode, H.SectionName ");

            SQL.AppendLine("From TblEmployeeAmendment A  ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCodeOld = C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On A.PosCodeOld = D.PosCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On A.GrdLvlCodeOld = E.GrdLvlCode ");
            SQL.AppendLine("Left Join TblSite F On A.SiteCodeOld = F.SiteCode ");
            SQL.AppendLine("Left Join TblCity G On A.CityCodeOld= G.CityCode ");
            SQL.AppendLine("Left Join TblSection H On A.SectionCodeOld = H.SectionCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "EmpCode", "EmpName",  "DeptNameOld", "PosNameOld",  
                        //6-10
                        "GrdLvlNameOld", "SiteNameOld", "AddressOld", "VillageOld", "CityCOdeOld",  
                        //11-15
                        "RTRWOld", "SUbDistrictOld", "PostalCOdeOld", "DeptCode", "PosCode", 
                        //16-20
                        "GrdLvlCode","SiteCode", "Address", "Village", "CityCode", 
                        //21-25
                        "RTRW", "SUbDistrict", "PostalCOde", "ResignDt", "Remark",

                        //26-30
                        "DomicileOld", "IDNumberOld", "MaritalStatusOld", "WeddingDtOld", "GenderOld", 
                        //31-35
                        "ReligionOld", "BirthPlaceOld", "BirthDtOld", "BloodTypeOld", "MotherOld", 
                        //36-40
                        "NPWPOld", "PTKPOld", "BankCodeOld", "BankBranchOld", "BankAcNameOld", 
                        //41-45
                        "BankACNoOld", "PhoneOld", "MobileOld", "EmailOld","Domicile", 

                        //46-50
                        "IDNumber", "MaritalStatus", "WeddingDt", "Gender", "Religion", 
                        //51-55
                        "BirthPlace", "BirthDt", "BloodType", "Mother", "NPWP", 
                        //56-60
                        "PTKP", "BankCode", "BankBranch", "BankAcName", "BankACNo", 
                        //61-65
                        "Phone", "Mobile", "Email", "EmpCodeOldOld","EmpCodeOld", 
                        //66-70
                        "EmpNameOld", "EmpName", "DisplayNameOld", "DisplayName", "TGDtOld", 
                        //71-74
                        "TGDt", "SectionCodeOld", "SectionCode", "SectionName"
                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtEmpCode.EditValue = Sm.DrStr(dr, c[2]);
                    TxtDeptCodeOld.EditValue = Sm.DrStr(dr, c[4]);
                    TxtPosCodeOld.EditValue = Sm.DrStr(dr, c[5]);
                    TxtGrdLvlCodeOld.EditValue = Sm.DrStr(dr, c[6]);
                    TxtSiteCodeOld.EditValue = Sm.DrStr(dr, c[7]);
                    MeeAddressOld.EditValue = Sm.DrStr(dr, c[8]);
                    TxtVillageOld.EditValue = Sm.DrStr(dr, c[9]);
                    TxtCityOld.EditValue = Sm.DrStr(dr, c[10]);
                    TxtRTRWOld.EditValue = Sm.DrStr(dr, c[11]);
                    TxtSubDistrictOld.EditValue = Sm.DrStr(dr, c[12]);
                    TxtPostalCodeOld.EditValue = Sm.DrStr(dr, c[13]);
                    //Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[14]));
                    Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[14]), string.Empty);
                    Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[15]));
                    SetLueGrdLvlCode(ref LueGrdLvlCode, Sm.DrStr(dr, c[16]));
                    Sm.SetLue(LueGrdLvlCode, Sm.DrStr(dr, c[16]));
                    //SetLueSite(ref LueSiteCode, Sm.DrStr(dr, c[17]));
                    //Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[17]));
                    Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[17]), string.Empty);
                    MeeAddress.EditValue = Sm.DrStr(dr, c[18]);
                    TxtVillage.EditValue = Sm.DrStr(dr, c[19]);
                    Sm.SetLue(LueCity, Sm.DrStr(dr, c[20]));
                    TxtRTRW.EditValue = Sm.DrStr(dr, c[21]);
                    TxtSubDistrict.EditValue = Sm.DrStr(dr, c[22]);
                    TxtPostalCode.EditValue = Sm.DrStr(dr, c[23]);
                    Sm.SetDte(DteResignDt, Sm.DrStr(dr, c[24]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[25]);

                    MeeDomicileOld.EditValue = Sm.DrStr(dr, c[26]);
                    TxtIdNumberOld.EditValue = Sm.DrStr(dr, c[27]);
                    Sm.SetLue(LueMaritalStatusOld, Sm.DrStr(dr, c[28]));
                    Sm.SetDte(DteWeddingDtOld, Sm.DrStr(dr, c[29]));
                    Sm.SetLue(LueGenderOld, Sm.DrStr(dr, c[30]));
                    Sm.SetLue(LueReligionOld, Sm.DrStr(dr, c[31]));
                    TxtBirthPlaceOld.EditValue = Sm.DrStr(dr, c[32]);
                    Sm.SetDte(DteBirthDtOld, Sm.DrStr(dr, c[33]));
                    Sm.SetLue(LueBloodTypeOld, Sm.DrStr(dr, c[34]));
                    TxtMotherOld.EditValue = Sm.DrStr(dr, c[35]);
                    TxtNPWPOld.EditValue = Sm.DrStr(dr, c[36]);
                    Sm.SetLue(LuePTKPOld, Sm.DrStr(dr, c[37]));
                    Sm.SetLue(LueBankCodeOld, Sm.DrStr(dr, c[38]));
                    TxtBankBranchOld.EditValue = Sm.DrStr(dr, c[39]);
                    TxtBankAcNameOld.EditValue = Sm.DrStr(dr, c[40]);
                    TxtBankAcNoOld.EditValue = Sm.DrStr(dr, c[41]);
                    TxtPhoneOld.EditValue = Sm.DrStr(dr, c[42]);
                    TxtMobileOld.EditValue = Sm.DrStr(dr, c[43]);
                    TxtEmailOld.EditValue = Sm.DrStr(dr, c[44]);
                    MeeDomicile.EditValue = Sm.DrStr(dr, c[45]);
                    TxtIdNumber.EditValue = Sm.DrStr(dr, c[46]);
                    Sm.SetLue(LueMaritalStatus, Sm.DrStr(dr, c[47]));
                    Sm.SetDte(DteWeddingDt, Sm.DrStr(dr, c[48]));
                    Sm.SetLue(LueGender, Sm.DrStr(dr, c[49]));
                    Sm.SetLue(LueReligion, Sm.DrStr(dr, c[50]));
                    TxtBirthPlace.EditValue = Sm.DrStr(dr, c[51]);
                    Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[52]));
                    Sm.SetLue(LueBloodType, Sm.DrStr(dr, c[53]));
                    TxtMother.EditValue = Sm.DrStr(dr, c[54]);
                    TxtNPWP.EditValue = Sm.DrStr(dr, c[55]);
                    Sm.SetLue(LuePTKP, Sm.DrStr(dr, c[56]));
                    Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[57]));
                    TxtBankBranch.EditValue = Sm.DrStr(dr, c[58]);
                    TxtBankAcName.EditValue = Sm.DrStr(dr, c[59]);
                    TxtBankAcNo.EditValue = Sm.DrStr(dr, c[60]);
                    TxtPhone.EditValue = Sm.DrStr(dr, c[61]);
                    TxtMobile.EditValue = Sm.DrStr(dr, c[62]);
                    TxtEmail.EditValue = Sm.DrStr(dr, c[63]);
                    TxtEmpCodeOldOld.EditValue = Sm.DrStr(dr, c[64]);
                    TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[65]);
                    TxtEmpNameOld.EditValue = Sm.DrStr(dr, c[66]);
                    TxtEmpName.EditValue = Sm.DrStr(dr, c[67]);
                    TxtDisplayNameOld.EditValue = Sm.DrStr(dr, c[68]);
                    TxtDisplayName.EditValue = Sm.DrStr(dr, c[69]);
                    Sm.SetDte(DteTGDtOld, Sm.DrStr(dr, c[70]));
                    Sm.SetDte(DteTGDt, Sm.DrStr(dr, c[71]));
                    TxtSectionOld.EditValue = Sm.DrStr(dr, c[74]);
                    Sm.SetLue(LueSection, Sm.DrStr(dr, c[73]));
                }, true
        );

        }

        public void ShowDataEmployee(string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode,A.EmpName, A.DeptCode, B.DeptName, A.PosCode, C.PosName, A.GrdLvlCode, D.GrdLvlName, ");
            SQL.AppendLine("A.SiteCode, E.SiteName, A.ResignDt, A.Address, A.CityCode, F.CityName, A.Village, A.SubDistrict, A.RTRW, ");
            SQL.AppendLine("A.PostalCode, A.Domicile, A.IDNumber, A.MaritalStatus, A.WeddingDt, A.Gender, A.Religion, A.BirthPlace, ");
            SQL.AppendLine("A.BirthDt, A.BloodType, A.Mother, A.NPWP, A.PTKP, A.BankCode, A.BankBranch, A.BankAcName, A.BankACNo, ");
            SQL.AppendLine("A.Phone, A.Mobile, A.Email, A.DisplayName, A.EmpCodeOld, A.TGDt, A.SectionCode, G.Sectionname  ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr D On A.GrdlvlCode=D.GrdLvlCode ");
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join TblCity F On A.CityCode = F.CityCode ");
            SQL.AppendLine("left Join Tblsection G On A.SectionCode = G.SectionCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                    //0
                    "EmpCode", 
                    //1-5
                    "EmpName", "DeptCode", "DeptName", "PosCode", "PosName", 
                    //6-10
                    "GrdLvlCode", "GrdLvlName", "SiteCode", "SiteName", "ResignDt",
                    //11-15
                    "Address", "CityCode", "CityName", "Village", "SubDistrict",  
                    //16-20
                    "RTRW", "PostalCode", "Domicile", "IDNumber", "MaritalStatus", 
                    //21-25
                    "WeddingDt", "Gender", "Religion", "BirthPlace", "BirthDt", 
                    //26-30
                    "BloodType", "Mother", "NPWP", "PTKP", "BankCode",  
                    //31-35
                    "BankBranch", "BankAcName", "BankACNo", "Phone", "Mobile", 
                    //36-40
                    "Email", "DisplayName", "EmpCodeOld", "TGDt", "SectionCode",
                    //41
                    "SectionName"
                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    #region Show Data Emp Old 

                    TxtEmpCode.EditValue = Sm.DrStr(dr, c[0]);
                    TxtEmpNameOld.EditValue = Sm.DrStr(dr, c[1]);
                    DeptCodeOld = Sm.DrStr(dr, c[2]);
                    TxtDeptCodeOld.EditValue = Sm.DrStr(dr, c[3]);
                    PosCodeOld = Sm.DrStr(dr, c[4]);
                    TxtPosCodeOld.EditValue = Sm.DrStr(dr, c[5]);
                    GrdLvlCodeOld = Sm.DrStr(dr, c[6]);
                    TxtGrdLvlCodeOld.EditValue = Sm.DrStr(dr, c[7]);
                    SiteCodeOld = Sm.DrStr(dr, c[8]);
                    TxtSiteCodeOld.EditValue = Sm.DrStr(dr, c[9]);
                    Sm.SetDte(DteResignDt, Sm.DrStr(dr, c[10]));
                    MeeAddressOld.EditValue = Sm.DrStr(dr, c[11]);
                    CityOld = Sm.DrStr(dr, c[12]);
                    TxtCityOld.EditValue = Sm.DrStr(dr, c[13]);
                    TxtVillageOld.EditValue = Sm.DrStr(dr, c[14]);
                    TxtSubDistrictOld.EditValue = Sm.DrStr(dr, c[15]);
                    TxtRTRWOld.EditValue = Sm.DrStr(dr, c[16]);
                    TxtPostalCodeOld.EditValue = Sm.DrStr(dr, c[17]);
                    MeeDomicileOld.EditValue = Sm.DrStr(dr, c[18]);
                    TxtIdNumberOld.EditValue = Sm.DrStr(dr, c[19]);
                    Sm.SetLue(LueMaritalStatusOld, Sm.DrStr(dr, c[20]));
                    Sm.SetDte(DteWeddingDtOld, Sm.DrStr(dr, c[21]));
                    Sm.SetLue(LueGenderOld, Sm.DrStr(dr, c[22]));
                    Sm.SetLue(LueReligionOld, Sm.DrStr(dr, c[23]));
                    TxtBirthPlaceOld.EditValue = Sm.DrStr(dr, c[24]);
                    Sm.SetDte(DteBirthDtOld, Sm.DrStr(dr, c[25]));
                    Sm.SetLue(LueBloodTypeOld, Sm.DrStr(dr, c[26]));
                    TxtMotherOld.EditValue = Sm.DrStr(dr, c[27]);
                    TxtNPWPOld.EditValue = Sm.DrStr(dr, c[28]);
                    Sm.SetLue(LuePTKPOld, Sm.DrStr(dr, c[29]));
                    Sm.SetLue(LueBankCodeOld, Sm.DrStr(dr, c[30]));
                    TxtBankBranchOld.EditValue = Sm.DrStr(dr, c[31]);
                    TxtBankAcNameOld.EditValue = Sm.DrStr(dr, c[32]);
                    TxtBankAcNoOld.EditValue = Sm.DrStr(dr, c[33]);
                    TxtPhoneOld.EditValue = Sm.DrStr(dr, c[34]);
                    TxtMobileOld.EditValue = Sm.DrStr(dr, c[35]);
                    TxtEmailOld.EditValue = Sm.DrStr(dr, c[36]);
                    TxtDisplayNameOld.EditValue = Sm.DrStr(dr, c[37]);
                    TxtEmpCodeOldOld.EditValue = Sm.DrStr(dr, c[38]);
                    Sm.SetDte(DteTGDtOld, Sm.DrStr(dr, c[39]));
                    SectionCodeOld = Sm.DrStr(dr, c[40]);
                    TxtSectionOld.EditValue = Sm.DrStr(dr, c[41]);
                    #endregion

                    #region Show Data Emp New

                    Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[2]));
                    Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[4]));
                    Sm.SetLue(LueGrdLvlCode, Sm.DrStr(dr, c[6]));
                    //SetLueSite(ref LueSiteCode, Sm.DrStr(dr, c[8]));
                    Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[8]));
                    MeeAddress.EditValue = Sm.DrStr(dr, c[11]);
                    Sm.SetLue(LueCity, Sm.DrStr(dr, c[12]));
                    TxtVillage.EditValue = Sm.DrStr(dr, c[14]);
                    TxtSubDistrict.EditValue = Sm.DrStr(dr, c[15]);
                    TxtRTRW.EditValue = Sm.DrStr(dr, c[16]);
                    TxtPostalCode.EditValue = Sm.DrStr(dr, c[17]);
                    MeeDomicile.EditValue = Sm.DrStr(dr, c[18]);
                    TxtIdNumber.EditValue = Sm.DrStr(dr, c[19]);
                    Sm.SetLue(LueMaritalStatus, Sm.DrStr(dr, c[20]));
                    Sm.SetDte(DteWeddingDt, Sm.DrStr(dr, c[21]));
                    Sm.SetLue(LueGender, Sm.DrStr(dr, c[22]));
                    Sm.SetLue(LueReligion, Sm.DrStr(dr, c[23]));
                    TxtBirthPlace.EditValue = Sm.DrStr(dr, c[24]);
                    Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[25]));
                    Sm.SetLue(LueBloodType, Sm.DrStr(dr, c[26]));
                    TxtMother.EditValue = Sm.DrStr(dr, c[27]);
                    TxtNPWP.EditValue = Sm.DrStr(dr, c[28]);
                    Sm.SetLue(LuePTKP, Sm.DrStr(dr, c[29]));
                    Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[30]));
                    TxtBankBranch.EditValue = Sm.DrStr(dr, c[31]);
                    TxtBankAcName.EditValue = Sm.DrStr(dr, c[32]);
                    TxtBankAcNo.EditValue = Sm.DrStr(dr, c[33]);
                    TxtPhone.EditValue = Sm.DrStr(dr, c[34]);
                    TxtMobile.EditValue = Sm.DrStr(dr, c[35]);
                    TxtEmail.EditValue = Sm.DrStr(dr, c[36]);
                    TxtDisplayName.EditValue = Sm.DrStr(dr, c[37]);
                    TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[38]);
                    TxtEmpName.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetDte(DteTGDt, Sm.DrStr(dr, c[39]));
                    Sm.SetLue(LueSection, Sm.DrStr(dr, c[40]));
                    #endregion
                }, true
        );

        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsNotAbleToAmendResigneeInfo = Sm.GetParameterBoo("IsNotAbleToAmendResigneeInfo");
            //mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mIsEmpOldCodeEditable = Sm.GetParameterBoo("IsEmpOldCodeEditable");
            mIsEmployeeUseBiologicalMother = Sm.GetParameterBoo("IsEmployeeUseBiologicalMother");
        }

        //private void SetLueSite(ref DXE.LookUpEdit Lue, string SiteCode)
        //{
        //    try
        //    {
        //        var SQL = new StringBuilder();

        //        SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
        //        SQL.AppendLine("From TblSite T ");
        //        SQL.AppendLine("Where (T.ActInd='Y' ");
        //        if (SiteCode.Length != 0)
        //            SQL.AppendLine("Or T.SiteCode='" + SiteCode + "' ");
        //        SQL.AppendLine(") ");
        //        SQL.AppendLine("Order By SiteName; ");

        //        Sm.SetLue2(
        //            ref Lue, SQL.ToString(),
        //            0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        private void SetLueGrdLvlCode(ref DXE.LookUpEdit Lue, string GrdLvlCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.GrdLvlCode As Col1, T.GrdLvlName As Col2 ");
                SQL.AppendLine("From TblGradeLevelHdr T ");
                if (GrdLvlCode.Length != 0)
                    SQL.AppendLine("Where T.GrdLvlCode='" + GrdLvlCode + "' ");
                //else
                //{
                //    if (!mIsNotFilterByAuthorization)
                //    {
                //        SQL.AppendLine("Where GrdLvlCode In ( ");
                //        SQL.AppendLine("    Select T2.GrdLvlCode ");
                //        SQL.AppendLine("    From TblPPAHdr T1 ");
                //        SQL.AppendLine("    Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                //        SQL.AppendLine("    Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                //        SQL.AppendLine(") ");
                //    }
                //}
                SQL.AppendLine("Order By GrdLvlName; ");
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.SetLue2(
                    ref Lue, ref cm,
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmEmployeeAmendmentDlg(this));
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void LueGrdLvlCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueGrdLvlCode, new Sm.RefreshLue2(SetLueGrdLvlCode), string.Empty);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
        }

        private void BtnEmpCode2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtEmpCode, "Employee", false))
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = TxtEmpCode.Text;
                f1.ShowDialog();
            }
        }

        private void LueMaritalStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueMaritalStatus, new Sm.RefreshLue2(Sl.SetLueOption), "MaritalStatus");
        }

        private void LueGender_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueGender, new Sm.RefreshLue1(Sl.SetLueGender));
        }

        private void LueReligion_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueReligion, new Sm.RefreshLue1(Sl.SetLueReligion));
        }

        private void LueBloodType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBloodType, new Sm.RefreshLue2(Sl.SetLueOption), "BloodType");
        }

        private void TxtMother_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtMother);
        }

        private void LuePTKP_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePTKP, new Sm.RefreshLue1(Sl.SetLuePTKP));
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void TxtBankBranch_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtBankBranch);
        }

        private void TxtBankAcName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtBankAcName);
        }

        private void TxtBankAcNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtBankAcNo);
        }

        private void TxtPhone_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPhone);
        }

        private void TxtMobile_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtMobile);
        }

        private void TxtEmail_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtEmail);
        }

        private void LueMaritalStatusOld_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueMaritalStatus, new Sm.RefreshLue2(Sl.SetLueOption), "MaritalStatus");
        }

        private void LueGenderOld_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueGender, new Sm.RefreshLue1(Sl.SetLueGender));
        }

        private void LueReligionOld_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueReligion, new Sm.RefreshLue1(Sl.SetLueReligion));
        }

        private void LueBloodTypeOld_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBloodType, new Sm.RefreshLue2(Sl.SetLueOption), "BloodType");
        }

        private void TxtMotherOld_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtMother);
        }

        private void LuePTKPOld_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePTKP, new Sm.RefreshLue1(Sl.SetLuePTKP));
        }

        private void LueCity_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCity, new Sm.RefreshLue1(Sl.SetLueCityCode));
        }

        private void LueSection_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSection, new Sm.RefreshLue1(Sl.SetLueSectionCode));
        }

        #endregion

        
    }
}
