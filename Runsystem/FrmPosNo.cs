﻿#region Update
/*
    04/07/2018 [TKG] Tambah site, tambah amount before discount
    29/05/2019 [TKG] Tambah cost center
 *  20/04/2021 [HAR/PHT] tambah inputan pos name
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPosNo : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmPosNoFind FrmFind;

        #endregion

        #region Constructor

        public FrmPosNo(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueWhsCode(ref LueWhsCode);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standar Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnCopy.Enabled = !BtnSave.Enabled;
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtPosNo, TxtPosName, LueCtCode, LueWhsCode, LueSiteCode, LueCCCode }, true);
                    TxtPosNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtPosNo, TxtPosName, LueCtCode, LueWhsCode, LueSiteCode, LueCCCode }, false);
                    TxtPosNo.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueWhsCode, TxtPosName, LueCtCode, LueSiteCode, LueCCCode }, false);
                    LueCtCode.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtPosNo, TxtPosName, LueCtCode, LueWhsCode, LueSiteCode, LueCCCode });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPosNoFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetLue(LueCtCode, Sm.GetParameter("StoreCtCode"));
                Sm.SetLue(LueWhsCode, Sm.GetParameter("WhsCodeForStore"));
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                SetLueCCCode(ref LueCCCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPosNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPosNo, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblPosNo Where PosNo=@PosNol;" };
                Sm.CmParam<String>(ref cm, "@PosNo", TxtPosNo.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblPosNo(PosNo, CtCode, WhsCode, SiteCode, CCCode, PosName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@PosNo, @CtCode, @WhsCode, @SiteCode, @CCCode, @PosName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update CtCode=@CtCode, WhsCode=@WhsCode, SiteCode=@SiteCode, CCCode=@CCCode, PosName=@PosName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@PosNo", TxtPosNo.Text);
                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@PosName", TxtPosName.Text);
                Sm.ExecCommand(cm);

                ShowData(TxtPosNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PosNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@PosNo", PosNo);
                Sm.ShowDataInCtrl(
                        ref cm, "Select CtCode, WhsCode, SiteCode, CCCode, PosName From TblPosNo Where PosNo=@PosNo; ",
                        new string[]{ "CtCode", "WhsCode", "SiteCode", "CCCode", "PosName" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPosNo.EditValue = PosNo;
                            Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[0]));
                            Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[1]));
                            Sl.SetLueSiteCode2(ref LueSiteCode, Sm.DrStr(dr, c[2]));
                            SetLueCCCode(ref LueCCCode, Sm.DrStr(dr, c[3]));
                            TxtPosName.EditValue = Sm.DrStr(dr, c[4]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPosNo, "Pos number", false) ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                IsPosNoExisted();
        }

        private bool IsPosNoExisted()
        {
            if (!TxtPosNo.Properties.ReadOnly &&
                Sm.IsDataExist("Select 1 From TblPosNo Where PosNo=@Param;", TxtPosNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Pos Number ( " + TxtPosNo.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #region Additional Method

        private void SetLueCCCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.CCCode As Col1, T.CCName As Col2 ");
            SQL.AppendLine("From TblCostCenter T ");
            SQL.AppendLine("Where T.CCCode Not In (Select Parent From TblCostCenter Where Parent Is Not Null) ");
            if (Code.Length > 0)
                SQL.AppendLine("And (T.ActInd='Y' Or T.CCCode=@Code) ");
            else
                SQL.AppendLine("And T.ActInd='Y' ");
            SQL.AppendLine("Order By T.CCName;");
            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(SetLueCCCode), string.Empty);
        }

        #endregion

        #region Button Event

        private void BtnCopy_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPosNo, "PoS number", false)) return;
            string PosNo = string.Empty; 
            try 
            {
                if (Sm.InputBox("Copy PoS Setting", "PoS Number (Source) : ", ref PosNo) == DialogResult.OK)
                {
                    if (PosNo.Length == 0) return;

                    if (Sm.IsDataExist("Select 1 From TblPosNo Where PosNo=@Param;", PosNo))
                    {
                        if (Sm.IsDataExist("Select 1 From TblPosSetting Where PosNo=@Param Limit 1;", TxtPosNo.Text))
                        {
                            if (Sm.StdMsgYN("Question", 
                                "PoS Number : " + TxtPosNo.Text + Environment.NewLine +
                                "This PoS number already has setting." + Environment.NewLine +
                                "Do you want to replace with the new ones ?"
                                ) == DialogResult.Yes)
                                CopyData(PosNo, true);
                        }
                        else
                            CopyData(PosNo, false);

                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "PoS Number : " + PosNo + Environment.NewLine + "This PoS number is not existed.");
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void CopyData(string PosNo, bool DelInd)
        {
            var cm = new MySqlCommand() 
            { 
                CommandText = 
                    (DelInd?"Delete From TblPosSetting Where PosNo=@PosNo;":string.Empty) +
                    "Insert TblPosSetting(PosNo, SetCode, SetValue, CreateBy, CreateDt) " +
                    "Select @PosNo, SetCode, SetValue, @UserCode, CurrentDateTime() " +
                    "From TblPosSetting Where PosNo=@Source; "
            };
            Sm.CmParam<String>(ref cm, "@Source", PosNo);
            Sm.CmParam<String>(ref cm, "@PosNo", TxtPosNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
            Sm.ExecCommand(cm);

            Sm.StdMsg(mMsgType.Info, "Updating process is completed.");
        }

        #endregion        

        #endregion
    }
}
