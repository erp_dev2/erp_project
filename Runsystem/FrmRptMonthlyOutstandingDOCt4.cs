﻿#region Update
/*
    16/06/2021 [SET/KSM] Reporting Outstanding DO to Customer (Stock WO)
    18/06/2021 [RDH/KSM] add query  Reporting Outstanding DO to Customer (Stock WO)
    05/09/2021 [TKG/KSM] rombak aplikasi
    24/09/2021 [MYA/KSM] Penyesuaian Print Out pada menu Monthly Outstanding DO to Customer (Stock WO)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using System.Collections;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthlyOutstandingDOCt4 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private string 
            mWhsCodeForMonthlyOutstadingDoct4 = string.Empty,
            mItemCtCodeForMonthlyOutstadingDoct4 = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMonthlyOutstandingDOCt4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueItCtCodeActive(ref LueItCtCode, string.Empty);
                SetGrd();
                GetParameter();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'WhsCodeForMonthlyOutstadingDoct4', 'ItemCtCodeForMonthlyOutstadingDoct4' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {                            
                            //string
                            case "WhsCodeForMonthlyOutstadingDoct4": mWhsCodeForMonthlyOutstadingDoct4 = ParValue; break;
                            case "ItemCtCodeForMonthlyOutstadingDoct4": mItemCtCodeForMonthlyOutstadingDoct4 = ParValue; break;   
                        }
                    }
                }
                dr.Close();
            }
        }

        private string GetSQL(string Filter1, string Filter2, string Filter3)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select G.ItName, I.ItCtName, A.DocDt, B.Qty, G.PurchaseUomCode, H.VdName, IfNull(J.Qty, 0.00) As RecvVdQty, ");
            SQL.AppendLine("B.Qty-IfNull(J.Qty, 0.00) As Balance, E.CurCode, F.UPrice ");
            SQL.AppendLine("From TblPOHdr A");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo");
            SQL.AppendLine("Inner Join TblQtHdr E On C.QtDocNo=E.DocNo ");
            if (Filter2.Length > 0) SQL.AppendLine(Filter2.Replace("X.", "E."));
            SQL.AppendLine("Inner Join TblQtDtl F On C.QtDocNo=F.DocNo And C.QtDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblItem G On D.ItCode=G.ItCode ");
            if (mItemCtCodeForMonthlyOutstadingDoct4.Length > 1)
                SQL.AppendLine("And Find_In_Set(G.ItCtCode, @param2) ");
            if (Filter1.Length>0) SQL.AppendLine(Filter1.Replace("X.", "G."));
            if (Filter3.Length > 0) SQL.AppendLine(Filter3.Replace("X.", "G."));
            SQL.AppendLine("Inner Join TblVendor H On E.VdCode=H.VdCode ");
            SQL.AppendLine("Inner Join TblItemCategory I On G.ItCtCode=I.ItCtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, Sum(T3.QtyPurchase) As Qty ");
            SQL.AppendLine("    From TblPOHdr T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T3 On T2.DocNo=T3.PODocNo And T2.DNo=T3.PODNo And T3.CancelInd='N' And T3.Status='A' ");
            SQL.AppendLine("    Inner Join TblRecvVdHdr T4 On T3.DocNo=T4.DocNo ");
            if (mWhsCodeForMonthlyOutstadingDoct4.Length > 1)
                SQL.AppendLine("    And Find_In_Set(T4.WhsCode,@param1) ");
            if (Filter2.Length > 0) SQL.AppendLine(Filter2.Replace("X.", "T4."));
            SQL.AppendLine("Inner Join TblItem T5 On T3.ItCode=T5.ItCode ");
            if (mItemCtCodeForMonthlyOutstadingDoct4.Length > 1)
                SQL.AppendLine("And Find_In_Set(T5.ItCtCode, @param2) ");
            if (Filter1.Length > 0) SQL.AppendLine(Filter1.Replace("X.", "T5."));
            if (Filter3.Length > 0) SQL.AppendLine(Filter3.Replace("X.", "T5."));
            SQL.AppendLine("    Where T1.Status='A' ");
            SQL.AppendLine("    And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine("    ) J On B.DocNo=J.DocNo And B.DNo=J.DNo ");
            SQL.AppendLine("Where A.Status='A' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Order By G.ItName, A.DocDt; ");

            //SQL.AppendLine("SELECT T1.ItName,T1.ItCtName,T1.DocDt,T1.Qty,Uom,VdName,IFNULL(T2.Qty1,0) QtyPurchase ");
            //SQL.AppendLine(",(T1.Qty - IFNULL(T2.Qty1,0)) restOrder");
            //SQL.AppendLine(",T1.Uprice");
            //SQL.AppendLine("FROM (");
            //SQL.AppendLine("    SELECT H.itCode,H.ItName,A1.DocDt, Sum(A.Qty),A.Discount,A.DiscountAmt,A.RoundingValue,H.PurchaseUomCode UoM");
            //SQL.AppendLine("    ,I.VdCode,I.VdName,G.UPrice,J.ItCtCode,J.ItCtName");
            //SQL.AppendLine("    FROM tblpohdr A1");
            //SQL.AppendLine("    INNER JOIN TblPODtl A ON A1.DocNo = A.DocNo And A.CancelInd='N' ");
            //SQL.AppendLine("    INNER JOIN TblPORequestHdr B ON A.PORequestDocNo=B.DocNo");
            //SQL.AppendLine("    INNER JOIN TblPORequestDtl C ON A.PORequestDocNo=C.DocNo AND A.PORequestDNo=C.DNo");
            //SQL.AppendLine("    INNER JOIN TblMaterialRequestHdr D ON C.MaterialRequestDocNo=D.DocNo");
            //SQL.AppendLine("    INNER JOIN TblMaterialRequestDtl E ON C.MaterialRequestDocNo=E.DocNo AND C.MaterialRequestDNo=E.DNo");
            //SQL.AppendLine("    INNER JOIN TblQtHdr F ON C.QtDocNo=F.DocNo ");
            //SQL.AppendLine("    INNER JOIN TblQtDtl G ON C.QtDocNo=G.DocNo AND C.QtDNo=G.DNo ");
            //SQL.AppendLine("    INNER JOIN TblItem H ON E.ItCode=H.ItCode ");
            //SQL.AppendLine("    INNER JOIN TblVendor I ON F.VdCode=I.VdCode ");
            //SQL.AppendLine("    INNER JOIN tblitemcategory J ON H.ItCtCode = J.ItCtCode ");
            //SQL.AppendLine("    Where A.Status='A' Group By H.ItName ");
            //SQL.AppendLine(") T1");
            //SQL.AppendLine("LEFT JOIN (");
            //SQL.AppendLine("       SELECT L.itCode,SUM(IFNULL(L.Qty,0))as Qty1 FROM tblrecvvdhdr K");
            //SQL.AppendLine("       INNER JOIN tblrecvvddtl L ON K.DocNo = L.DocNo AND L.CancelInd = 'N'");
            //SQL.AppendLine("       INNER JOIN tblpodtl M ON L.PODocNo = M.DocNo AND L.PODNo = M.DNo");
            //SQL.AppendLine("       WHERE 1=1");

            //if (mWhsCodeForMonthlyOutstadingDoct4.Length > 1)
            //    SQL.AppendLine(" AND FIND_IN_SET(K.WhsCode,@param1)");

            //SQL.AppendLine("       GROUP BY L.ItCode");
            //SQL.AppendLine(")T2 ON T1.ItCode = T2.ItCode");
            //SQL.AppendLine("WHERE 1=1 ");

            //if (mItemCtCodeForMonthlyOutstadingDoct4.Length > 1)
            //    SQL.AppendLine("AND FIND_IN_SET(T1.ItCtCode,@param2)");
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Contruction",
                    "Item's Category",
                    "PO's Date",
                    "PO's Quantity",
                    "UoM", 
                    
                    //6-10
                    "Vendor",
                    "Received#",
                    "Rest Order",
                    "Currency",
                    "Item's Price"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    200, 200, 100, 100, 80,

                    //6-10
                    200, 150, 120, 100, 150
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 7, 8, 10 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date")
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter1 = " ", Filter2 = " ", Filter3 = " ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter1, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "X.ItName" });
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueVdCode), "X.VdCode", true);
                Sm.FilterStr(ref Filter3, ref cm, Sm.GetLue(LueItCtCode), "X.ItCtCode", true);

                Sm.CmParam<String>(ref cm, "@param1", mWhsCodeForMonthlyOutstadingDoct4);
                Sm.CmParam<String>(ref cm, "@param2", mItemCtCodeForMonthlyOutstadingDoct4);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter1, Filter2, Filter3),
                    new string[]
                    {
                        //0
                        "ItName",

                        //1-5 
                        "ItCtName",
                        "DocDt",
                        "Qty",
                        "PurchaseUomCode",
                        "VdName",                        

                        //6-9
                        "RecvVdQty",
                        "Balance",
                        "CurCode",
                        "UPrice"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            try
            {
                if (Grd1.Rows.Count == 0)
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                    return;
                }

                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Additional method

        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            var l = new List<Header>();
            var ldtl = new List<Detail>();

            string[] TableName = { "Header", "Detail" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            l.Add(new Header()
            {
                CompanyLogo = Sm.GetValue("Select Distinct @Param As CompanyLogo", @Sm.CompanyLogo()),
                CompanyName = Gv.CompanyName,
            });
            myLists.Add(l);

            #endregion

            #region Detail

            int nomor = 0;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                nomor = nomor + 1;
                ldtl.Add(new Detail()
                {
                    No = nomor,
                    ItName = Sm.GetGrdStr(Grd1, i, 1),
                    ItCtName = Sm.GetGrdStr(Grd1, i, 2),
                    DocDt = Sm.FormatDate2("dd/MMM/yyyy", Sm.GetGrdDate(Grd1, i, 3)),
                    Qty = Sm.GetGrdDec(Grd1, i, 4),
                    PurchaseUomCode = Sm.GetGrdStr(Grd1, i, 5),
                    VdName = Sm.GetGrdStr(Grd1, i, 6),
                    RecvVdQty = Sm.GetGrdDec(Grd1, i, 7),
                    Balance = Sm.GetGrdDec(Grd1, i, 8),
                    UPrice = Sm.GetGrdDec(Grd1, i, 10),
                });
            }
            myLists.Add(ldtl);

            #endregion

            Sm.PrintReport("OutstandingSW", myLists, TableName, false);

        }

        private void SetGrdHdr(ref iGrid Grd, int row, int col, string Title, int SpanRows, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = SpanRows;
            Grd.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = ColWidth;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCodeActive), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion

        #region Class

        private class Header
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
        }

        private class Detail
        {
            public int No { get; set; }
            public string ItName { get; set; }
            public string ItCtName { get; set; }
            public string DocDt { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUomCode { get; set; }
            public string VdName { get; set; }
            public decimal RecvVdQty { get; set; }
            public decimal Balance { get; set; }
            public decimal UPrice { get; set; }

        }
        #endregion
        
    }
}
