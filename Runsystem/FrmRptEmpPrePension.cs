﻿#region Update 
/*
    04/11/2020 [VIN/PHT] new apps
 *  03/03/2021 [ICA/PHT] menambah kondisi untuk resigndt kurang dari 6 bulan
 *  19/03/2021 [ICA/PHT] employee Pre Pension dibuat berdasarkan tanggal pension (bukan resigndt)
 *  04/05/2021 [ICA/PHT] mengubah param SSRetiredMaxAge menjadi SSRetiredMaxAge2
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpPrePension : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptEmpPrePension(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                BtnRefresh.Visible = BtnPrint.Visible = true;
                SetMainGrd();
                SetMainSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        public void SetMainSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.EmpCode, A.EmpName, A.EmpCodeOld ");
            SQL.AppendLine("From tblemployee A  ");
            if (Sm.GetParameter("DocTitle") == "PHT")
            {
                SQL.AppendLine("INNER JOIN ( ");
                SQL.AppendLine("    SELECT A.EmpCode, ");
                SQL.AppendLine("    case ");
                SQL.AppendLine("        when Right(A.BirthDt, 2)='01' then Date_Format(A.BirthDt+INTERVAL @SSRetiredMaxAge2 Year, '%Y%m%d')");
                SQL.AppendLine("        ELSE concat(Date_Format(A.BirthDt + INTERVAL '1' MONTH + INTERVAL @SSRetiredMaxAge2 Year, '%Y%m'), '01') ");
                SQL.AppendLine("    END AS Pension ");
                SQL.AppendLine("    From tblemployee A ");
                SQL.AppendLine("    WHERE BirthDt IS NOT NULL ");
                SQL.AppendLine(") B ON A.EmpCode = B.EmpCode");
                SQL.AppendLine("WHERE Left(B.Pension, 4) = Year(INTERVAL 6 MONTH + CURRENT_TIMESTAMP) ");
                SQL.AppendLine("    AND SUBSTRING(B.Pension, 5, 2) = MONTH(INTERVAL 6 Month + CURRENT_TIMESTAMP) ");
                SQL.AppendLine("    AND (A.ResignDt IS NULL OR A.ResignDt > Left(CurrentDateTime(), 8)) ");
            }
            else
            {
                SQL.AppendLine("WHERE resigndt IS NOT NULL  ");
                SQL.AppendLine("    AND (LEFT(resigndt, 4)) = (YEAR(CURRENT_TIMESTAMP))+1 ");
                SQL.AppendLine("    AND (SUBSTRING(resigndt, 5, 2)) = MONTH(CURRENT_TIMESTAMP) ");
            }
            

            mSQL = SQL.ToString();
        }

        public void SetMainGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "No",
                    

                    //1-5
                    "Employee Code",
                    "",
                    "Employee Name",
                    "Employee Code Old",

                },
                new int[] { 
                    
                    30, 
                    
                    150, 20, 200, 150
                
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4 });

        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SSRetiredMaxAge2", Sm.GetValue("Select parvalue From TblParameter Where Parcode = 'SSRetiredMaxAge2'"));
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpCodeOld", "A.EmpName" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.EmpCode;",
                        new string[]
                        {
                            //0
                            "EmpCode",

                            //1-2
                            "EmpName", "EmpCodeOld"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            

                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        
        #endregion

        #region Event

        private void TxtEmpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        #endregion 

        
    }
}
