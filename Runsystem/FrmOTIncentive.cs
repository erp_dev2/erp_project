﻿#region Update
/*
    13/03/2018 [WED] Employee bisa mengisi lebih dari satu baris jam insentif di satu dokumen
    03/04/2018 [TKG] tambah allowance 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmOTIncentive : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal bool 
            mIsFilterBySiteHR = false, 
            mIsFilterByDeptHR = false, 
            mIsSiteMandatory = false, 
            mIsFilterBySite = false;

        iGCell fCell;
        bool fAccept;

        internal FrmOTIncentiveFind FrmFind;

        #endregion

        #region Constructor

        public FrmOTIncentive(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Incentive OT";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo }, true);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                if (mIsSiteMandatory)
                    LblSiteCode.ForeColor = Color.Red;
                SetLueADCode(ref LueADCode);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Employee's" + Environment.NewLine + "Code",           
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        
                        //6-9
                        "Start"+ Environment.NewLine + "Time",
                        "End"+ Environment.NewLine + "Time",
                        "Remark",
                        "Resign"+ Environment.NewLine + "Date"
                    },
                     new int[] 
                    {
                        //0
                        50, 
                        
                        //1-5
                        20, 100, 200, 100, 150, 
                        
                        //6-9
                        60, 60, 300, 80
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 9 });
            Sm.GrdFormatTime(Grd1, new int[] { 6, 7 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5 }, false);
            Grd1.Cols[9].Move(6);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd, LueDeptCode, LueSiteCode, LueADCode, DteDocDt, 
                        DteOTIncentiveDt, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 6, 7, 8 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueDeptCode, LueSiteCode, LueADCode, DteOTIncentiveDt, 
                        MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 6, 7, 8 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueADCode }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueDeptCode, LueSiteCode, LueADCode, 
                DteOTIncentiveDt, MeeRemark, TmeOTTm
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmOTIncentiveFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                DteOTIncentiveDt.DateTime = DteDocDt.DateTime;
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", "", mMenuCode) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            if (e.KeyCode == Keys.Enter)
            {
                if (Grd1.CurCell.Col.Index != Grd1.Cols.Count - 1)
                    Sm.FocusGrd(Grd1, Grd1.CurRow.Index, Grd1.CurCell.Col.Index + 1);
                else
                    Sm.FocusGrd(Grd1, Grd1.CurRow.Index + 1, 6);
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                if (e.ColIndex == 1 &&
                    !Sm.IsLueEmpty(LueDeptCode, "Department") &&
                    (!mIsSiteMandatory || (mIsSiteMandatory && !Sm.IsLueEmpty(LueSiteCode, "Site")))
                    )
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmOTIncentiveDlg(this, Sm.GetLue(LueDeptCode), Sm.GetLue(LueSiteCode)));
                }
                if (Sm.IsGrdColSelected(new int[] { 1, 6, 7, 8 }, e.ColIndex))
                {
                    if (e.ColIndex == 6 || e.ColIndex == 7)
                        TmeRequestEdit(Grd1, TmeOTTm, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                }
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 &&
                TxtDocNo.Text.Length == 0 &&
                !Sm.IsLueEmpty(LueDeptCode, "Department") &&
                (!mIsSiteMandatory || (mIsSiteMandatory && !Sm.IsLueEmpty(LueSiteCode, "Site")))
                )
            {
                Sm.FormShowDialog(new FrmOTIncentiveDlg(this, Sm.GetLue(LueDeptCode), Sm.GetLue(LueSiteCode)));
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0 && Sm.IsGrdColSelected(new int[] { 6, 7 }, e.ColIndex))
            {
                var Tm = Sm.GetGrdStr(Grd1, 0, e.ColIndex);
                for (int Row = 1; Row < Grd1.Rows.Count - 1; Row++)
                    Grd1.Cells[Row, e.ColIndex].Value = Tm;
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 8 }, e);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "OTIncentive", "TblOTIncentiveHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveOTIncentiveHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveOTIncentiveDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsDteEmpty(DteOTIncentiveDt, "OT Incentive Date") ||
                (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                Sm.IsLueEmpty(LueADCode, "Allowance") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsDtForResingeeInvalid() ||
                IsTmeSignedInOtherRow() ||
                IsTmeSignedInOtherDocument() ||
                IsDataNotValid();
        }

        private bool IsDataNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsDataNotValid(r)) return true;
                }
            }
            return false;
        }

        private bool IsDataNotValid(int r)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblOTIncentiveHdr A ");
            SQL.AppendLine("Inner Join TblOTIncentiveDtl B On A.DocNo=B.DocNo And B.EmpCode=@EmpCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.OTIncentiveDt=@OTIncentiveDt ");
            SQL.AppendLine("And A.ADCode=@ADCode ");
            SQL.AppendLine("Limit 1;");

            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, r, 2));
            Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetLue(LueADCode));
            Sm.CmParamDt(ref cm, "@OTIncentiveDt", Sm.GetDte(DteOTIncentiveDt));

            cm.CommandText = SQL.ToString();
            return Sm.IsDataExist(cm, 
                "Employee's Code : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                "Invalid data."
                );
        }

        private bool IsTmeSignedInOtherDocument()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var l = new List<OTTme>();

            string mEmpCode = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (mEmpCode.Length > 0) mEmpCode += ",";
                    mEmpCode += Sm.GetGrdStr(Grd1, i, 2);
                }
            }

            SQL.AppendLine("Select A.DocNo, B.EmpCode, B.OTIncentiveStartTm, B.OTIncentiveEndTm ");
            SQL.AppendLine("From TblOTIncentiveHdr A ");
            SQL.AppendLine("Inner Join TblOTIncentiveDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And A.OTIncentiveDt = @OTIncentiveDt ");
            SQL.AppendLine("And Find_In_Set(B.EmpCode, @EmpCode); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParamDt(ref cm, "@OTIncentiveDt", Sm.GetDte(DteOTIncentiveDt));
                Sm.CmParam<String>(ref cm, "@EmpCode", mEmpCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "EmpCode", "OTIncentiveStartTm", "OTIncentiveEndTm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OTTme()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),

                            EmpCode = Sm.DrStr(dr, c[1]),
                            OTIncentiveStartTm = Sm.DrStr(dr, c[2]),
                            OTIncentiveEndTm = Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                    {
                        for (int r1 = 0; r1 < l.Count; r1++)
                        {
                            if (Sm.GetGrdStr(Grd1, r, 2) == l[r1].EmpCode)
                            {
                                decimal StartTm = 0m, EndTm = 0m;
                                StartTm = Decimal.Parse(Sm.GetGrdStr(Grd1, r, 6).Replace(":", ""));
                                EndTm = Decimal.Parse(Sm.GetGrdStr(Grd1, r, 7).Replace(":", ""));

                                if((StartTm >= Decimal.Parse(l[r1].OTIncentiveStartTm) && StartTm <= Decimal.Parse(l[r1].OTIncentiveEndTm)) ||
                                    (EndTm >= Decimal.Parse(l[r1].OTIncentiveStartTm) && EndTm <= Decimal.Parse(l[r1].OTIncentiveEndTm))
                                    )
                                {
                                    var mNotif = new StringBuilder();

                                    mNotif.AppendLine("Employee : " + Sm.GetGrdStr(Grd1, r, 3) + " (" + Sm.GetGrdStr(Grd1, r, 2) + ")");
                                    mNotif.AppendLine("Row #" + (r + 1) + " = " + Sm.GetGrdStr(Grd1, r, 6) + " until " + Sm.GetGrdStr(Grd1, r, 7));
                                    mNotif.AppendLine(Environment.NewLine);
                                    mNotif.AppendLine("OT Incentive time is already signed on this document : " + l[r1].DocNo + " at : ");
                                    mNotif.AppendLine("Row #" + (r1 + 1) + " = " + Sm.Left(l[r1].OTIncentiveStartTm, 2) + ":" + Sm.Right(l[r1].OTIncentiveStartTm, 2) + " until " + Sm.Left(l[r1].OTIncentiveEndTm, 2) + ":" + Sm.Right(l[r1].OTIncentiveEndTm, 2));

                                    Sm.StdMsg(mMsgType.Warning, mNotif.ToString());
                                    Sm.FocusGrd(Grd1, r, 6);
                                    l.Clear();
                                    return true;
                                }
                                else if ((Decimal.Parse(l[r1].OTIncentiveStartTm) >= StartTm && Decimal.Parse(l[r1].OTIncentiveStartTm) <= EndTm) ||
                                        (Decimal.Parse(l[r1].OTIncentiveEndTm) >= StartTm && Decimal.Parse(l[r1].OTIncentiveEndTm) <= EndTm)
                                        )
                                {
                                    var mNotif = new StringBuilder();

                                    mNotif.AppendLine("Employee : " + Sm.GetGrdStr(Grd1, r, 3) + " (" + Sm.GetGrdStr(Grd1, r, 2) + ")");
                                    mNotif.AppendLine("Row #" + (r + 1) + " = " + Sm.GetGrdStr(Grd1, r, 6) + " until " + Sm.GetGrdStr(Grd1, r, 7));
                                    mNotif.AppendLine(Environment.NewLine);
                                    mNotif.AppendLine("OT Incentive time is already signed on this document : " + l[r1].DocNo + " at : ");
                                    mNotif.AppendLine("Row #" + (r1 + 1) + " = " + Sm.Left(l[r1].OTIncentiveStartTm, 2) + ":" + Sm.Right(l[r1].OTIncentiveStartTm, 2) + " until " + Sm.Left(l[r1].OTIncentiveEndTm, 2) + ":" + Sm.Right(l[r1].OTIncentiveEndTm, 2));

                                    Sm.StdMsg(mMsgType.Warning, mNotif.ToString());
                                    Sm.FocusGrd(Grd1, r, 6);
                                    l.Clear();
                                    return true;
                                }
                            }
                        }
                    }
                }
            }

            l.Clear();

            return false;
        }

        private bool IsTmeSignedInOtherRow()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                for (int r1 = r + 1; r1 < Grd1.Rows.Count - 1; r1++)
                {
                    if (Sm.GetGrdStr(Grd1, r1, 2).Length > 0 &&
                        Sm.GetGrdStr(Grd1, r, 2).Length > 0 && 
                        Sm.GetGrdStr(Grd1, r1, 2) == Sm.GetGrdStr(Grd1, r, 2))
                    {
                        decimal StartTm1 = 0m, StartTm2 = 0m, EndTm1 = 0m, EndTm2 = 0m;
                        StartTm1 = Decimal.Parse(Sm.GetGrdStr(Grd1, r, 6).Replace(":", ""));
                        StartTm2 = Decimal.Parse(Sm.GetGrdStr(Grd1, r1, 6).Replace(":", ""));
                        EndTm1 = Decimal.Parse(Sm.GetGrdStr(Grd1, r, 7).Replace(":", ""));
                        EndTm2 = Decimal.Parse(Sm.GetGrdStr(Grd1, r1, 7).Replace(":", ""));

                        if((StartTm1 >= StartTm2 && StartTm1 <= EndTm2) ||
                            (EndTm1 >= StartTm2 && EndTm1 <= EndTm2)
                            )
                        {
                            var mNotif = new StringBuilder();

                            mNotif.AppendLine("Employee : " + Sm.GetGrdStr(Grd1, r, 3) + " (" + Sm.GetGrdStr(Grd1, r, 2) + ")");
                            mNotif.AppendLine("Row #" + (r + 1) + " = " + Sm.GetGrdStr(Grd1, r, 6) + " until " + Sm.GetGrdStr(Grd1, r, 7));
                            mNotif.AppendLine("Row #" + (r1 + 1) + " = " + Sm.GetGrdStr(Grd1, r1, 6) + " until " + Sm.GetGrdStr(Grd1, r1, 7));
                            mNotif.AppendLine(Environment.NewLine);
                            mNotif.AppendLine("OT Incentive time is invalid. ");

                            Sm.StdMsg(mMsgType.Warning, mNotif.ToString());

                            Sm.FocusGrd(Grd1, r1, 6);

                            return true;
                        }
                        else if ((StartTm2 >= StartTm1 && StartTm2 <= EndTm1) ||
                                (EndTm2 >= StartTm1 && EndTm2 <= EndTm1)
                                )
                        {
                            var mNotif = new StringBuilder();

                            mNotif.AppendLine("Employee : " + Sm.GetGrdStr(Grd1, r, 3) + " (" + Sm.GetGrdStr(Grd1, r, 2) + ")");
                            mNotif.AppendLine("Row #" + (r + 1) + " = " + Sm.GetGrdStr(Grd1, r, 6) + " until " + Sm.GetGrdStr(Grd1, r, 7));
                            mNotif.AppendLine("Row #" + (r1 + 1) + " = " + Sm.GetGrdStr(Grd1, r1, 6) + " until " + Sm.GetGrdStr(Grd1, r1, 7));
                            mNotif.AppendLine(Environment.NewLine);
                            mNotif.AppendLine("OT Incentive time is invalid. ");

                            Sm.StdMsg(mMsgType.Warning, mNotif.ToString());

                            Sm.FocusGrd(Grd1, r1, 6);

                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsDtForResingeeInvalid()
        {
            string EmpCode = string.Empty;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                if (EmpCode.Length > 0)
                {
                    if (IsDocDtForResingeeInvalid(EmpCode, r)) return true;
                    if (IsOTIncentiveDtForResingeeInvalid(EmpCode, r)) return true;
                }
            }
            return false;
        }

        private bool IsDocDtForResingeeInvalid(string EmpCode, int r)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@DocDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + EmpCode + Environment.NewLine +
                    "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                    "Resign Date : " + Sm.FormatDate2("dd/MMM/yyyy", Sm.GetGrdDate(Grd1, r, 9)) + Environment.NewLine +
                    "Document Date :" + DteDocDt.Text + Environment.NewLine + Environment.NewLine +
                    "Document date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsOTIncentiveDtForResingeeInvalid(string EmpCode, int r)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@OTDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@OTDt", Sm.GetDte(DteOTIncentiveDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + EmpCode + Environment.NewLine +
                    "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                    "Resign Date : " + Sm.FormatDate2("dd/MMM/yyyy", Sm.GetGrdDate(Grd1, r, 9)) + Environment.NewLine +
                    "OT Incentive Date : " + DteOTIncentiveDt.Text + Environment.NewLine + Environment.NewLine +
                    "OT Incentive date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        //private bool IsOTIncentiveNotValid(int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select A.DocNo From TblOTIncentiveHdr A ");
        //    SQL.AppendLine("Inner Join TblOTIncentiveDtl B On A.DocNo=B.DocNo And B.EmpCode=@EmpCode ");
        //    SQL.AppendLine("Where A.CancelInd='N' And A.OTIncentiveDt=@OTIncentiveDt Limit 1;");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParamDt(ref cm, "@OTIncentiveDt", Sm.GetDte(DteOTIncentiveDt));
        //    Sm.CmParamDt(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
        //    if (Sm.IsDataExist(cm))
        //    {
        //        Sm.StdMsg(mMsgType.Warning,
        //            "Employee Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
        //            "Employee Name : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine + Environment.NewLine +
        //            "Duplicate OT Incentive Date."
        //            );
        //        return true;
        //    }
        //    else
        //        return false;
        //}

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Employee is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 6, false, "Start time is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 7, false, "End time is empty.")
                    ) return true;

                //if (IsOTIncentiveNotValid(Row)) return true;
            }
            return false;
        }

        private MySqlCommand SaveOTIncentiveHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblOTIncentiveHdr(DocNo, DocDt, CancelInd, DeptCode, SiteCode, ADCode, OTIncentiveDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @DeptCode, @SiteCode, @ADCode, @OTIncentiveDt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetLue(LueADCode));
            Sm.CmParamDt(ref cm, "@OTIncentiveDt", Sm.GetDte(DteOTIncentiveDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveOTIncentiveDtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblOTIncentiveDtl(DocNo, DNo, EmpCode, OTIncentiveStartTm, OTIncentiveEndTm, Remark, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @EmpCode, @OTIncentiveStartTm, @OTIncentiveEndTm, @Remark, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@OTIncentiveStartTm", Sm.GetGrdStr(Grd1, Row, 6).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@OTIncentiveEndTm", Sm.GetGrdStr(Grd1, Row, 7).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditOTIncentiveDtl());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return 
                //IsDataNotCancelled() || 
                Sm.IsLueEmpty(LueADCode, "Allowance") ||
                IsDataAlreadyCancelled();
        }

        private bool IsDataNotCancelled()
        {
            if(!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblOTIncentiveHdr ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='Y'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document has been cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditOTIncentiveDtl()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblOTIncentiveHdr Set ");
            SQL.AppendLine("    ADCode=@ADCode, CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetLue(LueADCode));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowOTIncentiveHdr(DocNo);
                ShowOTIncentiveDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowOTIncentiveHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, CancelInd, DeptCode, SiteCode, ADCode, OTIncentiveDt, Remark ");
            SQL.AppendLine("From TblOTIncentiveHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                       "DocNo", 
                       
                       //1-5
                       "DocDt", "CancelInd", "DeptCode", "SiteCode", "ADCode", 
                       
                       //6-7
                       "OTIncentiveDt", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[3]), string.Empty);
                        Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[4]), string.Empty);
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueADCode, Sm.DrStr(dr, c[5]));
                        Sm.SetDte(DteOTIncentiveDt, Sm.DrStr(dr, c[6]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
            );
        }

        private void ShowOTIncentiveDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, B.ResignDt, A.OTIncentiveStartTm, A.OTIncentiveEndTm, A.Remark ");
            SQL.AppendLine("From TblOTIncentiveDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B on A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode = C.PosCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DocNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    
                    //1-5
                    "EmpCode", "EmpName", "EmpCodeOld", "PosName", "OTIncentiveStartTm", 
                    
                    //6-8
                    "OTIncentiveEndTm", "Remark", "ResignDt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        private void SetLueADCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue, "Select ADCode As Col1, ADName As Col2 From TblAllowanceDeduction Where ADType='A' Order By ADName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueBankAcCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (Sm.GetParameter("BankAccountFormat") == "1")
            {
                SQL.AppendLine("Select ADCode As Col1, ADName As Col2 From TblAllowanceDeduction ");
                SQL.AppendLine("Where ADType='A' Order By ADName;");
            }
            else
            {
                SQL.AppendLine("Select A.BankAcCode As Col1, ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                SQL.AppendLine(")) As Col2 ");
                SQL.AppendLine("From TblBankAccount A ");
                SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            }


            if (Sm.GetParameterBoo("IsVoucherBankAccountFilteredByGrp"))
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            SQL.AppendLine("Order By A.Sequence;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void TmeRequestEdit(iGrid Grd, DevExpress.XtraEditors.TimeEdit Tme, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Tme.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Tme.EditValue = null;
            else
                Sm.SetTme(Tme, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Replace(":", ""));

            Tme.Visible = true;
            Tme.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                ClearGrd();
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                ClearGrd();
            }
        }

        private void LueADCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueADCode, new Sm.RefreshLue1(SetLueADCode));
        }

        private void TmeOTTm_Leave(object sender, EventArgs e)
        {
            if (TmeOTTm.Visible && fAccept &&
                Sm.IsGrdColSelected(new int[] { 6, 7 }, fCell.ColIndex)
                )
            {
                var Tm = Sm.GetTme(TmeOTTm);
                if (Tm.Length == 0)
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                else
                {
                    if (Tm.Length == 4) Tm = Sm.Left(Tm, 2) + ":" + Sm.Right(Tm, 2);
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = Tm;
                }
                TmeOTTm.Visible = false;
            }
        }

        #endregion

        #endregion

        #region Class

        class OTTme
        {
            public string DocNo { get; set; }
            public string EmpCode { get; set; }
            public string OTIncentiveStartTm { get; set; }
            public string OTIncentiveEndTm { get; set; }
        }

        #endregion
    }
}
