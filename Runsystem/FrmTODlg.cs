﻿#region Update
/*
    15/07/2017 [TKG] Menampilkan asset tanpa difilter depreciation method
    31/01/2018 [WED] tambah copy Display Name ke Main Form TO
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTODlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmTO mFrmParent;
        private string mSQL=string.Empty;

        #endregion

        #region Constructor

        public FrmTODlg(FrmTO FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AssetCode, A.Assetname, A.DisplayName, C.AssetCategoryName, A.AssetValue, A.EcoLife, B.OptDesc As Depreciation, ");
            SQL.AppendLine("A.PercentageAnnualDepreciation, A.AssetDt ");
            SQL.AppendLine("From TblAsset A ");
            SQL.AppendLine("Left Join TblOption B On A.DepreciationCode = B.OptCode And OptCat='DepreciationMethod'");
            SQL.AppendLine("Left Join TblAssetCategory C On A.AssetCategoryCode=C.AssetCategoryCode ");
            SQL.AppendLine("Where A.ActiveInd = 'Y' ");
            SQL.AppendLine("And A.AssetCode Not In ( ");
            SQL.AppendLine("    Select AssetCode ");
            SQL.AppendLine("    From TblTODtl T1 ");
            SQL.AppendLine("    Inner Join TblWOR T2 On T1.AssetCode=T2.TOCode And T2.Status='O' ");
            SQL.AppendLine("    ) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count=11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Asset Code", 
                        "", 
                        "Asset Name",
                        "Display Name",
                        "Asset Category",
                        
                        //6-10
                        "Asset Value",
                        "Economic Life",
                        "Depreciation",
                        "Percentage",
                        "Asset Date"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        100, 20, 150, 150, 150,
                        //6-10
                        150, 100, 100, 100, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 9 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "A.AssetCode", "A.AssetName", "A.DisplayName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By A.AssetName;",
                        new string[] 
                        { 
                            //0
                            "AssetCode",
 
                            //1-5
                            "AssetName", "DisplayName", "AssetCategoryName", "AssetValue", "EcoLife", 
                            //6
                            "Depreciation", "PercentageAnnualDepreciation", "AssetDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value=Row+1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                if (Sm.IsFindGridValid(Grd1, 1))
                {
                    mFrmParent.TxtAssetCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                    mFrmParent.TxtAssetName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                    mFrmParent.TxtDisplayName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                    mFrmParent.TxtAssetCtCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                    mFrmParent.TxtAcquisition.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 6), 0);
                    Sm.SetDte(mFrmParent.DteAcquisitionDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 10));
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        private void Grd1_EllipsisButtonClick_1(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmAsset(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit_1(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAsset(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        private void Grd1_DoubleClick(object sender, EventArgs e)
        {
            ChooseData();
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

    }
}
