﻿#region Update
/*
    21/06/2020 [TKG/IMS] New application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmOTVerificationFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmOTVerification mFrmParent;
        private string mSQL = string.Empty;
        
        #endregion

        #region Constructor

        public FrmOTVerificationFind(FrmOTVerification FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLuePayrollGrpCode(ref LuePGCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("B.EmpCode, C.EmpName, D.PosName, E.DeptName, C.EmpCodeOld, F.PGName, B.OTRequestDocNo, B.OTDt, ");
            SQL.AppendLine("B.OTRequestStartTm, B.OTRequestEndTm, B.LogStartTm, B.LogEndTm, B.OTVerificationStartTm, B.OTVerificationEndTm, B.Duration, B.Rate, B.Amt, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblOTVerificationHdr A ");
            SQL.AppendLine("Left Join TblOTVerificationDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblEmployee C On B.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblDepartment E On B.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr F On A.PGCode=F.PGCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "Status",
                    "Employee's Code",

                    //6-10
                    "Employee's Name",
                    "Old Code",
                    "Position",
                    "Department", 
                    "Payroll's Group",
                    
                    //11-15
                    "Requested#",
                    "OT Date",
                    "Requested"+Environment.NewLine+"Start",
                    "Requested"+Environment.NewLine+"End",
                    "Log"+Environment.NewLine+"Start",
                    
                    //16-20
                    "Log"+Environment.NewLine+"End",
                    "Verified"+Environment.NewLine+"Start",
                    "Verified"+Environment.NewLine+"End",
                    "Duration",
                    "Rate",
                    
                    //21-25
                    "Amount",
                    "Created By",
                    "Created Date", 
                    "Created Time", 
                    "Last Updated By", 
                    
                    //26-27
                    "Last Updated Date", 
                    "Last Updated Time"
                },
                new int[] 
                {
                    //0
                    50,
                    //1-5
                    130, 100, 80, 80, 120, 
                    //6-10
                    200, 120, 200, 200, 200, 
                    //11-15
                    150, 100, 100, 100, 100, 
                    //16-20
                    100, 100, 100, 100, 130, 
                    //21-25
                    130, 130, 130, 130, 130, 
                    //26-27
                    130, 130
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 19, 20, 21 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 12, 23, 26 });
            Sm.GrdFormatTime(Grd1, new int[] { 13, 14, 15, 16, 17, 18, 24, 27 });
            Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24, 25, 26, 27 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24, 25, 26, 27 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtOTRequestDocNo.Text, "B.OTRequestDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpName", "C.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePGCode), "A.PGCode", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {

                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "StatusDesc", "EmpCode", "EmpName", 
                            
                            //6-10
                            "PosName", "DeptName", "EmpCodeOld", "PGName", "OTRequestDocNo", 
                            
                            //11-15
                            "OTDt", "OTRequestStartTm", "OTRequestEndTm", "LogStartTm", "LogEndTm", 
                            
                            //16-20
                            "OTVerificationStartTm", "OTVerificationEndTm", "Duration", "Rate", "Amt", 
                            
                            //21-24
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 26, 24);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 27, 24);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtOTRequestDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkOTRequestDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "OT requested#");
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue1(Sl.SetLuePayrollGrpCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPGCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Payroll's Group");
        }

        #endregion

        #endregion
    }
}
