﻿#region Update
/*
    28/05/2018 [WED] tambah LocalDocNo DOWhs2 dan LocalDocNo TransferRequestWhs
    08/09/2018 [TKG] Bug saat do to other warehouse, apabila stok gudang sudah kosong masih bisa di-DO-kan.
    25/02/2019 [TKG] tambah informasi kawasan berikat
    19/01/2020 [TKG/IMS] tambah informasi local code, specification
    27/03/2020 [IBL/IMS] Penyesuaian Print Out (Penambahan Kode Material, Spesifikasi Material, Nama vendor, Nomer PO)
    08/10/2020 [WED/IMS] tambah cancel otomatis TransferRequestWhs jika dokumen tersebut ada di tabel DOWhs4
    25/11/2020 [TKG/MAI] tambah validasi saat save, kalau transfer request sudah dicancel keluar warning.
    30/06/2021 [VIN/IMS] tambah parameter untuk melihat dokumen do whs project 
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOWhs2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        private string mDeptCode = string.Empty, mDOWhs2FulfillTransferRequestType = string.Empty;
        internal FrmDOWhs2Find FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        internal bool mIsItGrpCodeShow = false, 
            mIsDOWhsCopyTransferRequestDRemark = false,
            mIsDOWhs2ShowDOWhsProject = false;
        internal bool mIsKawasanBerikatEnabled = false;

        #endregion

        #region Constructor

        public FrmDOWhs2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "DO To Other Warehouse (With Transfer Request)";
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            GetParameter();
            if (mIsKawasanBerikatEnabled) Tp2.PageVisible = true;
            SetGrd();
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
            base.FrmLoad(sender, e);

            //if this application is called from other application
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            mIsDOWhsCopyTransferRequestDRemark = Sm.GetParameterBoo("IsDOWhsCopyTransferRequestDRemark");
            mDOWhs2FulfillTransferRequestType = Sm.GetParameter("DOWhs2FulfillTransferRequestType");
            SetNumberOfInventoryUomCode();
            mIsKawasanBerikatEnabled = Sm.GetParameter("KB_Server").Length > 0;
            mIsDOWhs2ShowDOWhsProject = Sm.GetParameterBoo("IsDOWhs2ShowDOWhsProject");

        }

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 5;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo", 
                        
                        //1-5
                        "",
                        "Item's Code", 
                        "", 
                        "Item's Name", 
                        "Replacement",

                        //6-10
                        "Batch#", 
                        "Source",
                        "Lot",
                        "Bin", 
                        "Available",

                        //11-15
                        "Quantity",
                        "UoM", 
                        "Available",
                        "Quantity",
                        "UoM",

                        //16-20
                        "Available",
                        "Quantity",
                        "UoM",
                        "Asset Code",
                        "Asset Name",

                        //21-25
                        "",
                        "Cost Category",
                        "Inventory"+Environment.NewLine+" COA's Account#",
                        "Remark",
                        "Group",

                        //26-27
                        "Local Code",
                        "Specification"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 120, 20, 250, 90, 
                        
                        //6-10
                        250, 170, 80, 80, 100,  
                        
                        //11-15
                        100, 60, 100, 100, 60,  
                        
                        //16-20
                        100, 100, 60, 100, 150, 
                        
                        //21-25
                        20, 150, 150, 300, 100,

                        //26-27
                        120, 200
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 13, 14, 16, 17 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 5 });
            Sm.GrdColButton(Grd1, new int[] { 1, 3, 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 7, 8, 9, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 25, 27 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 26, 27 });
            Grd1.Cols[26].Move(5);
            Grd1.Cols[27].Move(6);
            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[25].Visible = true;
                Grd1.Cols[25].Move(4);
            }
            
            #endregion
            
            #region Grid 2

            Grd2.Cols.Count = 18;
            Grd2.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {                        
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Transfer Request From Whs DNo",
                        "Item Code", 
                        "Item Name",
                        "Requested"+Environment.NewLine+"Quantity", 
                        
                        //6-10
                        "DO"+Environment.NewLine+"Quantity",
                        "Balance",
                        "Requested"+Environment.NewLine+"Quantity (2)", 
                        "DO"+Environment.NewLine+"Quantity (2)",
                        "Balance (2)",

                        //11-15
                        "Requested"+Environment.NewLine+"Quantity (3)",
                        "DO"+Environment.NewLine+"Quantity (3)",
                        "Balance (3)",
                        "Asset Code",
                        "Asset",

                        //16-17
                        "Group", 
                        "DO To Other Warehouse"+Environment.NewLine+"For Project#"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        20, 0, 100, 250, 100, 
                        
                        //6-10
                        100, 100, 100, 100, 100,

                        //11-15
                        100, 100, 100, 100, 200,

                        //16-17
                        100, 200
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 1 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 1, 2, 3, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 }, false);
            Sm.GrdFormatDec(Grd2, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13 }, 0);

            if (mIsItGrpCodeShow)
            {
                Grd2.Cols[16].Visible = true;
                Grd2.Cols[16].Move(4);
            }
            if (mIsDOWhs2ShowDOWhsProject)
            {
                Grd2.Cols[17].Visible = true;
                Grd2.Cols[17].Move(3);
            }
            #endregion
            
            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 7, 8, 9, 27 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 2, 3, 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
            ShowInventoryUomCode();
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 8, 9, 10 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16, 17, 18 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 8, 9, 10, 11, 12, 13 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueWhsCode, MeeRemark, TxtLocalDocNo }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,  22, 23, 24, 25 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
                    Sm.GrdColInvisible(Grd1, new int[] { 10, 13, 16 }, false);
                    BtnTRWhsDocNo.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnKBContractNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueWhsCode, MeeRemark, TxtLocalDocNo }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 5, 11, 14, 17, 24 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1 });
                    Sm.GrdColInvisible(Grd1, new int[] { 10 }, true);
                    if (mNumberOfInventoryUomCode == 2) Sm.GrdColInvisible(Grd1, new int[] { 13 }, true);
                    if (mNumberOfInventoryUomCode == 3) Sm.GrdColInvisible(Grd1, new int[] { 13, 16 }, true);
                    BtnTRWhsDocNo.Enabled = true;
                    BtnKBContractNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, TxtStatus, TxtTRWhsDocNo, 
                TxtWhsCode1, TxtWhsCode2, MeeRemark2, MeeRemark, TxtLocalDocNo,
                TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                DteKBRegistrationDt
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 3 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10, 11, 13, 14, 16, 17 });
            Sm.FocusGrd(Grd1, 0, 1);

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.FocusGrd(Grd2, 0, 2);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && TxtTRWhsDocNo.Text.Length != 0)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDOWhs2Dlg2(this, Sm.GetLue(LueWhsCode), TxtTRWhsDocNo.Text));
                }

                if (Sm.IsGrdColSelected(new int[] { 1, 11, 14, 17, 19 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 11, 13, 14, 16, 17 });
                }
            }

            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAsset(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            ComputeBalance();
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && TxtTRWhsDocNo.Text.Length != 0)
                Sm.FormShowDialog(new FrmDOWhs2Dlg2(this, Sm.GetLue(LueWhsCode), TxtTRWhsDocNo.Text));

            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length != 0)
            {
                var f = new FrmAsset(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 11, 14, 17 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 19 }, e);

            if (e.ColIndex == 11)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 2, 11, 14, 17, 12, 15, 18);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 2, 11, 17, 14, 12, 18, 15);
            }

            if (e.ColIndex == 14)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 2, 14, 11, 17, 15, 12, 18);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 2, 14, 17, 11, 15, 18, 12);
            }

            if (e.ColIndex == 17)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 2, 17, 11, 14, 18, 12, 15);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 2, 17, 14, 11, 18, 15, 12);
            }

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 12), Sm.GetGrdStr(Grd1, e.RowIndex, 15)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 14, Grd1, e.RowIndex, 11);

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 12), Sm.GetGrdStr(Grd1, e.RowIndex, 18)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 17, Grd1, e.RowIndex, 11);

            if (e.ColIndex == 14 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), Sm.GetGrdStr(Grd1, e.RowIndex, 18)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 17, Grd1, e.RowIndex, 14);

            if (Sm.IsGrdColSelected(new int[] { 11, 14, 17 }, e.ColIndex)) ComputeBalance();

        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOWhs2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData();
                }
                else
                {
                    CancelData();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string ParValue = Sm.GetValue("Select ParValue From TblParameter Where Parcode='NumberOfInventoryUomCode' ");

            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "DOWhs2Hdr", "DOWhs2Hdr2", "DOWhs2Hdr3", "DOWhs2Hdr4", "DOWhs2Dtl", "DOWhs2Dtl2" };

            var l = new List<DOWhs2Hdr>();
            var l1 = new List<DOWhs2Hdr2>();
            var l2 = new List<DOWhs2Hdr3>();
            var l3 = new List<DOWhs2Hdr4>();
            var ldtl = new List<DOWhs2Dtl>();
            var ldtl2 = new List<DOWhs2Dtl2>();
            decimal Numb = 0;

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
          
            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As Phone, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode = 'DoWhsNotes')As DoWhsNotes, ");
            SQL.AppendLine(" A.DocNo, Date_Format(A.DocDt,'%d %M %Y') As DocDt, A.WhsCode, B.WhsName As WhsFrom, D.WhsName As WhsTo, D.WhsName, A.Remark, "); 
            SQL.AppendLine("Concat(IfNull(G.ParValue, ''), E.UserCode, '.JPG') As EmpPict, "); 
            SQL.AppendLine("Concat(IfNull(G.ParValue, ''), F.UserCode, '.JPG') As EmpPict2, E.UserName As CreateBy, F.UserName As Createby2, A.Cancelind, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode = 'IsShowBatchNo')As IsShowBatchNo "); 
            SQL.AppendLine("From TblDoWhsHdr A "); 
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode  ");
            SQL.AppendLine("Inner Join TblTransferRequestWhsHdr C On A.TransferRequestWhsDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse D On A.WhsCode2=D.WhsCode ");
            SQL.AppendLine("Inner Join tbluser E On C.CreateBy=e.UserCode ");
            SQL.AppendLine("Inner Join tbluser F On A.CreateBy=F.UserCode ");
            SQL.AppendLine("Left Join TblParameter G On G.ParCode = 'ImgFileSignature' ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    {
                     //0
                      "Company",
                     //1-5
                     "Address",
                     "Phone",
                     "DoWhsNotes",
                     "DocNo",
                     "DocDt",
                     //6-10
                     "WhsFrom",
                     "WhsTo",
                     "Remark",
                     "CompanyLogo",
                     "EmpPict",

                     //11-15
                     "EmpPict2",
                     "CreateBy",
                     "Createby2",
                     "CancelInd",
                     "IsShowBatchNo"
                    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DOWhs2Hdr()
                        {
                            Company = Sm.DrStr(dr, c[0]),
                            Address = Sm.DrStr(dr, c[1]),
                            Phone = Sm.DrStr(dr, c[2]),
                            DoWhsNotes = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),
                            WhsNameFrom = Sm.DrStr(dr, c[6]),
                            WhsNameTo = Sm.DrStr(dr, c[7]),
                            Remark = Sm.DrStr(dr, c[8]),
                            CompanyLogo = Sm.DrStr(dr, c[9]),
                            EmpPict = Sm.DrStr(dr, c[10]),
                            EmpPict2 = Sm.DrStr(dr, c[11]),
                            CreateBy = Sm.DrStr(dr, c[12]),
                            CreateBy2 = Sm.DrStr(dr, c[13]),
                            CancelInd = Sm.DrStr(dr, c[14]),
                            IsShowBatchNo = Sm.DrStr(dr, c[15]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }

                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Header2
            var cm1 = new MySqlCommand();
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
            SQL1.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict ");
            SQL1.AppendLine("from TblDocApproval A ");
            SQL1.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL1.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL1.AppendLine("Where DocType = 'TransferRequestWhs' ");
            SQL1.AppendLine("And DocNo =@DocNo ");
            SQL1.AppendLine("Group by ApprovalDno limit 1");

            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandText = SQL1.ToString();
                Sm.CmParam<String>(ref cm1, "@DocNo", TxtTRWhsDocNo.Text);
                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] 
                    {
                     //0
                     "ApprovalDno",
                     //1-3
                     "UserCode",
                     "UserName",
                     "EmpPict"

                    
                    });
                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        l1.Add(new DOWhs2Hdr2()
                        {
                            ApprovalDno = Sm.DrStr(dr1, c1[0]),
                            UserCode = Sm.DrStr(dr1, c1[1]),
                            UserName = Sm.DrStr(dr1, c1[2]),
                            EmpPict = Sm.DrStr(dr1, c1[3]),
                        });
                    }
                }

                dr1.Close();
            }
            myLists.Add(l1);
            #endregion

            #region Header3
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
            SQL2.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict ");
            SQL2.AppendLine("from TblDocApproval A ");
            SQL2.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL2.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL2.AppendLine("Where DocType = 'TransferRequestWhs' ");
            SQL2.AppendLine("And DocNo =@DocNo ");
            SQL2.AppendLine("Group by ApprovalDno Order by ApprovalDno Desc limit 1");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtTRWhsDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                    {
                     //0
                     "ApprovalDno",
                     //1-3
                     "UserCode",
                     "UserName",
                     "EmpPict"

                    
                    });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new DOWhs2Hdr3()
                        {
                            ApprovalDno = Sm.DrStr(dr2, c2[0]),
                            UserCode = Sm.DrStr(dr2, c2[1]),
                            UserName = Sm.DrStr(dr2, c2[2]),
                            EmpPict = Sm.DrStr(dr2, c2[3]),
                        });
                    }
                }

                dr2.Close();
            }
            myLists.Add(l2);
            #endregion

            #region Header 4
            var cm4 = new MySqlCommand();
            var SQLHdr4 = new StringBuilder();

            SQLHdr4.AppendLine("SELECT @CompanyLogo As CompanyLogo, A.DocNo, Date_Format(A.DocDt,'%d %M %Y') As DocDt, B.WhsName, A.TransferRequestWhsDocNo, C.UserName ");
            SQLHdr4.AppendLine("FROM TblDOWhsHdr A ");
            SQLHdr4.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode  ");
            SQLHdr4.AppendLine("INNER JOIN TblUser C ON A.CreateBy=C.UserCode ");
            SQLHdr4.AppendLine("Where A.DocNo=@DocNo");
            using (var cn4 = new MySqlConnection(Gv.ConnectionString))
            {
                cn4.Open();
                cm4.Connection = cn4;
                cm4.CommandText = SQLHdr4.ToString();
                Sm.CmParam<String>(ref cm4, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm4, "@CompanyLogo", @Sm.CompanyLogo());
                var dr4 = cm4.ExecuteReader();
                var c4 = Sm.GetOrdinal(dr4, new string[] 
                    {
                     //0
                     "DocNo",

                     //1-5
                     "CompanyLogo",
                     "DocDt",
                     "WhsName",
                     "TransferRequestWhsDocNo",
                     "UserName",
                    });
                if (dr4.HasRows)
                {
                    while (dr4.Read())
                    {
                        l3.Add(new DOWhs2Hdr4()
                        {
                            DocNo = Sm.DrStr(dr4, c4[0]),
                            CompanyLogo = Sm.DrStr(dr4, c4[1]),
                            DocDt = Sm.DrStr(dr4, c4[2]),
                            WhsName = Sm.DrStr(dr4, c4[3]),
                            TransferRequestWhsDocNo = Sm.DrStr(dr4, c4[4]),
                            UserName = Sm.DrStr(dr4, c4[5]),

                            PrintDt = String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }

                dr4.Close();
            }
            myLists.Add(l3);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.ItCode, B.ItName, A.BatchNo, B.ItGrpCode, A.Qty, A.Qty2, A.Qty3, B.InventoryUOMCode, B.InventoryUOMCode2, B.InventoryUOMCode3, A.Remark ");
                SQLDtl.AppendLine("From TblDOWhsDtl A ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                         //0
                         "ItCode",
                         
                         //1-5
                         "ItName",
                         "BatchNo",
                         "ItGrpCode",
                         "Qty",
                         "Qty2",
                        
                         //6-10
                         "Qty3",
                         "InventoryUOMCode",
                         "InventoryUOMCode2",
                         "InventoryUOMCode3",
                         "Remark",
                         
                    });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new DOWhs2Dtl()
                        {
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),

                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[2]),
                            ItGrpCode = Sm.DrStr(drDtl, cDtl[3]),
                            Qty1 = Sm.DrDec(drDtl, cDtl[4]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[5]),

                            Qty3 = Sm.DrDec(drDtl, cDtl[6]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[7]),
                            InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[8]),
                            InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[9]),
                            Remark = Sm.DrStr(drDtl, cDtl[10]),

                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail 2
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("SELECT B.ItCodeInternal, B.Specification, A.Qty, B.InventoryUomCode, C.ProjectName, A.Remark, ");
                SQLDtl2.AppendLine("CONCAT(IFNULL('',C.ProjectName), A.BatchNo) AS ProjectName2 ");
                SQLDtl2.AppendLine("FROM TblDOWhsDtl A ");
                SQLDtl2.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQLDtl2.AppendLine("Left JOIN TblProjectGroup C ON A.BatchNo=C.ProjectCode AND A.BatchNo IS NOT NULL ");
                SQLDtl2.AppendLine("Where A.DocNo = @DocNo ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                         //0
                         "ItCodeInternal",
                         
                         //1-5
                         "Specification",
                         "Qty",
                         "InventoryUomCode",
                         "ProjectName",
                         "ProjectName2",

                         //6
                         "Remark",                             
                    });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        Numb = Numb + 1; 
                        ldtl2.Add(new DOWhs2Dtl2()
                        {
                            Number = Numb,
                            IrCodeInternal = Sm.DrStr(drDtl2, cDtl2[0]),

                            Specification = Sm.DrStr(drDtl2, cDtl2[1]),
                            Qty = Sm.DrDec(drDtl2, cDtl2[2]),
                            InventoryUomCode = Sm.DrStr(drDtl2, cDtl2[3]),
                            ProjectName = Sm.DrStr(drDtl2, cDtl2[4]),
                            ProjectName2 = Sm.DrStr(drDtl2, cDtl2[5]),

                            Remark = Sm.DrStr(drDtl2, cDtl2[6]),

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            int a = int.Parse(ParValue);
            if (Sm.GetParameter("DocTitle") == "IMS")
            {
                Sm.PrintReport("DOWhs2IMS", myLists, TableName, false);
            }
            else
            {
                if (a == 1)
                {
                    Sm.PrintReport("DOWhs2_1", myLists, TableName, false);
                }
                else if (a == 2)
                {
                    Sm.PrintReport("DOWhs2_2", myLists, TableName, false);
                }
                else
                {
                    Sm.PrintReport("DOWhs2_3", myLists, TableName, false);
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid() ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOWhs2", "TblDOWhsHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDOWhsHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 1) cml.Add(SaveDOWhsDtl(DocNo, Row));

            int DNo = 0;
            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 1 && Sm.GetGrdDec(Grd2, Row, 6) > 0)
                {
                    DNo += 1;
                    cml.Add(SaveDOWhsDtl2(DocNo, DNo, Row));
                }
            }
            cml.Add(UpdateTransferRequestWhs(DocNo, "F"));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            ReComputeStock();
            return
                Sm.IsDteEmpty(DteDocDt, "Document date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsTxtEmpty(TxtTRWhsDocNo, "DO Request from department document#", false) ||
                IsTransferRequestWhsAlreadyCancelled() ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid()||
                IsGrdValueRequestedNotValid()
                ;
        }

        private bool IsTransferRequestWhsAlreadyCancelled()
        {
            if (TxtTRWhsDocNo.Text.Length > 0)
            {
                if (Sm.IsDataExist(
                    "Select 1 From TblTransferRequestWhsHdr " +
                    "Where DocNo=@Param And (CancelInd='Y' Or Status='C') Limit 1;",
                    TxtTRWhsDocNo.Text))
                {
                    Sm.StdMsg(mMsgType.Warning, "This transfer request document already cancelled.");
                    return true;
                }
            }
            return false;
        }

        void GenerateSQLConditionForInventory(
           ref MySqlCommand cm, ref string Filter, int No,
           ref iGrid Grd, int Row, int ColSource, int ColLot, int ColBin)
        {
            if (Filter.Length > 0) Filter += " Or ";
            Filter += "(A.Source=@Source" + No + " And A.Lot=@Lot" + No + " And A.Bin=@Bin" + No + ") ";
            Sm.CmParam<String>(ref cm, "@Source" + No, Sm.GetGrdStr(Grd, Row, ColSource));
            Sm.CmParam<String>(ref cm, "@Lot" + No, Sm.GetGrdStr(Grd, Row, ColLot));
            Sm.CmParam<String>(ref cm, "@Bin" + No, Sm.GetGrdStr(Grd, Row, ColBin));
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();
             
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        Source = Sm.GetGrdStr(Grd1, r, 7);
                        if (Source.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += "(A.Source=@Source0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@Source0" + r.ToString(), Source);
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ") ";

                SQL.AppendLine("Select A.Source, A.Lot, A.Bin, ");
                SQL.AppendLine("IfNull(A.Qty, 0)-IfNull(B.DOWhsQty, 0) As Qty, ");
                SQL.AppendLine("IfNull(A.Qty2, 0)-IfNull(B.DOWhsQty2, 0) As Qty2, ");
                SQL.AppendLine("IfNull(A.Qty3, 0)-IfNull(B.DOWhsQty3, 0) As Qty3 ");
                SQL.AppendLine("From TblStockSummary A ");
                SQL.AppendLine("Left join ( ");
                SQL.AppendLine("    Select T2.Lot, T2.Bin, T2.Source, ");
                SQL.AppendLine("    Sum(T2.Qty) As DOWhsQty, ");
                SQL.AppendLine("    Sum(T2.Qty2) As DOWhsQty2, ");
                SQL.AppendLine("    Sum(T2.Qty3) As DOWhsQty3 ");
                SQL.AppendLine("    From TblDOWhsHdr T1 ");
                SQL.AppendLine("    Inner Join TblDOWhsDtl T2 ");
                SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        And T2.ProcessInd='O' ");
                SQL.AppendLine("        And T2.CancelInd='N' ");
                SQL.AppendLine(Filter.Replace("A.", "T2."));
                SQL.AppendLine("    Where T1.CancelInd='N' ");
                SQL.AppendLine("    And IfNull(T1.Status, 'O') In ('O', 'A') ");
                SQL.AppendLine("    And T1.WhsCode=@WhsCode ");
                SQL.AppendLine("    Group By T2.Lot, T2.Bin, T2.Source ");
                SQL.AppendLine(") B On A.Lot=B.Lot And A.Bin=B.Bin And A.Source=B.Source ");
                SQL.AppendLine("Where A.WhsCode=@WhsCode ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(";");

                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 7), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 8), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 9), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 11, true, "Quantity (1) is empty.") ||
                    (Sm.GetGrdStr(Grd1, Row, 15).Length > 0 && mNumberOfInventoryUomCode >= 2 && Sm.IsGrdValueEmpty(Grd1, Row, 14, true, "Quantity (2) is empty.")) ||
                    (Sm.GetGrdStr(Grd1, Row, 18).Length > 0 && mNumberOfInventoryUomCode == 3 && Sm.IsGrdValueEmpty(Grd1, Row, 17, true, "Quantity (3) is empty."))
                    ) return true;

                if (((Sm.GetGrdStr(Grd1, Row, 11).Length == 0) || (Sm.GetGrdStr(Grd1, Row, 11).Length != 0 && Sm.GetGrdDec(Grd1, Row, 11) == 0m)) &&
                    ((Sm.GetGrdStr(Grd1, Row, 14).Length == 0) || (Sm.GetGrdStr(Grd1, Row, 14).Length != 0 && Sm.GetGrdDec(Grd1, Row, 14) == 0m)) &&
                    ((Sm.GetGrdStr(Grd1, Row, 17).Length == 0) || (Sm.GetGrdStr(Grd1, Row, 17).Length != 0 && Sm.GetGrdDec(Grd1, Row, 17) == 0m)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                        "Batch# : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                        "Source : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                        "Lot : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                        "Bin : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + Environment.NewLine +
                        "Quantity should be greater than 0.");
                    return true;
                }

                if (IsDOQtyBiggerThanStock(Row)) return true;
            }

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0)
                {
                    if (IsTransferRequestWhsAlreadyProcessed(Row)) return true;
                }
            }

            return false;
        }

        private bool IsGrdValueRequestedNotValid()
        {
            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 3).Length > 0)
                {
                    if (IsDOQtyBiggerThanRequest(Row)) return true;
                }
            }

            return false;
        }
        


        private bool IsDOQtyBiggerThanStock(int Row)
        {
            decimal
                Stock1 = Sm.GetGrdDec(Grd1, Row, 10),
                DO1 = Sm.GetGrdDec(Grd1, Row, 11),
                Stock2 = Sm.GetGrdDec(Grd1, Row, 13),
                DO2 = Sm.GetGrdDec(Grd1, Row, 14),
                Stock3 = Sm.GetGrdDec(Grd1, Row, 16),
                DO3 = Sm.GetGrdDec(Grd1, Row, 17);

            if (DO1 > Stock1)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "Item Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Batch Number : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + Environment.NewLine +
                    "DO quantity 1 (" + Sm.FormatNum(DO1, 0) + ") is greater than stock (" + Sm.FormatNum(Stock1, 0) + ")."
                    );
                return true;
            }

            if (DO2 > Stock2)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "Item Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Batch Number : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + Environment.NewLine +
                    "DO quantity 2 (" + Sm.FormatNum(DO2, 0) + ") is greater than stock (" + Sm.FormatNum(Stock2, 0) + ")."
                    );
                return true;
            }

            if (DO3 > Stock3)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "Item Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Batch Number : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + Environment.NewLine +
                    "DO quantity 3 (" + Sm.FormatNum(DO3, 0) + ") is greater than stock (" + Sm.FormatNum(Stock3, 0) + ")."
                    );
                return true;
            }

            return false;
        }


        private bool IsDOQtyBiggerThanRequest(int Row)
        {
            decimal
                Request1 = Sm.GetGrdDec(Grd2, Row, 5),
                DO1 = Sm.GetGrdDec(Grd2, Row, 6),
                Request2 = Sm.GetGrdDec(Grd2, Row, 8),
                DO2 = Sm.GetGrdDec(Grd2, Row, 9),
                Request3 = Sm.GetGrdDec(Grd2, Row, 11),
                DO3 = Sm.GetGrdDec(Grd2, Row, 12);

            if (DO1 > Request1)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item Code : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                    "Item Name : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine +
                    "DO quantity 1 (" + Sm.FormatNum(DO1, 0) + ") is greater than request (" + Sm.FormatNum(Request1, 0) + ")."
                    );
                return true;
            }

            if (DO2 > Request2)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item Code : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                    "Item Name : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine +
                    "DO quantity 2 (" + Sm.FormatNum(DO2, 0) + ") is greater than request (" + Sm.FormatNum(Request2, 0) + ")."
                    );
                return true;
            }

            if (DO3 > Request3)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item Code : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                    "Item Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "DO quantity 3 (" + Sm.FormatNum(DO3, 0) + ") is greater than request (" + Sm.FormatNum(Request3, 0) + ")."
                    );
                return true;
            }

            return false;
        }




        private bool IsTransferRequestWhsAlreadyProcessed(int Row)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select T1.DocNo ");
            SQL.AppendLine("From TblTransferRequestWhsHdr T1 ");
            SQL.AppendLine("Inner Join TblTransferRequestWhsDtl T2 On T1.DocNo=T2.DocNo And T2.ProcessInd='F' And T2.DNo=@DNo ");
            SQL.AppendLine("Where T1.DocNo=@DocNo Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtTRWhsDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd2, Row, 2));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Requested Document# : " + TxtTRWhsDocNo.Text + Environment.NewLine +
                    "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine + Environment.NewLine +
                    "This data already processed to DO."
                    );
                return true;
            }
            return false;
        }

        private MySqlCommand SaveDOWhsHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOWhsHdr(DocNo, DocDt, Status, LocalDocNo, WhsCode, WhsCode2, TransferRequestWhsDocNo, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @LocalDocNo, @WhsCode, (Select WhsCode2 From TblTransferRequestWhsHdr Where DocNo=@TRWhsDocNo), @TRWhsDocNo, ");
            SQL.AppendLine("@KBContractNo, @KBContractDt, @KBPLNo, @KBPLDt, @KBRegistrationNo, @KBRegistrationDt, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='DOWhs2'; ");

            SQL.AppendLine("Update TblDOWhsHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='DOWhs2' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@TRWhsDocNo", TxtTRWhsDocNo.Text);
            Sm.CmParam<String>(ref cm, "@KBContractNo", TxtKBContractNo.Text);
            Sm.CmParamDt(ref cm, "@KBContractDt", Sm.GetDte(DteKBContractDt));
            Sm.CmParam<String>(ref cm, "@KBPLNo", TxtKBPLNo.Text);
            Sm.CmParamDt(ref cm, "@KBPLDt", Sm.GetDte(DteKBPLDt));
            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", TxtKBRegistrationNo.Text);
            Sm.CmParamDt(ref cm, "@KBRegistrationDt", Sm.GetDte(DteKBRegistrationDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDOWhsDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDOWhsDtl(DocNo, DNo, CancelInd, ItCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) Values " +
                    "(@DocNo, @DNo, 'N', @ItCode, @BatchNo, @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDOWhsDtl2(string DocNo, int DNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDOWhsDtl2(DocNo, DNo, TransferRequestWhsDNo, Qty, Qty2, Qty3, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @TransferRequestWhsDNo, @Qty, @Qty2, @Qty3, @CreateBy, CurrentDateTime());", 
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + DNo.ToString(), 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd2, Row, 12));
            Sm.CmParam<String>(ref cm, "@TransferRequestWhsDNo", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateTransferRequestWhs(string DocNo, string ProcessInd)
        {
            var SQL = new StringBuilder();


            if (mDOWhs2FulfillTransferRequestType == "2")
            {
                SQL.AppendLine("Update TblTransferRequestWhsDtl T1 Set ");
                SQL.AppendLine("    T1.ProcessInd=@ProcessInd ");
                SQL.AppendLine("Where DocNo In ( ");
                SQL.AppendLine("    Select TransferRequestWhsDocNo ");
                SQL.AppendLine("    From TblDOWhsHdr ");
                SQL.AppendLine("    Where DocNo=@DocNo And TransferRequestWhsDocNo is Not Null ");
                SQL.AppendLine("    );");
            }
            else
            {
                SQL.AppendLine("Update TblTransferRequestWhsDtl T1 ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select A.TransferRequestWhsDocNo As DocNo, B.TransferRequestWhsDNo As DNo ");
                SQL.AppendLine("    From  TblDOWhsHdr A  ");
                SQL.AppendLine("    Inner Join TblDOWhsDtl2 B On A.DocNo=B.DocNo And B.TransferRequestWhsDNo Is Not Null  ");
                SQL.AppendLine("    Where A.DocNo=@DocNo And A.TransferRequestWhsDocNo Is Not Null ");
                SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo ");
                SQL.AppendLine("Set T1.ProcessInd=@ProcessInd; ");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ProcessInd", ProcessInd);
            return cm;
        }

        private MySqlCommand CancelDOWhs4()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTransferRequestWhsHdr A ");
            SQL.AppendLine("Inner Join TblDOWhs4Dtl B On B.TransferRequestWhsDocNo = A.DocNo ");
            SQL.AppendLine("Inner Join TblDOWhsHdr C On B.DOWhsDocNo = C.DocNo And C.DocNo = @DocNo ");
            SQL.AppendLine("Set A.CancelInd = 'Y', A.LastUpBy = @UserCode, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where C.DocNo = @DocNo; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelDOWhsHdr());
            cml.Add(UpdateTransferRequestWhs(TxtDocNo.Text, "O"));
            cml.Add(CancelDOWhs4()); // TASK/IMS/2020/40/015 by wed

            Sm.ExecCommands(cml);
            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyReceived();
        }



        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return
               Sm.IsDataExist(
                   "Select DocNo From TblDOWhsHdr " +
                   "Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                   TxtDocNo.Text,
                   "This document already cancelled."
               );
        }

        private bool IsDataAlreadyReceived()
        {
            return
               Sm.IsDataExist(
                   "Select DocNo From TblRecvWhs4Dtl " +
                   "Where CancelInd='N' And DOWhsDocNo=@Param Limit 1;",
                   TxtDocNo.Text,
                   "This data already received."
               );
        }


        private MySqlCommand CancelDOWhsHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOWhsHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowDOWhsHdr(DocNo);
                ShowDOWhsDtl2(DocNo);
                ShowDOWhsDtl(DocNo);
                ComputeBalance();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDOWhsHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.CancelInd, Case When A.Status = 'O' Then 'Outstanding' When A.status = 'A' Then 'Approve' When A.Status = 'C' Then 'Cancelled' End As Status, ");
            SQL.AppendLine("A.DocDt, A.WhsCode, B.WhsName As WhsFrom, A.TransferRequestWhsDocNo, ");
            SQL.AppendLine("D.WhsName As WhsTo, C.remark As RemarkDOR, A.Remark, A.LocalDocNo, C.LocalDocNo As TRWhsLocalDocNo, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt ");
            SQL.AppendLine("From TblDOWhsHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblTransferRequestWhsHdr C On A.TransferRequestWhsDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse D On A.WhsCode2=D.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelInd", "Status", "WhsCode", "TransferRequestWhsDocNo",  

                        //6-10
                        "WhsFrom", "WhsTo", "RemarkDOR", "Remark", "LocalDocNo", 
                        
                        //11-15
                        "TRWhsLocalDocNo", "KBContractNo", "KBContractDt", "KBPLNo", "KBPLDt", 

                        //16-17
                        "KBRegistrationNo", "KBRegistrationDt" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtStatus.EditValue = Sm.DrStr(dr, c[3]);
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[4]));
                        TxtTRWhsDocNo.EditValue = Sm.DrStr(dr, c[5]);
                        TxtWhsCode1.EditValue = Sm.DrStr(dr, c[6]);
                        TxtWhsCode2.EditValue = Sm.DrStr(dr, c[7]);
                        MeeRemark2.EditValue = Sm.DrStr(dr, c[8]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[10]);
                        TxtTRWhsLocalDocNo.EditValue = Sm.DrStr(dr, c[11]);
                        TxtKBContractNo.EditValue = Sm.DrStr(dr, c[12]);
                        Sm.SetDte(DteKBContractDt, Sm.DrStr(dr, c[13]));
                        TxtKBPLNo.EditValue = Sm.DrStr(dr, c[14]);
                        Sm.SetDte(DteKBPLDt, Sm.DrStr(dr, c[15]));
                        TxtKBRegistrationNo.EditValue = Sm.DrStr(dr, c[16]);
                        Sm.SetDte(DteKBRegistrationDt, Sm.DrStr(dr, c[17])); 
                    }, true
                );
        }

        private void ShowDOWhsDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.CancelInd, B.ItCode, C.ItName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, B.Remark, C.ItGrpCode, C.ItCodeInternal, C.Specification ");
            SQL.AppendLine("From TblDOWhsHdr A ");
            SQL.AppendLine("Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",  

                    //1-5
                    "ItCode", "ItName", "BatchNo", "Source", "Lot",   
                        
                    //6-10
                    "Bin", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", 
                    
                    //11-15
                    "Qty3", "InventoryUomCode3", "Remark", "ItGrpCode", "ItCodeInternal", 
                    
                    //16
                    "Specification"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Grd.Cells[Row, 10].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Grd.Cells[Row, 13].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                    Grd.Cells[Row, 16].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 16);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 11, 13, 14, 16, 17 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ShowDOWhsDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.ItCode, C.ItName,  B.Qty, B.Qty2, B.Qty3, C.ItGrpCode, ");
            if (mIsDOWhs2ShowDOWhsProject)
                SQL.AppendLine("E.DocNo DOWhs4DocNo ");
            else
                SQL.AppendLine("Null As DOWhs4DocNo ");

            SQL.AppendLine("From  TblTransferRequestWhsHdr A ");
            SQL.AppendLine("Inner Join TblTransferRequestWhsDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            if (mIsDOWhs2ShowDOWhsProject)
            {
                SQL.AppendLine("Inner Join tbldowhsdtl D ON D.DocNo=@DocNo ");
                SQL.AppendLine("Left Join tbldowhs4dtl E ON E.DOWhsDocNo=@DocNo AND E.DOWhsDNo=D.DNo ");
            }
            SQL.AppendLine("Where Concat(A.DocNo, B.DNo) In ( ");
            SQL.AppendLine("    Select Concat(T1.TransferRequestWhsDocNo, T2.TransferRequestWhsDNo) ");
            SQL.AppendLine("    From TblDOWhsHdr T1, TblDOWhsDtl2 T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo And T1.DocNo=@DocNo)");
            SQL.AppendLine("order By B.ItCode, C.ItName");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "ItCode", "ItName", "Qty", "Qty2", "Qty3",
                    
                    //6-8
                    "ItGrpCode", "DOWhs4DocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                    Grd.Cells[Row, 6].Value = 0m;
                    Grd.Cells[Row, 7].Value = Grd.Cells[Row, 5].Value;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    Grd.Cells[Row, 9].Value = 0m;
                    Grd.Cells[Row, 10].Value = Grd.Cells[Row, 8].Value;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 5);
                    Grd.Cells[Row, 12].Value = 0m;
                    Grd.Cells[Row, 13].Value = Grd.Cells[Row, 11].Value;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 7);
                }, false, false, false, false
                );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method

        //private void ComputeQtyBasedOnConvertionFormula(
        //    string ConvertType, iGrid Grd, int Row, int ColItCode,
        //    int ColQty1, int ColQty2, int ColUom1, int ColUom2)
        //{
        //    try
        //    {
        //        if (!Sm.CompareGrdStr(Grd, Row, ColUom1, Grd, Row, ColUom2))
        //        {
        //            decimal Convert = GetInventoryUomCodeConvert(ConvertType, Sm.GetGrdStr(Grd, Row, ColItCode));
        //            if (Convert != 0)
        //            {
        //                Grd.Cells[Row, ColQty2].Value = Convert * Sm.GetGrdDec(Grd, Row, ColQty1);
        //            }
        //        }
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        //private decimal GetInventoryUomCodeConvert(string ConvertType, string ItCode)
        //{
        //    var cm = new MySqlCommand
        //    {
        //        CommandText =
        //            "Select InventoryUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
        //    };
        //    Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
        //    return Sm.GetValueDec(cm);
        //}

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode='NumberOfInventoryUomCode'");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        //internal string GetSelectedItem()
        //{
        //    var SQL = string.Empty;
        //    if (Grd1.Rows.Count != 1)
        //    {
        //        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
        //            if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
        //                SQL +=
        //                    "##" +
        //                    Sm.GetGrdStr(Grd1, Row, 7) +
        //                    Sm.GetGrdStr(Grd1, Row, 8) +
        //                    Sm.GetGrdStr(Grd1, Row, 9) +
        //                    "##";
        //    }
        //    return (SQL.Length == 0 ? "##XXX##" : SQL);
        //}

        internal string GetSelectedDORequestItem()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count >0)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 3).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 3) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ComputeBalance()
        {
            decimal
                DORequestQty = 0m, DOQty = 0m, Balance = 0m,
                DORequestQty2 = 0m, DOQty2 = 0m, Balance2 = 0m,
                DORequestQty3 = 0m, DOQty3 = 0m, Balance3 = 0m;

           
                #region text DocNo null
                if (Grd2.Rows.Count != 0)
                {
                    for (int Row1 = 0; Row1 < Grd2.Rows.Count; Row1++)
                    {
                        if (Sm.GetGrdStr(Grd2, Row1, 2).Length != 0)
                        {
                            DORequestQty = Sm.GetGrdDec(Grd2, Row1, 5);
                            DOQty = 0m;
                            for (int Row2 = 0; Row2 < Grd1.Rows.Count; Row2++)
                            {
                                if (
                                    Sm.GetGrdStr(Grd1, Row2, 2).Length > 0 &&
                                    Sm.CompareStr(Sm.GetGrdStr(Grd2, Row1, 3), Sm.GetGrdStr(Grd1, Row2, 2)) 
                                    )
                                    DOQty += Sm.GetGrdDec(Grd1, Row2, 11);
                            }
                            Balance = DORequestQty - DOQty;
                        }

                        if (Sm.GetGrdStr(Grd2, Row1, 2).Length != 0)
                        {
                            DORequestQty2 = Sm.GetGrdDec(Grd2, Row1, 8);
                            DOQty2 = 0m;
                            for (int Row2 = 0; Row2 < Grd1.Rows.Count; Row2++)
                            {
                                if (
                                    Sm.GetGrdStr(Grd1, Row2, 2).Length > 0 &&
                                    Sm.CompareStr(Sm.GetGrdStr(Grd2, Row1, 3), Sm.GetGrdStr(Grd1, Row2, 2)) 
                                    )
                                    DOQty2 += Sm.GetGrdDec(Grd1, Row2, 14);
                            }
                            Balance2 = DORequestQty2 - DOQty2;
                        }

                        if (Sm.GetGrdStr(Grd2, Row1, 2).Length != 0)
                        {
                            DORequestQty3 = Sm.GetGrdDec(Grd2, Row1, 11);
                            DOQty3 = 0m;
                            for (int Row2 = 0; Row2 < Grd1.Rows.Count; Row2++)
                            {
                                if (
                                    Sm.GetGrdStr(Grd1, Row2, 2).Length > 0 &&
                                    Sm.CompareStr(Sm.GetGrdStr(Grd2, Row1, 3), Sm.GetGrdStr(Grd1, Row2, 2)) 
                                    )
                                    DOQty3 += Sm.GetGrdDec(Grd1, Row2, 17);
                            }
                            Balance3 = DORequestQty3 - DOQty3;
                        }

                        Grd2.Cells[Row1, 6].Value = DOQty;
                        Grd2.Cells[Row1, 7].Value = Balance;
                        Grd2.Cells[Row1, 9].Value = DOQty2;
                        Grd2.Cells[Row1, 10].Value = Balance2;
                        Grd2.Cells[Row1, 12].Value = DOQty3;
                        Grd2.Cells[Row1, 13].Value = Balance3;
                    }
                }
                #endregion
           
        
        }

        public void ShowTRWhsInfo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.ItCode, C.ItName, B.Qty, B.Qty2, B.Qty3 ");
            SQL.AppendLine("From  TblTransferRequestWhsHdr A ");
            SQL.AppendLine("Inner Join TblTransferRequestWhsDtl B On A.DocNo=B.DocNo And B.ProcessInd='O' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "ItCode", "ItName", "Qty", "Qty2", "Qty3" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                    Grd.Cells[Row, 6].Value = 0m;
                    Grd.Cells[Row, 7].Value = Grd.Cells[Row, 5].Value;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    Grd.Cells[Row, 9].Value = 0m;
                    Grd.Cells[Row, 10].Value = Grd.Cells[Row, 8].Value;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 5);
                    Grd.Cells[Row, 12].Value = 0m;
                    Grd.Cells[Row, 13].Value = Grd.Cells[Row, 11].Value;
                }, false, false, false, false
                );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        public void SetAsset()
        {
            for (int RowY = 0; RowY < Grd1.Rows.Count; RowY++)
            {
                string ItCode = Sm.GetGrdStr(Grd1, RowY, 2);
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (ItCode == Sm.GetGrdStr(Grd2, Row, 3))
                    {
                        Grd1.Cells[RowY, 19].Value = Sm.GetGrdStr(Grd2, Row, 14);
                        Grd1.Cells[RowY, 20].Value = Sm.GetGrdStr(Grd2, Row, 15);
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        
        private void BtnTRWhsDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
            {
                TxtWhsCode1.EditValue = null;
                ClearGrd();
                Sm.FormShowDialog(new FrmDOWhs2Dlg(this, Sm.GetLue(LueWhsCode)));
            }
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            ClearGrd();
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtTRWhsDocNo, TxtWhsCode1   
            });
        }

        private void BtnKBContractNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOWhs2Dlg4(this));
        }

        #endregion

        #region Grid Event

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && TxtTRWhsDocNo.Text.Length!=0)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDOWhs2Dlg3(this, TxtTRWhsDocNo.Text));
                }
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                string ItCode = string.Empty;

                for (int Index = Grd2.SelectedRows.Count - 1; Index >= 0; Index--)
                {
                    ItCode = Sm.GetGrdStr(Grd2, Grd2.SelectedRows[Index].Index, 3);
                    break;
                }

                if (Grd2.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        for (int Index = Grd2.SelectedRows.Count - 1; Index >= 0; Index--)
                            Grd2.Rows.RemoveAt(Grd2.SelectedRows[Index].Index);
                        if (Grd2.Rows.Count <= 0) Grd2.Rows.Add();
                    }
                }
                
                for (int Index = Grd1.Rows.Count - 1; Index >= 0; Index--)
                {
                    if (
                        Sm.GetGrdStr(Grd1, Index, 4).Length != 0 &&
                        Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd1, Index, 4))
                        )
                    {
                        Grd1.Rows.RemoveAt(Index);
                    }
                }
                if (Grd1.Rows.Count <= 0) Grd1.Rows.Add();
            }
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && 
                BtnSave.Enabled && 
                TxtDocNo.Text.Length == 0 && 
                TxtTRWhsDocNo.Text.Length != 0) 
                
                Sm.FormShowDialog(new FrmDOWhs2Dlg3(this, TxtTRWhsDocNo.Text));
        }

        #endregion

        #endregion

        #region Report Class

        class DOWhs2Hdr
        {
            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string DoWhsNotes { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsNameFrom { get; set; }
            public string WhsNameTo { get; set; }
            public string Remark { get; set; }
            public string CompanyLogo { get; set; }
            public string EmpPict { get; set; }
            public string EmpPict2 { get; set; }
            public string CreateBy { get; set; }
            public string CreateBy2 { get; set; }
            public string IsShowBatchNo { get; set; }
            public string PrintBy { get; set; }
            public string CancelInd { get; set; }
        }

        class DOWhs2Hdr2
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
        }

        class DOWhs2Hdr3
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
        }

        class DOWhs2Hdr4
        {
            public string DocNo { get; set; }
            public string CompanyLogo { get; set; }
            public string DocDt { get; set; }
            public string WhsName { get; set; }
            public string TransferRequestWhsDocNo { get; set; }
            public string UserName { get; set; }
            public string PrintDt { get; set; }
        }

        class DOWhs2Dtl
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string BatchNo { get; set; }
            public string ItGrpCode { get; set; }
            public decimal Qty1 { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUomCode { get; set; }
            public string InventoryUomCode2 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string Remark { get; set; }

        }

        class DOWhs2Dtl2
        {
            public decimal Number { get; set; }
            public string IrCodeInternal
            {get; set;}

            public string Specification {get; set;}
            public decimal Qty {get; set;}
            public string InventoryUomCode {get; set;}
            public string ProjectName {get; set;}
            public string ProjectName2 { get; set; }

            public string Remark { get; set; }
        }

        #endregion
    }
}
