﻿#region Update
/*
    21/03/2023 [WED/PHT] new apps to choose level
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPositionDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPosition mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPositionDlg(FrmPosition FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdr(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-4
                    "",
                    "Level's Code",
                    "Level's Name"
                }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 2 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3 });
            Sm.SetGrdProperty(Grd1, true);
        }

        protected override void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string LevelCode = string.Empty, Filter = string.Empty;

                if (mFrmParent.Grd2.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd2.Rows.Count; r++)
                    {
                        LevelCode = Sm.GetGrdStr(mFrmParent.Grd2, r, 1);
                        if (LevelCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " And ";
                            Filter += string.Concat("(LevelCode<>@LevelCode0", r.ToString(), ") ");
                            Sm.CmParam<String>(ref cm, "@LevelCode0" + r.ToString(), LevelCode);
                        }
                    }
                }
                if (Filter.Length != 0)
                    Filter = " Where (" + Filter + ") ";
                else
                    Filter = "Where 1=1 ";

                Sm.FilterStr(ref Filter, ref cm, TxtLevelCode.Text, "LevelName", false);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        string.Concat("Select LevelCode, LevelName From TblLevelHdr ", Filter, " Order By LevelName;"),
                        new string[] { "LevelCode", "LevelName" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 3);

                        mFrmParent.Grd2.Rows.Add();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 level.");
        }

        private bool IsCodeAlreadyChosen(int r)
        {
            var LevelCode = Sm.GetGrdStr(Grd1, r, 2);
            for (int i = 0; i < mFrmParent.Grd2.Rows.Count - 1; i++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd2, i, 1), LevelCode)) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtLevelCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLevelCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Position");
        }

        #endregion

        #endregion
    }
}
