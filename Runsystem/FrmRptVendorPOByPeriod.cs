﻿#region Update
/*
    04/04/2017 [WED] Penambahan kolom Discount %, Discount Amt, Rounding Value, Currency, Total, Grand Total per item PO
    04/04/2017 [WED] Perubahan nama dan isi kolom "Tax" menjadi "PPN"
    04/04/2017 [WED] Discount %, Discount Amt, Rounding Value, diambil dari PORevision yang sudah diapprove. Jika tidak ada, ambil dari PO
    28/07/2017 [HAR] bug saat receiving partial
    30/08/2017 [HAR] Bug PO tidak muncul, receving tidak sesaui
    18/10/2017 [HAR] harga sblm revisi dihapus yang ditamilkan harga stlh revisi saja
    23/03/2018 [WED] total dan grand total belum dikalkulasi dengan POQtyCancel
    29/05/2018 [TKG] PO yg ditampilkan cuma yang sudah diapproved.
    13/08/2018 [WED] tambah loop untuk melihat hasil file upload di QtHdr
    17/06/2021 [VIN/IMS] tambah info dokumen PO for Service
    25/06/2021 [HAR/IOK] bug : saat filter PO service
    02/07/2021 [VIN/IMS] PO Service yang sudah cancel tidak dimunculkan lagi 
    28/01/2022 [DEV/PHT] Menambah kolom price after revision, total after revision, discount % after revision, discount amount after revision, rounding value after revision & grand total after revision di Reporting PO List by Date Range dengan parameter IsRptPOListByDateRangeUsePORevision
    11/02/2022 [ISD/PHT] bug kolom discount % after revision, discount amount after revision, rounding value after revision tidak muncul
    14/04/2022 [RIS/PHT] Merubah parameter IsRptPOListByDateRangeUsePORevision menjadi IsRptProcurementUsePORevision
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmRptVendorPOByPeriod : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty;
        private bool 
            mIsSiteMandatory = false, 
            mIsFilterBySite = false,
            mIsShowForeignName = false,
            mIsShowTax = false,
            mIsFilterByItCt = false,
            mIsQTAllowToUploadFile = false,
            mIsRptProcurementUsePORevision = false;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmRptVendorPOByPeriod(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetSQL();
                SetGrd();
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCatCode, mIsFilterByItCt?"Y":"N");
                Sl.SetLueItScCode(ref LueSubItCat);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsShowTax = Sm.GetParameterBoo("IsShowTax");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsQTAllowToUploadFile = Sm.GetParameterBoo("IsQTAllowToUploadFile");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsRptProcurementUsePORevision = Sm.GetParameterBoo("IsRptProcurementUsePORevision");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.VdName, E.ItCode, F.ItName, F.ForeignName, G.ItCtName, N.PtName, ");
            SQL.AppendLine("H.ItScName, Ifnull(E2.MRDocNo, A.DocNo) DocNo , A.DocDt, Ifnull(E2.MRLocalDocNo, A.LocalDocNo) LocalDocNo, ");
            if (mIsSiteMandatory)
                SQL.AppendLine("K.SiteName, ");
            else
                SQL.AppendLine("Null As SiteName, ");
            if (!mIsRptProcurementUsePORevision)
            {
                SQL.AppendLine("B.Qty, IfNull(J.UPrice, 0) As UPrice, ");
                SQL.AppendLine("IfNull(M.Qty, 0) As QtyRecv, IfNull(L.Qty, 0) As QtyCancel, ");
                SQL.AppendLine("IfNull(B.Discount, 0) AS Discount, IfNull(B.DiscountAmt, 0) As DiscountAmt, IfNull(B.RoundingValue, 0) As RoundingValue, ");
                SQL.AppendLine("( ");
                SQL.AppendLine("  IfNull(If(A.TaxCode1 Is Null,0.00,(If(A.TaxCode1=P.ParValue,(((B.Qty - IfNull(L.Qty, 0))*IfNull(J.UPrice, 0))*0.10), 0))), 0) + ");
                SQL.AppendLine("  IfNull(If(A.TaxCode2 Is Null,0.00,(If(A.TaxCode2=P.ParValue,(((B.Qty - IfNull(L.Qty, 0))*IfNull(J.UPrice, 0))*0.10), 0))), 0) + ");
                SQL.AppendLine("  IfNull(If(A.TaxCode3 Is Null,0.00,(If(A.TaxCode3=P.ParValue,(((B.Qty - IfNull(L.Qty, 0))*IfNull(J.UPrice, 0))*0.10), 0))), 0) ");
                SQL.AppendLine(") As PPNTax,");
                SQL.AppendLine("((B.Qty - IfNull(L.Qty, 0))*IfNull(J.UPrice, 0)) As Total,  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("  ((((B.Qty - IfNull(L.Qty, 0))*IfNull(J.UPrice, 0)) * ((100.00-IfNull(B.Discount, 0))*0.01)) ");
                SQL.AppendLine("  - IfNull(B.DiscountAmt, 0) + IfNull(B.RoundingValue, 0)) + ");
                SQL.AppendLine("  IfNull(If(A.TaxCode1 Is Null,0.00,(((((B.Qty - IfNull(L.Qty, 0))*IfNull(J.UPrice, 0)) * ((100.00-IfNull(B.Discount, 0))*0.01)) - IfNull(B.DiscountAmt, 0) + IfNull(B.RoundingValue, 0)) * O1.TaxRate *0.01)), 0.00) + ");
                SQL.AppendLine("  IfNull(If(A.TaxCode2 Is Null,0.00,(((((B.Qty - IfNull(L.Qty, 0))*IfNull(J.UPrice, 0)) * ((100.00-IfNull(B.Discount, 0))*0.01)) - IfNull(B.DiscountAmt, 0) + IfNull(B.RoundingValue, 0)) * O2.TaxRate *0.01)), 0.00) + ");
                SQL.AppendLine("  IfNull(If(A.TaxCode3 Is Null,0.00,(((((B.Qty - IfNull(L.Qty, 0))*IfNull(J.UPrice, 0)) * ((100.00-IfNull(B.Discount, 0))*0.01)) - IfNull(B.DiscountAmt, 0) + IfNull(B.RoundingValue, 0)) * O3.TaxRate *0.01)), 0.00) ");
                SQL.AppendLine(") As GrandTotal, ");
                SQL.AppendLine("0.00 As UPriceRev, 0.00 As TotalRev, 0.00 As DiscountRev, 0.00 As DiscountAmtRev, 0.00 As RoundingValueRev, 0.00 As GrandTotalRev, ");
            }
            else
            {
                SQL.AppendLine("B.Qty, IfNull(P2.UPrice, J.UPrice) As UPrice, IfNull(P3.UPrice, 0) As UPriceRev, IfNull(M.Qty, 0) As QtyRecv, IfNull(L.Qty, 0) As QtyCancel, ");
                SQL.AppendLine("IfNull(P2.DiscountOld, B.Discount) AS Discount, IfNull(P3.Discount, 0) AS DiscountRev, ");
                SQL.AppendLine("IfNull(P2.DiscountAmtOld, B.DiscountAmt) As DiscountAmt, IfNull(P3.DiscountAmt, 0) As DiscountAmtRev, ");
                SQL.AppendLine("IfNull(P2.RoundingValueOld, B.RoundingValue) As RoundingValue, IfNull(P3.RoundingValue, 0) As RoundingValueRev, ");
                SQL.AppendLine("( ");
                SQL.AppendLine("  IfNull(If(A.TaxCode1 Is Null,0.00,(If(A.TaxCode1=P.ParValue,(((B.Qty - IfNull(L.Qty, 0))*IfNull(J.UPrice, 0))*0.10), 0))), 0) + ");
                SQL.AppendLine("  IfNull(If(A.TaxCode2 Is Null,0.00,(If(A.TaxCode2=P.ParValue,(((B.Qty - IfNull(L.Qty, 0))*IfNull(J.UPrice, 0))*0.10), 0))), 0) + ");
                SQL.AppendLine("  IfNull(If(A.TaxCode3 Is Null,0.00,(If(A.TaxCode3=P.ParValue,(((B.Qty - IfNull(L.Qty, 0))*IfNull(J.UPrice, 0))*0.10), 0))), 0) ");
                SQL.AppendLine(") As PPNTax,");
                SQL.AppendLine("((IfNull(P2.QtyOld, B.Qty) - IfNull(L.Qty, 0))*IfNull(P2.UPrice, J.UPrice)) As Total,  ");//Before revision
                SQL.AppendLine("( ");
                SQL.AppendLine("  ((((IfNull(P2.QtyOld, B.Qty) - IfNull(L.Qty, 0))*IfNull(P2.UPrice, J.UPrice)) * ((100.00-IfNull(P2.DiscountOld, B.Discount))*0.01)) ");
                SQL.AppendLine("  - IfNull(P2.DiscountAmtOld, B.DiscountAmt) + IfNull(P2.RoundingValueOld, B.RoundingValue)) + ");
                SQL.AppendLine("  IfNull(If(A.TaxCode1 Is Null,0.00,(((((IfNull(P2.QtyOld, B.Qty) - IfNull(L.Qty, 0))*IfNull(P2.UPrice, J.UPrice)) * ((100.00-IfNull(P2.DiscountOld, B.Discount))*0.01)) - IfNull(P2.DiscountAmtOld, B.DiscountAmt) + IfNull(P2.RoundingValueOld, B.RoundingValue)) * O1.TaxRate *0.01)), 0.00) + ");
                SQL.AppendLine("  IfNull(If(A.TaxCode2 Is Null,0.00,(((((IfNull(P2.QtyOld, B.Qty) - IfNull(L.Qty, 0))*IfNull(P2.UPrice, J.UPrice)) * ((100.00-IfNull(P2.DiscountOld, B.Discount))*0.01)) - IfNull(P2.DiscountAmtOld, B.DiscountAmt) + IfNull(P2.RoundingValueOld, B.RoundingValue)) * O2.TaxRate *0.01)), 0.00) + ");
                SQL.AppendLine("  IfNull(If(A.TaxCode3 Is Null,0.00,(((((IfNull(P2.QtyOld, B.Qty) - IfNull(L.Qty, 0))*IfNull(P2.UPrice, J.UPrice)) * ((100.00-IfNull(P2.DiscountOld, B.Discount))*0.01)) - IfNull(P2.DiscountAmtOld, B.DiscountAmt) + IfNull(P2.RoundingValueOld, B.RoundingValue)) * O3.TaxRate *0.01)), 0.00) ");
                SQL.AppendLine(") As GrandTotal, ");
                SQL.AppendLine("IfNull(P3.Amt, 0) As TotalRev,  ");//After revision
                SQL.AppendLine("( ");
                SQL.AppendLine("  ((((IfNull(P3.Qty, B.Qty) - IfNull(L.Qty, 0))*IfNull(P3.UPrice, J.UPrice)) * ((100.00-IfNull(P3.Discount, B.Discount))*0.01)) ");
                SQL.AppendLine("  - IfNull(P3.DiscountAmt, B.DiscountAmt) + IfNull(P3.RoundingValue, B.RoundingValue)) + ");
                SQL.AppendLine("  IfNull(If(A.TaxCode1 Is Null,0.00,(((((IfNull(P3.Qty, B.Qty) - IfNull(L.Qty, 0))*IfNull(P3.UPrice, J.UPrice)) * ((100.00-IfNull(P3.Discount, B.Discount))*0.01)) - IfNull(P3.DiscountAmt, B.DiscountAmt) + IfNull(P3.RoundingValue, B.RoundingValue)) * O1.TaxRate *0.01)), 0.00) + ");
                SQL.AppendLine("  IfNull(If(A.TaxCode2 Is Null,0.00,(((((IfNull(P3.Qty, B.Qty) - IfNull(L.Qty, 0))*IfNull(P3.UPrice, J.UPrice)) * ((100.00-IfNull(P3.Discount, B.Discount))*0.01)) - IfNull(P3.DiscountAmt, B.DiscountAmt) + IfNull(P3.RoundingValue, B.RoundingValue)) * O2.TaxRate *0.01)), 0.00) + ");
                SQL.AppendLine("  IfNull(If(A.TaxCode3 Is Null,0.00,(((((IfNull(P3.Qty, B.Qty) - IfNull(L.Qty, 0))*IfNull(P3.UPrice, J.UPrice)) * ((100.00-IfNull(P3.Discount, B.Discount))*0.01)) - IfNull(P3.DiscountAmt, B.DiscountAmt) + IfNull(P3.RoundingValue, B.RoundingValue)) * O3.TaxRate *0.01)), 0.00) ");
                SQL.AppendLine(") As GrandTotalRev, ");
            }
            SQL.AppendLine("I.CurCode, I.FileName1, I.FileName2 ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo = B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode = C.VdCode ");
            SQL.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo= D.DocNo And  B.PORequestDNo = D.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On D.MaterialRequestDocNo = E.DocNo And D.MaterialRequestDNo = E.DNo ");
            SQL.AppendLine("Left JOIN( ");
            SQL.AppendLine("    SELECT A.LocalDocNo As MRLocalDocNo, B.DocNo As MRDocNo, B.DNo ");
            SQL.AppendLine("    FROM TblMaterialRequestHdr A ");
            SQL.AppendLine("    INNER JOIN  TblMaterialRequestDtl B  ON A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And B.MaterialRequestServiceDocNo is not NULL ");
            SQL.AppendLine("    AND (A.DocDt Between @DocDt1 And @DocDt2)  ");
            SQL.AppendLine(")E2 On D.MaterialRequestDocNo = E2.MRDocNo And D.MaterialRequestDNo = E2.DNo ");
            SQL.AppendLine("Inner Join TblItem F On E.ItCode = F.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=F.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblItemCategory G On F.ItCtCode = G.ItCtCode ");
            SQL.AppendLine("Left Join TblItemSubCategory H On F.ItScCode = H.ItScCode ");
            SQL.AppendLine("Inner Join TblQtHdr I On D.QtDocNo = I.DocNo ");
            SQL.AppendLine("Inner Join TblQtdtl J On D.QtDocNo = J.DocNo And D.QtDNo = J.DNo ");            
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select B.PODocNO, B.PODno,  SUM(Qty) Qty ");
	        SQL.AppendLine("    From TblRecvVdHdr A ");
	        SQL.AppendLine("    Inner Join TblRecvVdDtl B On A.DocNo = B.DocNo ");
	        SQL.AppendLine("    Where  B.CancelInd = 'N' And B.Status = 'A' ");
	        SQL.AppendLine("    Group BY B.PODocNo, B.PODno ");
            SQL.AppendLine(")M On B.DocNo = M.PODocNo And B.Dno = M.PODno");
            SQL.AppendLine("Left Join TblPoQtyCancel L On B.DocNo = L.PODocNo And B.Dno = L.PODno And L.cancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblPaymentTerm N On I.PtCode = N.PtCode ");
            if (mIsSiteMandatory)
                SQL.AppendLine("Left Join TblSite K On A.SiteCode=K.SiteCode ");
            SQL.AppendLine("Left Join TblTax O1 On A.TaxCode1 = O1.TaxCode ");
            SQL.AppendLine("Left Join TblTax O2 On A.TaxCode2 = O2.TaxCode ");
            SQL.AppendLine("Left Join TblTax O3 On A.TaxCode3 = O3.TaxCode ");
            SQL.AppendLine("Inner Join TblParameter P On P.ParCode = 'PPNTaxCode' ");
            if (mIsRptProcurementUsePORevision)
            {
                SQL.AppendLine("Left Join(  ");
                SQL.AppendLine("	Select X1.DocNo, X1.PODocNo, X1.PODNo, X1.DiscountOld, X1.DiscountAmtOld, X1.RoundingValueOld, X1.QtyOld, X1.AmtOld, X2.UPrice ");
                SQL.AppendLine("	From TblPORevision X1 ");
                SQL.AppendLine("	Inner Join TblQtDtl X2 On X1.QtDocNoOld = X2.DocNo AND X1.QtDNoOld = X2.DNo ");
                SQL.AppendLine("	Inner Join TblPOHdr X3 On X1.PODocNo=X3.DocNo  And 0=0  And (Left(CONCAT(X3.DocDt, '000000'), 14) >=  @DocDt1 And Left(CONCAT(X3.DocDt, '000000'), 14) <=  @DocDt2) ");
                SQL.AppendLine("	Where X1.DocNo = (Select Min(DocNo) From TblPORevision Where PODocNo = X1.PODocNo) ");
                SQL.AppendLine(")P2 On B.DocNo = P2.PODocNo And B.DNo = P2.PODNo  "); //Before Revision
                SQL.AppendLine("Left Join( ");
                SQL.AppendLine("	Select X1.DocNo, X1.PODocNo, X1.PODNo, X1.Discount, X1.DiscountAmt, X1.RoundingValue, X1.Qty, X1.Amt, X2.UPrice ");
                SQL.AppendLine("	From TblPORevision X1 ");
                SQL.AppendLine("	Inner Join TblQtDtl X2 On X1.QtDocNo = X2.DocNo AND X1.QtDNo = X2.DNo ");
                SQL.AppendLine("	Inner Join TblPOHdr X3 On X1.PODocNo=X3.DocNo  And 0=0  And (Left(CONCAT(X3.DocDt, '000000'), 14) >=  @DocDt1 And Left(CONCAT(X3.DocDt, '000000'), 14) <=  @DocDt2) ");
                SQL.AppendLine("	Where X1.DocNo = (Select Max(DocNo) From TblPORevision Where PODocNo = X1.PODocNo) ");
                SQL.AppendLine(")P3 On B.DocNo = P3.PODocNo And B.DNo = P3.PODNo   "); //After Revision
            }
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And A.Status='A' ");
            if (mIsSiteMandatory && mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 34;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Vendor", 
                        "Item's Code", 
                        "Item's Name",
                        "Foreign Name",
                        "Category",

                        //6-10
                        "Sub Category", 
                        "PO#", 
                        "PO Date",
                        "Local#",
                        "Site",

                        //11-15
                        "PO",  
                        "Received",
                        "Cancelled",
                        "Currency",
                        "Price",
                        
                        //16-20
                        "Total",
                        "PPN",
                        "Term of" + Environment.NewLine + "Payment",
                        "",
                        "Discount %",

                        //21-25
                        "Discount" + Environment.NewLine + "Amount",
                        "Rounding Value",
                        "Grand Total",
                        "File 1",
                        "",

                        //26-30
                        "File 2",
                        "",
                        "Price" + Environment.NewLine + "After Revision",
                        "Total" + Environment.NewLine + "After Revision",
                        "Discount %" + Environment.NewLine + "After Revision",

                        //31-33
                        "Discount Amount" + Environment.NewLine + "After Revision",
                        "Rounding" + Environment.NewLine + "After Revision",
                        "Grand Total" + Environment.NewLine + "After Revision",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 80, 250, 230, 150,
                        
                        //6-10
                        150, 150, 100, 130, 150, 
                        
                        //11-15
                        100, 100, 100, 60, 100, 
                        
                        //16-20
                        100, 130, 150, 20, 100,

                        //21-25
                        100, 100, 130, 200, 20, 

                        //26-30
                        200, 20, 100, 100, 100,

                        //31-33
                        100, 100, 130
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 19, 25, 27 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13, 15, 16, 17, 20, 21, 22, 23, 27, 28, 29, 30, 31, 32, 33 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 19, 20, 21, 22, 30, 31, 32 }, false);
            if (!mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
            if (!mIsSiteMandatory) Sm.GrdColInvisible(Grd1, new int[] { 10 }, false);
            if (!mIsShowTax) Sm.GrdColInvisible(Grd1, new int[] { 17 }, false);
            Grd1.Cols[18].Move(7);
            Grd1.Cols[19].Move(9);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 26, 27, 28, 29, 30, 31, 32, 33 });
            if (!mIsQTAllowToUploadFile)
                Sm.GrdColInvisible(Grd1, new int[] { 24, 25, 26, 27 });
            if (!mIsRptProcurementUsePORevision)
                Sm.GrdColInvisible(Grd1, new int[] { 28, 29, 30, 31, 32, 33 });
            Grd1.Cols[28].Move(18); 
            Grd1.Cols[29].Move(20); 
            Grd1.Cols[30].Move(23); 
            Grd1.Cols[31].Move(25); 
            Grd1.Cols[32].Move(27); 
            Grd1.Cols[33].Move(29);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 20, 21, 22, 30, 31, 32 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                decimal GrandTotal = 0m;
                decimal GrandTotalRev = 0m;
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCatCode), "F.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSubItCat), "F.ItScCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.Itcode", "F.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo", "E2.MRDocNo", "E2.MRLocalDocNo" });

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By C.VdName, ((B.Qty - IfNull(L.Qty, 0))*J.UPrice) Desc; ",
                new string[]
                    {
                        //0
                        "VdName", 

                        //1-5
                        "ItCode", "ItName", "ForeignName", "ItCtName", "ItScName",
                        
                        //6-10
                        "DocNo", "DocDt", "LocalDocNo", "SiteName", "Qty", 
                        
                        //11-15
                        "QtyRecv", "QtyCancel", "CurCode", "UPrice", "PPNTax", 
                        
                        //16-20
                        "Total", "PtName", "Discount", "DiscountAmt", "RoundingValue",

                        //21-25
                        "GrandTotal", "FileName1", "FileName2", "UPriceRev", "TotalRev", 
                        
                        //26-29
                        "DiscountRev", "DiscountAmtRev", "RoundingValueRev", "GrandTotalRev"                        
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                        GrandTotal = dr.GetDecimal(c[21]);
                        if (GrandTotal==0)
                            Grd.Cells[Row, 23].Value = 0m;
                        else
                            Grd.Cells[Row, 23].Value = GrandTotal;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 24);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 25);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 26);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 27);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 28);
                        GrandTotalRev = dr.GetDecimal(c[29]);
                        if (GrandTotalRev == 0)
                            Grd.Cells[Row, 33].Value = 0m;
                        else
                            Grd.Cells[Row, 33].Value = GrandTotalRev;
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 12, 13, 15, 16, 17, 20, 21, 22, 23, 28, 29, 30, 31, 32, 33 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 19 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (e.ColIndex == 25 && Sm.GetGrdStr(Grd1, e.RowIndex, 24).Length != 0 && mIsQTAllowToUploadFile)
            {
                e.DoDefault = false;
                DownloadFileKu(e.RowIndex, 24);
                this.Text = string.Concat(mMenuCode, " - ", Sm.GetMenuDesc("FrmRptVendorPOByPeriod"));
            }

            if (e.ColIndex == 27 && Sm.GetGrdStr(Grd1, e.RowIndex, 26).Length != 0 && mIsQTAllowToUploadFile)
            {
                e.DoDefault = false;
                DownloadFileKu(e.RowIndex, 26);
                this.Text = string.Concat(mMenuCode, " - ", Sm.GetMenuDesc("FrmRptVendorPOByPeriod"));
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 19 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmPO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (e.ColIndex == 25 && Sm.GetGrdStr(Grd1, e.RowIndex, 24).Length != 0 && mIsQTAllowToUploadFile)
            {
                DownloadFileKu(e.RowIndex, 24);
                this.Text = string.Concat(mMenuCode, " - ", Sm.GetMenuDesc("FrmRptVendorPOByPeriod"));
            }

            if (e.ColIndex == 27 && Sm.GetGrdStr(Grd1, e.RowIndex, 26).Length != 0 && mIsQTAllowToUploadFile)
            {
                DownloadFileKu(e.RowIndex, 26);
                this.Text = string.Concat(mMenuCode, " - ", Sm.GetMenuDesc("FrmRptVendorPOByPeriod"));
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 12, 13, 15, 16, 17, 20, 21, 22, 23 });
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void DownloadFileKu(int Row, int Cols)
        {
            string mFileName = Sm.GetGrdStr(Grd1, Row, Cols);
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, mFileName, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD1.FileName = mFileName;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (!Sm.IsGrdValueEmpty(Grd1, Row, Cols, false, "File is empty") && downloadedData != null && downloadedData.Length != 0)
            {
                if (SFD1.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    Sm.StdMsg(mMsgType.Info, "Saved Successfully");
                }
            }
            else
                Sm.StdMsg(mMsgType.Warning, "No File was Downloaded Yet.");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkItCatCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void ChkSubItCat_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's sub category");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueItCatCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCatCode, new Sm.RefreshLue2(Sl.SetLueItCtCodeFilterByItCt), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueSubItCat_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSubItCat, new Sm.RefreshLue1(Sl.SetLueItScCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO#/Local#");
        }

        #endregion
    }
}
