﻿namespace RunSystem
{
    partial class FrmSInv2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSInv2));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnSO = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.LueItCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd11 = new TenTec.Windows.iGridLib.iGrid();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label59 = new System.Windows.Forms.Label();
            this.TxtFreight1 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt1 = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtSeal1 = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtTotal1 = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.LueItCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd12 = new TenTec.Windows.iGridLib.iGrid();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label62 = new System.Windows.Forms.Label();
            this.TxtFreight2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt2 = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtSeal2 = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtTotal2 = new DevExpress.XtraEditors.TextEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.LueItCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd13 = new TenTec.Windows.iGridLib.iGrid();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label63 = new System.Windows.Forms.Label();
            this.TxtFreight3 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt3 = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtSeal3 = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtTotal3 = new DevExpress.XtraEditors.TextEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.LueItCode4 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd14 = new TenTec.Windows.iGridLib.iGrid();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label64 = new System.Windows.Forms.Label();
            this.TxtFreight4 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt4 = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtSeal4 = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtTotal4 = new DevExpress.XtraEditors.TextEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.LueItCode5 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd15 = new TenTec.Windows.iGridLib.iGrid();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label65 = new System.Windows.Forms.Label();
            this.TxtFreight5 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt5 = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtSeal5 = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtTotal5 = new DevExpress.XtraEditors.TextEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.LueItCode6 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd16 = new TenTec.Windows.iGridLib.iGrid();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label66 = new System.Windows.Forms.Label();
            this.TxtFreight6 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt6 = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtSeal6 = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtTotal6 = new DevExpress.XtraEditors.TextEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.LueItCode7 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd17 = new TenTec.Windows.iGridLib.iGrid();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label67 = new System.Windows.Forms.Label();
            this.TxtFreight7 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt7 = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtSeal7 = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.TxtTotal7 = new DevExpress.XtraEditors.TextEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.LueItCode8 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd18 = new TenTec.Windows.iGridLib.iGrid();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label68 = new System.Windows.Forms.Label();
            this.TxtFreight8 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt8 = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtSeal8 = new DevExpress.XtraEditors.TextEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.TxtTotal8 = new DevExpress.XtraEditors.TextEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.LueItCode9 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd19 = new TenTec.Windows.iGridLib.iGrid();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label69 = new System.Windows.Forms.Label();
            this.TxtFreight9 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt9 = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtSeal9 = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.TxtTotal9 = new DevExpress.XtraEditors.TextEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.LueItCode10 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd20 = new TenTec.Windows.iGridLib.iGrid();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label70 = new System.Windows.Forms.Label();
            this.TxtFreight10 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt10 = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.TxtSeal10 = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.TxtTotal10 = new DevExpress.XtraEditors.TextEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.LueItCode11 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd21 = new TenTec.Windows.iGridLib.iGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label71 = new System.Windows.Forms.Label();
            this.TxtFreight11 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt11 = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.TxtSeal11 = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.TxtTotal11 = new DevExpress.XtraEditors.TextEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.LueItCode12 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd22 = new TenTec.Windows.iGridLib.iGrid();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label72 = new System.Windows.Forms.Label();
            this.TxtFreight12 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt12 = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.TxtSeal12 = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.TxtTotal12 = new DevExpress.XtraEditors.TextEdit();
            this.label52 = new System.Windows.Forms.Label();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.LueItCode13 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd23 = new TenTec.Windows.iGridLib.iGrid();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label73 = new System.Windows.Forms.Label();
            this.TxtFreight13 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt13 = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.TxtSeal13 = new DevExpress.XtraEditors.TextEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.TxtTotal13 = new DevExpress.XtraEditors.TextEdit();
            this.label53 = new System.Windows.Forms.Label();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.LueItCode14 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd24 = new TenTec.Windows.iGridLib.iGrid();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label74 = new System.Windows.Forms.Label();
            this.TxtFreight14 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt14 = new DevExpress.XtraEditors.TextEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.TxtSeal14 = new DevExpress.XtraEditors.TextEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.TxtTotal14 = new DevExpress.XtraEditors.TextEdit();
            this.label54 = new System.Windows.Forms.Label();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.LueItCode15 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd25 = new TenTec.Windows.iGridLib.iGrid();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label75 = new System.Windows.Forms.Label();
            this.TxtFreight15 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt15 = new DevExpress.XtraEditors.TextEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.TxtSeal15 = new DevExpress.XtraEditors.TextEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.TxtTotal15 = new DevExpress.XtraEditors.TextEdit();
            this.label55 = new System.Windows.Forms.Label();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.LueItCode16 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd26 = new TenTec.Windows.iGridLib.iGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label76 = new System.Windows.Forms.Label();
            this.TxtFreight16 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt16 = new DevExpress.XtraEditors.TextEdit();
            this.label77 = new System.Windows.Forms.Label();
            this.TxtSeal16 = new DevExpress.XtraEditors.TextEdit();
            this.label78 = new System.Windows.Forms.Label();
            this.TxtTotal16 = new DevExpress.XtraEditors.TextEdit();
            this.label79 = new System.Windows.Forms.Label();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.LueItCode17 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd27 = new TenTec.Windows.iGridLib.iGrid();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label80 = new System.Windows.Forms.Label();
            this.TxtFreight17 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt17 = new DevExpress.XtraEditors.TextEdit();
            this.label81 = new System.Windows.Forms.Label();
            this.TxtSeal17 = new DevExpress.XtraEditors.TextEdit();
            this.label82 = new System.Windows.Forms.Label();
            this.TxtTotal17 = new DevExpress.XtraEditors.TextEdit();
            this.label83 = new System.Windows.Forms.Label();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this.LueItCode18 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd28 = new TenTec.Windows.iGridLib.iGrid();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label84 = new System.Windows.Forms.Label();
            this.TxtFreight18 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt18 = new DevExpress.XtraEditors.TextEdit();
            this.label85 = new System.Windows.Forms.Label();
            this.TxtSeal18 = new DevExpress.XtraEditors.TextEdit();
            this.label86 = new System.Windows.Forms.Label();
            this.TxtTotal18 = new DevExpress.XtraEditors.TextEdit();
            this.label87 = new System.Windows.Forms.Label();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.LueItCode19 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd29 = new TenTec.Windows.iGridLib.iGrid();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label88 = new System.Windows.Forms.Label();
            this.TxtFreight19 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt19 = new DevExpress.XtraEditors.TextEdit();
            this.label89 = new System.Windows.Forms.Label();
            this.TxtSeal19 = new DevExpress.XtraEditors.TextEdit();
            this.label90 = new System.Windows.Forms.Label();
            this.TxtTotal19 = new DevExpress.XtraEditors.TextEdit();
            this.label91 = new System.Windows.Forms.Label();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this.LueItCode20 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd30 = new TenTec.Windows.iGridLib.iGrid();
            this.panel25 = new System.Windows.Forms.Panel();
            this.label92 = new System.Windows.Forms.Label();
            this.TxtFreight20 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt20 = new DevExpress.XtraEditors.TextEdit();
            this.label93 = new System.Windows.Forms.Label();
            this.TxtSeal20 = new DevExpress.XtraEditors.TextEdit();
            this.label94 = new System.Windows.Forms.Label();
            this.TxtTotal20 = new DevExpress.XtraEditors.TextEdit();
            this.label95 = new System.Windows.Forms.Label();
            this.tabPage21 = new System.Windows.Forms.TabPage();
            this.LueItCode21 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd31 = new TenTec.Windows.iGridLib.iGrid();
            this.panel26 = new System.Windows.Forms.Panel();
            this.label96 = new System.Windows.Forms.Label();
            this.TxtFreight21 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt21 = new DevExpress.XtraEditors.TextEdit();
            this.label97 = new System.Windows.Forms.Label();
            this.TxtSeal21 = new DevExpress.XtraEditors.TextEdit();
            this.label98 = new System.Windows.Forms.Label();
            this.TxtTotal21 = new DevExpress.XtraEditors.TextEdit();
            this.label99 = new System.Windows.Forms.Label();
            this.tabPage22 = new System.Windows.Forms.TabPage();
            this.LueItCode22 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd32 = new TenTec.Windows.iGridLib.iGrid();
            this.panel27 = new System.Windows.Forms.Panel();
            this.label100 = new System.Windows.Forms.Label();
            this.TxtFreight22 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt22 = new DevExpress.XtraEditors.TextEdit();
            this.label101 = new System.Windows.Forms.Label();
            this.TxtSeal22 = new DevExpress.XtraEditors.TextEdit();
            this.label102 = new System.Windows.Forms.Label();
            this.TxtTotal22 = new DevExpress.XtraEditors.TextEdit();
            this.label103 = new System.Windows.Forms.Label();
            this.tabPage23 = new System.Windows.Forms.TabPage();
            this.LueItCode23 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd33 = new TenTec.Windows.iGridLib.iGrid();
            this.panel28 = new System.Windows.Forms.Panel();
            this.label104 = new System.Windows.Forms.Label();
            this.TxtFreight23 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt23 = new DevExpress.XtraEditors.TextEdit();
            this.label105 = new System.Windows.Forms.Label();
            this.TxtSeal23 = new DevExpress.XtraEditors.TextEdit();
            this.label106 = new System.Windows.Forms.Label();
            this.TxtTotal23 = new DevExpress.XtraEditors.TextEdit();
            this.label107 = new System.Windows.Forms.Label();
            this.tabPage24 = new System.Windows.Forms.TabPage();
            this.LueItCode24 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd34 = new TenTec.Windows.iGridLib.iGrid();
            this.panel29 = new System.Windows.Forms.Panel();
            this.label108 = new System.Windows.Forms.Label();
            this.TxtFreight24 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt24 = new DevExpress.XtraEditors.TextEdit();
            this.label109 = new System.Windows.Forms.Label();
            this.TxtSeal24 = new DevExpress.XtraEditors.TextEdit();
            this.label110 = new System.Windows.Forms.Label();
            this.TxtTotal24 = new DevExpress.XtraEditors.TextEdit();
            this.label111 = new System.Windows.Forms.Label();
            this.tabPage25 = new System.Windows.Forms.TabPage();
            this.LueItCode25 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd35 = new TenTec.Windows.iGridLib.iGrid();
            this.panel30 = new System.Windows.Forms.Panel();
            this.label112 = new System.Windows.Forms.Label();
            this.TxtFreight25 = new DevExpress.XtraEditors.TextEdit();
            this.TxtCnt25 = new DevExpress.XtraEditors.TextEdit();
            this.label113 = new System.Windows.Forms.Label();
            this.TxtSeal25 = new DevExpress.XtraEditors.TextEdit();
            this.label114 = new System.Windows.Forms.Label();
            this.TxtTotal25 = new DevExpress.XtraEditors.TextEdit();
            this.label115 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label61 = new System.Windows.Forms.Label();
            this.LueNotify = new DevExpress.XtraEditors.LookUpEdit();
            this.label60 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnPL = new DevExpress.XtraEditors.SimpleButton();
            this.BtnPL2 = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.LCDocDt = new DevExpress.XtraEditors.DateEdit();
            this.MeeAccount = new DevExpress.XtraEditors.MemoExEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.TxtPLDocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtSalesContract = new DevExpress.XtraEditors.TextEdit();
            this.TxtLcNo = new DevExpress.XtraEditors.TextEdit();
            this.panel7 = new System.Windows.Forms.Panel();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblLocalDoc = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.BtnTemplate = new DevExpress.XtraEditors.SimpleButton();
            this.ChkShippingMark = new DevExpress.XtraEditors.CheckEdit();
            this.ChkFOB = new DevExpress.XtraEditors.CheckEdit();
            this.label57 = new System.Windows.Forms.Label();
            this.MeeRemark2 = new DevExpress.XtraEditors.MemoExEdit();
            this.label56 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIssued = new DevExpress.XtraEditors.MemoExEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.MeeOrder4 = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.MeeOrder3 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIssued2 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIssued3 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIssued4 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeOrder = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeOrder2 = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtSIDocNo = new DevExpress.XtraEditors.TextEdit();
            this.SFD1 = new System.Windows.Forms.SaveFileDialog();
            this.OD1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal1.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal2.Properties)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal3.Properties)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal4.Properties)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal5.Properties)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal6.Properties)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal7.Properties)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd18)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal8.Properties)).BeginInit();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd19)).BeginInit();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal9.Properties)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd20)).BeginInit();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal10.Properties)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd21)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal11.Properties)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd22)).BeginInit();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal12.Properties)).BeginInit();
            this.tabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd23)).BeginInit();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal13.Properties)).BeginInit();
            this.tabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd24)).BeginInit();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal14.Properties)).BeginInit();
            this.tabPage15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd25)).BeginInit();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal15.Properties)).BeginInit();
            this.tabPage16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd26)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal16.Properties)).BeginInit();
            this.tabPage17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd27)).BeginInit();
            this.panel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal17.Properties)).BeginInit();
            this.tabPage18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd28)).BeginInit();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal18.Properties)).BeginInit();
            this.tabPage19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd29)).BeginInit();
            this.panel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal19.Properties)).BeginInit();
            this.tabPage20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd30)).BeginInit();
            this.panel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal20.Properties)).BeginInit();
            this.tabPage21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd31)).BeginInit();
            this.panel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal21.Properties)).BeginInit();
            this.tabPage22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd32)).BeginInit();
            this.panel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal22.Properties)).BeginInit();
            this.tabPage23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd33)).BeginInit();
            this.panel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal23.Properties)).BeginInit();
            this.tabPage24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd34)).BeginInit();
            this.panel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal24.Properties)).BeginInit();
            this.tabPage25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd35)).BeginInit();
            this.panel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueNotify.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPLDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesContract.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLcNo.Properties)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkShippingMark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFOB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSIDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(889, 0);
            this.panel1.Size = new System.Drawing.Size(70, 470);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.splitContainer1);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Size = new System.Drawing.Size(889, 470);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 251);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(889, 219);
            this.splitContainer1.SplitterDistance = 101;
            this.splitContainer1.TabIndex = 43;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.Grd1);
            this.panel4.Controls.Add(this.BtnSO);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(889, 101);
            this.panel4.TabIndex = 88;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 23);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(889, 78);
            this.Grd1.TabIndex = 42;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnSO
            // 
            this.BtnSO.BackColor = System.Drawing.Color.LightBlue;
            this.BtnSO.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnSO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSO.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSO.Location = new System.Drawing.Point(0, 0);
            this.BtnSO.Name = "BtnSO";
            this.BtnSO.Size = new System.Drawing.Size(889, 23);
            this.BtnSO.TabIndex = 36;
            this.BtnSO.Text = "Sales Order";
            this.BtnSO.UseVisualStyleBackColor = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Controls.Add(this.tabPage13);
            this.tabControl1.Controls.Add(this.tabPage14);
            this.tabControl1.Controls.Add(this.tabPage15);
            this.tabControl1.Controls.Add(this.tabPage16);
            this.tabControl1.Controls.Add(this.tabPage17);
            this.tabControl1.Controls.Add(this.tabPage18);
            this.tabControl1.Controls.Add(this.tabPage19);
            this.tabControl1.Controls.Add(this.tabPage20);
            this.tabControl1.Controls.Add(this.tabPage21);
            this.tabControl1.Controls.Add(this.tabPage22);
            this.tabControl1.Controls.Add(this.tabPage23);
            this.tabControl1.Controls.Add(this.tabPage24);
            this.tabControl1.Controls.Add(this.tabPage25);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(889, 114);
            this.tabControl1.TabIndex = 47;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.LueItCode1);
            this.tabPage1.Controls.Add(this.Grd11);
            this.tabPage1.Controls.Add(this.panel8);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(881, 87);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "No.1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // LueItCode1
            // 
            this.LueItCode1.EnterMoveNextControl = true;
            this.LueItCode1.Location = new System.Drawing.Point(22, 61);
            this.LueItCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode1.Name = "LueItCode1";
            this.LueItCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.Appearance.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode1.Properties.DropDownRows = 20;
            this.LueItCode1.Properties.NullText = "[Empty]";
            this.LueItCode1.Properties.PopupWidth = 500;
            this.LueItCode1.Size = new System.Drawing.Size(152, 20);
            this.LueItCode1.TabIndex = 53;
            this.LueItCode1.ToolTip = "F4 : Show/hide list";
            this.LueItCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd11
            // 
            this.Grd11.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd11.DefaultRow.Height = 20;
            this.Grd11.DefaultRow.Sortable = false;
            this.Grd11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd11.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd11.Header.Height = 21;
            this.Grd11.Location = new System.Drawing.Point(3, 33);
            this.Grd11.Name = "Grd11";
            this.Grd11.RowHeader.Visible = true;
            this.Grd11.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd11.SingleClickEdit = true;
            this.Grd11.Size = new System.Drawing.Size(875, 51);
            this.Grd11.TabIndex = 51;
            this.Grd11.TreeCol = null;
            this.Grd11.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.label59);
            this.panel8.Controls.Add(this.TxtFreight1);
            this.panel8.Controls.Add(this.TxtCnt1);
            this.panel8.Controls.Add(this.label28);
            this.panel8.Controls.Add(this.TxtSeal1);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.TxtTotal1);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(875, 30);
            this.panel8.TabIndex = 44;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(690, 7);
            this.label59.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(84, 14);
            this.label59.TabIndex = 52;
            this.label59.Text = "Ocean Freight";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight1
            // 
            this.TxtFreight1.EditValue = "";
            this.TxtFreight1.EnterMoveNextControl = true;
            this.TxtFreight1.Location = new System.Drawing.Point(777, 4);
            this.TxtFreight1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight1.Name = "TxtFreight1";
            this.TxtFreight1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight1.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight1.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight1.TabIndex = 53;
            this.TxtFreight1.Validated += new System.EventHandler(this.TxtFreight1_Validated);
            // 
            // TxtCnt1
            // 
            this.TxtCnt1.EditValue = "";
            this.TxtCnt1.EnterMoveNextControl = true;
            this.TxtCnt1.Location = new System.Drawing.Point(85, 6);
            this.TxtCnt1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt1.Name = "TxtCnt1";
            this.TxtCnt1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt1.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt1.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt1.TabIndex = 51;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(5, 9);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(78, 14);
            this.label28.TabIndex = 50;
            this.label28.Text = "Container No";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal1
            // 
            this.TxtSeal1.EditValue = "";
            this.TxtSeal1.EnterMoveNextControl = true;
            this.TxtSeal1.Location = new System.Drawing.Point(365, 5);
            this.TxtSeal1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal1.Name = "TxtSeal1";
            this.TxtSeal1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal1.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal1.Size = new System.Drawing.Size(190, 20);
            this.TxtSeal1.TabIndex = 49;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(560, 8);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 14);
            this.label8.TabIndex = 47;
            this.label8.Text = "Total";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal1
            // 
            this.TxtTotal1.EditValue = "";
            this.TxtTotal1.EnterMoveNextControl = true;
            this.TxtTotal1.Location = new System.Drawing.Point(597, 5);
            this.TxtTotal1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal1.Name = "TxtTotal1";
            this.TxtTotal1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal1.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal1.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal1.TabIndex = 48;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(313, 8);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 14);
            this.label11.TabIndex = 45;
            this.label11.Text = "Seal No";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.LueItCode2);
            this.tabPage2.Controls.Add(this.Grd12);
            this.tabPage2.Controls.Add(this.panel9);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(881, 87);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "No.2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // LueItCode2
            // 
            this.LueItCode2.EnterMoveNextControl = true;
            this.LueItCode2.Location = new System.Drawing.Point(72, 58);
            this.LueItCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode2.Name = "LueItCode2";
            this.LueItCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.Appearance.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode2.Properties.DropDownRows = 20;
            this.LueItCode2.Properties.NullText = "[Empty]";
            this.LueItCode2.Properties.PopupWidth = 500;
            this.LueItCode2.Size = new System.Drawing.Size(152, 20);
            this.LueItCode2.TabIndex = 54;
            this.LueItCode2.ToolTip = "F4 : Show/hide list";
            this.LueItCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd12
            // 
            this.Grd12.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd12.DefaultRow.Height = 20;
            this.Grd12.DefaultRow.Sortable = false;
            this.Grd12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd12.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd12.Header.Height = 21;
            this.Grd12.Location = new System.Drawing.Point(3, 35);
            this.Grd12.Name = "Grd12";
            this.Grd12.RowHeader.Visible = true;
            this.Grd12.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd12.SingleClickEdit = true;
            this.Grd12.Size = new System.Drawing.Size(875, 49);
            this.Grd12.TabIndex = 51;
            this.Grd12.TreeCol = null;
            this.Grd12.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.label62);
            this.panel9.Controls.Add(this.TxtFreight2);
            this.panel9.Controls.Add(this.TxtCnt2);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.TxtSeal2);
            this.panel9.Controls.Add(this.label13);
            this.panel9.Controls.Add(this.TxtTotal2);
            this.panel9.Controls.Add(this.label41);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(875, 32);
            this.panel9.TabIndex = 44;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(688, 8);
            this.label62.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(84, 14);
            this.label62.TabIndex = 58;
            this.label62.Text = "Ocean Freight";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight2
            // 
            this.TxtFreight2.EditValue = "";
            this.TxtFreight2.EnterMoveNextControl = true;
            this.TxtFreight2.Location = new System.Drawing.Point(775, 5);
            this.TxtFreight2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight2.Name = "TxtFreight2";
            this.TxtFreight2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight2.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight2.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight2.TabIndex = 59;
            this.TxtFreight2.Validated += new System.EventHandler(this.TxtFreight2_Validated);
            // 
            // TxtCnt2
            // 
            this.TxtCnt2.EditValue = "";
            this.TxtCnt2.EnterMoveNextControl = true;
            this.TxtCnt2.Location = new System.Drawing.Point(85, 6);
            this.TxtCnt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt2.Name = "TxtCnt2";
            this.TxtCnt2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt2.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt2.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt2.TabIndex = 57;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(5, 9);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 14);
            this.label9.TabIndex = 56;
            this.label9.Text = "Container No";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal2
            // 
            this.TxtSeal2.EditValue = "";
            this.TxtSeal2.EnterMoveNextControl = true;
            this.TxtSeal2.Location = new System.Drawing.Point(365, 5);
            this.TxtSeal2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal2.Name = "TxtSeal2";
            this.TxtSeal2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal2.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal2.Size = new System.Drawing.Size(196, 20);
            this.TxtSeal2.TabIndex = 55;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(562, 8);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 14);
            this.label13.TabIndex = 53;
            this.label13.Text = "Total";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal2
            // 
            this.TxtTotal2.EditValue = "";
            this.TxtTotal2.EnterMoveNextControl = true;
            this.TxtTotal2.Location = new System.Drawing.Point(599, 5);
            this.TxtTotal2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal2.Name = "TxtTotal2";
            this.TxtTotal2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal2.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal2.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal2.TabIndex = 54;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Red;
            this.label41.Location = new System.Drawing.Point(313, 8);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(48, 14);
            this.label41.TabIndex = 52;
            this.label41.Text = "Seal No";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.LueItCode3);
            this.tabPage3.Controls.Add(this.Grd13);
            this.tabPage3.Controls.Add(this.panel10);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(881, 87);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "No.3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // LueItCode3
            // 
            this.LueItCode3.EnterMoveNextControl = true;
            this.LueItCode3.Location = new System.Drawing.Point(69, 57);
            this.LueItCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode3.Name = "LueItCode3";
            this.LueItCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.Appearance.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode3.Properties.DropDownRows = 20;
            this.LueItCode3.Properties.NullText = "[Empty]";
            this.LueItCode3.Properties.PopupWidth = 500;
            this.LueItCode3.Size = new System.Drawing.Size(152, 20);
            this.LueItCode3.TabIndex = 54;
            this.LueItCode3.ToolTip = "F4 : Show/hide list";
            this.LueItCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd13
            // 
            this.Grd13.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd13.DefaultRow.Height = 20;
            this.Grd13.DefaultRow.Sortable = false;
            this.Grd13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd13.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd13.Header.Height = 21;
            this.Grd13.Location = new System.Drawing.Point(0, 34);
            this.Grd13.Name = "Grd13";
            this.Grd13.RowHeader.Visible = true;
            this.Grd13.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd13.SingleClickEdit = true;
            this.Grd13.Size = new System.Drawing.Size(881, 53);
            this.Grd13.TabIndex = 51;
            this.Grd13.TreeCol = null;
            this.Grd13.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.Controls.Add(this.label63);
            this.panel10.Controls.Add(this.TxtFreight3);
            this.panel10.Controls.Add(this.TxtCnt3);
            this.panel10.Controls.Add(this.label10);
            this.panel10.Controls.Add(this.TxtSeal3);
            this.panel10.Controls.Add(this.label15);
            this.panel10.Controls.Add(this.TxtTotal3);
            this.panel10.Controls.Add(this.label43);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(881, 34);
            this.panel10.TabIndex = 44;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(695, 11);
            this.label63.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(84, 14);
            this.label63.TabIndex = 58;
            this.label63.Text = "Ocean Freight";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight3
            // 
            this.TxtFreight3.EditValue = "";
            this.TxtFreight3.EnterMoveNextControl = true;
            this.TxtFreight3.Location = new System.Drawing.Point(781, 8);
            this.TxtFreight3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight3.Name = "TxtFreight3";
            this.TxtFreight3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight3.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight3.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight3.TabIndex = 59;
            this.TxtFreight3.Validated += new System.EventHandler(this.TxtFreight3_Validated);
            // 
            // TxtCnt3
            // 
            this.TxtCnt3.EditValue = "";
            this.TxtCnt3.EnterMoveNextControl = true;
            this.TxtCnt3.Location = new System.Drawing.Point(86, 8);
            this.TxtCnt3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt3.Name = "TxtCnt3";
            this.TxtCnt3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt3.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt3.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt3.TabIndex = 57;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(6, 11);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 14);
            this.label10.TabIndex = 56;
            this.label10.Text = "Container No";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal3
            // 
            this.TxtSeal3.EditValue = "";
            this.TxtSeal3.EnterMoveNextControl = true;
            this.TxtSeal3.Location = new System.Drawing.Point(366, 7);
            this.TxtSeal3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal3.Name = "TxtSeal3";
            this.TxtSeal3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal3.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal3.Size = new System.Drawing.Size(197, 20);
            this.TxtSeal3.TabIndex = 55;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(566, 10);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 14);
            this.label15.TabIndex = 53;
            this.label15.Text = "Total";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal3
            // 
            this.TxtTotal3.EditValue = "";
            this.TxtTotal3.EnterMoveNextControl = true;
            this.TxtTotal3.Location = new System.Drawing.Point(603, 7);
            this.TxtTotal3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal3.Name = "TxtTotal3";
            this.TxtTotal3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal3.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal3.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal3.TabIndex = 54;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Red;
            this.label43.Location = new System.Drawing.Point(314, 10);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(48, 14);
            this.label43.TabIndex = 52;
            this.label43.Text = "Seal No";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.LueItCode4);
            this.tabPage4.Controls.Add(this.Grd14);
            this.tabPage4.Controls.Add(this.panel11);
            this.tabPage4.Location = new System.Drawing.Point(4, 23);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(881, 87);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "No.4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // LueItCode4
            // 
            this.LueItCode4.EnterMoveNextControl = true;
            this.LueItCode4.Location = new System.Drawing.Point(71, 57);
            this.LueItCode4.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode4.Name = "LueItCode4";
            this.LueItCode4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.Appearance.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode4.Properties.DropDownRows = 20;
            this.LueItCode4.Properties.NullText = "[Empty]";
            this.LueItCode4.Properties.PopupWidth = 500;
            this.LueItCode4.Size = new System.Drawing.Size(152, 20);
            this.LueItCode4.TabIndex = 54;
            this.LueItCode4.ToolTip = "F4 : Show/hide list";
            this.LueItCode4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd14
            // 
            this.Grd14.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd14.DefaultRow.Height = 20;
            this.Grd14.DefaultRow.Sortable = false;
            this.Grd14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd14.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd14.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd14.Header.Height = 21;
            this.Grd14.Location = new System.Drawing.Point(0, 32);
            this.Grd14.Name = "Grd14";
            this.Grd14.RowHeader.Visible = true;
            this.Grd14.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd14.SingleClickEdit = true;
            this.Grd14.Size = new System.Drawing.Size(881, 55);
            this.Grd14.TabIndex = 51;
            this.Grd14.TreeCol = null;
            this.Grd14.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel11.Controls.Add(this.label64);
            this.panel11.Controls.Add(this.TxtFreight4);
            this.panel11.Controls.Add(this.TxtCnt4);
            this.panel11.Controls.Add(this.label12);
            this.panel11.Controls.Add(this.TxtSeal4);
            this.panel11.Controls.Add(this.label19);
            this.panel11.Controls.Add(this.TxtTotal4);
            this.panel11.Controls.Add(this.label44);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(881, 32);
            this.panel11.TabIndex = 44;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(694, 8);
            this.label64.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(84, 14);
            this.label64.TabIndex = 58;
            this.label64.Text = "Ocean Freight";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight4
            // 
            this.TxtFreight4.EditValue = "";
            this.TxtFreight4.EnterMoveNextControl = true;
            this.TxtFreight4.Location = new System.Drawing.Point(779, 5);
            this.TxtFreight4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight4.Name = "TxtFreight4";
            this.TxtFreight4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight4.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight4.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight4.TabIndex = 59;
            this.TxtFreight4.Validated += new System.EventHandler(this.TxtFreight4_Validated);
            // 
            // TxtCnt4
            // 
            this.TxtCnt4.EditValue = "";
            this.TxtCnt4.EnterMoveNextControl = true;
            this.TxtCnt4.Location = new System.Drawing.Point(85, 5);
            this.TxtCnt4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt4.Name = "TxtCnt4";
            this.TxtCnt4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt4.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt4.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt4.TabIndex = 57;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(5, 8);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 14);
            this.label12.TabIndex = 56;
            this.label12.Text = "Container No";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal4
            // 
            this.TxtSeal4.EditValue = "";
            this.TxtSeal4.EnterMoveNextControl = true;
            this.TxtSeal4.Location = new System.Drawing.Point(363, 4);
            this.TxtSeal4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal4.Name = "TxtSeal4";
            this.TxtSeal4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal4.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal4.Size = new System.Drawing.Size(201, 20);
            this.TxtSeal4.TabIndex = 55;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(566, 7);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 14);
            this.label19.TabIndex = 53;
            this.label19.Text = "Total";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal4
            // 
            this.TxtTotal4.EditValue = "";
            this.TxtTotal4.EnterMoveNextControl = true;
            this.TxtTotal4.Location = new System.Drawing.Point(603, 4);
            this.TxtTotal4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal4.Name = "TxtTotal4";
            this.TxtTotal4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal4.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal4.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal4.TabIndex = 54;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Red;
            this.label44.Location = new System.Drawing.Point(311, 7);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(48, 14);
            this.label44.TabIndex = 52;
            this.label44.Text = "Seal No";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.LueItCode5);
            this.tabPage5.Controls.Add(this.Grd15);
            this.tabPage5.Controls.Add(this.panel12);
            this.tabPage5.Location = new System.Drawing.Point(4, 23);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(881, 87);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "No.5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // LueItCode5
            // 
            this.LueItCode5.EnterMoveNextControl = true;
            this.LueItCode5.Location = new System.Drawing.Point(73, 55);
            this.LueItCode5.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode5.Name = "LueItCode5";
            this.LueItCode5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.Appearance.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode5.Properties.DropDownRows = 20;
            this.LueItCode5.Properties.NullText = "[Empty]";
            this.LueItCode5.Properties.PopupWidth = 500;
            this.LueItCode5.Size = new System.Drawing.Size(152, 20);
            this.LueItCode5.TabIndex = 54;
            this.LueItCode5.ToolTip = "F4 : Show/hide list";
            this.LueItCode5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd15
            // 
            this.Grd15.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd15.DefaultRow.Height = 20;
            this.Grd15.DefaultRow.Sortable = false;
            this.Grd15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd15.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd15.Header.Height = 21;
            this.Grd15.Location = new System.Drawing.Point(0, 32);
            this.Grd15.Name = "Grd15";
            this.Grd15.RowHeader.Visible = true;
            this.Grd15.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd15.SingleClickEdit = true;
            this.Grd15.Size = new System.Drawing.Size(881, 55);
            this.Grd15.TabIndex = 51;
            this.Grd15.TreeCol = null;
            this.Grd15.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel12.Controls.Add(this.label65);
            this.panel12.Controls.Add(this.TxtFreight5);
            this.panel12.Controls.Add(this.TxtCnt5);
            this.panel12.Controls.Add(this.label14);
            this.panel12.Controls.Add(this.TxtSeal5);
            this.panel12.Controls.Add(this.label21);
            this.panel12.Controls.Add(this.TxtTotal5);
            this.panel12.Controls.Add(this.label45);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(881, 32);
            this.panel12.TabIndex = 44;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(693, 9);
            this.label65.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(84, 14);
            this.label65.TabIndex = 58;
            this.label65.Text = "Ocean Freight";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight5
            // 
            this.TxtFreight5.EditValue = "";
            this.TxtFreight5.EnterMoveNextControl = true;
            this.TxtFreight5.Location = new System.Drawing.Point(779, 5);
            this.TxtFreight5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight5.Name = "TxtFreight5";
            this.TxtFreight5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight5.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight5.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight5.TabIndex = 59;
            this.TxtFreight5.Validated += new System.EventHandler(this.TxtFreight5_Validated);
            // 
            // TxtCnt5
            // 
            this.TxtCnt5.EditValue = "";
            this.TxtCnt5.EnterMoveNextControl = true;
            this.TxtCnt5.Location = new System.Drawing.Point(84, 6);
            this.TxtCnt5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt5.Name = "TxtCnt5";
            this.TxtCnt5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt5.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt5.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt5.TabIndex = 57;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(4, 9);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 14);
            this.label14.TabIndex = 56;
            this.label14.Text = "Container No";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal5
            // 
            this.TxtSeal5.EditValue = "";
            this.TxtSeal5.EnterMoveNextControl = true;
            this.TxtSeal5.Location = new System.Drawing.Point(364, 5);
            this.TxtSeal5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal5.Name = "TxtSeal5";
            this.TxtSeal5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal5.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal5.Size = new System.Drawing.Size(201, 20);
            this.TxtSeal5.TabIndex = 55;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(567, 8);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 14);
            this.label21.TabIndex = 53;
            this.label21.Text = "Total";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal5
            // 
            this.TxtTotal5.EditValue = "";
            this.TxtTotal5.EnterMoveNextControl = true;
            this.TxtTotal5.Location = new System.Drawing.Point(604, 5);
            this.TxtTotal5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal5.Name = "TxtTotal5";
            this.TxtTotal5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal5.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal5.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal5.TabIndex = 54;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Red;
            this.label45.Location = new System.Drawing.Point(312, 8);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(48, 14);
            this.label45.TabIndex = 52;
            this.label45.Text = "Seal No";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.LueItCode6);
            this.tabPage6.Controls.Add(this.Grd16);
            this.tabPage6.Controls.Add(this.panel13);
            this.tabPage6.Location = new System.Drawing.Point(4, 23);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(881, 87);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "No.6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // LueItCode6
            // 
            this.LueItCode6.EnterMoveNextControl = true;
            this.LueItCode6.Location = new System.Drawing.Point(75, 56);
            this.LueItCode6.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode6.Name = "LueItCode6";
            this.LueItCode6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.Appearance.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode6.Properties.DropDownRows = 20;
            this.LueItCode6.Properties.NullText = "[Empty]";
            this.LueItCode6.Properties.PopupWidth = 500;
            this.LueItCode6.Size = new System.Drawing.Size(152, 20);
            this.LueItCode6.TabIndex = 54;
            this.LueItCode6.ToolTip = "F4 : Show/hide list";
            this.LueItCode6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd16
            // 
            this.Grd16.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd16.DefaultRow.Height = 20;
            this.Grd16.DefaultRow.Sortable = false;
            this.Grd16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd16.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd16.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd16.Header.Height = 21;
            this.Grd16.Location = new System.Drawing.Point(0, 33);
            this.Grd16.Name = "Grd16";
            this.Grd16.RowHeader.Visible = true;
            this.Grd16.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd16.SingleClickEdit = true;
            this.Grd16.Size = new System.Drawing.Size(881, 54);
            this.Grd16.TabIndex = 51;
            this.Grd16.TreeCol = null;
            this.Grd16.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel13.Controls.Add(this.label66);
            this.panel13.Controls.Add(this.TxtFreight6);
            this.panel13.Controls.Add(this.TxtCnt6);
            this.panel13.Controls.Add(this.label16);
            this.panel13.Controls.Add(this.TxtSeal6);
            this.panel13.Controls.Add(this.label23);
            this.panel13.Controls.Add(this.TxtTotal6);
            this.panel13.Controls.Add(this.label46);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(881, 33);
            this.panel13.TabIndex = 44;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(695, 9);
            this.label66.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(84, 14);
            this.label66.TabIndex = 58;
            this.label66.Text = "Ocean Freight";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight6
            // 
            this.TxtFreight6.EditValue = "";
            this.TxtFreight6.EnterMoveNextControl = true;
            this.TxtFreight6.Location = new System.Drawing.Point(781, 6);
            this.TxtFreight6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight6.Name = "TxtFreight6";
            this.TxtFreight6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight6.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight6.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight6.TabIndex = 59;
            this.TxtFreight6.Validated += new System.EventHandler(this.TxtFreight6_Validated);
            // 
            // TxtCnt6
            // 
            this.TxtCnt6.EditValue = "";
            this.TxtCnt6.EnterMoveNextControl = true;
            this.TxtCnt6.Location = new System.Drawing.Point(85, 6);
            this.TxtCnt6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt6.Name = "TxtCnt6";
            this.TxtCnt6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt6.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt6.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt6.TabIndex = 57;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(5, 9);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 14);
            this.label16.TabIndex = 56;
            this.label16.Text = "Container No";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal6
            // 
            this.TxtSeal6.EditValue = "";
            this.TxtSeal6.EnterMoveNextControl = true;
            this.TxtSeal6.Location = new System.Drawing.Point(365, 5);
            this.TxtSeal6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal6.Name = "TxtSeal6";
            this.TxtSeal6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal6.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal6.Size = new System.Drawing.Size(203, 20);
            this.TxtSeal6.TabIndex = 55;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(568, 9);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 14);
            this.label23.TabIndex = 53;
            this.label23.Text = "Total";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal6
            // 
            this.TxtTotal6.EditValue = "";
            this.TxtTotal6.EnterMoveNextControl = true;
            this.TxtTotal6.Location = new System.Drawing.Point(605, 6);
            this.TxtTotal6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal6.Name = "TxtTotal6";
            this.TxtTotal6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal6.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal6.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal6.TabIndex = 54;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Red;
            this.label46.Location = new System.Drawing.Point(313, 8);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(48, 14);
            this.label46.TabIndex = 52;
            this.label46.Text = "Seal No";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.LueItCode7);
            this.tabPage7.Controls.Add(this.Grd17);
            this.tabPage7.Controls.Add(this.panel14);
            this.tabPage7.Location = new System.Drawing.Point(4, 23);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(881, 87);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "No.7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // LueItCode7
            // 
            this.LueItCode7.EnterMoveNextControl = true;
            this.LueItCode7.Location = new System.Drawing.Point(67, 55);
            this.LueItCode7.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode7.Name = "LueItCode7";
            this.LueItCode7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.Appearance.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode7.Properties.DropDownRows = 20;
            this.LueItCode7.Properties.NullText = "[Empty]";
            this.LueItCode7.Properties.PopupWidth = 500;
            this.LueItCode7.Size = new System.Drawing.Size(152, 20);
            this.LueItCode7.TabIndex = 54;
            this.LueItCode7.ToolTip = "F4 : Show/hide list";
            this.LueItCode7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd17
            // 
            this.Grd17.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd17.DefaultRow.Height = 20;
            this.Grd17.DefaultRow.Sortable = false;
            this.Grd17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd17.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd17.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd17.Header.Height = 21;
            this.Grd17.Location = new System.Drawing.Point(0, 32);
            this.Grd17.Name = "Grd17";
            this.Grd17.RowHeader.Visible = true;
            this.Grd17.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd17.SingleClickEdit = true;
            this.Grd17.Size = new System.Drawing.Size(881, 55);
            this.Grd17.TabIndex = 51;
            this.Grd17.TreeCol = null;
            this.Grd17.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel14.Controls.Add(this.label67);
            this.panel14.Controls.Add(this.TxtFreight7);
            this.panel14.Controls.Add(this.TxtCnt7);
            this.panel14.Controls.Add(this.label20);
            this.panel14.Controls.Add(this.TxtSeal7);
            this.panel14.Controls.Add(this.label25);
            this.panel14.Controls.Add(this.TxtTotal7);
            this.panel14.Controls.Add(this.label47);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(881, 32);
            this.panel14.TabIndex = 44;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(691, 8);
            this.label67.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(84, 14);
            this.label67.TabIndex = 58;
            this.label67.Text = "Ocean Freight";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight7
            // 
            this.TxtFreight7.EditValue = "";
            this.TxtFreight7.EnterMoveNextControl = true;
            this.TxtFreight7.Location = new System.Drawing.Point(778, 5);
            this.TxtFreight7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight7.Name = "TxtFreight7";
            this.TxtFreight7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight7.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight7.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight7.TabIndex = 59;
            this.TxtFreight7.Validated += new System.EventHandler(this.TxtFreight7_Validated);
            // 
            // TxtCnt7
            // 
            this.TxtCnt7.EditValue = "";
            this.TxtCnt7.EnterMoveNextControl = true;
            this.TxtCnt7.Location = new System.Drawing.Point(85, 6);
            this.TxtCnt7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt7.Name = "TxtCnt7";
            this.TxtCnt7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt7.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt7.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt7.TabIndex = 57;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(5, 9);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 14);
            this.label20.TabIndex = 56;
            this.label20.Text = "Container No";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal7
            // 
            this.TxtSeal7.EditValue = "";
            this.TxtSeal7.EnterMoveNextControl = true;
            this.TxtSeal7.Location = new System.Drawing.Point(365, 5);
            this.TxtSeal7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal7.Name = "TxtSeal7";
            this.TxtSeal7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal7.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal7.Size = new System.Drawing.Size(193, 20);
            this.TxtSeal7.TabIndex = 55;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(559, 8);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(35, 14);
            this.label25.TabIndex = 53;
            this.label25.Text = "Total";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal7
            // 
            this.TxtTotal7.EditValue = "";
            this.TxtTotal7.EnterMoveNextControl = true;
            this.TxtTotal7.Location = new System.Drawing.Point(596, 5);
            this.TxtTotal7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal7.Name = "TxtTotal7";
            this.TxtTotal7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal7.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal7.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal7.TabIndex = 54;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Red;
            this.label47.Location = new System.Drawing.Point(313, 8);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(48, 14);
            this.label47.TabIndex = 52;
            this.label47.Text = "Seal No";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.LueItCode8);
            this.tabPage8.Controls.Add(this.Grd18);
            this.tabPage8.Controls.Add(this.panel15);
            this.tabPage8.Location = new System.Drawing.Point(4, 23);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(881, 87);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "No.8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // LueItCode8
            // 
            this.LueItCode8.EnterMoveNextControl = true;
            this.LueItCode8.Location = new System.Drawing.Point(71, 57);
            this.LueItCode8.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode8.Name = "LueItCode8";
            this.LueItCode8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.Appearance.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode8.Properties.DropDownRows = 20;
            this.LueItCode8.Properties.NullText = "[Empty]";
            this.LueItCode8.Properties.PopupWidth = 500;
            this.LueItCode8.Size = new System.Drawing.Size(152, 20);
            this.LueItCode8.TabIndex = 54;
            this.LueItCode8.ToolTip = "F4 : Show/hide list";
            this.LueItCode8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd18
            // 
            this.Grd18.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd18.DefaultRow.Height = 20;
            this.Grd18.DefaultRow.Sortable = false;
            this.Grd18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd18.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd18.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd18.Header.Height = 21;
            this.Grd18.Location = new System.Drawing.Point(0, 32);
            this.Grd18.Name = "Grd18";
            this.Grd18.RowHeader.Visible = true;
            this.Grd18.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd18.SingleClickEdit = true;
            this.Grd18.Size = new System.Drawing.Size(881, 55);
            this.Grd18.TabIndex = 51;
            this.Grd18.TreeCol = null;
            this.Grd18.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel15.Controls.Add(this.label68);
            this.panel15.Controls.Add(this.TxtFreight8);
            this.panel15.Controls.Add(this.TxtCnt8);
            this.panel15.Controls.Add(this.label22);
            this.panel15.Controls.Add(this.TxtSeal8);
            this.panel15.Controls.Add(this.label27);
            this.panel15.Controls.Add(this.TxtTotal8);
            this.panel15.Controls.Add(this.label48);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(881, 32);
            this.panel15.TabIndex = 44;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(692, 8);
            this.label68.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(84, 14);
            this.label68.TabIndex = 58;
            this.label68.Text = "Ocean Freight";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight8
            // 
            this.TxtFreight8.EditValue = "";
            this.TxtFreight8.EnterMoveNextControl = true;
            this.TxtFreight8.Location = new System.Drawing.Point(777, 6);
            this.TxtFreight8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight8.Name = "TxtFreight8";
            this.TxtFreight8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight8.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight8.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight8.TabIndex = 59;
            this.TxtFreight8.Validated += new System.EventHandler(this.TxtFreight8_Validated);
            // 
            // TxtCnt8
            // 
            this.TxtCnt8.EditValue = "";
            this.TxtCnt8.EnterMoveNextControl = true;
            this.TxtCnt8.Location = new System.Drawing.Point(86, 6);
            this.TxtCnt8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt8.Name = "TxtCnt8";
            this.TxtCnt8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt8.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt8.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt8.TabIndex = 57;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(6, 9);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(78, 14);
            this.label22.TabIndex = 56;
            this.label22.Text = "Container No";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal8
            // 
            this.TxtSeal8.EditValue = "";
            this.TxtSeal8.EnterMoveNextControl = true;
            this.TxtSeal8.Location = new System.Drawing.Point(366, 5);
            this.TxtSeal8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal8.Name = "TxtSeal8";
            this.TxtSeal8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal8.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal8.Size = new System.Drawing.Size(187, 20);
            this.TxtSeal8.TabIndex = 55;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(556, 8);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(35, 14);
            this.label27.TabIndex = 53;
            this.label27.Text = "Total";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal8
            // 
            this.TxtTotal8.EditValue = "";
            this.TxtTotal8.EnterMoveNextControl = true;
            this.TxtTotal8.Location = new System.Drawing.Point(593, 5);
            this.TxtTotal8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal8.Name = "TxtTotal8";
            this.TxtTotal8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal8.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal8.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal8.TabIndex = 54;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Red;
            this.label48.Location = new System.Drawing.Point(314, 8);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(48, 14);
            this.label48.TabIndex = 52;
            this.label48.Text = "Seal No";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.LueItCode9);
            this.tabPage9.Controls.Add(this.Grd19);
            this.tabPage9.Controls.Add(this.panel16);
            this.tabPage9.Location = new System.Drawing.Point(4, 23);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(881, 87);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "No.9";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // LueItCode9
            // 
            this.LueItCode9.EnterMoveNextControl = true;
            this.LueItCode9.Location = new System.Drawing.Point(67, 59);
            this.LueItCode9.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode9.Name = "LueItCode9";
            this.LueItCode9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.Appearance.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode9.Properties.DropDownRows = 20;
            this.LueItCode9.Properties.NullText = "[Empty]";
            this.LueItCode9.Properties.PopupWidth = 500;
            this.LueItCode9.Size = new System.Drawing.Size(152, 20);
            this.LueItCode9.TabIndex = 54;
            this.LueItCode9.ToolTip = "F4 : Show/hide list";
            this.LueItCode9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd19
            // 
            this.Grd19.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd19.DefaultRow.Height = 20;
            this.Grd19.DefaultRow.Sortable = false;
            this.Grd19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd19.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd19.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd19.Header.Height = 21;
            this.Grd19.Location = new System.Drawing.Point(0, 32);
            this.Grd19.Name = "Grd19";
            this.Grd19.RowHeader.Visible = true;
            this.Grd19.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd19.SingleClickEdit = true;
            this.Grd19.Size = new System.Drawing.Size(881, 55);
            this.Grd19.TabIndex = 51;
            this.Grd19.TreeCol = null;
            this.Grd19.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel16.Controls.Add(this.label69);
            this.panel16.Controls.Add(this.TxtFreight9);
            this.panel16.Controls.Add(this.TxtCnt9);
            this.panel16.Controls.Add(this.label24);
            this.panel16.Controls.Add(this.TxtSeal9);
            this.panel16.Controls.Add(this.label29);
            this.panel16.Controls.Add(this.TxtTotal9);
            this.panel16.Controls.Add(this.label49);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(881, 32);
            this.panel16.TabIndex = 44;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(692, 8);
            this.label69.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(84, 14);
            this.label69.TabIndex = 58;
            this.label69.Text = "Ocean Freight";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight9
            // 
            this.TxtFreight9.EditValue = "";
            this.TxtFreight9.EnterMoveNextControl = true;
            this.TxtFreight9.Location = new System.Drawing.Point(778, 5);
            this.TxtFreight9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight9.Name = "TxtFreight9";
            this.TxtFreight9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight9.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight9.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight9.TabIndex = 59;
            this.TxtFreight9.Validated += new System.EventHandler(this.TxtFreight9_Validated);
            // 
            // TxtCnt9
            // 
            this.TxtCnt9.EditValue = "";
            this.TxtCnt9.EnterMoveNextControl = true;
            this.TxtCnt9.Location = new System.Drawing.Point(85, 6);
            this.TxtCnt9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt9.Name = "TxtCnt9";
            this.TxtCnt9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt9.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt9.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt9.TabIndex = 57;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(5, 9);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(78, 14);
            this.label24.TabIndex = 56;
            this.label24.Text = "Container No";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal9
            // 
            this.TxtSeal9.EditValue = "";
            this.TxtSeal9.EnterMoveNextControl = true;
            this.TxtSeal9.Location = new System.Drawing.Point(365, 5);
            this.TxtSeal9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal9.Name = "TxtSeal9";
            this.TxtSeal9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal9.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal9.Size = new System.Drawing.Size(192, 20);
            this.TxtSeal9.TabIndex = 55;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(557, 9);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(35, 14);
            this.label29.TabIndex = 53;
            this.label29.Text = "Total";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal9
            // 
            this.TxtTotal9.EditValue = "";
            this.TxtTotal9.EnterMoveNextControl = true;
            this.TxtTotal9.Location = new System.Drawing.Point(594, 6);
            this.TxtTotal9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal9.Name = "TxtTotal9";
            this.TxtTotal9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal9.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal9.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal9.TabIndex = 54;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Red;
            this.label49.Location = new System.Drawing.Point(313, 8);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(48, 14);
            this.label49.TabIndex = 52;
            this.label49.Text = "Seal No";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.LueItCode10);
            this.tabPage10.Controls.Add(this.Grd20);
            this.tabPage10.Controls.Add(this.panel17);
            this.tabPage10.Location = new System.Drawing.Point(4, 23);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(881, 87);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "No.10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // LueItCode10
            // 
            this.LueItCode10.EnterMoveNextControl = true;
            this.LueItCode10.Location = new System.Drawing.Point(67, 57);
            this.LueItCode10.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode10.Name = "LueItCode10";
            this.LueItCode10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.Appearance.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode10.Properties.DropDownRows = 20;
            this.LueItCode10.Properties.NullText = "[Empty]";
            this.LueItCode10.Properties.PopupWidth = 500;
            this.LueItCode10.Size = new System.Drawing.Size(152, 20);
            this.LueItCode10.TabIndex = 54;
            this.LueItCode10.ToolTip = "F4 : Show/hide list";
            this.LueItCode10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd20
            // 
            this.Grd20.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd20.DefaultRow.Height = 20;
            this.Grd20.DefaultRow.Sortable = false;
            this.Grd20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd20.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd20.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd20.Header.Height = 21;
            this.Grd20.Location = new System.Drawing.Point(0, 33);
            this.Grd20.Name = "Grd20";
            this.Grd20.RowHeader.Visible = true;
            this.Grd20.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd20.SingleClickEdit = true;
            this.Grd20.Size = new System.Drawing.Size(881, 54);
            this.Grd20.TabIndex = 51;
            this.Grd20.TreeCol = null;
            this.Grd20.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel17.Controls.Add(this.label70);
            this.panel17.Controls.Add(this.TxtFreight10);
            this.panel17.Controls.Add(this.TxtCnt10);
            this.panel17.Controls.Add(this.label26);
            this.panel17.Controls.Add(this.TxtSeal10);
            this.panel17.Controls.Add(this.label31);
            this.panel17.Controls.Add(this.TxtTotal10);
            this.panel17.Controls.Add(this.label50);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(881, 33);
            this.panel17.TabIndex = 44;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(692, 9);
            this.label70.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(84, 14);
            this.label70.TabIndex = 58;
            this.label70.Text = "Ocean Freight";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight10
            // 
            this.TxtFreight10.EditValue = "";
            this.TxtFreight10.EnterMoveNextControl = true;
            this.TxtFreight10.Location = new System.Drawing.Point(777, 7);
            this.TxtFreight10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight10.Name = "TxtFreight10";
            this.TxtFreight10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight10.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight10.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight10.TabIndex = 59;
            this.TxtFreight10.Validated += new System.EventHandler(this.TxtFreight10_Validated);
            // 
            // TxtCnt10
            // 
            this.TxtCnt10.EditValue = "";
            this.TxtCnt10.EnterMoveNextControl = true;
            this.TxtCnt10.Location = new System.Drawing.Point(85, 7);
            this.TxtCnt10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt10.Name = "TxtCnt10";
            this.TxtCnt10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt10.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt10.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt10.TabIndex = 57;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(5, 10);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(78, 14);
            this.label26.TabIndex = 56;
            this.label26.Text = "Container No";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal10
            // 
            this.TxtSeal10.EditValue = "";
            this.TxtSeal10.EnterMoveNextControl = true;
            this.TxtSeal10.Location = new System.Drawing.Point(365, 6);
            this.TxtSeal10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal10.Name = "TxtSeal10";
            this.TxtSeal10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal10.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal10.Size = new System.Drawing.Size(197, 20);
            this.TxtSeal10.TabIndex = 55;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(564, 9);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(35, 14);
            this.label31.TabIndex = 53;
            this.label31.Text = "Total";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal10
            // 
            this.TxtTotal10.EditValue = "";
            this.TxtTotal10.EnterMoveNextControl = true;
            this.TxtTotal10.Location = new System.Drawing.Point(601, 6);
            this.TxtTotal10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal10.Name = "TxtTotal10";
            this.TxtTotal10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal10.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal10.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal10.TabIndex = 54;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Red;
            this.label50.Location = new System.Drawing.Point(313, 9);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(48, 14);
            this.label50.TabIndex = 52;
            this.label50.Text = "Seal No";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.LueItCode11);
            this.tabPage11.Controls.Add(this.Grd21);
            this.tabPage11.Controls.Add(this.panel5);
            this.tabPage11.Location = new System.Drawing.Point(4, 23);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(881, 87);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "No.11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // LueItCode11
            // 
            this.LueItCode11.EnterMoveNextControl = true;
            this.LueItCode11.Location = new System.Drawing.Point(68, 55);
            this.LueItCode11.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode11.Name = "LueItCode11";
            this.LueItCode11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.Appearance.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode11.Properties.DropDownRows = 20;
            this.LueItCode11.Properties.NullText = "[Empty]";
            this.LueItCode11.Properties.PopupWidth = 500;
            this.LueItCode11.Size = new System.Drawing.Size(152, 20);
            this.LueItCode11.TabIndex = 54;
            this.LueItCode11.ToolTip = "F4 : Show/hide list";
            this.LueItCode11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd21
            // 
            this.Grd21.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd21.DefaultRow.Height = 20;
            this.Grd21.DefaultRow.Sortable = false;
            this.Grd21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd21.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd21.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd21.Header.Height = 21;
            this.Grd21.Location = new System.Drawing.Point(0, 33);
            this.Grd21.Name = "Grd21";
            this.Grd21.RowHeader.Visible = true;
            this.Grd21.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd21.SingleClickEdit = true;
            this.Grd21.Size = new System.Drawing.Size(881, 54);
            this.Grd21.TabIndex = 51;
            this.Grd21.TreeCol = null;
            this.Grd21.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.label71);
            this.panel5.Controls.Add(this.TxtFreight11);
            this.panel5.Controls.Add(this.TxtCnt11);
            this.panel5.Controls.Add(this.label30);
            this.panel5.Controls.Add(this.TxtSeal11);
            this.panel5.Controls.Add(this.label32);
            this.panel5.Controls.Add(this.TxtTotal11);
            this.panel5.Controls.Add(this.label51);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(881, 33);
            this.panel5.TabIndex = 44;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(694, 10);
            this.label71.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(84, 14);
            this.label71.TabIndex = 58;
            this.label71.Text = "Ocean Freight";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight11
            // 
            this.TxtFreight11.EditValue = "";
            this.TxtFreight11.EnterMoveNextControl = true;
            this.TxtFreight11.Location = new System.Drawing.Point(779, 7);
            this.TxtFreight11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight11.Name = "TxtFreight11";
            this.TxtFreight11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight11.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight11.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight11.TabIndex = 59;
            this.TxtFreight11.Validated += new System.EventHandler(this.TxtFreight11_Validated);
            // 
            // TxtCnt11
            // 
            this.TxtCnt11.EditValue = "";
            this.TxtCnt11.EnterMoveNextControl = true;
            this.TxtCnt11.Location = new System.Drawing.Point(86, 7);
            this.TxtCnt11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt11.Name = "TxtCnt11";
            this.TxtCnt11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt11.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt11.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt11.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt11.TabIndex = 57;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(6, 10);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(78, 14);
            this.label30.TabIndex = 56;
            this.label30.Text = "Container No";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal11
            // 
            this.TxtSeal11.EditValue = "";
            this.TxtSeal11.EnterMoveNextControl = true;
            this.TxtSeal11.Location = new System.Drawing.Point(366, 6);
            this.TxtSeal11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal11.Name = "TxtSeal11";
            this.TxtSeal11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal11.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal11.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal11.Size = new System.Drawing.Size(196, 20);
            this.TxtSeal11.TabIndex = 55;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(563, 9);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(35, 14);
            this.label32.TabIndex = 53;
            this.label32.Text = "Total";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal11
            // 
            this.TxtTotal11.EditValue = "";
            this.TxtTotal11.EnterMoveNextControl = true;
            this.TxtTotal11.Location = new System.Drawing.Point(600, 6);
            this.TxtTotal11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal11.Name = "TxtTotal11";
            this.TxtTotal11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal11.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal11.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal11.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal11.TabIndex = 54;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Red;
            this.label51.Location = new System.Drawing.Point(314, 9);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(48, 14);
            this.label51.TabIndex = 52;
            this.label51.Text = "Seal No";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.LueItCode12);
            this.tabPage12.Controls.Add(this.Grd22);
            this.tabPage12.Controls.Add(this.panel18);
            this.tabPage12.Location = new System.Drawing.Point(4, 23);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(881, 87);
            this.tabPage12.TabIndex = 11;
            this.tabPage12.Text = "No.12";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // LueItCode12
            // 
            this.LueItCode12.EnterMoveNextControl = true;
            this.LueItCode12.Location = new System.Drawing.Point(79, 59);
            this.LueItCode12.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode12.Name = "LueItCode12";
            this.LueItCode12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.Appearance.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode12.Properties.DropDownRows = 20;
            this.LueItCode12.Properties.NullText = "[Empty]";
            this.LueItCode12.Properties.PopupWidth = 500;
            this.LueItCode12.Size = new System.Drawing.Size(152, 20);
            this.LueItCode12.TabIndex = 54;
            this.LueItCode12.ToolTip = "F4 : Show/hide list";
            this.LueItCode12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd22
            // 
            this.Grd22.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd22.DefaultRow.Height = 20;
            this.Grd22.DefaultRow.Sortable = false;
            this.Grd22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd22.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd22.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd22.Header.Height = 21;
            this.Grd22.Location = new System.Drawing.Point(0, 38);
            this.Grd22.Name = "Grd22";
            this.Grd22.RowHeader.Visible = true;
            this.Grd22.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd22.SingleClickEdit = true;
            this.Grd22.Size = new System.Drawing.Size(881, 49);
            this.Grd22.TabIndex = 51;
            this.Grd22.TreeCol = null;
            this.Grd22.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel18.Controls.Add(this.label72);
            this.panel18.Controls.Add(this.TxtFreight12);
            this.panel18.Controls.Add(this.TxtCnt12);
            this.panel18.Controls.Add(this.label33);
            this.panel18.Controls.Add(this.TxtSeal12);
            this.panel18.Controls.Add(this.label34);
            this.panel18.Controls.Add(this.TxtTotal12);
            this.panel18.Controls.Add(this.label52);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(881, 38);
            this.panel18.TabIndex = 44;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Black;
            this.label72.Location = new System.Drawing.Point(696, 9);
            this.label72.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(84, 14);
            this.label72.TabIndex = 58;
            this.label72.Text = "Ocean Freight";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight12
            // 
            this.TxtFreight12.EditValue = "";
            this.TxtFreight12.EnterMoveNextControl = true;
            this.TxtFreight12.Location = new System.Drawing.Point(782, 6);
            this.TxtFreight12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight12.Name = "TxtFreight12";
            this.TxtFreight12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight12.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight12.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight12.TabIndex = 59;
            this.TxtFreight12.Validated += new System.EventHandler(this.TxtFreight12_Validated);
            // 
            // TxtCnt12
            // 
            this.TxtCnt12.EditValue = "";
            this.TxtCnt12.EnterMoveNextControl = true;
            this.TxtCnt12.Location = new System.Drawing.Point(85, 7);
            this.TxtCnt12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt12.Name = "TxtCnt12";
            this.TxtCnt12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt12.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt12.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt12.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt12.TabIndex = 57;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(5, 10);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(78, 14);
            this.label33.TabIndex = 56;
            this.label33.Text = "Container No";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal12
            // 
            this.TxtSeal12.EditValue = "";
            this.TxtSeal12.EnterMoveNextControl = true;
            this.TxtSeal12.Location = new System.Drawing.Point(365, 6);
            this.TxtSeal12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal12.Name = "TxtSeal12";
            this.TxtSeal12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal12.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal12.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal12.Size = new System.Drawing.Size(204, 20);
            this.TxtSeal12.TabIndex = 55;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(568, 9);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(35, 14);
            this.label34.TabIndex = 53;
            this.label34.Text = "Total";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal12
            // 
            this.TxtTotal12.EditValue = "";
            this.TxtTotal12.EnterMoveNextControl = true;
            this.TxtTotal12.Location = new System.Drawing.Point(605, 6);
            this.TxtTotal12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal12.Name = "TxtTotal12";
            this.TxtTotal12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal12.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal12.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal12.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal12.TabIndex = 54;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Red;
            this.label52.Location = new System.Drawing.Point(313, 9);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(48, 14);
            this.label52.TabIndex = 52;
            this.label52.Text = "Seal No";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.LueItCode13);
            this.tabPage13.Controls.Add(this.Grd23);
            this.tabPage13.Controls.Add(this.panel19);
            this.tabPage13.Location = new System.Drawing.Point(4, 23);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Size = new System.Drawing.Size(881, 87);
            this.tabPage13.TabIndex = 12;
            this.tabPage13.Text = "No.13";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // LueItCode13
            // 
            this.LueItCode13.EnterMoveNextControl = true;
            this.LueItCode13.Location = new System.Drawing.Point(70, 56);
            this.LueItCode13.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode13.Name = "LueItCode13";
            this.LueItCode13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.Appearance.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode13.Properties.DropDownRows = 20;
            this.LueItCode13.Properties.NullText = "[Empty]";
            this.LueItCode13.Properties.PopupWidth = 500;
            this.LueItCode13.Size = new System.Drawing.Size(152, 20);
            this.LueItCode13.TabIndex = 54;
            this.LueItCode13.ToolTip = "F4 : Show/hide list";
            this.LueItCode13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd23
            // 
            this.Grd23.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd23.DefaultRow.Height = 20;
            this.Grd23.DefaultRow.Sortable = false;
            this.Grd23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd23.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd23.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd23.Header.Height = 21;
            this.Grd23.Location = new System.Drawing.Point(0, 33);
            this.Grd23.Name = "Grd23";
            this.Grd23.RowHeader.Visible = true;
            this.Grd23.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd23.SingleClickEdit = true;
            this.Grd23.Size = new System.Drawing.Size(881, 54);
            this.Grd23.TabIndex = 51;
            this.Grd23.TreeCol = null;
            this.Grd23.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel19.Controls.Add(this.label73);
            this.panel19.Controls.Add(this.TxtFreight13);
            this.panel19.Controls.Add(this.TxtCnt13);
            this.panel19.Controls.Add(this.label35);
            this.panel19.Controls.Add(this.TxtSeal13);
            this.panel19.Controls.Add(this.label36);
            this.panel19.Controls.Add(this.TxtTotal13);
            this.panel19.Controls.Add(this.label53);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(881, 33);
            this.panel19.TabIndex = 44;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Black;
            this.label73.Location = new System.Drawing.Point(692, 8);
            this.label73.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(84, 14);
            this.label73.TabIndex = 58;
            this.label73.Text = "Ocean Freight";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight13
            // 
            this.TxtFreight13.EditValue = "";
            this.TxtFreight13.EnterMoveNextControl = true;
            this.TxtFreight13.Location = new System.Drawing.Point(777, 6);
            this.TxtFreight13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight13.Name = "TxtFreight13";
            this.TxtFreight13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight13.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight13.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight13.TabIndex = 59;
            this.TxtFreight13.Validated += new System.EventHandler(this.TxtFreight13_Validated);
            // 
            // TxtCnt13
            // 
            this.TxtCnt13.EditValue = "";
            this.TxtCnt13.EnterMoveNextControl = true;
            this.TxtCnt13.Location = new System.Drawing.Point(84, 7);
            this.TxtCnt13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt13.Name = "TxtCnt13";
            this.TxtCnt13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt13.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt13.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt13.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt13.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt13.TabIndex = 57;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Red;
            this.label35.Location = new System.Drawing.Point(4, 10);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(78, 14);
            this.label35.TabIndex = 56;
            this.label35.Text = "Container No";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal13
            // 
            this.TxtSeal13.EditValue = "";
            this.TxtSeal13.EnterMoveNextControl = true;
            this.TxtSeal13.Location = new System.Drawing.Point(364, 6);
            this.TxtSeal13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal13.Name = "TxtSeal13";
            this.TxtSeal13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal13.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal13.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal13.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal13.Size = new System.Drawing.Size(195, 20);
            this.TxtSeal13.TabIndex = 55;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(562, 9);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(35, 14);
            this.label36.TabIndex = 53;
            this.label36.Text = "Total";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal13
            // 
            this.TxtTotal13.EditValue = "";
            this.TxtTotal13.EnterMoveNextControl = true;
            this.TxtTotal13.Location = new System.Drawing.Point(600, 5);
            this.TxtTotal13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal13.Name = "TxtTotal13";
            this.TxtTotal13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal13.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal13.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal13.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal13.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal13.TabIndex = 54;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Red;
            this.label53.Location = new System.Drawing.Point(312, 9);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(48, 14);
            this.label53.TabIndex = 52;
            this.label53.Text = "Seal No";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.LueItCode14);
            this.tabPage14.Controls.Add(this.Grd24);
            this.tabPage14.Controls.Add(this.panel20);
            this.tabPage14.Location = new System.Drawing.Point(4, 23);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(881, 87);
            this.tabPage14.TabIndex = 13;
            this.tabPage14.Text = "No.14";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // LueItCode14
            // 
            this.LueItCode14.EnterMoveNextControl = true;
            this.LueItCode14.Location = new System.Drawing.Point(69, 55);
            this.LueItCode14.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode14.Name = "LueItCode14";
            this.LueItCode14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.Appearance.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode14.Properties.DropDownRows = 20;
            this.LueItCode14.Properties.NullText = "[Empty]";
            this.LueItCode14.Properties.PopupWidth = 500;
            this.LueItCode14.Size = new System.Drawing.Size(152, 20);
            this.LueItCode14.TabIndex = 54;
            this.LueItCode14.ToolTip = "F4 : Show/hide list";
            this.LueItCode14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd24
            // 
            this.Grd24.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd24.DefaultRow.Height = 20;
            this.Grd24.DefaultRow.Sortable = false;
            this.Grd24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd24.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd24.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd24.Header.Height = 21;
            this.Grd24.Location = new System.Drawing.Point(0, 33);
            this.Grd24.Name = "Grd24";
            this.Grd24.RowHeader.Visible = true;
            this.Grd24.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd24.SingleClickEdit = true;
            this.Grd24.Size = new System.Drawing.Size(881, 54);
            this.Grd24.TabIndex = 51;
            this.Grd24.TreeCol = null;
            this.Grd24.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel20.Controls.Add(this.label74);
            this.panel20.Controls.Add(this.TxtFreight14);
            this.panel20.Controls.Add(this.TxtCnt14);
            this.panel20.Controls.Add(this.label37);
            this.panel20.Controls.Add(this.TxtSeal14);
            this.panel20.Controls.Add(this.label38);
            this.panel20.Controls.Add(this.TxtTotal14);
            this.panel20.Controls.Add(this.label54);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(881, 33);
            this.panel20.TabIndex = 44;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(693, 8);
            this.label74.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(84, 14);
            this.label74.TabIndex = 58;
            this.label74.Text = "Ocean Freight";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight14
            // 
            this.TxtFreight14.EditValue = "";
            this.TxtFreight14.EnterMoveNextControl = true;
            this.TxtFreight14.Location = new System.Drawing.Point(779, 5);
            this.TxtFreight14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight14.Name = "TxtFreight14";
            this.TxtFreight14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight14.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight14.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight14.TabIndex = 59;
            this.TxtFreight14.Validated += new System.EventHandler(this.TxtFreight14_Validated);
            // 
            // TxtCnt14
            // 
            this.TxtCnt14.EditValue = "";
            this.TxtCnt14.EnterMoveNextControl = true;
            this.TxtCnt14.Location = new System.Drawing.Point(86, 7);
            this.TxtCnt14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt14.Name = "TxtCnt14";
            this.TxtCnt14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt14.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt14.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt14.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt14.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt14.TabIndex = 57;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(6, 10);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(78, 14);
            this.label37.TabIndex = 56;
            this.label37.Text = "Container No";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal14
            // 
            this.TxtSeal14.EditValue = "";
            this.TxtSeal14.EnterMoveNextControl = true;
            this.TxtSeal14.Location = new System.Drawing.Point(366, 6);
            this.TxtSeal14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal14.Name = "TxtSeal14";
            this.TxtSeal14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal14.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal14.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal14.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal14.Size = new System.Drawing.Size(195, 20);
            this.TxtSeal14.TabIndex = 55;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(564, 9);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(35, 14);
            this.label38.TabIndex = 53;
            this.label38.Text = "Total";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal14
            // 
            this.TxtTotal14.EditValue = "";
            this.TxtTotal14.EnterMoveNextControl = true;
            this.TxtTotal14.Location = new System.Drawing.Point(601, 6);
            this.TxtTotal14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal14.Name = "TxtTotal14";
            this.TxtTotal14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal14.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal14.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal14.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal14.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal14.TabIndex = 54;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Red;
            this.label54.Location = new System.Drawing.Point(314, 9);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(48, 14);
            this.label54.TabIndex = 52;
            this.label54.Text = "Seal No";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.LueItCode15);
            this.tabPage15.Controls.Add(this.Grd25);
            this.tabPage15.Controls.Add(this.panel21);
            this.tabPage15.Location = new System.Drawing.Point(4, 23);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(881, 87);
            this.tabPage15.TabIndex = 14;
            this.tabPage15.Text = "No.15";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // LueItCode15
            // 
            this.LueItCode15.EnterMoveNextControl = true;
            this.LueItCode15.Location = new System.Drawing.Point(73, 56);
            this.LueItCode15.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode15.Name = "LueItCode15";
            this.LueItCode15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.Appearance.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode15.Properties.DropDownRows = 20;
            this.LueItCode15.Properties.NullText = "[Empty]";
            this.LueItCode15.Properties.PopupWidth = 500;
            this.LueItCode15.Size = new System.Drawing.Size(152, 20);
            this.LueItCode15.TabIndex = 54;
            this.LueItCode15.ToolTip = "F4 : Show/hide list";
            this.LueItCode15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd25
            // 
            this.Grd25.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd25.DefaultRow.Height = 20;
            this.Grd25.DefaultRow.Sortable = false;
            this.Grd25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd25.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd25.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd25.Header.Height = 21;
            this.Grd25.Location = new System.Drawing.Point(0, 33);
            this.Grd25.Name = "Grd25";
            this.Grd25.RowHeader.Visible = true;
            this.Grd25.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd25.SingleClickEdit = true;
            this.Grd25.Size = new System.Drawing.Size(881, 54);
            this.Grd25.TabIndex = 51;
            this.Grd25.TreeCol = null;
            this.Grd25.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel21.Controls.Add(this.label75);
            this.panel21.Controls.Add(this.TxtFreight15);
            this.panel21.Controls.Add(this.TxtCnt15);
            this.panel21.Controls.Add(this.label39);
            this.panel21.Controls.Add(this.TxtSeal15);
            this.panel21.Controls.Add(this.label40);
            this.panel21.Controls.Add(this.TxtTotal15);
            this.panel21.Controls.Add(this.label55);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(881, 33);
            this.panel21.TabIndex = 44;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Black;
            this.label75.Location = new System.Drawing.Point(685, 9);
            this.label75.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(84, 14);
            this.label75.TabIndex = 58;
            this.label75.Text = "Ocean Freight";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight15
            // 
            this.TxtFreight15.EditValue = "";
            this.TxtFreight15.EnterMoveNextControl = true;
            this.TxtFreight15.Location = new System.Drawing.Point(771, 6);
            this.TxtFreight15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight15.Name = "TxtFreight15";
            this.TxtFreight15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight15.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight15.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight15.TabIndex = 59;
            this.TxtFreight15.Validated += new System.EventHandler(this.TxtFreight15_Validated);
            // 
            // TxtCnt15
            // 
            this.TxtCnt15.EditValue = "";
            this.TxtCnt15.EnterMoveNextControl = true;
            this.TxtCnt15.Location = new System.Drawing.Point(84, 7);
            this.TxtCnt15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt15.Name = "TxtCnt15";
            this.TxtCnt15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt15.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt15.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt15.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt15.TabIndex = 57;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Location = new System.Drawing.Point(4, 10);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(78, 14);
            this.label39.TabIndex = 56;
            this.label39.Text = "Container No";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal15
            // 
            this.TxtSeal15.EditValue = "";
            this.TxtSeal15.EnterMoveNextControl = true;
            this.TxtSeal15.Location = new System.Drawing.Point(364, 6);
            this.TxtSeal15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal15.Name = "TxtSeal15";
            this.TxtSeal15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal15.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal15.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal15.Size = new System.Drawing.Size(190, 20);
            this.TxtSeal15.TabIndex = 55;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(556, 9);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(35, 14);
            this.label40.TabIndex = 53;
            this.label40.Text = "Total";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal15
            // 
            this.TxtTotal15.EditValue = "";
            this.TxtTotal15.EnterMoveNextControl = true;
            this.TxtTotal15.Location = new System.Drawing.Point(594, 6);
            this.TxtTotal15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal15.Name = "TxtTotal15";
            this.TxtTotal15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal15.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal15.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal15.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal15.TabIndex = 54;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Red;
            this.label55.Location = new System.Drawing.Point(312, 9);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(48, 14);
            this.label55.TabIndex = 52;
            this.label55.Text = "Seal No";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this.LueItCode16);
            this.tabPage16.Controls.Add(this.Grd26);
            this.tabPage16.Controls.Add(this.panel3);
            this.tabPage16.Location = new System.Drawing.Point(4, 23);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage16.Size = new System.Drawing.Size(881, 87);
            this.tabPage16.TabIndex = 15;
            this.tabPage16.Text = "No.16";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // LueItCode16
            // 
            this.LueItCode16.EnterMoveNextControl = true;
            this.LueItCode16.Location = new System.Drawing.Point(195, 54);
            this.LueItCode16.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode16.Name = "LueItCode16";
            this.LueItCode16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.Appearance.Options.UseFont = true;
            this.LueItCode16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode16.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode16.Properties.DropDownRows = 20;
            this.LueItCode16.Properties.NullText = "[Empty]";
            this.LueItCode16.Properties.PopupWidth = 500;
            this.LueItCode16.Size = new System.Drawing.Size(152, 20);
            this.LueItCode16.TabIndex = 55;
            this.LueItCode16.ToolTip = "F4 : Show/hide list";
            this.LueItCode16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd26
            // 
            this.Grd26.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd26.DefaultRow.Height = 20;
            this.Grd26.DefaultRow.Sortable = false;
            this.Grd26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd26.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd26.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd26.Header.Height = 21;
            this.Grd26.Location = new System.Drawing.Point(3, 36);
            this.Grd26.Name = "Grd26";
            this.Grd26.RowHeader.Visible = true;
            this.Grd26.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd26.SingleClickEdit = true;
            this.Grd26.Size = new System.Drawing.Size(875, 48);
            this.Grd26.TabIndex = 52;
            this.Grd26.TreeCol = null;
            this.Grd26.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.label76);
            this.panel3.Controls.Add(this.TxtFreight16);
            this.panel3.Controls.Add(this.TxtCnt16);
            this.panel3.Controls.Add(this.label77);
            this.panel3.Controls.Add(this.TxtSeal16);
            this.panel3.Controls.Add(this.label78);
            this.panel3.Controls.Add(this.TxtTotal16);
            this.panel3.Controls.Add(this.label79);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(875, 33);
            this.panel3.TabIndex = 45;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Black;
            this.label76.Location = new System.Drawing.Point(685, 9);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(84, 14);
            this.label76.TabIndex = 58;
            this.label76.Text = "Ocean Freight";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight16
            // 
            this.TxtFreight16.EditValue = "";
            this.TxtFreight16.EnterMoveNextControl = true;
            this.TxtFreight16.Location = new System.Drawing.Point(771, 6);
            this.TxtFreight16.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight16.Name = "TxtFreight16";
            this.TxtFreight16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight16.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight16.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight16.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight16.TabIndex = 59;
            this.TxtFreight16.Validated += new System.EventHandler(this.TxtFreight16_Validated);
            // 
            // TxtCnt16
            // 
            this.TxtCnt16.EditValue = "";
            this.TxtCnt16.EnterMoveNextControl = true;
            this.TxtCnt16.Location = new System.Drawing.Point(84, 7);
            this.TxtCnt16.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt16.Name = "TxtCnt16";
            this.TxtCnt16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt16.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt16.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt16.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt16.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt16.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt16.TabIndex = 57;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Red;
            this.label77.Location = new System.Drawing.Point(4, 10);
            this.label77.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(78, 14);
            this.label77.TabIndex = 56;
            this.label77.Text = "Container No";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal16
            // 
            this.TxtSeal16.EditValue = "";
            this.TxtSeal16.EnterMoveNextControl = true;
            this.TxtSeal16.Location = new System.Drawing.Point(364, 6);
            this.TxtSeal16.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal16.Name = "TxtSeal16";
            this.TxtSeal16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal16.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal16.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal16.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal16.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal16.Size = new System.Drawing.Size(190, 20);
            this.TxtSeal16.TabIndex = 55;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Black;
            this.label78.Location = new System.Drawing.Point(556, 9);
            this.label78.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(35, 14);
            this.label78.TabIndex = 53;
            this.label78.Text = "Total";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal16
            // 
            this.TxtTotal16.EditValue = "";
            this.TxtTotal16.EnterMoveNextControl = true;
            this.TxtTotal16.Location = new System.Drawing.Point(594, 6);
            this.TxtTotal16.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal16.Name = "TxtTotal16";
            this.TxtTotal16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal16.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal16.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal16.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal16.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal16.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal16.TabIndex = 54;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Red;
            this.label79.Location = new System.Drawing.Point(312, 9);
            this.label79.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(48, 14);
            this.label79.TabIndex = 52;
            this.label79.Text = "Seal No";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage17
            // 
            this.tabPage17.Controls.Add(this.LueItCode17);
            this.tabPage17.Controls.Add(this.Grd27);
            this.tabPage17.Controls.Add(this.panel22);
            this.tabPage17.Location = new System.Drawing.Point(4, 23);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage17.Size = new System.Drawing.Size(881, 87);
            this.tabPage17.TabIndex = 16;
            this.tabPage17.Text = "No.17";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // LueItCode17
            // 
            this.LueItCode17.EnterMoveNextControl = true;
            this.LueItCode17.Location = new System.Drawing.Point(214, 57);
            this.LueItCode17.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode17.Name = "LueItCode17";
            this.LueItCode17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.Appearance.Options.UseFont = true;
            this.LueItCode17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode17.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode17.Properties.DropDownRows = 20;
            this.LueItCode17.Properties.NullText = "[Empty]";
            this.LueItCode17.Properties.PopupWidth = 500;
            this.LueItCode17.Size = new System.Drawing.Size(152, 20);
            this.LueItCode17.TabIndex = 55;
            this.LueItCode17.ToolTip = "F4 : Show/hide list";
            this.LueItCode17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd27
            // 
            this.Grd27.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd27.DefaultRow.Height = 20;
            this.Grd27.DefaultRow.Sortable = false;
            this.Grd27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd27.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd27.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd27.Header.Height = 21;
            this.Grd27.Location = new System.Drawing.Point(3, 36);
            this.Grd27.Name = "Grd27";
            this.Grd27.RowHeader.Visible = true;
            this.Grd27.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd27.SingleClickEdit = true;
            this.Grd27.Size = new System.Drawing.Size(875, 48);
            this.Grd27.TabIndex = 52;
            this.Grd27.TreeCol = null;
            this.Grd27.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel22.Controls.Add(this.label80);
            this.panel22.Controls.Add(this.TxtFreight17);
            this.panel22.Controls.Add(this.TxtCnt17);
            this.panel22.Controls.Add(this.label81);
            this.panel22.Controls.Add(this.TxtSeal17);
            this.panel22.Controls.Add(this.label82);
            this.panel22.Controls.Add(this.TxtTotal17);
            this.panel22.Controls.Add(this.label83);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(3, 3);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(875, 33);
            this.panel22.TabIndex = 45;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Black;
            this.label80.Location = new System.Drawing.Point(685, 9);
            this.label80.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(84, 14);
            this.label80.TabIndex = 58;
            this.label80.Text = "Ocean Freight";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight17
            // 
            this.TxtFreight17.EditValue = "";
            this.TxtFreight17.EnterMoveNextControl = true;
            this.TxtFreight17.Location = new System.Drawing.Point(771, 6);
            this.TxtFreight17.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight17.Name = "TxtFreight17";
            this.TxtFreight17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight17.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight17.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight17.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight17.TabIndex = 59;
            this.TxtFreight17.Validated += new System.EventHandler(this.TxtFreight17_Validated);
            // 
            // TxtCnt17
            // 
            this.TxtCnt17.EditValue = "";
            this.TxtCnt17.EnterMoveNextControl = true;
            this.TxtCnt17.Location = new System.Drawing.Point(84, 7);
            this.TxtCnt17.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt17.Name = "TxtCnt17";
            this.TxtCnt17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt17.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt17.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt17.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt17.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt17.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt17.TabIndex = 57;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Red;
            this.label81.Location = new System.Drawing.Point(4, 10);
            this.label81.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(78, 14);
            this.label81.TabIndex = 56;
            this.label81.Text = "Container No";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal17
            // 
            this.TxtSeal17.EditValue = "";
            this.TxtSeal17.EnterMoveNextControl = true;
            this.TxtSeal17.Location = new System.Drawing.Point(364, 6);
            this.TxtSeal17.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal17.Name = "TxtSeal17";
            this.TxtSeal17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal17.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal17.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal17.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal17.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal17.Size = new System.Drawing.Size(190, 20);
            this.TxtSeal17.TabIndex = 55;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Black;
            this.label82.Location = new System.Drawing.Point(556, 9);
            this.label82.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(35, 14);
            this.label82.TabIndex = 53;
            this.label82.Text = "Total";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal17
            // 
            this.TxtTotal17.EditValue = "";
            this.TxtTotal17.EnterMoveNextControl = true;
            this.TxtTotal17.Location = new System.Drawing.Point(594, 6);
            this.TxtTotal17.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal17.Name = "TxtTotal17";
            this.TxtTotal17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal17.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal17.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal17.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal17.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal17.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal17.TabIndex = 54;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Red;
            this.label83.Location = new System.Drawing.Point(312, 9);
            this.label83.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(48, 14);
            this.label83.TabIndex = 52;
            this.label83.Text = "Seal No";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage18
            // 
            this.tabPage18.Controls.Add(this.LueItCode18);
            this.tabPage18.Controls.Add(this.Grd28);
            this.tabPage18.Controls.Add(this.panel23);
            this.tabPage18.Location = new System.Drawing.Point(4, 23);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage18.Size = new System.Drawing.Size(881, 87);
            this.tabPage18.TabIndex = 17;
            this.tabPage18.Text = "No.18";
            this.tabPage18.UseVisualStyleBackColor = true;
            // 
            // LueItCode18
            // 
            this.LueItCode18.EnterMoveNextControl = true;
            this.LueItCode18.Location = new System.Drawing.Point(188, 59);
            this.LueItCode18.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode18.Name = "LueItCode18";
            this.LueItCode18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.Appearance.Options.UseFont = true;
            this.LueItCode18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode18.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode18.Properties.DropDownRows = 20;
            this.LueItCode18.Properties.NullText = "[Empty]";
            this.LueItCode18.Properties.PopupWidth = 500;
            this.LueItCode18.Size = new System.Drawing.Size(152, 20);
            this.LueItCode18.TabIndex = 55;
            this.LueItCode18.ToolTip = "F4 : Show/hide list";
            this.LueItCode18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd28
            // 
            this.Grd28.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd28.DefaultRow.Height = 20;
            this.Grd28.DefaultRow.Sortable = false;
            this.Grd28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd28.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd28.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd28.Header.Height = 21;
            this.Grd28.Location = new System.Drawing.Point(3, 36);
            this.Grd28.Name = "Grd28";
            this.Grd28.RowHeader.Visible = true;
            this.Grd28.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd28.SingleClickEdit = true;
            this.Grd28.Size = new System.Drawing.Size(875, 48);
            this.Grd28.TabIndex = 52;
            this.Grd28.TreeCol = null;
            this.Grd28.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel23.Controls.Add(this.label84);
            this.panel23.Controls.Add(this.TxtFreight18);
            this.panel23.Controls.Add(this.TxtCnt18);
            this.panel23.Controls.Add(this.label85);
            this.panel23.Controls.Add(this.TxtSeal18);
            this.panel23.Controls.Add(this.label86);
            this.panel23.Controls.Add(this.TxtTotal18);
            this.panel23.Controls.Add(this.label87);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(3, 3);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(875, 33);
            this.panel23.TabIndex = 45;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Black;
            this.label84.Location = new System.Drawing.Point(685, 9);
            this.label84.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(84, 14);
            this.label84.TabIndex = 58;
            this.label84.Text = "Ocean Freight";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight18
            // 
            this.TxtFreight18.EditValue = "";
            this.TxtFreight18.EnterMoveNextControl = true;
            this.TxtFreight18.Location = new System.Drawing.Point(771, 6);
            this.TxtFreight18.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight18.Name = "TxtFreight18";
            this.TxtFreight18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight18.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight18.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight18.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight18.TabIndex = 59;
            this.TxtFreight18.Validated += new System.EventHandler(this.TxtFreight18_Validated);
            // 
            // TxtCnt18
            // 
            this.TxtCnt18.EditValue = "";
            this.TxtCnt18.EnterMoveNextControl = true;
            this.TxtCnt18.Location = new System.Drawing.Point(84, 7);
            this.TxtCnt18.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt18.Name = "TxtCnt18";
            this.TxtCnt18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt18.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt18.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt18.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt18.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt18.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt18.TabIndex = 57;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Red;
            this.label85.Location = new System.Drawing.Point(4, 10);
            this.label85.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(78, 14);
            this.label85.TabIndex = 56;
            this.label85.Text = "Container No";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal18
            // 
            this.TxtSeal18.EditValue = "";
            this.TxtSeal18.EnterMoveNextControl = true;
            this.TxtSeal18.Location = new System.Drawing.Point(364, 6);
            this.TxtSeal18.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal18.Name = "TxtSeal18";
            this.TxtSeal18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal18.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal18.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal18.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal18.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal18.Size = new System.Drawing.Size(190, 20);
            this.TxtSeal18.TabIndex = 55;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Black;
            this.label86.Location = new System.Drawing.Point(556, 9);
            this.label86.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(35, 14);
            this.label86.TabIndex = 53;
            this.label86.Text = "Total";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal18
            // 
            this.TxtTotal18.EditValue = "";
            this.TxtTotal18.EnterMoveNextControl = true;
            this.TxtTotal18.Location = new System.Drawing.Point(594, 6);
            this.TxtTotal18.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal18.Name = "TxtTotal18";
            this.TxtTotal18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal18.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal18.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal18.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal18.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal18.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal18.TabIndex = 54;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Red;
            this.label87.Location = new System.Drawing.Point(312, 9);
            this.label87.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(48, 14);
            this.label87.TabIndex = 52;
            this.label87.Text = "Seal No";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage19
            // 
            this.tabPage19.Controls.Add(this.LueItCode19);
            this.tabPage19.Controls.Add(this.Grd29);
            this.tabPage19.Controls.Add(this.panel24);
            this.tabPage19.Location = new System.Drawing.Point(4, 23);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage19.Size = new System.Drawing.Size(881, 87);
            this.tabPage19.TabIndex = 18;
            this.tabPage19.Text = "No.19";
            this.tabPage19.UseVisualStyleBackColor = true;
            // 
            // LueItCode19
            // 
            this.LueItCode19.EnterMoveNextControl = true;
            this.LueItCode19.Location = new System.Drawing.Point(231, 59);
            this.LueItCode19.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode19.Name = "LueItCode19";
            this.LueItCode19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.Appearance.Options.UseFont = true;
            this.LueItCode19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode19.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode19.Properties.DropDownRows = 20;
            this.LueItCode19.Properties.NullText = "[Empty]";
            this.LueItCode19.Properties.PopupWidth = 500;
            this.LueItCode19.Size = new System.Drawing.Size(152, 20);
            this.LueItCode19.TabIndex = 55;
            this.LueItCode19.ToolTip = "F4 : Show/hide list";
            this.LueItCode19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd29
            // 
            this.Grd29.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd29.DefaultRow.Height = 20;
            this.Grd29.DefaultRow.Sortable = false;
            this.Grd29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd29.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd29.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd29.Header.Height = 21;
            this.Grd29.Location = new System.Drawing.Point(3, 36);
            this.Grd29.Name = "Grd29";
            this.Grd29.RowHeader.Visible = true;
            this.Grd29.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd29.SingleClickEdit = true;
            this.Grd29.Size = new System.Drawing.Size(875, 48);
            this.Grd29.TabIndex = 52;
            this.Grd29.TreeCol = null;
            this.Grd29.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel24.Controls.Add(this.label88);
            this.panel24.Controls.Add(this.TxtFreight19);
            this.panel24.Controls.Add(this.TxtCnt19);
            this.panel24.Controls.Add(this.label89);
            this.panel24.Controls.Add(this.TxtSeal19);
            this.panel24.Controls.Add(this.label90);
            this.panel24.Controls.Add(this.TxtTotal19);
            this.panel24.Controls.Add(this.label91);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(3, 3);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(875, 33);
            this.panel24.TabIndex = 45;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Black;
            this.label88.Location = new System.Drawing.Point(685, 9);
            this.label88.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(84, 14);
            this.label88.TabIndex = 58;
            this.label88.Text = "Ocean Freight";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight19
            // 
            this.TxtFreight19.EditValue = "";
            this.TxtFreight19.EnterMoveNextControl = true;
            this.TxtFreight19.Location = new System.Drawing.Point(771, 6);
            this.TxtFreight19.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight19.Name = "TxtFreight19";
            this.TxtFreight19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight19.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight19.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight19.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight19.TabIndex = 59;
            this.TxtFreight19.Validated += new System.EventHandler(this.TxtFreight19_Validated);
            // 
            // TxtCnt19
            // 
            this.TxtCnt19.EditValue = "";
            this.TxtCnt19.EnterMoveNextControl = true;
            this.TxtCnt19.Location = new System.Drawing.Point(84, 7);
            this.TxtCnt19.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt19.Name = "TxtCnt19";
            this.TxtCnt19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt19.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt19.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt19.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt19.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt19.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt19.TabIndex = 57;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Red;
            this.label89.Location = new System.Drawing.Point(4, 10);
            this.label89.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(78, 14);
            this.label89.TabIndex = 56;
            this.label89.Text = "Container No";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal19
            // 
            this.TxtSeal19.EditValue = "";
            this.TxtSeal19.EnterMoveNextControl = true;
            this.TxtSeal19.Location = new System.Drawing.Point(364, 6);
            this.TxtSeal19.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal19.Name = "TxtSeal19";
            this.TxtSeal19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal19.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal19.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal19.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal19.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal19.Size = new System.Drawing.Size(190, 20);
            this.TxtSeal19.TabIndex = 55;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Black;
            this.label90.Location = new System.Drawing.Point(556, 9);
            this.label90.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(35, 14);
            this.label90.TabIndex = 53;
            this.label90.Text = "Total";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal19
            // 
            this.TxtTotal19.EditValue = "";
            this.TxtTotal19.EnterMoveNextControl = true;
            this.TxtTotal19.Location = new System.Drawing.Point(594, 6);
            this.TxtTotal19.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal19.Name = "TxtTotal19";
            this.TxtTotal19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal19.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal19.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal19.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal19.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal19.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal19.TabIndex = 54;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.Red;
            this.label91.Location = new System.Drawing.Point(312, 9);
            this.label91.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(48, 14);
            this.label91.TabIndex = 52;
            this.label91.Text = "Seal No";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage20
            // 
            this.tabPage20.Controls.Add(this.LueItCode20);
            this.tabPage20.Controls.Add(this.Grd30);
            this.tabPage20.Controls.Add(this.panel25);
            this.tabPage20.Location = new System.Drawing.Point(4, 23);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage20.Size = new System.Drawing.Size(881, 87);
            this.tabPage20.TabIndex = 19;
            this.tabPage20.Text = "No.20";
            this.tabPage20.UseVisualStyleBackColor = true;
            // 
            // LueItCode20
            // 
            this.LueItCode20.EnterMoveNextControl = true;
            this.LueItCode20.Location = new System.Drawing.Point(218, 59);
            this.LueItCode20.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode20.Name = "LueItCode20";
            this.LueItCode20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.Appearance.Options.UseFont = true;
            this.LueItCode20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode20.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode20.Properties.DropDownRows = 20;
            this.LueItCode20.Properties.NullText = "[Empty]";
            this.LueItCode20.Properties.PopupWidth = 500;
            this.LueItCode20.Size = new System.Drawing.Size(152, 20);
            this.LueItCode20.TabIndex = 55;
            this.LueItCode20.ToolTip = "F4 : Show/hide list";
            this.LueItCode20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd30
            // 
            this.Grd30.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd30.DefaultRow.Height = 20;
            this.Grd30.DefaultRow.Sortable = false;
            this.Grd30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd30.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd30.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd30.Header.Height = 21;
            this.Grd30.Location = new System.Drawing.Point(3, 36);
            this.Grd30.Name = "Grd30";
            this.Grd30.RowHeader.Visible = true;
            this.Grd30.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd30.SingleClickEdit = true;
            this.Grd30.Size = new System.Drawing.Size(875, 48);
            this.Grd30.TabIndex = 52;
            this.Grd30.TreeCol = null;
            this.Grd30.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel25.Controls.Add(this.label92);
            this.panel25.Controls.Add(this.TxtFreight20);
            this.panel25.Controls.Add(this.TxtCnt20);
            this.panel25.Controls.Add(this.label93);
            this.panel25.Controls.Add(this.TxtSeal20);
            this.panel25.Controls.Add(this.label94);
            this.panel25.Controls.Add(this.TxtTotal20);
            this.panel25.Controls.Add(this.label95);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel25.Location = new System.Drawing.Point(3, 3);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(875, 33);
            this.panel25.TabIndex = 45;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.ForeColor = System.Drawing.Color.Black;
            this.label92.Location = new System.Drawing.Point(685, 9);
            this.label92.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(84, 14);
            this.label92.TabIndex = 58;
            this.label92.Text = "Ocean Freight";
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight20
            // 
            this.TxtFreight20.EditValue = "";
            this.TxtFreight20.EnterMoveNextControl = true;
            this.TxtFreight20.Location = new System.Drawing.Point(771, 6);
            this.TxtFreight20.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight20.Name = "TxtFreight20";
            this.TxtFreight20.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight20.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight20.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight20.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight20.TabIndex = 59;
            this.TxtFreight20.Validated += new System.EventHandler(this.TxtFreight20_Validated);
            // 
            // TxtCnt20
            // 
            this.TxtCnt20.EditValue = "";
            this.TxtCnt20.EnterMoveNextControl = true;
            this.TxtCnt20.Location = new System.Drawing.Point(84, 7);
            this.TxtCnt20.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt20.Name = "TxtCnt20";
            this.TxtCnt20.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt20.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt20.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt20.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt20.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt20.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt20.TabIndex = 57;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.ForeColor = System.Drawing.Color.Red;
            this.label93.Location = new System.Drawing.Point(4, 10);
            this.label93.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(78, 14);
            this.label93.TabIndex = 56;
            this.label93.Text = "Container No";
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal20
            // 
            this.TxtSeal20.EditValue = "";
            this.TxtSeal20.EnterMoveNextControl = true;
            this.TxtSeal20.Location = new System.Drawing.Point(364, 6);
            this.TxtSeal20.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal20.Name = "TxtSeal20";
            this.TxtSeal20.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal20.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal20.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal20.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal20.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal20.Size = new System.Drawing.Size(190, 20);
            this.TxtSeal20.TabIndex = 55;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Black;
            this.label94.Location = new System.Drawing.Point(556, 9);
            this.label94.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(35, 14);
            this.label94.TabIndex = 53;
            this.label94.Text = "Total";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal20
            // 
            this.TxtTotal20.EditValue = "";
            this.TxtTotal20.EnterMoveNextControl = true;
            this.TxtTotal20.Location = new System.Drawing.Point(594, 6);
            this.TxtTotal20.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal20.Name = "TxtTotal20";
            this.TxtTotal20.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal20.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal20.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal20.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal20.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal20.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal20.TabIndex = 54;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.Red;
            this.label95.Location = new System.Drawing.Point(312, 9);
            this.label95.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(48, 14);
            this.label95.TabIndex = 52;
            this.label95.Text = "Seal No";
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage21
            // 
            this.tabPage21.Controls.Add(this.LueItCode21);
            this.tabPage21.Controls.Add(this.Grd31);
            this.tabPage21.Controls.Add(this.panel26);
            this.tabPage21.Location = new System.Drawing.Point(4, 23);
            this.tabPage21.Name = "tabPage21";
            this.tabPage21.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage21.Size = new System.Drawing.Size(881, 87);
            this.tabPage21.TabIndex = 20;
            this.tabPage21.Text = "No.21";
            this.tabPage21.UseVisualStyleBackColor = true;
            // 
            // LueItCode21
            // 
            this.LueItCode21.EnterMoveNextControl = true;
            this.LueItCode21.Location = new System.Drawing.Point(269, 54);
            this.LueItCode21.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode21.Name = "LueItCode21";
            this.LueItCode21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.Appearance.Options.UseFont = true;
            this.LueItCode21.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode21.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode21.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode21.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode21.Properties.DropDownRows = 20;
            this.LueItCode21.Properties.NullText = "[Empty]";
            this.LueItCode21.Properties.PopupWidth = 500;
            this.LueItCode21.Size = new System.Drawing.Size(152, 20);
            this.LueItCode21.TabIndex = 55;
            this.LueItCode21.ToolTip = "F4 : Show/hide list";
            this.LueItCode21.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd31
            // 
            this.Grd31.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd31.DefaultRow.Height = 20;
            this.Grd31.DefaultRow.Sortable = false;
            this.Grd31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd31.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd31.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd31.Header.Height = 21;
            this.Grd31.Location = new System.Drawing.Point(3, 36);
            this.Grd31.Name = "Grd31";
            this.Grd31.RowHeader.Visible = true;
            this.Grd31.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd31.SingleClickEdit = true;
            this.Grd31.Size = new System.Drawing.Size(875, 48);
            this.Grd31.TabIndex = 52;
            this.Grd31.TreeCol = null;
            this.Grd31.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel26.Controls.Add(this.label96);
            this.panel26.Controls.Add(this.TxtFreight21);
            this.panel26.Controls.Add(this.TxtCnt21);
            this.panel26.Controls.Add(this.label97);
            this.panel26.Controls.Add(this.TxtSeal21);
            this.panel26.Controls.Add(this.label98);
            this.panel26.Controls.Add(this.TxtTotal21);
            this.panel26.Controls.Add(this.label99);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel26.Location = new System.Drawing.Point(3, 3);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(875, 33);
            this.panel26.TabIndex = 45;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.Black;
            this.label96.Location = new System.Drawing.Point(685, 9);
            this.label96.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(84, 14);
            this.label96.TabIndex = 58;
            this.label96.Text = "Ocean Freight";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight21
            // 
            this.TxtFreight21.EditValue = "";
            this.TxtFreight21.EnterMoveNextControl = true;
            this.TxtFreight21.Location = new System.Drawing.Point(771, 6);
            this.TxtFreight21.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight21.Name = "TxtFreight21";
            this.TxtFreight21.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight21.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight21.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight21.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight21.TabIndex = 59;
            this.TxtFreight21.Validated += new System.EventHandler(this.TxtFreight21_Validated);
            // 
            // TxtCnt21
            // 
            this.TxtCnt21.EditValue = "";
            this.TxtCnt21.EnterMoveNextControl = true;
            this.TxtCnt21.Location = new System.Drawing.Point(84, 7);
            this.TxtCnt21.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt21.Name = "TxtCnt21";
            this.TxtCnt21.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt21.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt21.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt21.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt21.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt21.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt21.TabIndex = 57;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.Red;
            this.label97.Location = new System.Drawing.Point(4, 10);
            this.label97.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(78, 14);
            this.label97.TabIndex = 56;
            this.label97.Text = "Container No";
            this.label97.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal21
            // 
            this.TxtSeal21.EditValue = "";
            this.TxtSeal21.EnterMoveNextControl = true;
            this.TxtSeal21.Location = new System.Drawing.Point(364, 6);
            this.TxtSeal21.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal21.Name = "TxtSeal21";
            this.TxtSeal21.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal21.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal21.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal21.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal21.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal21.Size = new System.Drawing.Size(190, 20);
            this.TxtSeal21.TabIndex = 55;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.Black;
            this.label98.Location = new System.Drawing.Point(556, 9);
            this.label98.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(35, 14);
            this.label98.TabIndex = 53;
            this.label98.Text = "Total";
            this.label98.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal21
            // 
            this.TxtTotal21.EditValue = "";
            this.TxtTotal21.EnterMoveNextControl = true;
            this.TxtTotal21.Location = new System.Drawing.Point(594, 6);
            this.TxtTotal21.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal21.Name = "TxtTotal21";
            this.TxtTotal21.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal21.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal21.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal21.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal21.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal21.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal21.TabIndex = 54;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.Red;
            this.label99.Location = new System.Drawing.Point(312, 9);
            this.label99.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(48, 14);
            this.label99.TabIndex = 52;
            this.label99.Text = "Seal No";
            this.label99.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage22
            // 
            this.tabPage22.Controls.Add(this.LueItCode22);
            this.tabPage22.Controls.Add(this.Grd32);
            this.tabPage22.Controls.Add(this.panel27);
            this.tabPage22.Location = new System.Drawing.Point(4, 23);
            this.tabPage22.Name = "tabPage22";
            this.tabPage22.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage22.Size = new System.Drawing.Size(881, 87);
            this.tabPage22.TabIndex = 21;
            this.tabPage22.Text = "No.22";
            this.tabPage22.UseVisualStyleBackColor = true;
            // 
            // LueItCode22
            // 
            this.LueItCode22.EnterMoveNextControl = true;
            this.LueItCode22.Location = new System.Drawing.Point(237, 61);
            this.LueItCode22.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode22.Name = "LueItCode22";
            this.LueItCode22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.Appearance.Options.UseFont = true;
            this.LueItCode22.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode22.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode22.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode22.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode22.Properties.DropDownRows = 20;
            this.LueItCode22.Properties.NullText = "[Empty]";
            this.LueItCode22.Properties.PopupWidth = 500;
            this.LueItCode22.Size = new System.Drawing.Size(152, 20);
            this.LueItCode22.TabIndex = 55;
            this.LueItCode22.ToolTip = "F4 : Show/hide list";
            this.LueItCode22.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd32
            // 
            this.Grd32.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd32.DefaultRow.Height = 20;
            this.Grd32.DefaultRow.Sortable = false;
            this.Grd32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd32.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd32.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd32.Header.Height = 21;
            this.Grd32.Location = new System.Drawing.Point(3, 36);
            this.Grd32.Name = "Grd32";
            this.Grd32.RowHeader.Visible = true;
            this.Grd32.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd32.SingleClickEdit = true;
            this.Grd32.Size = new System.Drawing.Size(875, 48);
            this.Grd32.TabIndex = 52;
            this.Grd32.TreeCol = null;
            this.Grd32.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel27.Controls.Add(this.label100);
            this.panel27.Controls.Add(this.TxtFreight22);
            this.panel27.Controls.Add(this.TxtCnt22);
            this.panel27.Controls.Add(this.label101);
            this.panel27.Controls.Add(this.TxtSeal22);
            this.panel27.Controls.Add(this.label102);
            this.panel27.Controls.Add(this.TxtTotal22);
            this.panel27.Controls.Add(this.label103);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel27.Location = new System.Drawing.Point(3, 3);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(875, 33);
            this.panel27.TabIndex = 45;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Black;
            this.label100.Location = new System.Drawing.Point(685, 9);
            this.label100.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(84, 14);
            this.label100.TabIndex = 58;
            this.label100.Text = "Ocean Freight";
            this.label100.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight22
            // 
            this.TxtFreight22.EditValue = "";
            this.TxtFreight22.EnterMoveNextControl = true;
            this.TxtFreight22.Location = new System.Drawing.Point(771, 6);
            this.TxtFreight22.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight22.Name = "TxtFreight22";
            this.TxtFreight22.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight22.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight22.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight22.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight22.TabIndex = 59;
            this.TxtFreight22.Validated += new System.EventHandler(this.TxtFreight22_Validated);
            // 
            // TxtCnt22
            // 
            this.TxtCnt22.EditValue = "";
            this.TxtCnt22.EnterMoveNextControl = true;
            this.TxtCnt22.Location = new System.Drawing.Point(84, 7);
            this.TxtCnt22.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt22.Name = "TxtCnt22";
            this.TxtCnt22.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt22.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt22.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt22.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt22.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt22.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt22.TabIndex = 57;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Red;
            this.label101.Location = new System.Drawing.Point(4, 10);
            this.label101.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(78, 14);
            this.label101.TabIndex = 56;
            this.label101.Text = "Container No";
            this.label101.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal22
            // 
            this.TxtSeal22.EditValue = "";
            this.TxtSeal22.EnterMoveNextControl = true;
            this.TxtSeal22.Location = new System.Drawing.Point(364, 6);
            this.TxtSeal22.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal22.Name = "TxtSeal22";
            this.TxtSeal22.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal22.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal22.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal22.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal22.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal22.Size = new System.Drawing.Size(190, 20);
            this.TxtSeal22.TabIndex = 55;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Black;
            this.label102.Location = new System.Drawing.Point(556, 9);
            this.label102.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(35, 14);
            this.label102.TabIndex = 53;
            this.label102.Text = "Total";
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal22
            // 
            this.TxtTotal22.EditValue = "";
            this.TxtTotal22.EnterMoveNextControl = true;
            this.TxtTotal22.Location = new System.Drawing.Point(594, 6);
            this.TxtTotal22.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal22.Name = "TxtTotal22";
            this.TxtTotal22.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal22.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal22.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal22.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal22.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal22.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal22.TabIndex = 54;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.ForeColor = System.Drawing.Color.Red;
            this.label103.Location = new System.Drawing.Point(312, 9);
            this.label103.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(48, 14);
            this.label103.TabIndex = 52;
            this.label103.Text = "Seal No";
            this.label103.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage23
            // 
            this.tabPage23.Controls.Add(this.LueItCode23);
            this.tabPage23.Controls.Add(this.Grd33);
            this.tabPage23.Controls.Add(this.panel28);
            this.tabPage23.Location = new System.Drawing.Point(4, 23);
            this.tabPage23.Name = "tabPage23";
            this.tabPage23.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage23.Size = new System.Drawing.Size(881, 87);
            this.tabPage23.TabIndex = 22;
            this.tabPage23.Text = "No.23";
            this.tabPage23.UseVisualStyleBackColor = true;
            // 
            // LueItCode23
            // 
            this.LueItCode23.EnterMoveNextControl = true;
            this.LueItCode23.Location = new System.Drawing.Point(315, 56);
            this.LueItCode23.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode23.Name = "LueItCode23";
            this.LueItCode23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.Appearance.Options.UseFont = true;
            this.LueItCode23.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode23.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode23.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode23.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode23.Properties.DropDownRows = 20;
            this.LueItCode23.Properties.NullText = "[Empty]";
            this.LueItCode23.Properties.PopupWidth = 500;
            this.LueItCode23.Size = new System.Drawing.Size(152, 20);
            this.LueItCode23.TabIndex = 55;
            this.LueItCode23.ToolTip = "F4 : Show/hide list";
            this.LueItCode23.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd33
            // 
            this.Grd33.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd33.DefaultRow.Height = 20;
            this.Grd33.DefaultRow.Sortable = false;
            this.Grd33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd33.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd33.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd33.Header.Height = 21;
            this.Grd33.Location = new System.Drawing.Point(3, 36);
            this.Grd33.Name = "Grd33";
            this.Grd33.RowHeader.Visible = true;
            this.Grd33.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd33.SingleClickEdit = true;
            this.Grd33.Size = new System.Drawing.Size(875, 48);
            this.Grd33.TabIndex = 52;
            this.Grd33.TreeCol = null;
            this.Grd33.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel28.Controls.Add(this.label104);
            this.panel28.Controls.Add(this.TxtFreight23);
            this.panel28.Controls.Add(this.TxtCnt23);
            this.panel28.Controls.Add(this.label105);
            this.panel28.Controls.Add(this.TxtSeal23);
            this.panel28.Controls.Add(this.label106);
            this.panel28.Controls.Add(this.TxtTotal23);
            this.panel28.Controls.Add(this.label107);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel28.Location = new System.Drawing.Point(3, 3);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(875, 33);
            this.panel28.TabIndex = 45;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.Black;
            this.label104.Location = new System.Drawing.Point(685, 9);
            this.label104.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(84, 14);
            this.label104.TabIndex = 58;
            this.label104.Text = "Ocean Freight";
            this.label104.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight23
            // 
            this.TxtFreight23.EditValue = "";
            this.TxtFreight23.EnterMoveNextControl = true;
            this.TxtFreight23.Location = new System.Drawing.Point(771, 6);
            this.TxtFreight23.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight23.Name = "TxtFreight23";
            this.TxtFreight23.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight23.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight23.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight23.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight23.TabIndex = 59;
            this.TxtFreight23.Validated += new System.EventHandler(this.TxtFreight23_Validated);
            // 
            // TxtCnt23
            // 
            this.TxtCnt23.EditValue = "";
            this.TxtCnt23.EnterMoveNextControl = true;
            this.TxtCnt23.Location = new System.Drawing.Point(84, 7);
            this.TxtCnt23.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt23.Name = "TxtCnt23";
            this.TxtCnt23.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt23.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt23.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt23.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt23.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt23.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt23.TabIndex = 57;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Red;
            this.label105.Location = new System.Drawing.Point(4, 10);
            this.label105.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(78, 14);
            this.label105.TabIndex = 56;
            this.label105.Text = "Container No";
            this.label105.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal23
            // 
            this.TxtSeal23.EditValue = "";
            this.TxtSeal23.EnterMoveNextControl = true;
            this.TxtSeal23.Location = new System.Drawing.Point(364, 6);
            this.TxtSeal23.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal23.Name = "TxtSeal23";
            this.TxtSeal23.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal23.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal23.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal23.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal23.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal23.Size = new System.Drawing.Size(190, 20);
            this.TxtSeal23.TabIndex = 55;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.Black;
            this.label106.Location = new System.Drawing.Point(556, 9);
            this.label106.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(35, 14);
            this.label106.TabIndex = 53;
            this.label106.Text = "Total";
            this.label106.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal23
            // 
            this.TxtTotal23.EditValue = "";
            this.TxtTotal23.EnterMoveNextControl = true;
            this.TxtTotal23.Location = new System.Drawing.Point(594, 6);
            this.TxtTotal23.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal23.Name = "TxtTotal23";
            this.TxtTotal23.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal23.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal23.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal23.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal23.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal23.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal23.TabIndex = 54;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.Red;
            this.label107.Location = new System.Drawing.Point(312, 9);
            this.label107.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(48, 14);
            this.label107.TabIndex = 52;
            this.label107.Text = "Seal No";
            this.label107.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage24
            // 
            this.tabPage24.Controls.Add(this.LueItCode24);
            this.tabPage24.Controls.Add(this.Grd34);
            this.tabPage24.Controls.Add(this.panel29);
            this.tabPage24.Location = new System.Drawing.Point(4, 23);
            this.tabPage24.Name = "tabPage24";
            this.tabPage24.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage24.Size = new System.Drawing.Size(881, 87);
            this.tabPage24.TabIndex = 23;
            this.tabPage24.Text = "No.24";
            this.tabPage24.UseVisualStyleBackColor = true;
            // 
            // LueItCode24
            // 
            this.LueItCode24.EnterMoveNextControl = true;
            this.LueItCode24.Location = new System.Drawing.Point(315, 55);
            this.LueItCode24.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode24.Name = "LueItCode24";
            this.LueItCode24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.Appearance.Options.UseFont = true;
            this.LueItCode24.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode24.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode24.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode24.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode24.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode24.Properties.DropDownRows = 20;
            this.LueItCode24.Properties.NullText = "[Empty]";
            this.LueItCode24.Properties.PopupWidth = 500;
            this.LueItCode24.Size = new System.Drawing.Size(152, 20);
            this.LueItCode24.TabIndex = 55;
            this.LueItCode24.ToolTip = "F4 : Show/hide list";
            this.LueItCode24.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd34
            // 
            this.Grd34.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd34.DefaultRow.Height = 20;
            this.Grd34.DefaultRow.Sortable = false;
            this.Grd34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd34.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd34.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd34.Header.Height = 21;
            this.Grd34.Location = new System.Drawing.Point(3, 36);
            this.Grd34.Name = "Grd34";
            this.Grd34.RowHeader.Visible = true;
            this.Grd34.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd34.SingleClickEdit = true;
            this.Grd34.Size = new System.Drawing.Size(875, 48);
            this.Grd34.TabIndex = 52;
            this.Grd34.TreeCol = null;
            this.Grd34.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel29.Controls.Add(this.label108);
            this.panel29.Controls.Add(this.TxtFreight24);
            this.panel29.Controls.Add(this.TxtCnt24);
            this.panel29.Controls.Add(this.label109);
            this.panel29.Controls.Add(this.TxtSeal24);
            this.panel29.Controls.Add(this.label110);
            this.panel29.Controls.Add(this.TxtTotal24);
            this.panel29.Controls.Add(this.label111);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel29.Location = new System.Drawing.Point(3, 3);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(875, 33);
            this.panel29.TabIndex = 45;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.ForeColor = System.Drawing.Color.Black;
            this.label108.Location = new System.Drawing.Point(685, 9);
            this.label108.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(84, 14);
            this.label108.TabIndex = 58;
            this.label108.Text = "Ocean Freight";
            this.label108.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight24
            // 
            this.TxtFreight24.EditValue = "";
            this.TxtFreight24.EnterMoveNextControl = true;
            this.TxtFreight24.Location = new System.Drawing.Point(771, 6);
            this.TxtFreight24.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight24.Name = "TxtFreight24";
            this.TxtFreight24.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight24.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight24.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight24.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight24.TabIndex = 59;
            this.TxtFreight24.Validated += new System.EventHandler(this.TxtFreight24_Validated);
            // 
            // TxtCnt24
            // 
            this.TxtCnt24.EditValue = "";
            this.TxtCnt24.EnterMoveNextControl = true;
            this.TxtCnt24.Location = new System.Drawing.Point(84, 7);
            this.TxtCnt24.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt24.Name = "TxtCnt24";
            this.TxtCnt24.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt24.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt24.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt24.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt24.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt24.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt24.TabIndex = 57;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.ForeColor = System.Drawing.Color.Red;
            this.label109.Location = new System.Drawing.Point(4, 10);
            this.label109.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(78, 14);
            this.label109.TabIndex = 56;
            this.label109.Text = "Container No";
            this.label109.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal24
            // 
            this.TxtSeal24.EditValue = "";
            this.TxtSeal24.EnterMoveNextControl = true;
            this.TxtSeal24.Location = new System.Drawing.Point(364, 6);
            this.TxtSeal24.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal24.Name = "TxtSeal24";
            this.TxtSeal24.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal24.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal24.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal24.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal24.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal24.Size = new System.Drawing.Size(190, 20);
            this.TxtSeal24.TabIndex = 55;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.ForeColor = System.Drawing.Color.Black;
            this.label110.Location = new System.Drawing.Point(556, 9);
            this.label110.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(35, 14);
            this.label110.TabIndex = 53;
            this.label110.Text = "Total";
            this.label110.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal24
            // 
            this.TxtTotal24.EditValue = "";
            this.TxtTotal24.EnterMoveNextControl = true;
            this.TxtTotal24.Location = new System.Drawing.Point(594, 6);
            this.TxtTotal24.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal24.Name = "TxtTotal24";
            this.TxtTotal24.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal24.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal24.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal24.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal24.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal24.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal24.TabIndex = 54;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.ForeColor = System.Drawing.Color.Red;
            this.label111.Location = new System.Drawing.Point(312, 9);
            this.label111.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(48, 14);
            this.label111.TabIndex = 52;
            this.label111.Text = "Seal No";
            this.label111.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage25
            // 
            this.tabPage25.Controls.Add(this.LueItCode25);
            this.tabPage25.Controls.Add(this.Grd35);
            this.tabPage25.Controls.Add(this.panel30);
            this.tabPage25.Location = new System.Drawing.Point(4, 23);
            this.tabPage25.Name = "tabPage25";
            this.tabPage25.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage25.Size = new System.Drawing.Size(881, 87);
            this.tabPage25.TabIndex = 24;
            this.tabPage25.Text = "No.25";
            this.tabPage25.UseVisualStyleBackColor = true;
            // 
            // LueItCode25
            // 
            this.LueItCode25.EnterMoveNextControl = true;
            this.LueItCode25.Location = new System.Drawing.Point(299, 56);
            this.LueItCode25.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode25.Name = "LueItCode25";
            this.LueItCode25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.Appearance.Options.UseFont = true;
            this.LueItCode25.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode25.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode25.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode25.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode25.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode25.Properties.DropDownRows = 20;
            this.LueItCode25.Properties.NullText = "[Empty]";
            this.LueItCode25.Properties.PopupWidth = 500;
            this.LueItCode25.Size = new System.Drawing.Size(152, 20);
            this.LueItCode25.TabIndex = 55;
            this.LueItCode25.ToolTip = "F4 : Show/hide list";
            this.LueItCode25.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Grd35
            // 
            this.Grd35.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd35.DefaultRow.Height = 20;
            this.Grd35.DefaultRow.Sortable = false;
            this.Grd35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd35.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd35.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd35.Header.Height = 21;
            this.Grd35.Location = new System.Drawing.Point(3, 36);
            this.Grd35.Name = "Grd35";
            this.Grd35.RowHeader.Visible = true;
            this.Grd35.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd35.SingleClickEdit = true;
            this.Grd35.Size = new System.Drawing.Size(875, 48);
            this.Grd35.TabIndex = 52;
            this.Grd35.TreeCol = null;
            this.Grd35.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel30.Controls.Add(this.label112);
            this.panel30.Controls.Add(this.TxtFreight25);
            this.panel30.Controls.Add(this.TxtCnt25);
            this.panel30.Controls.Add(this.label113);
            this.panel30.Controls.Add(this.TxtSeal25);
            this.panel30.Controls.Add(this.label114);
            this.panel30.Controls.Add(this.TxtTotal25);
            this.panel30.Controls.Add(this.label115);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel30.Location = new System.Drawing.Point(3, 3);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(875, 33);
            this.panel30.TabIndex = 45;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.ForeColor = System.Drawing.Color.Black;
            this.label112.Location = new System.Drawing.Point(685, 9);
            this.label112.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(84, 14);
            this.label112.TabIndex = 58;
            this.label112.Text = "Ocean Freight";
            this.label112.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFreight25
            // 
            this.TxtFreight25.EditValue = "";
            this.TxtFreight25.EnterMoveNextControl = true;
            this.TxtFreight25.Location = new System.Drawing.Point(771, 6);
            this.TxtFreight25.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFreight25.Name = "TxtFreight25";
            this.TxtFreight25.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFreight25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFreight25.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFreight25.Properties.Appearance.Options.UseFont = true;
            this.TxtFreight25.Size = new System.Drawing.Size(90, 20);
            this.TxtFreight25.TabIndex = 59;
            this.TxtFreight25.Validated += new System.EventHandler(this.TxtFreight25_Validated);
            // 
            // TxtCnt25
            // 
            this.TxtCnt25.EditValue = "";
            this.TxtCnt25.EnterMoveNextControl = true;
            this.TxtCnt25.Location = new System.Drawing.Point(84, 7);
            this.TxtCnt25.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt25.Name = "TxtCnt25";
            this.TxtCnt25.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt25.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt25.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt25.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCnt25.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCnt25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCnt25.Size = new System.Drawing.Size(223, 20);
            this.TxtCnt25.TabIndex = 57;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.Red;
            this.label113.Location = new System.Drawing.Point(4, 10);
            this.label113.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(78, 14);
            this.label113.TabIndex = 56;
            this.label113.Text = "Container No";
            this.label113.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal25
            // 
            this.TxtSeal25.EditValue = "";
            this.TxtSeal25.EnterMoveNextControl = true;
            this.TxtSeal25.Location = new System.Drawing.Point(364, 6);
            this.TxtSeal25.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal25.Name = "TxtSeal25";
            this.TxtSeal25.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal25.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal25.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal25.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeal25.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeal25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSeal25.Size = new System.Drawing.Size(190, 20);
            this.TxtSeal25.TabIndex = 55;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.ForeColor = System.Drawing.Color.Black;
            this.label114.Location = new System.Drawing.Point(556, 9);
            this.label114.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(35, 14);
            this.label114.TabIndex = 53;
            this.label114.Text = "Total";
            this.label114.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal25
            // 
            this.TxtTotal25.EditValue = "";
            this.TxtTotal25.EnterMoveNextControl = true;
            this.TxtTotal25.Location = new System.Drawing.Point(594, 6);
            this.TxtTotal25.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal25.Name = "TxtTotal25";
            this.TxtTotal25.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal25.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal25.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal25.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal25.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal25.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal25.TabIndex = 54;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.ForeColor = System.Drawing.Color.Red;
            this.label115.Location = new System.Drawing.Point(312, 9);
            this.label115.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(48, 14);
            this.label115.TabIndex = 52;
            this.label115.Text = "Seal No";
            this.label115.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(127, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(234, 20);
            this.TxtDocNo.TabIndex = 88;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(14, 7);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(111, 14);
            this.label18.TabIndex = 87;
            this.label18.Text = "Document Number";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(127, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(126, 20);
            this.DteDocDt.TabIndex = 90;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Red;
            this.label61.Location = new System.Drawing.Point(54, 139);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(71, 14);
            this.label61.TabIndex = 99;
            this.label61.Text = "Notify Party";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueNotify
            // 
            this.LueNotify.EnterMoveNextControl = true;
            this.LueNotify.Location = new System.Drawing.Point(127, 136);
            this.LueNotify.Margin = new System.Windows.Forms.Padding(5);
            this.LueNotify.Name = "LueNotify";
            this.LueNotify.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.Appearance.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueNotify.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueNotify.Properties.DropDownRows = 20;
            this.LueNotify.Properties.NullText = "[Empty]";
            this.LueNotify.Properties.PopupWidth = 300;
            this.LueNotify.Size = new System.Drawing.Size(234, 20);
            this.LueNotify.TabIndex = 100;
            this.LueNotify.ToolTip = "F4 : Show/hide list";
            this.LueNotify.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueNotify.EditValueChanged += new System.EventHandler(this.LueNotify_EditValueChanged);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Red;
            this.label60.Location = new System.Drawing.Point(66, 73);
            this.label60.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(59, 14);
            this.label60.TabIndex = 93;
            this.label60.Text = "Customer";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(53, 205);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 14);
            this.label6.TabIndex = 105;
            this.label6.Text = "L/C Number";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(127, 70);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 20;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(234, 20);
            this.LueCtCode.TabIndex = 94;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(55, 95);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 14);
            this.label2.TabIndex = 95;
            this.label2.Text = "Packing List";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPL
            // 
            this.BtnPL.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPL.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPL.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPL.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPL.Appearance.Options.UseBackColor = true;
            this.BtnPL.Appearance.Options.UseFont = true;
            this.BtnPL.Appearance.Options.UseForeColor = true;
            this.BtnPL.Appearance.Options.UseTextOptions = true;
            this.BtnPL.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPL.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPL.Image = ((System.Drawing.Image)(resources.GetObject("BtnPL.Image")));
            this.BtnPL.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPL.Location = new System.Drawing.Point(364, 89);
            this.BtnPL.Name = "BtnPL";
            this.BtnPL.Size = new System.Drawing.Size(24, 21);
            this.BtnPL.TabIndex = 89;
            this.BtnPL.ToolTip = "List of Packing List";
            this.BtnPL.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPL.ToolTipTitle = "Run System";
            this.BtnPL.Click += new System.EventHandler(this.BtnSI_Click_1);
            // 
            // BtnPL2
            // 
            this.BtnPL2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPL2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPL2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPL2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPL2.Appearance.Options.UseBackColor = true;
            this.BtnPL2.Appearance.Options.UseFont = true;
            this.BtnPL2.Appearance.Options.UseForeColor = true;
            this.BtnPL2.Appearance.Options.UseTextOptions = true;
            this.BtnPL2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPL2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPL2.Image = ((System.Drawing.Image)(resources.GetObject("BtnPL2.Image")));
            this.BtnPL2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPL2.Location = new System.Drawing.Point(391, 89);
            this.BtnPL2.Name = "BtnPL2";
            this.BtnPL2.Size = new System.Drawing.Size(24, 21);
            this.BtnPL2.TabIndex = 90;
            this.BtnPL2.ToolTip = "Show Packing List";
            this.BtnPL2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPL2.ToolTipTitle = "Run System";
            this.BtnPL2.Click += new System.EventHandler(this.BtnSI2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(21, 183);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 14);
            this.label1.TabIndex = 103;
            this.label1.Text = "Sales Contract No";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(70, 227);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 14);
            this.label7.TabIndex = 107;
            this.label7.Text = "L/C Date";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(31, 29);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(94, 14);
            this.label17.TabIndex = 89;
            this.label17.Text = "Document Date";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LCDocDt
            // 
            this.LCDocDt.EditValue = null;
            this.LCDocDt.EnterMoveNextControl = true;
            this.LCDocDt.Location = new System.Drawing.Point(127, 224);
            this.LCDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LCDocDt.Name = "LCDocDt";
            this.LCDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LCDocDt.Properties.Appearance.Options.UseFont = true;
            this.LCDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LCDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LCDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LCDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.LCDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.LCDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.LCDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.LCDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.LCDocDt.Properties.MaxLength = 8;
            this.LCDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LCDocDt.Size = new System.Drawing.Size(120, 20);
            this.LCDocDt.TabIndex = 108;
            // 
            // MeeAccount
            // 
            this.MeeAccount.EnterMoveNextControl = true;
            this.MeeAccount.Location = new System.Drawing.Point(127, 158);
            this.MeeAccount.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAccount.Name = "MeeAccount";
            this.MeeAccount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAccount.Properties.Appearance.Options.UseFont = true;
            this.MeeAccount.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAccount.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAccount.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAccount.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAccount.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAccount.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAccount.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeAccount.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAccount.Properties.MaxLength = 400;
            this.MeeAccount.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAccount.Properties.ShowIcon = false;
            this.MeeAccount.Size = new System.Drawing.Size(234, 20);
            this.MeeAccount.TabIndex = 102;
            this.MeeAccount.ToolTip = "F4 : Show/hide text";
            this.MeeAccount.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAccount.ToolTipTitle = "Run System";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Red;
            this.label42.Location = new System.Drawing.Point(18, 161);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(107, 14);
            this.label42.TabIndex = 101;
            this.label42.Text = "Account of Messrs";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPLDocNo
            // 
            this.TxtPLDocNo.EnterMoveNextControl = true;
            this.TxtPLDocNo.Location = new System.Drawing.Point(127, 92);
            this.TxtPLDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPLDocNo.Name = "TxtPLDocNo";
            this.TxtPLDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPLDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPLDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPLDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPLDocNo.Properties.MaxLength = 250;
            this.TxtPLDocNo.Size = new System.Drawing.Size(234, 20);
            this.TxtPLDocNo.TabIndex = 96;
            // 
            // TxtSalesContract
            // 
            this.TxtSalesContract.EnterMoveNextControl = true;
            this.TxtSalesContract.Location = new System.Drawing.Point(127, 180);
            this.TxtSalesContract.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSalesContract.Name = "TxtSalesContract";
            this.TxtSalesContract.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSalesContract.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSalesContract.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSalesContract.Properties.Appearance.Options.UseFont = true;
            this.TxtSalesContract.Properties.MaxLength = 250;
            this.TxtSalesContract.Size = new System.Drawing.Size(234, 20);
            this.TxtSalesContract.TabIndex = 104;
            // 
            // TxtLcNo
            // 
            this.TxtLcNo.EnterMoveNextControl = true;
            this.TxtLcNo.Location = new System.Drawing.Point(127, 202);
            this.TxtLcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLcNo.Name = "TxtLcNo";
            this.TxtLcNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLcNo.Properties.MaxLength = 250;
            this.TxtLcNo.Size = new System.Drawing.Size(234, 20);
            this.TxtLcNo.TabIndex = 106;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.BtnDownload);
            this.panel7.Controls.Add(this.TxtLocalDocNo);
            this.panel7.Controls.Add(this.LblLocalDoc);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.panel6);
            this.panel7.Controls.Add(this.TxtLcNo);
            this.panel7.Controls.Add(this.label18);
            this.panel7.Controls.Add(this.TxtSalesContract);
            this.panel7.Controls.Add(this.TxtDocNo);
            this.panel7.Controls.Add(this.TxtPLDocNo);
            this.panel7.Controls.Add(this.label42);
            this.panel7.Controls.Add(this.MeeAccount);
            this.panel7.Controls.Add(this.LCDocDt);
            this.panel7.Controls.Add(this.label17);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.BtnPL2);
            this.panel7.Controls.Add(this.BtnPL);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.LueCtCode);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.label60);
            this.panel7.Controls.Add(this.LueNotify);
            this.panel7.Controls.Add(this.label61);
            this.panel7.Controls.Add(this.DteDocDt);
            this.panel7.Controls.Add(this.TxtSIDocNo);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(889, 251);
            this.panel7.TabIndex = 86;
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(363, 49);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 109;
            this.BtnDownload.ToolTip = "Download File";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(127, 48);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(234, 20);
            this.TxtLocalDocNo.TabIndex = 92;
            // 
            // LblLocalDoc
            // 
            this.LblLocalDoc.AutoSize = true;
            this.LblLocalDoc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLocalDoc.Location = new System.Drawing.Point(30, 51);
            this.LblLocalDoc.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblLocalDoc.Name = "LblLocalDoc";
            this.LblLocalDoc.Size = new System.Drawing.Size(95, 14);
            this.LblLocalDoc.TabIndex = 91;
            this.LblLocalDoc.Text = "Local Document";
            this.LblLocalDoc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(3, 117);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 14);
            this.label4.TabIndex = 97;
            this.label4.Text = "Shipment Instruction";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.BtnTemplate);
            this.panel6.Controls.Add(this.ChkShippingMark);
            this.panel6.Controls.Add(this.ChkFOB);
            this.panel6.Controls.Add(this.label57);
            this.panel6.Controls.Add(this.MeeRemark2);
            this.panel6.Controls.Add(this.label56);
            this.panel6.Controls.Add(this.MeeRemark);
            this.panel6.Controls.Add(this.MeeIssued);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.MeeOrder4);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.MeeOrder3);
            this.panel6.Controls.Add(this.MeeIssued2);
            this.panel6.Controls.Add(this.MeeIssued3);
            this.panel6.Controls.Add(this.MeeIssued4);
            this.panel6.Controls.Add(this.MeeOrder);
            this.panel6.Controls.Add(this.MeeOrder2);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(492, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(397, 251);
            this.panel6.TabIndex = 105;
            // 
            // BtnTemplate
            // 
            this.BtnTemplate.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnTemplate.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnTemplate.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTemplate.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnTemplate.Appearance.Options.UseBackColor = true;
            this.BtnTemplate.Appearance.Options.UseFont = true;
            this.BtnTemplate.Appearance.Options.UseForeColor = true;
            this.BtnTemplate.Appearance.Options.UseTextOptions = true;
            this.BtnTemplate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnTemplate.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnTemplate.Image = ((System.Drawing.Image)(resources.GetObject("BtnTemplate.Image")));
            this.BtnTemplate.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnTemplate.Location = new System.Drawing.Point(346, 204);
            this.BtnTemplate.Name = "BtnTemplate";
            this.BtnTemplate.Size = new System.Drawing.Size(24, 21);
            this.BtnTemplate.TabIndex = 110;
            this.BtnTemplate.ToolTip = "List Template";
            this.BtnTemplate.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnTemplate.ToolTipTitle = "Run System";
            this.BtnTemplate.Click += new System.EventHandler(this.BtnTemplate_Click);
            // 
            // ChkShippingMark
            // 
            this.ChkShippingMark.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkShippingMark.Location = new System.Drawing.Point(216, 226);
            this.ChkShippingMark.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkShippingMark.Name = "ChkShippingMark";
            this.ChkShippingMark.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkShippingMark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkShippingMark.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkShippingMark.Properties.Appearance.Options.UseBackColor = true;
            this.ChkShippingMark.Properties.Appearance.Options.UseFont = true;
            this.ChkShippingMark.Properties.Appearance.Options.UseForeColor = true;
            this.ChkShippingMark.Properties.Caption = "Print With Shipping Mark";
            this.ChkShippingMark.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkShippingMark.ShowToolTips = false;
            this.ChkShippingMark.Size = new System.Drawing.Size(171, 22);
            this.ChkShippingMark.TabIndex = 124;
            // 
            // ChkFOB
            // 
            this.ChkFOB.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkFOB.Location = new System.Drawing.Point(104, 226);
            this.ChkFOB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkFOB.Name = "ChkFOB";
            this.ChkFOB.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkFOB.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFOB.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkFOB.Properties.Appearance.Options.UseBackColor = true;
            this.ChkFOB.Properties.Appearance.Options.UseFont = true;
            this.ChkFOB.Properties.Appearance.Options.UseForeColor = true;
            this.ChkFOB.Properties.Caption = "Print With FOB";
            this.ChkFOB.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFOB.ShowToolTips = false;
            this.ChkFOB.Size = new System.Drawing.Size(143, 22);
            this.ChkFOB.TabIndex = 123;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(69, 208);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(34, 14);
            this.label57.TabIndex = 121;
            this.label57.Text = "Note";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark2
            // 
            this.MeeRemark2.EnterMoveNextControl = true;
            this.MeeRemark2.Location = new System.Drawing.Point(106, 204);
            this.MeeRemark2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark2.Name = "MeeRemark2";
            this.MeeRemark2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark2.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark2.Properties.MaxLength = 400;
            this.MeeRemark2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark2.Properties.ShowIcon = false;
            this.MeeRemark2.Size = new System.Drawing.Size(237, 20);
            this.MeeRemark2.TabIndex = 122;
            this.MeeRemark2.ToolTip = "F4 : Show/hide text";
            this.MeeRemark2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark2.ToolTipTitle = "Run System";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(36, 184);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(67, 14);
            this.label56.TabIndex = 119;
            this.label56.Text = "Description";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(106, 182);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 1000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(265, 20);
            this.MeeRemark.TabIndex = 120;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // MeeIssued
            // 
            this.MeeIssued.EnterMoveNextControl = true;
            this.MeeIssued.Location = new System.Drawing.Point(106, 6);
            this.MeeIssued.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIssued.Name = "MeeIssued";
            this.MeeIssued.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued.Properties.Appearance.Options.UseFont = true;
            this.MeeIssued.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIssued.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIssued.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIssued.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIssued.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIssued.Properties.MaxLength = 400;
            this.MeeIssued.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIssued.Properties.ShowIcon = false;
            this.MeeIssued.Size = new System.Drawing.Size(265, 20);
            this.MeeIssued.TabIndex = 110;
            this.MeeIssued.ToolTip = "F4 : Show/hide text";
            this.MeeIssued.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIssued.ToolTipTitle = "Run System";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(3, 97);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 14);
            this.label3.TabIndex = 114;
            this.label3.Text = "To The Order Of";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeOrder4
            // 
            this.MeeOrder4.EnterMoveNextControl = true;
            this.MeeOrder4.Location = new System.Drawing.Point(106, 160);
            this.MeeOrder4.Margin = new System.Windows.Forms.Padding(5);
            this.MeeOrder4.Name = "MeeOrder4";
            this.MeeOrder4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder4.Properties.Appearance.Options.UseFont = true;
            this.MeeOrder4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeOrder4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeOrder4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder4.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeOrder4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeOrder4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeOrder4.Properties.MaxLength = 400;
            this.MeeOrder4.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeOrder4.Properties.ShowIcon = false;
            this.MeeOrder4.Size = new System.Drawing.Size(265, 20);
            this.MeeOrder4.TabIndex = 118;
            this.MeeOrder4.ToolTip = "F4 : Show/hide text";
            this.MeeOrder4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeOrder4.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(44, 9);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 14);
            this.label5.TabIndex = 109;
            this.label5.Text = "Issued By";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeOrder3
            // 
            this.MeeOrder3.EnterMoveNextControl = true;
            this.MeeOrder3.Location = new System.Drawing.Point(106, 138);
            this.MeeOrder3.Margin = new System.Windows.Forms.Padding(5);
            this.MeeOrder3.Name = "MeeOrder3";
            this.MeeOrder3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder3.Properties.Appearance.Options.UseFont = true;
            this.MeeOrder3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeOrder3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeOrder3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder3.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeOrder3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeOrder3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeOrder3.Properties.MaxLength = 400;
            this.MeeOrder3.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeOrder3.Properties.ShowIcon = false;
            this.MeeOrder3.Size = new System.Drawing.Size(265, 20);
            this.MeeOrder3.TabIndex = 117;
            this.MeeOrder3.ToolTip = "F4 : Show/hide text";
            this.MeeOrder3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeOrder3.ToolTipTitle = "Run System";
            // 
            // MeeIssued2
            // 
            this.MeeIssued2.EnterMoveNextControl = true;
            this.MeeIssued2.Location = new System.Drawing.Point(106, 28);
            this.MeeIssued2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIssued2.Name = "MeeIssued2";
            this.MeeIssued2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued2.Properties.Appearance.Options.UseFont = true;
            this.MeeIssued2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIssued2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIssued2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIssued2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIssued2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIssued2.Properties.MaxLength = 400;
            this.MeeIssued2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIssued2.Properties.ShowIcon = false;
            this.MeeIssued2.Size = new System.Drawing.Size(265, 20);
            this.MeeIssued2.TabIndex = 111;
            this.MeeIssued2.ToolTip = "F4 : Show/hide text";
            this.MeeIssued2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIssued2.ToolTipTitle = "Run System";
            // 
            // MeeIssued3
            // 
            this.MeeIssued3.EnterMoveNextControl = true;
            this.MeeIssued3.Location = new System.Drawing.Point(106, 50);
            this.MeeIssued3.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIssued3.Name = "MeeIssued3";
            this.MeeIssued3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued3.Properties.Appearance.Options.UseFont = true;
            this.MeeIssued3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIssued3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIssued3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued3.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIssued3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIssued3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIssued3.Properties.MaxLength = 400;
            this.MeeIssued3.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIssued3.Properties.ShowIcon = false;
            this.MeeIssued3.Size = new System.Drawing.Size(265, 20);
            this.MeeIssued3.TabIndex = 112;
            this.MeeIssued3.ToolTip = "F4 : Show/hide text";
            this.MeeIssued3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIssued3.ToolTipTitle = "Run System";
            // 
            // MeeIssued4
            // 
            this.MeeIssued4.EnterMoveNextControl = true;
            this.MeeIssued4.Location = new System.Drawing.Point(106, 72);
            this.MeeIssued4.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIssued4.Name = "MeeIssued4";
            this.MeeIssued4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued4.Properties.Appearance.Options.UseFont = true;
            this.MeeIssued4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIssued4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIssued4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued4.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIssued4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIssued4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIssued4.Properties.MaxLength = 400;
            this.MeeIssued4.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIssued4.Properties.ShowIcon = false;
            this.MeeIssued4.Size = new System.Drawing.Size(265, 20);
            this.MeeIssued4.TabIndex = 113;
            this.MeeIssued4.ToolTip = "F4 : Show/hide text";
            this.MeeIssued4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIssued4.ToolTipTitle = "Run System";
            // 
            // MeeOrder
            // 
            this.MeeOrder.EnterMoveNextControl = true;
            this.MeeOrder.Location = new System.Drawing.Point(106, 94);
            this.MeeOrder.Margin = new System.Windows.Forms.Padding(5);
            this.MeeOrder.Name = "MeeOrder";
            this.MeeOrder.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder.Properties.Appearance.Options.UseFont = true;
            this.MeeOrder.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeOrder.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeOrder.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeOrder.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeOrder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeOrder.Properties.MaxLength = 400;
            this.MeeOrder.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeOrder.Properties.ShowIcon = false;
            this.MeeOrder.Size = new System.Drawing.Size(265, 20);
            this.MeeOrder.TabIndex = 115;
            this.MeeOrder.ToolTip = "F4 : Show/hide text";
            this.MeeOrder.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeOrder.ToolTipTitle = "Run System";
            // 
            // MeeOrder2
            // 
            this.MeeOrder2.EnterMoveNextControl = true;
            this.MeeOrder2.Location = new System.Drawing.Point(106, 116);
            this.MeeOrder2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeOrder2.Name = "MeeOrder2";
            this.MeeOrder2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder2.Properties.Appearance.Options.UseFont = true;
            this.MeeOrder2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeOrder2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeOrder2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeOrder2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeOrder2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeOrder2.Properties.MaxLength = 400;
            this.MeeOrder2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeOrder2.Properties.ShowIcon = false;
            this.MeeOrder2.Size = new System.Drawing.Size(265, 20);
            this.MeeOrder2.TabIndex = 116;
            this.MeeOrder2.ToolTip = "F4 : Show/hide text";
            this.MeeOrder2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeOrder2.ToolTipTitle = "Run System";
            // 
            // TxtSIDocNo
            // 
            this.TxtSIDocNo.EnterMoveNextControl = true;
            this.TxtSIDocNo.Location = new System.Drawing.Point(127, 114);
            this.TxtSIDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSIDocNo.Name = "TxtSIDocNo";
            this.TxtSIDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSIDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSIDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSIDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSIDocNo.Properties.MaxLength = 250;
            this.TxtSIDocNo.Size = new System.Drawing.Size(234, 20);
            this.TxtSIDocNo.TabIndex = 98;
            // 
            // OD1
            // 
            this.OD1.FileName = "NamaFile";
            // 
            // FrmSInv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 470);
            this.Name = "FrmSInv";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal1.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal2.Properties)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal3.Properties)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal4.Properties)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal5.Properties)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal6.Properties)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal7.Properties)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd18)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal8.Properties)).EndInit();
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd19)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal9.Properties)).EndInit();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd20)).EndInit();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal10.Properties)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd21)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal11.Properties)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd22)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal12.Properties)).EndInit();
            this.tabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd23)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal13.Properties)).EndInit();
            this.tabPage14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd24)).EndInit();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal14.Properties)).EndInit();
            this.tabPage15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd25)).EndInit();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal15.Properties)).EndInit();
            this.tabPage16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd26)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal16.Properties)).EndInit();
            this.tabPage17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd27)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal17.Properties)).EndInit();
            this.tabPage18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd28)).EndInit();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal18.Properties)).EndInit();
            this.tabPage19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd29)).EndInit();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal19.Properties)).EndInit();
            this.tabPage20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd30)).EndInit();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal20.Properties)).EndInit();
            this.tabPage21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd31)).EndInit();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal21.Properties)).EndInit();
            this.tabPage22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd32)).EndInit();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal22.Properties)).EndInit();
            this.tabPage23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd33)).EndInit();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal23.Properties)).EndInit();
            this.tabPage24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd34)).EndInit();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal24.Properties)).EndInit();
            this.tabPage25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd35)).EndInit();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFreight25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueNotify.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPLDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesContract.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLcNo.Properties)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkShippingMark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFOB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSIDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        protected System.Windows.Forms.Panel panel7;
        protected System.Windows.Forms.Panel panel6;
        private DevExpress.XtraEditors.MemoExEdit MeeIssued;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.MemoExEdit MeeOrder4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.MemoExEdit MeeOrder3;
        private DevExpress.XtraEditors.MemoExEdit MeeIssued2;
        private DevExpress.XtraEditors.MemoExEdit MeeIssued3;
        private DevExpress.XtraEditors.MemoExEdit MeeIssued4;
        private DevExpress.XtraEditors.MemoExEdit MeeOrder;
        private DevExpress.XtraEditors.MemoExEdit MeeOrder2;
        private DevExpress.XtraEditors.TextEdit TxtLcNo;
        private System.Windows.Forms.Label label18;
        private DevExpress.XtraEditors.TextEdit TxtSalesContract;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtPLDocNo;
        private System.Windows.Forms.Label label42;
        private DevExpress.XtraEditors.MemoExEdit MeeAccount;
        internal DevExpress.XtraEditors.DateEdit LCDocDt;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.SimpleButton BtnPL2;
        public DevExpress.XtraEditors.SimpleButton BtnPL;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LueCtCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label60;
        internal DevExpress.XtraEditors.LookUpEdit LueNotify;
        private System.Windows.Forms.Label label61;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        protected System.Windows.Forms.Panel panel4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Button BtnSO;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private DevExpress.XtraEditors.LookUpEdit LueItCode1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd11;
        protected System.Windows.Forms.Panel panel8;
        internal DevExpress.XtraEditors.TextEdit TxtCnt1;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtSeal1;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtTotal1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tabPage2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd12;
        protected System.Windows.Forms.Panel panel9;
        internal DevExpress.XtraEditors.TextEdit TxtCnt2;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtSeal2;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtTotal2;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TabPage tabPage3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd13;
        protected System.Windows.Forms.Panel panel10;
        internal DevExpress.XtraEditors.TextEdit TxtCnt3;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtSeal3;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtTotal3;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TabPage tabPage4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd14;
        protected System.Windows.Forms.Panel panel11;
        internal DevExpress.XtraEditors.TextEdit TxtCnt4;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtSeal4;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtTotal4;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TabPage tabPage5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd15;
        protected System.Windows.Forms.Panel panel12;
        internal DevExpress.XtraEditors.TextEdit TxtCnt5;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtSeal5;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtTotal5;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TabPage tabPage6;
        protected internal TenTec.Windows.iGridLib.iGrid Grd16;
        protected System.Windows.Forms.Panel panel13;
        internal DevExpress.XtraEditors.TextEdit TxtCnt6;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtSeal6;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtTotal6;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TabPage tabPage7;
        protected internal TenTec.Windows.iGridLib.iGrid Grd17;
        protected System.Windows.Forms.Panel panel14;
        internal DevExpress.XtraEditors.TextEdit TxtCnt7;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtSeal7;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit TxtTotal7;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TabPage tabPage8;
        protected internal TenTec.Windows.iGridLib.iGrid Grd18;
        protected System.Windows.Forms.Panel panel15;
        internal DevExpress.XtraEditors.TextEdit TxtCnt8;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtSeal8;
        private System.Windows.Forms.Label label27;
        internal DevExpress.XtraEditors.TextEdit TxtTotal8;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TabPage tabPage9;
        protected internal TenTec.Windows.iGridLib.iGrid Grd19;
        protected System.Windows.Forms.Panel panel16;
        internal DevExpress.XtraEditors.TextEdit TxtCnt9;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtSeal9;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit TxtTotal9;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TabPage tabPage10;
        protected internal TenTec.Windows.iGridLib.iGrid Grd20;
        protected System.Windows.Forms.Panel panel17;
        internal DevExpress.XtraEditors.TextEdit TxtCnt10;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.TextEdit TxtSeal10;
        private System.Windows.Forms.Label label31;
        internal DevExpress.XtraEditors.TextEdit TxtTotal10;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TabPage tabPage11;
        protected internal TenTec.Windows.iGridLib.iGrid Grd21;
        protected System.Windows.Forms.Panel panel5;
        internal DevExpress.XtraEditors.TextEdit TxtCnt11;
        private System.Windows.Forms.Label label30;
        internal DevExpress.XtraEditors.TextEdit TxtSeal11;
        private System.Windows.Forms.Label label32;
        internal DevExpress.XtraEditors.TextEdit TxtTotal11;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TabPage tabPage12;
        protected internal TenTec.Windows.iGridLib.iGrid Grd22;
        protected System.Windows.Forms.Panel panel18;
        internal DevExpress.XtraEditors.TextEdit TxtCnt12;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit TxtSeal12;
        private System.Windows.Forms.Label label34;
        internal DevExpress.XtraEditors.TextEdit TxtTotal12;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TabPage tabPage13;
        protected internal TenTec.Windows.iGridLib.iGrid Grd23;
        protected System.Windows.Forms.Panel panel19;
        internal DevExpress.XtraEditors.TextEdit TxtCnt13;
        private System.Windows.Forms.Label label35;
        internal DevExpress.XtraEditors.TextEdit TxtSeal13;
        private System.Windows.Forms.Label label36;
        internal DevExpress.XtraEditors.TextEdit TxtTotal13;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TabPage tabPage14;
        protected internal TenTec.Windows.iGridLib.iGrid Grd24;
        protected System.Windows.Forms.Panel panel20;
        internal DevExpress.XtraEditors.TextEdit TxtCnt14;
        private System.Windows.Forms.Label label37;
        internal DevExpress.XtraEditors.TextEdit TxtSeal14;
        private System.Windows.Forms.Label label38;
        internal DevExpress.XtraEditors.TextEdit TxtTotal14;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TabPage tabPage15;
        protected internal TenTec.Windows.iGridLib.iGrid Grd25;
        protected System.Windows.Forms.Panel panel21;
        internal DevExpress.XtraEditors.TextEdit TxtCnt15;
        private System.Windows.Forms.Label label39;
        internal DevExpress.XtraEditors.TextEdit TxtSeal15;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtTotal15;
        private System.Windows.Forms.Label label55;
        private DevExpress.XtraEditors.LookUpEdit LueItCode2;
        private DevExpress.XtraEditors.LookUpEdit LueItCode3;
        private DevExpress.XtraEditors.LookUpEdit LueItCode4;
        private DevExpress.XtraEditors.LookUpEdit LueItCode5;
        private DevExpress.XtraEditors.LookUpEdit LueItCode6;
        private DevExpress.XtraEditors.LookUpEdit LueItCode7;
        private DevExpress.XtraEditors.LookUpEdit LueItCode8;
        private DevExpress.XtraEditors.LookUpEdit LueItCode9;
        private DevExpress.XtraEditors.LookUpEdit LueItCode10;
        private DevExpress.XtraEditors.LookUpEdit LueItCode11;
        private DevExpress.XtraEditors.LookUpEdit LueItCode12;
        private DevExpress.XtraEditors.LookUpEdit LueItCode13;
        private DevExpress.XtraEditors.LookUpEdit LueItCode14;
        private DevExpress.XtraEditors.LookUpEdit LueItCode15;
        internal DevExpress.XtraEditors.TextEdit TxtSIDocNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label56;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label57;
        private DevExpress.XtraEditors.CheckEdit ChkFOB;
        private System.Windows.Forms.Label label59;
        internal DevExpress.XtraEditors.TextEdit TxtFreight1;
        private System.Windows.Forms.Label label62;
        internal DevExpress.XtraEditors.TextEdit TxtFreight2;
        private System.Windows.Forms.Label label63;
        internal DevExpress.XtraEditors.TextEdit TxtFreight3;
        private System.Windows.Forms.Label label64;
        internal DevExpress.XtraEditors.TextEdit TxtFreight4;
        private System.Windows.Forms.Label label65;
        internal DevExpress.XtraEditors.TextEdit TxtFreight5;
        private System.Windows.Forms.Label label66;
        internal DevExpress.XtraEditors.TextEdit TxtFreight6;
        private System.Windows.Forms.Label label67;
        internal DevExpress.XtraEditors.TextEdit TxtFreight7;
        private System.Windows.Forms.Label label68;
        internal DevExpress.XtraEditors.TextEdit TxtFreight8;
        private System.Windows.Forms.Label label69;
        internal DevExpress.XtraEditors.TextEdit TxtFreight9;
        private System.Windows.Forms.Label label70;
        internal DevExpress.XtraEditors.TextEdit TxtFreight10;
        private System.Windows.Forms.Label label71;
        internal DevExpress.XtraEditors.TextEdit TxtFreight11;
        private System.Windows.Forms.Label label72;
        internal DevExpress.XtraEditors.TextEdit TxtFreight12;
        private System.Windows.Forms.Label label73;
        internal DevExpress.XtraEditors.TextEdit TxtFreight13;
        private System.Windows.Forms.Label label74;
        internal DevExpress.XtraEditors.TextEdit TxtFreight14;
        private System.Windows.Forms.Label label75;
        internal DevExpress.XtraEditors.TextEdit TxtFreight15;
        private DevExpress.XtraEditors.CheckEdit ChkShippingMark;
        private DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label LblLocalDoc;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.TabPage tabPage18;
        private System.Windows.Forms.TabPage tabPage19;
        private System.Windows.Forms.TabPage tabPage20;
        private System.Windows.Forms.TabPage tabPage21;
        private System.Windows.Forms.TabPage tabPage22;
        private System.Windows.Forms.TabPage tabPage23;
        private System.Windows.Forms.TabPage tabPage24;
        private System.Windows.Forms.TabPage tabPage25;
        protected internal TenTec.Windows.iGridLib.iGrid Grd26;
        protected System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label76;
        internal DevExpress.XtraEditors.TextEdit TxtFreight16;
        internal DevExpress.XtraEditors.TextEdit TxtCnt16;
        private System.Windows.Forms.Label label77;
        internal DevExpress.XtraEditors.TextEdit TxtSeal16;
        private System.Windows.Forms.Label label78;
        internal DevExpress.XtraEditors.TextEdit TxtTotal16;
        private System.Windows.Forms.Label label79;
        protected internal TenTec.Windows.iGridLib.iGrid Grd27;
        protected System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label80;
        internal DevExpress.XtraEditors.TextEdit TxtFreight17;
        internal DevExpress.XtraEditors.TextEdit TxtCnt17;
        private System.Windows.Forms.Label label81;
        internal DevExpress.XtraEditors.TextEdit TxtSeal17;
        private System.Windows.Forms.Label label82;
        internal DevExpress.XtraEditors.TextEdit TxtTotal17;
        private System.Windows.Forms.Label label83;
        protected internal TenTec.Windows.iGridLib.iGrid Grd28;
        protected System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label84;
        internal DevExpress.XtraEditors.TextEdit TxtFreight18;
        internal DevExpress.XtraEditors.TextEdit TxtCnt18;
        private System.Windows.Forms.Label label85;
        internal DevExpress.XtraEditors.TextEdit TxtSeal18;
        private System.Windows.Forms.Label label86;
        internal DevExpress.XtraEditors.TextEdit TxtTotal18;
        private System.Windows.Forms.Label label87;
        protected internal TenTec.Windows.iGridLib.iGrid Grd29;
        protected System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label label88;
        internal DevExpress.XtraEditors.TextEdit TxtFreight19;
        internal DevExpress.XtraEditors.TextEdit TxtCnt19;
        private System.Windows.Forms.Label label89;
        internal DevExpress.XtraEditors.TextEdit TxtSeal19;
        private System.Windows.Forms.Label label90;
        internal DevExpress.XtraEditors.TextEdit TxtTotal19;
        private System.Windows.Forms.Label label91;
        protected internal TenTec.Windows.iGridLib.iGrid Grd30;
        protected System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label92;
        internal DevExpress.XtraEditors.TextEdit TxtFreight20;
        internal DevExpress.XtraEditors.TextEdit TxtCnt20;
        private System.Windows.Forms.Label label93;
        internal DevExpress.XtraEditors.TextEdit TxtSeal20;
        private System.Windows.Forms.Label label94;
        internal DevExpress.XtraEditors.TextEdit TxtTotal20;
        private System.Windows.Forms.Label label95;
        protected internal TenTec.Windows.iGridLib.iGrid Grd31;
        protected System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label label96;
        internal DevExpress.XtraEditors.TextEdit TxtFreight21;
        internal DevExpress.XtraEditors.TextEdit TxtCnt21;
        private System.Windows.Forms.Label label97;
        internal DevExpress.XtraEditors.TextEdit TxtSeal21;
        private System.Windows.Forms.Label label98;
        internal DevExpress.XtraEditors.TextEdit TxtTotal21;
        private System.Windows.Forms.Label label99;
        protected internal TenTec.Windows.iGridLib.iGrid Grd32;
        protected System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label label100;
        internal DevExpress.XtraEditors.TextEdit TxtFreight22;
        internal DevExpress.XtraEditors.TextEdit TxtCnt22;
        private System.Windows.Forms.Label label101;
        internal DevExpress.XtraEditors.TextEdit TxtSeal22;
        private System.Windows.Forms.Label label102;
        internal DevExpress.XtraEditors.TextEdit TxtTotal22;
        private System.Windows.Forms.Label label103;
        protected internal TenTec.Windows.iGridLib.iGrid Grd33;
        protected System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Label label104;
        internal DevExpress.XtraEditors.TextEdit TxtFreight23;
        internal DevExpress.XtraEditors.TextEdit TxtCnt23;
        private System.Windows.Forms.Label label105;
        internal DevExpress.XtraEditors.TextEdit TxtSeal23;
        private System.Windows.Forms.Label label106;
        internal DevExpress.XtraEditors.TextEdit TxtTotal23;
        private System.Windows.Forms.Label label107;
        protected internal TenTec.Windows.iGridLib.iGrid Grd34;
        protected System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Label label108;
        internal DevExpress.XtraEditors.TextEdit TxtFreight24;
        internal DevExpress.XtraEditors.TextEdit TxtCnt24;
        private System.Windows.Forms.Label label109;
        internal DevExpress.XtraEditors.TextEdit TxtSeal24;
        private System.Windows.Forms.Label label110;
        internal DevExpress.XtraEditors.TextEdit TxtTotal24;
        private System.Windows.Forms.Label label111;
        protected internal TenTec.Windows.iGridLib.iGrid Grd35;
        protected System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Label label112;
        internal DevExpress.XtraEditors.TextEdit TxtFreight25;
        internal DevExpress.XtraEditors.TextEdit TxtCnt25;
        private System.Windows.Forms.Label label113;
        internal DevExpress.XtraEditors.TextEdit TxtSeal25;
        private System.Windows.Forms.Label label114;
        internal DevExpress.XtraEditors.TextEdit TxtTotal25;
        private System.Windows.Forms.Label label115;
        private DevExpress.XtraEditors.LookUpEdit LueItCode16;
        private DevExpress.XtraEditors.LookUpEdit LueItCode17;
        private DevExpress.XtraEditors.LookUpEdit LueItCode18;
        private DevExpress.XtraEditors.LookUpEdit LueItCode19;
        private DevExpress.XtraEditors.LookUpEdit LueItCode20;
        private DevExpress.XtraEditors.LookUpEdit LueItCode21;
        private DevExpress.XtraEditors.LookUpEdit LueItCode22;
        private DevExpress.XtraEditors.LookUpEdit LueItCode23;
        private DevExpress.XtraEditors.LookUpEdit LueItCode24;
        private DevExpress.XtraEditors.LookUpEdit LueItCode25;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.SaveFileDialog SFD1;
        private System.Windows.Forms.OpenFileDialog OD1;
        public DevExpress.XtraEditors.SimpleButton BtnTemplate;
        public DevExpress.XtraEditors.MemoExEdit MeeRemark2;
    }
}