﻿#region Update
/*
  03/13/2021 [DITA/IMS] New Apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingAPPaidItemDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptAgingAPPaidItem mFrmParent;
        internal string
            VoucherDocNo = string.Empty;
        internal bool
            mVoucherAccessInd = false;

        #endregion

        #region Constructor

        public FrmRptAgingAPPaidItemDlg2(FrmRptAgingAPPaidItem FrmParent) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnChoose.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                  Grd1,
                  new string[] 
                    {
                        //0
                        "No",
                        
                        //1-3
                        "Voucher#",
                        "",
                        "Amount",
                    },
                   new int[] 
                    {
                        //0
                        50,
 
                        //1-3
                        120, 20, 100
                    }
              );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);

            mVoucherAccessInd = GetAccessInd("FrmVoucher");
            if (!mVoucherAccessInd)
                Sm.GrdColInvisible(Grd1, new int[] { 2 });

            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, Amt ");
            SQL.AppendLine("From TblVoucherHdr ");
            SQL.AppendLine("Where CancelInd = 'N' And Find_In_SET(DocNo ,@VoucherDocNo); ");

            return SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {

        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = string.Empty;
                Sm.CmParam<string>(ref cm, "@VoucherDocNo", VoucherDocNo);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL() + Filter,
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                            //1
                            "Amt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 1);
                        }, true, false, false, false
                    );
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3 });

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        internal bool GetAccessInd(string FormName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(Left(AccessInd, 1), 'N') ");
            SQL.AppendLine("From TblGroupMenu ");
            SQL.AppendLine("Where MenuCode In ( Select MenuCode From TblMenu Where Param = @Param1 ) ");
            SQL.AppendLine("And GrpCode In ");
            SQL.AppendLine("(Select GrpCode From TblUser Where UserCode = @Param2) ");
            SQL.AppendLine("Limit 1; ");

            return Sm.GetValue(SQL.ToString(), FormName, Gv.CurrentUserCode, string.Empty) == "Y";
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events
        #endregion

        #region Grid Control Events

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                var f = new FrmVoucher(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                var f = new FrmVoucher(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

    }
}
