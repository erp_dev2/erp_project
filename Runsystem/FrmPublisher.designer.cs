﻿namespace RunSystem
{
    partial class FrmPublisher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPublisher));
            this.label3 = new System.Windows.Forms.Label();
            this.TxtRegisterNo = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtPbsCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DtePbsDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkActiveInd = new DevExpress.XtraEditors.CheckEdit();
            this.MeeLocation = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtPosition = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtDeptName = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtPbsEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnEmpCode = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRegisterNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPbsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePbsDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePbsDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPbsEmpCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 274);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtPosition);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtDeptName);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.TxtEmpName);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.TxtPbsEmpCode);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.BtnEmpCode);
            this.panel2.Controls.Add(this.MeeLocation);
            this.panel2.Controls.Add(this.ChkActiveInd);
            this.panel2.Controls.Add(this.DtePbsDt);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtRegisterNo);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtPbsCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 274);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(106, 199);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 14);
            this.label3.TabIndex = 18;
            this.label3.Text = "Location";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRegisterNo
            // 
            this.TxtRegisterNo.EnterMoveNextControl = true;
            this.TxtRegisterNo.Location = new System.Drawing.Point(163, 175);
            this.TxtRegisterNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRegisterNo.Name = "TxtRegisterNo";
            this.TxtRegisterNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRegisterNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRegisterNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRegisterNo.Properties.Appearance.Options.UseFont = true;
            this.TxtRegisterNo.Properties.MaxLength = 50;
            this.TxtRegisterNo.Size = new System.Drawing.Size(277, 20);
            this.TxtRegisterNo.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(61, 177);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 14);
            this.label6.TabIndex = 16;
            this.label6.Text = "Register Number";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPbsCode
            // 
            this.TxtPbsCode.EnterMoveNextControl = true;
            this.TxtPbsCode.Location = new System.Drawing.Point(163, 43);
            this.TxtPbsCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPbsCode.Name = "TxtPbsCode";
            this.TxtPbsCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPbsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPbsCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPbsCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPbsCode.Properties.MaxLength = 16;
            this.TxtPbsCode.Size = new System.Drawing.Size(197, 20);
            this.TxtPbsCode.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(72, 45);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Publisher Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DtePbsDt
            // 
            this.DtePbsDt.EditValue = null;
            this.DtePbsDt.EnterMoveNextControl = true;
            this.DtePbsDt.Location = new System.Drawing.Point(163, 153);
            this.DtePbsDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePbsDt.Name = "DtePbsDt";
            this.DtePbsDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePbsDt.Properties.Appearance.Options.UseFont = true;
            this.DtePbsDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePbsDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePbsDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePbsDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePbsDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePbsDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePbsDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePbsDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePbsDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePbsDt.Size = new System.Drawing.Size(166, 20);
            this.DtePbsDt.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(126, 155);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 14);
            this.label4.TabIndex = 14;
            this.label4.Text = "Date";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActiveInd
            // 
            this.ChkActiveInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActiveInd.Location = new System.Drawing.Point(364, 41);
            this.ChkActiveInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActiveInd.Name = "ChkActiveInd";
            this.ChkActiveInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActiveInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActiveInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActiveInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActiveInd.Properties.Caption = "Active";
            this.ChkActiveInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActiveInd.ShowToolTips = false;
            this.ChkActiveInd.Size = new System.Drawing.Size(65, 22);
            this.ChkActiveInd.TabIndex = 11;
            // 
            // MeeLocation
            // 
            this.MeeLocation.EnterMoveNextControl = true;
            this.MeeLocation.Location = new System.Drawing.Point(163, 197);
            this.MeeLocation.Margin = new System.Windows.Forms.Padding(5);
            this.MeeLocation.Name = "MeeLocation";
            this.MeeLocation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLocation.Properties.Appearance.Options.UseFont = true;
            this.MeeLocation.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLocation.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeLocation.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLocation.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeLocation.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLocation.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeLocation.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLocation.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeLocation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeLocation.Properties.MaxLength = 400;
            this.MeeLocation.Properties.PopupFormSize = new System.Drawing.Size(700, 20);
            this.MeeLocation.Properties.ShowIcon = false;
            this.MeeLocation.Size = new System.Drawing.Size(277, 20);
            this.MeeLocation.TabIndex = 35;
            this.MeeLocation.ToolTip = "F4 : Show/hide text";
            this.MeeLocation.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeLocation.ToolTipTitle = "Run System";
            // 
            // TxtPosition
            // 
            this.TxtPosition.EnterMoveNextControl = true;
            this.TxtPosition.Location = new System.Drawing.Point(163, 131);
            this.TxtPosition.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosition.Name = "TxtPosition";
            this.TxtPosition.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPosition.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosition.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosition.Properties.Appearance.Options.UseFont = true;
            this.TxtPosition.Properties.MaxLength = 16;
            this.TxtPosition.Size = new System.Drawing.Size(276, 20);
            this.TxtPosition.TabIndex = 44;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(110, 133);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 14);
            this.label5.TabIndex = 43;
            this.label5.Text = "Position";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDeptName
            // 
            this.TxtDeptName.EnterMoveNextControl = true;
            this.TxtDeptName.Location = new System.Drawing.Point(163, 109);
            this.TxtDeptName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptName.Name = "TxtDeptName";
            this.TxtDeptName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDeptName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptName.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptName.Properties.MaxLength = 16;
            this.TxtDeptName.Size = new System.Drawing.Size(276, 20);
            this.TxtDeptName.TabIndex = 42;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(86, 111);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 14);
            this.label7.TabIndex = 41;
            this.label7.Text = "Department";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(163, 87);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 16;
            this.TxtEmpName.Size = new System.Drawing.Size(276, 20);
            this.TxtEmpName.TabIndex = 40;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(12, 89);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(147, 14);
            this.label15.TabIndex = 39;
            this.label15.Text = "Employee Publisher Name";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPbsEmpCode
            // 
            this.TxtPbsEmpCode.EnterMoveNextControl = true;
            this.TxtPbsEmpCode.Location = new System.Drawing.Point(163, 65);
            this.TxtPbsEmpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPbsEmpCode.Name = "TxtPbsEmpCode";
            this.TxtPbsEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPbsEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPbsEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPbsEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPbsEmpCode.Properties.MaxLength = 16;
            this.TxtPbsEmpCode.Size = new System.Drawing.Size(197, 20);
            this.TxtPbsEmpCode.TabIndex = 38;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(15, 67);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(144, 14);
            this.label8.TabIndex = 37;
            this.label8.Text = "Employee Publisher Code";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnEmpCode
            // 
            this.BtnEmpCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode.Appearance.Options.UseFont = true;
            this.BtnEmpCode.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode.Image")));
            this.BtnEmpCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode.Location = new System.Drawing.Point(362, 64);
            this.BtnEmpCode.Name = "BtnEmpCode";
            this.BtnEmpCode.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpCode.TabIndex = 36;
            this.BtnEmpCode.ToolTip = "Find Employee";
            this.BtnEmpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode.ToolTipTitle = "Run System";
            this.BtnEmpCode.Click += new System.EventHandler(this.BtnEmpCode_Click);
            // 
            // FrmPublisher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 274);
            this.Name = "FrmPublisher";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRegisterNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPbsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePbsDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePbsDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPbsEmpCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtRegisterNo;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtPbsCode;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DtePbsDt;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkActiveInd;
        private DevExpress.XtraEditors.MemoExEdit MeeLocation;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode;
        public DevExpress.XtraEditors.TextEdit TxtPosition;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.TextEdit TxtDeptName;
        private System.Windows.Forms.Label label7;
        public DevExpress.XtraEditors.TextEdit TxtEmpName;
        private System.Windows.Forms.Label label15;
        public DevExpress.XtraEditors.TextEdit TxtPbsEmpCode;
        private System.Windows.Forms.Label label8;
    }
}