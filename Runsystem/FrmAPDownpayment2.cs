﻿#region Update
/*
    04/12/2017 [HAR] tambah Local Document defaultnya ambil dari PO 
    03/05/2017 [TKG] nilai PO ditambah customs tax.
    18/05/2017 [TKG] tambah info downpayment dari dokumen ap downpayment yg lain
    26/10/2017 [ARI] bug printout
    14/11/2017 [WED] tambahan opsi untuk Paid To Bank apakah berdasarkan vendor atau tidak.
    18/12/2017 [TKG] department ada opsi difilter berdasarkan otorisasi di group
    15/03/2017 [TKG] tambah tax.
    28/04/2018 [TKG] tambah info pi di grid document qr code.
    02/05/2018 [ARI] tambah printout kim
    05/05/2018 [TKG] set voucher request document number berdasarkan standard document
    15/05/2018 [TKG] filter berdasarkan entity berdasarkan paramere IsFilterByEnt
    19/07/2018 [TKG] warning saat print apabila belum diapprove.
    05/10/2018 [HAR] tambah payment date dan ftp file
    06/10/2018 [TKG] update dept di vr berdasarkan setting approval
    10/03/2020 [HAR/SRN] tambah printout sementara ambil dari APDP biasa 
    13/04/2020 [TKG/IMS] menambah proses journal
    03/06/2020 [DITA/IMS] Printout APDP IMS
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    21/06/2021 [ICA/IMS] bug taxamt2 tidak muncul
    23/06/2021 [DITA/IMS] ketika minus akan pindah ke credit di jurnal APDP berdasarkan parameter : APDownpayment2JournalFormula
    06/01/2022 [BRI/IMS] Tambah CheckBox AddCost dan akan berpengaruh ketika checkbox tercentang VR APDP dan VR APDP Additional Amt jadi satu berdasarkan parameter IsAPDPProcessTo1Journal
    11/01/2022 [RDA/IMS] Penyesuaian data untuk PO for Service yang muncul berdasarkan param IsUsePOForServiceDocNo
    26/01/2022 [WED/GSS] BUG saat generate Voucher Request#, belum menambahkan melihat ke parameter IsVoucherDocSeqNoEnabled
    08/02/2021 [TKG/GSS] ubah GetParameter dan proses save.
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.Xml;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmAPDownpayment2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mVdCode = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application;
            mPODocNo = string.Empty;
        internal FrmAPDownpayment2Find FrmFind;
        private bool
            mIsRemarkForApprovalMandatory = false,
            mIsAPDownpaymentApprovalBasedOnDept = false,
            IsFilterBySite = false,
            mIsAPDownpaymentUseCOA = false,
            mIsAPDownpaymentPaidToBankBasedOnVendor = false,
            mIsEntityMandatory = false,
            mIsAPDownpaymentUseTax = false,
            mIsAPDownpaymentUseQRCode = false,
            mIsAPDownpaymentAbleToEditTax = false,
            mIsAutoJournalActived = false,
            mIsAPDownpayment2JournalEnabled = false,
            mIsAPDPProcessTo1Journal = false;
        public bool mIsAPDPAllowToUploadFile = false;
        internal bool 
            mIsFilterByDept = false, 
            mIsFilterByEnt = false,
            mIsUsePOForServiceDocNo = false;
        private string 
            mPIQRCodeTaxDocType = string.Empty,
            mVoucherCodeFormatType = "1",
            mMainCurCode = string.Empty,
            mAPDownpayment2JournalFormula = "1";
        iGCell fCell;
        bool fAccept;
        public string
           mPortForFTPClient = string.Empty,
           mHostAddrForFTPClient = string.Empty,
           mSharedFolderForFTPClient = string.Empty,
           mUsernameForFTPClient = string.Empty,
           mPasswordForFTPClient = string.Empty,
           mFileSizeMaxUploadFTPClient = string.Empty;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmAPDownpayment2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Vendor's AP Downpayment";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                TcAPDownpayment.SelectedTabPage = TpTax;
                Sl.SetLueTaxCode(ref LueTaxCode);
                Sl.SetLueTaxCode(ref LueTaxCode2);
                Sl.SetLueTaxCode(ref LueTaxCode3);
                TpTax.PageVisible = mIsAPDownpaymentUseTax;

                TcAPDownpayment.SelectedTabPage = TpCOA;
                TpCOA.PageVisible = mIsAPDownpaymentUseCOA;

                TcAPDownpayment.SelectedTabPage = TpGeneral;
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueUserCode(ref LuePIC);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                Sl.SetLueBankCode(ref LueBankCode);
                if (!mIsAPDownpaymentPaidToBankBasedOnVendor)
                    Sl.SetLueBankCode(ref LuePaidToBankCode);
                if (mIsEntityMandatory) LblEntCode.ForeColor = Color.Red;
                Sl.SetLueEntCode(ref LueEntCode);                
                if (mIsAPDownpaymentApprovalBasedOnDept)
                    LblDeptCode.ForeColor = Color.Red;

                SetFormControl(mState.View);

                base.FrmLoad(sender, e);

                if (!mIsAPDPAllowToUploadFile)
                {
                    BtnDownload.Visible = BtnUpload.Visible = false;
                    TxtFile.Visible = ChkFile.Visible = PbUpload.Visible = lblFileName.Visible = false;
                }

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsAPDownpaymentApprovalBasedOnDept = Sm.IsApprovalNeedXCode("APDownpayment", "Dept");
            
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'MainCurCode', 'APDownpayment2JournalFormula', 'IsUsePOForServiceDocNo', 'PIQRCodeTaxDocType', 'VoucherCodeFormatType', ");
            SQL.AppendLine("'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', 'PortForFTPClient', ");
            SQL.AppendLine("'FileSizeMaxUploadFTPClient', 'IsAPDownpayment2JournalEnabled', 'IsAutoJournalActived', 'IsAPDPProcessTo1Journal', 'IsAPDownpaymentRemarkForApprovalMandatory', ");
            SQL.AppendLine("'IsAPDPAllowToUploadFile', 'IsAPDownpaymentAbleToEditTax', 'IsFilterByEnt', 'IsAPDownpaymentUseQRCode', 'IsAPDownpaymentUseTax', ");
            SQL.AppendLine("'IsFilterBySite', 'IsAPDownpaymentUseCOA', 'IsEntityMandatory', 'IsAPDownpaymentPaidToBankBasedOnVendor', 'IsFilterByDept' ");            
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsAPDownpayment2JournalEnabled": mIsAPDownpayment2JournalEnabled = ParValue == "Y"; break;
                            case "IsUsePOForServiceDocNo": mIsUsePOForServiceDocNo = ParValue == "Y"; break;
                            case "IsAPDownpaymentPaidToBankBasedOnVendor": mIsAPDownpaymentPaidToBankBasedOnVendor = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsAPDownpaymentUseCOA": mIsAPDownpaymentUseCOA = ParValue == "Y"; break;
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                            case "IsFilterBySite": IsFilterBySite = ParValue == "Y"; break;
                            case "IsAPDPAllowToUploadFile": mIsAPDPAllowToUploadFile = ParValue == "Y"; break;
                            case "IsAPDownpaymentAbleToEditTax": mIsAPDownpaymentAbleToEditTax = ParValue == "Y"; break;
                            case "IsAPDownpaymentRemarkForApprovalMandatory": mIsRemarkForApprovalMandatory = ParValue == "Y"; break;
                            case "IsFilterByEnt": mIsFilterByEnt = ParValue == "Y"; break;
                            case "IsAPDownpaymentUseQRCode": mIsAPDownpaymentUseQRCode = ParValue == "Y"; break;
                            case "IsAPDownpaymentUseTax": mIsAPDownpaymentUseTax = ParValue == "Y"; break;
                            case "IsAPDPProcessTo1Journal": mIsAPDPProcessTo1Journal = ParValue == "Y"; break;

                            //string
                            case "APDownpayment2JournalFormula": mAPDownpayment2JournalFormula = ParValue; break;
                            case "PIQRCodeTaxDocType": mPIQRCodeTaxDocType = ParValue; break;
                            case "VoucherCodeFormatType": mVoucherCodeFormatType = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 5;
            Grd1.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "User", 
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4 });

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 9;
            Grd2.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark",

                        //6-8
                        "", //Checkbox debit ind
                        "", //Checkbox credit ind
                        "" //CheckBox Db Cr Indicator
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400,

                        //6-8
                        20, 20, 0
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdColCheck(Grd2, new int[] { 6, 7, 8 });
            Sm.GrdColReadOnly(Grd2, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd2, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 8 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 2 });
            if (!mIsAPDPProcessTo1Journal)
                Sm.GrdColInvisible(Grd2, new int[] { 6, 7, 8 }, false);
            Grd2.Cols[6].Move(3);
            Grd2.Cols[7].Move(5);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    {  
                        DteDocDt, ChkCancelInd, TxtLocalDocNo, TxtVdCode, LueCurCode, 
                        TxtAmt, LuePIC, LueDeptCode, MeeCancelReason, MeeRemark, 
                        LuePaymentType, LueBankCode, TxtGiroNo, DteOpeningDt, DteDueDt,
                        TxtPaymentUser, LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, 
                        TxtPaidToBankAcNo, LueEntCode, TxtAmtBefTax, TxtTaxInvoiceNo, DteTaxInvoiceDt, 
                        LueTaxCode, TxtTaxAmt, LueTaxCode2, TxtTaxInvoiceNo2, DteTaxInvoiceDt2, 
                        LueTaxCode3, TxtTaxInvoiceNo3, DteTaxInvoiceDt3, TxtDownpaymentPercentage, DtePaymentDt, 
                        TxtFile, TxtDownpaymentPercentage 
                    }, true);
                    BtnPODocNo.Enabled = false; 
                    BtnUpload.Enabled = false;
                    BtnDownload.Enabled = true;
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 3, 4, 5, 6, 7, 8 });
                   
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLocalDocNo, DteDocDt, LueCurCode, LuePIC, LueDeptCode, 
                        LuePaymentType, MeeRemark, TxtPaymentUser, LueEntCode, TxtDownpaymentPercentage, 
                        LueTaxCode, TxtTaxInvoiceNo, DteTaxInvoiceDt, LueTaxCode2, TxtTaxInvoiceNo2, 
                        DteTaxInvoiceDt2, LueTaxCode3, TxtTaxInvoiceNo3, DteTaxInvoiceDt3, DtePaymentDt
                    }, false);
                    if (!mIsAPDownpaymentPaidToBankBasedOnVendor)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo }, false);
                    BtnPODocNo.Enabled = true;
                    BtnUpload.Enabled = true;
                    BtnDownload.Enabled = false;
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 3, 4, 5, 6, 7 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mVdCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, TxtPODocNo, TxtNoOfDownpayment, TxtLocalDocNo,  TxtPOLocalDocNo,
                TxtVdCode, TxtPOCurCode, LueCurCode, LuePIC, LueDeptCode,
                LuePaymentType, LueBankCode, TxtGiroNo, DteOpeningDt, DteDueDt, 
                MeeCancelReason, TxtVoucherRequestDocNo, TxtVoucherDocNo, MeeRemark, TxtVoucherDocNo2, 
                TxtVoucherRequestDocNo2, TxtPaymentUser, LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, 
                TxtPaidToBankAcNo, LueEntCode, LueTaxCode, TxtTaxInvoiceNo, DteTaxInvoiceDt, 
                LueTaxCode2, TxtTaxInvoiceNo2, DteTaxInvoiceDt2, LueTaxCode3, TxtTaxInvoiceNo3, 
                DteTaxInvoiceDt3, DtePaymentDt, TxtFile, TxtPTCode, MeePORemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {  
                TxtPOAmt, TxtOtherAmt, TxtAmt, TxtCOAAmt, TxtAmtBefTax,
                TxtDownpaymentPercentage, TxtTaxAmt, TxtTaxAmt2, TxtTaxAmt3, TxtAmtAftTax
            }, 0);
            ChkCancelInd.Checked = false;
            ChkFile.Checked =  false;
            PbUpload.Value = 0;
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;

            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAPDownpayment2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
                Sl.SetLueEntCode(ref LueEntCode, string.Empty, mIsFilterByEnt ? "Y" : "N");
                BtnPODocNo_Click(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Print", "") == DialogResult.No || 
                    Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                    IsDataAlreadyCancelled()
                    ) return;

                string Doctitle = Sm.GetParameter("DocTitle");
                string[] TableName = { "APDPHdr", "APDPDtl", "APDPJournal", "APDPSignIMS" };
                var l = new List<APDPHdr>();
                var lDtl = new List<APDPDtl>();
                var lJournal = new List<APDPJournal>();
                var lSign = new List<APDPSignIMS>();
                var myLists = new List<IList>();
               
                #region Header

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                if (IsFilterBySite)
                {
                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, K.Company, K.Phone, K.Address,  ");
                }
                else
                {
                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
                    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As Phone, ");
                }
                SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y') As DocDt, A.PoDocNo, A.CurCode As POCurCode, ");
                SQL.AppendLine("F.Total+ if(length(A2.taxCode1>0), (F.Total*B.Taxrate*0.01), 0)+  ");
                SQL.AppendLine("if(length(A2.taxCode2>0), (F.Total*C.Taxrate*0.01), 0)+   ");
                SQL.AppendLine("if(length(A2.taxCode3>0), (F.Total*D.Taxrate*0.01), 0)- ifnull(A2.DiscountAmt, 0) As POAmt, A.Amt, ");
                SQL.AppendLine("E.VdName, A.VoucherRequestDocNo, G.VoucherDocNo,    ");
                SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
                SQL.AppendLine("H.Deptname, J.SiteName,  A.CreateBy, Date_Format(left(A.CreateDt, 8),'%d %M %Y') As CreateDt, A.Remark,  ");
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='IsFilterBySite') As SiteInd, P.UserName As PIC, A.PaymentUser, A.GiroNo, ");
                SQL.AppendLine("L.BankName As GiroBankName, M.OptDesc As PaymentType, Date_Format(A.OpeningDt,'%d %M %Y')As GiroDt, ");
                SQL.AppendLine("N.BankName As PaidToBankName, A.PaidToBankBranch, A.PaidToBankAcName, A.PaidToBankAcNo, ");
                SQL.AppendLine("E.Address As VdAddress, O.CityName As VdCity, G.LocalDocNo As VRLocal, F.ProjectName, A2.Remark PORemark, Q.UserName, R.CurName ");
                SQL.AppendLine("From TblAPDownpayment A  ");
                SQL.AppendLine("Inner Join tblPOhdr A2 On A.PODocNo=A2.DocNo  ");
                SQL.AppendLine("Left Join TblTax B On A2.TaxCode1 = B.taxCode  ");
                SQL.AppendLine("Left Join TblTax C On A2.TaxCode2 = C.taxCode  ");
                SQL.AppendLine("Left Join TblTax D On A2.TaxCode3 = D.taxCode  ");
                SQL.AppendLine("Inner Join TblVendor E On A2.VdCode = E.VdCode  ");
                SQL.AppendLine("Inner join   ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select B.DocNo, ");
                SQL.AppendLine("    SUM((H.Uprice * (B.Qty-ifnull(M.Qty, 0))) - (H.Uprice*(B.Qty-ifnull(M.Qty, 0))*B.Discount/100) - B.DiscountAmt + B.Roundingvalue) As Total,  Group_concat(Distinct IfNull(N.ProjectName, '') Separator '\r\n') ProjectName ");
                SQL.AppendLine("    From TblPODtl B ");
                SQL.AppendLine("    Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
                SQL.AppendLine("    Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
                SQL.AppendLine("    Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo ");
                SQL.AppendLine("    Inner Join TblQtHdr G On D.QtDocNo=G.DocNo ");
                SQL.AppendLine("    Inner Join TblQtDtl H On D.QtDocNo=H.DocNo And D.QtDNo=H.DNo ");
                SQL.AppendLine("    Left Join TblPOQtyCancel M On B.Docno = M.PODocno And B.DNo = M.PODno And M.CancelInd='N' ");
                SQL.AppendLine("    Left Join TblProjectGroup N On E.PGCode=N.PGCode ");
                SQL.AppendLine("    Where B.CancelInd='N' ");
                SQL.AppendLine("    And B.DocNo=@PODocNo ");
                SQL.AppendLine("    Group By B.DocNo ");
                SQL.AppendLine(")F On A2.Docno = F.DocNo ");
                SQL.AppendLine("Left Join TblVoucherRequestHdr G On A.VoucherRequestDocNo=G.DocNo ");
                SQL.AppendLine("Left Join TblDepartment H On A.DeptCode = H.DeptCode ");
                SQL.AppendLine("Left Join tblSite J On A2.SiteCode = J.SiteCode ");
                SQL.AppendLine("Left Join TblBank L On A.BankCode=L.BankCode ");
                SQL.AppendLine("Left Join TblOption M On A.PaymentType=M.OptCode And M.OptCat='VoucherPaymentType' ");
                SQL.AppendLine("Left Join TblBank N On A.PaidToBankCode=N.BankCode ");
                SQL.AppendLine("Left Join TblCity O On E.CityCode=O.CityCode ");
                SQL.AppendLine("Left Join TblUser P On A.PIC=P.UserCode ");
                SQL.AppendLine("Inner Join TblUser Q On A.CreateBy=Q.UserCode ");
                SQL.AppendLine("Inner Join TblCurrency R On A.CurCode=R.CurCode ");
                if (IsFilterBySite)
                {
                    SQL.AppendLine("Left Join (");
                    SQL.AppendLine("    Select distinct A.DocNo, D.EntName As Company, D.EntPhone As Phone,  D.EntAddress As Address ");
                    SQL.AppendLine("    From TblPOhdr A  ");
                    SQL.AppendLine("    Inner Join TblSite B On A.SiteCode = B.SiteCode  ");
                    SQL.AppendLine("    Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode  ");
                    SQL.AppendLine("    Inner Join TblEntity D On C.EntCode = D.EntCode  ");
                    SQL.AppendLine("    Where A.DocNo=@PODocNo ");
                    SQL.AppendLine(")K On A.PODocNo = K.DocNo ");
                }
                SQL.AppendLine("Where A.DocNo=@DocNo;");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@PODocNo", mIsUsePOForServiceDocNo ? mPODocNo : TxtPODocNo.Text);
                    if (IsFilterBySite)
                    {
                        string CompanyLogo = Sm.GetValue(
                           "Select D.EntLogoName " +
                           "From TblPOhdr A  " +
                           "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                           "Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode " +
                           "Inner Join TblEntity D On C.EntCode = D.EntCode  " +
                           "Where A.Docno='" + (mIsUsePOForServiceDocNo ? mPODocNo : TxtPODocNo.Text) + "' "
                       );
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                    }
                    else
                    {
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    }

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                            {
                             //0
                             "CompanyLogo",
                             //1-5
                             "Company",
                             "Address",
                             "Phone",
                             "DocNo",
                             "DocDt",
                             //6-10
                             "PODocNo",
                             "POCurCode",
                             "POAmt",
                             "VDName",
                             "VoucherRequestDocNo",
                             //11-15
                             "VoucherDocNo",
                             "CreateBy",
                             "DeptName",
                             "SiteName",
                             "StatusDEsc",
                             //16-20
                             "Amt",
                             "CreateDt",
                             "Remark",
                             "SiteInd",
                             "PIC",
                             //21-25
                             "PaymentUser",
                             "GiroNo",
                             "GiroBankName",
                             "PaymentType",
                             "GiroDt", 

                             //26-30
                             "PaidToBankName",
                             "PaidToBankBranch",
                             "PaidToBankAcName",
                             "PaidToBankAcNo",
                             "VdCity",

                             //31-35
                             "VdAddress",
                             "VRLocal",
                             "ProjectName", 
                             "PORemark", 
                             "UserName",

                             //36
                             "CurName"
                            });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new APDPHdr()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),
                                Company = Sm.DrStr(dr, c[1]),
                                Address = Sm.DrStr(dr, c[2]),
                                Phone = Sm.DrStr(dr, c[3]),
                                DocNo = Sm.DrStr(dr, c[4]),
                                DocDt = Sm.DrStr(dr, c[5]),

                                PODocNo = Sm.DrStr(dr, c[6]),
                                POCurCode = Sm.DrStr(dr, c[7]),
                                POAmt = Sm.DrDec(dr, c[8]),
                                VDName = Sm.DrStr(dr, c[9]),
                                VoucherRequestDocNo = Sm.DrStr(dr, c[10]),

                                VoucherDocNo = Sm.DrStr(dr, c[11]),
                                CreateBy = Sm.DrStr(dr, c[12]),
                                DeptName = Sm.DrStr(dr, c[13]),
                                SiteName = Sm.DrStr(dr, c[14]),
                                StatusDEsc = Sm.DrStr(dr, c[15]),

                                Amt = Sm.DrDec(dr, c[16]),
                                CreateDt = Sm.DrStr(dr, c[17]),
                                Remark = Sm.DrStr(dr, c[18]),
                                SiteInd = Sm.DrStr(dr, c[19]),
                                PIC = Sm.DrStr(dr, c[20]),

                                PaymentUser = Sm.DrStr(dr, c[21]),
                                GiroNo = Sm.DrStr(dr, c[22]),
                                GiroBankName = Sm.DrStr(dr, c[23]),
                                PaymentType = Sm.DrStr(dr, c[24]),
                                GiroDt = Sm.DrStr(dr, c[25]),

                                PaidToBankName = Sm.DrStr(dr, c[26]),
                                PaidToBankBranch = Sm.DrStr(dr, c[27]),
                                PaidToBankAcName = Sm.DrStr(dr, c[28]),
                                PaidToBankAcNo = Sm.DrStr(dr, c[29]),
                                VdCity = Sm.DrStr(dr, c[30]),

                                VdAddress = Sm.DrStr(dr, c[31]),
                                VRLocal = Sm.DrStr(dr, c[32]),
                                ProjectName = Sm.DrStr(dr, c[33]),
                                PORemark = Sm.DrStr(dr, c[34]),
                                UserName = Sm.DrStr(dr, c[35]),

                                CurName = Sm.DrStr(dr, c[36]),
                                Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[16])),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                            });
                        }
                    }

                    dr.Close();
                }
                myLists.Add(l);

                #endregion

                #region Detail Approval

                var cmDtl = new MySqlCommand();

                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                    SQLDtl.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                    SQLDtl.AppendLine("From ( ");
                    SQLDtl.AppendLine("    Select Distinct ");
                    SQLDtl.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                    SQLDtl.AppendLine("    B.ApprovalDNo As DNo, D.Level, if(D.Level=1,'Acknowledge By','Approved By') As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                    SQLDtl.AppendLine("    From TblAPDownpayment A ");
                    SQLDtl.AppendLine("    Inner Join TblDocApproval B On B.DocType='APDownpayment' And A.DocNo=B.DocNo ");
                    SQLDtl.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                    SQLDtl.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'APDownpayment' ");
                    SQLDtl.AppendLine("    Where A.DocNo=@DocNo ");
                    SQLDtl.AppendLine("    Union All ");
                    SQLDtl.AppendLine("    Select Distinct ");
                    SQLDtl.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, ");
                    SQLDtl.AppendLine("    '00' As DNo, 0 As Level, 'Prepared By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                    SQLDtl.AppendLine("    From TblAPDownpayment A ");
                    SQLDtl.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                    SQLDtl.AppendLine("    Where A.DocNo=@DocNo ");
                    SQLDtl.AppendLine(") T1 ");
                    SQLDtl.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                    SQLDtl.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                    SQLDtl.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                    SQLDtl.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                    SQLDtl.AppendLine("Order By T1.DNo desc; ");

                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@Space", "-------------------------");
                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                            {
                             //0
                             "Signature" ,

                             //1-5
                             "Username" ,
                             "PosName",
                             "Space",
                             "Level",
                             "Title",
                             "LastupDt"
                            });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {

                            lDtl.Add(new APDPDtl()
                            {
                                Signature = Sm.DrStr(drDtl, cDtl[0]),
                                UserName = Sm.DrStr(drDtl, cDtl[1]),
                                PosName = Sm.DrStr(drDtl, cDtl[2]),
                                Space = Sm.DrStr(drDtl, cDtl[3]),
                                DNo = Sm.DrStr(drDtl, cDtl[4]),
                                Title = Sm.DrStr(drDtl, cDtl[5]),
                                LastUpDt = Sm.DrStr(drDtl, cDtl[6])
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(lDtl);
                #endregion

                #region Journal
                var cm2 = new MySqlCommand();
                var SQL2 = new StringBuilder();

                using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                {
                cn2.Open();
                cm2.Connection = cn2;
                SQL2.AppendLine("SELECT A.JournalDocNo, C.AcNo, C.AcDesc, B.DAmt, B.CAmt ");
                SQL2.AppendLine("From TblAPDownPayment A ");
                SQL2.AppendLine("Inner Join TblJournalDtl B On A.JournalDocNo = B.DocNo ");
                SQL2.AppendLine("INNER JOIN TBLCOA C ON B.ACNO = C.ACNO ");
                SQL2.AppendLine("WHERE A.DocNo = @DocNo; ");

                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "AcNo" ,

                         //1-5
                         "AcDesc" ,
                         "DAmt",
                         "CAmt",
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {

                        lJournal.Add(new APDPJournal()
                        {
                            AcNo = Sm.DrStr(dr2, c2[0]),
                            AcDesc = Sm.DrStr(dr2, c2[1]),
                            DAmt = Sm.DrDec(dr2, c2[2]),
                            CAmt = Sm.DrDec(dr2, c2[3]),
                           
                        });
                    }
                }
                dr2.Close();
            }
           myLists.Add(lJournal);

           #endregion

                #region Sign IMS

               var cm3 = new MySqlCommand();

               var SQL3 = new StringBuilder();
               using (var cn3 = new MySqlConnection(Gv.ConnectionString))
               {
                   cn3.Open();
                   cm3.Connection = cn3;

                   SQL3.AppendLine("Select Distinct ");
                   SQL3.AppendLine("B.UserCode, C.UserCode2, D.UserCode3, B.UserName, C.UserName2, D.UserName3 ");
                   SQL3.AppendLine("From TblAPDownPayment A ");
                   SQL3.AppendLine("Left Join ( ");
                   SQL3.AppendLine("    Select Distinct ");
                   SQL3.AppendLine("    A.DocNo , B.UserCode As UserCode,  Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName ");
                   SQL3.AppendLine("    From TblAPDownpayment A ");
                   SQL3.AppendLine("    Inner Join TblDocApproval B On B.DocType='APDownpayment' And A.DocNo=B.DocNo ");
                   SQL3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                   SQL3.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'APDownpayment' ");
                   SQL3.AppendLine("    Where D.Level = '1' And A.DocNo=@DocNo ");
                   SQL3.AppendLine(" ) B On A.DocNo = B.DocNo ");
                   SQL3.AppendLine("Left Join ( ");
                   SQL3.AppendLine("    Select Distinct ");
                   SQL3.AppendLine("    A.DocNo, B.UserCode As UserCode2, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName2 ");
                   SQL3.AppendLine("    From TblAPDownpayment A ");
                   SQL3.AppendLine("    Inner Join TblDocApproval B On B.DocType='APDownpayment' And A.DocNo=B.DocNo ");
                   SQL3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                   SQL3.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'APDownpayment' ");
                   SQL3.AppendLine("    Where D.Level = '2' And A.DocNo=@DocNo ");
                   SQL3.AppendLine(" ) C On A.DocNo = C.DocNo ");
                   SQL3.AppendLine("Left Join ( "); 
                   SQL3.AppendLine("    Select Distinct ");
                   SQL3.AppendLine("   A.DocNo, B.UserCode As UserCode3, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName)))  As UserName3 ");
                   SQL3.AppendLine("    From TblAPDownpayment A ");
                   SQL3.AppendLine("    Inner Join TblDocApproval B On B.DocType='APDownpayment' And A.DocNo=B.DocNo ");
                   SQL3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                   SQL3.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'APDownpayment' ");
                   SQL3.AppendLine("    Where D.Level = '3' And A.DocNo=@DocNo ");
                   SQL3.AppendLine(") D On A.DocNo = D.DocNo ");
                   SQL3.AppendLine("Where A.DocNo = @DocNo; " );
                   
                   cm3.CommandText = SQL3.ToString();
                   Sm.CmParam<String>(ref cm3, "@DocNo", TxtDocNo.Text);
                   var dr3 = cm3.ExecuteReader();
                   var c3 = Sm.GetOrdinal(dr3, new string[] 
                                {
                                 //0
                                 "UserName" ,

                                 //1-2
                                 "Username2" ,
                                 "Username3" ,
                                 
                                });
                   if (dr3.HasRows)
                   {
                       while (dr3.Read())
                       {

                           lSign.Add(new APDPSignIMS()
                           {
                               UserName = Sm.DrStr(dr3, c3[0]),
                               UserName2 = Sm.DrStr(dr3, c3[1]),
                               UserName3 = Sm.DrStr(dr3, c3[2]),
                           });
                       }
                   }
                   dr3.Close();
               }
               myLists.Add(lSign);
           #endregion

            if (Doctitle == "KIM")
                Sm.PrintReport("APDownPaymentKIM", myLists, TableName, false);
            if (Doctitle == "IMS")
                Sm.PrintReport("APDownPaymentIMS", myLists, TableName, false);
            else
                Sm.PrintReport("APDownPayment", myLists, TableName, false);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                ShowAPDownPaymentHdr(DocNo);
                Sm.ShowDocApproval(DocNo, "APDownpayment", ref Grd1);
                if (mIsAPDownpaymentUseCOA) ShowAPDownPaymentDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAPDownPaymentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

           
            SQL.AppendLine("Select ");
            SQL.AppendLine("A.DocNo, A.DocDt, A.LocalDocNo, A.CancelInd, A.CancelReason, A.Status, ");
            if(mIsUsePOForServiceDocNo)
                SQL.AppendLine("case when I.MaterialRequestServiceDocNo IS NULL then A.PODocNo ELSE I.POSDocNo end AS PODocNo, ");
            else
                SQL.AppendLine("A.PODocNo, ");
            SQL.AppendLine("A.VdCode, A.VoucherRequestDocNo, A.VoucherRequestDocNo2, ");
            SQL.AppendLine("A.PaymentType, A.BankAcCode, A.BankCode, A.GiroNo, A.OpeningDt, ");
            SQL.AppendLine("A.DueDt, A.CurCode, A.DownpaymentPercentage, A.AmtBefTax, A.TaxInvoiceNo, ");
            SQL.AppendLine("A.TaxInvoiceDt, A.TaxCode, A.TaxAmt, A.TaxInvoiceNo2, A.TaxInvoiceDt2, ");
            SQL.AppendLine("A.TaxCode2, A.TaxAmt2, A.TaxInvoiceNo3, A.TaxInvoiceDt3, A.TaxCode3, A.TaxAmt3, ");
            SQL.AppendLine("A.JournalDocNo, A.JournalDocNo2, A.Amt, A.PIC, A.DeptCode, ");
            SQL.AppendLine("A.PaymentUser, A.PaidToBankCode, A.PaidToBankBranch, A.PaidToBankAcNo, A.PaidToBankAcName, ");
            SQL.AppendLine("A.EntCode, A.PaymentDt, A.FileName, A.Remark, A.AcNoType, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, ");
            SQL.AppendLine("A.CurCode As POCurCode, ifnull(F.Total, 0) + if(length(A2.taxCode1>0), (ifnull(F.Total, 0)*B.Taxrate*0.01), 0)+ ");
            SQL.AppendLine("if(length(A2.taxCode2>0), (F.Total*C.Taxrate*0.01), 0)+  ");
            SQL.AppendLine("if(length(A2.taxCode3>0), (F.Total*D.Taxrate*0.01), 0)- ifnull(A2.DiscountAmt, 0)+ifnull(A2.CustomsTaxAmt, 0) As POAmt, ");
            SQL.AppendLine("E.VdName, A.VoucherRequestDocNo, G.VoucherDocNo, ");
            SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("IfNull((Select Count(DocNo) From TblAPDownpayment Where PODocNo=A.PODocNo And (CancelInd='N' Or Status<>'C')), 0) As NoOfDownpayment, ");
            SQL.AppendLine("A.VoucherRequestDocNo2, H.VoucherDocNo As VoucherDocNo2, A.LocalDocNo, A2.LocalDOcNO As POLocalDocNo, ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("    Select Sum(Amt) As Amt ");
            SQL.AppendLine("    From TblAPDownpayment ");
            SQL.AppendLine("    Where PODocNo=A.PODocNo ");
            SQL.AppendLine("    And DocNo<>@DocNo ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And Status In ('O', 'A') ");
            SQL.AppendLine("), 0) As OtherAmt, ");
            SQL.AppendLine("A.AmtBefTax, A.TaxInvoiceNo, A.TaxInvoiceDt, A.TaxCode, A.TaxAmt, A.PaymentDt, A.FileName, ");
            SQL.AppendLine("F.PtName, A2.Remark As PORemark ");
            SQL.AppendLine("From TblAPDownpayment A ");
            SQL.AppendLine("Inner Join TblPOHdr A2 On A.PODocNo=A2.DocNo ");
            SQL.AppendLine("Left Join TblTax B On A2.TaxCode1 = B.taxCode ");
            SQL.AppendLine("Left Join TblTax C On A2.TaxCode2 = C.taxCode ");
            SQL.AppendLine("Left Join TblTax D On A2.TaxCode3 = D.taxCode ");
            SQL.AppendLine("Inner Join TblVendor E On A2.VdCode = E.VdCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select B.DocNo,  ");
            SQL.AppendLine("    Group_Concat(Distinct N.PtName Separator ', ') As PtName, ");
            SQL.AppendLine("    Sum((H.Uprice * (B.Qty-ifnull(M.Qty, 0)))-(H.Uprice*(B.Qty-ifnull(M.Qty, 0))*B.Discount/100) - B.DiscountAmt + B.Roundingvalue) As Total ");
            SQL.AppendLine("    From TblPODtl B  ");
            SQL.AppendLine("    Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo  ");
            SQL.AppendLine("    Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo  ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo  ");
            SQL.AppendLine("    Inner Join TblQtHdr G On D.QtDocNo=G.DocNo  ");
            SQL.AppendLine("    Inner Join TblQtDtl H On D.QtDocNo=H.DocNo And D.QtDNo=H.DNo  ");
            SQL.AppendLine("    Left Join TblPOQtyCancel M On B.Docno = M.PODocno And B.DNo = M.PODno And M.CancelInd='N'  ");
            SQL.AppendLine("    Left Join TblPaymentTerm N On G.PtCode = N.PtCode ");
            SQL.AppendLine("    Where B.CancelInd='N' ");
            SQL.AppendLine("    And B.DocNo In ( ");
            SQL.AppendLine("        Select PODocNo From TblAPDownpayment Where DocNo=@DocNo And PODocNo is Not Null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By B.DocNo ");
            SQL.AppendLine(")F On A2.Docno = F.DocNo ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr G On A.VoucherRequestDocNo=G.DocNo ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr H On A.VoucherRequestDocNo2=H.DocNo ");
            if (mIsUsePOForServiceDocNo)
            {
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select B.DocNo, F.DocNo AS POSDocNo, F.MaterialRequestServiceDocNo ");
                SQL.AppendLine("    From TblPODtl B  ");
                SQL.AppendLine("    Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo  ");
                SQL.AppendLine("    Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo  ");
                SQL.AppendLine("    Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo  ");
                SQL.AppendLine(")I On A2.Docno = I.DocNo ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "StatusDesc", "CancelInd", "PODocNo", "NoOfDownpayment",
                            
                            //6-10
                            "VdName", "POCurcode", "POAmt", "CurCode", "Amt",  
                            
                            //11-15
                            "PIC", "DeptCode", "VoucherRequestDocNo", "VoucherDocNo", "CancelReason", 
                            
                            //16-20
                            "PaymentType", "BankCode", "GiroNo", "OpeningDt", "DueDt", 

                            //21-25
                            "Remark", "VoucherRequestDocNo2", "VoucherDocNo2", "POLocalDocNo", "LocalDocNo",

                            //26-30
                            "OtherAmt", "PaymentUser", "PaidToBankCode", "PaidToBankBranch", "PaidToBankAcName", 
                            
                            //31-35
                            "PaidToBankAcNo", "EntCode", "AmtBefTax", "TaxInvoiceNo", "TaxInvoiceDt", 
                            
                            //36-40
                            "TaxCode", "TaxAmt", "PaymentDt", "FileName", "DownpaymentPercentage",

                            //41-45
                            "TaxInvoiceNo2", "TaxInvoiceDt2", "TaxCode2", "TaxAmt2", "TaxInvoiceNo3", 
                            
                            //46-50
                            "TaxInvoiceDt3", "TaxCode3", "TaxAmt3", "PtName", "PORemark"
                        },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[15]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                        TxtPODocNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtNoOfDownpayment.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[5]), 2);
                        TxtVdCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtPOCurCode.EditValue = Sm.DrStr(dr, c[7]);
                        TxtPOAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[8]), 0);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[9]));
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                        Sm.SetLue(LuePIC, Sm.DrStr(dr, c[11]));
                        Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[12]), string.Empty);
                        TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[13]);
                        TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[14]);
                        Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[16]));
                        Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[17]));
                        TxtGiroNo.EditValue = Sm.DrStr(dr, c[18]);
                        Sm.SetDte(DteOpeningDt, Sm.DrStr(dr, c[19]));
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[20]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[21]);
                        TxtVoucherRequestDocNo2.EditValue = (mIsAPDPProcessTo1Journal ? Sm.DrStr(dr, c[13]) : Sm.DrStr(dr, c[22]));
                        TxtVoucherDocNo2.EditValue = (mIsAPDPProcessTo1Journal ? Sm.DrStr(dr, c[14]) : Sm.DrStr(dr, c[23]));
                        TxtPOLocalDocNo.EditValue = Sm.DrStr(dr, c[24]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[25]);
                        TxtOtherAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[26]), 0);
                        TxtPaymentUser.EditValue = Sm.DrStr(dr, c[27]);
                        if (mIsAPDownpaymentPaidToBankBasedOnVendor)
                            SetLuePaidToBankCode(ref LuePaidToBankCode, string.Empty, "1");
                        Sm.SetLue(LuePaidToBankCode, Sm.DrStr(dr, c[28]));
                        TxtPaidToBankBranch.EditValue = Sm.DrStr(dr, c[29]);
                        TxtPaidToBankAcName.EditValue = Sm.DrStr(dr, c[30]);
                        TxtPaidToBankAcNo.EditValue = Sm.DrStr(dr, c[31]);
                        Sl.SetLueEntCode(ref LueEntCode, Sm.DrStr(dr, c[32]), string.Empty);
                        TxtAmtBefTax.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[33]), 0);
                        TxtTaxInvoiceNo.EditValue = Sm.DrStr(dr, c[34]);
                        Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[35]));
                        Sm.SetLue(LueTaxCode, Sm.DrStr(dr, c[36]));
                        TxtTaxAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[37]), 0);
                        TxtAmtAftTax.EditValue = TxtAmt.EditValue;
                        Sm.SetDte(DtePaymentDt, Sm.DrStr(dr, c[38]));
                        TxtFile.EditValue = Sm.DrStr(dr, c[39]);
                        TxtDownpaymentPercentage.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[40]), 0);
                        TxtTaxInvoiceNo2.EditValue = Sm.DrStr(dr, c[41]);
                        Sm.SetDte(DteTaxInvoiceDt2, Sm.DrStr(dr, c[42]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[43]));
                        TxtTaxAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[44]), 0);
                        TxtTaxInvoiceNo3.EditValue = Sm.DrStr(dr, c[45]);
                        Sm.SetDte(DteTaxInvoiceDt3, Sm.DrStr(dr, c[46]));
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[47]));
                        TxtTaxAmt3.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[48]), 0);
                        TxtPTCode.EditValue = Sm.DrStr(dr, c[49]);
                        MeePORemark.EditValue = Sm.DrStr(dr, c[50]);
                    }, true
                );
        }

        private void ShowAPDownPaymentDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark, A.ActInd ");
            SQL.AppendLine("From TblAPDownPaymentDtl A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] { "AcNo", "AcDesc", "DAmt", "CAmt", "Remark", "ActInd" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 5);
                    if (Sm.DrStr(dr, c[5]) == "Y")
                    {
                        if (Sm.GetGrdDec(Grd2, Row, 3) > 0)
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 5);
                        else
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 5);
                    }
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd2, 0, 1);
            ComputeCOAAmt();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "APDownpayment", "TblAPDownpayment");
            string VoucherRequestDocNo = string.Empty;

            if (mVoucherCodeFormatType == "2")
                VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");
            else
                VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();
            cml.Add(SaveAPDownpayment(DocNo, VoucherRequestDocNo));
            
            if (Grd2.Rows.Count > 1)
            {
                cml.Add(SaveAPDownpaymentDtl2(DocNo));
                //for (int r = 0; r < Grd2.Rows.Count; r++)
                //    if (Sm.GetGrdStr(Grd2, r, 1).Length > 0) 
                //        cml.Add(SaveAPDownpaymentDtl2(DocNo, r));
            }

            if (mIsAutoJournalActived && mIsAPDownpayment2JournalEnabled) cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            if (!mIsAPDPProcessTo1Journal)  InsertDataVR(DocNo);

            if (mIsAPDPAllowToUploadFile)
            {
                if (TxtFile.Text.Length > 1)
                    UploadFile(DocNo, TxtFile, PbUpload);
            }

            SetFormControl(mState.View);
            ShowData(DocNo);
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            bool IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("@Yr,'/', @Mth, '/', ");
            SQL.Append("( ");
            SQL.Append("    Select IfNull((");
            SQL.Append("    Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNo ");
            //SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
            //SQL.Append("        From ( ");
            //SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
            SQL.Append("            From TblVoucherRequestHdr ");
            SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
            SQL.Append("            Order By Substring(DocNo, 7, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("        ) As Temp ");
            SQL.Append("    ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("    ), '0001' ");
            SQL.Append(") As Number ");
            SQL.Append("), '/', ");
            SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
            SQL.Append(") As DocNo ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
            return Sm.GetValue(cm);
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    var SQL = new StringBuilder();
        //    SQL.Append("Select Concat( ");
        //    SQL.Append("@Yr,'/', @Mth, '/', ");
        //    SQL.Append("( ");
        //    SQL.Append("    Select IfNull((");
        //    SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
        //    SQL.Append("        From ( ");
        //    SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
        //    SQL.Append("            From TblVoucherRequestHdr ");
        //    SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
        //    SQL.Append("            Order By Substring(DocNo, 7, 5) Desc Limit 1 ");
        //    SQL.Append("        ) As Temp ");
        //    SQL.Append("    ), '0001') As Number ");
        //    SQL.Append("), '/', ");
        //    SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
        //    SQL.Append(") As DocNo ");
        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
        //    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
        //    return Sm.GetValue(cm);
        //}

        private void InsertDataVR(string APDocNo)
        {
            var cml = new List<MySqlCommand>();
            if (mIsAPDownpaymentUseCOA && Grd2.Rows.Count > 1)
            {
                var VoucherRequestDocNo2 = string.Empty;

                if (mVoucherCodeFormatType == "2")
                    VoucherRequestDocNo2 = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");
                else
                    VoucherRequestDocNo2 = GenerateVoucherRequestDocNo();

                cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo2, APDocNo));
                cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo2, APDocNo));
            }
            Sm.ExecCommands(cml);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsLueEmpty(LuePaymentType, "Payment Type") ||
                IsPaymentTypeNotValid() ||
                Sm.IsLueEmpty(LuePIC, "Person in charge") ||
                (mIsAPDownpaymentApprovalBasedOnDept && Sm.IsLueEmpty(LueDeptCode, "Department")) ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                IsAmtNotValid() ||
                (mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity"));
                
        }

        private bool IsAmtNotValid()
        {
            decimal POAmt = decimal.Parse(TxtPOAmt.Text);
            decimal OtherAmt = decimal.Parse(TxtOtherAmt.Text);
            decimal Amt = decimal.Parse(TxtAmt.Text);

            if ((OtherAmt+Amt) > POAmt)
            {
                if (Sm.StdMsgYN("Question",
                    "PO : " + Sm.FormatNum(POAmt, 0) + Environment.NewLine +
                    "Other Downpayment : " + Sm.FormatNum(OtherAmt, 0) + Environment.NewLine +
                    "Downpayment : " + Sm.FormatNum(Amt, 0) + Environment.NewLine + Environment.NewLine +
                    "AP downpayment amount is bigger than PO's amount." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No)
                    return true;
            }
            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private MySqlCommand SaveAPDownpayment(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* AP Downpayment - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            #region Save AP

            SQL.AppendLine("Insert Into TblAPDownpayment ");
            SQL.AppendLine("(DocNo, LocalDocNo, DocDt, CancelInd, Status, PODocNo, VoucherRequestDocNo, VoucherRequestDocNo2, CurCode, Amt, ");
            SQL.AppendLine("DownpaymentPercentage, AmtBefTax, ");
            SQL.AppendLine("TaxInvoiceNo, TaxInvoiceDt, TaxCode, TaxAmt, ");
            SQL.AppendLine("TaxInvoiceNo2, TaxInvoiceDt2, TaxCode2, TaxAmt2, ");
            SQL.AppendLine("TaxInvoiceNo3, TaxInvoiceDt3, TaxCode3, TaxAmt3, ");
            SQL.AppendLine("PIC, DeptCode, PaymentType, BankCode, GiroNo, OpeningDt, DueDt, PaymentUser, PaidToBankCode, PaidToBankBranch, PaidToBankAcName, PaidToBankAcNo, EntCode,  PaymentDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @LocalDocNo, @DocDt, 'N', 'O', @PODocNo, @VoucherRequestDocNo, @VoucherRequestDocNo2, @CurCode, @Amt, ");
            SQL.AppendLine("@DownpaymentPercentage, @AmtBefTax, ");
            SQL.AppendLine("@TaxInvoiceNo, @TaxInvoiceDt, @TaxCode, @TaxAmt, ");
            SQL.AppendLine("@TaxInvoiceNo2, @TaxInvoiceDt2, @TaxCode2, @TaxAmt2, ");
            SQL.AppendLine("@TaxInvoiceNo3, @TaxInvoiceDt3, @TaxCode3, @TaxAmt3, ");
            SQL.AppendLine("@PIC, @DeptCode, @PaymentType, @BankCode, @GiroNo, @OpeningDt, @DueDt, @PaymentUser, @PaidToBankCode, @PaidToBankBranch, @PaidToBankAcName, @PaidToBankAcNo, @EntCode, @PaymentDt, @Remark, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, @Dt ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='APDownpayment' ");
            if (mIsAPDownpaymentApprovalBasedOnDept)
            {
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select DeptCode From TblAPDownpayment Where DocNo=@DocNo ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblAPDownpayment A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select D1.CurCode1, D1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate D1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblAPDownpayment Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='APDownpayment' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            #endregion 

            #region Save VR utama

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, ");
            SQL.AppendLine("DeptCode, DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, GiroNo, BankCode, OpeningDt, DueDt, PIC, DocEnclosure, CurCode, Amt, PaymentUser, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, @DocDt, 'N', 'O', ");
            if (mIsAPDownpaymentApprovalBasedOnDept)
                SQL.AppendLine("@DeptCode, ");
            else
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='APDownPaymentDeptCode'), ");
            SQL.AppendLine("'04', Null, 'C', Null, @PaymentType, @GiroNo, @BankCode, @OpeningDt, @DueDt, @PIC, 0, @CurCode, " + 
                (mIsAPDPProcessTo1Journal ? "@Amt2" : "@Amt") + 
                ", Null, Concat('AP Downpayment ', @APDocNo, ' (', @VdName, '). ', @Remark), @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, '001', Concat('AP Downpayment ', @APDocNo, ' (', @VdName, '). ', @Remark), @Amt, Null, @UserCode, @Dt); ");

            if (mIsAPDPProcessTo1Journal && Decimal.Parse(TxtCOAAmt.Text) != 0)
            {
                SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@VoucherRequestDocNo, '002', 'Vendor Account Payable Downpayment', @Amt3, Null, @UserCode, @Dt); ");
            }

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='APDownPayment' "); 
            SQL.AppendLine("    And DocNo=@APDocNo ");
            SQL.AppendLine("    ); ");

            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PODocNo", mIsUsePOForServiceDocNo ? mPODocNo : TxtPODocNo.Text);
            Sm.CmParam<String>(ref cm, "@APDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@VdName", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt.Text) + Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt3", Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@OpeningDt", Sm.GetDte(DteOpeningDt));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
            if (Sm.GetLue(LuePaidToBankCode).Length == 0)
                Sm.CmParam<String>(ref cm, "@PaidToBankCode", "");
            else
            {
                if (mIsAPDownpaymentPaidToBankBasedOnVendor)
                    Sm.CmParam<String>(ref cm, "@PaidToBankCode", Sm.Left(Sm.GetLue(LuePaidToBankCode), Sm.GetLue(LuePaidToBankCode).Length - 3));
                else
                    Sm.CmParam<String>(ref cm, "@PaidToBankCode", Sm.GetLue(LuePaidToBankCode));
            }
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch", TxtPaidToBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName", TxtPaidToBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", TxtPaidToBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@DownpaymentPercentage", decimal.Parse(TxtDownpaymentPercentage.Text));
            Sm.CmParam<Decimal>(ref cm, "@AmtBefTax", decimal.Parse(TxtAmtBefTax.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LueTaxCode));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", TxtTaxInvoiceNo.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", decimal.Parse(TxtTaxAmt.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", TxtTaxInvoiceNo2.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", Sm.GetDte(DteTaxInvoiceDt2));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtTaxAmt2.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", TxtTaxInvoiceNo3.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", Sm.GetDte(DteTaxInvoiceDt3));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", decimal.Parse(TxtTaxAmt3.Text));
            Sm.CmParamDt(ref cm, "@PaymentDt", Sm.GetDte(DtePaymentDt));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveAPDownpaymentDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* AP Downpayment - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblAPDownPaymentDtl(DocNo, DNo, AcNo, DAmt, CAmt, ActInd, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(" (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @ActInd_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 4));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 5));
                    Sm.CmParam<String>(ref cm, "@ActInd_" + r.ToString(), Sm.GetGrdBool(Grd2, r, 8) ? "Y" : "N");
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code

        //private MySqlCommand SaveAPDownpaymentDtl2(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblAPDownPaymentDtl(DocNo, DNo, AcNo, DAmt, CAmt, ActInd, Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @ActInd, @Remark, @UserCode, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd2, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd2, Row, 3));
        //    Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd2, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@ActInd", Sm.GetGrdBool(Grd2, Row, 8) ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        //save vr additional COA
        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string APDocNo)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, OpeningDt, DueDt, ");
            SQL.AppendLine("PIC, CurCode, Amt, PaymentUser, EntCode, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', 'N', ");
            if (mIsAPDownpaymentApprovalBasedOnDept)
                SQL.AppendLine("@DeptCode, ");
            else
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='APDownPaymentDeptCode'), ");
            SQL.AppendLine("'19', Null, ");
            SQL.AppendLine("'C', @PaymentType, Null, @BankCode, @GiroNo, @OpeningDt, @DueDt, ");
            SQL.AppendLine("@PIC, @CurCode, @Amt, null, @EntCode, Concat('AP Downpayment ', @APDocNo, '. ', @Remark),");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");

            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='VoucherRequest' ");
            if (mIsAPDownpaymentApprovalBasedOnDept) SQL.AppendLine("And T.DeptCode=@DeptCode ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblAPDownpayment A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select D1.CurCode1, D1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate D1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");
            
            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequest' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblAPDownPayment Set VoucherRequestDocNo2=@DocNo ");
            SQL.AppendLine("Where DocNo=@APDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@APDocNo", APDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            //Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetParameter("IncomingPaymentDeptCode"));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@OpeningDt", Sm.GetDte(DteOpeningDt));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string VoucherRequestDocNo, string APDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, Concat('AP Downpayment ', @APDocNo, ' (', @VdName, '). ', @Remark), @Amt, Null, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@APDocNo", APDocNo);
            Sm.CmParam<String>(ref cm, "@VdName", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var CurCode = Sm.GetLue(LueCurCode);

            SQL.AppendLine("Set @Row:=0; ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");
            SQL.AppendLine("Set @JournalDocNo:=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblAPDownpayment Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, @DocDt, ");
            SQL.AppendLine("Concat('Vendor AP Downpayment : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@Remark, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");

            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, Null As Remark, @UserCode As CreateBy, @Dt As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            if (mAPDownpayment2JournalFormula == "1")
            {
                SQL.AppendLine("        Select Concat(B.ParValue, C.VdCode) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        A.Amt As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Inner Join TblPOHdr C On A.PODocNo=C.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.CurCode=@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Concat(B.ParValue, C.VdCode) As AcNo, ");
                SQL.AppendLine("        A.AmtBefTax As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoDownPayment' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Inner Join TblPOHdr C On A.PODocNo=C.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.CurCode=@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        A.TaxAmt As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode=B.TaxCode And B.AcNo1 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.TaxCode Is Not Null ");
                SQL.AppendLine("        And A.CurCode=@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        A.TaxAmt2 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo1 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.TaxCode2 Is Not Null ");
                SQL.AppendLine("        And A.CurCode=@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        A.TaxAmt3 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo1 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.TaxCode3 Is Not Null ");
                SQL.AppendLine("        And A.CurCode=@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo, B.DAmt, B.CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblAPDownpaymentDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.CurCode=@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Concat(B.ParValue, C.VdCode) As AcNo, "); ;
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.Amt As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Inner Join TblPOHdr C On A.PODocNo=C.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Concat(B.ParValue, C.VdCode) As AcNo, ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.AmtBefTax As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoDownPayment' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Inner Join TblPOHdr C On A.PODocNo=C.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode=B.TaxCode And B.AcNo1 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.TaxCode Is Not Null ");
                SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt2 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo1 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.TaxCode2 Is Not Null ");
                SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt3 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo1 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.TaxCode3 Is Not Null ");
                SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo, ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        B.DAmt, ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        B.CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblAPDownpaymentDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForForeignCurrencyExchangeGains' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.CurCode<>@MainCurCode ");
            }

            else
            {
                SQL.AppendLine("        Select Concat(B.ParValue, C.VdCode) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        A.Amt As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Inner Join TblPOHdr C On A.PODocNo=C.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.CurCode=@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Concat(B.ParValue, C.VdCode) As AcNo, ");
                SQL.AppendLine("        Case When A.AmtBefTax >= 0.00 Then A.AmtBefTax Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When A.AmtBefTax < 0.00 Then A.AmtBefTax*-1.00 Else 0.00 End As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoDownPayment' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Inner Join TblPOHdr C On A.PODocNo=C.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.CurCode=@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        Case When  A.TaxAmt >= 0.00 Then  A.TaxAmt Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When  A.TaxAmt < 0.00 Then  A.TaxAmt*-1.00 Else 0.00 End As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode=B.TaxCode And B.AcNo1 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.TaxCode Is Not Null ");
                SQL.AppendLine("        And A.CurCode=@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        Case When  A.TaxAmt2 >= 0.00 Then  A.TaxAmt2 Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When  A.TaxAmt2 < 0.00 Then  A.TaxAmt2*-1.00 Else 0.00 End As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo1 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.TaxCode2 Is Not Null ");
                SQL.AppendLine("        And A.CurCode=@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        Case When  A.TaxAmt3 >= 0.00 Then  A.TaxAmt3 Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When  A.TaxAmt3 < 0.00 Then  A.TaxAmt3*-1.00 Else 0.00 End As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo1 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.TaxCode3 Is Not Null ");
                SQL.AppendLine("        And A.CurCode=@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo, ");
                SQL.AppendLine("        Case When  B.DAmt >= 0.00 Then B.DAmt Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When  B.DAmt < 0.00 Then (B.DAmt*-1.00)+B.CAmt Else B.CAmt End As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblAPDownpaymentDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.CurCode=@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Concat(B.ParValue, C.VdCode) As AcNo, "); ;
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.Amt As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Inner Join TblPOHdr C On A.PODocNo=C.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Concat(B.ParValue, C.VdCode) As AcNo, ");
                SQL.AppendLine("        Case When  ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.AmtBefTax >= 0.00 Then ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.AmtBefTax Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When  ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.AmtBefTax < 0.00 Then ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.AmtBefTax *-1.00 Else 0.00 End As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoDownPayment' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Inner Join TblPOHdr C On A.PODocNo=C.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        Case When  ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt >= 0.00 Then ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When  ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt < 0.00 Then ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt *-1.00 Else 0.00 End As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode=B.TaxCode And B.AcNo1 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.TaxCode Is Not Null ");
                SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        Case When  ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt2 >= 0.00 Then ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt2 Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When  ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt2 < 0.00 Then ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt2 *-1.00 Else 0.00 End As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo1 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.TaxCode2 Is Not Null ");
                SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        Case When  ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt3 >= 0.00 Then ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt3 Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When  ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt3 < 0.00 Then ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        A.TaxAmt3 *-1.00 Else 0.00 End As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo1 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.TaxCode3 Is Not Null ");
                SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo, ");
                SQL.AppendLine("        Case When  ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        B.DAmt >= 0 Then ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        B.DAmt Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When  ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        B.DAmt < 0 Then ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        (B.DAmt*-1.00)+B.CAmt Else B.CAmt End As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblAPDownpaymentDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblAPDownpayment A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForForeignCurrencyExchangeGains' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.CurCode<>@MainCurCode ");
            }


            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo  ");
            SQL.AppendLine(") T;  ");

            SQL.AppendLine("Update TblJournalDtl A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select DAmt, CAmt From (");
            SQL.AppendLine("        Select Sum(DAmt) As DAmt, Sum(CAmt) As CAmt ");
            SQL.AppendLine("        From TblJournalDtl Where DocNo=@JournalDocNo ");
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0 End, ");
            SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0 End ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
            SQL.AppendLine("And A.AcNo In ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
            SQL.AppendLine("    And ParValue Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditAPDownpayment());

            if (mIsAutoJournalActived && mIsAPDownpayment2JournalEnabled) cml.Add(SaveJournal2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived && mIsAPDownpayment2JournalEnabled, true, Sm.GetDte(DteDocDt)) ||
                IsDataAlreadyCancelled() ||
                (!mIsAPDownpaymentAbleToEditTax && Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation")) ||
                IsDataAlreadyProcessed();
        }

        private bool IsDataNotApproved()
        { 
            bool IsApproved =
                Sm.IsDataExist(
                "Select 1 from TblAPDownpayment Where DocNo=@Param And CancelInd='N' And Status='A';",
                TxtDocNo.Text);

            if (!IsApproved)
            {
                Sm.StdMsg(mMsgType.Warning, "This document has not been approved yet.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 From TblAPDownpayment Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private bool IsDataAlreadyProcessed()
        {
            return Sm.IsDataExist(
                "Select 1 " +
                "From TblVoucherRequestHdr A, TblVoucherHdr B " +
                "Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' And A.DocNo=@Param;",
                TxtVoucherRequestDocNo.Text,
                "This document already processed into voucher.");
        }

        private MySqlCommand EditAPDownpayment()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAPDownpayment Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason=@CancelReason, Amt=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status In ('O', 'A'); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set CancelInd=@CancelInd, CancelReason=@CancelReason, Amt=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status In ('O', 'A'); ");

            SQL.AppendLine("Update TblVoucherRequestDtl Set Amt=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo; ");

            SQL.AppendLine("Delete From TblAPDownPaymentDtl2 Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Set @JournalDocNo:=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblAPDownpayment Set JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblAPDownpayment Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblAPDownpayment Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }

        #endregion

        #endregion

        #region Additional Method

        private void SetLuePaidToBankCode(ref DXE.LookUpEdit Lue, string VdCode, string Type)
        {
            try
            {
                //Type = 1 -> For View
                //Type = 2 -> For Insert

                var SQL = new StringBuilder();

                if (Type == "1")
                {
                    SQL.AppendLine("Select BankCode As Col1, BankName As Col2, ");
                    SQL.AppendLine("'-' As Col3, 'xxx' As Col4  ");
                    SQL.AppendLine("From TblBank Order By BankName;");
                }
                else
                {
                    SQL.AppendLine("Select Concat(A.BankCode, A.DNo) As Col1, B.BankName As Col2, ");
                    SQL.AppendLine("Trim(Concat(");
                    SQL.AppendLine("    Case When IfNull(A.BankBranch,'')='' Then '' Else Concat('Branch : ', A.BankBranch) End,  ");
                    SQL.AppendLine("    Case When IfNull(A.BankAcName,'')='' Then '' Else Concat(Case When IfNull(A.BankBranch,'')='' Then '' Else ', ' End, 'Name : ', A.BankAcName) End,  ");
                    SQL.AppendLine("    Case When IfNull(A.BankAcNo,'')='' Then '' Else Concat(Case When IfNull(A.BankBranch,'')='' Or IfNull(A.BankAcName,'')='' Then '' Else ', ' End, 'Account Number : ', A.BankAcNo) End ");
                    SQL.AppendLine(")) As Col3, ");
                    SQL.AppendLine("A.DNo As Col4  ");
                    SQL.AppendLine("From TblVendorBankAccount A ");
                    SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
                    SQL.AppendLine("Where VdCode='" + VdCode + "' ");
                    SQL.AppendLine("Order By B.BankName, A.DNo; ");
                }
                Sm.SetLue4(
                    ref Lue,
                    SQL.ToString(),
                    0, 170, 300, 0, false, true, true, false,
                    "Code", "Bank", "Bank Branch/Account Name/Account No.", "DNo", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowPaidToBankCodeInfo(string VdCode, string DNo)
        {
            var cm = new MySqlCommand();
            try
            {
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select BankBranch, BankAcName, BankAcNo " +
                        "From TblVendorBankAccount " +
                        "Where VdCode=@VdCode And DNo=@DNo; ",
                        new string[] { "BankBranch", "BankAcName", "BankAcNo" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPaidToBankBranch.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPaidToBankAcName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtPaidToBankAcNo.EditValue = Sm.DrStr(dr, c[2]);
                        }, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetForm(string VdCode)
        {
            if (mIsAPDownpaymentPaidToBankBasedOnVendor)
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo
                    });
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode }, true);
                if (VdCode.Length != 0)
                {
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode }, false);
                    SetLuePaidToBankCode(ref LuePaidToBankCode, VdCode, "2");
                }
            }
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ComputeCOAAmt()
        {
            decimal COAAmt = 0m;
            try
            {
                var SQL = new StringBuilder();

                if (mIsAPDPProcessTo1Journal)
                {
                    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0 && Sm.GetGrdBool(Grd2, Row, 8))
                        {

                            if (Sm.GetGrdBool(Grd2, Row, 6) && Sm.GetGrdDec(Grd2, Row, 3) != 0)
                            {
                                //kalau dicentang di debit, (+)
                                COAAmt += Sm.GetGrdDec(Grd2, Row, 3);
                            }
                            if (Sm.GetGrdBool(Grd2, Row, 7) && Sm.GetGrdDec(Grd2, Row, 4) != 0)
                            {
                                //kalau dicentang di credit, (-)
                                COAAmt -= Sm.GetGrdDec(Grd2, Row, 4);
                            }
                        }
                    }
                }
                else
                {
                    SQL.AppendLine("SELECT CONCAT( ");
                    SQL.AppendLine("   C.ParValue, A.VdCode ");
                    SQL.AppendLine(") AS AcNo ");
                    SQL.AppendLine("FROM TblPOHdr A ");
                    SQL.AppendLine("INNER JOIN TblVendor B ON A.VdCode = B.VdCode ");
                    SQL.AppendLine("LEFT JOIN TblParameter C ON C.Parcode = 'VendorAcNoAP' ");
                    SQL.AppendLine("WHERE A.DocNo = @PODocNo LIMIT 1; ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@PODocNo", mIsUsePOForServiceDocNo ? mPODocNo : TxtPODocNo.Text);
                    var AcNo = Sm.GetValue(cm);

                    cm.CommandText = "Select AcType From TblCOA Where AcNo=@AcNo;";
                    Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
                    string AcType = Sm.GetValue(cm);

                    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    {
                        if (AcNo.Length > 0 && Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd2, Row, 1)))
                        {
                            if (Sm.GetGrdDec(Grd2, Row, 3) != 0)
                            {
                                if (AcType == "D")
                                    COAAmt += Sm.GetGrdDec(Grd2, Row, 3);
                                else
                                    COAAmt -= Sm.GetGrdDec(Grd2, Row, 3);
                            }
                            if (Sm.GetGrdDec(Grd2, Row, 4) != 0)
                            {
                                if (AcType == "C")
                                    COAAmt += Sm.GetGrdDec(Grd2, Row, 4);
                                else
                                    COAAmt -= Sm.GetGrdDec(Grd2, Row, 4);
                            }
                        }
                    }
                }
                TxtCOAAmt.EditValue = Sm.FormatNum(COAAmt, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeOtherAmt()
        {
            var OtherAmt = string.Empty;

            OtherAmt = Sm.GetValue(
            "Select Amt From TblAPDownpayment " +
            "Where PODocNo=@Param And CancelInd='N' And Status In ('O', 'A');",
             mIsUsePOForServiceDocNo ? mPODocNo : TxtPODocNo.Text);
           
            if (OtherAmt.Length == 0) OtherAmt = "0";
            TxtOtherAmt.EditValue = Sm.FormatNum(decimal.Parse(OtherAmt), 0);
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex - 1));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void ComputeAmtAftTax()
        {
            decimal 
                POAmt = 0m,
                DownpaymentPercentage = 0m, 
                AmtBefTax = 0m, 
                TaxAmt = 0m,
                TaxAmt2 = 0m,
                TaxAmt3 = 0m
                ;
            string 
                TaxCode = Sm.GetLue(LueTaxCode),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3)
                ;

            POAmt = GetAmtBefTax();

            if (TxtDownpaymentPercentage.Text.Length > 0) DownpaymentPercentage = decimal.Parse(TxtDownpaymentPercentage.Text);
            AmtBefTax = DownpaymentPercentage * 0.01m * POAmt;
            TxtAmtBefTax.EditValue = Sm.FormatNum(AmtBefTax, 0);
            
            if (TaxCode.Length != 0)
            {
                var TaxRate = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode);
                if (TaxRate!=0) TaxAmt = TaxRate*0.01m*AmtBefTax;
            }
            if (TaxCode2.Length != 0)
            {
                var TaxRate2 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode2);
                if (TaxRate2 != 0) TaxAmt2 = TaxRate2 * 0.01m * AmtBefTax;
            }
            if (TaxCode3.Length != 0)
            {
                var TaxRate3 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode3);
                if (TaxRate3 != 0) TaxAmt3 = TaxRate3 * 0.01m * AmtBefTax;
            }
            
            TxtTaxAmt.EditValue = Sm.FormatNum(TaxAmt, 0);
            TxtTaxAmt2.EditValue = Sm.FormatNum(TaxAmt2, 0);
            TxtTaxAmt3.EditValue = Sm.FormatNum(TaxAmt3, 0);
            TxtAmtAftTax.EditValue = Sm.FormatNum(AmtBefTax + TaxAmt + TaxAmt2 + TaxAmt3, 0);
            TxtAmt.EditValue = TxtAmtAftTax.EditValue;
        }

        private decimal GetAmtBefTax()
        {
            var SQL = new StringBuilder();
            var Amt = 0m;

            SQL.AppendLine("Select ");
            SQL.AppendLine("IfNull(B.Total, 0.00)-IfNull(A.DiscountAmt, 0.00)+IfNull(A.CustomsTaxAmt, 0.00) As Amt ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Inner join ( ");
            SQL.AppendLine("    Select B.DocNo, ");
            SQL.AppendLine("    Sum((H.UPrice*(B.Qty-IfNull(M.Qty, 0.00)))-(H.UPrice*(B.Qty-IfNull(M.Qty, 0.00))*B.Discount*0.01)-B.DiscountAmt+B.RoundingValue) As Total ");
            SQL.AppendLine("    From TblPODtl B ");
            SQL.AppendLine("    Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo  ");
            SQL.AppendLine("    Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo  ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo  ");
            SQL.AppendLine("    Inner Join TblQtHdr G On D.QtDocNo=G.DocNo  ");
            SQL.AppendLine("    Inner Join TblQtDtl H On D.QtDocNo=H.DocNo And D.QtDNo=H.DNo  ");
            SQL.AppendLine("    Left Join TblPOQtyCancel M On B.Docno=M.PODocno And B.DNo=M.PODno And M.CancelInd='N' ");
            SQL.AppendLine("    Where B.CancelInd='N' ");
            SQL.AppendLine("    And B.DocNo=@Param ");
            SQL.AppendLine("    Group By B.DocNo ");
            SQL.AppendLine(") B On A.Docno=B.DocNo ");
            SQL.AppendLine("Where A.DocNo Not In (");
            SQL.AppendLine("    Select Distinct T3.PODocNo ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr T1 ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T3 On T2.RecvVdDocNo=T3.DocNo And T2.RecvVdDNo=T3.DNo ");
            SQL.AppendLine("    Inner Join TblPOHdr T4 On T3.PODocNo=T4.DocNo ");
            SQL.AppendLine("        And T4.DocNo=@Param ");
            SQL.AppendLine("        And T4.Status='A' ");
            SQL.AppendLine("    Where T1.CancelInd = 'N' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And A.DocNo=@Param ");
            SQL.AppendLine("And A.Status='A' ");

            Amt = Sm.GetValueDec(SQL.ToString(), mIsUsePOForServiceDocNo ? mPODocNo : TxtPODocNo.Text);
            return Amt;
        }


        #endregion

        #region Ftp

        private void AddFile(DevExpress.XtraEditors.TextEdit TxtFile, DevExpress.XtraEditors.CheckEdit ChkFile)
        {
            try
            {
                ChkFile.Checked = true;
                OD1.InitialDirectory = "c:";
                OD1.Filter = "PDF files (*.pdf)|*.pdf";
                OD1.FilterIndex = 2;
                OD1.ShowDialog();

                TxtFile.Text = OD1.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void UploadFile(string DocNo, DevExpress.XtraEditors.TextEdit TxtFile, System.Windows.Forms.ProgressBar PbUpload)
        {
            if (IsUploadFileNotValid(TxtFile.Text)) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateAttachmentFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private MySqlCommand UpdateAttachmentFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAPDownPayment Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private void DownloadFileKu(DevExpress.XtraEditors.TextEdit TxtFile, System.Windows.Forms.ProgressBar PbUpload)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, PbUpload);
            SFD1.FileName = TxtFile.Text;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD1.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared, System.Windows.Forms.ProgressBar PbUpload)
        {
            downloadedData = new byte[0];

            try
            {
                
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

              
                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private bool IsUploadFileNotValid(string FileName)
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid(FileName)
                //IsFileNameAlreadyExisted(FileName)
             ;
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsAPDPAllowToUploadFile && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsAPDPAllowToUploadFile && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsAPDPAllowToUploadFile && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsAPDPAllowToUploadFile && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(string FileName)
        {
            if (mIsAPDPAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(string FileName)
        {
            if (mIsAPDPAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo From TblAPDownpayment A ");
                SQL.AppendLine("Where A.FileName=@FileName ");
                SQL.AppendLine("And A.CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnPODocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAPDownpayment2Dlg(this));
        }
 
        private void BtnPODocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPODocNo, "PO#", false))
            {
                try
                {
                    if (mIsUsePOForServiceDocNo && TxtPODocNo.Text.Contains("POS-LOG"))
                    {
                        var f = new FrmMaterialRequest4(mMenuCode);
                        f.Tag = mMenuCode;
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = TxtPODocNo.Text;
                        f.ShowDialog();
                    }
                    else
                    {
                        var f = new FrmPO(mMenuCode);
                        f.Tag = mMenuCode;
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = TxtPODocNo.Text;
                        f.ShowDialog();
                    }
                    
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher request#", false))
            {
                try
                {
                    var f = new FrmVoucherRequest(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherRequestDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                try
                {
                    var f = new FrmVoucher(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmAPDownpayment2Dlg2(this));
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmAPDownpayment2Dlg2(this));
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                Grd2.Cells[e.RowIndex, 4].Value = 0;
                ComputeCOAAmt();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                Grd2.Cells[e.RowIndex, 3].Value = 0;
                ComputeCOAAmt();
            }

            if (e.ColIndex == 6 && Sm.GetGrdBool(Grd2, e.RowIndex, 6))
                Grd2.Cells[e.RowIndex, 7].Value = false;

            if (e.ColIndex == 7 && Sm.GetGrdBool(Grd2, e.RowIndex, 7))
                Grd2.Cells[e.RowIndex, 6].Value = false;

            if (e.ColIndex == 6 || e.ColIndex == 7)
                Grd2.Cells[e.RowIndex, 8].Value = Sm.GetGrdBool(Grd2, e.RowIndex, e.ColIndex);

            if (Sm.IsGrdColSelected(new int[] { 3, 4, 6, 7 }, e.ColIndex))
                ComputeCOAAmt();
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            ComputeCOAAmt();
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        private void BtnUpload_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile, ChkFile);
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            if (TxtFile.Text.Length > 0)
                DownloadFileKu(TxtFile, PbUpload);
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }
        
        #endregion

        #region Misc Control Event

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept?"Y":"N");
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmt, 0);
        }


        private void TxtTaxAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtTaxAmt, 0);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }
       
        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteOpeningDt, DteDueDt });

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, true);
                    Sm.SetControlReadOnly(DteOpeningDt, true);
                    Sm.SetControlReadOnly(DteDueDt, true);
                    return;
                }

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, false);
                    Sm.SetControlReadOnly(DteOpeningDt, false);
                    Sm.SetControlReadOnly(DteDueDt, false);
                    return;
                }

                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteOpeningDt, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void TxtPaymentUser_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaymentUser);
        }

        private void LuePaidToBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsAPDownpaymentPaidToBankBasedOnVendor)
                {
                    Sm.RefreshLookUpEdit(LuePaidToBankCode, new Sm.RefreshLue3(SetLuePaidToBankCode), mVdCode, "2");
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo });
                    if (Sm.GetLue(LuePaidToBankCode).Length != 0)
                        ShowPaidToBankCodeInfo(
                            mVdCode,
                            LuePaidToBankCode.GetColumnValue("Col4").ToString()
                            );
                }
                else
                    Sm.RefreshLookUpEdit(LuePaidToBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
            }
        }

        private void TxtPaidToBankBranch_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankBranch);
        }

        private void TxtPaidToBankAcName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankAcName);
        }

        private void TxtPaidToBankAcNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankAcNo);
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue3(Sl.SetLueEntCode), string.Empty, mIsFilterByEnt ? "Y" : "N");
        }

        private void TxtTaxInvoiceNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo);
        }

        private void TxtDownpaymentPercentage_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDownpaymentPercentage, 2);
                ComputeAmtAftTax();
            }
        }

        private void TxtAmtBefTax_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtAmtBefTax, 0);
                ComputeAmtAftTax();
            }
        }

        private void LueTaxCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

        private void TxtTaxInvoiceNo2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo2);
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

        private void TxtTaxInvoiceNo3_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo3);
        }

        #endregion

        #endregion

        #region Report Class

        class APDPHdr
        {
         public string CompanyLogo { get; set; }
         public string Company{ get; set; }
         public string Address{ get; set; }
         public string Phone{ get; set; }
         public string DocNo{ get; set; }
         public string DocDt{ get; set; }
         public string PODocNo{ get; set; }
         public string POCurCode{ get; set; }
         public decimal POAmt { get; set; }
         public string VDName{ get; set; }
         public string VoucherRequestDocNo{ get; set; }
         public string VoucherDocNo{ get; set; }
         public string CreateBy{ get; set; }
         public string DeptName{ get; set; }
         public string SiteName{ get; set; }
         public string StatusDEsc{ get; set; }
         public decimal Amt { get; set; }
         public string CreateDt { get; set; }
         public string Remark{ get; set; }
         public string SiteInd { get; set; }
         public string PrintBy { get; set; }
         public string PIC { get; set; }
         public string PaymentUser { get; set; }
         public string GiroNo { get; set; }
         public string GiroDt { get; set; }
         public string GiroBankName { get; set; }
         public string PaymentType { get; set; }
         public string Terbilang { get; set; }
         public string PaidToBankName { get; set; }
         public string PaidToBankBranch { get; set; }
         public string PaidToBankAcName { get; set; }
         public string PaidToBankAcNo { get; set; }
         public string VdAddress { get; set; }
         public string VdCity { get; set; }
         public string VRLocal { get; set; }
         public string ProjectName { get; set; }
         public string PORemark { get; set; }
         public string UserName { get; set; }
         public string CurName { get; set; } 
        }

        class APDPJournal
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        class APDPDtl
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        class APDPSignIMS
        {
            public string UserName { get; set; }
            public string UserName2 { get; set; }
            public string UserName3 { get; set; }
        }
      
        #endregion
    }
}
