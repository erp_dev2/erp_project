﻿#region Update
/*
    04/13/2017 [HAR] tambah fiter parameter untuk misahin status, jika exim status ambil dri MR hdr, kalo non exim status ambio dri MR dtl
    04/20/2017 [HAR] tambah container name, seal name, amount, 
    01/08/2017 [HAR] tambah ap dowpayment, filter local, filter vendor, tampah outstanding outgoingpayment
    08/07/2017 [HAR] ubah nama filter
    23/08/2017 [HAR] destinasi yang FOB juga diambil dari city untuk DR dan port untuk shipment instruction
    17/10/2017 [HAR] bug fixing data double 
    13/02/2018 [HAR] bug fixing seal keisi container
    08/08/2018 [TKG] bug fixing data exim yg sudah divoucher kan tidak muncul di kolom voucher
    19/11/2018 [WED] tambah kolom PEB dan PEB Date
    02/05/2019 [WED] BUG data OP masih salah grouping
    07/05/2018 [TKG] bug saat menghitung received quantity dan container/packaging.
    03/07/2019 [WED] bug di subquery G (nilai recvvd)
    05/07/2019 [WED] bug sum(qty) subquery G
    01/10/2019 [DITA/IOK] bug saat refresh data
    14/07/2020 [TKG/IOK] bug saat menampilkan data multi PI dan multi OP
    08/02/2021 [WED/IOK] tambah informasi kolom BL dari Shipment Instruction
    01/10/2021 [VIN/IOK] Bug: Data terdouble
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMaterialRequestEximStatus : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptMaterialRequestEximStatus(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            //SetSQL();
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
            Sl.SetLueCtCode(ref LueCtCode);
            Sl.SetLueVdCode(ref LueVdCode);
            base.FrmLoad(sender, e);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.LocalDocNo, IfNull(T.CtCode, U.CtCode) As CtCode, IfNull(T.Ctname,U.CtName) As CtName, ");
            SQL.AppendLine("A.DocNo, A.DocDt, B.DNo, A.DeptCode, C.DeptName, M.vdCode, O.Vdname, ");
            SQL.AppendLine("B.ItCode, D.Itname, D.ItName, B.Qty-ifnull(QtyCancel, 0) As Qty, D.PurchaseUomCode,  ");
            if (Sm.GetParameter("IsMREximSplitDocument") == "Y")
            {
                SQL.AppendLine("Case B.Status When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' When 'A' Then 'Approved' End As StatusMR, ");
            }
            else
            {
                SQL.AppendLine("Case A.Status When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' When 'A' Then 'Approved' End As StatusMR, ");
            }
            SQL.AppendLine("I.UserMr, IfNull(E.PORequestQty, 0) As PORequestQty, E.StatusPOR, E.UserPOR, ");
            SQL.AppendLine("IfNull(F.POQty, 0) As POQty, IfNull(RecvVdQty, 0) As RecvVdQty, ifnull(J.QtyReturn, 0) As QtyReturn, ");
            SQL.AppendLine("Q.PIDocNo, R.OPDocNo As OPDocNo, R.StatusOp, W.UserOP, R.VRDocNo As VoucherRequestDocNo,  R.VcDocNo As VoucherDocNo, ");
            SQL.AppendLine("S.CntName, S.SealName, S.SpStfDt, ((((100 - B.Discount) / 100) * (B.Qty * C2.UPrice)) - B.DiscountAmt + B.RoundingValue) As Amt, ");
            SQL.AppendLine("IfNull (A.SIDocNo, A.DRDocNo)As SIDocNo, IfNull(T.Tujuan,U.Tujuan)As Tujuan, IfNull(T.Cnt, U.QtyPackagingUnit)As Cnt, ");
            SQL.AppendLine("IfNull(T.Status,U.Status)As Status, ifnull(V.AmtAPDP, 0) As AMtApDp, IfNull(Q.AmtPI, 0.00)-IfNull(R.AmtOP, 0.00) As AmtOuts,  ");
            SQL.AppendLine("S.PEB, S.PEBDt, S.SpBLNo ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.QtDocNo is not null ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Inner Join TblQtDtl C2 On B.QtDocNo = C2.DocNo And B.QtDno = C2.Dno ");
            SQL.AppendLine("Inner Join TblItem D ");
            SQL.AppendLine("    On B.ItCode=D.ItCode ");
            SQL.AppendLine("    And Exists( ");
	        SQL.AppendLine("        Select 1 from Tblitem ");
            SQL.AppendLine("        Where ItCode=D.ItCode  ");
	        SQL.AppendLine("        And ItCtCode = ( ");
            SQL.AppendLine("            Select ParValue From tblparameter  ");
            SQL.AppendLine("            Where ParCode = 'ItCtExim') ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1A.MaterialRequestDocNo, T1A.MaterialRequestDNo, T1A.UserPOR, ");
            SQL.AppendLine("    Group_concat(Distinct T1A.StatusPOR Separator ', ') As StatusPOR, ");
            SQL.AppendLine("    Sum(T1A.Qty) As PORequestQty ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T1.MaterialRequestDocNo, T1.MaterialRequestDNo, T1.Qty, ");
            SQL.AppendLine("        Case T1.Status When 'C' Then 'Cancel' When 'O' Then 'Outstanding' When 'A' Then 'Approved' ");
            SQL.AppendLine("        End As  StatusPOR, T3.UserPOR ");
            SQL.AppendLine("        From TblPORequestDtl T1  ");
            SQL.AppendLine("        Inner Join TblMaterialRequestHdr T2 ");
            SQL.AppendLine("            On T1.MaterialRequestDocNo=T2.DocNo ");
            SQL.AppendLine("            And T2.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("        Select A.DocNo, A.DNo, Case When A.LastUpDt Is Not Null Then ");
            SQL.AppendLine("            Group_Concat(B.UserName, ' (', Concat(Substring(A.LastUpDt, 7, 2), '/', Substring(A.LastUpDt, 5, 2), '/', Left(A.LastUpDt, 4)) , ')')  ");
            SQL.AppendLine("            else  null end As UserPOR ");
            SQL.AppendLine("            From TblDocApproval A ");
            SQL.AppendLine("            Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("            Where A.DocType = 'PORequest' ");
            SQL.AppendLine("        Group By DocNo, DNo ");
            SQL.AppendLine("        )T3 On T1.DocNo = T3.DocNo And T1.DNo = T3.DNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' And T1.Status<>'C' ");

            SQL.AppendLine("    ) T1A ");
            SQL.AppendLine("    Group By T1A.MaterialRequestDocNo, T1A.MaterialRequestDNo, T1A.UserPOR ");
            SQL.AppendLine(") E On A.DocNo=E.MaterialRequestDocNo And B.DNo=E.MaterialRequestDNo ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.MaterialRequestDocNo, T2.MaterialRequestDNo, Sum(T1.Qty) As POQty ");
            SQL.AppendLine("    From TblPODtl T1 ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T2 On T1.PORequestDocNo=T2.DocNo And T1.PORequestDNo=T2.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr T3 ");
            SQL.AppendLine("        On T2.MaterialRequestDocNo=T3.DocNo ");
            SQL.AppendLine("        And T3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.MaterialRequestDocNo, T2.MaterialRequestDNo ");
            SQL.AppendLine(") F On A.DocNo=F.MaterialRequestDocNo And B.DNo=F.MaterialRequestDNo ");

            #region Old Code
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select T1.DocNo, T1.DNo, T3.MaterialRequestDocNo, T3.MaterialRequestDNo, T5.PIDOcNo, Sum(T1.Qty) As RecvVdQty ");
            //SQL.AppendLine("    From TblRecvVdDtl T1 ");
            //SQL.AppendLine("    Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd = 'N' ");
            //SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.PORequestDocNo=T3.DocNo And T2.PORequestDNo=T3.DNo And T3.CancelInd = 'N' ");
            //SQL.AppendLine("    Inner Join TblMaterialRequestHdr T4 ");
            //SQL.AppendLine("        On T3.MaterialRequestDocNo=T4.DocNo ");
            //SQL.AppendLine("        And T4.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("    Left Join ( ");
            //SQL.AppendLine("        Select X3.MaterialRequestDocNo, X3.MaterialRequestDNo, Group_Concat(Distinct X6.DocNo) As PIDocNo ");
            //SQL.AppendLine("        From TblRecvVdDtl X1 ");
            //SQL.AppendLine("        Inner Join TblPODtl X2 On X1.PODocNo=X2.DocNo And X1.PODNo=X2.DNo And X2.CancelInd = 'N' ");
            //SQL.AppendLine("        Inner Join TblPORequestDtl X3 On X2.PORequestDocNo=X3.DocNo And X2.PORequestDNo=X3.DNo And X3.CancelInd = 'N' ");
            //SQL.AppendLine("        Inner Join TblMaterialRequestHdr X4 ");
            //SQL.AppendLine("            On X3.MaterialRequestDocNo=X4.DocNo ");
            //SQL.AppendLine("            And X4.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("        Left Join TblPurchaseInvoiceDtl X5 On X1.DocNo=X5.RecvVdDocNo and X1.DNo=X5.RecvVdDNo ");
            //SQL.AppendLine("        Left Join TblPurchaseInvoiceHdr X6 On X5.DocNo=X6.DocNo And X6.CancelInd = 'N' ");
            //SQL.AppendLine("        Where X1.CancelInd='N' ");
            //SQL.AppendLine("        Group By X3.MaterialRequestDocNo, X3.MaterialRequestDNo ");
            //SQL.AppendLine("    ) T5 On T3.MaterialRequestDocNo=T5.MaterialRequestDocNo And T3.MaterialRequestDNo=T5.MaterialRequestDNo ");
            //SQL.AppendLine("    Group By T1.DocNo, T1.DNo, T3.MaterialRequestDocNo, T3.MaterialRequestDNo, T5.PIDOcNo ");
            //SQL.AppendLine(") G On A.DocNo=G.MaterialRequestDocNo And B.DNo=G.MaterialRequestDNo ");
            #endregion
            
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, T2.DNo, ");
            //SQL.AppendLine("    Group_Concat(Distinct T6.DocNo) PIDocNo, ");
            SQL.AppendLine("    Sum(T5.Qty) RecvVdQty ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd = 'N' And T2.Status = 'A' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo = T3.MaterialRequestDocNo And T2.DNo = T3.MaterialRequestDNo ");
            SQL.AppendLine("        And T3.CancelInd = 'N' And T3.Status = 'A' ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.DocNo = T4.PORequestDocNo And T3.DNo = T4.PORequestDNo  ");
            SQL.AppendLine("        And T4.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T5 On T4.DocNo = T5.PODocNo And T4.DNo = T5.PODNo ");
            SQL.AppendLine("        And T5.CancelInd = 'N' And T5.Status = 'A' ");
            //SQL.AppendLine("    Left Join TblPurchaseInvoiceDtl T6 On T5.DocNo = T6.RecvVdDocNo And T5.DNo = T6.RecvVdDNo ");
            //SQL.AppendLine("        And T6.DocNo In (Select DocNo From TblPurchaseInvoiceHdr Where CancelInd = 'N') ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    Group By T1.DocNo, T2.DNo ");
            SQL.AppendLine(") G On A.DocNo=G.DocNo And B.DNo=G.DNo ");

            SQL.AppendLine("Left Join (  ");
            SQL.AppendLine("     Select MRDocNo As DocNo, MRDno As DNo, SUM(Qty) As QtyCancel  ");
            SQL.AppendLine("     From TblMrQtyCancel Where cancelInd = 'N' ");
            SQL.AppendLine("     Group By MRDocNo, MRDno    ");
            SQL.AppendLine(") H On B.DocNo = H.DocNo And B.DNo = H.DNo ");

            #region DocApproval
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.DocNo, A.DNo, Case When A.LastUpDt Is Not Null Then ");
            SQL.AppendLine("    Group_Concat(B.UserName, ' (', Concat(Substring(A.LastUpDt, 7, 2), '/', Substring(A.LastUpDt, 5, 2), '/', Left(A.LastUpDt, 4)) , ')') ");
            SQL.AppendLine("    else  null end As UserMR ");
            SQL.AppendLine("    From TblDocApproval A ");
            SQL.AppendLine("    Inner JOin TblMaterialRequestHdr A1 On A.DocNo = A1.DocNo And (A1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("    Where A.DocType = 'MaterialRequest2' ");
            SQL.AppendLine("    Group By DocNo, DNo ");
            SQL.AppendLine(")I On A.DocNo = I.DocNo And B.DNo = I.DNo ");
            #endregion

            #region DO VD
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select E.MaterialRequestDocNo, E.MaterialRequestDNo, SUM(B.Qty) As QtyReturn ");
            SQL.AppendLine("    From TblDoVDHdr A ");
            SQL.AppendLine("    Inner Join TblDOVdDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl C On B.RecvVdDocNo = C.DocNo And C.CancelInd = 'N' And B.RecvVdDNo = C.DNo ");
            SQL.AppendLine("    Inner Join TblPODtl D On C.PODocNo=D.DocNo And C.PODNo=D.DNo  And D.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo  And E.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr F On E.MaterialRequestDocNo = F.DocNo And (F.DocNo Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Where B.CancelInd = 'N' ");
            SQL.AppendLine("    Group By E.MaterialRequestDocNo, E.MaterialRequestDNo ");
            SQL.AppendLine(") J On A.DocNo=J.MaterialRequestDocNo And B.DNo=J.MaterialRequestDNo ");
            SQL.AppendLine("Left Join TblSp K On A.LocalDocNo = K.LocalDocNo And IfNull(K.Status, 'P')<>'C' ");
            SQL.AppendLine("Left Join TblCustomer L On K.CtCode = L.CtCode ");
            SQL.AppendLine("Inner Join TblQthdr M On B.QtDocNo = M.DocNo ");
            SQL.AppendLine("Inner Join TblQtDtl N On M.DocNo = N.DocNo And B.QtDno = N.Dno  ");
            SQL.AppendLine("Inner Join TblVendor O On M.VdCode = O.VdCode ");
            //SQL.AppendLine("Left Join TblDRHdr O2 On A.DRDocNo=O2.DocNo ");
            //SQL.AppendLine("Left Join TblCustomer L2 On O2.CtCode = L2.CtCode ");

            //SQL.AppendLine("Left join tblpurchaseinvoicedtl P On G.DocNo=P.RecvVdDocNo and G.DNo=P.RecvVdDNo ");
            //SQL.AppendLine("Left join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select P.DocNo, P1.RecvVdDocNo, P1.RecvVdDNo ");
            //SQL.AppendLine("    From tblpurchaseinvoiceHdr P ");
            //SQL.AppendLine("    Inner Join tblpurchaseinvoicedtl P1 On P.DocNo= P1.DocNo ");
            //SQL.AppendLine("    Where P.CancelInd = 'N' ");
            //SQL.AppendLine(")P On G.DocNo=P.RecvVdDocNo and G.DNo=P.RecvVdDNo ");
            #endregion

            #region PI, OP, VR, VC 

            #region Old Code
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select Group_Concat(IfNull(Z.OpDocNo, '')) As OPDocNo, Z.invoiceDocNo, Z.Dno, Z.StatusOp, Z.UserOP,   ");
            //SQL.AppendLine("    Group_concat(IfNull(VoucherrequestDocNo, '')) As VRDocNo, Group_concat(IfNull(VCDocNo, '')) As VCDocNo,  ");
            //SQL.AppendLine("    IfNull(Z.AmtPi, 0.00) As AmtPi, ");
            //SQL.AppendLine("    Sum(IfNull(Amt, 0.00)) As AmtOP, ");
            //SQL.AppendLine("    (IfNull(Z.AmtPi, 0.00) - Sum(IfNull(Amt, 0.00))) As AmtBal ");
            //SQL.AppendLine("    from ( ");
            //SQL.AppendLine("        Select T.DocNo As OpDocNo, T.InvoiceDocNo, T.DNo,  ");
            //SQL.AppendLine("         Case T2.Status When 'C' Then 'Cancel' When 'O' Then 'Outstanding' When 'A' Then 'Approved' End As StatusOP,  ");
            //SQL.AppendLine("        Case When T2.LastUpDt Is Not Null  ");
            //SQL.AppendLine("         Then Group_Concat(Distinct T3.UserName, ' (', Concat(Substring(T2.LastUpDt, 7, 2), '/', Substring(T2.LastUpDt, 5, 2), '/',  ");
            //SQL.AppendLine("        Left(T2.LastUpDt, 4)) , ')')  else  null end As UserOP,  ");
            //SQL.AppendLine("         T0.VoucherRequestDocNo, T.Amt, T11.Amt+T11.taxAmt As AmtPI, T6.DocNo As VCDocNo ");
            //SQL.AppendLine("        From TblOutgoingpaymentdtl T  ");
            //SQL.AppendLine("        Inner Join TblOutgoingpaymentHdr T0 On T.DocNo=T0.DocNo And T0.CancelInd = 'N' ");
            //SQL.AppendLine("        Inner Join tblpurchaseinvoicedtl T1 On T.InvoiceDocNo=T1.DocNo  ");
            //SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T11 On T.InvoiceDocNo=T11.DocNo And T11.CancelInd = 'N'  ");
            //SQL.AppendLine("        Left Join TbldocApproval T2 On T.DocNo=T2.DocNo And T2.DocType='OutgoingPayment' ");
            //SQL.AppendLine("        Left Join TblUser T3 On T2.UserCode = T3.UserCode  ");
            //SQL.AppendLine("        Inner Join TblOutgoingpaymenthdr T4 On T.DocNo = T4.DocnO ");
            //SQL.AppendLine("        Inner Join TblVoucherRequestHdr T5 On T4.VoucherRequestDocNo = T5.DocNo ");
            //SQL.AppendLine("        Left Join TblVoucherHdr T6 On T5.VoucherDocNo = T6.DocNo ");
            //SQL.AppendLine("        Group By T.DocNo, T.DNo, T.InvoiceDocNo, T2.Status, T2.LastUpDt, T0.VoucherRequestDocNo, T.Amt, T11.taxAmt, T6.DocNo ");
            //SQL.AppendLine("    )Z ");
            //SQL.AppendLine("    Group By Z.invoiceDocNo, Z.Dno ");
            //SQL.AppendLine(")Q On G.PIDocNo=Q.InvoiceDocNo ");
            #endregion

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, ");
            SQL.AppendLine("    Group_Concat(Distinct T7.DocNo) PIDocNo, ");
            SQL.AppendLine("    Sum(T7.Amt+T7.TaxAmt) AmtPI ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo = T3.MaterialRequestDocNo And T2.DNo = T3.MaterialRequestDNo ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.DocNo = T4.PORequestDocNo And T3.DNo=T4.PORequestDNo ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T5 On T4.DocNo = T5.PODocNo And T4.DNo = T5.PODNo ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl T6 On T5.DocNo=T6.RecvVdDocNo And T5.DNo = T6.RecvVdDNo ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr T7 On T6.DocNo=T7.DocNo And T7.CancelInd = 'N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") Q On B.DocNo=Q.DocNo And B.DNo=Q.DNo ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, ");
            SQL.AppendLine("    Sum(T8.Amt) AmtOP, ");
            SQL.AppendLine("    Group_Concat(Distinct IfNull(T9.DocNo, '')) OPDocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct Case T9.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End) StatusOP, ");
            SQL.AppendLine("    Group_Concat(Distinct IfNull(T9.VoucherRequestDocNo, '')) VRDocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct IfNull(T10.DocNo, '')) VCDocNo ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo = T3.MaterialRequestDocNo And T2.DNo = T3.MaterialRequestDNo ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.DocNo = T4.PORequestDocNo And T3.DNo=T4.PORequestDNo ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T5 On T4.DocNo = T5.PODocNo And T4.DNo = T5.PODNo ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl T6 On T5.DocNo=T6.RecvVdDocNo And T5.DNo = T6.RecvVdDNo ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr T7 On T6.DocNo=T7.DocNo And T7.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T8 On T7.DocNo=T8.InvoiceDocNo ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T9 On T8.DocNo=T9.DocNo And T9.CancelInd='N' ");
            SQL.AppendLine("    Left Join TblVoucherHdr T10 On T9.VoucherRequestDocNo=T10.VoucherRequestDocNo And T10.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") R On B.DocNo=R.DocNo And B.DNo=R.DNo ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, ");
            SQL.AppendLine("    Group_Concat(Distinct ");
            SQL.AppendLine("    Concat(T9.DocNo, '( ', T11.UserName, ' on ',Date_Format(Left(T10.LastUpDt, 8), '%d/%m/%Y'), ' )') ");
            SQL.AppendLine("    ) As UserOP ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo = T3.MaterialRequestDocNo And T2.DNo = T3.MaterialRequestDNo ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.DocNo = T4.PORequestDocNo And T3.DNo=T4.PORequestDNo ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T5 On T4.DocNo = T5.PODocNo And T4.DNo = T5.PODNo ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl T6 On T5.DocNo=T6.RecvVdDocNo And T5.DNo = T6.RecvVdDNo ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr T7 On T6.DocNo=T7.DocNo And T7.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T8 On T7.DocNo=T8.InvoiceDocNo ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T9 On T8.DocNo=T9.DocNo And T9.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblDocApproval T10 On T9.DocNo=T10.DocNo And T10.DocType='Outgoingpayment' And T10.UserCode Is Not Null And T10.LastUpDt Is Not Null ");
            SQL.AppendLine("    Inner Join TblUser T11 On T10.UserCode=T11.UserCode ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") W On B.DocNo=W.DocNo And B.DNo=W.DNo ");


            //SQL.AppendLine("Select (T1.Amt+T1.TaxAmt) AmtPI, ((T1.Amt+T1.TaxAmt)-T2.AmtOP) AmtBal, T2.* ");
            //SQL.AppendLine("From TblPurchaseInvoiceHdr T1 ");
            //SQL.AppendLine("Inner Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select X2.InvoiceDocNo, ");
            //SQL.AppendLine("    IfNull(Sum(X2.Amt), 0.00) AmtOP, ");
            //SQL.AppendLine("    Group_Concat(Distinct IfNull(X1.DocNo, '')) OPDocNo, ");
            //SQL.AppendLine("    Group_Concat(Distinct Case X1.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End) StatusOP, ");
            //SQL.AppendLine("    X4.UserOP, ");
            //SQL.AppendLine("    Group_Concat(Distinct IfNull(X1.VoucherRequestDocNo, '')) VRDocNo, ");
            //SQL.AppendLine("    Group_Concat(Distinct IfNull(X3.DocNo, '')) VCDocNo ");
            //SQL.AppendLine("    From TblOutgoingpaymentHdr X1 ");
            //SQL.AppendLine("    Inner Join TblOutgoingpaymentDtl X2 ");
            //SQL.AppendLine("        On X1.DocNo = X2.DocNo And X2.InvoiceType = '1' ");
            //SQL.AppendLine("        And X2.InvoiceDocNo In ");
            //SQL.AppendLine("        ( ");
            //SQL.AppendLine("            Select Distinct Y6.DocNo ");
            //SQL.AppendLine("            From TblRecvVdDtl Y1 ");
            //SQL.AppendLine("            Inner Join TblPODtl Y2 On Y1.PODocNo = Y2.DocNo And Y1.PODNo = Y2.DNo And Y2.CancelInd = 'N' ");
            //SQL.AppendLine("            Inner Join TblPORequestDtl Y3 On Y2.PORequestDocNo = Y3.DocNo And Y2.PORequestDNo = Y3.DNo And Y3.CancelInd = 'N' ");
            //SQL.AppendLine("            Inner Join TblMaterialRequestHdr Y4 On Y3.MaterialRequestDocNo = Y4.DocNo And (Y4.DocDt Between @DocDt1 And @DocDt2) ");
            //SQL.AppendLine("            Inner Join TblPurchaseInvoiceDtl Y5 On Y1.DocNo = Y5.RecvVdDocNo And Y1.DNo = Y5.RecvVdDNo ");
            //SQL.AppendLine("            Inner Join TblPurchaseInvoiceHdr Y6 On Y5.DocNo = Y6.DocNo And Y6.CancelInd = 'N' ");
            //SQL.AppendLine("        ) ");
            //SQL.AppendLine("    Left Join TblVoucherHdr X3 ");
            //SQL.AppendLine("        On X1.VoucherRequestDocNo=X3.VoucherRequestDocNo ");
            //SQL.AppendLine("        And X3.CancelInd = 'N' ");
            //SQL.AppendLine("    Left Join ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select R1.DocNo,  ");
            //SQL.AppendLine("        Case When R1.LastUpDt Is Not Null  ");
            //SQL.AppendLine("        Then Group_Concat(Distinct R2.UserName, '(',Date_Format(Left(R1.LastUpDt, 8), '%d/%m/%Y'), ')') ");
            //SQL.AppendLine("        Else Null End As UserOP ");
            //SQL.AppendLine("        From TblDocApproval R1 ");
            //SQL.AppendLine("        Inner Join TblUser R2 On R1.UserCode = R2.UserCode And R1.DocType = 'Outgoingpayment' ");
            //SQL.AppendLine("        Group By R1.DocNo ");
            //SQL.AppendLine("    )X4 On X1.DocNo = X4.DocNo ");
            //SQL.AppendLine("    Where X1.CancelInd = 'N' ");
            //SQL.AppendLine("    Group By X2.InvoiceDocNo, X4.UserOP ");
            //SQL.AppendLine(") T2 On T1.DocNo = T2.InvoiceDocNo ");
            //SQL.AppendLine(")Q On G.PIDocNo=Q.InvoiceDocNo ");
           
            #endregion

            #region SI
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select Distinct A.DocNo,  ");
	        SQL.AppendLine("    Concat ( ");
	        SQL.AppendLine("    ifnull(B.Cnt1, ''), ifnull(concat(', ',B.Cnt2), ''), ifnull(concat(', ',B.Cnt3), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Cnt4), ''), ifnull(concat(', ', B.Cnt5), ''), ifnull(concat(', ', B.Cnt6), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Cnt7), ''), ifnull(concat(', ', B.Cnt8), ''), ifnull(concat(', ', B.Cnt9), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Cnt10), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Cnt11), ''), ifnull(concat(', ', B.Cnt12), ''), ifnull(concat(', ', B.Cnt13), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Cnt14), ''), ifnull(concat(', ', B.Cnt15), ''), ifnull(concat(', ', B.Cnt16), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Cnt17), ''), ifnull(concat(', ', B.Cnt18), ''), ifnull(concat(', ', B.Cnt19), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Cnt20), ''), ifnull(concat(', ', B.Cnt21), ''), ifnull(concat(', ', B.Cnt22), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Cnt23), ''), ifnull(concat(', ', B.Cnt24), ''), ifnull(concat(', ', B.Cnt25), '')  ");
	        SQL.AppendLine("    ) As CntName, ");
	        SQL.AppendLine("    Concat( ");
	        SQL.AppendLine("    ifnull(B.Seal1, ''), ifnull(concat(', ',B.Seal2), ''), ifnull(concat(', ',B.Seal3), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Seal4), ''), ifnull(concat(', ', B.Seal5), ''), ifnull(concat(', ', B.Seal6), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Seal7), ''), ifnull(concat(', ', B.Seal8), ''), ifnull(concat(', ', B.Seal9), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Seal10), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Seal11), ''), ifnull(concat(', ', B.Seal12), ''), ifnull(concat(', ', B.Seal13), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Seal14), ''), ifnull(concat(', ', B.Seal15), ''), ifnull(concat(', ', B.Seal16), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Seal17), ''), ifnull(concat(', ', B.Seal18), ''), ifnull(concat(', ', B.Seal19), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Seal20), ''), ifnull(concat(', ', B.Seal21), ''), ifnull(concat(', ', B.Seal22), ''),  ");
	        SQL.AppendLine("    ifnull(concat(', ', B.Seal23), ''), ifnull(concat(', ', B.Seal24), ''), ifnull(concat(', ', B.Seal25), '')  ");
	        SQL.AppendLine("    ) As SealName, A.SpStfDt, C.PEB, C.PEBDt, A.SPBlNo ");
	        SQL.AppendLine("    From TblSIHdr A ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr A1 On A.DocNo = A1.SIDocNo And (A1.DocDt Between @DocDt1 And @DocDt2) ");
	        SQL.AppendLine("    Inner Join TblPLHdr B On A.DocNo = B.SiDocNo ");
            SQL.AppendLine("    Inner Join TblSP C On A.SpDocNo = C.DocNo ");
            SQL.AppendLine("    Where C.Status <> 'C' ");
            SQL.AppendLine(")S On A.SiDocno = S.DocNo ");
            #endregion

            #region Address Export/Local

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select A.DOcNo, E.ShpMCode, I.CtCode, I2.CtName, G.PortName As Tujuan, ");
            SQL.AppendLine("	Max(Cast(H2.SectionNo As UNSIGNED))As Cnt, ");
            SQL.AppendLine("    Case I.Status ");
            SQL.AppendLine("        When 'F' Then 'Final' ");
            SQL.AppendLine("        When 'P' Then 'Planning' ");
            SQL.AppendLine("        When 'R' Then 'Released' ");
            SQL.AppendLine("        When 'C' Then 'Cancelled' ");
            SQL.AppendLine("    End As Status ");
            SQL.AppendLine("	From TblSIHDR A ");
            SQL.AppendLine("	Inner Join TblSIDTl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("	Inner Join TblSODtl C On B.SODocNo=C.DocNo And B.SODNo=C.DNo ");
            SQL.AppendLine("	Inner Join TblSOHdr D On C.DocNo = D.DOcNo ");
            SQL.AppendLine("	Inner Join TblCtQtHdr E On D.CtQtDocNo=E.DocNo ");
            SQL.AppendLine("	Inner Join TblDeliveryType F On E.ShpMCode= F.DTCode ");
            SQL.AppendLine("	Inner Join TblPort G On A.SPPortCode2=G.PortCode ");
            SQL.AppendLine("    Left Join TblPLHdr H On A.DocNo=H.SiDocNo ");
            SQL.AppendLine("    Left Join TblPlDtl H2 On H.DocNo = H2.DocNo ");
            SQL.AppendLine("    Left Join TblSp I On A.SPDocNo = I.DocNo ");
            SQL.AppendLine("    Left Join TblCustomer I2 On I.CtCode=I2.CtCode ");
            SQL.AppendLine("    Where A.DocNo In ( ");
            SQL.AppendLine("        Select SIDocNo ");
            SQL.AppendLine("        From TblMaterialRequestHdr ");
            SQL.AppendLine("        Where DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        And CancelInd='N' ");
            SQL.AppendLine("        And Status In ('O', 'A') ");
            SQL.AppendLine("        And DocNo In (");
            SQL.AppendLine("            Select Distinct X1.DocNo ");
            SQL.AppendLine("            From TblMaterialRequestHdr X1 ");
            SQL.AppendLine("            Inner Join TblMaterialRequestDtl X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("                And X2.CancelInd='N' ");
            SQL.AppendLine("                And X2.Status In ('O', 'A') ");
            SQL.AppendLine("            Where X1.CancelInd='N' ");
            SQL.AppendLine("            And X1.Status In ('O', 'A') ");
            SQL.AppendLine("            And X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("    ) ");

            SQL.AppendLine("    Group By A.DOcNo, E.ShpMCode, I.CtCode, I2.CtName, G.PortName, I.Status ");
            SQL.AppendLine(") T On A.SIDOcNo=T.DOcNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select A.DocNo, A.CtCode, H.CtName, ");
            SQL.AppendLine("	Case F.DtCode ");
            SQL.AppendLine("	    When '1' Then 'LOCO' ");
            SQL.AppendLine("	    When '5' Then Concat(A.SAAddress,'-', G.CityName) ");
            SQL.AppendLine("    End As Tujuan, ");
            SQL.AppendLine("    Round(sum(B.QtyPackagingUnit),2) As QtyPackagingUnit, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("        When A.ProcessInd='O' Then 'Outstanding' ");
            SQL.AppendLine("        When A.ProcessInd='F' Then 'Fulfilled'");
            SQL.AppendLine("        When A.ProcessInd='M' Then 'Manual Fullfilled' End As Status ");
            SQL.AppendLine("	From TblDRHdr A ");
            SQL.AppendLine("	Inner join TblDRDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("	Inner Join TblSODtl C On B.SODocNo=C.DocNo And B.SODNo=C.DNo ");
            SQL.AppendLine("	Inner Join TblSOHdr D On C.DocNo = D.DOcNo ");
            SQL.AppendLine("	Inner Join TblCtQtHdr E On D.CtQtDocNo=E.DocNo ");
            SQL.AppendLine("	Inner Join TblDeliveryType F On E.ShpMCode= F.DTCode ");
            SQL.AppendLine("	Left Join TblCity G On A.SACityCode = G.CityCode ");
            SQL.AppendLine("    Left Join TblCustomer H On A.CtCode=H.CtCode ");
            SQL.AppendLine("    Where A.DocNo In ( ");
            SQL.AppendLine("        Select DRDocNo From TblMaterialRequestHdr ");
            SQL.AppendLine("        Where CancelInd='N' ");
            SQL.AppendLine("        And Status In ('O', 'A') ");
            SQL.AppendLine("        And DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        And DocNo In (");
            SQL.AppendLine("            Select Distinct X1.DocNo ");
            SQL.AppendLine("            From TblMaterialRequestHdr X1 ");
            SQL.AppendLine("            Inner Join TblMaterialRequestDtl X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("                And X2.CancelInd='N' ");
            SQL.AppendLine("                And X2.Status In ('O', 'A') ");
            SQL.AppendLine("            Where X1.CancelInd='N' ");
            SQL.AppendLine("            And X1.Status In ('O', 'A') ");
            SQL.AppendLine("            And X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("	Group By A.DocNo, A.CtCode, H.CtName, A.ProcessInd, ");
            SQL.AppendLine("	Case F.DtCode ");
            SQL.AppendLine("	    When '1' Then 'LOCO' ");
            SQL.AppendLine("	    When '5' Then Concat(A.SAAddress,'-', G.CityName) ");
            SQL.AppendLine("    End ");
            SQL.AppendLine(")U On A.DRDocNo=U.DocNo ");

            #endregion

            #region APDP

            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select A.DocNo As MRDocNo, B.Dno  As MRDno, Sum(E.Amt) As AmtAPDP  ");
	        SQL.AppendLine("    From TblmaterialRequestHdr A ");
            SQL.AppendLine("    Inner Join TblmaterialRequestDtl B On A.DocNo = b.Docno And (A.DocNo Between @DocDt1 And @DocDt2) And B.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblPoRequestDtl  C On B.DocNo = C.materialRequestDocnO And  B.Dno = C.materialRequestDno And C.CancelInd = 'N' ");
	        SQL.AppendLine("    Inner Join TblPODtl D On C.Docno = D.PoRequestDocno  And C.Dno = D.PoRequestDno And  D.CancelInd = 'N'  ");
	        SQL.AppendLine("    Inner Join TblApdownpayment E On D.DocnO  = E.PODocNo And E.cancelInd = 'N' ");
	        SQL.AppendLine("    Where A.CancelInd='N'  And A.EximInd = 'Y' ");
            SQL.AppendLine("    Group By A.DocNo, B.DNo ");
            SQL.AppendLine(") V On B.DocNO = V.MrDocNO And B.Dno = V.MrDno ");

            #endregion
            
            SQL.AppendLine("Where 0 = 0 ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (ChkCtCode.Checked)
                SQL.AppendLine("And IfNull(T.CtCode, U.CtCode)=@CtCode ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 42;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Local#",
                        "Customer",
                        "Material Request#",
                        "Date",
                        "DNo",
                        
                        //6-10 
                        "Department"+Environment.NewLine+"Code",
                        "Department",
                        "Vendor",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",

                        //11-15
                        "Material Request"+Environment.NewLine+"Quantity",
                        "UoM",
                        "Approval"+Environment.NewLine+"Status",
                        "Checked"+Environment.NewLine+"By",
                        "",
                        
                        //16-20
                        "PO Request"+Environment.NewLine+"Quantity",
                        "Approval"+Environment.NewLine+"Status",
                        "Checked"+Environment.NewLine+"By",
                        "",
                        "PO"+Environment.NewLine+"Quantity",

                        //21-25
                        "Received"+Environment.NewLine+"Quantity",
                        "Returning"+Environment.NewLine+"Quantity",
                        "Purchase Invoice#",
                        "Outgoing Payment#",
                        "Approval"+Environment.NewLine+"Status",

                        //26-30
                        "Checked"+Environment.NewLine+"By",
                        "Voucher Request#",
                        "Voucher#",
                        "Container",
                        "Seal",

                        //31-35
                        "Stuffing Date",
                        "Amount",
                        "SI#/DR#",
                        "Destination",
                        "Container/Packaging",

                        //36-40
                        "Status",
                        "Down Payment",
                        "Outstanding"+Environment.NewLine+"Outgoing Payment",
                        "PEB No.",
                        "PEB Date",

                        //41
                        "Booking / BL No"
                    },
                    new int[] 
                    {
                        50,
                        150, 200, 150, 100, 0, 
                        0, 150, 200, 180, 200,  
                        100, 70, 80, 250, 20,  
                        100, 100, 250, 20, 100, 
                        100, 100, 150, 150, 100,
                        200, 150, 150, 200, 200, 
                        100, 150, 120, 200, 150, 
                        120, 150, 150, 200, 100,
                        120
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 15, 19 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 16, 20, 21, 22, 32, 37, 38 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4, 31, 40 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 9, 22, 33, 39, 40 }, false);
            Grd1.Cols[32].Move(13);
            Grd1.Cols[29].Move(14);
            Grd1.Cols[30].Move(15);
            Grd1.Cols[31].Move(16);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 33, 39, 40 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                if (ChkCtCode.Checked) Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                Sm.FilterStr(ref Filter, ref cm, TxtMRDocNo.Text, new string[] {"A.DocNo", "A.LocalDocNo"});
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "M.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "D.ItName" });

                SetSQL();
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter,
                    new string[]
                    { 
                        //0
                        "LocalDocNo", 

                        //1-5
                        "CtName", "DocNo", "DocDt", "Dno", "DeptCode",  
                        
                        //6-10
                        "DeptName", "VdName", "ItCode","ItName","Qty",   
                        
                        //11-15
                        "PurchaseUomCode", "StatusMR", "UserMR","PORequestQty", "StatusPOR", 

                        //16-20
                        "UserPOR", "POQty", "RecvVdQty", "QtyReturn", "PIDocNo",

                        //21-25
                        "OPDocNo", "StatusOP", "UserOP", "VoucherRequestDocNo", "VoucherDocNo",

                        //26-30
                        "CntName", "SealName", "SpStfDt", "Amt", "SIDocNo",

                        //31-35
                        "Tujuan", "Cnt", "Status", "AmtAPDP", "AmtOuts",

                        //36-38
                        "PEB", "PEBDt", "SpBLNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);

                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);

                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);

                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                        if (Sm.DrStr(dr, 13) == "Cancel")
                        {
                            Grd1.Rows[Row].BackColor = Color.Red;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 12);
                        }
                        else
                        {
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 12);
                        }


                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 16);

                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 20, 17);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 21, 18);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 22, 19);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 23, 20);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 21);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 25, 22);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 26, 23);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 27, 24);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 28, 25);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 29, 26);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 30, 27);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 31, 28);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 32, 29);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 33, 30);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 34, 31);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 35, 32);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 36, 33);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 37, 34);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 38, 35);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 39, 36);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 40, 37);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 41, 38);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRptMaterialRequestEximStatusDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 5), Sm.GetGrdStr(Grd1, e.RowIndex, 3));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtDocNo.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.TxtQty.EditValue = Sm.GetGrdDec(Grd1, e.RowIndex, 11);
                f.TxtDept.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
            if (e.ColIndex == 19 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmRptMaterialRequestEximStatusDlg2(this, Sm.GetGrdStr(Grd1, e.RowIndex, 5), Sm.GetGrdStr(Grd1, e.RowIndex, 3));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.TxtQty.EditValue = Sm.GetGrdDec(Grd1, e.RowIndex, 11);
                f.TxtDept.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmRptMaterialRequestEximStatusDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 5), Sm.GetGrdStr(Grd1, e.RowIndex, 3));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtDocNo.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.TxtQty.EditValue = Sm.GetGrdDec(Grd1, e.RowIndex, 11);
                f.TxtDept.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
            if (e.ColIndex == 19 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmRptMaterialRequestEximStatusDlg2(this, Sm.GetGrdStr(Grd1, e.RowIndex, 5), Sm.GetGrdStr(Grd1, e.RowIndex, 3));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.TxtQty.EditValue = Sm.GetGrdDec(Grd1, e.RowIndex, 11);
                f.TxtDept.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkMRDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "MR# / Local#");
        }
        
        private void TxtMRDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        
        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }
        
        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }


        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        #endregion
    }
}
