﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmWorkCenterFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmWorkCenter mfrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmWorkCenterFind(FrmWorkCenter FrmParent)
        {
            InitializeComponent();
            mfrmParent = FrmParent;
        }

        #endregion

        #region Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mfrmParent.Text;
            Sm.ButtonVisible(mfrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocName, ");
            SQL.AppendLine("A.Capacity, B.UomName, A.Location, A.ActiveInd, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblWorkCenterHdr A ");
            SQL.AppendLine("Inner Join TblUom B On A.UomCode=B.UomCode ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-6
                        "Work Center"+Environment.NewLine+"Code", 
                        "Work Center"+Environment.NewLine+"Name",
                        "Capacity",
                        "UoM",
                        "Location",
                        "Active",
                        
                        //7-12
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 6 });
            Sm.GrdFormatDate(Grd1, new int[] { 8, 11 });
            Sm.GrdFormatTime(Grd1, new int[] { 9, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenterName.Text, new string[] { "A.DocNo", "A.DocName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo",
                        new string[]
                        {
                            //0
                            "DocNo",
                            //1-5
                            "DocName", "Capacity", "UomName", "Location", "ActiveInd",
                            //6-9
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 6, 5);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 12, 9);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mfrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }
        #endregion

        #region Event
        private void TxtWorkCenterName_Validated(object sender, EventArgs e)
        {
            //Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWorkCenterName_CheckedChanged(object sender, EventArgs e)
        {
            //Sm.FilterSetTextEdit(this, sender, "Work Center");

        }
        #endregion
    }
}
