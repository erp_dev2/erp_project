﻿#region Update
/*
    20/04/2017 [HAR] tambah group Dno di query yang nyambung ke approval
    22/12/2017 [TKG] Filter department berdasarkan group
    28/12/2017 [HAR] tambah informasi currency dan estimated price dari MR
    19/03/2017 [TKG] Filter berdasarkan otorisasi group thd site
    30/08/2018 [WED] ambil MR yang bukan dari Tender
    02/09/2018 [TKG] Menampilkan nomor urut berdasarkan parameter
    20/04/2018 [TKG] tambah PIC
    25/07/2019 [TKG] tambah dropping request amount
    04/11/2019 [DITA/IMS] tambah informasi Specifications
    05/11/2019 [DITA/IMS] tambah filter berdasarkan Item Category
    06/11/2019 [DITA/YK] Choose data untuk dropping request amt ke master
    07/11/2019 [WED/YK] Dropping Request amount ambil per item nya
    28/11/2019 [WED/IMS] tambah kolom project code, project name
    20/12/2019 [WED/YK] pass item group code
    17/02/2020 [TKG/IMS] tambah project code, project name
    27/05/2020 [TKG/IMS] tambah filter Local Document#, Item's Local Code
    23/10/2020 [TKG/IMS] extimated price kalau kosong diisi 0.00
    27/10/2020 [ICA/IMS] Ubah Material Request menjadi Purchase Request
    03/12/2020 [IBL/SIER] Mengubah format Order By pada query berdasarkan parameter PORequestMRItemOrderFormat
    04/12/2020 [IBL/SIER] Saat mencentang salah satu item, otomatis centang semua item dengan Document# yg sama berdasarkan parameter PORequestMRItemOrderFormat
    09/12/2020 [IBL/SIER] Menambah kolom approved by, approved date, approved time berdasarkan parmeter IsMRInPORShowApprovalInfo
    14/04/2021 [IBL/KIM] MR ambil yg TenderInd = Y, berdasarkan parameter MenuCodeForPORTender dan IsMRWithTenderEnabled 
    14/07/2021 [WED/PADI] ada parameter IsUseECatalog untuk magerin yang TenderDocNo di MaterialRequestDtl
    29/09/2021 [ARI/PHT] Parameter baru IsMRUseProcurementType untuk magerin informasi procurement type
    29/09/2021 [ARI/PHT] Menambahkan select data untuk procurement type (desc dan code) untuk di pass ke mFrmParent
    25/11/2021 [WED/RM] item MR yang ditarik adalah MR yang tidak dicentang RMInd nya, atau yg dicentang dan sudah diproses di RFQ. berdasarkan parameter IsUseECatalog
    23/12/2021 [RIS/SIER] Menambahkan group kolom
    27/12/2021 [TYO/SIER] menambah kolom Division berdasarkan parameter IsPORShowDivisionInfo dan unhide kolom Document#
    28/12/2021 [ISD/SIER] choose data untuk division ke master
    01/03/2023 [MAU/HEX] menampilkan kolom MR Type berdasarkan parameter IsMRUseApprovalSheet
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPORequestDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPORequest mFrmParent;
        private string mSQL = string.Empty, Doctitle = Sm.GetParameter("Doctitle")
            ;

        public string selectedprocurementcode = string.Empty;

        #endregion

        #region Constructor

        public FrmPORequestDlg(FrmPORequest FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (Doctitle == "IMS")
                this.Text = "Purchase Request";
            else this.Text = "Material Request";
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                mFrmParent.mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsPORequestFilterByDept ? "Y" : "N");
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 39;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Document#",
                        "DNo", 
                        "Date",
                        "Department",
                        
                        //6-10
                        "Item's Code", 
                        "Item's Name",
                        "Foreign Name",
                        "Category",
                        "Outstanding"+Environment.NewLine+"Quantity",

                        //11-15
                        "UoM",
                        "Estimated"+Environment.NewLine+"Price",
                        "Usage"+Environment.NewLine+"Date",
                        "Vendor",
                        "Number of"+Environment.NewLine+"Cancelled PO Request",

                        //16-20
                        "Remark",
                        "Approver's Remark",
                        "ItScCode",
                        "Sub Category",
                        "Local#",

                        //21-25
                        "Site Code",
                        "Site",
                        "Item'S Local Code",
                        "Currency",
                        "Estimated"+Environment.NewLine+"Price",

                        //26-30
                        "Person In Charge",
                        "Dropping Request's"+Environment.NewLine+"Amount",
                        "Specification",
                        "Project's Code",
                        "Project's Name",

                        //31-35
                        "ItGrpCode",
                        "Approved By",
                        "Approved Date",
                        "Approved Time",
                        "Procurement Type",

                        //36-38
                        "Procurement Code",
                        "Division",
                        "MR Type"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 150, 0, 100, 150, 
                        
                        //6-10
                        80, 250, 230, 150, 80, 
                        
                        //11-15
                        60, 100, 80, 350, 150,

                        //16-20
                        300, 200, 0, 150, 180,

                        //21-25
                        0, 150, 150, 80, 120,

                        //26-30
                        200, 130, 300, 100, 200,

                        //31-35
                        0, 150, 100, 100, 160,

                        //36-38
                        0, 180, 180
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 37, 38 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12, 25, 27 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 15 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 4, 13, 33 });
            Sm.GrdFormatTime(Grd1, new int[] { 34 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6, 9, 21, 22, 23, 31, 36, 37 }, false);
            Grd1.Cols[23].Move(9);
            Grd1.Cols[35].Move(10);
            
            
            if (!mFrmParent.mIsMRShowEstimatedPrice)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 24, 25 });
            }

            if (!mFrmParent.mIsMRUseProcurementType)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 35 });
            }

            if (mFrmParent.mIsShowForeignName)
            {
                if (Sm.GetParameter("ProcFormatDocNo") == "0")
                    Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 12, 13, 15, 18 }, false);
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 12, 13, 15, 18 }, false);
                    Grd1.Cols[19].Move(10);
                }
            }
            else
            {
                if (Sm.GetParameter("ProcFormatDocNo") == "0")
                    Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 8, 12, 13, 15, 18 }, false);
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 8, 12, 13, 15, 18 }, false);
                    Grd1.Cols[19].Move(8);
                }
            }

            if (mFrmParent.mIsSiteMandatory)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 22 }, true);
                Grd1.Cols[22].Move(6);
            }

            if (mFrmParent.mIsMaterialRequestForDroppingRequestEnabled)
                Grd1.Cols[27].Move(12);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 27 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 28, 29, 30 });
            Grd1.Cols[28].Move(10);

            if (!mFrmParent.mIsMRInPORShowApprovalInfo) Sm.GrdColInvisible(Grd1, new int[] { 32, 33, 34 });

            if (mFrmParent.mIsPORShowDivisionInfo)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 37 }, true);
                Grd1.Cols[37].Move(7);
            }
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, true);

            Sm.SetGrdProperty(Grd1 , false);

            if (mFrmParent.mIsMRUseApprovalSheet)
                Grd1.Cols[38].Move(4);
            Sm.GrdColInvisible(Grd1, new int[] { 38 }, mFrmParent.mIsMRUseApprovalSheet);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 9, 12, 13, 15, 23 }, !ChkHideInfoInGrd.Checked);
            
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, C.DeptName, B.ItCode, E.ItCtName, D.PurchaseUomCode, B.UPrice, B.UsageDt, B.Remark, ");
            SQL.AppendLine("B.Qty-IfNull(K.Qty, 0)-IfNull(H.Qty, 0) As OutstandingQty, ");
            SQL.AppendLine("G.VdName, D.Specification, ");
            SQL.AppendLine("D.ItName, D.ForeignName, D.ItGrpCode, ");
            SQL.AppendLine("IfNull(F.NoOfCancelPORequest, 0) As Much, I.RemarkApproval, D.ItScCode, J.ItScName, A.LocalDocNo, A.SiteCode, L.SiteName, D.ItCodeInternal, B.CurCode, B.EstPrice, A.PICCode, ");
            if (mFrmParent.mIsMaterialRequestForDroppingRequestEnabled)
            {
                SQL.AppendLine("Case When A.DroppingRequestDocNo Is Null Then 0.00 ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When A.DroppingRequestBCCode Is Null Then IfNull(M.Amt, 0.00) Else IfNull(N.Amt, 0.00) End ");
                SQL.AppendLine("End As DroppingRequestAmt, ");
            }
            else
                SQL.AppendLine("0.00 As DroppingRequestAmt, ");
            SQL.AppendLine("O.ProjectCode, O.ProjectName, ");
            if (mFrmParent.mIsMRInPORShowApprovalInfo)
                SQL.AppendLine("P.UserName ApprovalUserName, P.LastUpDt ApprovalLastUpDt, ");
            else
                SQL.AppendLine("Null As ApprovalUserName, Null As ApprovalLastUpDt, ");
            if (mFrmParent.mIsMRUseProcurementType)
                SQL.AppendLine("Q.OptDesc As ProcurementType, A.ProcurementType AS ProcurementCode ");
            else
                SQL.AppendLine("Null As ProcurementType, Null AS ProcurementCode ");

            if (mFrmParent.mIsPORShowDivisionInfo)
                SQL.AppendLine(", R.DivisionName Division ");
            else
                SQL.AppendLine(", Null As Division ");

            if (mFrmParent.mIsMRUseApprovalSheet)
                SQL.AppendLine(", S.OptDesc MRType");
            else
                SQL.AppendLine(", Null As MRType");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And IfNull(B.Status,'X')='A' And B.CancelInd='N' And IfNull(B.ProcessInd, '')<>'F' ");
            if (!mFrmParent.mIsUseECatalog)
            {
                SQL.AppendLine("    And B.TenderDocNo Is Null ");
            }
            else
            {
                SQL.AppendLine("    And ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        A.RMInd = 'N' ");
                SQL.AppendLine("        Or ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            A.RMInd = 'Y' ");
                SQL.AppendLine("            And A.DocNo In ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select Distinct DocNo ");
                SQL.AppendLine("                From TblMaterialRequestDtl4 ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mFrmParent.mIsMRWithTenderEnabled) {
                if (mFrmParent.mMenuCodeForPORTender)
                    SQL.AppendLine("    And A.TenderInd = 'Y' ");
                else
                    SQL.AppendLine("    And A.TenderInd = 'N' ");
            }
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T1.MaterialRequestDocNo, T1.MaterialRequestDNo, Count(T1.MaterialRequestDocNo) As NoOfCancelPORequest ");
            SQL.AppendLine("    From TblPORequestDtl T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 ");
            SQL.AppendLine("        On T1.MaterialRequestDocNo=T2.DocNo ");
            SQL.AppendLine("        And T1.MaterialRequestDNo=T2.DNo ");
            SQL.AppendLine("        And IfNull(T2.ProcessInd, '')<>'F' ");
            SQL.AppendLine("        And T2.TenderDocNo Is Null ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("    Where T1.CancelInd = 'Y' Or T1.Status = 'C' ");
            SQL.AppendLine("    Group By T1.MaterialRequestDocNo, T1.MaterialRequestDNo ");
            SQL.AppendLine(") F On A.DocNo=F.MaterialRequestDocNo And B.DNo=F.MaterialRequestDNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.ItCode, Group_Concat(T.VdName Separator ', ') As VdName ");
            SQL.AppendLine("    From (");
            SQL.AppendLine("        Select Distinct T2.ItCode, T3.VdName ");
            SQL.AppendLine("        From TblQtHdr T1 ");
            SQL.AppendLine("        Inner Join TblQtDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Inner Join TblVendor T3 On T1.VdCode=T3.VdCode And T3.ActInd='Y' ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.ItCode ");
            SQL.AppendLine(") G On B.ItCode=G.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.MRDocNo As DocNo, T1.MRDNo As DNo, Sum(T1.Qty) Qty "); 
            SQL.AppendLine("    From TblMRQtyCancel T1 "); 
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 ");
            SQL.AppendLine("        On T1.MRDocNo=T2.DocNo ");
            SQL.AppendLine("        And T1.MRDNo=T2.DNo ");
            SQL.AppendLine("        And IfNull(T2.ProcessInd, '')<>'F' ");
            SQL.AppendLine("        And T2.TenderDocNo Is Null ");
            SQL.AppendLine("    Where T1.CancelInd = 'N' ");
            SQL.AppendLine("    Group By T1.MRDocNo, T1.MRDNo "); 
            SQL.AppendLine(") H On B.DocNo=H.DocNo And B.DNo=H.DNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select DocNo, Dno,  Group_concat(I.remark separator ', ') As RemarkApproval ");
	        SQL.AppendLine("    From TblDocApproval I ");
            SQL.AppendLine("    Where I.DocType = 'Materialrequest' And I.Status = 'A' ");
	        SQL.AppendLine("    Group By I.DocNo, I.Dno ");
            SQL.AppendLine(") I On A.DocNo = I.DocNo And B.Dno = I.Dno And I.RemarkApproval Is not null ");
            SQL.AppendLine("Left Join TblItemSubCategory J On D.ItScCode = J.ItScCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select MaterialRequestDocNo As DocNo, T1.MaterialRequestDNo As DNo, Sum(T1.Qty) As Qty ");
            SQL.AppendLine("    From TblPORequestDtl T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 ");
            SQL.AppendLine("        On T1.MaterialRequestDocNo=T2.DocNo ");
            SQL.AppendLine("        And T1.MaterialRequestDNo=T2.DNo ");
            SQL.AppendLine("        And IfNull(T2.ProcessInd, '')<>'F' ");
            SQL.AppendLine("        And T2.TenderDocNo Is Null ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("    Where T1.CancelInd='N' And T1.Status<>'C' ");
            SQL.AppendLine("    Group By T1.MaterialRequestDocNo, T1.MaterialRequestDNo ");
            SQL.AppendLine(") K On A.DocNo=K.DocNo And B.DNo=K.DNo ");
            SQL.AppendLine("Left Join TblSite L On A.SiteCode=L.SiteCode ");


            if (mFrmParent.mIsMaterialRequestForDroppingRequestEnabled)
            {
                SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine("    Select T2.DocNo, T2.DNo, Sum(IfNull(T5.Amt, 0.00)) As Amt ");
                SQL.AppendLine("    Select T2.DocNo, T2.DNo, T4.ResourceItCode, (T3.RemunerationAmt + T3.DirectCostAmt) Amt ");
                SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo And T2.Status='A' And T2.CancelInd='N' ");
                SQL.AppendLine("    Inner Join TblDroppingRequestDtl T3 On T1.DroppingRequestDocNo=T3.DocNo ");
                SQL.AppendLine("    Inner Join TblProjectImplementationRBPHdr T4 On T3.PRBPDocNo=T4.DocNo And T2.ItCode=T4.ResourceItCode ");
                //SQL.AppendLine("    Inner Join TblProjectImplementationDtl2 T5 On T4.PRJIDocNo=T5.DocNo And T4.ResourceItCode=T5.ResourceItCode ");
                SQL.AppendLine("    Where T1.DroppingRequestDocNo Is Not Null ");
                SQL.AppendLine("    And T1.DroppingRequestBCCode Is Null ");
                SQL.AppendLine("    And T1.DocDt Between @DocDt1 And @DocDt2 ");
                //SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
                SQL.AppendLine(") M On B.DocNo=M.DocNo And B.DNo=M.DNo AND B.ItCode = M.ResourceItCode ");

                SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine("    Select T2.DocNo, T2.DNo, Sum(IfNull(T3.Amt, 0.00)) As Amt ");
                SQL.AppendLine("    Select T2.DocNo, T2.DNo, T3.ItCode, T3.Amt ");
                SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo And T2.Status='A' And T2.CancelInd='N' ");
                SQL.AppendLine("    Inner Join TblDroppingRequestDtl2 T3 On T1.DroppingRequestDocNo=T3.DocNo And T1.DroppingRequestBCCode=T3.BCCode And T2.ItCode=T3.ItCode ");
                SQL.AppendLine("    Where T1.DroppingRequestDocNo Is Not Null ");
                SQL.AppendLine("    And T1.DroppingRequestBCCode Is Not Null ");
                SQL.AppendLine("    And T1.DocDt Between @DocDt1 And @DocDt2 ");
                //SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
                SQL.AppendLine(") N On B.DocNo=N.DocNo And B.DNo=N.DNo AND B.ItCode = N.ItCode ");
            }
            SQL.AppendLine("Left Join TblProjectGroup O On A.PGCode=O.PGCode ");

            if(mFrmParent.mIsMRInPORShowApprovalInfo)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select Distinct A.DocNo, A.DNo, C.UserName, B.LastUpDt ");
                SQL.AppendLine("    From ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select DocNo, DNo, Max(LastUpDt) LastUpDt ");
                SQL.AppendLine("        From TblDocApproval ");
                SQL.AppendLine("        Where DocType = 'MaterialRequest' And Status Is Not Null And UserCode Is Not Null And Status = 'A' ");
                SQL.AppendLine("        Group By DocNo, DNo ");
                SQL.AppendLine("    ) A ");
                SQL.AppendLine("    Inner Join TblDocApproval B On A.DocNo = B.DocNo And A.DNo = B.DNo And A.LastUpDt = B.LastUpDt ");
                SQL.AppendLine("        And B.DocType = 'MaterialRequest' ");
                SQL.AppendLine("    Inner Join TblUser C On B.UserCode = C.UserCode ");
                SQL.AppendLine("    Inner Join TblMaterialRequestHdr D On B.DocNo = D.DocNo And (D.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine(") P On A.DocNo = P.DocNo And B.DNo = P.DNo ");
            }
            if (mFrmParent.mIsMRUseProcurementType)
            {
                SQL.AppendLine("Left Join TblOption Q On A.ProcurementType = Q.OptCode AND Q.OptCat = 'ProcurementType' ");
            }

            if(mFrmParent.mIsPORShowDivisionInfo)
            {
                SQL.AppendLine("Left Join TblDivision R On A.DivisionCode = R.DivisionCode ");
            }

            if (mFrmParent.mIsMRUseApprovalSheet)
            {
                SQL.AppendLine("LEFT JOIN tbloption S ON A.MRType = S.OptCode and S.OptCat = 'MRType' ");
            }

            SQL.AppendLine("Where B.Qty-IfNull(K.Qty, 0)-IfNull(H.Qty, 0)<>0 ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsPORequestFilterByDept)
            {
                SQL.AppendLine("And (A.DeptCode Is Null Or ( ");
                SQL.AppendLine("    A.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And (D.ItCtCode Is Null Or ( ");
                SQL.AppendLine("    D.ItCtCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("        Where ItCtCode=IfNull(D.ItCtCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "And Concat(A.DocNo, B.DNo) Not In (" + mFrmParent.GetSelectedMaterialRequest() + ") ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "D.ItName", "D.ForeignName", "D.ItCodeInternal" });
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By " + (mFrmParent.mPORequestMRItemOrderFormat == "1" ? "ItName, DocDt, DocNo;" : "DocNo, DocDt, ItName;"),
                        new string[] 
                        { 
                        
                            //0
                            "DocNo",
                            
                            //1-5
                            "DNo", "DocDt", "DeptName", "ItCode", "ItName", 
                            
                            //6-10
                            "ItCtName", "OutstandingQty", "PurchaseUomCode", "UPrice", "UsageDt",   
                            
                            //11-15
                            "VdName", "Much", "Remark", "RemarkApproval", "ItScCode", 
                            
                            //16-20
                            "ItScName", "LocalDocNo", "SiteCode", "SiteName", "ForeignName", 
                            
                            //21-25
                            "ItCodeInternal", "CurCode", "EstPrice", "PICCode", "DroppingRequestAmt",

                            //26-30
                            "Specification", "ProjectCode", "ProjectName", "ItGrpCode", "ApprovalUserName",

                            //31 - 35
                            "ApprovalLastUpDt", "ProcurementType", "ProcurementCode", "Division", "MRType"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("I", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 30);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 33, 31);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 34, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 32);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 33);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 34);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 35);
                            //selectedprocurementcode = dr[c[33]].ToString();

                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 32, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 34, Grd1, Row2, 22);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 35, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 36, Grd1, Row2, 23);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 37, Grd1, Row2, 24);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 38, Grd1, Row2, 25);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 43, Grd1, Row2, 28);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 44, Grd1, Row2, 27);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 48, Grd1, Row2, 31);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 49, Grd1, Row2, 29);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 50, Grd1, Row2, 30);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 54, Grd1, Row2, 37);

                        mFrmParent.Grd1.Cells[Row1, 19].Value = 
                        mFrmParent.Grd1.Cells[Row1, 20].Value = 
                        mFrmParent.Grd1.Cells[Row1, 22].Value = 
                        mFrmParent.Grd1.Cells[Row1, 23].Value = 
                        mFrmParent.Grd1.Cells[Row1, 24].Value = 
                        mFrmParent.Grd1.Cells[Row1, 27].Value = null;

                        if (Sm.GetLue(mFrmParent.LueProcType).Length == 0) 
                            Sm.SetLue(mFrmParent.LueProcType, Sm.GetGrdStr(Grd1, Row2, 36));

                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 25, 26, 51 });

                        mFrmParent.Grd1.Rows.Add();

                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 14, 25, 26, 38, 51 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");

            mFrmParent.SetSeqNo();
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 6) + Sm.GetGrdStr(mFrmParent.Grd1, Index, 7),
                    Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 3)
                    )) return true;
            return false;
        }

        #endregion

        #region Additional Method

        private void IsItemDocumentEqual(string DocNo)
        {
            if (mFrmParent.mPORequestMRItemOrderFormat == "2")
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && Sm.GetGrdStr(Grd1, Row, 2) == DocNo)
                    {
                        Grd1.Cells[Row, 1].Value = true;
                    }
                }
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {

            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsPORequestFilterByDept ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #region Grid Control Event

        private void Grd1_BeforeCommitEdit(object sender, iGBeforeCommitEditEventArgs e)
        {
            string mDocNo = string.Empty;
            if (e.ColIndex == 1 && !Sm.GetGrdBool(Grd1, e.RowIndex, 1))
            {
                mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                IsItemDocumentEqual(mDocNo);
            }
        }

        #endregion

        #endregion
    }
}
