﻿#region Update
/*
   19/08/2021 [DITA/ALL] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCapitalStatementSettingDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmCapitalStatementSetting mFrmParent;
        private int mRow = 0;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmCapitalStatementSettingDlg2(FrmCapitalStatementSetting FrmParent, int Row) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "List Of Selected COA";
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnRefresh.Visible = BtnChoose.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-4
                    "Account#",
                    "Account Description",
                    "Alias",
                    "Type",

                }, new int[]
                {
                    50,
                    200, 300, 180, 100
                }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4});
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4}, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, Concat(A.AcNo, ' - ', A.AcDesc) AcDesc, A.Alias, ");
            SQL.AppendLine("Case A.AcType When 'C' Then 'Credit' When 'D' Then 'Debit' End As AcType ");
            SQL.AppendLine("From TblCOA A ");
            SQL.AppendLine("Where Find_In_Set(A.AcNo, @AcNo) ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            ShowDataHdr();
            ShowDataDtl();
        }

        private void ShowDataHdr()
        {
            TxtSettingCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 0);
            TxtSettingDesc.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 5);
        }

        private void ShowDataDtl()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                int mListIndex = 0;
                for (int t = 0; t < mFrmParent.lCR.Count; ++t)
                {
                    if (mFrmParent.lCR[t].Row == mRow)
                    {
                        mListIndex = t;
                        break;
                    }
                }

                //Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(mFrmParent.Grd1, mRow, 7));
                Sm.CmParam<String>(ref cm, "@AcNo", mFrmParent.lCR[mListIndex].AcNo);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + " Order By A.AcNo; ",
                    new string[] 
                    { 
                        "AcNo", 
                        "AcDesc", "Alias", "AcType"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

    }
}
