﻿#region Update
/*
    13/01/2020 [RF] Amount input setiap employee
    03/11/2020 [TKG] Saat validasi data sudah pernah diproses atau belum ditambah pengecekan indikator cancel
    24/05/2022 [har] bug saat save AmtInspnt get grd masih ambil string, harusnya decimal
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmEmpInsPnt3 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmEmpInsPnt3Find FrmFind;
        internal bool
            mIsSiteMandatory = false,
            mIsFilterBySite = false,
            mIsEmpInsPntBasedOnDept = false,
            mIsEmpInsPntBasedOnSite = false;

        #endregion

        #region Constructor

        public FrmEmpInsPnt3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Incentive/Penalty";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                SetLuePayment(ref LuePayment);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close(); 
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 5;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-5
                        "Employee's Code",
                        "",
                        "DocNo#",
                        "Old Code",
                        "Employee's Name",

                        //6-10
                        "Department",
                        "Position",
                        "Amount",
                        "Remark",
                        "Payrun Code",
                    },
                     new int[] 
                    {
                        20, 
                        130, 20, 200, 200, 250, 
                        150, 150, 100, 200, 200
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 3, 4, 5, 6, 7, 9, 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 },0);
            Sm.GrdColButton(Grd1, new int[] { 0, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4 }, false);
            Grd1.Cols[10].Move(6);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueInsPnt, LuePayment, ChkCancelInd}, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 8, 9 });
                    DteDocDt.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueInsPnt, LuePayment}, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 8, 9 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                DteDocDt, LueInsPnt, LuePayment
            });
            ChkCancelInd.Checked = false;
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpInsPnt3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueInsPnt(ref LueInsPnt, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(DteDocDt, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (ChkCancelInd.Properties.ReadOnly == true)
                    InsertData(sender, e);
                else
                    EditData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmEmpInsPnt3Dlg(this));

                }
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 &&
                BtnSave.Enabled)
                Sm.FormShowDialog(new FrmEmpInsPnt3Dlg(this));

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (DteDocDt.Text.Length > 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpInsPnt", "TblEmpInsPntHdr");

            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                        if (Row == 0)
                        {
                            cml.Add(SaveEmpInsPnt(DocNo, Row));
                        }
                        else
                        {
                            string SliceNumber = DocNo.Substring(0, 4);
                            int CountNumber = Convert.ToInt32(SliceNumber) + Row;
                            string DocNox = Sm.Right(String.Concat("00000", CountNumber.ToString()), 4);

                            string DocNoRow = String.Concat(DocNox, DocNo.Substring(4, (DocNo.Length - 4))); 
                            cml.Add(SaveEmpInsPnt(DocNoRow, Row));
                        }
                }

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueInsPnt, "Incentive/Penalty") ||
                IsGrdEmpty() ||
                IsAmtSmallerThanZero() ||
                IsDataInsPenExist() ||
                IsEmployeeExist();
            ;

        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveEmpInsPnt(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblEmpInsPntHdr " +
                    "(DocNo, DocDt, CancelInd, InspntCode, PaymentType, " +
                    "CreateBy, CreateDt) " +
                    "Values (@DocNo, @DocDt, 'N', @InspntCode, @PaymentType, " +
                    "@CreateBy, CurrentDateTime()); " +
                    "Insert Into TblEmpInsPntDtl(DocNo, DNo, EmpCode, AmtInspnt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @EmpCode, @AmtInspnt, @Remark, @CreateBy, CurrentDateTime());"
            };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@AmtInspnt", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@InspntCode", Sm.GetLue(LueInsPnt));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePayment));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditEmpInsPnt());

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueInsPnt, "Insentif/Penalty") ||
                IsDataCancelledAlready();
        }

        private bool IsAmtSmallerThanZero()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdDec(Grd1, Row, 8) <= 0)
                {
                    Msg =
                    "Employee's Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                    "Employee's Name : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine;

                    Sm.StdMsg(mMsgType.Warning, Msg + "Amount must be greater than zero.");
                    return true;
                 }
            }
            return false;
        }

        private bool IsEmployeeExist()
        {
            string Msg = string.Empty;
            string DataTemp = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                DataTemp = Sm.GetValue("Select * From TblEmployee Where EmpCode = @Param", Sm.GetGrdStr(Grd1, Row, 1));
                if (DataTemp.Length == 0)
                {
                    Msg = "Employee's Code : " + Sm.GetGrdStr(Grd1, Row, 1);

                    Sm.StdMsg(mMsgType.Warning, Msg + " is NOT exist.");
                    return true;
                }
                DataTemp = null;
            }
            return false;
        }

        private bool IsDataInsPenExist()
        {
            string 
                EmpCode = string.Empty,
                YrMth = Sm.GetDte(DteDocDt).Substring(0, 6),
                InsPntCode = Sm.GetLue(LueInsPnt);
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblEmpInsPntHdr A, TblEmpInsPntDtl B ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And Left(A.DocDt,6)=@Param1 ");
            SQL.AppendLine("And A.InsPntCode=@Param2 ");
            SQL.AppendLine("And B.EmpCode=@Param3; ");

            for (int r= 0; r < Grd1.Rows.Count - 1; r++)
            {
                EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                if (EmpCode.Length > 0)
                {
                    if (Sm.IsDataExist(SQL.ToString(), YrMth, InsPntCode, EmpCode))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Employee's Code : " + EmpCode + Environment.NewLine +
                            "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine + Environment.NewLine +
                            "This data already processed.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return
                Sm.IsDataExist(
                    "Select 1 From TblEmpInsPntHdr Where CancelInd='Y' And DocNo=@Param;",
                    Sm.GetGrdStr(Grd1, 0, 3),
                    "This document already cancelled.");
        }

        private MySqlCommand EditEmpInsPnt()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpInsPntHdr Set ");
            SQL.AppendLine("CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, 0, 3));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowEmpInsPntHdr(DocNo);
                ShowEmpInsPntDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpInsPntHdr(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.DeptCode, A.SiteCode, ");
            SQL.AppendLine("A.InspntCode, A.AmtInspnt, A.PaymentType, A.Remark  ");
            SQL.AppendLine("From TblEmpInspnthdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd",  "DeptCode", "SiteCode", "InspntCode", 
                        
                        //6-8
                        "AmtInspnt", "PaymentType", "Remark"
                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    SetLueInsPnt(ref LueInsPnt, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LueInsPnt, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LuePayment, Sm.DrStr(dr, c[7]));
                }, true
        );

        }

        private void ShowEmpInsPntDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.EmpCode, C.EmpCodeOld, C.Empname, D.DeptName, E.Posname, B.AmtInspnt, B.Remark, B.PayrunCode, F.SiteName ");
            SQL.AppendLine("From TblEmpInsPntHdr A ");
            SQL.AppendLine("Inner Join TblEmpInsPntDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode = C.EmpCode  ");
            SQL.AppendLine("Inner Join TblDepartment D on C.DeptCode = D.DeptCode ");
            SQL.AppendLine("Inner Join TblPosition E On C.PosCode = E.PosCode  ");

            if (mIsFilterBySite)
                SQL.AppendLine("Inner Join TblSite F on C.SiteCode=F.SiteCode ");
            else
                SQL.AppendLine("Left Join TblSite F on C.SiteCode=F.SiteCode ");

            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode",
 
                    //1-5
                    "EmpCodeOld", "Empname", "DeptName",  "PosName", "AmtInspnt",

                    //6-8
                    "Remark", "PayrunCode", "DocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ProcessData()
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date")) return;

            Cursor.Current = Cursors.WaitCursor;

            var lResult = new List<Result>();

            ClearGrd();
            try
            {
                Process1(ref lResult);
                if (lResult.Count > 0)
                {
                    Process2(ref lResult);
                    Process3();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lResult.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8 });
        }

        private void Process1(ref List<Result> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            if (openFileDialog1.FileName == "openFileDialog1" || openFileDialog1.FileName == null)
            {
                Sm.StdMsg(mMsgType.Warning, "File Not Found.");
            }
            else
            {
                var FileName = openFileDialog1.FileName;
                var EmpCodeTemp = string.Empty;
                var AmtTemp = 0m;

                bool IsFirst = true;

                using (var rd = new StreamReader(@FileName))
                {
                    while (!rd.EndOfStream)
                    {
                        var line = rd.ReadLine();
                        var arr = line.Split(',');

                        if (IsFirst)
                            IsFirst = false;
                        else
                        {
                            if (arr[0].Trim().Length > 0)
                            {
                                if (arr[0].Trim().Length != 8)
                                {
                                    EmpCodeTemp = Sm.Right(string.Concat("0000000" + arr[0].Trim()), 8);
                                }
                                else
                                {
                                    EmpCodeTemp = arr[0].Trim() ;
                                }
                                string AmtTrim = arr[1].Trim();
                                if (arr[1].Trim().Length > 0)
                                    AmtTemp = Decimal.Parse(AmtTrim);
                                else
                                    AmtTemp = 0m;

                                l.Add(new Result()
                                {
                                    EmpCode = EmpCodeTemp,
                                    DocNo = string.Empty,
                                    EmpCodeOld = string.Empty,
                                    EmpName = string.Empty,
                                    DeptName = string.Empty,
                                    Amount = AmtTemp,
                                    PosName = string.Empty,
                                    Remark = string.Empty,
                                    PayrunCode = string.Empty,
                                });
                            }
                        }
                    }
                }
            }
        }

        private void Process2(ref List<Result> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = string.Empty;
                r.Cells[1].Value = l[i].EmpCode;
                r.Cells[2].Value = string.Empty;
                r.Cells[3].Value = string.Empty;
                r.Cells[4].Value = string.Empty;
                r.Cells[5].Value = string.Empty;
                r.Cells[6].Value = string.Empty;
                r.Cells[7].Value = string.Empty;
                r.Cells[8].Value = l[i].Amount;
                r.Cells[9].Value = string.Empty;
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8 });
            Grd1.EndUpdate();
        }

        private void Process3()
        {
            Grd1.BeginUpdate();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var EmpCode = string.Empty;
            var Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + Row.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + Row.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 1=0 ";

            SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.EmpName, ");
            SQL.AppendLine("B.DeptName, C.PosName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode=C.PosCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "EmpCodeOld", 
                    "EmpName", 
                    "DeptName", 
                    "PosName"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (EmpCode == Sm.GetGrdStr(Grd1, Row, 1))
                            {
                                Grd1.Cells[Row, 4].Value = Sm.DrStr(dr, c[1]);
                                Grd1.Cells[Row, 5].Value = Sm.DrStr(dr, c[2]);
                                Grd1.Cells[Row, 6].Value = Sm.DrStr(dr, c[3]);
                                Grd1.Cells[Row, 7].Value = Sm.DrStr(dr, c[4]);
                            }
                        }
                    }
                }
                dr.Close();
            }
            Grd1.EndUpdate();
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
            mIsSiteMandatory = Sm.GetParameter("IsSiteMandatory") == "Y";
            mIsEmpInsPntBasedOnDept = Sm.GetParameter("IsEmpInsPntBasedOnDept") == "Y";
            mIsEmpInsPntBasedOnSite = Sm.GetParameter("IsEmpInsPntBasedOnSite") == "Y";
        }

        private void SetLuePenalty(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select InspntCode As Col1, InspntName As Col2 From TblInspnt;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLuePayment(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='PenaltyPaymentType'",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueInsPnt(ref LookUpEdit Lue, string InsPntCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select InsPntCode As Col1, InspntName As Col2 ");
            SQL.AppendLine("From TblInsPnt ");
            if (InsPntCode.Length <= 0)
                SQL.AppendLine("Where ActInd='Y' ");
            else
                SQL.AppendLine("Where InsPntCode=@InsPntCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (InsPntCode.Length > 0) Sm.CmParam<String>(ref cm, "@InsPntCode", InsPntCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void LuePayment_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePayment, new Sm.RefreshLue1(SetLuePayment));
            }
        }

        private void LueInsPnt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueInsPnt, new Sm.RefreshLue2(SetLueInsPnt), string.Empty);
            }
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            ProcessData();
        }

        #endregion

        #region Class

        private class Result
        {
            public string EmpCode { get; set; }
            public string DocNo { get; set; }
            public string EmpCodeOld { get; set; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public decimal Amount { get; set; }
            public string Remark { get; set; }
            public string PayrunCode { get; set; }     
        }

        #endregion
    }
}
