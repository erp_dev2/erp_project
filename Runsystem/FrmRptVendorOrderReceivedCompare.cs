﻿#region Update
/*
    06/10/2017 [TKG] merapikan title, amount menggunakan disc+rounding+tax, data didasarkan PO      
    11/04/2022 [RIS/PHT] Merubah source qty dan amount menggunakan parameter IsRptProcurementUsePORevision
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptVendorOrderReceivedCompare : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool mIsShowForeignName = false, mIsFilterByItCt = false;
        private bool mIsRptProcurementUsePORevision = false;

        #endregion

        #region Constructor

        public FrmRptVendorOrderReceivedCompare(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueVdCode(ref LueVdCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();
            var SubQuery = string.Empty;
            if (mIsRptProcurementUsePORevision)
            {
                SubQuery =
                    "(((ifnull(M.Qty, B.Qty)-IfNull(H.Qty, 0.00))*I.UPrice)- " +
                    "(((ifnull(M.Qty, B.Qty)-IfNull(H.Qty, 0.00))*I.UPrice)*ifnull(M.Discount, B.Discount)*0.01)- " +
                    "ifnull(M.DiscountAmt, B.DiscountAmt)+ifnull(M.RoundingValue, B.RoundingValue)) ";

            } else
            {
                SubQuery =
                    "(((B.Qty-IfNull(H.Qty, 0.00))*ifnull(N.UPrice, I.UPrice))- " +
                    "(((B.Qty-IfNull(H.Qty, 0.00))*ifnull(N.UPrice, I.UPrice))*ifnull(N.DiscountOld, B.Discount)*0.01)- " +
                    "ifnull(N.DiscountAmtOld, B.DiscountAmt)+ifnull(N.RoundingValueOld, B.RoundingValue)) ";
            }

            SQL.AppendLine("Select VdName, ItCode, ItName, ForeignName, CurCode, ");
            SQL.AppendLine("Sum(POQty) As POQty, Sum(RecvQty) As RecvQty, Sum(TotalOrder) As TotalOrder, Sum(Amt) As Amt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select F.VdName, E.ItCode, E.ItName, E.ForeignName, A.CurCode, ");
            if(mIsRptProcurementUsePORevision)
                SQL.AppendLine("ifnull(M.Qty, B.Qty)-IfNull(H.Qty, 0.00) As POQty, ");
            else
                SQL.AppendLine("B.Qty-IfNull(H.Qty, 0.00) As POQty, ");
            SQL.AppendLine("IfNull(G.Qty, 0.00) As RecvQty, ");
            SQL.AppendLine(SubQuery + "+");
            SQL.AppendLine("(" + SubQuery + "*IfNull(J.TaxRate, 0.00)*0.01)+");
            SQL.AppendLine("(" + SubQuery + "*IfNull(K.TaxRate, 0.00)*0.01)+");
            SQL.AppendLine("(" + SubQuery + "*IfNull(L.TaxRate, 0.00)*0.01) ");
            SQL.AppendLine(" As Amt, ");
            SQL.AppendLine("Case When B.Qty<=IfNull(H.Qty, 0.00) Then 0.00 Else 1.00 End As TotalOrder ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=E.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblVendor F On A.VdCode=F.VdCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.PODocNo, T2.PODNo, Sum(QtyPurchase) As Qty ");
            SQL.AppendLine("    From TblRecvVdHdr T1 ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.Status='A' ");
            SQL.AppendLine("    Inner Join TblPOHdr T3 On T2.PODocNo=T3.DocNo And Left(T3.DocDt, 6)=@YrMth ");
            SQL.AppendLine("    Group By T2.PODocNo, T2.PODNo ");
            SQL.AppendLine(") G On B.DocNo=G.PODocNo And B.DNo=G.PODNo ");
            SQL.AppendLine("Left Join TblPOQtyCancel H On B.DocNo=H.PODocNo And B.DNo=H.PODNo And H.CancelInd='N' ");
            SQL.AppendLine("Left Join TblQtDtl I On C.QtDocNo=I.DocNo And C.QtDNo=I.DNo ");
            SQL.AppendLine("Left Join TblTax J On A.TaxCode1=J.TaxCode ");
            SQL.AppendLine("Left Join TblTax K On A.TaxCode2=K.TaxCode ");
            SQL.AppendLine("Left Join TblTax L On A.TaxCode3=L.TaxCode ");
            if(mIsRptProcurementUsePORevision)
            {
                SQL.AppendLine("LEFT JOIN ( ");
                SQL.AppendLine("    SELECT A.DocNo AS PoDoCno, A.DNo AS PoDNo, B.Qty, C.UPrice, B.Discount, B.DiscountAmt, B.RoundingValue ");
                SQL.AppendLine("    FROM tblpodtl A ");
                SQL.AppendLine("    INNER JOIN TblPoRevision B ON A.DocNo = B.podocno AND A.DNo = B.PoDNo ");
                SQL.AppendLine("    INNER JOIN TblQtDtl C ON B.QtDocNo = C.DocNo AND B.QtDNo = C.DNo ");
                SQL.AppendLine("    WHERE B.docno = (SELECT MAX(DocNo) FROM TblPoRevision WHERE PoDocNo = B.PoDocNo AND PODNo = B.PODNo AND STATUS = 'A') ");
                SQL.AppendLine(") M ON A.DocNo = M.PoDocNo AND B.DNo = M.PoDNo ");
            }
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.DocNo AS PoDoCno, A.DNo AS PoDNo, C.DocNo AS QtDocNo, C.UPrice, B.Discountold, B.DiscountAmtOld, B.RoundingValueOld ");
            SQL.AppendLine("    From TblPoDtl A ");
            SQL.AppendLine("    Inner JOIN TblPoRevision B ON A.DocNo =  B.podocno AND A.DNo = B.PoDNo ");
            SQL.AppendLine("    Inner Join TblQtDtl C ON B.QtDocNoOld = C.DocNo AND B.QtDNoOld = C.DNo  ");
            SQL.AppendLine("    Where B.docno = (Select MIN(DocNo) From TblPoRevision Where PoDocNo = B.PoDocNo And PODNo = B.PODNo And Status = 'A' ) ");
            SQL.AppendLine(") N ON A.DocNo = N.PoDocNo And B.DNo = N.PoDNo ");
            SQL.AppendLine("Where Left(A.DocDt, 6)=@YrMth ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(") T ");
            SQL.AppendLine("Where POQty>0.00 ");
            SQL.AppendLine("Group By VdName, ItCode, ItName, ForeignName, CurCode ");
            SQL.AppendLine("Order By VdName, ItName, ItCode, CurCode; ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Vendor",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Foreign Name",
                        "Total"+Environment.NewLine+"Order",

                        //6-9
                        "Ordered"+Environment.NewLine+"Quantity",
                        "Received"+Environment.NewLine+"Quantity",
                        "Currency",
                        "Amount"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 100, 200, 150, 120,

                        //6-9
                        120, 120, 80, 130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 9 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            if (!mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {

            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            if (Sm.IsLueEmpty(LueMth, "Month")) return;

            Cursor.Current = Cursors.WaitCursor;
            string Filter = " ";
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Sm.GetLue(LueYr) + Sm.GetLue(LueMth)));
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);

            try
            {
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter),
                    new string[]
                    { 
                        //0
                        "VdName", 

                        //1-5
                        "ItCode", "ItName", "ForeignName", "TotalOrder", "POQty", 
                        
                        //6-8
                        "RecvQty", "CurCode", "Amt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 6, 7, 9 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsRptProcurementUsePORevision = Sm.GetParameterBoo("IsRptProcurementUsePORevision");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        #endregion

        #endregion
    }
}
