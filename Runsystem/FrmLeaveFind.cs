﻿#region Update
/*
    27/12/2017 [TKG] tambah informasi point.
 *  23/12/2019 [RF/SIER] tambah informasi leave color.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmLeaveFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmLeave mFrmParent;

        #endregion

        #region Constructor

        public FrmLeaveFind(FrmLeave FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Leave"+Environment.NewLine+"Code", 
                        "Leave Name",
                        "Short Name",
                        "Group",
                        "Active",
                        
                        //6-10
                        "Paid",
                        "Point",
                        "Leave Color",
                        "Created"+Environment.NewLine+"By",   
                        "Created"+Environment.NewLine+"Date",  
                        
                        //11-14
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 200, 120, 150, 60, 
                        
                        //6-10
                        60, 100, 100, 100, 100, 

                        //11-14
                        100, 100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 5, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 10, 13 });
            Sm.GrdFormatTime(Grd1, new int[] { 11, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, false);
            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtLeaveName.Text, new string[] { "LeaveCode", "LeaveName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select A.LeaveCode, A.LeaveName, A.ShortName, C.OptDesc As LeaveColor, B.LGName, A.ActInd, A.PaidInd, A.Point, " +
                        "A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt " +
                        "From TblLeave A " +
                        "Left Join TblLeaveGrp B On A.LGCode=B.LGCode " +
                        "Left Join tbloption C On A.LeaveColor = C.OptCode And C.OptCat = 'LeaveColor'  " +
                        Filter + " Order By A.LeaveName;",
                        new string[]
                        {
                            //0
                            "LeaveCode", 
                                
                            //1-5
                            "LeaveName", "ShortName", "LGName", "ActInd", "PaidInd", 
                            
                            //6-10
                            "Point", "LeaveColor", "CreateBy", "CreateDt", "LastUpBy", 
                            
                            //11
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 14, 11);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtLeaveName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLeaveName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Leave");
        }

        #endregion

        #endregion
    }
}
