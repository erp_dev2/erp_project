﻿#region Update
/*
    20/04/2021 [PHT/TKG] Travel Request PHT
 *  27/04/2021 [PHT/ICA] tambah fitur download
 *  27/06/2022 [TYO/PRODUCT] Menghapus Environment.NewLine di SetGrd() untuk Approval Status
 *  10/03/2023 [MAU/PHT] -> Tambah kolom Old Code, Site, Total
 *                       -> Tambah filter Employee (EmpName, EmpCode, EmpCodeOld)
 *                       -> Tambah filter dept
 *                       -> Tambah filter Site
 *  15/03/2023 [RDA/PHT] tambah informasi docno voucher berdasarkan parameter IsTravelRequestUseVoucher
 *  16/03/2023 [RDA/PHT] tambah validasi untuk task informasi docno voucher
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmTravelRequest5Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmTravelRequest5 mFrmParent;
        private string mSQL = string.Empty;

        private string
            mDocTitle = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty;

        internal byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmTravelRequest5Find(FrmTravelRequest5 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Docno, A.DocDt,   ");
            SQL.AppendLine("Case when A.Status = 'A' then 'Approve' ");
            SQL.AppendLine("When A.Status='O' Then 'Outstanding'  ");
            SQL.AppendLine("when A.Status='C' Then 'Cancelled' End As DocStatus, A.CancelInd, A.FileName,  ");
            SQL.AppendLine("B.EmpCode, C.EmpName, D.Deptname, A.StartDt, A.EndDt, ");
            SQL.AppendLine("A.TravelService, A.Result, E.RegionName, A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, C.EmpCodeOld,F.SiteName As Site,  ");
            SQL.AppendLine("(B.Amt1 + B.Amt2 + B.Amt3 + B.Amt4 + B.Amt5 + B.Amt6 + B.Detasering) As Total, G.DocNo as VoucherDocNo ");

            SQL.AppendLine("From TblTravelRequestHdr A  ");
            SQL.AppendLine("Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode = C.EmpCode ");
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And C.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And C.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(C.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblDepartment D On C.DeptCode = D.DeptCode ");
            SQL.AppendLine("Left Join TblRegion E On A.RegionCode=E.RegionCode ");
            SQL.AppendLine("LEFT JOIN tblsite F ON A.SiteCode = F.SiteCode ");
            SQL.AppendLine("LEFT JOIN tblvoucherhdr G ON B.VoucherRequestDocNo = G.VoucherRequestDocNo and G.CancelInd='N' ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "Document#", 
                        "Date",
                        "Approval Status",
                        "Cancel",
                        "Employee"+Environment.NewLine+"Code",

                        //6-10
                        "Employee",
                        "Department",
                        "Start Date",
                        "End Date",
                        "Travel Service",
                        
                        //11-15
                        "Result",
                        "Region",
                        "Remark",
                        "FileName", 
                        "",
                        
                        //16-20
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 

                        //21-25
                        "Last"+Environment.NewLine+"Updated Time", 
                        "Old Code",
                        "Site",
                        "Total",
                        "Voucher#"

                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        150, 100, 100, 80, 100,                         
                        //6-10
                        200, 150, 120, 120, 200, 
                        //11-15
                        250, 200, 250, 200, 20, 
                        //16-20
                        100, 120, 120, 100, 120, 
                        //21-25
                        120,200,250,180, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 15 });
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 8, 9, 17, 20 });
            Sm.GrdFormatTime(Grd1, new int[] { 18, 21 });
            Sm.GrdFormatDec(Grd1, new int[] { 24}, 0);
            if (!mFrmParent.mIsTravelRequestUseVoucher)
                Sm.GrdColInvisible(Grd1, new int[] { 5, 16, 17, 18, 19, 20, 21, 25 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 5, 16, 17, 18, 19, 20, 21 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 });
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[22].Move(5);
            Grd1.Cols[23].Move(9);
            Grd1.Cols[24].Move(15);
            Grd1.Cols[25].Move(17);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 20, 21 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "B.EmpCode", "C.EmpCodeOld", "C.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "C.SiteCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                        {
                            //0
                            "DocNo",
                            //1-5
                            "DocDt", "DocStatus", "CancelInd", "EmpCode", "EmpName",  
                            //6-10
                            "Deptname", "StartDt", "EndDt", "TravelService", "Result",
                            //11-15
                            "RegionName", "Remark", "FileName", "CreateBy", "CreateDt", 
                            //16-20
                            "LastUpBy", "LastUpDt" , "EmpCodeOld" , "Site", "Total",
                            //21
                            "VoucherDocNo"
                        },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 18, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 17);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);


                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 24 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length != 0)
            {
                DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 14), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                SFD.DefaultExt = "";
                SFD.AddExtension = true;

                if (downloadedData != null && downloadedData.Length != 0)
                {

                    if (SFD.ShowDialog() == DialogResult.OK)
                    {
                        Application.DoEvents();

                        //Write the bytes to a file
                        FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                        newFile.Write(downloadedData, 0, downloadedData.Length);
                        newFile.Close();
                        MessageBox.Show("Saved Successfully");
                    }
                }
                else
                    MessageBox.Show("No File was Downloaded Yet!");
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mDocTitle = Sm.GetParameter("DocTitle");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            if (mDocTitle == "PHT") mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient") + "/travel-request";
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkKPIName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "KPI Name");
        }

        private void TxtDocNo_Validating(object sender, CancelEventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtKPIName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }
        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }
        #endregion

        #endregion
    }
}
