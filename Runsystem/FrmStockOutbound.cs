﻿#region Update
/*
    12/11/2019 [TKG/SIER] New Application
    06/01/2020 [TKG/SIER] tambah customer, panjang, lebar, tinggi, berat. tampilkan lot/bin. sembungikan qty 2 dan qty 3.
    15/01/2020 [WED/SIER] tambah approval, DocType : StockOutbound
    28/02/2020 [VIN/SIER] print out
    05/03/2020 [WED/SIER] approval per warehouse berdasarkan parameter IsStockOutboundApprovalBasedOnWhs
    26/06/2020 [VIN/SIER] bisa edit stock outbound saat processind masih draft
    08/07/2020 [ICA/SIER] tambah kolom DO/SO Reference
    09/07/2020 [ICA/SIER] Perubahan printout untuk delivery order dan picking list
    13/07/2020 [VIN/SIER] Feedback edit stock outbound saat Processind sudah final
    16/07/2020 [TKG/SIER] outstanding quantity dikurangi quantity outbound yg masih dalam status draft (RecomputeStock)
    16/07/2020 [TKG/SIER] expired date dan stock status diambil dari source info
    17/07/2020 [VIN/SIER] Bug edit stock outbound saat Processind masih draft bisa remove row di grid
    17/07/2020 [DITA/SIER] Fasilitas import
    22/07/2020 [TKG/SIER] bug recomputestock
    22/07/2020 [IBL/SRN] Mengganti Customer menjadi Vendor berdasarkan parameter StockOutboundFormat
 *  24/08/2020 [ICA/SIER] Tambah Kolom Remark Inbound
    27/08/2020 [DITA/SIER] Tambah informasi lot dan bin untuk import data
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmStockOutbound : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,mInitProcessInd= string.Empty,
            mDocNo = string.Empty, //if this application is called from other application;
            mStockOutboundFormat = string.Empty;
        internal FrmStockOutboundFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private string mDocType = "53";
        private bool 
            mIsStockOutboundApprovalBasedOnWhs = false;

        
        #endregion

        #region Constructor

        public FrmStockOutbound(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                if(mStockOutboundFormat == "1")
                    Sl.SetLueCtCode(ref LueCtCode);
                else
                    Sl.SetLueVdCode(ref LueCtCode);
                SetLueProcess(ref LueProcessInd);

                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (mStockOutboundFormat == "1")
                    LblCtVd.Text = "Customer";
                else
                    LblCtVd.Text = "Vendor";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 31;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Item's Code",
                        "Item's Name",
                        
                        //6-10
                        "Batch#",
                        "Source",
                        "Lot",
                        "Bin",
                        "Stock",
                        
                        //11-15
                        "Quantity",
                        "UoM",
                        "Stock",
                        "Quantity",
                        "UoM",

                        //16-20
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Status",
                        "Expired",

                        //21-25
                        "Remark",
                        "Length",
                        "UoM",
                        "Width",
                        "UoM",
                        
                        //26-30
                        "Height",
                        "UoM",
                        "Weight",
                        "UoM",
                        "Remark Inbound"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        80, 0, 20, 100, 200,
                        
                        //6-10
                        200, 170, 60, 50, 80, 
                        
                        //11-15
                        80, 60, 0, 0, 0,

                        //16-20
                        0, 80, 60, 100, 100,

                        //21-25
                        200, 80, 80, 80, 80,

                        //26-30
                        80, 80, 80, 80, 200
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 20 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 13, 14, 16, 17, 22, 24, 26, 28 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 7, 10, 13, 14, 15, 16, 17, 18 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 15, 16, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30 });
            ShowInventoryUomCode();
            Grd1.Cols[30].Move(21);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15 }, true);
            
            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16, 17, 18 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueCtCode, TmeExitTm, TxtVehicleRegNo, 
                        TxtTransportType, MeeRemark, LueProcessInd , TxtDOReference 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 11, 14, 17, 21 });
                    Sm.GrdColInvisible(Grd1, new int[] { 3, 10, 13, 16 }, false);
                    Sm.GrdColInvisible(Grd1, new int[] { 1 }, true);
                    BtnImport.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueCtCode, TmeExitTm, TxtVehicleRegNo, 
                        TxtTransportType, MeeRemark  , TxtDOReference 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 11, 14, 17, 21 });
                    Sm.GrdColInvisible(Grd1, new int[] { 3, 10 }, true);
                    Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
                    if (mNumberOfInventoryUomCode == 2)
                        Sm.GrdColInvisible(Grd1, new int[] { 13 }, true);
                    if (mNumberOfInventoryUomCode == 3)
                        Sm.GrdColInvisible(Grd1, new int[] { 13, 16 }, true);
                    BtnImport.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    mInitProcessInd = Sm.GetValue("Select ProcessInd From TblStockOutboundHdr Where DocNo = @Param;", TxtDocNo.Text);
                    if (Sm.GetLue(LueProcessInd) == "D")
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LueProcessInd,  MeeRemark, TxtDOReference 
                        }, false);
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 11, 21 });
                        Sm.GrdColInvisible(Grd1, new int[] { 3 }, true);
                        BtnImport.Enabled = true;

                    }
                    else
                    {
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    }
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, LueCtCode, TmeExitTm, 
                TxtVehicleRegNo, TxtTransportType, MeeRemark, TxtStatus, LueProcessInd,
                TxtDOReference 
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10, 11, 13, 14, 16, 17, 22, 24, 26, 28 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmStockOutboundFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sm.SetLue(LueProcessInd, "D");
                TxtStatus.EditValue = "Outstanding";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;
            ParPrint();
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmStockOutboundDlg(this, Sm.GetLue(LueWhsCode)));
                }

                if (Sm.IsGrdColSelected(new int[] { 3, 11, 14, 17, 21 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 11, 13, 14, 16, 17, 22, 24, 26, 28 });
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (Sm.GetLue(LueProcessInd) == "D" || (mInitProcessInd.Length > 0 && mInitProcessInd == "D"))
                {
                    Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                    Sm.GrdRemoveRow(Grd1, e, BtnSave);
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && (mStockOutboundFormat == "1" ? true : !Sm.IsLueEmpty(LueCtCode, "Vendor")))
                Sm.FormShowDialog(new FrmStockOutboundDlg(this, Sm.GetLue(LueWhsCode)));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 11, 14, 17 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 21 }, e);

            if (e.ColIndex == 11)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 11, 14, 17, 12, 15, 18);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 11, 17, 14, 12, 18, 15);
            }

            if (e.ColIndex == 14)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 4, 14, 11, 17, 15, 12, 18);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 4, 14, 17, 11, 15, 18, 12);
            }

            if (e.ColIndex == 17)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 4, 17, 11, 14, 18, 12, 15);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 4, 17, 14, 11, 18, 15, 12);
            }

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 12), Sm.GetGrdStr(Grd1, e.RowIndex, 15)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 14, Grd1, e.RowIndex, 11);
            
            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 12), Sm.GetGrdStr(Grd1, e.RowIndex, 18)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 17, Grd1, e.RowIndex, 11);
            
            if (e.ColIndex == 14 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), Sm.GetGrdStr(Grd1, e.RowIndex, 18)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 17, Grd1, e.RowIndex, 14);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10, 11, 13, 14, 16, 17 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length!=0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "StockOutbound", "TblStockOutboundHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveStockOutboundHdr(DocNo));
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                    cml.Add(SaveStockOutboundDtl(DocNo, r));

            

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            ReComputeStock();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueProcessInd, "Process") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                (mStockOutboundFormat == "1" ? Sm.IsLueEmpty(LueCtCode, "Customer") : Sm.IsLueEmpty(LueCtCode, "Vendor")) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt));
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 100000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (99.999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 5, false, "Item is empty.")) return true;

                Msg =
                  "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                  "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                  "Batch# : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                  "Source : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                  "Lot : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                  "Bin : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 11) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.00.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 10) < Sm.GetGrdDec(Grd1, Row, 11))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Stock should not be less than quantity.");
                    return true;
                }

                if (Sm.GetGrdStr(Grd1, Row, 7).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Source is empty.");
                    return true;
                }

                if (Grd1.Cols[14].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 14) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.00.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 13) < Sm.GetGrdDec(Grd1, Row, 14))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock should not be less than quantity.");
                        return true;
                    }
                }

                if (Grd1.Cols[17].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 17) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.00.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 16) < Sm.GetGrdDec(Grd1, Row, 17))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock should not be less than quantity.");
                        return true;
                    }
                }
            }
            return false;
        }
        private MySqlCommand SaveDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='StockOutbound' ");
            if (mIsStockOutboundApprovalBasedOnWhs)
                SQL.AppendLine("And T.WhsCode = @WhsCode ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblStockOutboundHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;

        }
        private MySqlCommand SaveStockOutboundHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockOutboundHdr(DocNo, DocDt, Status, ProcessInd, WhsCode, CtCode, VdCode, ExitTm, VehicleRegNo, TransportType, DOReference, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @ProcessInd, @WhsCode, @CtCode, @VdCode, @ExitTm, @VehicleRegNo, @TransportType, DOReference, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            if(mStockOutboundFormat == "1")
                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            else
                Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@ExitTm", Sm.GetTme(TmeExitTm));
            Sm.CmParam<String>(ref cm, "@VehicleRegNo", TxtVehicleRegNo.Text);
            Sm.CmParam<String>(ref cm, "@TransportType", TxtTransportType.Text);
            Sm.CmParam<String>(ref cm, "@DOReference", TxtDOReference.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockOutboundDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockOutboundDtl(DocNo, DNo, ItCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @BatchNo, @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, @Remark, @UserCode, CurrentDateTime());");
            
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 5));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockMovement(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, B.Source, 'N', '', ");
            SQL.AppendLine("A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, ");
            SQL.AppendLine("-1.00*B.Qty, -1.00*B.Qty2, -1.00*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOutboundHdr A ");
            SQL.AppendLine("Inner Join TblStockOutboundDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3 ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And Lot=@Lot ");
            SQL.AppendLine("And Bin=@Bin ");
            SQL.AppendLine("And Source=@Source ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 7));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, r, 8));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, r, 14));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, r, 17));
            
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            string DNo = "'XXXXX'";
            string DocNo = TxtDocNo.Text;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            if (Sm.GetLue(LueProcessInd) == "D")
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid2() || IsInsertedDataNotValid()) return;

                cml.Add(EditStockOutboundHdr());
                cml.Add(DeleteStockOutboundDtl());
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                        cml.Add(SaveStockOutboundDtl(DocNo, Row));
            }
            if (Sm.GetLue(LueProcessInd) == "F")
            {
                if (mInitProcessInd == "D")
                {
                    if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid2() || IsInsertedDataNotValid()) return;

                    cml.Add(EditStockOutboundHdr());
                    cml.Add(DeleteStockOutboundDtl());
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                            cml.Add(SaveStockOutboundDtl(DocNo, Row));

                    cml.Add(SaveDocApproval(DocNo));
                    cml.Add(SaveStockMovement(DocNo));
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                        if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                            cml.Add(SaveStockSummary(r));
                }
                else
                {
                    UpdateCancelledItem();
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                            DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

                    if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid(DNo)) return;

                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                            cml.Add(EditStockOutboundDtl(Row));
                    }
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblStockOutboundDtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                {
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                }
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsEditedDataNotValid(string DNo)
        {
            return
                IsCancelledItemNotExisted(DNo);
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXXXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsEditedDataNotValid2()
        {
            return
                IsDataAlreadyFinal();
        }

        private bool IsDataAlreadyFinal()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblStockOutboundHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And ProcessInd = 'F' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already Final.");
                return true;
            }

            return false;
        }

        private MySqlCommand DeleteStockOutboundDtl()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblStockOutboundDtl Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists (Select DocNo From TblStockOutboundHdr Where DocNo = @DocNo); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }


        private MySqlCommand EditStockOutboundHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockOutboundHdr Set ");
            SQL.AppendLine(" ProcessInd=@ProcessInd, DOReference=@DOReference, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<String>(ref cm, "@DOReference", TxtDOReference.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


            return cm;
        }


        private MySqlCommand EditStockOutboundDtl(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockOutboundDtl Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DNo=@DNo; ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'Y', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOutboundHdr A ");
            SQL.AppendLine("Inner Join TblStockOutboundDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary T Set ");
            SQL.AppendLine("    T.Qty=T.Qty+@Qty, T.Qty2=T.Qty2+@Qty2, T.Qty3=T.Qty3+@Qty3, T.LastUpBy=@UserCode, T.LastUpDt=CurrentDateTime()  ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And Lot=@Lot ");
            SQL.AppendLine("And Bin=@Bin ");
            SQL.AppendLine("And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, r, 0));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, r, 8));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 9));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 7));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, r, 14));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, r, 17));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowStockOutboundHdr(DocNo);
                ShowStockOutboundDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowStockOutboundHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, WhsCode, CtCode, VdCode, ExitTm, VehicleRegNo, TransportType, DOReference, Remark, ProcessInd, ");
            SQL.AppendLine("Case Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc ");
            SQL.AppendLine("From TblStockOutboundHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DocNo", 
                    "DocDt", "WhsCode", "CtCode", "ExitTm", "VehicleRegNo", 
                    "TransportType", "DOReference", "Remark", "StatusDesc", "ProcessInd",
                    "VdCode"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                    if(mStockOutboundFormat == "1")
                        Sl.SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[3]));
                    else
                        Sl.SetLueVdCode2(ref LueCtCode, Sm.DrStr(dr, c[11]));
                    Sm.SetTme(TmeExitTm, Sm.DrStr(dr, c[4]));
                    TxtVehicleRegNo.EditValue = Sm.DrStr(dr, c[5]);
                    TxtTransportType.EditValue = Sm.DrStr(dr, c[6]);
                    TxtDOReference.EditValue = Sm.DrStr(dr, c[7]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                    TxtStatus.EditValue = Sm.DrStr(dr, c[9]);
                    Sm.SetLue(LueProcessInd, Sm.DrStr(dr, c[10]));

                }, true
            );
        }

        private void ShowStockOutboundDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.ItCode, B.ItCodeInternal, B.ItName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("A.Qty, B.InventoryUOMCode, ");
            SQL.AppendLine("A.Qty2, B.InventoryUOMCode2, ");
            SQL.AppendLine("A.Qty3, B.InventoryUOMCode3, D.OptDesc, C.Value1 As ExpiredDt, C.RemarkInbound, A.Remark, ");
            SQL.AppendLine("B.Length, B.LengthUomCode, B.Width, B.WidthUomCode, B.Height, B.HeightUomCode, B.Weight, B.WeightUomCode ");
            SQL.AppendLine("From TblStockOutboundDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("Select t1.Value1, t2.remark As RemarkInbound, t1.Value2, t1.Source ");
            SQL.AppendLine("From TblSourceInfo t1 ");
            SQL.AppendLine("Inner Join TblStockInboundDtl t2 On t1.Source = t2.Source ");
            SQL.AppendLine(")C On A.Source=C.Source ");
            SQL.AppendLine("Left Join TblOption D On C.Value2=D.OptCode And D.OptCat='StockStatus' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "ItCode", "ItName", "BatchNo", "Source",   
                    
                    //6-10
                    "Lot", "Bin", "Qty", "InventoryUomCode", "Qty2", 
                    
                    //11-15
                    "InventoryUomCode2", "Qty3", "InventoryUomCode3", "OptDesc", "ExpiredDt", 
                    
                    //16-20
                    "Remark", "Length", "LengthUomCode", "Width", "WidthUomCode", 
                    
                    //21-25
                    "Height", "HeightUomCode", "Weight", "WeightUomCode", "RemarkInbound"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 14);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 24);
                    Sm.SetGrdNumValueZero(ref Grd1, Row, new int[] { 10, 13, 16, 22, 24, 26, 28 });
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 25);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 11, 13, 14, 16, 17, 22, 24, 26, 28 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void ProcessImport()
        {
            Cursor.Current = Cursors.WaitCursor;

            var l = new List<StockSummary>();
            ClearGrd();
            try
            {
                ProcessImport1(ref l);
                if (l.Count > 0)
                {
                    if (IsImportStockOutbondInvalid(ref l))
                    {
                        ClearGrd(); l.Clear(); return;
                    }
                    ProcessImport2(ref l);
                    ProcessImport3(ref l);
                    ProcessImport4(ref l);
                    ProcessImport5(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                Cursor.Current = Cursors.Default;
            }
        }


        private bool IsImportStockOutbondInvalid(ref List<StockSummary> l)
        {
            return
                 IsItCodeEmpty(ref l) ||
                 IsLotEmpty(ref l) ||
                 IsBinEmpty(ref l);
        }

        private bool IsItCodeEmpty(ref List<StockSummary> l)
        {
            for (int i = 0; i < l.Count(); i++)
            {
                if (l[i].ItCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Item code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }
        private bool IsLotEmpty(ref List<StockSummary> l)
        {
            for (int i = 0; i < l.Count(); i++)
            {
                if (l[i].Lot.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Lot is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsBinEmpty(ref List<StockSummary> l)
        {
            for (int i = 0; i < l.Count(); i++)
            {
                if (l[i].Bin.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Bin is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }



        private void ProcessImport1(ref List<StockSummary> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var ListSeparator = Convert.ToChar(Sm.GetListSeparator());
            var FileName = openFileDialog1.FileName;
            string ItCodeTemp = string.Empty, BatchTemp = string.Empty, RemarkTemp = string.Empty, LotTemp = string.Empty, BinTemp = string.Empty;
            decimal QtyTemp = 0m;
            bool IsFirst = true;

            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(ListSeparator); // line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            ItCodeTemp = arr[0].Trim();
                            LotTemp = arr[1].Trim();
                            BinTemp = arr[2].Trim();
                            BatchTemp = arr[3].Trim();
                            if (arr[4].Trim().Length > 0) QtyTemp = decimal.Parse(arr[4].Trim());
                            else QtyTemp = 0m;
                            RemarkTemp = arr[5].Trim();
                            l.Add(new StockSummary()
                            {
                                ItCode = ItCodeTemp,
                                Qty = QtyTemp,
                                Qty2 = QtyTemp,
                                Qty3 = QtyTemp,
                                Lot = LotTemp,
                                Bin = BinTemp,
                                BatchNumber = BatchTemp,
                                Remark = RemarkTemp
                            });
                        }
                    }
                }
            }
        }

        private void ProcessImport2(ref List<StockSummary> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string
                Filter = string.Empty, Filter2 = string.Empty, Filter3 = string.Empty;

            // proses yg diisi batch nya
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));

            foreach (var x in l.Where(w => w.BatchNumber.Trim().Length > 0))
            {
                if (Filter.Length > 0) Filter += ",";
                Filter += string.Concat(Sm.GetLue(LueWhsCode), x.ItCode, x.Lot, x.Bin, x.BatchNumber);
            }

            if (Filter.Length > 0)
            {
                Filter2 = " And Find_In_Set(Concat(A.WhsCode, A.ItCode, A.Lot, A.Bin, A.BatchNo), @FilterData) ";
                Sm.CmParam<String>(ref cm, "@FilterData", Filter);
            }
            else
            {
                Filter2 = "And 0 = 0";
            }

            //hasil query di loop dengan list

            SQL.AppendLine("SELECT B.ItCode, B.ItName, B.Length, B.LengthUomCode, ");
            SQL.AppendLine("A.BatchNo, A.Source, A.Lot, A.Bin,  ");
            SQL.AppendLine("A.Qty-IfNull(F.Qty, 0.00) As Stock, B.InventoryUomCode,  ");
            SQL.AppendLine("A.Qty2-IfNull(F.Qty2, 0.00) As Stock2, B.InventoryUomCode2,  ");
            SQL.AppendLine("A.Qty3-IfNull(F.Qty3, 0.00) As Stock3, B.InventoryUomCode3,  ");
            SQL.AppendLine("D.OptDesc Status, C.Value1 As ExpiredDt, C.RemarkInbound,   ");
            SQL.AppendLine("B.Width, B.WidthUomCode, ");
            SQL.AppendLine("B.Height, B.HeightUomCode, ");
            SQL.AppendLine("B.Weight, B.WeightUomCode  ");
            SQL.AppendLine("From TblStockSummary A   ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode   ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("Select T1.Value1, T2.Remark As RemarkInbound, T1.Value2, T1.Source ");
            SQL.AppendLine("From TblSourceInfo T1 ");
            SQL.AppendLine("Inner Join TblStockInboundDtl T2 On T1.Source = T2.Source ");
            SQL.AppendLine(")C On A.Source=C.Source ");
            SQL.AppendLine("Left Join TblOption D On C.Value2=D.OptCode And D.OptCat='StockStatus'  ");
            SQL.AppendLine("INNER JOIN (   ");
            SQL.AppendLine("   SELECT A.WhsCode, A.ItCode, A.BatchNo, MAX(Concat(CreateDt, Source)) MaxCreateDtSource   ");
            SQL.AppendLine("   FROM tblstocksummary A  ");
            SQL.AppendLine("   WHERE A.qty > 0.00  ");
            SQL.AppendLine("   And WhsCode=@WhsCode ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("GROUP BY A.WhsCode, A.ItCode, A.Lot, A.Bin, A.BatchNo   ");
            SQL.AppendLine(") E ON E.WhsCode = A.WhsCode AND E.ItCode = A.ItCode   ");
            SQL.AppendLine("    AND E.MaxCreateDtSource = Concat(A.CreateDt, A.Source) And E.BatchNo = A.BatchNo   ");
            SQL.AppendLine("Left Join (  ");
            SQL.AppendLine("  Select B.Lot, B.Bin, B.Source, Sum(B.Qty) Qty, Sum(B.Qty2) Qty2, Sum(B.Qty3 ) Qty3  ");
            SQL.AppendLine("  From TblStockOutboundHdr A  ");
            SQL.AppendLine("  Inner Join TblStockOutboundDtl B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("  Where A.Status In ('O', 'A')  ");
            SQL.AppendLine("  And A.ProcessInd='D'  ");
            SQL.AppendLine("  And A.WhsCode=@WhsCode  ");
            SQL.AppendLine("  Group By B.Lot, B.Bin, B.Source  ");
            SQL.AppendLine(") F On A.Lot=F.Lot And A.Bin=F.Bin And A.Source=F.Source ");
            SQL.AppendLine("Where A.Qty>0.00   ");
            SQL.AppendLine("And A.WhsCode=@WhsCode ");

            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5 
                    "ItName",
                    "Source",
                    "Lot", 
                    "Bin", 
                    "Stock", 
                    
                    //6-10
                    "InventoryUomCode",
                    "Stock2",  
                    "InventoryUomCode2",
                    "Stock3", 
                    "InventoryUomCode3", 

                    //11-15
                    "Status", 
                    "ExpiredDt",  
                    "Width",
                    "WidthUomCode", 
                    "Height", 

                    //16-20
                    "HeightUomCode", 
                    "Weight", 
                    "WeightUomCode" ,
                    "BatchNo",
                    "RemarkInbound"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var x in l.Where(w => w.BatchNumber.Trim().Length > 0 &&
                            w.ItCode == Sm.DrStr(dr, c[0]) && // ItCode
                            w.Lot == Sm.DrStr(dr, c[3]) && //Lot
                            w.Bin == Sm.DrStr(dr, c[4]) && //Bin
                            w.BatchNumber == Sm.DrStr(dr, c[19])) // BatchNo
                         )
                        {
                            x.ItCode = Sm.DrStr(dr, c[0]);
                            x.ItName = Sm.DrStr(dr, c[1]);
                            x.Source = Sm.DrStr(dr, c[2]);
                            x.Lot = Sm.DrStr(dr, c[3]);
                            x.Bin = Sm.DrStr(dr, c[4]);
                            x.Stock = Sm.DrDec(dr, c[5]);
                            x.InventoryUomCode = Sm.DrStr(dr, c[6]);
                            x.Stock2 = Sm.DrDec(dr, c[7]);
                            x.InventoryUomCode2 = Sm.DrStr(dr, c[8]);
                            x.Stock3 = Sm.DrDec(dr, c[9]);
                            x.InventoryUomCode3 = Sm.DrStr(dr, c[10]);
                            x.Status = Sm.DrStr(dr, c[11]);
                            x.ExpiredDt = Sm.DrStr(dr, c[12]);
                            x.Width = Sm.DrDec(dr, c[13]);
                            x.WidthUomCode = Sm.DrStr(dr, c[14]);
                            x.Height = Sm.DrDec(dr, c[15]);
                            x.HeightUomCode = Sm.DrStr(dr, c[16]);
                            x.Weight = Sm.DrDec(dr, c[17]);
                            x.WeightUomCode = Sm.DrStr(dr, c[18]);
                            x.BatchNumber = Sm.DrStr(dr, c[19]);
                            x.RemarkInbound = Sm.DrStr(dr, c[20]);
                        }
                    }
                }
                dr.Close();
            }

        }

        private void ProcessImport3(ref List<StockSummary> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string
                 Filter = string.Empty, Filter2 = string.Empty, Filter3 = string.Empty, Filter4 = string.Empty;

            // proses yg ga diisi batch nya

            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            foreach (var x in l.Where(w => w.BatchNumber.Trim().Length == 0))
            {
                if (Filter.Length > 0) Filter += ",";
                Filter += string.Concat(Sm.GetLue(LueWhsCode), x.ItCode, x.Lot, x.Bin);
            }

            foreach (var x in l.Where(w => w.BatchNumber.Trim().Length > 0))
            {
                if (Filter3.Length > 0) Filter3 += ",";
                Filter3 += string.Concat(Sm.GetLue(LueWhsCode), x.ItCode, x.Lot, x.Bin, x.BatchNumber, x.Source);
            }

            if (Filter.Length > 0)
            {
                Filter2 = " And Find_In_Set(Concat(A.WhsCode, A.ItCode, A.Lot, A.Bin), @FilterData2) ";
                Sm.CmParam<String>(ref cm, "@FilterData2", Filter);
            }
            else
            {
                Filter2 = "And 0 = 0";
            }

            if (Filter3.Length > 0)
            {
                Filter4 = " And NOT FIND_IN_SET(CONCAT(A.WhsCode, A.ItCode, A.Lot, A.Bin, A.BatchNo, A.Source), @FilterData3) ";
                Sm.CmParam<String>(ref cm, "@FilterData3", Filter3);
            }
            else
            {
                Filter4 = "And 0 = 0";
            }

            SQL.AppendLine("SELECT B.ItCode, B.ItName, B.Length, B.LengthUomCode, ");
            SQL.AppendLine("A.BatchNo, A.Source, A.Lot, A.Bin,  ");
            SQL.AppendLine("A.Qty-IfNull(F.Qty, 0.00) As Stock, B.InventoryUomCode,  ");
            SQL.AppendLine("A.Qty2-IfNull(F.Qty2, 0.00) As Stock2, B.InventoryUomCode2,  ");
            SQL.AppendLine("A.Qty3-IfNull(F.Qty3, 0.00) As Stock3, B.InventoryUomCode3,  ");
            SQL.AppendLine("D.OptDesc Status, C.Value1 As ExpiredDt, C.RemarkInbound,   ");
            SQL.AppendLine("B.Width, B.WidthUomCode, ");
            SQL.AppendLine("B.Height, B.HeightUomCode, ");
            SQL.AppendLine("B.Weight, B.WeightUomCode  ");
            SQL.AppendLine("From TblStockSummary A   ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode   ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("Select T1.Value1, T2.Remark As RemarkInbound, T1.Value2, T1.Source ");
            SQL.AppendLine("From TblSourceInfo T1 ");
            SQL.AppendLine("Inner Join TblStockInboundDtl T2 On T1.Source = T2.Source ");
            SQL.AppendLine(")C On A.Source=C.Source ");
            SQL.AppendLine("Left Join TblOption D On C.Value2=D.OptCode And D.OptCat='StockStatus'  ");
            SQL.AppendLine("INNER JOIN (   ");
            SQL.AppendLine("   SELECT A.WhsCode, A.ItCode, MAX(Concat(A.CreateDt, A.Source)) MaxCreateDtSource   ");
            SQL.AppendLine("   FROM tblstocksummary A  ");
            SQL.AppendLine("   WHERE A.qty > 0.00  ");
            SQL.AppendLine("   And WhsCode=@WhsCode ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine(Filter4);
            SQL.AppendLine("GROUP BY A.WhsCode, A.ItCode, A.Lot, A.Bin   ");
            SQL.AppendLine(") E ON E.WhsCode = A.WhsCode AND E.ItCode = A.ItCode   ");
            SQL.AppendLine("    AND E.MaxCreateDtSource = Concat(A.CreateDt, A.Source)   ");
            SQL.AppendLine("Left Join (  ");
            SQL.AppendLine("  Select B.Lot, B.Bin, B.Source, Sum(B.Qty) Qty, Sum(B.Qty2) Qty2, Sum(B.Qty3 ) Qty3  ");
            SQL.AppendLine("  From TblStockOutboundHdr A  ");
            SQL.AppendLine("  Inner Join TblStockOutboundDtl B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("  Where A.Status In ('O', 'A')  ");
            SQL.AppendLine("  And A.ProcessInd='D'  ");
            SQL.AppendLine("  And A.WhsCode=@WhsCode  ");
            SQL.AppendLine("  Group By B.Lot, B.Bin, B.Source  ");
            SQL.AppendLine(") F On A.Lot=F.Lot And A.Bin=F.Bin And A.Source=F.Source ");
            SQL.AppendLine("Where A.Qty>0.00   ");
            SQL.AppendLine("And A.WhsCode=@WhsCode ");


            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5 
                    "ItName",
                    "Source",
                    "Lot", 
                    "Bin", 
                    "Stock", 
                    
                    //6-10
                    "InventoryUomCode",
                    "Stock2",  
                    "InventoryUomCode2",
                    "Stock3", 
                    "InventoryUomCode3", 

                    //11-15
                    "Status", 
                    "ExpiredDt",  
                    "Width",
                    "WidthUomCode", 
                    "Height", 

                    //16-20
                    "HeightUomCode", 
                    "Weight", 
                    "WeightUomCode" ,
                    "BatchNo",
                    "RemarkInbound"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var x in l.Where(w => w.BatchNumber.Trim().Length == 0 &&
                            w.ItCode == Sm.DrStr(dr, c[0]) &&
                            w.Lot == Sm.DrStr(dr, c[3]) &&
                            w.Bin == Sm.DrStr(dr, c[4]) )
                         )
                        {
                            x.ItCode = Sm.DrStr(dr, c[0]);
                            x.ItName = Sm.DrStr(dr, c[1]);
                            x.Source = Sm.DrStr(dr, c[2]);
                            x.Lot = Sm.DrStr(dr, c[3]);
                            x.Bin = Sm.DrStr(dr, c[4]);
                            x.Stock = Sm.DrDec(dr, c[5]);
                            x.InventoryUomCode = Sm.DrStr(dr, c[6]);
                            x.Stock2 = Sm.DrDec(dr, c[7]);
                            x.InventoryUomCode2 = Sm.DrStr(dr, c[8]);
                            x.Stock3 = Sm.DrDec(dr, c[9]);
                            x.InventoryUomCode3 = Sm.DrStr(dr, c[10]);
                            x.Status = Sm.DrStr(dr, c[11]);
                            x.ExpiredDt = Sm.DrStr(dr, c[12]);
                            x.Width = Sm.DrDec(dr, c[13]);
                            x.WidthUomCode = Sm.DrStr(dr, c[14]);
                            x.Height = Sm.DrDec(dr, c[15]);
                            x.HeightUomCode = Sm.DrStr(dr, c[16]);
                            x.Weight = Sm.DrDec(dr, c[17]);
                            x.WeightUomCode = Sm.DrStr(dr, c[18]);
                            x.BatchNumber = Sm.DrStr(dr, c[19]);
                            x.RemarkInbound = Sm.DrStr(dr, c[20]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessImport4(ref List<StockSummary> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string
                Filter = string.Empty, ItCode = string.Empty, ItName = string.Empty, InventoryUomCode = string.Empty,
                LengthUomCode = string.Empty,
                WidthUomCode = string.Empty,
                HeightUomCode = string.Empty,
                WeightUomCode = string.Empty;
            decimal Length = 0m, Width = 0m, Height = 0m, Weight = 0m;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].ItCode.Trim().Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(ItCode=@ItCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@ItCode0" + i.ToString(), l[i].ItCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select ItCode, ItName, InventoryUomCode, ");
            SQL.AppendLine("Length, LengthUomCode, Width, WidthUomCode, Height, HeightUomCode, Weight, WeightUomCode ");
            SQL.AppendLine("From TblItem ");
            SQL.AppendLine("Where InventoryUomCode=InventoryUomCode2 ");
            SQL.AppendLine("And InventoryUomCode2=InventoryUomCode3 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode", 
                    
                    //1-5
                    "ItName", 
                    "InventoryUomCode",
                    "Length", 
                    "LengthUomCode", 
                    "Width", 
                    
                    //6-10
                    "WidthUomCode", 
                    "Height", 
                    "HeightUomCode", 
                    "Weight", 
                    "WeightUomCode" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, c[0]);
                        ItName = Sm.DrStr(dr, c[1]);
                        InventoryUomCode = Sm.DrStr(dr, c[2]);
                        Length = Sm.DrDec(dr, c[3]);
                        LengthUomCode = Sm.DrStr(dr, c[4]);
                        Width = Sm.DrDec(dr, c[5]);
                        WidthUomCode = Sm.DrStr(dr, c[6]);
                        Height = Sm.DrDec(dr, c[7]);
                        HeightUomCode = Sm.DrStr(dr, c[8]);
                        Weight = Sm.DrDec(dr, c[9]);
                        WeightUomCode = Sm.DrStr(dr, c[10]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].ItCode, ItCode))
                            {
                                l[i].ItName = ItName;
                                l[i].InventoryUomCode = InventoryUomCode;
                                l[i].Length = Length;
                                l[i].LengthUomCode = LengthUomCode;
                                l[i].Width = Width;
                                l[i].WidthUomCode = WidthUomCode;
                                l[i].Height = Height;
                                l[i].HeightUomCode = HeightUomCode;
                                l[i].Weight = Weight;
                                l[i].WeightUomCode = WeightUomCode;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessImport5(ref List<StockSummary> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[4].Value = l[i].ItCode;
                r.Cells[5].Value = l[i].ItName;
                r.Cells[6].Value = l[i].BatchNumber;
                r.Cells[7].Value = l[i].Source;
                r.Cells[8].Value = l[i].Lot;
                r.Cells[9].Value = l[i].Bin;
                r.Cells[10].Value = l[i].Stock;
                r.Cells[11].Value = l[i].Qty;
                r.Cells[12].Value = l[i].InventoryUomCode;
                r.Cells[13].Value = l[i].Stock2;
                r.Cells[14].Value = l[i].Qty2;
                r.Cells[15].Value = l[i].InventoryUomCode2;
                r.Cells[16].Value = l[i].Stock3;
                r.Cells[14].Value = l[i].Qty3;
                r.Cells[18].Value = l[i].InventoryUomCode3;
                r.Cells[19].Value = l[i].Status;
                r.Cells[20].Value = l[i].ExpiredDt;
                if (l[i].ExpiredDt == null || l[i].ExpiredDt.Length == 0)
                    r.Cells[20].Value = null;
                else
                    r.Cells[20].Value = Sm.ConvertDate(l[i].ExpiredDt);
                r.Cells[21].Value = l[i].Remark;
                r.Cells[22].Value = l[i].Length;
                r.Cells[23].Value = l[i].LengthUomCode;
                r.Cells[24].Value = l[i].Width;
                r.Cells[25].Value = l[i].WidthUomCode;
                r.Cells[26].Value = l[i].Height;
                r.Cells[27].Value = l[i].HeightUomCode;
                r.Cells[28].Value = l[i].Weight;
                r.Cells[29].Value = l[i].WeightUomCode;
                r.Cells[30].Value = l[i].RemarkInbound;

            }
            r = Grd1.Rows.Add();
            Grd1.EndUpdate();
        }

        private void SetLueProcess(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select 'D' As Col1, 'Draft' As Col2 Union All " +
                "Select 'F' As Col1, 'Final' As Col2 ; ",
                0, 35, false, true, "Code", "Process", "Col2", "Col1");
        }


        private void ParPrint()
        {
            var l = new List<StockOutboundHdr>();
            var ldtl = new List<StockOutboundDtl>();
            string[] TableName = { "StockOutboundHdr", "StockOutboundDtl" };
            List<IList> myLists = new List<IList>();
            int Nomor = 1;

            #region Header

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "CompanyLogo",

                    //1-3
                    "CompanyName",
                    "CompanyAddress",
                    "CompanyAddressCity"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new StockOutboundHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CtName = LueCtCode.Text,
                            DocNo = TxtDocNo.Text,
                            DocDt = DteDocDt.Text,
                            DOReference = TxtDOReference.Text,
                            LocalDocNo = string.Empty,
                            RecvDt = string.Empty,
                            DeliveryDt = string.Empty,
                            DeliveryFrom = string.Empty,
                            DeliveryTo = string.Empty,
                            VehicleRegNo = TxtVehicleRegNo.Text,
                            Remark = MeeRemark.Text
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (!Sm.GetGrdBool(Grd1, i, 1)) // tidak di cancel
                {
                    ldtl.Add(new StockOutboundDtl()
                    {
                        No = Nomor,
                        ItCode = Sm.GetGrdStr(Grd1, i, 4),
                        ItName = Sm.GetGrdStr(Grd1, i, 5),
                        BatchNo = Sm.GetGrdStr(Grd1, i, 6),
                        Lot = Sm.GetGrdStr(Grd1, i, 8),
                        Bin = Sm.GetGrdStr(Grd1, i, 9),
                        ExpDt = Sm.GetGrdStr(Grd1, i, 20).Length == 0 ? string.Empty : Convert.ToDateTime(Sm.GetGrdStr(Grd1, i, 20)).ToString("dd/MMM/yyyy"),
                        Qty = Sm.GetGrdDec(Grd1, i, 11),
                        Uom = Sm.GetGrdStr(Grd1, i, 12),
                        Length = Sm.GetGrdDec(Grd1, i, 22),
                        Width = Sm.GetGrdDec(Grd1, i, 24),
                        Height = Sm.GetGrdDec(Grd1, i, 26),
                        Weight = Sm.GetGrdDec(Grd1, i, 28),
                        Remark = Sm.GetGrdStr(Grd1, i, 21)
                    });
                    Nomor += 1;
                }
            }
            myLists.Add(ldtl);

            #endregion

            if (Sm.Left(TxtStatus.Text.ToUpper(), 1) == "A" && Sm.Left(Sm.GetLue(LueProcessInd).ToUpper(), 1) == "F")
                Sm.PrintReport("StockDeliveryOrderOutbound", myLists, TableName, false);
            else
                Sm.PrintReport("StockPickingList", myLists, TableName, false);
        }


        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsStockOutboundApprovalBasedOnWhs = Sm.GetParameterBoo("IsStockOutboundApprovalBasedOnWhs");
            mStockOutboundFormat = Sm.GetParameter("StockOutboundFormat");
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T1.Source, T1.Lot, T1.Bin, ");
            SQL.AppendLine("    T1.Qty-IfNull(T2.Qty, 0.00) Qty, ");
            SQL.AppendLine("    T1.Qty2-IfNull(T2.Qty2, 0.00) Qty2, ");
            SQL.AppendLine("    T1.Qty3-IfNull(T2.Qty2, 0.00) Qty3 ");
            SQL.AppendLine("    From TblStockSummary T1 ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select B.Lot, B.Bin, B.Source, Sum(B.Qty) Qty, Sum(B.Qty2) Qty2, Sum(B.Qty3) Qty3 ");
            SQL.AppendLine("        From TblStockOutboundHdr A ");
            SQL.AppendLine("        Inner Join TblStockOutboundDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Where A.Status In ('O', 'A') ");
            SQL.AppendLine("        And A.ProcessInd='D' ");
            SQL.AppendLine("        And A.WhsCode=@WhsCode ");
            if (TxtDocNo.Text.Length > 0) SQL.AppendLine("        And A.DocNo<>@DocNo ");
            SQL.AppendLine("        Group By B.Lot, B.Bin, B.Source ");
            SQL.AppendLine("    ) T2 On T1.Lot=T2.Lot And T1.Bin=T2.Bin And T1.Source=T2.Source ");
            SQL.AppendLine("    Where T1.WhsCode=@WhsCode ");
            SQL.AppendLine(") T Where 1=1 ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 7, 8, 9);
                            No += 1;
                        }
                    }
                }
                cm.CommandText = SQL.ToString() + " And (" + Filter + ")";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 7), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 8), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 9), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 10, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 13, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 16, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnImport_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueWhsCode, "Warehouse")) return;
            ProcessImport();
        }

        private void LueProcessInd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProcessInd, new Sm.RefreshLue1(SetLueProcess));
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
                ClearGrd();
            }
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            if (mStockOutboundFormat == "2")
                ClearGrd();
        }

        private void TxtVehicleRegNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVehicleRegNo);
        }

        private void TxtTransportType_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTransportType);
        }

        #endregion

        #endregion

        #region Class

        private class StockSummary
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string BatchNumber { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Stock { get; set; }
            public string InventoryUomCode { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Stock2 { get; set; }
            public string InventoryUomCode2 { get; set; }
            public decimal Qty3 { get; set; }
            public decimal Stock3 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string Status { get; set; }
            public string ExpiredDt { get; set; }
            public decimal Length { get; set; }
            public string LengthUomCode { get; set; }
            public decimal Width { get; set; }
            public string WidthUomCode { get; set; }
            public decimal Height { get; set; }
            public string HeightUomCode { get; set; }
            public decimal Weight { get; set; }
            public string WeightUomCode { get; set; }
            public string Remark { get; set; }
            public string RemarkInbound { get; set; }

        }

        private class StockOutboundHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string DOReference { get; set; }
            public string LocalDocNo { get; set; }
            public string RecvDt { get; set; }
            public string DeliveryDt { get; set; }
            public string DeliveryFrom { get; set; }
            public string DeliveryTo { get; set; }
            public string VehicleRegNo { get; set; }
            public string Remark { get; set; }
        }

        public class StockOutboundDtl
        {
            public int No { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string BatchNo { get; set; }
            public string ExpDt { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Length { get; set; }
            public decimal Width { get; set; }
            public decimal Height { get; set; }
            public decimal Weight { get; set; }
            public string Remark { get; set; }
        }

        #endregion

      

    }
}
