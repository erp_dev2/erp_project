﻿#region Update
/*
    01/11/2017 [TKG] Aplikasi baru
    06/06/2018 [TKG] running machine menjadi 2 decimal point
    06/06/2018 [TKG] validasi apabila ada production date dan work center yg pernah diproses sebelumnya.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSpinningProduction1 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmSpinningProduction1Find FrmFind;

        #endregion

        #region Constructor

        public FrmSpinningProduction1(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }


        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Code",

                        //1-2
                        "Efficiency Variable",
                        "Value"
                    },
                     new int[] 
                    {
                        0,
                        200, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 3);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, DteProdDt, LueWCCode, 
                        TxtRunningMachine, TxtQty, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 2 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, DteProdDt, LueWCCode, TxtRunningMachine, TxtQty, 
                        MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtDocNo, DteDocDt, MeeCancelReason, DteProdDt, LueWCCode, 
                MeeRemark 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtRunningMachine }, 255);
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtQty }, 2);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSpinningProduction1Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetDteCurrentDate(DteProdDt);
                SetLueWCCode(ref LueWCCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 2) Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 2 }, e);
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSpinningProduction1Hdr(DocNo);
                ShowSpinningProduction1Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowSpinningProduction1Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                 ref cm,
                 "Select DocNo, DocDt, CancelReason, CancelInd, ProdDt, " +
                 "WCCode, RunningMachine, Qty, Remark " +
                 "From TblSpinningProduction1Hdr " +
                 "Where DocNo=@DocNo;",
                 new string[] 
                   {
                        //0
                        "DocNo", 
                      
                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "ProdDt", "WCCode", 
                        
                        //6-8
                        "RunningMachine", "Qty", "Remark"
                   },
                 (MySqlDataReader dr, int[] c) =>
                 {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                    Sm.SetDte(DteProdDt, Sm.DrStr(dr, c[4]));
                    SetLueWCCode(ref LueWCCode, Sm.DrStr(dr, c[5]));
                    TxtRunningMachine.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 2);
                    TxtQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 2);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                 }, true
             );
        }

        private void ShowSpinningProduction1Dtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.ProdEfficiencyVar, B.OptDesc, A.Value " +
                    "From TblSpinningProduction1Dtl A " +
                    "Left Join TblOption B On B.OptCat='ProdEfficiencyVar' And A.ProdEfficiencyVar=B.OptCode " +
                    "Where A.DocNo=@DocNo Order By A.DNo;",

                    new string[] 
                       { 
                           //0
                           "ProdEfficiencyVar",

                           //1-2
                           "OptDesc", "Value"
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SpinningProduction1", "TblSpinningProduction1Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSpinningProduction1Hdr(DocNo));

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) cml.Add(SaveSpinningProduction1Dtl(DocNo, r));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }


        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteProdDt, "Production date") ||
                Sm.IsLueEmpty(LueWCCode, "Work center") ||
                Sm.IsTxtEmpty(TxtQty, "Quantity", true) ||
                IsDataAlreadyExisted() ||
                IsGrdValueNotValid();
        }

        private bool IsDataAlreadyExisted()
        {
            if (Sm.IsDataExist(
                "Select 1 From TblSpinningProduction1Hdr " +
                "Where CancelInd='N' And ProdDt=@Param1 And WCCode=@Param2 Limit 1;", 
                Sm.GetDte(DteProdDt), Sm.GetLue(LueWCCode), string.Empty))                   
            {
                Sm.StdMsg(mMsgType.Warning, "Data with the same production date and work center already existed.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 2, true, "Value should not be 0."))
                        return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveSpinningProduction1Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSpinningProduction1Hdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, ProdDt, WCCode, RunningMachine, Qty, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', @ProdDt, @WCCode, @RunningMachine, @Qty, @Remark, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@ProdDt", Sm.GetDte(DteProdDt));
            Sm.CmParam<String>(ref cm, "@WCCode", Sm.GetLue(LueWCCode));
            Sm.CmParam<Decimal>(ref cm, "@RunningMachine", decimal.Parse(TxtRunningMachine.Text));
            Sm.CmParam<Decimal>(ref cm, "@Qty", decimal.Parse(TxtQty.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSpinningProduction1Dtl(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSpinningProduction1Dtl ");
            SQL.AppendLine("(DocNo, DNo, ProdEfficiencyVar, Value, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @ProdEfficiencyVar, @Value, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (r + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ProdEfficiencyVar", Sm.GetGrdStr(Grd1, r, 0));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd1, r, 2));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditSpinningProduction1Hdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataAlreadyCancelled();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist("Select DocNo From TblSpinningProduction1Hdr Where CancelInd='Y' And DocNo=@Param;", TxtDocNo.Text);
        }

        private MySqlCommand EditSpinningProduction1Hdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSpinningProduction1Hdr Set CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Additional Method

        private void ShowGrdData(string WCCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@WCCode", WCCode);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                "Select A.ProdEfficiencyVar, B.OptDesc, A.DefaultValue " +
                "From TblWCSpinningEffVar A " +
                "Inner Join TblOption B On B.OptCat='ProdEfficiencyVar' and A.ProdEfficiencyVar=B.OptCode " +
                "Where A.UseInd='Y' And A.WCCode=@WCCode Order By B.OptDesc; ",
                new string[] { "ProdEfficiencyVar", "OptDesc", "DefaultValue" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void SetLueWCCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WCCode As Col1, WCName As Col2 ");
            SQL.AppendLine("From TblWCSpinning ");
            if (Code.Length > 0)
                SQL.AppendLine("Where WCCode=@Code;");
            else
            {
                SQL.AppendLine("Where ActInd='Y' ");
                SQL.AppendLine("And ProdDataPerItemInd='N' ");
                SQL.AppendLine("Order By WCName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueWCCode_EditValueChanged(object sender, EventArgs e)
        {
        }

        private void LueWCCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWCCode, new Sm.RefreshLue2(SetLueWCCode), string.Empty);
                ClearGrd();
                var WCCode = Sm.GetLue(LueWCCode);
                if (WCCode.Length > 0) ShowGrdData(WCCode);
            }
        }

        private void TxtRunningMachine_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtRunningMachine, 2);
        }

        private void TxtQty_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtQty, 2);
        }

        #endregion

        private void BtnQty_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueWCCode, "Work center") ||
                Sm.IsDteEmpty(DteProdDt, "Production date")) return;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Sum(B.Qty) ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode And C.ItGrpCode='002' ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocDt=@DocDt ");
            SQL.AppendLine("And A.WorkCenterDocNo=@WorkCenterDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteProdDt));
            Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", Sm.GetLue(LueWCCode));

            try
            {
                var Qty = Sm.GetValue(cm);
                if (Qty.Length>0)
                    TxtQty.EditValue = Sm.FormatNum(decimal.Parse(Qty), 2);
                else
                    TxtQty.EditValue = Sm.FormatNum(0m, 2);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #endregion
    }
}
