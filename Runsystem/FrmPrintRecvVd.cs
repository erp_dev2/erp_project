﻿#region 
/*
    23/06/2020: [VIN/IMS] NEW APPS
 */ 
#endregion 

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmPrintRecvVd : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty, 
            DocNo= string.Empty;

        #endregion

        #region Constructor

        public FrmPrintRecvVd(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnPrint.Visible = false;

                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueVdCode(ref LueVdCode);

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, ");
            SQL.AppendLine("case ");
            SQL.AppendLine("when B.`Status`='A' then 'Approved' ");
            SQL.AppendLine("when B.`Status`='O' then 'Outstanding' ");
            SQL.AppendLine("ELSE 'Cancelled' ");
            SQL.AppendLine("END AS STATUS, ");
            SQL.AppendLine("D.WhsName, C.VdName, group_concat(distinct B.PODocNo SEPARATOR ', ') As PODocNo ");
            SQL.AppendLine("FROM tblrecvvdhdr A ");
            SQL.AppendLine("INNER JOIN tblrecvvddtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("INNER JOIN tblvendor C ON A.VdCode=C.VdCode ");
            SQL.AppendLine("INNER JOIN tblwarehouse D ON A.WhsCode = D.WhsCode ");
            SQL.AppendLine("WHERE A.DocNo NOT LIKE '%RV/%' And A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "", 
                        "Document#",
                        "Status",
                        " ",
                        "Warehouse",
                        
                        //6-7
                        "Vendor",
                        "PO#"

                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 150, 100, 20, 150, 
                        
                        //6-7
                        150, 250
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 2, 3, 5, 6, 7 });
            Sm.GrdColReadOnly(false, false, Grd1, new int[] { 1, 4 });


        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {

                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                string mDocNo = string.Empty;
                
                Filter += " And B.CancelInd='N' ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtPODocNo.Text, "B.PODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " GROUP BY A.DocNo Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "Status", "WhsName", "VdName", "PODocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            

                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion 

        #region Button Method

        private void ParPrint(string DocNo)
        {
            string mDocNo = string.Empty;

            var l = new List<RecvVdHdr>();
            var lDtl = new List<RecvVdDtl>();
            string[] TableName = { "RecvVdHdr", "RecvVdDtl" };
            var myLists = new List<IList>();
            
            if (Grd1.Rows.Count > 1)
            {

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {

                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (mDocNo.Length > 0) mDocNo += ",";
                        mDocNo += Sm.GetGrdStr(Grd1, Row, 2);
                    } 
                    
                } 
            }

            DocNo = mDocNo;


            #region Header IMS
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, date_format(A.DocDt, '%d %M %Y') DocDt, group_concat(distinct B.PODocNo SEPARATOR '\r \n') PODocNo, ");
            SQL.AppendLine("group_concat(D.ItName SEPARATOR '\r \n') ItName, date_format(E.DocDt, '%d %M %Y') StartDt,  ");
            SQL.AppendLine("date_format(C.DocDt, '%d %M %Y') EndDt, F.VdName, (Select A.EmpName FROM TBLEmployee A INNER JOIN  tblparameter D ON A.EmpCode=D.ParValue and D.ParCode='EmployeeSignName1ForPrintoutRecvVd') SignName1, ");
            SQL.AppendLine("(Select A.EmpName FROM TBLEmployee A INNER JOIN  tblparameter D ON A.EmpCode=D.ParValue and D.ParCode='EmployeeSignName2ForPrintoutRecvVd') SignName2, ");
            SQL.AppendLine("(Select A.EmpName FROM TBLEmployee A INNER JOIN  tblparameter D ON A.EmpCode=D.ParValue and D.ParCode='EmployeeSignName3ForPrintoutRecvVd') SignName3  ");
            SQL.AppendLine("FROM tblrecvvdhdr A  ");
            SQL.AppendLine("INNER JOIN tblrecvvddtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("INNER JOIN tblrecvexpeditionhdr C ON A.RecvExpeditionDocNo=C.DocNo ");
            SQL.AppendLine("INNER JOIN tblitem D ON B.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join tblpohdr E On B.PODocNo=E.DocNo  ");
            SQL.AppendLine("inner JOIN tblvendor F ON A.VdCode=F.VdCode ");
            SQL.AppendLine("WHERE FIND_IN_SET(A.DocNo, @DocNo) ");
            SQL.AppendLine("GROUP BY A.DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo );
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-5
                         "DocDt",
                         "PODocNo",
                         "ItName",
                         "StartDt",
                         "EndDt" ,

                         //6-9
                         "VdName",
                         "SignName1",
                         "SignName2",
                         "SignName3"
                         
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RecvVdHdr()
                        {

                            DocNo = Sm.DrStr(dr, c[0]),

                            DocDt = Sm.DrStr(dr, c[1]),
                            PODocNo = Sm.DrStr(dr, c[2]),
                            ItName = Sm.DrStr(dr, c[3]),
                            StartDt = Sm.DrStr(dr, c[4]),
                            EndDt = Sm.DrStr(dr, c[5]),

                            VdName = Sm.DrStr(dr, c[6]),
                            SignName1 = Sm.DrStr(dr, c[7]),
                            SignName2 = Sm.DrStr(dr, c[8]),
                            SignName3 = Sm.DrStr(dr, c[9]),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail IMS
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("SELECT Distinct A.DocNo,  ");
            SQLDtl.AppendLine("Group_Concat( IfNull(E.ItName, '')SEPARATOR ' \r\n') ItName ,   ");
            SQLDtl.AppendLine("Group_Concat( IfNull(E.Specification, '')SEPARATOR ' \r\n') Specification,   ");
            SQLDtl.AppendLine("Group_Concat( IfNull(Convert(FORMAT(A.Qty, 2) Using UTF8), '')SEPARATOR ' \r\n') Qty,  ");
            SQLDtl.AppendLine("Group_Concat( IfNull(J.LocalDocNo, '')SEPARATOR ' \r\n') LocalDocNo,    ");
            SQLDtl.AppendLine("Group_Concat( DISTINCT IfNull(date_format(J.DocDt, '%d %M %Y'), '')SEPARATOR ' \r\n') Date   ");
            SQLDtl.AppendLine("From TblRecvVdDtl A  ");
            SQLDtl.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo  ");
            SQLDtl.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo  ");
            SQLDtl.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo  ");
            SQLDtl.AppendLine("Inner Join tblitem E On D.ItCode=E.ItCode  ");
            SQLDtl.AppendLine("Inner Join TblPOHdr F On A.PODocNo=F.DocNo  ");
            SQLDtl.AppendLine("Left Join TblItemSubcategory G On E.ItScCode = G.ItScCode  ");
            SQLDtl.AppendLine("Left Join TblRecvExpeditionDtl I  ");
            SQLDtl.AppendLine("On A.RecvExpeditionDocNo=I.DocNo  ");
            SQLDtl.AppendLine("And A.RecvExpeditionDNo=I.DNo  ");
            SQLDtl.AppendLine("LEFT JOIN tblrecvexpeditionhdr J ON I.DocNo=J.DocNo ");
            SQLDtl.AppendLine("Where FIND_IN_SET(A.DocNo, @DocNo) GROUP BY A.DocNo Order By A.DNo ");

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ItName",

                         //1-5
                         "Specification",
                         "Qty",
                         "LocalDocNo",
                         "Date",
                         "DocNo"
                        });
                int nomor = 0, Row = -1;
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        Row = Row + 1;
                        lDtl.Add(new RecvVdDtl()
                        {
                            Nomor = nomor,
                            ItName = Sm.DrStr(drDtl, cDtl[0]),
                            Specification = Sm.DrStr(drDtl, cDtl[1]),
                            Qty = Sm.DrStr(drDtl, cDtl[2]),
                            LocalDocNo = Sm.DrStr(drDtl, cDtl[3]),
                            Date = Sm.DrStr(drDtl, cDtl[4]),
                            DocNo = Sm.DrStr(drDtl, cDtl[5])


                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(lDtl);
            #endregion

            

            Sm.PrintReport("RecvVdServices", myLists, TableName, false);

        }


        #endregion

        #region Grid Method

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtPODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO#");
        }

        #endregion

        #endregion 

        #region Reporting Class

        private class RecvVdHdr
        {
            public string VdName { get; set; }
            public string DocNo { get; set; }
            public string PODocNo { get; set; }
            public string DocDt { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string Services { get; set; }
            public string ItName { get; set; }
            public string SignName1 { get; set; }
            public string SignName2 { get; set; }
            public string SignName3 { get; set; }

        }

        private class RecvVdDtl
        {

            public string ItName { get; set; }
            public string Specification { get; set; }
            public string Qty { get; set; }
            public string LocalDocNo { get; set; }
            public string Date { get; set; }
            public int Nomor { get; set; }
            public string DocNo { get; set; }

        }

        #endregion

        private void BtnPrint2_Click(object sender, EventArgs e)
        {
            ParPrint(DocNo);

        }

    }
}
