﻿#region Update
/* 
    01/02/2021 [VIN/PHT] new apps
    08/06/2021 [TKG/PHT] budget request yearly# yg sudah digunakan tidak bisa digunakan kembali
    20/06/2021 [TKG/PHT]
        profit center dan cost center divalidasi berdasarkan otorisasi group terhadap cost center,
        cost center yg dimunculkan adalah cost center yang tidak menjadi parent di cost center yg lain.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetMaintenanceYearlyDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmBudgetMaintenanceYearly mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmBudgetMaintenanceYearlyDlg(FrmBudgetMaintenanceYearly FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                SetGrd();
                //SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "",
                        "Date",
                        "Year",
                        "Month",

                        //6-10
                        "Cost Center",
                        "Cost Center",
                        "Amount",
                        "Remark",
                        "Local Code",

                        //11
                        "EntCode"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 20, 80, 60, 60, 
                        
                        //6-10
                        0, 180, 150, 250, 100,

                        //11
                        0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 11 }, false);
            if (mFrmParent.mIsBudget2YearlyFormat)
                Sm.GrdColInvisible(Grd1, new int[] { 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.Yr, A.CCCode, B.CCName, A.Amt, A.Remark, ");
            SQL.AppendLine("C.LocalCode, C.BCName ");
            SQL.AppendLine("From TblBudgetRequestYearlyHdr A ");
            SQL.AppendLine("Inner Join TblCostCenter B ON A.CCCode=B.CCCode ");
            SQL.AppendLine("    And B.NotParentInd='Y' ");
            SQL.AppendLine("    And B.ActInd='Y' ");
            SQL.AppendLine("    And B.ProfitCenterCode Is Not Null ");;
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    SELECT T1.DocNo, GROUP_CONCAT(DISTINCT IFNULL(T3.LocalCode, '')) LocalCode, "); 
            SQL.AppendLine("    GROUP_CONCAT(DISTINCT IFNULL(T3.BCName, '')) BCName  ");
            SQL.AppendLine("    FROM tblbudgetrequestyearlyhdr T1  ");
            SQL.AppendLine("    INNER JOIN tblbudgetrequestyearlydtl T2 ON T1.DocNo = T2.DocNo  ");
            SQL.AppendLine("       AND (T1.DocDt BETWEEN @DocDt1 AND @DocDt2)  ");
            SQL.AppendLine("    AND T1.Status = 'A'  ");
            SQL.AppendLine("    AND T1.CancelInd = 'N'  ");
            SQL.AppendLine("    INNER JOIN TblBudgetCategory T3 ON T2.BCCode = T3.BCCode  ");
            SQL.AppendLine("       AND T1.CCCode = T3.CCCode  ");
            if (TxtLocalCode.Text.Length > 0)
                SQL.AppendLine("    AND T3.LocalCode LIKE @LocalCode ");
            SQL.AppendLine("   GROUP BY T1.DocNo  ");
            SQL.AppendLine(") C ON A.DocNo = C.DocNo  ");
            SQL.AppendLine("Inner Join TblGroupProfitCenter D On B.ProfitCenterCode=D.ProfitCenterCode ");
            SQL.AppendLine("Inner Join TblUser E On D.GrpCode=E.GrpCode And E.UserCode=@UserCode ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2)  ");
            SQL.AppendLine("And A.CancelInd='N'  ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("And A.DocNo Not In (");
            SQL.AppendLine("    Select Distinct T1.DocNo ");
            SQL.AppendLine("    From TblBudgetRequestYearlyHdr T1, TblBudgetMaintenanceYearlyHdr T2 ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status='A' ");
            SQL.AppendLine("    And T1.DocNo=T2.BudgetRequestDocNo ");
            SQL.AppendLine("    And T2.Status In ('O', 'A') ");
            SQL.AppendLine("    ) ");
            
            mSQL = SQL.ToString();

            return mSQL;
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<string>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.CmParam<string>(ref cm, "@LocalCode", string.Concat("%", TxtLocalCode.Text , "%"));
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SetSQL() + Filter + " Order By A.CreateDt Desc;",
                    new string[] 
                    { 
                         //0
                         "DocNo", 
                         
                         //1-5
                         "DocDt", 
                         "Yr", 
                         "CCCode",
                         "CCName", 
                         "Amt",

                         //6-9
                         "Remark",
                         "LocalCode",
                     },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10,7);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtBudgetRequestDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtYr.EditValue = Sm.GetGrdStr(Grd1, Row, 4);

                mFrmParent.mCCCode = Sm.GetGrdStr(Grd1, Row, 6);
                mFrmParent.TxtCCCode.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                mFrmParent.TxtBudgetRequestAmt.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                
                mFrmParent.ShowBudgetRequestInfo();
                
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmBudgetRequestYearly("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmBudgetRequestYearly("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtLocalCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local Code");
        }

        #endregion

        #endregion
    }
}
