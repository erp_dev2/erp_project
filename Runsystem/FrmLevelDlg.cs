﻿#region Update
/*
    14/10/2020 [WED/PHT] bisa dipakai untuk grid kedua (position allowance)
    09/12/2021 [RIS/PHT] Allowance deduction bisa diambil lebih dari 1 kali dengan param = IsAllowanceDeductionLevelCanBeSelectedMoreThanOnce
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLevelDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmLevel mFrmParent;
        private string mSQL = string.Empty;
        private byte mGrd = 0;

        #endregion

        #region Constructor

        public FrmLevelDlg(FrmLevel FrmParent, byte Grd)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mGrd = Grd;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetSQL(); 
            SetGrd();
            Sl.SetLueADType(ref LueADType);
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Allowance/Deduction"+Environment.NewLine+"Code",
                        "",
                        "Allowance/Deduction"+Environment.NewLine+"Name",
                        "Type"
                    }
                );
            Sm.SetGrdProperty(Grd1, true);
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ADCode, A.ADName, B.OptDesc ");
            SQL.AppendLine("From TblAllowanceDeduction A ");
            SQL.AppendLine("Inner Join TblOption B On A.ADType=B.OptCode And B.OptCat='ADType' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                var Filter = string.Empty;

                if (mGrd == 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(mFrmParent.Grd1, r, 2).Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " And ";
                            Filter += "(A.ADCode<>@ADCode0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ADCode0" + r.ToString(), Sm.GetGrdStr(mFrmParent.Grd1, r, 2));
                        }
                    }
                }
                else
                {
                    for (int r = 0; r < mFrmParent.Grd2.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(mFrmParent.Grd2, r, 2).Length != 0)
                        {
                            if (!mFrmParent.mIsAllowanceDeductionLevelCanBeSelectedMoreThanOnce)
                            {
                                if (Filter.Length > 0) Filter += " And ";
                                Filter += "(A.ADCode<>@ADCode0" + r.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@ADCode0" + r.ToString(), Sm.GetGrdStr(mFrmParent.Grd2, r, 2));
                            }
                            
                        }
                    }
                }
                if (Filter.Length > 0) Filter = " Where (" + Filter + ") ";
                

                Sm.FilterStr(ref Filter, ref cm, TxtADCode.Text, new string[] { "A.ADCode", "A.ADName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueADType), "A.ADType", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.ADName;",
                        new string[]{ "ADCode", "ADName","OptDesc" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1)  && !IsADCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        if (mGrd == 1)
                        {
                            Row1 = mFrmParent.Grd1.Rows.Count - 1;
                            Row2 = Row;
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                            mFrmParent.Grd1.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 5 });
                        }
                        else
                        {
                            Row1 = mFrmParent.Grd2.Rows.Count - 1;
                            Row2 = Row;
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 4);
                            mFrmParent.Grd2.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 11 });
                        }
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 record.");
        }

        private bool IsADCodeAlreadyChosen(int Row)
        {
            if (!mFrmParent.mIsAllowanceDeductionLevelCanBeSelectedMoreThanOnce)
            {
                var ADCode = Sm.GetGrdStr(Grd1, Row, 2);
                if (mGrd == 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count - 1; r++)
                        if (Sm.CompareStr(ADCode, Sm.GetGrdStr(mFrmParent.Grd1, r, 2))) return true;
                }
                else
                {
                    for (int r = 0; r < mFrmParent.Grd2.Rows.Count - 1; r++)
                        if (Sm.CompareStr(ADCode, Sm.GetGrdStr(mFrmParent.Grd2, r, 2))) return true;
                }
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAllowanceDeduction(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mADCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmAllowanceDeduction(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mADCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtADCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkADCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Allowance/Deduction");
        }

        private void LueADType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueADType, new Sm.RefreshLue1(Sl.SetLueADType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkADType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Allowance/Deduction Type");
        }
       
        #endregion

        #endregion
    }
}
