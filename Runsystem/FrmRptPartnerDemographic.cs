﻿#region Update
/*
    25/02/2019 [MEY] Penambahan informasi Types of Loans     
    15/07/2019 [DITA] Bug saat Filter Partner Demographic 
*/
#endregion

#region Namespace
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
#endregion

namespace RunSystem
{
    public partial class FrmRptPartnerDemographic : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptPartnerDemographic(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueCityCode(ref LueCityCode);
                Sl.SetLueProvCode(ref LueProvCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T3.ProvCode, T3.ProvName,  T2.CityCode, T2.CityName, T4.PnCode, ");  
            SQL.AppendLine("T1.PnName, T1.IdentityNo, T1.Address,  ");
            SQL.AppendLine("T9.OptDesc AS Gender, ");
            SQL.AppendLine("T1.BirthDt, T5.Amt, T6.DocDt, T5.ManPower, T5.Asset, T5.Omzet, ");
            SQL.AppendLine("T5.NoOfLoan, T7.BSCode, T7.BSName, T8.BCCode, T8.BCName, ");
            SQL.AppendLine("T10.SurveyDocNo, (T5.Amt - Sum(T10.PaymentAmtMain)) OutstandingAmt, T11.OptDesc as 'TypesOfLoans' ");
            SQL.AppendLine("From TblPartner T1   ");
            SQL.AppendLine("Inner Join TblCity T2 ON T1.CityCode=T2.CityCode  ");
            SQL.AppendLine("Inner Join TblProvince T3 ON T2.ProvCode=T3.ProvCode  ");
            SQL.AppendLine("Inner Join TblRequestlp T4 ON T1.PnCode=T4.PnCode  ");
            SQL.AppendLine("Inner Join TblSurvey T5 ON T4.DocNo=T5.RQLPDocNo  ");
            SQL.AppendLine("Inner Join TblVoucherHdr T6 ON T6.VoucherRequestDocNo=T5.VoucherRequestDocNo  ");
            SQL.AppendLine("Inner Join TblBusinessSector T7 ON T7.BSCode=T1.BSCode  ");
            SQL.AppendLine("Inner Join TblBusinessCategory T8 ON T8.BCCode=T1.BCCode  ");
            SQL.AppendLine("Left Join TblOption T9 On T1.Gender = T9.OptCode And T9.OptCat = 'Gender' ");
            SQL.AppendLine("Inner Join TblLoanSummary T10 ON T10.SurveyDocNo = T5.DocNo ");
            SQL.AppendLine("Left Join TblOption T11 On T4.TypesofLoans = T11.OptCode And T11.OptCat = 'TypesOfLoans'  ");
            
            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Province Code",
                    "Province Name",
                    "City Code",
                    "City Name",
                    "Partner Code",

                    //6-10
                    "Partner Name",
                    "Identity Number",
                    "Address",
                    "Gender",
                    "Birth Date",

                    //11-15
                    "Amount",
                    "Outstanding Amount",
                    "Loan Date",
                    "Status Code",
                    "Status",

                    //16-20
                    "Manpower",
                    "Asset",
                    "Omzet",
                    "No of Loan",
                    "Bussiness" + Environment.NewLine + "Sector Code",
                   
                    //21-24
                    "Bussiness Sector",
                    "Bussiness" + Environment.NewLine + "Category Code",
                    "Bussiness Category",
                    "SurveyDocNo",
                    "Types of Loans"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    100, 200, 100, 100, 100, 
                    
                    //6-10
                    100, 150, 200, 50, 100, 
                    
                    //11-15
                    150, 150, 100,100,100,

                    //16-20
                    100, 100, 100, 100, 150, 

                    //21-25
                    150, 150, 150, 0, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 16, 17, 18, 19 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 10, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5, 14, 20, 22, 24 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,  });
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[25].Move(13);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5, 14, 20, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Sm.ClearGrd(Grd1, false);
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty; ;
                
                var mGroupBy = new StringBuilder();
                mGroupBy.AppendLine(" Group By T3.ProvCode, T3.ProvName, T2.CityCode, T2.CityName, T4.PnCode, ");
                mGroupBy.AppendLine("T1.PnName, T1.IdentityNo, T1.Address, ");
                mGroupBy.AppendLine("T9.OptDesc, T1.BirthDt, T5.Amt, T6.DocDt, T5.ManPower, T5.Asset, T5.Omzet, ");
                mGroupBy.AppendLine("T5.NoOfLoan, T7.BSCode, T7.BSName, T8.BCCode, T8.BCName, T10.SurveyDocNo, T11.OptDesc; ");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtPartnerCode.Text, new string[] { "T4.PnCode", "T1.PnName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCityCode), "T2.CityCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueProvCode), "T3.ProvCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + mGroupBy.ToString(),
                    new string[]
                    {
                        //0
                        "ProvCode", 

                        //1-5
                        "ProvName", "CityCode", "CityName", "PnCode", "PnName", 
                        
                        //6-10
                        "IdentityNo","Address","Gender","BirthDt","Amt",
                        
                        //11-15
                        "OutstandingAmt","DocDt", "ManPower", "Asset","Omzet",

                        //16-20
                        "NoOfLoan", "BSCode", "BSName", "BCCode","BCName",

                        //21
                        "SurveyDocNo", "TypesOfLoans",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                    }, true, false, false, false
                );

                var l = new List<PLP>();

                PrepPLP(ref l);

                l.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void PrepPLP(ref List<PLP> l)
        {
            string mSurveyDocNo = string.Empty;
            var lA = new List<PLPFulfilled>();
            var lP = new List<PLPPaid>();
            var lB = new List<PLPBackDt>();

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (mSurveyDocNo.Length > 0) mSurveyDocNo += ",";
                    mSurveyDocNo += Sm.GetGrdStr(Grd1, i, 24);
                }
            }

            if (mSurveyDocNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select SurveyDocNo, Count(Mth) CountAll ");
                SQL.AppendLine("From TblLoanSummary ");
                SQL.AppendLine("Where Find_In_Set(SurveyDocNo, @SurveyDocNo) ");
                SQL.AppendLine("Group By SurveyDocNo; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@SurveyDocNo", mSurveyDocNo);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "SurveyDocNo", "CountAll" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new PLP()
                            {
                                SurveyDocNo = Sm.DrStr(dr, c[0]),
                                CountAll = Sm.DrDec(dr, c[1]),
                                CountOutstandingBackDt = 0m,
                                CountPaid = 0m,
                                CountOutstandingAmt = 0m,
                                CountFulfilled = 0m
                            });
                        }
                    }
                    dr.Close();
                }

                PrepCountFulfilled(ref lA, mSurveyDocNo, ref l);
                PrepCountPaid(ref lP, mSurveyDocNo, ref l);
                PrepCountOutstandingBackDt(ref lB, mSurveyDocNo, ref l);
                ProcessStatus(ref l);
                SetStatus(ref l);

                lA.Clear(); lP.Clear(); lB.Clear();
            }
        }

        private void PrepCountFulfilled(ref List<PLPFulfilled> lA, string SurveyDocNo, ref List<PLP> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select SurveyDocNo, Count(Mth) CountFulfilled ");
            SQL.AppendLine("From TblLoanSummary ");
            SQL.AppendLine("Where Find_In_Set(SurveyDocNo, @SurveyDocNo) ");
            SQL.AppendLine("And PaymentInd = 'F' ");
            SQL.AppendLine("Group By SurveyDocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SurveyDocNo", SurveyDocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SurveyDocNo", "CountFulfilled" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lA.Add(new PLPFulfilled()
                        {
                            SurveyDocNo = Sm.DrStr(dr, c[0]),
                            CountFulfilled = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }

            if (lA.Count > 0)
            {
                for (int i = 0; i < l.Count; i++)
                {
                    for (int j = 0; j < lA.Count; j++)
                    {
                        if (l[i].SurveyDocNo == lA[j].SurveyDocNo)
                        {
                            l[i].CountFulfilled = lA[j].CountFulfilled;
                            break;
                        }
                    }
                }
            }

        }

        private void PrepCountPaid(ref List<PLPPaid> lP, string SurveyDocNo, ref List<PLP> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select SurveyDocNo, Count(Mth) CountPaid ");
            SQL.AppendLine("From TblLoanSummary ");
            SQL.AppendLine("Where Find_In_Set(SurveyDocNo, @SurveyDocNo) ");
            SQL.AppendLine("And PaymentInd In ('F', 'P') ");
            SQL.AppendLine("Group By SurveyDocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SurveyDocNo", SurveyDocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SurveyDocNo", "CountPaid" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lP.Add(new PLPPaid()
                        {
                            SurveyDocNo = Sm.DrStr(dr, c[0]),
                            CountPaid = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }

            if (lP.Count > 0)
            {
                for (int i = 0; i < l.Count; i++)
                {
                    for (int j = 0; j < lP.Count; j++)
                    {
                        if (l[i].SurveyDocNo == lP[j].SurveyDocNo)
                        {
                            l[i].CountPaid = lP[j].CountPaid;
                            break;
                        }
                    }
                }
            }
        }

        private void PrepCountOutstandingBackDt(ref List<PLPBackDt> lB, string SurveyDocNo, ref List<PLP> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select SurveyDocNo, Count(Mth) CountBackDt ");
            SQL.AppendLine("From TblLoanSummary ");
            SQL.AppendLine("Where Find_In_Set(SurveyDocNo, @SurveyDocNo) ");
            SQL.AppendLine("And PaymentInd In ('O') ");
            SQL.AppendLine("And Concat(Yr, Mth) <= @CurrentYrMth ");
            SQL.AppendLine("Group By SurveyDocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SurveyDocNo", SurveyDocNo);
                Sm.CmParam<String>(ref cm, "@CurrentYrMth", Sm.Left(Sm.ServerCurrentDateTime(), 6));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SurveyDocNo", "CountBackDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lB.Add(new PLPBackDt()
                        {
                            SurveyDocNo = Sm.DrStr(dr, c[0]),
                            CountOutstandingBackDt = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }

            if (lB.Count > 0)
            {
                for (int i = 0; i < l.Count; i++)
                {
                    for (int j = 0; j < lB.Count; j++)
                    {
                        if (l[i].SurveyDocNo == lB[j].SurveyDocNo)
                        {
                            l[i].CountOutstandingBackDt = lB[j].CountOutstandingBackDt;
                            break;
                        }
                    }
                }
            }
        }

        private void ProcessStatus(ref List<PLP> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].CountFulfilled == l[i].CountAll)
                {
                    l[i].StatusCode = "6";
                    l[i].Status = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'PLPCollectabilityStatus' And OptCode = @Param; ", l[i].StatusCode);
                }
                else
                {
                    if (l[i].CountOutstandingBackDt < 1)
                    {
                        l[i].StatusCode = "1";
                        l[i].Status = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'PLPCollectabilityStatus' And OptCode = @Param; ", l[i].StatusCode);
                    }
                    else if (l[i].CountOutstandingBackDt >= 1 && l[i].CountOutstandingBackDt < 6)
                    {
                        l[i].StatusCode = "2";
                        l[i].Status = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'PLPCollectabilityStatus' And OptCode = @Param; ", l[i].StatusCode);
                    }
                    else if (l[i].CountOutstandingBackDt >= 6 && l[i].CountOutstandingBackDt < 9)
                    {
                        l[i].StatusCode = "3";
                        l[i].Status = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'PLPCollectabilityStatus' And OptCode = @Param; ", l[i].StatusCode);
                    }
                    else if (l[i].CountOutstandingBackDt >= 9 && l[i].CountOutstandingBackDt < 12)
                    {
                        l[i].StatusCode = "4";
                        l[i].Status = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'PLPCollectabilityStatus' And OptCode = @Param; ", l[i].StatusCode);
                    }
                    else if (l[i].CountOutstandingBackDt >= 12)
                    {
                        l[i].StatusCode = "5";
                        l[i].Status = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'PLPCollectabilityStatus' And OptCode = @Param; ", l[i].StatusCode);
                    }
                }
            }
        }

        private void SetStatus(ref List<PLP> l)
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                for (int j = 0; j < l.Count; j++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 24) == l[j].SurveyDocNo)
                    {
                        Grd1.Cells[i, 14].Value = l[j].StatusCode;
                        Grd1.Cells[i, 15].Value = l[j].Status;
                        break;
                    }
                }
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void LueCityCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue1(Sl.SetLueCityCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCityCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "City");
        }

        private void LueProvCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProvCode, new Sm.RefreshLue1(Sl.SetLueProvCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkProvCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Province");
        }

        private void TxtPartnerCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPartnerCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Partner");
        }

        #endregion

        #region Class

        private class PLP
        {
            public string SurveyDocNo { get; set; }
            public decimal CountOutstandingAmt { get; set; }
            public decimal CountAll { get; set; }
            public decimal CountFulfilled { get; set; }
            public decimal CountPaid { get; set; }
            public decimal CountOutstandingBackDt { get; set; }
            public string StatusCode { get; set; }
            public string Status { get; set; }
        }

        private class PLPFulfilled
        {
            public string SurveyDocNo { get; set; }
            public decimal CountFulfilled { get; set; }
        }

        private class PLPPaid
        {
            public string SurveyDocNo { get; set; }
            public decimal CountPaid { get; set; }
        }

        private class PLPBackDt
        {
            public string SurveyDocNo { get; set; }
            public decimal CountOutstandingBackDt { get; set; }
        }

        #endregion

    }
}
