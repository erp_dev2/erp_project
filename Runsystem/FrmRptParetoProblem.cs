﻿#region Update
/*
    09/08/2017 [WED] tambah count pareto nya
    13/04/2018 [ARI] tambah kolom asset category
    21/07/2018 [TKG] WO Request dan WO yg dicancel tidak diproses.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptParetoProblem : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptParetoProblem(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueLocCode(ref LueLocCode);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.OptDesc As Symptom, A.TOCode, B.AssetName, B.DisplayName, A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case A.WOStatus When 'O' Then 'Open' When 'C' Then 'Closed' End Status, ");
            SQL.AppendLine("IfNull(Right(Concat('0000', Sum(E.TotalDuration)), 4), '') As Duration, B.AssetCategorycode, F.AssetCategoryName ");
            SQL.AppendLine("From TblWOR A ");
            SQL.AppendLine("Inner Join TblAsset B On A.TOCode=B.AssetCode ");
            SQL.AppendLine("Left Join TblOption C On A.SymProblem = C.OptCode And C.OptCat ='SymptomProblem' ");
            SQL.AppendLine("Inner Join TblTOHdr D On A.TOCode = D.AssetCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo As WODocNo, T1.WORDocNo, T2.Tm3 As TotalDuration ");
	        SQL.AppendLine("    From TblWOHdr T1 ");
	        SQL.AppendLine("    Inner Join TblWODtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine(") E On A.DocNo = E.WORDocNo ");
            SQL.AppendLine("Left Join tblassetcategory F On B.AssetCategorycode=F.AssetCategoryCode ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status<>'C' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Symptom Problem", 
                        "Asset Code",
                        "Asset Name", 
                        "Display Name", 
                        "Asset"+Environment.NewLine+"Category Code", 

                        //6-10
                        "Asset"+Environment.NewLine+"Category Name", 
                        "WO Request", 
                        "",
                        "WO Request"+Environment.NewLine+"Date", 
                        "WO Request"+Environment.NewLine+"Status",

                        //11
                        "Duration",
                      
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        180, 120, 250, 250, 100,

                        //6-10
                        120, 200, 20, 100, 100, 

                        //11
                        100

                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 8 });
            Sm.GrdFormatDate(Grd1, new int[] { 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 8 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 8}, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtParetoCode.Text, "C.OptDesc", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode), "D.LocCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "A.TOCode", "B.AssetName", "B.DisplayName" });

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Group By A.DocNo, C.OptDesc Order By C.OptDesc;",
                new string[]
                    {
                        //0
                        "Symptom", 

                        //1-5
                        "TOCode", "AssetName", "DisplayName", "AssetCategorycode", "AssetCategoryName", 

                        //6-9
                        "DocNo", "DocDt", "Status", "Duration", 

                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                   
                }, true, false, false, false
                );
                ComputeDuration();
                Grd1.GroupObject.Add(1);
                Grd1.Group();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWOR(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmWOR(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void ComputeDuration()
        {
            string TotalHM = string.Empty;
            int hours = 0;
            decimal minutes = 0m;

            for (int x = 0; x < Grd1.Rows.Count; x++)
            {
                if (Sm.GetGrdStr(Grd1, x, 11).Length > 0)
                {
                    hours = Int32.Parse(Sm.Left(Sm.GetGrdStr(Grd1, x, 11), 2));
                    minutes = Decimal.Parse(Sm.Right(Sm.GetGrdStr(Grd1, x, 11), 2));

                    string m = Sm.Left(string.Concat((minutes % 60).ToString(), "00"), 2);
                    if (minutes > 59)
                    {
                        hours = hours + (Int32.Parse(minutes.ToString()) / 60);
                    }
                    string h = Sm.Right(string.Concat("00", hours.ToString()), 2);

                    TotalHM = string.Concat(h, ":", m);
                }
                else
                {
                    TotalHM = string.Empty;
                }

                Grd1.Cells[x, 11].Value = TotalHM;
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document Number");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtPareto_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPareto_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Symptom Problem");
        }

        private void TxtAsset_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAsset_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void LueLocCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocCode, new Sm.RefreshLue1(Sl.SetLueLocCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkLocCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location");
        }

        #endregion

    }
}
