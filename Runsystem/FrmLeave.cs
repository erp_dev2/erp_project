﻿#region Update
/*
    27/12/2017 [TKG] tambah informasi point.
    23/10/2019 [RF/SIER] tambah informasi leave color
    25/11/2020 [IBL/PHT] tambah informasi minimum leave
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmLeave : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmLeaveFind FrmFind;

        #endregion

        #region Constructor

        public FrmLeave(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueRLPCode, "ResidualLeave");
                Sl.SetLueOption(ref LueLeaveColor, "LeaveColor");
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "System Type",
                        
                        //1-4
                        "System Type",
                        "Payrun Period",
                        "Payrun Period",
                        "Deduct"+Environment.NewLine+"Take Home Pay"
                    },
                     new int[] 
                    {
                        //0
                        0,
 
                        //1-4
                        200, 0, 200, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLeaveCode, TxtLeaveName, LueLGCode, TxtNoOfDay, TxtNoOfDayExpired, 
                        LueRLPCode, TxtPoint, MeeRemark, TxtShortName, LueLeaveColor,
                        TxtMinLeave,
                    }, true);        
                    ChkActInd.Properties.ReadOnly = true;
                    ChkPaidInd.Properties.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 4 });
                    TxtLeaveCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLeaveCode, TxtLeaveName, LueLGCode, TxtNoOfDay, TxtNoOfDayExpired, 
                        LueRLPCode, TxtPoint, MeeRemark, TxtShortName, LueLeaveColor,
                        TxtMinLeave
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4 });
                    ChkPaidInd.Properties.ReadOnly = false;
                    TxtLeaveCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLeaveName, LueLGCode, TxtNoOfDay, TxtNoOfDayExpired, LueRLPCode, 
                        TxtPoint, MeeRemark, TxtShortName, LueLeaveColor, TxtMinLeave
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4 });
                    ChkActInd.Properties.ReadOnly = false;
                    ChkPaidInd.Properties.ReadOnly = false;
                    TxtLeaveName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtLeaveCode, TxtLeaveName, TxtShortName, LueLGCode, LueRLPCode, MeeRemark, LueLeaveColor });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtNoOfDay, TxtNoOfDayExpired, TxtMinLeave }, 0);
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtPoint }, 2);
            ChkActInd.Checked = false;
            ChkPaidInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 4 });
        }

        #endregion         

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmLeaveFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                SetLueLGCode(ref LueLGCode, string.Empty);
                ChkActInd.Checked = true;
                ShowLeaveDeductInfo();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtLeaveCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtLeaveCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() 
                { 
                    CommandText = 
                        "Delete From TblLeave Where LeaveCode=@LeaveCode;" +
                        "Delete From TblLeaveDtl Where LeaveCode=@LeaveCode;" 
                };
                Sm.CmParam<String>(ref cm, "@LeaveCode", TxtLeaveCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                SaveData(!TxtLeaveCode.Properties.ReadOnly);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string LeaveCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowLeave(LeaveCode);
                ShowLeaveDtl(LeaveCode);
                if (Sm.GetGrdStr(Grd1, 0, 0).Length == 0)
                    ShowLeaveDeductInfo();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowLeave(string LeaveCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@LeaveCode", LeaveCode);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select LeaveCode, LeaveName, LGCode, ActInd, PaidInd, " +
                    "NoOfDay, NoOfDayExpired, RLPCode, Point, Remark, ShortName, LeaveColor, MinimumLeave " +
                    "From TblLeave Where LeaveCode=@LeaveCode;",
                    new string[] 
                        {
                            //0
                            "LeaveCode", 
                            //1-5
                            "LeaveName", "LGCode", "ActInd", "PaidInd", "NoOfDay", 
                            //6-10
                            "NoOfDayExpired", "RLPCode", "Point", "Remark", "ShortName",

                            //11-12
                            "LeaveColor", "MinimumLeave"
                        },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtLeaveCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtLeaveName.EditValue = Sm.DrStr(dr, c[1]);
                        SetLueLGCode(ref LueLGCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueLGCode, Sm.DrStr(dr, c[2]));
                        ChkActInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                        ChkPaidInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                        TxtNoOfDay.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                        TxtNoOfDayExpired.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                        Sm.SetLue(LueRLPCode, Sm.DrStr(dr, c[7]));
                        TxtPoint.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 2);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                        TxtShortName.EditValue = Sm.DrStr(dr, c[10]);
                        Sm.SetLue(LueLeaveColor, Sm.DrStr(dr, c[11]));
                        TxtMinLeave.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    }, true
                );
        }

        private void ShowLeaveDtl(string LeaveCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.SystemType, B.OptDesc As SystemTypeDesc, ");
            SQL.AppendLine("A.PayrunPeriod, C.OptDesc As PayrunPeriodDesc, A.DeductTHPInd ");
            SQL.AppendLine("From TblLeaveDtl A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat='EmpSystemType' And A.SystemType=B.OptCode ");
            SQL.AppendLine("Left Join TblOption C On C.OptCat='PayrunPeriod' And A.PayrunPeriod=C.OptCode ");
            SQL.AppendLine("Where A.LeaveCode=@LeaveCode Order By B.OptDesc, C.OptDesc;");

            Sm.CmParam<String>(ref cm, "@LeaveCode", LeaveCode);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "SystemType",  

                    //1-4
                    "SystemTypeDesc", "PayrunPeriod", "PayrunPeriodDesc", "DeductTHPInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 4);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtLeaveCode, "Leave code", false) ||
                Sm.IsTxtEmpty(TxtLeaveName, "Leave name", false) ||
                Sm.IsLueEmpty(LueLGCode, "Leave group") ||
                Sm.IsTxtEmpty(TxtNoOfDay, "Number of days", true) ||
                IsLeaveCodeExisted();
        }

        private bool IsLeaveCodeExisted()
        {
            if (!TxtLeaveCode.Properties.ReadOnly && 
                Sm.IsDataExist("Select LeaveCode From TblLeave Where LeaveCode='" + TxtLeaveCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Leave code ( " + TxtLeaveCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private void SaveData(bool IsInserted)
        {
            var cml = new List<MySqlCommand>();

            cml.Add(SaveLeave(!TxtLeaveCode.Properties.ReadOnly));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveLeaveDtl(Row));

            Sm.ExecCommands(cml);
            ShowData(TxtLeaveCode.Text);
        }

        private MySqlCommand SaveLeave(bool IsInserted)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLeave(LeaveCode, LeaveName, ShortName, LGCode, ActInd, PaidInd, NoOfDay, NoOfDayExpired, MinimumLeave, RLPCode, Point, LeaveColor, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@LeaveCode, @LeaveName, @ShortName, @LGCode, @ActInd, @PaidInd, @NoOfDay, @NoOfDayExpired, @MinimumLeave, @RLPCode, @Point, @LeaveColor, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update LeaveName=@LeaveName, ShortName=@ShortName, LGCode=@LGCode, ActInd=@ActInd, PaidInd=@PaidInd, NoOfDay=@NoOfDay, NoOfDayExpired=@NoOfDayExpired, MinimumLeave=@MinimumLeave, RLPCode=@RLPCode, Point=@Point, LeaveColor=@LeaveColor, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            if (!IsInserted)
                SQL.AppendLine("Delete From TblLeaveDtl Where LeaveCode=@LeaveCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@LeaveCode", TxtLeaveCode.Text);
            Sm.CmParam<String>(ref cm, "@LeaveName", TxtLeaveName.Text);
            Sm.CmParam<String>(ref cm, "@ShortName", TxtShortName.Text);
            Sm.CmParam<String>(ref cm, "@LGCode", Sm.GetLue(LueLGCode));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@PaidInd", ChkPaidInd.Checked ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@NoOfDay", decimal.Parse(TxtNoOfDay.Text));
            Sm.CmParam<Decimal>(ref cm, "@NoOfDayExpired", decimal.Parse(TxtNoOfDayExpired.Text));
            Sm.CmParam<Decimal>(ref cm, "@MinimumLeave", decimal.Parse(TxtMinLeave.Text));
            Sm.CmParam<String>(ref cm, "@RLPCode", Sm.GetLue(LueRLPCode));
            Sm.CmParam<Decimal>(ref cm, "@Point", decimal.Parse(TxtPoint.Text));
            Sm.CmParam<String>(ref cm, "@LeaveColor", Sm.GetLue(LueLeaveColor));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveLeaveDtl(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblLeaveDtl(LeaveCode, SystemType, PayrunPeriod, DeductTHPInd, CreateBy, CreateDt, LastUpBy, LastUpDt) " +
                    "Select LeaveCode, @SystemType, @PayrunPeriod, @DeductTHPInd, CreateBy, CreateDt, LastUpBy, LastUpDt " +
                    "From TblLeave Where LeaveCode=@LeaveCode;"
            };
            Sm.CmParam<String>(ref cm, "@LeaveCode", TxtLeaveCode.Text);
            Sm.CmParam<String>(ref cm, "@SystemType", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@DeductTHPInd", Sm.GetGrdBool(Grd1, Row, 4)?"Y":"N");
            return cm;
        }

        #endregion

        #region Additional Method

        private void SetLueLGCode(ref DXE.LookUpEdit Lue, string LGCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select LGCode As Col1, LGName As Col2 ");
                SQL.AppendLine("From TblLeaveGrp Where ActInd='Y' ");

                if (LGCode.Length != 0)
                {
                    SQL.AppendLine("Union All ");
                    SQL.AppendLine("Select LGCode As Col1, LGName As Col2 From TblLeaveGrp Where LGCode='" + LGCode + "' ");
                }
                SQL.AppendLine("Order By Col2;");
                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowLeaveDeductInfo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.OptCode As SystemTypeCode, A.OptDesc As SystemTypeDesc, ");
            SQL.AppendLine("B.OptCode As PayrunPeriodCode, B.OptDesc As PayrunPeriodDesc ");
            SQL.AppendLine("From TblOption A ");
            SQL.AppendLine("Inner Join TblOption B On B.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Where A.OptCat='EmpSystemType' ");
            SQL.AppendLine("Order By A.OptDesc, B.OptDesc;");

            var cm = new MySqlCommand();
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "SystemTypeCode", 

                    //1-3
                    "SystemTypeDesc", "PayrunPeriodCode", "PayrunPeriodDesc"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Grd.Cells[Row, 4].Value = false;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length==0)
                e.DoDefault = false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtLeaveCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLeaveCode);
        }

        private void TxtLeaveName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLeaveName);
        }

        private void TxtNoOfDay_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtNoOfDay, 0);
        }

        private void TxtMinLeave_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMinLeave, 0);
        }

        private void TxtNoOfDayExpired_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtNoOfDayExpired, 0);
        }

        private void TxtPoint_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtPoint, 2);
        }

        private void LueLGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLGCode, new Sm.RefreshLue2(SetLueLGCode), string.Empty);
        }

        private void LueRLPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueRLPCode, new Sm.RefreshLue2(Sl.SetLueOption), "ResidualLeave");
        }

        private void TxtShortName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtShortName);
        }

        private void LueLeaveColor_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLeaveColor, new Sm.RefreshLue2(Sl.SetLueOption), "LeaveColor");
        }

        #endregion
      
        #endregion

    }
}
