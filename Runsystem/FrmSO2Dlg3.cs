﻿#region Update
/* 02/07/2018 [HAR] tambah parameter IsCtQtUseItemType.
   08/07/2020 [DITA/SIER] qty mengikuti qtypackagingunit dan luepackaging auto tampil untuk baris pertama saat pilih item param --> mIsSO2NotUsePackagingLabel
   15/06/2021 [MYA/SIER] Kolom item category pada menu Sales Order w/ fixed price terfilter berdasarkan grup log in.
 * */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSO2Dlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSO2Dlg2 mFrmParent;
        private string mSQL = string.Empty, mDocNo = string.Empty, mMenuCode = string.Empty, mCtCode = string.Empty;
        private bool mIsCustomerItemNameMandatory = false;
        private bool mIsCtQtUseItemType = false,
            mIsItCtFilteredByGroup = false;

        #endregion

        #region Constructor

        public FrmSO2Dlg3(FrmSO2Dlg2 FrmParent, string DocNo, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocNo = DocNo;
            mCtCode = CtCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            mIsCustomerItemNameMandatory = Sm.GetParameter("IsCustomerItemNameMandatory") == "Y";
            mIsCtQtUseItemType = Sm.GetParameter("IsCtQtUseItemType") == "Y";
            mIsItCtFilteredByGroup = Sm.GetParameterBoo("IsItCtFilteredByGroup");
            SetGrd();
            SetSQL();
            ShowData();
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select D.ItCode, A.ItCodeInternal, A.ItName, A.Specification, E.CtItCode, E.CtItName ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left Join TblItemPriceDtl D On A.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join TblCustomerItem E On A.ItCode=E.ItCode And E.CtCode=@CtCode ");
            SQL.AppendLine("Inner Join TblCtQtDtl B On B.ItemPriceDocno = D.DocNo  ");
	        SQL.AppendLine("    And B.ItemPriceDno=D.DNo ");
            SQL.AppendLine("Inner Join TblCtQtHdr C ");
            SQL.AppendLine("    On B.DocNo = C.DocNo   ");
            SQL.AppendLine("    And C.DocNo = @DocNo  ");
            SQL.AppendLine("    And C.ActInd = 'Y'    ");
            SQL.AppendLine("    And C.Status = 'A'    ");
            if (mIsCtQtUseItemType)
                SQL.AppendLine("    And B.CtQtType = '01' ");
            if (mIsItCtFilteredByGroup)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Item's Code", 
                        "",
                        "Item's"+Environment.NewLine+"Local Code",
                        "Item's Name",
                        "Specification",

                        //6-7
                        "Customer's"+Environment.NewLine+"Item Code",
                        "Customer's"+Environment.NewLine+"Item Name"
                    },
                    new int[]
                    {
                        //0
                        50,
                        
                        //1-5
                        80, 20, 80, 300, 130,

                        //6-7
                        130, 250
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 6 }, false);
            if (!mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 7 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItCodeInternal", "A.ItName", "E.CtItCode" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.ItName;",
                        new string[] 
                        { 
                            "ItCode",
                            "ItCodeInternal", "ItName", "Specification", "CtItCode", "CtItName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtItCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtItName.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                mFrmParent.TxtSpecification.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                mFrmParent.TxtCtItCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                mFrmParent.TxtCtItName.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                mFrmParent.ShowItemInfo();
                mFrmParent.SetLuePackaging(ref mFrmParent.LuePackaging, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), "");
                if (mFrmParent.mIsSO2NotUsePackagingLabel) Sm.SetLue(mFrmParent.LuePackaging, Sm.GetValue("Select UomCode From TblItemPackagingUnit Where ItCode=@Param Order By UomCode Limit 1; ", Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1)));
                this.Close();
            }

        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {             
                var f = new FrmItem("XXX");
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
