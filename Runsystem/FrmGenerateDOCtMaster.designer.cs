﻿namespace RunSystem
{
    partial class FrmGenerateDOCtMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtKVAQuota = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtLWBPHour = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtPJUPercentage = new DevExpress.XtraEditors.TextEdit();
            this.TxtLWBP = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtKAP = new DevExpress.XtraEditors.TextEdit();
            this.TxtWBP = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtWBPHour = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKVAQuota.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLWBPHour.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPJUPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLWBP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKAP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWBP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWBPHour.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtLWBPHour);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtPJUPercentage);
            this.panel2.Controls.Add(this.TxtLWBP);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtKAP);
            this.panel2.Controls.Add(this.TxtWBP);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtWBPHour);
            this.panel2.Controls.Add(this.TxtKVAQuota);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LueWhsCode);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(14, 11);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 14);
            this.label3.TabIndex = 9;
            this.label3.Text = "Warehouse";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(87, 8);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseBackColor = true;
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 20;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 500;
            this.LueWhsCode.Size = new System.Drawing.Size(255, 20);
            this.LueWhsCode.TabIndex = 10;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtKVAQuota
            // 
            this.TxtKVAQuota.EnterMoveNextControl = true;
            this.TxtKVAQuota.Location = new System.Drawing.Point(87, 29);
            this.TxtKVAQuota.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKVAQuota.Name = "TxtKVAQuota";
            this.TxtKVAQuota.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKVAQuota.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKVAQuota.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKVAQuota.Properties.Appearance.Options.UseFont = true;
            this.TxtKVAQuota.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtKVAQuota.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtKVAQuota.Properties.MaxLength = 30;
            this.TxtKVAQuota.Size = new System.Drawing.Size(97, 20);
            this.TxtKVAQuota.TabIndex = 12;
            this.TxtKVAQuota.Validated += new System.EventHandler(this.TxtKVAQuota_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(16, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 14);
            this.label4.TabIndex = 11;
            this.label4.Text = "kVA Quota";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLWBPHour
            // 
            this.TxtLWBPHour.EnterMoveNextControl = true;
            this.TxtLWBPHour.Location = new System.Drawing.Point(186, 50);
            this.TxtLWBPHour.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLWBPHour.Name = "TxtLWBPHour";
            this.TxtLWBPHour.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLWBPHour.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLWBPHour.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLWBPHour.Properties.Appearance.Options.UseFont = true;
            this.TxtLWBPHour.Properties.MaxLength = 30;
            this.TxtLWBPHour.Size = new System.Drawing.Size(156, 20);
            this.TxtLWBPHour.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(187, 116);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 14);
            this.label7.TabIndex = 23;
            this.label7.Text = "%";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(44, 52);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "LWBP";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPJUPercentage
            // 
            this.TxtPJUPercentage.EnterMoveNextControl = true;
            this.TxtPJUPercentage.Location = new System.Drawing.Point(87, 113);
            this.TxtPJUPercentage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPJUPercentage.Name = "TxtPJUPercentage";
            this.TxtPJUPercentage.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPJUPercentage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPJUPercentage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPJUPercentage.Properties.Appearance.Options.UseFont = true;
            this.TxtPJUPercentage.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPJUPercentage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPJUPercentage.Properties.MaxLength = 30;
            this.TxtPJUPercentage.Size = new System.Drawing.Size(97, 20);
            this.TxtPJUPercentage.TabIndex = 22;
            this.TxtPJUPercentage.Validated += new System.EventHandler(this.TxtPJUPercentage_Validated);
            // 
            // TxtLWBP
            // 
            this.TxtLWBP.EnterMoveNextControl = true;
            this.TxtLWBP.Location = new System.Drawing.Point(87, 50);
            this.TxtLWBP.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLWBP.Name = "TxtLWBP";
            this.TxtLWBP.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLWBP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLWBP.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLWBP.Properties.Appearance.Options.UseFont = true;
            this.TxtLWBP.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLWBP.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLWBP.Properties.MaxLength = 30;
            this.TxtLWBP.Size = new System.Drawing.Size(97, 20);
            this.TxtLWBP.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(56, 115);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 14);
            this.label6.TabIndex = 21;
            this.label6.Text = "PJU";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(50, 73);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 16;
            this.label2.Text = "WBP";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKAP
            // 
            this.TxtKAP.EnterMoveNextControl = true;
            this.TxtKAP.Location = new System.Drawing.Point(87, 92);
            this.TxtKAP.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKAP.Name = "TxtKAP";
            this.TxtKAP.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKAP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKAP.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKAP.Properties.Appearance.Options.UseFont = true;
            this.TxtKAP.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtKAP.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtKAP.Properties.MaxLength = 30;
            this.TxtKAP.Size = new System.Drawing.Size(97, 20);
            this.TxtKAP.TabIndex = 20;
            this.TxtKAP.Validated += new System.EventHandler(this.TxtKAP_Validated);
            // 
            // TxtWBP
            // 
            this.TxtWBP.EnterMoveNextControl = true;
            this.TxtWBP.Location = new System.Drawing.Point(87, 71);
            this.TxtWBP.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWBP.Name = "TxtWBP";
            this.TxtWBP.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWBP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWBP.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWBP.Properties.Appearance.Options.UseFont = true;
            this.TxtWBP.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtWBP.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtWBP.Properties.MaxLength = 30;
            this.TxtWBP.Size = new System.Drawing.Size(97, 20);
            this.TxtWBP.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(54, 94);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 14);
            this.label5.TabIndex = 19;
            this.label5.Text = "KAP";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWBPHour
            // 
            this.TxtWBPHour.EnterMoveNextControl = true;
            this.TxtWBPHour.Location = new System.Drawing.Point(186, 71);
            this.TxtWBPHour.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWBPHour.Name = "TxtWBPHour";
            this.TxtWBPHour.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWBPHour.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWBPHour.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWBPHour.Properties.Appearance.Options.UseFont = true;
            this.TxtWBPHour.Properties.MaxLength = 30;
            this.TxtWBPHour.Size = new System.Drawing.Size(156, 20);
            this.TxtWBPHour.TabIndex = 18;
            // 
            // FrmGenerateDOCtMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmGenerateDOCtMaster";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKVAQuota.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLWBPHour.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPJUPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLWBP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKAP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWBP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWBPHour.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        internal DevExpress.XtraEditors.TextEdit TxtKVAQuota;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtLWBPHour;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtPJUPercentage;
        internal DevExpress.XtraEditors.TextEdit TxtLWBP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtKAP;
        internal DevExpress.XtraEditors.TextEdit TxtWBP;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtWBPHour;
    }
}