﻿#region Update
/*
    11/06/2020 [DITA/IMS] New Application
    12/06/2020 [DITA/IMS] rombak termin dp 2
    17/06/2020 [DITA/IMS] Kolom outstanding amount belum ke update, masih 0
    19/06/2020 [DITA/IMS] bug : Lue Curcode jadi kosong karna ke clear data
    02/07/2020 [DITA/IMS] bug : Dte DocDt jadi kosong saat choose data
    06/08/2020 [VIN/IMS] SOC yang belum di termin DP 1 kan dapat dipilih 
    24/09/2020 [WED/IMS] @DocDt1 dan @DocDt2 diarahkan ke SOCOntractHdr (awalnya ke SOContractTerminDP1. tapi karena jadi left join, jadi dipindah)
    27/10/2020 [DITA/IMS] filter Document# untuk memfilter DocNo SO Contract (sebelumnya docno termindp1)
    17/12/2020 [DITA/IMS] hanya ardp yang sudah approve yang bisa terpilih
    27/01/2021 [IBL/IMS] tambah set default date DueDate saat choose data
    01/02/2021 [WED/IMS] rombak query 
    09/04/2021 [VIN/IMS] Amount ardp dikurangi settlement
    17/05/2021 [VIN/IMS] Bug DueDt
    31/05/2021 [ICA/IMS] menginputkan cost center otomatis ke field cost center pada parent
    10/06/2021 [BRI/IMS] otomatisasi nilai Tax di service/inventory
    11/06/2021 [BRI/IMS] Menambahkan "SO Contract amount after tax"
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSOContractDownpayment2Dlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSOContractDownpayment2 mFrmParent;
        
        #endregion

        #region Constructor

        public FrmSOContractDownpayment2Dlg4(FrmSOContractDownpayment2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            #region Old Query

            /*SQL.AppendLine("Select A.DocNo SOContractDownpaymentDocNo , B.DocNo,B.DocDt, C.CtName, A.CurCode, ");
            SQL.AppendLine("B.Amt+B.AmtBOM As SOContractAmt, ");
            SQL.AppendLine("A.Amt As SOContractDownpaymentAmt, ");
            SQL.AppendLine("IfNull(H.Others, 0.00) As Others, ");
            SQL.AppendLine("A.Amt-IfNull(H.Others, 0.00) As Balance, ");
            SQL.AppendLine("G.ProjectCode, G.ProjectName, B.PONo, D.PtName, ");
            SQL.AppendLine("A.Remark As SOContractDownpaymentRemark, ");
            SQL.AppendLine("B.Remark As SOContractRemark ");
            SQL.AppendLine("From TblSOContractDownpaymentHdr A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOContractDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("Left Join TblPaymentTerm D On B.PtCode=D.PtCode  ");
            SQL.AppendLine("Left Join TblBOQHdr E On B.BOQDocNo=E.DocNo  ");
            SQL.AppendLine("Left Join TblLOPHdr F On E.LOPDocNo=F.DocNO  ");
            SQL.AppendLine("Left Join TblProjectGroup G On F.PGCode=G.PGCode  ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("Select T1.SOContractDownpaymentDocNo, Sum(T1.Amt) As Others ");
            SQL.AppendLine("From TblSOContractDownpayment2Hdr T1 ");
            SQL.AppendLine("Inner Join TblSOContractDownpaymentHdr T2  ");
            SQL.AppendLine("On T1.SOContractDownpaymentDocNo=T2.DocNo ");
            SQL.AppendLine("And T2.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("Group By T1.SOContractDownpaymentDocNo ");
            SQL.AppendLine(") H On A.DocNo=H.SOContractDownpaymentDocNo ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocType = '2' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By B.DocDt Desc, B.DocNo; ");
             */

            #endregion

            #region Old query by wed
            //SQL.AppendLine("Select B.DocNo SOContractDownpaymentDocNo , A.DocNo,A.DocDt, C.CtName, A.CurCode, ");
            //SQL.AppendLine("A.Amt+A.AmtBOM As SOContractAmt,  ");
            //SQL.AppendLine("B.Amt As SOContractDownpaymentAmt,  ");
            //SQL.AppendLine("IfNull(H.Others, 0.00) As Others,  ");
            //SQL.AppendLine("B.Amt-IfNull(H.Others, 0.00) As Balance,  ");
            //SQL.AppendLine("G.ProjectCode, G.ProjectName, A.PONo, D.PtName,  ");
            //SQL.AppendLine("B.Remark As SOContractDownpaymentRemark,  ");
            //SQL.AppendLine("A.Remark As SOContractRemark, ");
            //SQL.AppendLine("Replace(Date_Add(I.DocDt, Interval D.PtDay Day), '-', '') As DueDt ");
            //SQL.AppendLine("FROM TblSOContractHdr A ");
            //SQL.AppendLine("LEFT JOIN TblSOContractDownpaymentHdr B On B.SOContractDocNo=A.DocNo And B.DocType = '2'  AND B.CancelInd = 'N' And B.Status In ('O', 'A') ");
            //SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode  ");
            //SQL.AppendLine("Left Join TblPaymentTerm D On A.PtCode=D.PtCode  ");
            //SQL.AppendLine("Left Join TblBOQHdr E On A.BOQDocNo=E.DocNo   ");
            //SQL.AppendLine("Left Join TblLOPHdr F On E.LOPDocNo=F.DocNO   ");
            //SQL.AppendLine("Left Join TblProjectGroup G On F.PGCode=G.PGCode   ");
            //SQL.AppendLine("Left Join (  ");
            //SQL.AppendLine("Select T1.SOContractDownpaymentDocNo, Sum(T1.Amt) As Others  ");
            //SQL.AppendLine("From TblSOContractDownpayment2Hdr T1  ");
            //SQL.AppendLine("Inner Join TblSOContractDownpaymentHdr T2   ");
            //SQL.AppendLine("On T1.SOContractDownpaymentDocNo=T2.DocNo  ");
            ////SQL.AppendLine("And T2.DocDt Between @DocDt1 And @DocDt2  ");
            //SQL.AppendLine("Where T1.CancelInd='N'  ");
            //SQL.AppendLine("Group By T1.SOContractDownpaymentDocNo  ");
            //SQL.AppendLine(") H On B.DocNo=H.SOContractDownpaymentDocNo  ");
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select T1.DocNo, T3.DocDt ");
            //SQL.AppendLine("    From TblSOContractHdr T1 ");
            //SQL.AppendLine("    Inner Join TblDOCtHdr T2 On T1.DocNo = T2.SOContractDocNo ");
            //SQL.AppendLine("        And T1.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("        And T1.Status = 'A' And T1.CancelInd = 'N' ");
            //SQL.AppendLine("    Inner Join TblDOCtVerifyHdr T3 On T2.DocNo = T3.DOCtDocNo ");
            //SQL.AppendLine("        And T3.CancelInd = 'N' ");
            //SQL.AppendLine("    Union All ");
            //SQL.AppendLine("    Select Distinct T1.DocNo, T3.DocDt ");
            //SQL.AppendLine("    From TblSOContractHdr T1 ");
            //SQL.AppendLine("    Inner Join TblDOCt2Dtl T2 On T1.DocNo = T2.SOContractDocNo ");
            //SQL.AppendLine("         And T1.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("        And T1.Status = 'A' And T1.CancelInd = 'N' ");
            //SQL.AppendLine("    Inner Join TblDOCtVerifyHdr T3 On T2.DocNo = T3.DOCt2DocNo ");
            //SQL.AppendLine("        And T3.CancelInd = 'N' ");
            //SQL.AppendLine(") I On A.DocNo = I.DocNo ");
            //SQL.AppendLine("Where A.CancelInd='N'  ");
            //SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine(Filter);
            //SQL.AppendLine(" Order By A.DocDt Desc, A.DocNo; ");
            #endregion

            SQL.AppendLine("Select C.DocNo SOContractDownpaymentDocNo, A.DocNo, A.DocDt, B.CtName, A.CurCode, ");
            SQL.AppendLine("(A.Amt + A.AmtBOM) SOContractAmt, IfNull(C.Amt-(IFNULL(C2.Amt, 0.00)), 0.00) SOContractDownpaymentAmt, IfNull(H.Others, 0.00) Others, ");
            SQL.AppendLine("IfNull(C.Amt, 0.00) - IfNull(H.Others, 0.00) Balance, F.ProjectCode, F.ProjectName, ");
            SQL.AppendLine("A.PONo, G.PtName, C.Remark SOContractDownpaymentRemark, A.Remark SOContractRemark,  ");
            SQL.AppendLine("Replace(Date_Add(I.DocDt, Interval IfNull(G.PtDay, 0.00) Day), '-', '') As DueDt, Sum(J.Amt) As AfterTax, ");
            SQL.AppendLine("IfNull(H.TaxCode,'1') TaxCode, IfNull(H.TaxCode2,'1') TaxCode2 ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.`Status` = 'A' ");
            SQL.AppendLine("Left Join TblSOContractDownpaymentHdr C On A.DocNo = C.SOContractDocNo ");
            SQL.AppendLine("    And C.DocType = '2' ");
            SQL.AppendLine("    And C.CancelInd = 'N' ");
            SQL.AppendLine("    And C.`Status` In ('O', 'A') ");
            SQL.AppendLine("LEFT JOIN tblsocontractdpsettlementhdr C2 ON C.DocNo=C2.SOContractDownpaymentDocNo ");
            SQL.AppendLine("    AND C2.CancelInd ='N' ");
            SQL.AppendLine("Inner Join TblBOQHdr D On A.BOQDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo = E.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup F On E.PGCode = F.PGCode ");
            SQL.AppendLine("Left Join TblPaymentTerm G On A.PtCode = G.PtCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.SOContractDownpaymentDocNo, T1.SOContractDocNo, Sum(T1.Amt) Others, T1.TaxCode, T1.TaxCode2 ");
            SQL.AppendLine("    From TblSOContractDownpayment2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblSOContractDownpaymentHdr T2 On T1.SOContractDownpaymentDocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.CancelInd = 'N' ");
            SQL.AppendLine("        And T2.CancelInd = 'N' ");
            SQL.AppendLine("    Group By T1.SOContractDownpaymentDocNo, T1.SOContractDocNo ");
            SQL.AppendLine(") H On C.DocNo = H.SOContractDownpaymentDocNo And A.DocNo = H.SOContractDocNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T.SOContractDocNo, T.DocDt ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select X2.SOContractDocNo, X1.DocDt ");
            SQL.AppendLine("        From TblDOCtVerifyHdr X1 ");
            SQL.AppendLine("        Inner Join TblDOCtHdr X2 On X1.DOCtDocNo = X2.Docno ");
            SQL.AppendLine("            And X1.CancelInd = 'N' ");
            SQL.AppendLine("            And X1.DOCtDocNo Is Not Null ");
            SQL.AppendLine("            And X2.SOContractDocNo Is Not Null ");
            SQL.AppendLine("        Inner Join TblSOContractHdr X3 On X2.SOContractDocNo = X3.DocNo ");
            SQL.AppendLine("            And X3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            And X3.CancelInd = 'N' ");
            SQL.AppendLine("            And X3.Status = 'A' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select X3.SOContractDocNo, X1.DocDt ");
            SQL.AppendLine("        From TblDOCtVerifyHdr X1 ");
            SQL.AppendLine("        Inner Join TblDOCt2Hdr X2 On X1.DOCt2DocNo = X2.DocNo ");
            SQL.AppendLine("            And X1.DOCt2DocNo Is Not Null ");
            SQL.AppendLine("            And X1.CancelInd = 'N' ");
            SQL.AppendLine("        Inner Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select Distinct DocNo, SOContractDocNo ");
            SQL.AppendLine("            From TblDOCt2Dtl ");
            SQL.AppendLine("            Where SOContractDocNo Is Not Null ");
            SQL.AppendLine("        ) X3 On X2.DocNo = X3.DocNo ");
            SQL.AppendLine("        Inner Join TblSOContractHdr X4 On X3.SOContractDocNo = X4.DocNo ");
            SQL.AppendLine("            And X4.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            And X4.CancelInd = 'N' ");
            SQL.AppendLine("            And X4.Status = 'A' ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") I On A.DocNo = I.SOContractDocNo ");
            SQL.AppendLine("Inner Join TblSOContractDtl J On A.DocNo = J.DocNo ");

            SQL.AppendLine("Where 0 = 0 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Group By A.DocNo ");
            SQL.AppendLine("Order By A.DocDt Desc, A.DocNo; ");


            return SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "",
                    "Date",
                    "Customer",
                    "Project's Code",

                    //6-10
                    "Project's Name",
                    "Customer's PO#",
                    "Amount",
                    "SO Contract"+Environment.NewLine+" Downpayment#",
                    "SO Contract"+Environment.NewLine+" Downpayment Amount",
 
                    //11-15
                    "Term of Payment",
                    "Remark",
                    "Others",
                    "Balance",
                    "Due Date",

                    //16-19
                    "Currency",
                    "AfterTax",
                    "TaxCode",
                    "TaxCode2"
                },
                new int[]
                {
                    50,
                    150, 20, 80, 200, 150,
                    200, 150, 130, 150, 150,
                    200, 200, 100, 100, 0, 
                    0, 0, 0, 0
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3, 15 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10, 13, 14, 17 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16 });
            Sm.SetGrdProperty(Grd1, false);
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter),
                    new string[]
                    {
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "CtName", "ProjectCode", "ProjectName", "PONo", 
                        //6-10
                        "SOContractAmt" ,"SOContractDownpaymentDocNo","SOContractDownpaymentAmt", "PtName", "SOContractRemark",
                        //11-15
                        "Others", "Balance", "DueDt", "CurCode", "AfterTax", 
                        //16-17
                        "TaxCode", "TaxCode2"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ClearData();
                Sm.SetLue(mFrmParent.LueCurCode, mFrmParent.mMainCurCode);
                Sm.SetDteCurrentDate(mFrmParent.DteDocDt);
                var r = Grd1.CurRow.Index;
                var DocNo = Sm.GetGrdStr(Grd1, r, 1);
                mFrmParent.TxtSOContractDocNo.EditValue = DocNo;
                mFrmParent.TxtCtCode.EditValue = Sm.GetGrdStr(Grd1, r, 4);
                mFrmParent.TxtProjectCode.EditValue = Sm.GetGrdStr(Grd1, r, 5);
                mFrmParent.TxtProjectName.EditValue = Sm.GetGrdStr(Grd1, r, 6);
                mFrmParent.TxtPONo.EditValue = Sm.GetGrdStr(Grd1, r, 7);
                mFrmParent.TxtSOContractAmt.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 8), 0);
                mFrmParent.TxtSOContractAmtAfterTax.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 17), 0);
                mFrmParent.TxtSOContractDownpaymentDocNo.EditValue = Sm.GetGrdStr(Grd1, r, 9);
                mFrmParent.TxtSOContractDownpaymentAmt.EditValue =  Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 10), 0);
                mFrmParent.TxtPtName.EditValue = Sm.GetGrdStr(Grd1, r, 11);
                mFrmParent.MeeSOContractRemark.EditValue = Sm.GetGrdStr(Grd1, r, 12);
                mFrmParent.TxtOthersAmt.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 13), 0);
                mFrmParent.TxtBalanceAmt.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 14), 0);
                Sm.SetLue(mFrmParent.LueCurCode, Sm.GetGrdStr(Grd1, r, 16));
                mFrmParent.ComputeAmt();
                mFrmParent.ClearGrd2();
                mFrmParent.ClearGrd1();
                Sm.SetDteTime(mFrmParent.DteDueDt, Sm.GetGrdDate(Grd1, r, 15));
                mFrmParent.LueTaxCode.EditValue = Sm.GetGrdStr(Grd1, r, 18);
                mFrmParent.LueTaxCode2.EditValue = Sm.GetGrdStr(Grd1, r, 19);
                this.Close();
            }
        }

        private bool isSOCservice()
        {
            var r = Grd1.CurRow.Index;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblSOContractDtl4 where DocNo = @DocNo limit 1; ");

            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 1));

            if (!Sm.IsDataExist(cm))
                return false;
            else
                return true;
        }

        private bool isSOCInventory()
        {
            var r = Grd1.CurRow.Index;
            var SQL = new StringBuilder();
            SQL.AppendLine("Select 1 From TblSOContractDtl5 where DocNo = @DocNo limit 1; ");

            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 1));


            if (!Sm.IsDataExist(cm))
                return false;
            else
                return true;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSOContract2(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSOContract2(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        #endregion

        #endregion

    }
}
