﻿#region Update
/*
    13/10/2017 [TKG] menambah fasilitas untuk select all
    30/06/2022 [BRI/PHT] tambah detail cost center dan parent berdasarkan param IsCostCenterGroupUseDetailInformation
    14/07/2022 [VIN/PHT] Bug saat filter cost center 
    03/08/2022 [VIN/PHT] Bug tambah detail cost center dan parent
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGroupDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmGroup mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmGroupDlg3(FrmGroup FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Cost Center Code",
                        "Cost Center Name",
                        "Parent",
                        "Profit Center",

                        //6
                        "Not Parent"
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 6 }, false);
            Sm.GrdColCheck(Grd1, new int[] { 1, 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6 });
            Sm.SetGrdProperty(Grd1, true);
            if (mFrmParent.mIsCostCenterGroupUseDetailInformation)
                Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 6 }, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CCCode, A.CCName, B.CCName ParentName, C.ProfitCenterName, A.NotParentInd ");
            SQL.AppendLine("From TblCostCenter A ");
            SQL.AppendLine("Left Join TblCostCenter B On A.Parent=B.CCCode ");
            SQL.AppendLine("Left Join TblProfitCenter C On A.ProfitCenterCode=C.ProfitCenterCode ");
            SQL.AppendLine("Where A.CCCode Not In (" + mFrmParent.GetSelectedCostCenter() + ") ");
            //SQL.AppendLine("And A.CCCode Not In (Select Distinct Parent From TblCostCenter Where Parent Is Not Null) ");
            

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "And 0=0 " ;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtCCCode.Text, "A.CCName", false);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CCName;",
                        new string[] 
                        {   //0
                            "CCCode", 
                            
                            //1-4
                            "CCName", "ParentName", "ProfitCenterName", "NotParentInd"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 4);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsCCCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd4.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 2, Grd1, Row2, 3);

                        mFrmParent.Grd4.Rows.Add();

                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 cost center.");
        }

        private bool IsCCCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd4.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd4, Index, 1),
                    Sm.GetGrdStr(Grd1, Row, 2)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtCCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cost center");
        }

        #endregion

        #endregion
    }
}
