﻿#region Update
/*
    04/04/2017 [TKG] tambahan informasi COA's Account
    04/04/2017 [TKG] tambah indikator nomor faktur pajak harus ada atau tidak.
    16/08/2017 [TKG] tambah grup tax.
    07/11/2017 [TKG] group mandatory berdasarkan parameter
    15/03/2018 [ARI] tambah indikator wapu
    02/04/2018 [WED] MaxLength TaxName --> 80
    04/03/2018 [HAR] tambah inputan entity
    13/08/2018 [WED] tambah indikator wajib isi Service atau tidak
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTax : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmTaxFind FrmFind;
        private bool mIsTaxGroupMandatory = false;
        private bool mIsWapuEnabled = false;

        #endregion

        #region Constructor

        public FrmTax(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
                GetParameter();
                SetGrd();
                if (mIsTaxGroupMandatory) LblTaxGrpCode.ForeColor = Color.Red;
                Sl.SetLueTaxGrpCode(ref LueTaxGrpCode);
                Sl.SetLueTaxCode(ref LueParent);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtTaxCode, TxtTaxName, TxtTaxRate, LueTaxGrpCode, TxtLevel, LueParent }, true);
                    ChkTaxInvoiceInd.Properties.ReadOnly = true;
                    ChkWapuInd.Properties.ReadOnly = true;
                    ChkServiceInd.Properties.ReadOnly = true;
                    BtnAcNo1.Enabled = false;
                    Grd1.ReadOnly = true;
                    TxtTaxCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTaxCode, TxtTaxName, TxtTaxRate, LueTaxGrpCode, TxtLevel, LueParent }, false);
                    ChkTaxInvoiceInd.Properties.ReadOnly = false;
                    ChkServiceInd.Properties.ReadOnly = false;
                    if (mIsWapuEnabled)
                        ChkWapuInd.Properties.ReadOnly = false;
                    BtnAcNo1.Enabled = true;
                    Grd1.ReadOnly = false;
                    TxtTaxCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTaxName, TxtTaxRate, LueTaxGrpCode, TxtLevel, LueParent }, false);
                    ChkTaxInvoiceInd.Properties.ReadOnly = false;
                    ChkServiceInd.Properties.ReadOnly = false;
                    ChkWapuInd.Properties.ReadOnly = false;
                    Grd1.ReadOnly = false;
                    BtnAcNo1.Enabled = true;
                    TxtTaxName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtTaxCode, TxtTaxName, LueTaxGrpCode, TxtAcNo1, TxtAcDesc1, 
                TxtAcNo2, TxtAcDesc2,LueParent 
            });
            ChkTaxInvoiceInd.Checked = false;
            ChkWapuInd.Checked = false;
            ChkServiceInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtTaxRate, TxtLevel }, 0);
            Sm.ClearGrd(Grd1, true);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
 
                        //1-2
                        "Entity Code", "Entity Name"
                    },
                    new int[] 
                    {
                        //0
                        20,

                        //1-2
                        0, 200
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2 });
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTaxFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtTaxCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtTaxCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblTax Where TaxCode=@TaxCode" };
                Sm.CmParam<String>(ref cm, "@TaxCode", TxtTaxCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            //try
            //{
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveTax());

                if (Grd1.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                            cml.Add(SaveTaxDtl(ref Grd1, TxtTaxCode.Text, Row)); 
                }

                Sm.ExecCommands(cml);

                ShowData(TxtTaxCode.Text);
            //}
            //catch (Exception Exc)
            //{
            //    Sm.StdMsg(mMsgType.Warning, Exc.Message);
            //}
            //finally
            //{
            //    Cursor.Current = Cursors.Default;
            //}

            
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string TaxCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowTax(TaxCode);
                ShowTaxDtl(ref Grd1, TaxCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTax(string TaxCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.TaxCode, A.TaxName, A.TaxRate, A.TaxGrpCode, A.Parent, A.Level, ");
            SQL.AppendLine("A.AcNo1, A.AcNo2, B.AcDesc As AcDesc1, C.AcDesc As AcDesc2, A.TaxInvoiceInd, A.WapuInd, A.ServiceInd ");
            SQL.AppendLine("From TblTax A ");
            SQL.AppendLine("Left Join TblCOA B On A.AcNo1=B.AcNo ");
            SQL.AppendLine("Left Join TblCOA C On A.AcNo2=C.AcNo ");
            SQL.AppendLine("Where A.TaxCode=@TaxCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@TaxCode", TaxCode);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                        {
                            //0
                            "TaxCode", 
                            //1-5
                            "TaxName", "TaxRate", "TaxGrpCode", "Parent", "Level",  
                            //6-10
                            "AcNo1", "AcDesc1", "AcNo2", "AcDesc2", "TaxInvoiceInd", 
                            //11-12
                            "WapuInd", "ServiceInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtTaxCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtTaxName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtTaxRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                            Sm.SetLue(LueTaxGrpCode, Sm.DrStr(dr, c[3]));
                            Sl.SetLueTaxCode(ref LueParent);
                            Sm.SetLue(LueParent, Sm.DrStr(dr, c[4]));
                            TxtLevel.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                            TxtAcNo1.EditValue = Sm.DrStr(dr, c[6]);
                            TxtAcDesc1.EditValue = Sm.DrStr(dr, c[7]);
                            TxtAcNo2.EditValue = Sm.DrStr(dr, c[8]);
                            TxtAcDesc2.EditValue = Sm.DrStr(dr, c[9]);
                            ChkTaxInvoiceInd.Checked = Sm.DrStr(dr, c[10]) == "Y";
                            ChkWapuInd.Checked = Sm.DrStr(dr, c[11]) == "Y";
                            ChkServiceInd.Checked = Sm.DrStr(dr, c[12]) == "Y";
                        }, true
                );
        }

        private void ShowTaxDtl(ref iGrid GrdTemp, string TaxCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.EntCode, B.EntName ");
            SQL.AppendLine("FROM TblTaxDtl A ");
            SQL.AppendLine("INNER JOIN TblEntity B ON A.EntCode = B.EntCode ");
            SQL.AppendLine("WHERE A.TaxCode=@TaxCode; ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@TaxCode", TaxCode);
            Sm.ShowDataInGrid(
                    ref GrdTemp, ref cm, SQL.ToString(),
                    new string[] { "EntCode", "EntName" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(GrdTemp, 0, 1);
        }
        #endregion

        #region Save Data

        private void GetParameter()
        {
            mIsTaxGroupMandatory = Sm.GetParameterBoo("IsTaxGroupMandatory");
            mIsWapuEnabled = Sm.GetParameter("IsWapuEnabled") == "Y";
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtTaxCode, "Tax code", false) ||
                Sm.IsTxtEmpty(TxtTaxName, "Tax name", false) ||
                Sm.IsTxtEmpty(TxtTaxRate, "Tax rate", true) ||
                (mIsTaxGroupMandatory && Sm.IsLueEmpty(LueTaxGrpCode, "Tax group")) ||
                IsTaxCodeExisted();
        }

        private bool IsTaxCodeExisted()
        {
            if (!TxtTaxCode.Properties.ReadOnly &&
                Sm.IsDataExist("Select TaxCode From TblTax Where TaxCode=@Param Limit 1;", TxtTaxCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Tax code ( " + TxtTaxCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveTax()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTax(TaxCode, TaxName, TaxRate, TaxGrpCode, Parent, Level, AcNo1, AcNo2, TaxInvoiceInd, WapuInd, ServiceInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@TaxCode, @TaxName, @TaxRate, @TaxGrpCode, @Parent, @Level, @AcNo1, @AcNo2, @TaxInvoiceInd, @WapuInd, @ServiceInd, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update TaxName=@TaxName, TaxRate=@TaxRate, TaxGrpCode=@TaxGrpCode, Parent=@Parent, Level=@Level, AcNo1=@AcNo1, AcNo2=@AcNo2, TaxInvoiceInd=@TaxInvoiceInd, WapuInd=@WapuInd, ServiceInd = @ServiceInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("DELETE FROM TblTAxDtl WHERE taxCode=@TaxCode ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@TaxCode", TxtTaxCode.Text);
            Sm.CmParam<String>(ref cm, "@TaxName", TxtTaxName.Text);
            Sm.CmParam<Decimal>(ref cm, "@TaxRate", decimal.Parse(TxtTaxRate.Text));
            Sm.CmParam<String>(ref cm, "@TaxGrpCode", Sm.GetLue(LueTaxGrpCode));
            Sm.CmParam<Decimal>(ref cm, "@Level", decimal.Parse(TxtLevel.Text));
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
            Sm.CmParam<String>(ref cm, "@AcNo1", TxtAcNo1.Text);
            Sm.CmParam<String>(ref cm, "@AcNo2", TxtAcNo2.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceInd", ChkTaxInvoiceInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@WapuInd", ChkWapuInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ServiceInd", ChkServiceInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;          
        }

        private MySqlCommand SaveTaxDtl(ref iGrid Grd, string TaxCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT INTO TblTaxDtl(TaxCode, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("VALUES(@TaxCode, @EntCode, @UserCode, CurrentDateTime()) ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@TaxCode", TaxCode);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetGrdStr(Grd, Row, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private string FormatNum(string Value, byte FormatType)
        {
            try
            {
                decimal NumValue = 0m;
                Value = (Value.Length == 0) ? "0" : Value.Trim();
                if (!decimal.TryParse(Value, out NumValue))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid numeric value.");
                    NumValue = 0m;
                }
                switch (FormatType)
                {
                    case 0:
                        return String.Format(
                            (Gv.FormatNum0.Length != 0) ?
                                Gv.FormatNum0 : "{0:#,##0.00##}",
                            NumValue);
                    case 1: return String.Format("{0:#,##0}", NumValue);
                    default: return String.Format("{0:###0}", NumValue);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return "0";
        }

        #endregion

       internal string GetSelectedEntity()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd1, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }


        #endregion

        #region Event

        #region Misc Control Event

        private void TxtTaxCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTaxCode);
        }

        private void TxtTaxName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTaxName);
        }

        private void TxtTaxRate_Validated(object sender, EventArgs e)
        {
            TxtTaxRate.EditValue = FormatNum(TxtTaxRate.Text, 0);
        }

        private void LueTaxGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaxGrpCode, new Sm.RefreshLue1(Sl.SetLueTaxGrpCode));
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmTaxDlg2(this));
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmTaxDlg2(this));
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue1(Sl.SetLueTaxCode));
        }

        private void TxtLevel_Validated(object sender, EventArgs e)
        {
            TxtLevel.EditValue = FormatNum(TxtLevel.Text, 0);
        }
        #endregion

        #region Button Event

        private void BtnAcNo1_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmTaxDlg(this, 1));
        }

        private void BtnAcNo2_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmTaxDlg(this, 2));
        }

        #endregion

     
        #endregion

        
    }
}
