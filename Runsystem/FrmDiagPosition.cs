﻿#region Update
/*
    09/11/2018 [TKG] resignee tidak dimunculkan. 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;


using Syncfusion.Windows.Forms.Diagram;
//using System.Drawing.Imaging;
//using Syncfusion.Windows.Forms.Tools;
//using Syncfusion.Windows.Forms.Diagram.Controls;
using System.Drawing.Drawing2D;
//using Syncfusion.Windows.Forms;
//using System.IO;
//using System.Drawing.Printing;
//using Syncfusion.SVG.IO;
using System.Collections;
//using System.Xml;


#endregion

namespace RunSystem
{
    public partial class FrmDiagPosition : RunSystem.FrmBase14
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";

        #endregion

        #region Constructor

        public FrmDiagPosition(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
            ShowData();
        }

        #endregion

        #region Method

        private void ShowData()
        {

            diagram1.BeginUpdate();

            PopulateFields();
            diagram1.Model.LineStyle.LineColor = Color.Transparent;

            HierarchicLayoutManager orgchartLayout = new HierarchicLayoutManager(diagram1.Model, 0, 40, 20);
            orgchartLayout.HorizontalSpacing = 20;
            orgchartLayout.VerticalSpacing = 50;
            orgchartLayout.LeftMargin = 20;
            orgchartLayout.TopMargin = 60;
            diagram1.LayoutManager = orgchartLayout;
            diagram1.LayoutManager.UpdateLayout(null);
            DiagramAppearance();

            diagram1.EndUpdate();
        }


        private void DiagramAppearance()
        {
            this.diagram1.Model.LineStyle.LineColor = Color.LightGray;
            //this.diagram1.HorizontalRuler.BackgroundColor = Color.White;
            //this.diagram1.VerticalRuler.BackgroundColor = Color.White;
            //this.diagram1.View.Grid.GridStyle = GridStyle.Line;
            //this.diagram1.View.Grid.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            //this.diagram1.View.Grid.Color = Color.LightGray;
            //this.diagram1.View.Grid.VerticalSpacing = 15;
            //this.diagram1.View.Grid.HorizontalSpacing = 15;
            //this.diagram1.Model.BackgroundStyle.GradientCenter = 0.5f;
            //this.diagram1.View.HandleRenderer.HandleColor = Color.AliceBlue;
            //this.diagram1.View.HandleRenderer.HandleOutlineColor = Color.SkyBlue;
            //this.diagram1.Model.RenderingStyle.SmoothingMode = SmoothingMode.HighQuality;
            //this.diagram1.Model.BoundaryConstraintsEnabled = false;
            //this.diagram1.View.BackgroundColor = Color.White;
            this.diagram1.View.SelectionList.Clear();
        }


        private void PopulateFields()
        {
            Layouting();

            var l = new List<DiagramPosition>();

            l.Clear();

            string[] TableName = { "DiagramPosition" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.PosCode, B.Posname, B.Parent, A.Empname ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("left Join TblPosition B On A.PosCode = B.PosCode ");
            SQL.AppendLine("Where B.Level>0 ");
            SQL.AppendLine("And (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>Left(CurrentDateTime(), 8))) ");
            SQL.AppendLine("Order By B.Level;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "PosCode",

                         //1-5
                         "PosName",
                         "Parent",
                         "EmpName",
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DiagramPosition()
                        {
                            PosCode = Sm.DrStr(dr, c[0]),

                            PosName = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            EmpName = Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            for (int i = 0; i < l.Count(); i++)
            {
                Syncfusion.Windows.Forms.Diagram.RoundRect rect = GetRoundRect();
                rect.Name = "Node" + l[i].PosCode.ToString();
                string labelText = l[i].EmpName.ToString() + "\r\n" + l[i].PosName.ToString();
                Syncfusion.Windows.Forms.Diagram.Label label = AddLabel(rect, labelText);
                rect.Labels.Add(label);
                diagram1.Model.AppendChild(rect);
                if (!string.IsNullOrEmpty(l[i].Parent.ToString()))
                {
                    Syncfusion.Windows.Forms.Diagram.RoundRect parentNode = diagram1.Model.Nodes.FindNodeByName("Node" + l[i].Parent.ToString()) as Syncfusion.Windows.Forms.Diagram.RoundRect;
                    ConnectNodes(parentNode, rect);
                }

            }
        }

        private static Syncfusion.Windows.Forms.Diagram.Label AddLabel(Syncfusion.Windows.Forms.Diagram.RoundRect rect, string labelText)
        {
            Syncfusion.Windows.Forms.Diagram.Label label = new Syncfusion.Windows.Forms.Diagram.Label(rect, labelText);
            label.FontStyle.Family = "Verdana";
            label.FontStyle.Size = 8;
            label.FontColorStyle.Color = Color.White;
            label.HorizontalAlignment = StringAlignment.Center;
            label.VerticalAlignment = StringAlignment.Center;
            return label;
        }


        private static Syncfusion.Windows.Forms.Diagram.RoundRect GetRoundRect()
        {
            Syncfusion.Windows.Forms.Diagram.RoundRect rect = new Syncfusion.Windows.Forms.Diagram.RoundRect(0, 0, 110, 80, MeasureUnits.Pixel);
            rect.FillStyle.Color = Color.FromArgb(152, 152, 186);
            rect.FillStyle.ForeColor = Color.FromArgb(102, 102, 153);
            rect.LineStyle.LineColor = Color.SlateBlue;
            rect.FillStyle.Type = FillStyleType.LinearGradient;
            return rect;
        }

        private void ConnectNodes(Node parentNode, Node childNode)
        {
            if (parentNode != null && childNode != null)
            {
                OrgLineConnector orgCon = new OrgLineConnector(new PointF(0, 0), new PointF(0, 1));
                orgCon.LineStyle.LineColor = Color.Red;
                orgCon.VerticalDistance = parentNode.BoundingRectangle.Height - 25;
                Decorator decor = orgCon.HeadDecorator;
                decor.DecoratorShape = DecoratorShape.Filled45Arrow;
                decor.Size = new SizeF(10, 10);
                decor.FillStyle.Color = Color.SlateBlue;
                decor.LineStyle.LineColor = Color.SlateBlue;

                parentNode.CentralPort.TryConnect(orgCon.TailEndPoint);
                childNode.CentralPort.TryConnect(orgCon.HeadEndPoint);
                this.diagram1.Model.AppendChild(orgCon);
                this.diagram1.Model.SendToBack(orgCon);

            }
        }

        private void Layouting()
        {
           this.diagram1.UpdateView();
        }     

        #endregion

        #region Event
      
        #endregion
    }

    #region Report Class

    class DiagramPosition
    {
        public string PosCode { set; get; }
        public string PosName { get; set; }
        public string Parent { get; set; }
        public string EmpName { get; set; }
    }
    #endregion
}
