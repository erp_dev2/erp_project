﻿#region Update
/*
    12/04/2019 [HAR] print multiple PP
    19/07/2019 [WED] nominal detail masih belom cocok dengan transaksi
    11/02/2020 [HAR/MAI] nilai total categorydan total akumulais belum sesuai
 */
#endregion

#region Namespace
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;
#endregion

namespace RunSystem
{
    public partial class FrmPrintPP : RunSystem.FrmBase5
    {
        #region Field

        internal string mMenuCode = string.Empty, mSQL = string.Empty, mAccessInd = string.Empty;
        private string mEmpCodeSI = string.Empty, mEmpCodeTaxCollector = string.Empty, mCountPrintOut = string.Empty;
        internal bool mIsDOCtAmtRounded = false;

        internal string
            mDocNo = string.Empty,
            mInformationForItemColour = string.Empty,
            mInformationForItemArtwork = string.Empty,
            mInformationForItemPackaging = string.Empty;
        internal bool mIsPPShowPackagingValue = false;

        #endregion

        #region Constructor

        public FrmPrintPP(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mAccessInd, ref BtnSave, ref BtnExcel);
            BtnSave.Visible = false;
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            GetParameter();
            SetGrd();
            SetSQL();
        }
        #endregion

        #region Standard Method
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select A.DocNo, A.DocDt, "); 
            SQL.AppendLine("Case  ");
            SQL.AppendLine("When A.Status = 'P' then 'planned' ");
            SQL.AppendLine("When A.Status = 'R' Then 'Releaased' ");
            SQL.AppendLine("End As PPStatus, A.Remark ");
            SQL.AppendLine("From TblPPHdr A  ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Document#",
                    "",
                    "Date",
                    "Status",
                    //6
                    "Remark",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 150, 20, 100, 120, 250,
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 } );
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6 });
            Sm.SetGrdProperty(Grd1, false);
        }

        private void GetParameter()
        {
            mInformationForItemColour = Sm.GetParameter("InformationForItemColour");
            mInformationForItemArtwork = Sm.GetParameter("InformationForItemArtwork");
            mInformationForItemPackaging = Sm.GetParameter("InformationForItemPackaging");
            mIsPPShowPackagingValue = Sm.GetParameterBoo("IsPPShowPackagingValue");
        }
        
        #region Button Method
        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            int ceklistDocNo = 0;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if(Sm.GetGrdBool(Grd1, Row, 1))
                {
                    ceklistDocNo = ceklistDocNo + 1;
                }
            }
            if (ceklistDocNo > 0)
            {
                ParPrint();
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "No data to be printed.");
                return;
            }
        }

        #endregion

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0 ";

                var cm = new MySqlCommand();
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtRemark.Text, new string[] { "A.Remark" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order by A.DocNo ",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-3
                            "DocDt", "PPStatus", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);               
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ParPrint()
        {
            if (Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<PPHdr>();
            var ldtl = new List<PPDtl>();
            var ltotal = new List<PPTotal>();
            var ldtl2 = new List<PPDtl2>();
            var ldtl3 = new List<PPDtl3>();

            string[] TableName = { "PPHdr", "PPDtl", "PPTotal", "PPDtl2", "PPDtl3" };

            List<IList> myLists = new List<IList>();

            string FormatDec = (Gv.FormatNum0.Length != 0) ? Gv.FormatNum0 : "{0:#,##0.00##}";

            var cm = new MySqlCommand();
            var cmDtl = new MySqlCommand();
            var cmDtl2 = new MySqlCommand();
            var cmDtl3 = new MySqlCommand();

            string
            Filter = string.Empty;
            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.DocNo=@DocNo" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 2));
                        Sm.CmParam<String>(ref cmDtl, "@DocNo" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 2));
                        Sm.CmParam<String>(ref cmDtl2, "@DocNo" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 2));
                        Sm.CmParam<String>(ref cmDtl3, "@DocNo" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 2));
                        No += 1;
                    }
                }
            }
            Filter = " (" + Filter + ")";


            #region Header

            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper5') As 'CompanyEmail',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt, '%d %M %Y') AS DocDt, ");
            SQL.AppendLine("A.Remark ");
            //SQL.AppendLine(", IFNULL(d.DocDt, 0) AS FirstSODocDt ");
            SQL.AppendLine("FROM TblPPHdr A ");
            SQL.AppendLine("INNER JOIN TblPPDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN TblProductionOrderHdr C ON B.ProductionOrderDocNo = C.DocNo ");
            SQL.AppendLine("LEFT JOIN TblSOHdr D ON C.SODocNo = D.DocNo ");
            SQL.AppendLine("WHERE A.CancelInd = 'N' ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "A.") + ") ; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                //Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyEmail",

                         //6-10
                         "CompanyFax",
                         "CompLocation2",
                         "DocNo",
                         "DocDt",
                         "Remark",
                         
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PPHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyEmail = Sm.DrStr(dr, c[5]),

                            CompanyFax = Sm.DrStr(dr, c[6]),
                            CompLocation2 = Sm.DrStr(dr, c[7]),
                            DocNo = Sm.DrStr(dr, c[8]),
                            DocDt = Sm.DrStr(dr, c[9]),
                            Remark = Sm.DrStr(dr, c[10]),

                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            FormatDec = FormatDec
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail
            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("SELECT DISTINCT DocNo, ItName, Specification, ProdOrRemark, Colour, Artwork, Packaging, ItGrpCode, CtItCode, ItCodeInternal, Qty, PlanningUomCode, Volume, QtyPackagingUnit, PackagingUnitUomCode ");
                SQLDtl.AppendLine("FROM ");
                SQLDtl.AppendLine("( ");
                SQLDtl.AppendLine("  SELECT A.DocNO, A.DNo, D.ItName, D.Specification, C.Remark AS ProdOrRemark, ");
                SQLDtl.AppendLine("  F.OptDesc AS Colour, G.OptDesc AS Artwork, H.OptDesc AS Packaging, ");
                SQLDtl.AppendLine("  D.ItGrpCode, E.CtItCode, D.ItCodeInternal, C.Qty, D.PlanningUomCode, ");
                SQLDtl.AppendLine("  IfNull((I.Volume * I.QtyPackagingUnit), 0) Volume, IfNull(I.QtyPackagingUnit, 0) QtyPackagingUnit, IfNull(I.PackagingUnitUomCode, '') PackagingUnitUomCode ");
                SQLDtl.AppendLine("  FROM TblPPDtl A ");
                SQLDtl.AppendLine("  INNER JOIN TblPPDtl2 B ON A.DocNo = B.DocNo And A.DocType = '1' And (" + Filter.Replace("A.", "A.") + ")  ");
                SQLDtl.AppendLine("  INNER JOIN TblProductionOrderHdr C ON A.ProductionOrderDocNo = C.DocNo ");
                SQLDtl.AppendLine("  INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
                //SQLDtl.AppendLine("  LEFT JOIN TblCustomerItem E ON C.ItCode = E.ItCode ");
                SQLDtl.AppendLine("Left Join ( ");
                SQLDtl.AppendLine("    Select T1.ItCode, Group_Concat(Distinct Concat(T2.CtName, ' [', T1.CtItCode, ']') Order By T2.CtName Separator ', ') As CtItCode ");
                SQLDtl.AppendLine("    From TblCustomerItem T1 ");
                SQLDtl.AppendLine("    Inner Join TblCustomer T2 On T1.CtCode=T2.CtCode ");
                SQLDtl.AppendLine("    Inner Join TblProductionOrderHdr T3 On T1.ItCode=T3.ItCode ");
                SQLDtl.AppendLine("    Inner Join TblPPDtl T4 On T3.DocNo=T4.ProductionOrderDocNo And (" + Filter.Replace("A.", "T4.") + ")  ");
                SQLDtl.AppendLine("    Group By T1.ItCode ");
                SQLDtl.AppendLine(") E On C.ItCode=E.ItCode ");

                if (mInformationForItemColour.Length == 0 || mInformationForItemColour == "")
                    SQLDtl.AppendLine("  LEFT JOIN TblOption F ON D.Information2 = F.OptCode AND F.OptCat = 'ItemInformation2'");
                else
                    SQLDtl.AppendLine("  LEFT JOIN TblOption F ON D." + mInformationForItemColour + " = F.OptCode AND F.OptCat = 'Item" + mInformationForItemColour + "'");

                if (mInformationForItemArtwork.Length == 0 || mInformationForItemArtwork == "")
                    SQLDtl.AppendLine("  LEFT JOIN TblOption G ON D.Information3 = G.OptCode AND G.OptCat = 'ItemInformation3'");
                else
                    SQLDtl.AppendLine("  LEFT JOIN TblOption G ON D." + mInformationForItemArtwork + " = G.OptCode AND G.OptCat = 'Item" + mInformationForItemArtwork + "'");

                if (mInformationForItemPackaging.Length == 0 || mInformationForItemPackaging == "")
                    SQLDtl.AppendLine("  LEFT JOIN TblOption H ON D.Information4 = H.OptCode AND H.OptCat = 'ItemInformation4'");
                else
                    SQLDtl.AppendLine("  LEFT JOIN TblOption H ON D." + mInformationForItemPackaging + " = H.OptCode AND H.OptCat = 'Item" + mInformationForItemPackaging + "'");

                SQLDtl.AppendLine("    Left Join ");
                SQLDtl.AppendLine("    ( ");
                SQLDtl.AppendLine("        Select T1.DocNo, T2.DNo, T4.ItCode, T2.QtyPackagingUnit, T2.PackagingUnitUomCode, T6.Volume ");
                SQLDtl.AppendLine("        From TblSOHdr T1 ");
                SQLDtl.AppendLine("        Inner Join TblSODtl T2 On T1.DocNo = T2.DocNo ");
                SQLDtl.AppendLine("        Inner Join TblCtQtDtl T3 On T1.CtQtDocNo = T3.DocNo And T2.CtQtDNo = T3.DNo ");
                SQLDtl.AppendLine("        Inner Join TblItemPriceDtl T4 On T3.ItemPriceDocNo = T4.DocNo And T3.ItemPriceDNo = T4.DNo ");
                SQLDtl.AppendLine("        Inner Join TblItem T5 On T4.ItCode = T5.ItCode ");
                SQLDtl.AppendLine("        Inner Join TblItemPackagingUnit T6 On T5.ItCode = T6.ItCode And T6.UomCode = T2.PackagingUnitUomCode ");
                SQLDtl.AppendLine("    ) I On C.SODocNo = I.DocNo And C.SODNo = I.DNo And D.ItCode = I.ItCode ");


                SQLDtl.AppendLine("  UNION ALL ");


                SQLDtl.AppendLine("  SELECT A.DocNo, A.DNo, D.ItName, D.Specification, C.Remark AS ProdOrRemark, G.OptDesc AS Colour, ");
                SQLDtl.AppendLine("  H.OptDesc AS Artwork, I.OptDesc AS Packaging, D.ItGrpCode, E.CtItCode, D.ItCodeInternal, ");
                SQLDtl.AppendLine("  F.Qty, D.PlanningUomCode, ");
                SQLDtl.AppendLine("  0 As Volume, 0 As QtyPackagingUnit, '' As PackagingUnitUomCode ");
                SQLDtl.AppendLine("  FROM TblPPDtl A ");
                SQLDtl.AppendLine("  INNER JOIN TblPPDtl2 B ON A.DocNo = B.DocNo AND A.DocType != '1'  And (" + Filter.Replace("A.", "A.") + ") ");
                SQLDtl.AppendLine("  INNER JOIN TblProductionOrderHdr C ON A.ProductionOrderDocNo = C.DocNo ");
                SQLDtl.AppendLine("  LEFT JOIN TblItem D ON C.ItCode = D.ItCode ");
                SQLDtl.AppendLine("  LEFT JOIN TblCustomerItem E ON C.ItCode = E.ItCode ");
                SQLDtl.AppendLine("  LEFT JOIN TblJCHdr F ON A.JCDocNo=F.DocNo ");

                if (mInformationForItemColour.Length == 0 || mInformationForItemColour == "")
                    SQLDtl.AppendLine("  LEFT JOIN TblOption G ON D.Information2 = G.OptCode AND G.OptCat = 'ItemInformation2'");
                else
                    SQLDtl.AppendLine("  LEFT JOIN TblOption G ON D." + mInformationForItemColour + " = G.OptCode AND G.OptCat = 'Item" + mInformationForItemColour + "'");

                if (mInformationForItemArtwork.Length == 0 || mInformationForItemArtwork == "")
                    SQLDtl.AppendLine("  LEFT JOIN TblOption H ON D.Information3 = H.OptCode AND H.OptCat = 'ItemInformation3'");
                else
                    SQLDtl.AppendLine("  LEFT JOIN TblOption H ON D." + mInformationForItemArtwork + " = H.OptCode AND H.OptCat = 'Item" + mInformationForItemArtwork + "'");

                if (mInformationForItemPackaging.Length == 0 || mInformationForItemPackaging == "")
                    SQLDtl.AppendLine("  LEFT JOIN TblOption I ON D.Information4 = I.OptCode AND I.OptCat = 'ItemInformation4'");
                else
                    SQLDtl.AppendLine("  LEFT JOIN TblOption I ON D." + mInformationForItemPackaging + " = I.OptCode AND I.OptCat = 'Item" + mInformationForItemPackaging + "'");

                SQLDtl.AppendLine(")T ");
                SQLDtl.AppendLine("ORDER BY DocNo; ");

                cmDtl.CommandText = SQLDtl.ToString();

                //Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "ItName" ,

                     //1-5
                     "Specification",
                     "ProdOrRemark",
                     "Colour",
                     "Artwork",
                     "Packaging",

                     //6-10
                     "ItGrpCode",
                     "CtItCode",
                     "ItCodeInternal",
                     "Qty",
                     "PlanningUomCode",

                     //11-14
                     "Volume",
                     "QtyPackagingUnit",
                     "PackagingUnitUomCode",
                     "DocNo",
                    });
                if (drDtl.HasRows)
                {
                    int NomorBaris = 0;
                    while (drDtl.Read())
                    {
                        NomorBaris = NomorBaris + 1;
                        ldtl.Add(new PPDtl()
                        {
                            Nomor = NomorBaris,

                            ItName = Sm.DrStr(drDtl, cDtl[0]),
                            Specification = Sm.DrStr(drDtl, cDtl[1]),
                            ProdOrRemark = Sm.DrStr(drDtl, cDtl[2]),
                            Colour = Sm.DrStr(drDtl, cDtl[3]),
                            Artwork = Sm.DrStr(drDtl, cDtl[4]),
                            Packaging = Sm.DrStr(drDtl, cDtl[5]),

                            ItGrpCode = Sm.DrStr(drDtl, cDtl[6]),
                            CtItCode = Sm.DrStr(drDtl, cDtl[7]),
                            ItCodeInternal = Sm.DrStr(drDtl, cDtl[8]),
                            Qty = Sm.DrDec(drDtl, cDtl[9]),
                            PlanningUomCode = Sm.DrStr(drDtl, cDtl[10]),
                            Volume = Sm.DrDec(drDtl, cDtl[11]),
                            QtyPackagingUnit = Sm.DrDec(drDtl, cDtl[12]),
                            PackagingUnitUomCode = Sm.DrStr(drDtl, cDtl[13]),
                            IsPPShowPackagingValue = mIsPPShowPackagingValue ? "Y" : "N",
                            DocNo = Sm.DrStr(drDtl, cDtl[14]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            #endregion

            #region Total

            decimal mTotal = 0m, mCubicTot = 0m, mTotCarton = 0m;
            
            if (ldtl.Count > 0)
            {
                for (int x = 0; x < ldtl.Count; x++)
                {
                    mTotal += ldtl[x].Qty;
                    mCubicTot += ldtl[x].Volume;
                    mTotCarton += ldtl[x].QtyPackagingUnit;
                }
            }

            ltotal.Add(new PPTotal()
            {
                Total = mTotal,
                CubicTot = mCubicTot,
                TotCarton = mTotCarton,
                FormatDec = FormatDec
            });

            myLists.Add(ltotal);

            #endregion

            #region Detail2 (Tgl MTS / SO)


            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("SELECT ");
                SQLDtl2.AppendLine("A.DocNo, DATE_FORMAT(CASE WHEN B.SoDocNo IS NULL THEN C.DocDt ELSE D.DocDt END, '%d %M %Y') AS MTSODocDt ");
                SQLDtl2.AppendLine("FROM TblPPDtl A ");
                SQLDtl2.AppendLine("INNER JOIN TblProductionOrderHdr B ON A.ProductionOrderDocNo = B.DocNo ");
                SQLDtl2.AppendLine("LEFT JOIN TblMakeToStockHdr C ON B.MakeToStockDocNo = C.DocNo ");
                SQLDtl2.AppendLine("LEFT JOIN TblSOHdr D ON B.SODocNo = D.DocNo ");
                SQLDtl2.AppendLine("WHERE (" + Filter.Replace("A.", "A.") + ")  ");
                SQLDtl2.AppendLine("ORDER BY A.DocNo, MTSODocDt ASC LIMIT 1 ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                //Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);

                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "DocNo",
                     "MTSODocDt"
                    });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new PPDtl2()
                        {
                            DocNo = Sm.DrStr(drDtl2, cDtl2[0]),
                            MTSODocDt = Sm.DrStr(drDtl2, cDtl2[1])
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);

            #endregion

            #region Detail3 (group name & qty1)
           
            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;

                SQLDtl3.AppendLine("SELECT T.ItGrpName, SUM(T.Qty) AS SubQty FROM ");
                SQLDtl3.AppendLine("( ");
                SQLDtl3.AppendLine("   SELECT A.DocNo, A.DNo, E.ItGrpName, C.Qty "); //B.Qty1 ");
                SQLDtl3.AppendLine("   FROM TblPPDtl A ");
                SQLDtl3.AppendLine("   INNER JOIN TblPPDtl2 B ON A.DocNo = B.DocNo ");
                SQLDtl3.AppendLine("   INNER JOIN TblProductionOrderHdr C ON A.ProductionOrderDocNo = C.DocNo ");
                SQLDtl3.AppendLine("   LEFT JOIN TblItem D ON C.ItCode = D.ItCode  ");
                SQLDtl3.AppendLine("   LEFT JOIN TblItemGroup E ON D.ItGrpCode = E.ItGrpCode  ");
                SQLDtl3.AppendLine("   WHERE A.DocType = '1' And (" + Filter.Replace("A.", "A.") + ")  ");
                SQLDtl3.AppendLine("   UNION ALL ");
                SQLDtl3.AppendLine("   SELECT A.DocNo, A.DNo, E.ItGrpName, C.Qty ");// B.Qty1 ");
                SQLDtl3.AppendLine("   FROM TblPPDtl A ");
                SQLDtl3.AppendLine("   INNER JOIN TblPPDtl2 B ON A.DocNo = B.DocNo ");
                SQLDtl3.AppendLine("   INNER JOIN TblProductionOrderHdr C ON A.ProductionOrderDocNo = C.DocNo ");
                SQLDtl3.AppendLine("   LEFT JOIN TblItem D ON C.ItCode = D.ItCode ");
                SQLDtl3.AppendLine("   LEFT JOIN TblItemGroup E ON D.ItGrpCode = E.ItGrpCode ");
                SQLDtl3.AppendLine("   WHERE A.DocType != '1' And (" + Filter.Replace("A.", "A.") + ")  ");
                SQLDtl3.AppendLine(")T ");
                SQLDtl3.AppendLine("GROUP BY T.ItGrpName ");
                SQLDtl3.AppendLine("ORDER BY T.ItGrpName ");

                cmDtl3.CommandText = SQLDtl3.ToString();
                //Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);

                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                    {
                     //0
                     "ItGrpName",

                     //1
                     "SubQty",
                    });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        ldtl3.Add(new PPDtl3()
                        {
                            ItGrpName = Sm.DrStr(drDtl3, cDtl3[0]),
                            SubQty = Sm.DrDec(drDtl3, cDtl3[1]),
                            //DocNo = Sm.DrStr(drDtl3, cDtl3[2]),
                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(ldtl3);

            #endregion

            Sm.PrintReport("PPAll", myLists, TableName, false);

            l.Clear(); ldtl.Clear(); ltotal.Clear(); ldtl2.Clear(); ldtl3.Clear();
        }
        #endregion

        #region Grid Methods

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Additional Method

        #endregion

        #endregion

        #region Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmPP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }
        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkRemark_CheckedChanged_1(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Remark#");
        }

        private void TxtRemark_Validated_1(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged_1(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtDocNo_Validated_1(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
  
        #region Class

        #region Report Class

        private class InvoiceHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string DueDt { get; set; }
            public string CurCode { get; set; }
            public decimal TotalTax { get; set; }
            public decimal TotalAmt { get; set; }
            public decimal DownPayment { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string Remark { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxCode3 { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public decimal TaxRate1 { get; set; }
            public decimal TaxRate2 { get; set; }
            public decimal TaxRate3 { get; set; }
            public decimal Amt { get; set; }
            public string Terbilang { get; set; }
            public string CityName { get; set; }
            public string SACityName { get; set; }
            public string NPWP { get; set; }
            public string RemarkSI { get; set; }
            public string ReceiptNo { get; set; }
            public string IsDOCtAmtRounded { get; set; }
        }

        class InvoiceDtl
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public decimal UPriceAfterTax { get; set; }
            public decimal Amt { get; set; }
            public string PriceUomCode { get; set; }
            public string Remark { get; set; }
        }

        class InvoiceDtl2
        {
            public string DocNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal TAmt { get; set; }
        }

        private class Employee
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string EmpPict { get; set; }
        }

        private class EmployeeTaxCollector
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string Mobile { get; set; }
        }

        #endregion

        #region Rate Class

        class Rate
        {
            public decimal Downpayment { get; set; }
            public decimal Amt { get; set; }
            public string CurCode { get; set; }
            public decimal ExcRate { get; set; }
            public string CtCode { get; set; }
        }

        #endregion

        

        #endregion

        #region Report Class

        class PPHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyEmail { get; set; }
            public string CompanyFax { get; set; }
            public string CompLocation2 { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string Remark { get; set; }

            public string PrintBy { get; set; }
            public string FormatDec { get; set; }
        }

        class PPDtl
        {
            public int Nomor { get; set; }

            public string ItName { get; set; }
            public string Specification { get; set; }
            public string ProdOrRemark { get; set; }
            public string Colour { get; set; }
            public string Artwork { get; set; }
            public string Packaging { get; set; }
            public string ItGrpCode { get; set; }
            public string CtItCode { get; set; }
            public string ItCodeInternal { get; set; }
            public decimal Qty { get; set; }
            public string PlanningUomCode { get; set; }
            public decimal Volume { get; set; }
            public decimal QtyPackagingUnit { get; set; }
            public string PackagingUnitUomCode { get; set; }
            public string IsPPShowPackagingValue { get; set; }
            public string DocNo { get; set; }
        }

        class PPTotal
        {
            public decimal Total { get; set; }
            public decimal CubicTot { get; set; }
            public decimal TotCarton { get; set; }
            public string FormatDec { get; set; }
        }

        class PPDtl2
        {
            public string DocNo { get; set; }
            public string MTSODocDt { get; set; }
        }

        class PPDtl3
        {
            public string DocNo { get; set; }
            public string ItGrpName { get; set; }
            public decimal SubQty { get; set; }
        }

        #endregion
    }
}
