﻿#region Update
/* 
   30/11/2020 [DITA/PHT] New Reporting
   22/12/2020 [DITA/PHT] Reporting dibuat yearly
   */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptCompanyBudgetPlanBalanceSheet : RunSystem.FrmBase6
    {
        #region Field

        private string
           mMenuCode = string.Empty,
           mAccessInd = string.Empty,
           mSQL = string.Empty;
        private bool
            mIsAccountingRptUseJournalPeriod = false;

        #endregion

        #region Constructor

        public FrmRptCompanyBudgetPlanBalanceSheet(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueProfitCenterCode(ref LueProfitCenter);
                SetLueCCCode(ref LueCCCode, string.Empty);
                SetGrd();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-4
                    "Account#",
                    "Account Name", 
                    "Total RKAP",
                    "Total Journal",
                    
                },
                new int[] 
                {
                    //0
                    25,

                    //1-4
                    250, 250, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4}, 0);
        }

        override protected void HideInfoInGrd()
        {
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsLueEmpty(LueYr, "Year")
                ) return;
            var Yr = Sm.GetLue(LueYr);
            string SelectedCOA = string.Empty;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<CBP>();
                var l2 = new List<COACBP>();
                var l3 = new List<COAJournal>();
                var l4 = new List<JournalTemp>();

                PrepDataCBP(ref l, Yr);
                if (l.Count > 0)
                {
                    SelectedCOA = GetSelectedCOA(ref l);
                    PrepCOACBP(ref l2, SelectedCOA);
                   
                    //proses total nilai RKAP
                    if (l2.Count > 0)
                    {
                        Process1(ref l, ref l2, Yr);
                    }
                    PrepCOAJournal(ref l3, SelectedCOA, Yr);
                    PrepJournalTemp(ref l4, SelectedCOA, Yr);

                    Process2(ref l3, ref l4);
                    Process3(ref l2, ref l3);
                    Process4(ref l2);
                    Process5(ref l2);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 4 });

                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        public static void SetLueCCCode(ref LookUpEdit Lue, string ProfitCenterCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode As Col1, CCName As Col2 From TblCostCenter Where CBPInd = 'Y' And ProfitCenterCode = @ProfitCenterCode Order By CCName ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            mIsAccountingRptUseJournalPeriod = Sm.GetParameterBoo("IsAccountingRptUseJournalPeriod");

        }

        private void PrepDataCBP(ref List<CBP> l, string Yr )
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.Yr, B.AcNo, SUM(B.Amt) Amt ");
            SQL.AppendLine("FROM TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("INNER JOIN TblCompanyBudgetPlanDtl B ON A.DocNo=B.DocNo AND A.CancelInd != 'Y'  ");
            if (Sm.GetLue(LueProfitCenter).Length != 0)
                SQL.AppendLine("INNER JOIN TblCostCenter C On A.CCCode = C.CCCode And C.ProfitCenterCode=@ProfitCenterCode ");
            if (Sm.GetLue(LueCCCode).Length != 0)
                SQL.AppendLine("And A.CCCode=@CCCode ");
            SQL.AppendLine("WHERE A.Yr= @Yr ");
            SQL.AppendLine("And A.DocType = '3' ");
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenter));
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));


                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                    "Yr", 

                    //1-5
                    "AcNo", 
                    "Amt" ,
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new CBP()
                        {
                            Yr = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                         

                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepCOACBP(ref List<COACBP> l2, string SelectedCOA)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT AcNo, AcDesc, LEVEL, Parent ");
            SQL.AppendLine("FROM TblCOA ");
            SQL.AppendLine("WHERE Find_In_Set(AcNo, @SelectedCOA); ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Level", "Parent" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new COACBP()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Level = Sm.DrDec(dr, c[2]),
                            Parent = Sm.DrStr(dr, c[3]),
                            AmtCBP = 0m,
                            AmtJN = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSelectedCOA(ref List<CBP> l)
        {
            string AcNo = string.Empty;

            foreach (var x in l)
            {
                if (AcNo.Length > 0) AcNo += ",";
                AcNo += x.AcNo;
            }

            return AcNo;
        }

        //Total RKAP
        private void Process1(ref List<CBP> l, ref List<COACBP> l2, string Yr)
        {
            foreach (var x in l)
            {
                foreach (var y in l2.Where(w => w.AcNo == x.AcNo))
                {
                    y.AmtCBP += x.Amt;
                }
            }
           
        }

        private void PrepCOAJournal(ref List<COAJournal> l3, string SelectedCOA, string Yr)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT AcNo, AcType ");
            SQL.AppendLine("FROM TblCOA  ");
            SQL.AppendLine("WHERE AcNo IN( ");
            SQL.AppendLine("	SELECT DISTINCT B.AcNo ");
            SQL.AppendLine("	FROM TblJournalHdr A ");
            SQL.AppendLine("	INNER JOIN TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("	WHERE FIND_IN_SET(B.AcNo, @SelectedCOA) ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("And B.Period Is Not Null ");
                SQL.AppendLine("And Left(B.Period, 6)>=CONCAT(@Yr, '01') ");
                SQL.AppendLine("And Left(B.Period, 6)<=CONCAT(@Yr, '12') ");
            }
            else SQL.AppendLine("	AND LEFT(A.DocDt, 4) = @Yr ");
            SQL.AppendLine(") ");

            SQL.AppendLine("AND ActInd = 'Y' ");
            SQL.AppendLine("AND Parent IS NOT NULL  ");
            SQL.AppendLine("AND AcNo NOT IN ( ");
            SQL.AppendLine("SELECT Parent FROM TblCOA WHERE Parent IS NOT null ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new COAJournal()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcType = Sm.DrStr(dr, c[1]),
                            DAmt = 0m,
                            CAmt = 0m,
                            
                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepJournalTemp(ref List<JournalTemp> l4, string SelectedCOA, string Yr )
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.AcNo, SUM(A.DAmt) DAmt, SUM(A.CAmt) CAmt ");
            SQL.AppendLine("FROM TblJournalDtl A ");
            SQL.AppendLine("INNER JOIN TblJournalHdr B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("WHERE FIND_IN_SET(A.AcNo, @SelectedCOA) ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("And B.Period Is Not Null ");
                SQL.AppendLine("And Left(B.Period, 6)>=CONCAT(@Yr, '01') ");
                SQL.AppendLine("And Left(B.Period, 6)<=CONCAT(@Yr, '12') ");
            }
            else SQL.AppendLine("AND LEFT(B.DocDt, 4) = @Yr ");
            SQL.AppendLine("GROUP BY A.AcNo; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new JournalTemp()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            DAmt = Sm.DrDec(dr, c[1]),
                            CAmt = Sm.DrDec(dr, c[2]),

                        });
                    }
                }
                dr.Close();
            }
        }

        // ceplokin DAmt & CAmt ke list
        private void Process2(ref List<COAJournal> l3, ref List<JournalTemp> l4)
        {
            foreach (var x in l3)
            {
                foreach (var y in l4.Where(w => w.AcNo == x.AcNo))
                {
                    x.DAmt = y.DAmt;
                    x.CAmt = y.CAmt;
                }
            }
        }

        // Total Journal
        private void Process3(ref List<COACBP> l2, ref List<COAJournal> l3)
        {
            foreach (var x in l3)
            {
                foreach (var y in l2.Where(w => w.AcNo == x.AcNo))
                {
                    y.AmtJN += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                }
            }
        }

        // proses masukin amount journal ke parent dari si amount bontotnya
        private void Process4(ref List<COACBP> l2)
        {
            int MaxLvlCOA = Int32.Parse(Sm.GetValue("SELECT MAX(LEVEL) FROM TblCOA"));

            for (int i = MaxLvlCOA; i > 0; i--)
            {
                foreach (var x in l2.Where(w => w.Level == i).OrderBy(o => o.Parent))
                {
                    foreach (var y in l2.Where(w => w.AcNo == x.Parent))
                    {
                        y.AmtJN += x.AmtJN;
                    }
                }
            }
        }

        // ceplokin data ke grid
        private void Process5(ref List<COACBP> l2)
        {
            Sm.ClearGrd(Grd1, false);
            int Row = 0;
            Grd1.BeginUpdate();

            foreach (var x in l2)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = Row + 1;
                Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = x.AcNo;
                Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = x.AcDesc;
                Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = Sm.FormatNum(x.AmtCBP, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = Sm.FormatNum(x.AmtJN, 0);


                Row += 1;
            }

            Grd1.EndUpdate();
        }


        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueProfitCenter_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProfitCenter, new Sm.RefreshLue1(Sl.SetLueProfitCenterCode));
            SetLueCCCode(ref LueCCCode, Sm.GetLue(LueProfitCenter));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(SetLueCCCode), Sm.GetLue(LueProfitCenter));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkProfitCenter_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Profit Center");
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");
        }
        #endregion

        #endregion

        #region Class

        private class CBP
        {
            public string Yr { get; set; }
            public string AcNo { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt01 { get; set; }
            public decimal Amt02 { get; set; }
            public decimal Amt03 { get; set; }
            public decimal Amt04 { get; set; }
            public decimal Amt05 { get; set; }
            public decimal Amt06 { get; set; }
            public decimal Amt07 { get; set; }
            public decimal Amt08 { get; set; }
            public decimal Amt09 { get; set; }
            public decimal Amt10 { get; set; }
            public decimal Amt11 { get; set; }
            public decimal Amt12 { get; set; }

        }

        private class COACBP
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string Parent { get; set; }
            public decimal Level { get; set; }
            public decimal AmtCBP { get; set; }
            public decimal AmtJN { get; set; }
        }

        private class COAJournal
        {
            public string AcNo { get; set; }
            public string AcType { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class JournalTemp
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        #endregion

        
    }
}
