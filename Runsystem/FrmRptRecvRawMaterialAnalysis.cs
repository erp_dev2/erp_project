﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptRecvRawMaterialAnalysis : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptRecvRawMaterialAnalysis(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetLueType(ref LueType);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, Left(A.CreateDt, 8) As DocDt, IfNull(C.DocType, '') As DocType, D.VdName, ");
            SQL.AppendLine("E.CityName, A.DrvName, A.LicenceNo, F.TTName, ");
            SQL.AppendLine("A.DocDateBf, A.DocTimeBf, A.DocDateAf, A.DocTimeAf, A.WeightBf, A.WeightAf, A.Netto, ");
            SQL.AppendLine("Case When Exists(Select 1 From TblRecvRawMaterialDtl Where DocNo=C.DocNo And EmpType='3' Limit 1) "); 
            SQL.AppendLine("Then 'Y' Else 'N' End As BongkarDalam, ");
            SQL.AppendLine("Case When Exists(Select 1 From TblRecvRawMaterialDtl Where DocNo=C.DocNo And EmpType='4' Limit 1) ");
            SQL.AppendLine("Then 'Y' Else 'N' End As BongkarLuar, ");
            SQL.AppendLine("C.UnloadStartDt, C.UnloadStartTm, C.UnloadEndDt, C.UnloadEndTm, ");
            SQL.AppendLine("H.DocNo As PurchaseInvoiceRawMaterialDocNo, ");
            SQL.AppendLine("J.DocNo As VoucherDocNo, ");
            SQL.AppendLine("K.OptDesc As PaymentType, ");
            SQL.AppendLine("H.Qty1, H.Qty2, H.Amt, H.TransportCost, H.Tax, H.RoundingValue, ");
            SQL.AppendLine("C.DocNo As RecvRawMaterialDocNo, C.Remark ");
            SQL.AppendLine("From TblLoadingQueue A ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr B On A.DocNo=B.QueueNo And B.CancelInd='N' ");
            SQL.AppendLine("Left Join TblRecvRawMaterialHdr C On B.DocNo=C.LegalDocVerifyDocNo And C.CancelInd='N' ");
            SQL.AppendLine("Left Join TblVendor D On B.VdCode=D.VdCode ");
            SQL.AppendLine("Left Join TblCity E On D.CityCode=E.CityCode ");
            SQL.AppendLine("Inner Join TblTransportType F On A.TTCode=F.TTCode ");
            SQL.AppendLine("Left Join TblRawMaterialVerify G On B.DocNo=G.LegalDocVerifyDocNo And G.CancelInd='N' ");
            SQL.AppendLine("Left Join TblPurchaseInvoiceRawMaterialHdr H On G.DocNo=H.RawMaterialVerifyDocNo And H.CancelInd='N' ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr I On H.VoucherRequestDocNo=I.DocNo And I.CancelInd='N' ");
            SQL.AppendLine("Left Join TblVoucherHdr J On I.VoucherDocNo=J.DocNo And J.CancelInd='N' ");
            SQL.AppendLine("Left Join TblOption K On J.PaymentType=K.OptCode And K.OptCat='VoucherPaymentType' ");
            SQL.AppendLine("Where Left(A.CreateDt, 8) Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void GeneratePersonInfo()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var l = new List<Person>();

            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length != 0 && Sm.GetGrdStr(Grd1, r, 3).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.QueueNo=@QueueNo0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@QueueNo0" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                        
                    }
                }
            }
            if (Filter.Length !=0) 
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 1=0 ";

            SQL.AppendLine("Select A.QueueNo, B.DocNo, C.EmpType, ");
            SQL.AppendLine("Group_Concat(Distinct D.EmpName Separator ', ') As EmpName ");
            SQL.AppendLine("From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("Inner Join TblRecvRawMaterialHdr B On A.DocNo=B.LegalDocVerifyDocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblRecvRawMaterialDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee D On C.EmpCode=D.EmpCode ");
            SQL.AppendLine("Where A.CancelInd='N' " + Filter);
            SQL.AppendLine("Group By A.QueueNo, B.DocNo, C.EmpType;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "QueueNo", "DocNo", "EmpType", "EmpName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Person()
                        {
                            QueueNo = Sm.DrStr(dr, c[0]),
                            DocNo = Sm.DrStr(dr, c[1]),
                            EmpType = Sm.DrStr(dr, c[2]),
                            EmpName = Sm.DrStr(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
            
            var QueueNo = string.Empty;
            var DocNo = string.Empty;
            var EmpType = string.Empty;
            int SeqNo = 0;
            int R = 0;

            Grd1.BeginUpdate();
            foreach (var x in l.OrderBy(x => x.QueueNo).ThenBy(x => x.DocNo).ThenBy(x => x.EmpType))
            {
                if (!(Sm.CompareStr(x.QueueNo, QueueNo) && Sm.CompareStr(x.DocNo, DocNo) && Sm.CompareStr(x.EmpType, EmpType)))
                {
                    SeqNo = 0;
                }
                SeqNo += 1;

                if (!(Sm.CompareStr(x.QueueNo, QueueNo) && Sm.CompareStr(x.DocNo, DocNo)))
                {
                    R = 0;
                }

                for (int Row = R; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.CompareStr(x.QueueNo, Sm.GetGrdStr(Grd1, Row, 1)) && Sm.CompareStr(x.DocNo, Sm.GetGrdStr(Grd1, Row, 43))) 
                    {
                        R = Row;
                        if (SeqNo <= 3)
                        {
                            if (x.EmpType == "1")
                                Grd1.Cells[Row, 16 + SeqNo].Value = x.EmpName;

                            if (x.EmpType == "2")
                                Grd1.Cells[Row, 19 + SeqNo].Value = x.EmpName;

                            if (x.EmpType == "3")
                                Grd1.Cells[Row, 22 + SeqNo].Value = x.EmpName;

                            if (x.EmpType == "4")
                                Grd1.Cells[Row, 25 + SeqNo].Value = x.EmpName;
                        }
                        else
                        {
                            if (x.EmpType == "1")
                                Grd1.Cells[Row, 19].Value += (", " + x.EmpName);

                            if (x.EmpType == "2")
                                Grd1.Cells[Row, 22].Value += (", " + x.EmpName);

                            if (x.EmpType == "3")
                                Grd1.Cells[Row, 25].Value += (", " + x.EmpName);

                            if (x.EmpType == "4")
                                Grd1.Cells[Row, 28].Value += (", " + x.EmpName);
                        }
                        break;
                    }
                }
                QueueNo = x.QueueNo;
                DocNo = x.DocNo;
                EmpType = x.EmpType;
            }
            Grd1.EndUpdate();
            l.Clear();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 45;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Dokumen#", 
                        "Tanggal",
                        "Jenis",
                        "Vendor",
                        "Kota",

                        //6-10
                        "Sopir",
                        "Plat#",
                        "Transport",
                        "Tanggal"+Environment.NewLine+"Timbang 1",
                        "Waktu"+Environment.NewLine+"Timbang 1",

                        //11-15
                        "Tanggal"+Environment.NewLine+"Timbang 2",
                        "Waktu"+Environment.NewLine+"Timbang 2",
                        "Berat"+Environment.NewLine+"Timbang 1",
                        "Berat"+Environment.NewLine+"Timbang 2",
                        "Berat"+Environment.NewLine+"Bersih",
                        
                        //16-20
                        "Jenis"+Environment.NewLine+"Bongkar",
                        "Grader 1",
                        "Grader 2",
                        "Grader 3-5",
                        "Asisten"+Environment.NewLine+"Grader 1",
                        
                        //21-25
                        "Asisten"+Environment.NewLine+"Grader 2",
                        "Asisten"+Environment.NewLine+"Grader 3-5",
                        "Bongkar"+Environment.NewLine+"Dalam 1",
                        "Bongkar"+Environment.NewLine+"Dalam 2",
                        "Bongkar"+Environment.NewLine+"Dalam 3-5",
                        
                        //26-30
                        "Bongkar"+Environment.NewLine+"Luar 1",
                        "Bongkar"+Environment.NewLine+"Luar 2",
                        "Bongkar"+Environment.NewLine+"Luar 3-5",
                        "Tanggal Mulai"+Environment.NewLine+"Bongkar",
                        "Waktu Mulai"+Environment.NewLine+"Bongkar",
                        
                        //31-35
                        "Tanggal Selesai"+Environment.NewLine+"Bongkar",
                        "Waktu Selesai"+Environment.NewLine+"Bongkar",
                        "Purchase"+Environment.NewLine+"Invoice#",
                        "Voucher#",
                        "Jenis"+Environment.NewLine+"Pembayaran",
                        
                        //36-40
                        "Jumlah"+Environment.NewLine+"(Batang)",
                        "Volume"+Environment.NewLine+"(Kubik)",
                        "Total"+Environment.NewLine+"(IDR)",
                        "Biaya"+Environment.NewLine+"Transportasi",
                        "Pajak",

                        //41-44
                        "Rounding",
                        "Grand Total",
                        "Received#",
                        "Keterangan"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 80, 150, 150, 
                        
                        //6-10
                        150, 100, 130, 100, 100,   

                        //11-15
                        100, 100, 100, 100, 100,

                        //16-20
                        100, 150, 150, 150, 150, 

                        //21-25
                        150, 150, 150, 150, 150, 

                        //26-30
                        150, 150, 150, 100, 100, 

                        //31-35
                        100, 100, 150, 150, 150, 
                        
                        //36-40
                        130, 130, 130, 130, 130,
                        
                        //41-44
                        130, 150, 150, 250
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 13, 14, 15, 36, 37, 38, 39, 40, 41, 42 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 9, 11, 29, 31 });
            Sm.GrdFormatTime(Grd1, new int[] { 10, 12, 30, 32 });
            Sm.GrdColInvisible(Grd1, new int[] { 9, 11, 29, 31 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 11, 29, 31, 36 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                bool IsDataExisted = false;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "C.DocType", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt, A.DocNo;",
                        new string[]
                        {
                
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "DocType", "VdName", "CityName", "DrvName", 
                            
                            //6-10
                            "LicenceNo", "TTName", "DocDateBf", "DocTimeBf", "DocDateAf",

                            //11-15
                            "DocTimeAf", "WeightBf", "WeightAf", "Netto", "BongkarDalam", 

                            //16-20
                            "BongkarLuar", "UnloadStartDt", "UnloadStartTm", "UnloadEndDt", "UnloadEndTm", 
                            
                            //21-25
                            "PurchaseInvoiceRawMaterialDocNo", "VoucherDocNo", "PaymentType",  "Qty1", "Qty2", 

                            //26-30
                            "Amt", "TransportCost", "Tax", "RoundingValue", "RecvRawMaterialDocNo", 
                            
                            //31
                            "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            IsDataExisted = true;
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            switch (dr.GetString(c[2]))
                            {
                                case "1" :
                                    Grd.Cells[Row, 3].Value = "Log";
                                    break;
                                case "2":
                                    Grd.Cells[Row, 3].Value = "Balok";
                                    break;      
                            }
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            if (dr.GetString(c[15]) == "Y")
                                Grd.Cells[Row, 16].Value = "Dalam";
                            else
                            {
                                if (dr.GetString(c[16]) == "Y")
                                    Grd.Cells[Row, 16].Value = "Luar";
                            }
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 29, 17);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 30, 18);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 31, 19);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 32, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 25);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 26);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 39, 27);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 40, 28);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 41, 29);
                            Grd.Cells[Row, 42].Value = Sm.GetGrdDec(Grd, Row, 38) - Sm.GetGrdDec(Grd, Row, 39) - Sm.GetGrdDec(Grd, Row, 40) + Sm.GetGrdDec(Grd, Row, 41);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 44, 31);
                        }, true, false, false, false
                    );
                if (IsDataExisted) GeneratePersonInfo();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 13, 14, 15, 36, 37, 38, 39, 40, 41, 42 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetLueType(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Col1, 'Log' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Col1, 'Balok' As Col2 ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        #endregion

        #endregion

        #region Class

        private class Person
        {
            public string QueueNo { get; set; }
            public string DocNo { get; set; }
            public string EmpType { get; set; }
            public string EmpName { get; set; }
        }

        #endregion
    }
}
