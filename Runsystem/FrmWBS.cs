﻿#region Update
/*
    25/09/2019 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWBS : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        private string mWBSFormula = string.Empty;
        private bool
            mIsWBSBobotColumnAutoCompute = false,
            mIsProjectUseWBSTransaction = false;
        internal bool mIsFilterBySite = false;
        internal FrmWBSFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmWBS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmWBS");

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                Sl.SetLueProjectStageCode(ref LueStageCode);
                Sl.SetLueProjectTaskCode(ref LueTaskCode);
                LueStageCode.Visible = false;
                LueTaskCode.Visible = false;
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "StageCode", 
                    //1-5
                    "Stage", "TaskCode", "Task", "Plan Start Date", "Plan End Date",
                    //6-10
                    "Price", "Bobot", "Percentage", "Remark", "Estimated Amount",
                },
                new int[] 
                {
                    //0
                    0,
                    //1-5
                    200, 0, 200, 120, 120, 
                    //6-10
                    150, 150, 150, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 10 }, 8);
            Sm.GrdFormatDate(Grd1, new int[] { 4, 5 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 8, 10 });
            if (mIsWBSBobotColumnAutoCompute)
                Sm.GrdColReadOnly(Grd1, new int[] { 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, MeeCancelReason, DteDocDt, MeeRemark, TxtPRJIDocNo, TxtAmt,
                        TxtBOQDocNo, LueStageCode, LueTaskCode, DtePlanEndDt, DtePlanStartDt
                    }, true);
                    Grd1.ReadOnly = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnBOQDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, LueStageCode, LueTaskCode, DtePlanStartDt, DtePlanEndDt
                    }, false);
                    Grd1.ReadOnly = false;
                    BtnBOQDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblWBSHdr ");
                    SQL.AppendLine("Where DocNo = @Param ");
                    SQL.AppendLine("And CancelInd = 'Y'; ");

                    if (!Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            MeeCancelReason, LueStageCode, LueTaskCode, DtePlanEndDt, DtePlanStartDt 
                        }, false);
                        Grd1.ReadOnly = false;
                        BtnBOQDocNo.Enabled = false;
                        ChkCancelInd.Properties.ReadOnly = false;
                        ChkCancelInd.Focus();
                    }
                    else
                    {
                        Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                        TxtDocNo.Focus();
                        return;
                    }
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, MeeCancelReason, DteDocDt, MeeRemark, TxtPRJIDocNo, TxtBOQDocNo, LueStageCode, LueTaskCode, DtePlanStartDt, DtePlanEndDt
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 7, 8, 10 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWBSFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                ChkCancelInd.Checked = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
                {
                    if (Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length > 0)
                    {
                        decimal mEstimatedAmt = 0m;
                        if (mWBSFormula == "1") mEstimatedAmt = Sm.GetGrdDec(Grd1, e.RowIndex, 6);

                        Grd1.Cells[e.RowIndex, 10].Value = mEstimatedAmt;
                        ComputeAmt();
                    }
                }
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 1, 3, 4, 5, 6 }, e.ColIndex))
                {
                    //e.DoDefault = false;
                    if (e.ColIndex == 1) LueRequestEdit(Grd1, LueStageCode, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 3) LueRequestEdit(Grd1, LueTaskCode, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 4) Sm.DteRequestEdit(Grd1, DtePlanStartDt, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 5) Sm.DteRequestEdit(Grd1, DtePlanEndDt, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7, 8 });
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmt();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "WBS", "TblWBSHdr");

            var cml = new List<MySqlCommand>();

            ComputeBobot();
            ComputeBobotPercentage();

            cml.Add(SaveWBSHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveWBSDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtBOQDocNo, "BOQ#", false) ||
                IsBOQAlreadyCancelled() ||
                IsBOQAlreadyProceed() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsBOQAlreadyProceed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblWBSHdr ");
            SQL.AppendLine("Where BOQDocNo = @Param ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtBOQDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This BOQ has been created in WBS#" + Sm.GetValue(SQL.ToString(), TxtBOQDocNo.Text));
                TxtBOQDocNo.Focus();
                return true;
            }

            return false;
        }

        private bool IsBOQAlreadyCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblBOQHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And (ActInd = 'N' Or Status = 'C'); ");

            if (Sm.IsDataExist(SQL.ToString(), TxtBOQDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This BOQ is already cancelled.");
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Stage is empty.")) { return true; }
                if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Task is empty.")) { return true; }
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Plan Start Date is empty.")) {  return true; }
                if (Sm.IsGrdValueEmpty(Grd1, Row, 5, false, "Plan End Date is empty.")) {  return true; }
                if (Sm.IsGrdValueEmpty(Grd1, Row, 6, true, "Price could not be empty or zero.")) {  return true; }
            }

            for (int r1 = 0; r1 < Grd1.Rows.Count - 1; r1++)
            {
                for (int r2 = (r1 + 1); r2 < Grd1.Rows.Count; r2++)
                {
                    if ((Sm.GetGrdStr(Grd1, r1, 0) == Sm.GetGrdStr(Grd1, r2, 0))
                        &&
                        (Sm.GetGrdStr(Grd1, r1, 2) == Sm.GetGrdStr(Grd1, r2, 2))
                        )
                    {
                        Sm.StdMsg(mMsgType.Warning, "Duplicate task found : " + Sm.GetGrdStr(Grd1, r2, 3));
                        Sm.FocusGrd(Grd1, r2, 3);
                        return true;
                    }
                }
            }

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.CompareDtTm(Sm.Left(Sm.GetGrdDate(Grd1, Row, 4), 8), Sm.Left(Sm.GetGrdDate(Grd1, Row, 5), 8)) > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Plan end date could not be earlier than plan start date.");
                    Sm.FocusGrd(Grd1, Row, 5);
                    return true;
                }
            }

            return false;
        }

        private MySqlCommand SaveWBSHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWBSHdr(DocNo, DocDt, CancelInd, BOQDocNo, Remark, Amt, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @BOQDocNo, @Remark, @Amt, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtBOQDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveWBSDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWBSDtl(DocNo, DNo, StageCode, TaskCode, PlanStartDt, PlanEndDt, ");
            SQL.AppendLine("Amt, Bobot, BobotPercentage, EstimatedAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @StageCode, @TaskCode, @PlanStartDt, @PlanEndDt, ");
            SQL.AppendLine("@Amt, @Bobot, @BobotPercentage, @EstimatedAmt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@StageCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@TaskCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParamDt(ref cm, "@PlanStartDt", Sm.GetGrdDate(Grd1, Row, 4));
            Sm.CmParamDt(ref cm, "@PlanEndDt", Sm.GetGrdDate(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Bobot", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@BobotPercentage", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@EstimatedAmt", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            ComputeBobot();
            ComputeBobotPercentage();

            cml.Add(EditWBSHdr());

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveWBSDtl(TxtDocNo.Text, Row));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtBOQDocNo, "BOQ#", false) ||
                IsDataAlreadyCancelled() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                (!ChkCancelInd.Checked && IsBOQAlreadyCancelled()) ||
                IsWBSAlreadyProceedToPRJI();
        }

        private bool IsWBSAlreadyProceedToPRJI()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select PRJIDocNo ");
            SQL.AppendLine("From TblWBSHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And PRJIDocNo Is Not Null; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This WBS is already proceed to PRJI#" + Sm.GetValue(SQL.ToString(), TxtDocNo.Text));
                TxtDocNo.Focus();
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblWBSHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And CancelInd = 'Y'; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditWBSHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblWBSHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason = @CancelReason, Amt = @Amt, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            SQL.AppendLine("Delete From TblWBSDtl Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowWBSHdr(DocNo);
                ShowWBSDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWBSHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.PRJIDocNo, ");
            SQL.AppendLine("A.BOQDocNo, A.Remark, A.Amt ");
            SQL.AppendLine("From TblWBSHdr A ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "PRJIDocNo", "BOQDocNo", 
                    
                    //6-7
                    "Remark", "Amt"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    TxtPRJIDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[5]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                }, true
            );
        }

        private void ShowWBSDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.StageCode, B.StageName, A.TaskCode, C.TaskName, ");
            SQL.AppendLine("A.PlanStartDt, A.PlanEndDt, A.Bobot, A.BobotPercentage, A.Remark, A.Amt, ");
            SQL.AppendLine("A.EstimatedAmt ");
            SQL.AppendLine("From TblWBSDtl A ");
            SQL.AppendLine("Inner Join TblProjectStage B On A.StageCode = B.StageCode And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblProjectTask C On A.TaskCode = C.TaskCode ");
            SQL.AppendLine("Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "StageCode", 
                    //1-5
                    "StageName", "TaskCode", "TaskName", "PlanStartDt", "PlanEndDt", 
                    //6-10
                    "Amt", "Bobot", "BobotPercentage", "Remark", "EstimatedAmt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7, 8, 10 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void ComputeBobotPercentage()
        {
            decimal mTotalBobot = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    mTotalBobot += Sm.GetGrdDec(Grd1, Row, 7);

            if (mTotalBobot >= 99.5m) mTotalBobot = 100m;
            
            if (mTotalBobot != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    {
                        Grd1.Cells[Row, 8].Value = Sm.Round(((Sm.GetGrdDec(Grd1, Row, 7) / mTotalBobot) * 100m), 2);
                    }
                }
            }
        }

        private void ComputeBobot()
        {
            decimal mTotalPrice = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    mTotalPrice += Sm.GetGrdDec(Grd1, Row, 6);

            if (mIsWBSBobotColumnAutoCompute)
            {
                if (mTotalPrice != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                        {
                            Grd1.Cells[Row, 7].Value = Sm.Round(((Sm.GetGrdDec(Grd1, Row, 6) / mTotalPrice) * 100m), 2);
                        }
                    }
                }
            }
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsWBSBobotColumnAutoCompute = Sm.GetParameterBoo("IsWBSBobotColumnAutoCompute");
            mIsProjectUseWBSTransaction = Sm.GetParameterBoo("IsProjectUseWBSTransaction");
            mWBSFormula = Sm.GetParameter("WBSFormula");

            if (mWBSFormula.Length == 0) mWBSFormula = "1";
        }

        internal void ComputeAmt()
        {
            decimal mAmt = 0m;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    mAmt += Sm.GetGrdDec(Grd1, i, 10);
                }
            }

            TxtAmt.EditValue = Sm.FormatNum(mAmt, 0);
        }

        #endregion

        #endregion

        #region Events

        #region Button Clicks

        private void BtnBOQDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmWBSDlg(this, TxtBOQDocNo.Text));
            }
        }

        private void BtnBOQDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtBOQDocNo, "BOQ#", false))
            {
                var f = new FrmBOQ(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmBOQ");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtBOQDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueStageCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueStageCode, new Sm.RefreshLue1(Sl.SetLueProjectStageCode));
            }
        }

        private void LueStageCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueStageCode_Leave(object sender, EventArgs e)
        {
            if (LueStageCode.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueStageCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 0].Value =
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueStageCode);
                    Grd1.Cells[fCell.RowIndex, 1].Value = LueStageCode.GetColumnValue("Col2");
                }
                LueStageCode.Visible = false;
            }
        }

        private void LueTaskCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaskCode, new Sm.RefreshLue1(Sl.SetLueProjectTaskCode));
            }
        }

        private void LueTaskCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueTaskCode_Leave(object sender, EventArgs e)
        {
            if (LueTaskCode.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (Sm.GetLue(LueTaskCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 2].Value =
                    Grd1.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueTaskCode);
                    Grd1.Cells[fCell.RowIndex, 3].Value = LueTaskCode.GetColumnValue("Col2");
                }
                LueTaskCode.Visible = false;
            }
        }

        private void DtePlanStartDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DtePlanStartDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DtePlanStartDt, ref fCell, ref fAccept);
        }

        private void DtePlanEndDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DtePlanEndDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DtePlanEndDt, ref fCell, ref fAccept);
        }

        #endregion

        #endregion
    }
}
