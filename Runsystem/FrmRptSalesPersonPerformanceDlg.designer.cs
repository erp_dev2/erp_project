﻿namespace RunSystem
{
    partial class FrmRptSalesPersonPerformanceDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.TxtSalesPerson = new DevExpress.XtraEditors.TextEdit();
            this.TxtYear = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtMth = new DevExpress.XtraEditors.TextEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnExcell = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesPerson.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtYear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 82);
            this.panel3.Size = new System.Drawing.Size(842, 391);
            // 
            // Grd1
            // 
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(842, 391);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtMth);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtYear);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtSalesPerson);
            this.panel2.Size = new System.Drawing.Size(842, 82);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 14);
            this.label6.TabIndex = 24;
            this.label6.Text = "Sales Person";
            // 
            // TxtSalesPerson
            // 
            this.TxtSalesPerson.EnterMoveNextControl = true;
            this.TxtSalesPerson.Location = new System.Drawing.Point(87, 10);
            this.TxtSalesPerson.Name = "TxtSalesPerson";
            this.TxtSalesPerson.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSalesPerson.Properties.Appearance.Options.UseFont = true;
            this.TxtSalesPerson.Properties.MaxLength = 30;
            this.TxtSalesPerson.Size = new System.Drawing.Size(323, 20);
            this.TxtSalesPerson.TabIndex = 25;
            // 
            // TxtYear
            // 
            this.TxtYear.EnterMoveNextControl = true;
            this.TxtYear.Location = new System.Drawing.Point(87, 32);
            this.TxtYear.Name = "TxtYear";
            this.TxtYear.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtYear.Properties.Appearance.Options.UseFont = true;
            this.TxtYear.Properties.MaxLength = 30;
            this.TxtYear.Size = new System.Drawing.Size(136, 20);
            this.TxtYear.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(51, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 14);
            this.label1.TabIndex = 26;
            this.label1.Text = "Year";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(41, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 14);
            this.label2.TabIndex = 28;
            this.label2.Text = "Month";
            // 
            // TxtMth
            // 
            this.TxtMth.EnterMoveNextControl = true;
            this.TxtMth.Location = new System.Drawing.Point(87, 54);
            this.TxtMth.Name = "TxtMth";
            this.TxtMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMth.Properties.Appearance.Options.UseFont = true;
            this.TxtMth.Properties.MaxLength = 30;
            this.TxtMth.Size = new System.Drawing.Size(136, 20);
            this.TxtMth.TabIndex = 29;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnExcell);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(728, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(114, 82);
            this.panel1.TabIndex = 30;
            // 
            // BtnExcell
            // 
            this.BtnExcell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.BtnExcell.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnExcell.Location = new System.Drawing.Point(0, 0);
            this.BtnExcell.Name = "BtnExcell";
            this.BtnExcell.Size = new System.Drawing.Size(114, 30);
            this.BtnExcell.TabIndex = 0;
            this.BtnExcell.Text = "Convert to Excell";
            this.BtnExcell.UseVisualStyleBackColor = false;
            this.BtnExcell.Click += new System.EventHandler(this.BtnExcell_Click);
            // 
            // FrmRptSalesPersonPerformanceDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmRptSalesPersonPerformanceDlg";
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesPerson.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtYear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.TextEdit TxtSalesPerson;
        private System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.TextEdit TxtMth;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.TextEdit TxtYear;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BtnExcell;
    }
}