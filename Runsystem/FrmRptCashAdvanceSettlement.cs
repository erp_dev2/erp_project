﻿#region Update
/*
    12/01/2020 [DITA/PHT] new reporting
    18/02/2021 [VIN/PHT] tambah informasi item, quantity, dan dan rate berdasarkan param parameter IsCASUseItemRate
    22/03/2021 [TKG/PHT] tambah validasi dan filter multi profit center
    21/01/2022 [TRI/PHT] mengurutkan profitcenter berdasar profitcentercode
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmRptCashAdvanceSettlement : RunSystem.FrmBase6
    {
        #region Field

        internal string
          mAccessInd = string.Empty,
          mMenuCode = string.Empty;
        private string mSQL = string.Empty;
        internal bool
            mIsCASFromTRProcessAllAmount = false,
            mIsCASUseItemRate = false;
        private bool
            mIsRptCashAdvanceSettlementUseProfitCenter = false,
            mIsAllProfitCenterSelected = false;
        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFormatFTPClient = string.Empty;

        private byte[] downloadedData;
        private List<String> mlProfitCenter = null;

        #endregion

        #region Constructor

        public FrmRptCashAdvanceSettlement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                if (mIsRptCashAdvanceSettlementUseProfitCenter)
                {
                    panel4.Visible = true;
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",
                        "Cost Center",
                        "Cost Category",
                        "Item's Code",
                        

                        //6-10
                        "Item's Name",
                        "Quantity",
                        "Rate",
                        "Amount" +Environment.NewLine +"(Cash Advance)",
                        "Amount" +Environment.NewLine +"(Cost)",

                        //11-13
                        "Amount" +Environment.NewLine +"(Outstanding)",
                        "File Name",
                        "",
                      

                    },
                    new int[] 
                    {
                        //0
                        25,

                        //1-5
                        180, 120, 150, 150, 100, 

                        //6-10
                        200, 100, 100, 100, 100, 
                        
                        //6-13
                        100, 200, 20
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9, 10, 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColButton(Grd1, new int[] { 13 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            if (!mIsCASUseItemRate) Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, false);
   
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                var cm = new MySqlCommand();

                SetProfitCenter();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });

                var SQL = new StringBuilder();

                if (mIsRptCashAdvanceSettlementUseProfitCenter)
                {
                    SQL.AppendLine("Select A.DocDt, A.DocNo, A.CCCode, C.CCName, B.CCtCode, D.CCtName, B.Amt CostAmt, ");
                    SQL.AppendLine("B.ItCode, G.ItName, B.Qty, B.Rate, ");
                    SQL.AppendLine("If(E.DocType='61', E.Amt, E.Amt) As CashAdvanceAmt, (E.Amt-B.Amt) OutstandingAmt, ");
                    SQL.AppendLine("B.FileName ");
                    SQL.AppendLine("From TblCashAdvanceSettlementHdr A ");
                    SQL.AppendLine("Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("Inner Join TblCostCenter C On A.CCCode = C.CCCode ");
                    SQL.AppendLine("Left Join TblCostCategory D On B.CCtCode = D.CCtCode ");
                    SQL.AppendLine("Left Join TblVoucherHdr E On B.VoucherDocNo = E.DocNo ");
                    SQL.AppendLine("Left Join TblItem G On G.ItCode = B.ItCode ");
                    SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
                    SQL.AppendLine("And A.CancelInd='N' ");
                    SQL.AppendLine("And A.Status='A' ");
                    SQL.AppendLine("And A.DocStatus='F' ");
                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter2 = string.Empty;
                        int i = 0;

                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        ) ");
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter2.Length > 0) Filter2 += " Or ";
                            Filter2 += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                            i++;
                        }
                        if (Filter2.Length == 0)
                            SQL.AppendLine("    And 1=0 ");
                        else
                            SQL.AppendLine("    And (" + Filter2 + ") ");
                        SQL.AppendLine("    ) ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("    And A.CCCode Is Not Null ");
                            SQL.AppendLine("    And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                            SQL.AppendLine("    ) ");
                            if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And ProfitCenterCode In (");
                            SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("        )))) ");
                        }
                    }
                }
                else
                {
                    SQL.AppendLine("SELECT A.DocDt, A.DocNo, A.CCCode, C.CCName, B.CCtCode, D.CCtName, B.Amt CostAmt, ");
                    SQL.AppendLine("B.ItCode, G.ItName, B.Qty, B.Rate, ");
                    SQL.AppendLine("If(E.DocType ='61', ");
                    if (mIsCASFromTRProcessAllAmount)
                        SQL.AppendLine("E.Amt, E.Amt) As CashAdvanceAmt, (E.Amt-B.Amt) OutstandingAmt, ");
                    else
                        SQL.AppendLine("(E.Amt- F.DailyAmt), E.Amt) As CashAdvanceAmt, If(E.DocType ='61',((E.Amt- F.DailyAmt)-B.Amt), (E.Amt-B.Amt))OutstandingAmt,  ");
                    SQL.AppendLine("B.FileName ");
                    SQL.AppendLine("FROM TblCashAdvanceSettlementHdr A ");
                    SQL.AppendLine("INNER JOIN TblCashAdvanceSettlementDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' AND A.Status = 'A' AND DocStatus = 'F' ");
                    SQL.AppendLine("LEFT JOIN TblCostCenter C ON A.CCCode = C.CCCode ");
                    SQL.AppendLine("LEFT JOIN TblCostCategory D ON B.CCtCode = D.CCtCode ");
                    SQL.AppendLine("LEFT JOIN TblVoucherHdr E ON B.VoucherDocNo = E.DocNo ");
                    if (!mIsCASFromTRProcessAllAmount)
                    {
                        SQL.AppendLine("Left Join ( ");

                        //SQL.AppendLine("    Select A.VoucherRequestDocNo As DocNo, SUM(B.Amt2) As DailyAmt  ");
                        //SQL.AppendLine("    From TblTravelRequestDtl8 A  ");
                        //SQL.AppendLine("    Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo  And A.EmpCode = B.PICCode ");
                        //SQL.AppendLine("    Inner Join TblTravelRequestHdr C On A.DocNo = C.DocNo And CancelInd = 'N' ");
                        //SQL.AppendLine("    Group By A.VoucherRequestDocNo ");

                        SQL.AppendLine("    Select T4.VoucherRequestDocNo As DocNo, Sum(T5.Amt2) As DailyAmt  ");
                        SQL.AppendLine("    From TblCashAdvanceSettlementHdr T1 ");
                        SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl T2 On T1.DocNo = T2.DocNo ");
                        SQL.AppendLine("    Inner Join TblVoucherHdr T3 On T2.VoucherDocNo=T3.DocNo ");
                        SQL.AppendLine("    Inner Join TblTravelRequestDtl8 T4 On T3.VoucherRequestDocNo=T4.VoucherRequestDocNo  ");
                        SQL.AppendLine("    Inner Join TblTravelRequestDtl7 T5 On T4.DocNo=T5.DocNo And T4.EmpCode=T5.PICCode ");
                        SQL.AppendLine("    Inner Join TblTravelRequestHdr T6 On T4.DocNo=T6.DocNo And T6.CancelInd='N' ");
                        SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
                        SQL.AppendLine("    And T1.CancelInd='N' ");
                        SQL.AppendLine("    And T1.Status='A' ");
                        SQL.AppendLine("    And T1.DocStatus='F' ");
                        SQL.AppendLine("    Group By T4.VoucherRequestDocNo ");
                        SQL.AppendLine(") F On E.VoucherRequestDocNo=F.DocNo ");
                    }
                    SQL.AppendLine("LEFT JOIN TblItem G ON G.ItCode = B.ItCode ");
                    SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
                }

                SQL.AppendLine(Filter);
                SQL.AppendLine("Order By A.DocNo, A.DocDt;");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        { 
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CCName", "CCtName", "ItCode", "ItName",
                            
                            //6-10
                            "Qty", "Rate", "CashAdvanceAmt", "CostAmt", "OutstandingAmt", 

                            //11
                            "FileName",

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            
                        }, true, false, false, false
                    );

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 8, 9, 10, 11 });
               
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {

                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsRptCashAdvanceSettlementUseProfitCenter) return;
            if (!ChkProfitCenterCode.Checked)
                mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col From (");
            SQL.AppendLine("    Select ProfitCenterName AS Col, profitcentercode ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Profitcentercode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }


        private void GetParameter()
        {
            mIsCASFromTRProcessAllAmount = Sm.GetParameterBoo("IsCASFromTRProcessAllAmount");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");
            mIsCASUseItemRate = Sm.GetParameterBoo("IsCASUseItemRate");
            mIsRptCashAdvanceSettlementUseProfitCenter = Sm.GetParameterBoo("IsRptCashAdvanceSettlementUseProfitCenter");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }
       

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 13)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 12), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 12, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;


            if (e.ColIndex == 13)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 12), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 12, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        #endregion

        #endregion
    }
}
