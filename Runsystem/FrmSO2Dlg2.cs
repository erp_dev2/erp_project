﻿#region Update
/* 14/05/2018 [HAR] bug validasi jika packaging kosong
 22/05/2018 [HAR] tambah volume
 25/05/2018 [HAR] tambah manggil function buat kalkulasi total
 21/08/2018 [HAR] tambah inputan container group
 13/11/2018 [HAR] tambah inputan container quantity group
 08/02/2019 [HAR] BUG : SO  dapatkan volume dari item packaging unit
 07/07/2020 [ICA/SIER] menghilangkan teks "packaging" di item information
 08/07/2020 [DITA/SIER] tambah parameter mIsSO2NotUsePackagingLabel di constructor
 * */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSO2Dlg2 : RunSystem.FrmBase8
    {
        #region Field

        private FrmSO2 mFrmParent;
        private bool mIsDataExisted = false;
        private bool mIsShowOnly = true;
        private string mItCode="", mCtCode = string.Empty, mCtQtDocNo = string.Empty, mSOQuotPromoDocNo = string.Empty;
        private int mRow = -1;
        internal string mCtQtDNo = string.Empty;
        internal decimal mVolumePackaging = 0;
        internal string mVolUom = string.Empty;
        internal bool mIsSO2NotUsePackagingLabel = false;
        
        
        #endregion

        #region Constructor

        public FrmSO2Dlg2(
            FrmSO2 FrmParent,
            int Row,
            bool IsShowQty2,
            bool IsCtQtUseUPrice2,
            bool IsDataExisted,
            bool IsShowOnly,
            string CtCode,
            string CtQtDocNo, 
            string SOQuotPromoDocNo, 
            string ItCode,
            bool IsSO2NotUsePackagingLabel )
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
            mIsDataExisted = IsDataExisted;
            mIsShowOnly = IsShowOnly;
            mCtCode = CtCode;
            mCtQtDocNo = CtQtDocNo;
            mSOQuotPromoDocNo = SOQuotPromoDocNo;
            mItCode = ItCode;
            mIsSO2NotUsePackagingLabel = IsSO2NotUsePackagingLabel;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetLueAgtCode(ref LueAgtCode, mCtCode, string.Empty);
                SetFormControl(mIsShowOnly);
                if (mIsDataExisted) ShowData(ref mFrmParent.Grd1);
                mVolUom = Sm.GetParameter("ItemVolumeUom");
                mFrmParent.GetParameter();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Button Method

        override protected void BtnAddClick(object sender, EventArgs e)
        {
            if (!IsInsertedDataNotValid()) InsertData(ref mFrmParent.Grd1);
        }

        #endregion

        private void SetFormControl(bool IsShowOnly)
        {
            Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
            { 
               LueAgtCode, LuePackaging, TxtQtyPackaging, TxtTaxRate, DteDeliveryDt, TxtContainerGroup, MeeRemark, TxtContainerGroupQty
            }, IsShowOnly);
            Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
            { 
                TxtQty, TxtUPrice, TxtDiscount, 
                TxtDiscountAmt, TxtUPrice1, TxtPromoRate, TxtUPriceBefore,
                TxtTaxAmt, TxtUPriceAfter, TxtTotal
            }, true);
            BtnItem.Enabled = !IsShowOnly;
            BtnAdd.Visible = !IsShowOnly;
            ClearData();
            LueAgtCode.Focus();
            if (mIsSO2NotUsePackagingLabel)
            {
                this.label4.Text = "UoM";
                this.label4.Location = new System.Drawing.Point(100, 112);
                this.label9.Text = "Quantity";
                this.label9.Location = new System.Drawing.Point(77, 134);
            }
        }

        public void ClearData()
        {
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                  TxtQtyPackaging, TxtQty, TxtUPrice, TxtDiscount, 
                  TxtDiscountAmt, TxtUPrice1, TxtPromoRate, TxtUPriceBefore, TxtTaxRate, 
                  TxtTaxAmt, TxtUPriceAfter, TxtTotal, TxtContainerGroupQty
            }, 0);
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtContainerGroup, MeeRemark, TxtCtItCode, TxtCtItName, TxtItCode, TxtItName,
                TxtSpecification, 
            });
            mCtQtDNo = string.Empty;
            mVolumePackaging = 0;
        }

        private void InsertData(ref iGrid Grd)
        {
            Grd.Rows[mRow].Cells[2].Value = Sm.GetLue(LueAgtCode);
            Grd.Rows[mRow].Cells[3].Value = LueAgtCode.GetColumnValue("Col2");
            Grd.Rows[mRow].Cells[4].Value = TxtItCode.Text;
            Grd.Rows[mRow].Cells[6].Value = TxtItName.Text;
            Grd.Rows[mRow].Cells[7].Value = Sm.GetLue(LuePackaging);
            Grd.Rows[mRow].Cells[8].Value = TxtQtyPackaging.Text;
            Grd.Rows[mRow].Cells[9].Value = TxtQty.Text;
            Grd.Rows[mRow].Cells[10].Value = TxtUomCode.Text;
            Grd.Rows[mRow].Cells[11].Value = TxtUPrice.Text;
            Grd.Rows[mRow].Cells[12].Value = TxtDiscount.Text;
            Grd.Rows[mRow].Cells[13].Value = TxtDiscountAmt.Text;
            Grd.Rows[mRow].Cells[14].Value = TxtUPrice1.Text;
            Grd.Rows[mRow].Cells[15].Value = TxtPromoRate.Text;
            Grd.Rows[mRow].Cells[16].Value = TxtUPriceBefore.Text;
            Grd.Rows[mRow].Cells[17].Value = TxtTaxRate.Text;
            Grd.Rows[mRow].Cells[18].Value = TxtTaxAmt.Text;
            Grd.Rows[mRow].Cells[19].Value = TxtUPriceAfter.Text;
            Grd.Rows[mRow].Cells[20].Value = TxtTotal.Text;
            if (Sm.GetDte(DteDeliveryDt).Length == 0)
                Grd.Rows[mRow].Cells[21].Value = null;
            else
                Grd.Rows[mRow].Cells[21].Value = Sm.SetGrdDate(Sm.GetDte(DteDeliveryDt).Substring(0, 8));
            Grd.Rows[mRow].Cells[22].Value = MeeRemark.Text;
            Grd.Rows[mRow].Cells[23].Value = mCtQtDNo;
            Grd.Rows[mRow].Cells[24].Value = TxtSpecification.Text;
            Grd.Rows[mRow].Cells[25].Value = TxtCtItCode.Text;
            Grd.Rows[mRow].Cells[26].Value = TxtCtItName.Text;
            Grd.Rows[mRow].Cells[27].Value = mVolumePackaging;
            Grd.Rows[mRow].Cells[28].Value = Decimal.Parse(TxtQtyPackaging.Text) * mVolumePackaging;
            Grd.Rows[mRow].Cells[29].Value = mVolUom;
            Grd.Rows[mRow].Cells[30].Value = TxtContainerGroup.Text;
            Grd.Rows[mRow].Cells[31].Value = TxtContainerGroupQty.Text;

            if (mRow==Grd.Rows.Count-1)
            {
                Grd.Rows.Add();
                Sm.SetGrdNumValueZero(ref Grd, Grd.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            }

            mFrmParent.ComputeTotal();
            mFrmParent.ComputeTotalQuantity();
            mFrmParent.ComputeTotalContainer();
            this.Close();
        }

        private void ShowData(ref iGrid Grd)
        {   
            SetLueAgtCode(ref LueAgtCode, mCtCode, Sm.GetGrdStr(Grd, mRow, 2));
            Sm.SetLue(LueAgtCode, Sm.GetGrdStr(Grd, mRow, 2));
            TxtItCode.EditValue = Sm.GetGrdStr(Grd, mRow, 4);
            TxtItName.EditValue = Sm.GetGrdStr(Grd, mRow, 6);
            SetLuePackaging(ref LuePackaging, mItCode, Sm.GetGrdStr(Grd, mRow, 7));    
            Sm.SetLue(LuePackaging, Sm.GetGrdStr(Grd, mRow, 7));
            TxtQtyPackaging.EditValue = Sm.GetGrdDec(Grd, mRow, 8, 0);
            TxtQty.EditValue = Sm.GetGrdDec(Grd, mRow, 9, 0);
            TxtUomCode.EditValue = Sm.GetGrdStr(Grd, mRow, 10);
            TxtUPrice.EditValue = Sm.GetGrdDec(Grd, mRow, 11, 0);
            TxtDiscount.EditValue = Sm.GetGrdDec(Grd, mRow, 12, 0);
            TxtDiscountAmt.EditValue = Sm.GetGrdDec(Grd, mRow, 13, 0);
            TxtUPrice1.EditValue = Sm.GetGrdDec(Grd, mRow, 14, 0);
            TxtPromoRate.EditValue = Sm.GetGrdDec(Grd, mRow, 15, 0);
            TxtUPriceBefore.EditValue = Sm.GetGrdDec(Grd, mRow, 16, 0);
            TxtTaxRate.EditValue = Sm.GetGrdDec(Grd, mRow, 17, 0);
            TxtTaxAmt.EditValue = Sm.GetGrdDec(Grd, mRow, 18, 0);
            TxtUPriceAfter.EditValue = Sm.GetGrdDec(Grd, mRow, 19, 0);
            TxtTotal.EditValue = Sm.GetGrdDec(Grd, mRow, 20, 0);
            Sm.SetDte(DteDeliveryDt, Sm.GetGrdDate(Grd, mRow, 21));
            MeeRemark.EditValue = Sm.GetGrdStr(Grd, mRow, 22);
            mCtQtDNo = Sm.GetGrdStr(Grd, mRow, 23);
            TxtSpecification.EditValue = Sm.GetGrdStr(Grd, mRow, 24);
            TxtCtItCode.EditValue = Sm.GetGrdStr(Grd, mRow, 25);
            TxtCtItName.EditValue = Sm.GetGrdStr(Grd, mRow, 26);
            TxtContainerGroup.EditValue = Sm.GetGrdStr(Grd, mRow, 30);
            TxtContainerGroupQty.EditValue = Sm.GetGrdDec(Grd, mRow, 31);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtItCode, "Item's code", false) ||
                Sm.IsLueEmpty(LuePackaging, "Packaging UOM") ||
                Sm.IsTxtEmpty(TxtQtyPackaging, "Packaging Quantity", true) ||
                Sm.IsTxtEmpty(TxtQty, "Quantity", true)||
                //IsQtyPackagingNotValid()||
                //IsQtyNotValid() ||
                IsQtyTaxNotValid();
        }

        //private bool IsQtyPackagingNotValid()
        //{
        //    if (Decimal.Parse(TxtQtyPackaging.Text)<0)
        //    {
        //        Sm.StdMsg(mMsgType.Warning,
        //            "Quantity packaging must be greater than 0.");
        //        return true;
        //    }
        //    return false;
        //}

        //private bool IsQtyNotValid()
        //{
        //    if (Decimal.Parse(TxtQty.Text) < 0)
        //    {
        //        Sm.StdMsg(mMsgType.Warning,
        //            "Quantity must be greater than 0.");
        //        return true;
        //    }
        //    return false;
        //}

        private bool IsQtyTaxNotValid()
        {
            if (Decimal.Parse(TxtTaxRate.Text) < 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Tax rate cannot less than 0.");
                return true;
            }
            return false;
        }

        private bool IsPackagingAlreadyExist()
        {
            string ItCode = TxtItCode.Text;
            string Packaging = Sm.GetLue(LuePackaging);
            string Uom = TxtUomCode.Text;

            for (int Row = 0; Row < mFrmParent.Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.CompareStr(ItCode + Packaging + Uom, Sm.GetGrdStr(mFrmParent.Grd1, Row, 4) + Sm.GetGrdStr(mFrmParent.Grd1, Row, 7) + Sm.GetGrdStr(mFrmParent.Grd1, Row, 10)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                               "Item Code : " + TxtItCode.Text + Environment.NewLine +
                               "Item Name : " + TxtItName.Text + Environment.NewLine +
                               "Packaging : " + Sm.GetLue(LuePackaging) + Environment.NewLine +
                               "Uom : " + TxtUomCode.Text + Environment.NewLine +
                               "Already exist.");
                    return true;
                }
            }
            return false;
        }

        internal void ShowItemInfo()
        {
            ShowItemInfo1();
            ShowItemInfo2();
        }

        private void ShowItemInfo1()
        {
            TxtUomCode.EditValue = null;
            TxtUPrice.EditValue = Sm.FormatNum(0m, 0);
            TxtDiscount.EditValue = Sm.FormatNum(0m, 0);
            TxtUPrice1.EditValue = Sm.FormatNum(0m, 0);
            TxtPromoRate.EditValue = Sm.FormatNum(0m, 0);
            mVolumePackaging = 0;
            string PackagingUomCode = Sm.GetLue(LuePackaging);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select D.PriceUomCode, E.UPrice As UPriceReal, B.Uprice As UpriceAf, B.Discount, ");
            SQL.AppendLine("C.DiscRate, B.DNo, A.Specification, F.CtItCode, F.CtItName, G.Volume  ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T3.ItCode, T2.ItemPriceDocNo, T2.ItemPriceDNo, T2.UPrice, T2.Discount, T2.DNo ");
	        SQL.AppendLine("    From TblCtQtHdr T1 ");
	        SQL.AppendLine("    Inner Join TblCtQtDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl T3 On T2.ItemPriceDocNo=T3.DocNo And T2.ItemPriceDno=T3.DNo ");
	        SQL.AppendLine("    Where T1.DocNo=@CtQtDocNo ");
            SQL.AppendLine("    ) B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.ItCode, T2.DiscRate ");
	        SQL.AppendLine("    From TblSOQuotPromo T1 ");
	        SQL.AppendLine("    Inner Join TblSOQuotPromoItem T2 On T1.DocNo=T2.DocNo ");
	        SQL.AppendLine("    Where T1.DocNo In ( ");
		    SQL.AppendLine("        Select DocNo From TblSOQuotPromoAgt ");
		    SQL.AppendLine("        Where AgtCode=@AgtCode ");
		    SQL.AppendLine("        And DocNo=@SOQuotPromoDocNo ");
		    SQL.AppendLine("        ) ");
	        SQL.AppendLine("    And T1.DocNo=@SOQuotPromoDocNo ");
            SQL.AppendLine(") C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblItemPricehdr D On B.ItemPriceDocNo = D.DocNo ");
            SQL.AppendLine("Left Join TblItemPriceDtl E On B.ItemPriceDocNo = E.DocNo And B.ItemPriceDNo = E.Dno ");
            SQL.AppendLine("Left Join TblCustomerItem F On A.ItCode=F.ItCode And F.CtCode=@CtCode ");
            //SQL.AppendLine("Left Join tblitempackagingunit G On A.ItCode = G.ItCode And A.SalesUomCode = G.UomCode ");
            SQL.AppendLine("Left Join tblitempackagingunit G On A.ItCode = G.ItCode And G.UomCode=@PackagingUomCode ");
            SQL.AppendLine("Where A.ItCode=@ItCode;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParam<String>(ref cm, "@CtQtDocNo", mCtQtDocNo);
            Sm.CmParam<String>(ref cm, "@SOQuotPromoDocNo", mSOQuotPromoDocNo);
            Sm.CmParam<String>(ref cm, "@AgtCode", Sm.GetLue(LueAgtCode));
            Sm.CmParam<String>(ref cm, "@PackagingUomCode", Sm.GetLue(LuePackaging));

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "PriceUomCode", 

                        //1-5
                        "UPriceReal", "UPriceAf", "Discount", "DiscRate", "DNo",
 
                        //6-9
                        "Specification", "CtItCode", "CtItName", "Volume"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtUomCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtUPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[1]), 0);
                        TxtUPrice1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                        TxtDiscount.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 0);
                        TxtPromoRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                        mCtQtDNo = Sm.DrStr(dr, c[5]);
                        TxtSpecification.EditValue = Sm.DrStr(dr, c[6]);
                        TxtCtItCode.EditValue = Sm.DrStr(dr, c[7]);
                        TxtCtItName.EditValue = Sm.DrStr(dr, c[8]);
                        mVolumePackaging = Sm.DrDec(dr, c[9]);
                    }, false
                );
        }

        private void ShowItemInfo2()
        {
            decimal
                Qty = 0m,
                UPrice = 0m,
                Discount = 0m,
                UPrice1 = 0m,
                PromoRate = 0m,
                UPriceBefore = 0m,
                TaxRate = 0m,
                TaxAmt = 0m,
                UPriceAfter = 0m;

            if (TxtQty.Text.Length > 0) Qty = decimal.Parse(TxtQty.Text);
            if (TxtUPrice.Text.Length > 0) UPrice = decimal.Parse(TxtUPrice.Text);
            if (TxtDiscount.Text.Length > 0) Discount = decimal.Parse(TxtDiscount.Text);
            TxtDiscountAmt.EditValue = Sm.FormatNum(Discount * 0.01m * UPrice, 0);
            if (TxtUPrice1.Text.Length > 0) UPrice1 = decimal.Parse(TxtUPrice1.Text);
            if (TxtPromoRate.Text.Length > 0) PromoRate = decimal.Parse(TxtPromoRate.Text);
            TxtUPriceBefore.EditValue = Sm.FormatNum(UPrice1 - (UPrice1 * 0.01m * PromoRate), 0);
            if (TxtUPriceBefore.Text.Length > 0) UPriceBefore = decimal.Parse(TxtUPriceBefore.Text);
            if (TxtTaxRate.Text.Length > 0) TaxRate = decimal.Parse(TxtTaxRate.Text);
            TxtTaxAmt.EditValue = Sm.FormatNum(TaxRate * 0.01m * UPriceBefore, 0);
            if (TxtTaxAmt.Text.Length > 0) TaxAmt = decimal.Parse(TxtTaxAmt.Text);
            TxtUPriceAfter.EditValue = Sm.FormatNum(UPriceBefore + TaxAmt, 0);
            if (TxtUPriceAfter.Text.Length > 0) UPriceAfter = decimal.Parse(TxtUPriceAfter.Text);
            TxtTotal.EditValue = Sm.FormatNum(UPriceAfter * Qty, 0);
            mVolumePackaging = Sm.GetValueDec("Select Volume From tblitempackagingunit Where ItCode = '" + TxtItCode.Text + "' And UomCode = '" + Sm.GetLue(LuePackaging) + "' ", string.Empty);
        }

      
        #region Additional Method

        private decimal GetSalesUomCodeConvert(string ConvertType, string ItCode)
        {
            var cm = new MySqlCommand
            {
                CommandText = "Select SalesUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
            };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            return Sm.GetValueDec(cm);
        }

        private decimal ComputeQtyBasedOnConvertionFormula(string ConvertType, decimal Qty, decimal Qty2)
        {
            decimal Convert = GetSalesUomCodeConvert(ConvertType, TxtItCode.Text);
            if (Convert != 0) Qty2 = Convert * Qty;
            return Qty2;
        }

        internal void ComputeUomSales()
        {
            decimal QtyPackaging = 0m, QtyUomSales1 = 0m, QtyUomSales2 = 0m;
            string UomSales;
            string UomPackaging;

            try
            {
                UomPackaging = Sm.GetLue(LuePackaging);
                QtyPackaging = Decimal.Parse(TxtQtyPackaging.Text);
                if (UomPackaging != string.Empty)
                {
                    QtyUomSales1 = Decimal.Parse(Sm.GetValue("Select Qty From TblItemPackagingUnit Where ItCode = '" + TxtItCode.Text + "' And UomCode = '" + UomPackaging + "' "));
                    QtyUomSales2 = Decimal.Parse(Sm.GetValue("Select Qty2 From TblItemPackagingUnit Where ItCode = '" + TxtItCode.Text + "' And UomCode = '" + UomPackaging + "' "));

                    UomSales = Sm.GetValue("Select SalesUomCode From TblItem Where ItCode = '" + TxtItCode.Text + "' ");
                    if (UomSales == TxtUomCode.Text)
                    {
                        TxtQty.EditValue = Sm.FormatNum((QtyPackaging * QtyUomSales1), 0);
                    }
                    else
                    {
                        TxtQty.EditValue = Sm.FormatNum((QtyPackaging * QtyUomSales2), 0);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex + "");
            }
        }

        private void SetLueAgtCode(ref LookUpEdit Lue, string CtCode, string AgtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AgtCode As Col1, AgtName As Col2 ");
            SQL.AppendLine("From TblAgent A ");
            SQL.AppendLine("Where ((CtCode=@CtCode ");
            SQL.AppendLine("And 1=Case When Exists(Select CtCode From TblCustomer Where AgentInd='N' And CtCode=@CtCode) Then 0 Else ");
            SQL.AppendLine("    Case When Exists(Select AgtCode From TblCtQtDtl3 Where AgtCode=A.AgtCode And DocNo=@CtQtDocNo) Then 1 Else 0 End ");
            SQL.AppendLine("End )");
            if (AgtCode.Length != 0)
                SQL.AppendLine(" Or AgtCode=@AgtCode");
            SQL.AppendLine(") Order By AgtName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@AgtCode", AgtCode);
            Sm.CmParam<String>(ref cm, "@CtQtDocNo", mCtQtDocNo);
            Sm.SetLue2(
             ref Lue, ref cm,
             0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public void SetLuePackaging(ref LookUpEdit Lue, string ItCode, string UomCode)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Col1, Col2 From ( ");
            SQL.AppendLine("    Select UomCode As Col1, UomCode As Col2 ");
            SQL.AppendLine("    From TblItemPackagingUnit ");
            SQL.AppendLine("    Where ItCode=@ItCode ");
            if (UomCode.Length != 0)
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select UomCode As Col1, UomCode As Col2 ");
                SQL.AppendLine("    From TblUom ");
                SQL.AppendLine("    Where UomCode=@UomCode ");
            }
            SQL.AppendLine(") T Order By Col1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.CmParam<String>(ref cm, "@UomCode", UomCode);

            Sm.SetLue2(
              ref Lue, ref cm,
              0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion


        #endregion

        #region Event

        #region Button Event

        private void BtnItem_Click(object sender, EventArgs e)
        {
            ClearData();    
            Sm.FormShowDialog(new FrmSO2Dlg3(this, mCtQtDocNo, mCtCode));
        }

        private void BtnMasterItem_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtItCode, "Item Code", false))
            {
                try
                {
                    var f = new FrmItem("XXX");
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = TxtItCode.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.StdMsg(mMsgType.Warning, Exc.Message);
                }
            }
        }

        #endregion

        #region Misc Control Event

        private void LueAgtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAgtCode, new Sm.RefreshLue3(SetLueAgtCode), mCtCode, "");
            ShowItemInfo();
        }

        private void TxtQty_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtQty, 0);
            ShowItemInfo2();
        }

     

        private void TxtTaxRate_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTaxRate, 0);
            ShowItemInfo2();
        }

        private void LuePackaging_EditValueChanged(object sender, EventArgs e)
        {
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                  TxtQtyPackaging, TxtQty, TxtTotal
            }, 0);
            Sm.RefreshLookUpEdit(LuePackaging, new Sm.RefreshLue3(SetLuePackaging), TxtItCode.Text, "");
        }

        private void TxtQtyPackaging_Validated(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LuePackaging, "Packaging UoM")) return;
            try
            {
                Sm.FormatNumTxt(TxtQtyPackaging, 0);
                if (mFrmParent.mIsSO2NotUsePackagingLabel) TxtQty.EditValue = TxtQtyPackaging.Text;
                else ComputeUomSales();
                ShowItemInfo2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void TxtContainerGroupQty_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtContainerGroupQty, 0);
        }

        #endregion

       


        #endregion

    }
}
