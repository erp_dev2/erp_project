﻿#region Update
/*
    11/06/2017 [TKG] tambah validasi vat settlement ketika data hendak dicancel. 
    30/07/2017 [TKG] tambah vr vat 2 dan 3, ubah validasi terkait dengan vr vat saat dicancel, 1 dokumen 1 do berdasarkan parameter.
    10/08/2017 [TKG] tambah tax# dan tax date untuk tax 2 dan 3
    01/08/2018 [WED] tambah field Service Code
    07/08/2018 [TKG] bisa edit nomor faktur pajak
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    10/06/2021 [TRI/IMS] Pilihan departemen dibuat hanya menampilkan departemen yang aktif
    12/10/2022 [RDA/PRODUCT] Purchase Return Invoice dapat menarik docno DO to Vendor dengan replace item ind aktif berdasarkan param DOtoVendorReplacedItem
    13/10/2022 [RDA/PRODUCT] Purchase Return Invoice menampilkan informasi Receiving, PI, dan Voucher berdasarkan param PurchaseReturnInvoiceShowSource & Update PaidInd
    25/10/2022 [RDA/PRODUCT] bug source receiving, PI, dan voucher tetap muncul ketika grid parent terisi (berdasar param PurchaseReturnInvoiceShowSource)
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseReturnInvoice : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application;
            mTaxCode1 = string.Empty, 
            mTaxCode2 = string.Empty, 
            mTaxCode3 = string.Empty,
            mDOtoVendorReplacedItem = string.Empty;
        private string mSiteCode = string.Empty;
        private bool 
            mIsAutoJournalActived = false, 
            mIsPurchaseReturnInvoiceCanOnlyHave1Record = false;
        internal bool 
            mIsSiteMandatory = false,
            mPurchaseReturnInvoiceShowSource = false;
        internal FrmPurchaseReturnInvoiceFind FrmFind;

        #endregion

        #region Constructor

        public FrmPurchaseReturnInvoice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Purchase Return Invoice";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();

                if (mPurchaseReturnInvoiceShowSource)
                {
                    label25.Visible = true;
                    TxtRecvVd.Visible = true;
                    BtnRecvVd.Visible = true;
                    
                    label26.Visible = true;
                    TxtPurchaseInvoice.Visible = true;
                    BtnPurchaseInvoice.Visible = true;

                    label28.Visible = true;
                    TxtVoucher.Visible = true;
                    BtnVoucher.Visible = true;
                }
                else
                {
                    label25.Visible = false;
                    TxtRecvVd.Visible = false;
                    BtnRecvVd.Visible = false;

                    label26.Visible = false;
                    TxtPurchaseInvoice.Visible = false;
                    BtnPurchaseInvoice.Visible = false;

                    label28.Visible = false;
                    TxtVoucher.Visible = false;
                    BtnVoucher.Visible = false;
                }

                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty);
                Sl.SetLueServiceCode(ref LueServiceCode1);
                Sl.SetLueServiceCode(ref LueServiceCode2);
                Sl.SetLueServiceCode(ref LueServiceCode3);
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd 1

            Grd1.Cols.Count = 42;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Returned#",
                        "Returned#"+Environment.NewLine+"DNo",
                        "",
                        "Date",
                        
                        //6-10
                        "Received#",
                        "Received"+Environment.NewLine+"D No",
                        "",
                        "PO#",
                        "PO DNo",

                        //11-15
                        "",
                        "Item's Code",
                        "", 
                        "Item's Name", 
                        "Quantity",

                        //16-20
                        "UoM",
                        "Term of"+Environment.NewLine+"Payment",
                        "Currency",
                        "Unit"+Environment.NewLine+"Price",
                        "Discount"+Environment.NewLine+"%",
                        
                        //21-25
                        "Discount"+Environment.NewLine+"Amount",
                        "Rounding"+Environment.NewLine+"Value",
                        "Total",
                        "Tax"+Environment.NewLine+"(1)",
                        "Tax"+Environment.NewLine+"Date (1)",

                        //26-30
                        "Tax"+Environment.NewLine+"Code (1)",
                        "Tax"+Environment.NewLine+"(1)",
                        "Tax"+Environment.NewLine+"Code (2)",
                        "Tax"+Environment.NewLine+"(2)",
                        "Tax"+Environment.NewLine+"Code (3)",

                        //31-35
                        "Tax"+Environment.NewLine+"(3)",
                        "Remark",
                        "Site",
                        "Site Name",
                        "Tax#"+Environment.NewLine+"(2)",

                        //36-40
                        "Tax"+Environment.NewLine+"Date (2)",
                        "Tax#"+Environment.NewLine+"(3)",
                        "Tax"+Environment.NewLine+"Date (3)",
                        "Service1",
                        "Service2",

                        //41
                        "Service3",
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 130, 0, 20, 80,
                        
                        //6-10
                        150, 0, 20, 150, 0, 
                        
                        //11-15
                        20, 100, 20, 250, 80, 
                        
                        //16-20
                        100, 150, 80, 100, 80,

                        //21-25
                        80, 80, 130, 130, 100, 
                        
                        //26-30
                        0, 150, 0, 150, 0,

                        //31-35
                        150, 200, 0, 180, 100, 
                        
                        //36-40
                        80, 100, 80, 0, 0, 

                        //41
                        0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 4, 8, 11, 13 });
            Sm.GrdFormatDec(Grd1, new int[] { 15, 19, 20, 21, 22, 23 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5, 25, 36, 38 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 9, 10, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 33, 34, 35, 36, 37, 38, 39, 40, 41 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 17, 26, 28, 30, 33, 34, 39, 40, 41 }, false);
            if (mIsSiteMandatory)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 34 }, true);
                Grd1.Cols[34].Move(12);
            }

            Grd1.Cols[35].Move(28);
            Grd1.Cols[36].Move(29);
            Grd1.Cols[37].Move(32);
            Grd1.Cols[38].Move(33);

            #endregion

            #region Grd 2

            Grd2.Cols.Count = 11;
            Grd2.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]
                    { 
                         //0
                        "DNo",
                        
                        //1-5
                        "", 
                        "Purchase Invoice#",
                        "Purchase Invoice"+Environment.NewLine+"D#",
                        "", 
                        "Amt Type",
                        
                        //6-10
                        "Type",
                        "Currency", 
                        "Outstanding"+Environment.NewLine+"Amount",
                        "Amount",
                        "Remark"
                    },
                    new int[] { 0, 20, 130, 0, 20, 0, 120, 80, 100, 100, 250 }
                );
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 2, 3, 5, 6, 7, 8 });
            Sm.GrdColButton(Grd2, new int[] { 1, 4 });
            Sm.GrdFormatDec(Grd2, new int[] { 8, 9 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 3, 5, 10 }, false);

            #endregion

            #region Grd 3

            Grd3.Cols.Count = 7;
            Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit"+Environment.NewLine+"Amount",
                        "Credit"+Environment.NewLine+"Amount",
                        "Remark",

                        //6
                        "Type"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400,

                        //6
                        0
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColReadOnly(Grd3, new int[] { 1, 2, 6 });
            Sm.GrdFormatDec(Grd3, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 2, 6 });

            #endregion
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 8, 9, 11, 12, 13, 17 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 10 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, LueVdCode, LueDeptCode, 
                        MeeRemark, LueServiceCode1, LueServiceCode2, LueServiceCode3, TxtTaxInvoiceNo,
                        TxtTaxInvoiceNo2, TxtTaxInvoiceNo3
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 32 });
                    Grd2.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 3, 4, 5 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueVdCode, LueDeptCode, MeeRemark, TxtTaxInvoiceNo,
                        TxtTaxInvoiceNo2, TxtTaxInvoiceNo3
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 32 });
                    Grd2.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mSiteCode = string.Empty;
            mTaxCode1 = string.Empty;
            mTaxCode2 = string.Empty;
            mTaxCode3 = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtDocNo, DteDocDt, MeeCancelReason, LueVdCode, LueDeptCode, 
                TxtSiteCode, TxtCurCode, TxtTaxInvoiceNo, DteTaxInvoiceDt, TxtTaxInvoiceNo2, 
                DteTaxInvoiceDt2, TxtTaxInvoiceNo3, DteTaxInvoiceDt3, TxtTaxCode1, TxtTaxCode2, 
                TxtTaxCode3, MeeRemark, TxtVoucherRequestPPNDocNo, TxtVoucherRequestPPNDocNo2, TxtVoucherRequestPPNDocNo3, 
                TxtJournalDocNo, TxtJournalDocNo2, LueServiceCode1, LueServiceCode2, LueServiceCode3, 
                TxtRecvVd, TxtPurchaseInvoice, TxtVoucher
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { TxtAmtBefTax, TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3, TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.FocusGrd(Grd2, 0, 1);
            Sm.FocusGrd(Grd3, 0, 1);
        }

        #region Clear Grid

        private void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 15, 19, 20, 21, 22, 23 });
        }

        private void ClearGrd2()
        {
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 8, 9 });
        }

        private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 3, 4 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPurchaseReturnInvoiceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueVdCode(ref LueVdCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            try
            {
                PrintData(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        private void BtnRecvVd_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtRecvVd, "Receiving Item From Vendor", false))
            {
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtRecvVd.Text;
                f.ShowDialog();
            }
        }

        private void BtnPurchaseInvoice_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPurchaseInvoice, "Purchase Invoice", false))
            {
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPurchaseInvoice.Text;
                f.ShowDialog();
            }
        }

        private void BtnVoucher_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucher, "Voucher", false))
            {
                Sm.FormShowDialog(new FrmPurchaseReturnInvoiceDlg4(this));
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PurchaseReturnInvoice", "TblPurchaseReturnInvoiceHdr");
            
            var cml = new List<MySqlCommand>();
            var cm = new List<MySqlCommand>();

            cml.Add(SavePurchaseReturnInvoiceHdr(DocNo));

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SavePurchaseReturnInvoiceDtl(DocNo, Row));
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int r = 0; r < Grd2.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd2, r, 2).Length > 0) cml.Add(SavePurchaseReturnInvoiceDtl2(DocNo, r));
            }

            if (Grd3.Rows.Count > 1)
            {
                for (int r = 0; r < Grd3.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd3, r, 1).Length > 0) cml.Add(SavePurchaseReturnInvoiceDtl3(DocNo, r));
            }

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));
            Sm.ExecCommands(cml);

            ShowData(DocNo);

            if (mPurchaseReturnInvoiceShowSource)
            {
                cm.Add(UpdatePaidInd(DocNo));
                Sm.ExecCommands(cm);
            }
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                Sm.IsTxtEmpty(TxtCurCode, "Currency", false) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsCurrencyNotValid() ||
                //IsTaxInvoiceNoNotValid() ||
                IsTaxInvoiceDtNotValid() ||
                IsTaxNotValid() ||
                IsDOVdExisted() ||
                IsJournalAmtNotBalanced() ||
                IsAmtTypeDataNotValid() ||
                IsAmtInvalid() ||
                IsSiteNotValid() ||
                IsPurchaseReturnInvoiceNotValid();
        }

        private bool IsPurchaseReturnInvoiceNotValid()
        { 
            if (mIsPurchaseReturnInvoiceCanOnlyHave1Record)
            {
                int x = 0;
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length>0)
                    {
                        x += 1;
                        if (x > 1)
                        {
                            Sm.StdMsg(mMsgType.Warning, "One document can only process 1 do#.");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsSiteNotValid()
        {
            if (!mIsSiteMandatory) return false;

            if (Grd1.Rows.Count>1)
            {
                mSiteCode = Sm.GetGrdStr(Grd1, 0, 33);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 33).Length > 0 &&
                        !Sm.CompareStr(mSiteCode, Sm.GetGrdStr(Grd1, r, 33)))
                    {
                        Sm.StdMsg(mMsgType.Warning, 
                            "Site : " + Sm.GetGrdStr(Grd1, r, 34) + Environment.NewLine + 
                            "Invalid site.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsAmtTypeDataNotValid()
        {
            ComputeOutstandingAmt();

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0 && IsQtyBiggerThanOutstandingQty(Row)) return true;

            return false;
        }

        private bool IsQtyBiggerThanOutstandingQty(int Row)
        {
            decimal
                Outstanding = Sm.GetGrdDec(Grd2, Row, 8),
                Qty = Sm.GetGrdDec(Grd2, Row, 9);

            if (Qty > Outstanding)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Purchase Invoice# : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                    "Type : " + Sm.GetGrdStr(Grd2, Row, 6) + Environment.NewLine + Environment.NewLine +
                    "Quantity (" + Sm.FormatNum(Qty, 0) + ") is bigger than outstanding (" + Sm.FormatNum(Outstanding, 0) + ")."
                    );
                return true;
            }

            return false;
        }

        private bool IsDOVdExisted()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0 &&
                        Sm.IsDataExist(
                            "Select A.DocNo " +
                            "From TblPurchaseReturnInvoiceHdr A, TblPurchaseReturnInvoiceDtl B " +
                            "Where A.CancelInd='N' " +
                            "And A.DocNo=B.DocNo " +
                            "And B.DOVdDocNo='" + Sm.GetGrdStr(Grd1, Row, 2) + "' And B.DOVdDNo='" + Sm.GetGrdStr(Grd1, Row, 3) + "' " +
                            "Limit 1;"
                            )
                        )
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "DO To Vendor# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Receiving Item# : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                            "PO# : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                            "Item's code : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                            "Item's name : " + Sm.GetGrdStr(Grd1, Row, 14) + Environment.NewLine + Environment.NewLine +
                            "Data already existed.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1 && Grd3.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in received document list or discount/customs tax/down payment list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Returned# is empty.")) return true;
                    if (mIsSiteMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 33, false, "Site is empty.")) return true;
                }
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Purchase invoice# is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 9, true,
                        "Purchase invoice document number : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                        "Type : " + Sm.GetGrdStr(Grd2, Row, 6) + Environment.NewLine + Environment.NewLine +
                        "Amount is 0.")) return true;
                }
            }

            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 1, false, "COA's account is empty.")) return true;
                    if (Sm.GetGrdDec(Grd3, Row, 3) == 0m && Sm.GetGrdDec(Grd3, Row, 4) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Account# : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount can't be 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd3, Row, 3) != 0m && Sm.GetGrdDec(Grd3, Row, 4) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Account# : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount should not be bigger than 0.");
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsJournalAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 3).Length > 0) Debit += Sm.GetGrdDec(Grd3, Row, 3);
                if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0) Credit += Sm.GetGrdDec(Grd3, Row, 4);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }
            return false;
        }

        private bool IsCurrencyNotValid()
        {
            bool NotValid = false;
            string CurCode = TxtCurCode.Text;

            if (Grd1.Rows.Count > 1 && CurCode.Length != 0)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length>0 && !Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, r, 18)))
                    {
                        NotValid = true;
                        break;
                    }
                }
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 7).Length != 0)
                    {
                        if (CurCode.Length != 0 && !Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd2, Row, 7)))
                        {
                            NotValid = true;
                            break;
                        }
                    }
                }
            }
            if (NotValid) Sm.StdMsg(mMsgType.Warning, "One document only allowed 1 currency.");
            return NotValid;
        }

        private bool IsTaxInvoiceNoNotValid()
        {
            string TaxInvoiceNo = TxtTaxInvoiceNo.Text;
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 && !Sm.CompareStr(TaxInvoiceNo, Sm.GetGrdStr(Grd1, r, 24)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "DO To Vendor# : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                            "Receiving Item# : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                            "PO# : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                            "Item's code : " + Sm.GetGrdStr(Grd1, r, 12) + Environment.NewLine +
                            "Item's name : " + Sm.GetGrdStr(Grd1, r, 14) + Environment.NewLine + 
                            "Invoice Tax# : " + Sm.GetGrdStr(Grd1, r, 24) + Environment.NewLine + Environment.NewLine +
                            "Invalid invoice tax#.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsTaxInvoiceDtNotValid()
        {
            bool NotValid = false;
            string TaxInvoiceDt = Sm.GetGrdStr(Grd1, 0, 25);

            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 && 
                        !Sm.CompareStr(TaxInvoiceDt, Sm.GetGrdStr(Grd1, r, 25)))
                    {
                        NotValid = true;
                        break;
                    }
                }
            }

            if (NotValid) Sm.StdMsg(mMsgType.Warning, "Only allowed 1 tax invoice date in 1 document.");
            return NotValid;
        }

        private bool IsTaxNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 &&
                        !Sm.CompareStr(mTaxCode1, Sm.GetGrdStr(Grd1, r, 26)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "DO To Vendor# : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                            "Receiving Item# : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                            "PO# : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                            "Item's code : " + Sm.GetGrdStr(Grd1, r, 12) + Environment.NewLine +
                            "Item's name : " + Sm.GetGrdStr(Grd1, r, 14) + Environment.NewLine + 
                            "Tax : " + Sm.GetGrdStr(Grd1, r, 27) + Environment.NewLine + Environment.NewLine +
                            "Invalid tax.");
                        return true;
                    }
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 &&
                        !Sm.CompareStr(mTaxCode2, Sm.GetGrdStr(Grd1, r, 28)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "DO To Vendor# : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                            "Receiving Item# : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                            "PO# : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                            "Item's code : " + Sm.GetGrdStr(Grd1, r, 12) + Environment.NewLine +
                            "Item's name : " + Sm.GetGrdStr(Grd1, r, 14) + Environment.NewLine +
                            "Tax : " + Sm.GetGrdStr(Grd1, r, 29) + Environment.NewLine + Environment.NewLine +
                            "Invalid tax.");
                        return true;
                    }
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 &&
                        !Sm.CompareStr(mTaxCode3, Sm.GetGrdStr(Grd1, r, 30)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "DO To Vendor# : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                            "Receiving Item# : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                            "PO# : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                            "Item's code : " + Sm.GetGrdStr(Grd1, r, 12) + Environment.NewLine +
                            "Item's name : " + Sm.GetGrdStr(Grd1, r, 14) + Environment.NewLine +
                            "Tax : " + Sm.GetGrdStr(Grd1, r, 31) + Environment.NewLine + Environment.NewLine +
                            "Invalid tax.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsAmtInvalid()
        {
            decimal Amt = 0m;
            if (TxtAmt.Text.Length > 0) Amt = decimal.Parse(TxtAmt.Text);
            if (Amt < 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Total amount should not be less than 0.");
                return true;
            }
            return false;
        }

        private MySqlCommand SavePurchaseReturnInvoiceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPurchaseReturnInvoiceHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, ProcessInd, VdCode, ");
            SQL.AppendLine("CurCode, AmtBefTax, Amt, DeptCode, SiteCode, ");
            SQL.AppendLine("TaxInvoiceNo, TaxInvoiceDt, ");
            SQL.AppendLine("TaxInvoiceNo2, TaxInvoiceDt2, ");
            SQL.AppendLine("TaxInvoiceNo3, TaxInvoiceDt3, ");
            SQL.AppendLine("TaxCode1, TaxCode2, TaxCode3, ");
            SQL.AppendLine("TaxAmt1, TaxAmt2, TaxAmt3, ");
            SQL.AppendLine("ServiceCode1, ServiceCode2, ServiceCode3, ");
            SQL.AppendLine("VoucherRequestPPNDocNo, Remark, JournalDocNo, JournalDocNo2, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, Null, 'N', 'O', @VdCode, ");
            SQL.AppendLine("@CurCode, @AmtBefTax, @Amt, @DeptCode, @SiteCode, ");
            SQL.AppendLine("@TaxInvoiceNo, @TaxInvoiceDt, ");
            SQL.AppendLine("@TaxInvoiceNo2, @TaxInvoiceDt2, ");
            SQL.AppendLine("@TaxInvoiceNo3, @TaxInvoiceDt3, ");
            SQL.AppendLine("@TaxCode1, @TaxCode2, @TaxCode3, ");
            SQL.AppendLine("@TaxAmt1, @TaxAmt2, @TaxAmt3, ");
            SQL.AppendLine("@ServiceCode1, @ServiceCode2, @ServiceCode3, ");
            SQL.AppendLine("Null, @Remark, Null, Null, @CreateBy, CurrentDateTime());");
            
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, 0, 18));
            Sm.CmParam<Decimal>(ref cm, "@AmtBefTax", Decimal.Parse(TxtAmtBefTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", TxtTaxInvoiceNo.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", TxtTaxInvoiceNo2.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", Sm.GetDte(DteTaxInvoiceDt2));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", TxtTaxInvoiceNo3.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", Sm.GetDte(DteTaxInvoiceDt3));
            Sm.CmParam<String>(ref cm, "@TaxCode1", mTaxCode1);
            Sm.CmParam<String>(ref cm, "@TaxCode2", mTaxCode2);
            Sm.CmParam<String>(ref cm, "@TaxCode3", mTaxCode3);
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", Decimal.Parse(TxtTaxAmt1.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", Decimal.Parse(TxtTaxAmt2.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", Decimal.Parse(TxtTaxAmt3.Text));
            Sm.CmParam<String>(ref cm, "@ServiceCode1", Sm.GetLue(LueServiceCode1));
            Sm.CmParam<String>(ref cm, "@ServiceCode2", Sm.GetLue(LueServiceCode2));
            Sm.CmParam<String>(ref cm, "@ServiceCode3", Sm.GetLue(LueServiceCode3));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePurchaseReturnInvoiceDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPurchaseReturnInvoiceDtl ");
            SQL.AppendLine("(DocNo, DNo, DOVdDocNo, DOVdDNo, ");
            SQL.AppendLine("ItCode, Qty, UPrice, Discount, DiscountAmt, RoundingValue, Amt, ");
            SQL.AppendLine("TaxInvoiceNo, TaxInvoiceDt, TaxCode1, TaxInvoiceNo2, TaxInvoiceDt2, TaxCode2, TaxInvoiceNo3, TaxInvoiceDt3, TaxCode3, ");
            SQL.AppendLine("SiteCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @DOVdDocNo, @DOVdDNo, ");
            SQL.AppendLine("@ItCode, @Qty, @UPrice, @Discount, @DiscountAmt, @RoundingValue, @Amt, ");
            SQL.AppendLine("@TaxInvoiceNo, @TaxInvoiceDt, @TaxCode1, @TaxInvoiceNo2, @TaxInvoiceDt2, @TaxCode2, @TaxInvoiceNo3, @TaxInvoiceDt3, @TaxCode3, ");
            SQL.AppendLine("@SiteCode, @Remark, @CreateBy, CurrentDateTime());");

            var cm = new MySqlCommand(){ CommandText=SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DOVdDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@DOVdDNo", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@Discount", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@DiscountAmt", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<Decimal>(ref cm, "@RoundingValue", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", Sm.GetGrdStr(Grd1, Row, 24));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetGrdDate(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetGrdStr(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", Sm.GetGrdStr(Grd1, Row, 35));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", Sm.GetGrdDate(Grd1, Row, 36));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetGrdStr(Grd1, Row, 28));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", Sm.GetGrdStr(Grd1, Row, 37));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", Sm.GetGrdDate(Grd1, Row, 38));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetGrdStr(Grd1, Row, 30));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 32));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePurchaseReturnInvoiceDtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPurchaseReturnInvoiceDtl2(DocNo, DNo, PurchaseInvoiceDocNo, PurchaseInvoiceDNo, CurCode, AmtType, Amt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @PurchaseInvoiceDocNo, @PurchaseInvoiceDNo, @CurCode, @AmtType, @Amt, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PurchaseInvoiceDocNo", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@PurchaseInvoiceDNo", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@AmtType", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd2, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 10));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePurchaseReturnInvoiceDtl3(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPurchaseReturnInvoiceDtl3(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd3, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdatePaidInd(string DocNo)
        {
            var SQL = new StringBuilder();

            if (TxtVoucher.Text.Length > 0 && TxtPurchaseInvoice.Text.Length > 0)
            {
                string VoucherTotalAmt = Sm.GetValue("SELECT SUM(Amt) Amt FROM tblvoucherhdr WHERE FIND_IN_SET(DocNo, @Param)", TxtVoucher.Text);
                string PIHdrAmt = Sm.GetValue("SELECT Amt FROM tblpurchaseinvoicehdr WHERE Docno = @Param", TxtPurchaseInvoice.Text);

                if (decimal.Parse(VoucherTotalAmt) - decimal.Parse(PIHdrAmt) == 0)
                    SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set PaidInd='P' Where DocNo=@DocNo;"); //Paid
                else
                    SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set PaidInd='O' Where DocNo=@DocNo;"); //Outstanding
            }
            else
                SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set PaidInd='O' Where DocNo=@DocNo;"); //Outstanding

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, @DocDt, Concat('Purchase Return Invoice : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select Concat(E.ParValue, A.VdCode) As AcNo, ");
            SQL.AppendLine("        0 As DAmt, C.Qty*D.UPrice*D.ExcRate As CAmt ");
            SQL.AppendLine("        From TblPurchaseReturnInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblPurchaseReturnInvoiceDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblDOVdDtl C On B.DOVdDocNo=C.DocNo And B.DOVdDNo=C.DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice D On C.Source=D.Source ");
            SQL.AppendLine("        Inner Join TblParameter E On E.ParCode='VendorAcNoUnInvoiceAP' ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select Concat(E.ParValue, A.VdCode) As AcNo, ");
            SQL.AppendLine("        C.Qty*D.UPrice*D.ExcRate As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblPurchaseReturnInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblPurchaseReturnInvoiceDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblDOVdDtl C On B.DOVdDocNo=C.DocNo And B.DOVdDNo=C.DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice D On C.Source=D.Source ");
            SQL.AppendLine("        Inner Join TblParameter E On E.ParCode='VendorAcNoAP' ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select AcNo, DAmt, CAmt ");
            SQL.AppendLine("        From TblPurchaseReturnInvoiceDtl3 Where DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl Group By AcNo ");
            SQL.AppendLine(") T;  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditPurchaseReturnInvoiceHdr());
            if (mIsAutoJournalActived) cml.Add(SaveJournal());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(Sm.ServerCurrentDate()) ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessToOutgoingPayment() ||
                IsDataAlreadyProcessToVoucherRequestPPN() ||
                (ChkCancelInd.Checked && IsVATSettlementExisted());
        }

        private bool IsDataCancelledAlready()
        {
            if (Sm.IsDataExist(
                "Select DocNo From TblPurchaseReturnInvoiceHdr " +
                "Where CancelInd='Y' And DocNo=@Param;", 
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyProcessToOutgoingPayment()
        {
            if (Sm.IsDataExist(
                "Select DocNo From TblPurchaseReturnInvoiceHdr " +
                "Where ProcessInd<>'O' And DocNo=@Param;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to outgoing payment.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyProcessToVoucherRequestPPN()
        {
            if (Sm.IsDataExist(
                "Select 1 From TblPurchaseReturnInvoiceHdr " +
                "Where (VoucherRequestPPNDocNo Is Not Null Or VoucherRequestPPNDocNo2 Is Not Null Or VoucherRequestPPNDocNo3 Is Not Null) " +
                "And DocNo=@Param;", 
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to voucher request VAT.");
                return true;
            }
            return false;
        }

        private bool IsVATSettlementExisted()
        {
            if (Sm.IsDataExist(
                "Select 1 From TblPurchaseReturnInvoiceHdr " +
                "Where DocNo=@Param " +
                "And (VATSettlementDocNo Is Not Null Or VATSettlementDocNo2 Is Not Null Or VATSettlementDocNo3 Is Not Null);",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to VAT settlement.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditPurchaseReturnInvoiceHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null ");
            SQL.AppendLine("And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Replace(CurDate(), '-', ''), Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo From TblPurchaseReturnInvoiceHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    );");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblPurchaseReturnInvoiceHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    );");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowPurchaseReturnInvoiceHdr(DocNo);
                if (mPurchaseReturnInvoiceShowSource) ShowPurchaseReturnInvoiceSource(DocNo);
                ShowPurchaseReturnInvoiceDtl(DocNo);
                ShowPurchaseReturnInvoiceDtl2(DocNo);
                ShowPurchaseReturnInvoiceDtl3(DocNo);
                ComputeAmt();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPurchaseReturnInvoiceHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("A.VdCode, A.DeptCode, A.SiteCode, B.SiteName, ");
            SQL.AppendLine("A.CurCode, A.AmtBefTax, A.Amt, ");
            SQL.AppendLine("A.TaxInvoiceNo, A.TaxInvoiceDt, ");
            SQL.AppendLine("A.TaxInvoiceNo2, A.TaxInvoiceDt2, ");
            SQL.AppendLine("A.TaxInvoiceNo3, A.TaxInvoiceDt3, ");
            SQL.AppendLine("A.TaxCode1, A.TaxCode2, A.TaxCode3,  ");
            SQL.AppendLine("C.TaxName As TaxName1, D.TaxName As TaxName2, E.TaxName As TaxName3, ");
            SQL.AppendLine("A.TaxAmt1, A.TaxAmt2, A.TaxAmt3, ");
            SQL.AppendLine("A.ServiceCode1, A.ServiceCode2, A.ServiceCode3, ");
            SQL.AppendLine("A.Remark, A.VoucherRequestPPNDocNo, A.VoucherRequestPPNDocNo2, A.VoucherRequestPPNDocNo3, A.JournalDocNo, A.JournalDocNo2 ");
            SQL.AppendLine("From TblPurchaseReturnInvoiceHdr A ");
            SQL.AppendLine("Left Join TblSite B On A.SiteCode=B.SiteCode ");
            SQL.AppendLine("Left Join TblTax C On A.TaxCode1=C.TaxCode ");
            SQL.AppendLine("Left Join TblTax D On A.TaxCode2=D.TaxCode ");
            SQL.AppendLine("Left Join TblTax E On A.TaxCode3=E.TaxCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt",  
                        "CancelReason", 
                        "CancelInd", 
                        "VdCode", 
                        "DeptCode", 

                        //6-10
                        "SiteCode", 
                        "SiteName", 
                        "CurCode", 
                        "AmtBefTax", 
                        "Amt", 
                        
                        //11-15
                        "TaxInvoiceNo",
                        "TaxInvoiceDt", 
                        "TaxInvoiceNo2",
                        "TaxInvoiceDt2", 
                        "TaxInvoiceNo3",

                        //16-20
                        "TaxInvoiceDt3", 
                        "TaxCode1", 
                        "TaxCode2", 
                        "TaxCode3", 
                        "TaxName1", 
                        
                        //21-25
                        "TaxName2", 
                        "TaxName3", 
                        "TaxAmt1", 
                        "TaxAmt2", 
                        "TaxAmt3",
                        
                        //26-30
                        "Remark",
                        "VoucherRequestPPNDocNo",
                        "VoucherRequestPPNDocNo2",
                        "VoucherRequestPPNDocNo3",
                        "JournalDocNo",

                        //31-34
                        "JournalDocNo2",
                        "ServiceCode1",
                        "ServiceCode2",
                        "ServiceCode3"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        SetLueVdCode(ref LueVdCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[5]));
                        mSiteCode = Sm.DrStr(dr, c[6]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[7]);
                        TxtCurCode.EditValue = Sm.DrStr(dr, c[8]);
                        TxtAmtBefTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                        TxtTaxInvoiceNo.EditValue = Sm.DrStr(dr, c[11]);
                        Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[12]));
                        TxtTaxInvoiceNo2.EditValue = Sm.DrStr(dr, c[13]);
                        Sm.SetDte(DteTaxInvoiceDt2, Sm.DrStr(dr, c[14]));
                        TxtTaxInvoiceNo3.EditValue = Sm.DrStr(dr, c[15]);
                        Sm.SetDte(DteTaxInvoiceDt3, Sm.DrStr(dr, c[16]));
                        mTaxCode1 = Sm.DrStr(dr, c[17]);
                        mTaxCode2 = Sm.DrStr(dr, c[18]);
                        mTaxCode3 = Sm.DrStr(dr, c[19]);
                        TxtTaxCode1.EditValue = Sm.DrStr(dr, c[20]);
                        TxtTaxCode2.EditValue = Sm.DrStr(dr, c[21]);
                        TxtTaxCode3.EditValue = Sm.DrStr(dr, c[22]);
                        TxtTaxAmt1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[23]), 0);
                        TxtTaxAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[24]), 0);
                        TxtTaxAmt3.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[25]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[26]);
                        TxtVoucherRequestPPNDocNo.EditValue = Sm.DrStr(dr, c[27]);
                        TxtVoucherRequestPPNDocNo2.EditValue = Sm.DrStr(dr, c[28]);
                        TxtVoucherRequestPPNDocNo3.EditValue = Sm.DrStr(dr, c[29]);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[30]);
                        TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[31]);
                        Sm.SetLue(LueServiceCode1, Sm.DrStr(dr, c[32]));
                        Sm.SetLue(LueServiceCode2, Sm.DrStr(dr, c[33]));
                        Sm.SetLue(LueServiceCode3, Sm.DrStr(dr, c[34]));
                    }, true
                );
        }

        private void ShowPurchaseReturnInvoiceSource(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT PRIDocNo, RecvVdDocNo, PIDocNo, GROUP_CONCAT(VoucherDocNo) VoucherDocNo ");
            SQL.AppendLine("FROM( ");
            SQL.AppendLine("	SELECT A.DocNo PRIDocNo, D.DocNo RecvVdDocNo, E.DocNo PIDocNo, G.DocNo OPDocNo, H.DocNo VoucherDocNo ");
            SQL.AppendLine("	FROM tblpurchasereturninvoicehdr A  ");
            SQL.AppendLine("	INNER JOIN tblpurchasereturninvoicedtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("	LEFT JOIN TblDOVdDtl C On B.DOVdDocNo=C.DocNo And B.DOVdDNo=C.DNo ");
            SQL.AppendLine("	LEFT JOIN tblrecvvddtl D ON C.RecvVdDocNo=D.DocNo AND C.RecvVdDNo=D.DNo ");
            SQL.AppendLine("	LEFT JOIN tblpurchaseinvoicedtl E ON D.DocNo=E.RecvVdDocNo AND D.DNo=E.RecvVdDNo ");
            SQL.AppendLine("	LEFT JOIN tbloutgoingpaymentdtl F ON E.DocNo=F.InvoiceDocNo ");
            SQL.AppendLine("	LEFT JOIN tbloutgoingpaymenthdr G ON F.DocNo=G.DocNo ");
            SQL.AppendLine("	LEFT JOIN tblvoucherhdr H ON G.VoucherRequestDocNo=H.VoucherRequestDocNo ");
            SQL.AppendLine("	GROUP BY A.DocNo, D.DocNo, E.DocNo, G.DocNo ");
            SQL.AppendLine(")X ");
            SQL.AppendLine("WHERE PRIDocNo=@DocNo ");
            SQL.AppendLine("GROUP BY PRIDocNo, RecvVdDocNo, PIDocNo ");

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "PRIDocNo",

                        //1-5
                        "RecvVdDocNo",
                        "PIDocNo",
                        "VoucherDocNo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtRecvVd.EditValue = Sm.DrStr(dr, c[1]);
                        TxtPurchaseInvoice.EditValue = Sm.DrStr(dr, c[2]);
                        TxtVoucher.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        private void ShowPurchaseReturnInvoiceDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.DOVdDocNo, A.DOVdDNo, B.DocDt, C.RecvVdDocNo, C.RecvVdDNo, D.PODocNo, D.PODNo, ");
            SQL.AppendLine("A.ItCode, I.ItName, A.Qty, I.InventoryUomCode, ");
            SQL.AppendLine("J.PtName, H.CurCode, A.UPrice, A.Discount, A.DiscountAmt, A.RoundingValue, A.Amt, ");
            SQL.AppendLine("A.TaxInvoiceNo, A.TaxInvoiceDt, ");
            SQL.AppendLine("A.TaxCode1, K.TaxName As TaxName1, ");
            SQL.AppendLine("A.TaxCode2, L.TaxName As TaxName2, ");
            SQL.AppendLine("A.TaxCode3, M.TaxName As TaxName3, ");
            SQL.AppendLine("A.Remark, A.SiteCode, O.SiteName, ");
            SQL.AppendLine("A.TaxInvoiceNo2, A.TaxInvoiceDt2, ");
            SQL.AppendLine("A.TaxInvoiceNo3, A.TaxInvoiceDt3 ");
            SQL.AppendLine("From TblPurchaseReturnInvoiceDtl A ");
            SQL.AppendLine("Left Join TblDOVdHdr B On A.DOVdDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblDOVdDtl C On A.DOVdDocNo=C.DocNo And A.DOVdDNo=C.DNo ");
            SQL.AppendLine("Left Join TblRecvVdDtl D On C.RecvVdDocNo=D.DocNo And C.RecvVdDNo=D.DNo ");
            SQL.AppendLine("Left Join TblPOHdr E On D.PODocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblPODtl F On D.PODocNo=F.DocNo And D.PODNo=F.DNo ");
            SQL.AppendLine("Left Join TblPORequestDtl G On F.PORequestDocNo=G.DocNo And F.PORequestDNo=G.DNo ");
            SQL.AppendLine("Left Join TblQtHdr H On G.QtDocNo=H.DocNo ");
            SQL.AppendLine("Left Join TblItem I On A.ItCode=I.ItCode ");
            SQL.AppendLine("Left Join TblPaymentTerm J On H.PtCode=J.PtCode ");
            SQL.AppendLine("Left Join TblTax K On A.TaxCode1=K.TaxCode ");
            SQL.AppendLine("Left Join TblTax L On A.TaxCode2=L.TaxCode ");
            SQL.AppendLine("Left Join TblTax M On A.TaxCode3=M.TaxCode ");
            SQL.AppendLine("Left Join TblSite O On A.SiteCode=O.SiteCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
                    
                    //1-5
                    "DOVdDocNo", "DOVdDNo", "DocDt", "RecvVdDocNo", "RecvVdDNo",  
                    
                    //6-10
                    "PODocNo", "PODNo", "ItCode", "ItName", "Qty", 
                    
                    //11-15
                    "InventoryUomCode", "PtName", "CurCode", "UPrice", "Discount",  
                    
                    //16-20
                    "DiscountAmt", "RoundingValue", "Amt", "TaxInvoiceNo", "TaxInvoiceDt", 
                    
                    //21-25
                    "TaxCode1", "TaxName1", "TaxCode2", "TaxName2", "TaxCode3", 
                    
                    //26-30
                    "TaxName3", "Remark", "SiteCode", "SiteName", "TaxInvoiceNo2", 
                    
                    //31-33
                    "TaxInvoiceDt2", "TaxInvoiceNo3", "TaxInvoiceDt3"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 19);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 27);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 28);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 29);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 30);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 36, 31);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 32);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 38, 33);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(Grd1, Grd1.Rows.Count - 1, new int[] { 15, 19, 20, 21, 22, 23 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowPurchaseReturnInvoiceDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.PurchaseInvoiceDocNo, A.PurchaseInvoiceDNo, A.AmtType, B.OptDesc, A.CurCode, A.Amt, A.Remark ");
            SQL.AppendLine("From TblPurchaseReturnInvoiceDtl2 A ");
            SQL.AppendLine("Inner Join TblOption B On A.AmtType=B.OptCode And B.OptCat='InvoiceAmtType' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
                    
                    //1-5
                    "PurchaseInvoiceDocNo", "PurchaseInvoiceDNo", "AmtType", "OptDesc", "CurCode",  
                    
                    //6-7
                    "Amt", "Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Grd.Cells[Row, 8].Value = Sm.FormatNum(0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(Grd2, Grd2.Rows.Count - 1, new int[] { 8, 9 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowPurchaseReturnInvoiceDtl3(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark, B.AcType ");
            SQL.AppendLine("From TblPurchaseReturnInvoiceDtl3 A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo", "AcDesc", "DAmt", "CAmt", "Remark", "AcType"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter() 
        {
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
            mIsSiteMandatory = Sm.GetParameter("IsSiteMandatory") == "Y";
            mIsPurchaseReturnInvoiceCanOnlyHave1Record = Sm.GetParameter("IsPurchaseReturnInvoiceCanOnlyHave1Record") == "Y";
            mDOtoVendorReplacedItem = Sm.GetParameter("DOtoVendorReplacedItem");
            mPurchaseReturnInvoiceShowSource = Sm.GetParameter("PurchaseReturnInvoiceShowSource") == "Y";
            if (mDOtoVendorReplacedItem.Length == 0) mDOtoVendorReplacedItem = "1";
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedDOVd()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0 &&
                        Sm.GetGrdStr(Grd1, Row, 3).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            Sm.GetGrdStr(Grd1, Row, 3) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedPO()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length != 0 &&
                        Sm.GetGrdStr(Grd2, Row, 3).Length != 0 &&
                        Sm.GetGrdStr(Grd2, Row, 5).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd2, Row, 2) +
                            Sm.GetGrdStr(Grd2, Row, 3) +
                            Sm.GetGrdStr(Grd2, Row, 5) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void ComputeAmt()
        {
            decimal 
                AmtBefTax = 0m, 
                AmtAftTax = 0m, 
                Tax = 0m;

            string Value = string.Empty;

            TxtAmtBefTax.EditValue = Sm.FormatNum(0m, 0);
            TxtAmt.EditValue = Sm.FormatNum(0m, 0);

            //TxtTaxInvoiceNo.EditValue = null;
            DteTaxInvoiceDt.EditValue = null;

            //TxtTaxInvoiceNo2.EditValue = null;
            DteTaxInvoiceDt2.EditValue = null;

            //TxtTaxInvoiceNo3.EditValue = null;
            DteTaxInvoiceDt3.EditValue = null;

            mTaxCode1 = string.Empty;
            mTaxCode2 = string.Empty;
            mTaxCode3 = string.Empty;

            TxtTaxCode1.EditValue = null;
            TxtTaxCode2.EditValue = null;
            TxtTaxCode3.EditValue = null;

            TxtTaxAmt1.EditValue = Sm.FormatNum(0m, 0);
            TxtTaxAmt2.EditValue = Sm.FormatNum(0m, 0);
            TxtTaxAmt3.EditValue = Sm.FormatNum(0m, 0);

            try
            {
                if (Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                        {
                            if (r == 0)
                            {
                                Value = Sm.GetGrdStr(Grd1, r, 18);
                                if (Value.Length > 0) TxtCurCode.EditValue = Value;

                                //Value = Sm.GetGrdStr(Grd1, r, 24);
                                //if (Value.Length > 0) TxtTaxInvoiceNo.EditValue = Value;

                                Value = Sm.GetGrdStr(Grd1, r, 25);
                                if (Value.Length > 0)
                                {
                                    Value = Sm.GetGrdDate(Grd1, r, 25).Substring(0, 8);
                                    Sm.SetDte(DteTaxInvoiceDt, Value);
                                }

                                //Value = Sm.GetGrdStr(Grd1, r, 35);
                                //if (Value.Length > 0) TxtTaxInvoiceNo2.EditValue = Value;

                                Value = Sm.GetGrdStr(Grd1, r, 36);
                                if (Value.Length > 0)
                                {
                                    Value = Sm.GetGrdDate(Grd1, r, 36).Substring(0, 8);
                                    Sm.SetDte(DteTaxInvoiceDt2, Value);
                                }

                                //Value = Sm.GetGrdStr(Grd1, r, 37);
                                //if (Value.Length > 0) TxtTaxInvoiceNo3.EditValue = Value;

                                Value = Sm.GetGrdStr(Grd1, r, 38);
                                if (Value.Length > 0)
                                {
                                    Value = Sm.GetGrdDate(Grd1, r, 38).Substring(0, 8);
                                    Sm.SetDte(DteTaxInvoiceDt3, Value);
                                }

                                Value = Sm.GetGrdStr(Grd1, r, 26);
                                if (Value.Length > 0) mTaxCode1 = Value;

                                Value = Sm.GetGrdStr(Grd1, r, 27);
                                if (Value.Length > 0) TxtTaxCode1.EditValue = Value;

                                Value = Sm.GetGrdStr(Grd1, r, 28);
                                if (Value.Length > 0) mTaxCode2 = Value;

                                Value = Sm.GetGrdStr(Grd1, r, 29);
                                if (Value.Length > 0) TxtTaxCode2.EditValue = Value;

                                Value = Sm.GetGrdStr(Grd1, r, 30);
                                if (Value.Length > 0) mTaxCode3 = Value;

                                Value = Sm.GetGrdStr(Grd1, r, 31);
                                if (Value.Length > 0) TxtTaxCode3.EditValue = Value;

                                Value = Sm.GetGrdStr(Grd1, r, 39);
                                if (Value.Length > 0) Sm.SetLue(LueServiceCode1, Value);

                                Value = Sm.GetGrdStr(Grd1, r, 40);
                                if (Value.Length > 0) Sm.SetLue(LueServiceCode2, Value);

                                Value = Sm.GetGrdStr(Grd1, r, 41);
                                if (Value.Length > 0) Sm.SetLue(LueServiceCode3, Value);
                            }
                            if (Sm.GetGrdStr(Grd1, r, 23).Length > 0) AmtBefTax += Sm.GetGrdDec(Grd1, r, 23);
                        }
                    }
                }

                if (Grd2.Rows.Count >= 1)
                {
                    for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd2, Row, 5).Length > 0 && Sm.GetGrdStr(Grd2, Row, 9).Length > 0)
                        {
                            switch (Sm.GetGrdStr(Grd2, Row, 5))
                            {
                                case "2":
                                    AmtBefTax += Sm.GetGrdDec(Grd2, Row, 9);
                                    break;
                                default:
                                    AmtBefTax -= Sm.GetGrdDec(Grd2, Row, 9);
                                    break;
                            }
                        }
                    }
                }

                var VdCode = Sm.GetLue(LueVdCode);
                var VendorAcNoAP = Sm.GetParameter("VendorAcNoAP");
                if (VdCode.Length>0 && VendorAcNoAP.Length>0)
                {
                    var AcNo=VendorAcNoAP+VdCode;
                    var AcType = string.Empty;
                    for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    {
                        AcType = Sm.GetGrdStr(Grd3, Row, 6);
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd3, Row, 1), AcNo))
                        {
                            if (Sm.GetGrdDec(Grd3, Row, 3) != 0)
                            {
                                if (AcType == "D")
                                    AmtBefTax -= Sm.GetGrdDec(Grd3, Row, 3);
                                else
                                    AmtBefTax += Sm.GetGrdDec(Grd3, Row, 3);
                            }
                            if (Sm.GetGrdDec(Grd3, Row, 4) != 0)
                            {
                                if (AcType == "C")
                                    AmtBefTax -= Sm.GetGrdDec(Grd3, Row, 4);
                                else
                                    AmtBefTax += Sm.GetGrdDec(Grd3, Row, 4);
                            }
                        }
                    }
                }

                TxtAmtBefTax.EditValue = Sm.FormatNum(AmtBefTax, 0);

                AmtAftTax = AmtBefTax;

                if (mTaxCode1.Length > 0)
                {
                    Tax = GetTaxRate(mTaxCode1) * AmtBefTax;
                    TxtTaxAmt1.EditValue = Sm.FormatNum(Tax, 0);
                    AmtAftTax += Tax;
                }

                if (mTaxCode2.Length > 0)
                {
                    Tax = GetTaxRate(mTaxCode2) * AmtBefTax;
                    TxtTaxAmt2.EditValue = Sm.FormatNum(Tax, 0);
                    AmtAftTax += Tax;
                }

                if (mTaxCode3.Length > 0)
                {
                    Tax = GetTaxRate(mTaxCode3) * AmtBefTax;
                    TxtTaxAmt3.EditValue = Sm.FormatNum(Tax, 0);
                    AmtAftTax += Tax;
                }

                TxtAmt.EditValue = Sm.FormatNum(AmtAftTax, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private decimal GetTaxRate(string TaxCode)
        {
            var TaxRate = Sm.GetValue("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode);
            if (TaxRate.Length > 0)
                return decimal.Parse(TaxRate)/100m;
            else
                return 0m;
        }

        private void ComputeOutstandingAmt()
        {
            var SQL = new StringBuilder();
            string Key = "'XXX'";

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0)
                    Key += (", '" + Sm.GetGrdStr(Grd2, Row, 2) + Sm.GetGrdStr(Grd2, Row, 3) + Sm.GetGrdStr(Grd2, Row, 5) + "'");
            }

            SQL.AppendLine("Select A.DocNo, B.DNo, B.AmtType, ");
            SQL.AppendLine("B.Amt-IfNull(( ");
            SQL.AppendLine("    Select Sum(T2.Amt) ");
            SQL.AppendLine("    From TblPurchaseReturnInvoiceHdr T1, TblPurchaseReturnInvoiceDtl2 T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo And T1.CancelInd='N' ");
            SQL.AppendLine("    And T2.PurchaseInvoiceDocNo=A.DocNo And T2.PurchaseInvoiceDNo=B.DNo And T2.AmtType=B.AmtType ");
            SQL.AppendLine("    ), 0) As OutstandingAmt ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A, TblPurchaseInvoiceDtl2 B ");
            SQL.AppendLine("Where A.DocNo=B.DocNo And B.AmtType In ('1', '3') ");
            SQL.AppendLine("And Concat(A.DocNo, B.DNo, B.AmtType) In ("+Key+") ");
            SQL.AppendLine("And A.CancelInd='N'");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "AmtType", "OutstandingAmt" });

                if (dr.HasRows)
                {
                    Grd2.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row <= Grd2.Rows.Count - 1; Row++)
                        {
                            if (
                                Sm.GetGrdStr(Grd2, Row, 2).Length > 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 2), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 3), Sm.DrStr(dr, 1)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 5), Sm.DrStr(dr, 2)))
                            {
                                Sm.SetGrdValue("N", Grd2, dr, c, Row, 8, 3);
                                break;
                            }
                        }
                    }
                    Grd2.EndUpdate();
                }
                dr.Close();
            }
        }

        private void SetLueVdCode(ref DXE.LookUpEdit Lue, string VdCode)
        {
            var SQL = new StringBuilder();

            if (VdCode.Length == 0)
            {
                SQL.AppendLine("Select Distinct A.VdCode As Col1, C.VdName As Col2 ");
                SQL.AppendLine("From TblDOVdHdr A ");
                SQL.AppendLine("Inner Join TblDOVdDtl B ");
                SQL.AppendLine("    On A.DocNo=B.DocNo ");
                SQL.AppendLine("    And B.CancelInd='N' ");
                SQL.AppendLine("    And Concat(B.DocNo, B.DNo) Not In ( ");
                SQL.AppendLine("        Select Concat(T2.DOVdDocNo, T2.DOVdDNo) ");
                SQL.AppendLine("        From TblPurchaseReturnInvoiceHdr T1, TblPurchaseReturnInvoiceDtl T2 ");
                SQL.AppendLine("        Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        And T1.CancelInd='N') ");
                SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
                SQL.AppendLine("Order By C.VdName; ");
            }
            else
                SQL.AppendLine("Select VdCode As Col1, VdName As Col2 From TblVendor Where VdCode='" + VdCode + "';");
            
            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void PrintData(string DocNo)
        {
            var l = new List<InvHdr>();
            var ldtl = new List<InvDtl>();
            var ldtl2 = new List<InvDtl2>();

            string[] TableName = { "InvHdr", "InvDtl", "InvDtl2" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
           
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='IsPOSplitBasedOnTax') As 'Kop', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, C.VdName, A.Remark, ");
            SQL.AppendLine("ifnull(if(B.Damt!=0,B.DAmt,B.CAmt),0)As Discount, D.UserCode, ifnull(F.AcAmt, 0) AS AcAmt ");
            SQL.AppendLine("From TblPurchaseReturnInvoiceHdr A ");
            SQL.AppendLine("Left Join TblPurchaseInvoiceDtl4 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select X.DocNo,  X.AcNo, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("    When X.AcType = 'D' && X.DAmt>0 Then X.DAmt");
            SQL.AppendLine("    When X.AcType = 'D' && X.CAmt>0 Then X.CAmt *-1");
            SQL.AppendLine("    When X.AcType = 'C' && X.CAmt>0 Then X.Camt");
            SQL.AppendLine("    When X.AcType = 'C' && X.DAmt>0 Then X.DAmt *-1");
            SQL.AppendLine("    End As AcAmt");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    (");
            SQL.AppendLine("        Select A.DocNo, A.AcNo, B.Actype, A.DAmt, A.Camt");
            SQL.AppendLine("        From TblPurchaseInvoiceDtl4 A");
            SQL.AppendLine("        Inner Join TblCoa B On A.AcNo = B.Acno");
            SQL.AppendLine("        Inner Join ");
            SQL.AppendLine("        (");
            SQL.AppendLine("            Select ParValue, Length(Parvalue) LPar From TblParameter Where ParCode = 'VendorAcNoAP' ");
            SQL.AppendLine("        )C On 0=0");
            SQL.AppendLine("        Where Left(B.AcNo, C. LPar) = C.ParValue ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine(")F ON A.DocNo = F.DocNo ");

            SQL.AppendLine("Where A.DocNo=@DocNo ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",
                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo",
                         "DocDt",
                         //6-10
                         "VdName",
                         "Remark",
                         "Discount",
                         "UserCode",
                         "AcAmt",

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new InvHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),

                            VdName = Sm.DrStr(dr, c[6]),
                            Remark = Sm.DrStr(dr, c[7]),
                            Discount = Sm.DrDec(dr, c[8]),
                            UserCode = Sm.DrStr(dr, c[9]),
                            AcAmt = Sm.DrDec(dr, c[10]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail data
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.DocNo, C.RecvVdDocNo, D.PODocNo, C.ItCode, K.ItName, C.Qty, K.InventoryUomCode, ");
                SQLDtl.AppendLine("L.PtName, I.CurCode, J.UPrice, F.DiscountAmt, ");
                SQLDtl.AppendLine("(C.Qty*J.UPrice)As Total, A.Remark, ((J.UPrice*F.Discount)/100*D.Qty) As Discount, ");
                SQLDtl.AppendLine("(((((C.Qty/D.Qty)*D.QtyPurchase)* J.UPrice * Case When IfNull(F.Discount, 0)=0 Then 1 Else (100-F.Discount)/100 End)-((((C.Qty/D.Qty)*D.QtyPurchase)/F.Qty)*F.DiscountAmt)+((((C.Qty/D.Qty)*D.QtyPurchase)/F.Qty)*F.RoundingValue))*Case When IfNull(M.TaxRate, 0)=0 Then 0 Else M.TaxRate/100 End) As TaxAmt1,"); 
                SQLDtl.AppendLine("(((((C.Qty/D.Qty)*D.QtyPurchase)* J.UPrice * Case When IfNull(F.Discount, 0)=0 Then 1 Else (100-F.Discount)/100 End)-((((C.Qty/D.Qty)*D.QtyPurchase)/F.Qty)*F.DiscountAmt)+((((C.Qty/D.Qty)*D.QtyPurchase)/F.Qty)*F.RoundingValue))*Case When IfNull(N.TaxRate, 0)=0 Then 0 Else N.TaxRate/100 End) As TaxAmt2,");
                SQLDtl.AppendLine("(((((C.Qty/D.Qty)*D.QtyPurchase)* J.UPrice * Case When IfNull(F.Discount, 0)=0 Then 1 Else (100-F.Discount)/100 End)-((((C.Qty/D.Qty)*D.QtyPurchase)/F.Qty)*F.DiscountAmt)+((((C.Qty/D.Qty)*D.QtyPurchase)/F.Qty)*F.RoundingValue))*Case When IfNull(O.TaxRate, 0)=0 Then 0 Else O.TaxRate/100 End) As TaxAmt3"); 
                SQLDtl.AppendLine("From TblPurchaseReturnInvoiceDtl A ");
                SQLDtl.AppendLine("Left Join TblDOVdHdr B On A.DOVdDocNo=B.DocNo ");
                SQLDtl.AppendLine("Left Join TblDOVdDtl C On A.DOVdDocNo=C.DocNo And A.DOVdDNo=C.DNo ");
                SQLDtl.AppendLine("Left Join TblRecvVdDtl D On C.RecvVdDocNo=D.DocNo And C.RecvVdDNo=D.DNo ");
                SQLDtl.AppendLine("Left Join TblPOHdr E On D.PODocNo=E.DocNo ");
                SQLDtl.AppendLine("Left Join TblPODtl F On D.PODocNo=F.DocNo And D.PODNo=F.DNo ");
                SQLDtl.AppendLine("Left Join TblPORequestDtl G On F.PORequestDocNo=G.DocNo And F.PORequestDNo=G.DNo ");
                SQLDtl.AppendLine("Left Join TblMaterialRequestDtl H On G.MaterialRequestDocNo=H.DocNo And G.MaterialRequestDNo=H.DNo ");
                SQLDtl.AppendLine("Left Join TblQtHdr I On G.QtDocNo=I.DocNo ");
                SQLDtl.AppendLine("Left Join TblQtDtl J On G.QtDocNo=J.DocNo And G.QtDNo=J.DNo ");
                SQLDtl.AppendLine("Left Join TblItem K On C.ItCode=K.ItCode ");
                SQLDtl.AppendLine("Left Join TblPaymentTerm L On I.PtCode=L.PtCode ");
                SQLDtl.AppendLine("Left Join TblTax M On E.TaxCode1=M.TaxCode ");
                SQLDtl.AppendLine("Left Join TblTax N On E.TaxCode2=N.TaxCode ");
                SQLDtl.AppendLine("Left Join TblTax O On E.TaxCode3=O.TaxCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");


                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DocNo" ,

                         //1-5
                         "RecvVdDocNo",
                         "PODocNo",
                         "ItCode",
                         "ItName",
                         "Qty",

                         //6-10
                         "InventoryUomCode",
                         "PtName" ,
                         "CurCode" ,
                         "UPrice",
                         "Discount",

                         //11-15
                         "DiscountAmt",
                         "Total",
                         "Remark",
                         "TaxAmt1",
                         "TaxAmt2",

                         //16
                         "TaxAmt3",

                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new InvDtl()
                        {
                            DocNo = Sm.DrStr(drDtl, cDtl[0]),

                            RecvVdDocNo = Sm.DrStr(drDtl, cDtl[1]),
                            PODocNo = Sm.DrStr(drDtl, cDtl[2]),
                            ItCode = Sm.DrStr(drDtl, cDtl[3]),
                            ItName = Sm.DrStr(drDtl, cDtl[4]),
                            Qty = Sm.DrDec(drDtl, cDtl[5]),

                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[6]),
                            PtName = Sm.DrStr(drDtl, cDtl[7]),
                            CurCode = Sm.DrStr(drDtl, cDtl[8]),
                            UPrice = Sm.DrDec(drDtl, cDtl[9]),
                            Discount = Sm.DrDec(drDtl, cDtl[10]),

                            DiscountAmt = Sm.DrDec(drDtl, cDtl[11]),
                            Total = Sm.DrDec(drDtl, cDtl[12]),
                            Remark = Sm.DrStr(drDtl, cDtl[13]),
                            TaxAmt1 = Sm.DrDec(drDtl, cDtl[14]),
                            TaxAmt2 = Sm.DrDec(drDtl, cDtl[15]),

                            TaxAmt3 = Sm.DrDec(drDtl, cDtl[16]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail data
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select DocNo, sum(DAmt)As Damt, sum(Camt)As Camt from TblPurchaseReturnInvoiceDtl3 ");
                SQLDtl2.AppendLine("Where DocNo=@DocNo ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "DocNo",
                         //1-2
                         "DAmt",
                         "CAmt",

                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new InvDtl2()
                        {
                            DocNo = Sm.DrStr(drDtl2, cDtl2[0]),
                            DAmt = Sm.DrDec(drDtl2, cDtl2[1]),
                            CAmt = Sm.DrDec(drDtl2, cDtl2[2]),

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion


            Sm.PrintReport("PurchaseReturnInvoice", myLists, TableName, false);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
                Grd1.Font = new Font(Grd1.Font.FontFamily.Name.ToString(), int.Parse(Sm.GetLue(LueFontSize)));
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(SetLueVdCode), "");
            ClearGrd1();
            ClearGrd2();
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueServiceCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueServiceCode1, new Sm.RefreshLue1(Sl.SetLueServiceCode));
        }

        private void LueServiceCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueServiceCode2, new Sm.RefreshLue1(Sl.SetLueServiceCode));
        }

        private void LueServiceCode3_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueServiceCode3, new Sm.RefreshLue1(Sl.SetLueServiceCode));
        }

        #endregion

        #region Grid Event

        #region Grid 1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmPurchaseReturnInvoiceDlg(this, Sm.GetLue(LueVdCode)));
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmDOVd(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmPO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmPO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 13 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                f.ShowDialog();
            }

            if (TxtDocNo.Text.Length == 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0 && e.ColIndex == 32)
                e.DoDefault = false;
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueVdCode, "Vendor")) Sm.FormShowDialog(new FrmPurchaseReturnInvoiceDlg(this, Sm.GetLue(LueVdCode)));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmDOVd(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmPO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 13 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmt();
            }

            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);

            if (mPurchaseReturnInvoiceShowSource && Grd1.Rows.Count <= 1 )
            {
                TxtRecvVd.EditValue = string.Empty;
                TxtPurchaseInvoice.EditValue = string.Empty;
                TxtVoucher.EditValue = string.Empty;
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 32 }, e);
        }

        #endregion

        #region Grid 2

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmPurchaseReturnInvoiceDlg2(this, Sm.GetLue(LueVdCode)));
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (TxtDocNo.Text.Length == 0 &&
                Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length == 0 &&
                (e.ColIndex == 9 || e.ColIndex == 10))
                e.DoDefault = false;
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            ComputeAmt();
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueVdCode, "Vendor")) Sm.FormShowDialog(new FrmPurchaseReturnInvoiceDlg2(this, Sm.GetLue(LueVdCode)));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 9)
            {
                if (Sm.GetGrdStr(Grd2, e.RowIndex, e.ColIndex).Length == 0)
                    Grd2.Cells[e.RowIndex, e.ColIndex].Value = 0m;
                ComputeAmt();
            }
        }

        #endregion

        #region Grid 3

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
            {
                Grd3.Cells[e.RowIndex, 4].Value = 0;
                ComputeAmt();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
            {
                Grd3.Cells[e.RowIndex, 3].Value = 0;
                ComputeAmt();
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmPurchaseReturnInvoiceDlg3(this));
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmPurchaseReturnInvoiceDlg3(this));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeAmt();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #endregion

        #region class

        private class InvHdr
        {
            public string CompanyLogo { set; get; }

            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdName { get; set; }
            public string DueDate { get; set; }
            public string Remark { get; set; }
            public string VdInvDt { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal DownPayment { get; set; }
            public decimal Discount { get; set; }
            public string UserCode { get; set; }
            public string LocalDocNo { get; set; }
            public decimal AcAmt { get; set; }
            public string PrintBy { get; set; }
        }

        private class InvDtl
        {
            public string DocNo { get; set; }
            public string RecvVdDocNo { get; set; }
            public string PODocNo { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string InventoryUomCode { get; set; }
            public string PtName { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice { get; set; }
            public decimal Discount { get; set; }
            public decimal DiscountAmt { get; set; }
            public decimal Total { get; set; }
            public string Remark { get; set; }
            public decimal TaxAmt1 { get; set; }
            public decimal TaxAmt2 { get; set; }
            public decimal TaxAmt3 { get; set; }
        }

        private class InvDtl2
        {
            public string DocNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        #endregion

    }
}
