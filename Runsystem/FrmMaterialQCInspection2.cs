﻿#region Update
/*
    16/10/2017 [WED] rubah posisi Grd1 dan Grd2
    24/10/2017 [HAR] validasi quantity, bandingin dgn qty planning 2 bukan qty planning 1
    30/10/2017 [WED] indicator di detail di ambil dari qc parameter yang dipilih di detail
    07/10/2017 [TKG] menggunakan data dari master inspector
    22/11/2017 [WED] Qty bisa diisi 0
    28/01/2018 [TKG] Inspection date harus diisi
    14/12/2021 [DEV/IOK] menambahkan kolom Work Center pada List of Planning Quantity, agar bisa inspect item sesuai work center
    13/01/2022 [SET/IOK] Parameter baru IsQCParameterInDetail
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialQCInspection2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mWhsCode = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal int mStateQCPIndicator = 0; // indikator untuk tombol QC Planning aktif jika dia insert
        internal FrmMaterialQCInspection2Find FrmFind;
        private string mNumberOfInventoryUomCode = string.Empty;
        internal bool 
            mIsMaterialQCInspection2UseWorkCenter = false,
            mIsQCParameterInDetail = false;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmMaterialQCInspection2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Material QC Inspection (Quality)";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                SetLueStatus(ref LueStatus);
                SetLueQCParameter(ref LueQCParameter);
                LueStatus.Visible = false;
                LueQCParameter.Visible = false;
                if (!mIsMaterialQCInspection2UseWorkCenter)
                {
                    label6.Visible = false;
                    TxtWorkCenter.Visible = false;
                }
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {

            #region Grid 1 (QC Planning's Detail Item)

            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "",
                        "QCPCode",

                        //6-10
                        "Status",
                        "QCParameterCode",
                        "Parameter",
                        "Item's Code",
                        "",
                        
                        //11-15
                        "Item's"+Environment.NewLine+"Local Code",
                        "Item's Name", 
                        "Batch Number", 
                        "Lot",
                        "Bin",

                        //16-20
                        "Planning"+Environment.NewLine+"Quantity",
                        "UoM",
                        "Planning"+Environment.NewLine+"Quantity 2",
                        "UoM 2",
                        "Planning"+Environment.NewLine+"Quantity 3",

                        //21-25
                        "UoM 3",
                        "Quantity",
                        "Indicator",
                        "QC Planning's Remark",
                        "Remark",
                        
                        //26
                        "QCPlanningDNo"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        50, 50, 20, 20, 0,
                        
                        //6-10
                        100, 20, 180, 80, 20, 
                        
                        //11-15
                        100, 250, 200, 60, 80, 
                        
                        //16-20
                        80, 80, 80, 80, 80, 
                        
                        //21-25
                        80, 130, 150, 200, 200, 
                        
                        //26
                        100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 16, 18, 20, 22 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 4, 10 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 5, 7, 9, 10, 11, 24, 26 }, false);

            if (mNumberOfInventoryUomCode == "1")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21 }, false);
            }

            if (mNumberOfInventoryUomCode == "2")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 20, 21 }, false);
            }

            #endregion

            #region Grid 2 (Inspector)

            Grd2.Cols.Count = 3;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "",

                        //1-2
                        "Inspector's Code",
                        "Inspector's Name"
                    },
                    new int[]{ 20, 0, 200 }
                );
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdColInvisible(Grd2, new int[] { 1 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 2 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 24 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtQCPlanningDocNo, TxtWhsCode, TxtWorkCenter, TmeStartTm, LueStatus, DteStartDt, MeeRemark, LueQCParameter
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0 });

                    BtnQCPlanningDocNo.Enabled = false;
                    BtnQCPlanningDocNo2.Enabled = true;
                    BtnAddItem.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TmeStartTm, MeeRemark, LueStatus, DteStartDt, LueQCParameter
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 6, 8, 10, 22, 25 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0 });
                    BtnQCPlanningDocNo.Enabled = true;
                    BtnQCPlanningDocNo2.Enabled = true;
                    BtnAddItem.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtQCPlanningDocNo, TxtWhsCode, TxtWorkCenter, TmeStartTm, DteStartDt, MeeRemark, LueStatus, LueQCParameter
            });
            mWhsCode = string.Empty;
            mStateQCPIndicator = 0;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.FocusGrd(Grd2, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 16, 18, 20, 22 });
            Sm.ClearGrd(Grd2, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmMaterialQCInspection2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                mStateQCPIndicator = 1;
                SetFormControl(mState.Insert);
                BtnQCPlanningDocNo.Enabled = true;
                Sm.SetDteCurrentDate(DteDocDt);
                BtnQCPlanningDocNo_Click(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            //Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 3 }, e);
            //if (e.ColIndex == 20)
            //{
            //    if (Sm.GetGrdDec(Grd1, e.RowIndex, 14) < Sm.GetGrdDec(Grd1, e.RowIndex, 20))
            //    {
            //        Sm.StdMsg(mMsgType.Info, "Quantity should be smaller than Planning Quantity.");
            //        Grd1.Cells[e.RowIndex, 20].Value = Sm.FormatNum(0m, 0);
            //        Sm.FocusGrd(Grd1, e.RowIndex, 20);
            //        return;
            //    }
            //    else
            //    {
            //        Grd1.Cells[e.RowIndex, 21].Value = Sm.FormatNum(ComputeResidualQty(Sm.GetGrdDec(Grd1, e.RowIndex, 14), Sm.GetGrdDec(Grd1, e.RowIndex, 20)), 0);
            //        Sm.FocusGrd(Grd1, e.RowIndex + 1, 20);
            //    }
            //}
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (Sm.IsGrdColSelected(new int[] { 3, 4, 6, 8 }, e.ColIndex))
                    {
                        if (e.ColIndex == 6) LueRequestEdit(Grd1, LueStatus, ref fCell, ref fAccept, e);
                        if (e.ColIndex == 8) LueRequestEdit(Grd1, LueQCParameter, ref fCell, ref fAccept, e);

                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 22 });
                        SetLueStatus(ref LueStatus);
                        SetLueQCParameter(ref LueQCParameter);
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmItem("XXX");
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 0)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ") && !Sm.IsTxtEmpty(TxtQCPlanningDocNo, "Planning Document#", false))
                            Sm.FormShowDialog(new FrmMaterialQCInspection2Dlg2(this));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    }
                }
            }
            
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmMaterialQCInspection2Dlg2(this));
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "MaterialQCInspection2", "TblMaterialQCInspection2Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveMaterialQCInspection2Hdr(DocNo));
            
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 9).Length > 0)
                    cml.Add(SaveMaterialQCInspection2Dtl(DocNo, Row));
            
            for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                if (Sm.GetGrdStr(Grd2, Row2, 2).Length > 0)
                    cml.Add(SaveMaterialQCInspection2Dtl2(DocNo, Row2));
            
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtQCPlanningDocNo, "Planned document#", false) ||
                Sm.IsDteEmpty(DteStartDt, "Inspection date") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsQCStatusStillOnHold() ||
                IsGrdValueNotValid() ||
                IsGrdValueDuplicate() ||
                IsMaterialQtyBiggerThanPlanningQty() ||
                IsGrd2Empty() ||
                IsGrd2ExceedMaxRecords() ||
                IsGrd2ValueNotValid();
        }

        private bool IsMaterialQtyBiggerThanPlanningQty()
        {
            decimal MaterialQty = 0m, PlanningQty = 0m;

            for (int r1 = 0; r1 < Grd1.Rows.Count - 1; r1++)
            {
                PlanningQty = Sm.GetGrdDec(Grd1, r1, 18);
                MaterialQty = Sm.GetGrdDec(Grd1, r1, 22);
                for (int r2 = (r1 + 1); r2 < Grd1.Rows.Count - 1; r2++)
                {
                    if(
                        Sm.GetGrdStr(Grd1, r1, 9) == Sm.GetGrdStr(Grd1, r2, 9) &&
                        Sm.GetGrdStr(Grd1, r1, 13) == Sm.GetGrdStr(Grd1, r2, 13) &&
                        Sm.GetGrdStr(Grd1, r1, 7) == Sm.GetGrdStr(Grd1, r2, 7)
                        )
                    {
                        MaterialQty += Sm.GetGrdDec(Grd1, r2, 22);
                    }
                }

                if (MaterialQty > PlanningQty)
                {
                    Sm.StdMsg(mMsgType.Info, "The quantity's summary should be smaller or equal to Planning quantity 2.");
                    Sm.FocusGrd(Grd1, r1, 22);
                    return true;
                }
            }
            
            return false;
        }

        private bool IsQCStatusStillOnHold()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                string QCStatusCode = Sm.GetGrdStr(Grd1, Row, 5);
                if (Sm.CompareStr(QCStatusCode,"2"))
                {
                    Sm.StdMsg(mMsgType.Warning, "QC Status is still On Hold.");
                    Sm.FocusGrd(Grd1, Row, 6);
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No detail planning item is recorded.");
                Grd1.Focus();
                return true;
            }
            return false;
        }

        private bool IsGrd2Empty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Inspector.");
                Grd2.Focus();
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrd2ExceedMaxRecords()
        {
            if (Grd2.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Inspector entered (" + (Grd2.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 6, false, "Status is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 8, false, "Parameter is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 9, false, "Item is empty.")
                    ) return true;
            }
            return false;
        }

        private bool IsGrdValueDuplicate()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                for (int Row2 = (Row + 1); Row2 < Grd1.Rows.Count - 1; Row2++)
                {
                    if( 
                        Sm.GetGrdStr(Grd1, Row, 5) == Sm.GetGrdStr(Grd1, Row2, 5) &&
                        Sm.GetGrdStr(Grd1, Row, 7) == Sm.GetGrdStr(Grd1, Row2, 7) &&
                        Sm.GetGrdStr(Grd1, Row, 9) == Sm.GetGrdStr(Grd1, Row2, 9) &&
                        Sm.GetGrdStr(Grd1, Row, 13) == Sm.GetGrdStr(Grd1, Row2, 13)
                        )
                    {
                        Sm.StdMsg(mMsgType.Warning, "Row #"+(Row + 1)+" and #"+(Row2 + 1)+" ."+
                            Environment.NewLine + Environment.NewLine +
                            "Cannot inspect with the same Parameter and QC Status on this batch item more than once.");
                        Sm.FocusGrd(Grd1, Row, 6);
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrd2ValueNotValid()
        {
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Inspector is empty.")) return true;
            return false;
        }

        private MySqlCommand SaveMaterialQCInspection2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialQCInspection2Hdr(DocNo, DocDt, QCPlanningDocNo, StartDt, StartTm, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @QCPlanningDocNo, @StartDt, @StartTm, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblQCPlanningHdr ");
            SQL.AppendLine("  Set ProcessInd = '3', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @QCPlanningDocNo And ActInd = 'Y'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@QCPlanningDocNo", TxtQCPlanningDocNo.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParam<String>(ref cm, "@StartTm", Sm.GetTme(TmeStartTm));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveMaterialQCInspection2Dtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblMaterialQCInspection2Dtl(DocNo, DNo, QCPlanningDNo, CancelInd, Status, QCPCode, Qty, Remark, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @QCPlanningDNo, 'N', @Status, @Parameter, @Qty, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@Parameter", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@QCPlanningDNo", Sm.GetGrdStr(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveMaterialQCInspection2Dtl2(string DocNo, int Row)
        {
            var SQLDtl3 = new StringBuilder();

            SQLDtl3.AppendLine("Insert Into TblMaterialQCInspection2Dtl2(DocNo, DNo, EmpCode, CreateBy, CreateDt) ");
            SQLDtl3.AppendLine("Values(@DocNo, @DNo, @EmpCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl3.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            int rowcount = Grd1.Rows.Count - 1;
            UpdateCancelledMaterialQCInspection();

            string DNo = "'XXX'";
            string QCPDNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 9).Length > 0)
                {
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";
                    QCPDNo = QCPDNo + ",'" + Sm.GetGrdStr(Grd1, Row, 26)+ "'";
                }
            }

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelMaterialQCInspection2Dtl(DNo, QCPDNo));

            if(AllDataCancelled())
            {
                cml.Add(UpdateQCPlanningProcessInd(TxtQCPlanningDocNo.Text));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool AllDataCancelled()
        {
            bool flag = false;

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) == true)
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                    break;
                }
            }

            return flag;
        }

        private void UpdateCancelledMaterialQCInspection()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, CancelInd ");
            SQL.AppendLine("From TblMaterialQCInspection2Dtl ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By DNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Index = 0; Index < Grd1.Rows.Count - 1; Index++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Index, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Index, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Index, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return IsCancelledDataNotExisted(DNo);
        }

        private bool IsCancelledDataNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelMaterialQCInspection2Dtl(string DNo, string QCPDNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialQCInspection2Dtl Set ");
            SQL.AppendLine("    CancelInd='Y', Status='2', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DNo In (" + DNo + "); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateQCPlanningProcessInd(string QCPlanningDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblQCPlanningHdr Set ");
            SQL.AppendLine("    ProcessInd = '0', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@QCPlanningDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@QCPlanningDocNo", QCPlanningDocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowMaterialQCInspection2Hdr(DocNo);
                ShowMaterialQCInspection2Dtl(DocNo);
                ShowMaterialQCInspection2Dtl2(DocNo);

                for (int Col = 26; Col < Grd1.Cols.Count; Col++)
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { Col });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        internal void ShowDataPlanning(string PlanningDocNo)
        {
            try
            {
                ClearGrd();
                ShowPlanningDtl(PlanningDocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPlanningDtl(string PlanningDocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PlanningDocNo", PlanningDocNo);

            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("SELECT A.DNo, 'N' AS CancelInd, A.Status, C.OptDesc As StatusDesc, E.QCPIndicator, ");
            SQLDtl.AppendLine("A.ItCode, A.BatchNo, A.Lot, A.Bin, A.Qty, A.Qty2, A.Qty3, A.Remark, ");
            SQLDtl.AppendLine("B.ItCodeInternal, B.ItName, B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3 ");
            SQLDtl.AppendLine("FROM TblQCPlanningDtl A ");
            SQLDtl.AppendLine("INNER JOIN TblItem B ON A.ItCode=B.ItCode ");
            SQLDtl.AppendLine("INNER JOIN TblOption C ON C.OptCat='QCStatus' AND A.Status=C.OptCode ");
            SQLDtl.AppendLine("INNER JOIN TblQCPlanningHdr D On A.DocNo = D.DocNo ");
            SQLDtl.AppendLine("LEFT JOIN TblQCParameter E ON D.QCPCode = E.QCPCode ");
            SQLDtl.AppendLine("Where A.DocNo=@PlanningDocNo AND A.Status = '2' And D.ProcessInd = '0' ");
            SQLDtl.AppendLine("Order By A.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQLDtl.ToString(),
                new string[] 
                { 
                    //0
                    "CancelInd", 

                    //1-5
                    "Status", "StatusDesc", "ItCode", "ItCodeInternal", "ItName",  
                    
                    //6-10
                    "BatchNo","Lot", "Bin", "Qty", "InventoryUomCode", 
                    
                    //11-15
                    "Remark", "DNo", "Qty2", "InventoryUomCode2", "Qty3", 
                    
                    //16
                    "InventoryUomCode3"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 8);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 9);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 10);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 13);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 14);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 20, 15);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 16);
                    //Sm.SetGrdValue("S", Grd1, dr, c, Row, 23, 17);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 11);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 26, 12);
                    Sm.SetGrdNumValueZero(ref Grd1, Row, new int[] { 22 });
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 16, 18, 20, 22 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowMaterialQCInspection2Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.DocNo, A.DocDt, A.QCPlanningDocNo, C.WhsName, D.DocName, ");
            SQL.AppendLine("A.StartDt, A.StartTm, A.Remark ");
            SQL.AppendLine("From TblMaterialQCInspection2Hdr A ");
            SQL.AppendLine("Inner Join TblQCPlanningHdr B On A.QCPlanningDocNo=B.DocNo ");
            if (mIsMaterialQCInspection2UseWorkCenter)
                SQL.AppendLine("Left Join TblWarehouse C On B.WhsCode=C.WhsCode ");
            else
                SQL.AppendLine("Inner Join TblWarehouse C On B.WhsCode=C.WhsCode ");
            SQL.AppendLine("Left Join TblWorkCenterHdr D On B.WorkCenterDocNo = D.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.ShowDataInCtrl(
                ref cm,
                SQL.ToString(),
                new string[] { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "QCPlanningDocNo", "WhsName", "DocName", "StartDt", 

                    //6-7
                    "StartTm", "Remark" 
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtQCPlanningDocNo.EditValue = Sm.DrStr(dr, c[2]);
                    TxtWhsCode.EditValue = Sm.DrStr(dr, c[3]);                 
                    if (mIsMaterialQCInspection2UseWorkCenter)
                        TxtWorkCenter.EditValue = Sm.DrStr(dr, c[4]);
                    Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[5]));
                    Sm.SetTme(TmeStartTm, Sm.DrStr(dr, c[6]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                }, true
            );
        }

        private void ShowMaterialQCInspection2Dtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, B.DNo AS QCPlanningDNo, A.CancelInd, A.Status, D.OptDesc As StatusDesc, G.QCPIndicator, ");
            SQL.AppendLine("B.ItCode, B.BatchNo, B.Lot, B.Bin, A.QCPCode, H.QCPDesc, A.Qty As InspectedQty, B.Qty, B.Qty2, B.Qty3, B.Remark AS PlanningRemark, ");
            SQL.AppendLine("C.ItCodeInternal, C.ItName, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, A.Remark ");
            SQL.AppendLine("From TblMaterialQCInspection2Dtl A ");
            SQL.AppendLine("INNER JOIN TblMaterialQCInspection2Hdr E ON A.DocNo = E.DocNo ");
            SQL.AppendLine("INNER JOIN TblQCPlanningDtl B ON E.QCPlanningDocNo = B.DocNo AND B.DNo = A.QCPlanningDNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join TblOption D On D.OptCat='QCStatus' And A.Status=D.OptCode ");
            SQL.AppendLine("Inner Join TblQCPlanningHdr F On B.DocNo = F.DocNo ");
            SQL.AppendLine("Left Join TblQCParameter G On A.QCPCode = G.QCPCode ");
            SQL.AppendLine("Inner Join TblQCParameter H On A.QCPCode = H.QCPCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "Status", "StatusDesc", "ItCode", "ItCodeInternal",  
                    
                    //6-10
                    "ItName","BatchNo", "Lot", "Bin", "Qty", 
                    
                    //11-15
                    "InventoryUomCode", "PlanningRemark", "InspectedQty", "Remark", "QCPlanningDNo",

                    //16-20
                    "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", "QCPIndicator", 

                    //21-22
                    "QCPCode", "QCPDesc"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 21);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 22);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 9);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 10);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 20, 18);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 19);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 22, 13);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 12);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 22, 13);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 25, 14);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 26, 15);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 16, 18, 20, 22 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowMaterialQCInspection2Dtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Select A.DNo, A.EmpCode, B.InspectorName ");
            SQLDtl2.AppendLine("From TblMaterialQCInspection2Dtl2 A ");
            SQLDtl2.AppendLine("Left Join TblInspector B On A.EmpCode=B.InspectorCode ");
            SQLDtl2.AppendLine("Where A.DocNo=@DocNo ");
            SQLDtl2.AppendLine("Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQLDtl2.ToString(),
                new string[] { "DNo", "EmpCode", "InspectorName" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row2) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row2, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row2, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row2, 2, 2);
                }, false, false, true, false
            );
        }

        #endregion

        #region Additional Method

        private void ReLoadItem(string PlanningDocNo)
        {
            var cm = new MySqlCommand();
            var l = new List<PlanningItem>();
            var SQLR = new StringBuilder();

            SQLR.AppendLine("SELECT A.DNo, 'N' AS CancelInd, A.Status, C.OptDesc As StatusDesc, E.QCPIndicator, ");
            SQLR.AppendLine("A.ItCode, A.BatchNo, A.Lot, A.Bin, A.Qty, A.Remark, ");
            SQLR.AppendLine("B.ItCodeInternal, B.ItName, B.InventoryUomCode, B.InventoryUOMCode2, B.InventoryUOMCode3, ");
            SQLR.AppendLine("A.Qty2, A.Qty3 ");
            SQLR.AppendLine("FROM TblQCPlanningDtl A ");
            SQLR.AppendLine("INNER JOIN TblItem B ON A.ItCode=B.ItCode ");
            SQLR.AppendLine("INNER JOIN TblOption C ON C.OptCat='QCStatus' AND A.Status=C.OptCode ");
            SQLR.AppendLine("Inner Join TblQCPlanningHdr D On A.DocNo = D.DocNo ");
            SQLR.AppendLine("Left Join TblQCParameter E On D.QCPCode = E.QCPCode ");
            SQLR.AppendLine("Where A.DocNo=@PlanningDocNo AND A.Status = '2' And D.ProcessInd = '0' ");
            SQLR.AppendLine("Order By A.DNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQLR.ToString();
                Sm.CmParam<String>(ref cm, "@PlanningDocNo", PlanningDocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "CancelInd", 

                    //1-5
                    "Status", "StatusDesc", "ItCode", "ItCodeInternal", "ItName",  
                    
                    //6-10
                    "BatchNo","Lot", "Bin", "Qty", "InventoryUomCode", 
                    
                    //11-15
                    "Remark", "DNo", "Qty2", "InventoryUOMCode2", "Qty3",

                    //16
                    "InventoryUOMCode3"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PlanningItem()
                        {
                            CancelInd = Sm.DrStr(dr, c[0]),
                            Status = Sm.DrStr(dr, c[1]),
                            StatusDesc = Sm.DrStr(dr, c[2]),
                            ItCode = Sm.DrStr(dr, c[3]),
                            ItCodeInternal = Sm.DrStr(dr, c[4]),
                            ItName = Sm.DrStr(dr, c[5]),
                            BatchNo = Sm.DrStr(dr, c[6]),
                            Lot = Sm.DrStr(dr, c[7]),
                            Bin = Sm.DrStr(dr, c[8]),
                            Qty = Sm.DrDec(dr, c[9]),
                            Uom1 = Sm.DrStr(dr, c[10]),
                            Remark = Sm.DrStr(dr, c[11]),
                            DNo = Sm.DrStr(dr, c[12]),
                            Qty2 = Sm.DrDec(dr, c[13]),
                            Uom2 = Sm.DrStr(dr, c[14]),
                            Qty3 = Sm.DrDec(dr, c[15]),
                            Uom3 = Sm.DrStr(dr, c[16])
                        });
                    }
                }
                dr.Close();
            }

            ReAddItem(ref l);
        }

        private void ReAddItem(ref List<PlanningItem> l)
        {
            if (l.Count > 0)
            {
                int Row = Grd1.Rows.Count - 1;

                for (int i = 0; i < l.Count; i++)
                {
                    Grd1.Cells[(i + Row), 1].Value = (string.Compare(l[i].CancelInd, "Y") == 0);
                    Grd1.Cells[(i + Row), 2].Value = (string.Compare(l[i].CancelInd, "Y") == 0);
                    Grd1.Cells[(i + Row), 5].Value = l[i].Status;
                    Grd1.Cells[(i + Row), 6].Value = l[i].StatusDesc;
                    Grd1.Cells[(i + Row), 9].Value = l[i].ItCode;
                    Grd1.Cells[(i + Row), 11].Value = l[i].ItCodeInternal;
                    Grd1.Cells[(i + Row), 12].Value = l[i].ItName;
                    Grd1.Cells[(i + Row), 13].Value = l[i].BatchNo;
                    Grd1.Cells[(i + Row), 14].Value = l[i].Lot;
                    Grd1.Cells[(i + Row), 15].Value = l[i].Bin;
                    Grd1.Cells[(i + Row), 16].Value = Sm.FormatNum(l[i].Qty, 0);
                    Grd1.Cells[(i + Row), 17].Value = l[i].Uom1;
                    Grd1.Cells[(i + Row), 18].Value = Sm.FormatNum(l[i].Qty2, 0);
                    Grd1.Cells[(i + Row), 19].Value = l[i].Uom2;
                    Grd1.Cells[(i + Row), 20].Value = Sm.FormatNum(l[i].Qty3, 0);
                    Grd1.Cells[(i + Row), 21].Value = l[i].Uom3;
                    //Grd1.Cells[(i + Row), 23].Value = l[i].QCPIndicator;
                    Grd1.Cells[(i + Row), 24].Value = l[i].Remark;
                    Grd1.Cells[(i + Row), 26].Value = l[i].DNo;
                    Sm.SetGrdNumValueZero(ref Grd1, (i + Row), new int[] { 22 });
                    Grd1.Rows.Add();
                }
            }

            l.Clear();
        }

        private void GetParameter()
        {
            mNumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            mIsMaterialQCInspection2UseWorkCenter = Sm.GetParameterBoo("IsMaterialQCInspection2UseWorkCenter");
            mIsQCParameterInDetail = Sm.GetParameterBoo("IsQCParameterInDetail");
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 9).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 9) +
                            Sm.GetGrdStr(Grd1, Row, 13) +
                            Sm.GetGrdStr(Grd1, Row, 14) +
                            Sm.GetGrdStr(Grd1, Row, 15) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        //internal string GetSelectedInspector()
        //{
        //    var SQLInspector = string.Empty;
        //    if (Grd2.Rows.Count != 1)
        //    {
        //        for (int Row2 = 0; Row2 < Grd2.Rows.Count - 1; Row2++)
        //            if (Sm.GetGrdStr(Grd2, Row2, 1).Length != 0)
        //                SQLInspector +=
        //                    "##" +
        //                    Sm.GetGrdStr(Grd2, Row2, 1) +
        //                    "##";
        //    }
        //    return (SQLInspector.Length == 0 ? "##XXX##" : SQLInspector);
        //}

        public static void SetLueStatus(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT OptCode AS Col1, OptDesc AS Col2");
            SQL.AppendLine("FROM TblOption WHERE OptCat='QCStatus' ");
            SQL.AppendLine("ORDER BY OptCode;");

            cm.CommandText = SQL.ToString();

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueQCParameter(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT QCPCode AS Col1, QCPDesc AS Col2 ");
            SQL.AppendLine("FROM TblQCParameter WHERE ActInd='Y' ");
            SQL.AppendLine("ORDER BY QCPDesc;");

            cm.CommandText = SQL.ToString();

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Event

        #region Button Events

        private void BtnQCPlanningDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMaterialQCInspection2Dlg(this));
        }

        private void BtnQCPlanningDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtQCPlanningDocNo, "Planning document#", false))
            {
                try
                {
                    var f = new FrmQCPlanning(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtQCPlanningDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnAddItem_Click(object sender, EventArgs e)
        {
            if (Grd1.Rows.Count > 1 && TxtQCPlanningDocNo.Text.Length > 0)
            {
                if (Sm.GetGrdStr(Grd1, 0, 9).Length > 0)
                {
                    int Row = Grd1.Rows.Count - 1;
                    ReLoadItem(TxtQCPlanningDocNo.Text);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 16, 18, 20, 22 });
                }
                else
                {
                    Sm.StdMsg(mMsgType.Info, "No Planning Item Detected yet. Please choose the Planning Document again.");
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Info, "No Planning Item Detected yet. Please choose the Planning Document again.");
            }
        }

        #endregion

        #region Misc Control Event

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void LueStatus_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueStatus_Leave(object sender, EventArgs e)
        {
            if (LueStatus.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (Sm.GetLue(LueStatus).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 5].Value =
                    Grd1.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueStatus);
                    Grd1.Cells[fCell.RowIndex, 6].Value = LueStatus.GetColumnValue("Col2");
                }
                LueStatus.Visible = false;
            }
        }

        private void LueQCParameter_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueQCParameter, new Sm.RefreshLue1(SetLueQCParameter));
        }

        private void LueQCParameter_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueQCParameter_Leave(object sender, EventArgs e)
        {
            if (LueQCParameter.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LueQCParameter).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 7].Value =
                    Grd1.Cells[fCell.RowIndex, 8].Value =
                    Grd1.Cells[fCell.RowIndex, 23].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LueQCParameter);
                    Grd1.Cells[fCell.RowIndex, 8].Value = LueQCParameter.GetColumnValue("Col2");
                    Grd1.Cells[fCell.RowIndex, 23].Value = Sm.GetValue("Select QCPIndicator From TblQCParameter Where QCPCode = '" + Sm.GetLue(LueQCParameter) + "' ");
                }
                LueQCParameter.Visible = false;
            }
        }

        #endregion

        #endregion

        #region Class

        private class PlanningItem
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ItCodeInternal { get; set; }
            public string BatchNo { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string Uom1 { get; set; }
            public string Uom2 { get; set; }
            public string Uom3 { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string Status { get; set; }
            public string StatusDesc { get; set; }
            public string CancelInd { get; set; }
            public string DNo { get; set; }
            public string DocNo { get; set; }
            public string Remark { get; set; }
            public string QCPIndicator { get; set; }
        }

        #endregion

    }
}
