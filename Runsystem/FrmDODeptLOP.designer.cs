﻿namespace RunSystem
{
    partial class FrmDODeptLOP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDODeptLOP));
            this.LblCCCode = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LueUserCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueEmpCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueCCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LueEntCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblEntCode = new System.Windows.Forms.Label();
            this.TxtJournalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.BtnLOPDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnLOPDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtLOPDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUserCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLOPDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(791, 0);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtProjectName);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.BtnLOPDocNo2);
            this.panel2.Controls.Add(this.BtnLOPDocNo);
            this.panel2.Controls.Add(this.TxtLOPDocNo);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.TxtLocalDocNo);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LueWhsCode);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(791, 134);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LueEmpCode);
            this.panel3.Controls.Add(this.Grd2);
            this.panel3.Location = new System.Drawing.Point(0, 134);
            this.panel3.Size = new System.Drawing.Size(791, 339);
            this.panel3.Controls.SetChildIndex(this.Grd2, 0);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.LueEmpCode, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.Location = new System.Drawing.Point(0, 138);
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(791, 201);
            this.Grd1.TabIndex = 32;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // LblCCCode
            // 
            this.LblCCCode.AutoSize = true;
            this.LblCCCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCCCode.ForeColor = System.Drawing.Color.Black;
            this.LblCCCode.Location = new System.Drawing.Point(19, 50);
            this.LblCCCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCCCode.Name = "LblCCCode";
            this.LblCCCode.Size = new System.Drawing.Size(72, 14);
            this.LblCCCode.TabIndex = 30;
            this.LblCCCode.Text = "Cost Center";
            this.LblCCCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(25, 112);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 14);
            this.label8.TabIndex = 24;
            this.label8.Text = "Warehouse";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(100, 109);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 30;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 300;
            this.LueWhsCode.Size = new System.Drawing.Size(284, 20);
            this.LueWhsCode.TabIndex = 25;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(96, 89);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(295, 20);
            this.MeeRemark.TabIndex = 35;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(44, 92);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 34;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(100, 25);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(104, 20);
            this.DteDocDt.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(61, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(100, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(226, 20);
            this.TxtDocNo.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(21, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(8, 71);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 14);
            this.label4.TabIndex = 32;
            this.label4.Text = "Requested By";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueUserCode
            // 
            this.LueUserCode.EnterMoveNextControl = true;
            this.LueUserCode.Location = new System.Drawing.Point(96, 68);
            this.LueUserCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueUserCode.Name = "LueUserCode";
            this.LueUserCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUserCode.Properties.Appearance.Options.UseFont = true;
            this.LueUserCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUserCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUserCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUserCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUserCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUserCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUserCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUserCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUserCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUserCode.Properties.DropDownRows = 30;
            this.LueUserCode.Properties.NullText = "[Empty]";
            this.LueUserCode.Properties.PopupWidth = 300;
            this.LueUserCode.Size = new System.Drawing.Size(295, 20);
            this.LueUserCode.TabIndex = 33;
            this.LueUserCode.ToolTip = "F4 : Show/hide list";
            this.LueUserCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUserCode.EditValueChanged += new System.EventHandler(this.LueUserCode_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(18, 7);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 14);
            this.label6.TabIndex = 26;
            this.label6.Text = "Department";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(96, 4);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(295, 20);
            this.LueDeptCode.TabIndex = 27;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // LueEmpCode
            // 
            this.LueEmpCode.EnterMoveNextControl = true;
            this.LueEmpCode.Location = new System.Drawing.Point(297, 160);
            this.LueEmpCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmpCode.Name = "LueEmpCode";
            this.LueEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode.Properties.Appearance.Options.UseFont = true;
            this.LueEmpCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmpCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmpCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmpCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmpCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmpCode.Properties.DropDownRows = 30;
            this.LueEmpCode.Properties.NullText = "[Empty]";
            this.LueEmpCode.Properties.PopupWidth = 250;
            this.LueEmpCode.Size = new System.Drawing.Size(200, 20);
            this.LueEmpCode.TabIndex = 33;
            this.LueEmpCode.ToolTip = "F4 : Show/hide list";
            this.LueEmpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmpCode.EditValueChanged += new System.EventHandler(this.LueEmpCode_EditValueChanged);
            this.LueEmpCode.Leave += new System.EventHandler(this.LueEmpCode_Leave);
            this.LueEmpCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueEmpCode_KeyDown);
            // 
            // LueCCCode
            // 
            this.LueCCCode.EnterMoveNextControl = true;
            this.LueCCCode.Location = new System.Drawing.Point(96, 47);
            this.LueCCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCCCode.Name = "LueCCCode";
            this.LueCCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.Appearance.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCCCode.Properties.DropDownRows = 30;
            this.LueCCCode.Properties.NullText = "[Empty]";
            this.LueCCCode.Properties.PopupWidth = 300;
            this.LueCCCode.Size = new System.Drawing.Size(295, 20);
            this.LueCCCode.TabIndex = 31;
            this.LueCCCode.ToolTip = "F4 : Show/hide list";
            this.LueCCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCCCode.EditValueChanged += new System.EventHandler(this.LueCCCode_EditValueChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.LueEntCode);
            this.panel4.Controls.Add(this.LblEntCode);
            this.panel4.Controls.Add(this.TxtJournalDocNo);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.LueCCCode);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.LblCCCode);
            this.panel4.Controls.Add(this.LueDeptCode);
            this.panel4.Controls.Add(this.LueUserCode);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(393, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(398, 134);
            this.panel4.TabIndex = 20;
            // 
            // LueEntCode
            // 
            this.LueEntCode.EnterMoveNextControl = true;
            this.LueEntCode.Location = new System.Drawing.Point(96, 26);
            this.LueEntCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEntCode.Name = "LueEntCode";
            this.LueEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.Appearance.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEntCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEntCode.Properties.DropDownRows = 30;
            this.LueEntCode.Properties.NullText = "[Empty]";
            this.LueEntCode.Properties.PopupWidth = 300;
            this.LueEntCode.Size = new System.Drawing.Size(295, 20);
            this.LueEntCode.TabIndex = 29;
            this.LueEntCode.ToolTip = "F4 : Show/hide list";
            this.LueEntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEntCode.EditValueChanged += new System.EventHandler(this.LueEntCode_EditValueChanged);
            // 
            // LblEntCode
            // 
            this.LblEntCode.AutoSize = true;
            this.LblEntCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEntCode.ForeColor = System.Drawing.Color.Black;
            this.LblEntCode.Location = new System.Drawing.Point(52, 29);
            this.LblEntCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEntCode.Name = "LblEntCode";
            this.LblEntCode.Size = new System.Drawing.Size(39, 14);
            this.LblEntCode.TabIndex = 28;
            this.LblEntCode.Text = "Entity";
            this.LblEntCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtJournalDocNo
            // 
            this.TxtJournalDocNo.EnterMoveNextControl = true;
            this.TxtJournalDocNo.Location = new System.Drawing.Point(96, 110);
            this.TxtJournalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo.Name = "TxtJournalDocNo";
            this.TxtJournalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo.Properties.MaxLength = 30;
            this.TxtJournalDocNo.Properties.ReadOnly = true;
            this.TxtJournalDocNo.Size = new System.Drawing.Size(215, 20);
            this.TxtJournalDocNo.TabIndex = 37;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(37, 113);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 14);
            this.label3.TabIndex = 36;
            this.label3.Text = "Journal#";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(791, 138);
            this.Grd2.TabIndex = 31;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(100, 46);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(226, 20);
            this.TxtLocalDocNo.TabIndex = 17;
            this.TxtLocalDocNo.Validated += new System.EventHandler(this.TxtLocalDocNo_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(51, 49);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 14);
            this.label10.TabIndex = 16;
            this.label10.Text = "Local#";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnLOPDocNo2
            // 
            this.BtnLOPDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnLOPDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnLOPDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLOPDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnLOPDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnLOPDocNo2.Appearance.Options.UseFont = true;
            this.BtnLOPDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnLOPDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnLOPDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnLOPDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnLOPDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnLOPDocNo2.Image")));
            this.BtnLOPDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnLOPDocNo2.Location = new System.Drawing.Point(360, 65);
            this.BtnLOPDocNo2.Name = "BtnLOPDocNo2";
            this.BtnLOPDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnLOPDocNo2.TabIndex = 21;
            this.BtnLOPDocNo2.ToolTip = "Show List of Project";
            this.BtnLOPDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnLOPDocNo2.ToolTipTitle = "Run System";
            this.BtnLOPDocNo2.Click += new System.EventHandler(this.BtnLOPDocNo2_Click);
            // 
            // BtnLOPDocNo
            // 
            this.BtnLOPDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnLOPDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnLOPDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLOPDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnLOPDocNo.Appearance.Options.UseBackColor = true;
            this.BtnLOPDocNo.Appearance.Options.UseFont = true;
            this.BtnLOPDocNo.Appearance.Options.UseForeColor = true;
            this.BtnLOPDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnLOPDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnLOPDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnLOPDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnLOPDocNo.Image")));
            this.BtnLOPDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnLOPDocNo.Location = new System.Drawing.Point(334, 65);
            this.BtnLOPDocNo.Name = "BtnLOPDocNo";
            this.BtnLOPDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnLOPDocNo.TabIndex = 20;
            this.BtnLOPDocNo.ToolTip = "Find List of Project";
            this.BtnLOPDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnLOPDocNo.ToolTipTitle = "Run System";
            this.BtnLOPDocNo.Click += new System.EventHandler(this.BtnLOPDocNo_Click);
            // 
            // TxtLOPDocNo
            // 
            this.TxtLOPDocNo.EnterMoveNextControl = true;
            this.TxtLOPDocNo.Location = new System.Drawing.Point(100, 67);
            this.TxtLOPDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLOPDocNo.Name = "TxtLOPDocNo";
            this.TxtLOPDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtLOPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLOPDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLOPDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLOPDocNo.Properties.MaxLength = 16;
            this.TxtLOPDocNo.Properties.ReadOnly = true;
            this.TxtLOPDocNo.Size = new System.Drawing.Size(226, 20);
            this.TxtLOPDocNo.TabIndex = 19;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(11, 70);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 14);
            this.label12.TabIndex = 18;
            this.label12.Text = "List of Project";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectName
            // 
            this.TxtProjectName.EnterMoveNextControl = true;
            this.TxtProjectName.Location = new System.Drawing.Point(100, 88);
            this.TxtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectName.Name = "TxtProjectName";
            this.TxtProjectName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectName.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectName.Properties.MaxLength = 16;
            this.TxtProjectName.Properties.ReadOnly = true;
            this.TxtProjectName.Size = new System.Drawing.Size(284, 20);
            this.TxtProjectName.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(13, 91);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 14);
            this.label7.TabIndex = 22;
            this.label7.Text = "Project Name";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmDODeptLOP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(861, 473);
            this.Name = "FrmDODeptLOP";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUserCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLOPDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblCCCode;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueUserCode;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private DevExpress.XtraEditors.LookUpEdit LueEmpCode;
        private DevExpress.XtraEditors.LookUpEdit LueCCCode;
        private System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo;
        private System.Windows.Forms.Label label3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraEditors.LookUpEdit LueEntCode;
        private System.Windows.Forms.Label LblEntCode;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label10;
        public DevExpress.XtraEditors.SimpleButton BtnLOPDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnLOPDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtLOPDocNo;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtProjectName;
        private System.Windows.Forms.Label label7;
    }
}