﻿#region Update
/*
    22/07/2021 [MYA/PHT] Menambahkan field location pada master city
    15/03/2023 [RDA/PHT] Menambahkan field region pada master city

*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmCityFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmCity mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmCityFind(FrmCity FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        { 
            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.CityCode, A.CityName, B.ProvName, C.CntName, D.RingName, E.OptDesc as LocationName, F.RegionName, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblCity A ");
            SQL.AppendLine("Left Join TblProvince B On A.ProvCode=B.ProvCode ");
            SQL.AppendLine("Left Join TblCountry C On B.CntCode=C.CntCode ");
            SQL.AppendLine("Left Join TblRingArea D On A.RingCode=D.RingCode ");
            SQL.AppendLine("Left Join TblOption E On A.LocationCode=E.OptCode AND E.OptCat = 'LevelLocation' ");
            SQL.AppendLine("Left Join TblRegion F On A.RegionCode=F.RegionCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "City Code", 
                        "City Name",
                        "Province",
                        "Country",
                        "Ring Area",

                        //6-10
                        "Location",
                        "Region",
                        "Created"+Environment.NewLine+"By",   
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        
                        //11-13
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 200, 200, 200,
                        
                        //6-10
                        200, 200, 80, 80, 80,  

                        //11-13
                        80, 80, 80
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 9, 12 });
            Sm.GrdFormatTime(Grd1, new int[] { 10, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 9, 10, 11, 12, 13 }, false);
            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();
                Sm.FilterStr(ref Filter, ref cm, TxtCityName.Text, new string[] { "A.CityCode", "A.CityName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By C.CntName, B.ProvName, A.CityName",
                        new string[]
                        {
                            //0
                            "CityCode", 
                                
                            //1-5
                            "CityName", "ProvName", "CntName", "RingName", "LocationName",
                            
                            //6-10
                            "RegionName", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 13, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtCityName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCityName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "City");
        }

        #endregion

        #endregion
    }
}
