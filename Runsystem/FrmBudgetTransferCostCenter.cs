﻿#region Update
/*
    06/05/2021 [WED/PHT] new apps
    10/11/2021 [ICA/PHT] menambah kolom itemname, rate dan qty
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetTransferCostCenter : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mDocNo = string.Empty
            ;
        internal FrmBudgetTransferCostCenterFind FrmFind;

        #endregion

        #region Constructor

        public FrmBudgetTransferCostCenter(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Budget Transfer (Cost Center)";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);
                Sl.SetLueMth(LueMth2);

                SetGrd();
                SetFormControl(mState.View);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "From Cost Center",
                    "Budget Category Code",
                    "Budget Category",
                    "Remaining Budget"+Environment.NewLine+"Amount From",
                    "Transferred Amount",

                    //6-10
                    "",
                    "To Cost Center",
                    "Budget Category Code",
                    "Budget Category",
                    "Remaining Budget"+Environment.NewLine+"Amount To",

                    //11-13
                    "Total Budget",
                    "DNo",
                    "Initial Remaining From", 
                    "",
                    "Item's Code", 

                    //16-20
                    "Item's Name",
                    "Rate",
                    "Quantity",
                    "Quantity2",
                    "", 

                    //21-25
                    "Item's Code",
                    "Item's Name", 
                    "Rate",
                    "Quantity",
                    "Quantity2"
                },
                new int[] { 
                    //0
                    20,
                    
                    //1-5
                    180, 100, 250, 150, 150, 

                    //6-10
                    20, 180, 100, 250, 150,

                    //11-15
                    150, 0, 0, 20, 0,

                    //16-20
                    150, 100, 100, 100, 20, 

                    //21-23
                    0, 150, 100, 100, 100
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0, 6, 14, 20 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 10, 11, 13, 17, 18, 19, 23, 24, 25 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 8, 12, 13, 19, 25 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 21, 22, 23, 24, 25 });
            Grd1.Cols[14].Move(5);
            Grd1.Cols[15].Move(6);
            Grd1.Cols[16].Move(7);
            Grd1.Cols[17].Move(8);
            Grd1.Cols[18].Move(9);
            Grd1.Cols[20].Move(16);
            Grd1.Cols[21].Move(17);
            Grd1.Cols[22].Move(18);
            Grd1.Cols[23].Move(19);
            Grd1.Cols[24].Move(20);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 8 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, LueYr, LueMth, LueMth2, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 5, 6, 14, 19 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnProfitCenterCode.Enabled = BtnProfitCenterCode2.Enabled = BtnCCCode.Enabled = BtnCCCode2.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueYr, LueMth, LueMth2, MeeRemark
                    }, false);
                    BtnProfitCenterCode.Enabled = BtnProfitCenterCode2.Enabled = BtnCCCode.Enabled = BtnCCCode2.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 5, 6, 14, 19 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason,
                LueYr, LueMth, LueMth2, MeeRemark,
                TxtProfitCenterCode, TxtProfitCenterName, TxtProfitCenterCode2, TxtProfitCenterName2,
                TxtCCCode, TxtCCName, TxtCCCode2, TxtCCName2
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(Grd1, 0, new int[] { 4, 5, 10, 11, 13, 17, 18, 19, 23, 24, 25 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBudgetTransferCostCenterFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sm.SetLue(LueMth2, CurrentDateTime.Substring(4, 2));
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Grid Methods

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmtAllDetail();
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 5)
            {
                ComputeAmtDetail(e.RowIndex);
                ComputeQtyDetail(e.RowIndex);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueYr, "Year") && !Sm.IsLueEmpty(LueMth, "Month From") && !Sm.IsLueEmpty(LueMth2, "Month To"))
            {
                if (e.ColIndex == 0)
                {
                    if (!Sm.IsTxtEmpty(TxtCCCode, "From Cost Center", false))
                    {
                        Sm.FormShowDialog(new FrmBudgetTransferCostCenterDlg2(this, 1, TxtCCCode.Text, e.RowIndex, Sm.GetLue(LueYr), Sm.GetLue(LueMth), Sm.GetLue(LueMth2)));
                    }
                }

                if (e.ColIndex == 6)
                {
                    if (!Sm.IsTxtEmpty(TxtCCCode2, "To Cost Center", false) && !Sm.IsLueEmpty(LueYr, "Year") && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 1, false, "From Cost Center on this row's record is empty."))
                    {
                        Sm.FormShowDialog(new FrmBudgetTransferCostCenterDlg2(this, 2, TxtCCCode2.Text, e.RowIndex, Sm.GetLue(LueYr), Sm.GetLue(LueMth), Sm.GetLue(LueMth2)));
                    }
                }

                if (e.ColIndex == 14)
                {
                    if (!Sm.IsTxtEmpty(TxtCCCode, "From Cost Center", false) && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 2, false, "From Budget Category on this row's record is empty.") && !Sm.IsLueEmpty(LueYr, "Year") && !Sm.IsLueEmpty(LueMth, "Month From"))
                    {
                        Sm.FormShowDialog(new FrmBudgetTransferCostCenterDlg3(this, 1, TxtCCCode.Text, Sm.GetGrdStr(Grd1, e.RowIndex, 2), e.RowIndex, Sm.GetLue(LueMth), Sm.GetLue(LueYr)));
                    }
                }

                if (e.ColIndex == 20)
                {
                    if (!Sm.IsTxtEmpty(TxtCCCode2, "To Cost Center", false) && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 8, false, "To Budget Category on this row's record is empty.") && !Sm.IsLueEmpty(LueYr, "Year") && !Sm.IsLueEmpty(LueMth2, "Month To"))
                    {
                        Sm.FormShowDialog(new FrmBudgetTransferCostCenterDlg3(this, 2, TxtCCCode2.Text, Sm.GetGrdStr(Grd1, e.RowIndex, 8), e.RowIndex, Sm.GetLue(LueMth2), Sm.GetLue(LueYr)));
                    }
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BudgetTransferCostCenter", "TblBudgetTransferCostCenterHdr");
            var cml = new List<MySqlCommand>();

            cml.Add(SaveBudgetTransferCostCenterHdr(DocNo));

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                    cml.Add(SaveBudgetTransferCostCenterDtl(DocNo, r));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month From") ||
                Sm.IsLueEmpty(LueMth2, "Month To") ||
                Sm.IsTxtEmpty(TxtProfitCenterCode, "From Profit Center", false) ||
                Sm.IsTxtEmpty(TxtProfitCenterCode2, "To Profit Center", false) ||
                Sm.IsTxtEmpty(TxtCCCode, "From Cost Center", false) ||
                Sm.IsTxtEmpty(TxtCCCode2, "To Cost Center", false) ||
                IsGrdEmpty() ||
                IsGrdExceedsMaxRecord() ||
                IsGrdValueNotValid()
                ;
        }

        private bool IsGrdExceedsMaxRecord()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Data1 = string.Empty, Data2 = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; ++Row)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 7, false, "You need to input To Cost Center record.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 5, true, "Transferred Amount could not be zero.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 15, false, "You need to input From Item record")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 21, false, "You need to input To Item record.")) return true;
                if (Sm.GetGrdDec(Grd1, Row, 4) < 0m) { Sm.StdMsg(mMsgType.Warning, "You're over transferred this budget amount."); Sm.FocusGrd(Grd1, Row, 4); return true; };
                if (TxtCCCode.Text == TxtCCCode2.Text && Sm.GetGrdStr(Grd1, Row, 2) == Sm.GetGrdStr(Grd1, Row, 8))
                {
                    Sm.StdMsg(mMsgType.Warning, "You could not transfer on the same Budget Category and Cost Center.");
                    Sm.FocusGrd(Grd1, Row, 8);
                    return true;
                }

                if(Sm.GetGrdDec(Grd1, Row, 18) > Sm.GetGrdDec(Grd1, Row, 19))
                {
                    Sm.StdMsg(mMsgType.Warning, "From Item's quantity could not bigger than CBP Item's quantity.");
                    Sm.FocusGrd(Grd1, Row, 18);
                    return true;
                }

                Data1 = string.Concat(TxtCCCode.Text, TxtCCCode2.Text, Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 8));
                for (int Row2 = (Row + 1); Row2 < Grd1.Rows.Count - 1; ++Row2)
                {
                    Data2 = string.Concat(TxtCCCode.Text, TxtCCCode2.Text, Sm.GetGrdStr(Grd1, Row2, 2), Sm.GetGrdStr(Grd1, Row2, 8));
                    if (Data1 == Data2)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Duplicated data found.");
                        Sm.FocusGrd(Grd1, Row2, 2);
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveBudgetTransferCostCenterHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudgetTransferCostCenterHdr(DocNo, DocDt, Status, Yr, Mth, Mth2, ");
            SQL.AppendLine("ProfitCenterCode, ProfitCenterCode2, CCCode, CCCode2, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @Yr, @Mth, @Mth2, ");
            SQL.AppendLine("@ProfitCenterCode, @ProfitCenterCode2, @CCCode, @CCCode2, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='BudgetTransferCostCenter' ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblBudgetTransferCostCenterHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@Mth2", Sm.GetLue(LueMth2));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", TxtProfitCenterCode.Text);
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", TxtProfitCenterCode2.Text);
            Sm.CmParam<String>(ref cm, "@CCCode", TxtCCCode.Text);
            Sm.CmParam<String>(ref cm, "@CCCode2", TxtCCCode2.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBudgetTransferCostCenterDtl(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudgetTransferCostCenterDtl(DocNo, DNo, BCCode, RemainingAmt, ItCode, Rate, Qty, ");
            SQL.AppendLine("TransferredAmt, BCCode2, RemainingAmt2, ItCode2, Rate2, Qty2, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @BCCode, @RemainingAmt, @ItCode, @Rate, @Qty, ");
            SQL.AppendLine("@TransferredAmt, @BCCode2, @RemainingAmt2, @ItCode2, @Rate2, @Qty2, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (r + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetGrdStr(Grd1, r, 2));
            Sm.CmParam<Decimal>(ref cm, "@RemainingAmt", Sm.GetGrdDec(Grd1, r, 4));
            Sm.CmParam<Decimal>(ref cm, "@TransferredAmt", Sm.GetGrdDec(Grd1, r, 5));
            Sm.CmParam<String>(ref cm, "@BCCode2", Sm.GetGrdStr(Grd1, r, 8));
            Sm.CmParam<Decimal>(ref cm, "@RemainingAmt2", Sm.GetGrdDec(Grd1, r, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, r, 11));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, r, 15));
            Sm.CmParam<Decimal>(ref cm, "@Rate", Sm.GetGrdDec(Grd1, r, 17));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 18));
            Sm.CmParam<String>(ref cm, "@ItCode2", Sm.GetGrdStr(Grd1, r, 21));
            Sm.CmParam<Decimal>(ref cm, "@Rate2", Sm.GetGrdDec(Grd1, r, 23));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, r, 24));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string mDocNo = TxtDocNo.Text;

            var cml = new List<MySqlCommand>();

            cml.Add(EditBudgetTransferCostCenterHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocNotCancelled() ||
                IsDataCancelledAlready()
                ;
            ;
        }

        private bool IsDocNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblBudgetTransferCostCenterHdr Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled."
                );
        }

        private MySqlCommand EditBudgetTransferCostCenterHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBudgetTransferCostCenterHdr ");
            SQL.AppendLine("Set CancelInd = 'Y', CancelReason = @CancelReason, ");
            SQL.AppendLine("LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowBudgetTransferCostCenterHdr(DocNo);
                ShowBudgetTransferCostCenterDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBudgetTransferCostCenterHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc, ");
            SQL.AppendLine("A.CancelReason, A.CancelInd, A.Yr, A.Mth, A.Mth2, A.ProfitCenterCode, ");
            SQL.AppendLine("B.ProfitCenterName, A.ProfitCenterCode2, C.ProfitCenterName As ProfitCenterName2, ");
            SQL.AppendLine("A.CCCode, D.CCName, A.CCCode2, E.CCName As CCName2, A.Remark ");
            SQL.AppendLine("From TblBudgetTransferCostCenterHdr A ");
            SQL.AppendLine("Inner Join TblProfitCenter B On A.ProfitCenterCode = B.ProfitCenterCode ");
            SQL.AppendLine("Inner Join TblProfitCenter C On A.ProfitCenterCode2 = C.ProfitCenterCode ");
            SQL.AppendLine("Inner Join TblCostCenter D On A.CCCode = D.CCCode ");
            SQL.AppendLine("Inner Join TblCostCenter E On A.CCCode2 = E.CCCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DocNo", 
                    "DocDt", "StatusDesc", "CancelReason", "CancelInd", "Yr", 
                    "Mth", "Mth2", "ProfitCenterCode", "ProfitCenterName", "ProfitCenterCode2", 
                    "ProfitCenterName2", "CCCode", "CCName", "CCCode2", "CCName2",
                    "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                    Sm.SetLue(LueYr, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LueMth, Sm.DrStr(dr, c[6]));
                    Sm.SetLue(LueMth2, Sm.DrStr(dr, c[7]));
                    TxtProfitCenterCode.EditValue = Sm.DrStr(dr, c[8]);
                    TxtProfitCenterName.EditValue = Sm.DrStr(dr, c[9]);
                    TxtProfitCenterCode2.EditValue = Sm.DrStr(dr, c[10]);
                    TxtProfitCenterName2.EditValue = Sm.DrStr(dr, c[11]);
                    TxtCCCode.EditValue = Sm.DrStr(dr, c[12]);
                    TxtCCName.EditValue = Sm.DrStr(dr, c[13]);
                    TxtCCCode2.EditValue = Sm.DrStr(dr, c[14]);
                    TxtCCName2.EditValue = Sm.DrStr(dr, c[15]);
                    MeeRemark.EditValue =  Sm.DrStr(dr, c[16]);
                }, true
            );
        }

        private void ShowBudgetTransferCostCenterDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select E.CCName, B.BCCode, C.BCName, B.RemainingAmt, B.TransferredAmt, ");
            SQL.AppendLine("F.CCName As CCName2, B.BCCode2, D.BCName As BCName2, B.RemainingAmt2, ");
            SQL.AppendLine("B.Amt, B.DNo, B.ItCode, G.ItName, B.Rate, B.Qty, B.ItCode2, H.ItName ItName2, B.Rate2, B.Qty2 ");
            SQL.AppendLine("From TblBudgetTransferCostCenterHdr A ");
            SQL.AppendLine("Inner Join TblBudgetTransferCostCenterDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblBudgetCategory C On B.BCCode = C.BCCode ");
            SQL.AppendLine("Inner Join TblBudgetCategory D On B.BCCode2 = D.BCCode ");
            SQL.AppendLine("Inner Join TblCostCenter E On A.CCCode = E.CCCode ");
            SQL.AppendLine("Inner Join TblCostCenter F On A.CCCode2 = F.CCCode ");
            SQL.AppendLine("Left Join TblItem G On B.ItCode = G.ItCode ");
            SQL.AppendLine("Left Join TblItem H On B.ItCode2 = H.ItCode ");
            SQL.AppendLine("Order By B.DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "CCName", 
                        "BCCode", "BCName", "RemainingAmt", "TransferredAmt", "CCName2", 
                        "BCCode2", "BCName2", "RemainingAmt2", "Amt", "DNo", 
                        "ItCode", "ItName", "Rate", "Qty", "ItCode2", 
                        "ItName2", "Rate2", "Qty2"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 18);

                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 5, 10, 11, 13, 17, 18, 19, 23, 24, 25 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Methods

        private void ComputeAmtDetail(int Row)
        {
            string BCCode = Sm.GetGrdStr(Grd1, Row, 2);
            decimal TransferredAmt = 0m, RemainingAmtFrom = 0m, RemainingAmtTo = 0m, Amt = 0m, AccTransferredAmt = 0m;

            RemainingAmtFrom = Sm.GetGrdDec(Grd1, Row, 13);
            TransferredAmt = Sm.GetGrdDec(Grd1, Row, 5);
            RemainingAmtTo = Sm.GetGrdDec(Grd1, Row, 10);

            RemainingAmtFrom -= TransferredAmt;
            Amt = RemainingAmtTo + TransferredAmt;

            Grd1.Cells[Row, 4].Value = Sm.FormatNum(RemainingAmtFrom, 0);
            Grd1.Cells[Row, 11].Value = Sm.FormatNum(Amt, 0);

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 2) == BCCode) AccTransferredAmt += Sm.GetGrdDec(Grd1, i, 5);
            }

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (i != Row)
                {
                    if (Sm.GetGrdStr(Grd1, i, 2) == BCCode)
                    {
                        RemainingAmtFrom = Sm.GetGrdDec(Grd1, i, 13) - Sm.GetGrdDec(Grd1, i, 5);
                        TransferredAmt = Sm.GetGrdDec(Grd1, Row, 5);
                        RemainingAmtTo = Sm.GetGrdDec(Grd1, i, 10);

                        RemainingAmtFrom -= TransferredAmt;
                        Amt = RemainingAmtTo + TransferredAmt;

                        Grd1.Cells[i, 4].Value = Sm.FormatNum(RemainingAmtFrom, 0);
                        Grd1.Cells[i, 11].Value = Sm.FormatNum(Amt, 0);
                    }
                }

                if (i == Row)
                {
                    RemainingAmtFrom = Sm.GetGrdDec(Grd1, Row, 13);
                    TransferredAmt = Sm.GetGrdDec(Grd1, Row, 5);
                    RemainingAmtTo = Sm.GetGrdDec(Grd1, Row, 10);

                    RemainingAmtFrom = Sm.GetGrdDec(Grd1, Row, 13) - AccTransferredAmt;
                    Amt = RemainingAmtTo + TransferredAmt;

                    Grd1.Cells[Row, 4].Value = Sm.FormatNum(RemainingAmtFrom, 0);
                    Grd1.Cells[Row, 11].Value = Sm.FormatNum(Amt, 0);
                }
            }
        }

        private void ComputeAmtAllDetail()
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                ComputeAmtDetail(i);
            }
        }

        internal void ComputeQtyDetail(int Row)
        {
            if (Sm.GetGrdDec(Grd1, Row, 17) > 0)
            {
                Grd1.Cells[Row, 18].Value = Sm.GetGrdDec(Grd1, Row, 5) / Sm.GetGrdDec(Grd1, Row, 17);
            }
            if(Sm.GetGrdDec(Grd1, Row, 23) > 0)
            {
                Grd1.Cells[Row, 24].Value = Sm.GetGrdDec(Grd1, Row, 5) / Sm.GetGrdDec(Grd1, Row, 23);
            }
        }

        #endregion

        #endregion

        #region Events

        #region Button Clicks

        private void BtnProfitCenterCode_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                Sm.FormShowDialog(new FrmBudgetTransferCostCenterDlg(this, 1, string.Empty));
            }
        }

        private void BtnProfitCenterCode2_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                Sm.FormShowDialog(new FrmBudgetTransferCostCenterDlg(this, 2, string.Empty));
            }
        }

        private void BtnCCCode_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtProfitCenterCode, "From Profit Center", false))
            {
                Sm.FormShowDialog(new FrmBudgetTransferCostCenterDlg(this, 3, TxtProfitCenterCode.Text));
            }
        }

        private void BtnCCCode2_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtProfitCenterCode2, "To Profit Center", false))
            {
                Sm.FormShowDialog(new FrmBudgetTransferCostCenterDlg(this, 4, TxtProfitCenterCode2.Text));
            }
        }

        #endregion

        #region Misc Control Events

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                ClearGrd();
            }
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                ClearGrd();
            }
        }

        private void LueMth2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                ClearGrd();
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #endregion

    }
}
