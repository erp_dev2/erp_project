﻿#region Update
/*
 * 18/08/2022 [TYO/PRODUCT] filter department berdasarkan parameter IsFilterByDept
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMaterialRequestOutstanding : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool 
            mIsShowForeignName = false, 
            mIsFilterByItCt = false,
            mIsFilterByDept = false;

        #endregion

        #region Constructor

        public FrmRptMaterialRequestOutstanding(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter1, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.DeptName, D.OptDesc, ");
            SQL.AppendLine("B.ItCode, E.ItName, E.ForeignName, B.Qty, E.PurchaseUomCode, B.UsageDt, ");
            SQL.AppendLine("IfNull(F.Qty, 0) As RecvQty, ");
            SQL.AppendLine("(IfNull(G.Qty, 0)+ifnull(H.Qty, 0)) As CancelQty, ");
            SQL.AppendLine("B.Qty-IfNull(F.Qty, 0)-IfNull(G.Qty, 0)- IfNull(H.Qty, 0) As OutstandingQty, ");
            SQL.AppendLine("A.LocalDocNo, ");
            SQL.AppendLine("Concat(IfNull(A.Remark,''),' ',IfNull(B.Remark,''))As Remark ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And IfNull(B.Status, 'O')<>'C' ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Inner Join TblOption D On D.OptCat='ReqType' And A.ReqType=D.OptCode ");
            SQL.AppendLine("Inner Join TblItem E On B.ItCode=E.ItCode " + Filter2);
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=E.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T4.DocNo, T4.DNo, Sum(T1.QtyPurchase) As Qty ");
            SQL.AppendLine("    From TblRecvVdDtl T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.PORequestDocNo=T3.DocNo And T2.PORequestDNo=T3.DNo And T3.CancelInd='N' And IfNull(T3.Status, 'O')='A' ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T4 On T3.MaterialRequestDocNo=T4.DocNo And T3.MaterialRequestDNo=T4.DNo And T4.CancelInd='N' And IfNull(T4.Status, 'O')<>'C' ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr T5 On T4.DocNo=T5.DocNo " + Filter1.Replace("A.", "T5."));
            SQL.AppendLine("        And T5.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Inner Join TblItem T6 On T4.ItCode=T6.ItCode " + Filter2.Replace("E.", "T6."));
            SQL.AppendLine("    Where T1.CancelInd='N' And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("    Group By T4.DocNo, T4.DNo ");
            SQL.AppendLine(") F On A.DocNo=F.DocNo And B.DNo=F.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T4.DocNo, T4.DNo, Sum(T1.Qty) As Qty ");
            SQL.AppendLine("    From TblPOQtyCancel T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.PORequestDocNo=T3.DocNo And T2.PORequestDNo=T3.DNo And T3.CancelInd='N' And IfNull(T3.Status, 'O')='A' ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T4 On T3.MaterialRequestDocNo=T4.DocNo And T3.MaterialRequestDNo=T4.DNo And T4.CancelInd='N' And IfNull(T4.Status, 'O')<>'C' ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr T5 On T4.DocNo=T5.DocNo " + Filter1.Replace("A.", "T5."));
            SQL.AppendLine("        And T5.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Inner Join TblItem T6 On T4.ItCode=T6.ItCode " + Filter2.Replace("E.", "T6."));
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T4.DocNo, T4.DNo ");
            SQL.AppendLine(") G On A.DocNo=G.DocNo And B.DNo=G.DNo ");
            SQL.AppendLine("Left Join TblMRQtyCancel H On B.DocNo = H.MRDocNo And B.DNo = H.MRDNo And H.CancelInd = 'N' ");
            SQL.AppendLine("Where B.Qty-IfNull(F.Qty, 0)-IfNull(G.Qty, 0)- IfNull(H.Qty, 0)>0 ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 " + Filter1);
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Local Document",
                        "Department",
                        "Type",

                        //6-10
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Requested"+Environment.NewLine+"Quantity",
                        "Received"+Environment.NewLine+"Quantity",
                        "Cancelled"+Environment.NewLine+"Quantity",
                        
                        //11-15
                        "Outstanding"+Environment.NewLine+"Quantity",
                        "UoM",
                        "Usage"+Environment.NewLine+"Date",
                        "Remark",
                        "Foreign Name"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 180, 150, 100,  
                        
                        //6-10
                        100, 250, 100, 100, 100,   
                        
                        //11-15
                        100, 80, 80, 270, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 13 });
            Grd1.Cols[15].Move(8);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 14 }, !ChkHideInfoInGrd.Checked);
            if(!mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 15 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 14 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter1 = " ", Filter2 = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter1, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter2, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "E.ItName", "E.ForeignName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter1, Filter2) + " Order By A.DocDt, A.DocNo, B.DNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "LocalDocNo", "DeptName", "OptDesc", "ItCode", 
                            
                            //6-10
                            "ItName",  "Qty", "RecvQty", "CancelQty", "OutstandingQty",  
                            
                            //11-14
                            "PurchaseUomCode", "UsageDt",  "Remark", "ForeignName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }
       
        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept"); 
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0)
                DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
