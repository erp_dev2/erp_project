namespace RunSystem
{
    partial class FrmVoucher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVoucher));
            this.label16 = new System.Windows.Forms.Label();
            this.LueAcType = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtVoucherRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LblBankAcCode2 = new System.Windows.Forms.Label();
            this.LueBankAcCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.LueAcType2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LuePIC = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtGiroNo = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.DteDueDt = new DevExpress.XtraEditors.DateEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.LuePaymentType = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.LblRemark = new System.Windows.Forms.Label();
            this.LblBankAcCode = new System.Windows.Forms.Label();
            this.LueBankAcCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtDocType = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtVdInvNo = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtCurCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtExcRate = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtCurCode2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.DteOpeningDt = new DevExpress.XtraEditors.DateEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.BtnAcNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnVoucherRequest = new DevExpress.XtraEditors.SimpleButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TcVoucher = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.TxtAddCostAmt = new DevExpress.XtraEditors.TextEdit();
            this.LblAdditionalAmt = new System.Windows.Forms.Label();
            this.TxtBillingID = new DevExpress.XtraEditors.TextEdit();
            this.LblBillingID = new System.Windows.Forms.Label();
            this.BtnSOContractDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSOContractDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.LblSOContractDocNo = new System.Windows.Forms.Label();
            this.TxtSOContractDocNo = new DevExpress.XtraEditors.TextEdit();
            this.BtnDocType = new DevExpress.XtraEditors.SimpleButton();
            this.label27 = new System.Windows.Forms.Label();
            this.BtnVoucherRequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAmt2 = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.ChkInterOfficeInd = new DevExpress.XtraEditors.CheckEdit();
            this.LueCashTypeGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.LblCashTypeGroup = new System.Windows.Forms.Label();
            this.LueCashTypeCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.LblCashType2 = new System.Windows.Forms.Label();
            this.LueCashTypeCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblCashType = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.BtnJournalDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnJournalDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnVoucherJournalDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtVoucherJournalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.LblEntCode = new System.Windows.Forms.Label();
            this.LueEntCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtJournalDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtJournalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.BtnBCCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtBCCode = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.Tp4 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp5 = new DevExpress.XtraTab.XtraTabPage();
            this.LueBankAcCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.LblEmpName = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePIC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdInvNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExcRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOpeningDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOpeningDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcVoucher)).BeginInit();
            this.TcVoucher.SuspendLayout();
            this.Tp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAddCostAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBillingID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt2.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInterOfficeInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashTypeGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashTypeCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashTypeCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherJournalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBCCode.Properties)).BeginInit();
            this.Tp4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.Tp5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(1230, 0);
            this.panel1.Size = new System.Drawing.Size(70, 509);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Location = new System.Drawing.Point(0, 188);
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(2);
            this.BtnPrint.Size = new System.Drawing.Size(70, 34);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Location = new System.Drawing.Point(0, 154);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.BtnCancel.Size = new System.Drawing.Size(70, 34);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Location = new System.Drawing.Point(0, 126);
            this.BtnSave.Margin = new System.Windows.Forms.Padding(2);
            this.BtnSave.Size = new System.Drawing.Size(70, 28);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Location = new System.Drawing.Point(0, 94);
            this.BtnDelete.Margin = new System.Windows.Forms.Padding(2);
            this.BtnDelete.Size = new System.Drawing.Size(70, 32);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Location = new System.Drawing.Point(0, 64);
            this.BtnEdit.Margin = new System.Windows.Forms.Padding(2);
            this.BtnEdit.Size = new System.Drawing.Size(70, 30);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Location = new System.Drawing.Point(0, 32);
            this.BtnInsert.Margin = new System.Windows.Forms.Padding(2);
            this.BtnInsert.Size = new System.Drawing.Size(70, 32);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Margin = new System.Windows.Forms.Padding(2);
            this.BtnFind.Size = new System.Drawing.Size(70, 32);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcVoucher);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Size = new System.Drawing.Size(1230, 434);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.Location = new System.Drawing.Point(0, 434);
            this.panel3.Size = new System.Drawing.Size(1230, 75);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 487);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowModeHasCurCell = true;
            this.Grd1.SelectionMode = TenTec.Windows.iGridLib.iGSelectionMode.MultiExtended;
            this.Grd1.Size = new System.Drawing.Size(1230, 75);
            this.Grd1.TabIndex = 60;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnExcel.Location = new System.Drawing.Point(0, 222);
            this.BtnExcel.Margin = new System.Windows.Forms.Padding(2);
            this.BtnExcel.Size = new System.Drawing.Size(70, 35);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(52, 101);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 14);
            this.label16.TabIndex = 31;
            this.label16.Text = "Debit/Credit";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueAcType
            // 
            this.LueAcType.EnterMoveNextControl = true;
            this.LueAcType.Location = new System.Drawing.Point(131, 98);
            this.LueAcType.Margin = new System.Windows.Forms.Padding(5);
            this.LueAcType.Name = "LueAcType";
            this.LueAcType.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueAcType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.Appearance.Options.UseBackColor = true;
            this.LueAcType.Properties.Appearance.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAcType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAcType.Properties.DropDownRows = 12;
            this.LueAcType.Properties.NullText = "[Empty]";
            this.LueAcType.Properties.PopupWidth = 500;
            this.LueAcType.Size = new System.Drawing.Size(84, 20);
            this.LueAcType.TabIndex = 32;
            this.LueAcType.ToolTip = "F4 : Show/hide list";
            this.LueAcType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAcType.EditValueChanged += new System.EventHandler(this.LueAcType_EditValueChanged);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(382, 23);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 18;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // TxtVoucherRequestDocNo
            // 
            this.TxtVoucherRequestDocNo.EnterMoveNextControl = true;
            this.TxtVoucherRequestDocNo.Location = new System.Drawing.Point(131, 32);
            this.TxtVoucherRequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestDocNo.Name = "TxtVoucherRequestDocNo";
            this.TxtVoucherRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestDocNo.Properties.MaxLength = 16;
            this.TxtVoucherRequestDocNo.Properties.ReadOnly = true;
            this.TxtVoucherRequestDocNo.Size = new System.Drawing.Size(252, 20);
            this.TxtVoucherRequestDocNo.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(14, 35);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 14);
            this.label4.TabIndex = 23;
            this.label4.Text = "Voucher Request#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(86, 25);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(114, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(48, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(86, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(233, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(8, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblBankAcCode2
            // 
            this.LblBankAcCode2.AutoSize = true;
            this.LblBankAcCode2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBankAcCode2.ForeColor = System.Drawing.Color.Black;
            this.LblBankAcCode2.Location = new System.Drawing.Point(25, 210);
            this.LblBankAcCode2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBankAcCode2.Name = "LblBankAcCode2";
            this.LblBankAcCode2.Size = new System.Drawing.Size(100, 14);
            this.LblBankAcCode2.TabIndex = 42;
            this.LblBankAcCode2.Text = "Debit / Credit To";
            this.LblBankAcCode2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankAcCode2
            // 
            this.LueBankAcCode2.EnterMoveNextControl = true;
            this.LueBankAcCode2.Location = new System.Drawing.Point(131, 207);
            this.LueBankAcCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode2.Name = "LueBankAcCode2";
            this.LueBankAcCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode2.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode2.Properties.DropDownRows = 25;
            this.LueBankAcCode2.Properties.NullText = "[Empty]";
            this.LueBankAcCode2.Properties.PopupWidth = 520;
            this.LueBankAcCode2.Size = new System.Drawing.Size(988, 20);
            this.LueBankAcCode2.TabIndex = 43;
            this.LueBankAcCode2.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode2.EditValueChanged += new System.EventHandler(this.LueBankAcCode2_EditValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(52, 189);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(73, 14);
            this.label24.TabIndex = 39;
            this.label24.Text = "Debit/Credit";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueAcType2
            // 
            this.LueAcType2.EnterMoveNextControl = true;
            this.LueAcType2.Location = new System.Drawing.Point(131, 186);
            this.LueAcType2.Margin = new System.Windows.Forms.Padding(5);
            this.LueAcType2.Name = "LueAcType2";
            this.LueAcType2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueAcType2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType2.Properties.Appearance.Options.UseBackColor = true;
            this.LueAcType2.Properties.Appearance.Options.UseFont = true;
            this.LueAcType2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAcType2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAcType2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAcType2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAcType2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAcType2.Properties.DropDownRows = 4;
            this.LueAcType2.Properties.NullText = "[Empty]";
            this.LueAcType2.Properties.PopupWidth = 130;
            this.LueAcType2.Size = new System.Drawing.Size(84, 20);
            this.LueAcType2.TabIndex = 40;
            this.LueAcType2.ToolTip = "F4 : Show/hide list";
            this.LueAcType2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAcType2.EditValueChanged += new System.EventHandler(this.LueAcType2_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(45, 289);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 14);
            this.label7.TabIndex = 54;
            this.label7.Text = "Person In Charge";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePIC
            // 
            this.LuePIC.EnterMoveNextControl = true;
            this.LuePIC.Location = new System.Drawing.Point(151, 287);
            this.LuePIC.Margin = new System.Windows.Forms.Padding(5);
            this.LuePIC.Name = "LuePIC";
            this.LuePIC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.Appearance.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePIC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePIC.Properties.DropDownRows = 25;
            this.LuePIC.Properties.NullText = "[Empty]";
            this.LuePIC.Properties.PopupWidth = 405;
            this.LuePIC.Size = new System.Drawing.Size(405, 20);
            this.LuePIC.TabIndex = 55;
            this.LuePIC.ToolTip = "F4 : Show/hide list";
            this.LuePIC.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePIC.EditValueChanged += new System.EventHandler(this.LuePIC_EditValueChanged);
            // 
            // TxtGiroNo
            // 
            this.TxtGiroNo.EnterMoveNextControl = true;
            this.TxtGiroNo.Location = new System.Drawing.Point(151, 93);
            this.TxtGiroNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGiroNo.Name = "TxtGiroNo";
            this.TxtGiroNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGiroNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGiroNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGiroNo.Properties.Appearance.Options.UseFont = true;
            this.TxtGiroNo.Properties.MaxLength = 80;
            this.TxtGiroNo.Size = new System.Drawing.Size(238, 20);
            this.TxtGiroNo.TabIndex = 32;
            this.TxtGiroNo.Validated += new System.EventHandler(this.TxtGiroNo_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(20, 97);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 14);
            this.label10.TabIndex = 31;
            this.label10.Text = "Bilyet Giro#/Cheque#";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDueDt
            // 
            this.DteDueDt.EditValue = null;
            this.DteDueDt.EnterMoveNextControl = true;
            this.DteDueDt.Location = new System.Drawing.Point(151, 135);
            this.DteDueDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDt.Name = "DteDueDt";
            this.DteDueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.Appearance.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDt.Properties.MaxLength = 8;
            this.DteDueDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDt.Size = new System.Drawing.Size(114, 20);
            this.DteDueDt.TabIndex = 36;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(87, 138);
            this.label12.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 14);
            this.label12.TabIndex = 35;
            this.label12.Text = "Due Date";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(78, 75);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 14);
            this.label14.TabIndex = 29;
            this.label14.Text = "Bank Name";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(151, 72);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 25;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 405;
            this.LueBankCode.Size = new System.Drawing.Size(405, 20);
            this.LueBankCode.TabIndex = 30;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(59, 10);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(87, 14);
            this.label15.TabIndex = 21;
            this.label15.Text = "Payment Type";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePaymentType
            // 
            this.LuePaymentType.EnterMoveNextControl = true;
            this.LuePaymentType.Location = new System.Drawing.Point(151, 7);
            this.LuePaymentType.Margin = new System.Windows.Forms.Padding(5);
            this.LuePaymentType.Name = "LuePaymentType";
            this.LuePaymentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.Appearance.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePaymentType.Properties.DropDownRows = 25;
            this.LuePaymentType.Properties.NullText = "[Empty]";
            this.LuePaymentType.Properties.PopupWidth = 250;
            this.LuePaymentType.Size = new System.Drawing.Size(238, 20);
            this.LuePaymentType.TabIndex = 22;
            this.LuePaymentType.ToolTip = "F4 : Show/hide list";
            this.LuePaymentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePaymentType.EditValueChanged += new System.EventHandler(this.LuePaymentType_EditValueChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(151, 332);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 5000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(550, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(538, 20);
            this.MeeRemark.TabIndex = 57;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // LblRemark
            // 
            this.LblRemark.AutoSize = true;
            this.LblRemark.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRemark.Location = new System.Drawing.Point(99, 335);
            this.LblRemark.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRemark.Name = "LblRemark";
            this.LblRemark.Size = new System.Drawing.Size(47, 14);
            this.LblRemark.TabIndex = 56;
            this.LblRemark.Text = "Remark";
            this.LblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblBankAcCode
            // 
            this.LblBankAcCode.AutoSize = true;
            this.LblBankAcCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBankAcCode.ForeColor = System.Drawing.Color.Red;
            this.LblBankAcCode.Location = new System.Drawing.Point(25, 122);
            this.LblBankAcCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBankAcCode.Name = "LblBankAcCode";
            this.LblBankAcCode.Size = new System.Drawing.Size(100, 14);
            this.LblBankAcCode.TabIndex = 33;
            this.LblBankAcCode.Text = "Debit / Credit To";
            this.LblBankAcCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankAcCode
            // 
            this.LueBankAcCode.EnterMoveNextControl = true;
            this.LueBankAcCode.Location = new System.Drawing.Point(131, 119);
            this.LueBankAcCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode.Name = "LueBankAcCode";
            this.LueBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode.Properties.DropDownRows = 25;
            this.LueBankAcCode.Properties.NullText = "[Empty]";
            this.LueBankAcCode.Properties.PopupWidth = 520;
            this.LueBankAcCode.Size = new System.Drawing.Size(988, 20);
            this.LueBankAcCode.TabIndex = 34;
            this.LueBankAcCode.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode.EditValueChanged += new System.EventHandler(this.LueBankAcCode_EditValueChanged);
            // 
            // TxtDocType
            // 
            this.TxtDocType.EnterMoveNextControl = true;
            this.TxtDocType.Location = new System.Drawing.Point(131, 53);
            this.TxtDocType.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocType.Name = "TxtDocType";
            this.TxtDocType.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocType.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocType.Properties.Appearance.Options.UseFont = true;
            this.TxtDocType.Properties.MaxLength = 16;
            this.TxtDocType.Properties.ReadOnly = true;
            this.TxtDocType.Size = new System.Drawing.Size(521, 20);
            this.TxtDocType.TabIndex = 28;
            this.TxtDocType.EditValueChanged += new System.EventHandler(this.TxtDocType_EditValueChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(90, 56);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 14);
            this.label18.TabIndex = 27;
            this.label18.Text = "Type";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(131, 161);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.Size = new System.Drawing.Size(236, 20);
            this.TxtAmt.TabIndex = 38;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(74, 164);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 14);
            this.label3.TabIndex = 37;
            this.label3.Text = "Amount";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdInvNo
            // 
            this.TxtVdInvNo.AllowDrop = true;
            this.TxtVdInvNo.EnterMoveNextControl = true;
            this.TxtVdInvNo.Location = new System.Drawing.Point(131, 74);
            this.TxtVdInvNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdInvNo.Name = "TxtVdInvNo";
            this.TxtVdInvNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVdInvNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdInvNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdInvNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVdInvNo.Properties.MaxLength = 250;
            this.TxtVdInvNo.Properties.ReadOnly = true;
            this.TxtVdInvNo.Size = new System.Drawing.Size(252, 20);
            this.TxtVdInvNo.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(18, 77);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 14);
            this.label6.TabIndex = 29;
            this.label6.Text = "Vendor\'s Invoice#";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(131, 11);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 300;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(252, 20);
            this.TxtLocalDocNo.TabIndex = 22;
            this.TxtLocalDocNo.Validated += new System.EventHandler(this.TxtLocalDocNo_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(82, 14);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 14);
            this.label8.TabIndex = 21;
            this.label8.Text = "Local#";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(10, 24);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(368, 20);
            this.MeeCancelReason.TabIndex = 17;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(10, 6);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(135, 14);
            this.label13.TabIndex = 16;
            this.label13.Text = "Reason For Cancellation";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCurCode
            // 
            this.TxtCurCode.EnterMoveNextControl = true;
            this.TxtCurCode.Location = new System.Drawing.Point(131, 140);
            this.TxtCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode.Name = "TxtCurCode";
            this.TxtCurCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode.Properties.MaxLength = 30;
            this.TxtCurCode.Properties.ReadOnly = true;
            this.TxtCurCode.Size = new System.Drawing.Size(84, 20);
            this.TxtCurCode.TabIndex = 36;
            // 
            // TxtExcRate
            // 
            this.TxtExcRate.EnterMoveNextControl = true;
            this.TxtExcRate.Location = new System.Drawing.Point(131, 249);
            this.TxtExcRate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtExcRate.Name = "TxtExcRate";
            this.TxtExcRate.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtExcRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtExcRate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtExcRate.Properties.Appearance.Options.UseFont = true;
            this.TxtExcRate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtExcRate.Size = new System.Drawing.Size(236, 20);
            this.TxtExcRate.TabIndex = 47;
            this.TxtExcRate.Validated += new System.EventHandler(this.TxtExcRate_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(93, 252);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 14);
            this.label9.TabIndex = 46;
            this.label9.Text = "Rate";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(70, 231);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 14);
            this.label11.TabIndex = 44;
            this.label11.Text = "Currency";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCurCode2
            // 
            this.TxtCurCode2.EnterMoveNextControl = true;
            this.TxtCurCode2.Location = new System.Drawing.Point(131, 228);
            this.TxtCurCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode2.Name = "TxtCurCode2";
            this.TxtCurCode2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCurCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode2.Properties.MaxLength = 30;
            this.TxtCurCode2.Properties.ReadOnly = true;
            this.TxtCurCode2.Size = new System.Drawing.Size(84, 20);
            this.TxtCurCode2.TabIndex = 45;
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(151, 202);
            this.TxtAcDesc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 16;
            this.TxtAcDesc.Properties.ReadOnly = true;
            this.TxtAcDesc.Size = new System.Drawing.Size(538, 20);
            this.TxtAcDesc.TabIndex = 44;
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(151, 181);
            this.TxtAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 16;
            this.TxtAcNo.Properties.ReadOnly = true;
            this.TxtAcNo.Size = new System.Drawing.Size(229, 20);
            this.TxtAcNo.TabIndex = 40;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(48, 183);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(98, 14);
            this.label19.TabIndex = 39;
            this.label19.Text = "COA\'s Account#";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteOpeningDt
            // 
            this.DteOpeningDt.EditValue = null;
            this.DteOpeningDt.EnterMoveNextControl = true;
            this.DteOpeningDt.Location = new System.Drawing.Point(151, 114);
            this.DteOpeningDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteOpeningDt.Name = "DteOpeningDt";
            this.DteOpeningDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteOpeningDt.Properties.Appearance.Options.UseFont = true;
            this.DteOpeningDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteOpeningDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteOpeningDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteOpeningDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteOpeningDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteOpeningDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteOpeningDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteOpeningDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteOpeningDt.Properties.MaxLength = 8;
            this.DteOpeningDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteOpeningDt.Size = new System.Drawing.Size(114, 20);
            this.DteOpeningDt.TabIndex = 34;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(63, 118);
            this.label25.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(83, 14);
            this.label25.TabIndex = 33;
            this.label25.Text = "Opening Date";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAcNo
            // 
            this.BtnAcNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo.Appearance.Options.UseBackColor = true;
            this.BtnAcNo.Appearance.Options.UseFont = true;
            this.BtnAcNo.Appearance.Options.UseForeColor = true;
            this.BtnAcNo.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo.Image")));
            this.BtnAcNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo.Location = new System.Drawing.Point(384, 179);
            this.BtnAcNo.Name = "BtnAcNo";
            this.BtnAcNo.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo.TabIndex = 41;
            this.BtnAcNo.ToolTip = "Find COA\'s Account";
            this.BtnAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo.ToolTipTitle = "Run System";
            this.BtnAcNo.Click += new System.EventHandler(this.BtnAcNo_Click);
            // 
            // BtnVoucherRequest
            // 
            this.BtnVoucherRequest.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucherRequest.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucherRequest.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucherRequest.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucherRequest.Appearance.Options.UseBackColor = true;
            this.BtnVoucherRequest.Appearance.Options.UseFont = true;
            this.BtnVoucherRequest.Appearance.Options.UseForeColor = true;
            this.BtnVoucherRequest.Appearance.Options.UseTextOptions = true;
            this.BtnVoucherRequest.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucherRequest.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucherRequest.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnVoucherRequest.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnVoucherRequest.Location = new System.Drawing.Point(386, 32);
            this.BtnVoucherRequest.Name = "BtnVoucherRequest";
            this.BtnVoucherRequest.Size = new System.Drawing.Size(31, 20);
            this.BtnVoucherRequest.TabIndex = 25;
            this.BtnVoucherRequest.ToolTip = "Find Voucher Request";
            this.BtnVoucherRequest.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucherRequest.ToolTipTitle = "Run System";
            this.BtnVoucherRequest.Click += new System.EventHandler(this.BtnVoucherRequest_Click);
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 84);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1230, 350);
            this.panel4.TabIndex = 19;
            // 
            // TcVoucher
            // 
            this.TcVoucher.Dock = System.Windows.Forms.DockStyle.Top;
            this.TcVoucher.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcVoucher.Location = new System.Drawing.Point(0, 51);
            this.TcVoucher.Name = "TcVoucher";
            this.TcVoucher.SelectedTabPage = this.Tp1;
            this.TcVoucher.Size = new System.Drawing.Size(1230, 384);
            this.TcVoucher.TabIndex = 20;
            this.TcVoucher.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2,
            this.Tp3,
            this.Tp4,
            this.Tp5});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.TxtAddCostAmt);
            this.Tp1.Controls.Add(this.LblAdditionalAmt);
            this.Tp1.Controls.Add(this.TxtBillingID);
            this.Tp1.Controls.Add(this.LblBillingID);
            this.Tp1.Controls.Add(this.BtnSOContractDocNo2);
            this.Tp1.Controls.Add(this.BtnSOContractDocNo);
            this.Tp1.Controls.Add(this.LblSOContractDocNo);
            this.Tp1.Controls.Add(this.TxtSOContractDocNo);
            this.Tp1.Controls.Add(this.BtnDocType);
            this.Tp1.Controls.Add(this.label27);
            this.Tp1.Controls.Add(this.BtnVoucherRequestDocNo);
            this.Tp1.Controls.Add(this.TxtAmt2);
            this.Tp1.Controls.Add(this.label20);
            this.Tp1.Controls.Add(this.label5);
            this.Tp1.Controls.Add(this.TxtLocalDocNo);
            this.Tp1.Controls.Add(this.label8);
            this.Tp1.Controls.Add(this.BtnVoucherRequest);
            this.Tp1.Controls.Add(this.TxtExcRate);
            this.Tp1.Controls.Add(this.label4);
            this.Tp1.Controls.Add(this.label9);
            this.Tp1.Controls.Add(this.TxtVoucherRequestDocNo);
            this.Tp1.Controls.Add(this.label11);
            this.Tp1.Controls.Add(this.LueAcType);
            this.Tp1.Controls.Add(this.TxtCurCode2);
            this.Tp1.Controls.Add(this.label16);
            this.Tp1.Controls.Add(this.LueBankAcCode);
            this.Tp1.Controls.Add(this.label18);
            this.Tp1.Controls.Add(this.TxtVdInvNo);
            this.Tp1.Controls.Add(this.TxtDocType);
            this.Tp1.Controls.Add(this.LblBankAcCode);
            this.Tp1.Controls.Add(this.LblBankAcCode2);
            this.Tp1.Controls.Add(this.TxtCurCode);
            this.Tp1.Controls.Add(this.label6);
            this.Tp1.Controls.Add(this.LueBankAcCode2);
            this.Tp1.Controls.Add(this.TxtAmt);
            this.Tp1.Controls.Add(this.label24);
            this.Tp1.Controls.Add(this.label3);
            this.Tp1.Controls.Add(this.LueAcType2);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(1224, 356);
            this.Tp1.Text = "General";
            // 
            // TxtAddCostAmt
            // 
            this.TxtAddCostAmt.EnterMoveNextControl = true;
            this.TxtAddCostAmt.Location = new System.Drawing.Point(131, 333);
            this.TxtAddCostAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAddCostAmt.Name = "TxtAddCostAmt";
            this.TxtAddCostAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAddCostAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAddCostAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAddCostAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAddCostAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAddCostAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAddCostAmt.Properties.ReadOnly = true;
            this.TxtAddCostAmt.Size = new System.Drawing.Size(236, 20);
            this.TxtAddCostAmt.TabIndex = 58;
            // 
            // LblAdditionalAmt
            // 
            this.LblAdditionalAmt.AutoSize = true;
            this.LblAdditionalAmt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAdditionalAmt.ForeColor = System.Drawing.Color.Black;
            this.LblAdditionalAmt.Location = new System.Drawing.Point(19, 336);
            this.LblAdditionalAmt.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAdditionalAmt.Name = "LblAdditionalAmt";
            this.LblAdditionalAmt.Size = new System.Drawing.Size(108, 14);
            this.LblAdditionalAmt.TabIndex = 57;
            this.LblAdditionalAmt.Text = "Additional Amount";
            this.LblAdditionalAmt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBillingID
            // 
            this.TxtBillingID.EnterMoveNextControl = true;
            this.TxtBillingID.Location = new System.Drawing.Point(131, 312);
            this.TxtBillingID.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBillingID.Name = "TxtBillingID";
            this.TxtBillingID.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtBillingID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBillingID.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBillingID.Properties.Appearance.Options.UseFont = true;
            this.TxtBillingID.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtBillingID.Size = new System.Drawing.Size(236, 20);
            this.TxtBillingID.TabIndex = 56;
            // 
            // LblBillingID
            // 
            this.LblBillingID.AutoSize = true;
            this.LblBillingID.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBillingID.ForeColor = System.Drawing.Color.Black;
            this.LblBillingID.Location = new System.Drawing.Point(75, 315);
            this.LblBillingID.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBillingID.Name = "LblBillingID";
            this.LblBillingID.Size = new System.Drawing.Size(52, 14);
            this.LblBillingID.TabIndex = 55;
            this.LblBillingID.Text = "Billing ID";
            this.LblBillingID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSOContractDocNo2
            // 
            this.BtnSOContractDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo2.Image")));
            this.BtnSOContractDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo2.Location = new System.Drawing.Point(333, 293);
            this.BtnSOContractDocNo2.Name = "BtnSOContractDocNo2";
            this.BtnSOContractDocNo2.Size = new System.Drawing.Size(16, 16);
            this.BtnSOContractDocNo2.TabIndex = 54;
            this.BtnSOContractDocNo2.ToolTip = "Show SO Contract Document";
            this.BtnSOContractDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo2.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo2.Click += new System.EventHandler(this.BtnSOContractDocNo2_Click);
            // 
            // BtnSOContractDocNo
            // 
            this.BtnSOContractDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo.Image")));
            this.BtnSOContractDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo.Location = new System.Drawing.Point(311, 293);
            this.BtnSOContractDocNo.Name = "BtnSOContractDocNo";
            this.BtnSOContractDocNo.Size = new System.Drawing.Size(16, 16);
            this.BtnSOContractDocNo.TabIndex = 53;
            this.BtnSOContractDocNo.ToolTip = "Find SO Contract Document";
            this.BtnSOContractDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo.ToolTipTitle = "Run System";
            // 
            // LblSOContractDocNo
            // 
            this.LblSOContractDocNo.AutoSize = true;
            this.LblSOContractDocNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSOContractDocNo.ForeColor = System.Drawing.Color.Black;
            this.LblSOContractDocNo.Location = new System.Drawing.Point(44, 294);
            this.LblSOContractDocNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSOContractDocNo.Name = "LblSOContractDocNo";
            this.LblSOContractDocNo.Size = new System.Drawing.Size(83, 14);
            this.LblSOContractDocNo.TabIndex = 51;
            this.LblSOContractDocNo.Text = "SO Contract#";
            this.LblSOContractDocNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSOContractDocNo
            // 
            this.TxtSOContractDocNo.EnterMoveNextControl = true;
            this.TxtSOContractDocNo.Location = new System.Drawing.Point(131, 291);
            this.TxtSOContractDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractDocNo.Name = "TxtSOContractDocNo";
            this.TxtSOContractDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractDocNo.Properties.MaxLength = 30;
            this.TxtSOContractDocNo.Properties.ReadOnly = true;
            this.TxtSOContractDocNo.Size = new System.Drawing.Size(177, 20);
            this.TxtSOContractDocNo.TabIndex = 52;
            // 
            // BtnDocType
            // 
            this.BtnDocType.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDocType.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDocType.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDocType.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDocType.Appearance.Options.UseBackColor = true;
            this.BtnDocType.Appearance.Options.UseFont = true;
            this.BtnDocType.Appearance.Options.UseForeColor = true;
            this.BtnDocType.Appearance.Options.UseTextOptions = true;
            this.BtnDocType.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDocType.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDocType.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnDocType.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnDocType.Location = new System.Drawing.Point(655, 52);
            this.BtnDocType.Name = "BtnDocType";
            this.BtnDocType.Size = new System.Drawing.Size(31, 20);
            this.BtnDocType.TabIndex = 29;
            this.BtnDocType.ToolTip = "Show Transaction Document Information";
            this.BtnDocType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDocType.ToolTipTitle = "Run System";
            this.BtnDocType.Click += new System.EventHandler(this.BtnDocType_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Green;
            this.label27.Location = new System.Drawing.Point(219, 189);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(226, 14);
            this.label27.TabIndex = 41;
            this.label27.Text = "(Transfer Between Cash/Bank Account)";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnVoucherRequestDocNo
            // 
            this.BtnVoucherRequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucherRequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucherRequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucherRequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseFont = true;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnVoucherRequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucherRequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucherRequestDocNo.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnVoucherRequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnVoucherRequestDocNo.Location = new System.Drawing.Point(421, 32);
            this.BtnVoucherRequestDocNo.Name = "BtnVoucherRequestDocNo";
            this.BtnVoucherRequestDocNo.Size = new System.Drawing.Size(31, 20);
            this.BtnVoucherRequestDocNo.TabIndex = 26;
            this.BtnVoucherRequestDocNo.ToolTip = "Show Voucher Request Information";
            this.BtnVoucherRequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucherRequestDocNo.ToolTipTitle = "Run System";
            this.BtnVoucherRequestDocNo.Click += new System.EventHandler(this.BtnVoucherRequestDocNo_Click);
            // 
            // TxtAmt2
            // 
            this.TxtAmt2.EnterMoveNextControl = true;
            this.TxtAmt2.Location = new System.Drawing.Point(131, 270);
            this.TxtAmt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt2.Name = "TxtAmt2";
            this.TxtAmt2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt2.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt2.Properties.ReadOnly = true;
            this.TxtAmt2.Size = new System.Drawing.Size(236, 20);
            this.TxtAmt2.TabIndex = 49;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(74, 273);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(51, 14);
            this.label20.TabIndex = 48;
            this.label20.Text = "Amount";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(70, 143);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 14);
            this.label5.TabIndex = 35;
            this.label5.Text = "Currency";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.LblEmpName);
            this.Tp2.Controls.Add(this.TxtEmpName);
            this.Tp2.Controls.Add(this.ChkInterOfficeInd);
            this.Tp2.Controls.Add(this.LueCashTypeGroup);
            this.Tp2.Controls.Add(this.LblCashTypeGroup);
            this.Tp2.Controls.Add(this.LueCashTypeCode2);
            this.Tp2.Controls.Add(this.LblCashType2);
            this.Tp2.Controls.Add(this.LueCashTypeCode);
            this.Tp2.Controls.Add(this.LblCashType);
            this.Tp2.Controls.Add(this.label26);
            this.Tp2.Controls.Add(this.BtnJournalDocNo2);
            this.Tp2.Controls.Add(this.BtnJournalDocNo);
            this.Tp2.Controls.Add(this.BtnVoucherJournalDocNo);
            this.Tp2.Controls.Add(this.TxtVoucherJournalDocNo);
            this.Tp2.Controls.Add(this.label23);
            this.Tp2.Controls.Add(this.LblEntCode);
            this.Tp2.Controls.Add(this.LueEntCode);
            this.Tp2.Controls.Add(this.label21);
            this.Tp2.Controls.Add(this.TxtJournalDocNo2);
            this.Tp2.Controls.Add(this.TxtJournalDocNo);
            this.Tp2.Controls.Add(this.label22);
            this.Tp2.Controls.Add(this.label17);
            this.Tp2.Controls.Add(this.LuePIC);
            this.Tp2.Controls.Add(this.DteOpeningDt);
            this.Tp2.Controls.Add(this.label7);
            this.Tp2.Controls.Add(this.LuePaymentType);
            this.Tp2.Controls.Add(this.label25);
            this.Tp2.Controls.Add(this.label15);
            this.Tp2.Controls.Add(this.label12);
            this.Tp2.Controls.Add(this.TxtAcDesc);
            this.Tp2.Controls.Add(this.DteDueDt);
            this.Tp2.Controls.Add(this.TxtGiroNo);
            this.Tp2.Controls.Add(this.LueBankCode);
            this.Tp2.Controls.Add(this.label10);
            this.Tp2.Controls.Add(this.LblRemark);
            this.Tp2.Controls.Add(this.label14);
            this.Tp2.Controls.Add(this.BtnAcNo);
            this.Tp2.Controls.Add(this.MeeRemark);
            this.Tp2.Controls.Add(this.TxtAcNo);
            this.Tp2.Controls.Add(this.label19);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(1224, 356);
            this.Tp2.Text = "Payment And Accounting";
            // 
            // ChkInterOfficeInd
            // 
            this.ChkInterOfficeInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkInterOfficeInd.Location = new System.Drawing.Point(411, 244);
            this.ChkInterOfficeInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkInterOfficeInd.Name = "ChkInterOfficeInd";
            this.ChkInterOfficeInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkInterOfficeInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkInterOfficeInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkInterOfficeInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkInterOfficeInd.Properties.Appearance.Options.UseFont = true;
            this.ChkInterOfficeInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkInterOfficeInd.Properties.Caption = "InterOffice by Cost Center";
            this.ChkInterOfficeInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkInterOfficeInd.Properties.ReadOnly = true;
            this.ChkInterOfficeInd.Size = new System.Drawing.Size(170, 22);
            this.ChkInterOfficeInd.TabIndex = 58;
            // 
            // LueCashTypeGroup
            // 
            this.LueCashTypeGroup.EnterMoveNextControl = true;
            this.LueCashTypeGroup.Location = new System.Drawing.Point(151, 28);
            this.LueCashTypeGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueCashTypeGroup.Name = "LueCashTypeGroup";
            this.LueCashTypeGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGroup.Properties.Appearance.Options.UseFont = true;
            this.LueCashTypeGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCashTypeGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCashTypeGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCashTypeGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCashTypeGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCashTypeGroup.Properties.DropDownRows = 25;
            this.LueCashTypeGroup.Properties.NullText = "[Empty]";
            this.LueCashTypeGroup.Properties.PopupWidth = 250;
            this.LueCashTypeGroup.Size = new System.Drawing.Size(238, 20);
            this.LueCashTypeGroup.TabIndex = 24;
            this.LueCashTypeGroup.ToolTip = "F4 : Show/hide list";
            this.LueCashTypeGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCashTypeGroup.EditValueChanged += new System.EventHandler(this.LueCashTypeGroup_EditValueChanged);
            // 
            // LblCashTypeGroup
            // 
            this.LblCashTypeGroup.AutoSize = true;
            this.LblCashTypeGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCashTypeGroup.ForeColor = System.Drawing.Color.Black;
            this.LblCashTypeGroup.Location = new System.Drawing.Point(46, 31);
            this.LblCashTypeGroup.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCashTypeGroup.Name = "LblCashTypeGroup";
            this.LblCashTypeGroup.Size = new System.Drawing.Size(101, 14);
            this.LblCashTypeGroup.TabIndex = 23;
            this.LblCashTypeGroup.Text = "Cash Type Group";
            this.LblCashTypeGroup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCashTypeCode2
            // 
            this.LueCashTypeCode2.EnterMoveNextControl = true;
            this.LueCashTypeCode2.Location = new System.Drawing.Point(475, 49);
            this.LueCashTypeCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueCashTypeCode2.Name = "LueCashTypeCode2";
            this.LueCashTypeCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode2.Properties.Appearance.Options.UseFont = true;
            this.LueCashTypeCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCashTypeCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCashTypeCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCashTypeCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCashTypeCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCashTypeCode2.Properties.DropDownRows = 25;
            this.LueCashTypeCode2.Properties.NullText = "[Empty]";
            this.LueCashTypeCode2.Properties.PopupWidth = 250;
            this.LueCashTypeCode2.Size = new System.Drawing.Size(238, 20);
            this.LueCashTypeCode2.TabIndex = 28;
            this.LueCashTypeCode2.ToolTip = "F4 : Show/hide list";
            this.LueCashTypeCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCashTypeCode2.EditValueChanged += new System.EventHandler(this.LueCashTypeCode2_EditValueChanged);
            // 
            // LblCashType2
            // 
            this.LblCashType2.AutoSize = true;
            this.LblCashType2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCashType2.ForeColor = System.Drawing.Color.Black;
            this.LblCashType2.Location = new System.Drawing.Point(395, 52);
            this.LblCashType2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCashType2.Name = "LblCashType2";
            this.LblCashType2.Size = new System.Drawing.Size(75, 14);
            this.LblCashType2.TabIndex = 27;
            this.LblCashType2.Text = "Cash Type 2";
            this.LblCashType2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCashTypeCode
            // 
            this.LueCashTypeCode.EnterMoveNextControl = true;
            this.LueCashTypeCode.Location = new System.Drawing.Point(151, 49);
            this.LueCashTypeCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCashTypeCode.Name = "LueCashTypeCode";
            this.LueCashTypeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode.Properties.Appearance.Options.UseFont = true;
            this.LueCashTypeCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCashTypeCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCashTypeCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCashTypeCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCashTypeCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCashTypeCode.Properties.DropDownRows = 25;
            this.LueCashTypeCode.Properties.NullText = "[Empty]";
            this.LueCashTypeCode.Properties.PopupWidth = 250;
            this.LueCashTypeCode.Size = new System.Drawing.Size(238, 20);
            this.LueCashTypeCode.TabIndex = 26;
            this.LueCashTypeCode.ToolTip = "F4 : Show/hide list";
            this.LueCashTypeCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCashTypeCode.EditValueChanged += new System.EventHandler(this.LueCashTypeCode_EditValueChanged);
            // 
            // LblCashType
            // 
            this.LblCashType.AutoSize = true;
            this.LblCashType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCashType.ForeColor = System.Drawing.Color.Black;
            this.LblCashType.Location = new System.Drawing.Point(82, 52);
            this.LblCashType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCashType.Name = "LblCashType";
            this.LblCashType.Size = new System.Drawing.Size(64, 14);
            this.LblCashType.TabIndex = 25;
            this.LblCashType.Text = "Cash Type";
            this.LblCashType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(22, 205);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(124, 14);
            this.label26.TabIndex = 43;
            this.label26.Text = "COA\'s Account Name";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnJournalDocNo2
            // 
            this.BtnJournalDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnJournalDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnJournalDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnJournalDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnJournalDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnJournalDocNo2.Appearance.Options.UseFont = true;
            this.BtnJournalDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnJournalDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnJournalDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnJournalDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnJournalDocNo2.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnJournalDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnJournalDocNo2.Location = new System.Drawing.Point(384, 263);
            this.BtnJournalDocNo2.Name = "BtnJournalDocNo2";
            this.BtnJournalDocNo2.Size = new System.Drawing.Size(24, 20);
            this.BtnJournalDocNo2.TabIndex = 52;
            this.BtnJournalDocNo2.ToolTip = "Show Journal Information";
            this.BtnJournalDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnJournalDocNo2.ToolTipTitle = "Run System";
            this.BtnJournalDocNo2.Click += new System.EventHandler(this.BtnJournalDocNo2_Click);
            // 
            // BtnJournalDocNo
            // 
            this.BtnJournalDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnJournalDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnJournalDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnJournalDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnJournalDocNo.Appearance.Options.UseBackColor = true;
            this.BtnJournalDocNo.Appearance.Options.UseFont = true;
            this.BtnJournalDocNo.Appearance.Options.UseForeColor = true;
            this.BtnJournalDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnJournalDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnJournalDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnJournalDocNo.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnJournalDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnJournalDocNo.Location = new System.Drawing.Point(384, 242);
            this.BtnJournalDocNo.Name = "BtnJournalDocNo";
            this.BtnJournalDocNo.Size = new System.Drawing.Size(24, 20);
            this.BtnJournalDocNo.TabIndex = 50;
            this.BtnJournalDocNo.ToolTip = "Show Journal Information";
            this.BtnJournalDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnJournalDocNo.ToolTipTitle = "Run System";
            this.BtnJournalDocNo.Click += new System.EventHandler(this.BtnJournalDocNo_Click);
            // 
            // BtnVoucherJournalDocNo
            // 
            this.BtnVoucherJournalDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucherJournalDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucherJournalDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucherJournalDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucherJournalDocNo.Appearance.Options.UseBackColor = true;
            this.BtnVoucherJournalDocNo.Appearance.Options.UseFont = true;
            this.BtnVoucherJournalDocNo.Appearance.Options.UseForeColor = true;
            this.BtnVoucherJournalDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnVoucherJournalDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucherJournalDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucherJournalDocNo.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnVoucherJournalDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnVoucherJournalDocNo.Location = new System.Drawing.Point(384, 221);
            this.BtnVoucherJournalDocNo.Name = "BtnVoucherJournalDocNo";
            this.BtnVoucherJournalDocNo.Size = new System.Drawing.Size(24, 20);
            this.BtnVoucherJournalDocNo.TabIndex = 47;
            this.BtnVoucherJournalDocNo.ToolTip = "Show Journal Voucher Information";
            this.BtnVoucherJournalDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucherJournalDocNo.ToolTipTitle = "Run System";
            this.BtnVoucherJournalDocNo.Click += new System.EventHandler(this.BtnVoucherJournalDocNo_Click);
            // 
            // TxtVoucherJournalDocNo
            // 
            this.TxtVoucherJournalDocNo.EnterMoveNextControl = true;
            this.TxtVoucherJournalDocNo.Location = new System.Drawing.Point(151, 223);
            this.TxtVoucherJournalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherJournalDocNo.Name = "TxtVoucherJournalDocNo";
            this.TxtVoucherJournalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherJournalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherJournalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherJournalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherJournalDocNo.Properties.MaxLength = 30;
            this.TxtVoucherJournalDocNo.Properties.ReadOnly = true;
            this.TxtVoucherJournalDocNo.Size = new System.Drawing.Size(229, 20);
            this.TxtVoucherJournalDocNo.TabIndex = 46;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(42, 226);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(104, 14);
            this.label23.TabIndex = 45;
            this.label23.Text = "Journal Voucher#";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblEntCode
            // 
            this.LblEntCode.AutoSize = true;
            this.LblEntCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEntCode.ForeColor = System.Drawing.Color.Black;
            this.LblEntCode.Location = new System.Drawing.Point(107, 163);
            this.LblEntCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEntCode.Name = "LblEntCode";
            this.LblEntCode.Size = new System.Drawing.Size(39, 14);
            this.LblEntCode.TabIndex = 37;
            this.LblEntCode.Text = "Entity";
            this.LblEntCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEntCode
            // 
            this.LueEntCode.EnterMoveNextControl = true;
            this.LueEntCode.Location = new System.Drawing.Point(151, 160);
            this.LueEntCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEntCode.Name = "LueEntCode";
            this.LueEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.Appearance.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEntCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEntCode.Properties.DropDownRows = 30;
            this.LueEntCode.Properties.MaxLength = 40;
            this.LueEntCode.Properties.NullText = "[Empty]";
            this.LueEntCode.Properties.PopupWidth = 405;
            this.LueEntCode.Size = new System.Drawing.Size(405, 20);
            this.LueEntCode.TabIndex = 38;
            this.LueEntCode.ToolTip = "F4 : Show/hide list";
            this.LueEntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEntCode.EditValueChanged += new System.EventHandler(this.LueEntCode_EditValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Green;
            this.label21.Location = new System.Drawing.Point(412, 267);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(81, 14);
            this.label21.TabIndex = 53;
            this.label21.Text = "(Cancel Data)";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtJournalDocNo2
            // 
            this.TxtJournalDocNo2.EnterMoveNextControl = true;
            this.TxtJournalDocNo2.Location = new System.Drawing.Point(151, 265);
            this.TxtJournalDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo2.Name = "TxtJournalDocNo2";
            this.TxtJournalDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo2.Properties.MaxLength = 30;
            this.TxtJournalDocNo2.Properties.ReadOnly = true;
            this.TxtJournalDocNo2.Size = new System.Drawing.Size(229, 20);
            this.TxtJournalDocNo2.TabIndex = 51;
            // 
            // TxtJournalDocNo
            // 
            this.TxtJournalDocNo.EnterMoveNextControl = true;
            this.TxtJournalDocNo.Location = new System.Drawing.Point(151, 244);
            this.TxtJournalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo.Name = "TxtJournalDocNo";
            this.TxtJournalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo.Properties.MaxLength = 30;
            this.TxtJournalDocNo.Properties.ReadOnly = true;
            this.TxtJournalDocNo.Size = new System.Drawing.Size(229, 20);
            this.TxtJournalDocNo.TabIndex = 49;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(92, 247);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 14);
            this.label22.TabIndex = 48;
            this.label22.Text = "Journal#";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Green;
            this.label17.Location = new System.Drawing.Point(412, 184);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(146, 14);
            this.label17.TabIndex = 42;
            this.label17.Text = "(For Verification Purpose)";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.BtnBCCode);
            this.Tp3.Controls.Add(this.TxtBCCode);
            this.Tp3.Controls.Add(this.label28);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(1224, 356);
            this.Tp3.Text = "Additional";
            // 
            // BtnBCCode
            // 
            this.BtnBCCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBCCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBCCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBCCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBCCode.Appearance.Options.UseBackColor = true;
            this.BtnBCCode.Appearance.Options.UseFont = true;
            this.BtnBCCode.Appearance.Options.UseForeColor = true;
            this.BtnBCCode.Appearance.Options.UseTextOptions = true;
            this.BtnBCCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBCCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBCCode.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnBCCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnBCCode.Location = new System.Drawing.Point(639, 11);
            this.BtnBCCode.Name = "BtnBCCode";
            this.BtnBCCode.Size = new System.Drawing.Size(31, 20);
            this.BtnBCCode.TabIndex = 26;
            this.BtnBCCode.ToolTip = "Find Voucher Request";
            this.BtnBCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBCCode.ToolTipTitle = "Run System";
            this.BtnBCCode.Click += new System.EventHandler(this.BtnBCCode_Click);
            // 
            // TxtBCCode
            // 
            this.TxtBCCode.EnterMoveNextControl = true;
            this.TxtBCCode.Location = new System.Drawing.Point(119, 12);
            this.TxtBCCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBCCode.Name = "TxtBCCode";
            this.TxtBCCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBCCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBCCode.Properties.Appearance.Options.UseFont = true;
            this.TxtBCCode.Properties.MaxLength = 30;
            this.TxtBCCode.Properties.ReadOnly = true;
            this.TxtBCCode.Size = new System.Drawing.Size(520, 20);
            this.TxtBCCode.TabIndex = 16;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(17, 13);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(100, 14);
            this.label28.TabIndex = 21;
            this.label28.Text = "Budget Category";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp4
            // 
            this.Tp4.Appearance.Header.Options.UseTextOptions = true;
            this.Tp4.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp4.Controls.Add(this.Grd2);
            this.Tp4.Margin = new System.Windows.Forms.Padding(2);
            this.Tp4.Name = "Tp4";
            this.Tp4.Size = new System.Drawing.Size(766, 356);
            this.Tp4.Text = "InterOffice Journal#";
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 356);
            this.Grd2.TabIndex = 37;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // Tp5
            // 
            this.Tp5.Appearance.Header.Options.UseTextOptions = true;
            this.Tp5.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp5.Controls.Add(this.LueBankAcCode3);
            this.Tp5.Controls.Add(this.Grd3);
            this.Tp5.Name = "Tp5";
            this.Tp5.Size = new System.Drawing.Size(766, 356);
            this.Tp5.Text = "Additional Cost and Others";
            // 
            // LueBankAcCode3
            // 
            this.LueBankAcCode3.EnterMoveNextControl = true;
            this.LueBankAcCode3.Location = new System.Drawing.Point(205, 43);
            this.LueBankAcCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode3.Name = "LueBankAcCode3";
            this.LueBankAcCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode3.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode3.Properties.DropDownRows = 20;
            this.LueBankAcCode3.Properties.NullText = "[Empty]";
            this.LueBankAcCode3.Properties.PopupWidth = 500;
            this.LueBankAcCode3.Size = new System.Drawing.Size(171, 20);
            this.LueBankAcCode3.TabIndex = 38;
            this.LueBankAcCode3.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode3.EditValueChanged += new System.EventHandler(this.LueBankAcCode3_EditValueChanged);
            this.LueBankAcCode3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueBankAcCode3_KeyDown);
            this.LueBankAcCode3.Leave += new System.EventHandler(this.LueBankAcCode3_Leave);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(766, 356);
            this.Grd3.TabIndex = 37;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.TxtDocNo);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.DteDocDt);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1230, 51);
            this.panel5.TabIndex = 10;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.MeeCancelReason);
            this.panel6.Controls.Add(this.ChkCancelInd);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(782, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(448, 51);
            this.panel6.TabIndex = 15;
            // 
            // LblEmpName
            // 
            this.LblEmpName.AutoSize = true;
            this.LblEmpName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEmpName.ForeColor = System.Drawing.Color.Black;
            this.LblEmpName.Location = new System.Drawing.Point(6, 312);
            this.LblEmpName.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEmpName.Name = "LblEmpName";
            this.LblEmpName.Size = new System.Drawing.Size(140, 14);
            this.LblEmpName.TabIndex = 59;
            this.LblEmpName.Text = "Employee Cash Adv. PIC";
            this.LblEmpName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(151, 309);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 30;
            this.TxtEmpName.Properties.ReadOnly = true;
            this.TxtEmpName.Size = new System.Drawing.Size(320, 20);
            this.TxtEmpName.TabIndex = 60;
            // 
            // FrmVoucher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 509);
            this.Name = "FrmVoucher";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePIC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdInvNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExcRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOpeningDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOpeningDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcVoucher)).EndInit();
            this.TcVoucher.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.Tp1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAddCostAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBillingID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt2.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.Tp2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInterOfficeInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashTypeGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashTypeCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashTypeCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherJournalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            this.Tp3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBCCode.Properties)).EndInit();
            this.Tp4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.Tp5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.LookUpEdit LueAcType;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestDocNo;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtGiroNo;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.DateEdit DteDueDt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.LookUpEdit LuePaymentType;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label LblRemark;
        private System.Windows.Forms.Label LblBankAcCode;
        private DevExpress.XtraEditors.LookUpEdit LueBankAcCode;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LuePIC;
        public DevExpress.XtraEditors.SimpleButton BtnVoucherRequest;
        internal DevExpress.XtraEditors.TextEdit TxtDocType;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtVdInvNo;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label LblBankAcCode2;
        private DevExpress.XtraEditors.LookUpEdit LueBankAcCode2;
        private System.Windows.Forms.Label label24;
        private DevExpress.XtraEditors.LookUpEdit LueAcType2;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtCurCode;
        internal DevExpress.XtraEditors.TextEdit TxtExcRate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtCurCode2;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.DateEdit DteOpeningDt;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraTab.XtraTabControl TcVoucher;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtAmt2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherJournalDocNo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label LblEntCode;
        internal DevExpress.XtraEditors.LookUpEdit LueEntCode;
        public DevExpress.XtraEditors.SimpleButton BtnVoucherRequestDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnJournalDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnJournalDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnVoucherJournalDocNo;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        public DevExpress.XtraEditors.SimpleButton BtnDocType;
        private DevExpress.XtraEditors.LookUpEdit LueCashTypeCode;
        private System.Windows.Forms.Label LblCashType;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        private System.Windows.Forms.Label label28;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo;
        private System.Windows.Forms.Label LblSOContractDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtSOContractDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnBCCode;
        internal DevExpress.XtraEditors.TextEdit TxtBCCode;
        internal DevExpress.XtraEditors.TextEdit TxtBillingID;
        private System.Windows.Forms.Label LblBillingID;
        private DevExpress.XtraEditors.LookUpEdit LueCashTypeCode2;
        private System.Windows.Forms.Label LblCashType2;
        private DevExpress.XtraEditors.LookUpEdit LueCashTypeGroup;
        private System.Windows.Forms.Label LblCashTypeGroup;
        private DevExpress.XtraTab.XtraTabPage Tp4;
        private DevExpress.XtraEditors.CheckEdit ChkInterOfficeInd;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraTab.XtraTabPage Tp5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private DevExpress.XtraEditors.LookUpEdit LueBankAcCode3;
        internal DevExpress.XtraEditors.TextEdit TxtAddCostAmt;
        private System.Windows.Forms.Label LblAdditionalAmt;
        private System.Windows.Forms.Label LblEmpName;
        internal DevExpress.XtraEditors.TextEdit TxtEmpName;
    }
}