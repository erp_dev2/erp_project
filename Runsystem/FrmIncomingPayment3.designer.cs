﻿namespace RunSystem
{
    partial class FrmIncomingPayment3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmIncomingPayment3));
            this.panel3 = new System.Windows.Forms.Panel();
            this.BtnCtCode = new DevExpress.XtraEditors.SimpleButton();
            this.label44 = new System.Windows.Forms.Label();
            this.LueCtCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPaymentUser = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.DteDueDt = new DevExpress.XtraEditors.DateEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtGiroNo = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueAcType = new DevExpress.XtraEditors.LookUpEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.LuePaymentType = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.LueBankAcCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LuePaidToBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPaidToBankAcNo = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtPaidToBankBranch = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtPaidToBankAcName = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.TxtVoucherDocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtVoucherRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.MeeVoucherRequestSummaryDesc = new DevExpress.XtraEditors.MemoExEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkMeeVoucherRequestSummaryInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel6 = new System.Windows.Forms.Panel();
            this.TxtTotalAmt = new DevExpress.XtraEditors.TextEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.TxtDepositAmt = new DevExpress.XtraEditors.TextEdit();
            this.LblDepositAmt = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtAmt2 = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.LueCurCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtRateAmt = new DevExpress.XtraEditors.TextEdit();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.TcIncomingPayment = new DevExpress.XtraTab.XtraTabControl();
            this.TpInvoice = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TpGiro = new DevExpress.XtraTab.XtraTabPage();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.TxtGiroAmt = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.TpCOA = new DevExpress.XtraTab.XtraTabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel7 = new System.Windows.Forms.Panel();
            this.TxtVoucherRequestDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtVoucherDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.TxtCOAAmt = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.TpCOA2 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.panel11 = new System.Windows.Forms.Panel();
            this.TxtVoucherRequestDocNo3 = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.TxtVoucherDocNo3 = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.TxtCOAAmt2 = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.TpApproval = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgProject = new DevExpress.XtraTab.XtraTabPage();
            this.label41 = new System.Windows.Forms.Label();
            this.LueProjectDocNo3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.LueProjectDocNo2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.LueProjectDocNo1 = new DevExpress.XtraEditors.LookUpEdit();
            this.TpDeposit = new DevExpress.XtraTab.XtraTabPage();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.TpInvoiceItem = new DevExpress.XtraTab.XtraTabPage();
            this.panel14 = new System.Windows.Forms.Panel();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.panel13 = new System.Windows.Forms.Panel();
            this.BtnRefreshData = new DevExpress.XtraEditors.SimpleButton();
            this.LblEntCode = new System.Windows.Forms.Label();
            this.LueEntCode = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.memoExEdit1 = new DevExpress.XtraEditors.MemoExEdit();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaymentUser.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaidToBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankAcName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeVoucherRequestSummaryDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkMeeVoucherRequestSummaryInd.Properties)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDepositAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRateAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcIncomingPayment)).BeginInit();
            this.TcIncomingPayment.SuspendLayout();
            this.TpInvoice.SuspendLayout();
            this.panel5.SuspendLayout();
            this.TpGiro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroAmt.Properties)).BeginInit();
            this.TpCOA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo2.Properties)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCOAAmt.Properties)).BeginInit();
            this.TpCOA2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo3.Properties)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCOAAmt2.Properties)).BeginInit();
            this.TpApproval.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TpgProject.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectDocNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectDocNo1.Properties)).BeginInit();
            this.TpDeposit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.TpInvoiceItem.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Location = new System.Drawing.Point(845, 0);
            this.panel1.Size = new System.Drawing.Size(70, 520);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcIncomingPayment);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(845, 520);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.BtnCtCode);
            this.panel3.Controls.Add(this.label44);
            this.panel3.Controls.Add(this.LueCtCtCode);
            this.panel3.Controls.Add(this.TxtPaymentUser);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.LuePaidToBankCode);
            this.panel3.Controls.Add(this.TxtPaidToBankAcNo);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.TxtPaidToBankBranch);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.TxtPaidToBankAcName);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.LueCtCode);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(845, 199);
            this.panel3.TabIndex = 10;
            // 
            // BtnCtCode
            // 
            this.BtnCtCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtCode.Appearance.Options.UseBackColor = true;
            this.BtnCtCode.Appearance.Options.UseFont = true;
            this.BtnCtCode.Appearance.Options.UseForeColor = true;
            this.BtnCtCode.Appearance.Options.UseTextOptions = true;
            this.BtnCtCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtCode.Image")));
            this.BtnCtCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtCode.Location = new System.Drawing.Point(365, 68);
            this.BtnCtCode.Name = "BtnCtCode";
            this.BtnCtCode.Size = new System.Drawing.Size(24, 21);
            this.BtnCtCode.TabIndex = 19;
            this.BtnCtCode.ToolTip = "Find Vendor";
            this.BtnCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtCode.ToolTipTitle = "Run System";
            this.BtnCtCode.Click += new System.EventHandler(this.BtnCtCode_Click);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Red;
            this.label44.Location = new System.Drawing.Point(1, 50);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(112, 14);
            this.label44.TabIndex = 15;
            this.label44.Text = "Customer Category";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCtCode
            // 
            this.LueCtCtCode.EnterMoveNextControl = true;
            this.LueCtCtCode.Location = new System.Drawing.Point(116, 47);
            this.LueCtCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCtCode.Name = "LueCtCtCode";
            this.LueCtCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCtCode.Properties.DropDownRows = 30;
            this.LueCtCtCode.Properties.NullText = "[Empty]";
            this.LueCtCtCode.Properties.PopupWidth = 300;
            this.LueCtCtCode.Size = new System.Drawing.Size(274, 20);
            this.LueCtCtCode.TabIndex = 16;
            this.LueCtCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCtCode.EditValueChanged += new System.EventHandler(this.LueCtCtCode_EditValueChanged);
            // 
            // TxtPaymentUser
            // 
            this.TxtPaymentUser.EnterMoveNextControl = true;
            this.TxtPaymentUser.Location = new System.Drawing.Point(116, 89);
            this.TxtPaymentUser.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaymentUser.Name = "TxtPaymentUser";
            this.TxtPaymentUser.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPaymentUser.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaymentUser.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaymentUser.Properties.Appearance.Options.UseFont = true;
            this.TxtPaymentUser.Properties.MaxLength = 50;
            this.TxtPaymentUser.Size = new System.Drawing.Size(274, 20);
            this.TxtPaymentUser.TabIndex = 21;
            this.TxtPaymentUser.Validated += new System.EventHandler(this.TxtPaymentUser_Validated);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(65, 92);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 14);
            this.label24.TabIndex = 20;
            this.label24.Text = "Paid To";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.MeeCancelReason);
            this.panel4.Controls.Add(this.DteDueDt);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.TxtGiroNo);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.TxtStatus);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.LueBankCode);
            this.panel4.Controls.Add(this.LueAcType);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.LuePaymentType);
            this.panel4.Controls.Add(this.ChkCancelInd);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.LueBankAcCode);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(457, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(388, 199);
            this.panel4.TabIndex = 29;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(2, 29);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(135, 14);
            this.label27.TabIndex = 32;
            this.label27.Text = "Reason For Cancellation";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(141, 26);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(178, 20);
            this.MeeCancelReason.TabIndex = 33;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // DteDueDt
            // 
            this.DteDueDt.EditValue = null;
            this.DteDueDt.EnterMoveNextControl = true;
            this.DteDueDt.Location = new System.Drawing.Point(141, 152);
            this.DteDueDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDt.Name = "DteDueDt";
            this.DteDueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.Appearance.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDt.Properties.MaxLength = 8;
            this.DteDueDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDt.Size = new System.Drawing.Size(106, 20);
            this.DteDueDt.TabIndex = 46;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(78, 155);
            this.label12.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 14);
            this.label12.TabIndex = 45;
            this.label12.Text = "Due Date";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGiroNo
            // 
            this.TxtGiroNo.EnterMoveNextControl = true;
            this.TxtGiroNo.Location = new System.Drawing.Point(141, 131);
            this.TxtGiroNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGiroNo.Name = "TxtGiroNo";
            this.TxtGiroNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGiroNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGiroNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGiroNo.Properties.Appearance.Options.UseFont = true;
            this.TxtGiroNo.Properties.MaxLength = 80;
            this.TxtGiroNo.Size = new System.Drawing.Size(242, 20);
            this.TxtGiroNo.TabIndex = 44;
            this.TxtGiroNo.Validated += new System.EventHandler(this.TxtGiroNo_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(21, 134);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 14);
            this.label10.TabIndex = 43;
            this.label10.Text = "Giro Bilyet / Cheque";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(69, 113);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 14);
            this.label14.TabIndex = 41;
            this.label14.Text = "Bank Name";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(141, 5);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 16;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(178, 20);
            this.TxtStatus.TabIndex = 31;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(52, 49);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 14);
            this.label16.TabIndex = 35;
            this.label16.Text = "Account Type";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(95, 8);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 14);
            this.label17.TabIndex = 30;
            this.label17.Text = "Status";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(141, 110);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 30;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 300;
            this.LueBankCode.Size = new System.Drawing.Size(242, 20);
            this.LueBankCode.TabIndex = 42;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // LueAcType
            // 
            this.LueAcType.EnterMoveNextControl = true;
            this.LueAcType.Location = new System.Drawing.Point(141, 47);
            this.LueAcType.Margin = new System.Windows.Forms.Padding(5);
            this.LueAcType.Name = "LueAcType";
            this.LueAcType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.Appearance.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAcType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAcType.Properties.DropDownRows = 20;
            this.LueAcType.Properties.NullText = "[Empty]";
            this.LueAcType.Properties.PopupWidth = 200;
            this.LueAcType.Size = new System.Drawing.Size(178, 20);
            this.LueAcType.TabIndex = 36;
            this.LueAcType.ToolTip = "F4 : Show/hide list";
            this.LueAcType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAcType.EditValueChanged += new System.EventHandler(this.LueAcType_EditValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(50, 71);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(87, 14);
            this.label15.TabIndex = 37;
            this.label15.Text = "Payment Type";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePaymentType
            // 
            this.LuePaymentType.EnterMoveNextControl = true;
            this.LuePaymentType.Location = new System.Drawing.Point(141, 68);
            this.LuePaymentType.Margin = new System.Windows.Forms.Padding(5);
            this.LuePaymentType.Name = "LuePaymentType";
            this.LuePaymentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.Appearance.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePaymentType.Properties.DropDownRows = 30;
            this.LuePaymentType.Properties.NullText = "[Empty]";
            this.LuePaymentType.Properties.PopupWidth = 300;
            this.LuePaymentType.Size = new System.Drawing.Size(242, 20);
            this.LuePaymentType.TabIndex = 38;
            this.LuePaymentType.ToolTip = "F4 : Show/hide list";
            this.LuePaymentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePaymentType.EditValueChanged += new System.EventHandler(this.LuePaymentType_EditValueChanged);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(319, 25);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 34;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(84, 92);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 14);
            this.label13.TabIndex = 39;
            this.label13.Text = "Account";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankAcCode
            // 
            this.LueBankAcCode.EnterMoveNextControl = true;
            this.LueBankAcCode.Location = new System.Drawing.Point(141, 89);
            this.LueBankAcCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode.Name = "LueBankAcCode";
            this.LueBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode.Properties.DropDownRows = 30;
            this.LueBankAcCode.Properties.NullText = "[Empty]";
            this.LueBankAcCode.Properties.PopupWidth = 300;
            this.LueBankAcCode.Size = new System.Drawing.Size(242, 20);
            this.LueBankAcCode.TabIndex = 40;
            this.LueBankAcCode.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode.EditValueChanged += new System.EventHandler(this.LueBankAcCode_EditValueChanged);
            // 
            // LuePaidToBankCode
            // 
            this.LuePaidToBankCode.EnterMoveNextControl = true;
            this.LuePaidToBankCode.Location = new System.Drawing.Point(116, 110);
            this.LuePaidToBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePaidToBankCode.Name = "LuePaidToBankCode";
            this.LuePaidToBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaidToBankCode.Properties.Appearance.Options.UseFont = true;
            this.LuePaidToBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaidToBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePaidToBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaidToBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePaidToBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaidToBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePaidToBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaidToBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePaidToBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePaidToBankCode.Properties.DropDownRows = 30;
            this.LuePaidToBankCode.Properties.NullText = "[Empty]";
            this.LuePaidToBankCode.Properties.PopupWidth = 300;
            this.LuePaidToBankCode.Size = new System.Drawing.Size(274, 20);
            this.LuePaidToBankCode.TabIndex = 23;
            this.LuePaidToBankCode.ToolTip = "F4 : Show/hide list";
            this.LuePaidToBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePaidToBankCode.EditValueChanged += new System.EventHandler(this.LuePaidToBankCode_EditValueChanged);
            // 
            // TxtPaidToBankAcNo
            // 
            this.TxtPaidToBankAcNo.EnterMoveNextControl = true;
            this.TxtPaidToBankAcNo.Location = new System.Drawing.Point(116, 173);
            this.TxtPaidToBankAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaidToBankAcNo.Name = "TxtPaidToBankAcNo";
            this.TxtPaidToBankAcNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPaidToBankAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaidToBankAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaidToBankAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPaidToBankAcNo.Properties.MaxLength = 80;
            this.TxtPaidToBankAcNo.Size = new System.Drawing.Size(274, 20);
            this.TxtPaidToBankAcNo.TabIndex = 29;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(25, 155);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(88, 14);
            this.label22.TabIndex = 26;
            this.label22.Text = "Account Name";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPaidToBankBranch
            // 
            this.TxtPaidToBankBranch.EnterMoveNextControl = true;
            this.TxtPaidToBankBranch.Location = new System.Drawing.Point(116, 131);
            this.TxtPaidToBankBranch.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaidToBankBranch.Name = "TxtPaidToBankBranch";
            this.TxtPaidToBankBranch.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPaidToBankBranch.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaidToBankBranch.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaidToBankBranch.Properties.Appearance.Options.UseFont = true;
            this.TxtPaidToBankBranch.Properties.MaxLength = 80;
            this.TxtPaidToBankBranch.Size = new System.Drawing.Size(274, 20);
            this.TxtPaidToBankBranch.TabIndex = 25;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(34, 134);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 14);
            this.label21.TabIndex = 24;
            this.label21.Text = "Branch Name";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(45, 113);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 14);
            this.label20.TabIndex = 22;
            this.label20.Text = "Bank Name";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPaidToBankAcName
            // 
            this.TxtPaidToBankAcName.EnterMoveNextControl = true;
            this.TxtPaidToBankAcName.Location = new System.Drawing.Point(116, 152);
            this.TxtPaidToBankAcName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaidToBankAcName.Name = "TxtPaidToBankAcName";
            this.TxtPaidToBankAcName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPaidToBankAcName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaidToBankAcName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaidToBankAcName.Properties.Appearance.Options.UseFont = true;
            this.TxtPaidToBankAcName.Properties.MaxLength = 80;
            this.TxtPaidToBankAcName.Size = new System.Drawing.Size(274, 20);
            this.TxtPaidToBankAcName.TabIndex = 27;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(51, 176);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(62, 14);
            this.label23.TabIndex = 28;
            this.label23.Text = "Account#";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(54, 71);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Customer";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(116, 68);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(244, 20);
            this.LueCtCode.TabIndex = 18;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(116, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(121, 20);
            this.DteDocDt.TabIndex = 14;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(80, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(116, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(274, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(40, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(100, 75);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 53;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EditValue = "";
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(151, 72);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(257, 20);
            this.MeeRemark.TabIndex = 54;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(839, 126);
            this.Grd1.TabIndex = 47;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // TxtVoucherDocNo
            // 
            this.TxtVoucherDocNo.EnterMoveNextControl = true;
            this.TxtVoucherDocNo.Location = new System.Drawing.Point(151, 51);
            this.TxtVoucherDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo.Name = "TxtVoucherDocNo";
            this.TxtVoucherDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo.Properties.MaxLength = 16;
            this.TxtVoucherDocNo.Properties.ReadOnly = true;
            this.TxtVoucherDocNo.Size = new System.Drawing.Size(257, 20);
            this.TxtVoucherDocNo.TabIndex = 52;
            // 
            // TxtVoucherRequestDocNo
            // 
            this.TxtVoucherRequestDocNo.EnterMoveNextControl = true;
            this.TxtVoucherRequestDocNo.Location = new System.Drawing.Point(151, 9);
            this.TxtVoucherRequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestDocNo.Name = "TxtVoucherRequestDocNo";
            this.TxtVoucherRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestDocNo.Properties.MaxLength = 16;
            this.TxtVoucherRequestDocNo.Properties.ReadOnly = true;
            this.TxtVoucherRequestDocNo.Size = new System.Drawing.Size(257, 20);
            this.TxtVoucherRequestDocNo.TabIndex = 47;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(94, 55);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 14);
            this.label6.TabIndex = 51;
            this.label6.Text = "Voucher";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeVoucherRequestSummaryDesc
            // 
            this.MeeVoucherRequestSummaryDesc.EnterMoveNextControl = true;
            this.MeeVoucherRequestSummaryDesc.Location = new System.Drawing.Point(151, 30);
            this.MeeVoucherRequestSummaryDesc.Margin = new System.Windows.Forms.Padding(5);
            this.MeeVoucherRequestSummaryDesc.Name = "MeeVoucherRequestSummaryDesc";
            this.MeeVoucherRequestSummaryDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeVoucherRequestSummaryDesc.Properties.Appearance.Options.UseFont = true;
            this.MeeVoucherRequestSummaryDesc.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeVoucherRequestSummaryDesc.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeVoucherRequestSummaryDesc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeVoucherRequestSummaryDesc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeVoucherRequestSummaryDesc.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeVoucherRequestSummaryDesc.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeVoucherRequestSummaryDesc.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeVoucherRequestSummaryDesc.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeVoucherRequestSummaryDesc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeVoucherRequestSummaryDesc.Properties.MaxLength = 400;
            this.MeeVoucherRequestSummaryDesc.Properties.PopupFormSize = new System.Drawing.Size(250, 20);
            this.MeeVoucherRequestSummaryDesc.Properties.ShowIcon = false;
            this.MeeVoucherRequestSummaryDesc.Size = new System.Drawing.Size(237, 20);
            this.MeeVoucherRequestSummaryDesc.TabIndex = 49;
            this.MeeVoucherRequestSummaryDesc.ToolTip = "F4 : Show/hide text";
            this.MeeVoucherRequestSummaryDesc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeVoucherRequestSummaryDesc.ToolTipTitle = "Run System";
            this.MeeVoucherRequestSummaryDesc.Validated += new System.EventHandler(this.MeeVoucherRequestSummaryDesc_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 34);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 14);
            this.label7.TabIndex = 48;
            this.label7.Text = "VR Summary Description";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(7, 12);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 14);
            this.label4.TabIndex = 46;
            this.label4.Text = "Voucher Request# (VR)";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkMeeVoucherRequestSummaryInd
            // 
            this.ChkMeeVoucherRequestSummaryInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkMeeVoucherRequestSummaryInd.Location = new System.Drawing.Point(388, 28);
            this.ChkMeeVoucherRequestSummaryInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkMeeVoucherRequestSummaryInd.Name = "ChkMeeVoucherRequestSummaryInd";
            this.ChkMeeVoucherRequestSummaryInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkMeeVoucherRequestSummaryInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkMeeVoucherRequestSummaryInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkMeeVoucherRequestSummaryInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkMeeVoucherRequestSummaryInd.Properties.Appearance.Options.UseFont = true;
            this.ChkMeeVoucherRequestSummaryInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkMeeVoucherRequestSummaryInd.Properties.Caption = "";
            this.ChkMeeVoucherRequestSummaryInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkMeeVoucherRequestSummaryInd.Size = new System.Drawing.Size(20, 22);
            this.ChkMeeVoucherRequestSummaryInd.TabIndex = 50;
            this.ChkMeeVoucherRequestSummaryInd.ToolTip = "Summary for Voucher Request Description";
            this.ChkMeeVoucherRequestSummaryInd.CheckedChanged += new System.EventHandler(this.ChkMeeVoucherRequestSummaryInd_CheckedChanged);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.TxtTotalAmt);
            this.panel6.Controls.Add(this.label45);
            this.panel6.Controls.Add(this.TxtDepositAmt);
            this.panel6.Controls.Add(this.LblDepositAmt);
            this.panel6.Controls.Add(this.LueCurCode);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.TxtAmt2);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.TxtAmt);
            this.panel6.Controls.Add(this.LueCurCode2);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.TxtRateAmt);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(490, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(349, 167);
            this.panel6.TabIndex = 55;
            // 
            // TxtTotalAmt
            // 
            this.TxtTotalAmt.EnterMoveNextControl = true;
            this.TxtTotalAmt.Location = new System.Drawing.Point(170, 9);
            this.TxtTotalAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalAmt.Name = "TxtTotalAmt";
            this.TxtTotalAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalAmt.Properties.ReadOnly = true;
            this.TxtTotalAmt.Size = new System.Drawing.Size(174, 20);
            this.TxtTotalAmt.TabIndex = 56;
            this.TxtTotalAmt.ToolTip = "Based On Purchase Invoice\'s Currency";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(44, 12);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(109, 14);
            this.label45.TabIndex = 55;
            this.label45.Text = "Total Sales Invoice";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDepositAmt
            // 
            this.TxtDepositAmt.EnterMoveNextControl = true;
            this.TxtDepositAmt.Location = new System.Drawing.Point(170, 136);
            this.TxtDepositAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDepositAmt.Name = "TxtDepositAmt";
            this.TxtDepositAmt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtDepositAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDepositAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDepositAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtDepositAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDepositAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDepositAmt.Properties.MaxLength = 80;
            this.TxtDepositAmt.Properties.ReadOnly = true;
            this.TxtDepositAmt.Size = new System.Drawing.Size(174, 20);
            this.TxtDepositAmt.TabIndex = 68;
            this.TxtDepositAmt.EditValueChanged += new System.EventHandler(this.TxtDepositAmt_EditValueChanged);
            this.TxtDepositAmt.Validated += new System.EventHandler(this.TxtDepositAmt_Validated);
            // 
            // LblDepositAmt
            // 
            this.LblDepositAmt.AutoSize = true;
            this.LblDepositAmt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDepositAmt.ForeColor = System.Drawing.Color.Black;
            this.LblDepositAmt.Location = new System.Drawing.Point(71, 139);
            this.LblDepositAmt.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDepositAmt.Name = "LblDepositAmt";
            this.LblDepositAmt.Size = new System.Drawing.Size(96, 14);
            this.LblDepositAmt.TabIndex = 67;
            this.LblDepositAmt.Text = "Deposit Amount";
            this.LblDepositAmt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(170, 31);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 30;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 200;
            this.LueCurCode.Size = new System.Drawing.Size(174, 20);
            this.LueCurCode.TabIndex = 58;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(38, 33);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(129, 14);
            this.label18.TabIndex = 57;
            this.label18.Text = "Sales Invoice Currency";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmt2
            // 
            this.TxtAmt2.EnterMoveNextControl = true;
            this.TxtAmt2.Location = new System.Drawing.Point(170, 115);
            this.TxtAmt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt2.Name = "TxtAmt2";
            this.TxtAmt2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt2.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt2.Properties.ReadOnly = true;
            this.TxtAmt2.Size = new System.Drawing.Size(174, 20);
            this.TxtAmt2.TabIndex = 66;
            this.TxtAmt2.ToolTip = "Based On Outgoing Payment\'s Currency";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(10, 118);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(157, 14);
            this.label25.TabIndex = 65;
            this.label25.Text = "Incoming Payment Amount";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(170, 52);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.Size = new System.Drawing.Size(174, 20);
            this.TxtAmt.TabIndex = 60;
            this.TxtAmt.ToolTip = "Based On Purchase Invoice\'s Currency";
            // 
            // LueCurCode2
            // 
            this.LueCurCode2.EnterMoveNextControl = true;
            this.LueCurCode2.Location = new System.Drawing.Point(170, 72);
            this.LueCurCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode2.Name = "LueCurCode2";
            this.LueCurCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode2.Properties.DropDownRows = 30;
            this.LueCurCode2.Properties.NullText = "[Empty]";
            this.LueCurCode2.Properties.PopupWidth = 200;
            this.LueCurCode2.Size = new System.Drawing.Size(174, 20);
            this.LueCurCode2.TabIndex = 62;
            this.LueCurCode2.ToolTip = "F4 : Show/hide list";
            this.LueCurCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode2.EditValueChanged += new System.EventHandler(this.LueCurCode2_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(10, 75);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(161, 14);
            this.label8.TabIndex = 61;
            this.label8.Text = "Incoming Payment Currency";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(42, 55);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(125, 14);
            this.label11.TabIndex = 59;
            this.label11.Text = "Sales Invoice Amount";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(135, 96);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 14);
            this.label9.TabIndex = 63;
            this.label9.Text = "Rate";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRateAmt
            // 
            this.TxtRateAmt.EnterMoveNextControl = true;
            this.TxtRateAmt.Location = new System.Drawing.Point(170, 93);
            this.TxtRateAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRateAmt.Name = "TxtRateAmt";
            this.TxtRateAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRateAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRateAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRateAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtRateAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRateAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRateAmt.Size = new System.Drawing.Size(174, 20);
            this.TxtRateAmt.TabIndex = 64;
            this.TxtRateAmt.Validated += new System.EventHandler(this.TxtRateAmt_Validated);
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 478);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 8;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            this.LueFontSize.EditValueChanged += new System.EventHandler(this.LueFontSize_EditValueChanged);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 498);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 9;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // TcIncomingPayment
            // 
            this.TcIncomingPayment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcIncomingPayment.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcIncomingPayment.Location = new System.Drawing.Point(0, 199);
            this.TcIncomingPayment.Name = "TcIncomingPayment";
            this.TcIncomingPayment.SelectedTabPage = this.TpInvoice;
            this.TcIncomingPayment.Size = new System.Drawing.Size(845, 321);
            this.TcIncomingPayment.TabIndex = 47;
            this.TcIncomingPayment.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TpInvoice,
            this.TpGiro,
            this.TpCOA,
            this.TpCOA2,
            this.TpApproval,
            this.TpgProject,
            this.TpDeposit,
            this.TpInvoiceItem});
            // 
            // TpInvoice
            // 
            this.TpInvoice.Appearance.Header.Options.UseTextOptions = true;
            this.TpInvoice.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpInvoice.Controls.Add(this.Grd1);
            this.TpInvoice.Controls.Add(this.panel5);
            this.TpInvoice.Name = "TpInvoice";
            this.TpInvoice.Size = new System.Drawing.Size(839, 293);
            this.TpInvoice.Text = "List of Invoices";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.TxtVoucherRequestDocNo);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.MeeVoucherRequestSummaryDesc);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.TxtVoucherDocNo);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.ChkMeeVoucherRequestSummaryInd);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 126);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(839, 167);
            this.panel5.TabIndex = 45;
            // 
            // TpGiro
            // 
            this.TpGiro.Appearance.Header.Options.UseTextOptions = true;
            this.TpGiro.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpGiro.Controls.Add(this.Grd4);
            this.TpGiro.Controls.Add(this.panel9);
            this.TpGiro.Name = "TpGiro";
            this.TpGiro.Size = new System.Drawing.Size(766, 6);
            this.TpGiro.Text = "Giro";
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(766, 0);
            this.Grd4.TabIndex = 46;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd4_EllipsisButtonClick);
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel9.Location = new System.Drawing.Point(0, -25);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(766, 31);
            this.panel9.TabIndex = 47;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.TxtGiroAmt);
            this.panel10.Controls.Add(this.label31);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel10.Location = new System.Drawing.Point(536, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(230, 31);
            this.panel10.TabIndex = 48;
            // 
            // TxtGiroAmt
            // 
            this.TxtGiroAmt.EnterMoveNextControl = true;
            this.TxtGiroAmt.Location = new System.Drawing.Point(63, 5);
            this.TxtGiroAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGiroAmt.Name = "TxtGiroAmt";
            this.TxtGiroAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGiroAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGiroAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGiroAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtGiroAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGiroAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtGiroAmt.Properties.ReadOnly = true;
            this.TxtGiroAmt.Size = new System.Drawing.Size(160, 20);
            this.TxtGiroAmt.TabIndex = 50;
            this.TxtGiroAmt.ToolTip = "Based On Purchase Invoice\'s Currency";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(6, 8);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(51, 14);
            this.label31.TabIndex = 49;
            this.label31.Text = "Amount";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpCOA
            // 
            this.TpCOA.Appearance.Header.Options.UseTextOptions = true;
            this.TpCOA.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpCOA.Controls.Add(this.Grd3);
            this.TpCOA.Controls.Add(this.panel7);
            this.TpCOA.Name = "TpCOA";
            this.TpCOA.Size = new System.Drawing.Size(766, 6);
            this.TpCOA.Text = "COA\'s Account List";
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(766, 0);
            this.Grd3.TabIndex = 44;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.TxtVoucherRequestDocNo2);
            this.panel7.Controls.Add(this.label19);
            this.panel7.Controls.Add(this.TxtVoucherDocNo2);
            this.panel7.Controls.Add(this.label26);
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, -45);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(766, 51);
            this.panel7.TabIndex = 45;
            // 
            // TxtVoucherRequestDocNo2
            // 
            this.TxtVoucherRequestDocNo2.EnterMoveNextControl = true;
            this.TxtVoucherRequestDocNo2.Location = new System.Drawing.Point(124, 5);
            this.TxtVoucherRequestDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestDocNo2.Name = "TxtVoucherRequestDocNo2";
            this.TxtVoucherRequestDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestDocNo2.Properties.MaxLength = 16;
            this.TxtVoucherRequestDocNo2.Properties.ReadOnly = true;
            this.TxtVoucherRequestDocNo2.Size = new System.Drawing.Size(212, 20);
            this.TxtVoucherRequestDocNo2.TabIndex = 51;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(57, 29);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 14);
            this.label19.TabIndex = 52;
            this.label19.Text = "Voucher#";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherDocNo2
            // 
            this.TxtVoucherDocNo2.EnterMoveNextControl = true;
            this.TxtVoucherDocNo2.Location = new System.Drawing.Point(124, 26);
            this.TxtVoucherDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo2.Name = "TxtVoucherDocNo2";
            this.TxtVoucherDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo2.Properties.MaxLength = 16;
            this.TxtVoucherDocNo2.Properties.ReadOnly = true;
            this.TxtVoucherDocNo2.Size = new System.Drawing.Size(212, 20);
            this.TxtVoucherDocNo2.TabIndex = 53;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(8, 8);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(111, 14);
            this.label26.TabIndex = 50;
            this.label26.Text = "Voucher Request#";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.TxtCOAAmt);
            this.panel8.Controls.Add(this.label28);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(536, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(230, 51);
            this.panel8.TabIndex = 50;
            // 
            // TxtCOAAmt
            // 
            this.TxtCOAAmt.EnterMoveNextControl = true;
            this.TxtCOAAmt.Location = new System.Drawing.Point(63, 5);
            this.TxtCOAAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCOAAmt.Name = "TxtCOAAmt";
            this.TxtCOAAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCOAAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCOAAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCOAAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtCOAAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCOAAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCOAAmt.Properties.ReadOnly = true;
            this.TxtCOAAmt.Size = new System.Drawing.Size(160, 20);
            this.TxtCOAAmt.TabIndex = 55;
            this.TxtCOAAmt.ToolTip = "Based On Purchase Invoice\'s Currency";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(6, 8);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(51, 14);
            this.label28.TabIndex = 54;
            this.label28.Text = "Amount";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpCOA2
            // 
            this.TpCOA2.Appearance.Header.Options.UseTextOptions = true;
            this.TpCOA2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpCOA2.Controls.Add(this.Grd5);
            this.TpCOA2.Controls.Add(this.panel11);
            this.TpCOA2.Name = "TpCOA2";
            this.TpCOA2.Size = new System.Drawing.Size(766, 6);
            this.TpCOA2.Text = "Advance Payment Refund";
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(766, 0);
            this.Grd5.TabIndex = 48;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd5_EllipsisButtonClick);
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd5_AfterCommitEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel11.Controls.Add(this.TxtVoucherRequestDocNo3);
            this.panel11.Controls.Add(this.label29);
            this.panel11.Controls.Add(this.TxtVoucherDocNo3);
            this.panel11.Controls.Add(this.label30);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel11.Location = new System.Drawing.Point(0, -45);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(766, 51);
            this.panel11.TabIndex = 49;
            // 
            // TxtVoucherRequestDocNo3
            // 
            this.TxtVoucherRequestDocNo3.EnterMoveNextControl = true;
            this.TxtVoucherRequestDocNo3.Location = new System.Drawing.Point(124, 5);
            this.TxtVoucherRequestDocNo3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestDocNo3.Name = "TxtVoucherRequestDocNo3";
            this.TxtVoucherRequestDocNo3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestDocNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestDocNo3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestDocNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestDocNo3.Properties.MaxLength = 16;
            this.TxtVoucherRequestDocNo3.Properties.ReadOnly = true;
            this.TxtVoucherRequestDocNo3.Size = new System.Drawing.Size(212, 20);
            this.TxtVoucherRequestDocNo3.TabIndex = 48;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(57, 29);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(62, 14);
            this.label29.TabIndex = 49;
            this.label29.Text = "Voucher#";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherDocNo3
            // 
            this.TxtVoucherDocNo3.EnterMoveNextControl = true;
            this.TxtVoucherDocNo3.Location = new System.Drawing.Point(124, 26);
            this.TxtVoucherDocNo3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo3.Name = "TxtVoucherDocNo3";
            this.TxtVoucherDocNo3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo3.Properties.MaxLength = 16;
            this.TxtVoucherDocNo3.Properties.ReadOnly = true;
            this.TxtVoucherDocNo3.Size = new System.Drawing.Size(212, 20);
            this.TxtVoucherDocNo3.TabIndex = 50;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(8, 8);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(111, 14);
            this.label30.TabIndex = 50;
            this.label30.Text = "Voucher Request#";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.TxtCOAAmt2);
            this.panel12.Controls.Add(this.label32);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel12.Location = new System.Drawing.Point(536, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(230, 51);
            this.panel12.TabIndex = 51;
            // 
            // TxtCOAAmt2
            // 
            this.TxtCOAAmt2.EnterMoveNextControl = true;
            this.TxtCOAAmt2.Location = new System.Drawing.Point(63, 5);
            this.TxtCOAAmt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCOAAmt2.Name = "TxtCOAAmt2";
            this.TxtCOAAmt2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCOAAmt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCOAAmt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCOAAmt2.Properties.Appearance.Options.UseFont = true;
            this.TxtCOAAmt2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCOAAmt2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCOAAmt2.Properties.ReadOnly = true;
            this.TxtCOAAmt2.Size = new System.Drawing.Size(160, 20);
            this.TxtCOAAmt2.TabIndex = 53;
            this.TxtCOAAmt2.ToolTip = "Based On Purchase Invoice\'s Currency";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(6, 8);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(51, 14);
            this.label32.TabIndex = 52;
            this.label32.Text = "Amount";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpApproval
            // 
            this.TpApproval.Appearance.Header.Options.UseTextOptions = true;
            this.TpApproval.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpApproval.Controls.Add(this.Grd2);
            this.TpApproval.Name = "TpApproval";
            this.TpApproval.Size = new System.Drawing.Size(766, 6);
            this.TpApproval.Text = "Approval Summary";
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 6);
            this.Grd2.TabIndex = 44;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TpgProject
            // 
            this.TpgProject.Appearance.Header.Options.UseTextOptions = true;
            this.TpgProject.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpgProject.Controls.Add(this.label41);
            this.TpgProject.Controls.Add(this.LueProjectDocNo3);
            this.TpgProject.Controls.Add(this.label42);
            this.TpgProject.Controls.Add(this.LueProjectDocNo2);
            this.TpgProject.Controls.Add(this.label43);
            this.TpgProject.Controls.Add(this.LueProjectDocNo1);
            this.TpgProject.Name = "TpgProject";
            this.TpgProject.Size = new System.Drawing.Size(766, 6);
            this.TpgProject.Text = "Project";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(55, 61);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(75, 14);
            this.label41.TabIndex = 46;
            this.label41.Text = "Project Child";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProjectDocNo3
            // 
            this.LueProjectDocNo3.EnterMoveNextControl = true;
            this.LueProjectDocNo3.Location = new System.Drawing.Point(136, 58);
            this.LueProjectDocNo3.Margin = new System.Windows.Forms.Padding(5);
            this.LueProjectDocNo3.Name = "LueProjectDocNo3";
            this.LueProjectDocNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo3.Properties.Appearance.Options.UseFont = true;
            this.LueProjectDocNo3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProjectDocNo3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProjectDocNo3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProjectDocNo3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProjectDocNo3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProjectDocNo3.Properties.DropDownRows = 30;
            this.LueProjectDocNo3.Properties.NullText = "[Empty]";
            this.LueProjectDocNo3.Properties.PopupWidth = 250;
            this.LueProjectDocNo3.Size = new System.Drawing.Size(223, 20);
            this.LueProjectDocNo3.TabIndex = 47;
            this.LueProjectDocNo3.ToolTip = "F4 : Show/hide list";
            this.LueProjectDocNo3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProjectDocNo3.EditValueChanged += new System.EventHandler(this.LueProjectDocNo3_EditValueChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(20, 39);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(110, 14);
            this.label42.TabIndex = 44;
            this.label42.Text = "Project Costcenter";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProjectDocNo2
            // 
            this.LueProjectDocNo2.EnterMoveNextControl = true;
            this.LueProjectDocNo2.Location = new System.Drawing.Point(136, 36);
            this.LueProjectDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.LueProjectDocNo2.Name = "LueProjectDocNo2";
            this.LueProjectDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo2.Properties.Appearance.Options.UseFont = true;
            this.LueProjectDocNo2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProjectDocNo2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProjectDocNo2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProjectDocNo2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProjectDocNo2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProjectDocNo2.Properties.DropDownRows = 30;
            this.LueProjectDocNo2.Properties.NullText = "[Empty]";
            this.LueProjectDocNo2.Properties.PopupWidth = 250;
            this.LueProjectDocNo2.Size = new System.Drawing.Size(223, 20);
            this.LueProjectDocNo2.TabIndex = 45;
            this.LueProjectDocNo2.ToolTip = "F4 : Show/hide list";
            this.LueProjectDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProjectDocNo2.EditValueChanged += new System.EventHandler(this.LueProjectDocNo2_EditValueChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(84, 17);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(46, 14);
            this.label43.TabIndex = 42;
            this.label43.Text = "Project";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProjectDocNo1
            // 
            this.LueProjectDocNo1.EnterMoveNextControl = true;
            this.LueProjectDocNo1.Location = new System.Drawing.Point(136, 14);
            this.LueProjectDocNo1.Margin = new System.Windows.Forms.Padding(5);
            this.LueProjectDocNo1.Name = "LueProjectDocNo1";
            this.LueProjectDocNo1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo1.Properties.Appearance.Options.UseFont = true;
            this.LueProjectDocNo1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProjectDocNo1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProjectDocNo1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProjectDocNo1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProjectDocNo1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProjectDocNo1.Properties.DropDownRows = 30;
            this.LueProjectDocNo1.Properties.NullText = "[Empty]";
            this.LueProjectDocNo1.Properties.PopupWidth = 250;
            this.LueProjectDocNo1.Size = new System.Drawing.Size(223, 20);
            this.LueProjectDocNo1.TabIndex = 43;
            this.LueProjectDocNo1.ToolTip = "F4 : Show/hide list";
            this.LueProjectDocNo1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProjectDocNo1.EditValueChanged += new System.EventHandler(this.LueProjectDocNo1_EditValueChanged);
            // 
            // TpDeposit
            // 
            this.TpDeposit.Appearance.Header.Options.UseTextOptions = true;
            this.TpDeposit.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpDeposit.Controls.Add(this.Grd6);
            this.TpDeposit.Name = "TpDeposit";
            this.TpDeposit.Size = new System.Drawing.Size(766, 6);
            this.TpDeposit.Text = "Deposit";
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(0, 0);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(766, 6);
            this.Grd6.TabIndex = 47;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TpInvoiceItem
            // 
            this.TpInvoiceItem.Controls.Add(this.panel14);
            this.TpInvoiceItem.Controls.Add(this.panel13);
            this.TpInvoiceItem.Name = "TpInvoiceItem";
            this.TpInvoiceItem.Size = new System.Drawing.Size(766, 6);
            this.TpInvoiceItem.Text = "Item Invoice";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel14.Controls.Add(this.Grd7);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(0, 26);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(766, 0);
            this.panel14.TabIndex = 50;
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(0, 0);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(766, 0);
            this.Grd7.TabIndex = 49;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd7_AfterCommitEdit);
            this.Grd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd7_KeyDown);
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel13.Controls.Add(this.BtnRefreshData);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(766, 26);
            this.panel13.TabIndex = 47;
            // 
            // BtnRefreshData
            // 
            this.BtnRefreshData.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRefreshData.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefreshData.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefreshData.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnRefreshData.Appearance.Options.UseBackColor = true;
            this.BtnRefreshData.Appearance.Options.UseFont = true;
            this.BtnRefreshData.Appearance.Options.UseForeColor = true;
            this.BtnRefreshData.Appearance.Options.UseTextOptions = true;
            this.BtnRefreshData.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRefreshData.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRefreshData.Dock = System.Windows.Forms.DockStyle.Right;
            this.BtnRefreshData.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRefreshData.Location = new System.Drawing.Point(677, 0);
            this.BtnRefreshData.Name = "BtnRefreshData";
            this.BtnRefreshData.Size = new System.Drawing.Size(89, 26);
            this.BtnRefreshData.TabIndex = 48;
            this.BtnRefreshData.Text = "Refresh Data";
            this.BtnRefreshData.ToolTip = "Import Data";
            this.BtnRefreshData.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRefreshData.ToolTipTitle = "Run System";
            this.BtnRefreshData.Click += new System.EventHandler(this.BtnRefreshData_Click);
            // 
            // LblEntCode
            // 
            this.LblEntCode.AutoSize = true;
            this.LblEntCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEntCode.ForeColor = System.Drawing.Color.Black;
            this.LblEntCode.Location = new System.Drawing.Point(115, 121);
            this.LblEntCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEntCode.Name = "LblEntCode";
            this.LblEntCode.Size = new System.Drawing.Size(39, 14);
            this.LblEntCode.TabIndex = 34;
            this.LblEntCode.Text = "Entity";
            this.LblEntCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEntCode
            // 
            this.LueEntCode.EnterMoveNextControl = true;
            this.LueEntCode.Location = new System.Drawing.Point(158, 118);
            this.LueEntCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEntCode.Name = "LueEntCode";
            this.LueEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.Appearance.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEntCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEntCode.Properties.DropDownRows = 30;
            this.LueEntCode.Properties.MaxLength = 40;
            this.LueEntCode.Properties.NullText = "[Empty]";
            this.LueEntCode.Properties.PopupWidth = 300;
            this.LueEntCode.Size = new System.Drawing.Size(269, 20);
            this.LueEntCode.TabIndex = 35;
            this.LueEntCode.ToolTip = "F4 : Show/hide list";
            this.LueEntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit1
            // 
            this.textEdit1.EnterMoveNextControl = true;
            this.textEdit1.Location = new System.Drawing.Point(158, 160);
            this.textEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.MaxLength = 16;
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(269, 20);
            this.textEdit1.TabIndex = 40;
            // 
            // textEdit2
            // 
            this.textEdit2.EnterMoveNextControl = true;
            this.textEdit2.Location = new System.Drawing.Point(158, 13);
            this.textEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.MaxLength = 50;
            this.textEdit2.Size = new System.Drawing.Size(269, 20);
            this.textEdit2.TabIndex = 25;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(92, 100);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(62, 14);
            this.label33.TabIndex = 32;
            this.label33.Text = "Account#";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit3
            // 
            this.textEdit3.EnterMoveNextControl = true;
            this.textEdit3.Location = new System.Drawing.Point(158, 76);
            this.textEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.MaxLength = 80;
            this.textEdit3.Size = new System.Drawing.Size(269, 20);
            this.textEdit3.TabIndex = 31;
            // 
            // textEdit4
            // 
            this.textEdit4.EnterMoveNextControl = true;
            this.textEdit4.Location = new System.Drawing.Point(158, 181);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit4.Properties.Appearance.Options.UseFont = true;
            this.textEdit4.Properties.MaxLength = 16;
            this.textEdit4.Properties.ReadOnly = true;
            this.textEdit4.Size = new System.Drawing.Size(269, 20);
            this.textEdit4.TabIndex = 42;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(42, 165);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(111, 14);
            this.label34.TabIndex = 39;
            this.label34.Text = "Voucher Request#";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // checkEdit1
            // 
            this.checkEdit1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.checkEdit1.Location = new System.Drawing.Point(407, 138);
            this.checkEdit1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.checkEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.checkEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit1.Properties.Appearance.Options.UseFont = true;
            this.checkEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.checkEdit1.Properties.Caption = "";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.checkEdit1.Size = new System.Drawing.Size(20, 22);
            this.checkEdit1.TabIndex = 38;
            this.checkEdit1.ToolTip = "Summary for Voucher Request Description";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(86, 37);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(68, 14);
            this.label35.TabIndex = 26;
            this.label35.Text = "Bank Name";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(75, 58);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(79, 14);
            this.label36.TabIndex = 28;
            this.label36.Text = "Branch Name";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit5
            // 
            this.textEdit5.EnterMoveNextControl = true;
            this.textEdit5.Location = new System.Drawing.Point(158, 55);
            this.textEdit5.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit5.Properties.Appearance.Options.UseFont = true;
            this.textEdit5.Properties.MaxLength = 80;
            this.textEdit5.Size = new System.Drawing.Size(269, 20);
            this.textEdit5.TabIndex = 29;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(91, 185);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(62, 14);
            this.label37.TabIndex = 41;
            this.label37.Text = "Voucher#";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(13, 143);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(140, 14);
            this.label38.TabIndex = 36;
            this.label38.Text = "VR Summary Description";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoExEdit1
            // 
            this.memoExEdit1.EnterMoveNextControl = true;
            this.memoExEdit1.Location = new System.Drawing.Point(158, 139);
            this.memoExEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit1.Name = "memoExEdit1";
            this.memoExEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit1.Properties.MaxLength = 400;
            this.memoExEdit1.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.memoExEdit1.Properties.ShowIcon = false;
            this.memoExEdit1.Size = new System.Drawing.Size(246, 20);
            this.memoExEdit1.TabIndex = 37;
            this.memoExEdit1.ToolTip = "F4 : Show/hide text";
            this.memoExEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit1.ToolTipTitle = "Run System";
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.EnterMoveNextControl = true;
            this.lookUpEdit1.Location = new System.Drawing.Point(158, 34);
            this.lookUpEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.DropDownRows = 30;
            this.lookUpEdit1.Properties.NullText = "[Empty]";
            this.lookUpEdit1.Properties.PopupWidth = 300;
            this.lookUpEdit1.Size = new System.Drawing.Size(269, 20);
            this.lookUpEdit1.TabIndex = 27;
            this.lookUpEdit1.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(66, 79);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(88, 14);
            this.label39.TabIndex = 30;
            this.label39.Text = "Account Name";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(106, 16);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(48, 14);
            this.label40.TabIndex = 24;
            this.label40.Text = "Paid To";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit6
            // 
            this.textEdit6.EnterMoveNextControl = true;
            this.textEdit6.Location = new System.Drawing.Point(158, 97);
            this.textEdit6.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.MaxLength = 80;
            this.textEdit6.Size = new System.Drawing.Size(269, 20);
            this.textEdit6.TabIndex = 33;
            // 
            // FrmIncomingPayment3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 520);
            this.Name = "FrmIncomingPayment3";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaymentUser.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaidToBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankAcName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeVoucherRequestSummaryDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkMeeVoucherRequestSummaryInd.Properties)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDepositAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRateAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcIncomingPayment)).EndInit();
            this.TcIncomingPayment.ResumeLayout(false);
            this.TpInvoice.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.TpGiro.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroAmt.Properties)).EndInit();
            this.TpCOA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo2.Properties)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCOAAmt.Properties)).EndInit();
            this.TpCOA2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo3.Properties)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCOAAmt2.Properties)).EndInit();
            this.TpApproval.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TpgProject.ResumeLayout(false);
            this.TpgProject.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectDocNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectDocNo1.Properties)).EndInit();
            this.TpDeposit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.TpInvoiceItem.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.panel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private DevExpress.XtraEditors.LookUpEdit LuePaidToBankCode;
        internal DevExpress.XtraEditors.TextEdit TxtPaidToBankAcNo;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtPaidToBankBranch;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtPaidToBankAcName;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtGiroNo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private DevExpress.XtraEditors.LookUpEdit LueAcType;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.LookUpEdit LuePaymentType;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.LookUpEdit LueBankAcCode;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Panel panel6;
        internal DevExpress.XtraEditors.TextEdit TxtAmt2;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtRateAmt;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestDocNo;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.MemoExEdit MeeVoucherRequestSummaryDesc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkMeeVoucherRequestSummaryInd;
        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        internal DevExpress.XtraEditors.DateEdit DteDueDt;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtPaymentUser;
        private System.Windows.Forms.Label label24;
        private DevExpress.XtraTab.XtraTabControl TcIncomingPayment;
        private DevExpress.XtraTab.XtraTabPage TpInvoice;
        private DevExpress.XtraTab.XtraTabPage TpApproval;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraTab.XtraTabPage TpCOA;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        internal DevExpress.XtraEditors.TextEdit TxtCOAAmt;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestDocNo2;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo2;
        private System.Windows.Forms.Label label26;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraTab.XtraTabPage TpGiro;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        internal DevExpress.XtraEditors.TextEdit TxtGiroAmt;
        private System.Windows.Forms.Label label31;
        private DevExpress.XtraTab.XtraTabPage TpCOA2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        private System.Windows.Forms.Panel panel11;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestDocNo3;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo3;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel12;
        internal DevExpress.XtraEditors.TextEdit TxtCOAAmt2;
        private System.Windows.Forms.Label label32;
        private DevExpress.XtraTab.XtraTabPage TpgProject;
        private System.Windows.Forms.Label LblEntCode;
        internal DevExpress.XtraEditors.LookUpEdit LueEntCode;
        internal DevExpress.XtraEditors.TextEdit textEdit1;
        internal DevExpress.XtraEditors.TextEdit textEdit2;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit textEdit3;
        internal DevExpress.XtraEditors.TextEdit textEdit4;
        private System.Windows.Forms.Label label34;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        internal DevExpress.XtraEditors.TextEdit textEdit5;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit textEdit6;
        private System.Windows.Forms.Label label41;
        private DevExpress.XtraEditors.LookUpEdit LueProjectDocNo3;
        private System.Windows.Forms.Label label42;
        private DevExpress.XtraEditors.LookUpEdit LueProjectDocNo2;
        private System.Windows.Forms.Label label43;
        private DevExpress.XtraEditors.LookUpEdit LueProjectDocNo1;
        private DevExpress.XtraTab.XtraTabPage TpDeposit;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        internal DevExpress.XtraEditors.TextEdit TxtDepositAmt;
        private System.Windows.Forms.Label LblDepositAmt;
        private System.Windows.Forms.Label label44;
        internal DevExpress.XtraEditors.TextEdit TxtTotalAmt;
        private System.Windows.Forms.Label label45;
        public DevExpress.XtraEditors.SimpleButton BtnCtCode;
        internal DevExpress.XtraEditors.LookUpEdit LueCtCtCode;
        internal DevExpress.XtraEditors.LookUpEdit LueCtCode;
        private DevExpress.XtraTab.XtraTabPage TpInvoiceItem;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        protected System.Windows.Forms.Panel panel14;
        protected System.Windows.Forms.Panel panel13;
        public DevExpress.XtraEditors.SimpleButton BtnRefreshData;
    }
}