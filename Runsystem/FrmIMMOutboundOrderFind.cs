﻿#region Update
/*
    24/07/2019 [TKG] New application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmIMMOutboundOrderFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmIMMOutboundOrder mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmIMMOutboundOrderFind(FrmIMMOutboundOrder FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From (");
            SQL.AppendLine("    Select Concat(A.DocNo, A.DocSeqNo) As DOCtReq, A.DocNo, A.DocSeqNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("    A.OrderDocNo, A.ReceiptDocNo, B.PFName, A.Buyer, A.ProdCode, C.ProdDesc, C.Color, A.Qty, A.CurCode, A.Price, ");
            SQL.AppendLine("    A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("    From TblIMMOutboundOrder A ");
            SQL.AppendLine("    Left Join TblIMMPlatform B On A.PFCode=B.PFCode ");
            SQL.AppendLine("    Left Join TblIMMProduct C On A.ProdCode=C.ProdCode ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("    And Concat(A.DocNo, A.DocSeqNo) In ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select Concat(T1.DocNo, T1.DocSeqNo) From TblIMMDOCtReq T1, TblIMMDOCtReqDtl T2 ");
            //SQL.AppendLine("        Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("        And T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("        And T1.DocSeqNo=T2.DocSeqNo ");
            //SQL.AppendLine("        And Exists( ");
            //SQL.AppendLine("            Select 1 From TblGroupWarehouse ");
            //SQL.AppendLine("            Where WhsCode=T2.WhsCode ");
            //SQL.AppendLine("            And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            //SQL.AppendLine("        ) ");
            //SQL.AppendLine("    ) ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "DocNo", 
                        "DocSeqNo", 
                        "Date",
                        "Cancel",

                        //6-10
                        "Order#",
                        "Receipt#",
                        "Platform",
                        "Buyer",
                        "Product's Code",

                        //11-15
                        "Product's Description",
                        "Color",
                        "Quantity",
                        "Currency",
                        "Price",

                        //16-20
                        "Amount",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 

                        //21-22
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 0, 0, 80, 80, 
                        
                        //6-10
                        150, 150, 200, 200, 100, 
                        
                        //11-15
                        200, 100, 100, 80, 130,  

                        //16-20
                        130, 100, 100, 100, 100,

                        //21-22
                        100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15, 16 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4, 18, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22 });
            Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DOCtReq", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProdCode.Text, new string[] { "ProdCode", "ProdDesc" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By CreateDt Desc, DocSeqNo;",
                        new string[]
                        {
                            //0
                            "DOCtReq", 

                            //1-5
                            "DocNo", "DocSeqNo", "DocDt", "CancelInd", "OrderDocNo", 

                            //6-10
                            "ReceiptDocNo", "PFName", "Buyer", "ProdCode", "ProdDesc", 

                            //11-15
                            "Color", "Qty", "CurCode", "Price", "CreateBy", 
                            
                            //16-18
                            "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Grd.Cells[Row, 16].Value = Sm.GetGrdDec(Grd, Row, 13) * Sm.GetGrdDec(Grd, Row, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 18);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtProdCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Product");
        }

        #endregion

        #endregion
    }
}
