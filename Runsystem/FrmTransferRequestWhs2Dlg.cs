﻿#region Update
// 18/05/2017 [TKG] stock yg bisa diambil adalah aktual stok dikurangi tr yg belum di-do dikurangi tr yg sudah di-do tapi belum di-received
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTransferRequestWhs2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmTransferRequestWhs2 mFrmParent;
        string mWhsCode = string.Empty;

        #endregion

        #region Constructor

        public FrmTransferRequestWhs2Dlg(FrmTransferRequestWhs2 FrmParent, string WhsCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL(string Filter1, string Filter2, string Filter3)
        {
            var subSQL = new StringBuilder();
            var SQL = new StringBuilder();

            subSQL.AppendLine("Select A.ItCode, A.PropCode, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            subSQL.AppendLine("A.Qty-IfNull(B.Qty, 0)-IfNull(C.Qty, 0) As Qty, ");
            subSQL.AppendLine("A.Qty2-IfNull(B.Qty2, 0)-IfNull(C.Qty2, 0) As Qty2, ");
            subSQL.AppendLine("A.Qty3-IfNull(B.Qty3, 0)-IfNull(C.Qty3, 0) As Qty3 ");
            subSQL.AppendLine("From TblStockSummary A ");
            
            subSQL.AppendLine("Left Join ( ");
            subSQL.AppendLine("     Select T2.Source, T2.Lot, T2.Bin, ");
            subSQL.AppendLine("     Sum(T2.Qty) Qty, Sum(T2.Qty2) Qty2, Sum(T2.Qty3) Qty3 ");
            subSQL.AppendLine("     From TblTransferRequestWhs2Hdr T1 ");
            subSQL.AppendLine("     Inner Join TblTransferRequestWhs2Dtl T2 On T1.DocNo=T2.DocNo ");
            if (Filter1.Length > 0) 
                subSQL.AppendLine("         And Exists(Select ItCode From TblItem Where ItCode=T2.ItCode " + Filter1 + ") ");
            if (Filter2.Length > 0) 
                subSQL.AppendLine(Filter2.Replace("A.", "T2."));
            subSQL.AppendLine("     Where T1.WhsCode=@WhsCode ");
            subSQL.AppendLine("     And T1.CancelInd='N' ");
            subSQL.AppendLine("     And T1.DOWhs3DocNo Is Null ");
            subSQL.AppendLine("     Group By T2.Source, T2.Lot, T2.Bin ");
            subSQL.AppendLine(") B On A.Source=B.Source And A.Lot=B.Lot And A.Bin=B.Bin ");

            subSQL.AppendLine("Left Join ( ");
            subSQL.AppendLine("     Select T2.Source, T2.Lot, T2.Bin, ");
            subSQL.AppendLine("     Sum(T2.Qty) Qty, Sum(T2.Qty2) Qty2, Sum(T2.Qty3) Qty3 ");
            subSQL.AppendLine("     From TblTransferRequestWhs2Hdr T1 ");
            subSQL.AppendLine("     Inner Join TblTransferRequestWhs2Dtl T2 On T1.DocNo=T2.DocNo ");
            if (Filter1.Length > 0)
                subSQL.AppendLine("         And Exists(Select ItCode From TblItem Where ItCode=T2.ItCode " + Filter1 + ") ");
            if (Filter2.Length > 0)
                subSQL.AppendLine(Filter2.Replace("A.", "T2."));
            subSQL.AppendLine("     Where T1.WhsCode=@WhsCode ");
            subSQL.AppendLine("     And T1.CancelInd='N' ");
            subSQL.AppendLine("     And T1.DOWhs3DocNo Is Not Null ");
            subSQL.AppendLine("     And Exists(");
            subSQL.AppendLine("         Select Tbl1.DocNo ");
            subSQL.AppendLine("         From TblDOWhsHdr Tbl1 ");
            subSQL.AppendLine("         Inner Join TblDOWhsDtl Tbl2 On Tbl1.DocNo=Tbl2.DocNo And Tbl2.CancelInd='N' And Tbl2.ProcessInd='O' ");
            subSQL.AppendLine("         Where Tbl1.CancelInd='N' ");
            subSQL.AppendLine("         And Tbl1.Status In ('O', 'A') ");
            subSQL.AppendLine("         And Tbl1.TransferRequestWhs2DocNo Is Not Null ");
            subSQL.AppendLine("         And Tbl2.TransferRequestWhs2DNo Is Not Null ");
            subSQL.AppendLine("         And Tbl1.TransferRequestWhs2DocNo=T2.DocNo ");
            subSQL.AppendLine("         And Tbl2.TransferRequestWhs2DNo=T2.DNo ");
            subSQL.AppendLine("     ) ");

            subSQL.AppendLine("     Group By T2.Source, T2.Lot, T2.Bin ");
            subSQL.AppendLine(") C On A.Source=C.Source And A.Lot=C.Lot And A.Bin=C.Bin ");

            subSQL.AppendLine("Where A.WhsCode=@WhsCode ");
            subSQL.AppendLine("And A.Qty>0 ");
            if (Filter1.Length > 0) subSQL.AppendLine("And Exists(Select ItCode From TblItem Where ItCode=A.ItCode " + Filter1 + ") ");
            if (Filter2.Length>0) subSQL.AppendLine(Filter2);
            if (Filter3.Length>0) subSQL.AppendLine(Filter3);

            SQL.AppendLine("Select X1.ItCode, X2.ItName, X2.ForeignName, ");
            SQL.AppendLine("X1.PropCode, X3.PropName, X1.BatchNo, X1.Source, X1.Lot, X1.Bin, ");
            SQL.AppendLine("X1.Qty, X2.InventoryUomCode, X1.Qty2, X2.InventoryUomCode2, X1.Qty3, X2.InventoryUomCode3 ");
            SQL.AppendLine("From (" +subSQL.ToString()+") X1 ");
            SQL.AppendLine("Inner Join TblItem X2 On X1.ItCode=X2.ItCode ");
            SQL.AppendLine("Left Join TblProperty X3 On X1.PropCode=X3.PropCode ");
            SQL.AppendLine("Where X1.Qty>0 ");
            SQL.AppendLine("Order By X2.ItName;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "", 
                        "Item's Code", 
                        "",
                        "Item's Name", 
                        "Foreign Name", 
                        
                        //6-10
                        "Property Code",
                        "Property",
                        "Batch#",
                        "Source",
                        "Lot",
                        
                        //11-15
                        "Bin", 
                        "Quantity",
                        "UoM",
                        "Quantity 2",
                        "Uom 2",

                        //16-17
                        "Quantity 3",
                        "Uom 3"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 20, 200, 100, 
                        
                        //6-10
                        0, 80, 150, 170, 60, 
                        
                        //11-15
                        60, 80, 60, 80, 60, 
                        
                        //16-17
                        80, 60
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 16 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 9, 14, 15, 16, 17 }, false);
            if (!mFrmParent.mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 5 }, false);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 9, 14, 15, 16, 17 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter1 = " ", Filter2 = " ", Filter3 = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);

                Sm.FilterStr(ref Filter1, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItName" });
                Sm.FilterStr(ref Filter2, ref cm, TxtBatchNo.Text, new string[] { "A.BatchNo" });
                Sm.FilterStr(ref Filter2, ref cm, TxtLot.Text, new string[] { "A.Lot" });
                Sm.FilterStr(ref Filter2, ref cm, TxtBin.Text, new string[] { "A.Bin" });

                Sm.GenerateSQLFilterForInventory(ref cm, ref Filter3, "A", ref mFrmParent.Grd1, 9, 10, 11);
                Filter3 = (Filter3.Length > 0) ? " And (" + Filter3 + ") " : string.Empty;

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter1, Filter2, Filter3),
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItName", "ForeignName", "PropCode", "PropName", "BatchNo", 
                            
                            //6-10
                            "Source", "Lot", "Bin", "Qty", "InventoryUomCode", 
                            
                            //11-14
                            "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Grd1.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 17);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string
                Source = Sm.GetGrdStr(Grd1, Row, 9),
                Lot = Sm.GetGrdStr(Grd1, Row, 10),
                Bin = Sm.GetGrdStr(Grd1, Row, 11);

            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 9), Source) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 10), Lot) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 11), Bin)  
                    )
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion
        
        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        #endregion

        #endregion
    }
}
