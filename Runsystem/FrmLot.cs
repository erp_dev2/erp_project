﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLot : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmLotFind FrmFind;

        #endregion

        #region Constructor

        public FrmLot(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetGrd();
            SetFormControl(mState.View);
            base.FrmLoad(sender, e);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Bin",
                        "Remark",
                    },
                     new int[] 
                    {
                       
                        //0
                        20, 

                        //1-5
                        150, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2});
            Sm.GrdColInvisible(Grd1, new int[] {  }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLot, MeeRemark, 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2});
                    ChkActInd.Properties.ReadOnly = true;
                    TxtLot.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLot, MeeRemark, 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    ChkActInd.Checked = true;
                    TxtLot.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2 });
                    ChkActInd.Properties.ReadOnly = false;
                    ChkActInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtLot, MeeRemark
            });
            ChkActInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                    Sm.FormShowDialog(new FrmLotDlg(this));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmLotDlg(this));
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtLot.Text.Length != 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmLotFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtLot, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtLot.Properties.ReadOnly == false)
                    InsertData();
                else
                    UpdateData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

    
        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string Lot = TxtLot.Text;

            if (IsBinAlreadyExist()) return;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveLotHdr(Lot));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveLotDtl(Lot, Row));

            Sm.ExecCommands(cml);

            ShowData(Lot);
        }

        private void UpdateData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string Lot = TxtLot.Text;

            if (IsBinAlreadyExist()) return;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveLotHdr(Lot));
            
            Sm.ExecCommands(cml);

            ShowData(Lot);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtLot, "Lot", false);
        }

        private MySqlCommand SaveLotHdr(string Lot)
        {
            
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLotHdr(Lot, ActInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@Lot, @ActInd, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Lot", TxtLot.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveLotDtl(string Lot, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLotDtl(Lot, Bin, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@Lot, @Bin, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Lot", Lot);
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowLotHdr(DocNo);
                ShowLotDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowLotHdr(string Lot)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Lot", Lot);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select Lot, ActInd, Remark From TblLotHdr Where Lot=@Lot",
                    new string[] 
                    { 
                        "Lot", "ActInd", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtLot.EditValue = Sm.DrStr(dr, c[0]);
                        ChkActInd.Checked = Sm.DrStr(dr, c[1])== "Y"; 
                        MeeRemark.EditValue = Sm.DrStr(dr, c[2]);
                    }, true
                );
        }

        private void ShowLotDtl(string Lot)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Lot", Lot);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Bin, B.Remark From TblLotDtl A Left Join TblBin B On A.Bin = B.Bin ");
            SQL.AppendLine("Where A.Lot=@Lot ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "Bin",

                    //1-5
                    "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #endregion

        #region Additional Event
        internal string GetSelectedBin()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0 )
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 1)+ 
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }


        private bool IsBinAlreadyExist()
        {
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                {
                    string bin = Sm.GetValue("Select A.Bin From TblLotDtl A Inner join TblLotHdr B On A.Lot=B.Lot Where A.Bin = '"+Sm.GetGrdStr(Grd1, Row, 1)+"' And B.ActInd = 'Y' ");
                    if (bin.Length > 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Bin : '"+bin+"' have been choose in another lot");
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLot);
        }

        #endregion

        #endregion
    }
}
