﻿#region update
/*
13/11/2019 [VIN/IMS] new application 
 */
#endregion 

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmFingerMachine : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmFingerMachineFind FrmFind;
      
        #endregion

        #region Constructor

        public FrmFingerMachine(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtMachineIP, TxtMachineKey, TxtMachinePort, TxtMachineName
                    }, true);
                    TxtMachineIP.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtMachineIP, TxtMachineKey, TxtMachinePort, TxtMachineName
                    }, false);
                    TxtMachineIP.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtMachineIP, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtMachineKey, TxtMachinePort, TxtMachineName
                    }, false);
                    TxtMachineKey.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtMachineIP, TxtMachineKey, TxtMachinePort, TxtMachineName
            });
        }

        #endregion

        #region Button Method
        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmFingerMachineFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtMachineIP, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {

        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblFingerMachine(MachineIP, MachineKey, MachinePort, MachineName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@MachineIP, @MachineKey, @MachinePort, @MachineName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update MachineKey=@MachineKey, MachinePort=@MachinePort, MachineName=@MachineName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@MachineIP", TxtMachineIP.Text);
                Sm.CmParam<String>(ref cm, "@MachineKey", TxtMachineKey.Text);
                Sm.CmParam<String>(ref cm, "@MachinePort", TxtMachinePort.Text);
                Sm.CmParam<String>(ref cm, "@MachineName", TxtMachineName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtMachineIP.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string MachineIP)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@MachineIP", MachineIP);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblFingerMachine Where MachineIP=@MachineIP",
                        new string[] 
                        {
                            //0-4
                            "MachineIP", "MachineKey", "MachinePort", "MachineName"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtMachineIP.EditValue = Sm.DrStr(dr, c[0]);
                            TxtMachineKey.EditValue = Sm.DrStr(dr, c[1]);
                            TxtMachinePort.EditValue = Sm.DrStr(dr, c[2]);
                            TxtMachineName.EditValue = Sm.DrStr(dr, c[3]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtMachineIP, "Machine IP", false) ||
                Sm.IsTxtEmpty(TxtMachineKey, "Machine Key", false) ||
                Sm.IsTxtEmpty(TxtMachinePort, "Machine Port", false) ||
                Sm.IsTxtEmpty(TxtMachineName, "Machine Name", false) ||

                IsMachineIPExisted();
        }

        private bool IsMachineIPExisted()
        {
            if (!TxtMachineIP.Properties.ReadOnly && Sm.IsDataExist("Select MachineIP From TblFingerMachine Where MachineIP='" + TxtMachineIP.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Machine IP ( " + TxtMachineIP.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region event
        private void TxtMachineIP_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtMachineIP);
        }
        private void TxtMachineKey_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtMachineKey);
        }
        private void TxtMachinePort_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtMachinePort);
        }
        private void TxtMachineName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtMachineName);
        }

        #endregion 
       
    }
}
