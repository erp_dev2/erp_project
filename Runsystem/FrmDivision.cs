﻿#region Update
/*
    18/09/2019 [TKG/IMS] tambah daftar department
    28/04/2020 [TKG/SIER] tambah short code
    23/03/2023 [BRI/SIER] tambah maxlenght
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmDivision : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmDivisionFind FrmFind;
        private bool mIsDivisionDepartmentEnabled = false;

        #endregion

        #region Constructor

        public FrmDivision(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                Tc1.SelectedTabPage = Tp1;
                Tp2.PageVisible = mIsDivisionDepartmentEnabled;
                SetGrd();
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsDivisionDepartmentEnabled = Sm.GetParameterBoo("IsDivisionDepartmentEnabled");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] { "", "Code", "Department" },
                new int[] { 20, 0, 300 }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2 });
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDivisionCode, TxtDivisionName, ChkActInd, TxtShortCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtDivisionCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtDivisionCode, TxtDivisionName, TxtShortCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtDivisionCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDivisionName, ChkActInd, TxtShortCode }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtDivisionName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtDivisionCode, TxtDivisionName, TxtShortCode });
            ChkActInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDivisionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDivisionCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDivisionCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblDivision Where DivisionCode=@DivisionCode" };
                Sm.CmParam<String>(ref cm, "@DivisionCode", TxtDivisionCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                var DeptCode = string.Empty;

                SQL.AppendLine("Insert Into TblDivision(DivisionCode, DivisionName, ActInd, ShortCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DivisionCode, @DivisionName, @ActInd, @ShortCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ActInd=@ActInd, DivisionName=@DivisionName, ShortCode=@ShortCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                SQL.AppendLine("Delete From TblDivisionDepartment Where DivisionCode=@DivisionCode; ");

                if (Grd1.Rows.Count > 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        DeptCode = Sm.GetGrdStr(Grd1, r, 1);
                        if (DeptCode.Length > 0)
                        {
                            SQL.AppendLine("Insert Into TblDivisionDepartment(DivisionCode, DeptCode, CreateBy, CreateDt) ");
                            SQL.AppendLine("Values (@DivisionCode, @DeptCode0" + r.ToString() + ", @UserCode, CurrentDateTime()); ");

                            Sm.CmParam<String>(ref cm, "@DeptCode0" + r.ToString(), DeptCode);
                        }
                    }
                }

                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DivisionCode", TxtDivisionCode.Text);
                Sm.CmParam<String>(ref cm, "@DivisionName", TxtDivisionName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@ShortCode", TxtShortCode.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtDivisionCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        internal void ShowData(string DivisionCode)
        {
            try
            {
                ClearData();
                ShowDivision(DivisionCode);
                ShowDivisionDepartment(DivisionCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDivision(string DivisionCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DivisionCode", DivisionCode);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DivisionCode, DivisionName, ActInd, ShortCode From TblDivision Where DivisionCode=@DivisionCode;",
                    new string[]{"DivisionCode", "DivisionName", "ActInd", "ShortCode" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDivisionCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtDivisionName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        TxtShortCode.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        private void ShowDivisionDepartment(string DivisionCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DivisionCode", DivisionCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.DeptCode, B.DeptName From TblDivisionDepartment A, TblDepartment B " +
                    "Where A.DeptCode=B.DeptCode And A.DivisionCode=@DivisionCode;",
                    new string[] { "DeptCode", "DeptName" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDivisionCode, "Division Code", false) ||
                Sm.IsTxtEmpty(TxtDivisionName, "Division Name", false) ||
                IsDivisionExisted() ||
                IsDataInactiveAlready();

        }

        private bool IsDivisionExisted()
        {
            if (!TxtDivisionCode.Properties.ReadOnly &&
                Sm.IsDataExist("Select 1 From TblDivision Where DivisionCode=@Param Limit 1;", TxtDivisionCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Division code ( " + TxtDivisionCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsDataInactiveAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblDivision Where ActInd='N' And DivisionCode=@Param;", 
                TxtDivisionCode.Text, 
                "This division already not actived.");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDivisionCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtDivisionCode);
        }

        private void TxtDivisionName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtDivisionName);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmDivisionDlg(this));
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmDivisionDlg(this));
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void TxtShortCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtShortCode);
        }

        #endregion

        #endregion
    }
}
