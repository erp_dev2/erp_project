﻿#region Update
/*
    19/03/2020 [WED/SIER] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGenerateDOCtMaster : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mGenerateDOCtWhsCode = string.Empty;
        private string 
            mDocType = string.Empty;
        internal FrmGenerateDOCtMasterFind FrmFind;

        #endregion

        #region Constructor

        public FrmGenerateDOCtMaster(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            GetParameter();
            SetLueWhsCode(ref LueWhsCode, mGenerateDOCtWhsCode);
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueWhsCode, TxtKVAQuota, TxtLWBP, TxtLWBPHour, TxtWBP, TxtWBPHour, TxtKAP, TxtPJUPercentage
                    }, true);
                    LueWhsCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         LueWhsCode, TxtKVAQuota, TxtLWBP, TxtLWBPHour, TxtWBP, TxtWBPHour, TxtKAP, TxtPJUPercentage
                    }, false);
                    LueWhsCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(LueWhsCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtKVAQuota, TxtLWBP, TxtLWBPHour, TxtWBP, TxtWBPHour, TxtKAP, TxtPJUPercentage
                    }, false);
                    TxtKVAQuota.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mDocType = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 LueWhsCode, TxtLWBPHour, TxtWBPHour
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtKVAQuota, TxtLWBP, TxtWBP, TxtKAP, TxtPJUPercentage }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmGenerateDOCtMasterFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueWhsCode, "")) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                if (mDocType.Length == 0) mDocType = "1";

                SQL.AppendLine("Insert Into TblGenerateDOCtMaster(DocType, WhsCode, KVAQuota, LWBP, LWBPHour, WBP, WBPHour, KAP, PJUPercentage, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocType, @WhsCode, @KVAQuota, @LWBP, @LWBPHour, @WBP, @WBPHour, @KAP, @PJUPercentage, @CreateBy, CurrentDateTime()) ");
                SQL.AppendLine("    On Duplicate Key Update ");
                SQL.AppendLine("    KVAQuota = @KVAQuota, LWBP = @LWBP, LWBPHour = @LWBPHour, WBP = @WBP, WBPHour = @WBPHour, KAP = @KAP, PJUPercentage = @PJUPercentage, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocType", mDocType);
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<Decimal>(ref cm, "@KVAQuota", Decimal.Parse(TxtKVAQuota.Text));
                Sm.CmParam<Decimal>(ref cm, "@LWBP", Decimal.Parse(TxtLWBP.Text));
                Sm.CmParam<String>(ref cm, "@LWBPHour", TxtLWBPHour.Text);
                Sm.CmParam<Decimal>(ref cm, "@WBP", Decimal.Parse(TxtWBP.Text));
                Sm.CmParam<String>(ref cm, "@WBPHour", TxtWBPHour.Text);
                Sm.CmParam<Decimal>(ref cm, "@KAP", Decimal.Parse(TxtKAP.Text));
                Sm.CmParam<Decimal>(ref cm, "@PJUPercentage", Decimal.Parse(TxtPJUPercentage.Text));
                Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(mDocType, Sm.GetLue(LueWhsCode));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string DocType, string WhsCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select Doctype, WhsCode, KVAQuota, LWBP, LWBPHour, WBP, WBPHour, ");
                SQL.AppendLine("KAP, PJUPercentage ");
                SQL.AppendLine("From TblGenerateDOCtMaster ");
                SQL.AppendLine("Where DocType = @DocType And WhsCode = @WhsCode; ");

                Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
                Sm.CmParam<String>(ref cm, "@DocType", DocType);
                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        "Doctype", 
                        "WhsCode", "KVAQuota", "LWBP", "LWBPHour", "WBP", 
                        "WBPHour", "KAP", "PJUPercentage"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        mDocType = Sm.DrStr(dr, c[0]);
                        Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[1]));
                        TxtKVAQuota.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                        TxtLWBP.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 0);
                        TxtLWBPHour.EditValue = Sm.DrStr(dr, c[4]);
                        TxtWBP.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                        TxtWBPHour.EditValue = Sm.DrStr(dr, c[6]);
                        TxtKAP.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        TxtPJUPercentage.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueWhsCode, "Warehouse");
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mGenerateDOCtWhsCode = Sm.GetParameter("GenerateDOCtWhsCode");
        }

        internal void SetLueWhsCode(ref DXE.LookUpEdit Lue, string WhsCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WhsCode Col1, WhsName Col2 ");
            SQL.AppendLine("From TblWarehouse ");
            if (WhsCode.Length > 0)
                SQL.AppendLine("Where Find_In_Set(WhsCode, @Param) ");
            else
                SQL.AppendLine("Where 1=0 ");
            SQL.AppendLine("Order By WhsName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Param", WhsCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtKVAQuota_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtKVAQuota, 0);
        }

        private void TxtKAP_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtKAP, 0);
        }

        private void TxtPJUPercentage_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtPJUPercentage, 0);
        }

        #endregion

        #endregion
    }
}
