﻿#region Update
/*
    08/05/2017 [TKG] Journal untuk VAT
    21/05/2017 [TKG] Journal untuk Voucher Tax
    11/07/2017 [TKG] Journal untuk RHA dan AP downpayment (Giro)
    17/07/2017 [TKG] bug fixing Journal incoming payment/outgoing payment (partial)
    30/07/2017 [TKG] ubah journal voucher request vat menggunakan account# dari master tax
    21/08/2017 [TKG] Journal Incoming Payment Additional Amount : nomor rekening coa piutang penjualan diubah menjadi nomor rekening coa bank account.
    23/08/2017 [TKG] Journal AR DP Additional Amount : Piutang penjualan diubah menjadi COA bank account.
    03/09/2017 [TKG] validasi voucher vr tax saat cancel apakah sudah diproses menjadi vr vat.
    01/11/2017 [WED] insert ke tabel deposit summary baru saat ardp, apdp, dan return apdp di voucher kan
    05/11/2017 [WED] bug fixing saat save ke summary baru tapi curcode = IDR
    03/12/2017 [TKG] Vendor/customer deposit summary 2 hanya diproses untuk non IDR
    29/01/2018 [TKG] bug journal returning ap dp
    06/03/2018 [WED] tambah journal return ardp
    14/04/2018 [TKG] menggunakan format khusus voucher atau format standard dokumen run system berdasarkan parameter VoucherCodeFormatType
    20/07/2018 [TKG] validasi apabila currency bank dan currency tidak sama akan menampilkan warning.
    07/09/2018 [TKG] Journal otomatis untuk Bonus.
    25/09/2018 [TKG] ubah journal ap dp
    15/10/2018 [TKG] tambah proses untuk PI raw material untuk double payment.
    19/10/2018 [TKG] tambah informasi tipe pembayaran, button untuk menampilkan PI raw material
    26/11/2020 [TKG/SIER] ubah journal return arpdp
    05/02/2023 [PHT-ALL/TKG] tambah exchange rate di table deposit movement 
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucher2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        internal FrmVoucher2Find FrmFind;
        private string
            mVoucherCodeFormatType = "1",
            mItCtRMPActual = string.Empty,
            mItCtRMPActual2 = string.Empty,
            mCustomerDatabase = string.Empty,
            mMInd = "N",
            mMainCurCode = string.Empty;
        private bool 
            mTransferVoucherRMToCustDB = false,
            mIsAutoJournalActived = false;
        internal bool mIsUseMInd = false;

        #endregion

        #region Constructor

        public FrmVoucher2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Voucher (Standard And Raw Material)";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                Sl.SetLueAcType(ref LueAcType);
                Sl.SetLueVoucherPaymentType(ref LuePaymentType);
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueUserCode(ref LuePIC);
                SetParameter();
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]{ "Description", "Amount", "Remark" },
                    new int[]{ 300, 100, 300 }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 1 }, 0);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd, DteDocDt, TxtLocalDocNo, LueAcType, LueBankAcCode, 
                        LuePaymentType, LueBankCode, TxtGiroNo, DteOpeningDt, DteDueDt, 
                        LuePIC, MeeRemark
                    }, true);
                    BtnVoucherRequest.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueBankAcCode, LuePaymentType, LuePIC, 
                        MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2 });
                    BtnVoucherRequest.Enabled = true;
                    BtnVoucherRequest.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtLocalDocNo, TxtVoucherRequestDocNo, TxtDocType, 
                TxtVdInvNo, TxtVdCode, TxtVdInvNo, TxtQueueNo, LueAcType, 
                LueBankAcCode, LuePaymentType, LueBankCode, TxtGiroNo, DteOpeningDt, DteDueDt, 
                LuePIC, TxtCurCode, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 1 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVoucher2Find(this, mMInd);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            BtnVoucherRequest_Click(sender, e);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "Voucher", "VoucherDtl" };

            var l = new List<VoucherHdr>();
            var ldtl = new List<VoucherDtl>();

            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, J.CancelInd, A.DocType, ");
            SQL.AppendLine("A.AcType, J.DocNo As VoucherDocNo, ");
            SQL.AppendLine("(Select DATE_FORMAT(DocDt,'%d %M %Y') As DocDt from tblvoucherhdr Where DocNo=@DocNo Limit 1) As VoucherDocDt, ");
            SQL.AppendLine("Concat(IfNull(C.BankAcNo, ''), ' ', IfNull(C.BankAcNm, '')) As BankAcc, ");
            SQL.AppendLine("(Select OptDesc From TblOption Where OptCat='VoucherPaymentType' AND OptCode=J.PaymentType Limit 1) As PaymentType, ");
            SQL.AppendLine("A.GiroNo,E.BankName As GiroBankName, DATE_FORMAT(A.DueDt,'%d %M %Y') As GiroDueDt, F.UserName As PICName, A.Amt As AmtHdr, J.Remark As RemarkHdr, A.CurCode, A.DocEnclosure, ");
            SQL.AppendLine("	 ifnull(Concat(A.PaymentUser, '     ', I.VdName), ifnull(I.VdName, A.PaymentUser)) As PaymentUser, G.DeptName ");
            SQL.AppendLine(" From TblVoucherRequestHdr A ");
            SQL.AppendLine(" left Join TblVoucherHdr J On A.DocNo = J.VoucherRequestDocNo ");
            SQL.AppendLine(" Left Join TblBankAccount C On J.BankAcCode=C.BankAcCode ");
            SQL.AppendLine(" Left Join TblBank D On C.BankCode=D.BankCode ");
            SQL.AppendLine(" Left Join TblBank E On J.BankCode=E.BankCode ");
            SQL.AppendLine(" Left Join TblDepartment G On A.DeptCode = G.DeptCode ");
            SQL.AppendLine(" left Join TblOutgoingPaymentHdr H On A.DocNo = H.VoucherRequestDocNo ");
            SQL.AppendLine(" left Join TblVendor I On H.VdCode = I.VdCode ");
            SQL.AppendLine(" Left Join TblUser F On J.PIC=F.UserCode ");
            SQL.AppendLine(" Where J.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyName",

                         //1-5
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo", 
                         "DocDt", 
                         "CancelInd",
 
                         //6-10
                         "DocType", 
                         "AcType",
                         "VoucherDocNo", 
                         "VoucherDocDt", 
                         "BankAcc", 
                         
                         //11-15
                         "PaymentType",
                         "GiroNo",
                         "GiroBankName", 
                         "GiroDueDt", 
                         "PICName",
 
                         //16-20
                         "AmtHdr", 
                         "RemarkHdr",
                         "CompanyLogo",
                         "CurCode",
                         "DocEnclosure",

                         //21-22
                         "PaymentUser",
                         "DeptName"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new VoucherHdr()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),
                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyPhone = Sm.DrStr(dr, c[2]),

                            DocNo = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            CancelInd = Sm.DrStr(dr, c[5]),
                            DocType = Sm.DrStr(dr, c[6]),
                            AcType = Sm.DrStr(dr, c[7]),
                            VoucherDocNo = Sm.DrStr(dr, c[8]),
                            VoucherDocDt = Sm.DrStr(dr, c[9]),
                            BankAcc = Sm.DrStr(dr, c[10]),
                            PaymentType = Sm.DrStr(dr, c[11]),
                            GiroNo = Sm.DrStr(dr, c[12]),
                            GiroBankName = Sm.DrStr(dr, c[13]),
                            GiroDueDt = Sm.DrStr(dr, c[14]),
                            PICName = Sm.DrStr(dr, c[15]),
                            AmtHdr = Sm.DrDec(dr, c[16]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[16])),
                            RemarkHdr = Sm.DrStr(dr, c[17]),

                            CompanyLogo = Sm.DrStr(dr, c[18]),
                            CurCode = Sm.DrStr(dr, c[19]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            DocEnclosure = Sm.DrDec(dr, c[20]),
                            PaymentUser = Sm.DrStr(dr, c[21]),
                            Department = Sm.DrStr(dr, c[22])
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);
            #endregion

            #region detail
            var cmDtl = new MySqlCommand();

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                cmDtl.CommandText = "Select DNo, Description, Amt, Remark from TblVoucherDtl Where DocNo=@DocNo";
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DNo",

                         //1-5
                         "Description",
                         "Amt",
                         "Remark" 
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new VoucherDtl()
                        {
                            DNo = Sm.DrStr(drDtl, cDtl[0]),
                            Description = Sm.DrStr(drDtl, cDtl[1]),
                            Amt = Sm.DrDec(drDtl, cDtl[2]),
                            Remark = Sm.DrStr(drDtl, cDtl[3])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            Sm.PrintReport("Voucher", myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!(BtnSave.Enabled && TxtDocNo.Text.Length == 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length!=0))
                e.DoDefault = false;
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 2 }, e);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            mTransferVoucherRMToCustDB = Sm.GetParameterBoo("TransferVoucherRMToCustDB");
            string DocNo = string.Empty;

            if (mVoucherCodeFormatType == "2")
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr");
            else
                DocNo = GenerateDocNo();

            SaveData(DocNo);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher request#", false) ||
                Sm.IsLueEmpty(LueBankAcCode, "Debit/Credit to") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LuePIC, "Person in charge") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsVoucherRequestNotValid() ||
                IsPaymentTypeNotValid() ||
                IsBankAccountCurrencyInvalid();
        }

        private bool IsBankAccountCurrencyInvalid()
        {
            var CurCode = Sm.GetValue("Select CurCode From TblBankAccount Where BankAcCode=@Param;", Sm.GetLue(LueBankAcCode));

            if (!Sm.CompareStr(CurCode, TxtCurCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning,
                        "Bank Account's Currency : " + CurCode + Environment.NewLine +
                        "Voucher's Currency : " + TxtCurCode.Text + Environment.NewLine +
                        "Bank Account and voucher should have the same currency.");
                LueBankAcCode.Focus();
                return true;
            }
            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.GetLue(LuePaymentType) == "B")
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank Name")) return true;
            }
            else if (Sm.GetLue(LuePaymentType) == "G")
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank Name")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Bilyet Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }
            else if (Sm.GetLue(LuePaymentType) == "K")
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank Name")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private bool IsVoucherRequestNotValid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblVoucherRequestHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status='A' And VoucherDocNo Is Null ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);

            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Voucher request is not valid.");
                return true;
            }

            return false;
        }

        private string GenerateDocNo()
        {
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Voucher'"),
                Type = string.Empty; 

            if (Sm.GetLue(LueAcType) == "C")
                Type = Sm.GetValue("Select ifnull(AutoNoCredit, '') From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");
            else
                Type = Sm.GetValue("Select ifnull(AutoNoDebit, '') From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");

            var SQL = new StringBuilder();

            if (Type.Length==0)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7,4) Desc Limit 1) As temp ");
                SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + Type.Length + "') = '" + Type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7,4) Desc Limit 1) As temp ");
                SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "', '/', '" + Type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateDocNo(int i)
        {
            //utk PI raw material IOK
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Voucher'"),
                Type = string.Empty;

            var SQL = new StringBuilder();
    
            SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', ");
            SQL.Append("(Select IfNull ( ");
            SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+" + i.ToString() + ", Char)), 4) As Numb From ( ");
            SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherHdr ");
            SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
            SQL.Append("Order By SUBSTRING(DocNo,7,4) Desc Limit 1) As temp ");
            SQL.Append("), '000"+i.ToString()+"') As Number), '/', '" + DocAbbr + "' ) As DocNo ");
        
            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateVoucherRequestDocNo(int i)
        {
            //utk PI raw material IOK
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'");
            
            var SQL = new StringBuilder();

            SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
            SQL.Append("(Select Right(Concat('0000', Convert(DocNo+" + i.ToString() + ", Char)), 4) As Numb From ( ");
            SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
            SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
            SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
            SQL.Append("), '000"+i.ToString()+"') As Number), '/', '" + DocAbbr + "' ) As DocNo ");

            return Sm.GetValue(SQL.ToString());
        }

        private void SaveData(string DocNo)
        {
            var cml = new List<MySqlCommand>();

            cml.Add(SaveVoucherHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveVoucherDtl(DocNo, Row));

            if (IsVoucherForAPDownpayment())
                cml.Add(SaveVendorDeposit(DocNo));

            if (IsVoucherForReturnAPDownpayment())
                cml.Add(SaveVendorDepositReturn(DocNo));

            if (IsVoucherForARDownpayment())
                cml.Add(SaveCustomerDeposit(DocNo));

            if (IsVoucherForReturnARDownpayment())
                cml.Add(SaveCustomerDepositReturn(DocNo));

            if (mIsAutoJournalActived && IsDocTypeValid()) 
                cml.Add(SaveJournal(DocNo));

            if (Sm.GetValue(
                "Select DocType From TblVoucherRequestHdr " +
                "Where DocNo=@Param;", 
                TxtVoucherRequestDocNo.Text)=="11")
            {
                if (Sm.IsDataExist(
                    "Select 1 From TblPurchaseInvoiceRawMaterialHdr " +
                    "Where VoucherRequestDocNo=@Param " +
                    "And Amt2<>0.00;",
                    TxtVoucherRequestDocNo.Text))
                {
                    string
                        DocNo2 = GenerateDocNo(2),
                        DocNo3 = GenerateDocNo(3),
                        VoucherRequestDocNo2 = GenerateVoucherRequestDocNo(1),
                        VoucherRequestDocNo3 = GenerateVoucherRequestDocNo(2);

                    cml.Add(SavePIRawMaterialVoucher(
                        DocNo, 
                        DocNo2, 
                        DocNo3,
                        VoucherRequestDocNo2,
                        VoucherRequestDocNo3
                    ));
                }
            }

            Sm.ExecCommands(cml);
        }

        private MySqlCommand SavePIRawMaterialVoucher(
            string DocNo, 
            string DocNo2, 
            string DocNo3,
            string VoucherRequestDocNo2,
            string VoucherRequestDocNo3
            )
        {
            //PI Raw Material
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherHdr Set ");
            SQL.AppendLine("    Remark = 'Pemisahan metode pembayaran (Process 1)' ");
            SQL.AppendLine("Where DocNo=@DocNo And Remark Is Null; ");

            SQL.AppendLine("Insert Into TblVoucherHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, MInd, LocalDocNo, VoucherRequestDocNo, DocType, ");
            SQL.AppendLine("AcType, BankAcCode, PaymentType, BankCode, GiroNo, OpeningDt, DueDt, ");
            SQL.AppendLine("PIC, CurCode, Amt, TransType, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo2, A.DocDt, A.CancelInd, A.MInd, A.LocalDocNo, ");
            SQL.AppendLine("@VoucherRequestDocNo2, A.DocType, 'D', A.BankAcCode, A.PaymentType, ");
            SQL.AppendLine("A.BankCode, A.GiroNo, A.OpeningDt, A.DueDt, A.PIC, ");
            SQL.AppendLine("A.CurCode, B.Amt2, A.TransType, 'Pemisahan metode pembayaran (Process 2)', A.CreateBy, ");
            SQL.AppendLine("A.CreateDt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceRawMaterialHdr B ");
            SQL.AppendLine("    On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblVoucherDtl(DocNo,DNo,Description,Amt,Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo2, '001', 'Pemisahan metode pembayaran (Process 2)', B.Amt2, Null, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceRawMaterialHdr B ");
            SQL.AppendLine("    On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblVoucherHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, MInd, LocalDocNo, VoucherRequestDocNo, DocType, ");
            SQL.AppendLine("AcType, BankAcCode, PaymentType, BankCode, GiroNo, OpeningDt, DueDt, ");
            SQL.AppendLine("PIC, CurCode, Amt, TransType, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo3, A.DocDt, A.CancelInd, A.MInd, A.LocalDocNo, ");
            SQL.AppendLine("@VoucherRequestDocNo3, A.DocType, 'C', B.BankAcCode2, B.PaymentType2, ");
            SQL.AppendLine("A.BankCode, A.GiroNo, A.OpeningDt, A.DueDt, A.PIC, ");
            SQL.AppendLine("A.CurCode, B.Amt2, A.TransType, 'Pemisahan metode pembayaran (Process 3)', A.CreateBy, ");
            SQL.AppendLine("A.CreateDt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceRawMaterialHdr B ");
            SQL.AppendLine("    On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblVoucherDtl(DocNo,DNo,Description,Amt,Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo3, '001', 'Pemisahan metode pembayaran (Process 3)', B.Amt2, Null, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceRawMaterialHdr B ");
            SQL.AppendLine("    On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
            SQL.AppendLine("DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, ");
            SQL.AppendLine("GiroNo, BankCode, DueDt, PIC, DocEnclosure, ");
            SQL.AppendLine("CurCode, Amt, PaymentUser, Remark, CreateBy, ");
            SQL.AppendLine("CreateDt) ");
            SQL.AppendLine("Select @VoucherRequestDocNo2, B.DocDt, B.CancelInd, 'A', B.DeptCode, ");
            SQL.AppendLine("B.DocType, @DocNo2, 'D', B.BankAcCode, B.PaymentType, ");
            SQL.AppendLine("B.GiroNo, B.BankCode, B.DueDt, B.PIC, B.DocEnclosure, ");
            SQL.AppendLine("B.CurCode, C.Amt, B.PaymentUser, 'Pemisahan metode pembayaran (Process 2)', A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblVoucherHdr C On C.DocNo=@DocNo2 ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @VoucherRequestDocNo2, '001', 'Pemisahan metode pembayaran (Process 2)', C.Amt, Null, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblVoucherHdr C On C.DocNo=@DocNo2 ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
            SQL.AppendLine("DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, ");
            SQL.AppendLine("GiroNo, BankCode, DueDt, PIC, DocEnclosure, ");
            SQL.AppendLine("CurCode, Amt, PaymentUser, Remark, CreateBy, ");
            SQL.AppendLine("CreateDt) ");
            SQL.AppendLine("Select @VoucherRequestDocNo3, B.DocDt, B.CancelInd, 'A', B.DeptCode, ");
            SQL.AppendLine("B.DocType, @DocNo3, 'C', B.BankAcCode, B.PaymentType, ");
            SQL.AppendLine("B.GiroNo, B.BankCode, B.DueDt, B.PIC, B.DocEnclosure, ");
            SQL.AppendLine("B.CurCode, C.Amt, B.PaymentUser, 'Pemisahan metode pembayaran (Process 3)', A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblVoucherHdr C On C.DocNo=@DocNo3 ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @VoucherRequestDocNo3, '001', 'Pemisahan metode pembayaran (Process 3)', C.Amt, Null, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblVoucherHdr C On C.DocNo=@DocNo3 ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblPurchaseInvoiceRawMaterialHdr Set ");
            SQL.AppendLine("    VoucherRequestDocNo2=@VoucherRequestDocNo2, ");
            SQL.AppendLine("    VoucherRequestDocNo3=@VoucherRequestDocNo3 ");
            SQL.AppendLine("Where VoucherRequestDocNo In ( ");
            SQL.AppendLine("    Select VoucherRequestDocNo ");
            SQL.AppendLine("    From TblVoucherHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    );");

            SQL.AppendLine("Update TblVoucherHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine("Where DocNo=@DocNo2;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, Concat('Voucher (Raw Material) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblVoucherHdr Where DocNo=@DocNo2;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select Concat(E.ParValue, D.VdCode) As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        IfNull(B.Amt2, 0.00) As CAmt ");
            SQL.AppendLine("        From TblVoucherRequestHdr A ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceRawMaterialHdr B On A.DocNo=B.VoucherRequestDocNo2 ");
            SQL.AppendLine("        Inner Join TblRawMaterialVerify C On B.RawMaterialVerifyDocNo=C.DocNo ");
            SQL.AppendLine("        Inner Join TblLegalDocVerifyHdr D On C.LegalDocVerifyDocNo=D.DocNo ");
            SQL.AppendLine("        Inner Join TblParameter E On E.ParCode='VendorAcNoAP' And E.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@VoucherRequestDocNo2 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("        IfNull(B.Amt2, 0.00) As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblVoucherRequestHdr A ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceRawMaterialHdr B On A.DocNo=B.VoucherRequestDocNo2 ");
            SQL.AppendLine("        Inner Join TblBankAccount C On C.BankAcCode=@BankAcCode And C.COAAcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@VoucherRequestDocNo2 ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblVoucherHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo2 ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblVoucherHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine("Where DocNo=@DocNo3;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, Concat('Voucher (Raw Material) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblVoucherHdr Where DocNo=@DocNo3;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select Concat(E.ParValue, D.VdCode) As AcNo, ");
            SQL.AppendLine("        IfNull(B.Amt2, 0.00) As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblVoucherRequestHdr A ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceRawMaterialHdr B On A.DocNo=B.VoucherRequestDocNo3 ");
            SQL.AppendLine("        Inner Join TblRawMaterialVerify C On B.RawMaterialVerifyDocNo=C.DocNo ");
            SQL.AppendLine("        Inner Join TblLegalDocVerifyHdr D On C.LegalDocVerifyDocNo=D.DocNo ");
            SQL.AppendLine("        Inner Join TblParameter E On E.ParCode='VendorAcNoAP' And E.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@VoucherRequestDocNo3 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        IfNull(B.Amt2, 0.00) As CAmt ");
            SQL.AppendLine("        From TblVoucherRequestHdr A ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceRawMaterialHdr B On A.DocNo=B.VoucherRequestDocNo3 ");
            SQL.AppendLine("        Inner Join TblBankAccount C On B.BankAcCode2=C.BankAcCode And C.COAAcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@VoucherRequestDocNo3 ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblVoucherHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo3 ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocNo2", DocNo2);
            Sm.CmParam<String>(ref cm, "@DocNo3", DocNo3);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo2", VoucherRequestDocNo2);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo3", VoucherRequestDocNo3);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            
            return cm;
        }

        private MySqlCommand SaveVoucherHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.Append("Insert Into TblVoucherHdr ");
            SQL.Append("(DocNo, DocDt, CancelInd, MInd, LocalDocNo, VoucherRequestDocNo, DocType, ");
            SQL.Append("AcType, BankAcCode, PaymentType, BankCode, GiroNo, OpeningDt, DueDt, ");
            SQL.Append("PIC, CurCode, Amt, TransType, Remark, CreateBy, CreateDt) ");
            SQL.Append("Values (@DocNo, @DocDt, 'N', @MInd, @LocalDocNo, @VoucherRequestDocNo, ");
            SQL.Append("(Select DocType From TblVoucherRequestHdr Where DocNo=@VoucherRequestDocNo), ");
            SQL.Append("@AcType, @BankAcCode, @PaymentType, @BankCode, @GiroNo, @OpeningDt, @DueDt, ");
            SQL.Append("@PIC, (Select ParValue From TblParameter Where ParCode='MainCurCode'), @Amt, @TransType, @Remark, ");
            SQL.Append("@CreateBy, CurrentDateTime()); ");

            SQL.Append("Update TblVoucherRequestHdr Set ");
            SQL.Append("    VoucherDocNo=@DocNo ");
            SQL.Append("Where DocNo=@VoucherRequestDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MInd", mMInd);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@OpeningDt", Sm.GetDte(DteOpeningDt));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            if (TxtQueueNo.Text.Length > 0)
            {
                Sm.CmParam<String>(ref cm, "@TransType", "R");
                if (MeeRemark.Text.Length > 0)
                    Sm.CmParam<string>(ref cm, "@Remark", MeeRemark.Text + " (Raw Material Voucher)");
                else
                    Sm.CmParam<string>(ref cm, "@Remark", "Raw Material Voucher");
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@TransType", "N");
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            }
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveVoucherDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherDtl(DocNo,DNo,Description,Amt,Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @Description, @Amt, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsVoucherForAPDownpayment()
        {
            var cm = new MySqlCommand
            {
                CommandText = "Select 1 From TblAPDownpayment Where VoucherRequestDocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);
            return (Sm.IsDataExist(cm));
        }

        private bool IsVoucherForReturnAPDownpayment()
        {
            return Sm.IsDataExist("Select 1 From TblVoucherRequestHdr Where DocType='14' And DocNo=@Param;", TxtVoucherRequestDocNo.Text);
        }

        private bool IsVoucherForReturnARDownpayment()
        {
            return Sm.IsDataExist("Select 1 From TblVoucherRequestHdr Where DocType='51' And DocNo=@Param;", TxtVoucherRequestDocNo.Text);
        }

        private bool IsVoucherForARDownpayment()
        {
            var cm = new MySqlCommand
            {
                CommandText = "Select 1 From TblARDownpayment Where VoucherRequestDocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);
            return (Sm.IsDataExist(cm));
        }

        private MySqlCommand SaveVendorDeposit(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @VdCode:=D.VdCode, @EntCode:=F.EntCode, @Amt:=(Case @CancelInd When 'Y' Then -1 Else 1 End *A.Amt) ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblAPDownpayment C On B.DocNo=C.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblPOHdr D On C.PODocNo=D.DocNo ");
            SQL.AppendLine("Left Join TblSite E On D.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter F On E.ProfitCenterCode=F.ProfitCenterCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Select @ExcRate:=(Case @CurCode When 'IDR' Then 1 Else IfNull(Amt, 0) End) From TblCurrencyRate ");
            SQL.AppendLine("Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("Order By RateDt Desc Limit 1; ");

            SQL.AppendLine("Insert Into TblVendorDepositMovement(DocNo, DocType, DocDt, VdCode, EntCode, CurCode, Amt, ExcRate, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, ");
            SQL.AppendLine("(Case @CancelInd When 'Y' Then '02' Else '01' End) As DocType, ");
            SQL.AppendLine("DocDt, @VdCode, @EntCode, @CurCode, @Amt, @ExcRate, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblVoucherHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblVendorDepositSummary(VdCode, EntCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @VdCode, IfNull(@EntCode, ''), @CurCode, @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update Amt=Amt+@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Update TblVendorDepositSummary2 Set ");
            SQL.AppendLine("   Amt=Amt+@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where VdCode=@VdCode ");
            SQL.AppendLine("And CurCode=@CurCode ");
            SQL.AppendLine("And CurCode<>@MainCurCode ");
            SQL.AppendLine("And IfNull(EntCode, '')=IfNull(@EntCode, '') ");
            SQL.AppendLine("And ExcRate=IfNull(@ExcRate, 1.00); ");

            SQL.AppendLine("Insert Into TblVendorDepositSummary2(VdCode, EntCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select VdCode, EntCode, CurCode, ExcRate, Amt, CreateBy, CreateDt ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select @VdCode As VdCode, IfNull(@EntCode, '') As EntCode, ");
            SQL.AppendLine("    @CurCode As CurCode, IfNull(@ExcRate, 1.00) As ExcRate, @Amt As Amt, ");
            SQL.AppendLine("    @UserCode As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Where T.CurCode<>@MainCurCode ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblVendorDepositSummary2 ");
            SQL.AppendLine("    Where VdCode=@VdCode ");
            SQL.AppendLine("    And CurCode=@CurCode ");
            SQL.AppendLine("    And IfNull(EntCode, '')=IfNull(@EntCode, '') ");
            SQL.AppendLine("    And ExcRate=IfNull(@ExcRate, 1.00) ");
            SQL.AppendLine(");");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked)?"Y":"N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveVendorDepositReturn(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @VdCode:=C.VdCode, @EntCode:=C.EntCode, @CurCode:=C.CurCode, ");
            SQL.AppendLine("@Amt:=(Case @CancelInd When 'Y' Then 1 Else -1 End *C.Amt) ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblReturnAPDownpayment C On B.DocNo=C.VoucherRequestDocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Select @ExcRate:=(Case @CurCode When 'IDR' Then 1 Else IfNull(Amt, 0) End) From TblCurrencyRate ");
            SQL.AppendLine("Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("Order By RateDt Desc Limit 1; ");

            SQL.AppendLine("Insert Into TblVendorDepositMovement(DocNo, DocType, DocDt, VdCode, EntCode, CurCode, Amt, ExcRate, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, ");
            SQL.AppendLine("(Case @CancelInd When 'Y' Then '10' Else '09' End) As DocType, ");
            SQL.AppendLine("DocDt, @VdCode, @EntCode, @CurCode, @Amt, @ExcRate, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblVoucherHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblVendorDepositSummary(VdCode, EntCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @VdCode, IfNull(@EntCode, ''), @CurCode, @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update Amt=Amt+@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblVendorDepositSummary2(VdCode, EntCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @VdCode, IfNull(@EntCode, ''), @CurCode, IfNull(@ExcRate, 1), @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update Amt=Amt+@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveCustomerDepositReturn(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CtCode:=C.CtCode, @EntCode:=C.EntCode, @CurCode:=C.CurCode, ");
            SQL.AppendLine("@Amt:=(Case @CancelInd When 'Y' Then 1 Else -1 End *C.Amt) ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblReturnARDownpayment C On B.DocNo=C.VoucherRequestDocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Select @ExcRate:=(Case @CurCode When 'IDR' Then 1 Else IfNull(Amt, 0) End) From TblCurrencyRate ");
            SQL.AppendLine("Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("Order By RateDt Desc Limit 1; ");

            SQL.AppendLine("Insert Into TblCustomerDepositMovement(DocNo, DocType, DocDt, CtCode, EntCode, CurCode, Amt, ExcRate, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, ");
            SQL.AppendLine("(Case @CancelInd When 'Y' Then '08' Else '07' End) As DocType, ");
            SQL.AppendLine("DocDt, @CtCode, @EntCode, @CurCode, @Amt, @ExcRate, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblVoucherHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblCustomerDepositSummary(CtCode, EntCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @CtCode, IfNull(@EntCode, ''), @CurCode, @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update Amt=Amt+@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblCustomerDepositSummary2(CtCode, EntCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @CtCode, IfNull(@EntCode, ''), @CurCode, IfNull(@ExcRate, 1), @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update Amt=Amt+@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveCustomerDeposit(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CtCode:=D.CtCode, @CurCode:=B.CurCode,  @Amt:=(Case @CancelInd When 'Y' Then -1 Else 1 End *A.Amt) ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblARDownpayment C On B.DocNo=C.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblSOHdr D On C.SODocNo=D.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Select @ExcRate:=(Case @CurCode When 'IDR' Then 1 Else IfNull(Amt, 0) End) From TblCurrencyRate ");
            SQL.AppendLine("Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("Order By RateDt Desc Limit 1; ");

            SQL.AppendLine("Insert Into TblCustomerDepositMovement(DocNo, DocType, DocDt, CtCode, CurCode, Amt, ExcRate, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, ");
            SQL.AppendLine("(Case @CancelInd When 'Y' Then '02' Else '01' End) As DocType, ");
            SQL.AppendLine("DocDt, @CtCode, @CurCode, @Amt, @ExcRate, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblVoucherHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblCustomerDepositSummary(CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @CtCode, @CurCode, @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update Amt=Amt+@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Update TblCustomerDepositSummary2 Set ");
            SQL.AppendLine("   Amt=Amt+@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where CtCode=@CtCode ");
            SQL.AppendLine("And CurCode=@CurCode ");
            SQL.AppendLine("And CurCode<>@MainCurCode ");
            SQL.AppendLine("And ExcRate=IfNull(@ExcRate, 1.00); ");

            SQL.AppendLine("Insert Into TblCustomerDepositSummary2(CtCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select CtCode, CurCode, ExcRate, Amt, CreateBy, CreateDt ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select @CtCode As CtCode, ");
            SQL.AppendLine("    @CurCode As CurCode, IfNull(@ExcRate, 1.00) As ExcRate, @Amt As Amt, ");
            SQL.AppendLine("    @UserCode As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Where T.CurCode<>@MainCurCode ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblCustomerDepositSummary2 ");
            SQL.AppendLine("    Where CtCode=@CtCode ");
            SQL.AppendLine("    And CurCode=@CurCode ");
            SQL.AppendLine("    And ExcRate=IfNull(@ExcRate, 1.00) ");
            SQL.AppendLine(");");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private void SaveData2(string DocNo)
        {
            if (TxtQueueNo.Text.Length > 0 && mTransferVoucherRMToCustDB)
            {
                var lrmd = new List<RawMaterialDtl>();
                
                SetRawMaterialDtl(ref lrmd, DocNo);

                var cml = new List<MySqlCommand>();

                cml.Add(SaveRawMaterialHdr(DocNo));
                lrmd.ForEach(i => { cml.Add(SaveRawMaterialDtl(i)); });
                Sm.ExecCommands(cml);
            }
        }

        private void SetRawMaterialDtl(ref List<RawMaterialDtl> lrmd, string DocNo)
        {
            int i = 0;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Voucher 2 */ ");
            SQL.Append("Select CancelInd, Bin, ");
            SQL.Append("ItCode, ItName, Length, Width, Height, Diameter, ");
            SQL.Append("PGCode, PGName, CurCode, UPrice, ");
            SQL.Append("Sum(Qty1) As Qty1, Sum(Qty2) As Qty2 ");
            SQL.Append("From ( ");
            SQL.Append("        Select A.DocNo, A.CancelInd, ");
            SQL.Append("        Case F.SectionNo ");
            SQL.Append("        When '1' Then E.Shelf1 ");
            SQL.Append("        When '2' Then E.Shelf2 ");
            SQL.Append("        When '3' Then E.Shelf3 ");
            SQL.Append("        When '4' Then E.Shelf4 ");
            SQL.Append("        When '5' Then E.Shelf5 ");
            SQL.Append("        When '6' Then E.Shelf6 ");
            SQL.Append("        When '7' Then E.Shelf7 ");
            SQL.Append("        When '8' Then E.Shelf8 ");
            SQL.Append("        When '9' Then E.Shelf9 ");
            SQL.Append("        When '10' Then E.Shelf10 ");
            SQL.Append("        When '11' Then E.Shelf11 ");
            SQL.Append("        When '12' Then E.Shelf12 ");
            SQL.Append("        When '13' Then E.Shelf13 ");
            SQL.Append("        When '14' Then E.Shelf14 ");
            SQL.Append("        When '15' Then E.Shelf15 ");
            SQL.Append("        End As Bin, ");
            SQL.Append("        F.ItCode, G.ItName, G.Length, G.Width, G.Height, G.Diameter, ");
            SQL.Append("        F.Qty As Qty1, ");
            SQL.Append("        (F.Qty * ( ");
            SQL.Append("            Case G.ItCtCode ");
            SQL.Append("                When @ItCtRMPActual Then Truncate(((0.25*3.1415926*G.Diameter*G.Diameter*G.Length)/1000000), 4) ");
            SQL.Append("                When @ItCtRMPActual2 Then Truncate(((G.Length*G.Width*G.Height)/1000000), 4) ");
            SQL.Append("                Else 0 End ");
            SQL.Append("        )) As Qty2, ");
            SQL.Append("        H.PGCode, H.PGName, H.CurCode, H.UPrice ");
            SQL.Append("        From TblVoucherHdr A ");
            SQL.Append("        Inner Join TblVoucherRequestHdr B On A.DocNo=B.VoucherDocNo And B.CancelInd='N' ");
            SQL.Append("        Inner Join TblPurchaseInvoiceRawMaterialHdr C On B.DocNo=C.VoucherRequestDocNo And C.CancelInd='N' ");
            SQL.Append("        Inner Join TblRawMaterialVerify D On C.RawMaterialverifyDocNo=D.DocNo And D.CancelInd='N' ");
            SQL.Append("        Inner Join TblRecvRawMaterialHdr E On D.LegalDocVerifyDocNo=E.LegalDocVerifyDocNo And C.DocType=E.DocType And E.CancelInd='N' ");
            SQL.Append("        Inner Join TblRecvRawMaterialDtl2 F On E.DocNo=F.DocNo ");
            SQL.Append("        Inner Join TblItem G On F.ItCode=G.ItCode ");
            SQL.Append("        Inner Join ( ");
            SQL.Append("            Select Distinct T4.ItCode, T6.PGCode, T7.PGName, T5.CurCode, T6.UPrice ");
            SQL.Append("            From TblVoucherHdr T1 ");
            SQL.Append("            Inner Join TblVoucherRequestHdr T2 On T1.DocNo=T2.VoucherDocNo And T2.CancelInd='N' ");
            SQL.Append("            Inner Join TblPurchaseInvoiceRawMaterialHdr T3 On T2.DocNo=T3.VoucherRequestDocNo And T3.CancelInd='N' ");
            SQL.Append("            Inner Join TblPurchaseInvoiceRawMaterialDtl T4 On T3.DocNo=T4.DocNo  ");
            SQL.Append("            Inner Join TblQt2Hdr T5 On T4.QtDocNo=T5.DocNo ");
            SQL.Append("            Inner Join TblQt2Dtl T6 On T5.DocNo=T6.DocNo And T4.QtDNo=T6.DNo ");
            SQL.Append("            Inner Join TblPricingGroup T7 On T6.PGCode=T7.PGCode ");
            SQL.Append("            Where T1.DocNo=@DocNo ");
            SQL.Append("        ) H On G.ItCode=H.ItCode ");
            SQL.Append("        Where A.DocNo=@DocNo  ");
            SQL.Append("    ) T ");
            SQL.Append("    Group By CancelInd, Bin, ");
            SQL.Append("    ItCode, ItName, Length, Width, Height, Diameter, ");
            SQL.Append("    PGCode, PGName, CurCode, UPrice ");
            SQL.Append("    Order By Bin, ItCode;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@ItCtRMPActual", mItCtRMPActual);
                Sm.CmParam<String>(ref cm, "@ItCtRMPActual2", mItCtRMPActual2);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CancelInd",

                         //1-5
                         "Bin", "ItCode", "ItName", "Length", "Width", 
                         
                         //6-10
                         "Height", "Diameter", "PGCode", "PGName", "CurCode",

                         //11-13
                         "UPrice", "Qty1", "Qty2"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        i += 1;
                        lrmd.Add(new RawMaterialDtl()
                        {
                            DocNo = DocNo,
                            DNo = Sm.Right("00" + i.ToString(), 3),
                            CancelInd = Sm.DrStr(dr, c[0]),
                            Bin = Sm.DrStr(dr, c[1]),
                            ItCode = Sm.DrStr(dr, c[2]),
                            ItName = Sm.DrStr(dr, c[3]),
                            Length = Sm.DrDec(dr, c[4]),
                            Width = Sm.DrDec(dr, c[5]),
                            Height = Sm.DrDec(dr, c[6]),
                            Diameter = Sm.DrDec(dr, c[7]),
                            PGCode = Sm.DrStr(dr, c[8]),
                            PGName = Sm.DrStr(dr, c[9]),
                            CurCode = Sm.DrStr(dr, c[10]),
                            UPrice = Sm.DrDec(dr, c[11]),
                            Qty1 = Sm.DrDec(dr, c[12]),
                            Qty2 = Sm.DrDec(dr, c[13])
                        });
                    }
                }
                dr.Close();
            }
        }

        private MySqlCommand SaveRawMaterialHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Voucher 2 */ ");
            SQL.Append("Insert Into " + mCustomerDatabase + ".TblRawMaterialHdr ");
            SQL.Append("(DocNo, CancelInd, ProcessInd, DocDt, QueueNo, VehicleRegNo, TTName, ");
            SQL.Append("VdCode, VdName, VdCode1, VdName1, VdCode2, VdName2, TIN, TaxInd, "); 
            SQL.Append("EmpName1, EmpName2, EmpName3, EmpName4, LegalDoc, "); 
            SQL.Append("Amt, Tax, TransportCost, RoundingValue, "); 
            SQL.Append("CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.Append("Select A.DocNo, A.CancelInd, '0', Concat(Left(A.DocDt, 4),'-',Substring(A.DocDt, 5, 2), '-',Substring(A.DocDt, 7, 2)) As DocDt, E.QueueNo, E.VehicleRegNo, G.TTName, ");
            SQL.Append("E.VdCode, H.VdName, E.VdCode1, I.VdName As VdName1, E.VdCode2, J.VdName As VdName2, H.TIN, H.TaxInd, ");
            SQL.Append("( ");
	        SQL.Append("    Select Group_Concat(Distinct T3.EmpName Order By T3.EmpName) ");
	        SQL.Append("    From TblRecvRawmaterialHdr T1, TblRecvRawmaterialDtl T2, TblEmployee T3 "); 
	        SQL.Append("    Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.EmpCode=T3.EmpCode And T2.EmpType='1' "); 
	        SQL.Append("    And T1.LegalDocVerifyDocNo=E.DocNo And T1.DocType=C.DocType ");
            SQL.Append(") As EmpName1, ");
            SQL.Append("( ");
	        SQL.Append("    Select Group_Concat(Distinct T3.EmpName Order By T3.EmpName) ");
	        SQL.Append("    From TblRecvRawmaterialHdr T1, TblRecvRawmaterialDtl T2, TblEmployee T3 "); 
	        SQL.Append("    Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.EmpCode=T3.EmpCode And T2.EmpType='2' "); 
	        SQL.Append("    And T1.LegalDocVerifyDocNo=E.DocNo And T1.DocType=C.DocType ");
            SQL.Append(") As EmpName2, ");
            SQL.Append("( ");
	        SQL.Append("    Select Group_Concat(Distinct T3.EmpName Order By T3.EmpName) ");
	        SQL.Append("    From TblRecvRawmaterialHdr T1, TblRecvRawmaterialDtl T2, TblEmployee T3 "); 
	        SQL.Append("    Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.EmpCode=T3.EmpCode And T2.EmpType='3' "); 
	        SQL.Append("    And T1.LegalDocVerifyDocNo=E.DocNo And T1.DocType=C.DocType ");
            SQL.Append(") As EmpName3, ");
            SQL.Append("( ");
	        SQL.Append("    Select Group_Concat(Distinct T3.EmpName Order By T3.EmpName) ");
	        SQL.Append("    From TblRecvRawmaterialHdr T1, TblRecvRawmaterialDtl T2, TblEmployee T3 "); 
	        SQL.Append("    Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.EmpCode=T3.EmpCode And T2.EmpType='4' "); 
	        SQL.Append("    And T1.LegalDocVerifyDocNo=E.DocNo And T1.DocType=C.DocType ");
            SQL.Append(") As EmpName4, ");
            SQL.Append("( ");
	        SQL.Append("    Select Group_Concat(Distinct "); 
	        SQL.Append("        Concat(T3.DLName, Case When T2.DLDocNo Is Null Then '' Else Concat(' (', T2.DLDocNo, ')') End ");
		    SQL.Append("        ) Order By T3.DLName, T2.DLDocNo ");
            SQL.Append("    ) ");
	        SQL.Append("    From TblLegalDocVerifyHdr T1, TblLegalDocVerifyDtl T2, TblDocLegality T3 "); 
	        SQL.Append("    Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.DLCode=T3.DLCode "); 
	        SQL.Append("    And T1.DocNo=E.DocNo And T2.DLType=C.DocType ");
            SQL.Append(") As LegalDoc, ");
            SQL.Append("C.Amt, C.Tax, C.TransportCost, C.RoundingValue, ");
            SQL.Append("A.CreateBy, A.Createdt, A.LastUpBy, A.LastUpDt "); 
            SQL.Append("From TblVoucherHdr A ");
            SQL.Append("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.Append("Inner Join TblPurchaseInvoiceRawMaterialHdr C On B.DocNo=C.VoucherRequestDocNo And C.CancelInd='N' ");
            SQL.Append("Inner Join TblRawMaterialVerify D On C.RawMaterialverifyDocNo=D.DocNo And D.CancelInd='N' ");
            SQL.Append("Inner Join TblLegalDocVerifyHdr E On D.LegalDocVerifyDocNo=E.DocNo And E.CancelInd='N' ");
            SQL.Append("Inner Join TblLoadingQueue F On E.QueueNo=F.DocNo "); 
            SQL.Append("Left Join TblTransportType G  On F.TTCode=G.TTCode ");
            SQL.Append("Left Join TblVendor H  On E.VdCode=H.VdCode ");
            SQL.Append("Left Join TblVendor I  On E.VdCode1=I.VdCode ");
            SQL.Append("Left Join TblVendor J  On E.VdCode2=J.VdCode ");
            SQL.Append("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private MySqlCommand SaveRawMaterialDtl(RawMaterialDtl i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Voucher 2 */ ");
            SQL.Append("Insert Into " + mCustomerDatabase + ".TblRawMaterialDtl ");
            SQL.Append("(DocNo, CancelInd, DNo, ");
            SQL.Append("Bin, ItCode, ItName, Length, Width, Height, Diameter, ");
            SQL.Append("Qty1, Qty2, PGCode, PGName, CurCode, UPrice, ");
            SQL.Append("CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.Append("Values(@DocNo, @CancelInd, @DNo, ");
            SQL.Append("@Bin, @ItCode, @ItName, @Length, @Width, @Height, @Diameter, ");
            SQL.Append("@Qty1, @Qty2, @PGCode, @PGName, @CurCode, @UPrice, ");
            SQL.Append("@CreateBy, CurrentDateTime(), Null, Null); ");
           
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", i.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", i.DNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", i.CancelInd);
            Sm.CmParam<String>(ref cm, "@Bin", i.Bin);
            Sm.CmParam<String>(ref cm, "@ItCode", i.ItCode);
            Sm.CmParam<String>(ref cm, "@ItName", i.ItName);
            Sm.CmParam<Decimal>(ref cm, "@Length", i.Length);
            Sm.CmParam<Decimal>(ref cm, "@Width", i.Width);
            Sm.CmParam<Decimal>(ref cm, "@Height", i.Height);
            Sm.CmParam<Decimal>(ref cm, "@Diameter", i.Diameter);
            Sm.CmParam<Decimal>(ref cm, "@Qty1", i.Qty1);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", i.Qty2);
            Sm.CmParam<String>(ref cm, "@PGCode", i.PGCode);
            Sm.CmParam<String>(ref cm, "@PGName", i.PGName);
            Sm.CmParam<String>(ref cm, "@CurCode", i.CurCode);
            Sm.CmParam<Decimal>(ref cm, "@UPrice", i.UPrice);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsDocTypeValid()
        {
            var DocType = Sm.GetValue("Select DocType From TblVoucherRequestHdr Where DocNo=@Param;", TxtVoucherRequestDocNo.Text);
            return (
                DocType == "02" ||
                DocType == "03" ||
                DocType == "04" ||
                DocType == "05" ||
                DocType == "06" ||
                DocType == "07" ||
                DocType == "09" ||
                DocType == "11" ||
                DocType == "13" ||
                DocType == "14" ||
                DocType == "15" ||
                DocType == "17" ||
                DocType == "18" ||
                DocType == "19" ||
                DocType == "20" ||
                DocType == "21" ||
                DocType == "22" ||
                DocType == "51" ||
                DocType == "54"
                );
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            //01 : Manual
            //02 : Incoming Payment
            //03 : Outgoing Payment
            //04 : AP Downpayment
            //05 : AR Downpayment
            //06 : Payroll Processing
            //07 : Social Security
            //08 : Claim
            //09 : Advance Payment
            //10 : Point of Sale
            //11 : Raw Material
            //13 : RHA
            //14 : Returning AP Downpayment
            //15 : VAT
            //17 : Outgoing Payment Additional Amount
            //18 : Incoming Payment Additional Amount
            //19 : AP Downpayment Additional Amount
            //20 : AR Downpayment Additional Amount
            //21 : Voucher Request Tax
            //51 : Returning AR Downpayment
            //54 : Bonus

            var DocType = Sm.GetValue("Select DocType From TblVoucherRequestHdr Where DocNo=@Param;", TxtVoucherRequestDocNo.Text);
            string DocTypeDesc = string.Empty;
            switch (DocType)
            {
                case "01":
                    DocTypeDesc = "Manual";
                    break;
                case "02":
                    DocTypeDesc = "Incoming Payment";
                    break;
                case "03":
                    DocTypeDesc = "Outgoing Payment";
                    break;
                case "04":
                    DocTypeDesc = "AP Downpayment";
                    break;
                case "05":
                    DocTypeDesc = "AR Downpayment";
                    break;
                case "06":
                    DocTypeDesc = "Payroll Processing";
                    break;
                case "07":
                    DocTypeDesc = "Social Security";
                    break;
                case "08":
                    DocTypeDesc = "Claim";
                    break;
                case "09":
                    DocTypeDesc = "Advance Payment";
                    break;
                case "10":
                    DocTypeDesc = "Point of Sale";
                    break;
                case "11":
                    DocTypeDesc = "Raw Material";
                    break;
                case "13":
                    DocTypeDesc = "Religious Holiday Allowance";
                    break;
                case "14":
                    DocTypeDesc = "Returning AP Downpayment";
                    break;
                case "15":
                    DocTypeDesc = "VAT";
                    break;
                case "17":
                    DocTypeDesc = "Outgoing Payment Additional Amount";
                    break;
                case "18":
                    DocTypeDesc = "Incoming Payment Additional Amount";
                    break;
                case "19":
                    DocTypeDesc = "AP Downpayment Additional Amount";
                    break;
                case "20":
                    DocTypeDesc = "AR Downpayment Additional Amount";
                    break;
                case "21":
                    DocTypeDesc = "Voucher Request Tax";
                    break;
                case "51":
                    DocTypeDesc = "Returning AR Downpayment";
                    break;
                case "54":
                    DocTypeDesc = "Bonus";
                    break;
            }

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@DocType", DocTypeDesc);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);

            SQL.AppendLine("Update TblVoucherHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Voucher (', IfNull(@DocType, 'None'), ') : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblVoucherHdr Where DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, ");
            if (Sm.CompareStr(TxtCurCode.Text, mMainCurCode))
            {
                SQL.AppendLine("B.DAmt, ");
                SQL.AppendLine("B.CAMt, ");
            }
            else
            {
                if (DocType == "02" || DocType == "03" || DocType == "16")
                {
                    SQL.AppendLine("B.DAmt, ");
                    SQL.AppendLine("B.CAMt, ");
                }
                else
                {
                    SQL.AppendLine("B.DAmt*( ");
                    SQL.AppendLine("IfNull(( ");
                    SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("), 0) ");
                    SQL.AppendLine(") As DAmt, ");
                    SQL.AppendLine("B.CAMt*( ");
                    SQL.AppendLine("IfNull(( ");
                    SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("), 0) ");
                    SQL.AppendLine(") As CAmt, ");
                }
            }
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            SQL.AppendLine(GetJournalSQL(ref cm));

            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            //SQL.AppendLine("Where A.DocNo=@JournalDocNo;");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblVoucherHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            if (DocType == "02" || DocType == "03")
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select DAmt, CAmt From (");
                SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
                SQL.AppendLine("        From TblJournalDtl ");
                //Where DocNo=@JournalDocNo ");  
                SQL.AppendLine("        Where DocNo In ( ");
                SQL.AppendLine("            Select JournalDocNo ");
                SQL.AppendLine("            From TblVoucherHdr ");
                SQL.AppendLine("            Where DocNo=@DocNo ");
                SQL.AppendLine("            And JournalDocNo Is Not Null ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine(") B On 0=0 ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0 End, ");
                SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0 End ");
                //SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
                SQL.AppendLine("Where A.DocNo In ( ");
                SQL.AppendLine("    Select JournalDocNo ");
                SQL.AppendLine("    From TblVoucherHdr ");
                SQL.AppendLine("    Where DocNo=@DocNo ");
                SQL.AppendLine("    And JournalDocNo Is Not Null ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                SQL.AppendLine("    And ParValue Is Not Null ");
                SQL.AppendLine("    );");

                SQL.AppendLine("Delete From TblJournalDtl ");
                //SQL.AppendLine("Where DocNo=@JournalDocNo ");
                SQL.AppendLine("Where DocNo In ( ");
                SQL.AppendLine("    Select JournalDocNo ");
                SQL.AppendLine("    From TblVoucherHdr ");
                SQL.AppendLine("    Where DocNo=@DocNo ");
                SQL.AppendLine("    And JournalDocNo Is Not Null ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("And (DAmt=0 And CAmt=0) ");
                SQL.AppendLine("And AcNo In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                SQL.AppendLine("    And ParValue Is Not Null ");
                SQL.AppendLine("    );");
            }

            cm.CommandText = SQL.ToString();

            return cm;
        }

        private string GetJournalSQL(ref MySqlCommand cm)
        {
            //01 : Manual
            //02 : Incoming Payment
            //03 : Outgoing Payment
            //04 : AP Downpayment
            //05 : AR Downpayment
            //06 : Payroll Processing
            //07 : Social Security
            //08 : Claim
            //09 : Advance Payment
            //10 : Point of Sale
            //11 : Raw Material
            //13 : RHA
            //14 : Returning AP Downpayment
            //15 : VAT
            //17 : Outgoing Payment Additional Amount
            //18 : Incoming Payment Additional Amount
            //19 : AP Downpayment Additional Amount
            //20 : AR Downpayment Additional Amount
            //21 : Voucher Request Tax
            //51 : Returning AR Downpayment
            //54 : Bonus

            var DocType = Sm.GetValue("Select DocType From TblVoucherRequestHdr Where DocNo=@Param;", TxtVoucherRequestDocNo.Text);
            var SQL = string.Empty;
            switch (DocType)
            {
                case "02":
                    SQL = GetJournalSQL02(ref cm);
                    break;
                case "03":
                    SQL = GetJournalSQL03(ref cm);
                    break;
                case "04":
                    SQL = GetJournalSQL04(ref cm);
                    break;
                case "05":
                    SQL = GetJournalSQL05(ref cm);
                    break;
                case "06":
                    SQL = GetJournalSQL06(ref cm);
                    break;
                case "07":
                    SQL = GetJournalSQL07(ref cm);
                    break;
                case "09":
                    SQL = GetJournalSQL09(ref cm);
                    break;
                case "11":
                    SQL = GetJournalSQL11(ref cm);
                    break;
                case "13":
                    SQL = GetJournalSQL13(ref cm);
                    break;
                case "14":
                    SQL = GetJournalSQL14(ref cm);
                    break;
                case "15":
                    SQL = GetJournalSQL15(ref cm);
                    break;
                case "17":
                    SQL = GetJournalSQL17(ref cm);
                    break;
                case "18":
                    SQL = GetJournalSQL18(ref cm);
                    break;
                case "19":
                    SQL = GetJournalSQL19(ref cm);
                    break;
                case "20":
                    SQL = GetJournalSQL20(ref cm);
                    break;
                case "21":
                    SQL = GetJournalSQL21(ref cm);
                    break;
                case "51":
                    SQL = GetJournalSQL51(ref cm);
                    break;
                case "54":
                    SQL = GetJournalSQL54(ref cm);
                    break;
            }
            return SQL;
        }

        private string GetJournalSQL02(ref MySqlCommand cm)
        {
            //02 : Incoming Payment
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select C.COAAcNo As AcNo, ");
            //SQL.AppendLine("Case When @CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("IfNull(( ");
            //SQL.AppendLine("    Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("), 0.00) End * ");
            //SQL.AppendLine("IfNull(B.Amt2, 0) As DAmt, ");
            //SQL.AppendLine("0 As CAmt ");
            //SQL.AppendLine("From TblVoucherRequestHdr A ");
            //SQL.AppendLine("Inner Join TblIncomingPaymentHdr B On A.DocNo=B.VoucherRequestDocNo ");
            //SQL.AppendLine("Inner Join TblBankAccount C On C.BankAcCode=@BankAcCode And C.COAAcNo Is Not Null ");
            //SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");
            //SQL.AppendLine("Union All ");

            //SQL.AppendLine("Select Concat(E.ParValue, B.CtCode) As AcNo, ");
            //SQL.AppendLine("0 As DAmt, ");
            //SQL.AppendLine("Case When G.CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("IfNull(( ");
            //SQL.AppendLine("    Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("    Where RateDt<=G.DocDt And CurCode1=G.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("), 0.00) End * ");
            //SQL.AppendLine("IfNull(G.TotalAmt, 0)+IfNull(G.TotalTax, 0)-IfNull(G.DownPayment, 0) As CAmt ");
            //SQL.AppendLine("From TblVoucherRequestHdr A ");
            //SQL.AppendLine("Inner Join TblIncomingPaymentHdr B On A.DocNo=B.VoucherRequestDocNo ");
            //SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            //SQL.AppendLine("Inner Join TblCustomerCategory D On C.CtCtCode=D.CtCtCode ");
            //SQL.AppendLine("Inner Join TblParameter E On E.ParCode='CustomerAcNoAR' And E.ParValue Is Not Null ");
            //SQL.AppendLine("Inner Join TblIncomingPaymentDtl F On B.DocNo=F.DocNo And F.InvoiceType='1' ");
            //SQL.AppendLine("Inner Join TblSalesInvoiceHdr G On F.InvoiceDocNo=G.DocNo ");
            //SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");

            SQL.AppendLine("Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    Case When C.CurCode<>@MainCurCode Then ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=B.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) ");
            SQL.AppendLine("    Else B.RateAmt End ");
            SQL.AppendLine("End ");
            SQL.AppendLine("*IfNull(A.Amt, 0.00) As DAmt, ");
            SQL.AppendLine("0.00 As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblBankAccount C On C.BankAcCode=@BankAcCode And C.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select T.AcNo, ");
            SQL.AppendLine("0.00 As DAmt, ");
            SQL.AppendLine("Sum(Amt) As CAmt ");
            SQL.AppendLine("From ( ");

            SQL.AppendLine("    Select Concat(E.ParValue, B.CtCode) As AcNo, ");
            SQL.AppendLine("    Case When B.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=G.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0.00) End ");
            SQL.AppendLine("    *F.Amt As Amt ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("    Inner Join TblCustomerCategory D On C.CtCtCode=D.CtCtCode ");
            SQL.AppendLine("    Inner Join TblParameter E On E.ParCode='CustomerAcNoAR' And E.ParValue Is Not Null ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl F On B.DocNo=F.DocNo And F.InvoiceType='1' ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceHdr G On F.InvoiceDocNo=G.DocNo ");
            SQL.AppendLine("    Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Concat(E.ParValue, B.CtCode) As AcNo, ");
            SQL.AppendLine("    Case When B.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=G.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0.00) End ");
            SQL.AppendLine("    *F.Amt As Amt ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("    Inner Join TblCustomerCategory D On C.CtCtCode=D.CtCtCode ");
            SQL.AppendLine("    Inner Join TblParameter E On E.ParCode='CustomerAcNoAR' And E.ParValue Is Not Null ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl F On B.DocNo=F.DocNo And F.InvoiceType='2' ");
            SQL.AppendLine("    Inner Join TblSalesReturnInvoiceHdr G On F.InvoiceDocNo=G.DocNo ");
            SQL.AppendLine("    Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine(") T Group By T.AcNo ");

            ////Laba rugi selisih kurs
            //if (!Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            //{
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblParameter Where ParCode='AcNoForForeignCurrencyExchangeGains' And ParValue Is Not Null ");
            //}


            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

            return SQL.ToString();
        }

        private string GetJournalSQL03(ref MySqlCommand cm)
        {
            //03 : Outgoing Payment

            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.AcNo, Sum(T.Amt) As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select Concat(F.ParValue, B.VdCode) As AcNo, ");
            SQL.AppendLine("    Case When IfNull(D.CurCode, E.CurCode)=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=IfNull(D.DocDt, E.DocDt) And CurCode1=IfNull(D.CurCode, E.CurCode) And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0.00) End * ");
            SQL.AppendLine("    C.Amt As Amt ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl C On B.DocNo=C.DocNo And C.InvoiceType='1' ");
            SQL.AppendLine("    Left Join TblPurchaseInvoiceHdr D On C.InvoiceDocNo=D.DocNo ");
            SQL.AppendLine("    Left Join TblPurchaseReturnInvoiceHdr E On C.InvoiceDocNo=E.DocNo ");
            SQL.AppendLine("    Inner Join TblParameter F On F.ParCode='VendorAcNoAP' And F.ParValue Is Not Null ");
            SQL.AppendLine("    Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine(") T Group By T.AcNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("0.00 As DAmt, ");
            SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    Case When C.CurCode<>@MainCurCode Then ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=B.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) ");
            SQL.AppendLine("    Else B.RateAmt End ");
            SQL.AppendLine("End ");
            SQL.AppendLine("*IfNull(A.Amt, 0.00) As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblBankAccount C On C.BankAcCode=@BankAcCode And C.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            //Laba rugi selisih kurs

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select ParValue As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblParameter Where ParCode='AcNoForForeignCurrencyExchangeGains' And ParValue Is Not Null ");


            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

            return SQL.ToString();
        }

        private string GetJournalSQL04(ref MySqlCommand cm)
        {
            //04 : AP Downpayment

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(D.ParValue, C.VdCode) As AcNo, ");
            SQL.AppendLine("IfNull(B.Amt, 0) As DAmt, ");
            SQL.AppendLine("0.00 As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblAPDownpayment B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblPOHdr C On B.PODocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblParameter D On D.ParCode='VendorAcNoDownPayment' And D.ParValue Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("0.00 As DAmt, ");
            SQL.AppendLine("IfNull(B.Amt, 0.00) As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblAPDownpayment B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblBankAccount C On A.BankAcCode=C.BankAcCode And C.COAAcNo Is Not Null ");
            SQL.AppendLine("Inner Join TblParameter D On D.ParCode='GiroPaymentType' And D.ParValue Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists(Select 1 from TblParameter Where ParCode='AcNoForGiroAP' And ParValue Is Not Null) ");
            SQL.AppendLine("And A.PaymentType=D.ParValue ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("0.00 As DAmt, ");
            SQL.AppendLine("IfNull(B.Amt, 0) As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblAPDownpayment B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblBankAccount C On A.BankAcCode=C.BankAcCode And C.COAAcNo Is Not Null ");
            SQL.AppendLine("Inner Join TblParameter D On D.ParCode='GiroPaymentType' And D.ParValue Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.PaymentType<>D.ParValue ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Concat(C.ParValue, E.VdCode) As AcNo, ");
            SQL.AppendLine("0.00 As DAmt, ");
            SQL.AppendLine("IfNull(B.Amt, 0) As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblAPDownpayment B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblParameter C On C.ParCode='AcNoForGiroAP' And C.ParValue Is Not Null ");
            SQL.AppendLine("Inner Join TblParameter D On D.ParCode='GiroPaymentType' And D.ParValue Is Not Null ");
            SQL.AppendLine("Inner Join TblPOHdr E On B.PODocNo=E.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.PaymentType=D.ParValue ");

            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

            return SQL.ToString();
        }

        private string GetJournalSQL05(ref MySqlCommand cm)
        {
            //05 : AR Downpayment

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.COAAcNo As AcNo, IfNull(A.Amt, 0) As DAmt, 0 As CAmt ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblBankAccount B On B.BankAcCode=@BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Concat(E.ParValue, B.CtCode) As AcNo, ");
            SQL.AppendLine("0 As DAmt, IfNull(B.Amt, 0) As CAmt ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblARDownpayment B On A.DocNo=B.VoucherRequestDocNo And B.CtCode Is Not Null ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("Inner Join TblCustomerCategory D On C.CtCtCode=D.CtCtCode ");
            SQL.AppendLine("Inner Join TblParameter E On E.ParCode='CustomerAcNoDownPayment' And E.ParValue Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Concat(F.ParValue, B.CtCode) As AcNo, ");
            SQL.AppendLine("0 As DAmt, IfNull(B.Amt, 0) As CAmt ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblARDownpayment B On A.DocNo=B.VoucherRequestDocNo And B.CtCode Is Null ");
            SQL.AppendLine("Inner Join TblSOHdr C On B.SODocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer D On C.CtCode=D.CtCode ");
            SQL.AppendLine("Inner Join TblCustomerCategory E On D.CtCtCode=E.CtCtCode ");
            SQL.AppendLine("Inner Join TblParameter F On F.ParCode='CustomerAcNoDownPayment' And F.ParValue Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");

            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

            return SQL.ToString();
        }

        private string GetJournalSQL06(ref MySqlCommand cm)
        {
            //07 : Payroll Processing

            var SQL = new StringBuilder();

            SQL.AppendLine("Select @AcNoForAllowanceSSHealth As AcNo, ");
            SQL.AppendLine("B.SSEmployeeHealth+B.SSEmployerHealth As DAmt, 0 As CAmt ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestPayrollHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select @AcNoForAllowanceSSEmployment As AcNo, ");
            SQL.AppendLine("B.SSEmployeeEmployment+B.SSEmployerEmployment As DAmt, 0 As CAmt ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestPayrollHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select @AcNoForAllowanceSSPension As AcNo, ");
            SQL.AppendLine("B.SSEmployeePension+B.SSEmployerPension As DAmt, 0 As CAmt ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestPayrollHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");
            //SQL.AppendLine("Union All ");
            //SQL.AppendLine("Select @AcNoForTaxLiability As AcNo, ");
            //SQL.AppendLine("B.Tax As DAmt, 0 As CAmt ");
            //SQL.AppendLine("From TblVoucherRequestHdr A ");
            //SQL.AppendLine("Inner Join TblVoucherRequestPayrollHdr B On A.DocNo=B.VoucherRequestDocNo ");
            //SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select @AcNoForAccruedSalary As AcNo, ");
            SQL.AppendLine("B.Amt-B.SSEmployeeHealth-B.SSEmployerHealth-B.SSEmployeeEmployment-B.SSEmployerEmployment-B.SSEmployeePension-B.SSEmployerPension As DAmt, 0 As CAmt ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestPayrollHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("0 As DAmt, ");
            //SQL.AppendLine("IfNull(B.Amt, 0)+IfNull(B.Tax, 0) As CAmt ");
            SQL.AppendLine("IfNull(B.Amt, 0) As CAmt ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestPayrollHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblBankAccount C On C.BankAcCode=@BankAcCode And C.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");

            Sm.CmParam<String>(ref cm, "@AcNoForAllowanceSSHealth", Sm.GetParameter("AcNoForAllowanceSSHealth"));
            Sm.CmParam<String>(ref cm, "@AcNoForAllowanceSSEmployment", Sm.GetParameter("AcNoForAllowanceSSEmployment"));
            Sm.CmParam<String>(ref cm, "@AcNoForAllowanceSSPension", Sm.GetParameter("AcNoForAllowanceSSPension"));
            Sm.CmParam<String>(ref cm, "@AcNoForTaxLiability", Sm.GetParameter("AcNoForTaxLiability"));
            Sm.CmParam<String>(ref cm, "@AcNoForAccruedSalary", Sm.GetParameter("AcNoForAccruedSalary"));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

            return SQL.ToString();
        }

        private string GetJournalSQL07(ref MySqlCommand cm)
        {
            //07 : Social Security

            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.SSPCode ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestSSHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblEmpSSListHdr C On B.EmpSSListDocNo=C.DocNo ");
            SQL.AppendLine("Where A.DocNo=@Param;");

            var SSPCode = Sm.GetValue(SQL.ToString(), TxtVoucherRequestDocNo.Text);

            SQL.Length = 0;

            SQL.AppendLine("Select C.ParValue As AcNo, ");
            SQL.AppendLine("IfNull(B.Amt, 0) As DAmt, ");
            SQL.AppendLine("0 As CAmt ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestSSHdr B On A.DocNo=B.VoucherRequestDocNo ");
            if (Sm.CompareStr(SSPCode, Sm.GetParameter("SSPCodeForHealth")))
                SQL.AppendLine("Left Join TblParameter C On C.ParCode='AcNoForPremiumDebtSSHealth' And C.ParValue Is Not Null ");
            if (Sm.CompareStr(SSPCode, Sm.GetParameter("SSPCodeForEmployment")))
                SQL.AppendLine("Left Join TblParameter C On C.ParCode='AcNoForPremiumDebtSSEmployment' And C.ParValue Is Not Null ");
            if (Sm.CompareStr(SSPCode, Sm.GetParameter("SSPCodeForPension")))
                SQL.AppendLine("Left Join TblParameter C On C.ParCode='AcNoForPremiumDebtSSPension' And C.ParValue Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("0 As DAmt, ");
            SQL.AppendLine("IfNull(B.Amt, 0) As CAmt ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestSSHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblBankAccount C On C.BankAcCode=@BankAcCode And C.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");

            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

            return SQL.ToString();
        }

        private string GetJournalSQL09(ref MySqlCommand cm)
        {
            //09 : Advance Payment

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(C.ParValue, B.EmpCode) As AcNo, ");
            SQL.AppendLine("IfNull(B.Amt, 0) As DAmt, ");
            SQL.AppendLine("0.00 As CAmt ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblAdvancePaymentHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblParameter C On C.ParCode='AcNoForAdvancePayment' And C.ParValue Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("0 As DAmt, ");
            SQL.AppendLine("IfNull(B.Amt, 0) As CAmt ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblAdvancePaymentHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblBankAccount C On C.BankAcCode=@BankAcCode And C.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");

            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

            return SQL.ToString();
        }

        private string GetJournalSQL11(ref MySqlCommand cm)
        {
            //11 : Raw Material

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(E.ParValue, D.VdCode) As AcNo, ");
            SQL.AppendLine("IfNull(A.Amt, 0) As DAmt, ");
            SQL.AppendLine("0.00 As CAmt ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceRawMaterialHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblRawMaterialVerify C On B.RawMaterialVerifyDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr D On C.LegalDocVerifyDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblParameter E On E.ParCode='VendorAcNoAP' And E.ParValue Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("0.00 As DAmt, ");
            SQL.AppendLine("IfNull(A.Amt, 0) As CAmt ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceRawMaterialHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblBankAccount C On C.BankAcCode=@BankAcCode And C.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo ");

            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

            return SQL.ToString();
        }

        private string GetJournalSQL13(ref MySqlCommand cm)
        {
            //13 : RHA

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ParValue As AcNo, ");
            SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("    Select Amt From TblCurrencyRate ");
            SQL.AppendLine("    Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("), 0.00) End * ");
            SQL.AppendLine("A.Amt As DAmt, ");
            SQL.AppendLine("0.00 As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblParameter B On B.ParCode='AcNoForRHAPayable' And B.ParValue Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.COAAcNo As AcNo, ");
            SQL.AppendLine("0.00 As DAmt, ");
            SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("    Select Amt From TblCurrencyRate ");
            SQL.AppendLine("    Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("), 0.00) End * ");
            SQL.AppendLine("A.Amt As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

            return SQL.ToString();
        }

        private string GetJournalSQL14(ref MySqlCommand cm)
        {
            //14 : Returning AP Downpayment

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(C.ParValue, B.VdCode) As AcNo, ");
            SQL.AppendLine("0.00 As DAmt, ");
            SQL.AppendLine("A.Amt As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblReturnAPDownpayment B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblParameter C On C.ParCode='VendorAcNoDownPayment' And C.ParValue Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.COAAcNo As AcNo, ");
            SQL.AppendLine("A.Amt As DAmt, ");
            SQL.AppendLine("0.00 As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            return SQL.ToString();
        }

        private string GetJournalSQL15(ref MySqlCommand cm)
        {
            // VAT
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.AcNo1 As AcNo, 0.00 As DAmt, A.Amt As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestPPNHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblTaxGrp C On B.TaxGrpCode=C.TaxGrpCode And C.AcNo1 Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcType='D' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.COAAcNo As AcNo, A.Amt As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcType='D' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select C.AcNo2 As AcNo, A.Amt As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestPPNHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblTaxGrp C On B.TaxGrpCode=C.TaxGrpCode And C.AcNo2 Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcType='C' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.COAAcNo As AcNo, 0.00 As DAmt, A.Amt As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcType='C' ");

            return SQL.ToString();
        }

        private string GetJournalSQL17(ref MySqlCommand cm)
        {
            //Outgoing Payment Additional Amount

            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.AcNo, C.DAmt, C.CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentHdr B On B.VoucherRequestDocNo2 Is Not Null And A.VoucherRequestDocNo=B.VoucherRequestDocNo2 ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentDtl2 C On B.DocNo=C.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            return SQL.ToString();
        }

        private string GetJournalSQL18(ref MySqlCommand cm)
        {
            //Incoming Payment Additional Amount

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Case When Left(C.AcNo, Length(E.ParValue))=E.ParValue Then ");
            SQL.AppendLine("    D.COAAcNo ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    C.AcNo ");
            SQL.AppendLine("End As AcNo, ");
            SQL.AppendLine("C.DAmt, C.CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr B On B.VoucherRequestDocNo2 Is Not Null And A.VoucherRequestDocNo=B.VoucherRequestDocNo2 ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl2 C On B.DocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblBankAccount D On A.BankAcCode=D.BankAcCode And D.COAAcNo Is Not Null ");
            SQL.AppendLine("Inner Join TblParameter E On E.ParCode='CustomerAcNoAR' And E.ParValue Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            return SQL.ToString();
        }

        private string GetJournalSQL19(ref MySqlCommand cm)
        {
            //AP Downpayment Additional Amount

            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.AcNo, C.DAmt, C.CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblAPDownpayment B On B.VoucherRequestDocNo2 Is Not Null And A.VoucherRequestDocNo=B.VoucherRequestDocNo2 ");
            SQL.AppendLine("Inner Join TblAPDownpaymentDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            return SQL.ToString();
        }

        private string GetJournalSQL20(ref MySqlCommand cm)
        {
            //AR Downpayment Additional Amount

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Case When Left(C.AcNo, Length(E.ParValue))=E.ParValue Then ");
            SQL.AppendLine("    D.COAAcNo ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    C.AcNo ");
            SQL.AppendLine("End As AcNo, ");
            SQL.AppendLine("C.DAmt, C.CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblARDownpayment B On B.VoucherRequestDocNo2 Is Not Null And A.VoucherRequestDocNo=B.VoucherRequestDocNo2 ");
            SQL.AppendLine("Inner Join TblARDownpaymentDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblBankAccount D On A.BankAcCode=D.BankAcCode And D.COAAcNo Is Not Null ");
            SQL.AppendLine("Inner Join TblParameter E On E.ParCode='CustomerAcNoAR' And E.ParValue Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            return SQL.ToString();
        }

        private string GetJournalSQL21(ref MySqlCommand cm)
        {
            // Voucher Request Tax
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.AcNo1 As AcNo, A.Amt As DAmt, 0 As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestTax B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblTax C On B.TaxCode=C.TaxCode And C.AcNo1 Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.COAAcNo As AcNo, 0 As DAmt, A.Amt As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            return SQL.ToString();
        }

        private string GetJournalSQL22(ref MySqlCommand cm)
        {
            //22 : Incoming Payment (CBD)
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select B.COAAcNo As AcNo, ");
            //SQL.AppendLine("IfNull(A.Amt, 0) As DAmt, ");
            //SQL.AppendLine("0 As CAmt ");
            //SQL.AppendLine("From TblVoucherHdr A ");
            //SQL.AppendLine("Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            //SQL.AppendLine("Where A.DocNo=@DocNo ");

            SQL.AppendLine("Select E.COAAcNo As AcNo, ");
            SQL.AppendLine("Sum(TotalTax) As DAmt, ");
            SQL.AppendLine("0.00 As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A  ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo  ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl C On B.DocNo=C.DocNo  ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr D On C.InvoiceDocNo=D.DocNo  ");
            SQL.AppendLine("Inner Join TblBankAccount E On A.BankAcCode=E.BankAcCode And E.COAAcNo Is Not Null  ");
            SQL.AppendLine("Where A.DocNo=@DocNo  ");
            SQL.AppendLine("Group By E.COAAcNo ");
            SQL.AppendLine("Union All  ");
            SQL.AppendLine("Select T1.AcNo, ");
            SQL.AppendLine("Case When (T1.Amt-IfNull(T2.Amt, 0.00))>=0.00 Then (T1.Amt-IfNull(T2.Amt, 0.00)) Else 0.00 End As DAmt,  ");
            SQL.AppendLine("Case When (T1.Amt-IfNull(T2.Amt, 0.00))>=0.00 Then 0.00 Else Abs((T1.Amt-IfNull(T2.Amt, 0.00))) End As CAmt  ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select E.COAAcNo As AcNo,  ");
            SQL.AppendLine("    Sum(D.TotalAmt) As Amt  ");
            SQL.AppendLine("    From TblVoucherHdr A  ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo  ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl C On B.DocNo=C.DocNo  ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceHdr D On C.InvoiceDocNo=D.DocNo  ");
            SQL.AppendLine("    Inner Join TblBankAccount E On A.BankAcCode=E.BankAcCode And E.COAAcNo Is Not Null  ");
            SQL.AppendLine("    Where A.DocNo=@DocNo  ");
            SQL.AppendLine("    Group By E.COAAcNo ");
            SQL.AppendLine(") T1  ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Sum(E.CAmt-E.DAmt) As Amt  ");
            SQL.AppendLine("    From TblVoucherHdr A  ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo  ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl C On B.DocNo=C.DocNo  ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceHdr D On C.InvoiceDocNo=D.DocNo  ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceDtl2 E On D.DocNo=E.DocNo And E.AcInd='N'  ");
            SQL.AppendLine("    Where A.DocNo=@DocNo  ");
            SQL.AppendLine(") T2 On 0=0  ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select E.ParValue As AcNo, ");
            SQL.AppendLine("0 As DAmt, ");
            SQL.AppendLine("Sum(TotalTax) As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr D On C.InvoiceDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblParameter E On E.ParCode='AcNoForVATOut' And E.ParValue is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Group By E.ParValue ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select E.AcNo, ");
            SQL.AppendLine("Case When Sum(E.CAmt-E.DAmt)>=0 Then 0 Else Abs(Sum(E.CAmt-E.DAmt)) End As DAmt, ");
            SQL.AppendLine("Case When Sum(E.CAmt-E.DAmt)>=0 Then Sum(E.CAmt-E.DAmt) Else 0 End As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr D On C.InvoiceDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl2 E On D.DocNo=E.DocNo And E.AcInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Group By E.AcNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T1.AcNo, ");
            SQL.AppendLine("Case When (T1.Amt-IfNull(T2.Amt, 0))>=0 Then 0 Else Abs((T1.Amt-IfNull(T2.Amt, 0))) End As DAmt, ");
            SQL.AppendLine("Case When (T1.Amt-IfNull(T2.Amt, 0))>=0 Then (T1.Amt-IfNull(T2.Amt, 0)) Else 0 End As CAmt ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select Concat(E.ParValue, D.CtCode) As AcNo, ");
            SQL.AppendLine("    Sum(D.TotalAmt) As Amt ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceHdr D On C.InvoiceDocNo=D.DocNo ");
            SQL.AppendLine("    Inner Join TblParameter E On E.ParCode='CustomerAcNoDownPayment' And E.ParValue Is Not Null ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Group By E.ParValue, D.CtCode ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select Sum(E.CAmt-E.DAmt) As Amt ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceHdr D On C.InvoiceDocNo=D.DocNo ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceDtl2 E On D.DocNo=E.DocNo And E.AcInd='N' ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine(") T2 On 0=0 ");

            return SQL.ToString();
        }

        private string GetJournalSQL51(ref MySqlCommand cm)
        {
            //51 : Returning AR Downpayment

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(C.ParValue, B.CtCode) As AcNo, ");
            SQL.AppendLine("IfNull(A.Amt, 0.00) As DAmt, ");
            SQL.AppendLine("0.00 As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblReturnARDownpayment B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblParameter C On C.ParCode='CustomerAcNoDownPayment' And C.ParValue Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.COAAcNo As AcNo, ");
            SQL.AppendLine("0.00 As DAmt, ");
            SQL.AppendLine("IfNull(A.Amt, 0.00) As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            return SQL.ToString();
        }

        private string GetJournalSQL54(ref MySqlCommand cm)
        {
            //54 : Bonus

            var SQL = new StringBuilder();

            SQL.AppendLine("Select D.AcNo2 As AcNo, ");
            SQL.AppendLine("IfNull(C.Amt, 0.00) As DAmt, ");
            SQL.AppendLine("0.00 As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblBonusHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblBonusDtl C On B.DocNo=C.DocNo And C.DeptCode Is Not Null ");
            SQL.AppendLine("Inner Join TblDepartment D On C.DeptCode=D.DeptCode And D.AcNo2 Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.COAAcNo As AcNo, ");
            SQL.AppendLine("0.00 As DAmt, ");
            SQL.AppendLine("IfNull(A.Amt, 0.00) As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            return SQL.ToString();
        }


        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsCancelledDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;
           
            var cml = new List<MySqlCommand>();

            cml.Add(CancelVoucher());

            if (IsVoucherForAPDownpayment()) 
                cml.Add(SaveVendorDeposit(TxtDocNo.Text));

            if (IsVoucherForARDownpayment())
                cml.Add(SaveCustomerDeposit(TxtDocNo.Text));

            if (IsVoucherForReturnAPDownpayment())
                cml.Add(SaveVendorDepositReturn(TxtDocNo.Text));

            if (IsVoucherForReturnARDownpayment())
                cml.Add(SaveCustomerDepositReturn(TxtDocNo.Text));

            if (mIsAutoJournalActived && IsDocTypeValid()) cml.Add(SaveJournal2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsVoucherNotCancelled() ||
                IsDataCancelledAlready() ||
                IsVoucherAlreadyProcessToVJ() ||
                IsCustomerDownpaymentNotValid() ||
                IsVRTaxInvalid() ||
                IsVoucherDocTypeNotValid();
        }

        private bool IsVoucherDocTypeNotValid()
        {
            if (!mIsAutoJournalActived) return false;
            if (!IsDocTypeValid())
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid voucher's document type.");
                return true;
            }
            return false;
        }

        private bool IsVRTaxInvalid()
        {
            if (Sm.GetValue("Select DocType From TblVoucherRequestHdr Where DocNo=@Param;", TxtVoucherRequestDocNo.Text) != "21") return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblVoucherRequestTax T ");
            SQL.AppendLine("Where T.VoucherRequestDocNo=@Param ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblVoucherRequestPPNHdr A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestPPNDtl B ");
            SQL.AppendLine("        On A.DocNo=B.DocNo ");
            SQL.AppendLine("        And B.DocType='3' ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.Status In ('O', 'A') ");
            SQL.AppendLine("    And B.DocNoIn=T.DocNo ");
            SQL.AppendLine(");");

            return Sm.IsDataExist(
                SQL.ToString(),
                TxtVoucherRequestDocNo.Text,
                "Invalid voucher request." + Environment.NewLine +
                "The voucher request tax already processed to voucher request vat."
                );
        }

        private bool IsVoucherAlreadyProcessToVJ()
        {
            if (Sm.IsDataExist(
                "Select 1 From TblVoucherHdr " +
                "Where DocNo=@Param And VoucherJournalDocNo Is Not Null;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This voucher already processed to journal voucher.");
                return true;
            }
            else
                return false;
        }

        private bool IsCustomerDownpaymentNotValid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocNo, T4.CtCode, T2.CurCode, T1.Amt ");
            SQL.AppendLine("From TblVoucherHdr T1 ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr T2 On T1.VoucherRequestDocNo=T2.DocNo ");
            SQL.AppendLine("Inner Join TblARDownpayment T3 On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblSOHdr T4 On  T3.SODocNo=T4.DocNo ");
            SQL.AppendLine("Where T1.CancelInd='N'  ");
            SQL.AppendLine("And T1.DocNo=@Param ");
            SQL.AppendLine("And Exists(Select T.DocNo From TblARDownpayment T Where T.VoucherRequestDocNo=T1.VoucherRequestDocNo) ");
            SQL.AppendLine("And Exists(Select T.CtCode From TblCustomerDepositSummary T Where T.CtCode=T4.CtCode And T.CurCode=T2.CurCode And T.Amt<T1.Amt);");

            return Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text, "Insufficient amount.");
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand() 
            {
                CommandText = 
                    "Select DocNo From TblVoucherHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled .");
                return true;
            }

            return false;
        }

        private bool IsVoucherNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this voucher.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelVoucher()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherHdr Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DocNo=@DocNo;");

            SQL.AppendLine("Update TblVoucherRequestHdr Set VoucherDocNo=Null Where DocNo=@VoucherRequestDocNo;");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblVoucherHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblVoucherHdr Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblVoucherHdr Where DocNo=@DocNo);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));

            return cm;
        }

        private void SaveData3(string DocNo)
        {
            var cml = new List<MySqlCommand>();

            cml.Add(SaveRawMaterial2(DocNo));

            Sm.ExecCommands(cml);
        }

        private MySqlCommand SaveRawMaterial2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.Append("Insert Into " + mCustomerDatabase + ".TblRawMaterialHdr ");
            SQL.Append("(DocNo, CancelInd, ProcessInd, DocDt, QueueNo, VehicleRegNo, TTName, ");
            SQL.Append("VdCode, VdName, VdCode1, VdName1, VdCode2, VdName2, TIN, TaxInd, ");
            SQL.Append("EmpName1, EmpName2, EmpName3, EmpName4, LegalDoc, ");
            SQL.Append("Amt, Tax, TransportCost, RoundingValue, ");
            SQL.Append("CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.Append("Select DocNo, 'Y', ProcessInd, DocDt, QueueNo, VehicleRegNo, TTName, ");
            SQL.Append("VdCode, VdName, VdCode1, VdName1, VdCode2, VdName2, TIN, TaxInd, ");
            SQL.Append("EmpName1, EmpName2, EmpName3, EmpName4, LegalDoc, ");
            SQL.Append("Amt, Tax, TransportCost, RoundingValue, ");
            SQL.Append("CreateBy, CreateDt, LastUpBy, LastUpDt ");
            SQL.Append("From " + mCustomerDatabase + ".TblRawMaterialHdr ");
            SQL.Append("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.Append("Insert Into " + mCustomerDatabase + ".TblRawMaterialDtl ");
            SQL.Append("(DocNo, CancelInd, DNo, ");
            SQL.Append("Bin, ItCode, ItName, Length, Width, Height, Diameter, ");
            SQL.Append("Qty1, Qty2, PGCode, PGName, CurCode, UPrice, ");
            SQL.Append("CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.Append("Select DocNo, 'Y', DNo, ");
            SQL.Append("Bin, ItCode, ItName, Length, Width, Height, Diameter, ");
            SQL.Append("Qty1, Qty2, PGCode, PGName, CurCode, UPrice, ");
            SQL.Append("CreateBy, CreateDt, LastUpBy, LastUpDt ");
            SQL.Append("From " + mCustomerDatabase + ".TblRawMaterialDtl ");
            SQL.Append("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowVoucherHdr(DocNo);
                ShowVoucherDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowVoucherHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.*, B.OptDesc As DocTypeDesc, ");
            SQL.AppendLine("IfNull(IfNull(J.VdName, IfNull(N.VdName, R.VdName)), F.VdName) As VdName, ");
            SQL.AppendLine("E.VdInvNo, ");
            SQL.AppendLine("IfNull(IfNull(I.QueueNo, M.QueueNo), Q.QueueNo) As QueueNo, ");
            SQL.AppendLine("C.CurCode ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Left Join TblOption B On A.DocType=B.OptCode and B.OptCat='VoucherDocType' ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr C On A.VoucherRequestDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblOutgoingPaymentHdr D On C.DocNo=D.VoucherRequestDocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.DocNo, Group_Concat(T.VdInvNo Separator ', ') As VdInvNo ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Distinct T1.DocNo, T2.VdInvNo ");
            SQL.AppendLine("        From TblOutgoingPaymentDtl T1 ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T2 On T1.InvoiceDocNo=T2.DocNo And T2.VdInvNo Is Not Null ");
            SQL.AppendLine("    ) T Group By T.DocNo ");
            SQL.AppendLine(") E On D.DocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblVendor F On D.VdCode=F.VdCode ");

            SQL.AppendLine("Left Join TblPurchaseInvoiceRawMaterialHdr G On C.DocNo=G.VoucherRequestDocNo ");
            SQL.AppendLine("Left Join TblRawMaterialVerify H On G.RawMaterialVerifyDocNo=H.DocNo ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr I On H.LegalDocVerifyDocNo=I.DocNo ");
            SQL.AppendLine("Left Join TblVendor J On I.VdCode=J.VdCode ");

            SQL.AppendLine("Left Join TblPurchaseInvoiceRawMaterialHdr K On C.DocNo=K.VoucherRequestDocNo2 And K.VoucherRequestDocNo2 Is Not Null ");
            SQL.AppendLine("Left Join TblRawMaterialVerify L On K.RawMaterialVerifyDocNo=L.DocNo ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr M On L.LegalDocVerifyDocNo=M.DocNo ");
            SQL.AppendLine("Left Join TblVendor N On M.VdCode=N.VdCode ");

            SQL.AppendLine("Left Join TblPurchaseInvoiceRawMaterialHdr O On C.DocNo=O.VoucherRequestDocNo3 And O.VoucherRequestDocNo3 Is Not Null ");
            SQL.AppendLine("Left Join TblRawMaterialVerify P On O.RawMaterialVerifyDocNo=P.DocNo ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr Q On P.LegalDocVerifyDocNo=Q.DocNo ");
            SQL.AppendLine("Left Join TblVendor R On Q.VdCode=R.VdCode ");

            SQL.AppendLine("Where A.DocNo=@DocNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelInd", "LocalDocNo", "VoucherRequestDocNo", "DocTypeDesc", 
                        
                        //6-10
                        "VdName", "VdInvNo", "QueueNo", "AcType", "BankAcCode", 
                        
                        //11-15
                        "PaymentType", "BankCode", "GiroNo", "OpeningDt", "DueDt", 
                        
                        //16-19
                        "PIC", "CurCode", "Amt", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtDocType.EditValue = Sm.DrStr(dr, c[5]);
                        TxtVdCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtVdInvNo.EditValue = Sm.DrStr(dr, c[7]);
                        TxtQueueNo.EditValue = Sm.DrStr(dr, c[8]);
                        Sm.SetLue(LueAcType, Sm.DrStr(dr, c[9]));
                        Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[10]));
                        Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[11]));
                        Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[12]));
                        TxtGiroNo.EditValue = Sm.DrStr(dr, c[13]);
                        Sm.SetDte(DteOpeningDt, Sm.DrStr(dr, c[14]));
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[15]));
                        Sm.SetLue(LuePIC, Sm.DrStr(dr, c[16]));
                        TxtCurCode.EditValue = Sm.DrStr(dr, c[17]);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[19]);
                    }, true
                );
        }

        private void ShowVoucherDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select Description, Amt, Remark From TblVoucherDtl " +
                    "Where DocNo=@DocNo Order By DNo",
                    new string[] { "Description", "Amt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);

                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            string MenuCodeForDocWithMInd = Sm.GetParameter("MenuCodeForDocWithMInd");
            if (MenuCodeForDocWithMInd.Length > 0)
                mMInd =
                    MenuCodeForDocWithMInd.IndexOf("##" + mMenuCode + "##") != -1 ?
                        "Y" : "N";
            mIsUseMInd = Sm.GetParameter("IsUseMInd") == "Y";
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mVoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType");
        }

        private void SetParameter()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ParCode, ParValue From TblParameter ");
            SQL.AppendLine("Where ParCode In ( ");
            SQL.AppendLine("    'TransferVoucherRMToCustDB', 'ItCtRMPActual', 'ItCtRMPActual2', 'CustomerDatabase' ");
            SQL.AppendLine(") Order By ParCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                while (dr.Read())
                {
                    switch (Sm.DrStr(dr, c[0]))
                    {
                        case "TransferVoucherRMToCustDB":
                            mTransferVoucherRMToCustDB = Sm.DrStr(dr, c[1]) == "Y";
                            break;
                        case "ItCtRMPActual":
                            mItCtRMPActual = Sm.DrStr(dr, c[1]);
                            break;
                        case "ItCtRMPActual2":
                            mItCtRMPActual2 = Sm.DrStr(dr, c[1]);
                            break;
                        case "CustomerDatabase":
                            mCustomerDatabase = Sm.DrStr(dr, c[1]);
                            break;
                    }
                }
                dr.Close();
            }
        }

        internal void ShowVoucherRequestInfo(string DocNo)
        {
            try
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtVoucherRequestDocNo, TxtDocType, TxtVdCode, TxtVdInvNo, TxtQueueNo, 
                    LueAcType, LueBankAcCode, LuePaymentType, LueBankCode, TxtGiroNo, 
                    DteOpeningDt, DteDueDt, LuePIC, TxtCurCode 
                });
                Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtAmt }, 0);
                ClearGrd();

                ShowVoucherRequestHdr(DocNo);
                ShowVoucherRequestDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void ShowVoucherRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.OptDesc As DocTypeDesc, ");
            SQL.AppendLine("IfNull(IfNull(I.VdName, IfNull(M.VdName, Q.VdName)), E.VdName) As VdName, ");
            SQL.AppendLine("D.VdInvNo, IfNull(IfNull(H.QueueNo, L.QueueNo), P.QueueNo) as QueueNo, A.CurCode, A.Amt, ");
            SQL.AppendLine("A.AcType, A.BankAcCode, A.PaymentType, A.GiroNo, A.BankCode, A.OpeningDt, A.DueDt, A.PIC, A.Remark ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblOption B On A.DocType=B.OptCode And B.OptCat='VoucherDocType' ");
            SQL.AppendLine("Left Join TblOutgoingPaymentHdr C On A.DocNo=C.VoucherRequestDocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.DocNo, Group_Concat(T.VdInvNo Separator ', ') As VdInvNo ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Distinct T1.DocNo, T2.VdInvNo ");
            SQL.AppendLine("        From TblOutgoingPaymentDtl T1 ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T2 On T1.InvoiceDocNo=T2.DocNo And T2.VdInvNo Is Not Null ");
            SQL.AppendLine("    ) T Group By T.DocNo ");
            SQL.AppendLine(") D On C.DocNo=D.DocNo ");
            SQL.AppendLine("Left Join TblVendor E On C.VdCode=E.VdCode ");

            SQL.AppendLine("Left Join TblPurchaseInvoiceRawMaterialHdr F On A.DocNo=F.VoucherRequestDocNo ");
            SQL.AppendLine("Left Join TblRawMaterialVerify G On F.RawMaterialVerifyDocNo=G.DocNo ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr H On G.LegalDocVerifyDocNo=H.DocNo ");
            SQL.AppendLine("Left Join TblVendor I On H.VdCode=I.VdCode ");

            SQL.AppendLine("Left Join TblPurchaseInvoiceRawMaterialHdr J On A.DocNo=J.VoucherRequestDocNo2 And J.VoucherRequestDocNo2 Is Not Null ");
            SQL.AppendLine("Left Join TblRawMaterialVerify K On J.RawMaterialVerifyDocNo=K.DocNo ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr L On K.LegalDocVerifyDocNo=L.DocNo ");
            SQL.AppendLine("Left Join TblVendor M On L.VdCode=M.VdCode ");

            SQL.AppendLine("Left Join TblPurchaseInvoiceRawMaterialHdr N On A.DocNo=N.VoucherRequestDocNo3 And N.VoucherRequestDocNo3 Is Not Null ");
            SQL.AppendLine("Left Join TblRawMaterialVerify O On N.RawMaterialVerifyDocNo=O.DocNo ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr P On O.LegalDocVerifyDocNo=P.DocNo ");
            SQL.AppendLine("Left Join TblVendor Q On P.VdCode=Q.VdCode ");


            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("And A.DocNo Not In (Select VoucherRequestDocNo From TblVoucherHdr Where CancelInd='N') ");
            SQL.AppendLine("And A.DocNo=@DocNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                 ref cm,
                 SQL.ToString(),
                 new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocTypeDesc", "VdName", "VdInvNo", "QueueNo", "AcType", 
                    
                    //6-10
                    "BankAcCode", "PaymentType", "GiroNo", "BankCode", "OpeningDt", 
                    
                    //11-15
                    "DueDt", "PIC", "CurCode", "Amt", "Remark"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     TxtDocType.EditValue = Sm.DrStr(dr, c[1]);
                     TxtVdCode.EditValue = Sm.DrStr(dr, c[2]);
                     TxtVdInvNo.EditValue = Sm.DrStr(dr, c[3]);
                     TxtQueueNo.EditValue = Sm.DrStr(dr, c[4]);
                     Sm.SetLue(LueAcType, Sm.DrStr(dr, c[5]));
                     Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[6]));
                     Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[7]));
                     TxtGiroNo.EditValue = Sm.DrStr(dr, c[8]);
                     Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[9]));
                     Sm.SetDte(DteOpeningDt, Sm.DrStr(dr, c[10]));
                     Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[11]));
                     Sm.SetLue(LuePIC, Sm.DrStr(dr, c[12]));
                     TxtCurCode.EditValue = Sm.DrStr(dr, c[13]);
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                 }, true
             );
        }

        private void ShowVoucherRequestDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select Description, Amt, Remark From TblVoucherRequestDtl " +
                    "Where DocNo=@DocNo Order By DNo",
                    new string[] 
                    { 
                        "Description", "Amt", "Remark",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Button Event


        #endregion

        #region Misc Control Event

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(Sl.SetLueAcType));
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueBankCode, TxtGiroNo, DteOpeningDt, DteDueDt
            });

            if (Sm.GetLue(LuePaymentType) == "C")
            {
                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteOpeningDt, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
            else if (Sm.GetLue(LuePaymentType) == "B")
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteOpeningDt, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
            else if (Sm.GetLue(LuePaymentType) == "G")
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteOpeningDt, false);
                Sm.SetControlReadOnly(DteDueDt, false);
            }
            else if (Sm.GetLue(LuePaymentType) == "K")
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteOpeningDt, false);
                Sm.SetControlReadOnly(DteDueDt, false);
            }
            else
            {
                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteOpeningDt, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
            //Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

            //Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDt });

            //if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            //{
            //    Sm.SetControlReadOnly(LueBankCode, false);
            //    Sm.SetControlReadOnly(TxtGiroNo, true);
            //    Sm.SetControlReadOnly(DteDueDt, true);
            //    return;
            //}

            //if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            //{
            //    Sm.SetControlReadOnly(LueBankCode, true);
            //    Sm.SetControlReadOnly(TxtGiroNo, false);
            //    Sm.SetControlReadOnly(DteDueDt, false);
            //    return;
            //}

            //Sm.SetControlReadOnly(LueBankCode, true);
            //Sm.SetControlReadOnly(TxtGiroNo, true);
            //Sm.SetControlReadOnly(DteDueDt, true);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void TxtGiroNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtGiroNo);
        }

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }

        #endregion

        #endregion

        #region Report Class

        class VoucherHdr
        {
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CancelInd { get; set; }
            public string DocType { get; set; }
            public string AcType { get; set; }
            public string VoucherDocNo { get; set; }
            public string VoucherDocDt { get; set; }
            public string BankAcc { get; set; }
            public string PaymentType { get; set; }
            public string GiroNo { get; set; }
            public string GiroBankName { get; set; }
            public string GiroDueDt { get; set; }
            public string PICName { get; set; }
            public decimal AmtHdr { get; set; }
            public string Terbilang { get; set; }
            public string RemarkHdr { get; set; }
            public string CompanyLogo { get; set; }
            public string PrintBy { get; set; }
            public string CurCode { get; set; }
            public decimal DocEnclosure { get; set; }
            public string PaymentUser { get; set; }
            public string Department { get; set; }
        }

        class VoucherDtl
        {
            public string DNo { get; set; }
            public string Description { get; set; }
            public decimal Amt { get; set; }
            public string Remark { get; set; }
        }

        #endregion

        #region Additional Class

        class RawMaterialDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string CancelInd { get; set; }
            public string Bin { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Length { get; set; }
            public decimal Width { get; set; }
            public decimal Height { get; set; }
            public decimal Diameter { get; set; }
            public decimal Qty1 { get; set; }
            public decimal Qty2 { get; set; }
            public string PGCode { get; set; }
            public string PGName { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice { get; set; }
        }

        #endregion

        private void BtnVoucherRequest_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucher2Dlg(this, mMInd));
        }

        private void BtnPurchaseInvoiceRawMaterialDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher request#", false))
            {
                try
                {
                    var DocNo = Sm.GetValue(
                        "Select DocNo From TblPurchaseInvoiceRawMaterialHdr " +
                        "Where (VoucherRequestDocNo=@Param Or " +
                        "IfNull(VoucherRequestDocNo2, '')=@Param Or " +
                        "IfNull(VoucherRequestDocNo3, '')=@Param);", 
                        TxtVoucherRequestDocNo.Text);

                    if (DocNo.Length > 0)
                    {
                        var f = new FrmPurchaseInvoiceRawMaterial2(mMenuCode);
                        f.Tag = "***";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = DocNo;
                        f.ShowDialog();
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "Invalid document.");
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

    }
}
