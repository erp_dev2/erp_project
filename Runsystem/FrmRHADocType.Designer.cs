﻿namespace RunSystem
{
    partial class FrmRHADocType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtMIndex = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtDocTypeName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocTypeCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMIndex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocTypeName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocTypeCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtMIndex);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtDocTypeName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocTypeCode);
            this.panel2.Controls.Add(this.label1);
            // 
            // TxtMIndex
            // 
            this.TxtMIndex.EnterMoveNextControl = true;
            this.TxtMIndex.Location = new System.Drawing.Point(121, 74);
            this.TxtMIndex.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMIndex.Name = "TxtMIndex";
            this.TxtMIndex.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMIndex.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMIndex.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMIndex.Properties.Appearance.Options.UseFont = true;
            this.TxtMIndex.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMIndex.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMIndex.Size = new System.Drawing.Size(78, 20);
            this.TxtMIndex.TabIndex = 14;
            this.TxtMIndex.Validated += new System.EventHandler(this.TxtMIndex_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(27, 77);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 14);
            this.label3.TabIndex = 13;
            this.label3.Text = "Multiplier Index";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocTypeName
            // 
            this.TxtDocTypeName.EnterMoveNextControl = true;
            this.TxtDocTypeName.Location = new System.Drawing.Point(121, 53);
            this.TxtDocTypeName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocTypeName.Name = "TxtDocTypeName";
            this.TxtDocTypeName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocTypeName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocTypeName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocTypeName.Properties.Appearance.Options.UseFont = true;
            this.TxtDocTypeName.Properties.MaxLength = 80;
            this.TxtDocTypeName.Size = new System.Drawing.Size(319, 20);
            this.TxtDocTypeName.TabIndex = 12;
            this.TxtDocTypeName.Validated += new System.EventHandler(this.TxtDocTypeName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(78, 56);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocTypeCode
            // 
            this.TxtDocTypeCode.EnterMoveNextControl = true;
            this.TxtDocTypeCode.Location = new System.Drawing.Point(121, 32);
            this.TxtDocTypeCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocTypeCode.Name = "TxtDocTypeCode";
            this.TxtDocTypeCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocTypeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocTypeCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocTypeCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDocTypeCode.Properties.MaxLength = 16;
            this.TxtDocTypeCode.Size = new System.Drawing.Size(126, 20);
            this.TxtDocTypeCode.TabIndex = 10;
            this.TxtDocTypeCode.Validated += new System.EventHandler(this.TxtDocTypeCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(81, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmRHADocType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmRHADocType";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMIndex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocTypeName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocTypeCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtMIndex;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtDocTypeName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocTypeCode;
        private System.Windows.Forms.Label label1;
    }
}