﻿#region Update
/*
    19/03/2019 [MEY] Tambah informasi Active Indicator
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmItemBOQFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmItemBOQ mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmItemBOQFind(FrmItemBOQ FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }

        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Item BOQ#", 
                    "Item BOQ Name",
                    "Active",
                    "Parent",
                    "Level",
                    
                    //6-10
                    "Created"+Environment.NewLine+"By",   
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By",
                    "Last"+Environment.NewLine+"Updated Date",

                    //11
                    "Last"+Environment.NewLine+"Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 300, 80, 200, 60, 
                    
                    //6-10
                    100, 100, 100, 100, 100,

                    //11
                    100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 7, 10 });
            Sm.GrdFormatTime(Grd1, new int[] { 8, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtItBOQCode.Text, new string[] { "A.ItBOQCode", "A.ItBOQName" });
                
                SQL.AppendLine("Select A.ItBOQCode, A.ItBOQName, A.ActInd, C.ItBOQName As ParentAcDesc, ");
                SQL.AppendLine("A.Level, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblItemBOQ A ");
                SQL.AppendLine("Left Join TblItemBOQ C on A.Parent = C.ItBOQCode ");
                SQL.AppendLine(Filter + " Order By A.ItBOQCode; ");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[]
                    {
                        //0
                        "ItBOQCode",

                        //1-5
                        "ItBOQName",
                        "ActInd",
                        "ParentAcDesc",
                        "Level",
                        "CreateBy",

                        //6-8
                        "CreateDt", 
                        "LastUpBy", 
                        "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 8);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtItBOQCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItBOQCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item BOQ");
        }

        #endregion

        #endregion

    }
}
