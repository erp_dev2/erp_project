﻿#region Update
/*
  27/02/2023 [MYA/MNET] BUG: Saat find doc Voucher Request for External Payroll
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestExternalPayrollFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmVoucherRequestExternalPayroll mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestExternalPayrollFind(FrmVoucherRequestExternalPayroll FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' ELSE '' END AS StatusDesc, ");
            SQL.AppendLine("C.DeptName, D.BankName, G.ItCode, G.ItName, B.Amt, IfNull(B.Remark, A.Remark) Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("FROM TblVoucherRequestExternalPayrollHdr A ");
            SQL.AppendLine("INNER JOIN TblVoucherRequestExternalPayrollDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    AND (A.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            SQL.AppendLine("INNER JOIN TblDepartment C ON A.DeptCode = C.DeptCode ");
            SQL.AppendLine("INNER JOIN TblBank D ON A.BankCode = D.BankCode ");
            SQL.AppendLine("INNER JOIN TblDroppingRequestDtl E ON B.DroppingRequestDocNo = E.DocNo AND B.DroppingRequestDNo = E.DNo ");
            SQL.AppendLine("INNER JOIN TblProjectImplementationRBPHdr F ON E.PRBPDocNo = F.DocNo ");
            SQL.AppendLine("INNER JOIN TblItem G ON F.ResourceItCode = G.ItCode ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("Inner Join TblProjectImplementationHdr H On F.PRJIDocNo = H.DocNo ");
                SQL.AppendLine("INner Join TblSOContractRevisionHdr I On H.SOContractDocNo = I.DocNo ");
                SQL.AppendLine("Inner join TblSOCOntractHdr J On I.SOCDocNo = J.DocNo ");
                SQL.AppendLine("Inner Join TblBOQHdr K On J.BOQDocNo = K.DocNo ");
                SQL.AppendLine("INner Join TblLOPHdr L On K.LOPDocNo = L.DocNo ");
                SQL.AppendLine("        And (L.SiteCode Is Null Or ( ");
                SQL.AppendLine("            L.SiteCode Is Not Null ");
                SQL.AppendLine("            And Exists( ");
                SQL.AppendLine("                Select 1 From TblGroupSite ");
                SQL.AppendLine("                Where SiteCode=IfNull(L.SiteCode, '') ");
                SQL.AppendLine("                And GrpCode In ( ");
                SQL.AppendLine("                    Select GrpCode From TblUser ");
                SQL.AppendLine("                    Where UserCode=@UserCode ");
                SQL.AppendLine("                    ) ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        )) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel", 
                    "Status",
                    "Department",
                    
                    //6-10
                    "Bank",
                    "Item Code",
                    "Item Name",
                    "Amount",
                    "Remark",

                    //11-15
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date",
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date", 
                    
                    //16
                    "Last"+Environment.NewLine+"Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    160, 80, 60, 60, 80, 
                    
                    //6-10
                    130, 150, 200, 200, 300,

                    //11-15
                    100, 100, 100, 100, 100,

                    //16
                    100
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 12, 15 });
            Sm.GrdFormatTime(Grd1, new int[] { 13, 16 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 11, 12, 13, 14, 15, 16 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 11, 12, 13, 14, 15, 16 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtDroppingRequestDocNo.Text, "B.DroppingRequestDocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo",                            
                        //1-5
                        "DocDt", "CancelInd", "StatusDesc", "DeptName", "BankName",                            
                        //6-10
                        "ItCode", "ItName", "Amt", "Remark", "CreateBy", 
                        //11-13
                        "CreateDt", "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 13);
                    }, true, true, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9 });
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtDroppingRequestDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDroppingRequestDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Dropping#");
        }

        #endregion

        #endregion
    }
}
