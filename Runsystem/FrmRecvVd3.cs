﻿#region Update
/*
    17/07/2018 [WED] Vendor bisa diketik full
    17/07/2018 [WED] Stock Summary langsung diisi 0, di DO nya nggak ngapa apain Stock Summary
    21/07/2018 [TKG] gabungkan proses journal receiving dan do
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvVd3 : RunSystem.FrmBase15
    {
        #region Field

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, IsProcFormat = string.Empty,
            mHeaderDate = string.Empty,
            mDetailDate = string.Empty,
            compare1 = string.Empty,
            compare2 = string.Empty;
        private string
            mRecvVdMenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmRecvVd' Limit 1; "),
            mDODept2MenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmDODept2' Limit 1; "),
            mRecvVdEximDeptCode = string.Empty,
            mRecvVdEximCCCode = string.Empty,
            mRecvVdDocType = "01",
            mDODept2DocType = "14",
            mPODocTitle = string.Empty,
            mPODocAbbr = string.Empty,
            mIsPOSplitBasedOnTax = string.Empty,
            mEntCode = string.Empty,
            mMainCurCode = string.Empty,
            mRecvVdAutoBatchNoFormat = "1",
            mDODeptRequestBySource = string.Empty;
        internal int mNumberOfInventoryUomCode = 1;
        private int mPOWithTitleLength = 0, mPOWithoutTitleLength = 0;
        private bool
            mIsFilterLocalDocNo = false,
            mIsRecvVdSplitOutstandingPO = false,
            mIsRemarkForApprovalMandatory = false,
            mIsRecvVdApprovalBasedOnWhs = false,
            mIsAutoJournalActived = false,
            mIsRemarkForJournalMandatory = false,
            mIsEntityMandatory = false,
            mIsRecvVdLocalDocNoBasedOnMRLocalDocNo = false;
        internal bool
            mIsRecvVdNeedApproval = false,
            mIsAutoGenerateBatchNo = false,
            mIsAutoGenerateBatchNoEditable = false,
            mIsAutoGeneratePurchaseLocalDocNo = false,
            mIsItGrpCodeShow = false,
            mIsShowForeignName = false,
            mIsComparedToDetailDate = false,
            mIsRecvVdValidateByEntity = false,
            mIsSystemUseCostCenter = false,
            mIsFilterByCC = false;
        private List<LocalDocument> mlLocalDocument = null;
        iGCell fCell;
        bool fAccept;
        
        #endregion

        #region Constructor

        public FrmRecvVd3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmRecvVd3");
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                GetParameter();
                if (mIsSystemUseCostCenter) LblCCCode.ForeColor = Color.Red;

                SetGrd();
                Sl.SetLueVdCode(ref LueVdCode);
                SetLueLot(ref LueLot);
                SetLueBin(ref LueBin);
                LueLot.Visible = false;
                LueBin.Visible = false;
                mlLocalDocument = new List<LocalDocument>();
                if (mIsRemarkForJournalMandatory)
                    LblRemark.ForeColor = Color.Red;

                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                SetLueVdCode(ref LueVdCode, "");
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueEntCode(ref LueEntCode, string.Empty);
                Sl.SetLueCCCode(ref LueCCCode);
                //Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mIsFilterByCC ? "Y" : "N");
                LueEmpCode.Visible = false;

                SetFormControl(mState.Insert);

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 39;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "DNo",

                        //1-5
                        "Cancel", 
                        "Old Cancel", 
                        "Cancel Reason",
                        "", 
                        "PO#", 

                        //6-10
                        "PO"+Environment.NewLine+"DNo",
                        "Import",
                        "Item's Code", 
                        "Item's Name", 
                        "Batch#", 
                        
                        //11-15
                        "Source",
                        "Lot", 
                        "Bin", 
                        "Quantity"+Environment.NewLine+"(Purchase)", 
                        "UoM"+Environment.NewLine+"(Purchase)", 
                        
                        //16-20
                        "Quantity"+Environment.NewLine+"(Inventory)",
                        "UoM"+Environment.NewLine+"(Inventory)", 
                        "Quantity"+Environment.NewLine+"(Inventory 2)", 
                        "UoM"+Environment.NewLine+"(Inventory 2)", 
                        "Quantity"+Environment.NewLine+"(Inventory 3)", 
                        
                        //21-25
                        "UoM"+Environment.NewLine+"(Inventory)",
                        "Remark",
                        "PO's Remark",
                        "ItScCode",
                        "Sub-Category",

                        //26-30
                        "Status",
                        "",
                        "Document#-Source",
                        "Local#",
                        "Foreign Name",

                        //31-35
                        "PO Date",
                        "",
                        "AssetCode",
                        "Asset",
                        "EmpCode",

                        //36-38
                        "Request By",
                        "COA",
                        "Cost Category",
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        50, 0, 120, 20, 130, 
                        
                        //6-10
                        0, 60, 100, 200, 150,  
                        
                        //11-15
                        150, 130, 130, 80, 80, 
                        
                        //16-20
                        100, 100, 100, 100, 100, 
            
                        //21-25
                        100, 200, 250, 100, 150, 

                        //26-30
                        100, 20, 0, 140, 230, 

                        //31-35
                        80, 20, 0, 200, 0, 

                        //36-38
                        200, 200, 200
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 1, 33, 34, 35, 37, 38 });
            Sm.GrdColInvisible(Grd1, new int[] { 33, 35 });
            if (mIsAutoGenerateBatchNo)
            {
                if (!mIsAutoGenerateBatchNoEditable)
                    Sm.GrdColReadOnly(Grd1, new int[] { 10 });
                Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 9, 11, 15, 17, 19, 21, 23, 24, 25, 26, 28, 29, 30, 31 });
            }
            else
                Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 5, 6, 7, 8, 9, 11, 15, 17, 19, 21, 23, 24, 25, 26, 28, 29, 30, 31 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 16, 18, 20 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 31 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2, 7 });
            Sm.GrdColButton(Grd1, new int[] { 4, 27, 32 });
            Grd1.Cols[31].Move(7);

            if (mIsShowForeignName)
            {
                Grd1.Cols[30].Move(10);
                if (IsProcFormat == "0")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 6, 8, 11, 12, 13, 18, 19, 20, 21, 24, 25, 26, 27, 28, 31 }, false);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 6, 8, 11, 12, 13, 18, 19, 20, 21, 24, 26, 27, 28, 31 }, false);
                    Grd1.Cols[25].Move(11);
                }
            }
            else
            {
                if (IsProcFormat == "0")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 6, 8, 11, 12, 13, 18, 19, 20, 21, 24, 25, 26, 27, 28, 30, 31 }, false);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 6, 8, 11, 12, 13, 18, 19, 20, 21, 24, 26, 27, 28, 30, 31 }, false);
                    Grd1.Cols[25].Move(10);
                }
            }

            if (mIsRecvVdNeedApproval)
            {
                Grd1.Cols[26].Move(4);
                Grd1.Cols[27].Move(5);
            }

            Grd1.Cols[29].Move(10);
            if (!mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 29 }, false);

            ShowInventoryUomCode();

            #endregion

            #region Grd2

            Grd2.Cols.Count = 10;
            Grd2.FrozenArea.ColCount = 3;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                         //0
                        "PO#", 
                        
                        //1-5
                        "PO"+Environment.NewLine+"DNo",
                        "Item's Code", 
                        "Item's Name", 
                        "UoM", 
                        "Outstanding"+Environment.NewLine+"Quantity", 
                        
                        //6-9
                        "Received"+Environment.NewLine+"Quantity",
                        "Balance",
                        "PO# (Source)",
                        "Foreign Name"
                    },
                     new int[] 
                    {
                        //0
                        150,

                        //1-5
                        0, 100, 200, 80, 100, 
                        
                        //6-9
                        100, 100, 150, 230
                    }
                );

            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdFormatDec(Grd2, new int[] { 5, 6, 7 }, 0);
            if (mIsShowForeignName)
            {
                Grd2.Cols[9].Move(4);
                Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, false);
            }
            else
                Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 9 }, false);

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 11, 12, 13, 31 }, !ChkHideInfoInGrd.Checked);
            if (mIsRecvVdNeedApproval)
                Sm.GrdColInvisible(Grd1, new int[] { 26, 27, 31 }, !ChkHideInfoInGrd.Checked);
            if (!mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 29 }, !ChkHideInfoInGrd.Checked);

            Sm.GrdColInvisible(Grd2, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            switch (state)
            {
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueDeptCode, LueCCCode, LueEntCode
                    }, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueVdCode, TxtVdDONo, MeeRemark, LueUserCode, LueEmpCode
                    }, false);
                    if (!mIsAutoGeneratePurchaseLocalDocNo)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);
                    if (mIsAutoGenerateBatchNo)
                    {
                        if (mIsAutoGenerateBatchNoEditable)
                            Sm.GrdColReadOnly(false, true, Grd1, new int[] { 10 });
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4, 12, 13, 14, 16, 18, 20, 22, 32, 36 });
                    }
                    else
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4, 10, 12, 13, 14, 16, 18, 20, 22, 32, 36 });
                    Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);

                    Sm.SetLue(LueDeptCode, mRecvVdEximDeptCode);
                    Sm.SetLue(LueCCCode, mRecvVdEximCCCode);
                    Sm.SetDteCurrentDate(DteDocDt);
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mDetailDate = string.Empty;
            mHeaderDate = string.Empty;
            compare1 = string.Empty;
            compare2 = string.Empty;
            mEntCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                DteDocDt, TxtLocalDocNo, LueWhsCode, LueVdCode, 
                TxtVdDONo, MeeRemark, LueEntCode, LueDeptCode, LueCCCode, LueUserCode, LueEmpCode
            });
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 16, 18, 20 });
            Sm.FocusGrd(Grd1, 0, 0);

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 5, 6, 7 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Button Methods

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
            {
                //e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmRecvVd3Dlg(this, Sm.GetLue(LueVdCode)));
            }

            if (e.ColIndex == 32 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 8, false, "Item is empty."))
            {
                //e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmRecvVd3Dlg2(this, e.RowIndex, Sm.GetLue(LueCCCode)));
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                //e.DoDefault = false;
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 7));
            }

            if (e.ColIndex == 27)
            {
                //e.DoDefault = false;
                ShowApprovalInfo(e.RowIndex);
            }

            if (BtnSave.Enabled)
            {
                //if (
                //((e.ColIndex == 1 && Sm.GetGrdBool(Grd1, e.RowIndex, 2)) || e.ColIndex != 1))
                //    e.DoDefault = false;

                //if (e.ColIndex == 3)
                //    e.DoDefault = true;

                //if (e.ColIndex == 1)
                //    e.DoDefault = false;

                if (!IsPOEmpty(e) && Sm.IsGrdColSelected(new int[] { 8, 12, 13, 14, 16, 18, 20, 22 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 13 }, e.ColIndex))
            {
                LueRequestEdit(Grd1, LueBin, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                //SetLueBin(ref LueBin);
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 12 }, e.ColIndex))
            {
                LueRequestEdit(Grd1, LueLot, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                //SetLueLot(ref LueLot);
            }

            if (BtnSave.Enabled && e.ColIndex == 36)
            {
                LueRequestEdit(Grd1, LueEmpCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                if (Grd1.SelectedRows.Count > 0)
                {
                    if (Grd1.Rows[Grd1.Rows[Grd1.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            string PODocNo = string.Empty, PODNo = string.Empty;

                            for (int Index = Grd1.SelectedRows.Count - 1; Index >= 0; Index--)
                            {
                                PODocNo = Sm.GetGrdStr(Grd1, Grd1.SelectedRows[Index].Index, 4);
                                PODNo = Sm.GetGrdStr(Grd1, Grd1.SelectedRows[Index].Index, 5);

                                Grd1.Rows.RemoveAt(Grd1.SelectedRows[Index].Index);

                                RemovePOInfo(PODocNo, PODNo);
                            }
                            if (Grd1.Rows.Count <= 0) Grd1.Rows.Add();
                        }
                    }
                }
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
                Sm.FormShowDialog(new FrmRecvVd3Dlg(this, Sm.GetLue(LueVdCode)));

            if (e.ColIndex == 27) ShowApprovalInfo(e.RowIndex);

            if (e.ColIndex == 32 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 8, false, "Item is empty."))
            {
                Sm.FormShowDialog(new FrmRecvVd3Dlg2(this, e.RowIndex, Sm.GetLue(LueCCCode)));
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                if (Sm.IsGrdColSelected(new int[] { 14, 16, 18, 20 }, e.ColIndex) &&
                    Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length == 0)
                    Grd1.Cells[e.RowIndex, e.ColIndex].Value = 0;

                if (e.ColIndex == 16)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 8, 16, 18, 20, 17, 19, 21);
                    Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 8, 16, 20, 18, 17, 21, 19);
                }

                if (e.ColIndex == 18)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 8, 18, 16, 20, 19, 17, 21);
                    Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 8, 18, 20, 16, 19, 21, 17);
                }

                if (e.ColIndex == 20)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 8, 20, 16, 18, 21, 17, 19);
                    Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 8, 20, 18, 16, 21, 19, 17);
                }

                if (e.ColIndex == 14 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), Sm.GetGrdStr(Grd1, e.RowIndex, 17)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 16, Grd1, e.RowIndex, 14);

                if (e.ColIndex == 14 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 14);

                if (e.ColIndex == 14 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 20, Grd1, e.RowIndex, 14);

                if (e.ColIndex == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 16);

                if (e.ColIndex == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 20, Grd1, e.RowIndex, 16);

                if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 20, Grd1, e.RowIndex, 18);

                if (e.ColIndex == 1 || e.ColIndex == 14) ComputeQty();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 14, 16, 18, 20 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }

            if (e.ColIndex == 34 && BtnSave.Enabled)
            {
                for (int Row = 1; Row < Grd1.Rows.Count - 1; Row++)
                {
                    Grd1.Cells[Row, 33].Value = Sm.GetGrdStr(Grd1, 0, 33);
                    Grd1.Cells[Row, e.ColIndex].Value = Sm.GetGrdStr(Grd1, 0, e.ColIndex);
                }
            }
            if (e.ColIndex == 36 && BtnSave.Enabled)
            {
                for (int Row = 1; Row < Grd1.Rows.Count - 1; Row++)
                {
                    Grd1.Cells[Row, 35].Value = Sm.GetGrdStr(Grd1, 0, 35);
                    Grd1.Cells[Row, e.ColIndex].Value = Sm.GetGrdStr(Grd1, 0, e.ColIndex);
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            string
                LocalDocNo = mIsAutoGeneratePurchaseLocalDocNo ? string.Empty : TxtLocalDocNo.Text,
                POLocalDocNo = string.Empty,
                PORevision = string.Empty;

            string
                SeqNo = string.Empty,
                DeptCode = string.Empty,
                ItSCCode = string.Empty,
                Mth = string.Empty,
                Yr = string.Empty,
                Revision = string.Empty;

            #region IsAutoGeneratePurchaseLocalDocNo

            if (mIsAutoGeneratePurchaseLocalDocNo)
            {
                SetLocalDocument(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr
                );
                if (mlLocalDocument.Count == 0) return;
                if (IsLocalDocumentNotValid(
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr
                    )) return;

                SetRevision(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr,
                    ref Revision
                    );

                if (SeqNo.Length > 0)
                {
                    SetLocalDocNo(
                        "RecvVd",
                        ref LocalDocNo,
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr,
                        ref Revision
                        );
                }
                mlLocalDocument.Clear();
            }

            #endregion

            Cursor.Current = Cursors.WaitCursor;

            string SubCategory = Sm.GetGrdStr(Grd1, 0, 24);
            string DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "RecvVd", "TblRecvVdHdr", SubCategory);
            string DODept2DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DODept2", "TblDODeptHdr");

            var cml = new List<MySqlCommand>();

            #region Split PO

            if (mIsRecvVdSplitOutstandingPO)
            {
                if (mIsAutoGeneratePurchaseLocalDocNo)
                {
                    if (SeqNo.Length > 0)
                    {
                        PORevision = GetPORevision(
                            ref SeqNo,
                            ref DeptCode,
                            ref ItSCCode,
                            ref Mth,
                            ref Yr
                            );
                        SetLocalDocNo(
                            "PO",
                            ref POLocalDocNo,
                            ref SeqNo,
                            ref DeptCode,
                            ref ItSCCode,
                            ref Mth,
                            ref Yr,
                            ref PORevision
                        );
                    }
                }

                var lh = new List<SOPOHdr>();
                var lda = new List<SOPODtla>();

                GenerateSOPOHdr(ref lh);
                GenerateSOPODtla(ref lda);
                UpdateBalanceSOPODtla(ref lda);
                var ldb = GenerateSOPODtlb(lh, ref lda);

                var lh2 = new List<SOPOHdr>();
                string PODocNoTemp = string.Empty;
                ldb.ForEach(d =>
                {
                    if (!Sm.CompareStr(PODocNoTemp, d.PODocNo))
                    {
                        foreach (var i in lh.Where(Index => Index.PODocNo2 == d.PODocNo2))
                        {
                            cml.Add(SavePOHdr(
                                d.PODocNo,
                                d.PODocNo2,
                                i.PODocNoSource,
                                i.RevNo,
                                POLocalDocNo,
                                SeqNo,
                                DeptCode,
                                ItSCCode,
                                Mth,
                                Yr,
                                PORevision
                                ));
                            break;
                        }
                    }

                    cml.Add(SavePODtl(
                        d.PODocNo, d.PODNo,
                        d.PODocNo2, d.PODNo2,
                        d.Qty2
                        ));

                    PODocNoTemp = d.PODocNo;
                });

                foreach (var po in ldb.Select(PO => new { PO.PODocNo, PO.PODocNo2 }).Distinct())
                    cml.Add(SavePOHdr(po.PODocNo, po.PODocNo2));
            }

            #endregion

            cml.Add(SaveRecvVdHdr(
                DocNo,
                LocalDocNo,
                SeqNo,
                ItSCCode,
                Mth,
                Yr,
                Revision,
                DODept2DocNo
                ));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 5).Length > 0) cml.Add(SaveRecvVdDtl(DocNo, Row));

            cml.Add(SaveStock(DocNo));

            cml.Add(UpdatePOProcessInd());

            // Save DO Dept Without DO Request
            cml.Add(SaveDODept2Hdr(DODept2DocNo, DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 5).Length > 0)
                    cml.Add(SaveDODept2Dtl(DODept2DocNo, DocNo, Row));

            cml.Add(SaveStockMovement(DODept2DocNo, "", "N"));

            if (mIsAutoJournalActived)
            {
                cml.Add(SaveJournal(DocNo));
                if (mIsEntityMandatory && IsEntityDifferent())
                    cml.Add(SaveJournalDODept2(DODept2DocNo));
                else
                    cml.Add(SaveJournal2DODept2(DODept2DocNo));
            }

            Sm.ExecCommands(cml);

            var mInfo = new StringBuilder();

            mInfo.AppendLine("Receiving Item From Vendor# : " + DocNo);
            mInfo.AppendLine("DO To Department (Without DO Request)# : " + DODept2DocNo);
            mInfo.AppendLine("These documents have been created.");

            Sm.StdMsg(mMsgType.Info, mInfo.ToString());

            ClearData();
            SetFormControl(mState.Insert);
        }

        #region Generate Local Document

        private void SetLocalDocNo(
            string DocType,
            ref string LocalDocNo,
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            var ShortCode = Sm.GetValue("Select IfNull(ShortCode, DeptCode) From TblDepartment Where DeptCode='" + DeptCode + "'");
            LocalDocNo = SeqNo + "/" + DocAbbr + "/" + ShortCode + "/" + ItSCCode + "/" + Mth + "/" + Yr;
            if (Revision.Length > 0 && Revision != "0")
                LocalDocNo = LocalDocNo + "/R" + Revision;
        }

        private void SetRevision(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select IfNull(Revision, '0') ");
            SQL.AppendLine("From TblRecvVdHdr ");
            SQL.AppendLine("Where SeqNo Is Not Null ");
            SQL.AppendLine("And SeqNo=@SeqNo ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And ItSCCode=@ItSCCode ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr ");
            SQL.AppendLine("Order By Revision Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            Revision = Sm.GetValue(cm);
            if (Revision.Length == 0)
                Revision = "0";
            else
                Revision = (int.Parse(Revision) + 1).ToString();
        }

        private bool IsLocalDocumentNotValid(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr
            )
        {
            if (SeqNo.Length == 0) return false;

            foreach (var x in mlLocalDocument.Where(x => x.SeqNo.Length > 0))
            {
                if (!(
                  Sm.CompareStr(SeqNo, x.SeqNo) &&
                  Sm.CompareStr(DeptCode, x.DeptCode) &&
                  Sm.CompareStr(ItSCCode, x.ItSCCode) &&
                  Sm.CompareStr(Mth, x.Mth) &&
                  Sm.CompareStr(Yr, x.Yr)
                ))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Document# : " + x.DocNo + Environment.NewLine +
                        "Local# : " + x.LocalDocNo + Environment.NewLine + Environment.NewLine +
                        "Invalid data.");
                    return true;
                }
            }
            return false;
        }

        private void SetLocalDocument(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr)
        {
            mlLocalDocument.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            bool IsFirst = true;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(DocNo=@DocNo" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 5));
                        No += 1;
                    }
                }
            }
            Filter = " Where (" + Filter + ")";

            SQL.AppendLine("Select DocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision ");
            SQL.AppendLine("From TblPOHdr " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "LocalDocNo", "SeqNo", "DeptCode", "ItSCCode", "Mth", 
                    
                    //6-7
                    "Yr", "Revision" 
                });
                if (dr.HasRows)
                {
                    string
                        DocNoTemp = string.Empty,
                        LocalDocNoTemp = string.Empty,
                        SeqNoTemp = string.Empty,
                        DeptCodeTemp = string.Empty,
                        ItSCCodeTemp = string.Empty,
                        MthTemp = string.Empty,
                        YrTemp = string.Empty,
                        RevisionTemp = string.Empty;

                    while (dr.Read())
                    {
                        DocNoTemp = Sm.DrStr(dr, c[0]);
                        LocalDocNoTemp = Sm.DrStr(dr, c[1]);
                        SeqNoTemp = Sm.DrStr(dr, c[2]);
                        DeptCodeTemp = Sm.DrStr(dr, c[3]);
                        ItSCCodeTemp = Sm.DrStr(dr, c[4]);
                        MthTemp = Sm.DrStr(dr, c[5]);
                        YrTemp = Sm.DrStr(dr, c[6]);
                        RevisionTemp = Sm.DrStr(dr, c[7]);

                        mlLocalDocument.Add(new LocalDocument()
                        {
                            DocNo = DocNoTemp,
                            LocalDocNo = LocalDocNoTemp,
                            SeqNo = SeqNoTemp,
                            DeptCode = DeptCodeTemp,
                            ItSCCode = ItSCCodeTemp,
                            Mth = MthTemp,
                            Yr = YrTemp,
                            Revision = RevisionTemp
                        });

                        if (IsFirst && SeqNoTemp.Length > 0)
                        {
                            SeqNo = SeqNoTemp;
                            DeptCode = DeptCodeTemp;
                            ItSCCode = ItSCCodeTemp;
                            Mth = MthTemp;
                            Yr = YrTemp;
                            IsFirst = false;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        private MySqlCommand SavePOHdr(
            string PODocNo,
            string PODocNo2,
            string PODocNoSource,
            string RevNo,
            string LocalDocNo,
            string SeqNo,
            string DeptCode,
            string ItSCCode,
            string Mth,
            string Yr,
            string Revision
            )
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPOHdr ");
            SQL.AppendLine("(DocNo, DocDt, ");
            SQL.AppendLine("LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ");
            SQL.AppendLine("DocNoSource, RevNo, VdCode, VdContactPerson, ShipTo, BillTo, CurCode, ");
            SQL.AppendLine("TaxCode1, TaxCode2, TaxCode3, TaxAmt, CustomsTaxAmt, DiscountAmt, Amt, SiteCode, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select @PODocNo2, DocDt, ");
            SQL.AppendLine("@LocalDocNo, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, ");
            SQL.AppendLine("@PODocNoSource, @RevNo, VdCode, VdContactPerson, ShipTo, BillTo, CurCode, ");
            SQL.AppendLine("TaxCode1, TaxCode2, TaxCode3, TaxAmt, CustomsTaxAmt, DiscountAmt, Amt, SiteCode, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt, Null, Null ");
            SQL.AppendLine("From TblPOHdr Where DocNo=@PODocNo;");

            SQL.AppendLine("Update TblPOHdr Set Remark=Concat('Split To PO ', @PODocNo2) Where DocNo=@PODocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PODocNo", PODocNo);
            Sm.CmParam<String>(ref cm, "@PODocNo2", PODocNo2);
            Sm.CmParam<String>(ref cm, "@PODocNoSource", PODocNoSource);
            Sm.CmParam<String>(ref cm, "@RevNo", RevNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            return cm;
        }

        private MySqlCommand SavePODtl(string PODocNo, string PODNo, string PODocNo2, string PODNo2, decimal Qty2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPODtl(DocNo, DNo, CancelInd, ProcessInd, PORequestDocNo, PORequestDNo, Qty, Discount, DiscountAmt, RoundingValue, EstRecvDt, Remark, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select @PODocNo2, @PODNo2, CancelInd, 'O', PORequestDocNo, PORequestDNo, @Qty2, Discount, DiscountAmt, RoundingValue, EstRecvDt, Remark, CreateBy, CreateDt, Null, Null ");
            SQL.AppendLine("From TblPODtl Where DocNo=@PODocNo And DNo=@PODNo; ");

            SQL.AppendLine("Update TblPODtl Set ");
            SQL.AppendLine("    CancelInd = Case When Qty=@Qty2 Then 'Y' Else CancelInd End, ");
            SQL.AppendLine("    Qty=Qty-@Qty2 ");
            SQL.AppendLine("Where DocNo=@PODocNo And DNo=@PODNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblPODtl Set ");
            SQL.AppendLine("    ProcessInd='F' ");
            SQL.AppendLine("Where DocNo=@PODocNo And DNo=@PODNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PODocNo", PODocNo);
            Sm.CmParam<String>(ref cm, "@PODNo", PODNo);
            Sm.CmParam<String>(ref cm, "@PODocNo2", PODocNo2);
            Sm.CmParam<String>(ref cm, "@PODNo2", PODNo2);
            Sm.CmParam<decimal>(ref cm, "@Qty2", Qty2);

            return cm;
        }

        private MySqlCommand SavePOHdr(
            string PODocNo,
            string PODocNo2
            )
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPOHdr As T1  ");
            SQL.AppendLine("Inner Join (");
            SQL.AppendLine("    Select A.DocNo, Sum( ");
            SQL.AppendLine("        Case When A.CancelInd='Y' Then 0 Else ");
            SQL.AppendLine("            (A.Qty*C.UPrice)-((A.Qty*C.UPrice)*A.Discount/100)-A.DiscountAmt+A.RoundingValue ");
            SQL.AppendLine("        End ");
            SQL.AppendLine("    ) As Amt ");
            SQL.AppendLine("    From TblPODtl A ");
            SQL.AppendLine("    Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
            SQL.AppendLine("    Inner Join TblQtDtl C On B.QtDocNo=C.DocNo And B.QtDNo=C.DNo ");
            SQL.AppendLine("    Where A.DocNo=@PODocNo ");
            SQL.AppendLine("    Group By A.DocNo ");
            SQL.AppendLine(") T3 On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("Left Join TblTax T4 On T1.TaxCode1=T4.TaxCode ");
            SQL.AppendLine("Left Join TblTax T5 On T1.TaxCode2=T5.TaxCode ");
            SQL.AppendLine("Left Join TblTax T6 On T1.TaxCode3=T6.TaxCode ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("T1.TaxAmt=(IfNull(T3.Amt, 0)*(IfNull(T4.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T5.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T6.TaxRate, 0)/100)), ");
            SQL.AppendLine("T1.Amt=IfNull(T3.Amt, 0)+((IfNull(T3.Amt, 0)*(IfNull(T4.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T5.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T6.TaxRate, 0)/100)))+T1.CustomsTaxAmt-T1.DiscountAmt ");
            SQL.AppendLine("Where T1.DocNo=@PODocNo; ");

            SQL.AppendLine("Update TblPOHdr As T1  ");
            SQL.AppendLine("Inner Join (");
            SQL.AppendLine("    Select A.DocNo, Sum( ");
            SQL.AppendLine("        Case when A.CancelInd='Y' Then 0 Else ");
            SQL.AppendLine("            (A.Qty*C.UPrice)-((A.Qty*C.UPrice)*A.Discount/100)-A.DiscountAmt+A.RoundingValue ");
            SQL.AppendLine("        End ");
            SQL.AppendLine("    ) As Amt ");
            SQL.AppendLine("    From TblPODtl A ");
            SQL.AppendLine("    Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
            SQL.AppendLine("    Inner Join TblQtDtl C On B.QtDocNo=C.DocNo And B.QtDNo=C.DNo ");
            SQL.AppendLine("    Where A.DocNo=@PODocNo2 ");
            SQL.AppendLine("    Group By A.DocNo ");
            SQL.AppendLine(") T3 On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("Left Join TblTax T4 On T1.TaxCode1=T4.TaxCode ");
            SQL.AppendLine("Left Join TblTax T5 On T1.TaxCode2=T5.TaxCode ");
            SQL.AppendLine("Left Join TblTax T6 On T1.TaxCode3=T6.TaxCode ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("T1.TaxAmt=(IfNull(T3.Amt, 0)*(IfNull(T4.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T5.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T6.TaxRate, 0)/100)), ");
            SQL.AppendLine("T1.Amt=IfNull(T3.Amt, 0)+((IfNull(T3.Amt, 0)*(IfNull(T4.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T5.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T6.TaxRate, 0)/100)))+T1.CustomsTaxAmt-T1.DiscountAmt ");
            SQL.AppendLine("Where T1.DocNo=@PODocNo2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PODocNo", PODocNo);
            Sm.CmParam<String>(ref cm, "@PODocNo2", PODocNo2);
            return cm;
        }

        private void GenerateSOPOHdr(ref List<SOPOHdr> lh)
        {
            string PODocNoSourceTemp = string.Empty, RevNoTemp = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNoSource, Max(RevNo) RevNo ");
            SQL.AppendLine("From TblPOHdr ");
            SQL.AppendLine("Where Position(Concat('##', DocNoSource, '##') In @Param)>0 ");
            SQL.AppendLine("Group By DocNoSource; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Param", GetSelectedSOPO());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNoSource", "RevNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        PODocNoSourceTemp = Sm.DrStr(dr, c[0]);
                        RevNoTemp = (Sm.DrDec(dr, c[1]) + 1m).ToString();
                        lh.Add(new SOPOHdr()
                        {
                            PODocNoSource = PODocNoSourceTemp,
                            RevNo = RevNoTemp,
                            PODocNo2 = PODocNoSourceTemp + "/R" + RevNoTemp
                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSelectedSOPO()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 8).Length > 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 8) + "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void GenerateSOPODtla(ref List<SOPODtla> lda)
        {
            string PODocNoSource = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocNoSource, ");
            SQL.AppendLine("(B.Qty-IfNull(C.Qty2, 0)-IfNull(D.Qty3, 0)+IfNull(E.Qty4, 0)) As OutstandingQty ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.QtyPurchase) As Qty2 ");
            SQL.AppendLine("    From TblRecvVdDtl T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Where T1.CancelInd='N' Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.Qty) As Qty3 ");
            SQL.AppendLine("    From TblPOQtyCancel T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Where T1.CancelInd='N' Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo And B.DNo=D.DNo  ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T4.DocNo, T4.DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As Qty4 ");
            SQL.AppendLine("    From TblDOVdHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T3 On T2.RecvVdDocNo=T3.DocNo And T2.RecvVdDNo=T3.DNo And T3.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.PODocNo=T4.DocNo And T3.PODNo=T4.DNo And T4.CancelInd='N' ");
            SQL.AppendLine("    Where T1.ReplacedItemInd='Y' And T1.VdCode=@VdCode ");
            SQL.AppendLine("    Group By T4.DocNo, T4.DNo ");
            SQL.AppendLine(") E On A.DocNo=E.DocNo And B.DNo=E.DNo ");
            SQL.AppendLine("Where Position(Concat('##', A.DocNo, '##') In @Param)>0 ");
            SQL.AppendLine("Order By A.DocNo, B.DNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Param", GetSelectedSOPO2());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "DocNoSource", "OutstandingQty" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lda.Add(new SOPODtla()
                        {
                            PODocNo = Sm.DrStr(dr, c[0]),
                            PODNo = Sm.DrStr(dr, c[1]),
                            PODocNoSource = Sm.DrStr(dr, c[2]),
                            POOutstandingQty = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSelectedSOPO2()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 0) + "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void UpdateBalanceSOPODtla(ref List<SOPODtla> lda)
        {
            lda.ForEach(d =>
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (
                        Sm.CompareStr(d.PODocNo, Sm.GetGrdStr(Grd2, Row, 0)) &&
                        Sm.CompareStr(d.PODNo, Sm.GetGrdStr(Grd2, Row, 1))
                        )
                    {
                        d.RecvQty = Sm.GetGrdDec(Grd2, Row, 6);
                        break;
                    }
                }
                d.Balance = d.POOutstandingQty - d.RecvQty;
            }
            );
        }

        private List<SOPODtlb> GenerateSOPODtlb(List<SOPOHdr> lh, ref List<SOPODtla> lda)
        {
            int DNo = 0;
            string
                PODocNoTemp = string.Empty,
                PODocNo2 = string.Empty;

            var ldb = new List<SOPODtlb>();
            lda.ForEach(d =>
            {
                if (d.Balance > 0)
                {
                    if (Sm.CompareStr(PODocNoTemp, d.PODocNo))
                        DNo += 1;
                    else
                        DNo = 1;

                    PODocNo2 = string.Empty;
                    foreach (var i in lh.Where(Index => Index.PODocNoSource == d.PODocNoSource))
                    {
                        PODocNo2 = i.PODocNo2;
                        break;
                    }

                    ldb.Add(new SOPODtlb()
                    {
                        PODocNo = d.PODocNo,
                        PODNo = d.PODNo,
                        PODocNo2 = PODocNo2,
                        PODNo2 = Sm.Right("00" + DNo, 3),
                        Qty2 = d.Balance
                    });
                }
                PODocNoTemp = d.PODocNo;
            }
            );
            return ldb;
        }

        private string GetPORevision(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr
        )
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select IfNull(Revision, '0') ");
            SQL.AppendLine("From TblPOHdr ");
            SQL.AppendLine("Where SeqNo Is Not Null ");
            SQL.AppendLine("And SeqNo=@SeqNo ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And ItSCCode=@ItSCCode ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr ");
            SQL.AppendLine("Order By Revision Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            var Revision = Sm.GetValue(cm);
            if (Revision.Length == 0)
                Revision = "0";
            else
                Revision = (int.Parse(Revision) + 1).ToString();

            return Revision;
        }

        private bool IsInsertedDataNotValid()
        {
            IsSubCategoryNull();
            ComputeOutstandingQty();

            if (mIsSystemUseCostCenter)
            {
                SetCCtName();
                SetAcNo();
            }

            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                ((mIsSystemUseCostCenter || mIsFilterByCC) &&
                Sm.IsLueEmpty(LueCCCode, "Cost center")) ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                (mIsRemarkForJournalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsQtyNotValid() ||
                IsBalanceNotValid() ||
                IsSubcategoryDifferent() ||
                IsDateNotValid() ||
                IsEntityNotValid()
                ;
        }

        private bool IsEntityNotValid()
        {
            string WhsEntCode = string.Empty;
            string POEntCode = string.Empty;
            if (mIsRecvVdValidateByEntity)
            {
                WhsEntCode = Sm.GetValue("Select D.EntName From  tblwarehouse A " +
                                        "Inner Join TblCostcenter B on A.CCCode = B.CCCode " +
                                        "Inner Join TblProfitcenter C On B.ProfitcenterCode = C.ProfitcenterCode " +
                                        "Inner Join TblEntity D On C.EntCode = D.EntCode " +
                                        "Where A.WhsCode = '" + Sm.GetLue(LueWhsCode) + "' ");

                if (Grd1.Rows.Count != 1)
                {
                    //loop grid untuk bandingin entity PO 
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        POEntCode = Sm.GetValue("Select C.EntName " +
                                        "From TblPOHdr A1 " +
                                        "Inner Join tblSite A On A1.SiteCode = A.SiteCode " +
                                        "Inner Join TblProfitCenter B On A.ProfitCenterCode = B.Profitcentercode " +
                                        "Inner Join TblEntity C On B.EntCode=C.EntCode " +
                                        "Where A1.DocNo = '" + Sm.GetGrdStr(Grd1, Row, 5) + "' ");

                        if (!Sm.CompareStr(WhsEntCode, POEntCode))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                            "Entity PO ( " + Sm.GetGrdStr(Grd1, Row, 5) + " ) : " + POEntCode + ".\n" +
                            "Entity Receiving : " + WhsEntCode + ".\n" +
                            "This transaction has different entity.");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsDateNotValid()
        {
            mHeaderDate = Sm.GetDte(DteDocDt);

            if (mIsComparedToDetailDate)
            {
                if (Grd1.Rows.Count != 1)
                {
                    //loop grid untuk mendapatkan tanggal terbaru
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareDtTm(compare1, Sm.GetGrdDate(Grd1, Row, 31)) < 0)
                        {
                            compare1 = Sm.GetGrdDate(Grd1, Row, 31);
                            compare2 = compare1;
                        }
                        else
                            compare2 = compare1;
                    }
                    mDetailDate = compare2;

                    if (Sm.CompareDtTm(mHeaderDate, mDetailDate) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Date should be the same or greater than PO Date.");
                        DteDocDt.Focus();
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 5, false, "PO# is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 14, true, "Quantity (Purchase) is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 16, true, "Quantity (Inventory) is empty.") ||
                    (mIsRemarkForApprovalMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 22, false, "Remark is empty."))
                    )
                    return true;

                if ((mIsSystemUseCostCenter &&
                    Sm.IsGrdValueEmpty(Grd1, Row, 37, false,
                        "COA's account# is empty." + Environment.NewLine +
                        "Please contact Finance/Accounting Department !"
                        ))) return true;

                if ((mIsSystemUseCostCenter &&
                    Sm.IsGrdValueEmpty(Grd1, Row, 38, false,
                    "Cost category is empty." + Environment.NewLine +
                    "Please contact Finance/Accounting Department !"
                    ))) return true;

            }
            return false;
        }

        private bool IsQtyNotValid()
        {
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 5).Length > 0)
                {
                    if (
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 15), Sm.GetGrdStr(Grd1, Row, 17)) && Sm.GetGrdDec(Grd1, Row, 14) != Sm.GetGrdDec(Grd1, Row, 16)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 15), Sm.GetGrdStr(Grd1, Row, 18)) && Sm.GetGrdDec(Grd1, Row, 14) != Sm.GetGrdDec(Grd1, Row, 18)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 15), Sm.GetGrdStr(Grd1, Row, 21)) && Sm.GetGrdDec(Grd1, Row, 14) != Sm.GetGrdDec(Grd1, Row, 20)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 17), Sm.GetGrdStr(Grd1, Row, 19)) && Sm.GetGrdDec(Grd1, Row, 16) != Sm.GetGrdDec(Grd1, Row, 18)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 17), Sm.GetGrdStr(Grd1, Row, 21)) && Sm.GetGrdDec(Grd1, Row, 16) != Sm.GetGrdDec(Grd1, Row, 20)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 19), Sm.GetGrdStr(Grd1, Row, 21)) && Sm.GetGrdDec(Grd1, Row, 18) != Sm.GetGrdDec(Grd1, Row, 20))
                        )
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "PO Number : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + Environment.NewLine +
                            "Quantity is not valid.");
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsBalanceNotValid()
        {
            for (int Row = 0; Row <= Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0 && Sm.GetGrdDec(Grd2, Row, 7) < 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "PO# : " + Sm.GetGrdStr(Grd2, Row, 0) + Environment.NewLine +
                        "Item Code : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                        "Item Name : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                        "Balance : " + Sm.GetGrdDec(Grd2, Row, 7) + Environment.NewLine +
                        "Uom : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine + Environment.NewLine +
                        "Balance is less than 0.");
                    return true;
                }
            }

            return false;
        }

        private void IsSubCategoryNull()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 24).Length == 0)
                {
                    Grd1.Cells[Row, 24].Value = Grd1.Cells[Row, 25].Value = "XXX";
                }
            }
        }

        private bool IsSubcategoryDifferent()
        {
            if (IsProcFormat == "1")
            {
                string SubCat = Sm.GetGrdStr(Grd1, 0, 24);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (SubCat != Sm.GetGrdStr(Grd1, Row, 24))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Item have different subcategory ");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private MySqlCommand SaveRecvVdHdr(
            string DocNo,
            string LocalDocNo,
            string SeqNo,
            string ItSCCode,
            string Mth,
            string Yr,
            string Revision,
            string DODept2DocNo
            )
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
 
            SQL.AppendLine("Insert Into TblRecvVdHdr ");
            SQL.AppendLine("(DocNo, DocDt, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ");
            SQL.AppendLine("POInd, WhsCode, VdCode, VdDONo, JournalDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @LocalDocNo, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, ");
            SQL.AppendLine("'Y', @WhsCode, @VdCode, @VdDONo, Null, @Remark, @CreateBy, CurrentDateTime()); ");

            if (mIsRecvVdLocalDocNoBasedOnMRLocalDocNo && LocalDocNo.Length == 0)
            {
                string PODocNo = string.Empty, PODNo = string.Empty, Filter = string.Empty;

                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    PODocNo = Sm.GetGrdStr(Grd1, r, 5);
                    PODNo = Sm.GetGrdStr(Grd1, r, 6);
                    if (PODocNo.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T1.DocNo=@PODocNo0" + r.ToString() + " And T1.DNo=@PODNo0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@PODocNo0" + r.ToString(), PODocNo);
                        Sm.CmParam<String>(ref cm, "@PODNo0" + r.ToString(), PODNo);
                    }
                }

                if (Filter.Length > 0)
                    Filter = " Where ( " + Filter + ") ";
                else
                    Filter = " Where 1=0 ";

                SQL.AppendLine("Update TblRecvVdHdr Set ");
                SQL.AppendLine("    LocalDocNo=(");
                SQL.AppendLine("    Select Group_Concat(Distinct T3.LocalDocNo Separator ', ') As Value ");
                SQL.AppendLine("    From TblPODtl T1 ");
                SQL.AppendLine("    Inner Join TblPORequestDtl T2 On T1.PORequestDocNo=T2.DocNo And T1.PORequestDNo=T2.DNo  ");
                SQL.AppendLine("    Inner Join TblMaterialRequestHdr T3 On T2.MaterialRequestDocNo=T3.DocNo And T3.LocalDocNo Is Not Null ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(") ");
                SQL.AppendLine("Where LocalDocNo Is Null ");
                SQL.AppendLine("And DocNo=@DocNo; ");
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@VdDONo", TxtVdDONo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", string.Concat(MeeRemark.Text, "[EXIM]"));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRecvVdDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            if (mIsAutoGenerateBatchNo)
            {
                SQL.AppendLine("Insert Into TblRecvVdDtl(DocNo, DNo, CancelInd, CancelReason, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, QtyPurchase, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @DocNo, @DNo, 'N', @CancelReason, @Status, @PODocNo, @PODNo, @ItCode, ");
                if (mRecvVdAutoBatchNoFormat == "2")
                    SQL.AppendLine("Concat(IfNull(B.ShortName, ''), '-', A.DocDt, '-', A.DocNo, '-', @DNo) As BatchNo, ");
                else
                {
                    if (mIsAutoGenerateBatchNoEditable)
                        SQL.AppendLine("Concat(IfNull(B.ShortName, ''), '-', A.DocDt, '-', A.DocNo, Case When @BatchNo Is Null Then '' Else Concat('-', @BatchNo) End) As BatchNo, ");
                    else
                        SQL.AppendLine("Concat(IfNull(B.ShortName, ''), '-', A.DocDt, '-', A.DocNo) As BatchNo, ");
                }
                SQL.AppendLine("Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-'), @QtyPurchase, @Qty, @Qty2, @Qty3, @Remark, ");
                SQL.AppendLine("@CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblRecvVdHdr A ");
                SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo; ");
            }
            else
            {
                SQL.AppendLine("Insert Into TblRecvVdDtl(DocNo, DNo, CancelInd, CancelReason, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, QtyPurchase, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocNo, @DNo, 'N', @CancelReason, @Status, @PODocNo, @PODNo, @ItCode, IfNull(@BatchNo, '-'), Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-'), ");
                SQL.AppendLine("@QtyPurchase, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Status", "A");
            Sm.CmParam<String>(ref cm, "@DocType", "01");
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@PODocNo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@PODNo", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@QtyPurchase", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.Status='A' And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode And C.FixedItemInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, 0, 0, 0, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.Status='A' And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode And C.FixedItemInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.ItCode, A.BatchNo, A.Source, E.CurCode, ");
            //SQL.AppendLine("(A.QtyPurchase/A.Qty)*F.UPrice, ");

            SQL.AppendLine("((A.QtyPurchase/A.Qty)*F.UPrice)-");
            SQL.AppendLine("(((A.QtyPurchase/A.Qty)*F.UPrice)*C.Discount/100.00)-");
            SQL.AppendLine("((1.00/A.Qty)*(A.QtyPurchase/C.Qty)*C.DiscountAmt)+");
            SQL.AppendLine("((1.00/A.Qty)*(A.QtyPurchase/C.Qty)*C.RoundingValue) ");
            SQL.AppendLine("As UPrice, ");

            SQL.AppendLine("Case When E.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=G.DocDt And CurCode1=E.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0.00) End As ExcRate, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdDtl A ");
            SQL.AppendLine("Inner Join TblPOHdr B On A.PODocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl C On A.PODocNo=C.DocNo And A.PODNo=C.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl D On C.PORequestDocNo=D.DocNo And C.PORequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblQtDtl F On D.QtDocNo=F.DocNo And D.QtDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblRecvVdHdr G On A.DocNo=G.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.Status='A' And A.CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mRecvVdDocType);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdatePOProcessInd()
        {
            if (mIsRecvVdSplitOutstandingPO)
                return UpdatePOProcessInd_Split();
            else
                return UpdatePOProcessInd_Standard();
        }

        private MySqlCommand UpdatePOProcessInd_Standard()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            
            if (Grd2.Rows.Count != 1)
            {
                for (int r = 0;r<Grd2.Rows.Count ;r++)
                {
                    if (Sm.GetGrdStr(Grd2, r, 0).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.PODocNo=@PODocNo" + r.ToString() + " And A.PODNo=@PODNo" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@PODocNo" + r.ToString(), Sm.GetGrdStr(Grd2, r, 0));
                        Sm.CmParam<String>(ref cm, "@PODNo" + r.ToString(), Sm.GetGrdStr(Grd2, r, 1));
                    }
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 1=0 ";

            SQL.AppendLine("Update TblPODtl T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.DocNo, A.DNo, A.Qty, ");
            SQL.AppendLine("    IfNull(B.RecvVdQty, 0)+IfNull(C.POQtyCancelQty, 0)-IfNull(D.DOVdQty, 0) As Qty2  ");
            SQL.AppendLine("    From TblPODtl A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select PODocNo As DocNo, PODNo As DNo, Sum(QtyPurchase) As RecvVdQty ");
            SQL.AppendLine("        From TblRecvVdDtl ");
            SQL.AppendLine("        Where CancelInd='N' ");
            SQL.AppendLine("        And IfNull(Status, 'O')<>'C' ");
            SQL.AppendLine(Filter.Replace("A.", string.Empty));
            SQL.AppendLine("        Group By PODocNo, PODNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select PODocNo As DocNo, PODNo As DNo, Sum(Qty) As POQtyCancelQty ");
            SQL.AppendLine("        From TblPOQtyCancel ");
            SQL.AppendLine("        Where CancelInd='N' ");
            SQL.AppendLine(Filter.Replace("A.", string.Empty));
            SQL.AppendLine("        Group By PODocNo, PODNo ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.PODocNo As DocNo, T3.PODNo As DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As DOVdQty ");
            SQL.AppendLine("    From TblDOVdHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T3 ");
            SQL.AppendLine("        On T2.RecvVdDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.RecvVdDNo=T3.DNo ");
            SQL.AppendLine("        And T3.CancelInd='N' And IfNull(T3.Status, 'O')<>'C' ");
            SQL.AppendLine(Filter.Replace("A.", "T3."));
            SQL.AppendLine("    Where T1.ReplacedItemInd='Y' And T1.VdCode=@VdCode ");
            SQL.AppendLine("    Group By T3.PODocNo, T3.PODNo ");
            SQL.AppendLine("    ) D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine(Filter.Replace(".PO", "."));
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo ");
            SQL.AppendLine("Set T1.ProcessInd= ");
            SQL.AppendLine("    Case When T2.Qty2=0 Then 'O' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If((T2.Qty-T2.Qty2)<=0, 'F', 'P') ");
            SQL.AppendLine("    End; ");
           
            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));

            return cm;
        }

        private MySqlCommand UpdatePOProcessInd_Split()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPODtl T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.DocNo, A.DNo, A.Qty, ");
            SQL.AppendLine("    IfNull(B.RecvVdQty, 0)+IfNull(C.POQtyCancelQty, 0)-IfNull(D.DOVdQty, 0) As Qty2  ");
            SQL.AppendLine("    From TblPODtl A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select PODocNo As DocNo, PODNo As DNo, Sum(QtyPurchase) As RecvVdQty ");
            SQL.AppendLine("        From TblRecvVdDtl ");
            SQL.AppendLine("        Where Locate(Concat('##', PODocNo, '##'), @Param)>0 ");
            SQL.AppendLine("        And CancelInd='N' And IfNull(Status, 'O')<>'C' ");
            SQL.AppendLine("        Group By PODocNo, PODNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select PODocNo As DocNo, PODNo As DNo, Sum(Qty) As POQtyCancelQty ");
            SQL.AppendLine("        From TblPOQtyCancel ");
            SQL.AppendLine("        Where Locate(Concat('##', PODocNo, '##'), @Param)>0 ");
            SQL.AppendLine("        And CancelInd='N' ");
            SQL.AppendLine("        Group By PODocNo, PODNo ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.PODocNo As DocNo, T3.PODNo As DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As DOVdQty ");
            SQL.AppendLine("    From TblDOVdHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T3 ");
            SQL.AppendLine("        On T2.RecvVdDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.RecvVdDNo=T3.DNo ");
            SQL.AppendLine("        And T3.CancelInd='N' And IfNull(T3.Status, 'O')<>'C' ");
            SQL.AppendLine("        And Locate(Concat('##', T3.PODocNo, '##'), @Param)>0 ");
            SQL.AppendLine("    Where T1.ReplacedItemInd='Y' And T1.VdCode=@VdCode ");
            SQL.AppendLine("    Group By T3.PODocNo, T3.PODNo ");
            SQL.AppendLine("    ) D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            SQL.AppendLine("    Where Locate(Concat('##', A.DocNo, '##'), @Param)>0 ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo ");
            SQL.AppendLine("Set T1.ProcessInd= ");
            SQL.AppendLine("    Case When T2.Qty2=0 Then 'O' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If((T2.Qty-T2.Qty2)<=0, 'F', 'P') ");
            SQL.AppendLine("    End; ");
           
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Param", GetSelectedSOPO2());
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblRecvVdHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RecvVd' And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Vendor : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvVdHdr Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RecvVd' And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select D.AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblRecvVdDtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
            SQL.AppendLine("        0 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
            SQL.AppendLine("        From TblRecvVdHdr A ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo is Not Null ");
            SQL.AppendLine("    Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            //SQL.AppendLine("Where A.DocNo=@JournalDocNo;");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblRecvVdHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mRecvVdMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);

            return cm;
        }

        private MySqlCommand SaveDODept2Hdr(string DODept2DocNo, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDODeptHdr(DocNo, DocDt, LocalDocNo, WhsCode, DeptCode, EntCode, CCCode, RequestByDocNo, EmpCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DODept2DocNo, @DocDt, @LocalDocNo, @WhsCode, @DeptCode, @EntCode, @CCCode, ");
            if (mDODeptRequestBySource == "1")
            {
                SQL.AppendLine("(Select A.DocNo ");
                SQL.AppendLine("From TblRequestByHdr A, TblRequestByDtl B ");
                SQL.AppendLine("Where A.CancelInd='N' And A.DocNo=B.DocNo And B.EmpCode=@EmpCode Limit 1), ");
            }
            else
            {
                SQL.AppendLine("Null, ");
            }
            SQL.AppendLine("@EmpCode, @Remark, @CreateBy, CurrentDateTime()); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DODept2DocNo", DODept2DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetLue(LueUserCode));
            Sm.CmParam<String>(ref cm, "@Remark", string.Concat(MeeRemark.Text, "[EXIM]"));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDODept2Dtl(string DODept2DocNo, string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDODeptDtl(DocNo, DNo, CancelInd, ItCode, ReplacementInd, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, RequestByDocNo, EmpCode, AssetCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DODept2DocNo, @DNo, 'N', A.ItCode, 'N', @PropCode, A.BatchNo, A.Source, A.Lot, A.Bin, A.Qty, A.Qty2, A.Qty3, ");
            if (mDODeptRequestBySource == "1")
            {
                SQL.AppendLine("(Select A.DocNo ");
                SQL.AppendLine("From TblRequestByHdr A, TblRequestByDtl B ");
                SQL.AppendLine("Where A.CancelInd='N' And A.DocNo=B.DocNo And B.EmpCode=@EmpCode Limit 1), ");
            }
            else
            {
                SQL.AppendLine("Null, ");
            }
            SQL.AppendLine("@EmpCode, @AssetCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblRecvVdDtl A ");
            SQL.AppendLine("Where A.DocNo = @DocNo And A.DNo = @DNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DODept2DocNo", DODept2DocNo);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PropCode", "-");
            Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd1, Row, 33));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 35));
            return cm;
        }

        private MySqlCommand SaveStockMovement(string DocNo, string DNo, string CancelInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");

            if (CancelInd == "N")
            {
                SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, @CancelInd, A.DocDt, ");
                SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
                SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
                SQL.AppendLine("Case When A.Remark Is Null Then ");
                SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
                SQL.AppendLine("End As Remark, ");
                SQL.AppendLine("@UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDODeptHdr A ");
                SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode And C.FixedItemInd = 'N' ");
                SQL.AppendLine("Where A.DocNo=@DocNo; ");
            }
            else
            {
                SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, @CancelInd, A.DocDt, ");
                SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
                SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
                SQL.AppendLine("Case When A.Remark Is Null Then ");
                SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
                SQL.AppendLine("End As Remark, ");
                SQL.AppendLine("@UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDODeptHdr A ");
                SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode And C.FixedItemInd = 'N' ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("And Position(Concat('##', DNo, '##') In @DNo)>0; ");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (DNo.Length > 0) Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDODept2DocType);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournalDODept2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDODeptHdr Set JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 2));
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, ");
            SQL.AppendLine("Concat('DO To Department : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblDODeptHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
            SQL.AppendLine("    D.ParValue As AcNo, ");
            SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
            SQL.AppendLine("    0.00 As CAmt ");
            SQL.AppendLine("    From TblDODeptHdr A ");
            SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
            SQL.AppendLine("    T.AcNo, ");
            SQL.AppendLine("    0.00 As DAmt, ");
            SQL.AppendLine("    Sum(T.Amt) As CAmt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select E.AcNo, ");
            SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
            SQL.AppendLine("        From TblDODeptHdr A ");
            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) T Group By T.AcNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
            SQL.AppendLine("    D.AcNo2 As AcNo, ");
            SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
            SQL.AppendLine("    0.00 As CAmt ");
            SQL.AppendLine("    From TblDODeptHdr A ");
            SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
            SQL.AppendLine("    D.ParValue As AcNo, ");
            SQL.AppendLine("    0.00 As DAmt, ");
            SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
            SQL.AppendLine("    From TblDODeptHdr A ");
            SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' And D.ParValue Is Not Null ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
            SQL.AppendLine("    T.AcNo, ");
            SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
            SQL.AppendLine("    0.00 As CAmt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select E.AcNo, ");
            SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
            SQL.AppendLine("        From TblDODeptHdr A ");
            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
            SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) T Group By T.AcNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
            SQL.AppendLine("    D.AcNo1 As AcNo, ");
            SQL.AppendLine("    0.00 As DAmt, ");
            SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
            SQL.AppendLine("    From TblDODeptHdr A ");
            SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC And D.AcNo1 Is Not Null ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblDODeptHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");
            //SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 2));
            Sm.CmParam<String>(ref cm, "@MenuCode", mDODept2MenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeCC", Sm.GetEntityCostCenter(Sm.GetLue(LueCCCode)));

            return cm;
        }

        private MySqlCommand SaveJournal2DODept2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDODeptHdr Set JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 2));
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, ");
            SQL.AppendLine("Concat('DO To Department : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblDODeptHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt ");
            SQL.AppendLine("    From (");
            SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("        From TblDODeptHdr A ");
            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
            SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select D.AcNo, 0.00 As DAmt, A.Qty*B.UPrice*B.ExcRate As CAmt ");
            SQL.AppendLine("        From TblDODeptDtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblDODeptHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");
            //SQL.AppendLine("Where A.DocNo=@JournalDocNo;");
            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 2));
            Sm.CmParam<String>(ref cm, "@MenuCode", mDODept2MenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            return cm;
        }

        #endregion

        #endregion

        #region Additional Method

        private void SetLueUserCode(ref LookUpEdit Lue, string Param1, string Param2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (mDODeptRequestBySource == "1")
            {
                //Param1 = RequestByDocNo
                //Param2 = EmpCodeCode
                if (Param2.Length > 0)
                {
                    SQL.AppendLine("Select T2.EmpCode As Col1, T2.RequestName As Col2 ");
                    SQL.AppendLine("From TblRequestByHdr T1 ");
                    SQL.AppendLine("Inner Join TblRequestByDtl T2 On T1.DocNo=T2.DocNo And T2.EmpCode=@EmpCode ");
                    SQL.AppendLine("Where T1.DocNo=@DocNo;");

                    Sm.CmParam<String>(ref cm, "@DocNo", Param1);
                    Sm.CmParam<String>(ref cm, "@EmpCode", Param2);
                }
                else
                {
                    SQL.AppendLine("Select T2.EmpCode As Col1, T2.RequestName As Col2 ");
                    SQL.AppendLine("From TblRequestByHdr T1 ");
                    SQL.AppendLine("Inner Join TblRequestByDtl T2 On T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("Inner Join TblEmployee T3 On T2.EmpCode=T3.EmpCode ");
                    SQL.AppendLine("    And T3.DeptCode Is Not Null ");
                    SQL.AppendLine("    And T3.DeptCode=@DeptCode ");
                    SQL.AppendLine("    And (T3.ResignDt Is Null Or (T3.ResignDt Is Not Null And T3.ResignDt>=Replace(curdate(), '-', ''))) ");
                    SQL.AppendLine("Where T1.CancelInd='N' ");
                    SQL.AppendLine("Order By T2.RequestName;");

                    Sm.CmParam<String>(ref cm, "@DeptCode", Param1);
                }
            }
            else
            {
                //Param1 = DeptCode
                //Param2 = UserCode

                SQL.AppendLine("Select UserCode As Col1, UserName As Col2 From TblUser ");
                if (Param2.Length == 0)
                {
                    SQL.AppendLine("Where ExpDt Is Null ");
                    SQL.AppendLine("And UserCode In ( ");
                    SQL.AppendLine("    Select UserCode From TblEmployee ");
                    SQL.AppendLine("    Where UserCode Is Not Null ");
                    SQL.AppendLine("    And DeptCode=@DeptCode ");
                    SQL.AppendLine("    And DeptCode Is Not Null ");
                    SQL.AppendLine("    And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>=Replace(curdate(), '-', ''))) ");
                    SQL.AppendLine(") Order By UserName;");

                    Sm.CmParam<String>(ref cm, "@DeptCode", Param1);
                }
                else
                {
                    SQL.AppendLine("Where UserCode=@UserCode;");

                    Sm.CmParam<String>(ref cm, "@UserCode", Param2);
                }
            }
            cm.CommandText = SQL.ToString();
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Param2.Length > 0) Sm.SetLue(Lue, Param2);
        }

        private void SetLueEmpCode(ref LookUpEdit Lue, string DeptCode, string EmpCode)
        {
            var SQL = new StringBuilder();

            if (EmpCode.Length == 0)
            {
                SQL.AppendLine("Select A.EmpCode As Col1, A.EmpName As Col2, B.PosName As Col3 ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Inner Join TblPosition B On A.PosCode = B.PosCode And B.RequestItemInd = 'Y' ");
                SQL.AppendLine("Where A.ResignDt Is Null And IfNull(A.DeptCode, '')='" + DeptCode + "' ");
                SQL.AppendLine("Order By A.EmpName; ");
            }
            else
            {
                SQL.AppendLine("Select A.EmpCode As Col1, A.EmpName As Col2, B.PosName As Col3 ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Left Join TblPosition B On A.PosCode = B.PosCode ");
                SQL.AppendLine("Where A.EmpCode='" + EmpCode + "'; ");
            }

            Sm.SetLue3(
                ref Lue,
                SQL.ToString(),
                0, 40, 40, false, true, true, "Code", "Name", "Position", "Col2", "Col1");
        }

        private bool IsEntityDifferent()
        {
            var EntCode1 = Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode));
            var EntCode2 = string.Empty;

            if (Sm.GetLue(LueCCCode).Length > 0) EntCode2 = Sm.GetEntityCostCenter(Sm.GetLue(LueCCCode));

            return (EntCode1 != EntCode2);
        }

        private void SetCCtName()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string ItCode = string.Empty, CCtName = string.Empty, Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    ItCode = Sm.GetGrdStr(Grd1, r, 8);
                    if (ItCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(ItCode=@ItCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " And (" + Filter + ");";
            else
                Filter = " And 1=0;";

            SQL.AppendLine("Select A.ItCode, B.CCtName ");
            SQL.AppendLine("From TblItemCostCategory A ");
            SQL.AppendLine("Inner Join TblCostCategory B On A.CCCode=B.CCCode And A.CCtCode=B.CCtCode ");
            SQL.AppendLine("Where A.CCCode=@CCCode ");
            SQL.AppendLine(Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "CCtName" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        CCtName = Sm.DrStr(dr, 1);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), ItCode))
                                Grd1.Cells[r, 38].Value = CCtName;
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void SetAcNo()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string ItCode = string.Empty, AcNo = string.Empty, Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    ItCode = Sm.GetGrdStr(Grd1, r, 8);
                    if (ItCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(ItCode=@ItCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ");";
            else
                Filter = " Where 1=0;";

            SQL.AppendLine("Select A.ItCode, B.AcNo ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine(Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "AcNo" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        AcNo = Sm.DrStr(dr, 1);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), ItCode))
                                Grd1.Cells[r, 37].Value = AcNo;
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        public static void SetLueLot(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Distinct Lot As Col1 From TblLotHdr Where ActInd='Y' Order By Lot",
                "Lot");
        }

        public static void SetLueBin(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Distinct Bin As Col1 from TblBin Where ActInd='Y' Order By Bin;",
                "Bin");
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsAutoGeneratePurchaseLocalDocNo = Sm.GetParameterBoo("IsAutoGeneratePurchaseLocalDocNo");
            mIsAutoGenerateBatchNo = Sm.GetParameterBoo("IsAutoGenerateBatchNo");
            mIsAutoGenerateBatchNoEditable = Sm.GetParameterBoo("IsAutoGenerateBatchNoEditable");
            IsProcFormat = Sm.GetParameter("ProcFormatDocNo");
            mIsRecvVdSplitOutstandingPO = Sm.GetParameterBoo("IsRecvVdSplitOutstandingPO");
            mPODocTitle = Sm.GetParameter("DocTitle");
            mPODocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='PO'");
            mIsPOSplitBasedOnTax = Sm.GetParameter("IsPOSplitBasedOnTax");
            mIsRecvVdNeedApproval = Sm.GetParameterBoo("IsRecvVdNeedApproval");
            mIsRemarkForApprovalMandatory = Sm.GetParameterBoo("IsRecvVdRemarkForApprovalMandatory");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            mIsRecvVdApprovalBasedOnWhs = Sm.GetParameterBoo("IsRecvVdApprovalBasedOnWhs");
            mPOWithTitleLength = 4 + mPODocTitle.Length + mPODocAbbr.Length + 4 + 4; //4=nomor urut(0001), 4=bln+tahun(1115), 4 = jmlh "/"
            mPOWithoutTitleLength = 4 + mPODocAbbr.Length + 4 + 3;
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsComparedToDetailDate = Sm.GetParameterBoo("IsComparedToDetailDate");
            mIsRemarkForJournalMandatory = Sm.GetParameterBoo("IsRemarkForJournalMandatory");
            mIsRecvVdValidateByEntity = Sm.GetParameterBoo("IsRecvVdValidateByEntity");
            mRecvVdAutoBatchNoFormat = Sm.GetParameter("RecvVdAutoBatchNoFormat");
            mIsSystemUseCostCenter = Sm.GetParameterBoo("IsSystemUseCostCenter");
            mIsFilterByCC = Sm.GetParameterBoo("IsFilterByCC");
            mDODeptRequestBySource = Sm.GetParameter("DODeptRequestBySource");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mRecvVdEximDeptCode = Sm.GetParameter("RecvVdEximDeptCode");
            mRecvVdEximCCCode = Sm.GetParameter("RecvVdEximCCCode");
            mIsRecvVdLocalDocNoBasedOnMRLocalDocNo = Sm.GetParameterBoo("IsRecvVdLocalDocNoBasedOnMRLocalDocNo");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private static string GenerateDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");

            var SQL = new StringBuilder();

            if (IsProcFormat == "1")
            {
                SQL.Append("Select Concat('" + SubCategory + "', '/', ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("         Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                SQL.Append("      Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }

            return Sm.GetValue(SQL.ToString());
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            mNumberOfInventoryUomCode = (NumberOfInventoryUomCode.Length == 0) ? 1 : int.Parse(NumberOfInventoryUomCode);
        }

        internal void ShowPOInfo(int Row)
        {
            bool IsExisted = false;
            decimal ReceivedQty = 0m;

            if (Grd2.Rows.Count != 1)
            {
                for (int Row2 = 0; Row2 <= Grd2.Rows.Count - 1; Row2++)
                {
                    if (Sm.GetGrdStr(Grd2, Row2, 0).Length > 0 &&
                        Sm.CompareStr(Sm.GetGrdStr(Grd2, Row2, 0) + Sm.GetGrdStr(Grd2, Row2, 1), Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 6)))
                    {
                        IsExisted = true;
                        ReceivedQty = Sm.GetGrdDec(Grd2, Row2, 7);
                        if (ReceivedQty < 0) ReceivedQty = 0m;
                        break;
                    }
                }
            }
            if (!IsExisted)
            {
                int Row2 = Grd2.Rows.Count - 1;

                Grd2.Cells[Row2, 0].Value = Sm.GetGrdStr(Grd1, Row, 5);
                Grd2.Cells[Row2, 1].Value = Sm.GetGrdStr(Grd1, Row, 6);
                Grd2.Cells[Row2, 2].Value = Sm.GetGrdStr(Grd1, Row, 8);
                Grd2.Cells[Row2, 3].Value = Sm.GetGrdStr(Grd1, Row, 9);
                Grd2.Cells[Row2, 4].Value = Sm.GetGrdStr(Grd1, Row, 15);
                Grd2.Cells[Row2, 5].Value = Grd2.Cells[Row2, 6].Value = Sm.GetGrdDec(Grd1, Row, 14);
                Grd2.Cells[Row2, 7].Value = 0;
                Grd2.Cells[Row2, 8].Value = Sm.GetGrdStr(Grd1, Row, 28);
                Grd2.Cells[Row2, 9].Value = Sm.GetGrdStr(Grd1, Row, 30);
                Grd2.Rows.Add();
            }
            else
            {
                Grd1.Cells[Row, 14].Value = ReceivedQty;
            }

            ComputeQty();
        }

        internal void RemovePOInfo(string PODocNo, string PODNo)
        {
            var IsExisted = false;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                {
                    if (
                        Sm.GetGrdStr(Grd1, Row, 5).Length > 0 &&
                        Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 6), PODocNo + PODNo))
                    {
                        IsExisted = true;
                        break;
                    }
                }
            }
            if (!IsExisted)
            {
                for (int Row = Grd2.Rows.Count - 1; Row >= 0; Row--)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0 &&
                        Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 0) + Sm.GetGrdStr(Grd2, Row, 1), PODocNo + PODNo))
                    {
                        Grd2.Rows.RemoveAt(Row);
                        break;
                    }
                }
            }
            else
                ComputeQty();
        }

        internal void ComputeQty()
        {
            decimal ReceivedQty = 0m;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row2 = 0; Row2 <= Grd2.Rows.Count - 1; Row2++)
                {
                    if (Sm.GetGrdStr(Grd2, Row2, 0).Length > 0)
                    {
                        ReceivedQty = 0m;
                        for (int Row1 = 0; Row1 <= Grd1.Rows.Count - 1; Row1++)
                        {
                            if (
                                !Sm.GetGrdBool(Grd1, Row1, 1) &&
                                Sm.GetGrdStr(Grd1, Row1, 5).Length > 0 &&
                                Sm.CompareStr(
                                    Sm.GetGrdStr(Grd2, Row2, 0) + Sm.GetGrdStr(Grd2, Row2, 1),
                                    Sm.GetGrdStr(Grd1, Row1, 5) + Sm.GetGrdStr(Grd1, Row1, 6)))
                                ReceivedQty += Sm.GetGrdDec(Grd1, Row1, 14);
                        }
                        Grd2.Cells[Row2, 6].Value = ReceivedQty;
                        Grd2.Cells[Row2, 7].Value = Sm.GetGrdDec(Grd2, Row2, 5) - ReceivedQty;
                    }
                }
            }

        }

        private bool IsPOEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length == 0)
            {
                //e.DoDefault = false;
                //Sm.StdMsg(mMsgType.Warning, "PO document number is empty.");
                return true;
            }
            return false;
        }

        private void ComputeOutstandingQty()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DNo, ");
            SQL.AppendLine("(If(A.CancelInd='N', A.Qty, 0)-IfNull(B.Qty2, 0)-IfNull(C.Qty3, 0)+IfNull(D.Qty4, 0)) As OutstandingQty ");
            SQL.AppendLine("From TblPODtl A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select PODocNo As DocNo, PODNo As DNo, Sum(QtyPurchase) As Qty2 ");
            SQL.AppendLine("    From TblRecvVdDtl ");
            SQL.AppendLine("    Where Locate(Concat('##', PODocNo, PODNo, '##'), @Param)>0 ");
            SQL.AppendLine("    And CancelInd='N' Group By PODocNo, PODNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select PODocNo As DocNo, PODNo As DNo, Sum(Qty) As Qty3 ");
            SQL.AppendLine("    From TblPOQtyCancel ");
            SQL.AppendLine("    Where Locate(Concat('##', PODocNo, PODNo, '##'), @Param)>0 ");
            SQL.AppendLine("    And CancelInd='N' Group By PODocNo, PODNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo And A.DNo=C.DNo  ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.PODocNo As DocNo, T3.PODNo As DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As Qty4 ");
            SQL.AppendLine("    From TblDOVdHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T3 ");
            SQL.AppendLine("        On T2.RecvVdDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.RecvVdDNo=T3.DNo ");
            SQL.AppendLine("        And T3.CancelInd='N' ");
            SQL.AppendLine("        And Locate(Concat('##', T3.PODocNo, T3.PODNo, '##'), @Param)>0 ");
            SQL.AppendLine("    Where T1.ReplacedItemInd='Y' And T1.VdCode=@VdCode ");
            SQL.AppendLine("    Group By T3.PODocNo, T3.PODNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            SQL.AppendLine("Where Locate(Concat('##', A.DocNo, A.DNo, '##'), @Param)>0 ");
            SQL.AppendLine("Order By A.DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@Param", GetSelectedPO());
                Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "OutstandingQty" });

                if (dr.HasRows)
                {
                    Grd2.ProcessTab = true;
                    Grd2.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row <= Grd2.Rows.Count - 1; Row++)
                        {
                            if (
                                Sm.GetGrdStr(Grd2, Row, 0).Length > 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 0), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 1), Sm.DrStr(dr, 1)))
                            {
                                Sm.SetGrdValue("N", Grd2, dr, c, Row, 5, 2);
                                break;
                            }
                        }
                    }
                    Grd2.EndUpdate();
                }
                dr.Close();
            }
            ComputeQty();
        }

        internal string GetSelectedPO()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                           "##" +
                            Sm.GetGrdStr(Grd2, Row, 0) +
                            Sm.GetGrdStr(Grd2, Row, 1) +
                            "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void SetLueVdCode(ref DXE.LookUpEdit Lue, string VdCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select VdCode As Col1, VdName As Col2 ");
            SQL.AppendLine("From TblVendor ");

            if (VdCode.Length == 0)
            {
                SQL.AppendLine("Where VdCode In ( ");
                SQL.AppendLine("    Select Distinct A.VdCode ");
                SQL.AppendLine("    From TblPOHdr A ");
                SQL.AppendLine("    Inner Join TblPODtl B ");
                SQL.AppendLine("        On A.DocNo=B.DocNo ");
                SQL.AppendLine("        And B.CancelInd='N' ");
                SQL.AppendLine("        And B.ProcessInd<>'F' ");
                SQL.AppendLine(") ");
            }
            else
            {
                SQL.AppendLine("Where VdCode='" + VdCode + "' ");
            }
            SQL.AppendLine("Order By VdName; ");
            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ShowApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, ");
            SQL.AppendLine("Case Status ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
            SQL.AppendLine("    Concat(Substring(LastUpDt, 7, 2), '/', Substring(LastUpDt, 5, 2), '/', Left(LastUpDt, 4))  ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("Remark  ");
            SQL.AppendLine("From TblDocApproval ");
            SQL.AppendLine("Where DocType='RecvVd' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("Order By ApprovalDNo;");

            var cm = new MySqlCommand();
            //Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                         new string[] { "UserCode", "StatusDesc", "LastUpDt", "Remark" }
                    );
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            "Remark : " + Sm.DrStr(dr, 3) + Environment.NewLine + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        private bool ShowPrintApproval()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select Status From TblRecvVdDtl " +
                    "Where DocNo=@DocNo And Status ='O';"
            };
            //Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Can't print. This document has not been approved.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
                Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue2(Sl.SetLueEntCode), string.Empty);
                Sm.SetLue(LueEntCode, string.Empty);

                if (Sm.GetLue(LueWhsCode).Length > 0)
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select C.EntCode ");
                    SQL.AppendLine("From TblWarehouse A ");
                    SQL.AppendLine("Inner Join TblCostCenter B on A.CCCode=B.CCCode ");
                    SQL.AppendLine("Inner Join TblProfitCenter C on B.ProfitCenterCode=C.ProfitCenterCode ");
                    SQL.AppendLine("Where A.WhsCode=@Param limit 1; ");

                    string mEntCode = Sm.GetValue(SQL.ToString(), Sm.GetLue(LueWhsCode));

                    Sm.SetLue(LueEntCode, mEntCode);
                }
            }
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(SetLueVdCode), string.Empty);
            }
        }

        private void LueVdCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearGrd();
        }

        private void TxtVdDONo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtVdDONo);
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));

                LueUserCode.EditValue = null;
                LueEmpCode.EditValue = null;
                for (int row = 0; row < Grd1.Rows.Count; row++)
                {
                    for (int col = 24; col <= 26; col++)
                        Grd1.Cells[row, col].Value = null;
                }

                if (Sm.GetLue(LueDeptCode).Length != 0)
                {
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueUserCode }, false);
                    SetLueUserCode(ref LueUserCode, Sm.GetLue(LueDeptCode), string.Empty);
                    SetLueUserCode(ref LueEmpCode, Sm.GetLue(LueDeptCode), string.Empty);
                    //SetLueEmpCode(ref LueEmpCode, Sm.GetLue(LueDeptCode), string.Empty);
                }
                else
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueUserCode }, true);
            }
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue2(Sl.SetLueEntCode), string.Empty);
            }
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mIsFilterByCC ? "Y" : "N");
            }
        }

        private void LueUserCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueUserCode, new Sm.RefreshLue3(SetLueUserCode), Sm.GetLue(LueDeptCode), string.Empty);
        }

        private void LueLot_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLot, new Sm.RefreshLue1(SetLueLot));
        }

        private void LueLot_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueLot_Leave(object sender, EventArgs e)
        {
            if (LueLot.Visible && fAccept && fCell.ColIndex == 12)
            {
                if (Sm.GetLue(LueLot).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 12].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 12].Value = LueLot.GetColumnValue("Col1");
                }
                LueLot.Visible = false;
            }
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue1(SetLueBin));
        }

        private void LueBin_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_Leave(object sender, EventArgs e)
        {
            if (LueBin.Visible && fAccept && fCell.ColIndex == 13)
            {
                if (Sm.GetLue(LueBin).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 13].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 13].Value = LueBin.GetColumnValue("Col1");
                }
                LueBin.Visible = false;
            }
        }

        private void LueEmpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmpCode, new Sm.RefreshLue3(SetLueUserCode), Sm.GetLue(LueDeptCode), string.Empty);
        }

        private void LueEmpCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueEmpCode_Leave(object sender, EventArgs e)
        {
            if (LueEmpCode.Visible && fAccept && fCell.ColIndex == 36)
            {
                if (Sm.GetLue(LueEmpCode).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 35].Value = null;
                    Grd1.Cells[fCell.RowIndex, 36].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 35].Value = Sm.GetLue(LueEmpCode);
                    Grd1.Cells[fCell.RowIndex, 36].Value = LueEmpCode.GetColumnValue("Col2");
                }
                LueEmpCode.Visible = false;
            }
        }

        #endregion

        #endregion

        #region Class

        #region Split Outstanding PO

        private class SOPOHdr
        {
            public string PODocNoSource { get; set; }
            public string RevNo { get; set; }
            public string PODocNo2 { get; set; }
        }

        private class SOPODtla
        {
            public string PODocNo { get; set; }
            public string PODNo { get; set; }
            public string PODocNoSource { get; set; }
            public decimal POOutstandingQty { get; set; }
            public decimal RecvQty { get; set; }
            public decimal Balance { get; set; }
        }

        private class SOPODtlb
        {
            public string PODocNo { get; set; }
            public string PODNo { get; set; }
            public string PODocNo2 { get; set; }
            public string PODNo2 { get; set; }
            public decimal Qty2 { get; set; }
        }

        #endregion

        private class LocalDocument
        {
            public string DocNo { set; get; }
            public string LocalDocNo { set; get; }
            public string SeqNo { get; set; }
            public string DeptCode { get; set; }
            public string ItSCCode { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string Revision { get; set; }
        }

        #endregion
    }
}
