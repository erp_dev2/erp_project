﻿#region Update
/*
  04/11/2019 [DITA/IMS] tambah informasi Specifications
  27/10/2020 [ICA/IMS] Ubah Material Request menjadi Purchase Request
  03/09/2021 [VIN/IMS] Ubah Purchase Request mjd Purchase requisition 
 * 
 * */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;


#endregion

namespace RunSystem
{
    public partial class FrmItemVendorCollection : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal string mDocNo = "";
        internal FrmItemVendorCollectionFind FrmFind;
        internal bool mIsFilterBySite = false;
        internal bool mIsBOMShowSpecifications = false;

        #endregion

        #region Constructor

        public FrmItemVendorCollection(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    this.Text = "Item Vendor Collection";
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            string Doctitle = Sm.GetParameter("DocTitle");

            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        (Doctitle=="IMS") ? "Purchase Requisition" : "Material Request",
                        (Doctitle=="IMS") ? "Purchase Requisition Dno" : "Material Request Dno", 
                        //6-10
                        "Item Code",
                        "Item Local Code",
                        "",
                        "Item Name",
                        "Foreign Name",
                        //11-15
                        "Category Code",
                        "Category",
                        "Vendor Code",
                        "Vendor Name",
                        "Remark",

                        //16
                        "Specification"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        50, 50, 20, 150, 80,
                        
                        //6-10
                        100, 120, 20, 200, 150,  
                        
                        //11-15
                        80, 150, 100, 500, 200,

                        //16
                        300
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3, 8 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5, 11, 13 });
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 16 });
            Grd1.Cols[16].Move(11);

        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtDocNo, DteDocDt, DteExpiredDt,  MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, DteExpiredDt, MeeRemark, }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 8, 15 });
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 8 });
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, DteExpiredDt,  MeeRemark,
            });
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
           mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
           mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
        }
    
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemVendorCollectionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
           

        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ItemVendorCollection", "TblItemVendorCollectionHdr");
           
            var cml = new List<MySqlCommand>();

            cml.Add(SaveItemVendorHdr(DocNo));
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 6).Length > 0) cml.Add(SaveItemVendorDtl(DocNo, r));

                string VdCode = Sm.GetGrdStr(Grd1, r, 13);
                string VCode = String.Empty;
                int index = 0;
                int CountData = 0;
                for (int i = 1; i < VdCode.Length; i++)
                {
                    if (VdCode[i] == '#')
                    {
                        VCode = VdCode.Substring(index, i - index);
                        index = i + 1;
                        CountData = CountData + 1;
                        cml.Add(SaveItemVendorDtl2(DocNo, r, CountData, VCode));
                    }
                }
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdEmpty()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }


        private MySqlCommand SaveItemVendorHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblItemVendorCollectionHdr ");
            SQL.AppendLine("(DocNo, DocDt, ExpiredDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @ExpiredDt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@ExpiredDt", Sm.GetDte(DteExpiredDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveItemVendorDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblItemVendorCollectionDtl(DocNo, DNo, MRDocNo, MRDno, ItCode, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @MRDocNo, @MRDno, @ItCode, @Remark, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@MRDocNo", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@MRDNo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 15));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveItemVendorDtl2(string DocNo, int Row, int Count, string VdCode)
        {
            string Tm = string.Empty;
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblItemVendorCollectionDtl2(DocNo, DNo, DNo2, VdCode, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @DNo2, @VdCode, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DNo2", Sm.Right("00" + Count.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            string DNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 6).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid(DNo)) return;

            string DocNo = TxtDocNo.Text;
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelItemVendorDtl(DocNo, DNo));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                IsCancelledItemNotExisted(DNo) 
                ;
        }


        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }


        private MySqlCommand CancelItemVendorDtl(string DocNo, string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblItemvendorCollectionDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DNo In (" + DNo + "); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowItemVendorHdr(DocNo);
                ShowItemVendorDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowItemVendorHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, ExpiredDt, Remark ");
            SQL.AppendLine("From TblItemVendorCollectionHdr ");
            SQL.AppendLine("Where DocNo=@DocNo ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "ExpiredDt", "Remark"     
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetDte(DteExpiredDt, Sm.DrStr(dr, c[2]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        private void ShowItemVendorDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.CancelInd, A.MRDocNo, A.MRDno,  ");
            SQL.AppendLine("A.ItCode, B.ItCodeInternal, B.Itname, B.Foreignname, B.ItCtCode, C.ItCtName, Concat(D.VdCode, '#') As VdCode, D.Vdname,  A.Remark, B.Specification  ");
            SQL.AppendLine("From TblItemVendorCollectionDtl A  ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.itCode  ");
            SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode = C.ItCtCode   ");
            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("(  ");
	        SQL.AppendLine("    Select A.DocNo, A.Dno,   ");
	        SQL.AppendLine("    group_concat(distinct A.VdCode separator '#') vdCode,  ");
	        SQL.AppendLine("    group_concat(distinct B.VdName) vdName  ");
	        SQL.AppendLine("    from tblitemvendorcollectiondtl2 A  ");
	        SQL.AppendLine("    Inner Join TblVendor B On A.VdCode = B.VdCode  ");
	        SQL.AppendLine("    group By DocNo, Dno   ");
            SQL.AppendLine(")D On A.Docno = D.DocNo And A.Dno = D.Dno  ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString() + " Order bY A.Docno, A.DNo;",
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "CancelInd", "MRDocNo", "MRDno", "ItCode", "ItCodeInternal",  
                    //6-10
                    "Itname", "Foreignname", "ItCtCode", "ItCtName", "VdCode", 
                    //11-13
                    "VdName", "Remark" , "Specification"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

       
        #endregion

        #region Additional Method
        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 4)+Sm.GetGrdStr(Grd1, Row, 5)  + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }
        #endregion

        #region Event
        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled)
                Sm.FormShowDialog(new FrmItemVendorCollectionDlg(this));

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length > 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3) Sm.FormShowDialog(new FrmItemVendorCollectionDlg(this));
                }
                else
                {
                  if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
        }
       
        #endregion
      
    }
}
