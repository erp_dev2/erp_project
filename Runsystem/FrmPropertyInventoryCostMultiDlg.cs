﻿#region Update
/*
    11/04/2023 [SET/BBT] Manu Baru 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryCostMultiDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPropertyInventoryCostMulti mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPropertyInventoryCostMultiDlg(FrmPropertyInventoryCostMulti FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetLuePropertyCategory(ref LuePropertyCt);
                Sl.SetLueSiteCode(ref LueSiteCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No",

                    //1-5
                    "",
                    "",
                    "Date of Registration",
                    "Property Code",
                    "Property Name",
                        
                    //6-10
                    "Property Category",
                    "Site",
                    "Cost Center",
                    "Inventory Quantity",
                    "UoM",
                        
                    //11-15
                    "Property Inventory Value",
                    "Remaining Stock Quantity",
                    "CoA#",
                    "CoA Description",
                    "PropertyCtCode",

                    //16-17
                    "SiteCode",
                    "CCCode",
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    20, 20, 130, 170, 200, 
                        
                    //6-10
                    150, 150, 150, 150, 100, 
                        
                    //11-15
                    150, 150, 100, 200, 100,

                    //16-17
                    100, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11, 12 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PropertyCode, A.PropertyName, A.RegistrationDt, A.PropertyCategoryCode, B.PropertyCategoryName, A.PropertyInventoryValue, ");
            SQL.AppendLine("F.SiteName, D.CCName, A.InventoryQty, A.UoMCode, A.RemStockQty, A.RemStockValue, A.AcNo, E.AcDesc, A.SiteCode, A.CCCode ");
            SQL.AppendLine("");
            SQL.AppendLine("From TblPropertyInventoryHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryCategory B On A.PropertyCategoryCode = B.PropertyCategoryCode ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join TblCostCenter D On A.CCCode = D.CCCode ");
            SQL.AppendLine("Left Join TblCOA E On A.AcNo = E.AcNo ");
            SQL.AppendLine("Left Join TblSite F On A.SiteCode = F.SiteCode ");
            SQL.AppendLine("Where A.CancelInd = 'N' AND A.ActInd = 'Y' And A.Status = 'A' AND A.RemStockQty > 0 ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                string Filter = " ";

                var cm = new MySqlCommand();
                Sm.FilterStr(ref Filter, ref cm, TxtProperty.Text, new string[] { "A.PropertyCode", "A.PropertyName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePropertyCt), "A.PropertyCategoryCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By A.PropertyCode;",
                        new string[]
                        { 
                            //0
                            "RegistrationDt",

                            //1-5
                            "PropertyCode", "PropertyName", "PropertyCategoryName", "SiteName", "CCName",

                            //6-10
                            "InventoryQty", "UoMCode", "PropertyInventoryValue", "RemStockQty", "AcNo",

                            //11-14
                            "AcDesc", "PropertyCategoryCode", "SiteCode", "CCCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                        
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                        }, true, false, false, false
                    );

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {

            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDocumentAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd4.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 1, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 2, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 3, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 4, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 5, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 6, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 7, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 8, Grd1, Row2, 11);
                        mFrmParent.Grd4.Cells[Row1, 9].Value = 0m;
                        mFrmParent.Grd4.Cells[Row1, 10].Value = 0m;
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 11, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 12, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 13, Grd1, Row2, 17);
                        mFrmParent.Grd4.Rows.Add();
                    }
                }
                mFrmParent.IsDifferentCCSite();
                mFrmParent.ComputeRemStockQty();
                mFrmParent.ComputeValueBeforeAdd();
                mFrmParent.ComputeCostComponentValue();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsDocumentAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd4.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd4, Index, 1), Sm.GetGrdStr(Grd1, Row, 4)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd4, Index, 2), Sm.GetGrdStr(Grd1, Row, 5))
                    )
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 3).Contains("DODR"))
                {
                    e.DoDefault = false;
                    var f = new FrmDODept(mFrmParent.mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f.ShowDialog();
                }
                else
                {
                    e.DoDefault = false;
                    var f = new FrmDODept2(mFrmParent.mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f.ShowDialog();
                }
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 3).Contains("DODR"))
                {
                    var f = new FrmDODept(mFrmParent.mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmDODept2(mFrmParent.mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method
        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        public void SetLuePropertyCategory(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine(" Select PropertyCategoryCode As Col1, PropertyCategoryName As Col2 From TblPropertyInventoryCategory where Actind = 'Y' ");
                SQL.AppendLine(" Order By PropertyCategoryName ");


                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Event

        private void TxtProperty_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProperty_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Property");
        }

        private void ChkPropertyCt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Property's category");
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LuePropertyCt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePropertyCt, new Sm.RefreshLue1(SetLuePropertyCategory));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0)
                        Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion
    }
}
