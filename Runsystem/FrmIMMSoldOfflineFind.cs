﻿#region Update
/*
    04/09/2019 [TKG] New Application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmIMMSoldOfflineFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmIMMSoldOffline mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmIMMSoldOfflineFind(FrmIMMSoldOffline FrmParent) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select Concat(A.DocNo, A.DocSeqNo) As SoldOffline, A.DocNo, A.DocSeqNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("    E.WhsName, B.Bin, F.LocName, B.Source, C.ProdCode, D.ProdDesc, D.Color, ");
            SQL.AppendLine("    A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("    From TblIMMSoldOfflineHdr A ");
            SQL.AppendLine("    Inner Join TblIMMSoldOfflineDtl B On A.DocNo=B.DocNo And A.DocSeqNo=B.DocSeqNo ");
            SQL.AppendLine("    Inner Join TblImmStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("    Inner Join TblImmProduct D On C.ProdCode=D.ProdCode ");
            SQL.AppendLine("    Inner Join TblWarehouse E On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("    Left Join TblIMMLocation F On B.LocCode=F.LocCode ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=A.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") T ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "DocNo",
                        "DocSeqNo",
                        "Date",
                        "Cancel",

                        //6-10
                        "Warehouse",
                        "Bin",
                        "Location",
                        "Product Code",
                        "Product Description",

                        //11-15
                        "Color",
                        "Created By",
                        "Created Date",
                        "Created Time",
                        "Last Updated By", 

                        //16-17
                        "Last Updated Date", 
                        "Last Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 0, 0, 100, 80, 
                        
                        //6-10
                        200, 150, 150, 100, 200,  
                        
                        //11-15
                        100, 130, 130, 130, 130, 
                        
                        //16-17
                        130, 130
                    }
                );

            Sm.GrdColCheck(Grd1, new int[] { 5 });
            Sm.GrdFormatDate(Grd1, new int[] { 4, 13, 16 });
            Sm.GrdFormatTime(Grd1, new int[] { 14, 17 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 12, 13, 14, 15, 16, 17 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14, 15, 16, 17 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "SoldOffline", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueBin), "Bin", true);
                Sm.FilterStr(ref Filter, ref cm, TxtSource.Text, "Source", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProdCode.Text, new string[] { "ProdCode", "ProdDesc" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "SoldOffline",

                            //1-5
                            "DocNo", "DocSeqNo", "DocDt", "CancelInd", "WhsName", 
                            
                            //6-10
                            "Bin", "LocName", "ProdCode", "ProdDesc", "Color", 

                            //11-14
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 17, 14);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtProdCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Product");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue1(Sl.SetLueBin));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Bin");
        }

        private void TxtSource_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSource_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Source");
        }

        #endregion

        #endregion
    }
}
