﻿#region Update
/*
    04/11/2019 [WED/IMS] new apps
    17/01/2020 [TKG/IMS] beri warna merah apabila delivery date sudah H-14
    25/02/2021 [WED/IMS] Delivered qty ambil dari kolom verified di DO Ct Verified.
                         Outstanding Qty = SO Qty - Delivered Qty
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptDeliveryControl : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptDeliveryControl(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.PONo, F.ProjectCode, F.ProjectName, B.ItCode, E.ItName, E.ItCodeInternal, ");
            SQL.AppendLine("E.Specification, B.DeliveryDt, B.Qty, IfNull(G.VerifiedQty, 0) DeliveredQty, (B.Qty - IFNULL(G.VerifiedQty, 0)) OutstandingQty ");
            SQL.AppendLine("FROM TblSOContractHdr A ");
            SQL.AppendLine("INNER JOIN TblSOContractDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    AND A.Status = 'A' ");
            SQL.AppendLine("    AND A.CancelInd = 'N' ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("INNER JOIN TblBOQHdr C ON A.BOQDocNo = C.DocNo ");
            SQL.AppendLine("INNER JOIN TblLOPHdr D ON C.LOPDocNo = D.DocNo ");
            SQL.AppendLine("INNER JOIN TblItem E ON B.ItCode = E.ItCode ");
            SQL.AppendLine("LEFT JOIN TblProjectGroup F ON D.PGCode = F.PGCode ");
            //SQL.AppendLine("LEFT JOIN ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    SELECT X2.SODocNo, X2.SODNo, Sum(X4.Qty) Qty ");
            //SQL.AppendLine("    FROM TblDRHdr X1 ");
            //SQL.AppendLine("    INNER JOIN TblDRDtl X2 ON X1.DocNo = X2.DocNo AND X1.DocType = '2' ");
            //SQL.AppendLine("        AND X1.CancelInd = 'N' ");
            //SQL.AppendLine("    INNER JOIN TblDOCt2Hdr X3 ON X1.DocNo = X3.DRDocNo ");
            //SQL.AppendLine("    INNER JOIN TblDOCt2Dtl2 X4 ON X3.DocNo = X4.DocNo AND X4.DRDNo = X2.DNo ");
            //SQL.AppendLine("    INNER JOIN TblSOContractHdr X5 ON X2.SODocNo = X5.DocNo AND (X5.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            //SQL.AppendLine("    GROUP BY X2.SODocNo, X2.SODNo ");
            //SQL.AppendLine(") G ON B.DocNo = G.SODocNo AND B.DNo = G.SODNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T4.SOContractDocNo, T4.SOContractDNo, Sum(T3.Qty) VerifiedQty ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr T2 On T1.DOCt2DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DOCt2DocNo Is Not Null ");
            SQL.AppendLine("        And T1.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T3 On T1.DocNo = T3.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl T4 On T2.DocNo = T4.DocNo And T3.DOCt2DNo = T4.DNo ");
            SQL.AppendLine("        And T4.CancelInd = 'N' ");
            SQL.AppendLine("    Group By T4.SOContractDocNo, T4.SOContractDNo ");
                    
            SQL.AppendLine("    Union All ");
                
            SQL.AppendLine("    Select T2.SOContractDocNo, T4.SOContractDNo, Sum(T3.Qty) VerifiedQty ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCtHdr T2 On T1.DOCtDocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DOCtDocNo Is Not Null ");
            SQL.AppendLine("        And T1.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T3 On T1.DocNo = T3.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCtDtl T4 On T2.DocNo = T4.DocNo And T3.DOCtDNo = T4.DNo ");
            SQL.AppendLine("    Group By T2.SOContractDocNo, T4.SOContractDNo ");
            SQL.AppendLine(") G On A.DocNo = G.SOContractDocNo And B.DNo = G.SOContractDNo ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "SO Contract#",
                    "Customer PO#",
                    "Project's Code", 
                    "Project's Name",
                    "Item's Code",

                    //6-10
                    "Item's Name",
                    "Local Code",
                    "Delivery"+Environment.NewLine+"Date",
                    "SO"+Environment.NewLine+"Quantity",
                    "Delivered"+Environment.NewLine+"Quantity",

                    //11
                    "Outstanding"+Environment.NewLine+"Quantity"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 120, 120, 150, 80,

                    //6-10
                    180, 120, 120, 120, 120,

                    //11
                    120
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Grd1.Rows.Count>0) 
                Grd1.Rows[0].BackColor = Color.White;
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 1=1 ";
                DateTime d = DateTime.Now;
                String 
                    CurrentDt = Sm.ServerCurrentDate(),
                    Dt = string.Empty;
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "E.ItName", "E.ItCodeInternal" });
                Sm.FilterStr(ref Filter, ref cm, TxtPONo.Text, "A.PONo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectCode.Text, new string[] { "F.ProjectCode", "F.ProjectName" });

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By A.DocNo;",
                new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "PONo", 
                        "ProjectCode", 
                        "ProjectName", 
                        "ItCode", 
                        "ItName",

                        //6-10
                        "ItCodeInternal", 
                        "DeliveryDt", 
                        "Qty", 
                        "DeliveredQty", 
                        "OutstandingQty"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        if (Sm.GetGrdDec(Grd, Row, 11) > 0m)
                        {
                            Dt = Sm.GetGrdStr(Grd, Row, 8);
                            if (Dt.Length > 0)
                            {
                                Dt = Sm.GetGrdDate(Grd, Row, 8);
                                d = new DateTime(
                                   Int32.Parse(Dt.Substring(0, 4)),
                                   Int32.Parse(Dt.Substring(4, 2)),
                                   Int32.Parse(Dt.Substring(6, 2)),
                                   0, 0, 0
                                   );

                                d = d.AddDays(14);
                                Dt =
                                    d.Year.ToString() +
                                    ("00" + d.Month.ToString()).Substring(("00" + d.Month.ToString()).Length - 2, 2) +
                                    ("00" + d.Day.ToString()).Substring(("00" + d.Day.ToString()).Length - 2, 2);

                                if (Sm.CompareDtTm(Dt, CurrentDt) == -1)
                                    Grd1.Rows[Row].BackColor = Color.Red;
                                else
                                    Grd1.Rows[Row].BackColor = Color.White;
                            }
                        }
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10, 11 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtPONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer PO#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        #endregion

        #endregion
    }
}
