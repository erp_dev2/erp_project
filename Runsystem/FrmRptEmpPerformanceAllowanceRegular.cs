﻿#region Update
/*
    29/11/2019 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpPerformanceAllowanceRegular : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptEmpPerformanceAllowanceRegular(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueDeptCode(ref LueDeptCode);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Methods

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.PayrunCode, A.EmpCode, B.EmpCodeOld, B.EmpName, G.DeptName, H.PosName, ");
            SQL.AppendLine("IF(A.WorkingDay <= IfNull(D.ParValue, 0), (IfNull(D.ParValue, 0) - A.WorkingDay) , 0) AS WorkPermit, ");
            SQL.AppendLine("A.WorkingDay, A.Salary, IFNULL(J.Amt, 0.00) FunctionalAllowance, A.Functional, ");
            SQL.AppendLine("(IFNULL(J.Amt, 0.00) + A.Functional) TotalFunctional, (A.Salary + (IFNULL(J.Amt, 0.00) + A.Functional)) Total, ");
            SQL.AppendLine("((A.Salary + (IFNULL(J.Amt, 0.00) + A.Functional)) / 12) PerformanceAllowanceBudget, IfNull(I.IKP, 0.00) IKP, ");
            SQL.AppendLine("((IfNull(I.IKP, 0.00) * (A.Salary + (IFNULL(J.Amt, 0.00) + A.Functional))) / 100) Conversion, ");
            SQL.AppendLine("IfNull(D.ParValue, 0) NoWorkDayPerMth, IfNull(F.ParValue, 1.5) BruttoPerformanceAllowanceMultiplier ");
            SQL.AppendLine("FROM TblPayrollProcess1 A ");
            SQL.AppendLine("INNER JOIN TblEmployee B ON A.EmpCode = B.EmpCode ");
            SQL.AppendLine("    AND LEFT(A.PayrunCode, 6) = CONCAT(@Yr, @Mth) ");
            if (TxtEmpCode.Text.Length > 0)
                SQL.AppendLine("    And (B.EmpCode Like @EmpCode Or B.EmpName Like @EmpCode Or B.EmpCodeOld Like @EmpCode) ");
            if (Sm.GetLue(LueDeptCode).Length > 0)
                SQL.AppendLine("    And B.DeptCode = @DeptCode ");
            SQL.AppendLine("INNER JOIN TblParameter C ON C.ParCode = 'ADCodeFunctional' ");
            SQL.AppendLine("INNER JOIN TblParameter D ON D.ParCode = 'NoWorkDayPerMth' ");
            SQL.AppendLine("INNER JOIN TblParameter E ON E.ParCode = 'SalaryPerformanceAllowanceMultiplier' ");
            SQL.AppendLine("INNER JOIN TblParameter F ON F.ParCode = 'BruttoPerformanceAllowanceMultiplier' ");
            SQL.AppendLine("LEFT JOIN TblDepartment G ON B.DeptCode = G.DeptCode ");
            SQL.AppendLine("LEFT JOIN TblPosition H ON B.PosCode = H.PosCode ");
            SQL.AppendLine("LEFT JOIN TblEmpPerformanceAllowance I ON I.Yr = @Yr AND I.Mth = @Mth And B.EmpCode = I.EmpCode ");
            SQL.AppendLine("LEFT JOIN TblPayrollProcessAD J ON A.PayrunCode = J.PayrunCode AND A.EmpCode = J.EmpCode AND J.ADCode = C.ParValue ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Employee"+Environment.NewLine+"Code Old",
                    "Employee Code",
                    "Employee Name", 
                    "Departement",
                    "Position", 
                    
                    //6-10
                    "CT",
                    "CP",
                    "CD",
                    "CH",
                    "Time Wasted",

                    //11-15
                    "Work Permit",
                    "Working Day",
                    "Salary",
                    "Functional Allowance",
                    "Functional",

                    //16-20
                    "Total Functional",
                    "Total",
                    "Performance Allowance's" + Environment.NewLine + "Budget",
                    "Performance Allowance's" + Environment.NewLine + "Value",
                    "Brutto",

                    //21-23
                    "Conversion",
                    "Performance Allowance",
                    "Received" + Environment.NewLine + "Performance Allowance"
                },
                new int[] 
                {
                    //0
                    30, 

                    //1-5
                    150, 150, 150, 150, 150,  
                    
                    //6-10
                    120, 120, 120, 120, 120,

                    //11-15
                    150, 150, 150, 150, 150,

                    //16-20
                    150, 150, 150, 150, 150, 

                    //21-23
                    150, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var l = new List<Employee>();
                decimal mSumTotal = 0m, mSumConversion = 0m;

                Process1(ref l, ref mSumTotal, ref mSumConversion);
                if (l.Count > 0)
                {
                    Process2(ref l, ref mSumTotal, ref mSumConversion);
                    Process3(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                l.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void Process1(ref List<Employee> l, ref decimal SumTotal, ref decimal SumConversion)
        {
            var cm = new MySqlCommand();

            Sm.CmParam(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@EmpCode", string.Concat("%", TxtEmpCode.Text, "%"));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = GetSQL() + " Order By B.EmpName; ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "PayrunCode", 
                    //1-5
                    "EmpCode", "EmpCodeOld", "EmpName", "DeptName", "PosName",
                    //6-10
                    "WorkPermit", "WorkingDay", "Salary", "FunctionalAllowance", "Functional", 
                    //11-15
                    "TotalFunctional", "Total", "PerformanceAllowanceBudget", "IKP", "Conversion",
                    //16-17
                    "NoWorkDayPerMth", "BruttoPerformanceAllowanceMultiplier"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        SumTotal += Sm.DrDec(dr, c[12]);
                        SumConversion += Sm.DrDec(dr, c[15]);

                        l.Add(new Employee()
                        {
                            PayrunCode = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]).Trim(),
                            EmpCodeOld = Sm.DrStr(dr, c[2]),
                            EmpName = Sm.DrStr(dr, c[3]),
                            DeptName = Sm.DrStr(dr, c[4]),
                            PosName = Sm.DrStr(dr, c[5]),
                            CT = 0m,
                            CP = 0m,
                            CD = 0m,
                            CH = 0m,
                            TimeWasted = 0m,
                            WorkPermit = Sm.DrDec(dr, c[6]),
                            WorkingDay = Sm.DrDec(dr, c[7]),
                            Salary = Sm.DrDec(dr, c[8]),
                            FunctionalAllowance = Sm.DrDec(dr, c[9]),
                            Functional = Sm.DrDec(dr, c[10]),
                            TotalFunctional = Sm.DrDec(dr, c[11]),
                            Total = Sm.DrDec(dr, c[12]),
                            PerformanceAllowanceBudget = Sm.DrDec(dr, c[13]),
                            PerformanceAllowanceValue = Sm.DrDec(dr, c[14]),
                            Brutto = 0m,
                            Conversion = Sm.DrDec(dr, c[15]),
                            PerformanceAllowance = 0m,
                            ReceivedPerformanceAllowance = 0m,
                            NoWorkDayPerMth = Sm.DrDec(dr, c[16]),
                            BruttoPerformanceAllowanceMultiplier = Sm.DrDec(dr, c[17])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Employee> l, ref decimal SumTotal, ref decimal SumConversion)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                if (SumTotal != 0) l[i].Brutto = (l[i].Total / SumTotal) * l[i].PerformanceAllowanceBudget * l[i].BruttoPerformanceAllowanceMultiplier;
                if (SumConversion != 0) l[i].PerformanceAllowance = (l[i].Conversion / SumConversion) * l[i].Brutto;
                if (l[i].NoWorkDayPerMth != 0) l[i].ReceivedPerformanceAllowance = l[i].PerformanceAllowance * l[i].WorkingDay;
            }
        }

        private void Process3(ref List<Employee> l)
        {
            Sm.ClearGrd(Grd1, false);
            int No = 0;
            for (int i = 0; i < l.Count; ++i)
            {
                Grd1.Rows.Add();
                Grd1.Cells[No, 0].Value = No + 1;
                Grd1.Cells[No, 1].Value = l[i].EmpCode;
                Grd1.Cells[No, 2].Value = l[i].EmpCodeOld;
                Grd1.Cells[No, 3].Value = l[i].EmpName;
                Grd1.Cells[No, 4].Value = l[i].DeptName;
                Grd1.Cells[No, 5].Value = l[i].PosName;
                Grd1.Cells[No, 6].Value = l[i].CT;
                Grd1.Cells[No, 7].Value = l[i].CP;
                Grd1.Cells[No, 8].Value = l[i].CD;
                Grd1.Cells[No, 9].Value = l[i].CH;
                Grd1.Cells[No, 10].Value = l[i].TimeWasted;
                Grd1.Cells[No, 11].Value = l[i].WorkPermit;
                Grd1.Cells[No, 12].Value = l[i].WorkingDay;
                Grd1.Cells[No, 13].Value = l[i].Salary;
                Grd1.Cells[No, 14].Value = l[i].FunctionalAllowance;
                Grd1.Cells[No, 15].Value = l[i].Functional;
                Grd1.Cells[No, 16].Value = l[i].TotalFunctional;
                Grd1.Cells[No, 17].Value = l[i].Total;
                Grd1.Cells[No, 18].Value = l[i].PerformanceAllowanceBudget;
                Grd1.Cells[No, 19].Value = l[i].PerformanceAllowanceValue;
                Grd1.Cells[No, 20].Value = l[i].Brutto;
                Grd1.Cells[No, 21].Value = l[i].Conversion;
                Grd1.Cells[No, 22].Value = l[i].PerformanceAllowance;
                Grd1.Cells[No, 23].Value = l[i].ReceivedPerformanceAllowance;
                No += 1;
            }

            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 });
        }

        #endregion

        #endregion method

        #region Events

        #region Misc Control Events

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #endregion

        #region Class

        private class Employee
        {
            public string PayrunCode { get; set; }
            public string EmpCodeOld { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public decimal CT { get; set; }
            public decimal CP { get; set; }
            public decimal CD { get; set; }
            public decimal CH { get; set; }
            public decimal TimeWasted { get; set; }
            public decimal WorkPermit { get; set; }
            public decimal WorkingDay { get; set; }
            public decimal Salary { get; set; }
            public decimal FunctionalAllowance { get; set; }
            public decimal Functional { get; set; }
            public decimal TotalFunctional { get; set; }
            public decimal Total { get; set; }
            public decimal PerformanceAllowanceBudget { get; set; }
            public decimal PerformanceAllowanceValue { get; set; }
            public decimal Brutto { get; set; }
            public decimal Conversion { get; set; }
            public decimal PerformanceAllowance { get; set; }
            public decimal ReceivedPerformanceAllowance { get; set; }
            public decimal NoWorkDayPerMth { get; set; }
            public decimal BruttoPerformanceAllowanceMultiplier { get; set; }
        }

        #endregion
    }
}
