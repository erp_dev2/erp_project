﻿#region Update
/*
    30/05/2017 [WED] source bisa ambil dari DO To Dept With DO Request
    07/08/2017 [WED] tambah kolom asset display name
    18/08/2020 [DITA/IOK] tambah kolom dan filter department
    08/09/2020 [WED/IOK] Department nya ambil dari Cost Center
* */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAssetItemConsumptionWithValue : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        internal bool mIsShowForeignName = false;
        private bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptAssetItemConsumptionWithValue(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                string CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-7);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);

                iGCellStyle myPercentBarStyle = new iGCellStyle();
                myPercentBarStyle.CustomDrawFlags = TenTec.Windows.iGridLib.iGCustomDrawFlags.Foreground;
                myPercentBarStyle.Flags = ((TenTec.Windows.iGridLib.iGCellFlags)((TenTec.Windows.iGridLib.iGCellFlags.DisplayText | TenTec.Windows.iGridLib.iGCellFlags.DisplayImage)));
                Grd1.Cols[16].CellStyle = myPercentBarStyle;
                Sl.SetLueDeptCode(ref LueDeptCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }            
        }

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");

            string NumberOfInventoryUomCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode='NumberOfInventoryUomCode'");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select F.AssetName, F.DisplayName, T1.DocNo, T1.DocDt, T1.ItCode, ");
            SQL.AppendLine("    D.ItName, T1.Lot, T1.Bin, SUM(T1.Qty) As Qty, SUM(Amount) As Amount, T1.CurCode, ");
            SQL.AppendLine("    SUM(Qty2) As Qty2, SUM(Qty3) As Qty3, D.InventoryUomCode, D.InventoryUomCode2, ");
            SQL.AppendLine("    D.InventoryUomCode3, T1.AssetCode, D.ForeignName, ");
            SQL.AppendLine("    IfNull(G.DeptCode, H.DeptCode) CCDeptCode, IfNull(G.DeptName, H.DeptName) CCDeptName ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select B.AssetCode, A.DocNo, A.DocDt, A.DeptCode, B.ItCode, B.Lot, ");
            SQL.AppendLine("        B.Bin, B.Source, ifnull(B.Qty, 0) As Qty, ifnull(F.Uprice, 0) As UPrice, ");
            SQL.AppendLine("        (ifnull(B.Qty, 0) * ifnull(F.UPrice, 0)) As Amount, ifnull(B.Qty2, 0) As Qty2, ");
            SQL.AppendLine("        Ifnull(B.Qty3, 0) As Qty3, F.CurCode, ");
            SQL.AppendLine("        A.CCCode, A.DORequestDeptDocNo ");
            SQL.AppendLine("        From TblDODeptHdr A ");
            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
            SQL.AppendLine("        Left Join TblstockPrice F On B.ItCode = F.ItCode And B.BatchNo = F.BatchNo And B.Source = F.Source ");
            SQL.AppendLine("        Where A.DocDt Between @DocDt1 And @DocDt2");
            SQL.AppendLine("        Group By A.DocNo, B.DNo, B.ItCode, B.BatchNo, B.Source ");
            SQL.AppendLine("        )T1  ");
            SQL.AppendLine("    Inner Join TblItem D On T1.ItCode=D.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Inner Join TblDepartment E On T1.DeptCode=E.DeptCode ");
            SQL.AppendLine("    Inner Join TblAsset F On T1.AssetCode = F.AssetCode ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select X1.CCCode, X2.DeptCode, X2.DeptName ");
            SQL.AppendLine("        From TblCostCenter X1 ");
            SQL.AppendLine("        Left Join TblDepartment X2 On X1.DeptCode = X2.DeptCode ");
            SQL.AppendLine("    ) G On T1.CCCode = G.CCCode ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select X1.DocNo, X1.CCCode, X2.DeptCode, X3.DeptName ");
            SQL.AppendLine("        From TblDORequestDeptHdr X1 ");
            SQL.AppendLine("        Inner Join TblCostCenter X2 On X1.CCCode = X2.CCCode ");
            SQL.AppendLine("        Left join TblDepartment X3 On X2.DeptCode = X3.DeptCode ");
            SQL.AppendLine("    ) H On T1.DORequestDeptDocNo = H.DocNo ");
            SQL.AppendLine("    Group By T1.AssetCode, T1.ItCode, T1.DocNo, T1.DocDt, T1.Lot, ");
            SQL.AppendLine("    T1.Bin, D.InventoryUomCode, G.DeptCode, H.DeptCode, G.DeptName, ");
            SQL.AppendLine("    H.DeptName Order By T1.Qty Desc ");
            SQL.AppendLine(") Y1 "); 

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Asset Name",
                        "Document#",
                        "Date",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",                      
                                            
                        //6-10
                        "Lot",
                        "Bin",
                        "Quantity",
                        "UoM",
                        "Quantity",

                        //11-15
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Amount",
                        "Currency",

                        //16-19
                        "Percentage",
                        "Foreign Name",
                        "Display Name",
                        "Cost Center's"+Environment.NewLine+"Department"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 150, 100, 100, 250,   
                        
                        //6-10
                        80, 80, 100, 80, 100,  
                        
                        //11-15
                        80, 100, 80, 100, 80, 
                        
                        //16-19
                        100, 170, 200, 200
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10, 12, 14 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            if (mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 4, 10, 11, 12, 13, 18 }, false);
                Grd1.Cols[17].Move(6);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 4, 10, 11, 12, 13, 17, 18 }, false);
            }

            Grd1.Cols[18].Move(2);
            Grd1.Cols[19].Move(5);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 18 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 10, 11 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13 }, true);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0=0 ";
                decimal TotalAmt = 0;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "Y1.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "Y1.AssetCode", "Y1.AssetName", "Y1.DisplayName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "Y1.CCDeptCode", true);

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 10, 12, 14 });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        {
                            //0
                            "AssetName",  
                            
                            //1-5
                            "DocNo", "DocDt", "ItCode", "ItName",  "Lot",   
                            
                            //6-10
                            "Bin", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2",  
                            
                            //11-15
                            "Qty3", "InventoryUomCode3", "Amount", "CurCode", "ForeignName",

                            //16-17
                            "DisplayName", "CCDeptName"
                 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 17);
                            TotalAmt = TotalAmt + Sm.DrDec(dr, 8);
                        }, true, true, false, false
                    );

                Grd1.Rows.CollapseAll();
                for (int intX = 0; intX < Grd1.Rows.Count; intX++)
                {
                    if (Sm.GetGrdDec(Grd1, intX, 8) != 0)
                        if(TotalAmt !=0)
                        Grd1.Cells[intX, 16].Value = Convert.ToDouble(Sm.GetGrdDec(Grd1, intX, 14) / TotalAmt);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }

        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            if (e.ColIndex == 16)
            {
                object myObjValue = Grd1.Cells[e.RowIndex, e.ColIndex].Value;
                if (myObjValue == null)
                    return;

                Rectangle myBounds = e.Bounds;
                myBounds.Inflate(-2, -2);
                myBounds.Width = myBounds.Width - 1;
                myBounds.Height = myBounds.Height - 1;
                if (myBounds.Width > 0)
                {
                    e.Graphics.FillRectangle(Brushes.Bisque, myBounds);
                    double myValue = (double)myObjValue;
                    int myWidth = (int)(myBounds.Width * myValue);
                    e.Graphics.FillRectangle(Brushes.SandyBrown, myBounds.X, myBounds.Y, myWidth, myBounds.Height);

                    e.Graphics.DrawRectangle(Pens.SaddleBrown, myBounds);

                    StringFormat myStringFormat = new StringFormat();
                    myStringFormat.Alignment = StringAlignment.Center;
                    myStringFormat.LineAlignment = StringAlignment.Center;
                    e.Graphics.DrawString(string.Format("{0:F2}%", myValue * 100), Font, SystemBrushes.ControlText, new RectangleF(myBounds.X, myBounds.Y, myBounds.Width, myBounds.Height), myStringFormat);
                }
            }
        }

       
        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion


        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0)
                DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        
        #endregion

        
    }
}
