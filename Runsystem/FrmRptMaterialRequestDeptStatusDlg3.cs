﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
#endregion

namespace RunSystem
{
    public partial class FrmRptMaterialRequestDeptStatusDlg3 : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptMaterialRequestDeptStatusDlg2 mFrmParent;
        private string mSQL = string.Empty, mDNo = string.Empty, mDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMaterialRequestDeptStatusDlg3(FrmRptMaterialRequestDeptStatusDlg2 FrmParent, string DNo, string DocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDNo = DNo;
            mDocNo = DocNo;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            SetFormControl(mState.View);
            SetGrd(); 
            ShowData();
        }

        private void SetFormControl(RunSystem.mState state)
        {

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtQty
                    }, true);
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo,             
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
               TxtQty
            }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "Approve",

                        //1-3
                        "Status", 
                        "Approve Date",
                        "Remark"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.SetGrdProperty(Grd1, true);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3 });
        }

        private void HideInfoInGrd()
        {
            Sm.SetGrdAutoSize(Grd1);
        }

        #endregion

        #region Show data
        public void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Select B.UserName, ");
                SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' ");
                SQL.AppendLine("When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As Status, A.LastUpDt, A.Remark ");
                SQL.AppendLine("From TblDocApproval A ");
                SQL.AppendLine("Inner Join TblUser B on A.UserCode = B.UserCode ");

                mSQL = SQL.ToString();

                string Filter = "Where A.DocType = 'PORequest' And A.DocNo = '" + mDocNo + "' And A.DNo = '" + mDNo + "' ";

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " ",
                        new string[]
                        {
                            //0
                            "UserName",
                            //1-5
                            "Status", "LastUpDt", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                        }, false, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion
    }
}
