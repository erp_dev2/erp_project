﻿#region Update
/*
    30/10/2019 [WED/IMS] Site tidak mandatory di LOP
    04/11/2019 [WED/IMS] BOQDocNo di remark di query, tp lupa alesannya kenapa ada BOQ di query itu
    15/02/2020 [TKG/IMS] menggunakan proses BOM yg baru
    06/07/2021 [VIN/IMS] button project di disable
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest3Dlg5 : RunSystem.FrmBase4
    {
        #region Field

        private FrmMaterialRequest3 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmMaterialRequest3Dlg5(FrmMaterialRequest3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, D.CtName, E.SiteName, F.PGCode, F.ProjectCode, F.ProjectName, A.PONo ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo And A.DocType = '2' ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.DocNo Not In ");
            SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select Distinct X2.SOCDocNo ");
            SQL.AppendLine("        Select Distinct X2.DocNo ");
            SQL.AppendLine("        From TblProjectImplementationHdr X1 ");
            //SQL.AppendLine("        Inner Join TblSOContractRevisionHdr X2 On X1.SOContractDocNo = X2.DocNo ");
            SQL.AppendLine("        Inner Join TblSOContractHdr X2 On X1.SOContractDocNo = X2.DocNo ");
            SQL.AppendLine("        Where X1.CancelInd = 'N' And X1.Status In ('O', 'A') ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblLOPHdr C On B.LOPDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer D On C.CtCode = D.CtCode ");
            SQL.AppendLine("Left Join TblSite E On C.SiteCode = E.SiteCode ");
            SQL.AppendLine("Left Join TblProjectGroup F On C.PGCode=F.PGCode ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "",
                    "Date",
                    "Customer",
                    "Site",

                    //6-9
                    "Project Group Code",
                    "Project's Code",
                    "Project's Name",
                    "Customer's PO#"
                },
                new int[]
                {
                    50,
                    150, 20, 80, 200, 200,
                    0, 150, 200, 150
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 6 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSOContract2(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.Text = Sm.GetMenuDesc("FrmSOContract2");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSOContract2(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.Text = Sm.GetMenuDesc("FrmSOContract2");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 1=1 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocDt Desc, A.DocNo; ",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CtName", "SiteName", "PGCode", "ProjectCode", 
                        
                        //6-7
                        "ProjectName", "PONo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtSOCDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.mPGCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                mFrmParent.TxtProjectCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                mFrmParent.TxtProjectName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
                mFrmParent.TxtPONo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9);
                mFrmParent.BtnPGCode.Enabled = false;
                mFrmParent.ClearGrd();
                this.Hide();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

        #endregion

    }
}
