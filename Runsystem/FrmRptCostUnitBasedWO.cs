﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptCostUnitBasedWO : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private int mNumberOfInventoryUomCode = 1;

        #endregion

        #region Constructor

        public FrmRptCostUnitBasedWO(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueCCCode(ref LueCCCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
           
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);

        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.WODocNo, D.WhsName, E.DeptName, F.CCName, B.ItCode, C.ItName,  B.BatchNo, B.Qty, C.InventoryUomCode, ");
            SQL.AppendLine("B.Qty2, C.InventoryUomCode2, G.AssetName, G.DisplayName, B.Remark ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
            SQL.AppendLine("Inner Join TblDepartment E On A.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblCostCenter F On A.CCCode=F.CCCode "); 
            SQL.AppendLine("Left Join TblAsset G On B.AssetCode =G.AssetCode ");
            SQL.AppendLine("Where A.WODocNo>0 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "DO Document", 
                        "Document Date",
                        "WO Document",
                        "",
                        "Warehouse",

                        //6-10
                        "Department",
                        "Cost Center",
                        "Item Code",
                        "Item Name",
                        "Batch",

                        //11-15
                        "Quantity",
                        "Uom",
                        "Quantity 2",
                        "Uom 2",
                        "Asset Name",

                        //16-17
                        "Display Name",
                        "Remark",
                      
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 120, 150, 20, 250,

                        //6-10
                        250, 200, 100, 250, 250,

                        //11-15
                        150, 100, 150, 100, 150,

                        //16-17
                        250, 200

                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 13, 14 }, false); 
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.SetGrdProperty(Grd1, false);
            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "And 0=0 ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "A.CCCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] {"B.ItCode", "C.ItName" });

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By A.DocNo ;",
                new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "WODocNo", "WhsName", "DeptName", "CCName",

                        //6-10
                        "ItCode", "ItName", "BatchNo", "Qty", "InventoryUomCode",

                        //11-15
                        "Qty2", "InventoryUomCode2", "AssetName", "DisplayName", "Remark"


                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                  
                }, true, false, false, false
                );
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }
       
        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(Sl.SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        #endregion

    }
}
