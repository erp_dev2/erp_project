﻿#region Update
/*
    30/08/2020 [TKG/IOK] actual item hanya yg aktif saja.
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmRecvRawMaterial2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmRecvRawMaterial2Find FrmFind;
        iGCell fCell;
        bool fAccept;
        string
            mItCtRMPActual = string.Empty,
            mSQLForActualItCode = string.Empty,
            mPosCodeQC = string.Empty;
        decimal mRMInspDefaultQueueDt = 60;
        internal byte mType = 1; //1=Log;2=Balok

        #endregion

        #region Constructor

        public FrmRecvRawMaterial2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Material Inspection";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                GetParameter();
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                SetTabControls("2", ref LueShelf2, ref LueWhsCode2, ref LueItCode2);
                SetTabControls("3", ref LueShelf3, ref LueWhsCode3, ref LueItCode3);
                SetTabControls("4", ref LueShelf4, ref LueWhsCode4, ref LueItCode4);
                SetTabControls("5", ref LueShelf5, ref LueWhsCode5, ref LueItCode5);
                SetTabControls("6", ref LueShelf6, ref LueWhsCode6, ref LueItCode6);
                SetTabControls("7", ref LueShelf7, ref LueWhsCode7, ref LueItCode7);
                SetTabControls("8", ref LueShelf8, ref LueWhsCode8, ref LueItCode8);
                SetTabControls("9", ref LueShelf9, ref LueWhsCode9, ref LueItCode9);
                SetTabControls("10", ref LueShelf10, ref LueWhsCode10, ref LueItCode10);
                SetTabControls("11", ref LueShelf11, ref LueWhsCode11, ref LueItCode11);
                SetTabControls("12", ref LueShelf12, ref LueWhsCode12, ref LueItCode12);
                SetTabControls("13", ref LueShelf13, ref LueWhsCode13, ref LueItCode13);
                SetTabControls("14", ref LueShelf14, ref LueWhsCode14, ref LueItCode14);
                SetTabControls("15", ref LueShelf15, ref LueWhsCode15, ref LueItCode15);
                SetTabControls("1", ref LueShelf1, ref LueWhsCode1, ref LueItCode1);

                mItCtRMPActual = Sm.GetParameter("ItCtRMPActual"+((mType==2)?"2":""));
                SetSQLForActualItCode();
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = 
                    BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd, DteDocDt, LueQueueNo, TxtTotal, 
                        LueEmpCode1, LueEmpCode2, TxtCreateBy, MeeRemark, 
                        LueShelf1, LueShelf2, LueShelf3, LueShelf4, LueShelf5, 
                        LueShelf6, LueShelf7, LueShelf8, LueShelf9, LueShelf10,
                        LueShelf11, LueShelf12, LueShelf13, LueShelf14, LueShelf15,
                        LueWhsCode1, LueWhsCode2, LueWhsCode3, LueWhsCode4, LueWhsCode5, 
                        LueWhsCode6, LueWhsCode7, LueWhsCode8, LueWhsCode9, LueWhsCode10,
                        LueWhsCode11, LueWhsCode12, LueWhsCode13, LueWhsCode14, LueWhsCode15,
                        TxtTotal1, TxtTotal2, TxtTotal3, TxtTotal4, TxtTotal5, 
                        TxtTotal6, TxtTotal7, TxtTotal8, TxtTotal9, TxtTotal10,
                        TxtTotal11, TxtTotal12, TxtTotal13, TxtTotal14, TxtTotal15,
                        TxtLabelQty1, TxtLabelQty2, TxtLabelQty3, TxtLabelQty4, TxtLabelQty5, 
                        TxtLabelQty6, TxtLabelQty7, TxtLabelQty8, TxtLabelQty9, TxtLabelQty10,
                        TxtLabelQty11, TxtLabelQty12, TxtLabelQty13, TxtLabelQty14, TxtLabelQty15
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd11, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd12, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd13, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd14, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd15, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd16, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd17, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd18, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd19, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd20, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd21, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd22, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd23, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd24, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd25, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueQueueNo, LueEmpCode1, LueEmpCode2, MeeRemark, 
                        LueShelf1, LueShelf2, LueShelf3, LueShelf4, LueShelf5, 
                        LueShelf6, LueShelf7, LueShelf8, LueShelf9, LueShelf10,
                        LueShelf11, LueShelf12, LueShelf13, LueShelf14, LueShelf15,
                        LueWhsCode1, LueWhsCode2, LueWhsCode3, LueWhsCode4, LueWhsCode5, 
                        LueWhsCode6, LueWhsCode7, LueWhsCode8, LueWhsCode9, LueWhsCode10,
                        LueWhsCode11, LueWhsCode12, LueWhsCode13, LueWhsCode14, LueWhsCode15,
                        TxtLabelQty1, TxtLabelQty2, TxtLabelQty3, TxtLabelQty4, TxtLabelQty5, 
                        TxtLabelQty6, TxtLabelQty7, TxtLabelQty8, TxtLabelQty9, TxtLabelQty10,
                        TxtLabelQty11, TxtLabelQty12, TxtLabelQty13, TxtLabelQty14, TxtLabelQty15
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd11, new int[] { 0, 3, 4, 8 });
                    Sm.GrdColReadOnly(false, true, Grd12, new int[] { 0, 3, 4, 8 });
                    Sm.GrdColReadOnly(false, true, Grd13, new int[] { 0, 3, 4, 8 });
                    Sm.GrdColReadOnly(false, true, Grd14, new int[] { 0, 3, 4, 8 });
                    Sm.GrdColReadOnly(false, true, Grd15, new int[] { 0, 3, 4, 8 });
                    Sm.GrdColReadOnly(false, true, Grd16, new int[] { 0, 3, 4, 8 });
                    Sm.GrdColReadOnly(false, true, Grd17, new int[] { 0, 3, 4, 8 });
                    Sm.GrdColReadOnly(false, true, Grd18, new int[] { 0, 3, 4, 8 });
                    Sm.GrdColReadOnly(false, true, Grd19, new int[] { 0, 3, 4, 8 });
                    Sm.GrdColReadOnly(false, true, Grd20, new int[] { 0, 3, 4, 8 });
                    Sm.GrdColReadOnly(false, true, Grd21, new int[] { 0, 3, 4, 8 });
                    Sm.GrdColReadOnly(false, true, Grd22, new int[] { 0, 3, 4, 8 });
                    Sm.GrdColReadOnly(false, true, Grd23, new int[] { 0, 3, 4, 8 });
                    Sm.GrdColReadOnly(false, true, Grd24, new int[] { 0, 3, 4, 8 });
                    Sm.GrdColReadOnly(false, true, Grd25, new int[] { 0, 3, 4, 8 });
                    DteDocDt.Focus();
                    break;

                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueEmpCode1, LueEmpCode2, MeeRemark, 
                        LueShelf1, LueShelf2, LueShelf3, LueShelf4, LueShelf5, 
                        LueShelf6, LueShelf7, LueShelf8, LueShelf9, LueShelf10,
                        LueWhsCode1, LueWhsCode2, LueWhsCode3, LueWhsCode4, LueWhsCode5, 
                        LueWhsCode6, LueWhsCode7, LueWhsCode8, LueWhsCode9, LueWhsCode10,
                        LueWhsCode11, LueWhsCode12, LueWhsCode13, LueWhsCode14, LueWhsCode15,
                        TxtLabelQty1, TxtLabelQty2, TxtLabelQty3, TxtLabelQty4, TxtLabelQty5, 
                        TxtLabelQty6, TxtLabelQty7, TxtLabelQty8, TxtLabelQty9, TxtLabelQty10,
                        TxtLabelQty11, TxtLabelQty12, TxtLabelQty13, TxtLabelQty14, TxtLabelQty15
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueQueueNo, LueEmpCode1, LueEmpCode2, 
                TxtCreateBy, MeeRemark,
                LueShelf1, LueShelf2, LueShelf3, LueShelf4, LueShelf5, 
                LueShelf6, LueShelf7, LueShelf8, LueShelf9, LueShelf10,
                LueShelf11, LueShelf12, LueShelf13, LueShelf14, LueShelf15, 
                LueWhsCode1, LueWhsCode2, LueWhsCode3, LueWhsCode4, LueWhsCode5, 
                LueWhsCode6, LueWhsCode7, LueWhsCode8, LueWhsCode9, LueWhsCode10,
                LueWhsCode11, LueWhsCode12, LueWhsCode13, LueWhsCode14, LueWhsCode15
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtTotal, 
                TxtTotal1, TxtTotal2, TxtTotal3, TxtTotal4, TxtTotal5, 
                TxtTotal6, TxtTotal7, TxtTotal8, TxtTotal9, TxtTotal10,
                TxtTotal11, TxtTotal12, TxtTotal13, TxtTotal14, TxtTotal15, 
            }, 0);

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtLabelQty1, TxtLabelQty2, TxtLabelQty3, TxtLabelQty4, TxtLabelQty5, 
                TxtLabelQty6, TxtLabelQty7, TxtLabelQty8, TxtLabelQty9, TxtLabelQty10,
                TxtLabelQty11, TxtLabelQty12, TxtLabelQty13, TxtLabelQty14, TxtLabelQty15,
            }, 1);
            ChkCancelInd.Checked = false;

            ClearGrd2(new List<iGrid>(){
                Grd11, Grd12, Grd13, Grd14, Grd15,
                Grd16, Grd17, Grd18, Grd19, Grd20,
                Grd21, Grd22, Grd23, Grd24, Grd25
            });
        }

        private void ClearGrd1(List<iGrid> ListOfGrd)
        {
            ListOfGrd.ForEach(Grd =>
            {
                Sm.ClearGrd(Grd, true);
                Sm.FocusGrd(Grd, 0, 1);
            });
        }

        private void ClearGrd2(List<iGrid> ListOfGrd)
        {
            ListOfGrd.ForEach(Grd =>
                {
                    Sm.ClearGrd(Grd, true);
                    Sm.SetGrdNumValueZero(ref Grd, 0, new int[] { 2, 3, 4, 8 });
                    Sm.FocusGrd(Grd, 0, 0);
                });
        }

        private void SetGrd()
        {
            SetGrdItem(new List<iGrid> 
                { 
                    Grd11, Grd12, Grd13, Grd14, Grd15,
                    Grd16, Grd17, Grd18, Grd19, Grd20,
                    Grd21, Grd22, Grd23, Grd24, Grd25
                });

            SetGrdCanvas();
        }

        private void SetGrdItem(List<iGrid> ListofGrd)
        {
            string
                Label1 = (mType == 1) ? "Length" : "Length*Height",
                Label2 = (mType == 1) ? "Diameter" : "Width";

            ListofGrd.ForEach(Grd =>
            {
                Grd.Cols.Count = 9;
                Grd.FrozenArea.ColCount = 2;
                Grd.ReadOnly = false;
                Sm.GrdHdrWithColWidth(
                        Grd,
                        new string[] 
                        {
                            //0
                            "Item Code",

                            //1-5
                            "Item Name", Label1, Label2, "Quantity", "UoM", 
 
                            //6-9
                            "Remark", 
                            "Actual"+Environment.NewLine+"Item Code", 
                            "Actual"+Environment.NewLine+"Length"
                        },
                        new int[] 
                        { 
                            100, 
                            200, 150, 80, 80, 60, 
                            150, 100, 80
                        }
                    );
                Sm.GrdFormatDec(Grd, new int[] { 2, 3, 8 }, 2);
                Sm.GrdFormatDec(Grd, new int[] { 4 }, 1);
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRecvRawMaterial2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueQueueNo(ref LueQueueNo, string.Empty);
                SetLueEmpCode(ref LueEmpCode1, string.Empty);
                SetLueEmpCode(ref LueEmpCode2, string.Empty);
                SetUserName();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RecvRawMaterial2", "TblRecvRawMaterial2Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRecvRawMaterial2Hdr(DocNo));

            int DNo = 0;
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "1", Grd11);
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "2", Grd12);
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "3", Grd13);
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "4", Grd14);
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "5", Grd15);
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "6", Grd16);
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "7", Grd17);
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "8", Grd18);
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "9", Grd19);
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "10", Grd20);
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "11", Grd21);
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "12", Grd22);
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "13", Grd23);
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "14", Grd24);
            SaveRecvRawMaterial2Dtl(ref cml, DocNo, ref DNo, "15", Grd25);
            
            Sm.ExecCommands(cml);
            BtnInsertClick(sender, e);
        }

        private void SaveRecvRawMaterial2Dtl(ref List<MySqlCommand> cml, string DocNo, ref int DNo, string SectionNo, iGrid Grd)
        {
            for (int Row = 0; Row < Grd.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd, Row, 0).Length > 0 && Sm.GetGrdStr(Grd, Row, 1).Length > 0)
                    cml.Add(SaveRecvRawMaterial2Dtl(DocNo, ref DNo, SectionNo, Grd, Row));
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueQueueNo, "Nomor antrian") ||
                Sm.IsLueEmpty(LueEmpCode1, "QC personel") ||
                Sm.IsLueEmpty(LueShelf1, "Rak [1]") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsItemInfoNotValid(LueShelf1, LueWhsCode1, Grd11, "1") ||
                IsItemInfoNotValid(LueShelf2, LueWhsCode2, Grd12, "2") ||
                IsItemInfoNotValid(LueShelf3, LueWhsCode3, Grd13, "3") ||
                IsItemInfoNotValid(LueShelf4, LueWhsCode4, Grd14, "4") ||
                IsItemInfoNotValid(LueShelf5, LueWhsCode5, Grd15, "5") ||
                IsItemInfoNotValid(LueShelf6, LueWhsCode6, Grd16, "6") ||
                IsItemInfoNotValid(LueShelf7, LueWhsCode7, Grd17, "7") ||
                IsItemInfoNotValid(LueShelf8, LueWhsCode8, Grd18, "8") ||
                IsItemInfoNotValid(LueShelf9, LueWhsCode9, Grd19, "9") ||
                IsItemInfoNotValid(LueShelf10, LueWhsCode10, Grd20, "10") ||
                IsItemInfoNotValid(LueShelf11, LueWhsCode11, Grd21, "11") ||
                IsItemInfoNotValid(LueShelf12, LueWhsCode12, Grd22, "12") ||
                IsItemInfoNotValid(LueShelf13, LueWhsCode13, Grd23, "13") ||
                IsItemInfoNotValid(LueShelf14, LueWhsCode14, Grd24, "14") ||
                IsItemInfoNotValid(LueShelf15, LueWhsCode15, Grd25, "15") ||
                IsBinNotValid(LueShelf1) ||
                IsBinNotValid(LueShelf2) ||
                IsBinNotValid(LueShelf3) ||
                IsBinNotValid(LueShelf4) ||
                IsBinNotValid(LueShelf5) ||
                IsBinNotValid(LueShelf6) ||
                IsBinNotValid(LueShelf7) ||
                IsBinNotValid(LueShelf8) ||
                IsBinNotValid(LueShelf9) ||
                IsBinNotValid(LueShelf10) ||
                IsBinNotValid(LueShelf11) ||
                IsBinNotValid(LueShelf12) ||
                IsBinNotValid(LueShelf13) ||
                IsBinNotValid(LueShelf14) ||
                IsBinNotValid(LueShelf15) 
                ;
        }

        //private bool IsQueueNoNotValid()
        //{
        //    var QueueNo = Sm.GetLue(LueQueueNo);
        //    return Sm.IsDataExist(
        //        "Select DocNo From TblLoadingQueue " +
        //        "Where RecvRawMaterial2Ind='F' And DocNo=@Param;",
        //        QueueNo,
        //        "Queue# : " + QueueNo + Environment.NewLine +
        //        "This queue# already inspected.");
        //}

        private bool IsBinNotValid(DXE.LookUpEdit LueShelf)
        {
            var Bin = Sm.GetLue(LueShelf);
            if (Bin.Length == 0) return false;

            var SQL = new StringBuilder();
            var Bins = new StringBuilder();

            for (int i = 1; i <= 15; i++)
            {
                if (i != 1) Bins.AppendLine(" Or ");
                Bins.AppendLine(" (Shelf"+ i + " Is Not Null And Shelf"+ i + "=@Bin) "); 
            }

            SQL.AppendLine("Select QueueNo ");
            SQL.AppendLine("From TblRecvRawMaterial2Hdr ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And QueueNo=@QueueNo ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("And DocNo<>@DocNo ");
            SQL.AppendLine("And (");
            SQL.AppendLine(Bins.ToString());
            SQL.AppendLine(");");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@QueueNo", Sm.GetLue(LueQueueNo));
            Sm.CmParam<String>(ref cm, "@Bin", Bin);
            if (TxtDocNo.Text.Length > 0)
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                   "Bin : " + Bin + Environment.NewLine +
                   "This bin already inspected in another document.");
                return true;
            }
            return false;
        }

        private bool IsItemInfoNotValid(DXE.LookUpEdit LueShelf, DXE.LookUpEdit LueWhsCode, iGrid Grd, string Number)
        {
            if (Grd.Rows.Count > 1 && 
                (Sm.IsLueEmpty(LueShelf, "Bin# " + Number)) 
                ) 
                return true;

            if (Grd.Rows.Count == 1 && ((Sm.GetLue(LueShelf)).Length != 0))
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Bin : " + Number + Environment.NewLine +
                    "Warehouse : " + Number + Environment.NewLine + Environment.NewLine + 
                    "Item still empty.");
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd11.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 data item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (
                IsItemGrdValueNotValid(Grd11, "1") ||
                IsItemGrdValueNotValid(Grd12, "2") ||
                IsItemGrdValueNotValid(Grd13, "3") ||
                IsItemGrdValueNotValid(Grd14, "4") ||
                IsItemGrdValueNotValid(Grd15, "5") ||
                IsItemGrdValueNotValid(Grd16, "6") ||
                IsItemGrdValueNotValid(Grd17, "7") ||
                IsItemGrdValueNotValid(Grd18, "8") ||
                IsItemGrdValueNotValid(Grd19, "9") ||
                IsItemGrdValueNotValid(Grd20, "10") ||
                IsItemGrdValueNotValid(Grd21, "11") ||
                IsItemGrdValueNotValid(Grd22, "12") ||
                IsItemGrdValueNotValid(Grd23, "13") ||
                IsItemGrdValueNotValid(Grd24, "14") ||
                IsItemGrdValueNotValid(Grd25, "15") ||
                IsBinNotValid(LueShelf1) ||
                IsBinNotValid(LueShelf2) ||
                IsBinNotValid(LueShelf3) ||
                IsBinNotValid(LueShelf4) ||
                IsBinNotValid(LueShelf5) ||
                IsBinNotValid(LueShelf6) ||
                IsBinNotValid(LueShelf7) ||
                IsBinNotValid(LueShelf8) ||
                IsBinNotValid(LueShelf9) ||
                IsBinNotValid(LueShelf10) ||
                IsBinNotValid(LueShelf11) ||
                IsBinNotValid(LueShelf12) ||
                IsBinNotValid(LueShelf13) ||
                IsBinNotValid(LueShelf14) ||
                IsBinNotValid(LueShelf15)
                ) 
                
                return true;
            
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (
                IsGrdExceedMaxRecords(Grd11, "Bahan baku #1") ||
                IsGrdExceedMaxRecords(Grd12, "Bahan baku #2") ||
                IsGrdExceedMaxRecords(Grd13, "Bahan baku #3") ||
                IsGrdExceedMaxRecords(Grd14, "Bahan baku #4") ||
                IsGrdExceedMaxRecords(Grd15, "Bahan baku #5") ||
                IsGrdExceedMaxRecords(Grd16, "Bahan baku #6") ||
                IsGrdExceedMaxRecords(Grd17, "Bahan baku #7") ||
                IsGrdExceedMaxRecords(Grd18, "Bahan baku #8") ||
                IsGrdExceedMaxRecords(Grd19, "Bahan baku #9") ||
                IsGrdExceedMaxRecords(Grd20, "Bahan baku #10") ||
                IsGrdExceedMaxRecords(Grd21, "Bahan baku #11") ||
                IsGrdExceedMaxRecords(Grd22, "Bahan baku #12") ||
                IsGrdExceedMaxRecords(Grd23, "Bahan baku #13") ||
                IsGrdExceedMaxRecords(Grd24, "Bahan baku #14") ||
                IsGrdExceedMaxRecords(Grd25, "Bahan baku #15")
                ) return true;
            
            return false;
        }

        private bool IsGrdExceedMaxRecords(iGrid Grd, string Msg)
        {
            if (Grd.Rows.Count > 999)
            {
                Sm.StdMsg(mMsgType.Warning,
                    Msg + " data entered (" + (Grd.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsItemGrdValueNotValid(iGrid Grd, string Msg)
        {
            if (Grd.Rows.Count > 1)
            {
                SetActualItCode(Grd);
                for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd, Row, 0, false, "Item #" + Msg + " is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd, Row, 3, true, "Bahan baku #" + Msg + Environment.NewLine + "Diameter should be greater than 0.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd, Row, 4, true, "Bahan baku #" + Msg + Environment.NewLine + "Quantity should be greater than 0.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd, Row, 7, false, 
                        "Actual Item #" + Msg + " is empty." + Environment.NewLine +
                        "Please contact Purchaser."
                        )) return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveRecvRawMaterial2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRecvRawMaterial2Hdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, DocType, QueueNo, EmpCode1, EmpCode2, ");
            SQL.AppendLine("Shelf1, Shelf2, Shelf3, Shelf4, Shelf5, Shelf6, Shelf7, Shelf8, Shelf9, Shelf10,Shelf11, Shelf12, Shelf13, Shelf14, Shelf15, ");
            SQL.AppendLine("WhsCode1, WhsCode2, WhsCode3, WhsCode4, WhsCode5, WhsCode6, WhsCode7, WhsCode8, WhsCode9, WhsCode10,WhsCode11, WhsCode12, WhsCode13, WhsCode14, WhsCode15, ");
            SQL.AppendLine("LabelQty1, LabelQty2, LabelQty3, LabelQty4, LabelQty5, LabelQty6, LabelQty7, LabelQty8, LabelQty9, LabelQty10,LabelQty11, LabelQty12, LabelQty13, LabelQty14, LabelQty15, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @DocType, @QueueNo, @EmpCode1, @EmpCode2, ");
            SQL.AppendLine("@Shelf1, @Shelf2, @Shelf3, @Shelf4, @Shelf5, @Shelf6, @Shelf7, @Shelf8, @Shelf9, @Shelf10, @Shelf11, @Shelf12, @Shelf13, @Shelf14, @Shelf15, ");
            SQL.AppendLine("@WhsCode1, @WhsCode2, @WhsCode3, @WhsCode4, @WhsCode5, @WhsCode6, @WhsCode7, @WhsCode8, @WhsCode9, @WhsCode10, @WhsCode11, @WhsCode12, @WhsCode13, @WhsCode14, @WhsCode15, ");
            SQL.AppendLine("@LabelQty1, @LabelQty2, @LabelQty3, @LabelQty4, @LabelQty5, @LabelQty6, @LabelQty7, @LabelQty8, @LabelQty9, @LabelQty10, @LabelQty11, @LabelQty12, @LabelQty13, @LabelQty14, @LabelQty15, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblLoadingQueue Set RecvRawMaterial2Ind='F' ");
            SQL.AppendLine("Where RecvRawMaterial2Ind='O' And DocNo=@QueueNo;");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", mType.ToString());
            Sm.CmParam<String>(ref cm, "@QueueNo", Sm.GetLue(LueQueueNo));
            Sm.CmParam<String>(ref cm, "@EmpCode1", Sm.GetLue(LueEmpCode1));
            Sm.CmParam<String>(ref cm, "@EmpCode2", Sm.GetLue(LueEmpCode2));
            Sm.CmParam<String>(ref cm, "@Shelf1", Sm.GetLue(LueShelf1));
            Sm.CmParam<String>(ref cm, "@Shelf2", Sm.GetLue(LueShelf2));
            Sm.CmParam<String>(ref cm, "@Shelf3", Sm.GetLue(LueShelf3));
            Sm.CmParam<String>(ref cm, "@Shelf4", Sm.GetLue(LueShelf4));
            Sm.CmParam<String>(ref cm, "@Shelf5", Sm.GetLue(LueShelf5));
            Sm.CmParam<String>(ref cm, "@Shelf6", Sm.GetLue(LueShelf6));
            Sm.CmParam<String>(ref cm, "@Shelf7", Sm.GetLue(LueShelf7));
            Sm.CmParam<String>(ref cm, "@Shelf8", Sm.GetLue(LueShelf8));
            Sm.CmParam<String>(ref cm, "@Shelf9", Sm.GetLue(LueShelf9));
            Sm.CmParam<String>(ref cm, "@Shelf10", Sm.GetLue(LueShelf10));
            Sm.CmParam<String>(ref cm, "@Shelf11", Sm.GetLue(LueShelf11));
            Sm.CmParam<String>(ref cm, "@Shelf12", Sm.GetLue(LueShelf12));
            Sm.CmParam<String>(ref cm, "@Shelf13", Sm.GetLue(LueShelf13));
            Sm.CmParam<String>(ref cm, "@Shelf14", Sm.GetLue(LueShelf14));
            Sm.CmParam<String>(ref cm, "@Shelf15", Sm.GetLue(LueShelf15));
            Sm.CmParam<String>(ref cm, "@WhsCode1", Sm.GetLue(LueWhsCode1));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@WhsCode3", Sm.GetLue(LueWhsCode3));
            Sm.CmParam<String>(ref cm, "@WhsCode4", Sm.GetLue(LueWhsCode4));
            Sm.CmParam<String>(ref cm, "@WhsCode5", Sm.GetLue(LueWhsCode5));
            Sm.CmParam<String>(ref cm, "@WhsCode6", Sm.GetLue(LueWhsCode6));
            Sm.CmParam<String>(ref cm, "@WhsCode7", Sm.GetLue(LueWhsCode7));
            Sm.CmParam<String>(ref cm, "@WhsCode8", Sm.GetLue(LueWhsCode8));
            Sm.CmParam<String>(ref cm, "@WhsCode9", Sm.GetLue(LueWhsCode9));
            Sm.CmParam<String>(ref cm, "@WhsCode10", Sm.GetLue(LueWhsCode10));
            Sm.CmParam<String>(ref cm, "@WhsCode11", Sm.GetLue(LueWhsCode11));
            Sm.CmParam<String>(ref cm, "@WhsCode12", Sm.GetLue(LueWhsCode12));
            Sm.CmParam<String>(ref cm, "@WhsCode13", Sm.GetLue(LueWhsCode13));
            Sm.CmParam<String>(ref cm, "@WhsCode14", Sm.GetLue(LueWhsCode14));
            Sm.CmParam<String>(ref cm, "@WhsCode15", Sm.GetLue(LueWhsCode15));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty1", Decimal.Parse(TxtLabelQty1.Text));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty2", Decimal.Parse(TxtLabelQty2.Text));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty3", Decimal.Parse(TxtLabelQty3.Text));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty4", Decimal.Parse(TxtLabelQty4.Text));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty5", Decimal.Parse(TxtLabelQty5.Text));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty6", Decimal.Parse(TxtLabelQty6.Text));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty7", Decimal.Parse(TxtLabelQty7.Text));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty8", Decimal.Parse(TxtLabelQty8.Text));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty9", Decimal.Parse(TxtLabelQty9.Text));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty10", Decimal.Parse(TxtLabelQty10.Text));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty11", Decimal.Parse(TxtLabelQty11.Text));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty12", Decimal.Parse(TxtLabelQty12.Text));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty13", Decimal.Parse(TxtLabelQty13.Text));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty14", Decimal.Parse(TxtLabelQty14.Text));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty15", Decimal.Parse(TxtLabelQty15.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRecvRawMaterial2Dtl(string DocNo, ref int DNo, string SectionNo, iGrid Grd, int Row)
        {
            var SQL = new StringBuilder();

            DNo += 1;

            SQL.AppendLine("Insert Into TblRecvRawMaterial2Dtl ");
            SQL.AppendLine("(DocNo, DNo, SectionNo, ItCode, Diameter, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @SectionNo, @ItCodeActual, @Diameter, @Qty, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblRecvRawMaterial2Dtl2 ");
            SQL.AppendLine("(DocNo, DNo, SectionNo, ItCode, Diameter, Qty, ActualLength, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @SectionNo, @ItCode, @Diameter, @Qty, @ActualLength, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + DNo.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SectionNo", SectionNo);
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd, Row, 0));
            Sm.CmParam<String>(ref cm, "@ItCodeActual", Sm.GetGrdStr(Grd, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Diameter", Sm.GetGrdDec(Grd, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@ActualLength", Sm.GetGrdDec(Grd, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        
        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditRecvRawMaterial2Hdr());

            int DNo = 0;

            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "1", Grd11);
            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "2", Grd12);
            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "3", Grd13);
            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "4", Grd14);
            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "5", Grd15);
            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "6", Grd16);
            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "7", Grd17);
            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "8", Grd18);
            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "9", Grd19);
            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "10", Grd20);
            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "11", Grd21);
            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "12", Grd22);
            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "13", Grd23);
            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "14", Grd24);
            SaveRecvRawMaterial2Dtl(ref cml, TxtDocNo.Text, ref DNo, "15", Grd25);

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                Sm.IsLueEmpty(LueEmpCode1, "QC personel") ||
                Sm.IsLueEmpty(LueShelf1, "Rak [1]") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsItemInfoNotValid(LueShelf1, LueWhsCode1, Grd11, "1") ||
                IsItemInfoNotValid(LueShelf2, LueWhsCode2, Grd12, "2") ||
                IsItemInfoNotValid(LueShelf3, LueWhsCode3, Grd13, "3") ||
                IsItemInfoNotValid(LueShelf4, LueWhsCode4, Grd14, "4") ||
                IsItemInfoNotValid(LueShelf5, LueWhsCode5, Grd15, "5") ||
                IsItemInfoNotValid(LueShelf6, LueWhsCode6, Grd16, "6") ||
                IsItemInfoNotValid(LueShelf7, LueWhsCode7, Grd17, "7") ||
                IsItemInfoNotValid(LueShelf8, LueWhsCode8, Grd18, "8") ||
                IsItemInfoNotValid(LueShelf9, LueWhsCode9, Grd19, "9") ||
                IsItemInfoNotValid(LueShelf10, LueWhsCode10, Grd20, "10") ||
                IsItemInfoNotValid(LueShelf11, LueWhsCode11, Grd21, "11") ||
                IsItemInfoNotValid(LueShelf12, LueWhsCode12, Grd22, "12") ||
                IsItemInfoNotValid(LueShelf13, LueWhsCode13, Grd23, "13") ||
                IsItemInfoNotValid(LueShelf14, LueWhsCode14, Grd24, "14") ||
                IsItemInfoNotValid(LueShelf15, LueWhsCode15, Grd25, "15") ||
                IsBinNotValid(LueShelf1) ||
                IsBinNotValid(LueShelf2) ||
                IsBinNotValid(LueShelf3) ||
                IsBinNotValid(LueShelf4) ||
                IsBinNotValid(LueShelf5) ||
                IsBinNotValid(LueShelf6) ||
                IsBinNotValid(LueShelf7) ||
                IsBinNotValid(LueShelf8) ||
                IsBinNotValid(LueShelf9) ||
                IsBinNotValid(LueShelf10) ||
                IsBinNotValid(LueShelf11) ||
                IsBinNotValid(LueShelf12) ||
                IsBinNotValid(LueShelf13) ||
                IsBinNotValid(LueShelf14) ||
                IsBinNotValid(LueShelf15)
                ;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblRecvRawMaterial2Hdr ");
            SQL.AppendLine("Where CancelInd='Y' ");
            SQL.AppendLine("And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Dokumen ini telah dibatalkan.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditRecvRawMaterial2Hdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvRawMaterial2Hdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, Remark=@Remark, ");
            SQL.AppendLine("    Shelf1=@Shelf1, Shelf2=@Shelf2, Shelf3=@Shelf3, Shelf4=@Shelf4, Shelf5=@Shelf5, ");
            SQL.AppendLine("    Shelf6=@Shelf6, Shelf7=@Shelf7, Shelf8=@Shelf8, Shelf9=@Shelf9, Shelf10=@Shelf10, ");
            SQL.AppendLine("    Shelf11=@Shelf11, Shelf12=@Shelf12, Shelf13=@Shelf13, Shelf14=@Shelf14, Shelf15=@Shelf15, ");
            SQL.AppendLine("    WhsCode1=@WhsCode1, WhsCode2=@WhsCode2, WhsCode3=@WhsCode3, WhsCode4=@WhsCode4, WhsCode5=@WhsCode5, ");
            SQL.AppendLine("    WhsCode6=@WhsCode6, WhsCode7=@WhsCode7, WhsCode8=@WhsCode8, WhsCode9=@WhsCode9, WhsCode10=@WhsCode10, ");
            SQL.AppendLine("    WhsCode11=@WhsCode11, WhsCode12=@WhsCode12, WhsCode13=@WhsCode13, WhsCode14=@WhsCode14, WhsCode15=@WhsCode15, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            if (ChkCancelInd.Checked)
            {
                SQL.AppendLine("Update TblLoadingQueue Set RecvRawMaterial2Ind='O' ");
                SQL.AppendLine("Where RecvRawMaterial2Ind='F' And DocNo=@QueueNo;");
            }

            SQL.AppendLine("Delete From TblRecvRawMaterial2Dtl Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblRecvRawMaterial2Dtl2 Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@Shelf1", Sm.GetLue(LueShelf1));
            Sm.CmParam<String>(ref cm, "@Shelf2", Sm.GetLue(LueShelf2));
            Sm.CmParam<String>(ref cm, "@Shelf3", Sm.GetLue(LueShelf3));
            Sm.CmParam<String>(ref cm, "@Shelf4", Sm.GetLue(LueShelf4));
            Sm.CmParam<String>(ref cm, "@Shelf5", Sm.GetLue(LueShelf5));
            Sm.CmParam<String>(ref cm, "@Shelf6", Sm.GetLue(LueShelf6));
            Sm.CmParam<String>(ref cm, "@Shelf7", Sm.GetLue(LueShelf7));
            Sm.CmParam<String>(ref cm, "@Shelf8", Sm.GetLue(LueShelf8));
            Sm.CmParam<String>(ref cm, "@Shelf9", Sm.GetLue(LueShelf9));
            Sm.CmParam<String>(ref cm, "@Shelf10", Sm.GetLue(LueShelf10));
            Sm.CmParam<String>(ref cm, "@Shelf11", Sm.GetLue(LueShelf11));
            Sm.CmParam<String>(ref cm, "@Shelf12", Sm.GetLue(LueShelf12));
            Sm.CmParam<String>(ref cm, "@Shelf13", Sm.GetLue(LueShelf13));
            Sm.CmParam<String>(ref cm, "@Shelf14", Sm.GetLue(LueShelf14));
            Sm.CmParam<String>(ref cm, "@Shelf15", Sm.GetLue(LueShelf15));
            Sm.CmParam<String>(ref cm, "@WhsCode1", Sm.GetLue(LueWhsCode1));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@WhsCode3", Sm.GetLue(LueWhsCode3));
            Sm.CmParam<String>(ref cm, "@WhsCode4", Sm.GetLue(LueWhsCode4));
            Sm.CmParam<String>(ref cm, "@WhsCode5", Sm.GetLue(LueWhsCode5));
            Sm.CmParam<String>(ref cm, "@WhsCode6", Sm.GetLue(LueWhsCode6));
            Sm.CmParam<String>(ref cm, "@WhsCode7", Sm.GetLue(LueWhsCode7));
            Sm.CmParam<String>(ref cm, "@WhsCode8", Sm.GetLue(LueWhsCode8));
            Sm.CmParam<String>(ref cm, "@WhsCode9", Sm.GetLue(LueWhsCode9));
            Sm.CmParam<String>(ref cm, "@WhsCode10", Sm.GetLue(LueWhsCode10));
            Sm.CmParam<String>(ref cm, "@WhsCode11", Sm.GetLue(LueWhsCode11));
            Sm.CmParam<String>(ref cm, "@WhsCode12", Sm.GetLue(LueWhsCode12));
            Sm.CmParam<String>(ref cm, "@WhsCode13", Sm.GetLue(LueWhsCode13));
            Sm.CmParam<String>(ref cm, "@WhsCode14", Sm.GetLue(LueWhsCode14));
            Sm.CmParam<String>(ref cm, "@WhsCode15", Sm.GetLue(LueWhsCode15));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowRecvRawMaterial2Hdr(DocNo);
                ShowRecvRawMaterial2Dtl(DocNo, "1", ref Grd11, TxtTotal1);
                ShowRecvRawMaterial2Dtl(DocNo, "2", ref Grd12, TxtTotal2);
                ShowRecvRawMaterial2Dtl(DocNo, "3", ref Grd13, TxtTotal3);
                ShowRecvRawMaterial2Dtl(DocNo, "4", ref Grd14, TxtTotal4);
                ShowRecvRawMaterial2Dtl(DocNo, "5", ref Grd15, TxtTotal5);
                ShowRecvRawMaterial2Dtl(DocNo, "6", ref Grd16, TxtTotal6);
                ShowRecvRawMaterial2Dtl(DocNo, "7", ref Grd17, TxtTotal7);
                ShowRecvRawMaterial2Dtl(DocNo, "8", ref Grd18, TxtTotal8);
                ShowRecvRawMaterial2Dtl(DocNo, "9", ref Grd19, TxtTotal9);
                ShowRecvRawMaterial2Dtl(DocNo, "10", ref Grd20, TxtTotal10);
                ShowRecvRawMaterial2Dtl(DocNo, "11", ref Grd21, TxtTotal11);
                ShowRecvRawMaterial2Dtl(DocNo, "12", ref Grd22, TxtTotal12);
                ShowRecvRawMaterial2Dtl(DocNo, "13", ref Grd23, TxtTotal13);
                ShowRecvRawMaterial2Dtl(DocNo, "14", ref Grd24, TxtTotal14);
                ShowRecvRawMaterial2Dtl(DocNo, "15", ref Grd25, TxtTotal15);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRecvRawMaterial2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.QueueNo, A.EmpCode1, A.EmpCode2, B.UserName, A.Remark, ");
            SQL.AppendLine("A.Shelf1, A.Shelf2, A.Shelf3, A.Shelf4, A.Shelf5, A.Shelf6, A.Shelf7, A.Shelf8, A.Shelf9, A.Shelf10, A.Shelf11, A.Shelf12, A.Shelf13, A.Shelf14, A.Shelf15, ");
            SQL.AppendLine("A.WhsCode1, A.WhsCode2, A.WhsCode3, A.WhsCode4, A.WhsCode5, A.WhsCode6, A.WhsCode7, A.WhsCode8, A.WhsCode9, A.WhsCode10, A.WhsCode11, A.WhsCode12, A.WhsCode13, A.WhsCode14, A.WhsCode15, ");
            SQL.AppendLine("A.LabelQty1, A.LabelQty2, A.LabelQty3, A.LabelQty4, A.LabelQty5, A.LabelQty6, A.LabelQty7, A.LabelQty8, A.LabelQty9, A.LabelQty10, A.LabelQty11, A.LabelQty12, A.LabelQty13, A.LabelQty14, A.LabelQty15 ");
            SQL.AppendLine("From TblRecvRawMaterial2Hdr A ");
            SQL.AppendLine("Left Join TblUser B On A.CreateBy=B.UserCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "QueueNo", "EmpCode1", "EmpCode2", 
                        
                        //6-10
                        "UserName", "Remark", "Shelf1", "Shelf2", "Shelf3", 
                        
                        //11-15
                        "Shelf4", "Shelf5", "Shelf6", "Shelf7", "Shelf8", 
                        
                        //16-20
                        "Shelf9", "Shelf10", "Shelf11", "Shelf12", "Shelf13", 
                        
                        //21-25
                        "Shelf14", "Shelf15", "WhsCode1", "WhsCode2", "WhsCode3", 
                        
                        //26-30
                        "WhsCode4", "WhsCode5", "WhsCode6", "WhsCode7",  "WhsCode8", 
                        
                        //31-35
                        "WhsCode9", "WhsCode10", "WhsCode11", "WhsCode12", "WhsCode13", 
                        
                        //36-40
                        "WhsCode14", "WhsCode15", "LabelQty1", "LabelQty2", "LabelQty3", 

                        //41-45
                        "LabelQty4", "LabelQty5", "LabelQty6", "LabelQty7",  "LabelQty8", 
                        
                        //46-50
                        "LabelQty9", "LabelQty10", "LabelQty11", "LabelQty12", "LabelQty13", 
                        
                        //51-52
                        "LabelQty14", "LabelQty15"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        SetLueQueueNo(ref LueQueueNo, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueQueueNo, Sm.DrStr(dr, c[3]));
                        SetLueEmpCode(ref LueEmpCode1, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueEmpCode1, Sm.DrStr(dr, c[4]));
                        SetLueEmpCode(ref LueEmpCode2, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueEmpCode2, Sm.DrStr(dr, c[5]));
                        TxtCreateBy.EditValue = Sm.DrStr(dr, c[6]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                        SetLueShelf(ref LueShelf1, Sm.DrStr(dr, c[8]));
                        SetLueShelf(ref LueShelf2, Sm.DrStr(dr, c[9]));
                        SetLueShelf(ref LueShelf3, Sm.DrStr(dr, c[10]));
                        SetLueShelf(ref LueShelf4, Sm.DrStr(dr, c[11]));
                        SetLueShelf(ref LueShelf5, Sm.DrStr(dr, c[12]));
                        SetLueShelf(ref LueShelf6, Sm.DrStr(dr, c[13]));
                        SetLueShelf(ref LueShelf7, Sm.DrStr(dr, c[14]));
                        SetLueShelf(ref LueShelf8, Sm.DrStr(dr, c[15]));
                        SetLueShelf(ref LueShelf9, Sm.DrStr(dr, c[16]));
                        SetLueShelf(ref LueShelf10, Sm.DrStr(dr, c[17]));
                        SetLueShelf(ref LueShelf11, Sm.DrStr(dr, c[18]));
                        SetLueShelf(ref LueShelf12, Sm.DrStr(dr, c[19]));
                        SetLueShelf(ref LueShelf13, Sm.DrStr(dr, c[20]));
                        SetLueShelf(ref LueShelf14, Sm.DrStr(dr, c[21]));
                        SetLueShelf(ref LueShelf15, Sm.DrStr(dr, c[22]));
                        Sm.SetLue(LueShelf1, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LueShelf2, Sm.DrStr(dr, c[9]));
                        Sm.SetLue(LueShelf3, Sm.DrStr(dr, c[10]));
                        Sm.SetLue(LueShelf4, Sm.DrStr(dr, c[11]));
                        Sm.SetLue(LueShelf5, Sm.DrStr(dr, c[12]));
                        Sm.SetLue(LueShelf6, Sm.DrStr(dr, c[13]));
                        Sm.SetLue(LueShelf7, Sm.DrStr(dr, c[14]));
                        Sm.SetLue(LueShelf8, Sm.DrStr(dr, c[15]));
                        Sm.SetLue(LueShelf9, Sm.DrStr(dr, c[16]));
                        Sm.SetLue(LueShelf10, Sm.DrStr(dr, c[17]));
                        Sm.SetLue(LueShelf11, Sm.DrStr(dr, c[18]));
                        Sm.SetLue(LueShelf12, Sm.DrStr(dr, c[19]));
                        Sm.SetLue(LueShelf13, Sm.DrStr(dr, c[20]));
                        Sm.SetLue(LueShelf14, Sm.DrStr(dr, c[21]));
                        Sm.SetLue(LueShelf15, Sm.DrStr(dr, c[22]));
                        SetLueWhsCode(ref LueWhsCode1, Sm.DrStr(dr, c[23]));
                        SetLueWhsCode(ref LueWhsCode2, Sm.DrStr(dr, c[24]));
                        SetLueWhsCode(ref LueWhsCode3, Sm.DrStr(dr, c[25]));
                        SetLueWhsCode(ref LueWhsCode4, Sm.DrStr(dr, c[26]));
                        SetLueWhsCode(ref LueWhsCode5, Sm.DrStr(dr, c[27]));
                        SetLueWhsCode(ref LueWhsCode6, Sm.DrStr(dr, c[28]));
                        SetLueWhsCode(ref LueWhsCode7, Sm.DrStr(dr, c[29]));
                        SetLueWhsCode(ref LueWhsCode8, Sm.DrStr(dr, c[30]));
                        SetLueWhsCode(ref LueWhsCode9, Sm.DrStr(dr, c[31]));
                        SetLueWhsCode(ref LueWhsCode10, Sm.DrStr(dr, c[32]));
                        SetLueWhsCode(ref LueWhsCode11, Sm.DrStr(dr, c[33]));
                        SetLueWhsCode(ref LueWhsCode12, Sm.DrStr(dr, c[34]));
                        SetLueWhsCode(ref LueWhsCode13, Sm.DrStr(dr, c[35]));
                        SetLueWhsCode(ref LueWhsCode14, Sm.DrStr(dr, c[36]));
                        SetLueWhsCode(ref LueWhsCode15, Sm.DrStr(dr, c[37]));
                        Sm.SetLue(LueWhsCode1, Sm.DrStr(dr, c[23]));
                        Sm.SetLue(LueWhsCode2, Sm.DrStr(dr, c[24]));
                        Sm.SetLue(LueWhsCode3, Sm.DrStr(dr, c[25]));
                        Sm.SetLue(LueWhsCode4, Sm.DrStr(dr, c[26]));
                        Sm.SetLue(LueWhsCode5, Sm.DrStr(dr, c[27]));
                        Sm.SetLue(LueWhsCode6, Sm.DrStr(dr, c[28]));
                        Sm.SetLue(LueWhsCode7, Sm.DrStr(dr, c[29]));
                        Sm.SetLue(LueWhsCode8, Sm.DrStr(dr, c[30]));
                        Sm.SetLue(LueWhsCode9, Sm.DrStr(dr, c[31]));
                        Sm.SetLue(LueWhsCode10, Sm.DrStr(dr, c[32]));
                        Sm.SetLue(LueWhsCode11, Sm.DrStr(dr, c[33]));
                        Sm.SetLue(LueWhsCode12, Sm.DrStr(dr, c[34]));
                        Sm.SetLue(LueWhsCode13, Sm.DrStr(dr, c[35]));
                        Sm.SetLue(LueWhsCode14, Sm.DrStr(dr, c[36]));
                        Sm.SetLue(LueWhsCode15, Sm.DrStr(dr, c[37]));
                        TxtLabelQty1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[38]), 1);
                        TxtLabelQty2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[39]), 1);
                        TxtLabelQty3.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[40]), 1);
                        TxtLabelQty4.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[41]), 1);
                        TxtLabelQty5.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[42]), 1);
                        TxtLabelQty6.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[43]), 1);
                        TxtLabelQty7.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[44]), 1);
                        TxtLabelQty8.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[45]), 1);
                        TxtLabelQty9.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[46]), 1);
                        TxtLabelQty10.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[47]), 1);
                        TxtLabelQty11.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[48]), 1);
                        TxtLabelQty12.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[49]), 1);
                        TxtLabelQty13.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[50]), 1);
                        TxtLabelQty14.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[51]), 1);
                        TxtLabelQty15.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[52]), 1);
                    }, true
                );
        }

        private void ShowRecvRawMaterial2Dtl(string DocNo, string SectionNo, ref iGrid ItemGrd, DXE.TextEdit TxtTotal)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SectionNo", SectionNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItName, A.Diameter, A.Qty, B.InventoryUomCode2, C.ItCode As ItCodeActual, ");

            if (mType == 1)
                SQL.AppendLine("Trim(Concat(Convert(Format(B.Length, 2) Using utf8), ' ', IfNull(B.LengthUomCode, ''))) As Info ");
            else
                SQL.AppendLine("Trim(Concat(Convert(Format(B.Length, 2) Using utf8), ' ', IfNull(B.LengthUomCode, ''), '*', Convert(Format(B.Height, 2) Using utf8), ' ', IfNull(B.HeightUomCode, ''))) As Info ");

            SQL.AppendLine(", A.ActualLength ");
            SQL.AppendLine("From TblRecvRawMaterial2Dtl2 A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblRecvRawMaterial2Dtl C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.SectionNo=@SectionNo ");
            SQL.AppendLine("Order By A.ItCode, A.DNo;");

            Sm.ShowDataInGrid(
                ref ItemGrd, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItName", "Info", "Diameter", "Qty", "InventoryUomCode2",
 
                    //6-7
                    "ItCodeActual", "ActualLength"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("I", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("I", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    ComputeTotal(Grd, TxtTotal);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref ItemGrd, ItemGrd.Rows.Count - 1, new int[] { 3, 4, 8 });
            Sm.FocusGrd(ItemGrd, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter() 
        {
            if (Sm.CompareStr(Sm.GetParameter("RRMCMenuCode2"), mMenuCode)) mType = 2;
            mPosCodeQC = Sm.GetParameter("PosCodeQC");
            mRMInspDefaultQueueDt = Sm.GetParameterDec("RMInspDefaultQueueDt");
        }

        private void SetUserName()
        {
            var cm = new MySqlCommand() { CommandText = "Select UserName From TblUser Where UserCode=@UserCode;" };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            TxtCreateBy.EditValue = Sm.GetValue(cm);
        }

        private void SetLueQueueNo(ref DXE.LookUpEdit Lue, string QueueNo)
        {
            try
            {
                var SQL = new StringBuilder();
                var Dt = Sm.ConvertDate(Sm.ServerCurrentDateTime()).AddDays((double)mRMInspDefaultQueueDt);
                
                SQL.AppendLine("Select DocNo As Col1, DocNo As Col2 From TblLoadingQueue Where ");
                if (QueueNo.Length == 0)
                    SQL.AppendLine("Left(CreateDt, 8)>@Dt;");
                else
                    SQL.AppendLine("DocNo=@QueueNo;");
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (QueueNo.Length == 0)
                    Sm.CmParamDt(ref cm, "@Dt", Sm.FormatDate(Dt));
                else
                    Sm.CmParam<String>(ref cm, "@QueueNo", QueueNo);
                Sm.SetLue2(ref Lue, ref cm, 0, 50, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueEmpCode(ref LookUpEdit Lue, string EmpCode)
        {
            var SQL = new StringBuilder();
            
            try
            {
                SQL.AppendLine("Select EmpCode As Col1, Concat(EmpName, ' [', EmpCode,']') As Col2 ");
                SQL.AppendLine("From TblEmployee ");
                if (EmpCode.Length == 0)
                    SQL.AppendLine("Where ResignDt Is Null And PosCode=@PosCode Order By EmpName;");
                else
                    SQL.AppendLine("Where EmpCode=@EmpCode;"); 
                
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@PosCode", mPosCodeQC);
                Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                Sm.SetLue2(ref Lue, ref cm, 0, 50, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueItCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select ItCode As Col1, ItName As Col2, InventoryUomCode2 As Col3, ");
                if (mType==1)
                    SQL.AppendLine("Trim(Concat(Convert(Format(Length, 2) Using utf8), ' ', IfNull(LengthUomCode, ''))) As Col4 ");
                else
                    SQL.AppendLine("Trim(Concat(Convert(Format(Length, 2) Using utf8), ' ', IfNull(LengthUomCode, ''), '*', Convert(Format(Height, 2) Using utf8), ' ', IfNull(HeightUomCode, ''))) As Col4 ");
                
                SQL.AppendLine("From TblItem ");
                SQL.AppendLine("Where ActInd='Y' ");
                SQL.AppendLine("And ItCtCode In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='ItCtRMP" + (mType==2?"2":"") +" ') ");
                SQL.AppendLine("Order By ItName");

                Sm.SetLue4(
                    ref Lue,
                    SQL.ToString(),
                    10, 60, 0, 50, false, true, false, true, "Code", "Name", "UoM", "Length", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueShelf(ref DXE.LookUpEdit Lue, string Bin)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct Col1, Col2 From ( ");
                SQL.AppendLine("    Select Bin As Col1, Bin As Col2 From TblBin Where ActInd='Y' ");
                if (Bin.Length!=0)
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Bin As Col1, Bin As Col2 From TblBin Where Bin='"+Bin+"' ");
                }
                SQL.AppendLine(") T Order By Col1;");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(), 0, 50, false, true, "Bin", "Bin", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueWhsCode(ref DXE.LookUpEdit Lue, string WhsCode)
        {
            try
            {
                var SQL = new StringBuilder();

                string RecvVdRawMaterialWhs= Sm.GetParameter("RecvVdRawMaterialWhs"); 

                SQL.AppendLine("Select Distinct Col1, Col2 From ( ");
                SQL.AppendLine("    Select WhsCode As Col1, WhsName As Col2 From TblWarehouse ");
                if (RecvVdRawMaterialWhs.Length>0)
                    SQL.AppendLine("    Where Locate(Concat('##', WhsCode, '##'), '" + RecvVdRawMaterialWhs + "')>0 ");
                if (WhsCode.Length != 0)
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select WhsCode As Col1, WhsName As Col2 From TblWarehouse Where WhsCode='" + WhsCode + "' ");
                }
                SQL.AppendLine(") T Order By Col2;");
                
                Sm.SetLue2(
                    ref Lue, SQL.ToString(), 0, 50, false, true, "Bin", "Bin", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueRequestEdit(iGrid Grd, DXE.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }                 

        private void SetGrdCanvas()
        {
            iGCellStyle CanvasStyle = new iGCellStyle();
            CanvasStyle.CustomDrawFlags = TenTec.Windows.iGridLib.iGCustomDrawFlags.Foreground;
            CanvasStyle.Flags = ((TenTec.Windows.iGridLib.iGCellFlags)(TenTec.Windows.iGridLib.iGCellFlags.DisplayImage));

            Grd11.Cols[6].CellStyle =
            Grd12.Cols[6].CellStyle =
            Grd13.Cols[6].CellStyle =
            Grd14.Cols[6].CellStyle =
            Grd15.Cols[6].CellStyle =
            Grd16.Cols[6].CellStyle =
            Grd17.Cols[6].CellStyle =
            Grd18.Cols[6].CellStyle =
            Grd19.Cols[6].CellStyle =
            Grd20.Cols[6].CellStyle =
            Grd21.Cols[6].CellStyle =
            Grd22.Cols[6].CellStyle =
            Grd23.Cols[6].CellStyle =
            Grd24.Cols[6].CellStyle =
            Grd25.Cols[6].CellStyle = CanvasStyle;
        }

        private void GrdCustomDrawCellForeground(iGrid Grd, object sender, iGCustomDrawCellEventArgs e)
        {
            if (e.ColIndex != 6) return;

            try
            {
                object myObjValue = Grd.Cells[e.RowIndex, e.ColIndex].Value;

                if (myObjValue == null) return;

                Pen RedPen = new Pen(Color.Red, 2);
                Rectangle myBounds = e.Bounds;

                myBounds.Inflate(-2, -2);

                myBounds.Width = myBounds.Width - 1;

                myBounds.Height = myBounds.Height - 1;

                int X1 = myBounds.X, Y1 = myBounds.Y;

                Int32 myValue = Sm.GetGrdInt(Grd, e.RowIndex, e.ColIndex);

                Int32 DivValue = (Int32)(myValue / 5);

                Int32 RemainingValue = myValue % 5;

                for (int intX = 0; intX < DivValue; intX++)
                {

                    e.Graphics.DrawLine(RedPen, X1, Y1, X1, Y1 + myBounds.Height);

                    e.Graphics.DrawLine(RedPen, X1, Y1 + myBounds.Height, X1 + myBounds.Height, Y1 + myBounds.Height);

                    e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1 + myBounds.Height, X1 + myBounds.Height, Y1);

                    e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1, X1, Y1);

                    e.Graphics.DrawLine(RedPen, X1, Y1, X1 + myBounds.Height, Y1 + myBounds.Height);

                    X1 = X1 + myBounds.Height + 5;

                }

                if (RemainingValue >= 1)
                {
                    e.Graphics.DrawLine(RedPen, X1, Y1, X1, Y1 + myBounds.Height);
                    if (RemainingValue >= 2)
                    {
                        e.Graphics.DrawLine(RedPen, X1, Y1 + myBounds.Height, X1 + myBounds.Height, Y1 + myBounds.Height);
                        if (RemainingValue >= 3)
                        {
                            e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1 + myBounds.Height, X1 + myBounds.Height, Y1);
                            if (RemainingValue >= 4)
                            {
                                e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1, X1, Y1);
                                if (RemainingValue >= 5)
                                    e.Graphics.DrawLine(RedPen, X1, Y1, X1 + myBounds.Height, Y1 + myBounds.Height);
                            }
                        }
                    }
                }

                int TotalWidth = 0;
                for (int intX = 0; intX <= e.ColIndex; intX++)
                    TotalWidth = TotalWidth + Grd.Cols[intX].Width;

                if (X1 > TotalWidth)
                    Grd.Cols[e.ColIndex].Width = Grd.Cols[e.ColIndex].Width + (X1 - TotalWidth) + myBounds.Height;
             }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetTabControls(string TabPage, ref DXE.LookUpEdit LueShelf, ref DXE.LookUpEdit LueWhsCode, ref DXE.LookUpEdit LueItCode)
        {
            tabControl1.SelectTab("tabPage" + TabPage);
            SetLueShelf(ref LueShelf, "");
            SetLueWhsCode(ref LueWhsCode, "");
            SetLueItCode(ref LueItCode);
            LueItCode.Visible = false;
        }

        private void LueItCodeLeave(object sender, EventArgs e, DXE.LookUpEdit Lue, iGrid Grd)
        {
            if (Lue.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (Sm.GetLue(Lue).Length == 0)
                {
                    Grd.Cells[fCell.RowIndex, 0].Value =
                    Grd.Cells[fCell.RowIndex, 1].Value =
                    Grd.Cells[fCell.RowIndex, 2].Value =
                    Grd.Cells[fCell.RowIndex, 5].Value = null;
                }
                else
                {
                    Grd.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(Lue);
                    Grd.Cells[fCell.RowIndex, 1].Value = Lue.GetColumnValue("Col2");
                    Grd.Cells[fCell.RowIndex, 2].Value = Lue.GetColumnValue("Col4");
                    Grd.Cells[fCell.RowIndex, 5].Value = Lue.GetColumnValue("Col3");

                    if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length != 0 &&
                        Sm.GetGrdStr(Grd, fCell.RowIndex, 3).Length != 0 && Sm.GetGrdDec(Grd, fCell.RowIndex, 3) != 0)
                    {
                        Grd.Cells[fCell.RowIndex, 7].Value = SetActualItCode2(Grd, fCell.RowIndex);
                        if (Sm.GetGrdStr(Grd, fCell.RowIndex, 7).Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Actual item is empty.");
                        }
                    }
                }
                Sm.FocusGrd(Grd, fCell.RowIndex, 3);
                Lue.Visible = false;
            }
        }

        private void GrdItemAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e, iGrid Grd, DXE.TextEdit TxtTotal)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd, new int[] { 3, 4, 8, 9 }, e);

            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd, e.RowIndex, 0).Length != 0 &&
                    Sm.GetGrdStr(Grd, e.RowIndex, 3).Length != 0 && Sm.GetGrdDec(Grd, e.RowIndex, 3) != 0)
                {
                    Grd.Cells[e.RowIndex, 7].Value = SetActualItCode2(Grd, e.RowIndex);
                    if (Sm.GetGrdStr(Grd, e.RowIndex, 7).Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Actual item is empty.");
                    }
                }
                Sm.FocusGrd(Grd, e.RowIndex, 4);
            }
            
            if (e.ColIndex == 4)
            {
                Grd.Cells[e.RowIndex, 6].Value = Sm.GetGrdDec(Grd, e.RowIndex, 4);
                ComputeTotal(Grd, TxtTotal);

                //Grd.Cells[e.RowIndex + 1, 0].Value = Sm.GetGrdStr(Grd, e.RowIndex, 0);
                //Grd.Cells[e.RowIndex + 1, 1].Value = Sm.GetGrdStr(Grd, e.RowIndex, 1);
                //Grd.Cells[e.RowIndex + 1, 2].Value = Sm.GetGrdStr(Grd, e.RowIndex, 2);
                //Grd.Cells[e.RowIndex + 1, 5].Value = Sm.GetGrdStr(Grd, e.RowIndex, 5);
                //Grd.Cells[e.RowIndex + 1, 7].Value = Sm.GetGrdStr(Grd, e.RowIndex, 7);

                //Sm.FocusGrd(Grd, e.RowIndex + 1, 3);

                //if (Grd.Rows.Count - 1 == e.RowIndex+1)
                //{
                //    Grd.Rows.Add();
                //    Sm.SetGrdNumValueZero(ref Grd, Grd.Rows.Count - 1, new int[] { 3, 4 });
                //}
            }
        }

        private void GrdItemRequestEdit(object sender, iGRequestEditEventArgs e, iGrid Grd, DXE.LookUpEdit Lue)
        {
            if (BtnSave.Enabled &&
                // TxtDocNo.Text.Length == 0 &&
                Sm.IsGrdColSelected(new int[] { 0, 3, 4 }, e.ColIndex))
            {
                if (e.ColIndex == 0) LueRequestEdit(Grd, Lue, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd, Grd.Rows.Count - 1, new int[] { 3, 4, 8 });
            }
        }

        private void GrdItemKeyDown(iGrid Grd, KeyEventArgs e, DXE.SimpleButton Btn1, DXE.SimpleButton Btn2)
        {
            try
            {
                if (Grd.CurCell!=null && e.KeyCode == Keys.Enter && Grd.CurCell.RowIndex != Grd.Rows.Count - 1)
                {
                    if (Grd.CurCell.ColIndex == 4)
                    {
                        Sm.FocusGrd(Grd, Grd.CurCell.RowIndex, 8);
                    }
                    else
                    {
                        if (Grd.CurCell.ColIndex == 8)
                        {
                            Grd.Cells[Grd.CurCell.RowIndex + 1, 0].Value = Sm.GetGrdStr(Grd, Grd.CurCell.RowIndex, 0);
                            Grd.Cells[Grd.CurCell.RowIndex + 1, 1].Value = Sm.GetGrdStr(Grd, Grd.CurCell.RowIndex, 1);
                            Grd.Cells[Grd.CurCell.RowIndex + 1, 2].Value = Sm.GetGrdStr(Grd, Grd.CurCell.RowIndex, 2);
                            Grd.Cells[Grd.CurCell.RowIndex + 1, 5].Value = Sm.GetGrdStr(Grd, Grd.CurCell.RowIndex, 5);
                            Grd.Cells[Grd.CurCell.RowIndex + 1, 7].Value = Sm.GetGrdStr(Grd, Grd.CurCell.RowIndex, 7);
                            Grd.Cells[Grd.CurCell.RowIndex + 1, 8].Value = 0m;

                            Sm.FocusGrd(Grd, Grd.CurCell.RowIndex + 1, 3);

                            if (Grd.Rows.Count == Grd.CurCell.RowIndex + 1)
                            {
                                Grd.Rows.Add();
                                Sm.SetGrdNumValueZero(ref Grd, Grd.Rows.Count - 1, new int[] { 3, 4, 8 });
                            }
                        }
                    
                    }
                }

                if (e.KeyCode == Keys.Tab && Grd.CurCell != null && Grd.CurCell.RowIndex == Grd.Rows.Count - 1)
                {
                    int LastVisibleCol = Grd.CurCell.ColIndex;
                    for (int Col = LastVisibleCol; Col <= Grd.Cols.Count - 1; Col++)
                        if (Grd.Cols[Col].Visible) LastVisibleCol = Col;

                    if (Grd.CurCell.Col.Order == LastVisibleCol)
                    {
                        if (Btn1.Enabled)
                            Btn1.Focus();
                        else
                            Btn2.Focus();
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GrdItemColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e, iGrid Grd)
        {
            if (e.ColIndex == 0 && Sm.GetGrdStr(Grd, 0, 0).Length != 0)
            {
                for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                {
                    Sm.CopyGrdValue(Grd, Row, 0, Grd, 0, 0);
                    Sm.CopyGrdValue(Grd, Row, 1, Grd, 0, 1);
                    Sm.CopyGrdValue(Grd, Row, 2, Grd, 0, 2);
                    Sm.CopyGrdValue(Grd, Row, 5, Grd, 0, 5);
                }
            }
            if (e.ColIndex==4)
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        private string SetActualItCode2(iGrid Grd, int Row)
        {
            var cm = new MySqlCommand() { CommandText = mSQLForActualItCode };
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@P", Sm.GetGrdDec(Grd, Row, 3));
            Sm.CmParam<String>(ref cm, "@ItCtCode", mItCtRMPActual);

            string ItCode = Sm.GetValue(cm);
            return ItCode;
        }

        private void SetSQLForActualItCode()
        {
            var SQL = new StringBuilder();

            if (mType == 1)
            {
                SQL.AppendLine("Select A.ItCode ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select Trim(ItName) As ItName, Length, @P As Diameter ");
                SQL.AppendLine("    From TblItem ");
                SQL.AppendLine("    Where ItCode=@ItCode ");
                SQL.AppendLine(") B ");
                SQL.AppendLine("    On Trim(A.ItName)=B.ItName ");
                SQL.AppendLine("    And A.Length=B.Length ");
                SQL.AppendLine("    And A.Diameter=B.Diameter ");
                SQL.AppendLine("Where A.ItCtCode=@ItCtCode ");
                SQL.AppendLine("And A.ActInd='Y' ");
                SQL.AppendLine("Limit 1;");
            }
            else
            {
                SQL.AppendLine("Select A.ItCode ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select Trim(ItName) As ItName, Height, Length, @P As Width ");
                SQL.AppendLine("    From TblItem ");
                SQL.AppendLine("    Where ItCode=@ItCode ");
                SQL.AppendLine(") B ");
                SQL.AppendLine("    On Trim(A.ItName)=B.ItName ");
                SQL.AppendLine("    And A.Height=B.Height ");
                SQL.AppendLine("    And A.Length=B.Length ");
                SQL.AppendLine("    And A.Width=B.Width ");
                SQL.AppendLine(" Where A.ItCtCode=@ItCtCode ");
                SQL.AppendLine("And A.ActInd='Y' ");
                SQL.AppendLine("Limit 1;");
            }
            mSQLForActualItCode = SQL.ToString();
        }

        private void SetActualItCode(iGrid Grd)
        {
            if (Grd.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd, Row, 0).Length != 0)
                        Grd.Cells[Row, 7].Value = SetActualItCode2(Grd, Row);
            }
        }

        private void ComputeTotal(iGrid Grd, DXE.TextEdit Txt)
        {
            decimal Total = 0m;
            for (int Row = 0; Row <= Grd.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd, Row, 4).Length != 0) Total += Sm.GetGrdDec(Grd, Row, 4);
            Txt.Text = Sm.FormatNum(Total, 0);
            ComputeTotal();
        }

        private void ComputeTotal()
        {
            TxtTotal.Text = 
                Sm.FormatNum(
                    decimal.Parse(TxtTotal1.Text)+
                    decimal.Parse(TxtTotal2.Text)+
                    decimal.Parse(TxtTotal3.Text)+
                    decimal.Parse(TxtTotal4.Text)+
                    decimal.Parse(TxtTotal5.Text)+
                    decimal.Parse(TxtTotal6.Text)+
                    decimal.Parse(TxtTotal7.Text)+
                    decimal.Parse(TxtTotal8.Text)+
                    decimal.Parse(TxtTotal9.Text)+
                    decimal.Parse(TxtTotal10.Text)+
                    decimal.Parse(TxtTotal11.Text)+
                    decimal.Parse(TxtTotal12.Text)+
                    decimal.Parse(TxtTotal13.Text)+
                    decimal.Parse(TxtTotal14.Text)+
                    decimal.Parse(TxtTotal15.Text)
                    , 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueQueueNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueQueueNo, new Sm.RefreshLue2(SetLueQueueNo), string.Empty);
        }

        private void LueEmpCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmpCode1, new Sm.RefreshLue2(SetLueEmpCode), string.Empty);
        }

        private void LueEmpCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmpCode2, new Sm.RefreshLue2(SetLueEmpCode), string.Empty);
        }


        #region Item

        #region LueItCode_EditValueChanged

        private void LueItCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode1, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode2, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode3_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode3, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode4_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode4, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode5_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode5, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode6_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode6, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode7_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode7, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode8_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode8, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode9_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode9, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode10_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode10, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode11_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode11, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode12_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode12, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode13_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode13, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode14_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode14, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode15_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode15, new Sm.RefreshLue1(SetLueItCode));
        }

        #endregion

        #region LueItCode_Leave

        private void LueItCode1_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode1, Grd11);
        }

        private void LueItCode2_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode2, Grd12);
        }

        private void LueItCode3_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode3, Grd13);
        }

        private void LueItCode4_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode4, Grd14);
        }

        private void LueItCode5_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode5, Grd15);
        }

        private void LueItCode6_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode6, Grd16);
        }

        private void LueItCode7_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode7, Grd17);
        }

        private void LueItCode8_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode8, Grd18);
        }

        private void LueItCode9_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode9, Grd19);
        }

        private void LueItCode10_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode10, Grd20);
        }

        private void LueItCode11_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode11, Grd21);
        }

        private void LueItCode12_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode12, Grd22);
        }

        private void LueItCode13_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode13, Grd23);
        }

        private void LueItCode14_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode14, Grd24);
        }

        private void LueItCode15_Leave(object sender, EventArgs e)
        {
            LueItCodeLeave(sender, e, LueItCode15, Grd25);
        }

        #endregion

        #region LueItCode_KeyDown

        private void LueItCode1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd11, ref fAccept, e);
        }

        private void LueItCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd12, ref fAccept, e);
        }

        private void LueItCode3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd13, ref fAccept, e);
        }

        private void LueItCode4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd14, ref fAccept, e);
        }

        private void LueItCode5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd15, ref fAccept, e);
        }

        private void LueItCode6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd16, ref fAccept, e);
        }

        private void LueItCode7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd17, ref fAccept, e);
        }

        private void LueItCode8_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd18, ref fAccept, e);
        }

        private void LueItCode9_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd19, ref fAccept, e);
        }

        private void LueItCode10_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd20, ref fAccept, e);
        }

        private void LueItCode11_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd21, ref fAccept, e);
        }

        private void LueItCode12_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd22, ref fAccept, e);
        }

        private void LueItCode13_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd23, ref fAccept, e);
        }

        private void LueItCode14_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd24, ref fAccept, e);
        }

        private void LueItCode15_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd25, ref fAccept, e);
        }

        #endregion

        #endregion

        #region Shelf

        private void LueShelf1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf1, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf2, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf3_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf3, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf4_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf4, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf5_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf5, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf6_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf6, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf7_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf7, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf8_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf8, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf9_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf9, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf10_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf10, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf11_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf11, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf12_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf12, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf13_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf13, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf14_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf14, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf15_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShelf15, new Sm.RefreshLue2(SetLueShelf), "");
        }

        #endregion

        #region Label

        private void TxtLabelQty1_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty1, 1);
        }

        private void TxtLabelQty2_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty2, 1);
        }

        private void TxtLabelQty3_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty3, 1);
        }

        private void TxtLabelQty4_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty4, 1);
        }

        private void TxtLabelQty5_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty5, 1);
        }

        private void TxtLabelQty6_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty6, 1);
        }

        private void TxtLabelQty7_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty7, 1);
        }

        private void TxtLabelQty8_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty8, 1);
        }

        private void TxtLabelQty9_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty9, 1);
        }

        private void TxtLabelQty10_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty10, 1);
        }

        private void TxtLabelQty11_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty11, 1);
        }

        private void TxtLabelQty12_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty12, 1);
        }

        private void TxtLabelQty13_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty13, 1);
        }

        private void TxtLabelQty14_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty14, 1);
        }

        private void TxtLabelQty15_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLabelQty15, 1);
        }

        #endregion

        #region Warehouse

        private void LueWhsCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode1, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode3_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode3, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode4_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode4, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode5_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode5, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode6_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode6, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode7_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode7, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode8_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode8, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode9_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode9, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode10_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode10, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode11_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode11, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode12_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode12, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode13_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode13, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode14_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode14, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode15_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode15, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        #endregion

        #endregion

        #region Grid Event

        #region Item

        #region Grd_RequestEdit

        private void Grd11_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd11, LueItCode1);
        }

        private void Grd12_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd12, LueItCode2);
        }

        private void Grd13_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd13, LueItCode3);
        }

        private void Grd14_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd14, LueItCode4);
        }

        private void Grd15_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd15, LueItCode5);
        }

        private void Grd16_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd16, LueItCode6);
        }

        private void Grd17_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd17, LueItCode7);
        }

        private void Grd18_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd18, LueItCode8);
        }

        private void Grd19_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd19, LueItCode9);
        }

        private void Grd20_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd20, LueItCode10);
        }

        private void Grd21_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd21, LueItCode11);
        }

        private void Grd22_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd22, LueItCode12);
        }

        private void Grd23_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd23, LueItCode13);
        }

        private void Grd24_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd24, LueItCode14);
        }

        private void Grd25_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd25, LueItCode15);
        }

        #endregion

        #region Grd_KeyDown

        private void Grd11_KeyDown(object sender, KeyEventArgs e)
        {
            //if (TxtDocNo.Text.Length == 0)
            //{
            Sm.GrdRemoveRow(Grd11, e, BtnSave);
            ComputeTotal(Grd11, TxtTotal1);
            //}
            GrdItemKeyDown(Grd11, e, BtnFind, BtnSave);
        }

        private void Grd12_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd12, e, BtnSave);
            ComputeTotal(Grd12, TxtTotal2);
            GrdItemKeyDown(Grd12, e, BtnFind, BtnSave);
        }

        private void Grd13_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd13, e, BtnSave);
            ComputeTotal(Grd13, TxtTotal3);
            GrdItemKeyDown(Grd13, e, BtnFind, BtnSave);
        }

        private void Grd14_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd14, e, BtnSave);
            ComputeTotal(Grd14, TxtTotal4);
            GrdItemKeyDown(Grd14, e, BtnFind, BtnSave);
        }

        private void Grd15_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd15, e, BtnSave);
            ComputeTotal(Grd15, TxtTotal5);
            GrdItemKeyDown(Grd15, e, BtnFind, BtnSave);
        }

        private void Grd16_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd16, e, BtnSave);
            ComputeTotal(Grd16, TxtTotal6);
            GrdItemKeyDown(Grd16, e, BtnFind, BtnSave);
        }

        private void Grd17_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd17, e, BtnSave);
            ComputeTotal(Grd17, TxtTotal7);
            GrdItemKeyDown(Grd17, e, BtnFind, BtnSave);
        }

        private void Grd18_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd18, e, BtnSave);
            ComputeTotal(Grd18, TxtTotal8);
            GrdItemKeyDown(Grd18, e, BtnFind, BtnSave);
        }

        private void Grd19_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd19, e, BtnSave);
            ComputeTotal(Grd19, TxtTotal9);
            GrdItemKeyDown(Grd19, e, BtnFind, BtnSave);
        }

        private void Grd20_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd20, e, BtnSave);
            ComputeTotal(Grd20, TxtTotal10);
            GrdItemKeyDown(Grd20, e, BtnFind, BtnSave);
        }

        private void Grd21_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd21, e, BtnSave);
            ComputeTotal(Grd21, TxtTotal11);
            GrdItemKeyDown(Grd21, e, BtnFind, BtnSave);
        }

        private void Grd22_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd22, e, BtnSave);
            ComputeTotal(Grd22, TxtTotal12);
            GrdItemKeyDown(Grd22, e, BtnFind, BtnSave);
        }

        private void Grd23_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd23, e, BtnSave);
            ComputeTotal(Grd23, TxtTotal13);
            GrdItemKeyDown(Grd23, e, BtnFind, BtnSave);
        }

        private void Grd24_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd24, e, BtnSave);
            ComputeTotal(Grd24, TxtTotal14);
            GrdItemKeyDown(Grd24, e, BtnFind, BtnSave);
        }

        private void Grd25_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd25, e, BtnSave);
            ComputeTotal(Grd25, TxtTotal15);
            GrdItemKeyDown(Grd25, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grd_AfterCommitEdit

        private void Grd11_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd11, TxtTotal1);
        }

        private void Grd12_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd12, TxtTotal2);
        }

        private void Grd13_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd13, TxtTotal3);
        }

        private void Grd14_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd14, TxtTotal4);
        }

        private void Grd15_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd15, TxtTotal5);
        }

        private void Grd16_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd16, TxtTotal6);
        }

        private void Grd17_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd17, TxtTotal7);
        }

        private void Grd18_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd18, TxtTotal8);
        }

        private void Grd19_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd19, TxtTotal9);
        }

        private void Grd20_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd20, TxtTotal10);
        }

        private void Grd21_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd21, TxtTotal11);
        }

        private void Grd22_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd22, TxtTotal12);
        }

        private void Grd23_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd23, TxtTotal13);
        }

        private void Grd24_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd24, TxtTotal14);
        }

        private void Grd25_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd25, TxtTotal15);
        }

        #endregion

        #region Grd_CustomDrawCellForeground

        private void Grd11_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd11, sender, e);
        }

        private void Grd12_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd12, sender, e);
        }

        private void Grd13_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd13, sender, e);
        }

        private void Grd14_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd14, sender, e);
        }

        private void Grd15_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd15, sender, e);
        }

        private void Grd16_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd16, sender, e);
        }

        private void Grd17_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd17, sender, e);
        }

        private void Grd18_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd18, sender, e);
        }

        private void Grd19_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd19, sender, e);
        }

        private void Grd20_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd20, sender, e);
        }

        private void Grd21_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd21, sender, e);
        }

        private void Grd22_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd22, sender, e);
        }

        private void Grd23_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd23, sender, e);
        }

        private void Grd24_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd24, sender, e);
        }

        private void Grd25_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd25, sender, e);
        }

        #endregion

        #region Grd_ColHdrDoubleClick

        private void Grd11_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd11);
        }

        private void Grd12_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd12);
        }

        private void Grd13_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd13);
        }

        private void Grd14_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd14);
        }

        private void Grd15_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd15);
        }

        private void Grd16_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd16);
        }

        private void Grd17_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd17);
        }

        private void Grd18_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd18);
        }

        private void Grd19_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd19);
        }

        private void Grd20_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd20);
        }

        private void Grd21_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd21);
        }

        private void Grd22_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd22);
        }

        private void Grd23_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd23);
        }

        private void Grd24_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd24);
        }

        private void Grd25_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd25);
        }
        #endregion

        #endregion

        #endregion

        #endregion

        
    }
}
