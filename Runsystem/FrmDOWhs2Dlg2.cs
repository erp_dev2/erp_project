﻿#region Update
/*
    19/01/2020 [TKG/IMS] tambah informasi local code, specification
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOWhs2Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOWhs2 mFrmParent;
        private string mWhsCode = string.Empty, mTransferRequestDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmDOWhs2Dlg2(FrmDOWhs2 FrmParent, string WhsCode, string TransferRequestDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
            mTransferRequestDocNo = TransferRequestDocNo;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL(string Filter1, string Filter2, string Filter3)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.ItCode, D.ItName, D.ItCodeInternal, E.ItCtName, ");
            SQL.AppendLine("C.PropCode, F.PropName, C.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("D.InventoryUomCode, D.InventoryUomCode2, D.InventoryUomCode3, D.ItGrpCode, D.ItCodeInternal, D.Specification, ");
            SQL.AppendLine("IfNull(A.StockQty, 0)-IfNull(B.DOWhsQty, 0) As Qty, ");
            SQL.AppendLine("IfNull(A.StockQty2, 0)-IfNull(B.DOWhsQty2, 0) As Qty2, ");
            SQL.AppendLine("IfNull(A.StockQty3, 0)-IfNull(B.DOWhsQty3, 0) As Qty3, G.Remark ");
            SQL.AppendLine("From ( ");
	        SQL.AppendLine("    Select Lot, Bin, Source, Qty As StockQty, Qty2 As StockQty2, Qty3 As StockQty3 ");
	        SQL.AppendLine("    From TblStockSummary ");
	        SQL.AppendLine("    Where Qty>0 ");
            SQL.AppendLine("    And WhsCode=@WhsCode ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select T2.Lot, T2.Bin, T2.Source, ");
            SQL.AppendLine("    Sum(T2.Qty) As DOWhsQty, Sum(T2.Qty2) As DOWhsQty2, Sum(T2.Qty3) As DOWhsQty3 ");
	        SQL.AppendLine("    From TblDOWhsHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl T2 On T1.DocNo=T2.DocNo And T2.ProcessInd='O' And T2.CancelInd='N' "); 
	        SQL.AppendLine("    Where T1.CancelInd='N' And IfNull(T1.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("    And T1.WhsCode=@WhsCode ");
            SQL.AppendLine("    Group By T2.Lot, T2.Bin, T2.Source");
            SQL.AppendLine(") B On A.Lot=B.Lot And A.Bin=B.Bin And A.Source=B.Source");
            SQL.AppendLine("Inner Join TblStockPrice C On A.Source=C.Source ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode " + Filter1);
            SQL.AppendLine("Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
            SQL.AppendLine("Left Join TblProperty F On C.PropCode=F.PropCode ");
            SQL.AppendLine("Left Join TblTransferRequestWhsDtl G On C.ItCode=G.ItCode And G.DocNo=@TransferRequestDocNo ");
            SQL.AppendLine("Where IfNull(A.StockQty, 0)-IfNull(B.DOWhsQty, 0)>0 " + Filter2 + Filter3);
            SQL.AppendLine(" Order By D.ItName, C.BatchNo;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.ReadOnly = false;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Item's Code", 
                        "",
                        "Item's Name",
                        "Batch#",
                       
                        //6-10
                        "Source",
                        "Lot",
                        "Bin",
                        "Quantity",
                        "UoM",
                        
                        
                        //11-15
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Cost Category",

                        //16-20
                        "Account#",
                        "Group",
                        "Remark",
                        "Local Code",
                        "Specification"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11, 13 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 7, 8, 11, 12, 13, 14, 15, 16, 17, 20 }, false);
            ShowInventoryUomCode();
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[17].Visible = true;
                Grd1.Cols[17].Move(4);
            }
            Grd1.Cols[19].Move(5);
            Grd1.Cols[20].Move(6);
            Sm.SetGrdAutoSize(Grd1);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 11, 12 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 13, 14 }, true);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 7, 8, 20 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string
                    Filter1 = " ",
                    Filter2 = string.Empty,
                    Filter3 = " And Locate(Concat('##', D.ItCode, '##'), @SelectedDORequestItem)>0 ";

                var cm = new MySqlCommand();
                Sm.GenerateSQLFilterForInventory(ref cm, ref Filter2, "A", ref mFrmParent.Grd1, 7, 8, 9);
                Filter2 = (Filter2.Length > 0) ? " And (" + Filter2 + ") " : " And 0=0 ";
                Sm.CmParam<String>(ref cm, "@TransferRequestDocNo", mTransferRequestDocNo);
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@SelectedDORequestItem", mFrmParent.GetSelectedDORequestItem());

                Sm.FilterStr(ref Filter1, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter1, Filter2, Filter3),
                        new string[] 
                        { 
                            //0
                            "ItCode",

                            //1-5
                            "ItName", "BatchNo", "Source", "Lot", "Bin",
                            
                            //6-10
                            "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", 
                            
                            //11-15
                            "InventoryUomCode3", "ItGrpCode", "Remark", "ItCodeInternal", "Specification"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 15);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 17);
                        if (mFrmParent.mIsDOWhsCopyTransferRequestDRemark)
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 26, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 20);

                        mFrmParent.ComputeBalance();                     
                        mFrmParent.Grd1.Rows.Add();

                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 10, 11, 13, 14, 16, 17 });
                    }
                }
            }
            mFrmParent.SetAsset();
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), Sm.GetGrdStr(Grd1, Row, 2)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 6), Sm.GetGrdStr(Grd1, Row, 5)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 7), Sm.GetGrdStr(Grd1, Row, 6)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 8), Sm.GetGrdStr(Grd1, Row, 7)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 9), Sm.GetGrdStr(Grd1, Row, 8))
                    )
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
    }
}
