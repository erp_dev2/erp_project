﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmShopFloorControlFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmShopFloorControl mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmShopFloorControlFind(FrmShopFloorControl FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.ProductionOrderDocNo, B.DocType, ");
            SQL.AppendLine("B.ProdStartDt, B.ProdEndDt, B.ProductionRoutingDocNo, F.WorkCenterDocNo, I.DocName As WorkCenterDocName, C.BomDocNo, ");
            SQL.AppendLine("Case B.DocType When '1' Then B.SODocNo Else B.MakeToStockDocNo End As SOMTSDocNo, ");
            SQL.AppendLine("Case B.DocType When '1' Then G.ItCode Else H.ItCode End As ItCode, ");
            SQL.AppendLine("Case B.DocType When '1' Then G.ItName Else H.ItName End As ItName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblProductionOrderHdr B On A.ProductionOrderDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblProductionOrderDtl C On A.ProductionOrderDocNo=C.DocNo And A.ProductionOrderDNo=C.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, T2.DNo, T4.ItCode ");
            SQL.AppendLine("    From TblSOHdr T1 ");
            SQL.AppendLine("    Inner Join TblSODtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Left Join TblCtQtDtl T3 On T1.CtQtDocNo=T3.DocNo And T2.CtQtDNo=T3.DNo ");
            SQL.AppendLine("    Left Join TblItemPriceDtl T4 On T3.ItemPriceDocNo=T4.DocNo And T3.ItemPriceDNo=T4.DNo ");
            SQL.AppendLine(") D On B.SODocNo=D.DocNo And B.SODNo=D.DNo ");
            SQL.AppendLine("Left Join TblMakeToStockDtl E On B.MakeToStockDocNo=E.DocNo And B.MakeToStockDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblProductionRoutingDtl F On B.ProductionRoutingDocNo=F.DocNo And C.ProductionRoutingDNo=F.DNo ");
            SQL.AppendLine("Left Join TblItem G On D.ItCode=G.ItCode ");
            SQL.AppendLine("Left Join TblItem H On E.ItCode=H.ItCode ");
            SQL.AppendLine("Inner Join TblWorkCenterHdr I On F.WorkCenterDocNo=I.DocNo ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Production Order#",
                        "", 
                        
                        //6-10
                        "Start Date",
                        "End Date",
                        "SO#/MTS#",
                        "", 
                        "Type",
                        
                        //11-15
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Routing#",
                        "Work Center#",
                        "Work Center"+Environment.NewLine+"Name",
                        
                        //16-20
                        "BOM#",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        
                        //21-22
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 60, 150, 20, 
                        
                        //6-10
                        80, 80, 130, 20, 80, 

                        //11-15
                        80, 250, 130, 130, 200, 

                        //16-20
                        130, 80, 100, 100, 100,

                        //21-22
                        100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 5, 9 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 6, 7, 18, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 17, 18, 19, 20, 21, 22 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 11, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtProductionOrderDocNo.Text, "ProductionOrderDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenterDocNo.Text, new string[] { "WorkCenterDocNo", "WorkCenterDocName" });
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By DocDt, DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "CancelInd", "ProductionOrderDocNo", "ProdStartDt", "ProdEndDt",   
                            
                            //6-10
                            "SOMTSDocNo", "DocType", "ItCode", "ItName", "ProductionRoutingDocNo",   

                            //11-15
                            "WorkCenterDocNo", "WorkCenterDocName", "BomDocNo", "CreateBy", "CreateDt",  
                            
                            //16-17
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 17);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 17);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmProductionOrder(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (Sm.GetGrdStr(Grd1, e.RowIndex, 10) == "1")
            {
                if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
                {
                    var f = new FrmSO2(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                    f.ShowDialog();
                }
            }
            else
            {
                if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
                {
                    var MenuCodeForMakeToStockStandard = Sm.GetParameter("MenuCodeForMakeToStockStandard");
                    var MenuCodeForMakeToStockMaklon = Sm.GetParameter("MenuCodeForMakeToStockMaklon");
                    bool IsMTSStandard = Sm.IsDataExist(
                        "Select DocNo From TblMakeToStockHdr Where DocNo=@Param And BomDocNo Is Null;",
                        Sm.GetGrdStr(Grd1, e.RowIndex, 8));

                    var f = new FrmMakeToStock(mFrmParent.mMenuCode);
                    if (IsMTSStandard)
                    {
                        if (MenuCodeForMakeToStockStandard.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockStandard;
                            f.Tag = MenuCodeForMakeToStockStandard;
                        }
                    }
                    else
                    {
                        if (MenuCodeForMakeToStockMaklon.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockMaklon;
                            f.Tag = MenuCodeForMakeToStockMaklon;
                        }
                    }
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmProductionOrder(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (Sm.GetGrdStr(Grd1, e.RowIndex, 10) == "1")
            {
                if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
                {
                    var f = new FrmSO2(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                    f.ShowDialog();
                }
            }
            else
            {
                if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
                {
                    var MenuCodeForMakeToStockStandard = Sm.GetParameter("MenuCodeForMakeToStockStandard");
                    var MenuCodeForMakeToStockMaklon = Sm.GetParameter("MenuCodeForMakeToStockMaklon");
                    bool IsMTSStandard = Sm.IsDataExist(
                        "Select DocNo From TblMakeToStockHdr Where DocNo=@Param And BomDocNo Is Null;",
                        Sm.GetGrdStr(Grd1, e.RowIndex, 8));

                    var f = new FrmMakeToStock(mFrmParent.mMenuCode);
                    if (IsMTSStandard)
                    {
                        if (MenuCodeForMakeToStockStandard.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockStandard;
                            f.Tag = MenuCodeForMakeToStockStandard;
                        }
                    }
                    else
                    {
                        if (MenuCodeForMakeToStockMaklon.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockMaklon;
                            f.Tag = MenuCodeForMakeToStockMaklon;
                        }
                    }
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtProductionOrderDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProductionOrderDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Production order#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtWorkCenterDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWorkCenterDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work Center");
        }

        #endregion

        #endregion
    }
}
