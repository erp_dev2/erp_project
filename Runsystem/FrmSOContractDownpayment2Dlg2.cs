﻿#region Update
/*
    12/03/2020 [TKG/IMS] New Application
    12/06/2020 [DITA/IMS] rombak termin dp 2
    24/09/2020 [WED/IMS] tambah informasi No dari SO Contract berdasarkan parameter IsDetailShowColumnNumber.
    19/11/2020 [IBL/IMS] penambahan rumus nilai default pada kolom Termin/DP1
    16/12/2020 [DITA/IMS] Specification ambil dari remark detail so contract
    01/02/2021 [WED/IMS] ambil DO Verify yang dari DO To Customer
    11/06/2021 [BRI/IMS] penyesuaian di menu Sales Invoice Project (Membuat jurnal receiving per SOCnya)
    13/06/2021 [TKG/IMS] tambah amount before tax
    16/06/2021 [VIN/IMS] tambah item dismantle
    23/06/2021 [BRI/IMS] memunculkan local doc di detail tab inventory dan service
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSOContractDownpayment2Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSOContractDownpayment2 mFrmParent;
        
        #endregion

        #region Constructor

        public FrmSOContractDownpayment2Dlg2(FrmSOContractDownpayment2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                  Grd1,
                  new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "",
                        "Verification#",
                        "Verification DNo#",
                        "",
                        "SO Contract#",

                        //6-10
                        "",
                        "Warehouse",
                        "Item's Code",
                        "Item's Name",
                        "Local Code",
                        
                        //11-15
                        "Specification",
                        "Batch#",
                        "Source",
                        "Quantity",
                        "UoM",

                        //16-20
                        "Price",
                        "Amount",
                        "SO Contract's"+Environment.NewLine+"No",
                        "Local DO"+Environment.NewLine+"Verification#",
                        "Item's Code"+Environment.NewLine+"Dismantle",

                        //21
                        "Item's Name"+Environment.NewLine+"Dismantle"
                    },
                   new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 150, 0, 20, 150, 
                        
                        //6-10
                        20, 200, 130,200, 100, 
                        
                        //11-15
                        200, 180, 180,120, 80, 
                        
                        //16-20
                        130, 130, 100, 150, 130,
                        
                        //21
                        200,                 
                    }
              );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4, 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
            if (!mFrmParent.mIsDetailShowColumnNumber)
                Sm.GrdColInvisible(Grd1, new int[] { 18 });
            Grd1.Cols[18].Move(9);
            Grd1.Cols[19].Move(5);
            Grd1.Cols[20].Move(12);
            Grd1.Cols[21].Move(13);
            Sm.GrdFormatDec(Grd1, new int[] { 14, 16, 17 }, 0);
           
            Sm.SetGrdProperty(Grd1, false);
        }
        
        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            #region Old Code
            //SQL.AppendLine("Select T.* From ( ");
            //SQL.AppendLine("Select  ");
            //SQL.AppendLine("A.DocNo, A.DocDt, B.DNo, F.SODocNo As SOContractDocNo,  ");
            //SQL.AppendLine("K.WhsName, H.No, H.ItCode, J.ItName, J.ItCodeInternal, H.Remark Specification,  ");
            //SQL.AppendLine("B.BatchNo, B.Source, B.Qty, J.SalesUomCode, H.UPrice, B.Qty*H.UPrice As Amt, ");
            //SQL.AppendLine(" Case When ");
            //SQL.AppendLine("Exists(Select 1 FROM TblSOContractDtl4 WHERE DocNo=@SOContractDocNo AND ItCode=H.ItCode LIMIT 1) Then 'Y' ELSE 'N' End ");
            //SQL.AppendLine(" AS ServiceInd, ");
            //SQL.AppendLine("Case When ");
            //SQL.AppendLine("Exists(Select 1 FROM TblSOContractDtl5 WHERE DocNo=@SOContractDocNo AND ItCode=H.ItCode LIMIT 1) Then 'Y' ELSE 'N' End ");
            //SQL.AppendLine("AS InventoryInd  ");
            //SQL.AppendLine("From TblDOCtVerifyHdr A  ");
            //SQL.AppendLine("Inner Join tblDOCtVerifyDtl B   ");
            //SQL.AppendLine(" On A.DocNo=B.DocNo  ");
            //SQL.AppendLine(" And Not Exists(  ");
            //SQL.AppendLine("    Select 1 From TblSOContractDownpayment2Hdr T1, TblSOContractDownpayment2Dtl T2  ");
            //SQL.AppendLine("    Where T1.DocNo=T2.DocNo  ");
            //SQL.AppendLine("    And T1.CancelInd='N'  ");
            //SQL.AppendLine("    And T2.DOCtVerifyDocNo=B.DocNo  ");
            //SQL.AppendLine("    And T2.DOCtVerifyDNo=B.DNo  ");
            //SQL.AppendLine(" )  ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select 1 From TblSOContractDownpayment2Hdr T1, TblSOContractDownpayment2Dtl2 T2  ");
            //SQL.AppendLine("    Where T1.DocNo=T2.DocNo  ");
            //SQL.AppendLine("     And T1.CancelInd='N'  ");
            //SQL.AppendLine("    And T2.DOCtVerifyDocNo=B.DocNo  ");
            //SQL.AppendLine("    And T2.DOCtVerifyDNo=B.DNo  ");
            //SQL.AppendLine(")  ");
            //SQL.AppendLine("Inner Join TblDOCt2Hdr C On B.DOCt2DocNo=C.DocNo  ");
            //SQL.AppendLine("Inner Join TblDOCt2Dtl2 D On B.DOCt2DocNo=D.DocNo   ");
            //SQL.AppendLine("Inner Join TblDRHdr E On C.DRDocNo=E.DocNo   ");
            //SQL.AppendLine("Inner Join TblDRDtl F On E.DocNo=F.DocNo And D.DRDNo=F.DNo  And F.SODocNo=@SOContractDocNo ");
            //SQL.AppendLine("Inner Join TblSOContractHdr G On F.SODocNo=G.DocNo   ");
            //SQL.AppendLine("Inner Join TblSOContractDtl H On F.SODocNo=H.DocNo And F.SODNo=H.DNo  ");
            //SQL.AppendLine("Inner Join TblDOCt2Dtl I On B.DOCt2DocNo=I.DocNo And H.ItCode=I.ItCode  ");
            //SQL.AppendLine("Left Join TblItem J On H.ItCode=J.ItCode  ");
            //SQL.AppendLine("Left Join TblWarehouse K On C.WhsCode=K.WhsCode  ");
            //SQL.AppendLine("Where A.CancelInd='N'  ");
            //SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine(Filter);
            //SQL.AppendLine(") T Where ServiceInd='Y'  ");
            //SQL.AppendLine("Order By DocDt Desc, DocNo;");
            #endregion

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("    Select Distinct ");
            SQL.AppendLine("    A.DocNo, A.DocDt, B.DNo, C.SOContractDocNo, ");
            SQL.AppendLine("    G.WhsName, E.No, E.ItCode, F.ItName, F.ItCodeInternal, E.Remark Specification, ");
            SQL.AppendLine("    B.BatchNo, B.Source, B.Qty, F.SalesUomCode, E.UPrice, B.Qty*E.UPrice As Amt, ");
            SQL.AppendLine("    Case When ");
            SQL.AppendLine("    Exists(Select 1 FROM TblSOContractDtl4 WHERE DocNo=@SOContractDocNo AND ItCode=E.ItCode LIMIT 1) Then 'Y' ELSE 'N' End ");
            SQL.AppendLine("    AS ServiceInd, ");
            SQL.AppendLine("    Case When ");
            SQL.AppendLine("    Exists(Select 1 FROM TblSOContractDtl5 WHERE DocNo=@SOContractDocNo AND ItCode=E.ItCode LIMIT 1) Then 'Y' ELSE 'N' End ");
            SQL.AppendLine("    AS InventoryInd, ");
            SQL.AppendLine("    A.LocalDocNo, H.ItCode ItCodeDismantle, H.ItName ItNameDismantle ");
            SQL.AppendLine("    From TblDOCtVerifyHdr A ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And Not Exists ( ");
            SQL.AppendLine("        Select 1 ");
            SQL.AppendLine("         From TblSOContractDownpayment2Hdr T1, TblSOContractDownpayment2Dtl T2 Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("            And T1.CancelInd='N' ");
            SQL.AppendLine("            And T2.DOCtVerifyDocNo=B.DocNo ");
            SQL.AppendLine("            And T2.DOCtVerifyDNo=B.DNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And Not Exists ( ");
            SQL.AppendLine("        Select 1 ");
            SQL.AppendLine("         From TblSOContractDownpayment2Hdr T1, TblSOContractDownpayment2Dtl2 T2 Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("            And T1.CancelInd='N' ");
            SQL.AppendLine("            And T2.DOCtVerifyDocNo=B.DocNo ");
            SQL.AppendLine("            And T2.DOCtVerifyDNo=B.DNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Distinct X1.DocNo, X2.DNo, X3.WhsCode, X4.SOContractDocNo, X4.SOContractDNo ");
            SQL.AppendLine("        From TblDOCtVerifyHdr X1 ");
            SQL.AppendLine("        Inner Join TblDOCtVerifyDtl X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("            And (X1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("            And X1.DOCt2DocNo Is Not Null ");
            SQL.AppendLine("        Inner Join TblDOCt2Hdr X3 On X1.DOCt2DocNo = X3.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl X4 On X3.DocNo = X4.DocNo And X2.DOCt2DNo = X4.DNo ");
            SQL.AppendLine("            And X4.SOContractDocNo Is Not Null ");
            SQL.AppendLine("            And X4.SOContractDocNo = @SOContractDocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Distinct X1.DocNo, X2.DNo, X3.WhsCode, X3.SOContractDocNo, X4.SOContractDNo ");
            SQL.AppendLine("        From TblDOCtVerifyHdr X1 ");
            SQL.AppendLine("        Inner Join TblDOCtVerifyDtl X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("            And (X1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("            And X1.DOCtDocNo Is Not Null ");
            SQL.AppendLine("        Inner Join TblDOCtHdr X3 On X1.DOCtDocNo = X3.DocNo ");
            SQL.AppendLine("            And X3.SOContractDocNo Is Not Null ");
            SQL.AppendLine("            And X3.SOContractDocNo = @SOContractDocNo ");
            SQL.AppendLine("        Inner Join TblDOCtDtl X4 On X3.DocNo = X4.DocNo And X2.DOCtDNo = X4.DNo ");
            SQL.AppendLine("    ) C On B.DocNo = C.DocNo And B.DNo = C.DNo ");
            SQL.AppendLine("    Inner Join TblSOContractHdr D On C.SOContractDocNo = D.DocNo ");
            SQL.AppendLine("    Inner Join TblSOContractDtl E On C.SOContractDocNo = E.DocNo And C.SOContractDNo = E.DNo ");
            SQL.AppendLine("    Inner Join TblItem F On E.ItCode = F.ItCode ");
            SQL.AppendLine("    Inner Join TblWarehouse G On C.WhsCode = G.WhsCode ");
            SQL.AppendLine("    Left JOIN  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT A.DocNo, A.DNo, A.ItCode, C.ItCodeDismantle From TblSOContractDtl A ");
            SQL.AppendLine("        Inner Join TblSOContractDtl4 B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("            And A.DocNo = @SOContractDocNo  ");
            SQL.AppendLine("            And A.ItCode = B.ItCode  And A.No = B.No  ");
            SQL.AppendLine("        Inner Join TblBOQDtl2 C On B.BOQDocNo = C.DocNo  ");
            SQL.AppendLine("           And A.ItCode = C.ItCode And B.SeqNo = C.SeqNo  ");
            SQL.AppendLine("        Left Join TblItem D On C.ItCodeDismantle = D.ItCode ");
            SQL.AppendLine("     ) C2 ON E.DocNo=C2.DocNo AND E.DNo=C2.DNo AND F.ItCode=C2.ItCode ");
            SQL.AppendLine("     LEFT JOIN tblitem H ON C2.ItCodeDismantle=H.ItCode  ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            
            SQL.AppendLine(Filter);
            SQL.AppendLine(") T Where ServiceInd='Y' ");
            SQL.AppendLine("Order By DocDt Desc, DocNo; ");

            return SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
          
        }

        override protected void ShowData()
        {
            if (
               Sm.IsDteEmpty(DteDocDt1, "Start date") ||
               Sm.IsDteEmpty(DteDocDt2, "End date") ||
               Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
               ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string
                   DocNo = string.Empty,
                   DNo = string.Empty,
                   Filter = string.Empty;

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        DocNo = Sm.GetGrdStr(mFrmParent.Grd1, r, 2);
                        DNo = Sm.GetGrdStr(mFrmParent.Grd1, r, 3);
                        if (DocNo.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " (B.DocNo=@DocNo0" + r.ToString() + " And B.DNo=@DNo0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                            Sm.CmParam<String>(ref cm, "@DNo0" + r.ToString(), DNo);
                        }
                    }
                }

                if (Filter.Length != 0)
                    Filter = " And Not ( " + Filter + ") ";
                else
                    Filter = " ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.CmParam<String>(ref cm, "@SOContractDocNo", mFrmParent.TxtSOContractDocNo.Text);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                            //1-5
                            "DNo", "SOContractDocNo", "WhsName", "ItCode", "ItName",  
                            //6-10
                            "ItCodeInternal", "Specification", "BatchNo", "Source", "Qty",  
                            //11-15
                            "SalesUomCode", "UPrice", "Amt", "No", "LocalDocNo", 
                            //16-17
                            "ItCodeDismantle", "ItNameDismantle"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;
            string mPercentage = Sm.GetValue("Select Percentage From TblSOContractDownpaymentHdr Where DocNo = @Param;", mFrmParent.TxtSOContractDownpaymentDocNo.Text);
            mPercentage = Sm.FormatNum(mPercentage, 0);

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                {
                    if (IsChoose == false) IsChoose = true;

                    Row1 = mFrmParent.Grd1.Rows.Count - 1;
                    Row2 = Row;

                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 7);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 8);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 9);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 10);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 11);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 12);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 13);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 14);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 15);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 16);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 17);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 18);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 19);
                    mFrmParent.Grd1.Cells[Row1, 18].Value = 0m;
                    mFrmParent.Grd1.Cells[Row1, 19].Value =
                        Sm.GetGrdDec(mFrmParent.Grd1, Row1, 15) -
                        Sm.GetGrdDec(mFrmParent.Grd1, Row1, 16);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 20);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 21);
                    
                    if (mFrmParent.TxtSOContractDownpaymentDocNo.Text.Length > 0)
                        mFrmParent.Grd1.Cells[Row1, 16].Value = Sm.GetGrdDec(Grd1, Row2, 17) * Decimal.Parse(mPercentage);

                    mFrmParent.Grd1.Rows.Add();
                    Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 12, 14, 15, 16, 17, 18, 19, 20 });
                }
            }
            mFrmParent.ComputeAmt();
            mFrmParent.ComputeAmtBeforeTax();
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 data.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            var key = string.Concat(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 3));
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(key,
                    string.Concat(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), Sm.GetGrdStr(mFrmParent.Grd1, Index, 3)))) 
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDOCtVerify("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSOContract2("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 20).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSOContractDownpayment("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 20);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmDOCtVerify("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmSOContract2("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 20).Length != 0)
            {
                var f = new FrmSOContractDownpayment("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 20);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        #endregion

        #endregion
    }
}
