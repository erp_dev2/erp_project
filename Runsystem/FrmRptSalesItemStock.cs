﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSalesItemStock : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            UomCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptSalesItemStock(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item's Code",
                        "Item's"+Environment.NewLine+"Local Code",
                        "Item's Name", 
                        "Foreign Name",
                        "Item's Category",
                        
                        //6-10
                        "Item's Group",
                        "Stock",
                        "Uom",
                        "Stock 2",
                        "Uom 2",

                        //11-15
                        "Stock 3",
                        "Uom 3",
                        "Currency",
                        "Price",
                        "Value",

                        //16-17
                        "Value 2",
                        "Value 3"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 100, 300, 250, 120, 
                        
                        //6-10
                        250, 100, 80, 100, 80,

                        //11-15
                        100, 80, 60, 100, 100,

                        //16-17
                        100, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 9, 11, 14, 15, 16, 17 }, 2);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 6 }, false);

            UomCode = Sm.GetValue("SELECT ParValue FROM TblParameter WHERE ParCode = 'NumberOfInventoryUomCode'");

            if (UomCode == "1")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 16, 17 }, false);
            }
            else if (UomCode == "2")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 17 }, false);
            }

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 6 }, !ChkHideInfoInGrd.Checked);

            UomCode = Sm.GetValue("SELECT ParValue FROM TblParameter WHERE ParCode = 'NumberOfInventoryUomCode'");

            if (UomCode == "1")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 16, 17 }, !ChkHideInfoInGrd.Checked);
            }
            else if (UomCode == "2")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 17 }, !ChkHideInfoInGrd.Checked);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.ItCode, B.ItName, B.ItCodeInternal, B.ForeignName, D.ItCtName, E.ItGrpName, ");
            SQL.AppendLine("SUM(Qty) Stock, B.InventoryUomCode, SUM(Qty2) Stock2, B.InventoryUomcode2, ");
            SQL.AppendLine("SUM(Qty3) Stock3, B.InventoryUomCode3, IFNULL(C.Uprice, 0) UPrice, C.CurCode, ");
            SQL.AppendLine("(SUM(Qty)*IFNULL(C.Uprice, 0)) AS Value, (SUM(Qty2)*IFNULL(C.Uprice, 0)) AS Value2, ");
            SQL.AppendLine("(SUM(Qty3)*IFNULL(C.Uprice, 0)) AS Value3 ");
            SQL.AppendLine("FROM TblStockSummary A ");
            SQL.AppendLine("INNER JOIN TblItem B On A.ItCode = B.ItCode AND B.SalesItemInd = 'Y' ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("    SELECT B.ItCode, A.CurCode, B.UPrice  FROM TblItemPriceHdr A ");
            SQL.AppendLine("    INNER JOIN TblItemPriceDtl B ON A.Docno = B.DocNo ");
            SQL.AppendLine("    WHERE A.ActInd = 'Y' ");
            SQL.AppendLine(")C ON A.ItCode = C.ItCode ");
            SQL.AppendLine("INNER JOIN TblItemCategory D ON B.ItCtCode = D.ItCtCode ");
            SQL.AppendLine("LEFT JOIN TblItemGroup E ON B.ItGrpCode = E.ItGrpCode ");
            SQL.AppendLine("WHERE A.Qty <> 0 ");
            
            mSQL =  SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Sm.ClearGrd(Grd1, false);

                Cursor.Current = Cursors.WaitCursor;
                string Filter = " AND 0 = 0 ";
                var cm = new MySqlCommand();
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "B.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "B.ItCodeInternal", "B.ItName", "B.ForeignName", "E.ItGrpName" });

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " GROUP BY A.ItCode,B.ItName, B.ItCodeInternal, B.ForeignName,B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3 ORDER BY B.ItName;",
                new string[]
                    {
                        //0
                        "ItCode", 

                        //1-5
                        "ItName", "ItCodeInternal", "ForeignName", "ItCtName", "ItGrpName", 

                        //6-10
                        "Stock", "InventoryUomCode", "Stock2", "InventoryUomcode2", "Stock3",

                        //11-15
                        "InventoryUomCode3", "UPrice", "CurCode", "Value", "Value2",

                        //16
                        "Value3"
                    },
                    (
                        MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Category");
        }

        #endregion

        #endregion
    }
}
