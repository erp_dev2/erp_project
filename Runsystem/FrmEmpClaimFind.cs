﻿#region Update
/*
    20/10/2017 [TKG] ubah tampilan
 *  21/04/2021 [ICA] menambah fitur upload file
    14/03/2023 [DITA/PHT] tambah kolom voucher#
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmEmpClaimFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmEmpClaim mFrmParent;
        private string mSQL = string.Empty;

        internal byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmEmpClaimFind(FrmEmpClaim FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("when 'O' Then 'Outstanding'");
            SQL.AppendLine("when 'A' Then 'Approved'");
            SQL.AppendLine("When 'C' Then 'Cancel'");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("A.CancelInd, A.EmpCode, B.EmpName, B.EmpCodeOld, C.DeptName, D.PosName, ");
            SQL.AppendLine("E.ClaimName, A.Amt, A.Remark, A.FileName, G.DocNo VoucherDocNo, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblEmpClaimHdr A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("And B.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(B.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Left Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode=D.PosCode ");
            SQL.AppendLine("Inner Join TblClaim E On A.ClaimCode=E.ClaimCode ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr F On A.VoucherRequestDocNo=F.DocNo ");
            SQL.AppendLine("Left Join TblVoucherHdr G On F.DocNo=G.VoucherRequestDocNo ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Status",
                    "Cancel",
                    "Employee's"+Environment.NewLine+"Code",

                    //6-10
                    "Employee's Name",
                    "Old Code",
                    "Department",
                    "Position",
                    "Claim",
                    
                    //11-15
                    "Amount",
                    "Remark",
                    "File Name", 
                    "",
                    "Created"+Environment.NewLine+"By",
                    
                    //16-20
                    "Created"+Environment.NewLine+"Date",
                    "Created"+Environment.NewLine+"Time",
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time",

                    //21
                    "Voucher#"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 80, 60, 100, 
                    
                    //6-10
                    200, 100, 150, 150, 200,  
                    
                    //11-15
                    120, 200, 200, 20, 100, 

                    //16-20
                    100, 100, 100, 100, 100,

                    //21
                    120
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 14 });
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 16, 19 });
            Sm.GrdFormatTime(Grd1, new int[] { 17, 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 9, 12, 15, 16, 17, 18, 19, 20 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20, 21 });
            Grd1.Cols[21].Move(15);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 9, 12, 15, 16, 17, 18, 19, 20 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "B.EmpCode", "B.EmpName", "B.EmpCodeOld" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "StatusDesc", "CancelInd", "EmpCode", "EmpName", 

                            //6-10
                            "EmpCodeOld", "DeptName", "PosName", "ClaimName", "Amt", 
                            
                            //11-15
                            "Remark", "FileName", "CreateBy", "CreateDt", "LastUpBy", 

                            //16-17
                            "LastUpDt", "VoucherDocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 20, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                        }, true, false, true, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length != 0)
            {
                DownloadFile(mFrmParent.mHostAddrForFTPClient, mFrmParent.mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 13), mFrmParent.mUsernameForFTPClient, mFrmParent.mPasswordForFTPClient, mFrmParent.mSharedFolderForFTPClient);
                SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                SFD.DefaultExt = "";
                SFD.AddExtension = true;

                if (downloadedData != null && downloadedData.Length != 0)
                {

                    if (SFD.ShowDialog() == DialogResult.OK)
                    {
                        Application.DoEvents();

                        //Write the bytes to a file
                        FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                        newFile.Write(downloadedData, 0, downloadedData.Length);
                        newFile.Close();
                        MessageBox.Show("Saved Successfully");
                    }
                }
                else
                    MessageBox.Show("No File was Downloaded Yet!");
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method
        
        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        #endregion 

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        #endregion

    }
}
