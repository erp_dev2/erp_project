﻿#region Update
// 01/03/2018 [HAR] Asset masuknya ke detail bukan header
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCtQt3Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmCtQt3 mFrmParent;
        private int mRow;
        #endregion

        #region Constructor

        public FrmCtQt3Dlg(FrmCtQt3 FrmParent, int Row)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            this.Text = "List of Asset";
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            ShowData();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-3
                        "Asset Code", 
                        "Asset Name",
                        "Display Name",
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAsset.Text, new string[] { "AssetCode", "AssetName", "DisplayName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select AssetCode, AssetName, DisplayName " +
                        "From TblAsset " +
                        "Where RentedInd = 'Y' And SoldInd = 'Y' " +
                        Filter + " Order By AssetName ;",
                        new string[] 
                        { 
                            //0
                            "AssetCode", 

                            //1-3
                            "AssetName", "DisplayName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                if (Sm.IsFindGridValid(Grd1, 1))
                {
                    Sm.CopyGrdValue(mFrmParent.Grd2, mRow, 19, Grd1, Grd1.CurRow.Index, 1);
                    Sm.CopyGrdValue(mFrmParent.Grd2, mRow, 20, Grd1, Grd1.CurRow.Index, 2);
                    Sm.CopyGrdValue(mFrmParent.Grd2, mRow, 21, Grd1, Grd1.CurRow.Index, 3);
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        private bool IsCityCodeAlreadyChosen(int Row)
        {
            
            return false;
        }

        #endregion

        #endregion

        #region Event

        private void TxtAsset_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAsset_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void Grd1_DoubleClick(object sender, EventArgs e)
        {
            ChooseData();
        }
        #endregion

       

       
    }
}
