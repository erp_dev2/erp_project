﻿#region Update
    // 05/06/2017 [ARI] Reporting Item Property
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptItemProperty : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private int mPropertyCount = 10;
        private bool mIsFilterByItCt = false; 

        #endregion

        #region Constructor

        public FrmRptItemProperty(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter(); 
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mPropertyCount = (int)Sm.GetParameterDec("PropertyCount");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }
        
        private void SetGrd()
        {
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = (mPropertyCount*2)+6;
            Grd1.FrozenArea.ColCount = 5;

            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Header.Cells[0, 1].Value = "Brand";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Header.Cells[0, 2].Value = "Specification";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Header.Cells[0, 3].Value = "Item's Code";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Header.Cells[0, 4].Value = "Item's Name";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Grd1.Header.Cells[0, 5].Value = "Foreign Name";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 2;

            for (int i = 1; i <= mPropertyCount; i++)
            {
                SetPropertyInfo("Property " + i.ToString(), 4 + (i * 2));
                Grd1.Cols[4 + (i * 2)].Visible = false;
                Grd1.Cols[5 + (i * 2)].Visible = false;
            }
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        private void SetPropertyInfo(string Property, int c)
        {
            Grd1.Header.Cells[1, c].Value = Property;
            Grd1.Header.Cells[1, c].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, c].SpanCols = 2;
            Grd1.Header.Cells[0, c].Value = "Property Code";
            Grd1.Header.Cells[0, c].SpanRows = 1;
            Grd1.Header.Cells[0, c + 1].Value = "Property Name";
            Grd1.Header.Cells[0, c + 1].SpanRows = 1;
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            for (int i = 1; i <= mPropertyCount; i++)
            {
                Grd1.Cols[4 + (i * 2)].Visible = false;
                Grd1.Cols[5 + (i * 2)].Visible = false;
            }
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();
                var Filter = " ";
                var ItCode = string.Empty;
                var Row = -1;
                var Col = 1;
                bool IsExisted = false;

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName", "A.ForeignName" });

                SQL.AppendLine("Select A.ItCode, A.ItName, D.ItBrName, A.ForeignName, A.Specification, B.PropCode, C.PropName ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemProperty B On A.ItCode=B.ItCode ");
                SQL.AppendLine("Inner Join TblProperty C On B.PropCode=C.PropCode ");
                SQL.AppendLine("Left Join TblItemBrand D On A.ItBrCode=D.ItBrCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("Where Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine(Filter);
                SQL.AppendLine("Order By A.ItCode, B.PropCode; ");

                Grd1.BeginUpdate();

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                    {
                        //0
                        "ItBrName",

                        //1-5
                        "Specification",
                        "ItCode",
                        "ItName",
                        "ForeignName",
                        "PropCode", 

                        //6
                        "PropName"
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (Sm.CompareStr(ItCode, Sm.DrStr(dr, c[2])))
                            {
                                Col += 1;
                                if (Col <= mPropertyCount)
                                {
                                    Grd1.Cells[Row, 4 + (Col * 2)].Value = Sm.DrStr(dr, c[5]);
                                    Grd1.Cells[Row, 5 + (Col * 2)].Value = Sm.DrStr(dr, c[6]);
                                }
                            }
                            else
                            {
                                Row += 1;
                                Col = 1;
                                Grd1.Rows.Add();
                                Grd1.Cells[Row, 0].Value = Row + 1;
                                Grd1.Cells[Row, 1].Value = Sm.DrStr(dr, c[0]);
                                Grd1.Cells[Row, 2].Value = Sm.DrStr(dr, c[1]);
                                Grd1.Cells[Row, 3].Value = Sm.DrStr(dr, c[2]);
                                Grd1.Cells[Row, 4].Value = Sm.DrStr(dr, c[3]);
                                Grd1.Cells[Row, 5].Value = Sm.DrStr(dr, c[4]);
                                Grd1.Cells[Row, 6].Value = Sm.DrStr(dr, c[5]);
                                Grd1.Cells[Row, 7].Value = Sm.DrStr(dr, c[6]);
                            }
                            ItCode = Sm.DrStr(dr, c[2]);
                        }

                        for (int i = 1; i <= mPropertyCount; i++)
                        {
                            IsExisted = false;
                            for (int r = 0; r < Grd1.Rows.Count; r++)
                            {
                                if (Sm.GetGrdStr(Grd1, r, 4 + (i * 2)).Length > 0)
                                {
                                    IsExisted = true;
                                    break;
                                }
                            }
                            if (IsExisted)
                            {
                                Grd1.Cols[4 + (i * 2)].Visible = true;
                                Grd1.Cols[5 + (i * 2)].Visible = true;
                            }
                        }

                        Grd1.Cols.AutoWidth();
                    }
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Grd1.EndUpdate();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion
      
        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion
    }
}
