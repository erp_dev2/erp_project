﻿#region Update
/*
    31/01/2018 [WED] tambah field Asset Display Name
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvAsset : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "",
            mDocNo = string.Empty;
        internal FrmRecvAssetFind FrmFind;

        #endregion

        #region Constructor

        public FrmRecvAsset(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Receiving Asset";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                SetLueMtcStatusCode(ref LueMtcStatusCode);
                SetLueMtcTypeCode(ref LueMtcTypeCode);
                SetLueSymProblemCode(ref LueSymProblemCode);
                base.FrmLoad(sender, e);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Dno",

                        //1-5
                        "",
                        "Item Code",
                        "",
                        "Item Name",
                        "Foreign Name",
                        
                        //6-10
                        "Batch Number",
                        "Reason",
                        "Allocation",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        20, 130, 20, 250, 230, 
                        
                        //6-10
                        150, 250, 150, 250
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtTOCode, TxtAssetName, MeeRemark, ChkCancelInd, MeeCancelReason,
                        LueMtcStatusCode, LueMtcTypeCode, LueSymProblemCode, DteDownDt, TmeDown, TxtWORDocNo,
                        TxtDisplayName
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9 });
                    TxtDocNo.Focus();
                    BtnTOCode.Enabled = false;
                    BtnWORDocNo.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, LueMtcStatusCode, LueMtcTypeCode, LueSymProblemCode, DteDownDt, TmeDown
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3, 7, 8, 9 });
                    BtnTOCode.Enabled = true;
                    BtnTOCode2.Enabled = true;
                    BtnTOCode.Focus();
                    ChkCancelInd.Properties.ReadOnly = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, TxtTOCode, TxtAssetName, MeeRemark, 
                 LueMtcStatusCode, LueMtcTypeCode, LueSymProblemCode, DteDownDt, TmeDown, TxtWORDocNo,
                 TxtDisplayName
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRecvAssetFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            Sm.FormShowDialog(new FrmRecvAssetDlg(this));
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmRecvAssetDlg2(this));
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f1.ShowDialog();
            }

        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled) Sm.FormShowDialog(new FrmRecvAssetDlg2(this));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f1 = new FrmItem(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RecvAsset", "TblRecvAssetHdr");
            string WORDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "WOR", "TblWOR");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRecvAssetHdr(DocNo, WORDocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveRecvAssetDtl(DocNo, Row));

            cml.Add(SaveWOR(DocNo, WORDocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtTOCode, "Technical Object", false) ||
                Sm.IsLueEmpty(LueMtcStatusCode, "Maintenance status") ||
                Sm.IsLueEmpty(LueMtcTypeCode, "Maintenance type") ||
                Sm.IsDteEmpty(DteDownDt, "Down Date") ||
                Sm.IsTmeEmpty(TmeDown, "Down Time") ||
                Sm.IsLueEmpty(LueSymProblemCode, "Symptom problem") ||
                IsGrdEmpty()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }


        private MySqlCommand SaveRecvAssetHdr(string DocNo, string WORDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRecvAssetHdr(DocNo, DocDt, TOCode, WORDocNo, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @TOCode, @WORDocNo, @Remark, @CreateBy, CurrentDateTime()); "                     
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@TOCode", TxtTOCode.Text);
            Sm.CmParam<String>(ref cm, "@WORDocNo", WORDocNo);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveWOR(string DocNo, string WORDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWor(DocNo, DocDt, CancelInd, Status, WOStatus, MtcStatus, MtcType, SymProblem, ToCode, Description, DownDt, DownTm, CloseDt, RecvAssetDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@WORDocNo, @DocDt, 'N', 'O', 'O', @MtcStatus, @MtcType, @SymProblem, @ToCode, null, @DownDt, @DownTm, null, @RecvAssetDocNo, null, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @WORDocNo, '001', T.DNo, @CreateBy, CurrentDateTime()  ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='WORequest' ; ");

            SQL.AppendLine("Update TblWOR Set Status='A'  ");
            SQL.AppendLine("Where DocNo=@WORDocNo And Not Exists(  ");
            SQL.AppendLine("    Select DocNo From TblDocApproval  ");
            SQL.AppendLine("    Where DocType='WORequest' ");
            SQL.AppendLine("    And DocNo=@WORDocNo  ");
            SQL.AppendLine("    );  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WORDocNo", WORDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MtcStatus", Sm.GetLue(LueMtcStatusCode));
            Sm.CmParam<String>(ref cm, "@MtcType", Sm.GetLue(LueMtcTypeCode));
            Sm.CmParam<String>(ref cm, "@SymProblem", Sm.GetLue(LueSymProblemCode));
            Sm.CmParam<String>(ref cm, "@ToCode", TxtTOCode.Text);
            Sm.CmParam<String>(ref cm, "@RecvAssetDocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DownDt", Sm.GetDte(DteDownDt));
            Sm.CmParam<String>(ref cm, "@DownTm", Sm.GetTme(TmeDown));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }


        private MySqlCommand SaveRecvAssetDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRecvAssetDtl(DocNo, Dno, ItCode, BatchNo, Reason, Allocation, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNO, @Dno, @ItCode, @BatchNo, @Reason, @Allocation, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@Reason", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@Allocation", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            EditRecvAsset();
            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                IsCancelIndEditedAlready();
        }

        private bool IsCancelIndEditedAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblRecvAssetHdr Where DocNo=@DocNo And CancelInd='Y' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private void EditRecvAsset()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblRecvAssetHdr Set CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text); 
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowRecvAssetHdr(DocNo);
                ShowRecvAssetDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

       
        private void ShowRecvAssetHdr(string DocNo)
        {
           var SQL = new StringBuilder();

           SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.cancelInd,  A.TOCode, C.AssetName, A.WORDocNo,  ");
            SQL.AppendLine("D.MtcStatus, D.MtcType, D.SymProblem, D.DownDt, D.DownTm, A.Remark, C.DisplayName ");
            SQL.AppendLine("From TblRecvAssetHdr A   ");
            SQL.AppendLine("Inner Join TblTOHdr B On A.TOCode = B.AssetCode  "); 
            SQL.AppendLine("Inner Join TblAsset C On B.AssetCode = C.AssetCode   ");
            SQL.AppendLine("Inner JOin TblWOR D  On A.WORDocNo = D.DocNo  ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "TOCode", "AssetName",    

                        //6-10
                        "WORDocNo", "MtcStatus", "MtcType", "SymProblem", "DownDt",  

                        //11-13
                        "DownTm", "Remark", "DisplayName"
                      
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        TxtTOCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtAssetName.EditValue = Sm.DrStr(dr, c[5]);
                        TxtWORDocNo.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetLue(LueMtcStatusCode, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueMtcTypeCode, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LueSymProblemCode, Sm.DrStr(dr, c[9]));
                        Sm.SetDte(DteDownDt, Sm.DrStr(dr, c[10]));
                        Sm.SetTme(TmeDown, Sm.DrStr(dr, c[11]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                        TxtDisplayName.EditValue = Sm.DrStr(dr, c[13]);
                    }, true
                );
        }

        private void ShowRecvAssetDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.Dno, A.ItCode, B.Itname, A.batchNo, B.Foreignname, A.Reason, A.Allocation, A.Remark  ");
            SQL.AppendLine("from TblRecvAssetDtl A  ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ItCode", "ItName", "BatchNo", "ForeignName", "Reason", 
                    
                    //6-7
                    "Allocation", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private static void SetLueMtcStatusCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
               "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'MaintenanceStatus' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private static void SetLueMtcTypeCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'MaintenanceType' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private static void SetLueSymProblemCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'SymptomProblem' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        internal string GetSelectedDocument()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += ((SQL.Length != 0 ? ", " : "") + "'" + Sm.GetGrdStr(Grd1, Row, 1) + Sm.GetGrdStr(Grd1, Row, 2) + "'");

            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        #endregion

        #endregion

        #region Event

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }
        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void BtnTOCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmRecvAssetDlg(this));
        }

        private void BtnTOCode2_Click(object sender, EventArgs e)
        {
            if (TxtTOCode.Text.Length != 0)
            {
                var f1 = new FrmTO(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mAssetCode = TxtTOCode.Text;
                f1.ShowDialog();
            }
        }

        private void LueMtcStatusCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMtcStatusCode, new Sm.RefreshLue1(SetLueMtcStatusCode));
        }

        private void LueMtcTypeCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMtcTypeCode, new Sm.RefreshLue1(SetLueMtcTypeCode));
        }

        private void LueSymProblemCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSymProblemCode, new Sm.RefreshLue1(SetLueSymProblemCode));

        }

        private void BtnWORDocNo2_Click(object sender, EventArgs e)
        {
            if (TxtWORDocNo.Text.Length != 0)
            {
                var f1 = new FrmWOR(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtWORDocNo.Text;
                f1.ShowDialog();
            }
        }
       

        #endregion
    }
}
