﻿#region Update
/*
    11/09/2019 [WED] ubah alur ke SO COntract Revision
    25/09/2019 [WED/YK] ProjectImplementationDocNo ambil dari detail
    04/02/2023 [RDA/MNET] tambahan informasi kolom Tax Amount yang muncul ikut param SLITaxFormat
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoice5Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmSalesInvoice5 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmSalesInvoice5Find(FrmSalesInvoice5 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCtCode(ref LueCtCode);
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.CancelInd, B.CtName, ");
            SQL.AppendLine("A.CurCode, A.TotalAmt, A.TotalTax, A.Downpayment, A.Amt, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, A.TaxAmt1, A.TaxAmt2, A.TaxAmt3 ");
            SQL.AppendLine("FROM  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    SELECT Distinct T2.ProjectImplementationDocNo, T1.DocNo, T1.LocalDocNo, T1.DocDt, T1.CancelInd,  ");
	        SQL.AppendLine("    T1.CurCode, T1.CtCode, T1.TotalAmt, T1.TotalTax, T1.Downpayment, T1.Amt, ");
	        SQL.AppendLine("    T1.CreateBy, T1.CreateDt, T1.LastUpBy, T1.LastUpDt, T1.TaxAmt1, T1.TaxAmt2, T1.TaxAmt3 ");
	        SQL.AppendLine("    FROM TblSalesInvoice5Hdr T1 ");
	        SQL.AppendLine("    INNER JOIN TblSalesInvoice5Dtl T2 ON T1.DocNo = T2.DocNo ");
		    SQL.AppendLine("        AND (T1.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("Inner Join TblProjectImplementationHdr C On A.ProjectImplementationDocNo=C.DocNo ");
                SQL.AppendLine("Inner Join TblSOContractRevisionHdr C1 On C.SOContractDocNo = C1.DocNo ");
                SQL.AppendLine("Inner Join TblSOContractHdr D On C1.SOCDocNo=D.DocNo ");
                SQL.AppendLine("Inner Join TblBOQHdr E On D.BOQDocNo=E.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr F On E.LOPDocNo=F.DocNo ");
                SQL.AppendLine("And (F.SiteCode Is Null Or ( ");
                SQL.AppendLine("    F.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(F.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }

            SQL.AppendLine("Where 0 = 0 ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel", 
                        "Local"+Environment.NewLine+"Document",
                        "Customer",
                        
                        //6-10
                        "Currency", 
                        "Total Amount",
                        "Total Tax",
                        "Tax 1 Amount",
                        "Tax 2 Amount",

                         //11-15
                        "Tax 3 Amount",
                        "Downpayment",
                        "Invoice Amount",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",

                        //16-19
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 50, 130, 200, 
                        
                        //6-10
                        60, 120, 120, 120, 120,

                        //11-15
                        120, 120, 120, 100, 100, 
                        
                        //16-19
                        100, 100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 15, 18 });
            Sm.GrdFormatTime(Grd1, new int[] { 16, 19 });
            if (mFrmParent.mSLIPTaxFormat == "1")
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 14, 15, 16, 17, 18, 19 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked) Filter = " And A.CancelInd='N' ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "A.LocalDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "LocalDocNo", "CtName", "CurCode", 
                            
                            //6-10
                            "TotalAmt", "TotalTax",  "TaxAmt1", "TaxAmt2", "TaxAmt3",
                            
                            //11-15
                            "Downpayment", "Amt", "CreateBy", "CreateDt", "LastUpBy", 
                            
                            //16
                            "LastUpDt" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 16);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local document#");
        }

        #endregion

        #endregion
    }
}
