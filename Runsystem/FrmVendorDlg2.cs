﻿#region Update
/*
    06/06/2018 [WED] tambah kolom registrasi eproc
    02/07/2018 [WED] tambah kolom ID Number, website
    03/07/2018 [WED] tambah Head Office
    19/09/2018 [WED] tambah establishment year, position, contact number, bank code, bank name, branch name, bankacno, bankacname, other address (remark)
    12/07/2021 [WED/PADI] tambah category dan NIB
    07/09/2021 [WED/PADI] tambah proses data detail lainnya
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVendorDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmVendor mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVendorDlg2(FrmVendor FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Registered Company";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("select T1.*, T2.CityName, Concat(T3.VdName, ' [ ', T4.CityName, ' ]') As HeadOffice, T5.BankName, T6.VdCtName ");
            SQL.AppendLine("from tblvendorregister T1 ");
            SQL.AppendLine("left join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    select A.CityCode, Concat(A.CityName, ' ( ', IfNull(B.ProvName, ''), ' ', IfNull(C.CntName, ''), ' )') As CityName ");
	        SQL.AppendLine("    From TblCity A ");
	        SQL.AppendLine("    Left Join TblProvince B On A.ProvCode=B.ProvCode ");
	        SQL.AppendLine("    Left Join TblCountry C On B.CntCode=C.CntCode ");
            SQL.AppendLine(") T2 On T1.CityCode = T2.CityCode ");
            SQL.AppendLine("Left Join TblVendor T3 On T1.Parent = T3.VdCode ");
            SQL.AppendLine("Left Join TblCity T4 On T3.CityCode = T4.CityCode ");
            SQL.AppendLine("Left Join TblBank T5 On T1.BankCode = T5.BankCode ");
            SQL.AppendLine("Left Join TblVendorCategory T6 On T1.VdCtCode = T6.VdCtCode ");
            SQL.AppendLine("where T1.verifiedind = 'N' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Company Name",
                    "Company" + Environment.NewLine + "Short Name",
                    "Email",
                    "Contact Person",
                    "ID Number",

                    //6-10
                    "NPWP",
                    "Address",
                    "CityCode",
                    "City",
                    "Postal",
                    
                    //11-15
                    "Phone",
                    "Fax",
                    "Mobile",
                    "Website",
                    "HOCode",

                    //16-20
                    "Head Office",
                    "BankCode",
                    "BankName",
                    "BankBranch",
                    "BankAcNo",

                    //21-25
                    "BankAcName",
                    "Position",
                    "ContactNumber",
                    "Remark",
                    "Establishment",

                    //26-29
                    "NIB",
                    "Vendor Category"+Environment.NewLine+"Code",
                    "Vendor Category",
                    "Register ID"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    220, 180, 200, 200, 180, 

                    //6-10
                    100, 200, 0, 200, 100, 

                    //11-15
                    100, 100, 100, 130, 0, 
                    
                    //16-20
                    200, 0, 0, 0, 0,

                    //21-25
                    0, 0, 0, 0, 0,

                    //26-29
                    100, 0, 180, 0
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 29 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtVdRegister.Text, new string[] { "T1.VdName", "T1.ShortName", "T1.Email" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL +
                    Filter + " Order By VdName ;",
                    new string[] 
                    { 
                        //0
                        "VdName", 
                        //1-5
                        "ShortName", "Email", "ContactPersonName", "IDNumber", "TIN", 
                        //6-10
                        "Address", "CityCode", "CityName", "PostalCd", "Phone", 
                        //11-15
                        "Fax", "Mobile", "Website", "Parent", "HeadOffice",
                        //16-20
                        "BankCode", "BankName", "BankBranch", "BankAcNo", "BankAcName", 
                        //21-25
                        "Position", "ContactNumber", "Remark", "EstablishedYr", "NIB",
                        //26-28
                        "VdCtCode", "VdCtName", "RegisterID"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtVdName.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtShortName.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.TxtEmail.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                mFrmParent.mVdRegisterEmail = Sm.GetGrdStr(Grd1, Row, 3);
                mFrmParent.TxtTIN.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                mFrmParent.MeeAddress.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                Sm.SetLue(mFrmParent.LueCityCode, Sm.GetGrdStr(Grd1, Row, 8));
                mFrmParent.TxtPostalCd.EditValue = Sm.GetGrdStr(Grd1, Row, 10);
                mFrmParent.TxtPhone.EditValue = Sm.GetGrdStr(Grd1, Row, 11);
                mFrmParent.TxtFax.EditValue = Sm.GetGrdStr(Grd1, Row, 12);
                mFrmParent.TxtMobile.EditValue = Sm.GetGrdStr(Grd1, Row, 13);
                mFrmParent.TxtWebsite.EditValue = Sm.GetGrdStr(Grd1, Row, 14);
                Sm.SetLue(mFrmParent.LueParent, Sm.GetGrdStr(Grd1, Row, 15));
                mFrmParent.MeeRemark.EditValue = Sm.GetGrdStr(Grd1, Row, 24);
                mFrmParent.TxtEstablishedYr.EditValue = Sm.GetGrdStr(Grd1, Row, 25);
                mFrmParent.TxtNIB.EditValue = Sm.GetGrdStr(Grd1, Row, 26);
                Sm.SetLue(mFrmParent.LueVdCtCode, Sm.GetGrdStr(Grd1, Row, 27));
                
                mFrmParent.Grd1.Rows.Add();
                mFrmParent.Grd1.Cells[mFrmParent.Grd1.Rows.Count - 2, 0].Value = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.Grd1.Cells[mFrmParent.Grd1.Rows.Count - 2, 1].Value = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.Grd1.Cells[mFrmParent.Grd1.Rows.Count - 2, 2].Value = Sm.GetGrdStr(Grd1, Row, 22);
                mFrmParent.Grd1.Cells[mFrmParent.Grd1.Rows.Count - 2, 3].Value = Sm.GetGrdStr(Grd1, Row, 23);

                mFrmParent.Grd2.Rows.Add();
                mFrmParent.Grd2.Cells[mFrmParent.Grd1.Rows.Count - 2, 0].Value = Sm.GetGrdStr(Grd1, Row, 17);
                mFrmParent.Grd2.Cells[mFrmParent.Grd1.Rows.Count - 2, 1].Value = Sm.GetGrdStr(Grd1, Row, 18);
                mFrmParent.Grd2.Cells[mFrmParent.Grd1.Rows.Count - 2, 2].Value = Sm.GetGrdStr(Grd1, Row, 19);
                mFrmParent.Grd2.Cells[mFrmParent.Grd1.Rows.Count - 2, 3].Value = Sm.GetGrdStr(Grd1, Row, 21);
                mFrmParent.Grd2.Cells[mFrmParent.Grd1.Rows.Count - 2, 4].Value = Sm.GetGrdStr(Grd1, Row, 20);
                mFrmParent.ChkCreateUserVendor.Checked = true;

                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    mFrmParent.TxtVdName, mFrmParent.TxtShortName, mFrmParent.TxtEmail, mFrmParent.ChkCreateUserVendor,
                    mFrmParent.TxtTIN, mFrmParent.MeeAddress, mFrmParent.LueCityCode, mFrmParent.TxtPostalCd,
                    mFrmParent.TxtPhone, mFrmParent.TxtFax, mFrmParent.TxtMobile, mFrmParent.TxtWebsite, mFrmParent.LueParent,
                    mFrmParent.MeeRemark, mFrmParent.TxtEstablishedYr
                }, true);
                mFrmParent.Grd1.ReadOnly = true;
                mFrmParent.Grd2.ReadOnly = true;
                if (mFrmParent.mIsUseECatalog)
                {
                    mFrmParent.GetAdditionalRegisterInformation(Sm.GetGrdStr(Grd1, Row, 29));
                }
                this.Close();
            }
        }

        #endregion

        #region Grid Method


        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }


        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtVdRegister_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdRegister_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Company");
        }

        #endregion

        #endregion

    }
}
