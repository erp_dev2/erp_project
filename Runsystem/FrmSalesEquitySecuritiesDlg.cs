﻿#region update

/*
    25/04/2022 [SET/PRODUCT] Menu dialog baru
    18/05/2022 [SET/PRODUCT] Feedback merubah & tambah source Investment Code -> PortofolioId
                             List yang ditampilkan menyesuaikan dari "Equity Securities Portofolio Summary"
    19/05/2022 [SET/PRODUCT] merubah posisi tampilan Investment Name & Investment Code
    23/05/2022 [IBL/PRODUCT] Ubah group by utk pengambilan tanggal paling terahir. Berdasarkan InvestmentCode & InvestmentType
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesEquitySecuritiesDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmSalesEquitySecurities mFrmParent;
        string mSQL = string.Empty;
        private int mRow = 0;

        #endregion

        #region Constructor

        public FrmSalesEquitySecuritiesDlg(FrmSalesEquitySecurities FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT C.PortofolioId InvestmentCode, D.PortofolioName InvestmentName, F.OptCode, F.OptDesc AS Type, E.InvestmentCtName, A.BankAcCode, ");
            SQL.AppendLine("G.BankAcNm, C.UomCode, D.CurCode, E.InvestmentCtCode, H.CreateDt, A.Source, A.InvestmentCode InvestmentEquityCode ");
            SQL.AppendLine("FROM tblinvestmentstocksummary A ");
            SQL.AppendLine("INNER JOIN tblinvestmentstockmovement B ON A.Source = B.Source AND A.BankAcCode = B.BankAcCode ");
            SQL.AppendLine("INNER JOIN tblinvestmentitemequity C ON B.InvestmentCode = C.InvestmentEquityCode ");
            SQL.AppendLine("INNER JOIN tblinvestmentportofolio D ON C.PortofolioId = D.PortofolioId ");
            SQL.AppendLine("INNER JOIN tblinvestmentcategory E ON D.InvestmentCtCode = E.InvestmentCtCode ");
            SQL.AppendLine("INNER JOIN tbloption F ON B.InvestmentType = F.OptCode AND F.OptCat = 'InvestmentType' ");
            SQL.AppendLine("INNER JOIN tblBankAccount G ON A.BankAcCode = G.BankAcCode ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("    SELECT A.InvestmentCode, B.InvestmentType, MAX(A.CreateDt) CreateDt ");
            SQL.AppendLine("    FROM tblinvestmentstocksummary A ");
            SQL.AppendLine("    INNER JOIN tblinvestmentstockmovement B ON A.Source = B.Source AND A.BankAcCode = B.BankAcCode ");
            SQL.AppendLine("    INNER JOIN tbloption C ON B.InvestmentType = C.OptCode ");
            SQL.AppendLine("    WHERE B.DocDt <= @Dt ");
            SQL.AppendLine("    GROUP BY A.InvestmentCode, C.OptCode, A.BankAcCode ");
            SQL.AppendLine(") H ON A.InvestmentCode = H.InvestmentCode AND F.OptCode = H.InvestmentType ");
            SQL.AppendLine("WHERE B.CancelInd = 'N' AND A.Qty > 0  ");
            //SQL.AppendLine("GROUP BY A.InvestmentCode, A.BankAcCode, F.OptCode ");


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.ReadOnly = true;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Investment Code",
                        "Investment Name",
                        "OptCode",
                        "Type",
                        "Category",

                        //6-10
                        "Bank Account#",
                        "Bank Name",
                        "UoM",
                        "CurCode",
                        "InvestmentCtCode",

                        //11-13
                        "CreateDt",
                        "Source",
                        "InvestmentEquityCode"
                    },
                     new int[]
                    {
                        //0
                        50,

                        //1-5
                        250, 120, 50, 100, 100,

                        //6-10
                        120, 150, 100, 100, 120,

                        //11-13
                        100, 150, 120
                    }
                );
            //Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 11 });
            //Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 8, 9, 10, 11, 12, 13 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            //if (
            //    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
            //    Sm.IsDteEmpty(DteDocDt2, "End date") ||
            //    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
            //    ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtInvestment.Text, new string[] { "PortofolioName", "C.PortofolioId" });
                Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(mFrmParent.DteDocDt2));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " GROUP BY A.InvestmentCode, A.BankAcCode, F.OptCode Order By A.CreateDt;",
                        new string[]
                        {
                            //0
                            "InvestmentCode", 

                            //1-5
                            "InvestmentName", "OptCode", "Type", "InvestmentCtName", "BankAcCode", 
                            
                            //6-10
                            "BankAcNm", "UoMCode", "CurCode", "InvestmentCtCode", "CreateDt", 
                            
                            //11
                            "Source", "InvestmentEquityCode"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.Grd1.Cells[mRow, 1].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.Grd1.Cells[mRow, 2].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.Grd1.Cells[mRow, 3].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                mFrmParent.Grd1.Cells[mRow, 4].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                mFrmParent.Grd1.Cells[mRow, 5].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                mFrmParent.Grd1.Cells[mRow, 6].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                mFrmParent.Grd1.Cells[mRow, 7].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                mFrmParent.Grd1.Cells[mRow, 9].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
                mFrmParent.Grd1.Cells[mRow, 18].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 10);
                Sm.CopyGrdValue(mFrmParent.Grd1, mRow, 21, Grd1, Grd1.CurRow.Index, 11);
                Sm.CopyGrdValue(mFrmParent.Grd1, mRow, 22, Grd1, Grd1.CurRow.Index, 12);
                Sm.CopyGrdValue(mFrmParent.Grd1, mRow, 23, Grd1, Grd1.CurRow.Index, 13);
                mFrmParent.TxtCurCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9);

                mFrmParent.ComputeStock();
                this.Close();
            }
        }

        #endregion

        #region Event

        private void ChkInvestment_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment");
        }

        private void TxtInvestment_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

    }
}
