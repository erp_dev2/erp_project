﻿namespace RunSystem
{
    partial class FrmRptProjectMonthlyCostDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.TxtMth = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtYr = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtCCtName = new DevExpress.XtraEditors.TextEdit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCtName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 78);
            this.panel3.Size = new System.Drawing.Size(710, 315);
            // 
            // Grd1
            // 
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.ReadOnly = true;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(710, 315);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtMth);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtYr);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtCCtName);
            this.panel2.Size = new System.Drawing.Size(710, 78);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(50, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 14);
            this.label2.TabIndex = 15;
            this.label2.Text = "Month";
            // 
            // TxtMth
            // 
            this.TxtMth.EnterMoveNextControl = true;
            this.TxtMth.Location = new System.Drawing.Point(97, 51);
            this.TxtMth.Name = "TxtMth";
            this.TxtMth.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMth.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMth.Properties.Appearance.Options.UseFont = true;
            this.TxtMth.Properties.MaxLength = 30;
            this.TxtMth.Properties.ReadOnly = true;
            this.TxtMth.Size = new System.Drawing.Size(67, 20);
            this.TxtMth.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(60, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "Year";
            // 
            // TxtYr
            // 
            this.TxtYr.EnterMoveNextControl = true;
            this.TxtYr.Location = new System.Drawing.Point(97, 29);
            this.TxtYr.Name = "TxtYr";
            this.TxtYr.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtYr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtYr.Properties.Appearance.Options.UseFont = true;
            this.TxtYr.Properties.MaxLength = 30;
            this.TxtYr.Properties.ReadOnly = true;
            this.TxtYr.Size = new System.Drawing.Size(67, 20);
            this.TxtYr.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 14);
            this.label6.TabIndex = 11;
            this.label6.Text = "Cost Category";
            // 
            // TxtCCtName
            // 
            this.TxtCCtName.EnterMoveNextControl = true;
            this.TxtCCtName.Location = new System.Drawing.Point(97, 7);
            this.TxtCCtName.Name = "TxtCCtName";
            this.TxtCCtName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCCtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCtName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCCtName.Properties.Appearance.Options.UseFont = true;
            this.TxtCCtName.Properties.MaxLength = 30;
            this.TxtCCtName.Properties.ReadOnly = true;
            this.TxtCCtName.Size = new System.Drawing.Size(323, 20);
            this.TxtCCtName.TabIndex = 12;
            // 
            // FrmRptProjectMonthlyCostDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 393);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmRptProjectMonthlyCostDlg";
            this.Text = "Cost Category Information";
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCtName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.TextEdit TxtMth;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.TextEdit TxtYr;
        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.TextEdit TxtCCtName;
    }
}