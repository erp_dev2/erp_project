﻿#region Update
/*
    13/03/2020 [HAR/TWC] Tambah Informasi Total
    05/04/2020 [IBL/VIR] : Tambah informasi Budget Category Code internal
    11/05/2020 [WED/SIER] budget tahunan berdasarkan parameter IsBudget2YearlyFormat
    05/02/2021 [WED/SIER] tambah indikator cancel berdasarkan parameter IsBudget2Cancellable
    10/11/2021 [ARI/AMKA] menambah filter departemen pada lue dan detail sesuai group dengan paramter IsBudgetMaintenanceDeptFilteredByGroup
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmBudget2Find : RunSystem.FrmBase2
    {
        private FrmBudget2 mFrmParent;
        string mSQL = string.Empty;

        #region Constructor

        public FrmBudget2Find(FrmBudget2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsBudgetMaintenanceDeptFilteredByGroup ? "Y" : "N");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.BudgetRequestDocNo, C.Yr, C.Mth, E.DeptName, ");
            SQL.AppendLine("F.BCName, F.LocalCode, D.Amt As BudgetRequestAmt, B.Amt, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, ");
            if (mFrmParent.mIsBudget2Cancellable)
                SQL.AppendLine("A.CancelInd ");
            else
                SQL.AppendLine("'N' As CancelInd ");
            SQL.AppendLine("From TblBudgetHdr A ");
            SQL.AppendLine("Inner Join TblBudgetDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblBudgetRequestHdr C On A.BudgetRequestDocNo=C.DocNo ");
            if (mFrmParent.mIsBudgetMaintenanceDeptFilteredByGroup)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=C.DeptCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblBudgetRequestDtl D On B.BudgetRequestDocNo=D.DocNo And B.BudgetRequestDNo=D.DNo ");
            SQL.AppendLine("Right Join TblDepartment E On C.DeptCode=E.DeptCode ");

            SQL.AppendLine("Left Join TblBudgetCategory F On D.BCCode=F.BCCode ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (mFrmParent.mIsBudgetMaintenanceDeptFilteredByGroup)
            {
                SQL.AppendLine("And Exists ( ");
                SQL.AppendLine("    Select 1 From tblgroupdepartment ");
                SQL.AppendLine("    Where DeptCode=E.DeptCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where ");
                SQL.AppendLine("    UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "Date",
                    "Cancel",
                    "Budget"+Environment.NewLine+"Request#",
                    "Year",

                    //6-10
                    "Month",
                    "Department", 
                    "Category",
                    "Local"+Environment.NewLine+"Code",
                    "Requested"+Environment.NewLine+"Amount",
                    
                    //11-15
                    "Budget"+Environment.NewLine+"Amount",
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By", 

                    //16-17
                    "Last"+Environment.NewLine+"Updated Date",
                    "Last"+Environment.NewLine+"Updated Time",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 60, 150, 80, 
                    
                    //6-10
                    80, 180, 180, 100, 130, 
                    
                    //11-15
                    130, 100, 100, 100, 100, 
                    
                    //16-17
                    100, 100
                }
            );

            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 13, 16 });
            Sm.GrdFormatTime(Grd1, new int[] { 14, 17 });
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14, 15, 16, 17 }, false);
            if (mFrmParent.mIsBudget2YearlyFormat)
                Sm.GrdColInvisible(Grd1, new int[] { 6 });

            if(!mFrmParent.mIsBudget2Cancellable)
                Sm.GrdColInvisible(Grd1, new int[] { 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14, 15, 16, 17 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBudgetRequestDocNo.Text, "A.BudgetRequestDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalCode.Text, "F.LocalCode", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL.ToString() + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",
 
                            //1-5
                            "DocDt", "CancelInd", "BudgetRequestDocNo", "Yr", "Mth", 
                            
                            //6-10
                            "DeptName", "BCName", "LocalCode", "BudgetRequestAmt", "Amt", 
                            
                            //11-14
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt", 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 17, 14);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Budget#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtBudgetRequestDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBudgetRequestDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Budget request#");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsBudgetMaintenanceDeptFilteredByGroup ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtLocalCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local Code");
        }

        #endregion

        #endregion
    }
}
