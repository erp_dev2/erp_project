﻿#region Update
/*
    18/08/2020 [DITA/MGI] new reporting
    24/08/2020 [WED/MGI] DR Ind dipindah ke detail
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptCustomerPickupItem : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptCustomerPickupItem(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCtCode(ref LueCtCode);
                SetLueCPIStatus(ref LueCPIStatus);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT T.* ");
            SQL.AppendLine("FROM( ");
            SQL.AppendLine("    SELECT A.DocNo, A.DocDt, C.CtCode, C.CtName, D.ItCode, D.ItName, B.BatchNo, B.CPIStatus CPIStatusCode,  ");
            SQL.AppendLine("    Case When B.CPIStatus = 'O' Then 'Outstanding' When B.CPIStatus = 'P' Then 'Partial' Else 'Fullfiled' End As CPIStatus,  ");
            SQL.AppendLine("    IFNULL(B.Qty, 0.00) DOQty, IFNULL(E.Qty, 0.00) CPIQty, IFNull(B.Qty-E.Qty, B.Qty) Balance   ");
            SQL.AppendLine("    FROM TblDOCtHdr A ");
            SQL.AppendLine("    INNER JOIN TblDOCtDtl B ON A.DocNo = B.DocNo AND B.CancelInd = 'N' AND A.DocDt BETWEEN @DocDt1 AND @DocDt2 ");
            SQL.AppendLine("    INNER JOIN TblCustomer C ON A.CtCode = C.CtCode ");
            SQL.AppendLine("    INNER JOIN TblItem D ON B.ItCode = D.ItCode ");
            SQL.AppendLine("    LEFT JOIN  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("    	SELECT X2.DOCtDocNo, X2.DOCtDNo, ifnull(SUM(X2.Qty), 0.00) Qty ");
            SQL.AppendLine("    	From TblCustomerPickupItemHdr X1 ");
            SQL.AppendLine("    	INNER JOIN TblCustomerPickupItemDtl X2 ON X1.DocNo = X2.DocNo AND X2.CancelInd = 'N' ");
            SQL.AppendLine("        AND X2.DRInd = 'N' ");
            SQL.AppendLine("        Group BY X2.DOCtDocNo, X2.DOCtDNo ");
            SQL.AppendLine("    )E ON A.DocNo = E.DOCtDocNo AND B.DNo = E.DOCtDNo ");

            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT A.DocNo, A.DocDt, C.CtCode, C.CtName, D.ItCode, D.ItName, B.BatchNo, B.CPIStatus CPIStatusCode,  ");
            SQL.AppendLine("    Case When B.CPIStatus = 'O' Then 'Outstanding' When B.CPIStatus = 'P' Then 'Partial' Else 'Fullfiled' End As CPIStatus,  ");
            SQL.AppendLine("    IFNULL(B.Qty, 0.00) DOQty, IFNULL(E.Qty, 0.00) CPIQty, IFNull(B.Qty-E.Qty, B.Qty) Balance   ");
            SQL.AppendLine("    FROM TblDOCt2Hdr A ");
            SQL.AppendLine("    INNER JOIN TblDOCt2Dtl B ON A.DocNo = B.DocNo AND B.CancelInd = 'N' AND A.DocDt BETWEEN @DocDt1 AND @DocDt2 ");
            SQL.AppendLine("    INNER JOIN TblCustomer C ON A.CtCode = C.CtCode ");
            SQL.AppendLine("    INNER JOIN TblItem D ON B.ItCode = D.ItCode ");
            SQL.AppendLine("    LEFT JOIN  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("    	SELECT X2.DOCtDocNo, X2.DOCtDNo, ifnull(SUM(X2.Qty), 0.00) Qty ");
            SQL.AppendLine("    	From TblCustomerPickupItemHdr X1 ");
            SQL.AppendLine("    	INNER JOIN TblCustomerPickupItemDtl X2 ON X1.DocNo = X2.DocNo AND X2.CancelInd = 'N' ");
            SQL.AppendLine("        AND X2.DRInd = 'Y' ");
            SQL.AppendLine("        Group BY X2.DOCtDocNo, X2.DOCtDNo ");
            SQL.AppendLine("    )E ON A.DocNo = E.DOCtDocNo AND B.DNo = E.DOCtDNo ");
            SQL.AppendLine(")T ");
            SQL.AppendLine("Where T.DocDt BETWEEN @DocDt1 AND @DocDt2 ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5   
                        "Customer"+Environment.NewLine+"Code",
                        "Customer",
                        "DO#",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's"+Environment.NewLine+"Name",

                        //6-10
                        "Batch#",
                        "Status",
                        "DO Quantity",
                        "Pickup Quantity",
                        "Balance"
                    },
                    new int[]
                {
                    50, 
                    80, 250, 150, 80, 200,
                    100, 100, 100, 100, 100
                }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = " And 0=0 ";


                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "T.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCPIStatus), "T.CPIStatusCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL() + Filter + " Order By T.CtName, T.DocNo; ",
                        new string[]
                        {
                            //0
                            "CtCode",

                            //1-5
                            "CtName", "DocNo", "ItCode", "ItName", "BatchNo", 
                            
                            //6-9
                            "CPIStatus", "DOQty", "CPIQty", "Balance"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 9, 10 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void SetLueCPIStatus(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocType As Col1, T.Status As Col2 From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 'O' As Doctype, 'Outstanding' As Status ");
            SQL.AppendLine("    Union ALL ");
            SQL.AppendLine("    Select 'P' As Doctype, 'Partial' As Status ");
            SQL.AppendLine("    Union ALL ");
            SQL.AppendLine("    Select 'F' As Doctype, 'Fullfiled' As Status ");
            SQL.AppendLine(")T ");


            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #region Grid method

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkCPIStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueCPIStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCPIStatus, new Sm.RefreshLue1(SetLueCPIStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion
    }
}
