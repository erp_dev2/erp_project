﻿#region Update
/*
    03/01/2018 [HAR] Hapus informasi competence
    15/08/2018 [WED] tambah MinScore dari Training
 *  19/05/2020 [HAR/HIN] tambah validasi filter site hr
 * 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTrainingEvaluationDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmTrainingEvaluation mFrmParent;
        internal string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmTrainingEvaluationDlg(FrmTrainingEvaluation FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            this.Text = "List of Training";
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

            SetGrd();
            SetSQL();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-4
                    "Training"+Environment.NewLine+"Code", 
                    "Training"+Environment.NewLine+"Name",
                    "Training Type",
                    "Min. Score",
                    "Site"
                },
                new int[] 
                {
                    //0
                    20,

                    //1-4
                    100, 200, 120, 100, 120
                }
            );
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.TrainingCode, A.TrainingName, ");
            SQL.AppendLine("Case A.TrainingType When 'I' Then 'Internal' When 'E' Then 'External' End As TrainingType, A.MinScore, B.SiteName ");
            SQL.AppendLine("From TblTraining A ");
            SQL.AppendLine("left Join TblSite B On A.SiteCode = B.SiteCode ");
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        )) ");
            }
            
           
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtTrainingName.Text, new string[] { "A.TrainingCode", "A.TrainingName" });
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.TrainingName",
                        new string[] 
                        { 
                            "TrainingCode", 
                            "TrainingName", "TrainingType", "MinScore", "Sitename"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);

                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, ChkHideInfoInGrd.Checked ? 2 : 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtTrainingCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtTrainingName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.ShowTrainingSourceData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                mFrmParent.TxtMinScore.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 4), 0);
                mFrmParent.mMinScore = Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 4);
                this.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void ChkTrainingName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Training");
        }

        private void TxtTrainingName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion
    }
}
