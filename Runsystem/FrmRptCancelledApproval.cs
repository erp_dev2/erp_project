﻿#region Update
/*
    13/11/2020 [IBL/IMS] New Application
    03/03/2021 [IBL/IMS] Tambah cancelled approval : NTP
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptCancelledApproval : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private string
            mNumberOfDecimalPointForPrice = string.Empty,
            mMainCurCode = string.Empty;
        private bool
            mIsApprovalBySiteMandatory = false,
            mIsPORequestApprovalShowMonthlyDeptAmt = false,
            mIsUseItemConsumption = false,
            mIsRecvVdApprovalBasedOnWhs = false;

        #endregion

        #region Constructor

        public FrmRptCancelledApproval(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method
        
        #region Standard Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                BtnRefresh.Visible = BtnPrint.Visible = true;
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                SetLueDocType(ref LueDocType);
                GetParameter();
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select * From ( ");

            #region Drawing Approval
            SQL.AppendLine("Select CancelInd, ApproverRemark, DocType, DocNo, DNo, DocDt, Concat( ");
            SQL.AppendLine("'Remark     : ', Remark ) As Remarks ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.CancelInd, B.Remark ApproverRemark, 'Drawing Approval' AS DocType, A.DocNo, '001' AS DNo, A.DocDt, ");
            SQL.AppendLine("    IfNull(A.Remark, '') As Remark ");
            SQL.AppendLine("    From TblDrawingApprovalHdr A ");
            SQL.AppendLine("    Inner Join TblDocApproval B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        And B.DocType = 'DrawingApproval' ");
            SQL.AppendLine("        And B.Status = 'C' ");
            SQL.AppendLine("    Where A.Status = 'C' ");
            SQL.AppendLine("    And Exists ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.DocNo ");
            SQL.AppendLine("        From TblDocApproval A ");
            SQL.AppendLine("        Inner Join TblDocApprovalSetting B  On A.DocType=B.DocType And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblDrawingApprovalHdr C On A.DocNo = C.DocNo ");
            SQL.AppendLine("        Where A.DocType = 'DrawingApproval' And A.UserCode Is Not Null And A.Status = 'C' ");
            SQL.AppendLine("        And C.CreateBy = @UserCode ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And A.CreateBy = @UserCode ");
            SQL.AppendLine(") A Union All ");
            #endregion

            #region DO To Otehr Whs
            SQL.AppendLine("Select CancelInd, ApproverRemark, DocType, DocNo, DNo, DocDt, Concat( ");
            SQL.AppendLine("'Warehouse From : ', WhsFrom, '\n', ");
            SQL.AppendLine("'Warehouse To   : ', WhsTo, '\n', ");
            SQL.AppendLine("'Remark         : ', Remark ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T1.CancelInd, 'DO To Other Warehouse' As DocType, T1.DocNo, '001' As DNo, T1.DocDt, ");
            SQL.AppendLine("    T2.WhsName As WhsFrom, T3.WhsName As WhsTo, IfNull(T1.Remark, '') As Remark, T4.Remark As ApproverRemark  ");
            SQL.AppendLine("    From TblDOWhsHdr T1  ");
            SQL.AppendLine("    Inner Join TblWarehouse T2 On T1.WhsCode=T2.WhsCode  ");
            SQL.AppendLine("    Left Join TblWarehouse T3 On T1.WhsCode2=T3.WhsCode  ");
            SQL.AppendLine("    Inner Join TblDocApproval T4 On T1.DocNo = T4.DocNo And T4.DocType = 'DOWhs' ");
            SQL.AppendLine("    Where T1.TransferRequestWhsDocNo Is Null ");
            SQL.AppendLine("    And IfNull(T1.Status, 'C')='C' ");
            SQL.AppendLine("    And Exists ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.DocNo ");
            SQL.AppendLine("        From TblDocApproval A ");
            SQL.AppendLine("        Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblDOWhsDtl C On A.DocNo = C.DocNo And A.DNo = C.DNo ");
            SQL.AppendLine("        Where A.DocType = 'DOWhs' And A.UserCode Is Not Null And A.Status = 'C' ");
            SQL.AppendLine("        And C.CreateBy = @UserCode ");
            SQL.AppendLine("    )");
            SQL.AppendLine("    And T1.CreateBy = @UserCode ");
            SQL.AppendLine(") A Union All ");
            #endregion

            #region Receiving Item From Vendor Without PO
            SQL.AppendLine("Select CancelInd, ApproverRemark, DocType, DocNo, DNo, DocDt, Concat(");
            SQL.AppendLine("    'Warehouse : ', WhsName, '\n', ");
            SQL.AppendLine("    'Vendor    : ', VdName, '\n', ");
            SQL.AppendLine("    'Item Code : ', ItCode, '\n', ");
            SQL.AppendLine("    'Item Name : ', ItName, '\n', ");
            SQL.AppendLine("    'Quantity  : ', Convert(Format(QtyPurchase, 2) Using utf8), ' ', PurchaseUomCode, '\n', ");
            SQL.AppendLine("    'Price     : ', CurCode, ' ', Convert(Format(UPrice, 2) Using utf8), '\n', ");
            SQL.AppendLine("    'Amount    : ', Convert(Format(Amt, 2) Using utf8), '\n', ");
            SQL.AppendLine("    'Remark    : ', Remark");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select T2.CancelInd, 'Receiving Item From Vendor Without PO' As DocType, T1.DocNo, T2.DNo, T1.DocDt, ");
            SQL.AppendLine("    T3.WhsName, T4.VdName, T2.ItCode, T5.ItName, T2.QtyPurchase, T5.PurchaseUomCode, T1.CurCode, T2.UPrice, ");
            SQL.AppendLine("    ((((100 - T2.Discount) / 100) * (T2.QtyPurchase * T2.UPrice)) + T2.RoundingValue) As Amt, ");
            SQL.AppendLine("    IfNull(T2.Remark, '') As Remark, T6.Remark As ApproverRemark ");
            SQL.AppendLine("    From TblRecvVdHdr T1 ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T2 On T1.DocNo=T2.DocNo And IfNull(T2.Status, '') Not In ('A', 'O') ");
            SQL.AppendLine("    Inner Join TblWarehouse T3 On T1.WhsCode=T3.WhsCode ");
            SQL.AppendLine("    Inner Join TblVendor T4 On T1.VdCode=T4.VdCode ");
            SQL.AppendLine("    Inner Join TblItem T5 On T2.ItCode=T5.ItCode ");
            SQL.AppendLine("    Inner Join TblDocApproval T6 On T2.DocNo = T6.DocNo And T2.DNo = T6.ApprovalDNo And T6.DocType = 'RecvVd2' ");
            SQL.AppendLine("    Where Exists ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.DocNo ");
            SQL.AppendLine("        From TblDocApproval A ");
            SQL.AppendLine("        Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl C On A.DocNo = C.DocNo And A.DNo = C.DNo ");
            SQL.AppendLine("        Where A.DocType = 'RecvVd2' And A.UserCode Is Not Null And A.Status = 'C' ");
            SQL.AppendLine("        And C.CreateBy = @UserCode ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And T1.CreateBy = @UserCode ");
            SQL.AppendLine(") A Union All ");
            #endregion

            #region PO Revision
            SQL.AppendLine("Select CancelInd, ApproverRemark, DocType, DocNo, DNo, DocDt, Concat(");
            SQL.AppendLine("    'Vendor          : ', VdName, '\n', ");
            SQL.AppendLine("    'Item Code       : ', ItCode, '\n', ");
            SQL.AppendLine("    'Item Name       : ', ItName, '\n', ");
            SQL.AppendLine("    'Quantity        : ', Convert(Format(Qty, 2) Using utf8), ' ', PurchaseUomCode, '\n', ");
            SQL.AppendLine("    'Unit Price      : ', CurCodeOld, ' ', Convert(Format(UPriceOld, 2) Using utf8), ' (Previous) ', CurCodeNew, ' ', Convert(Format(UPriceNew, 2) Using utf8), ' (New) ', '\n', ");
            SQL.AppendLine("    'Discount %      : ', CurCodeOld, ' ', Convert(Format(DiscountOld, 2) Using utf8), ' (Previous) ', CurCodeNew, ' ', Convert(Format(DiscountNew, 2) Using utf8), ' (New) ', '\n', ");
            SQL.AppendLine("    'Discount Amount : ', CurCodeOld, ' ', Convert(Format(DiscountAmtOld, 2) Using utf8), ' (Previous) ', CurCodeNew, ' ', Convert(Format(DiscountAmtNew, 2) Using utf8), ' (New) ', '\n', ");
            SQL.AppendLine("    'Rounding Value  : ', CurCodeOld, ' ', Convert(Format(RoundingValueOld, 2) Using utf8), ' (Previous) ', CurCodeNew, ' ', Convert(Format(RoundingValueNew, 2) Using utf8), ' (New) ', '\n', ");
            SQL.AppendLine("    'Remark Header   : ', PORemarkHdrOld, ' ', ' (Previous) ', PORemarkHdr, ' ', ' (New) ', '\n', ");
            SQL.AppendLine("    'Remark Detail   : ', PORemarkDtlOld, ' ', ' (Previous) ', PORemarkDtl, ' ', ' (New) ', '\n', ");
            SQL.AppendLine("    'Remark          : ', Remark");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select T1.CancelInd, T12.Remark ApproverRemark, 'PO Revision' As DocType, T1.DocNo, '001' As DNo, T1.DocDt, ");
            SQL.AppendLine("    T4.VdName, T6.ItCode, T7.ItName, T3.Qty, T7.PurchaseUomCode, ");
            SQL.AppendLine("    T8.CurCode As CurCodeOld, T9.UPrice As UPriceOld, ");
            SQL.AppendLine("    T10.CurCode As CurCodeNew, T11.UPrice As UPriceNew, ");
            SQL.AppendLine("    T1.DiscountOld, T1.Discount As DiscountNew, ");
            SQL.AppendLine("    T1.DiscountAmtOld, T1.DiscountAmt As DiscountAmtNew, ");
            SQL.AppendLine("    T1.RoundingValueOld, T1.RoundingValue As RoundingValueNew, ");
            SQL.AppendLine("    ifnull(T1.PORemarkHdrOld, '-') As PORemarkHdrOld, ifnull(T1.PORemarkHdr, '-') As PORemarkHdr,  ");
            SQL.AppendLine("    ifnull(T1.PORemarkDtlOld, '-') As PORemarkDtlOld, ifnull(T1.PORemarkDtl, '-') As PORemarkDtl, ");
            SQL.AppendLine("    IfNull(T1.Remark, '') As Remark ");
            SQL.AppendLine("    From TblPORevision T1 ");
            SQL.AppendLine("    Inner Join TblPOHdr T2 On T1.PODocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPODtl T3 On T1.PODocNo=T3.DocNo And T1.PODNo=T3.DNo ");
            SQL.AppendLine("    Inner Join TblVendor T4 On T2.VdCode=T4.VdCode ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T5 On T3.PORequestDocNo=T5.DocNo And T3.PORequestDNo=T5.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T6 On T5.MaterialRequestDocNo=T6.DocNo And T5.MaterialRequestDNo=T6.DNo ");
            SQL.AppendLine("    Inner Join TblItem T7 On T6.ItCode=T7.ItCode ");
            SQL.AppendLine("    Inner Join TblQtHdr T8 On T1.QtDocNoOld=T8.DocNo ");
            SQL.AppendLine("    Inner Join TblQtDtl T9 On T1.QtDocNoOld=T9.DocNo And T1.QtDNoOld=T9.DNo ");
            SQL.AppendLine("    Inner Join TblQtHdr T10 On T1.QtDocNo=T10.DocNo ");
            SQL.AppendLine("    Inner Join TblQtDtl T11 On T1.QtDocNo=T11.DocNo And T1.QtDNo=T11.DNo ");
            SQL.AppendLine("    Inner Join TblDocApproval T12 On T1.DocNo = T12.DocNo ");
            SQL.AppendLine("        And T12.DocType = 'PORequest' ");
            SQL.AppendLine("        And T12.Status = 'C' ");
            SQL.AppendLine("    Where IfNull(T1.Status, '')='C' ");
            SQL.AppendLine("    And Exists ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.DocNo ");
            SQL.AppendLine("        From TblDocApproval A ");
            SQL.AppendLine("        Inner Join TblDocApprovalSetting B  On A.DocType=B.DocType And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblPORevision C On A.DocNo = C.DocNo ");
            SQL.AppendLine("        Where A.DocType = 'PORevision' And A.UserCode Is Not Null And A.Status = 'C' ");
            SQL.AppendLine("        And C.CreateBy = @UserCode ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And T1.CreateBy = @UserCode ");
            SQL.AppendLine(") A Union All ");
            #endregion

            #region PO
            SQL.AppendLine("Select CancelInd, ApproverRemark, DocType, DocNo, DNo, DocDt, Concat( ");
            SQL.AppendLine("    'Vendor   : ', VdName, '\n', ");
            SQL.AppendLine("    'Currency : ', CurCode, '\n', ");
            SQL.AppendLine("    'Tax      : ', Convert(Format(TaxAmt, 2) Using utf8), '\n', ");
            SQL.AppendLine("    'Amount   : ', Convert(Format(Amt, 2) Using utf8), '\n', ");
            SQL.AppendLine("    'Remark   : ', Remark ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From ( ");
	        SQL.AppendLine("    Select T2.CancelInd, T4.Remark ApproverRemark, 'PO' As DocType, T1.DocNo, '001' As DNo, T1.DocDt, ");
	        SQL.AppendLine("    T3.VdName, T1.CurCode, T1.TaxAmt, T1.Amt, IfNull(T1.Remark, '') As Remark ");
            SQL.AppendLine("    From TblPOHdr T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Inner Join TblVendor T3 On T1.VdCode=T3.VdCode ");
            SQL.AppendLine("    Inner Join TblDocApproval T4 On T1.DocNo = T4.DocNo ");
    	    SQL.AppendLine("        And T4.DocType = 'PO' ");
    	    SQL.AppendLine("        And T4.Status = 'C' ");
            SQL.AppendLine("    Where IfNull(T1.Status, '')='C' ");
            SQL.AppendLine("    And Exists(Select 1 From TblPODtl Where DocNo=T1.DocNo) ");
            SQL.AppendLine("    And Exists ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.DocNo ");
            SQL.AppendLine("        From TblDocApproval A ");
            SQL.AppendLine("        Inner Join TblDocApprovalSetting B  On A.DocType=B.DocType And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblPOHdr C On A.DocNo = C.DocNo ");
            SQL.AppendLine("        Where A.DocType = 'PO' And A.UserCode Is Not Null And A.Status = 'C' ");
            SQL.AppendLine("        And C.CreateBy = @UserCode ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And T1.CreateBy = @UserCode ");
            SQL.AppendLine(") A Union All ");
            #endregion

            #region Transfer Request For Project
            SQL.AppendLine("Select CancelInd, ApproverRemark, DocType, DocNo, DNo, DocDt, Concat( ");
            SQL.AppendLine("'Warehouse (From) : ', WhsFrom, '\n', ");
            SQL.AppendLine("'Warehouse (To)   : ', WhsTo, '\n', ");
            SQL.AppendLine("'Remark           : ', Remark ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T1.CancelInd, T4.Remark ApproverRemark, 'Transfer Request Whs' As DocType, T1.DocNo, '001' As DNo, T1.DocDt, ");
            SQL.AppendLine("    T2.WhsName As WhsFrom, T3.WhsName As WhsTo, IfNull(T1.Remark, '') As Remark ");
            SQL.AppendLine("    From TblTransferRequestWhsHdr T1 ");
            SQL.AppendLine("    Inner Join TblWarehouse T2 On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    Left Join TblWarehouse T3 On T1.WhsCode2=T3.WhsCode ");
            SQL.AppendLine("    Inner Join TblDocApproval T4 On T1.DocNo = T4.DocNo ");
    	    SQL.AppendLine("        And T4.DocType = 'TransferRequestWhs' ");
    	    SQL.AppendLine("        And T4.Status = 'C' ");
            SQL.AppendLine("    Where T1.Status Not In ('A', 'O') ");
            SQL.AppendLine("    And Exists ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.DocNo ");
            SQL.AppendLine("        From TblDocApproval A ");
            SQL.AppendLine("        Inner Join TblDocApprovalSetting B  On A.DocType=B.DocType And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblTransferRequestWhsHdr C On A.DocNo = C.DocNo ");
            SQL.AppendLine("        Where A.DocType = 'TransferRequestWhs' And A.UserCode Is Not Null And A.Status = 'C' ");
            SQL.AppendLine("        And C.CreateBy = @UserCode ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And T1.CreateBy = @UserCode ");
            SQL.AppendLine(") A Union All ");
            #endregion

            #region Receiving Item From Other Warehouse
            SQL.AppendLine("Select CancelInd, ApproverRemark, DocType, DocNo, DNo, DocDt, Concat( ");
            SQL.AppendLine("'Transfer From  : ', WhsName2, '\n', ");
            SQL.AppendLine("'Transfer To    : ', WhsName, '\n', ");
            SQL.AppendLine("'Item Code      : ', ItCode, '\n', ");
            SQL.AppendLine("'Item Name      : ', ItName, '\n', ");
            SQL.AppendLine("'Quantity (1)   : ', Convert(Format(Qty, 2) Using utf8), ' ', InventoryUomCode, '\n', ");
            SQL.AppendLine("'Quantity (2)   : ', Convert(Format(Qty2, 2) Using utf8), ' ', IfNull(InventoryUomCode2, ''), '\n', ");
            SQL.AppendLine("'Quantity (3)   : ', Convert(Format(Qty3, 2) Using utf8), ' ', IfNull(InventoryUomCode3, ''), '\n', ");
            SQL.AppendLine("'Remark         : ', Remark ) As Remarks ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T2.CancelInd, T6.Remark ApproverRemark, 'Receiving Item From Other Warehouse' As DocType, T1.DocNo, T2.DNo, T1.DocDt, ");
            SQL.AppendLine("    T2.ItCode, T3.WhsName As WhsName2, T4.WhsName, T5.ItName, ");
            SQL.AppendLine("    T2.Qty, T5.InventoryUomCode, T2.Qty2, T5.InventoryUomCode2, T2.Qty3, T5.InventoryUomCode3, IfNull(T2.Remark, '') As Remark ");
            SQL.AppendLine("    From TblRecvWhs2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblRecvWhs2Dtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Inner Join TblWarehouse T3 On T1.WhsCode2 = T3.WhsCode ");
            SQL.AppendLine("    Inner Join TblWarehouse T4 On T1.WhsCode = T4.WhsCode ");
            SQL.AppendLine("    Inner Join TblItem T5 On T2.ItCode = T5.ItCode ");
            SQL.AppendLine("    Inner Join TblDocApproval T6 On T1.DocNo = T6.DocNo  ");
            SQL.AppendLine("    	And T6.DocType = 'RecvWhs2' ");
            SQL.AppendLine("    	And T6.Status = 'C' ");
            SQL.AppendLine("    Where T2.Status='C' ");
            SQL.AppendLine("    And Exists ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("    	Select A.DocNo ");
            SQL.AppendLine("        From TblDocApproval A ");
            SQL.AppendLine("        Inner Join TblDocApprovalSetting B  On A.DocType=B.DocType And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblRecvWhs2Hdr C On A.DocNo = C.DocNo ");
            SQL.AppendLine("        Where A.DocType = 'RecvWhs2' And A.UserCode Is Not Null And A.Status = 'C' ");
            SQL.AppendLine("        And C.CreateBy = @UserCode ");
            SQL.AppendLine("	 ) ");
            SQL.AppendLine("	 And T1.CreateBy = @UserCode ");
            SQL.AppendLine(") A Union All ");
            #endregion

            #region PO Reqeust
            var l = new List<OutstandingPORequestItem>();
            var FilterForPORequest = string.Empty;

            GetOutstandingPORequestItem(ref l);

            if (l.Count == 0)
                FilterForPORequest = "'X'";
            else
            {
                foreach (var i in l)
                {
                    if (FilterForPORequest.Length > 0) FilterForPORequest = string.Concat(FilterForPORequest, ",");
                    FilterForPORequest = string.Concat(FilterForPORequest, "'", i.ItCode, "'");
                }
            }

            SQL.AppendLine("Select CancelInd, ApproverRemark, DocType, DocNo, DNo, DocDt, Concat(");
            SQL.AppendLine("    Case When LocalDocNo Is Null Then '' Else Concat('Local#                        : ', LocalDocNo, '\n') End, ");
            SQL.AppendLine("    'Department                     : ', DeptName, '\n', ");
            SQL.AppendLine("    'Type                           : ', OptDesc, '\n', ");
            SQL.AppendLine("    'Item Code                      : ', ItCode, '\n', ");
            SQL.AppendLine("    'Item Name                      : ', ItName, '\n', ");
            SQL.AppendLine("    'Quantity                       : ', Convert(Format(Qty, 2) Using utf8), ' ', PurchaseUomCode, '\n', ");
            SQL.AppendLine("    'Usage Date                     : ', Right(UsageDt, 2), '/', MonthName(Str_To_Date(Substring(UsageDt, 5, 2), '%m')), '/', Left(UsageDt, 4), '\n', ");
            SQL.AppendLine("    'MR Remark                      : ', MaterialRequestRemark, '\n', ");
            SQL.AppendLine("    'Vendor                         : ', VdName, '\n', ");
            SQL.AppendLine("    'TOP                            : ', PtName, '\n', ");
            if (mNumberOfDecimalPointForPrice.Length == 0)
                SQL.AppendLine("    'Price                          : ', CurCode, ' ', Convert(Format(UPrice, 2) Using utf8), '\n', ");
            else
                SQL.AppendLine("    'Price                          : ', CurCode, ' ', Convert(Trim(Trailing '.' From TRIM(Trailing '0' From Format(UPrice, @NumberOfDecimalPointForPrice))) Using utf8), '\n', ");
            SQL.AppendLine("    If(PrevCurCode Is Null, '', ");
            SQL.AppendLine("    Concat( ");

            if (mNumberOfDecimalPointForPrice.Length == 0)
                SQL.AppendLine("    'Previous Price                 : ', PrevCurCode, ' ', Convert(Format(PrevUPrice, 2) Using utf8), '\n') ");
            else
                SQL.AppendLine("    'Previous Price                 : ', PrevCurCode, ' ', Convert(Trim(Trailing '.' From TRIM(Trailing '0' From Format(PrevUPrice, @NumberOfDecimalPointForPrice))) Using utf8), '\n') ");
            SQL.AppendLine("    ),");

            if (mNumberOfDecimalPointForPrice.Length == 0)
                SQL.AppendLine("    'Amount                         : ', CurCode, ' ', Convert(Format(UPrice*Qty, 2) Using utf8), '\n', ");
            else
                SQL.AppendLine("    'Amount                         : ', CurCode, ' ', Convert(Trim(Trailing '.' From TRIM(Trailing '0' From Format(UPrice*Qty, @NumberOfDecimalPointForPrice))) Using utf8), '\n', ");

            if (mIsPORequestApprovalShowMonthlyDeptAmt)
                SQL.AppendLine("    'Monthly Amount                 : ', Convert(Format(MonthlyDeptAmt, 2) Using utf8), '\n', ");

            if (mIsUseItemConsumption)
            {
                SQL.AppendLine("    'Item Consumption 1 last month  : ', Convert(Format(Mth01, 2) Using utf8),'\n', ");
                SQL.AppendLine("    'Item Consumption 3 last month  : ', Convert(Format(Mth03, 2) Using utf8),'\n', ");
                SQL.AppendLine("    'Item Consumption 6 last month  : ', Convert(Format(Mth06, 2) Using utf8),'\n', ");
                SQL.AppendLine("    'Item Consumption 9 last month  : ', Convert(Format(Mth09, 2) Using utf8),'\n', ");
                SQL.AppendLine("    'Item Consumption 12 last month : ', Convert(Format(Mth12, 2) Using utf8),'\n', ");
            }
            SQL.AppendLine("    'PO Request Remark              : ', PORequestRemark, '\n', ");
            SQL.AppendLine("    'Quotation Remark               : ', QtRemark , '\n', ");
            SQL.AppendLine("    'Delivery Type                  : ', DTName , '\n', ");
            SQL.AppendLine("    'Created By                     : ', UserName");
            SQL.AppendLine(") As Remarks ");

            SQL.AppendLine("From (");
            SQL.AppendLine("    Select T2.CancelInd, T17.Remark ApproverRemark, 'PO Request' As DocType, T1.DocNo, ");
            SQL.AppendLine("    T1.LocalDocNo, ");
            SQL.AppendLine("    T2.DNo, T1.DocDt, ");
            SQL.AppendLine("    T5.DeptName, T6.OptDesc, T4.ItCode, T7.ItName, T2.Qty, T7.PurchaseUomCode, T4.UsageDt, ");
            SQL.AppendLine("    Trim(Concat(IfNull(T3.Remark, ''), ' ', IfNull(T4.Remark, ''))) As MaterialRequestRemark, ");
            SQL.AppendLine("    T10.VdName, T11.PtName, T8.CurCode, T9.UPrice, T13.CurCode As PrevCurCode, T13.UPrice As PrevUPrice, T15.UserName ,IfNull(T16.DTName, '') DTName, ");

            if (mIsPORequestApprovalShowMonthlyDeptAmt)
                SQL.AppendLine("    IfNull(T14.MonthlyDeptAmt, 0) As MonthlyDeptAmt, ");

            SQL.AppendLine("    IfNull(T2.Remark, '') As PORequestRemark, ");
            SQL.AppendLine("    Trim(Concat(IfNull(T8.Remark, ''), ' ', IfNull(T9.Remark, ''))) As QtRemark ");
            if (mIsUseItemConsumption)
            {
                SQL.AppendLine("    , ifnull(T12.Qty01, 0) As Mth01, ifnull(T12.Qty03, 0) As Mth03, ifnull(T12.Qty06, 0) As Mth06, ");
                SQL.AppendLine("    ifnull(T12.Qty09, 0) As Mth09, ifnull(T12.Qty12, 0) As Mth12 ");
            }

            SQL.AppendLine("    From TblPORequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T2 On T1.DocNo=T2.DocNo And IfNull(T2.Status, '')='C'  ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr T3 On T2.MaterialRequestDocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T4 On T2.MaterialRequestDocNo=T4.DocNo And T2.MaterialRequestDNo=T4.DNo And T4.CancelInd='N' And IfNull(T4.Status, '')='A' ");
            SQL.AppendLine("    Inner Join TblDepartment T5 On T3.DeptCode=T5.DeptCode ");
            SQL.AppendLine("    Inner Join TblOption T6 On T6.OptCat='ReqType' And T3.ReqType=T6.OptCode ");
            SQL.AppendLine("    Inner Join TblItem T7 On T4.ItCode=T7.ItCode ");
            SQL.AppendLine("    Inner Join TblQtHdr T8 On T2.QtDocNo=T8.DocNo ");
            SQL.AppendLine("    Inner Join TblQtDtl T9 On T2.QtDocNo=T9.DocNo And T2.QtDNo=T9.DNo  ");
            SQL.AppendLine("    Inner Join TblVendor T10 On T8.VdCode=T10.VdCode ");
            SQL.AppendLine("    Inner Join TblPaymentTerm T11 On T8.PtCode=T11.PtCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Distinct B.ItCode, A.CurCode, B.UPrice ");
            SQL.AppendLine("        From TblQtHdr A ");
            SQL.AppendLine("        Inner Join TblQtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            And B.ItCode In ( ");
            SQL.AppendLine(FilterForPORequest);
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.QtDocNo And B.DNo=C.QtDNo ");
            SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo ");
            SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status = 'C' ");
            SQL.AppendLine("        Where Concat(B.ItCode, E.DocDt, E.DocNo, D.DNo) In ( ");
            SQL.AppendLine("            Select Concat(ItCode, Doc) ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select X4.ItCode, Max(Concat(X1.DocDt, X1.DocNo, X2.DNo)) Doc ");
            SQL.AppendLine("                From TblPOHdr X1 ");
            SQL.AppendLine("                Inner Join TblPODtl X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("                Inner Join TblPORequestDtl X3  ");
            SQL.AppendLine("                    On X2.PORequestDocNo=X3.DocNo  ");
            SQL.AppendLine("                    And X2.PORequestDNo=X3.DNo ");
            SQL.AppendLine("                Inner Join TblQtDtl X4 ");
            SQL.AppendLine("                    On X3.QtDocNo=X4.DocNo ");
            SQL.AppendLine("                    And X3.QtDNo=X4.DNo  ");
            SQL.AppendLine("                    And X4.ItCode In ( ");
            SQL.AppendLine(FilterForPORequest);
            SQL.AppendLine("                    ) ");
            SQL.AppendLine("                Group By X4.ItCode ");
            SQL.AppendLine("            ) X ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") T13 On T4.ItCode=T13.ItCode ");

            if (mIsUseItemConsumption)
            {
                SQL.AppendLine("	Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("		            And B.ItCode In ( ");
                SQL.AppendLine(FilterForPORequest);
                SQL.AppendLine("                    ) ");
                SQL.AppendLine("		            Where A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	                Group By  B.ItCode ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("		            And B.ItCode In ( ");
                SQL.AppendLine(FilterForPORequest);
                SQL.AppendLine("                    ) ");
                SQL.AppendLine("		            Where A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("		            And B.ItCode In ( ");
                SQL.AppendLine(FilterForPORequest);
                SQL.AppendLine("                    ) ");
                SQL.AppendLine("		            Where A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("		            And B.ItCode In ( ");
                SQL.AppendLine(FilterForPORequest);
                SQL.AppendLine("                    ) ");
                SQL.AppendLine("                    Where A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode, SUM(B.Qty)/12 As Qty ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("		            And B.ItCode In ( ");
                SQL.AppendLine(FilterForPORequest);
                SQL.AppendLine("                    ) ");
                SQL.AppendLine("		            Where A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By B.ItCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("    )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" )T12 On T12.ItCode = T4.ItCode ");

            }

            if (mIsPORequestApprovalShowMonthlyDeptAmt)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select Left(A.DocDt, 6) As YrMth, C.DeptCode, ");
                SQL.AppendLine("    Sum(B.Qty*E.UPrice*If(D.CurCode=@MainCurCode, 1, IfNull(F.Amt, 1))) As MonthlyDeptAmt ");
                SQL.AppendLine("    From TblPORequestHdr A ");
                SQL.AppendLine("    Inner Join TblPORequestDtl B ");
                SQL.AppendLine("        On A.DocNo=B.DocNo ");
                SQL.AppendLine("        And B.Status='C' ");
                SQL.AppendLine("    Inner Join TblMaterialRequestHdr C ");
                SQL.AppendLine("        On B.MaterialRequestDocNo=C.DocNo ");
                SQL.AppendLine("    Inner Join TblQtHdr D On B.QtDocNo=D.DocNo ");
                SQL.AppendLine("    Inner Join TblQtDtl E On B.QtDocNo=E.DocNo And B.QtDNo=E.DNo ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select X1.CurCode1, X1.Amt  ");
                SQL.AppendLine("        From TblCurrencyRate X1 ");
                SQL.AppendLine("        Inner Join ( ");
                SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
                SQL.AppendLine("            From TblCurrencyRate ");
                SQL.AppendLine("            Where CurCode2=@MainCurCode ");
                SQL.AppendLine("            Group By CurCode1 ");
                SQL.AppendLine("        ) X2 On X1.CurCode1=X2.CurCode1 And X1.RateDt=X2.RateDt ");
                SQL.AppendLine("        Where CurCode2=@MainCurCode ");
                SQL.AppendLine("    ) F On D.CurCode=F.CurCode1 ");
                SQL.AppendLine("    Where Left(A.DocDt, 6) In (");
                SQL.AppendLine("        Select Distinct Left(X1.DocDt, 6) ");
                SQL.AppendLine("        From TblPORequestHdr X1 ");
                SQL.AppendLine("        Inner Join TblPORequestDtl X2 ");
                SQL.AppendLine("            On X1.DocNo=X2.DocNo ");
                SQL.AppendLine("            And X2.Status='C' ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("    Group By Left(A.DocDt, 6), C.DeptCode ");
                SQL.AppendLine(") T14 ");
                SQL.AppendLine("    On Left(T1.DocDt, 6)=T14.YrMth ");
                SQL.AppendLine("    And T3.DeptCode=T14.DeptCode ");
            }
            SQL.AppendLine("Inner Join TblUser T15 On T2.CreateBy=T15.UserCode ");
            SQL.AppendLine("Left Join TblDeliveryType T16 On T16.DTCode=T8.DTCode ");
            SQL.AppendLine("Inner Join TblDocApproval T17 On T1.DocNo = T17.DocNo  ");
            SQL.AppendLine("    	And T2.DNo = T17.DNo ");
            SQL.AppendLine("    	And T17.DocType = 'PORequest' ");
            SQL.AppendLine("    	And T17.Status = 'C' ");
            SQL.AppendLine("    Where T2.Status='C' ");
            SQL.AppendLine("    And Exists ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("    	Select A.DocNo ");
            SQL.AppendLine("        From TblDocApproval A ");
            SQL.AppendLine("        Inner Join TblDocApprovalSetting B  On A.DocType=B.DocType And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblPORequestDtl C On A.DocNo = C.DocNo And A.DNo = C.DNo ");
            SQL.AppendLine("        Where A.DocType = 'PORequest' And A.UserCode Is Not Null And A.Status = 'C' ");
            SQL.AppendLine("        And C.CreateBy = @UserCode ");
            SQL.AppendLine("	 ) ");
            SQL.AppendLine("	 And T1.CreateBy = @UserCode ");
            SQL.AppendLine(") A Union All ");

            #endregion

            #region Material Request

            SQL.AppendLine("Select CancelInd, ApproverRemark, DocType, DocNo, DNo, DocDt, Concat( ");
            SQL.AppendLine("    'Department     : ', DeptName, '\n', ");

            #region 29/07/2016

            if (mIsApprovalBySiteMandatory)
                SQL.AppendLine("    'Site           : ', SiteName, '\n', ");

            #endregion

            SQL.AppendLine("    'Request Type   : ', OptDesc, '\n', ");
            SQL.AppendLine("    'Item Code      : ', ItCode, '\n', ");
            SQL.AppendLine("    'Item Name      : ', ItName, '\n', ");
            SQL.AppendLine("    'Quantity       : ', Convert(Format(Qty, 2) Using utf8), ' ', PurchaseUomCode, '\n', ");
            SQL.AppendLine("    'In Stock       : ', Convert(Format(InStock, 2) Using utf8), '\n', ");
            SQL.AppendLine("    'Outstanding MR : ', Convert(Format(OutstandingMaterialRequest, 2) Using utf8), '\n', ");
            SQL.AppendLine("    'Ordered (PO)   : ', Convert(Format(OutstandingPO, 2) Using utf8), '\n', ");
            SQL.AppendLine("	'Minimum Stock  : ', Convert(Format(MinStock, 2) Using utf8),'\n', ");
            SQL.AppendLine("	'Reorder Point  : ', Convert(Format(ROP, 2) Using utf8),'\n', ");
            if (mIsUseItemConsumption)
            {
                SQL.AppendLine("    'Item Consumption 1 last month  : ', Convert(Format(Mth01, 2) Using utf8),'\n', ");
                SQL.AppendLine("    'Item Consumption 3 last month  : ', Convert(Format(Mth03, 2) Using utf8),'\n', ");
                SQL.AppendLine("    'Item Consumption 6 last month  : ', Convert(Format(Mth06, 2) Using utf8),'\n', ");
                SQL.AppendLine("    'Item Consumption 9 last month  : ', Convert(Format(Mth09, 2) Using utf8),'\n', ");
                SQL.AppendLine("    'Item Consumption 12 last month : ', Convert(Format(Mth12, 2) Using utf8),'\n', ");
            }
            SQL.AppendLine("	Case When IfNull(UsageDt, '')='' Then '' Else ");
            SQL.AppendLine("	    Concat('Usage Date     : ', Right(UsageDt, 2), '/', MonthName(Str_To_Date(Substring(UsageDt, 5, 2), '%m')), '/', Left(UsageDt, 4), '\n') ");
            SQL.AppendLine("	End, ");
            SQL.AppendLine("    'Remark         : ', Remark, '\n',  ");
            SQL.AppendLine("    'Created By     : ', UserName ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T2.CancelInd, T12.Remark As ApproverRemark, 'Material Request From Department' As DocType, T1.DocNo, T2.DNo, T1.DocDt, T11.UserName, ");
            SQL.AppendLine("    T3.DeptName, ");
            if (mIsApprovalBySiteMandatory) SQL.AppendLine("    T10.SiteName, ");
            SQL.AppendLine("    T4.OptDesc, T2.ItCode, T5.ItName, T2.Qty, T5.PurchaseUomCode, T2.UsageDt, ");
            SQL.AppendLine("    IfNull(T5.MinStock, 0) As MinStock, ");
            SQL.AppendLine("    IfNull(T5.ReOrderStock, 0) As ROP, ");
            SQL.AppendLine("    IfNull(T6.InStock, 0) As InStock, ");
            SQL.AppendLine("    IfNull(T7.OutstandingPO, 0) As OutstandingPO, ");
            SQL.AppendLine("    IfNull(T8.OutstandingMaterialRequest, 0) As OutstandingMaterialRequest, ");

            if (mIsUseItemConsumption)
            {
                SQL.AppendLine("    ifnull(T9.Qty01, 0) As Mth01, ifnull(T9.Qty03, 0) As Mth03, ifnull(T9.Qty06, 0) As Mth06, ");
                SQL.AppendLine("    ifnull(T9.Qty09, 0) As Mth09, ifnull(T9.Qty12, 0) As Mth12, ");
            }
            SQL.AppendLine("    Concat(IfNull(T1.Remark, ''), Case When IfNull(T1.Remark, '')<>'' And IfNull(T2.Remark, '')<>'' Then '\n' Else '' End, IfNull(T2.Remark, '')) As Remark ");

            SQL.AppendLine("    From TblMaterialRequestHdr T1  ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo And IfNull(T2.Status, '') = 'C' ");
            SQL.AppendLine("    Inner Join TblDepartment T3 On T1.DeptCode=T3.DeptCode ");
            SQL.AppendLine("    Inner Join TblOption T4 On T4.OptCat='ReqType' And T1.ReqType=T4.OptCode ");
            SQL.AppendLine("    Inner Join TblItem T5 On T2.ItCode=T5.ItCode ");

            SQL.AppendLine("	Left Join ( ");
            SQL.AppendLine("		Select ItCode, Sum(Qty) As InStock ");
            SQL.AppendLine("		From TblStockSummary ");
            SQL.AppendLine("		Where Qty>0 ");
            SQL.AppendLine("		And ItCode In ( ");
            SQL.AppendLine("		    Select Distinct ItCode From TblMaterialRequestDtl ");
            SQL.AppendLine("		    Where IfNull(Status, 'C')='C' ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("		Group By ItCode ");
            SQL.AppendLine("		) T6 On T2.ItCode=T6.ItCode ");

            SQL.AppendLine("	Left Join ( ");
            SQL.AppendLine("	    Select ItCode, Sum(Qty) OutstandingPO From (");
            SQL.AppendLine("            Select C.ItCode, A.Qty-IfNull(Qty2, 0)-IfNull(Qty3, 0) As Qty ");
            SQL.AppendLine("            From TblPODtl A ");
            SQL.AppendLine("            Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
            SQL.AppendLine("            Inner Join TblMaterialRequestDtl C ");
            SQL.AppendLine("                On B.MaterialRequestDocNo=C.DocNo ");
            SQL.AppendLine("                And B.MaterialRequestDNo=C.DNo ");
            SQL.AppendLine("		        And ItCode In ( ");
            SQL.AppendLine("		            Select Distinct ItCode From TblMaterialRequestDtl ");
            SQL.AppendLine("		            Where IfNull(Status, 'C')='C' ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("            Left Join ( ");
            SQL.AppendLine("                Select D1.PODocNo As DocNo, D1.PODNo As DNo, Sum(D1.QtyPurchase) As Qty2 ");
            SQL.AppendLine("                From TblRecvVdDtl D1 ");
            SQL.AppendLine("                Inner Join TblPODtl D2 On D1.PODocNo=D2.DocNo And D1.PODNo=D2.DNo And D2.ProcessInd<>'F' ");
            SQL.AppendLine("		        Where D1.ItCode In ( ");
            SQL.AppendLine("		            Select Distinct ItCode From TblMaterialRequestDtl ");
            SQL.AppendLine("		            Where IfNull(Status, 'C')='C' ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("                And D1.CancelInd='N' ");
            SQL.AppendLine("                Group By D1.PODocNo, D1.PODNo ");
            SQL.AppendLine("            ) D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            SQL.AppendLine("            Left Join ( ");
            SQL.AppendLine("                Select E1.PODocNo As DocNo, E1.PODNo As DNo, Sum(E1.Qty) As Qty3 ");
            SQL.AppendLine("                From TblPOQtyCancel E1 ");
            SQL.AppendLine("                Inner Join TblPODtl E2 On E1.PODocNo=E2.DocNo And E1.PODNo=E2.DNo And E2.ProcessInd<>'F' ");
            SQL.AppendLine("                Inner Join TblPORequestDtl E3 On E2.PORequestDocNo=E3.DocNo And E2.PORequestDNo=E3.DNo ");
            SQL.AppendLine("                Inner Join TblMaterialRequestDtl E4 ");
            SQL.AppendLine("                    On E3.MaterialRequestDocNo=E4.DocNo ");
            SQL.AppendLine("                    And E3.MaterialRequestDNo=E4.DNo ");
            SQL.AppendLine("		            And E4.ItCode In ( ");
            SQL.AppendLine("		                Select Distinct ItCode From TblMaterialRequestDtl ");
            SQL.AppendLine("		                Where IfNull(Status, 'C')='C' ");
            SQL.AppendLine("                    ) ");
            SQL.AppendLine("                Where E1.CancelInd='N' ");
            SQL.AppendLine("                Group By E1.PODocNo, E1.PODNo ");
            SQL.AppendLine("            ) E On A.DocNo=E.DocNo And A.DNo=E.DNo ");
            SQL.AppendLine("		    Where A.ProcessInd<>'F' ");
            SQL.AppendLine("        ) Tbl Where Qty>0 Group By ItCode ");
            SQL.AppendLine(") T7 On T2.ItCode=T7.ItCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select ItCode, Sum(Qty) OutstandingMaterialRequest From (");
            SQL.AppendLine("        Select A.ItCode, (A.Qty-IfNull(B.Qty, 0)-ifnull(C.QtyRecv, 0)) As Qty ");
            SQL.AppendLine("        From TblMaterialRequestDtl A ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select B1.DocNo As DocNo, B1.DNo As DNo, Sum(B1.Qty) As Qty ");
            SQL.AppendLine("            From TblPODtl B1 ");
            SQL.AppendLine("            Inner Join TblPORequestDtl B2 On B1.PORequestDocNo=B2.DocNo And B1.PORequestDNo=B2.DNo ");
            SQL.AppendLine("            Inner Join TblMaterialRequestDtl B3 ");
            SQL.AppendLine("                On B2.MaterialRequestDocNo=B3.DocNo ");
            SQL.AppendLine("                And B2.MaterialRequestDNo=B3.DNo ");
            SQL.AppendLine("                And B3.ProcessInd<>'F' ");
            SQL.AppendLine("		        And B3.ItCode In ( ");
            SQL.AppendLine("		            Select Distinct ItCode From TblMaterialRequestDtl ");
            SQL.AppendLine("		            Where IfNull(Status, 'C')='C' ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("            Group By B1.DocNo, B1.DNo ");
            SQL.AppendLine("        ) B On A.DocNo=B.DocNo And A.DNo=B.DNo ");

            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select D.DocNo As DocNo, D.DNo As DNo, Sum(A.Qty) As QtyRecv  ");
            SQL.AppendLine("        From TblRecvVdDtl A ");
            SQL.AppendLine("        Inner Join TblPODtl B On A.PODocNo = B.DocNo And A.PODno = B.Dno  ");
            SQL.AppendLine("        Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo  ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl D ");
            SQL.AppendLine("            On C.MaterialRequestDocNo=D.DocNo ");
            SQL.AppendLine("            And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("            And D.ProcessInd<>'F' ");
            SQL.AppendLine("        Group By B.DocNo, B.DNo  ");
            SQL.AppendLine("    )C ON C.Docno = A.DocNo And C.Dno = A.Dno ");

            SQL.AppendLine("        Where A.ProcessInd<>'F' And IfNull(A.Status, '')='C' ");
            SQL.AppendLine("        And A.ItCode In ( ");
            SQL.AppendLine("		    Select Distinct ItCode From TblMaterialRequestDtl ");
            SQL.AppendLine("            Where IfNull(Status, 'C')='C' ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) Tbl Where Qty>0 Group By ItCode ");
            SQL.AppendLine(") T8 On T2.ItCode=T8.ItCode ");

            if (mIsUseItemConsumption)
            {
                SQL.AppendLine("	Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, Sum(Qty01) As Qty01, Sum(Qty03) As Qty03, Sum(Qty06) As Qty06, Sum(Qty09) As Qty09, Sum(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, Sum(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, Sum(B.Qty) As Qty ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("		            Where A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("		            And B.ItCode In ( ");
                SQL.AppendLine("		                Select ItCode From TblMaterialRequestDtl ");
                SQL.AppendLine("		                Where IfNull(Status, 'C')='C' ");
                SQL.AppendLine("                    ) ");
                SQL.AppendLine("	                Group By B.ItCode ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("		            Where A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("		            And B.ItCode In ( ");
                SQL.AppendLine("		                Select ItCode From TblMaterialRequestDtl ");
                SQL.AppendLine("		                Where IfNull(Status, 'C')='C' ");
                SQL.AppendLine("                    ) ");
                SQL.AppendLine("	                Group By  B.ItCode");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode, SUM(B.Qty)/6 As Qty ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("		            Where A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("		            And B.ItCode In ( ");
                SQL.AppendLine("		                Select ItCode From TblMaterialRequestDtl ");
                SQL.AppendLine("		                Where IfNull(Status, 'C')='C' ");
                SQL.AppendLine("                    ) ");
                SQL.AppendLine("	                Group By  B.ItCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("		            Where A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("		            And B.ItCode In ( ");
                SQL.AppendLine("		                Select ItCode From TblMaterialRequestDtl ");
                SQL.AppendLine("		                Where IfNull(Status, 'C')='C' ");
                SQL.AppendLine("                    ) ");
                SQL.AppendLine("	                Group By  B.ItCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode, Sum(B.Qty)/12 As Qty ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("		            Where A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("		            And B.ItCode In ( ");
                SQL.AppendLine("		                Select ItCode From TblMaterialRequestDtl ");
                SQL.AppendLine("		                Where IfNull(Status, 'C')='C' ");
                SQL.AppendLine("                    ) ");
                SQL.AppendLine("	                Group By  B.ItCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("    )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" ) T9 On T9.ItCode = T2.ItCode ");
            }
            if (mIsApprovalBySiteMandatory) SQL.AppendLine("    Inner Join TblSite T10 On T1.SiteCode=T10.SiteCode ");
            SQL.AppendLine("Inner Join TblUser T11 On T2.CreateBy=T11.UserCode ");
            SQL.AppendLine("Inner Join TblDocApproval T12 On T2.DocNo = T12.DocNo ");
	        SQL.AppendLine("    And T2.DNo = T12.DNo ");
	        SQL.AppendLine("    And T12.DocType = 'MaterialRequest' ");
	        SQL.AppendLine("    And T12.UserCode Is Not Null ");
	        SQL.AppendLine("    And T12.Status = 'C' ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.DocNo ");
            SQL.AppendLine("    From TblDocApproval A ");
            SQL.AppendLine("    Inner Join TblDocApprovalSetting B  On A.DocType=B.DocType And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl C On A.DocNo = C.DocNo ");
            SQL.AppendLine("    Where A.DocType = 'MaterialRequest' And A.UserCode Is Not Null And A.Status = 'C' ");
            SQL.AppendLine("    And C.CreateBy = @UserCode ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And T2.CreateBy = @UserCode ");
            SQL.AppendLine(") A Union All ");

            #endregion

            #region NTP
            SQL.AppendLine("Select CancelInd, ApproverRemark, DocType, DocNo, DNo, DocDt, Concat( ");
            SQL.AppendLine("'From         : ', NtpFrom, '\n', ");
            SQL.AppendLine("'To           : ', NtpTo, '\n', ");
            SQL.AppendLine("'Project Name : ', ProjectName, '\n', ");
            SQL.AppendLine("'Customer     : ', CtName ) As Remarks ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T1.CancelInd, T4.Remark As ApproverRemark, 'Notice To Proceed' As DocType, T1.DocNo, '001' As DNo, T1.DocDt, ");
            SQL.AppendLine("    IfNull(T1.NtpFrom, '') NtpFrom, IfNull(T1.NtpTo, '') NtpTo, IfNull(T2.ProjectName, '') ProjectName, ");
            SQL.AppendLine("    IfNull(T3.CtName, '') CtName ");
            SQL.AppendLine("    From TblNoticeToProceed T1 ");
            SQL.AppendLine("    Inner Join TblProjectGroup T2 ON T1.PGCode = T2.PGCode AND T2.ActInd = 'Y' ");
            SQL.AppendLine("    Inner Join TblCustomer T3 On T2.CtCode=T3.CtCode ");
            SQL.AppendLine("    Inner Join TblDocApproval T4 On T1.DocNo = T4.DocNo ");
            SQL.AppendLine("        And T4.DocType = 'NTP' ");
            SQL.AppendLine("        And T4.Status = 'C' ");
            SQL.AppendLine("    And Exists ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.DocNo ");
            SQL.AppendLine("        From TblDocApproval A ");
            SQL.AppendLine("        Inner Join TblDocApprovalSetting B  On A.DocType=B.DocType And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblNoticeToProceed C On A.DocNo = C.DocNo ");
            SQL.AppendLine("        Where A.DocType = 'NTP' And A.UserCode Is Not Null And A.Status = 'C' ");
            SQL.AppendLine("        And C.CreateBy = @UserCode ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And T1.CreateBy = @UserCode ");
            SQL.AppendLine(") A ");
            #endregion

            SQL.AppendLine(") T");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5      
                        "Date",
                        "Document#",
                        "Type",
                        "Cancel",
                        "Notes",

                        //6
                        "Approvers Remark",
                   },
                     new int[] 
                    {
                        //0
                        40,

                        //1-5
                        100, 150, 150, 70, 250,
                        
                        //6
                        180
                   }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.SetGrdProperty(Grd1, false);
            
            for (int i = 0; i < 5; i++ )
                Grd1.Cols[i].CellStyle.TextAlign = iGContentAlignment.MiddleLeft;

            for (int i = 0; i < Grd1.Cols.Count - 1; i++)
            {
                if (i % 2 != 0)
                    Grd1.Cols[i].CellStyle.BackColor = Color.LightGray;
            }
        }

        override protected void ShowData()
        {
            DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);

            if (Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)) return;
            try
            {
                string Filter = " Where 0=0 ";
                var cm = new MySqlCommand();

                if (ChkDocDt.Checked)
                {
                    Filter += " And DocDt Between @DocDt_1 And @DocDt_2 ";
                    Sm.CmParam<String>(ref cm, "@DocDt_1", Sm.GetDte(DteDocDt1).Substring(0, 8));
                    Sm.CmParam<String>(ref cm, "@DocDt_2", Sm.GetDte(DteDocDt2).Substring(0, 8));
                }
                Sm.CmParam<String>( ref cm, "@UserCode", Gv.CurrentUserCode );
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "DocType", true);
                Sm.CmParamDt(ref cm, "@DocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));

                Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode.Length > 0 ? mMainCurCode : "XXX");
                Sm.CmParam<String>(ref cm, "@NumberOfDecimalPointForPrice", mNumberOfDecimalPointForPrice);

                Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            SetSQL() + Filter + "Order By DocNo;",
                            new string[] { "DocDt", "DocNo", "DocType", "CancelInd", "Remarks", "ApproverRemark", "DNo"  },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                                Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                                Grd.Cells[Row, 5].Value = Sm.DrStr(dr, 6);
                                Grd.Cells[Row, 5].Font = new Font("Lucida Console", 9);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            }, true, false, true, true
                        );
                if (Grd1.Rows.Count > 1)
                {
                    Grd1.Rows.RemoveAt(Grd1.Rows.Count - 1);
                }
                Grd1.Rows.AutoHeight();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mNumberOfDecimalPointForPrice = Sm.GetParameter("NumberOfDecimalPointForPrice");
            mIsUseItemConsumption = Sm.GetParameterBoo("IsUseItemConsumption");
            mIsPORequestApprovalShowMonthlyDeptAmt = Sm.GetParameterBoo("IsPORequestApprovalShowMonthlyDeptAmt");
            mIsApprovalBySiteMandatory = Sm.GetParameterBoo("IsApprovalBySiteMandatory");
        }

        private void GetOutstandingPORequestItem(ref List<OutstandingPORequestItem> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct B.ItCode  ");
            SQL.AppendLine("From TblPORequestDtl A, TblQtDtl B ");
            SQL.AppendLine("Where A.Status='O' ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.QtDocNo=B.DocNo ");
            SQL.AppendLine("And A.QtDNo=B.DNo ");
            SQL.AppendLine("Order By B.ItCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OutstandingPORequestItem()
                        {
                            ItCode = Sm.DrStr(dr, c[0])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SetLueDocType(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptDesc As Col1, OptDesc As Col2 ");
                SQL.AppendLine("From TblOption Where OptCat='ApprovalType' ");
                SQL.AppendLine("And OptCode In ");
                SQL.AppendLine("( ");
	            SQL.AppendLine("    Select Distinct DocType From TblDocApproval ");
	            SQL.AppendLine("    Where UserCode Is Not Null ");
	            SQL.AppendLine("    And Status = 'C' ");
	            SQL.AppendLine("    And CreateBy = @UserCode ");
                SQL.AppendLine(") ");
                SQL.AppendLine("Order By OptDesc; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Type", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events
        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(SetLueDocType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }
        #endregion

        #endregion

        #region Class
        private class OutstandingPORequestItem
        {
            public string ItCode { get; set; }
        }
        #endregion
    }
}
