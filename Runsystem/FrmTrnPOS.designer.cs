﻿namespace RunSystem
{
    partial class FrmTrnPOS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            TenTec.Windows.iGridLib.iGRowPattern iGRowPattern1 = new TenTec.Windows.iGridLib.iGRowPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern1 = new TenTec.Windows.iGridLib.iGCellPattern();
            this.iGCellStyleDesign1 = new TenTec.Windows.iGridLib.iGCellStyleDesign();
            this.iGColHdrStyleDesign1 = new TenTec.Windows.iGridLib.iGColHdrStyleDesign();
            this.iGCellStyleDesign2 = new TenTec.Windows.iGridLib.iGCellStyleDesign();
            this.iGCellStyleDesign3 = new TenTec.Windows.iGridLib.iGCellStyleDesign();
            this.iGCellStyleDesign4 = new TenTec.Windows.iGridLib.iGCellStyleDesign();
            this.iGCellStyleDesign5 = new TenTec.Windows.iGridLib.iGCellStyleDesign();
            this.iGCellStyleDesign6 = new TenTec.Windows.iGridLib.iGCellStyleDesign();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.pnlCode = new System.Windows.Forms.Panel();
            this.LblCode = new System.Windows.Forms.Label();
            this.pnlMainTop1 = new System.Windows.Forms.Panel();
            this.pnlNoTrn = new System.Windows.Forms.Panel();
            this.LblTrnNo = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.LblCashier = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pnlShift = new System.Windows.Forms.Panel();
            this.LblShift = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PnlPosNo = new System.Windows.Forms.Panel();
            this.LblPos = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlDate = new System.Windows.Forms.Panel();
            this.LblDate = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.LbAuditRoll = new System.Windows.Forms.ListBox();
            this.PnlRate = new System.Windows.Forms.Panel();
            this.LblUpdateRate = new System.Windows.Forms.Label();
            this.TxtBuyRate = new System.Windows.Forms.TextBox();
            this.TxtSellRate = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PnlTotal = new System.Windows.Forms.Panel();
            this.LbTotalValue = new System.Windows.Forms.ListBox();
            this.LbTotalTitle = new System.Windows.Forms.ListBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtGrandTotal = new System.Windows.Forms.TextBox();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.PnlConnStatus = new System.Windows.Forms.Panel();
            this.lblConnStatus = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.pnlCode.SuspendLayout();
            this.pnlMainTop1.SuspendLayout();
            this.pnlNoTrn.SuspendLayout();
            this.panel10.SuspendLayout();
            this.pnlShift.SuspendLayout();
            this.PnlPosNo.SuspendLayout();
            this.pnlDate.SuspendLayout();
            this.panel14.SuspendLayout();
            this.PnlRate.SuspendLayout();
            this.PnlTotal.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.PnlConnStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // iGColHdrStyleDesign1
            // 
            this.iGColHdrStyleDesign1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iGColHdrStyleDesign1.TextAlign = TenTec.Windows.iGridLib.iGContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pnlTop);
            this.panel1.Controls.Add(this.pnlBottom);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1017, 504);
            this.panel1.TabIndex = 0;
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.panel15);
            this.pnlTop.Controls.Add(this.panel14);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(1017, 479);
            this.pnlTop.TabIndex = 57;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.Grd1);
            this.panel15.Controls.Add(this.pnlCode);
            this.panel15.Controls.Add(this.pnlMainTop1);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(788, 479);
            this.panel15.TabIndex = 1;
            // 
            // Grd1
            // 
            this.Grd1.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.AutoResizeCols = true;
            this.Grd1.BorderColor = System.Drawing.Color.White;
            this.Grd1.BorderStyle = TenTec.Windows.iGridLib.iGBorderStyle.Flat;
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.FrozenArea.ColsEdge = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.White, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.GroupRows = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.Horizontal = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.HorizontalExtended = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GridLines.Vertical = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.VerticalLastCol = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.White, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.HighlightSelCells = false;
            this.Grd1.Location = new System.Drawing.Point(0, 70);
            this.Grd1.Name = "Grd1";
            this.Grd1.ReadOnly = true;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowMode = true;
            iGRowPattern1.Height = 20;
            iGRowPattern1.Sortable = false;
            this.Grd1.Rows.AddRange(new TenTec.Windows.iGridLib.iGRowPattern[] {
            iGRowPattern1}, new TenTec.Windows.iGridLib.iGCellPattern[] {
            iGCellPattern1});
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SelCellsBackColor = System.Drawing.Color.Transparent;
            this.Grd1.SelCellsBackColorNoFocus = System.Drawing.Color.Transparent;
            this.Grd1.SelCellsForeColor = System.Drawing.Color.Transparent;
            this.Grd1.SelCellsForeColorNoFocus = System.Drawing.Color.Transparent;
            this.Grd1.Size = new System.Drawing.Size(788, 409);
            this.Grd1.TabIndex = 56;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.CellClick += new TenTec.Windows.iGridLib.iGCellClickEventHandler(this.Grd1_CellClick);
            // 
            // pnlCode
            // 
            this.pnlCode.BackColor = System.Drawing.Color.Black;
            this.pnlCode.Controls.Add(this.LblCode);
            this.pnlCode.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCode.Location = new System.Drawing.Point(0, 24);
            this.pnlCode.Name = "pnlCode";
            this.pnlCode.Size = new System.Drawing.Size(788, 46);
            this.pnlCode.TabIndex = 55;
            // 
            // LblCode
            // 
            this.LblCode.AutoSize = true;
            this.LblCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LblCode.Font = new System.Drawing.Font("Franklin Gothic Medium", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCode.ForeColor = System.Drawing.Color.White;
            this.LblCode.Location = new System.Drawing.Point(0, 0);
            this.LblCode.Name = "LblCode";
            this.LblCode.Size = new System.Drawing.Size(226, 43);
            this.LblCode.TabIndex = 0;
            this.LblCode.Text = "123456789";
            // 
            // pnlMainTop1
            // 
            this.pnlMainTop1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.pnlMainTop1.Controls.Add(this.pnlNoTrn);
            this.pnlMainTop1.Controls.Add(this.panel10);
            this.pnlMainTop1.Controls.Add(this.pnlShift);
            this.pnlMainTop1.Controls.Add(this.PnlPosNo);
            this.pnlMainTop1.Controls.Add(this.pnlDate);
            this.pnlMainTop1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMainTop1.ForeColor = System.Drawing.Color.Black;
            this.pnlMainTop1.Location = new System.Drawing.Point(0, 0);
            this.pnlMainTop1.Name = "pnlMainTop1";
            this.pnlMainTop1.Size = new System.Drawing.Size(788, 24);
            this.pnlMainTop1.TabIndex = 54;
            // 
            // pnlNoTrn
            // 
            this.pnlNoTrn.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.pnlNoTrn.Controls.Add(this.LblTrnNo);
            this.pnlNoTrn.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlNoTrn.ForeColor = System.Drawing.Color.Black;
            this.pnlNoTrn.Location = new System.Drawing.Point(640, 0);
            this.pnlNoTrn.Name = "pnlNoTrn";
            this.pnlNoTrn.Size = new System.Drawing.Size(148, 24);
            this.pnlNoTrn.TabIndex = 58;
            // 
            // LblTrnNo
            // 
            this.LblTrnNo.AutoSize = true;
            this.LblTrnNo.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTrnNo.ForeColor = System.Drawing.Color.White;
            this.LblTrnNo.Location = new System.Drawing.Point(48, 4);
            this.LblTrnNo.Name = "LblTrnNo";
            this.LblTrnNo.Size = new System.Drawing.Size(48, 17);
            this.LblTrnNo.TabIndex = 1;
            this.LblTrnNo.Text = "R1234";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel10.Controls.Add(this.LblCashier);
            this.panel10.Controls.Add(this.label8);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.ForeColor = System.Drawing.Color.Black;
            this.panel10.Location = new System.Drawing.Point(385, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(403, 24);
            this.panel10.TabIndex = 57;
            // 
            // LblCashier
            // 
            this.LblCashier.AutoSize = true;
            this.LblCashier.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCashier.ForeColor = System.Drawing.Color.White;
            this.LblCashier.Location = new System.Drawing.Point(59, 4);
            this.LblCashier.Name = "LblCashier";
            this.LblCashier.Size = new System.Drawing.Size(37, 17);
            this.LblCashier.TabIndex = 1;
            this.LblCashier.Text = "Anita";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(5, 4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Cashier :";
            // 
            // pnlShift
            // 
            this.pnlShift.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.pnlShift.Controls.Add(this.LblShift);
            this.pnlShift.Controls.Add(this.label6);
            this.pnlShift.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlShift.ForeColor = System.Drawing.Color.Black;
            this.pnlShift.Location = new System.Drawing.Point(314, 0);
            this.pnlShift.Name = "pnlShift";
            this.pnlShift.Size = new System.Drawing.Size(71, 24);
            this.pnlShift.TabIndex = 56;
            // 
            // LblShift
            // 
            this.LblShift.AutoSize = true;
            this.LblShift.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblShift.ForeColor = System.Drawing.Color.White;
            this.LblShift.Location = new System.Drawing.Point(41, 4);
            this.LblShift.Name = "LblShift";
            this.LblShift.Size = new System.Drawing.Size(16, 17);
            this.LblShift.TabIndex = 1;
            this.LblShift.Text = "1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(5, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Shift :";
            // 
            // PnlPosNo
            // 
            this.PnlPosNo.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.PnlPosNo.Controls.Add(this.LblPos);
            this.PnlPosNo.Controls.Add(this.label4);
            this.PnlPosNo.Dock = System.Windows.Forms.DockStyle.Left;
            this.PnlPosNo.ForeColor = System.Drawing.Color.Black;
            this.PnlPosNo.Location = new System.Drawing.Point(194, 0);
            this.PnlPosNo.Name = "PnlPosNo";
            this.PnlPosNo.Size = new System.Drawing.Size(120, 24);
            this.PnlPosNo.TabIndex = 55;
            // 
            // LblPos
            // 
            this.LblPos.AutoSize = true;
            this.LblPos.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPos.ForeColor = System.Drawing.Color.White;
            this.LblPos.Location = new System.Drawing.Point(51, 5);
            this.LblPos.Name = "LblPos";
            this.LblPos.Size = new System.Drawing.Size(40, 17);
            this.LblPos.TabIndex = 1;
            this.LblPos.Tag = "pan";
            this.LblPos.Text = "A100";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(13, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Pos :";
            // 
            // pnlDate
            // 
            this.pnlDate.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.pnlDate.Controls.Add(this.LblDate);
            this.pnlDate.Controls.Add(this.label1);
            this.pnlDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlDate.ForeColor = System.Drawing.Color.Black;
            this.pnlDate.Location = new System.Drawing.Point(0, 0);
            this.pnlDate.Name = "pnlDate";
            this.pnlDate.Size = new System.Drawing.Size(194, 24);
            this.pnlDate.TabIndex = 54;
            // 
            // LblDate
            // 
            this.LblDate.AutoSize = true;
            this.LblDate.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDate.ForeColor = System.Drawing.Color.White;
            this.LblDate.Location = new System.Drawing.Point(97, 4);
            this.LblDate.Name = "LblDate";
            this.LblDate.Size = new System.Drawing.Size(81, 17);
            this.LblDate.TabIndex = 1;
            this.LblDate.Text = "14 Apr 2016";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(5, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Business Date :";
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.LbAuditRoll);
            this.panel14.Controls.Add(this.PnlRate);
            this.panel14.Controls.Add(this.PnlTotal);
            this.panel14.Controls.Add(this.panel3);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel14.Location = new System.Drawing.Point(788, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(229, 479);
            this.panel14.TabIndex = 0;
            // 
            // LbAuditRoll
            // 
            this.LbAuditRoll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LbAuditRoll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LbAuditRoll.Enabled = false;
            this.LbAuditRoll.Font = new System.Drawing.Font("Lucida Console", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbAuditRoll.FormattingEnabled = true;
            this.LbAuditRoll.ItemHeight = 9;
            this.LbAuditRoll.Location = new System.Drawing.Point(0, 59);
            this.LbAuditRoll.Name = "LbAuditRoll";
            this.LbAuditRoll.Size = new System.Drawing.Size(229, 317);
            this.LbAuditRoll.TabIndex = 63;
            // 
            // PnlRate
            // 
            this.PnlRate.BackColor = System.Drawing.Color.MistyRose;
            this.PnlRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlRate.Controls.Add(this.LblUpdateRate);
            this.PnlRate.Controls.Add(this.TxtBuyRate);
            this.PnlRate.Controls.Add(this.TxtSellRate);
            this.PnlRate.Controls.Add(this.label3);
            this.PnlRate.Controls.Add(this.label2);
            this.PnlRate.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlRate.Location = new System.Drawing.Point(0, 0);
            this.PnlRate.Name = "PnlRate";
            this.PnlRate.Size = new System.Drawing.Size(229, 59);
            this.PnlRate.TabIndex = 62;
            this.PnlRate.Visible = false;
            // 
            // LblUpdateRate
            // 
            this.LblUpdateRate.AutoSize = true;
            this.LblUpdateRate.Font = new System.Drawing.Font("Franklin Gothic Medium", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblUpdateRate.Location = new System.Drawing.Point(3, 40);
            this.LblUpdateRate.Name = "LblUpdateRate";
            this.LblUpdateRate.Size = new System.Drawing.Size(100, 16);
            this.LblUpdateRate.TabIndex = 5;
            this.LblUpdateRate.Text = "Last Update Date :";
            // 
            // TxtBuyRate
            // 
            this.TxtBuyRate.BackColor = System.Drawing.Color.MistyRose;
            this.TxtBuyRate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtBuyRate.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBuyRate.ForeColor = System.Drawing.Color.Maroon;
            this.TxtBuyRate.Location = new System.Drawing.Point(95, 22);
            this.TxtBuyRate.Name = "TxtBuyRate";
            this.TxtBuyRate.Size = new System.Drawing.Size(122, 15);
            this.TxtBuyRate.TabIndex = 4;
            this.TxtBuyRate.Text = "0.00";
            this.TxtBuyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtSellRate
            // 
            this.TxtSellRate.BackColor = System.Drawing.Color.MistyRose;
            this.TxtSellRate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtSellRate.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSellRate.ForeColor = System.Drawing.Color.Maroon;
            this.TxtSellRate.Location = new System.Drawing.Point(95, 2);
            this.TxtSellRate.Name = "TxtSellRate";
            this.TxtSellRate.Size = new System.Drawing.Size(122, 15);
            this.TxtSellRate.TabIndex = 3;
            this.TxtSellRate.Text = "0.00";
            this.TxtSellRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(3, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Buy Rate :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Sell Rate :";
            // 
            // PnlTotal
            // 
            this.PnlTotal.BackColor = System.Drawing.Color.Cornsilk;
            this.PnlTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlTotal.Controls.Add(this.LbTotalValue);
            this.PnlTotal.Controls.Add(this.LbTotalTitle);
            this.PnlTotal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PnlTotal.Location = new System.Drawing.Point(0, 384);
            this.PnlTotal.Name = "PnlTotal";
            this.PnlTotal.Size = new System.Drawing.Size(229, 30);
            this.PnlTotal.TabIndex = 60;
            // 
            // LbTotalValue
            // 
            this.LbTotalValue.BackColor = System.Drawing.Color.Cornsilk;
            this.LbTotalValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LbTotalValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LbTotalValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbTotalValue.ItemHeight = 18;
            this.LbTotalValue.Location = new System.Drawing.Point(127, 0);
            this.LbTotalValue.Name = "LbTotalValue";
            this.LbTotalValue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LbTotalValue.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.LbTotalValue.Size = new System.Drawing.Size(100, 18);
            this.LbTotalValue.TabIndex = 59;
            // 
            // LbTotalTitle
            // 
            this.LbTotalTitle.BackColor = System.Drawing.Color.Cornsilk;
            this.LbTotalTitle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LbTotalTitle.Dock = System.Windows.Forms.DockStyle.Left;
            this.LbTotalTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbTotalTitle.FormattingEnabled = true;
            this.LbTotalTitle.ItemHeight = 18;
            this.LbTotalTitle.Location = new System.Drawing.Point(0, 0);
            this.LbTotalTitle.Name = "LbTotalTitle";
            this.LbTotalTitle.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.LbTotalTitle.Size = new System.Drawing.Size(127, 18);
            this.LbTotalTitle.TabIndex = 16;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Cornsilk;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.TxtGrandTotal);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 414);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(229, 65);
            this.panel3.TabIndex = 59;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Cornsilk;
            this.label7.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 21);
            this.label7.TabIndex = 5;
            this.label7.Text = "Grand Total";
            // 
            // TxtGrandTotal
            // 
            this.TxtGrandTotal.BackColor = System.Drawing.Color.Cornsilk;
            this.TxtGrandTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtGrandTotal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TxtGrandTotal.Font = new System.Drawing.Font("Franklin Gothic Medium", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrandTotal.Location = new System.Drawing.Point(0, 29);
            this.TxtGrandTotal.Name = "TxtGrandTotal";
            this.TxtGrandTotal.ReadOnly = true;
            this.TxtGrandTotal.Size = new System.Drawing.Size(227, 34);
            this.TxtGrandTotal.TabIndex = 6;
            this.TxtGrandTotal.TabStop = false;
            this.TxtGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.PnlConnStatus);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 479);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1017, 25);
            this.pnlBottom.TabIndex = 56;
            // 
            // PnlConnStatus
            // 
            this.PnlConnStatus.BackColor = System.Drawing.Color.Red;
            this.PnlConnStatus.Controls.Add(this.lblConnStatus);
            this.PnlConnStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PnlConnStatus.Location = new System.Drawing.Point(0, 0);
            this.PnlConnStatus.Name = "PnlConnStatus";
            this.PnlConnStatus.Size = new System.Drawing.Size(1017, 25);
            this.PnlConnStatus.TabIndex = 1;
            this.PnlConnStatus.Visible = false;
            // 
            // lblConnStatus
            // 
            this.lblConnStatus.AutoSize = true;
            this.lblConnStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConnStatus.ForeColor = System.Drawing.Color.White;
            this.lblConnStatus.Location = new System.Drawing.Point(7, 4);
            this.lblConnStatus.Name = "lblConnStatus";
            this.lblConnStatus.Size = new System.Drawing.Size(59, 13);
            this.lblConnStatus.TabIndex = 0;
            this.lblConnStatus.Text = "Connected";
            // 
            // FrmTrnPOS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 504);
            this.Controls.Add(this.panel1);
            this.KeyPreview = true;
            this.Name = "FrmTrnPOS";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmTrnPOS";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmTrnPOS_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FrmTrnPOS_KeyPress);
            this.panel1.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.pnlCode.ResumeLayout(false);
            this.pnlCode.PerformLayout();
            this.pnlMainTop1.ResumeLayout(false);
            this.pnlNoTrn.ResumeLayout(false);
            this.pnlNoTrn.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.pnlShift.ResumeLayout(false);
            this.pnlShift.PerformLayout();
            this.PnlPosNo.ResumeLayout(false);
            this.PnlPosNo.PerformLayout();
            this.pnlDate.ResumeLayout(false);
            this.pnlDate.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.PnlRate.ResumeLayout(false);
            this.PnlRate.PerformLayout();
            this.PnlTotal.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            this.PnlConnStatus.ResumeLayout(false);
            this.PnlConnStatus.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel panel15;
        internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Panel pnlCode;
        private System.Windows.Forms.Panel pnlMainTop1;
        private System.Windows.Forms.Panel pnlNoTrn;
        private System.Windows.Forms.Label LblTrnNo;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label LblCashier;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel pnlShift;
        private System.Windows.Forms.Label LblShift;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel PnlPosNo;
        private System.Windows.Forms.Label LblPos;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pnlDate;
        private System.Windows.Forms.Label LblDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel pnlBottom;
        private TenTec.Windows.iGridLib.iGCellStyleDesign iGCellStyleDesign1;
        private TenTec.Windows.iGridLib.iGColHdrStyleDesign iGColHdrStyleDesign1;
        private TenTec.Windows.iGridLib.iGCellStyleDesign iGCellStyleDesign2;
        private TenTec.Windows.iGridLib.iGCellStyleDesign iGCellStyleDesign3;
        private TenTec.Windows.iGridLib.iGCellStyleDesign iGCellStyleDesign4;
        private TenTec.Windows.iGridLib.iGCellStyleDesign iGCellStyleDesign5;
        private TenTec.Windows.iGridLib.iGCellStyleDesign iGCellStyleDesign6;
        private System.Windows.Forms.Panel PnlTotal;
        private System.Windows.Forms.ListBox LbTotalValue;
        private System.Windows.Forms.ListBox LbTotalTitle;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtGrandTotal;
        private System.Windows.Forms.ListBox LbAuditRoll;
        private System.Windows.Forms.Panel PnlRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtBuyRate;
        private System.Windows.Forms.TextBox TxtSellRate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LblUpdateRate;
        private System.Windows.Forms.Panel PnlConnStatus;
        private System.Windows.Forms.Label lblConnStatus;
        internal System.Windows.Forms.Label LblCode;
    }
}