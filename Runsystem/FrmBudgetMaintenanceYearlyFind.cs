﻿#region Update
/* 
    01/02/2021 [VIN/PHT] new apps
    18/05/2021 [VIN/PHT] hanya data header yang ditampilkan
    20/06/2021 [TKG/PHT]
        profit center dan cost center divalidasi berdasarkan otorisasi group terhadap cost center,
        cost center yg dimunculkan adalah cost center yang tidak menjadi parent di cost center yg lain.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetMaintenanceYearlyFind : RunSystem.FrmBase2
    {
        private FrmBudgetMaintenanceYearly mFrmParent;
        string mSQL = string.Empty;

        #region Constructor

        public FrmBudgetMaintenanceYearlyFind(FrmBudgetMaintenanceYearly FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCCCodeFilterByProfitCenter(ref LueCCCode, string.Empty, "N");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT  ");
            SQL.AppendLine("A.DocNo, A.DocDt, A.BudgetRequestDocNo, C.Yr, E.CCName, ");
            SQL.AppendLine("F.BCName, F.LocalCode, '' Mth, ");
            SQL.AppendLine("C.Amt BudgetRequestAmt, A.Amt As Amt,  ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt  ");

            SQL.AppendLine("From tblbudgetmaintenanceyearlyhdr A  ");
            SQL.AppendLine("Inner Join tblbudgetmaintenanceyearlydtl B On A.DocNo=B.DocNo "); 
            SQL.AppendLine("Inner Join tblbudgetrequestyearlyhdr C On A.BudgetRequestDocNo=C.DocNo  ");
            SQL.AppendLine("    And Exists(  ");
            SQL.AppendLine("        Select 1 From tblgroupcostcenter ");
            SQL.AppendLine("        Where CCCode=C.CCCode  ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) "); 
            SQL.AppendLine("    )  ");
            SQL.AppendLine("Left Join tblbudgetrequestyearlydtl D On B.BudgetRequestDocNo=D.DocNo And B.BudgetRequestSeqNo=D.SeqNo  ");
            SQL.AppendLine("Inner Join TblCostCenter E ");
            SQL.AppendLine("    On C.CCCode=E.CCCode ");
            SQL.AppendLine("    And E.ProfitCenterCode Is Not Null ");
            SQL.AppendLine("Left Join TblBudgetCategory F On D.BCCode=F.BCCode  ");
            SQL.AppendLine("Inner Join TblGroupProfitCenter G On E.ProfitCenterCode=G.ProfitCenterCode ");
            SQL.AppendLine("Inner Join TblUser H On G.GrpCode=H.GrpCode And H.UserCode=@UserCode ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2)  ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "Date",
                    "Budget Request#",
                    "Year",
                    "Month",

                    //6-10
                    "Cost Center", 
                    "Category",
                    "Requested",
                    "Budget",
                    "Created By",
                    
                    //11-15
                    "Created Date", 
                    "Created Time", 
                    "Last Updated By", 
                    "Last Updated Date",
                    "Last Updated Time",

                    //16
                    "Local Code"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 150, 80, 80, 
                    
                    //6-10
                    180, 180, 130, 130, 130, 
                    
                    //11-15
                    130, 130, 130, 130, 130,

                    //16
                    130
                }
            );
            Grd1.Cols[16].Move(8);
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 11, 14 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 10, 11, 12, 13, 14, 15 }, false);
            if (mFrmParent.mIsBudget2YearlyFormat)
                Sm.GrdColInvisible(Grd1, new int[] { 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBudgetRequestDocNo.Text, "A.BudgetRequestDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalCode.Text, "F.LocalCode", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "C.CCCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL.ToString() + Filter + " GROUP BY A.DocNo Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",
 
                            //1-5
                            "DocDt", "BudgetRequestDocNo", "Yr", "Mth", "CCName", 
                            
                            //6-10
                            "BCName", "BudgetRequestAmt", "Amt", "CreateBy", "CreateDt", 
                            
                            //11-13
                            "LastUpBy", "LastUpDt", "LocalCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 9 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Budget#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtBudgetRequestDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBudgetRequestDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Budget request#");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCodeFilterByProfitCenter), string.Empty, "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");
        }

        private void TxtLocalCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local Code");
        }

        #endregion

        #endregion
    }
}
