﻿#region Update
/*
    13/10/2017 [TKG] Bug fixing saat query grouping
    16/07/2018 [TKG] tambah group item, department, dan cost center
    07/08/2018 [TKG] berdasarkan parameter mIsRptStockMovement2ShowPORemark, apakah menampilkan remark PO
    13/08/2018 [TKG] tambah informasi entity
    21/11/2018 [WED] tambah created by
    01/04/2019 [HAR] tambah info expedisi (vehiclenumber, vendor) ambil dari DOw Other 
    16/04/2019 [HAR] info expedisi (vehiclenumber, vendor) ambil dari receiving from other stockpile
    13/10/2017 [TKG] menambah informasi price dan amount untuk main currency
    23/03/2020 [TKG/IMS] berdasarkan parameter IsInvTrnShowItSpec, menampilkan specifikasi item
    05/08/2020 [HAR/SRN] bug format grid spesification
    21/12/2020 [TKG/IMS] bug saat menambah createby (karena ada cancel data)
    29/12/2020 [DITA/SRN] ubah kolom QTY menjadi QTY 1, UOM menjadi UOM 1, QTY menjadi QTY2, UOM menjadi UOM 2
    19/01/2021 [VIN/IMS] menampilkan informasi item berdasarkan parameter IsBOMShowSpecifications
    11/03/2021 [TKG/IMS] menambah heat number
    05/07/2021 [TKG/IMS] utk item category yg menggunakan moving average, currency, price, dan rate-nya menggunakan data moving avg
    06/07/2021 [TKG/IMS] perhitungan total utk item category yg menggunakan moving average, currency, price, dan rate-nya menggunakan data moving avg
    13/07/2021 [TKG/IMS] perhitungan MAP berdasarkan informasi MAP di stockmovement
    26/07/2021 [SET/IMS] Untuk type transakai "Do to other warehouse based TR dan Receiving item from whs with DO TR" no document# nya yang muncul adalah doc DO TO OTH WHS PROJECT ( ini terhubung ketika save do to oth whs project maka akan membentul otomatis TR - DO OTH WHS TR - RCV OTH WHS DO TR) di stock movement with price
    29/07/2021 [ICA/IMS] menambah filter multi warehouse
    29/10/2021 [ICA/IMS] untuk document type 01 dan 13 tidak menggunakan MAP
    25/11/2021 [TKG/IMS] untuk data yg diambil dari DOWhs4 (type 26), menggunakan function position untuk mencek posisi ':' , sample : 'abcde:12345'
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;
using System.Text.RegularExpressions;

#endregion

namespace RunSystem
{
    public partial class FrmRptStockMovement2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mMainCurCode = string.Empty;
        private int 
            mNumberOfInventoryUomCode = 1;
        private string
            mRptStockMovementDocNoSource = string.Empty;
        private bool 
            mIsInventoryShowTotalQty = false,
            mIsItGrpCodeShow = false,
            mIsShowForeignName = false,
            mIsInventoryRptFilterByGrpWhs = false,
            mIsFilterByItCt = false,
            mIsRptStockMovement2ShowDODeptInfo = false,
            mIsRptStockMovement2ShowPORemark = false,
            mIsEntityMandatory = false,
            mIsInvTrnShowItSpec = false,
            mIsRptStockMovement2ShowCreateBy = false,
            mIsBOMShowSpecifications = false,
            mIsStockMovement2HeatNumberEnabled = false,
            mIsMovingAvgEnabled = false;

        #endregion

        #region Constructor

        public FrmRptStockMovement2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                GetParameter();
                SetGrd();
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCatCode, mIsFilterByItCt ? "Y" : "N");
                Sl.SetLueTransType(ref LueDocType);
                if (mIsInventoryRptFilterByGrpWhs)
                    Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                else
                    Sl.SetLueWhsCode(ref LueWhsCode);
                SetCcbWhsName(ref CcbWhsName, string.Empty);
                base.FrmLoad(sender, e);
               
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsInventoryShowTotalQty = Sm.GetParameterBoo("IsInventoryShowTotalQty");
            mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsInventoryRptFilterByGrpWhs = Sm.GetParameterBoo("IsInventoryRptFilterByGrpWhs");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsRptStockMovement2ShowDODeptInfo = Sm.GetParameterBoo("IsRptStockMovement2ShowDODeptInfo");
            mIsRptStockMovement2ShowPORemark = Sm.GetParameterBoo("IsRptStockMovement2ShowPORemark");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mIsInvTrnShowItSpec = Sm.GetParameterBoo("IsInvTrnShowItSpec");
            mIsRptStockMovement2ShowCreateBy = Sm.GetParameterBoo("IsRptStockMovement2ShowCreateBy");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsStockMovement2HeatNumberEnabled = Sm.GetParameterBoo("IsStockMovement2HeatNumberEnabled");
            mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");
            mRptStockMovementDocNoSource = Sm.GetParameter("RptStockMovementDocNoSource");
            if (mRptStockMovementDocNoSource.Length == 0) mRptStockMovementDocNoSource = "1";
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        private string SetSQL(string Filter)
        {
            var subSQL = new StringBuilder();
            var SQL = new StringBuilder();

            subSQL.AppendLine("Select A.DocType, A.DocNo, A.DNo, A.DocDt, A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, ");
            subSQL.AppendLine("A.BatchNo, A.Source, Sum(A.Qty) Qty, Sum(A.Qty2) Qty2, Sum(A.Qty3) Qty3 ");
            subSQL.AppendLine("From TblStockMovement A ");
            subSQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            if (mIsFilterByItCt)
            {
                subSQL.AppendLine("And Exists( ");
                subSQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                subSQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                subSQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                subSQL.AppendLine("    ) ");
            }
            if (mIsInventoryRptFilterByGrpWhs)
            {
                subSQL.AppendLine("    And Exists( ");
                subSQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                subSQL.AppendLine("        Where WhsCode=A.WhsCode ");
                subSQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                subSQL.AppendLine("    ) ");
            }
            subSQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            subSQL.AppendLine(Filter);
            subSQL.AppendLine("Group By A.DocType, A.DocNo, A.DNo, A.DocDt, A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Source ");
            subSQL.AppendLine("Having Sum(A.Qty)<>0.00 ");

            SQL.AppendLine("/* Stock Movement 2 */ ");

            SQL.AppendLine("Select T1.DocType, T2.OptDesc, ");
            if (mRptStockMovementDocNoSource == "2")
            {
                SQL.AppendLine("Case When T1.DocType = '26' Then T29.DocNo ");
                SQL.AppendLine("When T1.DocType = '27' Then T31.DocNo ");
                SQL.AppendLine("ELSE T1.DocNo END DocNo, ");
            }
            else
            {
                SQL.AppendLine("T1.DocNo, ");
            }
            SQL.AppendLine("T1.DocDt, T3.WhsName, T1.Lot, T1.Bin, ");
            SQL.AppendLine("T1.ItCode, T4.ItName, T4.ForeignName, T5.PropName, T1.BatchNo, T1.Source, ");
            SQL.AppendLine("T1.Qty, T1.Qty2, T1.Qty3, ");
            SQL.AppendLine("T4.InventoryUomCode, T4.InventoryUomCode2, T4.InventoryUomCode3, ");
            if (mIsMovingAvgEnabled)
            {
                //SQL.AppendLine("Case When T25.MovingAvgInd='Y' Then IfNull(T26.MovingAvgCurCode, 0.00)  Else T6.CurCode End As CurCode, ");
                //SQL.AppendLine("Case When T25.MovingAvgInd='Y' Then IfNull(T26.MovingAvgPrice, 0.00) Else T6.Uprice End As Uprice, ");
                //SQL.AppendLine("Case When T25.MovingAvgInd='Y' Then 1.00 Else T6.ExcRate End As ExcRate, ");

                SQL.AppendLine("Case When T25.MovingAvgInd='Y' Then (Case when T1.DocType IN ('01', '13') then T6.CurCode ELSE IfNull(T26.MovingAvgCurCode, 0.00) END)  Else T6.CurCode End As CurCode, ");
                SQL.AppendLine("Case When T25.MovingAvgInd='Y' Then (Case when T1.DocType IN ('01', '13') then T6.Uprice ELSE IfNull(T26.MovingAvgPrice, 0.00) END)  Else T6.Uprice End As Uprice, ");
                SQL.AppendLine("Case When T25.MovingAvgInd='Y' Then (Case when T1.DocType IN ('01', '13') then T6.ExcRate ELSE 1.00 END)  Else T6.ExcRate End As ExcRate, ");
            }
            else
                SQL.AppendLine("T6.CurCode, T6.UPrice, T6.ExcRate, ");
            if (mIsMovingAvgEnabled)
                //SQL.AppendLine("T1.Qty*Case When T25.MovingAvgInd='Y' Then IfNull(T26.MovingAvgPrice, 0.00) Else T6.Uprice End As Total, ");
                SQL.AppendLine("T1.Qty*Case When T25.MovingAvgInd='Y' Then (Case When T1.DocType IN ('01', '13') then T6.Uprice else IfNull(T26.MovingAvgPrice, 0.00) END) Else T6.Uprice End As Total, ");
            else
                SQL.AppendLine("(T1.Qty*T6.UPrice) As Total, ");
            SQL.AppendLine("Null As FromToName, T4.ItGrpCode, T7.ItGrpName, T8.ItScName, ");
            if (mIsRptStockMovement2ShowCreateBy)
                SQL.AppendLine("T23.CreateBy, ");
            else
                SQL.AppendLine("Null As CreateBy, ");
            SQL.AppendLine("T22.VdName, T22.VdDriver, T22.vdLicenceNo, ");
            if (mIsRptStockMovement2ShowDODeptInfo)
            {
                SQL.AppendLine("Case When T1.DocType In ('05', '14', '30') Then T10.DeptName ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When T1.DocType='18' Then T15.DeptName Else Null End ");
                SQL.AppendLine("End As DeptName, ");
                SQL.AppendLine("Case When T1.DocType In ('05', '14', '30') Then T11.CCName ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When T1.DocType='18' Then T16.CCName Else Null End ");
                SQL.AppendLine("End As CCName, ");
            }
            else
            {
                SQL.AppendLine(" Null As DeptName, Null As CCName, ");
            }
            if (mIsRptStockMovement2ShowPORemark)
                SQL.AppendLine("T18.Remark As PORemark, ");
            else
                SQL.AppendLine("Null As PORemark, ");

            if (mIsEntityMandatory)
                SQL.AppendLine("T21.EntName, ");
            else
                SQL.AppendLine("Null As EntName, ");
            if (mIsMovingAvgEnabled)
            {
                //SQL.AppendLine("Case When T25.MovingAvgInd='Y' Then IfNull(T26.MovingAvgPrice, 0.00) Else T6.Uprice End * ");
                //SQL.AppendLine("Case When T25.MovingAvgInd='Y' Then 1.00 Else T6.ExcRate End As UPriceMain, ");

                SQL.AppendLine("Case When T25.MovingAvgInd='Y' Then (Case when T1.DocType IN ('01', '13') then T6.Uprice else IfNull(T26.MovingAvgPrice, 0.00) End) Else T6.Uprice End * ");
                SQL.AppendLine("Case When T25.MovingAvgInd='Y' Then (Case when T1.DocType IN ('01', '13') then T6.ExcRate else 1.00 End) Else T6.ExcRate End As UPriceMain, ");
            }
            else
                SQL.AppendLine("(T6.UPrice*T6.ExcRate) As UPriceMain, ");
            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("T1.Qty*");
                //SQL.AppendLine("Case When T25.MovingAvgInd='Y' Then IfNull(T26.MovingAvgPrice, 0.00) Else T6.Uprice End*");
                //SQL.AppendLine("Case When T25.MovingAvgInd='Y' Then 1.00 Else T6.ExcRate End As TotalMain, ");

                SQL.AppendLine("Case When T25.MovingAvgInd='Y' Then (Case when T1.DocType IN ('01', '13') then T6.Uprice else IfNull(T26.MovingAvgPrice, 0.00) End) Else T6.Uprice End* ");
                SQL.AppendLine("Case When T25.MovingAvgInd='Y' Then (Case when T1.DocType IN ('01', '13') then T6.ExcRate else 1.00 End) Else T6.ExcRate End As TotalMain, ");
            }
            else
                SQL.AppendLine("(T1.Qty*T6.UPrice*T6.ExcRate) As TotalMain, ");
            SQL.AppendLine("T4.Specification, T4.ItCodeInternal ");
            if (mIsStockMovement2HeatNumberEnabled)
                SQL.AppendLine(", T24.Value2 As HeatNumber ");
            else
                SQL.AppendLine(", Null As HeatNumber ");
            SQL.AppendLine("From ( " + subSQL + ") T1 ");
            SQL.AppendLine("Inner Join TblOption T2 On T1.DocType = T2.OptCode And T2.OptCat='InventoryTransType' ");
            SQL.AppendLine("Inner Join TblWarehouse T3 On T1.WhsCode = T3.WhsCode ");
            SQL.AppendLine("Inner Join TblItem T4 On T1.ItCode = T4.ItCode  ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=T4.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblProperty T5 On T1.PropCode = T5.PropCode ");
            SQL.AppendLine("Inner Join TblStockPrice T6 On T1.Source=T6.Source ");
            SQL.AppendLine("Left Join TblItemGroup T7 On T4.ItGrpCode = T7.ItGrpCode ");
            SQL.AppendLine("Left Join TblItemSubCategory T8 On T4.ItScCode = T8.ItScCode ");
            if (mIsRptStockMovement2ShowDODeptInfo)
            {
                //05, 14, 30, 18
                SQL.AppendLine("Left Join TblDODeptHdr T9 On T1.DocType In ('05', '14', '30') And T1.DocNo=T9.DocNo ");
                SQL.AppendLine("Left Join TblDepartment T10 On T9.DeptCode=T10.DeptCode ");
                SQL.AppendLine("Left Join TblCostCenter T11 On T9.CCCode=T11.CCCode ");
                SQL.AppendLine("Left Join TblRecvDeptHdr T12 On T1.DocType In ('18') And T1.DocNo=T12.DocNo ");
                SQL.AppendLine("Left Join TblRecvDeptDtl T13 On T1.DocNo=T13.DocNo And T1.DNo=T13.DNo ");
                SQL.AppendLine("Left Join TblDODeptHdr T14 On T13.DODeptDocNo=T14.DocNo ");
                SQL.AppendLine("Left Join TblDepartment T15 On T12.DeptCode=T15.DeptCode ");
                SQL.AppendLine("Left Join TblCostCenter T16 On T14.CCCode=T16.CCCode ");
            }
            if (mIsRptStockMovement2ShowPORemark)
            {
                SQL.AppendLine("Left Join TblRecvVdDtl T17 On T1.Source=T17.Source ");
                SQL.AppendLine("Left Join TblPODtl T18 On T17.PODocNo=T18.DocNo And T17.PODNo=T18.DNo ");
            }

            if (mIsEntityMandatory)
            {
                SQL.AppendLine("Left Join TblCostCenter T19 On T3.CCCode=T19.CCCode ");
                SQL.AppendLine("Left Join TblProfitCenter T20 On T19.ProfitCenterCode=T20.ProfitCenterCode ");
                SQL.AppendLine("Left Join TblEntity T21 On T20.EntCode=T21.EntCode ");
            }
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.DocNO, A.ItCode, group_concat(distinct B.Vdname) As Vdname, ");
            SQL.AppendLine("    group_concat(distinct vdLicenceNo) As vdLicenceNo, ");
            SQL.AppendLine("    group_concat(distinct Driver) As vdDriver ");
            SQL.AppendLine("    from TblRecvWhs2Dtl2 A ");
            SQL.AppendLine("    Inner Join Tblvendor B On A.VdCode = B.VdCode  ");
            SQL.AppendLine("    group By Docno, itCode ");
            SQL.AppendLine(")T22 On T1.DocNo = T22.DocNo And T1.ItCode = T22.ItCode ");

            if (mIsRptStockMovement2ShowCreateBy) SQL.AppendLine("Left Join TblStockMovement T23 On T1.DocType=T23.DocType And T1.DocNo=T23.DocNo And T1.DNo=T23.DNo And T23.CancelInd='N' ");
            if (mIsStockMovement2HeatNumberEnabled) SQL.AppendLine("Left Join TblSourceInfo T24 On T1.Source=T24.Source ");
            SQL.AppendLine("Inner Join TblItemCategory T25 On T4.ItCtCode=T25.ItCtCode  ");
            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("Left Join TblStockMovement T26 On T1.DocType=T26.DocType And T1.DocNo=T26.DocNo And T1.DNo=T26.DNo And T26.CancelInd='N' ");
            }
            if (mRptStockMovementDocNoSource == "2")
            {
                SQL.AppendLine("Left Join tbldowhsdtl2 T28 ");
                SQL.AppendLine("    On T1.DocType = '26'  ");
                SQL.AppendLine("    And Case When Position(':' In T1.DocNo)=0 Then T1.DocNo Else Left(T1.DocNo, Position(':' In T1.DocNo)-1) End= T28.DocNo ");
                SQL.AppendLine("    And T1.DNo=T28.DNo ");
                SQL.AppendLine("Left Join tbldowhs4dtl T29 ON T28.DocNo = T29.DOWhsDocNo AND T28.DNo = T29.DOWhsDNo ");
                SQL.AppendLine("Left Join tblrecvwhs4dtl T30 ON T1.DocType = '27' AND T1.DocNo = T30.DocNo AND T1.DNo = T30.DNo ");
                SQL.AppendLine("Left Join tbldowhs4dtl T31 ON T30.DoWhsDocNo = T31.DOWhsDocNo AND T30.DoWhsDNo = T31.DOWhsDNo ");
            }
            SQL.AppendLine("Order By T1.DocNo;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 40;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Type",   
                        "Document#",
                        "Date",
                        "From/To",
                        "Warehouse",
                        
                        //6-10
                        "Lot",
                        "Bin",
                        "Item's Code",
                        "Item's Name",
                        "Foreign Name",

                        //11-15
                        "Property",
                        "Batch#",
                        "Source",
                        "Quantity", 
                        "UoM",                     
                        
                        //16-20
                        "Quantity", 
                        "UoM",
                        "Quantity", 
                        "UoM",
                        "Currency",

                        //21-25
                        "Price",
                        "Rate",
                        "Total", 
                        "Item's Group Code",
                        "Item's Group Name",

                        //26-30
                        "Sub-Category",
                        "DO Dept's Department",
                        "DO Dept's Cost Center",
                        "PO's Remark",
                        "Entity",

                        //31-35
                        "Created By",
                        "Vendor",
                        "Vehicle No",
                        "Driver",
                        "Price" + Environment.NewLine + mMainCurCode,

                        //36-39
                        "Total" + Environment.NewLine + mMainCurCode,
                        "Specification", 
                        "Item's"+Environment.NewLine+"Local Code",
                        "Heat Number"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        220, 140, 80, 200, 180,
                        
                        //6-10
                        60, 80, 80, 200, 230,
                        
                        //11-15
                        80, 200, 180, 80, 80,

                        //16-20
                        80, 80, 80, 80, 70,

                        //21-25
                        100, 100, 150, 130, 200,

                        //26-30
                        150, 200, 200, 300, 150,

                        //31-35
                        200, 120, 100, 120, 130,

                        //36-39
                        130, 200, 100, 200
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 16, 18, 21, 22, 23, 35, 36 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 8, 13, 16, 17, 18, 19, 22, 26, 37 }, false);
            if (!mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 10 }, false);
            if (!mIsItGrpCodeShow) Sm.GrdColInvisible(Grd1, new int[] { 24, 25 }, false);
            if (!mIsRptStockMovement2ShowDODeptInfo) Sm.GrdColInvisible(Grd1, new int[] { 27, 28 }, false);
            if (!mIsRptStockMovement2ShowPORemark) Sm.GrdColInvisible(Grd1, new int[] { 29 }, false);
            if (!mIsRptStockMovement2ShowCreateBy) Sm.GrdColInvisible(Grd1, new int[] { 31 }, false);
            Grd1.Cols[24].Move(11);
            Grd1.Cols[25].Move(12);
            Grd1.Cols[26].Move(13);
            Grd1.Cols[35].Move(27);
            Grd1.Cols[36].Move(28);
            if (mIsEntityMandatory)
                Grd1.Cols[30].Move(6);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 30 }, false);

            if (mIsInvTrnShowItSpec)
            {
                Grd1.Cols[37].Move(10);
                Sm.GrdColInvisible(Grd1, new int[] { 37 }, false);
            }
            if (mIsBOMShowSpecifications)
            {
                Grd1.Cols[38].Move(9);
                Sm.GrdColInvisible(Grd1, new int[] { 37, 38 }, true);
            }
            if (mIsStockMovement2HeatNumberEnabled)
                Grd1.Cols[39].Move(19);
            else
                Grd1.Cols[39].Visible = false;
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);       
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 13, 26 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                if (ChkWhsName.Checked && ChkWhsCode.Checked)
                {
                    Sm.StdMsg(mMsgType.Warning, "Please just use Multi Warehouse or Warehouse filter, instead of using both of them.");
                    CcbWhsName.Focus();
                    return;
                }

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                //Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCatCode), "B.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "B.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "A.DocType", true);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "A.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "A.Bin", false);
                if (ChkWhsName.Checked)
                    FilterStr(ref Filter, ref cm, ProcessCcb(Sm.GetCcb(CcbWhsName)), "A.WhsCode", "_2");
                else
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SetSQL(Filter),
                new string[]
                    {
                        //0
                        "OptDesc", 

                        //1-5
                        "DocNo", "DocDt", "FromToName", "WhsName", "Lot", 
                        
                        //6-10
                        "Bin", "ItCode", "ItName", "ForeignName", "PropName",

                        //11-15
                        "BatchNo", "Source", "Qty", "InventoryUomCode", "Qty2",
                        
                        //16-20
                        "InventoryUOMCode2", "Qty3", "InventoryUOMCode3", "CurCode", "UPrice",

                        //21-25
                        "ExcRate", "Total", "ItGrpCode", "ItGrpName", "ItScName", 
                        
                        //26-30
                        "DeptName", "CCName", "PORemark", "EntName", "CreateBy",

                        //31-35
                        "VdName", "VdDriver", "vdLicenceNo", "UPriceMain", "TotalMain",

                        //36-38
                        "Specification", "ItCodeInternal", "HeatNumber"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdVal("D", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdVal("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                    Sm.SetGrdVal("N", Grd, dr, c, Row, 21, 20);
                    Sm.SetGrdVal("N", Grd, dr, c, Row, 22, 21);
                    Sm.SetGrdVal("N", Grd, dr, c, Row, 23, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 30);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 31);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 32);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 33);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 34);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 35);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 36);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 37);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 38);
                }, true, false, false, false
                );
                if (ChkShowTotal.Checked)
                {
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 14, 16, 18, 23, 36 });
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }
       
        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void SetCcbWhsName(ref CheckedComboBoxEdit Ccb, string WhsName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.WhsName As Col From TblWarehouse T ");
            SQL.AppendLine("Where 0 = 0 ");
            if (WhsName.Length > 0)
            {
                SQL.AppendLine("And T.WhsName = @WhsName ");
            }
            else
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("    Where WhsCode=T.WhsCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By T.WhsName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsName", WhsName);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetCcb(ref Ccb, cm);
        }

        private string ProcessCcb(string Value)
        {
            if (Value.Length != 0)
            {
                Value = GetWhsCode(Value);
                Value = "#" + Value.Replace(", ", "# #") + "#";
                Value = Value.Replace("#", @"""");
            }
            return Value;
        }

        private string GetWhsCode(string Value)
        {
            if (Value.Length != 0)
            {
                Value = Value.Replace(", ", ",");

                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(T.WhsCode Separator ', ') WhsCode ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select WhsCode ");
                SQL.AppendLine("    From TblWarehouse ");
                SQL.AppendLine("    Where Find_In_Set(WhsName, @Param) ");
                SQL.AppendLine(")T; ");

                Value = Sm.GetValue(SQL.ToString(), Value);
            }

            return Value;
        }

        private void FilterStr(ref string SQL, ref MySqlCommand cm, string Filter, string Column, string Param)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string pattern = @"(""[^""]+""|\w+)\s*", SQL2 = "";
                MatchCollection mc = null;
                var group = new List<string>();
                int Index = 0;

                string Column2 = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));

                if (Filter.IndexOf(@"""") < 0) Filter = @"""" + Filter + @"""";

                mc = Regex.Matches(Filter, pattern);

                group.Clear();

                foreach (Match m in mc)
                    group.Add(m.Groups[0].Value.Replace(@"""", "").Trim());

                Index = 0;
                foreach (string s in group)
                {
                    if (s.Length != 0)
                    {
                        Index += 1;
                        SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + Column + ") Like @" + Column2 + Param + Index.ToString();
                        //Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), "%" + s + "%");
                        Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), s);
                    }
                }
                if (SQL2.Length != 0) SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void ChkItCatCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsInventoryRptFilterByGrpWhs)
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            else
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(Sl.SetLueTransType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueItCatCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCatCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void CcbWhsName_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkWhsName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Warehouse");
        }

        #endregion
    }
}
