﻿ #region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSalesOrderBasedLocation : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptSalesOrderBasedLocation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                SetGrd();
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueCityCode(ref LueCityCode);
                SetLueStatus();
                Sm.SetLue(LueStatus, "OP");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocNo, T1.DocDt, T5.CtName, ");
            SQL.AppendLine("Case T1.Status ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'P' Then 'Partial Fulfilled' ");
            SQL.AppendLine("    When 'M' Then 'Manual Fulfilled' ");
            SQL.AppendLine("    When 'F' Then 'Fulfilled' ");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("T2.DNo, T8.ItCode, T8.ItName, T8.ItCodeInternal, T9.PriceUomCode As SalesUomCode, T8.InventoryUomCode, ");
            SQL.AppendLine("T2.QtyPackagingUnit, T2.PackagingUnitUomCode, T2.Qty As SOQty, ");
            SQL.AppendLine("IfNull(T3.Qty, 0) As DRQty, ");
            SQL.AppendLine("IfNull(T10.Qty, 0) As PLQty, ");
            SQL.AppendLine("IfNull(T3.QtyInventory, 0) As DRQtyInventory, ");
            SQL.AppendLine("IfNull(T10.QtyInventory, 0) As PLQtyInventory, ");
            SQL.AppendLine("IfNull(T4.Qty, 0) As DOQtyInventory, ");
            SQL.AppendLine("Case When OverseaInd='N' Then ");
            SQL.AppendLine("    Case When IfNull(T3.QtyInventory, 0)=0 Then 0 ");
            SQL.AppendLine("    Else (IfNull(T4.Qty, 0)/IfNull(T3.QtyInventory, 0))*IfNull(T3.Qty, 0) ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When IfNull(T10.QtyInventory, 0)=0 Then 0 ");
            SQL.AppendLine("    Else (IfNull(T4.Qty, 0)/IfNull(T10.QtyInventory, 0))*IfNull(T10.Qty, 0) ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("End As DOQty, ");
            SQL.AppendLine("T2.DNo, T1.OverseaInd, T1.SAAddress, T12.CntName, T11.CityName ");
            SQL.AppendLine("From TblSOHdr T1 ");
            SQL.AppendLine("Inner Join TblSODtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select B.SODocNo, B.SODNo, ");
            SQL.AppendLine("    Sum(B.Qty) As Qty, Sum(B.QtyInventory) As QtyInventory ");
            SQL.AppendLine("    From TblDRHdr A ");
            SQL.AppendLine("    Inner Join TblDRDtl B On A.DocNo=B.DocNo ");
            if (Filter2.Length != 0)
                SQL.AppendLine(Filter2 + " On B.SODocNo=Tbl.DocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    Group By B.SODocNo, B.SODNo ");
            SQL.AppendLine(") T3 On T1.DocNo=T3.SODocNo And T2.DNo=T3.SODNo ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("Select X.SODocNo, X.SoDno, X.QtyDO As Qty ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select '1' As DR, C.SODocNo, C.SODNo, Sum(B.Qty) As QtyDO");
            SQL.AppendLine("    From TblDOCt2Hdr A");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo");
            SQL.AppendLine("    Left Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo");
            SQL.AppendLine("    Group By C.SODocNo, C.SODNo");
            SQL.AppendLine("    Union All");
            SQL.AppendLine("    Select '2' As PL, C.SODocNo, C.SODNo, Sum(B.Qty) As QtyDO ");
            SQL.AppendLine("    From TblDOCt2Hdr A ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Left Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PlDno = C.Dno ");
            SQL.AppendLine("    Left Join TblPlHdr D On C.DocNo = D.Docno  ");
            SQL.AppendLine("    Left Join TblSIHdr E On D.SiDocno = E.DocNo  ");
            SQL.AppendLine("    Left Join TblSP F On E.SpDocNo = F.DocNo And F.Status <> 'C'  ");
            SQL.AppendLine("    Group By C.SODocNo, C.SODNo");
            SQL.AppendLine("    ) X ");
            SQL.AppendLine("Group By  X.SODocNo, X.SoDno ");
            SQL.AppendLine(") T4 On T1.DocNo=T4.SODocNo And T2.DNo=T4.SODNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select B.SODocNo, B.SoDno, Sum(B.Qty) As Qty, Sum(B.QtyInventory) As QtyInventory   ");
            SQL.AppendLine("    From TblPlhdr A ");
            SQL.AppendLine("    Inner Join TblPlDtl B On B.DocNo = A.DocNo ");
            SQL.AppendLine("    Left Join TblSODtl D On B.SODocno = D.DocNo And B.SODno = D.Dno ");
            SQL.AppendLine("    Left Join TblSOhdr E On D.DocNo = E.DocNo And E.CancelInd='N' And E.Status In ('O', 'P') ");
            SQL.AppendLine("    Left Join TblSiHdr F on F.DocNo = A.SiDocNo ");
            SQL.AppendLine("    Left Join TblSP G On F.SpDocNo = G.Docno And G.Status<>'C' ");
            SQL.AppendLine("    Group By B.SODocNo, B.SODNo ");
            SQL.AppendLine(") T10 On T1.DocNo=T10.SODocNo And T2.DNo=T10.SODNo ");

            SQL.AppendLine("Inner Join TblCustomer T5 On T1.CtCode=T5.CtCode ");
            SQL.AppendLine("Inner Join TblCtQtDtl T6 On T1.CtQtDocNo=T6.DocNo And T2.CtQtDNo=T6.DNo ");
            SQL.AppendLine("Inner Join TblItemPriceDtl T7 On T6.ItemPriceDocNo=T7.DocNo And T6.ItemPriceDNo=T7.DNo ");
            SQL.AppendLine("Inner Join TblItem T8 On T7.ItCode=T8.ItCode ");
            SQL.AppendLine("Inner Join TblItemPriceHdr T9 On T6.ItemPriceDocNo = T9.DocNo ");
            SQL.AppendLine("Left Join TblCity T11 On T1.SACityCode = T11.CityCode ");
            SQL.AppendLine("Left Join TblCountry T12 on T1.SACntCode = T12.CntCode " + Filter);
            SQL.AppendLine("Order By T1.DocDt, T1.DocNo, T2.DNo; ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Status",
                        "Customer",
                        "Item Code",

                        //6-10
                        "",
                        "Local Code",
                        "Item Name",
                        "SO" + Environment.NewLine + "(Packaging)",
                        "UoM" + Environment.NewLine + "(Packaging)",
                        
                        //11-15
                        "SO" + Environment.NewLine + "(Sales)",
                        "DR" + Environment.NewLine + "(Sales)",
                        "PL" + Environment.NewLine + "(Sales)",
                        "DO" + Environment.NewLine + "(Sales)",
                        "UoM" + Environment.NewLine + "(Sales)",
                        
                        //16-20
                        "DR" + Environment.NewLine + "(Inventory)",
                        "PL" + Environment.NewLine + "(Inventory)",
                        "DO" + Environment.NewLine + "(Inventory)",
                        "UoM" + Environment.NewLine + "(Inventory)",
                        "DNo",

                        //21-25
                        "Oversea",
                        "",
                        "Shipping Address",
                        "Country",
                        "City"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 120, 250, 80, 

                        //6-10
                        20, 80, 250, 100, 100, 
                        
                        //11-15
                        100, 100, 100, 100, 80,  

                        //16-20
                        100, 100, 100, 80, 60,

                        //21-22
                        80, 20, 200, 100, 120
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 21 });
            Sm.GrdColButton(Grd1, new int[] { 6, 22 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11, 12, 13, 14, 16, 17, 18 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 12, 13, 16, 17, 20, 21, 23 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[23].Move(5);
            Grd1.Cols[24].Move(6);
            Grd1.Cols[25].Move(7);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 12, 13, 16, 17, 23 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where T1.CancelInd='N' ", Filter2 = string.Empty;
                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "T1.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "T1.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCityCode), "T1.SACityCode", true);

                switch (Sm.GetLue(LueStatus))
                {
                    case "OP":
                        Filter += " And T1.Status In ('O', 'P') ";
                        break;
                    case "MF":
                        Filter += " And T1.Status In ('M', 'F') ";
                        break;
                    default:
                        Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueStatus), "T1.Status", true);
                        break;
                }

                Filter2 = 
                    " Inner Join (Select T1.DocNo From TblSOHdr T1 " + Filter + ") Tbl ";
                
                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                GetSQL(Filter, Filter2),
                new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "StatusDesc", "CtName", "ItCode", "ItCodeInternal",

                        //6-10
                        "ItName", "QtyPackagingUnit", "PackagingUnitUomCode", "SOQty", "DRQty",
 
                        //11-15
                        "PLQty", "DOQty", "SalesUomCode", "DRQtyInventory", "PLQtyInventory",  
                        
                        //16-20
                        "DOQtyInventory", "InventoryUomCode", "DNo", "OverseaInd", "SAAddress",

                        //21-22
                        "CntName", "CityName"
                    },
                (
                    MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 5));

            if (e.ColIndex == 22 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                ShowDOCtInfo(e.RowIndex);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 5));

            if (e.ColIndex == 22 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                ShowDOCtInfo(e.RowIndex);
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetLueStatus()
        {
            Sm.SetLue2(
                ref LueStatus,
                "Select 'O' As Col1, 'Outstanding' As Col2 " +
                "Union All Select 'P' As Col1, 'Partial Fulfilled' As Col2 " +
                "Union All Select 'M' As Col1, 'Manual Fulfilled' As Col2 " +
                "Union All Select 'F' As Col1, 'Fulfilled' As Col2 " +
                "Union All Select 'MF' As Col1, 'Manual Fulfilled+Fulfilled' As Col2 " +
                "Union All Select 'OP' As Col1, 'Outstanding+Partial' As Col2;",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void ShowDOCtInfo(int row)
        {
            string
                DocNo = Sm.GetGrdStr(Grd1, row, 1),
                DNo = Sm.GetGrdStr(Grd1, row, 20),
                Msg = string.Empty; 

            try
            {
                if (Sm.GetGrdBool(Grd1, row, 21))
                    Msg = GetDOCtOversea(DocNo, DNo);
                else
                    Msg = GetDOCtLocal(DocNo, DNo);

                if (Msg.Length == 0)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    Sm.StdMsg(mMsgType.Info, Msg);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetDOCtLocal(string DocNo, string DNo)
        {
            StringBuilder SQL = new StringBuilder(), Msg = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, Sum(B.Qty) As Qty ");
            SQL.AppendLine("From TblDOCt2Hdr A ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo And B.Qty<>0 ");
            SQL.AppendLine("Inner Join TblDRDtl C ");
            SQL.AppendLine("    On A.DRDocNo=C.DocNo ");
            SQL.AppendLine("    And B.DRDNo=C.DNo ");
            SQL.AppendLine("    And C.SODocNo=@SODocNo ");
            SQL.AppendLine("    And C.SODNo=@SODNo ");
            SQL.AppendLine("Group By A.DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SODocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SODNo", DNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]{ "DocNo", "Qty" },
               (MySqlDataReader dr, int[] c) =>
               {
                   if (Sm.DrStr(dr, c[0]).Length != 0)
                   {
                       Msg.Append("DO# : " + Sm.DrStr(dr, c[0]));
                       Msg.AppendLine(" ( Qty : " + Sm.FormatNum(Sm.DrDec(dr, c[1]), 0) + " )");
                   }
               }, false
            );
            return Msg.ToString();
        }

        private string GetDOCtOversea(string DocNo, string DNo)
        {
            StringBuilder SQL = new StringBuilder(), Msg = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, Sum(B.Qty) As Qty ");
            SQL.AppendLine("From TblDOCt2Hdr A ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo And B.Qty<>0 ");
            SQL.AppendLine("Inner Join TblPLDtl C ");
            SQL.AppendLine("    On A.PLDocNo=C.DocNo ");
            SQL.AppendLine("    And B.PLDNo=C.DNo ");
            SQL.AppendLine("    And C.SODocNo=@SODocNo ");
            SQL.AppendLine("    And C.SODNo=@SODNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SODocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SODNo", DNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] { "DocNo", "Qty" },
               (MySqlDataReader dr, int[] c) =>
               {
                   if (Sm.DrStr(dr, c[0]).Length != 0)
                   {
                       Msg.Append("DO# : " + Sm.DrStr(dr, c[0]));
                       Msg.AppendLine(" ( Qty : " + Sm.FormatNum(Sm.DrDec(dr, c[1]), 0) + " )");
                   }
               }, false
            );
            return Msg.ToString();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueStatus), "<Refresh>")) LueStatus.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        private void ChkCityCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "City");
        }

        private void LueCityCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue1(Sl.SetLueCityCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion

       

        #endregion

       
    }
}
