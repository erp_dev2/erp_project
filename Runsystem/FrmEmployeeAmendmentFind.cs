﻿#region Update
/*
    22/12/2017 [TKG] tambah filter dept+site berdasarkan group
    11/07/2018 [HAR] filter employee old code
    15/11/2019 [DITA/IOK] bug created time tidak muncul
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmEmployeeAmendmentFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmEmployeeAmendment mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmEmployeeAmendmentFind(FrmEmployeeAmendment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.EmpCode, B.EmpCodeOld, B.EmpName, ");
            SQL.AppendLine("C.DeptName As DeptNameOld, G.DeptName, ");
            SQL.AppendLine("D.PosName As PosNameOld, H.PosName, ");
            SQL.AppendLine("E.GrdLvlName As GrdLvlNameOld, I.GrdLvlName, ");
            SQL.AppendLine("F.SiteName As SiteNameOld, J.SiteName, ");
            SQL.AppendLine("A.ResignDt, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblEmployeeAmendment A ");
            if (mFrmParent.mIsFilterByDeptHR || mFrmParent.mIsFilterBySiteHR)
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            else
                SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("And B.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=B.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("And B.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCodeOld = C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On A.PosCodeOld = D.PosCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On A.GrdLvlCodeOld = E.GrdLvlCode ");
            SQL.AppendLine("Left Join TblSite F On A.SiteCodeOld = F.SiteCode ");
            SQL.AppendLine("Left Join TblDepartment G On A.DeptCode = G.DeptCode ");
            SQL.AppendLine("Left Join TblPosition H On A.PosCode  = H.PosCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr I On A.GrdLvlCode = I.GrdLvlCode ");
            SQL.AppendLine("Left Join TblSite J On A.SiteCode = J.SiteCode ");
            SQL.AppendLine("Where 0=0 ");
            
            //if (!mFrmParent.mIsNotFilterByAuthorization)
            //{
            //    SQL.AppendLine("And Exists( ");
            //    SQL.AppendLine("    Select EmpCode From TblEmployee ");
            //    SQL.AppendLine("    Where EmpCode=A.EmpCode ");
            //    SQL.AppendLine("    And GrdLvlCode In ( ");
            //    SQL.AppendLine("        Select T2.GrdLvlCode ");
            //    SQL.AppendLine("        From TblPPAHdr T1 ");
            //    SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
            //    SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
            //    SQL.AppendLine("    ) ");
            //    SQL.AppendLine(") ");
            //}

            mSQL = SQL.ToString();
        }
        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Employee's"+Environment.NewLine+"Code", 
                        "Employee's"+Environment.NewLine+"Old Code", 
                        "Employee's"+Environment.NewLine+"Name",

                        //6-10
                        "Department"+Environment.NewLine+"(Previous)",
                        "Department"+Environment.NewLine+"(New)",
                        "Position"+Environment.NewLine+"(Previous)",
                        "Position"+Environment.NewLine+"(New)",
                        "Grade Level"+Environment.NewLine+"(Previous)",

                        //11-15
                        "Grade Level"+Environment.NewLine+"(New)",
                        "Site"+Environment.NewLine+"(Previous)",
                        "Site"+Environment.NewLine+"(New)",
                        "Resign Date",
                        "Created"+Environment.NewLine+"By",
                        
                        //16-20
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 100, 100, 100, 250, 
                        
                        //6-10
                         150, 150, 150, 150, 150, 

                        //11-15
                        150, 150, 150, 100, 100, 
                        
                        //16-20
                        100, 100, 100, 100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 14, 16, 19 });
            Sm.GrdFormatTime(Grd1, new int[] { 17, 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 15, 16, 17, 18, 19, 20 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 15, 16, 17, 18, 19, 20 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "B.EmpName", "B.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By B.EmpName;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            //1-5
                            "DocDt", "EmpCode", "EmpCodeOld", "EmpName", "DeptNameOld",  
                            
                            //6-10
                            "DeptName", "PosNameOld", "PosName", "GrdLvlNameOld", "GrdLvlName",  
                            
                            //11-15
                            "SiteNameOld", "SiteName", "ResignDt", "CreateBy", "CreateDt", 
                            
                            //16
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 20, 17);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        #endregion
    }
}
