﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptItemStockSummary : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        private string[] mCols = null;
        private bool mIsFilterByItCt = false, mIsShowForeignName = false;
        
        #endregion

        #region Constructor

        public FrmRptItemStockSummary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetCols();
                SetGrd();
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt ? "Y" : "N");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);

            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        private string SetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ActInd, ifnull(A.ItCodeInternal, '') As ItCodeInternal, A.ItName, C.ItCtName, IfNull(B.Qty, 0) As Qty, A.InventoryUomCode ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine(", IfNull(B.Qty2, 0) As Qty2, A.InventoryUomCode2 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine(", IfNull(B.Qty3, 0) As Qty3, A.InventoryUomCode3 ");
            SQL.AppendLine(", ifnull(C.QtyCommitted, 0) As QtyCommitted, A.ActInd, A.ForeignName ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.ItCode, ");
            SQL.AppendLine("    Sum(T1.Qty) As Qty ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , Sum(T1.Qty2) As Qty2 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , Sum(T1.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockSummary T1 ");
            SQL.AppendLine("    Inner Join TblItem T2 On T1.ItCode=T2.ItCode " + Filter.Replace("A.", "T2."));
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=T2.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Where (T1.Qty<>0 ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    Or T1.Qty2<>0 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    Or T1.Qty3<>0 ");
            SQL.AppendLine("    ) Group By T1.ItCode ");
            SQL.AppendLine(") B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory C On A.ItCtCode=C.ItCtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select B.ItCode, SUm(B.Qty) As QtyCommitted ");
            SQL.AppendLine("    From TblDowhsHdr A ");
            SQL.AppendLine("    inner Join TblDOWhsDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Where B.ProcessInd = 'O' And B.CancelInd = 'N'  ");
            SQL.AppendLine("    Group By B.ItCode ");
            SQL.AppendLine(")C On A.ItCode = C.ItCode ");
            SQL.AppendLine("Where ((A.ActInd='N' And B.Qty>0) Or (A.ActInd='Y' Or B.Qty>=0)) ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine(Filter);
            SQL.AppendLine(" Order By A.ItName;");

            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5  
                        "Item's Code",
                        "Local Code",
                        "Item's Name",
                        "Category",
                        "Quantity",
                        
                        //6-10
                        "UoM",                        
                        "Quantity", 
                        "UoM",
                        "Quantity", 
                        "UoM",

                        //11-13
                        "Committed",
                        "Active",
                        "Foreign Name"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 110, 300, 200, 100, 
                        
                        //6-10
                        80, 100, 80, 100, 80,
 
                        //11-13
                        80, 80, 170
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 7, 9, 11 }, 0);
            if (mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 7, 8, 9, 10 }, false);
                Grd1.Cols[13].Move(4);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 7, 8, 9, 10, 13 }, false);
            }
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 7, 8 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10 }, true);
        }

        private void SetCols()
        {
            if (mNumberOfInventoryUomCode == 1)
                mCols = new string[] { "ItCode", "ItCodeInternal", "ItName", "ItCtName", "Qty", "InventoryUomCode", "QtyCommitted", "ActInd", "ForeignName" };

            if (mNumberOfInventoryUomCode == 2)
                mCols = new string[] { "ItCode", "ItCodeInternal", "ItName", "ItCtName", "Qty", "InventoryUomCode", "Qty2", "InventoryUOMCode2", "QtyCommitted", "ActInd", "ForeignName" };

            if (mNumberOfInventoryUomCode == 3)
                mCols = new string[] { "ItCode", "ItCodeInternal", "ItName", "ItCtName", "Qty", "InventoryUomCode", "Qty2", "InventoryUOMCode2", "Qty3", "InventoryUOMCode3", "QtyCommitted", "ActInd", "ForeignName" };

        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName", "A.ForeignName" });
                
                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SetSQL(Filter),
                mCols,
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdVal("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdVal("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdVal("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdVal("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdVal("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdVal("S", Grd, dr, c, Row, 6, 5);
                        if (mNumberOfInventoryUomCode >= 1)
                        {
                            Sm.SetGrdVal("N", Grd, dr, c, Row, 11, 6);
                            Sm.SetGrdVal("B", Grd, dr, c, Row, 12, 7);
                            Sm.SetGrdVal("S", Grd, dr, c, Row, 13, 8);
                        }
                        if (mNumberOfInventoryUomCode >= 2)
                        {
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        }
                        if (mNumberOfInventoryUomCode >= 3)
                        {
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        }
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        #endregion

        #endregion
    }
}
