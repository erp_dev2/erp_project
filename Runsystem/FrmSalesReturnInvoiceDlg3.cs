﻿#region Update
/*
    19/10/2022 [MAU/PRODUCT] tambah dialog rujukan DO To Customer
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesReturnInvoiceDlg3 : RunSystem.FrmBase4
    {
        #region Field
        private FrmSalesReturnInvoice mFrmParent;
        string mSQL = string.Empty;
        private string mMenuCode = string.Empty;

        #endregion

        #region Constructor


        public FrmSalesReturnInvoiceDlg3(FrmSalesReturnInvoice FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = "List Of DO To Customer";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            BtnChoose.Visible = false; 
            SetGrd();
            SetSQL();
            ShowData();
            // Sl.SetLueDeptCode(ref LueDeptCode);
        }
        #endregion

        #region Standard method

        //SetGrd
        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            //Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, 
                    new string[]
                    {
                        //0
                        "No",

                        //1-2
                        "DO To Customer#", ""

                    }, new int[]
                    {
                        50, 300, 20
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1 });
            Sm.GrdColInvisible(Grd1, new int[] { }, false);
            Sm.SetGrdProperty(Grd1, true);

        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine(" SELECT A.DocNo FROM tbldocthdr A ");
            SQL.AppendLine(" WHERE FIND_IN_SET(A.DocNo , @DocNo) ");
            SQL.AppendLine(" UNION ALL ");
            SQL.AppendLine(" SELECT A.DocNo FROM tbldoct2hdr A ");
            SQL.AppendLine(" WHERE FIND_IN_SET(A.DocNo , @DocNo) ");



            mSQL = SQL.ToString();
        }

        //ShowData
        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDOCtDocNo.Text, new string[] { "A.DocNo"  });
                Sm.CmParam(ref cm, "@DocNo", mFrmParent.TxtDOCt.Text);

               


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter ,
                        new string[]
                        { 
                            //1-2
                            "DocNo", 
                            

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            

                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }



        //

        #region Grid Method
        //override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        //{
        //    if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
        //    {
        //        e.DoDefault = false;
        //        if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).IndexOf("DOCDR") != -1)
        //        {
        //            var f = new FrmDOCt2(mMenuCode);
        //            f.Tag = mMenuCode;
        //            f.WindowState = FormWindowState.Normal;
        //            f.StartPosition = FormStartPosition.CenterScreen;
        //            f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
        //            f.ShowDialog();
        //        } else
        //        {
        //            var f = new FrmDOCt(mMenuCode);
        //            f.Tag = mMenuCode;
        //            f.WindowState = FormWindowState.Normal;
        //            f.StartPosition = FormStartPosition.CenterScreen;
        //            f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
        //            f.ShowDialog();
        //        }

        //    }
        //}
        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).IndexOf("DOCDR") != -1)
                {
                    var f = new FrmDOCt2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmDOCt(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                
            }
        }
        #endregion

        #endregion

        #region Event

        #region Grid Event
        #endregion

        #region Button Event

        #endregion

        #region Misc Control Event
        #endregion



        #endregion
    }
}
