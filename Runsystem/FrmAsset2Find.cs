﻿#region Update
/*
 * 29/12/2021 [TYO/PRODUCT] New apps
 */
#endregion


#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAsset2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmAsset2 mFrmParent;
        

        private List<String> mlProfitCenter;
        private bool mIsAllProfitCenterSelected = false;
        private bool mIsAssetUseMultiProfitCenterFilter = false;

        #endregion

        #region Constructor

        public FrmAsset2Find(FrmAsset2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sl.SetLueAssetCategoryCode(ref LueAssetCategoryCode);
                SetLueStatus(ref LueStatus);
                Sm.SetLue(LueStatus, "1");
                SetGrd();
                if(!mIsAssetUseMultiProfitCenterFilter) 
                {
                    label3.Visible = false;
                    CcbProfitCenterCode.Visible = false;
                    ChkProfitCenterCode.Visible = false;
                }
                mlProfitCenter = new List<String>();
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 44;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Asset's Code", 
                        "Asset's Name",
                        "Parent",
                        "Display Name",
                        "Active",
                        
                        //6-10
                        "Disabled Reason",
                        "Work Center",
                        "Category",
                        "Asset Date",
                        "Asset Value",
                        
                        //11-15
                        "Economic Life"+Environment.NewLine+"(Year)",
                        "Economic Life"+Environment.NewLine+"(Month)",
                        "Cost Center's Code",
                        "Cost Center's Name",
                        "Location",
                        
                        //16-20
                        "Depreciation Method",
                        "Annual Depreciation",
                        "COA Account#",
                        "COA Description",
                        "COA Account#"+Environment.NewLine+"(Depreciation)",
                        
                        //21-25
                        "COA Account"+Environment.NewLine+"Description"+Environment.NewLine+"(Depreciation)",
                        "Insurance"+Environment.NewLine+"Asset",
                        "Rented"+Environment.NewLine+"Asset",
                        "Sold"+Environment.NewLine+"Asset",
                        "Fiskal",
                        
                        //26-30
                        "Classification",
                        "Sub Classification", 
                        "Type", 
                        "Sub Type",
                        "Location", 

                        //31-35
                        "Sub Location",
                        "KPH/BKPH/TPK"+Environment.NewLine+"Location",
                        "Site",
                        "Created By",
                        "Created Date", 

                        //36-40
                        "Created Time",
                        "Last Updated By",
                        "Last Updated Date",
                        "Last Updated Time",
                        "Short Code",

                        //41-43
                        "Profit Center Code",
                        "Profit Center Name",
                        "Local# PO"
                        
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 250, 200, 200, 60, 
                        
                        //6-10
                        200, 80, 200, 100, 120, 

                        //11-15
                        150, 150, 130, 200, 150, 
                        
                        //16-20
                        150, 130, 100, 200, 100, 
                        
                        //21-25
                        200, 130, 130, 130, 130, 
                        
                        //26-30
                        130, 130, 130, 130, 130,

                        //31-35
                        130, 130, 130, 130, 130,

                        //36-40
                        130, 130, 130, 130, 130,

                        //41-43
                        130, 130, 180
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 12, 17 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 5, 7, 22, 23, 24, 25 });
            Sm.GrdFormatDate(Grd1, new int[] { 9, 27, 30 });
            Sm.GrdFormatTime(Grd1, new int[] { 28, 31 });
            Sm.GrdColInvisible(Grd1, new int[] { 34, 35, 36, 37, 38, 39 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 26, 27, 28, 29, 30, 31, 32, 33 }, mFrmParent.mIsAssetShowAdditionalInformation);
            Sm.GrdColInvisible(Grd1, new int[] { 41, 42 }, mIsAssetUseMultiProfitCenterFilter);
            Sm.GrdColInvisible(Grd1, new int[] { 43 }, mFrmParent.mIsRecvForAssetShowPOLocalDocNo);
            Grd1.Cols[40].Move(9);
            Grd1.Cols[41].Move(16);
            Grd1.Cols[42].Move(17);
            Grd1.Cols[43].Move(2);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 34, 35, 36, 37, 38, 39 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if(mIsAssetUseMultiProfitCenterFilter)
                if (IsProfitCenterInvalid()) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 1=1 ";
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                if (ChkStatus.Checked)
                {
                    switch (Sm.GetLue(LueStatus))
                    {
                        case "1":
                            Filter += " And A.ActiveInd='Y' ";
                            break;
                        case "2":
                            Filter += " And A.ActiveInd='N' ";
                            break;
                    }
                }

                Sm.FilterStr(ref Filter, ref cm, TxtAssetName.Text, new string[] { "A.AssetCode", "A.AssetName", "A.DisplayName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAssetCategoryCode), "A.AssetCategoryCode", true);

                SQL.AppendLine("Select A.AssetCode, A.AssetName, G.AssetName As ParentName, A.DisplayName, A.AssetType, A.ActiveInd, A.DisabledReason, ");
                SQL.AppendLine("F.AssetCategoryName, A.AssetDt, A.AssetValue, A.EcoLifeYr, A.EcoLife, A.Location, ");
                SQL.AppendLine("A.CCCode, B.CCName, P.ProfitCenterCode, P.ProfitCenterName, C.OptDesc, A.PercentageAnnualDepreciation, ");
                SQL.AppendLine("A.AcNo, A.AcNo2, ");
                if (mFrmParent.mIsCOAUseAlias)
                {
                    SQL.AppendLine("Concat(D.AcDesc, Case When D.Alias Is Null Then '' Else Concat(' [', D.Alias, ']') End) As AcDesc, ");
                    SQL.AppendLine("Concat(E.AcDesc, Case When E.Alias Is Null Then '' Else Concat(' [', E.Alias, ']') End) As AcDesc2, ");
                }
                else
                    SQL.AppendLine("D.AcDesc, E.AcDesc As AcDesc2, ");
                SQL.AppendLine("A.LeasingInd, A.RentedInd, A.SoldInd, A.FiskalInd, A.ShortCode, ");
                SQL.AppendLine("H.OptDesc Classification, I.OptDesc SubClassification, J.OptDesc Type, K.OptDesc SubType, L.OptDesc Location2, M.OptDesc SubLocation, N.OptDesc Location3, O.SiteName, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, Q.LocalDocNo ");
                SQL.AppendLine("From TblAsset A ");
                SQL.AppendLine("Left Join TblCostCenter B On A.CCCode = B.CCCode ");
                SQL.AppendLine("Left Join TblOption C On A.DepreciationCode = C.OptCode And C.OptCat = 'DepreciationMethod' ");
                SQL.AppendLine("Left Join TblCOA D On A.AcNo=D.AcNo ");
                SQL.AppendLine("Left Join TblCOA E On A.AcNo2=E.AcNo ");
                SQL.AppendLine("Left Join TblAssetCategory F On A.AssetCategoryCode=F.AssetCategoryCode ");
                SQL.AppendLine("Left Join TblAsset G On A.Parent=G.AssetCode ");
                SQL.AppendLine("Left Join TblOption H On A.Classification = H.OptCode And H.OptCat = 'AssetClassification' ");
                SQL.AppendLine("Left Join TblOption I On A.SubClassification = I.OptCode And I.OptCat = 'AssetSubClassification' ");
                SQL.AppendLine("Left Join TblOption J On A.Type = J.OptCode And J.OptCat = 'AssetType' ");
                SQL.AppendLine("Left Join TblOption K On A.SubType = K.OptCode And K.OptCat = 'AssetSubType' ");
                SQL.AppendLine("Left Join TblOption L On A.Location2 = L.OptCode And L.OptCat = 'AssetLocation' ");
                SQL.AppendLine("Left Join TblOption M On A.SubLocation = M.OptCode And M.OptCat = 'AssetSubLocation' ");
                SQL.AppendLine("Left Join TblOption N On A.Location3 = N.OptCode And N.OptCat = 'AssetLocation2' ");
                SQL.AppendLine("Left Join TblSite O On A.SiteCode = O.SiteCode ");
                SQL.AppendLine("Left Join TblProfitCenter P On B.ProfitCenterCode = P.ProfitCenterCode ");
                SQL.AppendLine("LEFT JOIN( ");
                SQL.AppendLine("    SELECT T1.assetcode, group_concat(distinct T4.LocalDocNo) AS LocalDocNo ");
                SQL.AppendLine("    FROM tblasset T1 ");
                SQL.AppendLine("    INNER JOIN tblassetdtl T2 ON T1.AssetCode = T2.AssetCode ");
                SQL.AppendLine("    INNER JOIN tblrecvvddtl T3 ON T2.RecvVdDocNo = T3.DocNo AND T2.RecvVdDNo = T3.DNo ");
                SQL.AppendLine("    INNER JOIN tblpohdr T4 ON T3.PODocNo = T4.DocNo ");
                SQL.AppendLine("    GROUP BY T1.assetcode ");
                SQL.AppendLine(") Q ON Q.assetcode = A.assetcode ");

                SQL.AppendLine(Filter);

                SetProfitCenter();

                if (mIsAssetUseMultiProfitCenterFilter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter2 = string.Empty;
                        int i = 0;

                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        ) ");
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter2.Length > 0) Filter2 += " Or ";
                            Filter2 += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                            i++;
                        }
                        if (Filter2.Length == 0)
                            SQL.AppendLine("    And 1=0 ");
                        else
                            SQL.AppendLine("    And (" + Filter2 + ") ");
                        SQL.AppendLine("    ) ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("    And A.CCCode Is Not Null ");
                            SQL.AppendLine("    And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                            SQL.AppendLine("    ) ");
                            if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And ProfitCenterCode In (");
                            SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("        )))) ");
                        }
                    }
                }
                SQL.AppendLine("Order By AssetName;");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "AssetCode", 
                                
                            //1-5
                            "AssetName", "ParentName", "DisplayName", "ActiveInd", "DisabledReason", 
                            
                            //6-10
                            "AssetType", "AssetCategoryName", "AssetDt", "AssetValue", "EcoLifeYr", 
                            
                            //11-15
                            "EcoLife", "CCCode", "CCName", "Location", "OptDesc", 
                            
                            //16-20
                            "PercentageAnnualDepreciation", "AcNo", "AcDesc", "AcNo2", "AcDesc2", 
                            
                            //21-25
                            "LeasingInd", "RentedInd", "SoldInd", "FiskalInd", "Classification",

                            //26-30
                            "SubClassification", "Type", "SubType", "Location2", "SubLocation",

                            //31-35
                            "Location3", "SiteName", "CreateBy", "CreateDt", "LastUpBy", 
                            
                            //36-340
                            "LastUpDt", "ShortCode", "ProfitCenterCode", "ProfitCenterName", "LocalDocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 32);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 33);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 35, 34);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 36, 34);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 35);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 38, 36);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 39, 36);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 37);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 41, 38);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 39);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 40);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11, 12, 17 });

                
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Active' As Col2 Union All " +
                "Select '2' As Col1, 'Inactive' As Col2; ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsAssetUseMultiProfitCenterFilter = Sm.GetParameterBoo("IsAssetUseMultiProfitCenterFilter");
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;
            
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            if (mIsAssetUseMultiProfitCenterFilter)
            {
                 SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            }
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtAssetName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void LueAssetCategoryCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAssetCategoryCode, new Sm.RefreshLue1(Sl.SetLueAssetCategoryCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkAssetCategoryCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Asset's category");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Asset's status");
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        #endregion

        #endregion
    }
}
