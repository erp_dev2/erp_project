﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPurchaserPerformance : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private string DocDt = string.Empty;
        private bool mIsShowForeignName = false, mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptPurchaserPerformance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();

                string CurrentDateTime = Sm.ServerCurrentDateTime();

                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select X1.ItCode, X2.ItName, X2.ForeignName, X1.UserName, ");
            SQL.AppendLine("Case When X1.MRDtVsRecvDtCount<> 0 Then (X1.MRDtVsRecvDt/X1.MRDtVsRecvDtCount) Else 0 End As AvgMRDtVsRecvDt ");
            SQL.AppendLine("from (");
            SQL.AppendLine("    Select  T.ItCode, T.UserName, ");
            SQL.AppendLine("    Sum(Difference) As MRDtVsRecvDt, Sum(Case when Difference=0 Then 0 Else 1 End) As MRDtVsRecvDtCount ");
            SQL.AppendLine("    from (");
            SQL.AppendLine("        Select T3.ItCode, T11.UserName, T2.DocDt As MRDt, T10.DocDt As RecvDt, ");
            SQL.AppendLine("        Case When DateDiff(T10.DocDt, T2.DocDt) Is Null then 0 else DateDiff(T10.DocDt, T2.DocDt) End As Difference ");
            SQL.AppendLine("        From tblmaterialrequesthdr T2 ");
            SQL.AppendLine("        inner join tblmaterialrequestdtl T3 on T3.DocNo=T2.DocNo And T3.CancelInd='N' And T3.Status<>'C' ");
            SQL.AppendLine("        left join tblporequestdtl T5 on T5.MaterialRequestDocNo=T3.DocNo AND T5.MaterialRequestDNo=T3.DNo  And T5.CancelInd='N' And T5.Status<>'C' ");
            SQL.AppendLine("        left join tblporequesthdr T6 on T6.DocNo=T5.DocNo ");
            SQL.AppendLine("        left join tblpodtl T7 on T7.PORequestDocNo=T5.DocNo and T7.PORequestDNo=T5.DNo And T7.CancelInd='N' ");
            SQL.AppendLine("        left join tblpohdr T8 on T8.DocNo=T7.DocNo ");
            SQL.AppendLine("        left join tblrecvvddtl T9 on T9.PODocNo=T7.DocNo AND T9.PODNo=T7.DNo And T9.CancelInd='N' ");
            SQL.AppendLine("        left join tblrecvvdhdr T10 on T10.DocNo=T9.DocNo And IfNull(T10.POInd, 'Y')='Y' ");
			SQL.AppendLine("	    Inner Join TblUser T11 On T5.CreateBy = T11.UserCode ");
            SQL.AppendLine("	    Where Left(T2.DocDt, 6)=@MonthFilter ");
            SQL.AppendLine("	    ) T Group By T.ItCode, T.UserName  ");
            SQL.AppendLine(") X1 ");
            SQL.AppendLine("Inner Join TblItem X2 On X1.ItCode=X2.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=X2.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Where Case When X1.MRDtVsRecvDtCount<> 0 Then (X1.MRDtVsRecvDt/X1.MRDtVsRecvDtCount) Else 0 End<> 0 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item's Code",
                        "Item's  Name", 
                        "Foreign Name",
                        "Average Day", 
                        "User"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 300, 200, 100, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            if (!mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Month = Sm.GetLue(LueMth);
            DocDt = string.Concat(Year, Month);

            if (Year == string.Empty && Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month And Year is Empty.");
            }
            else if (Year == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
            }
            else if (Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
            }
            else
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = " ";

                    var cm = new MySqlCommand();
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                    Sm.CmParam<String>(ref cm, "@MonthFilter", DocDt);
                    Sm.FilterStr(ref Filter, ref cm, TxtUserCode.Text, new string[] { "X1.UserName" });
                    Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "X1.ItCode", "X2.ItName", "X2.ForeignName" });

                    Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By X1.UserName, X2.ItName;",
                        new string[]{ "ItCode", "ItName", "ForeignName", "AvgMRDtVsRecvDt", "UserName"  },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            
                        }, true, false, false, false
                        );
                    Grd1.GroupObject.Add(5);
                    Grd1.Group();
                    Grd1.Rows.CollapseAll();
                    AdjustSubtotals();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Sm.FocusGrd(Grd1, 0, 1);
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        #endregion

        #endregion

        #region Event
        private void ChkUserCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "User");
        }

        private void TxtUserCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
