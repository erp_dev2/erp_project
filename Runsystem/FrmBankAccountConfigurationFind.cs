﻿#region Update
/*
   30/11/2021 [DITA/PHT] New apps 
 */ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmBankAccountConfigurationFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmBankAccountConfiguration mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmBankAccountConfigurationFind(FrmBankAccountConfiguration FrmParent) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueOption(ref LueType, "BankAccountTransactionType");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Type, B.OptDesc, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblBankAccountConfigurationHdr A ");
            SQL.AppendLine("Inner Join TblOption B  On A.Type = B.OptCode And B.OptCat = 'BankAccountTransactionType' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Type Code",
                        "Type",
                        "Created By",
                        "Created Date",
                        "Created Time",
                        
                        
                        //6-8
                        "Last Updated By",
                        "Last Updated Date",
                        "Last Updated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4, 7 });
            Sm.GrdFormatTime(Grd1, new int[] { 5, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 5, 6, 7, 8 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 7, 8 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "A.Type", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By B.OptDesc;",
                        new string[]
                        {
                              //0
                             "Type",

                              //1-5
                             "OptDesc",
                             "CreateBy",
                             "CreateDt",
                             "LastUpBy",
                             "LastUpDt"

                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 8, 5);
                           
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueType_EditValueChanged(object sender, EventArgs e)
        { 
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue2(Sl.SetLueOption), "BankAccountTransactionType");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }


        #endregion

        #endregion
    }
}
