﻿#region Update
/*
    06/06/2017 [TKG] tambah informasi active indicator.
    23/05/2018 [HAR] tambah grid inputan asset category dan COA
    06/06/2018 [TKG] berdasarkan parameter IsAADJournalBasedOnAssetCategory, tambah validasi asset category dan coa tidak boleh kosong.
    04/01/2019 [HAR] bug saat generate kode costcenter
    13/05/2019 [DITA] bug : department yg tampil masih ada department yg tidak aktif
    12/07/2019 [WED] bug : Department saat refresh 
    11/06/2020 [TKG/SRN] tambah indikator IsCCPayrollJournalEnabled
    23/12/2020 [ICA/PHT] Menambahkan identitas RKAP / CBP Indicator
    04/03/2021 [HAR/PHT] menambah validasi saat generate costcenter
    18/05/2021 [TKG/PHT] menambah not parent indicator
    21/05/2021 [VIN/ALL] bug show data
    08/06/2021 [HAR/PHT] bug show data (parent ind)
    02/09/2021 [ICA/AMKA] membuat bisa dibuka di aplikasi lain
    21/01/2022 [DEV/PRODUCT] Modifikasi pada menu Cost Center dengan menambahkan TickBox berjudul "Asset Project" dengan parameter IsCostCenterUseAssetProject
    23/03/2022 [VIN/YK] BUG Generate CC
    16/06/2022 [TYO/VIR] Set insert cost center code manual berdasarkan parameter IsCostCenterCodeNotGenerateAutomatic
    02/03/2023 [WED/BBT] tambah karakter di Cost Center name jadi 255
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCostCenter : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mCCCode = string.Empty; //if this application is called from other application
        internal FrmCostCenterFind FrmFind;
        private bool
            mIsAADJournalBasedOnAssetCategory = false,
            mIsCostCenterCodeNotGenerateAutomatic = false;
        internal bool 
            mIsCCPayrollJournalEnabled = false,
            mIsCostCenterNotParentIndEnabled = false,
            mIsCostCenterUseAssetProject = false;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmCostCenter(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Cost Category";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                ExecQuery();
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                if (mIsCostCenterNotParentIndEnabled)
                    SetLueCCCode(ref LueParent);
                else
                    Sl.SetLueCCCode(ref LueParent);
                Sl.SetLueProfitCenterCode(ref LueProfitCenter);
                Sl.SetLueDeptCode2(ref LueDeptCode, string.Empty);
                Sl.SetLueAcNo(ref LueAcCode);
                Sl.SetLueAssetCategoryCode(ref LueAssetCatCode);
                LueAcCode.Visible = false;
                LueAssetCatCode.Visible = false;
                if(!mIsCCPayrollJournalEnabled)
                {
                    ChkIsCCPayrollJournalEnabled.Visible = false;
                    ChkCBPInd.Location = new System.Drawing.Point(345, 7);
                }
                if (!mIsCostCenterUseAssetProject) ChkAssetProjectInd.Visible = false;

                //if this application is called from other application
                if (mCCCode.Length != 0)
                {
                    ShowData(mCCCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        //1-4
                        "Code",
                        "Asset's Category",
                        "COA's Account#",
                        "COA's Account Description"
                    },
                    new int[] 
                    {
                        0, 
                        0, 250, 180, 350
                    }
                );
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCCCode, ChkActInd, TxtCCName, LueParent, LueProfitCenter, 
                        LueDeptCode, ChkIsCCPayrollJournalEnabled, ChkCBPInd, ChkNotParentInd, ChkAssetProjectInd
                    }, true);
                    BtnAcNo.Enabled = false;
                    Grd1.ReadOnly = true;
                    TxtCCCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkIsCCPayrollJournalEnabled, TxtCCName, LueParent, LueProfitCenter, LueDeptCode, 
                        ChkCBPInd, ChkNotParentInd, ChkAssetProjectInd
                    }, false);
                    if(mIsCostCenterCodeNotGenerateAutomatic) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtCCCode }, false);
                    BtnAcNo.Enabled = true;
                    Grd1.ReadOnly = false;
                    TxtCCName.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkActInd, ChkIsCCPayrollJournalEnabled, TxtCCName, LueParent, LueProfitCenter, 
                        LueDeptCode, ChkCBPInd, ChkNotParentInd, ChkAssetProjectInd
                    }, false);
                    BtnAcNo.Enabled = true;
                    Grd1.ReadOnly = false;
                    TxtCCName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtCCCode, TxtCCName, LueParent, LueProfitCenter, LueDeptCode,
                TxtAcNo, TxtAcDesc, LueAcCode, LueAssetCatCode
            });
            ChkActInd.Checked = false;
            ChkIsCCPayrollJournalEnabled.Checked = false;
            ChkCBPInd.Checked = false;
            ChkNotParentInd.Checked = false;
            ChkAssetProjectInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }
        #endregion    

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCostCenterFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            Sl.SetLueDeptCode2(ref LueDeptCode, string.Empty);
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCCCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblCostCenter Where CCCode=@CCCode And CCName=@CCName And Parent=@Parent " };
                Sm.CmParam<String>(ref cm, "@CCCode", TxtCCCode.Text);
                Sm.CmParam<String>(ref cm, "@CCName", TxtCCName.Text);
                Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;
                if (TxtCCCode.Text.Length == 0 && mIsCostCenterCodeNotGenerateAutomatic == false)
                    TxtCCCode.EditValue = GenerateCCCode();


                var cml = new List<MySqlCommand>();

                cml.Add(SaveCostCenter());
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveCostCenterDtl(TxtCCCode.Text, Row));
                
                Sm.ExecCommands(cml);

                ShowData(TxtCCCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string CCCode)
        {
            try
            {
                ClearData();
                ShowCC(CCCode);
                ShowCCDtl(CCCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }


        private void ShowCC(string CCCode)
        {
            Cursor.Current = Cursors.WaitCursor;

            ClearData();

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CCCode, A.CCName, A.ActInd, A.IsCCPayrollJournalEnabled, A.CBPInd, A.Parent, A.ProfitCenterCode, A.DeptCode, A.AcNo, B.AcDesc, ");
            if (mIsCostCenterUseAssetProject)
                SQL.AppendLine("A.AssetProjectInd, ");
            else
                SQL.AppendLine("Null As AssetProjectInd, ");
            if (mIsCostCenterNotParentIndEnabled)
                SQL.AppendLine("NotParentInd ");
            else
                SQL.AppendLine("Case When Exists(Select 1 From TblCostCenter Where Parent Is Not Null And Parent=A.CCCode Limit 1) Then 'N' Else 'Y' End As NotParentInd ");
            SQL.AppendLine("From TblCostCenter A ");
            SQL.AppendLine("Left Join TblCOA B On A.AcNo=B.AcNo ");
            SQL.AppendLine("Where A.CCCode=@CCCode;");

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                        {
                            "CCCode", 
                            "CCName", "ActInd", "IsCCPayrollJournalEnabled", "CBPInd", "Parent", 
                            "ProfitCenterCode", "DeptCode", "AcNo", "AcDesc", "NotParentInd", 
                            "AssetProjectInd"
                        },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtCCCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtCCName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        ChkIsCCPayrollJournalEnabled.Checked = Sm.DrStr(dr, c[3]) == "Y";
                        ChkCBPInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                        Sm.SetLue(LueParent, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueProfitCenter, Sm.DrStr(dr, c[6]));
                        Sl.SetLueDeptCode2(ref LueDeptCode, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[7]));
                        TxtAcNo.EditValue = Sm.DrStr(dr, c[8]);
                        TxtAcDesc.EditValue = Sm.DrStr(dr, c[9]);
                        ChkNotParentInd.Checked = Sm.DrStr(dr, c[10]) == "Y";
                        ChkAssetProjectInd.Checked = Sm.DrStr(dr, c[11]) == "Y";
                    }, true
                );
        }

        private void ShowCCDtl(string CCCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select B.Dno, B.AssetcategoryCode, C.AssetCategoryName, B.AcNo, Concat(D.AcNo, ' [ ', D.AcDesc, ' ]') As AcDesc " +
                    "From TblCostCenter A "+
                    "Inner Join tblCostCenterDtl  B On A.CCCode = B.CCCode " +
                    "Inner join TblAssetCategory C On B.AssetCategoryCode = C.AssetCategoryCode "+
                    "Left Join TblCOA D On B.Acno = D.AcNo "+
                    "Where A.CCCode=@CCCode " +
                    "Order By B.DNo;",
                    new string[] 
                    { 
                        "Dno", 
                        "AssetCategoryCode", "AssetCategoryName", "AcNo", "AcDesc", 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtCCName, "Cost Center Name", false) ||
                Sm.IsLueEmpty(LueProfitCenter, "Profit Center") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                IsCCCodeExisted() ||
                (mIsAADJournalBasedOnAssetCategory && IsGrdEmpty()) ||
                IsNotParentInvalid() ||
                IsParentInvalid()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 asset category.");
                return true;
            }
            return false;
        }

        private bool IsNotParentInvalid()
        {
            if (!mIsCostCenterNotParentIndEnabled) return false;
            if (!TxtCCCode.Properties.ReadOnly) return false;

            if (ChkNotParentInd.Checked && 
                Sm.IsDataExist("Select 1 From TblCostCenter Where Parent Is Not Null And Parent=@Param Limit 1;",
                TxtCCCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This cost center is another cost center's parent.");
                return true;
            }
            return false;
        }

        private bool IsParentInvalid()
        {
            if (!mIsCostCenterNotParentIndEnabled) return false;
            var Parent = Sm.GetLue(LueParent);
            if (Parent.Length==0) return false;

            if (ChkNotParentInd.Checked &&
                Sm.IsDataExist("Select 1 From TblCostCenter Where NotParentInd='Y' And CCCode=@Param Limit 1;", Parent))
            {
                Sm.StdMsg(mMsgType.Warning, "This cost center should not become parent.");
                return true;
            }
            return false;
        }

        private bool IsCCCodeExisted()
        {
            if (!TxtCCCode.Properties.ReadOnly &&
                Sm.IsDataExist("Select 1 From TblCostCenter Where CCCode=@Param Limit 1;",
                TxtCCCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Cost center's code already existed.");
                return true;
            }
            return false;
        }

        private string GenerateCCCode()
        {
            var SQL = new StringBuilder();
            int lenCCCode = 5;

            var MaxCCCode = Sm.GetValue("SELECT MAX(LENGTH(CCCode)) FROM tblcostcenter");
            if (MaxCCCode.Length > 0)
                lenCCCode = Convert.ToInt32(MaxCCCode);

            SQL.AppendLine("Select Right(Concat('0000', IfNull(( ");
            //SQL.AppendLine("    Select MAX(CAST(CCCode As int))+1 As Value ");
            SQL.AppendLine("    Select Cast(IfNull(Max(CCCode), '00000') As UNSIGNED)+1 As Value ");
            SQL.AppendLine("    From TblCostCenter ");
            SQL.AppendLine("    WHERE LENGTH(CCCode) = "+lenCCCode+" ");
            SQL.AppendLine("    Group By CCCode ");
            SQL.AppendLine("    Order By CCCode Desc Limit 1 ");
            SQL.AppendLine("), 1)), "+lenCCCode+") As CCCode");

            return Sm.GetValue(SQL.ToString());
        }

        private MySqlCommand SaveCostCenter()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From tblCostCenterDtl Where CCCode =@CCCode ;");

            SQL.AppendLine("Insert Into TblCostCenter(CCCode, CCName, ActInd, IsCCPayrollJournalEnabled, CBPInd, Parent, ProfitCenterCode, DeptCode, AcNo, ");
            if (mIsCostCenterNotParentIndEnabled) SQL.AppendLine("NotParentInd, ");
            if (mIsCostCenterUseAssetProject) SQL.AppendLine("AssetProjectInd, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@CCCode, @CCName, @ActInd, @IsCCPayrollJournalEnabled, @CBPInd, @Parent, @ProfitCenterCode, @DeptCode, @AcNo, ");
            if (mIsCostCenterNotParentIndEnabled) SQL.AppendLine("@NotParentInd, ");
            if (mIsCostCenterUseAssetProject) SQL.AppendLine("@AssetProjectInd, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("        CCName=@CCName, ActInd=@ActInd, IsCCPayrollJournalEnabled=@IsCCPayrollJournalEnabled, CBPInd=@CBPInd, Parent=@Parent, ProfitCenterCode=@ProfitCenterCode, DeptCode=@DeptCode, AcNo=@AcNo, ");
            if (mIsCostCenterNotParentIndEnabled) SQL.AppendLine("NotParentInd=@NotParentInd, ");
            if (mIsCostCenterUseAssetProject) SQL.AppendLine("AssetProjectInd=@AssetProjectInd, ");
            SQL.AppendLine("        LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CCCode", TxtCCCode.Text);
            Sm.CmParam<String>(ref cm, "@CCName", TxtCCName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@IsCCPayrollJournalEnabled", ChkIsCCPayrollJournalEnabled.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CBPInd", ChkCBPInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));            
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenter));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@NotParentInd", ChkNotParentInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@AssetProjectInd", ChkAssetProjectInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        private MySqlCommand SaveCostCenterDtl(string CCCode, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblCostCenterDtl(CCCode, DNo, AssetCategoryCode, AcNo, CreateBy, CreateDt) " +
                    "Values(@CCCode, @DNo, @AssetCategoryCode, @AcNo, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AssetCategoryCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        
        #endregion

        #region Additional Method

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("ALTER TABLE `tblcostcenter` ");
            SQL.AppendLine("    CHANGE COLUMN `CCName` `CCName` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci' AFTER `CCCode`; ");

            Sm.ExecQuery(SQL.ToString());
        }

        private void GetParameter()
        {
            mIsAADJournalBasedOnAssetCategory = Sm.GetParameterBoo("IsAADJournalBasedOnAssetCategory");
            mIsCCPayrollJournalEnabled = Sm.GetParameterBoo("IsCCPayrollJournalEnabled");
            mIsCostCenterNotParentIndEnabled = Sm.GetParameterBoo("IsCostCenterNotParentIndEnabled");
            mIsCostCenterUseAssetProject = Sm.GetParameterBoo("IsCostCenterUseAssetProject");
            mIsCostCenterCodeNotGenerateAutomatic = Sm.GetParameterBoo("IsCostCenterCodeNotGenerateAutomatic");
        }

        private void SetLueCCCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CCCode As Col1, CCName As Col2 From TblCostCenter " +
                "Where NotParentInd='N' " +
                "Order By CCName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion 

        #endregion

        #region Event

        #region Misc Control Event

        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsCostCenterNotParentIndEnabled)
                    Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue1(SetLueCCCode));
                else
                    Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue1(Sl.SetLueCCCode));
            }
        }

        private void LueProfitCenter_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueProfitCenter, new Sm.RefreshLue1(Sl.SetLueProfitCenterCode));
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(Sl.SetLueDeptCode2), string.Empty);
        }

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmCostCenterDlg(this));
        }

        private void LueAssetCatCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueAssetCatCode, new Sm.RefreshLue1(Sl.SetLueAssetCategoryCode));
        }

        private void LueAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueAcCode, new Sm.RefreshLue1(Sl.SetLueAcNo));
        }

        private void LueAssetCatCode_Leave(object sender, EventArgs e)
        {
            if (LueAssetCatCode.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueAssetCatCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 1].Value =
                    Grd1.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueAssetCatCode);
                    Grd1.Cells[fCell.RowIndex, 2].Value = LueAssetCatCode.GetColumnValue("Col2");
                }
                LueAssetCatCode.Visible = false;
            }
        }

        private void LueAcCode_Leave(object sender, EventArgs e)
        {
            if (LueAcCode.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LueAcCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 3].Value =
                    Grd1.Cells[fCell.RowIndex, 4].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 3].Value = Sm.GetLue(LueAcCode);
                    Grd1.Cells[fCell.RowIndex, 4].Value = LueAcCode.GetColumnValue("Col2");
                }
                LueAcCode.Visible = false;
            }
        }

        private void LueAssetCatCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueAcCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                LueRequestEdit(Grd1, LueAssetCatCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sl.SetLueAssetCategoryCode(ref LueAssetCatCode);
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 4 }, e.ColIndex))
            {
                LueRequestEdit(Grd1, LueAcCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sl.SetLueAcNo(ref LueAcCode);
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion
    }
}
