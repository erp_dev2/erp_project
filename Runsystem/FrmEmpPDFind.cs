﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmEmpPDFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmEmpPD mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmEmpPDFind(FrmEmpPD FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetSQL();
            SetGrd();
            Sl.SetLueDeptCode(ref LueDeptCode);
        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("Select A.DocNo, A.DocDt, I.OptDesc As JobTransfer, A.EmpCode, B.EmpName, ");
            SQL.AppendLine("C.DeptName As DeptNameOld, D.PosName As PosNameOld, E.GrdLvlName As GrdLvlNameOld, F.Deptname, ");
            SQL.AppendLine("G.PosName, H.GrdLvlName, ");
            SQL.AppendLine("A.CreateBy,A.CreateDt,A.LastUpBy,A.LastUpDt ");
            SQL.AppendLine("From TblEmpPD A ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCodeOld = C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On A.PosCodeOld = D.PosCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On A.GrdLvlCodeOld = E.GrdLvlCode ");
            SQL.AppendLine("Left Join TblDepartment F On A.DeptCode = F.DeptCode ");
            SQL.AppendLine("Left Join TblPosition G On A.PosCode  = G.PosCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr H On A.GrdLvlCode = H.GrdLvlCode ");
            SQL.AppendLine("Left join TblOption I On A.JobTransfer = I.OptCode And OptCat = 'EmpJobTransfer'");
            SQL.AppendLine("Where 0=0 ");
            if (!mFrmParent.mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            mSQL = SQL.ToString();
        }
        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document"+Environment.NewLine+"Number", 
                        "Document"+Environment.NewLine+"Date",
                        "Type",
                        "Employee"+Environment.NewLine+"Code", 
                        "Employee"+Environment.NewLine+"Name",
 
                        //6-10
                        "Department"+Environment.NewLine+"Name Old",
                        "Position"+Environment.NewLine+"Name Old",
                        "Grade Level"+Environment.NewLine+"Name Old",
                        "Department",
                        "Position",

                        //11-15
                        "Grade level"+Environment.NewLine+"Name",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        //17
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 13, 16 });
            Sm.GrdFormatTime(Grd1, new int[] { 14, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 12, 13, 14, 15, 16, 17 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 12, 13, 14, 15, 16, 17 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "B.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By B.EmpName",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            //1-5
                            "DocDt", "JobTransfer", "EmpCode", "EmpName", "DeptNameOld",  
                            //6-10
                            "PosNameOld", "GrdLvlNameOld", "DeptName", "PosName", "GrdLvlName", 
                            //11-14
                            "CreateBy","CreateDt","LastUpBy","LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);

                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);

                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 17, 14);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event
        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion
    }
}
