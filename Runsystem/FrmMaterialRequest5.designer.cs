﻿namespace RunSystem
{
    partial class FrmMaterialRequest5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMaterialRequest5));
            this.DteUsageDt = new DevExpress.XtraEditors.DateEdit();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.LueDurationUom = new DevExpress.XtraEditors.LookUpEdit();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.BtnCopyGeneral = new DevExpress.XtraEditors.SimpleButton();
            this.LblCopyGeneral = new System.Windows.Forms.Label();
            this.BtnBCCode = new DevExpress.XtraEditors.SimpleButton();
            this.LueDeptCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.LblDeptCode2 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.LueDivCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtTotalEstPrice = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.LueProcessInd = new DevExpress.XtraEditors.LookUpEdit();
            this.LblProcessInd = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblPICCode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LuePICCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.LueReqType = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnDORequestDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.BtnDORequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtDORequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtRemainingBudget = new DevExpress.XtraEditors.TextEdit();
            this.LblDORequestDocNo = new System.Windows.Forms.Label();
            this.BtnPOQtyCancelDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LueBCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPOQtyCancelDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.BtnPOQtyCancelDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label76 = new System.Windows.Forms.Label();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.BtnDroppingRequestDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtDR_Balance = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtDR_MRAmt = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.MeeDR_Remark = new DevExpress.XtraEditors.MemoExEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtDR_DroppingRequestAmt = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtDR_BCCode = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtDR_PRJIDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtDR_Mth = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtDR_Yr = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtDR_DeptCode = new DevExpress.XtraEditors.TextEdit();
            this.BtnDroppingRequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtDroppingRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.ChkFile3 = new DevExpress.XtraEditors.CheckEdit();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.PbUpload2 = new System.Windows.Forms.ProgressBar();
            this.BtnDownload3 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload3 = new System.Windows.Forms.ProgressBar();
            this.BtnFile3 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile2 = new DevExpress.XtraEditors.TextEdit();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile2 = new DevExpress.XtraEditors.CheckEdit();
            this.TxtFile3 = new DevExpress.XtraEditors.TextEdit();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnFile2 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDurationUom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDivCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalEstPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcessInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePICCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReqType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDORequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOQtyCancelDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Balance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_MRAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDR_Remark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_DroppingRequestAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_BCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_PRJIDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Mth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Yr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_DeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDroppingRequestDocNo.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(764, 0);
            this.panel1.Size = new System.Drawing.Size(70, 501);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Location = new System.Drawing.Point(0, 144);
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Location = new System.Drawing.Point(0, 120);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Location = new System.Drawing.Point(0, 96);
            this.BtnSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Location = new System.Drawing.Point(0, 72);
            this.BtnDelete.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Location = new System.Drawing.Point(0, 48);
            this.BtnEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Location = new System.Drawing.Point(0, 24);
            this.BtnInsert.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Size = new System.Drawing.Size(764, 353);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LueDurationUom);
            this.panel3.Controls.Add(this.LueCurCode);
            this.panel3.Controls.Add(this.DteUsageDt);
            this.panel3.Location = new System.Drawing.Point(0, 353);
            this.panel3.Size = new System.Drawing.Size(764, 148);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.DteUsageDt, 0);
            this.panel3.Controls.SetChildIndex(this.LueCurCode, 0);
            this.panel3.Controls.SetChildIndex(this.LueDurationUom, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 479);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(764, 148);
            this.Grd1.TabIndex = 57;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.GrdAfterCommitEdit);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnExcel.Location = new System.Drawing.Point(0, 168);
            this.BtnExcel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnExcel.Size = new System.Drawing.Size(70, 24);
            // 
            // DteUsageDt
            // 
            this.DteUsageDt.EditValue = null;
            this.DteUsageDt.EnterMoveNextControl = true;
            this.DteUsageDt.Location = new System.Drawing.Point(74, 19);
            this.DteUsageDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUsageDt.Name = "DteUsageDt";
            this.DteUsageDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.Appearance.Options.UseFont = true;
            this.DteUsageDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUsageDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUsageDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUsageDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUsageDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUsageDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUsageDt.Size = new System.Drawing.Size(125, 20);
            this.DteUsageDt.TabIndex = 58;
            this.DteUsageDt.Visible = false;
            this.DteUsageDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteUsageDt_KeyDown);
            this.DteUsageDt.Leave += new System.EventHandler(this.DteUsageDt_Leave);
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(207, 19);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 14;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 200;
            this.LueCurCode.Size = new System.Drawing.Size(125, 20);
            this.LueCurCode.TabIndex = 59;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            this.LueCurCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode_KeyDown);
            this.LueCurCode.Leave += new System.EventHandler(this.LueCurCode_Leave);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // LueDurationUom
            // 
            this.LueDurationUom.EnterMoveNextControl = true;
            this.LueDurationUom.Location = new System.Drawing.Point(342, 19);
            this.LueDurationUom.Margin = new System.Windows.Forms.Padding(5);
            this.LueDurationUom.Name = "LueDurationUom";
            this.LueDurationUom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.Appearance.Options.UseFont = true;
            this.LueDurationUom.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDurationUom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDurationUom.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDurationUom.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDurationUom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDurationUom.Properties.DropDownRows = 14;
            this.LueDurationUom.Properties.NullText = "[Empty]";
            this.LueDurationUom.Properties.PopupWidth = 200;
            this.LueDurationUom.Size = new System.Drawing.Size(125, 20);
            this.LueDurationUom.TabIndex = 60;
            this.LueDurationUom.ToolTip = "F4 : Show/hide list";
            this.LueDurationUom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDurationUom.EditValueChanged += new System.EventHandler(this.LueDurationUom_EditValueChanged);
            this.LueDurationUom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueDurationUom_KeyDown);
            this.LueDurationUom.Leave += new System.EventHandler(this.LueDurationUom_Leave);
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 0);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(764, 353);
            this.Tc1.TabIndex = 15;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2,
            this.Tp3});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel5);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(758, 325);
            this.Tp1.Text = "General";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.BtnCopyGeneral);
            this.panel5.Controls.Add(this.LblCopyGeneral);
            this.panel5.Controls.Add(this.BtnBCCode);
            this.panel5.Controls.Add(this.LueDeptCode2);
            this.panel5.Controls.Add(this.LblDeptCode2);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.LueDivCode);
            this.panel5.Controls.Add(this.TxtTotalEstPrice);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.LueProcessInd);
            this.panel5.Controls.Add(this.LblProcessInd);
            this.panel5.Controls.Add(this.TxtDocNo);
            this.panel5.Controls.Add(this.LblPICCode);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.LuePICCode);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.DteDocDt);
            this.panel5.Controls.Add(this.LueReqType);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.LueDeptCode);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.BtnDORequestDocNo2);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.BtnDORequestDocNo);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.TxtDORequestDocNo);
            this.panel5.Controls.Add(this.TxtRemainingBudget);
            this.panel5.Controls.Add(this.LblDORequestDocNo);
            this.panel5.Controls.Add(this.BtnPOQtyCancelDocNo);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.LueBCCode);
            this.panel5.Controls.Add(this.TxtPOQtyCancelDocNo);
            this.panel5.Controls.Add(this.LblSiteCode);
            this.panel5.Controls.Add(this.BtnPOQtyCancelDocNo2);
            this.panel5.Controls.Add(this.LueSiteCode);
            this.panel5.Controls.Add(this.label76);
            this.panel5.Controls.Add(this.TxtLocalDocNo);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(758, 325);
            this.panel5.TabIndex = 22;
            // 
            // BtnCopyGeneral
            // 
            this.BtnCopyGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnCopyGeneral.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCopyGeneral.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCopyGeneral.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCopyGeneral.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCopyGeneral.Appearance.Options.UseBackColor = true;
            this.BtnCopyGeneral.Appearance.Options.UseFont = true;
            this.BtnCopyGeneral.Appearance.Options.UseForeColor = true;
            this.BtnCopyGeneral.Appearance.Options.UseTextOptions = true;
            this.BtnCopyGeneral.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCopyGeneral.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCopyGeneral.Image = ((System.Drawing.Image)(resources.GetObject("BtnCopyGeneral.Image")));
            this.BtnCopyGeneral.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCopyGeneral.Location = new System.Drawing.Point(731, 300);
            this.BtnCopyGeneral.Name = "BtnCopyGeneral";
            this.BtnCopyGeneral.Size = new System.Drawing.Size(24, 21);
            this.BtnCopyGeneral.TabIndex = 56;
            this.BtnCopyGeneral.ToolTip = "Show Employee List";
            this.BtnCopyGeneral.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCopyGeneral.ToolTipTitle = "Run System";
            this.BtnCopyGeneral.Click += new System.EventHandler(this.BtnCopyGeneral_Click);
            // 
            // LblCopyGeneral
            // 
            this.LblCopyGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LblCopyGeneral.AutoSize = true;
            this.LblCopyGeneral.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCopyGeneral.ForeColor = System.Drawing.Color.Green;
            this.LblCopyGeneral.Location = new System.Drawing.Point(657, 303);
            this.LblCopyGeneral.Name = "LblCopyGeneral";
            this.LblCopyGeneral.Size = new System.Drawing.Size(71, 14);
            this.LblCopyGeneral.TabIndex = 55;
            this.LblCopyGeneral.Tag = "Copy Data From Existing Employee";
            this.LblCopyGeneral.Text = "Copy Data";
            // 
            // BtnBCCode
            // 
            this.BtnBCCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBCCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBCCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBCCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBCCode.Appearance.Options.UseBackColor = true;
            this.BtnBCCode.Appearance.Options.UseFont = true;
            this.BtnBCCode.Appearance.Options.UseForeColor = true;
            this.BtnBCCode.Appearance.Options.UseTextOptions = true;
            this.BtnBCCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBCCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBCCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnBCCode.Image")));
            this.BtnBCCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBCCode.Location = new System.Drawing.Point(454, 256);
            this.BtnBCCode.Name = "BtnBCCode";
            this.BtnBCCode.Size = new System.Drawing.Size(24, 21);
            this.BtnBCCode.TabIndex = 55;
            this.BtnBCCode.ToolTip = "Show Budget Category List";
            this.BtnBCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBCCode.ToolTipTitle = "Run System";
            this.BtnBCCode.Click += new System.EventHandler(this.BtnBCCode_Click);
            // 
            // LueDeptCode2
            // 
            this.LueDeptCode2.EnterMoveNextControl = true;
            this.LueDeptCode2.Location = new System.Drawing.Point(149, 194);
            this.LueDeptCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode2.Name = "LueDeptCode2";
            this.LueDeptCode2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.LueDeptCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.Appearance.Options.UseBackColor = true;
            this.LueDeptCode2.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode2.Properties.DropDownRows = 30;
            this.LueDeptCode2.Properties.NullText = "[Empty]";
            this.LueDeptCode2.Properties.PopupWidth = 300;
            this.LueDeptCode2.Size = new System.Drawing.Size(300, 20);
            this.LueDeptCode2.TabIndex = 42;
            this.LueDeptCode2.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode2.EditValueChanged += new System.EventHandler(this.LueDeptCode2_EditValueChanged);
            this.LueDeptCode2.Validated += new System.EventHandler(this.LueDeptCode2_Validated);
            // 
            // LblDeptCode2
            // 
            this.LblDeptCode2.AutoSize = true;
            this.LblDeptCode2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDeptCode2.ForeColor = System.Drawing.Color.Red;
            this.LblDeptCode2.Location = new System.Drawing.Point(5, 197);
            this.LblDeptCode2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDeptCode2.Name = "LblDeptCode2";
            this.LblDeptCode2.Size = new System.Drawing.Size(138, 14);
            this.LblDeptCode2.TabIndex = 41;
            this.LblDeptCode2.Text = "Department For Budget";
            this.LblDeptCode2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(97, 155);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(46, 14);
            this.label23.TabIndex = 37;
            this.label23.Text = "Division";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDivCode
            // 
            this.LueDivCode.EnterMoveNextControl = true;
            this.LueDivCode.Location = new System.Drawing.Point(149, 152);
            this.LueDivCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDivCode.Name = "LueDivCode";
            this.LueDivCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivCode.Properties.Appearance.Options.UseFont = true;
            this.LueDivCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDivCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDivCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDivCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDivCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDivCode.Properties.DropDownRows = 30;
            this.LueDivCode.Properties.NullText = "[Empty]";
            this.LueDivCode.Properties.PopupWidth = 300;
            this.LueDivCode.Size = new System.Drawing.Size(300, 20);
            this.LueDivCode.TabIndex = 38;
            this.LueDivCode.ToolTip = "F4 : Show/hide list";
            this.LueDivCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDivCode.EditValueChanged += new System.EventHandler(this.LueDivCode_EditValueChanged);
            this.LueDivCode.Validated += new System.EventHandler(this.LueDivCode_Validated);
            // 
            // TxtTotalEstPrice
            // 
            this.TxtTotalEstPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtTotalEstPrice.EnterMoveNextControl = true;
            this.TxtTotalEstPrice.Location = new System.Drawing.Point(587, 5);
            this.TxtTotalEstPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalEstPrice.Name = "TxtTotalEstPrice";
            this.TxtTotalEstPrice.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalEstPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalEstPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalEstPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalEstPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalEstPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalEstPrice.Properties.ReadOnly = true;
            this.TxtTotalEstPrice.Size = new System.Drawing.Size(166, 20);
            this.TxtTotalEstPrice.TabIndex = 54;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(461, 8);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(123, 14);
            this.label22.TabIndex = 53;
            this.label22.Text = "Total Estimated Price";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProcessInd
            // 
            this.LueProcessInd.EnterMoveNextControl = true;
            this.LueProcessInd.Location = new System.Drawing.Point(149, 68);
            this.LueProcessInd.Margin = new System.Windows.Forms.Padding(5);
            this.LueProcessInd.Name = "LueProcessInd";
            this.LueProcessInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.Appearance.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProcessInd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProcessInd.Properties.DropDownRows = 30;
            this.LueProcessInd.Properties.NullText = "[Empty]";
            this.LueProcessInd.Properties.PopupWidth = 350;
            this.LueProcessInd.Size = new System.Drawing.Size(259, 20);
            this.LueProcessInd.TabIndex = 30;
            this.LueProcessInd.ToolTip = "F4 : Show/hide list";
            this.LueProcessInd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProcessInd.EditValueChanged += new System.EventHandler(this.LueProcessInd_EditValueChanged);
            // 
            // LblProcessInd
            // 
            this.LblProcessInd.AutoSize = true;
            this.LblProcessInd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblProcessInd.ForeColor = System.Drawing.Color.Black;
            this.LblProcessInd.Location = new System.Drawing.Point(95, 71);
            this.LblProcessInd.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblProcessInd.Name = "LblProcessInd";
            this.LblProcessInd.Size = new System.Drawing.Size(48, 14);
            this.LblProcessInd.TabIndex = 29;
            this.LblProcessInd.Text = "Process";
            this.LblProcessInd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(149, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDocNo.TabIndex = 24;
            this.TxtDocNo.Validated += new System.EventHandler(this.TxtLocalDocNo_Validated);
            // 
            // LblPICCode
            // 
            this.LblPICCode.AutoSize = true;
            this.LblPICCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPICCode.ForeColor = System.Drawing.Color.Black;
            this.LblPICCode.Location = new System.Drawing.Point(118, 218);
            this.LblPICCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPICCode.Name = "LblPICCode";
            this.LblPICCode.Size = new System.Drawing.Size(25, 14);
            this.LblPICCode.TabIndex = 43;
            this.LblPICCode.Text = "PIC";
            this.LblPICCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(70, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 23;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePICCode
            // 
            this.LuePICCode.EnterMoveNextControl = true;
            this.LuePICCode.Location = new System.Drawing.Point(149, 215);
            this.LuePICCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePICCode.Name = "LuePICCode";
            this.LuePICCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.Appearance.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePICCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePICCode.Properties.DropDownRows = 30;
            this.LuePICCode.Properties.NullText = "[Empty]";
            this.LuePICCode.Properties.PopupWidth = 300;
            this.LuePICCode.Size = new System.Drawing.Size(300, 20);
            this.LuePICCode.TabIndex = 44;
            this.LuePICCode.ToolTip = "F4 : Show/hide list";
            this.LuePICCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePICCode.EditValueChanged += new System.EventHandler(this.LuePICCode_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(110, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 27;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(149, 47);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(109, 20);
            this.DteDocDt.TabIndex = 28;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            this.DteDocDt.Validated += new System.EventHandler(this.DteDocDt_Validated);
            // 
            // LueReqType
            // 
            this.LueReqType.EnterMoveNextControl = true;
            this.LueReqType.Location = new System.Drawing.Point(149, 110);
            this.LueReqType.Margin = new System.Windows.Forms.Padding(5);
            this.LueReqType.Name = "LueReqType";
            this.LueReqType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.Appearance.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReqType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReqType.Properties.DropDownRows = 30;
            this.LueReqType.Properties.NullText = "[Empty]";
            this.LueReqType.Properties.PopupWidth = 300;
            this.LueReqType.Size = new System.Drawing.Size(300, 20);
            this.LueReqType.TabIndex = 34;
            this.LueReqType.ToolTip = "F4 : Show/hide list";
            this.LueReqType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueReqType.EditValueChanged += new System.EventHandler(this.LueReqType_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(59, 113);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 14);
            this.label3.TabIndex = 33;
            this.label3.Text = "Request Type";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(149, 173);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(300, 20);
            this.LueDeptCode.TabIndex = 40;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            this.LueDeptCode.Validated += new System.EventHandler(this.LueDeptCode_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(70, 176);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 39;
            this.label4.Text = "Department";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(96, 303);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 51;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDORequestDocNo2
            // 
            this.BtnDORequestDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDORequestDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDORequestDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDORequestDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDORequestDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseFont = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnDORequestDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDORequestDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDORequestDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDORequestDocNo2.Location = new System.Drawing.Point(347, 214);
            this.BtnDORequestDocNo2.Name = "BtnDORequestDocNo2";
            this.BtnDORequestDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnDORequestDocNo2.TabIndex = 35;
            this.BtnDORequestDocNo2.ToolTip = "Show DO Request Document";
            this.BtnDORequestDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDORequestDocNo2.ToolTipTitle = "Run System";
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(149, 300);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 2000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(500, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(500, 20);
            this.MeeRemark.TabIndex = 52;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // BtnDORequestDocNo
            // 
            this.BtnDORequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDORequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDORequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDORequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDORequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnDORequestDocNo.Appearance.Options.UseFont = true;
            this.BtnDORequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnDORequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnDORequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDORequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDORequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDORequestDocNo.Location = new System.Drawing.Point(318, 214);
            this.BtnDORequestDocNo.Name = "BtnDORequestDocNo";
            this.BtnDORequestDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnDORequestDocNo.TabIndex = 34;
            this.BtnDORequestDocNo.ToolTip = "Find DO Request Document";
            this.BtnDORequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDORequestDocNo.ToolTipTitle = "Run System";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(46, 282);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 14);
            this.label11.TabIndex = 49;
            this.label11.Text = "Available Budget";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDORequestDocNo
            // 
            this.TxtDORequestDocNo.EnterMoveNextControl = true;
            this.TxtDORequestDocNo.Location = new System.Drawing.Point(149, 236);
            this.TxtDORequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDORequestDocNo.Name = "TxtDORequestDocNo";
            this.TxtDORequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDORequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDORequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDORequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDORequestDocNo.Properties.MaxLength = 250;
            this.TxtDORequestDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDORequestDocNo.TabIndex = 46;
            // 
            // TxtRemainingBudget
            // 
            this.TxtRemainingBudget.EnterMoveNextControl = true;
            this.TxtRemainingBudget.Location = new System.Drawing.Point(149, 279);
            this.TxtRemainingBudget.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemainingBudget.Name = "TxtRemainingBudget";
            this.TxtRemainingBudget.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemainingBudget.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemainingBudget.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseFont = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRemainingBudget.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRemainingBudget.Properties.ReadOnly = true;
            this.TxtRemainingBudget.Size = new System.Drawing.Size(166, 20);
            this.TxtRemainingBudget.TabIndex = 50;
            // 
            // LblDORequestDocNo
            // 
            this.LblDORequestDocNo.AutoSize = true;
            this.LblDORequestDocNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDORequestDocNo.Location = new System.Drawing.Point(61, 239);
            this.LblDORequestDocNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDORequestDocNo.Name = "LblDORequestDocNo";
            this.LblDORequestDocNo.Size = new System.Drawing.Size(82, 14);
            this.LblDORequestDocNo.TabIndex = 45;
            this.LblDORequestDocNo.Text = "DO Request#";
            this.LblDORequestDocNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPOQtyCancelDocNo
            // 
            this.BtnPOQtyCancelDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPOQtyCancelDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPOQtyCancelDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPOQtyCancelDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseBackColor = true;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseFont = true;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseForeColor = true;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnPOQtyCancelDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPOQtyCancelDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPOQtyCancelDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPOQtyCancelDocNo.Location = new System.Drawing.Point(318, 26);
            this.BtnPOQtyCancelDocNo.Name = "BtnPOQtyCancelDocNo";
            this.BtnPOQtyCancelDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnPOQtyCancelDocNo.TabIndex = 16;
            this.BtnPOQtyCancelDocNo.ToolTip = "Find Cancellation PO Document";
            this.BtnPOQtyCancelDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPOQtyCancelDocNo.ToolTipTitle = "Run System";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(43, 260);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 14);
            this.label7.TabIndex = 47;
            this.label7.Text = "Budget Category";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(43, 29);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 14);
            this.label8.TabIndex = 25;
            this.label8.Text = "Cancellation PO#";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBCCode
            // 
            this.LueBCCode.EnterMoveNextControl = true;
            this.LueBCCode.Location = new System.Drawing.Point(149, 257);
            this.LueBCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBCCode.Name = "LueBCCode";
            this.LueBCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.Appearance.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBCCode.Properties.DropDownRows = 30;
            this.LueBCCode.Properties.NullText = "[Empty]";
            this.LueBCCode.Properties.PopupWidth = 300;
            this.LueBCCode.Size = new System.Drawing.Size(300, 20);
            this.LueBCCode.TabIndex = 48;
            this.LueBCCode.ToolTip = "F4 : Show/hide list";
            this.LueBCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBCCode.EditValueChanged += new System.EventHandler(this.LueBCCode_EditValueChanged);
            // 
            // TxtPOQtyCancelDocNo
            // 
            this.TxtPOQtyCancelDocNo.EnterMoveNextControl = true;
            this.TxtPOQtyCancelDocNo.Location = new System.Drawing.Point(149, 26);
            this.TxtPOQtyCancelDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPOQtyCancelDocNo.Name = "TxtPOQtyCancelDocNo";
            this.TxtPOQtyCancelDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPOQtyCancelDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPOQtyCancelDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPOQtyCancelDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPOQtyCancelDocNo.Properties.MaxLength = 16;
            this.TxtPOQtyCancelDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtPOQtyCancelDocNo.TabIndex = 26;
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(115, 134);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 35;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPOQtyCancelDocNo2
            // 
            this.BtnPOQtyCancelDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPOQtyCancelDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPOQtyCancelDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPOQtyCancelDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseFont = true;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnPOQtyCancelDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPOQtyCancelDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPOQtyCancelDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPOQtyCancelDocNo2.Location = new System.Drawing.Point(347, 26);
            this.BtnPOQtyCancelDocNo2.Name = "BtnPOQtyCancelDocNo2";
            this.BtnPOQtyCancelDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnPOQtyCancelDocNo2.TabIndex = 17;
            this.BtnPOQtyCancelDocNo2.ToolTip = "Show Cancellation PO Document";
            this.BtnPOQtyCancelDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPOQtyCancelDocNo2.ToolTipTitle = "Run System";
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(149, 131);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(300, 20);
            this.LueSiteCode.TabIndex = 36;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            this.LueSiteCode.Validated += new System.EventHandler(this.LueSiteCode_Validated);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(48, 92);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(95, 14);
            this.label76.TabIndex = 31;
            this.label76.Text = "Local Document";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(149, 89);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 250;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(300, 20);
            this.TxtLocalDocNo.TabIndex = 32;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.panel6);
            this.Tp2.Name = "Tp2";
            this.Tp2.PageVisible = false;
            this.Tp2.Size = new System.Drawing.Size(766, 325);
            this.Tp2.Text = "For Dropping Request";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.BtnDroppingRequestDocNo2);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.TxtDR_Balance);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Controls.Add(this.TxtDR_MRAmt);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.MeeDR_Remark);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.TxtDR_DroppingRequestAmt);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.TxtDR_BCCode);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.TxtDR_PRJIDocNo);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.TxtDR_Mth);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Controls.Add(this.TxtDR_Yr);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.TxtDR_DeptCode);
            this.panel6.Controls.Add(this.BtnDroppingRequestDocNo);
            this.panel6.Controls.Add(this.TxtDroppingRequestDocNo);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(766, 325);
            this.panel6.TabIndex = 22;
            // 
            // BtnDroppingRequestDocNo2
            // 
            this.BtnDroppingRequestDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDroppingRequestDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDroppingRequestDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDroppingRequestDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDroppingRequestDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnDroppingRequestDocNo2.Appearance.Options.UseFont = true;
            this.BtnDroppingRequestDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnDroppingRequestDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnDroppingRequestDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDroppingRequestDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDroppingRequestDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDroppingRequestDocNo2.Location = new System.Drawing.Point(371, 11);
            this.BtnDroppingRequestDocNo2.Name = "BtnDroppingRequestDocNo2";
            this.BtnDroppingRequestDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnDroppingRequestDocNo2.TabIndex = 15;
            this.BtnDroppingRequestDocNo2.ToolTip = "Find DO Request Document";
            this.BtnDroppingRequestDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDroppingRequestDocNo2.ToolTipTitle = "Run System";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(116, 190);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(48, 14);
            this.label20.TabIndex = 32;
            this.label20.Text = "Balance";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_Balance
            // 
            this.TxtDR_Balance.EnterMoveNextControl = true;
            this.TxtDR_Balance.Location = new System.Drawing.Point(170, 187);
            this.TxtDR_Balance.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_Balance.Name = "TxtDR_Balance";
            this.TxtDR_Balance.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_Balance.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_Balance.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_Balance.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_Balance.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDR_Balance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDR_Balance.Properties.ReadOnly = true;
            this.TxtDR_Balance.Size = new System.Drawing.Size(166, 20);
            this.TxtDR_Balance.TabIndex = 33;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(53, 168);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(111, 14);
            this.label19.TabIndex = 30;
            this.label19.Text = "MR\'s Total Amount";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_MRAmt
            // 
            this.TxtDR_MRAmt.EnterMoveNextControl = true;
            this.TxtDR_MRAmt.Location = new System.Drawing.Point(170, 165);
            this.TxtDR_MRAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_MRAmt.Name = "TxtDR_MRAmt";
            this.TxtDR_MRAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_MRAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_MRAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_MRAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_MRAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDR_MRAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDR_MRAmt.Properties.ReadOnly = true;
            this.TxtDR_MRAmt.Size = new System.Drawing.Size(166, 20);
            this.TxtDR_MRAmt.TabIndex = 31;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(117, 212);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 14);
            this.label18.TabIndex = 34;
            this.label18.Text = "Remark";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeDR_Remark
            // 
            this.MeeDR_Remark.EnterMoveNextControl = true;
            this.MeeDR_Remark.Location = new System.Drawing.Point(170, 209);
            this.MeeDR_Remark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDR_Remark.Name = "MeeDR_Remark";
            this.MeeDR_Remark.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeDR_Remark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.Appearance.Options.UseBackColor = true;
            this.MeeDR_Remark.Properties.Appearance.Options.UseFont = true;
            this.MeeDR_Remark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDR_Remark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDR_Remark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDR_Remark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDR_Remark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDR_Remark.Properties.MaxLength = 400;
            this.MeeDR_Remark.Properties.PopupFormSize = new System.Drawing.Size(500, 20);
            this.MeeDR_Remark.Properties.ShowIcon = false;
            this.MeeDR_Remark.Size = new System.Drawing.Size(500, 20);
            this.MeeDR_Remark.TabIndex = 35;
            this.MeeDR_Remark.ToolTip = "F4 : Show/hide text";
            this.MeeDR_Remark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDR_Remark.ToolTipTitle = "Run System";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(11, 147);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(153, 14);
            this.label16.TabIndex = 26;
            this.label16.Text = "Dropping Request Amount";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_DroppingRequestAmt
            // 
            this.TxtDR_DroppingRequestAmt.EnterMoveNextControl = true;
            this.TxtDR_DroppingRequestAmt.Location = new System.Drawing.Point(170, 143);
            this.TxtDR_DroppingRequestAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_DroppingRequestAmt.Name = "TxtDR_DroppingRequestAmt";
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDR_DroppingRequestAmt.Properties.ReadOnly = true;
            this.TxtDR_DroppingRequestAmt.Size = new System.Drawing.Size(166, 20);
            this.TxtDR_DroppingRequestAmt.TabIndex = 27;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(64, 124);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 14);
            this.label15.TabIndex = 24;
            this.label15.Text = "Budget Category";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_BCCode
            // 
            this.TxtDR_BCCode.EnterMoveNextControl = true;
            this.TxtDR_BCCode.Location = new System.Drawing.Point(170, 121);
            this.TxtDR_BCCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_BCCode.Name = "TxtDR_BCCode";
            this.TxtDR_BCCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_BCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_BCCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_BCCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_BCCode.Properties.MaxLength = 250;
            this.TxtDR_BCCode.Size = new System.Drawing.Size(300, 20);
            this.TxtDR_BCCode.TabIndex = 25;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(19, 102);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(145, 14);
            this.label14.TabIndex = 22;
            this.label14.Text = "Project Implementation#";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_PRJIDocNo
            // 
            this.TxtDR_PRJIDocNo.EnterMoveNextControl = true;
            this.TxtDR_PRJIDocNo.Location = new System.Drawing.Point(170, 99);
            this.TxtDR_PRJIDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_PRJIDocNo.Name = "TxtDR_PRJIDocNo";
            this.TxtDR_PRJIDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_PRJIDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_PRJIDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_PRJIDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_PRJIDocNo.Properties.MaxLength = 250;
            this.TxtDR_PRJIDocNo.Size = new System.Drawing.Size(300, 20);
            this.TxtDR_PRJIDocNo.TabIndex = 23;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(122, 80);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 14);
            this.label13.TabIndex = 20;
            this.label13.Text = "Month";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_Mth
            // 
            this.TxtDR_Mth.EnterMoveNextControl = true;
            this.TxtDR_Mth.Location = new System.Drawing.Point(170, 77);
            this.TxtDR_Mth.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_Mth.Name = "TxtDR_Mth";
            this.TxtDR_Mth.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_Mth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_Mth.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_Mth.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_Mth.Properties.MaxLength = 250;
            this.TxtDR_Mth.Size = new System.Drawing.Size(106, 20);
            this.TxtDR_Mth.TabIndex = 21;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(132, 58);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 14);
            this.label12.TabIndex = 18;
            this.label12.Text = "Year";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_Yr
            // 
            this.TxtDR_Yr.EnterMoveNextControl = true;
            this.TxtDR_Yr.Location = new System.Drawing.Point(170, 55);
            this.TxtDR_Yr.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_Yr.Name = "TxtDR_Yr";
            this.TxtDR_Yr.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_Yr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_Yr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_Yr.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_Yr.Properties.MaxLength = 250;
            this.TxtDR_Yr.Size = new System.Drawing.Size(106, 20);
            this.TxtDR_Yr.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(91, 36);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 14);
            this.label10.TabIndex = 16;
            this.label10.Text = "Department";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_DeptCode
            // 
            this.TxtDR_DeptCode.EnterMoveNextControl = true;
            this.TxtDR_DeptCode.Location = new System.Drawing.Point(170, 33);
            this.TxtDR_DeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_DeptCode.Name = "TxtDR_DeptCode";
            this.TxtDR_DeptCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_DeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_DeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_DeptCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_DeptCode.Properties.MaxLength = 250;
            this.TxtDR_DeptCode.Size = new System.Drawing.Size(300, 20);
            this.TxtDR_DeptCode.TabIndex = 17;
            // 
            // BtnDroppingRequestDocNo
            // 
            this.BtnDroppingRequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDroppingRequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDroppingRequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDroppingRequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDroppingRequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnDroppingRequestDocNo.Appearance.Options.UseFont = true;
            this.BtnDroppingRequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnDroppingRequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnDroppingRequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDroppingRequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDroppingRequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDroppingRequestDocNo.Location = new System.Drawing.Point(342, 11);
            this.BtnDroppingRequestDocNo.Name = "BtnDroppingRequestDocNo";
            this.BtnDroppingRequestDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnDroppingRequestDocNo.TabIndex = 14;
            this.BtnDroppingRequestDocNo.ToolTip = "Find DO Request Document";
            this.BtnDroppingRequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDroppingRequestDocNo.ToolTipTitle = "Run System";
            // 
            // TxtDroppingRequestDocNo
            // 
            this.TxtDroppingRequestDocNo.EnterMoveNextControl = true;
            this.TxtDroppingRequestDocNo.Location = new System.Drawing.Point(170, 11);
            this.TxtDroppingRequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDroppingRequestDocNo.Name = "TxtDroppingRequestDocNo";
            this.TxtDroppingRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDroppingRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDroppingRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDroppingRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDroppingRequestDocNo.Properties.MaxLength = 30;
            this.TxtDroppingRequestDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDroppingRequestDocNo.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(50, 14);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 14);
            this.label9.TabIndex = 12;
            this.label9.Text = "Dropping Request#";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Appearance.PageClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.Tp3.Appearance.PageClient.Options.UseBackColor = true;
            this.Tp3.Controls.Add(this.panel4);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(758, 325);
            this.Tp3.Text = "Upload File";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.ChkFile3);
            this.panel4.Controls.Add(this.TxtFile);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.PbUpload2);
            this.panel4.Controls.Add(this.BtnDownload3);
            this.panel4.Controls.Add(this.ChkFile);
            this.panel4.Controls.Add(this.PbUpload3);
            this.panel4.Controls.Add(this.BtnFile3);
            this.panel4.Controls.Add(this.TxtFile2);
            this.panel4.Controls.Add(this.BtnDownload);
            this.panel4.Controls.Add(this.ChkFile2);
            this.panel4.Controls.Add(this.TxtFile3);
            this.panel4.Controls.Add(this.BtnFile);
            this.panel4.Controls.Add(this.BtnDownload2);
            this.panel4.Controls.Add(this.BtnFile2);
            this.panel4.Controls.Add(this.PbUpload);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(758, 325);
            this.panel4.TabIndex = 0;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(27, 105);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(24, 14);
            this.label21.TabIndex = 24;
            this.label21.Text = "File";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkFile3
            // 
            this.ChkFile3.Location = new System.Drawing.Point(451, 101);
            this.ChkFile3.Name = "ChkFile3";
            this.ChkFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile3.Properties.Appearance.Options.UseFont = true;
            this.ChkFile3.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile3.Properties.Caption = " ";
            this.ChkFile3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile3.Size = new System.Drawing.Size(20, 22);
            this.ChkFile3.TabIndex = 26;
            this.ChkFile3.ToolTip = "Remove File";
            this.ChkFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile3.ToolTipTitle = "Run System";
            this.ChkFile3.CheckedChanged += new System.EventHandler(this.ChkFile3_CheckedChanged);
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(56, 16);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(390, 20);
            this.TxtFile.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(27, 19);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 14);
            this.label6.TabIndex = 12;
            this.label6.Text = "File";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(27, 62);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(24, 14);
            this.label17.TabIndex = 18;
            this.label17.Text = "File";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PbUpload2
            // 
            this.PbUpload2.Location = new System.Drawing.Point(56, 82);
            this.PbUpload2.Name = "PbUpload2";
            this.PbUpload2.Size = new System.Drawing.Size(390, 17);
            this.PbUpload2.TabIndex = 23;
            // 
            // BtnDownload3
            // 
            this.BtnDownload3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload3.Appearance.Options.UseBackColor = true;
            this.BtnDownload3.Appearance.Options.UseFont = true;
            this.BtnDownload3.Appearance.Options.UseForeColor = true;
            this.BtnDownload3.Appearance.Options.UseTextOptions = true;
            this.BtnDownload3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload3.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload3.Image")));
            this.BtnDownload3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload3.Location = new System.Drawing.Point(510, 101);
            this.BtnDownload3.Name = "BtnDownload3";
            this.BtnDownload3.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload3.TabIndex = 28;
            this.BtnDownload3.ToolTip = "DownloadFile";
            this.BtnDownload3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload3.ToolTipTitle = "Run System";
            this.BtnDownload3.Click += new System.EventHandler(this.BtnDownload3_Click);
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(451, 17);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 14;
            this.ChkFile.ToolTip = "Remove File";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // PbUpload3
            // 
            this.PbUpload3.Location = new System.Drawing.Point(56, 125);
            this.PbUpload3.Name = "PbUpload3";
            this.PbUpload3.Size = new System.Drawing.Size(390, 17);
            this.PbUpload3.TabIndex = 29;
            // 
            // BtnFile3
            // 
            this.BtnFile3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile3.Appearance.Options.UseBackColor = true;
            this.BtnFile3.Appearance.Options.UseFont = true;
            this.BtnFile3.Appearance.Options.UseForeColor = true;
            this.BtnFile3.Appearance.Options.UseTextOptions = true;
            this.BtnFile3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile3.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile3.Image")));
            this.BtnFile3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile3.Location = new System.Drawing.Point(479, 101);
            this.BtnFile3.Name = "BtnFile3";
            this.BtnFile3.Size = new System.Drawing.Size(24, 21);
            this.BtnFile3.TabIndex = 27;
            this.BtnFile3.ToolTip = "BrowseFile";
            this.BtnFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile3.ToolTipTitle = "Run System";
            this.BtnFile3.Click += new System.EventHandler(this.BtnFile3_Click);
            // 
            // TxtFile2
            // 
            this.TxtFile2.EnterMoveNextControl = true;
            this.TxtFile2.Location = new System.Drawing.Point(56, 59);
            this.TxtFile2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile2.Name = "TxtFile2";
            this.TxtFile2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile2.Properties.Appearance.Options.UseFont = true;
            this.TxtFile2.Properties.MaxLength = 16;
            this.TxtFile2.Size = new System.Drawing.Size(390, 20);
            this.TxtFile2.TabIndex = 19;
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(510, 17);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 16;
            this.BtnDownload.ToolTip = "DownloadFile";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // ChkFile2
            // 
            this.ChkFile2.Location = new System.Drawing.Point(451, 58);
            this.ChkFile2.Name = "ChkFile2";
            this.ChkFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile2.Properties.Appearance.Options.UseFont = true;
            this.ChkFile2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile2.Properties.Caption = " ";
            this.ChkFile2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile2.Size = new System.Drawing.Size(20, 22);
            this.ChkFile2.TabIndex = 20;
            this.ChkFile2.ToolTip = "Remove File";
            this.ChkFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile2.ToolTipTitle = "Run System";
            this.ChkFile2.CheckedChanged += new System.EventHandler(this.ChkFile2_CheckedChanged);
            // 
            // TxtFile3
            // 
            this.TxtFile3.EnterMoveNextControl = true;
            this.TxtFile3.Location = new System.Drawing.Point(56, 102);
            this.TxtFile3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile3.Name = "TxtFile3";
            this.TxtFile3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile3.Properties.Appearance.Options.UseFont = true;
            this.TxtFile3.Properties.MaxLength = 16;
            this.TxtFile3.Size = new System.Drawing.Size(390, 20);
            this.TxtFile3.TabIndex = 25;
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(479, 17);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(24, 21);
            this.BtnFile.TabIndex = 15;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // BtnDownload2
            // 
            this.BtnDownload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload2.Appearance.Options.UseBackColor = true;
            this.BtnDownload2.Appearance.Options.UseFont = true;
            this.BtnDownload2.Appearance.Options.UseForeColor = true;
            this.BtnDownload2.Appearance.Options.UseTextOptions = true;
            this.BtnDownload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload2.Image")));
            this.BtnDownload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload2.Location = new System.Drawing.Point(510, 58);
            this.BtnDownload2.Name = "BtnDownload2";
            this.BtnDownload2.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload2.TabIndex = 22;
            this.BtnDownload2.ToolTip = "DownloadFile";
            this.BtnDownload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload2.ToolTipTitle = "Run System";
            this.BtnDownload2.Click += new System.EventHandler(this.BtnDownload2_Click);
            // 
            // BtnFile2
            // 
            this.BtnFile2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile2.Appearance.Options.UseBackColor = true;
            this.BtnFile2.Appearance.Options.UseFont = true;
            this.BtnFile2.Appearance.Options.UseForeColor = true;
            this.BtnFile2.Appearance.Options.UseTextOptions = true;
            this.BtnFile2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile2.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile2.Image")));
            this.BtnFile2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile2.Location = new System.Drawing.Point(479, 58);
            this.BtnFile2.Name = "BtnFile2";
            this.BtnFile2.Size = new System.Drawing.Size(24, 21);
            this.BtnFile2.TabIndex = 21;
            this.BtnFile2.ToolTip = "BrowseFile";
            this.BtnFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile2.ToolTipTitle = "Run System";
            this.BtnFile2.Click += new System.EventHandler(this.BtnFile2_Click);
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(56, 39);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(390, 17);
            this.PbUpload.TabIndex = 17;
            // 
            // FrmMaterialRequest5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 501);
            this.Name = "FrmMaterialRequest5";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDurationUom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDivCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalEstPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcessInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePICCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReqType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDORequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOQtyCancelDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Balance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_MRAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDR_Remark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_DroppingRequestAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_BCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_PRJIDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Mth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Yr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_DeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDroppingRequestDocNo.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteUsageDt;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.SaveFileDialog SFD;
        private DevExpress.XtraEditors.LookUpEdit LueDurationUom;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.LookUpEdit LueProcessInd;
        private System.Windows.Forms.Label LblProcessInd;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label LblPICCode;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.LookUpEdit LuePICCode;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        internal DevExpress.XtraEditors.LookUpEdit LueReqType;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.SimpleButton BtnDORequestDocNo2;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        public DevExpress.XtraEditors.SimpleButton BtnDORequestDocNo;
        private System.Windows.Forms.Label label11;
        protected internal DevExpress.XtraEditors.TextEdit TxtDORequestDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtRemainingBudget;
        private System.Windows.Forms.Label LblDORequestDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnPOQtyCancelDocNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.LookUpEdit LueBCCode;
        internal DevExpress.XtraEditors.TextEdit TxtPOQtyCancelDocNo;
        private System.Windows.Forms.Label LblSiteCode;
        public DevExpress.XtraEditors.SimpleButton BtnPOQtyCancelDocNo2;
        public DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label label76;
        private DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.Panel panel6;
        public DevExpress.XtraEditors.SimpleButton BtnDroppingRequestDocNo2;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtDR_Balance;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtDR_MRAmt;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.MemoExEdit MeeDR_Remark;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtDR_DroppingRequestAmt;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtDR_BCCode;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtDR_PRJIDocNo;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtDR_Mth;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtDR_Yr;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtDR_DeptCode;
        public DevExpress.XtraEditors.SimpleButton BtnDroppingRequestDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtDroppingRequestDocNo;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.CheckEdit ChkFile3;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ProgressBar PbUpload2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload3;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        private System.Windows.Forms.ProgressBar PbUpload3;
        public DevExpress.XtraEditors.SimpleButton BtnFile3;
        internal DevExpress.XtraEditors.TextEdit TxtFile2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private DevExpress.XtraEditors.CheckEdit ChkFile2;
        internal DevExpress.XtraEditors.TextEdit TxtFile3;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        public DevExpress.XtraEditors.SimpleButton BtnDownload2;
        public DevExpress.XtraEditors.SimpleButton BtnFile2;
        private System.Windows.Forms.ProgressBar PbUpload;
        internal DevExpress.XtraEditors.TextEdit TxtTotalEstPrice;
        private System.Windows.Forms.Label label23;
        public DevExpress.XtraEditors.LookUpEdit LueDivCode;
        public DevExpress.XtraEditors.LookUpEdit LueDeptCode2;
        private System.Windows.Forms.Label LblDeptCode2;
        public DevExpress.XtraEditors.SimpleButton BtnBCCode;
        public DevExpress.XtraEditors.SimpleButton BtnCopyGeneral;
        private System.Windows.Forms.Label LblCopyGeneral;
    }
}