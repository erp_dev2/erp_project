﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptBudgetManagement : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptBudgetManagement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                string CurrentDate = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDate.Substring(0, 4));
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                SetGrd();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 40;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "Department"+Environment.NewLine+"Code", 
                        "Department",
                        "Budget Of Year",
                        "Budget Of Month"+Environment.NewLine+"January",
                        "Material Request"+Environment.NewLine+"January",
                        //6-10
                        "Purchase Order"+Environment.NewLine+"January",
                        "Budget Of Month"+Environment.NewLine+"February",
                        "Material Request"+Environment.NewLine+"February",
                        "Purchase Order"+Environment.NewLine+"February",
                        "Budget Of Month"+Environment.NewLine+"March",
                        //11-15
                        "Material Request"+Environment.NewLine+"March",
                        "Purchase Order"+Environment.NewLine+"March",
                        "Budget Of Month"+Environment.NewLine+"April",
                        "Material Request"+Environment.NewLine+"April",
                        "Purchase Order"+Environment.NewLine+"April",
                        //16-20
                        "Budget Of Month"+Environment.NewLine+"May",
                        "Material Request"+Environment.NewLine+"May",
                        "Purchase Order"+Environment.NewLine+"May",
                        "Budget Of Month"+Environment.NewLine+"June",
                        "Material Request"+Environment.NewLine+"June",
                        //21-25
                        "Purchase Order"+Environment.NewLine+"June",
                        "Budget Of Month"+Environment.NewLine+"July",
                        "Material Request"+Environment.NewLine+"July",
                        "Purchase Order"+Environment.NewLine+"July",
                        "Budget Of Month"+Environment.NewLine+"August",
                        //26-30
                        "Material Request"+Environment.NewLine+"August",
                        "Purchase Order"+Environment.NewLine+"August",
                        "Budget Of Month"+Environment.NewLine+"September",
                        "Material Request"+Environment.NewLine+"September",
                        "Purchase Order"+Environment.NewLine+"September",
                        //31-35
                        "Budget Of Month"+Environment.NewLine+"October",
                        "Material Request"+Environment.NewLine+"October",
                        "Purchase Order"+Environment.NewLine+"October",
                        "Budget Of Month"+Environment.NewLine+"November",
                        "Material Request"+Environment.NewLine+"November",
                        //36-39
                        "Purchase Order"+Environment.NewLine+"November",
                        "Budget Of Month"+Environment.NewLine+"December",
                        "Material Request"+Environment.NewLine+"December",
                        "Purchase Order"+Environment.NewLine+"December",
                        
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        80, 150, 120, 120, 120,
                        //6-10
                        120, 120, 120, 120, 120,
                        //11-15
                        120, 120, 120, 120, 120,
                        //16-20
                        120, 120, 120, 120, 120,
                        //21-25
                        120, 120, 120, 120, 120,
                        //26-30
                        120, 120, 120, 120, 120,
                        //31-35
                        120, 120, 120, 120, 120,
                        //36-39
                        120, 120, 120, 120
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 
            16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39 }, 0);
            //Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 12, 13, 16, 17, 20, 21 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        private bool IsFilterByDateInvalid()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return true;
            if (Sm.IsLueEmpty(LueSiteCode, "Site")) return true;

            return false;
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueSiteCode, "Site")) return;
            
            var LSI = new List<L1>();

            try
            {
                Process1(ref LSI);
                Process2(ref LSI);
                Process3(ref LSI);
                Process4(ref LSI);
                if (Grd1.Rows.Count == 0)
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void Process1(ref List<L1> LP1)
        {
            LP1.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));


            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.Yr, A.DeptCode, B.DeptName, SUM(A.Amt) Amt ");
            SQL.AppendLine("    From tblBudget A ");
            SQL.AppendLine("    Inner Join tblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("    Inner Join TblDepartmentBudgetSite C On A.deptCode = C.DeptCode  ");
            SQL.AppendLine("    Where A.Amt >0 And A.Yr=@Yr And C.SiteCode=@SiteCode ");
            SQL.AppendLine("    Group BY Yr, DeptCode ");
            SQL.AppendLine(")Z ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Yr",
                    //1-3
                    "DeptCode", "DeptName", "Amt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LP1.Add(new L1()
                        {
                            Yr = Sm.DrStr(dr, c[0]), 
                            DeptCode = Sm.DrStr(dr, c[1]),
                            DeptName = Sm.DrStr(dr, c[2]),
                            BudgetOfYear = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<L1> LP1)
        {
            int Temp = 0;

            //LP2.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string Filter = string.Empty;

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));

            SQL.AppendLine("Select X2.Yr, X2.DeptCode, ");
            SQL.AppendLine("Sum(Mth01) As Mth01, Sum(Mth02) As Mth02, Sum(Mth03) As Mth03, Sum(Mth04) As Mth04, ");
            SQL.AppendLine("Sum(Mth05) As Mth05, Sum(Mth06) As Mth06, Sum(Mth07) As Mth07, Sum(Mth08) As Mth08, ");
            SQL.AppendLine("Sum(Mth09) As Mth09, Sum(Mth10) As Mth10, Sum(Mth11) As Mth11, Sum(Mth12) As Mth12 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select X.Yr, X.DeptCode, ");
            SQL.AppendLine("    Case Mth When '01' Then Amt Else 0.0000 End As Mth01, ");
            SQL.AppendLine("    Case Mth When '02' Then Amt Else 0.0000 End As Mth02, ");
            SQL.AppendLine("    Case Mth When '03' Then Amt Else 0.0000 End As Mth03, ");
            SQL.AppendLine("    Case Mth When '04' Then Amt Else 0.0000 End As Mth04, ");
            SQL.AppendLine("    Case Mth When '05' Then Amt Else 0.0000 End As Mth05, ");
            SQL.AppendLine("    Case Mth When '06' Then Amt Else 0.0000 End As Mth06, ");
            SQL.AppendLine("    Case Mth When '07' Then Amt Else 0.0000 End As Mth07, ");
            SQL.AppendLine("    Case Mth When '08' Then Amt Else 0.0000 End As Mth08, ");
            SQL.AppendLine("    Case Mth When '09' Then Amt Else 0.0000 End As Mth09, ");
            SQL.AppendLine("    Case Mth When '10' Then Amt Else 0.0000 End As Mth10, ");
            SQL.AppendLine("    Case Mth When '11' Then Amt Else 0.0000 End As Mth11, ");
            SQL.AppendLine("    Case Mth When '12' Then Amt Else 0.0000 End As Mth12 ");
            SQL.AppendLine("    From (  ");
	        SQL.AppendLine("        Select A.Yr, A.DeptCode, A.Mth, A.Amt  ");
	        SQL.AppendLine("        From TblBudget A ");
            SQL.AppendLine("        Inner Join TblDepartmentBudgetSite B On A.deptCode = B.DeptCode  ");
	        SQL.AppendLine("        Left Join ( ");
		    SQL.AppendLine("            Select convert('01' using latin1) As Mth Union All ");
		    SQL.AppendLine("            Select convert('02' using latin1) Union All ");
		    SQL.AppendLine("            Select convert('03' using latin1) Union All ");
		    SQL.AppendLine("            Select convert('04' using latin1) Union All ");
		    SQL.AppendLine("            Select convert('05' using latin1) Union All ");
		    SQL.AppendLine("            Select convert('06' using latin1) Union All ");
		    SQL.AppendLine("            Select convert('07' using latin1) Union All ");
		    SQL.AppendLine("            Select convert('08' using latin1) Union All ");
		    SQL.AppendLine("            Select convert('09' using latin1) Union All ");
		    SQL.AppendLine("            Select convert('10' using latin1) Union All ");
		    SQL.AppendLine("            Select convert('11' using latin1) Union All ");
		    SQL.AppendLine("            Select convert('12' using latin1) ");
	        SQL.AppendLine("            ) B On A.mth=B.mth ");
	        SQL.AppendLine("    Where A.Amt>0 And A.Yr=@Yr And B.SiteCode=@SiteCode ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine(")X2 ");
            SQL.AppendLine("Group By X2.Yr, X2.DeptCode ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Yr",
                    //1-5
                    "DeptCode", "Mth01", "Mth02", "Mth03", "Mth04",
                    //6-10
                    "Mth05", "Mth06", "Mth07", "Mth08", "Mth09",
                    //11-13
                    "Mth10", "Mth11", "Mth12", 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        for (var i = Temp; i < LP1.Count; i++)
                        {
                            if (string.Compare(LP1[i].DeptCode, dr.GetString(1)) == 0 && string.Compare(LP1[i].Yr, dr.GetString(0)) == 0)
                            {
                                LP1[i].DeptCode = LP1[i].DeptCode;
                                LP1[i].MthJan = dr.GetDecimal(c[2]);
                                LP1[i].MthFeb = dr.GetDecimal(c[3]);
                                LP1[i].MthMar = dr.GetDecimal(c[4]);
                                LP1[i].MthApr = dr.GetDecimal(c[5]);
                                LP1[i].MthMay = dr.GetDecimal(c[6]);
                                LP1[i].MthJun = dr.GetDecimal(c[7]);
                                LP1[i].MthJul = dr.GetDecimal(c[8]);
                                LP1[i].MthAug = dr.GetDecimal(c[9]);
                                LP1[i].MthSep = dr.GetDecimal(c[10]);
                                LP1[i].MthOct = dr.GetDecimal(c[11]); 
                                LP1[i].MthNov = dr.GetDecimal(c[12]);
                                LP1[i].MthDec = dr.GetDecimal(c[13]);
                                Temp = i;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<L1> LP1)
        {
            int Temp = 0;

            //LP2.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string Filter = string.Empty;

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));

            SQL.AppendLine("Select Y1.Yr, Y1.DeptCode, ");
            SQL.AppendLine("Sum(Mth01) As Mth01, Sum(Mth02) As Mth02, Sum(Mth03) As Mth03, Sum(Mth04) As Mth04, ");
            SQL.AppendLine("Sum(Mth05) As Mth05, Sum(Mth06) As Mth06, Sum(Mth07) As Mth07, Sum(Mth08) As Mth08, ");
            SQL.AppendLine("Sum(Mth09) As Mth09, Sum(Mth10) As Mth10, Sum(Mth11) As Mth11, Sum(Mth12) As Mth12 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select X1.DeptCode, X1.Yr, X1.Mth, ");
            SQL.AppendLine("    Case Mth When '01' Then Amt Else 0.0000 End As Mth01, ");
            SQL.AppendLine("    Case Mth When '02' Then Amt Else 0.0000 End As Mth02, ");
            SQL.AppendLine("    Case Mth When '03' Then Amt Else 0.0000 End As Mth03, ");
            SQL.AppendLine("    Case Mth When '04' Then Amt Else 0.0000 End As Mth04, ");
            SQL.AppendLine("    Case Mth When '05' Then Amt Else 0.0000 End As Mth05,  ");
            SQL.AppendLine("    Case Mth When '06' Then Amt Else 0.0000 End As Mth06, ");
            SQL.AppendLine("    Case Mth When '07' Then Amt Else 0.0000 End As Mth07,  ");
            SQL.AppendLine("    Case Mth When '08' Then Amt Else 0.0000 End As Mth08,  ");
            SQL.AppendLine("    Case Mth When '09' Then Amt Else 0.0000 End As Mth09, ");
            SQL.AppendLine("    Case Mth When '10' Then Amt Else 0.0000 End As Mth10, ");
            SQL.AppendLine("    Case Mth When '11' Then Amt Else 0.0000 End As Mth11, ");
            SQL.AppendLine("    Case Mth When '12' Then Amt Else 0.0000 End As Mth12 ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.DeptCode, left(A.DocDt, 4) As Yr,  ");
            SQL.AppendLine("        Substring(A.DocDt, 5, 2) As Mth, (B.Qty * B.UPrice) As Amt ");
            SQL.AppendLine("        From TblMaterialRequesthdr A  ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("	            Select convert('01' using latin1) As Mth Union All ");
            SQL.AppendLine("	            Select convert('02' using latin1) Union All ");
            SQL.AppendLine("	            Select convert('03' using latin1) Union All ");
            SQL.AppendLine("	            Select convert('04' using latin1) Union All ");
            SQL.AppendLine("	            Select convert('05' using latin1) Union All ");
            SQL.AppendLine("	            Select convert('06' using latin1) Union All ");
            SQL.AppendLine("	            Select convert('07' using latin1) Union All ");
            SQL.AppendLine("	            Select convert('08' using latin1) Union All ");
            SQL.AppendLine("	            Select convert('09' using latin1) Union All ");
            SQL.AppendLine("	            Select convert('10' using latin1) Union All ");
            SQL.AppendLine("	            Select convert('11' using latin1) Union All ");
            SQL.AppendLine("	            Select convert('12' using latin1) ");
            SQL.AppendLine("            ) C On Substring(A.DocDt, 5, 2)=C.mth ");
            SQL.AppendLine("        Where left(A.DocDt, 4)=@Yr And A.SiteCode=@SiteCode And A.cancelInd = 'N' And A.Status = 'A' And B.cancelInd = 'N' And B.Status = 'A'  ");
            SQL.AppendLine("        And A.SiteCode is not null ");
            SQL.AppendLine("    )X1 ");
            SQL.AppendLine(")Y1 ");
            SQL.AppendLine("Where Y1.Yr=@Yr ");
            SQL.AppendLine("Group By Y1.Yr, Y1.DeptCode ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Yr",
                    //1-5
                    "DeptCode", "Mth01", "Mth02", "Mth03", "Mth04",
                    //6-10
                    "Mth05", "Mth06", "Mth07", "Mth08", "Mth09",
                    //11-13
                    "Mth10", "Mth11", "Mth12", 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        for (var i = Temp; i < LP1.Count; i++)
                        {
                            if (string.Compare(LP1[i].DeptCode, dr.GetString(1)) == 0 && string.Compare(LP1[i].Yr, dr.GetString(0)) == 0)
                            {
                                LP1[i].DeptCode = LP1[i].DeptCode;
                                LP1[i].MRJan = dr.GetDecimal(c[2]);
                                LP1[i].MRFeb = dr.GetDecimal(c[3]);
                                LP1[i].MRMar = dr.GetDecimal(c[4]);
                                LP1[i].MRApr = dr.GetDecimal(c[5]);
                                LP1[i].MRMay = dr.GetDecimal(c[6]);
                                LP1[i].MRJun = dr.GetDecimal(c[7]);
                                LP1[i].MRJul = dr.GetDecimal(c[8]);
                                LP1[i].MRAug = dr.GetDecimal(c[9]);
                                LP1[i].MRSep = dr.GetDecimal(c[10]);
                                LP1[i].MROct = dr.GetDecimal(c[11]);
                                LP1[i].MRNov = dr.GetDecimal(c[12]);
                                LP1[i].MRDec = dr.GetDecimal(c[13]);
                                Temp = i;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process4(ref List<L1> LP1)
        {
            int Temp = 0;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));

            SQL.AppendLine("Select Y1.Yr, Y1.DeptCode, ");
            SQL.AppendLine("Sum(Mth01) As Mth01, Sum(Mth02) As Mth02, Sum(Mth03) As Mth03, Sum(Mth04) As Mth04, ");
            SQL.AppendLine("Sum(Mth05) As Mth05, Sum(Mth06) As Mth06, Sum(Mth07) As Mth07, Sum(Mth08) As Mth08, ");
            SQL.AppendLine("Sum(Mth09) As Mth09, Sum(Mth10) As Mth10, Sum(Mth11) As Mth11, Sum(Mth12) As Mth12 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select X1.DeptCode, X1.Yr, X1.Mth, ");
	        SQL.AppendLine("    Case Mth When '01' Then Amt Else 0.0000 End As Mth01, ");
	        SQL.AppendLine("    Case Mth When '02' Then Amt Else 0.0000 End As Mth02, ");
	        SQL.AppendLine("    Case Mth When '03' Then Amt Else 0.0000 End As Mth03, ");
	        SQL.AppendLine("    Case Mth When '04' Then Amt Else 0.0000 End As Mth04, ");
	        SQL.AppendLine("    Case Mth When '05' Then Amt Else 0.0000 End As Mth05, ");
	        SQL.AppendLine("    Case Mth When '06' Then Amt Else 0.0000 End As Mth06, ");
	        SQL.AppendLine("    Case Mth When '07' Then Amt Else 0.0000 End As Mth07, ");
	        SQL.AppendLine("    Case Mth When '08' Then Amt Else 0.0000 End As Mth08, ");
	        SQL.AppendLine("    Case Mth When '09' Then Amt Else 0.0000 End As Mth09, ");
	        SQL.AppendLine("    Case Mth When '10' Then Amt Else 0.0000 End As Mth10, ");
	        SQL.AppendLine("    Case Mth When '11' Then Amt Else 0.0000 End As Mth11, ");
	        SQL.AppendLine("    Case Mth When '12' Then Amt Else 0.0000 End As Mth12 ");
	        SQL.AppendLine("    From ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select distinct E.DeptCode, left(E.DocDt, 4) As Yr, "); 
		    SQL.AppendLine("        Substring(E.DocDt, 5, 2) As Mth, A.Amt As Amt  ");
		    SQL.AppendLine("        From TblPOHdr A ");
		    SQL.AppendLine("        Inner Join TblPODtl B On A.Docno = B.DocnO ");
		    SQL.AppendLine("        Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
		    SQL.AppendLine("        Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
		    SQL.AppendLine("        Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
		    SQL.AppendLine("        Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo ");
		    SQL.AppendLine("        Left Join ( ");
			SQL.AppendLine("			            Select convert('01' using latin1) As Mth Union All ");
			SQL.AppendLine("			            Select convert('02' using latin1) Union All ");
			SQL.AppendLine("			            Select convert('03' using latin1) Union All ");
			SQL.AppendLine("			            Select convert('04' using latin1) Union All ");
			SQL.AppendLine("			            Select convert('05' using latin1) Union All ");
			SQL.AppendLine("			            Select convert('06' using latin1) Union All ");
			SQL.AppendLine("			            Select convert('07' using latin1) Union All ");
			SQL.AppendLine("			            Select convert('08' using latin1) Union All ");
			SQL.AppendLine("			            Select convert('09' using latin1) Union All ");
			SQL.AppendLine("			            Select convert('10' using latin1) Union All ");
			SQL.AppendLine("			            Select convert('11' using latin1) Union All ");
			SQL.AppendLine("			            Select convert('12' using latin1) ");
			SQL.AppendLine("                    ) G On Substring(E.DocDt, 5, 2)=G.mth ");
            SQL.AppendLine("        Where left(A.DocDt, 4)=@Yr And A.SiteCode=@SiteCode And B.CancelInd = 'N' And F.CancelInd = 'N' ");
            SQL.AppendLine("        And E.cancelInd = 'N' And E.Status = 'A' And E.cancelInd = 'N' And F.Status = 'A' And E.SiteCode is not null ");
	        SQL.AppendLine("    )X1 ");
            SQL.AppendLine(")Y1 ");
            SQL.AppendLine("Group By Y1.Yr, Y1.DeptCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString() + " ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Yr",  

                    //1-5
                    "DeptCode", "Mth01", "Mth02", "Mth03", "Mth04",
                    //6-10
                    "Mth05", "Mth06", "Mth07", "Mth08", "Mth09",
                    //11-13
                    "Mth10", "Mth11", "Mth12",
                });
                if (dr.HasRows)
                {
                    int r = 0;

                    while (dr.Read())
                    {
                        for (var i = Temp; i < LP1.Count; i++)
                        {
                            if (LP1[i].DeptCode == dr.GetString(1) && LP1[i].Yr == dr.GetString(0))
                            {
                                r = r + 1;
                                Grd1.Rows.Add();
                                int Row = Grd1.Rows.Count - 1;
                                Grd1.Cells[Row, 0].Value = r + 1;
                                Grd1.Cells[Row, 1].Value = LP1[i].DeptCode;
                                Grd1.Cells[Row, 2].Value = LP1[i].DeptName;
                                Grd1.Cells[Row, 3].Value = LP1[i].BudgetOfYear;
                                Grd1.Cells[Row, 4].Value = LP1[i].MthJan;
                                Grd1.Cells[Row, 5].Value = LP1[i].MRJan;
                                Grd1.Cells[Row, 6].Value = dr.GetDecimal(c[2]);

                                Grd1.Cells[Row, 7].Value = LP1[i].MthFeb;
                                Grd1.Cells[Row, 8].Value = LP1[i].MRFeb;
                                Grd1.Cells[Row, 9].Value = dr.GetDecimal(c[3]);

                                Grd1.Cells[Row, 10].Value = LP1[i].MthMar;
                                Grd1.Cells[Row, 11].Value = LP1[i].MRMar;
                                Grd1.Cells[Row, 12].Value = dr.GetDecimal(c[4]);

                                Grd1.Cells[Row, 13].Value = LP1[i].MthApr;
                                Grd1.Cells[Row, 14].Value = LP1[i].MRApr;
                                Grd1.Cells[Row, 15].Value = dr.GetDecimal(c[5]);

                                Grd1.Cells[Row, 16].Value = LP1[i].MthMay;
                                Grd1.Cells[Row, 17].Value = LP1[i].MRMay;
                                Grd1.Cells[Row, 18].Value = dr.GetDecimal(c[6]);

                                Grd1.Cells[Row, 19].Value = LP1[i].MthJun;
                                Grd1.Cells[Row, 20].Value = LP1[i].MRJun;
                                Grd1.Cells[Row, 21].Value = dr.GetDecimal(c[7]);

                                Grd1.Cells[Row, 22].Value = LP1[i].MthJul;
                                Grd1.Cells[Row, 23].Value = LP1[i].MRJul;
                                Grd1.Cells[Row, 24].Value = dr.GetDecimal(c[8]);

                                Grd1.Cells[Row, 25].Value = LP1[i].MthAug;
                                Grd1.Cells[Row, 26].Value = LP1[i].MRAug;
                                Grd1.Cells[Row, 27].Value = dr.GetDecimal(c[9]);

                                Grd1.Cells[Row, 28].Value = LP1[i].MthSep;
                                Grd1.Cells[Row, 29].Value = LP1[i].MRSep;
                                Grd1.Cells[Row, 30].Value = dr.GetDecimal(c[10]);

                                Grd1.Cells[Row, 31].Value = LP1[i].MthOct;
                                Grd1.Cells[Row, 32].Value = LP1[i].MROct;
                                Grd1.Cells[Row, 33].Value = dr.GetDecimal(c[11]);

                                Grd1.Cells[Row, 34].Value = LP1[i].MthNov;
                                Grd1.Cells[Row, 35].Value = LP1[i].MRNov;
                                Grd1.Cells[Row, 36].Value = dr.GetDecimal(c[12]);

                                Grd1.Cells[Row, 37].Value = LP1[i].MthDec;
                                Grd1.Cells[Row, 38].Value = LP1[i].MRDec;
                                Grd1.Cells[Row, 39].Value = dr.GetDecimal(c[13]);
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }


        #endregion

        #endregion

        #region Events

        #region Misc Control Events
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode), string.Empty);
        }
        #endregion

        #endregion

        #region Class

        private class L1
        {
            public string Yr { get; set; }
            public string DeptCode{ get; set; }
            public string DeptName { get; set; }
            public decimal BudgetOfYear{ get; set; }
            public decimal MthJan{ get; set; }
            public decimal MRJan{ get; set; }
            public decimal POJan{ get; set; }
            public decimal MthFeb{ get; set; }
            public decimal MRFeb{ get; set; }
            public decimal POFeb{ get; set; }
            public decimal MthMar{ get; set; }
            public decimal MRMar{ get; set; }
            public decimal POMar{ get; set; }
            public decimal MthApr{ get; set; }
            public decimal MRApr{ get; set; }
            public decimal POApr{ get; set; }
            public decimal MthMay{ get; set; }
            public decimal MRMay{ get; set; }
            public decimal POMay{ get; set; }
            public decimal MthJun{ get; set; }
            public decimal MRJun{ get; set; }
            public decimal POJun{ get; set; }
            public decimal MthJul{ get; set; }
            public decimal MRJul{ get; set; }
            public decimal POJul{ get; set; }
            public decimal MthAug{ get; set; }
            public decimal MRAug{ get; set; }
            public decimal POAug{ get; set; }
            public decimal MthSep{ get; set; }
            public decimal MRSep{ get; set; }
            public decimal POSep{ get; set; }
            public decimal MthOct{ get; set; }
            public decimal MROct{ get; set; }
            public decimal POOct{ get; set; }
            public decimal MthNov{ get; set; }
            public decimal MRNov{ get; set; }
            public decimal PONov{ get; set; }
            public decimal MthDec{ get; set; }
            public decimal MRDec{ get; set; }
            public decimal PODec{ get; set; }
        }

        #endregion

     
    }
}
