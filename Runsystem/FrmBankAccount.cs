﻿#region Update
/*
    04/07/2017 [TKG] menambah entity
    05/10/2019 [WED/IOK] salah query delete
    22/01/2020 [DITA/YK] tambah site
    18/02/2021 [BRI/PHT] menambah COA's Account(Cost Center) & Inter Office
 *  09/04/2021 [ICA/SIER] menambah ChkActInd
 *  21/04/2022 [HAR/GSS] tambah bank account type ambil dari option berdasrkn param investment active / tidak
 *  27/05/2022 [HAR/PHT] BUG saat edit untuk param investment tidak aktif bank inputan account name nya tidak terbuka
 *  01/11/2022 [DITA/BBT] penyesuaian show dan save account type saat param = IsModuleInvestmentActive aktif
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBankAccount : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, 
            mAccessInd = string.Empty;
        private bool mIsJournalCreateAdditionalPOS = false;
        private bool mIsBankAccountUseCostCenterAndInterOffice = false;
        private bool mIsModuleInvestmentActive = false;
        internal FrmBankAccountFind FrmFind;
                                                             
        #endregion

        #region Constructor

        public FrmBankAccount(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion                                                 

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
                GetParameter();
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueAcNo(ref LueCoaAcNo);
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueEntCode(ref LueEntCode);
                Sl.SetLueSiteCode(ref LueSiteCode);
                if (!mIsBankAccountUseCostCenterAndInterOffice)
                {
                    LblCOACostCenter.Visible = false;
                    TxtCCCode.Visible = false;
                    TxtCCName.Visible = false;
                    BtnCCCode.Visible = false;
                    LblInterOffice.Visible = false;
                    TxtAcNo.Visible = false;
                    TxtAcDesc.Visible = false;
                    BtnAcNo.Visible = false;
                }

               
                if (!mIsJournalCreateAdditionalPOS) lblSite.ForeColor = Color.Black;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtBankAcCode, ChkHiddenInd, ChkActInd, LueBankCode, TxtBankAcNo, TxtBankAcTp,
                        TxtBankAcNm, MeeRemark, LueCoaAcNo, TxtAutoNoDebit, AutoNoCredit, 
                        LueCurCode, TxtSequence, TxtBranchAcNm, MeeBranchAddress, TxtBranchFax, 
                        TxtBranchPhone, TxtBranchSwiftCode, LueEntCode, LueSiteCode,
                    }, true);
                    BtnCCCode.Enabled = false;
                    BtnAcNo.Enabled = false;
                    BtnBankAcTp.Enabled = false;
                    TxtBankAcCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtBankAcCode, ChkHiddenInd, LueBankCode, TxtBankAcNo,
                         TxtBankAcNm, MeeRemark, LueCoaAcNo, TxtAutoNoDebit, AutoNoCredit,
                         LueCurCode, TxtSequence, TxtBranchAcNm, MeeBranchAddress, TxtBranchFax, 
                         TxtBranchPhone, TxtBranchSwiftCode, LueEntCode, LueSiteCode
                    }, false);
                    BtnCCCode.Enabled = true;
                    BtnAcNo.Enabled = true;
                    if (mIsModuleInvestmentActive)
                    {
                        lblBankAccountType.ForeColor = Color.Red;
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                        {
                             TxtBankAcTp
                        }, true);
                        BtnBankAcTp.Enabled = true;
                    }
                    else
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                        {
                             TxtBankAcTp
                        }, false);
                        BtnBankAcTp.Enabled = false;
                    }
                    TxtBankAcCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkHiddenInd, ChkActInd, LueBankCode, TxtBankAcNo, 
                        MeeRemark, LueCoaAcNo, TxtAutoNoDebit, AutoNoCredit, LueCurCode,
                        TxtSequence, TxtBranchAcNm, MeeBranchAddress, TxtBranchFax, TxtBranchPhone, 
                        TxtBranchSwiftCode, LueEntCode, LueSiteCode
                    }, false);
                    BtnCCCode.Enabled = true;
                    BtnAcNo.Enabled = true;
                    if (mIsModuleInvestmentActive)
                    {
                        lblBankAccountType.ForeColor = Color.Red;
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                        {
                             TxtBankAcTp
                        }, true);
                        BtnBankAcTp.Enabled = true;
                    }
                    else
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                        {
                             TxtBankAcTp, TxtBankAcNm
                        }, false);
                        BtnBankAcTp.Enabled = false;
                    }
                    ChkHiddenInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtBankAcCode, LueBankCode, TxtBankAcNo, TxtBankAcTp, TxtBankAcNm,
                 MeeRemark, LueCoaAcNo, TxtAutoNoDebit, AutoNoCredit, LueCurCode,
                 TxtSequence, TxtBranchAcNm, MeeBranchAddress, TxtBranchFax, TxtBranchPhone, 
                 TxtBranchSwiftCode, LueEntCode, LueSiteCode, TxtCCCode, TxtCCName, TxtAcNo, TxtAcDesc
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtSequence }, 0);
            ChkHiddenInd.Checked = false;
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBankAccountFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            
            SetFormControl(mState.Insert);

            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtBankAcCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtBankAcCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblBankAccount Where BankAcCode=@BankAcCode And BankCode=@BankCode" };
                Sm.CmParam<String>(ref cm, "@BankAcCode", TxtBankAcCode.Text);
                Sm.CmParam<String>(ref cm, "@BankCode", LueBankCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblBankAccount (BankAcCode, HiddenInd, ActInd, BankCode, BankAcNo, BankAcTp, BankAcNm, Remark, CoaAcNo, CCCode, CoaAcNoInterOffice, AutoNoDebit, AutoNoCredit, CurCode, Sequence, BranchAcNm, BranchAcAddress, BranchAcFax, BranchAcPhone, BranchAcSwiftCode, EntCode, SiteCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@BankAcCode, @HiddenInd, @ActInd, @BankCode, @BankAcNo, @BankAcTp, @BankAcNm, @Remark, @CoaAcNo, @CCCode, @CoaAcNoInterOffice, @AutoNoDebit, @AutoNoCredit, @CurCode, @Sequence, @BranchAcNm, @BranchAcAddress, @BranchAcFax, @BranchAcPhone, @BranchAcSwiftCode, @EntCode, @SiteCode,@UserCode,CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update HiddenInd=@HiddenInd, ActInd=@ActInd, BankCode=@BankCode, BankAcNo=@BankAcNo, BankAcTp=@BankAcTp, BankAcNm=@BankAcNm, Remark=@Remark, CoaAcNo=@CoaAcNo, CCCode=@CCCode, CoaAcNoInterOffice=@CoaAcNoInterOffice, AutoNoDebit=@AutoNoDebit, AutoNoCredit=@AutoNoCredit, CurCode=@CurCode, Sequence=@Sequence, ");
                SQL.AppendLine("   BranchAcNm=@BranchAcNm, BranchAcAddress=@BranchAcAddress, BranchAcFax=@BranchAcFax, BranchACPhone=@BranchACPhone, BranchACSwiftCode=@BranchACSwiftCode, EntCode=@EntCode, SiteCode=@SiteCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@BankAcCode", TxtBankAcCode.Text);
                Sm.CmParam<String>(ref cm, "@HiddenInd", ChkHiddenInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
                Sm.CmParam<String>(ref cm, "@BankAcNo", TxtBankAcNo.Text);
                if(!mIsModuleInvestmentActive)
                    Sm.CmParam<String>(ref cm, "@BankAcTp", TxtBankAcTp.Text);
                else
                    Sm.CmParam<String>(ref cm, "@BankAcTp", TxtBankAcTpCode.Text);
                Sm.CmParam<String>(ref cm, "@BankAcNm", TxtBankAcNm.Text);
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@CoaAcNo", Sm.GetLue(LueCoaAcNo));
                Sm.CmParam<String>(ref cm, "@CCCode", TxtCCCode.Text);
                Sm.CmParam<String>(ref cm, "@CoaAcNoInterOffice", TxtAcNo.Text);
                Sm.CmParam<String>(ref cm, "@AutoNoDebit", TxtAutoNoDebit.Text);
                Sm.CmParam<String>(ref cm, "@AutoNoCredit", AutoNoCredit.Text);
                Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
                Sm.CmParam<Decimal>(ref cm, "@Sequence", decimal.Parse(TxtSequence.Text));
                Sm.CmParam<String>(ref cm, "@BranchAcNm", TxtBranchAcNm.Text);
                Sm.CmParam<String>(ref cm, "@BranchACAddress", MeeBranchAddress.Text);
                Sm.CmParam<String>(ref cm, "@BranchAcFax", TxtBranchFax.Text);
                Sm.CmParam<String>(ref cm, "@BranchAcPhone", TxtBranchPhone.Text);
                Sm.CmParam<String>(ref cm, "@BranchACSwiftCode", TxtBranchSwiftCode.Text);
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtBankAcCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string BankAcCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);
                string SQLInvestment2 = mIsModuleInvestmentActive ? "Left Join TblOption D On A.BankAcTp = D.OptCode And OptCat = 'InvestmentAccount' " : " ";
                string SQLInvestment = mIsModuleInvestmentActive ? "D.OptDesc BankAcTp " : "A.BankAcTp ";

                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select A.BankAcCode, A.HiddenInd, A.ActInd, A.BankCode, A.BankAcNo, A.BankAcNm, A.Remark, " +
                        "A.CoaAcNo, A.AutoNoDebit, A.AutoNoCredit, A.CurCode, A.Sequence, " +
                        "A.BranchAcNm, A.BranchAcAddress, A.BranchAcFax, A.BranchAcPhone, A.BranchAcSwiftCode, A.EntCode, A.SiteCode, " +
                        "B.CCCode, B.CCName, C.AcNo, C.AcDesc, " +
                        SQLInvestment +
                        "From TblBankAccount A " +
                        "LEFT JOIN TblCostCenter B ON A.CCCode = B.CCCode " +
                        "LEFT JOIN TblCoa C ON A.COAAcNoInterOffice = C.AcNo " +
                        SQLInvestment2 +
                        "Where BankAcCode=@BankAcCode;",
                        new string[] 
                        {
                            //0
                            "BankAcCode",
 
                            //1-5
                            "HiddenInd", "BankCode", "BankAcNo", "BankAcTp", "BankAcNm", 
                            
                            //6-10
                            "Remark", "CoaAcNo", "AutoNoDebit", "AutoNoCredit", "CurCode", 
                            
                            //11-15
                            "Sequence", "BranchAcNm", "BranchAcAddress", "BranchAcFax", "BranchAcPhone", 
                            
                            //16-20
                            "BranchAcSwiftCode", "EntCode", "SiteCode", "CCCode", "CCName",

                            //21-22
                            "AcNo", "AcDesc", "ActInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtBankAcCode.EditValue = Sm.DrStr(dr, c[0]);
                            ChkHiddenInd.Checked = Sm.DrStr(dr, c[1]) == "Y";
                            Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[2]));
                            TxtBankAcNo.EditValue = Sm.DrStr(dr, c[3]);
                            TxtBankAcTp.EditValue = Sm.DrStr(dr, c[4]);
                            TxtBankAcNm.EditValue = Sm.DrStr(dr, c[5]);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                            Sm.SetLue(LueCoaAcNo, Sm.DrStr(dr, c[7]));
                            TxtAutoNoDebit.EditValue = Sm.DrStr(dr, c[8]);
                            AutoNoCredit.EditValue = Sm.DrStr(dr, c[9]);
                            Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[10]));
                            TxtSequence.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 2);
                            TxtBranchAcNm.EditValue = Sm.DrStr(dr, c[12]);
                            MeeBranchAddress.EditValue = Sm.DrStr(dr, c[13]);
                            TxtBranchFax.EditValue = Sm.DrStr(dr, c[14]);
                            TxtBranchPhone.EditValue = Sm.DrStr(dr, c[15]);
                            TxtBranchSwiftCode.EditValue = Sm.DrStr(dr, c[16]);
                            Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[17]));
                            Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[18]));
                            TxtCCCode.EditValue = Sm.DrStr(dr, c[19]);
                            TxtCCName.EditValue = Sm.DrStr(dr, c[20]);
                            TxtAcNo.EditValue = Sm.DrStr(dr, c[21]);
                            TxtAcDesc.EditValue = Sm.DrStr(dr, c[22]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[23]) == "Y";
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtBankAcCode, "Account code", false) ||
                Sm.IsTxtEmpty(TxtBankAcNm, "Account name", false) ||
                Sm.IsLueEmpty(LueCoaAcNo, "Chart of Account") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsTxtEmpty(TxtSequence, "Report sequence", true) ||
                (mIsJournalCreateAdditionalPOS && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                (mIsBankAccountUseCostCenterAndInterOffice && Sm.IsTxtEmpty(TxtCCCode, "Cost Center", false)) ||
                (mIsBankAccountUseCostCenterAndInterOffice && Sm.IsTxtEmpty(TxtAcNo, "COA's Account(Inter Office)", false)) ||
                (mIsModuleInvestmentActive && Sm.IsTxtEmpty(TxtBankAcTp, "Bank AccountType", false))||
                 IsAcNoExisted();
        }

        private bool IsAcNoExisted()
        {
            if (!TxtBankAcCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                { CommandText = "Select BankAcCode From TblBankAccount Where BankAcCode=@BankAcCode;" };
                Sm.CmParam<String>(ref cm, "@BankAcCode", TxtBankAcCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Account code already existed.");
                    TxtBankAcCode.Focus();
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Additional Method
        private void GetParameter()
        {
            mIsJournalCreateAdditionalPOS = Sm.GetParameterBoo("IsJournalCreateAdditionalPOS");
            mIsBankAccountUseCostCenterAndInterOffice = Sm.GetParameterBoo("IsBankAccountUseCostCenterAndInterOffice");
            mIsModuleInvestmentActive = Sm.GetParameterBoo("IsModuleInvestmentActive");
        }
        #endregion 

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCoaAcNo_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCoaAcNo, new Sm.RefreshLue1(Sl.SetLueAcNo));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void TxtSequence_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtSequence, 2);
        }

        private void TxtBankAcCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBankAcCode);
        }

        private void TxtBankAcNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBankAcNo);
        }

        private void TxtBankAcTp_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBankAcTp);
        }

        private void TxtBankAcNm_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBankAcNm);
        }

        private void TxtAutoNoIn_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAutoNoDebit);
        }

        private void TxtAutoNoOut_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(AutoNoCredit);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }

        #endregion

        private void BtnCCCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBankAccountDlg(this));
        }

        private void BtnBankAcTp_Click(object sender, EventArgs e)
        {
            TxtBankAcTp.Text = string.Empty;
            Sm.FormShowDialog(new FrmBankAccountDlg3(this));
        }

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBankAccountDlg2(this));
        }

        #endregion

        
    }
}
