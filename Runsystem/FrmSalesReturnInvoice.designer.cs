namespace RunSystem
{
    partial class FrmSalesReturnInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSalesReturnInvoice));
            this.panel3 = new System.Windows.Forms.Panel();
            this.LblDeptCode = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtTaxInvoiceDocument = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtTotalAmt = new DevExpress.XtraEditors.TextEdit();
            this.BtnRecvCtDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtRecvCtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TcSalesReturnInvoice = new DevExpress.XtraTab.XtraTabControl();
            this.TpGeneral = new DevExpress.XtraTab.XtraTabPage();
            this.BtnVoucher = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSalesInvoice = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDOCt = new DevExpress.XtraEditors.SimpleButton();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtVoucher = new DevExpress.XtraEditors.TextEdit();
            this.TxtSalesInvoice = new DevExpress.XtraEditors.TextEdit();
            this.TxtDOCt = new DevExpress.XtraEditors.TextEdit();
            this.BtnRecvCtDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtTaxRate = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtCurCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtCtCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtVoucherRequestPPNDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtAmtBefTax = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtTaxAmt = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtJournalDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtJournalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.DteTaxInvoiceDt = new DevExpress.XtraEditors.DateEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TpItem = new DevExpress.XtraTab.XtraTabPage();
            this.TpAdditional = new DevExpress.XtraTab.XtraTabPage();
            this.TpTax = new DevExpress.XtraTab.XtraTabPage();
            this.LueTaxCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueTaxCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueTaxCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtAlias3 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAlias2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAlias1 = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.DteTaxInvoiceDt3 = new DevExpress.XtraEditors.DateEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.TxtTaxInvoiceNo3 = new DevExpress.XtraEditors.TextEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.DteTaxInvoiceDt2 = new DevExpress.XtraEditors.DateEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.TxtTaxInvoiceNo2 = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.DteTaxInvoiceDt1 = new DevExpress.XtraEditors.DateEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtTaxAmt3 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTaxAmt2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTaxAmt1 = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.TxtTaxInvoiceNo = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceDocument.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecvCtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TcSalesReturnInvoice)).BeginInit();
            this.TcSalesReturnInvoice.SuspendLayout();
            this.TpGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDOCt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestPPNDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmtBefTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties)).BeginInit();
            this.TpItem.SuspendLayout();
            this.TpAdditional.SuspendLayout();
            this.TpTax.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAlias3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAlias2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAlias1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Size = new System.Drawing.Size(70, 456);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 456);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LblDeptCode);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.LueDeptCode);
            this.panel3.Controls.Add(this.TxtLocalDocNo);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 131);
            this.panel3.TabIndex = 9;
            // 
            // LblDeptCode
            // 
            this.LblDeptCode.AutoSize = true;
            this.LblDeptCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDeptCode.ForeColor = System.Drawing.Color.Red;
            this.LblDeptCode.Location = new System.Drawing.Point(55, 79);
            this.LblDeptCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDeptCode.Name = "LblDeptCode";
            this.LblDeptCode.Size = new System.Drawing.Size(73, 14);
            this.LblDeptCode.TabIndex = 16;
            this.LblDeptCode.Text = "Department";
            this.LblDeptCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LblDeptCode.Visible = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.MeeCancelReason);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.ChkCancelInd);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(356, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(416, 131);
            this.panel4.TabIndex = 16;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(9, 29);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(392, 20);
            this.MeeCancelReason.TabIndex = 19;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(9, 10);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(135, 14);
            this.label27.TabIndex = 18;
            this.label27.Text = "Reason For Cancellation";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(9, 52);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(65, 22);
            this.ChkCancelInd.TabIndex = 20;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(134, 75);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.MaxLength = 16;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(208, 20);
            this.LueDeptCode.TabIndex = 17;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.Visible = false;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(134, 52);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(208, 20);
            this.TxtLocalDocNo.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(24, 55);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 14);
            this.label10.TabIndex = 14;
            this.label10.Text = "Local Document#";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(134, 29);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(116, 20);
            this.DteDocDt.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(95, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(134, 7);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(208, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(55, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(162, 206);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(564, 20);
            this.MeeRemark.TabIndex = 42;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(108, 210);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 14);
            this.label7.TabIndex = 41;
            this.label7.Text = "Remark";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxInvoiceDocument
            // 
            this.TxtTaxInvoiceDocument.EnterMoveNextControl = true;
            this.TxtTaxInvoiceDocument.Location = new System.Drawing.Point(162, 52);
            this.TxtTaxInvoiceDocument.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceDocument.Name = "TxtTaxInvoiceDocument";
            this.TxtTaxInvoiceDocument.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceDocument.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceDocument.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceDocument.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceDocument.Properties.MaxLength = 30;
            this.TxtTaxInvoiceDocument.Size = new System.Drawing.Size(208, 20);
            this.TxtTaxInvoiceDocument.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(100, 99);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 14);
            this.label5.TabIndex = 27;
            this.label5.Text = "Currency";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(76, 56);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 14);
            this.label3.TabIndex = 23;
            this.label3.Text = "Tax Invoice#";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(16, 188);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 14);
            this.label6.TabIndex = 39;
            this.label6.Text = "Total Amount After Tax";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalAmt
            // 
            this.TxtTotalAmt.EnterMoveNextControl = true;
            this.TxtTotalAmt.Location = new System.Drawing.Point(162, 184);
            this.TxtTotalAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalAmt.Name = "TxtTotalAmt";
            this.TxtTotalAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalAmt.Properties.ReadOnly = true;
            this.TxtTotalAmt.Size = new System.Drawing.Size(208, 20);
            this.TxtTotalAmt.TabIndex = 40;
            // 
            // BtnRecvCtDocNo
            // 
            this.BtnRecvCtDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRecvCtDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRecvCtDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRecvCtDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRecvCtDocNo.Appearance.Options.UseBackColor = true;
            this.BtnRecvCtDocNo.Appearance.Options.UseFont = true;
            this.BtnRecvCtDocNo.Appearance.Options.UseForeColor = true;
            this.BtnRecvCtDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnRecvCtDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRecvCtDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRecvCtDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnRecvCtDocNo.Image")));
            this.BtnRecvCtDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRecvCtDocNo.Location = new System.Drawing.Point(374, 8);
            this.BtnRecvCtDocNo.Name = "BtnRecvCtDocNo";
            this.BtnRecvCtDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnRecvCtDocNo.TabIndex = 20;
            this.BtnRecvCtDocNo.ToolTip = "Received Document List";
            this.BtnRecvCtDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRecvCtDocNo.ToolTipTitle = "Run System";
            this.BtnRecvCtDocNo.Click += new System.EventHandler(this.BtnRecvDocNo_Click);
            // 
            // TxtRecvCtDocNo
            // 
            this.TxtRecvCtDocNo.EnterMoveNextControl = true;
            this.TxtRecvCtDocNo.Location = new System.Drawing.Point(162, 10);
            this.TxtRecvCtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRecvCtDocNo.Name = "TxtRecvCtDocNo";
            this.TxtRecvCtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRecvCtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRecvCtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRecvCtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtRecvCtDocNo.Properties.MaxLength = 30;
            this.TxtRecvCtDocNo.Properties.ReadOnly = true;
            this.TxtRecvCtDocNo.Size = new System.Drawing.Size(208, 20);
            this.TxtRecvCtDocNo.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(36, 14);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 14);
            this.label4.TabIndex = 18;
            this.label4.Text = "Received Document";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(98, 35);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 14);
            this.label8.TabIndex = 21;
            this.label8.Text = "Customer";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(766, 74);
            this.Grd1.TabIndex = 18;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 74);
            this.Grd2.TabIndex = 18;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 414);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 12;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 434);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 13;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.TcSalesReturnInvoice);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 131);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(772, 325);
            this.panel5.TabIndex = 10;
            // 
            // TcSalesReturnInvoice
            // 
            this.TcSalesReturnInvoice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcSalesReturnInvoice.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcSalesReturnInvoice.Location = new System.Drawing.Point(0, 0);
            this.TcSalesReturnInvoice.Name = "TcSalesReturnInvoice";
            this.TcSalesReturnInvoice.SelectedTabPage = this.TpGeneral;
            this.TcSalesReturnInvoice.Size = new System.Drawing.Size(772, 325);
            this.TcSalesReturnInvoice.TabIndex = 17;
            this.TcSalesReturnInvoice.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TpGeneral,
            this.TpItem,
            this.TpAdditional,
            this.TpTax});
            // 
            // TpGeneral
            // 
            this.TpGeneral.Appearance.Header.Options.UseTextOptions = true;
            this.TpGeneral.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpGeneral.Controls.Add(this.BtnVoucher);
            this.TpGeneral.Controls.Add(this.BtnSalesInvoice);
            this.TpGeneral.Controls.Add(this.BtnDOCt);
            this.TpGeneral.Controls.Add(this.label20);
            this.TpGeneral.Controls.Add(this.label19);
            this.TpGeneral.Controls.Add(this.label18);
            this.TpGeneral.Controls.Add(this.TxtVoucher);
            this.TpGeneral.Controls.Add(this.TxtSalesInvoice);
            this.TpGeneral.Controls.Add(this.TxtDOCt);
            this.TpGeneral.Controls.Add(this.BtnRecvCtDocNo2);
            this.TpGeneral.Controls.Add(this.label16);
            this.TpGeneral.Controls.Add(this.TxtTaxRate);
            this.TpGeneral.Controls.Add(this.label15);
            this.TpGeneral.Controls.Add(this.TxtCurCode);
            this.TpGeneral.Controls.Add(this.TxtCtCode);
            this.TpGeneral.Controls.Add(this.TxtVoucherRequestPPNDocNo);
            this.TpGeneral.Controls.Add(this.label14);
            this.TpGeneral.Controls.Add(this.TxtAmtBefTax);
            this.TpGeneral.Controls.Add(this.label12);
            this.TpGeneral.Controls.Add(this.TxtTaxAmt);
            this.TpGeneral.Controls.Add(this.label13);
            this.TpGeneral.Controls.Add(this.label17);
            this.TpGeneral.Controls.Add(this.TxtJournalDocNo2);
            this.TpGeneral.Controls.Add(this.TxtJournalDocNo);
            this.TpGeneral.Controls.Add(this.label11);
            this.TpGeneral.Controls.Add(this.MeeRemark);
            this.TpGeneral.Controls.Add(this.DteTaxInvoiceDt);
            this.TpGeneral.Controls.Add(this.label7);
            this.TpGeneral.Controls.Add(this.label9);
            this.TpGeneral.Controls.Add(this.label5);
            this.TpGeneral.Controls.Add(this.label6);
            this.TpGeneral.Controls.Add(this.BtnRecvCtDocNo);
            this.TpGeneral.Controls.Add(this.TxtTotalAmt);
            this.TpGeneral.Controls.Add(this.TxtTaxInvoiceDocument);
            this.TpGeneral.Controls.Add(this.label3);
            this.TpGeneral.Controls.Add(this.TxtRecvCtDocNo);
            this.TpGeneral.Controls.Add(this.label4);
            this.TpGeneral.Controls.Add(this.label8);
            this.TpGeneral.Name = "TpGeneral";
            this.TpGeneral.Size = new System.Drawing.Size(766, 297);
            this.TpGeneral.Text = "General";
            // 
            // BtnVoucher
            // 
            this.BtnVoucher.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucher.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucher.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucher.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucher.Appearance.Options.UseBackColor = true;
            this.BtnVoucher.Appearance.Options.UseFont = true;
            this.BtnVoucher.Appearance.Options.UseForeColor = true;
            this.BtnVoucher.Appearance.Options.UseTextOptions = true;
            this.BtnVoucher.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucher.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucher.Image = ((System.Drawing.Image)(resources.GetObject("BtnVoucher.Image")));
            this.BtnVoucher.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnVoucher.Location = new System.Drawing.Point(727, 54);
            this.BtnVoucher.Name = "BtnVoucher";
            this.BtnVoucher.Size = new System.Drawing.Size(24, 21);
            this.BtnVoucher.TabIndex = 57;
            this.BtnVoucher.ToolTip = "Show Received Document Information";
            this.BtnVoucher.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucher.ToolTipTitle = "Run System";
            this.BtnVoucher.Click += new System.EventHandler(this.BtnVoucher_Click);
            // 
            // BtnSalesInvoice
            // 
            this.BtnSalesInvoice.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSalesInvoice.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSalesInvoice.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSalesInvoice.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSalesInvoice.Appearance.Options.UseBackColor = true;
            this.BtnSalesInvoice.Appearance.Options.UseFont = true;
            this.BtnSalesInvoice.Appearance.Options.UseForeColor = true;
            this.BtnSalesInvoice.Appearance.Options.UseTextOptions = true;
            this.BtnSalesInvoice.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSalesInvoice.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSalesInvoice.Image = ((System.Drawing.Image)(resources.GetObject("BtnSalesInvoice.Image")));
            this.BtnSalesInvoice.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSalesInvoice.Location = new System.Drawing.Point(727, 32);
            this.BtnSalesInvoice.Name = "BtnSalesInvoice";
            this.BtnSalesInvoice.Size = new System.Drawing.Size(24, 21);
            this.BtnSalesInvoice.TabIndex = 54;
            this.BtnSalesInvoice.ToolTip = "Show Received Document Information";
            this.BtnSalesInvoice.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSalesInvoice.ToolTipTitle = "Run System";
            this.BtnSalesInvoice.Click += new System.EventHandler(this.BtnSalesInvoice_Click);
            // 
            // BtnDOCt
            // 
            this.BtnDOCt.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDOCt.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDOCt.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDOCt.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDOCt.Appearance.Options.UseBackColor = true;
            this.BtnDOCt.Appearance.Options.UseFont = true;
            this.BtnDOCt.Appearance.Options.UseForeColor = true;
            this.BtnDOCt.Appearance.Options.UseTextOptions = true;
            this.BtnDOCt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDOCt.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDOCt.Image = ((System.Drawing.Image)(resources.GetObject("BtnDOCt.Image")));
            this.BtnDOCt.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDOCt.Location = new System.Drawing.Point(727, 10);
            this.BtnDOCt.Name = "BtnDOCt";
            this.BtnDOCt.Size = new System.Drawing.Size(24, 21);
            this.BtnDOCt.TabIndex = 51;
            this.BtnDOCt.ToolTip = "Show Received Document Information";
            this.BtnDOCt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDOCt.ToolTipTitle = "Run System";
            this.BtnDOCt.Click += new System.EventHandler(this.BtnDOCt_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(490, 55);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(62, 14);
            this.label20.TabIndex = 55;
            this.label20.Text = "Voucher#";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(475, 34);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 14);
            this.label19.TabIndex = 52;
            this.label19.Text = "Sales Invoice";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(455, 13);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(99, 14);
            this.label18.TabIndex = 49;
            this.label18.Text = "DO To Customer";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucher
            // 
            this.TxtVoucher.EnterMoveNextControl = true;
            this.TxtVoucher.Location = new System.Drawing.Point(553, 55);
            this.TxtVoucher.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucher.Name = "TxtVoucher";
            this.TxtVoucher.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucher.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucher.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucher.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucher.Properties.MaxLength = 30;
            this.TxtVoucher.Properties.ReadOnly = true;
            this.TxtVoucher.Size = new System.Drawing.Size(173, 20);
            this.TxtVoucher.TabIndex = 56;
            // 
            // TxtSalesInvoice
            // 
            this.TxtSalesInvoice.EnterMoveNextControl = true;
            this.TxtSalesInvoice.Location = new System.Drawing.Point(553, 33);
            this.TxtSalesInvoice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSalesInvoice.Name = "TxtSalesInvoice";
            this.TxtSalesInvoice.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSalesInvoice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSalesInvoice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSalesInvoice.Properties.Appearance.Options.UseFont = true;
            this.TxtSalesInvoice.Properties.MaxLength = 30;
            this.TxtSalesInvoice.Properties.ReadOnly = true;
            this.TxtSalesInvoice.Size = new System.Drawing.Size(173, 20);
            this.TxtSalesInvoice.TabIndex = 53;
            // 
            // TxtDOCt
            // 
            this.TxtDOCt.EnterMoveNextControl = true;
            this.TxtDOCt.Location = new System.Drawing.Point(553, 11);
            this.TxtDOCt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDOCt.Name = "TxtDOCt";
            this.TxtDOCt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDOCt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDOCt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDOCt.Properties.Appearance.Options.UseFont = true;
            this.TxtDOCt.Properties.MaxLength = 30;
            this.TxtDOCt.Properties.ReadOnly = true;
            this.TxtDOCt.Size = new System.Drawing.Size(173, 20);
            this.TxtDOCt.TabIndex = 50;
            // 
            // BtnRecvCtDocNo2
            // 
            this.BtnRecvCtDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRecvCtDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRecvCtDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRecvCtDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRecvCtDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnRecvCtDocNo2.Appearance.Options.UseFont = true;
            this.BtnRecvCtDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnRecvCtDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnRecvCtDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRecvCtDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRecvCtDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnRecvCtDocNo2.Image")));
            this.BtnRecvCtDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRecvCtDocNo2.Location = new System.Drawing.Point(400, 8);
            this.BtnRecvCtDocNo2.Name = "BtnRecvCtDocNo2";
            this.BtnRecvCtDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnRecvCtDocNo2.TabIndex = 21;
            this.BtnRecvCtDocNo2.ToolTip = "Show Received Document Information";
            this.BtnRecvCtDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRecvCtDocNo2.ToolTipTitle = "Run System";
            this.BtnRecvCtDocNo2.Click += new System.EventHandler(this.BtnRecvCtDocNo2_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(284, 142);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(19, 14);
            this.label16.TabIndex = 36;
            this.label16.Text = "%";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxRate
            // 
            this.TxtTaxRate.EnterMoveNextControl = true;
            this.TxtTaxRate.Location = new System.Drawing.Point(163, 140);
            this.TxtTaxRate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxRate.Name = "TxtTaxRate";
            this.TxtTaxRate.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxRate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxRate.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxRate.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxRate.Properties.ReadOnly = true;
            this.TxtTaxRate.Size = new System.Drawing.Size(116, 20);
            this.TxtTaxRate.TabIndex = 35;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(99, 144);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 14);
            this.label15.TabIndex = 34;
            this.label15.Text = "Tax Rate";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCurCode
            // 
            this.TxtCurCode.EnterMoveNextControl = true;
            this.TxtCurCode.Location = new System.Drawing.Point(162, 96);
            this.TxtCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode.Name = "TxtCurCode";
            this.TxtCurCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode.Properties.MaxLength = 3;
            this.TxtCurCode.Properties.ReadOnly = true;
            this.TxtCurCode.Size = new System.Drawing.Size(116, 20);
            this.TxtCurCode.TabIndex = 28;
            // 
            // TxtCtCode
            // 
            this.TxtCtCode.EnterMoveNextControl = true;
            this.TxtCtCode.Location = new System.Drawing.Point(162, 31);
            this.TxtCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtCode.Name = "TxtCtCode";
            this.TxtCtCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCtCode.Properties.MaxLength = 80;
            this.TxtCtCode.Properties.ReadOnly = true;
            this.TxtCtCode.Size = new System.Drawing.Size(282, 20);
            this.TxtCtCode.TabIndex = 22;
            // 
            // TxtVoucherRequestPPNDocNo
            // 
            this.TxtVoucherRequestPPNDocNo.EnterMoveNextControl = true;
            this.TxtVoucherRequestPPNDocNo.Location = new System.Drawing.Point(162, 228);
            this.TxtVoucherRequestPPNDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestPPNDocNo.Name = "TxtVoucherRequestPPNDocNo";
            this.TxtVoucherRequestPPNDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestPPNDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestPPNDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestPPNDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestPPNDocNo.Properties.MaxLength = 30;
            this.TxtVoucherRequestPPNDocNo.Properties.ReadOnly = true;
            this.TxtVoucherRequestPPNDocNo.Size = new System.Drawing.Size(208, 20);
            this.TxtVoucherRequestPPNDocNo.TabIndex = 44;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(15, 231);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(139, 14);
            this.label14.TabIndex = 43;
            this.label14.Text = "Voucher Request VAT#";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmtBefTax
            // 
            this.TxtAmtBefTax.EnterMoveNextControl = true;
            this.TxtAmtBefTax.Location = new System.Drawing.Point(162, 118);
            this.TxtAmtBefTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmtBefTax.Name = "TxtAmtBefTax";
            this.TxtAmtBefTax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmtBefTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmtBefTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmtBefTax.Properties.Appearance.Options.UseFont = true;
            this.TxtAmtBefTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmtBefTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmtBefTax.Properties.ReadOnly = true;
            this.TxtAmtBefTax.Size = new System.Drawing.Size(208, 20);
            this.TxtAmtBefTax.TabIndex = 33;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(8, 122);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(147, 14);
            this.label12.TabIndex = 32;
            this.label12.Text = "Total Amount Before Tax";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxAmt
            // 
            this.TxtTaxAmt.EnterMoveNextControl = true;
            this.TxtTaxAmt.Location = new System.Drawing.Point(162, 162);
            this.TxtTaxAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt.Name = "TxtTaxAmt";
            this.TxtTaxAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt.Properties.ReadOnly = true;
            this.TxtTaxAmt.Size = new System.Drawing.Size(208, 20);
            this.TxtTaxAmt.TabIndex = 38;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(80, 166);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 14);
            this.label13.TabIndex = 37;
            this.label13.Text = "Tax Amount";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(375, 275);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 14);
            this.label17.TabIndex = 48;
            this.label17.Text = "(Cancel Data)";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtJournalDocNo2
            // 
            this.TxtJournalDocNo2.EnterMoveNextControl = true;
            this.TxtJournalDocNo2.Location = new System.Drawing.Point(162, 272);
            this.TxtJournalDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo2.Name = "TxtJournalDocNo2";
            this.TxtJournalDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo2.Properties.MaxLength = 30;
            this.TxtJournalDocNo2.Properties.ReadOnly = true;
            this.TxtJournalDocNo2.Size = new System.Drawing.Size(208, 20);
            this.TxtJournalDocNo2.TabIndex = 47;
            // 
            // TxtJournalDocNo
            // 
            this.TxtJournalDocNo.EnterMoveNextControl = true;
            this.TxtJournalDocNo.Location = new System.Drawing.Point(162, 250);
            this.TxtJournalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo.Name = "TxtJournalDocNo";
            this.TxtJournalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo.Properties.MaxLength = 30;
            this.TxtJournalDocNo.Properties.ReadOnly = true;
            this.TxtJournalDocNo.Size = new System.Drawing.Size(208, 20);
            this.TxtJournalDocNo.TabIndex = 46;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(101, 254);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 14);
            this.label11.TabIndex = 45;
            this.label11.Text = "Journal#";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvoiceDt
            // 
            this.DteTaxInvoiceDt.EditValue = null;
            this.DteTaxInvoiceDt.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt.Location = new System.Drawing.Point(162, 74);
            this.DteTaxInvoiceDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt.Name = "DteTaxInvoiceDt";
            this.DteTaxInvoiceDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt.Size = new System.Drawing.Size(116, 20);
            this.DteTaxInvoiceDt.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(55, 78);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 14);
            this.label9.TabIndex = 25;
            this.label9.Text = "Tax Invoice Date";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpItem
            // 
            this.TpItem.Appearance.Header.Options.UseTextOptions = true;
            this.TpItem.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpItem.Controls.Add(this.Grd1);
            this.TpItem.Name = "TpItem";
            this.TpItem.Size = new System.Drawing.Size(766, 74);
            this.TpItem.Text = "Item List";
            // 
            // TpAdditional
            // 
            this.TpAdditional.Appearance.Header.Options.UseTextOptions = true;
            this.TpAdditional.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpAdditional.Controls.Add(this.Grd2);
            this.TpAdditional.Name = "TpAdditional";
            this.TpAdditional.Size = new System.Drawing.Size(766, 74);
            this.TpAdditional.Text = "Discount, Additional Cost, Etc";
            // 
            // TpTax
            // 
            this.TpTax.Appearance.Header.Options.UseTextOptions = true;
            this.TpTax.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpTax.Controls.Add(this.LueTaxCode1);
            this.TpTax.Controls.Add(this.LueTaxCode3);
            this.TpTax.Controls.Add(this.LueTaxCode2);
            this.TpTax.Controls.Add(this.label48);
            this.TpTax.Controls.Add(this.label47);
            this.TpTax.Controls.Add(this.label21);
            this.TpTax.Controls.Add(this.TxtAlias3);
            this.TpTax.Controls.Add(this.TxtAlias2);
            this.TpTax.Controls.Add(this.TxtAlias1);
            this.TpTax.Controls.Add(this.label31);
            this.TpTax.Controls.Add(this.DteTaxInvoiceDt3);
            this.TpTax.Controls.Add(this.label32);
            this.TpTax.Controls.Add(this.label38);
            this.TpTax.Controls.Add(this.TxtTaxInvoiceNo3);
            this.TpTax.Controls.Add(this.label39);
            this.TpTax.Controls.Add(this.label22);
            this.TpTax.Controls.Add(this.DteTaxInvoiceDt2);
            this.TpTax.Controls.Add(this.label28);
            this.TpTax.Controls.Add(this.label29);
            this.TpTax.Controls.Add(this.TxtTaxInvoiceNo2);
            this.TpTax.Controls.Add(this.label30);
            this.TpTax.Controls.Add(this.label23);
            this.TpTax.Controls.Add(this.DteTaxInvoiceDt1);
            this.TpTax.Controls.Add(this.label24);
            this.TpTax.Controls.Add(this.TxtTaxAmt3);
            this.TpTax.Controls.Add(this.TxtTaxAmt2);
            this.TpTax.Controls.Add(this.TxtTaxAmt1);
            this.TpTax.Controls.Add(this.label26);
            this.TpTax.Controls.Add(this.TxtTaxInvoiceNo);
            this.TpTax.Controls.Add(this.label34);
            this.TpTax.Name = "TpTax";
            this.TpTax.Size = new System.Drawing.Size(766, 74);
            this.TpTax.Text = "Tax";
            // 
            // LueTaxCode1
            // 
            this.LueTaxCode1.EnterMoveNextControl = true;
            this.LueTaxCode1.Location = new System.Drawing.Point(112, 45);
            this.LueTaxCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode1.Name = "LueTaxCode1";
            this.LueTaxCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode1.Properties.DropDownRows = 25;
            this.LueTaxCode1.Properties.NullText = "[Empty]";
            this.LueTaxCode1.Properties.PopupWidth = 500;
            this.LueTaxCode1.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode1.TabIndex = 87;
            this.LueTaxCode1.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LueTaxCode3
            // 
            this.LueTaxCode3.EnterMoveNextControl = true;
            this.LueTaxCode3.Location = new System.Drawing.Point(112, 172);
            this.LueTaxCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode3.Name = "LueTaxCode3";
            this.LueTaxCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode3.Properties.DropDownRows = 25;
            this.LueTaxCode3.Properties.NullText = "[Empty]";
            this.LueTaxCode3.Properties.PopupWidth = 200;
            this.LueTaxCode3.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode3.TabIndex = 86;
            this.LueTaxCode3.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LueTaxCode2
            // 
            this.LueTaxCode2.EnterMoveNextControl = true;
            this.LueTaxCode2.Location = new System.Drawing.Point(112, 109);
            this.LueTaxCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode2.Name = "LueTaxCode2";
            this.LueTaxCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode2.Properties.DropDownRows = 25;
            this.LueTaxCode2.Properties.NullText = "[Empty]";
            this.LueTaxCode2.Properties.PopupWidth = 200;
            this.LueTaxCode2.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode2.TabIndex = 85;
            this.LueTaxCode2.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(77, 196);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(30, 14);
            this.label48.TabIndex = 74;
            this.label48.Text = "Alias";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(77, 134);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(30, 14);
            this.label47.TabIndex = 68;
            this.label47.Text = "Alias";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(77, 70);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(30, 14);
            this.label21.TabIndex = 62;
            this.label21.Text = "Alias";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAlias3
            // 
            this.TxtAlias3.EnterMoveNextControl = true;
            this.TxtAlias3.Location = new System.Drawing.Point(112, 193);
            this.TxtAlias3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAlias3.Name = "TxtAlias3";
            this.TxtAlias3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAlias3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAlias3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAlias3.Properties.Appearance.Options.UseFont = true;
            this.TxtAlias3.Properties.MaxLength = 30;
            this.TxtAlias3.Size = new System.Drawing.Size(250, 20);
            this.TxtAlias3.TabIndex = 75;
            // 
            // TxtAlias2
            // 
            this.TxtAlias2.EnterMoveNextControl = true;
            this.TxtAlias2.Location = new System.Drawing.Point(112, 130);
            this.TxtAlias2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAlias2.Name = "TxtAlias2";
            this.TxtAlias2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAlias2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAlias2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAlias2.Properties.Appearance.Options.UseFont = true;
            this.TxtAlias2.Properties.MaxLength = 30;
            this.TxtAlias2.Size = new System.Drawing.Size(250, 20);
            this.TxtAlias2.TabIndex = 69;
            // 
            // TxtAlias1
            // 
            this.TxtAlias1.EnterMoveNextControl = true;
            this.TxtAlias1.Location = new System.Drawing.Point(112, 67);
            this.TxtAlias1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAlias1.Name = "TxtAlias1";
            this.TxtAlias1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAlias1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAlias1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAlias1.Properties.Appearance.Options.UseFont = true;
            this.TxtAlias1.Properties.MaxLength = 30;
            this.TxtAlias1.Size = new System.Drawing.Size(250, 20);
            this.TxtAlias1.TabIndex = 63;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(368, 176);
            this.label31.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(51, 14);
            this.label31.TabIndex = 73;
            this.label31.Text = "Amount";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvoiceDt3
            // 
            this.DteTaxInvoiceDt3.EditValue = null;
            this.DteTaxInvoiceDt3.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt3.Location = new System.Drawing.Point(424, 152);
            this.DteTaxInvoiceDt3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt3.Name = "DteTaxInvoiceDt3";
            this.DteTaxInvoiceDt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt3.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt3.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt3.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt3.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt3.Size = new System.Drawing.Size(107, 20);
            this.DteTaxInvoiceDt3.TabIndex = 70;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(386, 156);
            this.label32.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(33, 14);
            this.label32.TabIndex = 69;
            this.label32.Text = "Date";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(80, 175);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(27, 14);
            this.label38.TabIndex = 72;
            this.label38.Text = "Tax";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxInvoiceNo3
            // 
            this.TxtTaxInvoiceNo3.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo3.Location = new System.Drawing.Point(112, 151);
            this.TxtTaxInvoiceNo3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo3.Name = "TxtTaxInvoiceNo3";
            this.TxtTaxInvoiceNo3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo3.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo3.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvoiceNo3.TabIndex = 71;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(28, 154);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(79, 14);
            this.label39.TabIndex = 70;
            this.label39.Text = "Tax Invoice#";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(368, 112);
            this.label22.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(51, 14);
            this.label22.TabIndex = 82;
            this.label22.Text = "Amount";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvoiceDt2
            // 
            this.DteTaxInvoiceDt2.EditValue = null;
            this.DteTaxInvoiceDt2.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt2.Location = new System.Drawing.Point(424, 88);
            this.DteTaxInvoiceDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt2.Name = "DteTaxInvoiceDt2";
            this.DteTaxInvoiceDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt2.Size = new System.Drawing.Size(107, 20);
            this.DteTaxInvoiceDt2.TabIndex = 81;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(386, 91);
            this.label28.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(33, 14);
            this.label28.TabIndex = 80;
            this.label28.Text = "Date";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(80, 112);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(27, 14);
            this.label29.TabIndex = 66;
            this.label29.Text = "Tax";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxInvoiceNo2
            // 
            this.TxtTaxInvoiceNo2.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo2.Location = new System.Drawing.Point(112, 88);
            this.TxtTaxInvoiceNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo2.Name = "TxtTaxInvoiceNo2";
            this.TxtTaxInvoiceNo2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo2.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo2.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvoiceNo2.TabIndex = 65;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(28, 91);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(79, 14);
            this.label30.TabIndex = 64;
            this.label30.Text = "Tax Invoice#";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(368, 48);
            this.label23.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(51, 14);
            this.label23.TabIndex = 78;
            this.label23.Text = "Amount";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvoiceDt1
            // 
            this.DteTaxInvoiceDt1.EditValue = null;
            this.DteTaxInvoiceDt1.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt1.Location = new System.Drawing.Point(424, 25);
            this.DteTaxInvoiceDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt1.Name = "DteTaxInvoiceDt1";
            this.DteTaxInvoiceDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt1.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt1.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt1.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt1.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt1.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt1.Size = new System.Drawing.Size(107, 20);
            this.DteTaxInvoiceDt1.TabIndex = 77;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(386, 28);
            this.label24.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(33, 14);
            this.label24.TabIndex = 76;
            this.label24.Text = "Date";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxAmt3
            // 
            this.TxtTaxAmt3.EnterMoveNextControl = true;
            this.TxtTaxAmt3.Location = new System.Drawing.Point(424, 173);
            this.TxtTaxAmt3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt3.Name = "TxtTaxAmt3";
            this.TxtTaxAmt3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt3.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt3.Properties.ReadOnly = true;
            this.TxtTaxAmt3.Size = new System.Drawing.Size(147, 20);
            this.TxtTaxAmt3.TabIndex = 74;
            // 
            // TxtTaxAmt2
            // 
            this.TxtTaxAmt2.EnterMoveNextControl = true;
            this.TxtTaxAmt2.Location = new System.Drawing.Point(424, 109);
            this.TxtTaxAmt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt2.Name = "TxtTaxAmt2";
            this.TxtTaxAmt2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt2.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt2.Properties.ReadOnly = true;
            this.TxtTaxAmt2.Size = new System.Drawing.Size(147, 20);
            this.TxtTaxAmt2.TabIndex = 83;
            // 
            // TxtTaxAmt1
            // 
            this.TxtTaxAmt1.EnterMoveNextControl = true;
            this.TxtTaxAmt1.Location = new System.Drawing.Point(424, 46);
            this.TxtTaxAmt1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt1.Name = "TxtTaxAmt1";
            this.TxtTaxAmt1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt1.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt1.Properties.ReadOnly = true;
            this.TxtTaxAmt1.Size = new System.Drawing.Size(147, 20);
            this.TxtTaxAmt1.TabIndex = 79;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(80, 49);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(27, 14);
            this.label26.TabIndex = 60;
            this.label26.Text = "Tax";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxInvoiceNo
            // 
            this.TxtTaxInvoiceNo.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo.Location = new System.Drawing.Point(112, 25);
            this.TxtTaxInvoiceNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo.Name = "TxtTaxInvoiceNo";
            this.TxtTaxInvoiceNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvoiceNo.TabIndex = 59;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(28, 28);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(79, 14);
            this.label34.TabIndex = 48;
            this.label34.Text = "Tax Invoice#";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmSalesReturnInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 456);
            this.Name = "FrmSalesReturnInvoice";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceDocument.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecvCtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TcSalesReturnInvoice)).EndInit();
            this.TcSalesReturnInvoice.ResumeLayout(false);
            this.TpGeneral.ResumeLayout(false);
            this.TpGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDOCt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestPPNDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmtBefTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties)).EndInit();
            this.TpItem.ResumeLayout(false);
            this.TpAdditional.ResumeLayout(false);
            this.TpTax.ResumeLayout(false);
            this.TpTax.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAlias3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAlias2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAlias1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        internal DevExpress.XtraEditors.TextEdit TxtTotalAmt;
        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.SimpleButton BtnRecvCtDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtRecvCtDocNo;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceDocument;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraTab.XtraTabControl TcSalesReturnInvoice;
        private DevExpress.XtraTab.XtraTabPage TpGeneral;
        private DevExpress.XtraTab.XtraTabPage TpItem;
        private DevExpress.XtraTab.XtraTabPage TpAdditional;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtAmtBefTax;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestPPNDocNo;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtCtCode;
        internal DevExpress.XtraEditors.TextEdit TxtCurCode;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtTaxRate;
        private System.Windows.Forms.Label label15;
        public DevExpress.XtraEditors.SimpleButton BtnRecvCtDocNo2;
        private System.Windows.Forms.Label LblDeptCode;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        public DevExpress.XtraEditors.SimpleButton BtnVoucher;
        public DevExpress.XtraEditors.SimpleButton BtnSalesInvoice;
        public DevExpress.XtraEditors.SimpleButton BtnDOCt;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtVoucher;
        internal DevExpress.XtraEditors.TextEdit TxtSalesInvoice;
        internal DevExpress.XtraEditors.TextEdit TxtDOCt;
        private DevExpress.XtraTab.XtraTabPage TpTax;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtAlias3;
        internal DevExpress.XtraEditors.TextEdit TxtAlias2;
        internal DevExpress.XtraEditors.TextEdit TxtAlias1;
        private System.Windows.Forms.Label label31;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt3;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label38;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo3;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt1;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt3;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt2;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt1;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo;
        private System.Windows.Forms.Label label34;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode3;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode2;
        internal DevExpress.XtraEditors.LookUpEdit LueTaxCode1;
    }
}