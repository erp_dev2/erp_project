﻿#region Update
/*
    03/03/2021 [WED/IMS] new apps
    10/03/2021 [VIN/IMS] bug: filter item category belum sesuai
    11/03/2021 [VIN/IMS] tambah kolom total price 
    08/04/2021 [WED/IMS] rombak. warehouse jadi multi, tanggal jadi periode, real stock s.d re-order di hide
    05/07/2021 [BRI/IMS] tambah total price (in) & total price (out)
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptDailyStockMovement2 : RunSystem.FrmBase6
    {
        #region Field
        
        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool
            mIsFilterByItCt = false,
            mIsRptDailyStockMovementUsePrice = false,
            mIsMovingAvgEnabled = false; 
        private int mUom = 1;

        #endregion

        #region Constructor

        public FrmRptDailyStockMovement2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                GetParameter();
                Sl.SetLueItCtCode(ref LueItCtCode, string.Empty);
                SetGrd();
                SetCcbWhsCode(ref CcbWhsCode, string.Empty);
                base.FrmLoad(sender, e);
                
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");
            mIsRptDailyStockMovementUsePrice = Sm.GetParameterBoo("IsRptDailyStockMovementUsePrice ");
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A1.WhsName, A.ItCode, E.ItName, E.ItCodeInternal, E.Specification, E1.ItCtName, E.MinStock, ");
            SQL.AppendLine("E.InventoryUomCode, IfNull(B.Init, 0) As Init, IfNull(C.I, 0) As I, IfNull(D.O, 0) As O, ");
            SQL.AppendLine("E.InventoryUomCode2, IfNull(B.Init2, 0) As Init2, IfNull(C.I2, 0) As I2, IfNull(D.O2, 0) As O2, ");
            SQL.AppendLine("E.InventoryUomCode3, IfNull(B.Init3, 0) As Init3, IfNull(C.I3, 0) As I3, IfNull(D.O3, 0) As O3, ");
            //SQL.AppendLine("F.QtyReal, G.QtyAvgUsed, H.QtyAvgUsed3, ");
            SQL.AppendLine("0.00 As QtyReal, 0.00 As QtyAvgUsed, 0.00 As QtyAvgUsed3, ");
            SQL.AppendLine("E.ReorderStock, I.ItScName ");
            if (mIsRptDailyStockMovementUsePrice)
                SQL.AppendLine(", J.Price");
            else
                SQL.AppendLine(", 0.00 As Price");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct WhsCode, ItCode ");
            SQL.AppendLine("    From TblStockMovement ");
            SQL.AppendLine("    Where DocDt Between @DocDt1 And @DocDt2 ");
            if (CcbWhsCode.Text.Length > 0)
                SQL.AppendLine("    And Find_In_Set(WhsCode, @WhsCode) ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblWarehouse A1 On A.WhsCode = A1.WhsCode ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select WhsCode, ItCode, Sum(Qty) As Init, Sum(Qty2) As Init2, Sum(Qty3) As Init3 ");
	        SQL.AppendLine("    From TblStockMovement ");
            SQL.AppendLine("    Where DocDt < @DocDt1 ");
            if (CcbWhsCode.Text.Length > 0)
                SQL.AppendLine("    And Find_In_Set(WhsCode, @WhsCode) ");
	        SQL.AppendLine("    Group By WhsCode, ItCode ");
	        SQL.AppendLine("    Having Sum(Qty)>0 ");
            SQL.AppendLine(") B On A.WhsCode = B.WhsCode And A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select WhsCode, ItCode, Sum(Qty) As I, Sum(Qty2) As I2, Sum(Qty3) As I3 ");
	        SQL.AppendLine("    From TblStockMovement ");
            SQL.AppendLine("    Where DocDt Between @DocDt1 And @DocDt2 ");
            if (CcbWhsCode.Text.Length > 0)
                SQL.AppendLine("    And Find_In_Set(WhsCode, @WhsCode) ");
	        SQL.AppendLine("    And Qty>0 ");
	        SQL.AppendLine("    Group By WhsCode, ItCode ");
            SQL.AppendLine(") C On A.WhsCode = C.WhsCode And A.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select WhsCode, ItCode, Sum(Qty) As O, Sum(Qty2) As O2, Sum(Qty3) As O3 ");
	        SQL.AppendLine("    From TblStockMovement ");
            SQL.AppendLine("    Where DocDt Between @DocDt1 And @DocDt2 ");
            if (CcbWhsCode.Text.Length > 0)
                SQL.AppendLine("    And Find_In_Set(WhsCode, @WhsCode) ");
	        SQL.AppendLine("    And Qty<0 ");
	        SQL.AppendLine("    Group By WhsCode, ItCode ");
            SQL.AppendLine(") D On A.WhsCode = D.WhsCode And A.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblItem E On A.ItCode=E.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory E1 On E.ItCtCode = E1.ItCtCode ");

            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=E.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select A.WhsCode, A.ItCode, ");
            //SQL.AppendLine("    Case When @DocDt=@DocDt1 Then A.QtySum ");
            //SQL.AppendLine("    When @DocDt<@DocDt1 then B.QtyMov ");
            //SQL.AppendLine("    End As QtyReal ");
            //SQL.AppendLine("    From ");
            //SQL.AppendLine("    (  ");
            //SQL.AppendLine("        Select WhsCode, ItCode, SUM(Qty) As QtySum From TblStockSummary ");
            //if (CcbWhsCode.Text.Length > 0)
            //    SQL.AppendLine("    Where Find_In_Set(WhsCode, @WhsCode) ");
            //SQL.AppendLine("        Group By WhsCode, ItCode ");
            //SQL.AppendLine("    ) A ");
            //SQL.AppendLine("    Left Join ( ");
            //SQL.AppendLine("        Select WhsCode, ItCode, SUM(Qty) As QtyMov From TblStockMovement  ");
            //SQL.AppendLine("        Where DocDt < @DocDt ");
            //if (CcbWhsCode.Text.Length > 0)
            //    SQL.AppendLine("    And Find_In_Set(WhsCode, @WhsCode) ");
            //SQL.AppendLine("        Group By WhsCode, ItCode ");
            //SQL.AppendLine("    ) B On A.WhsCode = B.WhsCode And A.ItCode = B.ItCode  ");
            //SQL.AppendLine(") F On A.WhsCode = F.WhsCode And A.ItCode=F.ItCode ");

            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select A.WhsCode, B.ItCode, ");
            //SQL.AppendLine("    Sum(B.Qty)/C.Value As QtyAvgUsed ");
            //SQL.AppendLine("    From TblDODeptHdr A ");
            //SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
            //SQL.AppendLine("    Inner Join ( ");
            //SQL.AppendLine("        Select DAY(LAST_DAY(Date_Add(@DocDt, interval -1 month))) As Value ");
            //SQL.AppendLine("    ) C On 0=0 ");
            //SQL.AppendLine("    Where Left(A.DocDt, 6) = Concat(Left(Date_Add(@DocDt, interval -1 month), 4), substring(Date_Add(@DocDt, interval -1 month), 6, 2)) ");
            //if (CcbWhsCode.Text.Length > 0)
            //    SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCode) ");
            //SQL.AppendLine("    And A.DORequestDeptDocNo Is Null ");
            //SQL.AppendLine("    Group By A.WhsCode, B.ItCode ");
            //SQL.AppendLine(") G On A.WhsCode = G.WhsCode And A.ItCode=G.ItCode ");

            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select A.WhsCode, B.ItCode, ");
            //SQL.AppendLine("    Sum(B.Qty)/C.Value As QtyAvgUsed3 ");
            //SQL.AppendLine("    From TblDODeptHdr A ");
            //SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
            //SQL.AppendLine("    Inner Join ( ");
            //SQL.AppendLine("        Select DateDiff(Date_Add(@DocDt, interval -1 month), Date_Add(@DocDt, interval -3 month)) As Value ");
            //SQL.AppendLine("    ) C On 0=0");
            //SQL.AppendLine("    Where A.DocDt Between DATE_FORMAT(Date_Add(@DocDt, interval -3 month),'%Y%m%d') And DATE_FORMAT(Date_Add(@DocDt, interval -1 month),'%Y%m%d')  ");
            //if (CcbWhsCode.Text.Length > 0)
            //    SQL.AppendLine("    And Find_In_Set(A.WhsCode, @WhsCode) ");
            //SQL.AppendLine("    And A.DORequestDeptDocNo Is Null ");
            //SQL.AppendLine("    Group By A.WhsCode, B.ItCode ");
            //SQL.AppendLine(") H On A.WhsCode = H.WhsCode And A.ItCode = H.ItCode  ");

            SQL.AppendLine("Left Join TblItemSubCategory I On E.ItScCode=I.ItScCode ");
            if (mIsRptDailyStockMovementUsePrice)
            {
                if (mIsMovingAvgEnabled)
                {
                    SQL.AppendLine("Left Join ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select ItCode, IfNull(MovingAvgPrice, 0) As Price ");
                    SQL.AppendLine("    From TblItemMovingAvg ");
                    SQL.AppendLine("    Group By ItCode ");
                    SQL.AppendLine(") J On A.ItCode = J.ItCode ");
                }
                else
                {
                    SQL.AppendLine("Left Join ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select ItCode, IfNull(Sum(UPrice*ExcRate), 0) As Price ");
                    SQL.AppendLine("    From TblStockPrice ");
                    SQL.AppendLine("    Group By ItCode ");
                    SQL.AppendLine(") J On A.ItCode = J.ItCode ");
                }
            }

            SQL.AppendLine("Where (IfNull(B.Init, 0)<>0 Or IfNull(C.I, 0)<>0 Or IfNull(D.O, 0)<>0) ");
            
            
            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            var NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length != 0)
                mUom = int.Parse(NumberOfInventoryUomCode);

            Grd1.Cols.Count = 36;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Warehouse",
                    "Date",
                    "Item's"+Environment.NewLine+"Code", 
                    "Item's Name",
                    "UoM"+Environment.NewLine+"(1)",   
                    
                    //6-10
                    "Initial"+Environment.NewLine+"(1)",
                    "In"+Environment.NewLine+"(1)", 
                    "Out"+Environment.NewLine+"(1)", 
                    "Last"+Environment.NewLine+"(1)", 
                    "UoM"+Environment.NewLine+"(2)",   
                    
                    //11-15
                    "Initial"+Environment.NewLine+"(2)",
                    "In"+Environment.NewLine+"(2)", 
                    "Out"+Environment.NewLine+"(2)", 
                    "Last"+Environment.NewLine+"(2)", 
                    "UoM"+Environment.NewLine+"(3)",   
                    
                    //16-20
                    "Initial"+Environment.NewLine+"(3)",
                    "In"+Environment.NewLine+"(3)", 
                    "Out"+Environment.NewLine+"(3)", 
                    "Last"+Environment.NewLine+"(3)",
                    "Real Stock"+Environment.NewLine+" All Warehouse",
                    
                    //21-25
                    "Average Used Item"+Environment.NewLine+"1 month",
                    "Average Used Item"+Environment.NewLine+"3 month",
                    "Re-Order",
                    "Sub-Category",
                    "Minimum Stock",

                    //26-30
                    "Price"+Environment.NewLine+"(Initial)",
                    "Price"+Environment.NewLine+"(In)",
                    "Price"+Environment.NewLine+"(Out)",
                    "Price"+Environment.NewLine+"(Last)",
                    "Local Code",

                    //31-35
                    "Specification",
                    "Item Category",
                    "Total Price",
                    "Total Price"+Environment.NewLine+"(In)",
                    "Total Price"+Environment.NewLine+"(Out)"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 150, 100, 200, 60, 
                    
                    //6-10
                    80, 80, 80, 80, 60, 
                    
                    //11-15
                    80, 80, 80, 80, 60, 

                    //16-20
                    80, 80, 80, 80, 100,
                    
                    //21-25
                    120, 120, 120, 150, 100,

                    //26-30
                    120, 120, 120, 120, 100,

                    //31-35
                    200, 180, 110, 120, 120
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 9, 11, 12, 13, 14, 16, 17, 18, 19, 20, 21, 22, 23, 25, 26, 27, 28, 29, 33, 34, 35 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 },false);
            Grd1.Cols[24].Move(5);
            Grd1.Cols[25].Move(21);
            Grd1.Cols[26].Move(8);
            Grd1.Cols[27].Move(10);
            Grd1.Cols[28].Move(12);
            Grd1.Cols[29].Move(14);
            Grd1.Cols[30].Move(5);
            Grd1.Cols[32].Move(6);
            Grd1.Cols[31].Move(6);
            Grd1.Cols[33].Move(25);
            Grd1.Cols[34].Move(14);
            Grd1.Cols[35].Move(17);

            if (!mIsRptDailyStockMovementUsePrice)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 26, 27, 28, 29, 33 }, false);
            }

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
            if (mUom >= 2)
                Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13, 14, 24 }, !ChkHideInfoInGrd.Checked);

            if (mUom == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18, 19, 24 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";
                var DocDt = Sm.ConvertDate(Sm.GetDte(DteDocDt1)); 
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                string a = Sm.GetDte(DteDocDt1);
                string b = Sm.Left(Sm.ServerCurrentDateTime(), 8);
                Sm.CmParam<String>(ref cm, "@WhsCode", ProcessCcb(Sm.GetCcb(CcbWhsCode)));

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "E1.ItCtCode", true);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By ItName;",
                        new string[]
                        { 
                            //0
                            "ItCode", 
                            
                            //1-5
                            "ItName", "InventoryUomCode", "Init", "I", "O", 
                            
                            //6-10
                            "InventoryUomCode2", "Init2", "I2", "O2", "InventoryUomCode3", 
                            
                            //11-15
                            "Init3", "I3", "O3", "QtyReal", "QtyAvgUsed", 

                            //16-20
                            "QtyAvgUsed3", "ReorderStock", "ItScName", "MinStock", "Price",

                            //21-24
                            "ItCodeInternal", "Specification", "ItCtName", "WhsName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 24);
                            Grd.Cells[Row, 2].Value = DocDt;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                            Grd.Cells[Row, 8].Value = Math.Abs(dr.GetDecimal(c[5]));
                            Grd.Cells[Row, 9].Value = Sm.GetGrdDec(Grd, Row, 6) + Sm.GetGrdDec(Grd, Row, 7) - Sm.GetGrdDec(Grd, Row, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                            Grd.Cells[Row, 13].Value = Math.Abs(dr.GetDecimal(c[9]));
                            Grd.Cells[Row, 14].Value = Sm.GetGrdDec(Grd, Row, 11) + Sm.GetGrdDec(Grd, Row, 12) - Sm.GetGrdDec(Grd, Row, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 12);
                            Grd.Cells[Row, 18].Value = Math.Abs(dr.GetDecimal(c[13]));
                            Grd.Cells[Row, 19].Value = Sm.GetGrdDec(Grd, Row, 16) + Sm.GetGrdDec(Grd, Row, 17) - Sm.GetGrdDec(Grd, Row, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 23);
                            Grd.Cells[Row, 33].Value = (Sm.GetGrdDec(Grd, Row, 11) + Sm.GetGrdDec(Grd, Row, 12) - Sm.GetGrdDec(Grd, Row, 13)) * Sm.GetGrdDec(Grd, Row, 29);
                            Grd.Cells[Row, 34].Value = Sm.GetGrdDec(Grd, Row, 7) * Sm.GetGrdDec(Grd, Row, 27);
                            Grd.Cells[Row, 35].Value = Sm.GetGrdDec(Grd, Row, 8) * Sm.GetGrdDec(Grd, Row, 28);

                            CheckData(Row);
                            //CheckData2(Row);
                        }, true, false, false, false
                    );
                if (mIsRptDailyStockMovementUsePrice)
                    AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void CheckData(int RowX)
        {
            decimal Last = Sm.GetGrdDec(Grd1, RowX, 9);
            decimal Reorder = Sm.GetGrdDec(Grd1, RowX, 23);

            if (Reorder > Last)
            {
                Grd1.Cells[RowX, 9].Font = new Font("Tahoma", 9, FontStyle.Bold);
                Grd1.Cells[RowX, 23].Font = new Font("Tahoma", 9, FontStyle.Bold);
            }
        }

        private void CheckData2(int RowX)
        {
            decimal Out = Sm.GetGrdDec(Grd1, RowX, 8);
            decimal Avg = Sm.GetGrdDec(Grd1, RowX, 22);

            if (Out > Avg)
            {
                Grd1.Rows[RowX].BackColor = Color.Red;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 26, 27, 28, 29, 33, 34, 35 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void SetCcbWhsCode(ref CheckedComboBoxEdit Ccb, string WhsName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.WhsName As Col From TblWarehouse T ");
            SQL.AppendLine("Where 0 = 0 ");
            if (WhsName.Length > 0)
            {
                SQL.AppendLine("And T.WhsName = @WhsName ");
            }
            else
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("    Where WhsCode=T.WhsCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By T.WhsName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsName", WhsName);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetCcb(ref Ccb, cm);
        }

        private string ProcessCcb(string Value)
        {
            if (Value.Length != 0)
            {
                Value = GetWhsCode(Value);
                //Value = "#" + Value.Replace(", ", "# #") + "#";
                //Value = Value.Replace("#", @"""");
            }
            return Value;
        }

        private string GetWhsCode(string Value)
        {
            if (Value.Length != 0)
            {
                Value = Value.Replace(", ", ",");

                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(Distinct T.WhsCode) WhsCode ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select WhsCode ");
                SQL.AppendLine("    From TblWarehouse ");
                SQL.AppendLine("    Where Find_In_Set(WhsName, @Param) ");
                SQL.AppendLine(")T; ");

                Value = Sm.GetValue(SQL.ToString(), Value);
            }

            return Value;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void CcbWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Warehouse");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item Category");
        }

        #endregion

        #endregion

        #region Class
        class PriceTemp
        {
            public string ItCode { get; set; }
            public decimal Price { get; set; }
        }
        #endregion

    }
}
