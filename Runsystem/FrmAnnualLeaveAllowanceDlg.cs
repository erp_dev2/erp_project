﻿#region Update
/*
    13/12/2018 [TKG] Tambah validasi employee yg sudah pernah diproses tidak boleh diproses lagi di tahun yg sama.
    16/04/2019 [WED] tambah filter month, kolom LeaveName dan EmploymentStatus
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAnnualLeaveAllowanceDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmAnnualLeaveAllowance mFrmParent;
        private string mSQL = string.Empty, mYr = string.Empty;

        #endregion

        #region Constructor

        public FrmAnnualLeaveAllowanceDlg(FrmAnnualLeaveAllowance FrmParent, string Year)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mYr = Year;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            this.Text = "List of Employee";
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            Sl.SetLueMth(LueMth);
            Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
            Sl.SetLueDeptCode(ref LueDeptCode);
            Sl.SetLuePosCode(ref LuePosCode);
        }

        #endregion

        #region Standard Method

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, B.DeptName, C.SiteName, D.PosName, ");
            SQL.AppendLine("G.OptDesc EmploymentStatus, E.LeaveCode, F.LeaveName, ");
            SQL.AppendLine("(IfNull(E.NoOfDays2, 0) + IfNull(E.NoOfDays3, 0) + IfNull(E.NoOfDays5, 0)) As LeaveTaken, ");
            SQL.AppendLine("IfNull(E.Balance, 0) As LeaveBalance ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            if (Sm.GetLue(LueMth).Length > 0)
            {
                SQL.AppendLine("    And A.LeaveStartDt Is Not Null ");
                SQL.AppendLine("    And Substr(LeaveStartDt, 5, 2) = @Mth ");
            }
            SQL.AppendLine("Left Join TblSite C On A.SiteCode = C.SiteCode ");
            SQL.AppendLine("Left Join TblPosition D On A.PosCode = D.PosCode ");
            SQL.AppendLine("Inner Join TblLeaveSummary E On A.EmpCode = E.EmpCode And E.Yr = @Yr And (IfNull(E.NoOfDays2, 0) + IfNull(E.NoOfDays3, 0)) >= @MinDayTakenForLeaveAllowance ");
            SQL.AppendLine("Inner Join TblLeave F On E.LeaveCode = F.LeaveCode ");
            SQL.AppendLine("Left Join TblOption G On A.EmploymentStatus = G.OptCode And G.OptCat = 'EmploymentStatus' ");
            SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And ResignDt>@CurrentDate)) ");
            SQL.AppendLine("And A.EmpCode Not In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T2.EmpCode ");
            SQL.AppendLine("    From TblAnnualLeaveAllowanceHdr T1 ");
            SQL.AppendLine("    Inner Join TblAnnualLeaveAllowanceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.Yr = @Yr ");
            SQL.AppendLine("    And T1.CancelInd = 'N' ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Employee's Code", 
                    "Employee's Name",
                    "Site",
                    "Department",

                    //6-10
                    "Position",
                    "Employment"+Environment.NewLine+"Status",
                    "Leave Code",
                    "Leave Name",
                    "Leave Taken (Days)",

                    //11
                    "Leave Balance (Days)",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 100, 250, 200, 200, 
                    
                    //6-10
                    250, 120, 100, 150, 120, 
                    
                    //11
                    130
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 8 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter = string.Empty, EmpCode = string.Empty;

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        EmpCode = Sm.GetGrdStr(mFrmParent.Grd1, r, 2);
                        if (EmpCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " A.EmpCode=@EmpCode0" + r.ToString();
                            Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                        }
                    }
                }

                if (Filter.Length != 0)
                    Filter = " And Not ( " + Filter + ") ";
                else
                    Filter = " ";

                //Sm.CmParam<String>(ref cm, "@SelectedEmployee", mFrmParent.GetSelectedEmployee());
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.CmParam<String>(ref cm, "@Yr", mYr);
                if (Sm.GetLue(LueMth).Length > 0)
                    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<Decimal>(ref cm, "@MinDayTakenForLeaveAllowance", (mFrmParent.mMinDayTakenForLeaveAllowance.Length > 0) ? decimal.Parse(mFrmParent.mMinDayTakenForLeaveAllowance) : 0m);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName", "A.DisplayName", "A.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePosCode), "A.PosCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL() + Filter + " Order By A.EmpName;",
                    new string[] 
                    { 
                        "EmpCode", 
                        "EmpName", "SiteName", "DeptName", "PosName", "EmploymentStatus", 
                        "LeaveCode", "LeaveName", "LeaveTaken", "LeaveBalance"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 8);
                        mFrmParent.Grd1.Rows.Add();
                        mFrmParent.ComputeAmt();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 employee.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            var EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), EmpCode)) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkPosCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Position");
        }

        #endregion

        #endregion

    }
}
