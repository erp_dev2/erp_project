﻿#region Update
/*
    03/04/2017 Outstanding quantity berdasarkan PO request dan pemutihan MR
 *  16/11/2020 Mengubah Keterangan Material Request menjadi Purchase Request
 *  09/12/2020 [ICA/IMS] Mengubah keterangan Material Request menjadi Purchase Request pada Alert
*/ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMRQtyCancel : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal bool
            mIsFilterBySite = false,
            mIsFilterByDept = false,
            mIsAutoGeneratePurchaseLocalDocNo = false;
        internal string mDocTitle = string.Empty;
        private List<LocalDocument> mlLocalDocument = null;
        internal FrmMRQtyCancelFind FrmFind;

        #endregion

        #region Constructor

        public FrmMRQtyCancel(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0)
            {
                if (mDocTitle == "IMS")
                    this.Text = "Cancellation of Purchase Request Quantity";
                else
                    this.Text = "Cancellation of Material Request Quantity";
            }

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                GetParameter();
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                mlLocalDocument = new List<LocalDocument>();
                if (mDocTitle == "IMS")
                {
                    this.label4.Text = "Purchase Request#";
                    this.label4.Location = new System.Drawing.Point(18, 79);
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtLocalDocNo, TxtMRDocNo, TxtItCode, 
                        TxtDeptCode, TxtItName, TxtOutstandingQtyMR, TxtCancelledQtyMR, TxtQtyMR, 
                        MeeRemark
                    }, true);
                    BtnMRDocNo.Enabled = false;
                    TxtMRDNo.Visible = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtCancelledQtyMR, MeeRemark 
                    }, false);
                    if (!mIsAutoGeneratePurchaseLocalDocNo)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);
                    BtnMRDocNo.Enabled = true;
                    TxtMRDNo.Visible = false;
                    ChkCancelInd.Checked = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNo, DteDocDt, TxtLocalDocNo, TxtMRDocNo, TxtItCode, 
               TxtDeptCode, TxtItName, MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtOutstandingQtyMR, TxtCancelledQtyMR, TxtQtyMR,
            }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmMRQtyCancelFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            string LocalDocNo = mIsAutoGeneratePurchaseLocalDocNo ? string.Empty : TxtLocalDocNo.Text;
            string
                SeqNo = string.Empty,
                DeptCode = string.Empty,
                ItSCCode = string.Empty,
                Mth = string.Empty,
                Yr = string.Empty,
                Revision = string.Empty;

            if (mIsAutoGeneratePurchaseLocalDocNo)
            {
                SetLocalDocument(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr
                );
                if (mlLocalDocument.Count == 0) return;

                SetRevision(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr,
                    ref Revision
                    );

                if (SeqNo.Length > 0)
                {
                    SetLocalDocNo(
                        "MRQtyCancel",
                        ref LocalDocNo,
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr,
                        ref Revision
                        );
                }
                mlLocalDocument.Clear();
            }

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "MRQtyCancel", "TblMRQtyCancel");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveMRCancelQty(
                DocNo,
                LocalDocNo,
                SeqNo,
                DeptCode,
                ItSCCode,
                Mth,
                Yr,
                Revision
                ));

            cml.Add(UpdateMaterialRequestProcessInd());

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtMRDocNo, ((mDocTitle == "IMS") ? "Purchase Request#" : "Material Request#"), false) ||
                Sm.IsTxtEmpty(TxtCancelledQtyMR, "Quantity Cancelled", false) ||
                IsQtyCancelBiggerThanQtyOutstanding();
        }

        #region Generate Local Document

        private void SetLocalDocNo(
            string DocType,
            ref string LocalDocNo,
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            var ShortCode = Sm.GetValue("Select IfNull(ShortCode, DeptCode) From TblDepartment Where DeptCode='" + DeptCode + "'");
            LocalDocNo = SeqNo + "/" + DocAbbr + "/" + ShortCode + "/" + ItSCCode + "/" + Mth + "/" + Yr;
            if (Revision.Length > 0 && Revision != "0")
                LocalDocNo = LocalDocNo + "/R" + Revision;
        }

        private void SetRevision(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select IfNull(Revision, '0') ");
            SQL.AppendLine("From TblMRQtyCancel ");
            SQL.AppendLine("Where SeqNo Is Not Null ");
            SQL.AppendLine("And SeqNo=@SeqNo ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And ItSCCode=@ItSCCode ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr ");
            SQL.AppendLine("Order By Revision Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            Revision = Sm.GetValue(cm);
            if (Revision.Length == 0)
                Revision = "0";
            else
                Revision = (int.Parse(Revision) + 1).ToString();
        }


        private void SetLocalDocument(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr)
        {
            mlLocalDocument.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision ");
            SQL.AppendLine("From TblMaterialRequestHdr Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtMRDocNo.Text);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "LocalDocNo", "SeqNo", "DeptCode", "ItSCCode", "Mth", 
                    
                    //6-7
                    "Yr", "Revision" 
                });
                if (dr.HasRows)
                {
                    string
                        DocNoTemp = string.Empty,
                        LocalDocNoTemp = string.Empty,
                        SeqNoTemp = string.Empty,
                        DeptCodeTemp = string.Empty,
                        ItSCCodeTemp = string.Empty,
                        MthTemp = string.Empty,
                        YrTemp = string.Empty,
                        RevisionTemp = string.Empty;

                    while (dr.Read())
                    {
                        DocNoTemp = Sm.DrStr(dr, c[0]);
                        LocalDocNoTemp = Sm.DrStr(dr, c[1]);
                        SeqNoTemp = Sm.DrStr(dr, c[2]);
                        DeptCodeTemp = Sm.DrStr(dr, c[3]);
                        ItSCCodeTemp = Sm.DrStr(dr, c[4]);
                        MthTemp = Sm.DrStr(dr, c[5]);
                        YrTemp = Sm.DrStr(dr, c[6]);
                        RevisionTemp = Sm.DrStr(dr, c[7]);

                        mlLocalDocument.Add(new LocalDocument()
                        {
                            DocNo = DocNoTemp,
                            LocalDocNo = LocalDocNoTemp,
                            SeqNo = SeqNoTemp,
                            DeptCode = DeptCodeTemp,
                            ItSCCode = ItSCCodeTemp,
                            Mth = MthTemp,
                            Yr = YrTemp,
                            Revision = RevisionTemp
                        });

                        if (SeqNoTemp.Length > 0)
                        {
                            SeqNo = SeqNoTemp;
                            DeptCode = DeptCodeTemp;
                            ItSCCode = ItSCCodeTemp;
                            Mth = MthTemp;
                            Yr = YrTemp;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        private MySqlCommand SaveMRCancelQty(
            string DocNo,
            string LocalDocNo,
            string SeqNo,
            string DeptCode,
            string ItSCCode,
            string Mth,
            string Yr,
            string Revision
            )
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblMRQtyCancel ");
            SQL.AppendLine("(DocNo, DocDt, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ");
            SQL.AppendLine("CancelInd, MRDocNo, MRDNo, Qty, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @LocalDocNo, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, ");
            SQL.AppendLine("'N', @MRDocNo, @MRDno, @Qty, @Remark, @CreateBy, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            Sm.CmParam<String>(ref cm, "@MRDocNo", TxtMRDocNo.Text);
            Sm.CmParam<String>(ref cm, "@MRDNo", TxtMRDNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Qty", Decimal.Parse(TxtCancelledQtyMR.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateMaterialRequestProcessInd()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequestDtl T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.DocNo, A.DNo, A.Qty, IfNull(B.Qty, 0)+IfNull(C.Qty, 0) As Qty2 ");
            SQL.AppendLine("    From TblMaterialRequestDtl A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select X2.MaterialRequestDocNo As DocNo, X2.MaterialRequestDNo As DNo, ");
            SQL.AppendLine("        Sum(X1.Qty) As Qty ");
            SQL.AppendLine("        From TblPODtl X1 ");
            SQL.AppendLine("        Inner Join TblPORequestDtl X2 ");
            SQL.AppendLine("            On X1.PORequestDocNo=X2.DocNo ");
            SQL.AppendLine("            And X1.PORequestDNo=X2.DNo ");
            SQL.AppendLine("            And X2.MaterialRequestDocNo=@DocNo ");
            SQL.AppendLine("            And X2.MaterialRequestDNo=@DNo ");
            SQL.AppendLine("        Where X1.CancelInd='N' ");
            SQL.AppendLine("        Group By X2.MaterialRequestDocNo, X2.MaterialRequestDNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select X.MRDocNo As DocNo, X.MRDNo As DNo, Sum(X.Qty) As Qty ");
            SQL.AppendLine("        From TblMRQtyCancel X ");
            SQL.AppendLine("        Where X.CancelInd='N' ");
            SQL.AppendLine("        And X.MRDocNo=@DocNo ");
            SQL.AppendLine("        And X.MRDNo=@DNo ");
            SQL.AppendLine("        Group By X.MRDocNo, X.MRDNo ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.Status In ('O', 'A') ");
            SQL.AppendLine("    And A.DocNo=@DocNo ");
            SQL.AppendLine("    And A.DNo=@DNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo ");
            SQL.AppendLine("Set T1.ProcessInd=");
            SQL.AppendLine("    Case When T2.Qty2=0 Then 'O' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If((T2.Qty-T2.Qty2)<=0, 'F', 'P') ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where T1.DocNo=@DocNo And T1.DNo=@DNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtMRDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", TxtMRDNo.Text);

            return cm;
        }


        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (IsCancelledDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            
            var cml = new List<MySqlCommand>();

            cml.Add(EditMRQtyCancel());
            cml.Add(UpdateMaterialRequestProcessInd());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, ((mDocTitle == "IMS") ? "Purchase Request Cancelled" : "Material Request Cancelled"), false) ||
                IsCancelIndEditedAlready();
        }

        private bool IsCancelIndEditedAlready()
        {
            var CancelInd = ChkCancelInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblMRQtyCancel Where DocNo=@DocNo And CancelInd='Y' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditMRQtyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblMRQtyCancel Set CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowMRQtyCancel(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowMRQtyCancel(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocNo, T.CancelInd, T.DocDt, T.LocalDocNo, T.MRDocNo, T.MRDno, T.DeptName, T.ItCode, T.ItName, T.ItCtName, T.PurchaseUomCode, T.Remark, T.Qty, T.OutstandingQty, T.CancelQty, ");
            SQL.AppendLine("T.CreateBy, T.CreateDt, T.LastUpBy, T.LastUpDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select H.DocNo, H.CancelInd, H.DocDt, H.LocalDocNo, H.MRDocNo, H.MRDNo, C.DeptName, B.ItCode, D.ItName, E.ItCtName, D.PurchaseUomCode, H.Remark, ");
            SQL.AppendLine("    B.Qty, B.Qty-IfNull(( ");
            SQL.AppendLine("        Select Sum(T.Qty) From TblPORequestDtl T ");
            SQL.AppendLine("        Where T.CancelInd='N' ");
            SQL.AppendLine("        And IfNull(T.Status,'X')<>'C' ");
            SQL.AppendLine("        And T.MaterialRequestDocNo=A.DocNo ");
            SQL.AppendLine("        And T.MaterialRequestDNo=B.DNo ");
            SQL.AppendLine("      ), 0)- ifnull(H.Qty, 0) As OutstandingQty, ");
            SQL.AppendLine("    ifnull(H.Qty, 0) As CancelQty, H.CreateBy, H.CreateDt, H.LastUpBy, H.LastUpDt ");
            SQL.AppendLine("    From TblMaterialRequestHdr A ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And IfNull(B.Status,'X')='A' And B.CancelInd='N' ");
            SQL.AppendLine("    Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("    Left Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("    Left Join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
            SQL.AppendLine("    Inner Join TblMRQtyCancel H On B.DocNo = H.MRDocNo And B.DNo = H.MRDNo ");
            SQL.AppendLine(") T Where T.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "LocalDocNo", "MRDocNo", "MRDNo",   
                        
                        //6-10
                        "ItCode", "ItName", "DeptName", "Qty", "OutstandingQty", 
                        
                        //11-12
                        "CancelQty", "Remark", 
                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[3]);
                    TxtMRDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtMRDNo.Text = Sm.DrStr(dr, c[5]);
                    TxtItCode.EditValue = Sm.DrStr(dr, c[6]);
                    TxtItName.EditValue = Sm.DrStr(dr, c[7]);
                    TxtDeptCode.EditValue = Sm.DrStr(dr, c[8]);
                    TxtQtyMR.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                    TxtOutstandingQtyMR.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                    TxtCancelledQtyMR.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                }, true
            );

        }



        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsAutoGeneratePurchaseLocalDocNo = Sm.GetParameter("IsAutoGeneratePurchaseLocalDocNo") == "Y";
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mDocTitle = Sm.GetParameter("DocTitle");
        }

        private bool IsQtyCancelBiggerThanQtyOutstanding()
        {
            decimal QtyOut = Decimal.Parse(TxtOutstandingQtyMR.Text);
            decimal QtyCancel = Decimal.Parse(TxtCancelledQtyMR.Text);

            if (QtyCancel > QtyOut)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Cancelled Quantity (" + Sm.FormatNum(QtyCancel, 0) + ") " + Environment.NewLine
                    + "is bigger than Outstanding Quantity (" + Sm.FormatNum(QtyOut, 0) + ").");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnMRDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMRQtyCancelDlg(this));
        }

        private void TxtCancelledQtyMR_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtCancelledQtyMR, 0);
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }

        #endregion
        
        #endregion

        #region Class

        private class LocalDocument
        {
            public string DocNo { set; get; }
            public string LocalDocNo { set; get; }
            public string SeqNo { get; set; }
            public string DeptCode { get; set; }
            public string ItSCCode { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string Revision { get; set; }
        }

        #endregion
    }
}
