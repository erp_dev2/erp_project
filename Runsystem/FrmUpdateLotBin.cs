﻿#region Update
/*
 [DITA/MMM] New Apps
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmUpdateLotBin : RunSystem.FrmBase3
    {
        #region Field, Property

        internal FrmUpdateLotBinFind FrmFind;
        internal string mMenuCode = "", mAccessInd = "", mDocNo= "";
        iGCell fCell;
        bool fAccept;
        private List<UpdateLotBin> l = null;
        private List<StockSummary> l2 = null;

        #endregion

        #region Constructor

        public FrmUpdateLotBin(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetLueLot(ref LueLot, string.Empty);
                LueLot.Visible = false;
                LueBin.Visible = false;
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Lot" +Environment.NewLine+ "(Stock)",
			            "Lot" +Environment.NewLine+ "(New)",
                        "Bin" +Environment.NewLine+ "(Stock)",   
			            "Bin" +Environment.NewLine+ "(New)",

                        //6-10 
                        "Item's Code",
                        "Item's Name", 
                        "Property",
                        "Batch#",
                        "Source",

                        //11-15
                        "Quantity",
                        "Quantity 2", 
                        "Quantity 3",
			            "Remark",
			            "CreateBy",

                        //16-18
			            "CreateDt",
			            "LastUpBy",
			            "LastUpDt"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        20, 100, 100, 100, 100,
                        
                        //6-10
                        80, 200, 80, 200, 200, 
                        
                        //11-15
                        100, 0, 0, 0, 0, 0,

                        //16-17
                        0, 0

                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 16, 18 });
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 6, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18}, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 9 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, MeeRemark,
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 5 });
                    Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, MeeRemark 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3, 5 });
                    Sm.GrdColInvisible(Grd1, new int[] { 1 }, true);
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, MeeRemark
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 11, 12, 13 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmUpdateLotBinFind(this);
                Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmUpdateLotBinDlg(this, Sm.GetLue(LueWhsCode)));
                }

                if (Sm.IsGrdColSelected(new int[] { 1, 3, 5 }, e.ColIndex))
                {
                    if (Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
                    {
                        LueRequestEdit(Grd1, LueLot, ref fCell, ref fAccept, e, 3);
                        //SetLueLot(ref LueLot);
                    }

                    if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
                    {
                        LueRequestEdit(Grd1, LueBin, ref fCell, ref fAccept, e, 5);
                        SetLueBin(ref LueBin, Sm.GetGrdStr(Grd1, e.RowIndex, 3));
                    }

                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 12, 13 });
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                Sm.FormShowDialog(new FrmUpdateLotBinDlg(this, Sm.GetLue(LueWhsCode)));
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            l = new List<UpdateLotBin>();
            l2 = new List<StockSummary>();

            Processl(ref l);
            Processl2(ref l, ref l2);
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "UpdateLotBin", "TblUpdateLotBinHdr");

            var cml = new List<MySqlCommand>();
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                    cml.Add(DeleteStockSummary(r));

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                    cml.Add(SaveStockSummary(r));

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                    cml.Add(UpdateStockMovement(r));

            cml.Add(SaveUpdateLotBinHdr(DocNo));

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                    cml.Add(SaveUpdateLotBinDtl(DocNo, r));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid()||
                IsLotBinValid(ref l, ref l2) ||
                IsLotBinValid2(ref l);
        }

        private bool IsLotBinValid(ref List<UpdateLotBin> l, ref List<StockSummary> l2)
        {
            string mLotBinNew = string.Empty, mLotBinOld= string.Empty, mData = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                for (int j = 0; j < l2.Count; j++)
                {
                    mLotBinNew = string.Concat(l[i].LotNew, l[i].BinNew, l[i].ItCode, l[i].WhsCode, l[i].PropCode, l[i].BatchNo, l[i].Source);
                    mLotBinOld = string.Concat(l2[j].LotStock, l2[j].BinStock, l2[j].ItCode, l2[j].WhsCode, l2[j].PropCode, l2[j].BatchNo, l2[j].Source);
                    if (mLotBinNew == mLotBinOld)
                    {
                        if(l[i].LotNew != l[i].LotStock || l[i].BinNew != l[i].BinStock)
                            l[i].ValidInd = "Y";
                        break;
                    }
                }
            }

            foreach (var x in l2.Where(w => w.ValidInd == "N"))
            {
                foreach (var y in l.Where(w => w.WhsCode == x.WhsCode &&
                    w.LotNew == x.LotStock &&
                    w.BinNew == x.BinStock &&
                    w.ItCode == x.ItCode &&
                    w.PropCode == x.PropCode &&
                    w.BatchNo == x.BatchNo &&
                    w.Source == x.Source))
                {
                    if (mData.Length > 0) mData += ',';
                    mData += (y.Row + 1).ToString();
                }

                if (mData.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "You can't process this row(s) of data " +
                        "\ndue to data duplication in stock summary, row# : " + Environment.NewLine +mData);
                    return true;
                }
            }
            return false;
        }

        private bool IsLotBinValid2(ref List<UpdateLotBin> l)
        {
            string mLotBinNew = string.Empty, mLotBinOld = string.Empty, mData = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                for (int j = 0; j < l.Count; j++)
                {
                    mLotBinNew = string.Concat(l[i].LotNew, l[i].BinNew, l[i].ItCode, l[i].WhsCode, l[i].PropCode, l[i].BatchNo, l[i].Source);
                    mLotBinOld = string.Concat(l[j].LotNew, l[j].BinNew, l[j].ItCode, l[j].WhsCode, l[j].PropCode, l[j].BatchNo, l[j].Source);
                    if (l[i].Row != l[j].Row)
                    {
                        if (mLotBinNew == mLotBinOld)
                        {
                            if (mData.Length > 0) mData += Environment.NewLine;
                            mData += string.Concat((l[i].Row + 1).ToString(), "->", (l[j].Row + 1).ToString());
                        }
                    }
                }
            }

            if (mData.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You can't process this row(s) of data " +
                    "\ndue to data duplication in Grid, row# : " + Environment.NewLine +mData);
                return true;
            }

            
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 100000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (99.999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Lot(New)is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 5, false, "Bin(New)is empty.")) return true;
              
            }
            return false;
        }

        private MySqlCommand SaveUpdateLotBinHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblUpdateLotBinHdr(DocNo, DocDt, WhsCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @WhsCode, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveUpdateLotBinDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblUpdateLotBinDtl(DocNo, DNo, LotStock, LotNew, BinStock, BinNew, ItCode, BatchNo, ");
            SQL.AppendLine("PropCode, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @LotStock, @LotNew, @BinStock, @BinNew, @ItCode, @BatchNo, ");
            SQL.AppendLine("@PropCode, @Source, @Qty, @Qty2, @Qty3, @Remark, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 5));
            Sm.CmParam<String>(ref cm, "@LotStock", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@LotNew", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@BinStock", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@BinNew", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateStockMovement(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("UPDATE TblStockMovement ");
            SQL.AppendLine("SET Lot=@LotNew, Bin=@BinNew, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("WHERE WhsCode=@WhsCode And Source=@Source AND ItCode=@ItCode And BatchNo=@BatchNo  ");
            SQL.AppendLine("AND PropCode=@PropCode AND Lot=@LotStock AND Bin=@BinStock And CancelInd = 'N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@LotNew", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@BinNew", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@LotStock", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@BinStock", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeleteStockSummary(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblStockSummary ");
            SQL.AppendLine("WHERE WhsCode=@WhsCode And Source=@Source AND ItCode=@ItCode And BatchNo=@BatchNo  ");
            SQL.AppendLine("AND PropCode=@PropCode AND Lot=@LotStock AND Bin=@BinStock; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@LotStock", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@BinStock", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("PropCode, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Values(@WhsCode, @Lot, @Bin, @ItCode, @BatchNo, ");
            SQL.AppendLine("@PropCode, @Source, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, @CreateDt, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@CreateBy", Sm.GetGrdStr(Grd1, Row, 15));
            Sm.CmParam<String>(ref cm, "@CreateDt", Sm.GetGrdStr(Grd1, Row, 16));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowUpdateLotBinHdr(DocNo);
                ShowUpdateLotBinDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowUpdateLotBinHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, WhsCode, Remark ");
            SQL.AppendLine("From TblUpdateLotBinHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DocNo", 
                    "DocDt", "WhsCode",  "Remark",
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[3]);

                }, true
            );
        }

        private void ShowUpdateLotBinDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItName, A.LotStock, A.LotNew, A.BinStock, A.BinNew, A.BatchNo, ");
            SQL.AppendLine("A.PropCode, A.Source, A.Qty, A.Qty2, A.Qty3, A.Remark ");
            SQL.AppendLine("From TblUpdateLotBinDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "LotStock", "LotNew", "BinStock", "BinNew","ItCode",   
                    
                    //6-10
                    "ItName", "PropCode", "BatchNo", "Source", "Qty", 
                    
                    //11-13
                    "Qty2", "Qty3", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 12, 13 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void Processl(ref List<UpdateLotBin> l)
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                l.Add(new UpdateLotBin() 
                {
                    LotStock = Sm.GetGrdStr(Grd1, i, 2),
                    LotNew = Sm.GetGrdStr(Grd1, i, 3),
                    BinStock = Sm.GetGrdStr(Grd1, i, 4),
                    BinNew = Sm.GetGrdStr(Grd1, i, 5),
                    WhsCode = Sm.GetLue(LueWhsCode),
                    ItCode = Sm.GetGrdStr(Grd1, i, 6),
                    PropCode = Sm.GetGrdStr(Grd1, i, 8),
                    BatchNo = Sm.GetGrdStr(Grd1, i, 9),
                    Source = Sm.GetGrdStr(Grd1, i, 10),
                    ValidInd = "N",
                    Row = i
                });
                
            }
        }

        private void Processl2(ref List<UpdateLotBin> l ,ref List<StockSummary> l2)
        {
            string mUpdateLotBin = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                if (mUpdateLotBin.Length > 0) mUpdateLotBin += ",";
                mUpdateLotBin = string.Concat(l[i].LotNew, l[i].BinNew, l[i].ItCode, l[i].WhsCode, l[i].PropCode, l[i].BatchNo, l[i].Source);
            }
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                var SQL2 = new StringBuilder();

                SQL2.AppendLine("Select * ");
                SQL2.AppendLine("From TblStockSummary ");
                SQL2.AppendLine("Where Find_In_Set(Concat(Lot,Bin,ItCode,WhsCode,PropCode,BatchNo,Source), @UpdateLotBin) ");

                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm, "@UpdateLotBin", mUpdateLotBin);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] {
                    //0
                    "Lot",

                    //1-5
                    "Bin" ,
                    "ItCode",
                    "WhsCode",
                    "PropCode",
                    "BatchNo",

                    //6
                    "Source"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new StockSummary()
                        {
                            LotStock = Sm.DrStr(dr, c[0]),
                            BinStock = Sm.DrStr(dr, c[1]),
                            ItCode = Sm.DrStr(dr, c[2]),
                            WhsCode = Sm.DrStr(dr, c[3]),
                            PropCode = Sm.DrStr(dr, c[4]),
                            BatchNo = Sm.DrStr(dr, c[5]),
                            Source = Sm.DrStr(dr, c[6]),
                            ValidInd = "N"
                        });
                    }
                }
                dr.Close();
            }
        }

        internal string GetSelectedItem()
        {
            string SQL = string.Empty;

            string 
                ItCode = string.Empty, 
                Lot = string.Empty, 
                Bin = string.Empty, 
                PropCode = string.Empty, 
                BatchNo = string.Empty, 
                Source = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                Lot = Sm.GetGrdStr(Grd1, i, 2);
                Bin = Sm.GetGrdStr(Grd1, i, 4);
                ItCode = Sm.GetGrdStr(Grd1, i, 6);
                PropCode = Sm.GetGrdStr(Grd1, i, 8);
                BatchNo = Sm.GetGrdStr(Grd1, i, 9);
                Source = Sm.GetGrdStr(Grd1, i, 10);

                if (SQL.Length > 0) SQL += ",";
                SQL += string.Concat(Lot, Bin, ItCode, PropCode, BatchNo, Source);
            }

            return SQL.Length == 0 ? "XXX" : SQL;
            
        }

        public static void SetLueLot(ref LookUpEdit Lue, string WhsCode)
        {
            if (WhsCode.Length > 0)
                Sm.SetLue1(ref Lue, "Select Distinct A.Lot As Col1 From TblLotHdr A Inner Join TblWhsLotDtl B On A.Lot = B.Lot And B.WhsCode = '" + WhsCode + "' Where A.ActInd='Y' Order By A.Lot ; ", "Lot");
        }

        public static void SetLueBin(ref LookUpEdit Lue, string Lot)
        {
            if (Lot.Length > 0)
                Sm.SetLue1(ref Lue, "Select Distinct Bin As Col1 from TblBin Where ActInd='Y' And Bin In (Select Bin From TblLotDtl Where Lot='" + Lot + "') Order By Bin;", "Bin");
        }

        private void LueRequestEdit(
         iGrid Grd,
         DevExpress.XtraEditors.LookUpEdit Lue,
         ref iGCell fCell,
         ref bool fAccept,
         TenTec.Windows.iGridLib.iGRequestEditEventArgs e,
         int Col)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, Col).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, Col));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;

        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
                SetLueLot(ref LueLot, Sm.GetLue(LueWhsCode));
                ClearGrd();
            }
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue2(SetLueBin), string.Empty);
        }

        private void LueBin_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_Leave(object sender, EventArgs e)
        {
            if (LueBin.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueBin).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 5].Value = LueBin.GetColumnValue("Col1");
                }
                LueBin.Visible = false;
            }
        }

        private void LueLot_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLot, new Sm.RefreshLue2(SetLueLot), string.Empty);
            SetLueBin(ref LueBin, Sm.GetLue(LueLot));
        }

        private void LueLot_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueLot_Leave(object sender, EventArgs e)
        {
            if (LueLot.Visible && fAccept && fCell.ColIndex == 3)
            {
                string mLot = string.Empty;
                if (Sm.GetLue(LueLot).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 3].Value = LueLot.GetColumnValue("Col1");
                    mLot = LueLot.GetColumnValue("Col1").ToString();
                }
                Grd1.Cells[fCell.RowIndex, 5].Value = null;
                //SetLueBin(ref LueBin, mLot);
                LueLot.Visible = false;
            }
        }

        #endregion

        #endregion

        #region Class

        private class UpdateLotBin
        {
            public string LotStock{ get; set; }
            public string LotNew{ get; set; }
            public string BinStock{ get; set; }
            public string BinNew{ get; set; }
            public string WhsCode{ get; set; }
            public string ItCode{ get; set; }
            public string PropCode{ get; set; }
            public string BatchNo{ get; set; }
            public string Source { get; set; }
            public string ValidInd { get; set; }
            public int Row { get; set; }
        }

        private class StockSummary
        {
            public string LotStock { get; set; }
            public string BinStock { get; set; }
            public string WhsCode { get; set; }
            public string ItCode { get; set; }
            public string PropCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string ValidInd { get; set; }
        }

        #endregion
    }
}
