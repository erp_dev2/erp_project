﻿#region Update
/*
    22/01/2018 [TKG] tambah filter due date, informasi nomor giro/cheque, bank, due date dan total     
    06/08/2018 [TKG] tambah filter berdasarkan otorisasi group terhadap entity
    24/10/2018 [DITA] tambah kolom PO DocNo
    16/11/2021 [ICA/AMKA] mengaktikan lup untuk doctype Return AP Downpayment dan Cancel Return AP Downpayment
    24/11/2021 [NJP/AMKA] Membuat Reporting Vendor Deposit Movement menampilkan data di baris Create Voucher (Downpayment) dan Cancel Create Voucher (Downpayment) berdasarkan Department dari Group User dan menambah kolom departemen menggunakan parameter IsRptVendorDepositMovementFileredByDept, IsRptVendorDepositMovementUseDept
    24/11/2021 [NJP/AMKA] Membuat Reporting Vendor Deposit Movement menampilkan data di baris Return AP Downpayment dan Cancel Return AP Downpayment berdasarkan Department dari Group User dan menambah kolom departemen menggunakan parameter IsRptVendorDepositMovementFileredByDept, IsRptVendorDepositMovementUseDept
    24/11/2021 [NJP/AMKA] Membuat Reporting Vendor Deposit Movement menampilkan data di baris Create Purchase Invoice with Downpayment dan Cancel Purchase Invoice with Downpayment berdasarkan Department dari Group User dan menambah kolom departemen menggunakan parameter IsRptVendorDepositMovementFileredByDept, IsRptVendorDepositMovementUseDept
    13/10/2022 [HPH/PRODUCT] Membuat amount dari Return berubah menjadi AP deposit customer terpilih ketika transaksi yang berdasarkan DO sudah terbayar semua
    28/10/2022 [TYO/AMKA] Menambah kolom Amt Before Tax, Tax1-3, TaxAmt1-3
    09/12/2022 [BRI/ALL] bug parameter mDepositVendorReturnSource
    21/03/2023 [RDA/PHT] benerin bug filter dari mDepositVendorReturnSource
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptVendorDepositMovement : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private string mDepositVendorReturnSource = string.Empty;
        private bool 
            mIsAPDownpaymentUseEntity = false, 
            mIsFilterByEnt = false,
            mIsRptVendorDepositMovementFileredByDept = false, 
            mIsRptVendorDepositMovementUseDept = false,
            mIsPurchaseInvoiceUseTabToInputDownpayment = false;

        #endregion

        #region Constructor

        public FrmRptVendorDepositMovement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -2);
                Sl.SetLueVdCode(ref LueVdCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsAPDownpaymentUseEntity = Sm.GetParameterBoo("IsAPDownpaymentUseEntity");
            mIsFilterByEnt = Sm.GetParameterBoo("IsFilterByEnt");
            mIsRptVendorDepositMovementFileredByDept = Sm.GetParameterBoo("IsRptVendorDepositMovementFileredByDept");
            mIsRptVendorDepositMovementUseDept = Sm.GetParameterBoo("IsRptVendorDepositMovementUseDept");
            mDepositVendorReturnSource = Sm.GetParameter("DepositVendorReturnSource");
            mIsPurchaseInvoiceUseTabToInputDownpayment = Sm.GetParameterBoo("IsPurchaseInvoiceUseTabToInputDownpayment ");

            if (mDepositVendorReturnSource.Length == 0) mDepositVendorReturnSource = "1";
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT * FROM ( ");
            SQL.AppendLine("Select A.DocNo, A.DocType, C.OptDesc, ");
            if (mDepositVendorReturnSource == "1")
                SQL.AppendLine("A.DocDt, A.Amt,");
            else if (mDepositVendorReturnSource == "2")
                SQL.AppendLine("if(A.DocType='07',I.DocDt,A.DocDt) DocDt, if(A.DocType='07', I.Amt, A.Amt) Amt, ");
            SQL.AppendLine("A.VdCode, B.VdName, A.CurCode, D.EntName, F.BankName, E.GiroNo, E.DueDt, G.PODocNo, H.DeptName, A.CreateDt ");
            SQL.AppendLine(", J.AmtBefTax, J.TaxName, J.TaxAmt, J.TaxName2, J.TaxAmt2, J.TaxName3, J.TaxAmt3 ");
            SQL.AppendLine("From TblVendorDepositMovement A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
            SQL.AppendLine("Inner Join TblOption C On A.Doctype = C.OptCode And C.OptCat = 'VendorDepositDocType' ");
            SQL.AppendLine("Left Join TblEntity D On A.EntCode=D.EntCode ");
            SQL.AppendLine("Left Join TblVoucherHdr E On A.DocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblBank F On E.BankCode=F.BankCode ");
            SQL.AppendLine("Left Join tblapdownpayment G on G.VoucherRequestDocNo = E.VoucherRequestDocNo ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("			SELECT DISTINCT T.DocNo, T2.DeptCode, T3.DeptName FROM TblVendorDepositMovement T ");
            SQL.AppendLine("			INNER JOIN tblvoucherhdr T1 ON T.DocNo = T1.DocNo ");
            SQL.AppendLine("			INNER JOIN tblvoucherrequesthdr T2 ON T1.VoucherRequestDocNo = T2.DocNo ");
            SQL.AppendLine("			Inner JOIN tbldepartment T3 ON T2.DeptCode = T3.DeptCode ");
            SQL.AppendLine("			WHERE T.DocType IN ('01','02', '09', '10') ");
            SQL.AppendLine("			UNION ALL ");
            SQL.AppendLine("			SELECT DISTINCT T.DocNo, T5.DeptCode, T5.DeptName FROM TblVendorDepositMovement T ");
            SQL.AppendLine("			INNER JOIN tblpurchaseinvoicehdr T1 ON T.DocNo = T1.DocNo ");
            SQL.AppendLine("			INNER JOIN tbldepartment T5 ON T1.DeptCode = T5.DeptCode ");
            SQL.AppendLine("			WHERE T.DocType IN ('03','04') ");
            SQL.AppendLine(" ) H ON A.DocNo = H.DocNo ");
            if (mDepositVendorReturnSource == "2")
            {
                SQL.AppendLine("LEFT JOIN ( ");
                SQL.AppendLine("            SELECT K.DOVdDocNo DocNo, L.Amt, L.DocDt FROM tblpurchasereturninvoicedtl K ");
                SQL.AppendLine("            INNER JOIN tblpurchasereturninvoicehdr L ON K.DocNo = L.DocNo AND PaidInd = 'P' ");
                SQL.AppendLine("            INNER JOIN( ");
                SQL.AppendLine("                SELECT X1.DOVdDocNo, X1.DOVdDNo FROM tblpurchasereturninvoicedtl X1 ");
                SQL.AppendLine("                INNER JOIN tbldovddtl X2 ON X1.DOVdDocNo=X2.DocNo AND X1.DOVdDNo=X2.DNo ");
                SQL.AppendLine("            WHERE X2.CancelInd='N' ");
                SQL.AppendLine("            ) C ON K.DOVdDocNo=C.DOVdDocNo AND K.DOVdDNo=C.DOVdDNo ");
                SQL.AppendLine("            WHERE L.CancelInd='N' ");
                SQL.AppendLine("            GROUP BY K.DOVdDocNo ");
                SQL.AppendLine(") I ON A.DocNo = I.DocNo ");
            }

            SQL.AppendLine("LEFT Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine(" 	SELECT T1.DocNo, T1.APDownpaymentDocNo, SUM(T1.DownpaymentBefTax) AmtBefTax, T2.TaxName, Sum(T1.PITaxAmt) TaxAmt,   ");
            SQL.AppendLine("	T3.TaxName TaxName2, Sum(T1.PITaxAmt2) TaxAmt2, T4.TaxName TaxName3, Sum(T1.PITaxAmt3) TaxAmt3 ");
            SQL.AppendLine("	FROM tblpurchaseinvoicedtl8 T1 ");
            SQL.AppendLine("	Left JOIN TblTax T2 ON T1.TaxCode = T2.TaxCode ");
            SQL.AppendLine("	Left JOIN tbltax T3 ON T1.TaxCode2 = T3.TaxCode ");
            SQL.AppendLine("	Left JOIN tbltax T4 ON T1.TaxCode3 = T4.TaxCode  ");
            SQL.AppendLine("	GROUP BY T1.DocNo ");

            SQL.AppendLine("	UNION ALL ");
            SQL.AppendLine("	SELECT T1.DocNo, T3.DocNo APDownpaymentDocNo, T3.AmtBefTax, T4.TaxName, T3.TaxAmt, T5.TaxName TaxName2,  ");
            SQL.AppendLine("	T3.TaxAmt2, T6.TaxName TaxName3, T3.TaxAmt3 ");
            SQL.AppendLine("	FROM tblvoucherhdr T1 ");
            SQL.AppendLine("	INNER JOIN tblvoucherrequesthdr T2 ON T1.VoucherRequestDocNo = T2.DocNo ");
            SQL.AppendLine("	INNER JOIN tblapdownpayment T3 ON T2.DocNo = T3.VoucherRequestDocNo ");
            SQL.AppendLine("	LEFT JOIN tbltax T4 ON T3.TaxCode = T4.TaxCode ");
            SQL.AppendLine("	LEFT JOIN tbltax T5 ON T3.TaxCode2 = T5.TaxCode ");
            SQL.AppendLine("	LEFT JOIN tbltax T6 ON T3.TaxCode3 = T6.TaxCode ");
            SQL.AppendLine(") J ON J.DocNo = A.DocNo ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mIsFilterByEnt)
            {
                SQL.AppendLine("And (A.EntCode Is Null Or (A.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=A.EntCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            if (mIsRptVendorDepositMovementFileredByDept)
            {
                SQL.AppendLine("And (H.DeptCode Is Null Or (H.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From tblgroupdepartment ");
                SQL.AppendLine("    Where DeptCode=H.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine(") X ");
            SQL.AppendLine("WHERE X.DocDt IS NOT NULL ");

            

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "PO#",
                        "Date",
                        "",
                        "Type", 
                        
                        
                        //6-10
                        "Description",
                        "Vendor",
                        "Currency",
                        (mIsPurchaseInvoiceUseTabToInputDownpayment ? "Amount After Tax" : "Amount"),
                        "Department",

                        //11-15
                        "Entity",
                        "Bank",
                        "BG/Cheque",
                        "Due Date",
                        "DP Amount Before Tax",

                        //16-20
                        "Tax 1",
                        "Tax 1 Amount",
                        "Tax 2",
                        "Tax 2 Amount",
                        "Tax 3",

                        //21
                        "Tax 3 Amount"

                    },
                    new int[]
                    {
                        //0
                        50,
                        //1-5
                        150,150, 100, 20, 20,  
                        //6-10
                        280,200, 60, 150, 120,
                        //11-15
                        200, 230,130, 80, 200,
                        //16-20
                        150, 100, 150, 100, 150,
                        //21
                        100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 15, 17, 19, 21 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 14 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 12, 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 11 }, mIsAPDownpaymentUseEntity);
            Sm.GrdColInvisible(Grd1, new int[] { 10 }, mIsRptVendorDepositMovementUseDept);
            Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18, 19, 20, 21 }, mIsPurchaseInvoiceUseTabToInputDownpayment);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[15].Move(9);
            Grd1.Cols[16].Move(10);
            Grd1.Cols[17].Move(11);
            Grd1.Cols[18].Move(12);
            Grd1.Cols[19].Move(13);
            Grd1.Cols[20].Move(14);
            Grd1.Cols[21].Move(15);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                //comment by RDA
                //Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, mDepositVendorReturnSource == "1" ? "A.DocNo" : "X.DocNo", false);
                //Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), mDepositVendorReturnSource == "1" ? "A.VdCode" : "X.VdCode", true);
                //Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDueDt1), Sm.GetDte(DteDueDt2), mDepositVendorReturnSource == "1" ? "E.DueDt" : "X.DueDt");

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "X.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "X.VdCode", true);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDueDt1), Sm.GetDte(DteDueDt2), "X.DueDt");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By CreateDt, DocNo;",
                    new string[]
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "PODocNo", "DocDt", "DocType",  "OptDesc", "VdName",   
                        
                        //6-10
                        "CurCode", "Amt", "DeptName", "EntName", "BankName", 
                        
                        //11-15
                        "GiroNo", "DueDt", "AmtBefTax", "TaxName", "TaxAmt",

                        //16-19
                        "TaxName2", "TaxAmt2", "TaxName3", "TaxAmt3"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                    }, true, false, false, false
                );
                Grd1.GroupObject.Add(7);
                Grd1.GroupObject.Add(8);
                Grd1.Group();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                string DocType = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                switch (DocType)
                {
                    case "01":
                        var f1 = new FrmVoucher(mMenuCode);
                        f1.Tag = mMenuCode;
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f1.ShowDialog();
                    break;
                    case "02":
                        var f2 = new FrmVoucher(mMenuCode);
                        f2.Tag = mMenuCode;
                        f2.WindowState = FormWindowState.Normal;
                        f2.StartPosition = FormStartPosition.CenterScreen;
                        f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f2.ShowDialog();
                    break;
                    case "03":
                        var f3 = new FrmPurchaseInvoice(mMenuCode);
                        f3.Tag = mMenuCode;
                        f3.WindowState = FormWindowState.Normal;
                        f3.StartPosition = FormStartPosition.CenterScreen;
                        f3.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f3.ShowDialog();
                    break;
                    case "04":
                        var f4 = new FrmPurchaseInvoice(mMenuCode);
                        f4.Tag = mMenuCode;
                        f4.WindowState = FormWindowState.Normal;
                        f4.StartPosition = FormStartPosition.CenterScreen;
                        f4.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f4.ShowDialog();
                    break;
                    case "05":
                        var f5 = new FrmVendorDepositCurSwitch(mMenuCode);
                        f5.Tag = mMenuCode;
                        f5.WindowState = FormWindowState.Normal;
                        f5.StartPosition = FormStartPosition.CenterScreen;
                        f5.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f5.ShowDialog();
                    break;
                    case "06":
                        var f6 = new FrmVendorDepositCurSwitch(mMenuCode);
                        f6.Tag = mMenuCode;
                        f6.WindowState = FormWindowState.Normal;
                        f6.StartPosition = FormStartPosition.CenterScreen;
                        f6.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f6.ShowDialog();
                    break;
                    case "07":
                    var f7 = new FrmDOVd(mMenuCode);
                    f7.Tag = mMenuCode;
                    f7.WindowState = FormWindowState.Normal;
                    f7.StartPosition = FormStartPosition.CenterScreen;
                    f7.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f7.ShowDialog();
                    break;
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                string DocType = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                switch (DocType)
                {
                    case "01":
                        var f1 = new FrmVoucher(mMenuCode);
                        f1.Tag = mMenuCode;
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f1.ShowDialog();
                        break;
                    case "02":
                        var f2 = new FrmVoucher(mMenuCode);
                        f2.Tag = mMenuCode;
                        f2.WindowState = FormWindowState.Normal;
                        f2.StartPosition = FormStartPosition.CenterScreen;
                        f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f2.ShowDialog();
                        break;
                    case "03":
                        var f3 = new FrmPurchaseInvoice(mMenuCode);
                        f3.Tag = mMenuCode;
                        f3.WindowState = FormWindowState.Normal;
                        f3.StartPosition = FormStartPosition.CenterScreen;
                        f3.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f3.ShowDialog();
                        break;
                    case "04":
                        var f4 = new FrmPurchaseInvoice(mMenuCode);
                        f4.Tag = mMenuCode;
                        f4.WindowState = FormWindowState.Normal;
                        f4.StartPosition = FormStartPosition.CenterScreen;
                        f4.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f4.ShowDialog();
                        break;
                    case "05":
                        var f5 = new FrmVendorDepositCurSwitch(mMenuCode);
                        f5.Tag = mMenuCode;
                        f5.WindowState = FormWindowState.Normal;
                        f5.StartPosition = FormStartPosition.CenterScreen;
                        f5.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f5.ShowDialog();
                        break;
                    case "06":
                        var f6 = new FrmVendorDepositCurSwitch(mMenuCode);
                        f6.Tag = mMenuCode;
                        f6.WindowState = FormWindowState.Normal;
                        f6.StartPosition = FormStartPosition.CenterScreen;
                        f6.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f6.ShowDialog();
                        break;
                    case "07":
                        var f7 = new FrmDOVd(mMenuCode);
                        f7.Tag = mMenuCode;
                        f7.WindowState = FormWindowState.Normal;
                        f7.StartPosition = FormStartPosition.CenterScreen;
                        f7.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f7.ShowDialog();
                        break;
                    case "09":
                        var f9 = new FrmVoucher(mMenuCode);
                        f9.Tag = mMenuCode;
                        f9.WindowState = FormWindowState.Normal;
                        f9.StartPosition = FormStartPosition.CenterScreen;
                        f9.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f9.ShowDialog();
                        break;
                    case "10":
                        var f10 = new FrmVoucher(mMenuCode);
                        f10.Tag = mMenuCode;
                        f10.WindowState = FormWindowState.Normal;
                        f10.StartPosition = FormStartPosition.CenterScreen;
                        f10.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f10.ShowDialog();
                        break;
                }
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDueDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDueDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDueDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Due date");
        }

        #endregion

        #endregion
    }
}
