﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRating : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private int mStateInd = 0;
        internal FrmRatingFind FrmFind;

        #endregion

        #region Constructor

        public FrmRating(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length <= 0) this.Text = Sm.GetMenuDesc("FrmRating");
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtRatingCode, MeeDescription, ChkActInd
                    }, true);
                    TxtRatingCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtRatingCode, MeeDescription
                    }, false);
                    TxtRatingCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtRatingCode, true);
                    if (Sm.IsDataExist("Select RatingCode From TblRating Where RatingCode = @Param And ActInd = 'Y'; ", TxtRatingCode.Text))
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeDescription, ChkActInd }, false);
                    MeeDescription.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mStateInd = 0;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtRatingCode, MeeDescription
            });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRatingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            mStateInd = 1;
            ChkActInd.Checked = true;
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtRatingCode, "", false)) return;

            if (Sm.IsDataExist("Select RatingCode From TblRating Where RatingCode = @Param And ActInd = 'Y'; ", TxtRatingCode.Text))
            {
                mStateInd = 2;
                SetFormControl(mState.Edit);
            }
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            //if (Sm.IsTxtEmpty(TxtRatingCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            //Cursor.Current = Cursors.WaitCursor;
            //try
            //{
            //    var cm = new MySqlCommand() { CommandText = "Delete From TblRating Where RatingCode=@RatingCode" };
            //    Sm.CmParam<String>(ref cm, "@RatingCode", TxtRatingCode.Text);
            //    Sm.ExecCommand(cm);

            //    BtnCancelClick(sender, e);
            //}
            //catch (Exception Exc)
            //{
            //    Sm.ShowErrorMsg(Exc);
            //}
            //finally
            //{
            //    Cursor.Current = Cursors.Default;
            //}
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            InsertData();
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string RatingCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@RatingCode", RatingCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select RatingCode, Description, ActInd From TblRating Where RatingCode=@RatingCode",
                        new string[] 
                        {
                            "RatingCode", "Description", "ActInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtRatingCode.EditValue = Sm.DrStr(dr, c[0]);
                            MeeDescription.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtRatingCode, "Indicator code", false) ||
                Sm.IsMeeEmpty(MeeDescription, "Description") ||
                (mStateInd == 1 && IsRatingCodeExisted()) ||
                (mStateInd == 2 && IsDataAlreadyInactive());
        }

        private bool IsDataAlreadyInactive()
        {
            if (Sm.IsDataExist("Select RatingCode From TblRating Where RatingCode = @Param And ActInd = 'N'; ", TxtRatingCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already inactive.");
                TxtRatingCode.Focus();
                return true;
            }

            return false;
        }

        private bool IsRatingCodeExisted()
        {
            if (!TxtRatingCode.Properties.ReadOnly && Sm.IsDataExist("Select RatingCode From TblRating Where RatingCode='" + TxtRatingCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Indicator code ( " + TxtRatingCode.Text + " ) is exists.");
                return true;
            }
            return false;
        }

        private void InsertData()
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                if (mStateInd == 1)
                {
                    SQL.AppendLine("Insert Into TblRating(RatingCode, Description, ActInd, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@RatingCode, @Description, @ActInd, @UserCode2, CurrentDateTime()); ");
                }
                else if (mStateInd == 2)
                {
                    SQL.AppendLine("Update TblRating Set ");
                    SQL.AppendLine("    Description = @Description, ActInd = @ActInd, LastUpBy = @UserCode2, LastUpDt = CurrentDateTime() ");
                    SQL.AppendLine("Where RatingCode = @RatingCode And ActInd = 'Y'; ");
                }

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@RatingCode", TxtRatingCode.Text);
                Sm.CmParam<String>(ref cm, "@Description", MeeDescription.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", (ChkActInd.Checked) ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode2", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtRatingCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

    }
}
