﻿#region Update
/*
    28/02/2019 [TKG] New application
 *  28/04/2020 [HAR/SIER] disable journal aotomatic.karena fungsinya hanya memasukkna nilai depresiasinya, sedangkan untuk journal dilakukan manual 
 *  29/04/2021 [VIN/SIER] tambah param IsDocNoDepreciationAsset2BasedOnPurchaseDate
 *  17/05/2021 [IBL/ALL] tambah param DepreciationSalvageValue
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDepreciationAsset2 : RunSystem.FrmBase5
    {
        #region Field

        internal string mMenuCode = string.Empty;
        private bool mIsAutoJournalActived = false,
            mIsDocNoDepreciationAsset2BasedOnPurchaseDate = false;
        private string mDepreciationTypeGarisLurus = string.Empty;
        private string mDepreciationTypeSaldoMenurun = string.Empty;
        private string mDepreciationSalvageValue = string.Empty;
        private string LastYr = string.Empty;

        #endregion

        #region Constructor

        public FrmDepreciationAsset2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Asset's Code",
                        "Asset's Name", 
                        "",
                        "Date of Purchase"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 120, 300, 20, 120
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@AssetDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@AssetDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "AssetCode", "AssetName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        string.Concat(
                        "Select AssetCode, AssetName, AssetDt From TblAsset ",
                        "Where ActiveInd='Y' ",
                        "And AssetDt Is Not Null ",
                        "And AssetDt Between @AssetDt1 And @AssetDt2 ",
                        "And AssetCode Not In (Select AssetCode From TblDepreciationAssetHdr Where CancelInd='N')" ,
                        Filter, " Order By AssetDt, AssetName;"),
                        new string[]
                        { "AssetCode", "AssetName", "AssetDt" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 2);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Save Data

        override protected void SaveData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsProcessDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                int n = 0;
                if (Grd1.Rows.Count > 0)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        if (Sm.GetGrdBool(Grd1, r, 1) && Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                            SaveData(r, ref n);
                    }
                }
                if (n <= 1)
                    Sm.StdMsg(mMsgType.Info, n.ToString() + " record already processed.");
                else
                    Sm.StdMsg(mMsgType.Info, n.ToString() + " records already processed.");

                Sm.ClearGrd(Grd1, false);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsProcessDataNotValid()
        {
            return IsGrdValueNotValid();
        }

        private bool IsGrdValueNotValid()
        {
            var Dt = string.Empty;
            bool IsProcess = false;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1))
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                    {
                        IsProcess = true;
                        Dt = Sm.GetGrdDate(Grd1, r, 5);
                        if (Dt.Length > 0)
                        {
                            if (Sm.IsClosingJournalInvalid(Sm.Left(Dt, 8))) return true;
                        }
                    }
                }
            }
            if (!IsProcess)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process minimum 1 asset.");
                return true;
            }
            return false;
        }

        private void SaveData(int r, ref int n)
        {
            var o = new DepreciationAssetHdr();
            var l = new List<DepreciationAssetDtl>();

            string DocNo = Sm.GenerateDocNo(Sm.GetGrdDate(Grd1, r, 5).Substring(0, 8), "DepreciationAsset", "TblDepreciationAssetHdr");
            string AssetCode = Sm.GetGrdStr(Grd1, r, 2);

            Process1(DocNo, AssetCode, ref o);
            Process2(DocNo, AssetCode, ref o, ref l);
            Process3(ref o, ref l);
            Process4(ref o, ref l);

            n += 1;
        }

        private void Process1(string DocNo, string AssetCode, ref DepreciationAssetHdr o)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);

            SQL.AppendLine("Select AssetDt, AssetValue, EcoLife, EcoLifeYr, DepreciationCode, CCCode, PercentageAnnualDepreciation ");
            SQL.AppendLine("From TblAsset Where AssetCode=@AssetCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    "AssetDt", 
                    "AssetValue", "EcoLife", "EcoLifeYr", "DepreciationCode", "CCCode",
                    "PercentageAnnualDepreciation"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        o.DocNo = DocNo;
                        o.AssetCode = AssetCode;
                        o.DepreciationDt = Sm.DrStr(dr, c[0]);
                        o.AssetValue = Sm.DrDec(dr, c[1]);
                        o.EcoLife = Sm.DrDec(dr, c[2]);
                        o.EcoLifeYr = Sm.DrDec(dr, c[3]);
                        o.DepreciationCode = Sm.DrStr(dr, c[4]);
                        o.CCCode = Sm.DrStr(dr, c[5]);
                        o.PercentageAnnualDepreciation = Sm.DrDec(dr, c[6]);
                        o.Remark = "Processed By System.";
                    }
                }
                dr.Close();
            }
        }

        private void Process2(string DocNoTemp, string AssetCode, ref DepreciationAssetHdr o, ref List<DepreciationAssetDtl> l)
        {
            try
            {
                decimal
                    StartMth = decimal.Parse(o.DepreciationDt.Substring(4, 2)) - 1,
                    Year = decimal.Parse(o.DepreciationDt.Substring(0, 4)),
                    EndMth = 0m;

                if (mDepreciationTypeGarisLurus == "1" && o.DepreciationCode == "1")
                    EndMth = o.EcoLife;
                else if (mDepreciationTypeSaldoMenurun == "1" && o.DepreciationCode == "2")
                    EndMth = o.EcoLife;
                else
                {
                    EndMth = 12 - StartMth;
                    EndMth += ((o.EcoLifeYr - 1) * 12);
                }

                for (int x = 0; x < EndMth; x++)
                {
                    if (StartMth == 12)
                    {
                        StartMth = 1;
                        Year = Year + 1;
                    }
                    else
                        StartMth += 1;
                    
                    l.Add(new DepreciationAssetDtl()
                    {
                        DocNo = DocNoTemp,
                        MthPurchase = (x+1).ToString(),
                        Mth = Sm.Right(string.Concat("0", StartMth), 2),
                        Yr = Year.ToString(),
                        DepreciationValue = 0m,
                        CCCode = o.CCCode,
                        CostValue = 0m,
                        AnnualCostValue = 0m
                    });
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void Process3(ref DepreciationAssetHdr o, ref List<DepreciationAssetDtl> l)
        {
            decimal
                StartMth = 0m,
                CostValue = 0m,
                Prosen = 0m,
                EcoLife = 0m,
                ResidualMonth = 0m;
            string Year = string.Empty;

            StartMth = decimal.Parse(o.DepreciationDt.Substring(4, 2)) - 1;
            Year = o.DepreciationDt.Substring(0, 4);
            CostValue = o.AssetValue;
            Prosen = o.PercentageAnnualDepreciation;
            EcoLife = 12 - StartMth;
            ResidualMonth = 12 - StartMth;
            EcoLife += ((o.EcoLifeYr - 1) * 12);
            if (mDepreciationTypeGarisLurus == "1") EcoLife = o.EcoLife;
            
            if (o.DepreciationCode == "2")
            {
                if (mDepreciationTypeSaldoMenurun == "0")
                {
                    for (int i = 0; i < l.Count; i++)
                    {
                        if (i== 0)
                        {
                            l[i].CostValue = Math.Round(CostValue, 2);
                            l[i].AnnualCostValue = Math.Round((Prosen * CostValue / 100m), 2);
                            l[i].DepreciationValue = Math.Round(l[i].AnnualCostValue / ResidualMonth, 2);
                        }
                        else
                        {
                            #region Old

                            if (i == (EcoLife - 1))
                            {
                                if (mDepreciationSalvageValue == "0")
                                {
                                    l[i].CostValue = 0m;
                                    l[i].AnnualCostValue = 0m;
                                    l[i].DepreciationValue = Math.Round((l[i - 1].CostValue) - l[i - 1].DepreciationValue, 2);
                                }
                                else
                                {
                                    l[i].CostValue = 1m;
                                    l[i].AnnualCostValue = 1m;
                                    l[i].DepreciationValue = Math.Round((l[i - 1].CostValue) - l[i - 1].DepreciationValue - 1, 2);
                                }
                            }
                            else
                            {
                                if (l[i].Yr == Year)
                                {
                                    if (i == 0)
                                        l[i].CostValue = Math.Round(CostValue, 2);
                                    else
                                    {
                                        if (i == 0)
                                            l[i].CostValue = 0m;
                                        else
                                            l[i].CostValue = Math.Round(l[i - 1].CostValue - l[i-1].DepreciationValue, 2);
                                    }
                                    l[i].AnnualCostValue = Math.Round((Prosen * CostValue / 100m), 2);
                                    l[i].DepreciationValue = Math.Round(l[i].AnnualCostValue / GetYrCount(l[i].Yr, ref l), 2);
                                }
                                else
                                {
                                    Year = l[i].Yr;
                                    if (i == 0)
                                        CostValue = 0m;
                                    else
                                        CostValue = l[i-1].CostValue - l[i-1].DepreciationValue;

                                    if (i == 0)
                                        l[i].CostValue = 0m;
                                    else
                                        l[i].CostValue = Math.Round(l[i-1].CostValue - l[i].DepreciationValue, 2);

                                    l[i].AnnualCostValue = Math.Round((Prosen * CostValue / 100m), 2);
                                    l[i].DepreciationValue = Math.Round(l[i].AnnualCostValue / GetYrCount(l[i].Yr, ref l), 2);
                                }
                            }
                            #endregion
                        }
                    }
                }
                else
                {
                    int CountData = 0;
                    int lastRow = l.Count - 1;
                    if (l.Count > 1) LastYr = l[lastRow].Yr;

                    for (int i = 0; i < l.Count; i++)
                    {
                        Year = l[i].Yr;
                        CountData = GetYrCount(l[i].Yr, ref l);

                        if (i == 0)
                        {
                            l[i].DepreciationValue = Math.Round((((CountData * (Prosen / 100m) * CostValue) / 12m) / CountData), 2);
                            l[i].CostValue = Math.Round(CostValue - (((CountData * (Prosen / 100m) * CostValue) / 12m) / CountData), 2);
                            l[i].AnnualCostValue = Math.Round((((CountData * (Prosen / 100m) * CostValue) / 12m)), 2);
                        }
                        else
                        {
                            if (i == l.Count - 1)
                            {
                                if (mDepreciationSalvageValue == "0")
                                {
                                    l[i].DepreciationValue = Math.Round(l[i - 1].DepreciationValue, 2);
                                    l[i].CostValue = 0m;
                                    l[i].AnnualCostValue = Math.Round(l[i - 1].AnnualCostValue, 2);
                                }
                                else
                                {
                                    l[i].DepreciationValue = Math.Round(l[i - 1].DepreciationValue - 1, 2);
                                    l[i].CostValue = 1m;
                                    l[i].AnnualCostValue = Math.Round(l[i - 1].AnnualCostValue - 1, 2);
                                }
                            }
                            else
                            {
                                if (l[i].Yr == l[i-1].Yr)
                                {
                                    l[i].DepreciationValue = Math.Round(l[i - 1].DepreciationValue, 2);
                                    l[i].CostValue = Math.Round(l[i-1].CostValue - l[i-1].DepreciationValue, 2);
                                    l[i].AnnualCostValue = Math.Round(l[i-1].AnnualCostValue, 2);
                                }
                                else
                                {
                                    if (LastYr == l[i].Yr && i != l.Count - 1)
                                    {
                                        l[i].DepreciationValue = Math.Round(l[i-1].CostValue / CountData, 2);
                                        decimal Balance = l[i - 1].CostValue - (l[i - 1].CostValue / CountData);
                                        l[i].CostValue = Balance <= 0m ? 0m : Balance;
                                        l[i].AnnualCostValue = Math.Round(l[i - 1].CostValue, 2);
                                    }
                                    else
                                    {
                                        l[i].DepreciationValue = Math.Round((l[i - 1].CostValue * (Prosen / 100m) / CountData), 2);
                                        decimal Balance = Math.Round(l[i - 1].CostValue - (l[i - 1].CostValue * (Prosen / 100m) / CountData), 2);
                                        l[i].CostValue = Balance <= 0m ? 0m : Balance;
                                        l[i].AnnualCostValue = Math.Round(l[i - 1].CostValue * (Prosen / 100m), 2);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            else
            {
                if (mDepreciationTypeGarisLurus == "1")
                {
                    int CountData = 0;
                    int lastRow = l.Count - 1;
                    for (int i = 0; i < l.Count; i++)
                    {
                        Year = l[i].Yr;
                        CountData = GetYrCount(l[i].Yr, ref l);

                        if (i == 0)
                        {
                            l[i].CostValue = Math.Round((CostValue - (CostValue / EcoLife)), 2);
                        }
                        else if (i == lastRow)
                        {
                            if (mDepreciationSalvageValue == "0")
                                l[i].CostValue = 0m;
                            else
                                l[i].CostValue = 1m;
                        }
                        else
                        {
                            l[i].CostValue = Math.Round(l[i - 1].CostValue - l[i - 1].DepreciationValue, 2);
                        }

                        if (i == lastRow)
                        {
                            if (mDepreciationSalvageValue == "0")
                            {
                                l[i].AnnualCostValue = Math.Round(((CostValue / EcoLife) * CountData), 2);
                                l[i].DepreciationValue = Math.Round((CostValue / EcoLife), 2);
                            }
                            else
                            {
                                l[i].AnnualCostValue = Math.Round(((CostValue / EcoLife) * CountData) - 1, 2);
                                l[i].DepreciationValue = Math.Round((CostValue / EcoLife) - 1, 2);
                            }
                        }
                        else
                        {
                            l[i].AnnualCostValue = Math.Round(((CostValue / EcoLife) * CountData), 2);
                            l[i].DepreciationValue = Math.Round((CostValue / EcoLife), 2);
                        }
                    }
                }
                else
                {
                    int CountData = 0;
                    for (int i = 0; i < l.Count; i++)
                    {
                        if (l[i].Yr == Year)
                        {
                            if (i == 0)
                            {
                                CountData = GetYrCount(l[i].Yr, ref l);
                                l[i].CostValue = Math.Round(CostValue, 2);
                            }
                            else
                                l[i].CostValue = Math.Round(l[i - 1].CostValue - l[i - 1].DepreciationValue, 2);
                            l[i].AnnualCostValue = Math.Round((CostValue - ((CostValue / EcoLife) * CountData)), 2);
                            l[i].DepreciationValue = Math.Round((CostValue / EcoLife), 2);
                        }
                        else
                        {
                            CountData = CountData + GetYrCount(l[i].Yr, ref l);
                            Year = l[i].Yr;
                            if (i == 0)
                                l[i].CostValue = 0m;
                            else
                                l[i].CostValue = Math.Round(l[i - 1].CostValue - l[i - 1].DepreciationValue, 2);
                            
                            l[i].AnnualCostValue = Math.Round((CostValue - ((CostValue / EcoLife) * CountData)), 2);
                            l[i].DepreciationValue = Math.Round((CostValue / EcoLife), 2);
                        }
                    }
                }
            }
        }

        private void Process4(ref DepreciationAssetHdr o, ref List<DepreciationAssetDtl> l)
        {
            var cml = new List<MySqlCommand>();
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDepreciationAssetHdr(DocNo, DocDt, CancelInd, AssetCode, AssetValue, DepreciationDt, EcoLifeYr, EcoLife, DepreciationCode, PercentageAnnualDepreciation, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @AssetCode, @AssetValue, @DepreciationDt, @EcoLifeYr, @EcoLife, @DepreciationCode, @PercentageAnnualDepreciation, @Remark, @UserCode, CurrentDateTime());");

            for (int i = 0; i <l.Count; i++)
            {
                SQL.AppendLine("Insert Into TblDepreciationAssetDtl(DocNo, DNo, ActiveInd, MthPurchase, Mth, Yr, DepreciationValue, CCCode, CostValue, AnnualCostValue, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocNo, @DNo0" + i.ToString() + ", 'Y', @MthPurchase0" + i.ToString() + ", @Mth0" + i.ToString() + ", @Yr0" + i.ToString() + ", @DepreciationValue0" + i.ToString() + ", @CCCode0" + i.ToString() + ", @CostValue0" + i.ToString() + ", @AnnualCostValue0" + i.ToString() + ", @UserCode, CurrentDateTime());");

                Sm.CmParam<String>(ref cm, "@DNo0" + i.ToString(), Sm.Right("00" + (i + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@MthPurchase0" + i.ToString(), l[i].MthPurchase);
                Sm.CmParam<String>(ref cm, "@Mth0" + i.ToString(), l[i].Mth);
                Sm.CmParam<String>(ref cm, "@Yr0" + i.ToString(), l[i].Yr);
                Sm.CmParam<Decimal>(ref cm, "@DepreciationValue0" + i.ToString(), l[i].DepreciationValue);
                Sm.CmParam<String>(ref cm, "@CCCode0" + i.ToString(), l[i].CCCode);
                Sm.CmParam<Decimal>(ref cm, "@CostValue0" + i.ToString(), l[i].CostValue);
                Sm.CmParam<Decimal>(ref cm, "@AnnualCostValue0" + i.ToString(), l[i].AnnualCostValue);
            }

            //if (mIsAutoJournalActived)
            //{
            //    SQL.AppendLine("Update TblDepreciationAssetHdr Set ");
            //    SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            //    SQL.AppendLine("Where DocNo=@DocNo;");

            //    SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
            //    SQL.AppendLine("Select A.JournalDocNo, IfNull(A.DepreciationDt, B.AssetDt) As JournalDt, ");
            //    SQL.AppendLine("Concat('Depreciation of Business Assets : ', A.DocNo) As JnDesc, ");
            //    SQL.AppendLine("@MenuCode As MenuCode, ");
            //    SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            //    SQL.AppendLine("B.CCCode, A.CreateBy, A.CreateDt ");
            //    SQL.AppendLine("From TblDepreciationAssetHdr A ");
            //    SQL.AppendLine("Inner Join TblAsset B On A.AssetCode=B.AssetCode ");
            //    SQL.AppendLine("Where A.DocNo=@DocNo ");
            //    SQL.AppendLine("And A.AssetCode = B.AssetCode; ");

            //    SQL.AppendLine("Set @Row:=0; ");

            //    SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            //    SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            //    SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            //    SQL.AppendLine("From TblJournalHdr A ");
            //    SQL.AppendLine("Inner Join ( ");
            //    SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt, EntCode From (");
            //    SQL.AppendLine("        Select B.AcNo, 'D' As ACtype, B.AssetValue As DAmt, 0 As CAmt, D.EntCode ");
            //    SQL.AppendLine("        From TblDepreciationAssetHdr A ");
            //    SQL.AppendLine("        Inner Join TblAsset B ");
            //    SQL.AppendLine("        On A.AssetCode=B.AssetCode ");
            //    SQL.AppendLine("        And B.AcNo Is Not Null ");
            //    SQL.AppendLine("        And B.ActiveInd='Y' ");
            //    SQL.AppendLine("        Left Join TblCostCenter C On B.CCCode = C.CCCode ");
            //    SQL.AppendLine("        Left Join TblProfitCenter D On C.ProfitCenterCode = D.ProfitCenterCode ");
            //    SQL.AppendLine("        Where A.DocNo=@DocNo ");
            //    SQL.AppendLine("        Union All ");
            //    SQL.AppendLine("        Select D.AcNo, 'C' As ACtype, 0 As DAmt, B.AssetValue As CAmt, F.EntCode ");
            //    SQL.AppendLine("        From TblDepreciationAssetHdr A ");
            //    SQL.AppendLine("        Inner Join TblAsset B On A.AssetCode=B.AssetCode And B.ActiveInd='Y' ");
            //    SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode ");
            //    SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            //    SQL.AppendLine("        Left Join TblCostCenter E On B.CCCode = E.CCCode ");
            //    SQL.AppendLine("        Left Join TblProfitCenter F On E.ProfitCenterCode = F.ProfitCenterCode  ");
            //    SQL.AppendLine("        Where A.DocNo=@DocNo ");
            //    SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo, Actype ");
            //    SQL.AppendLine(") B On 1=1 ");
            //    SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            //    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(o.DepreciationDt, 1));
            //    Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            //}

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", o.DocNo);
            if(!mIsDocNoDepreciationAsset2BasedOnPurchaseDate)
                Sm.CmParamDt(ref cm, "@DocDt", Sm.ServerCurrentDate());
            else
                Sm.CmParamDt(ref cm, "@DocDt", o.DepreciationDt);

            Sm.CmParam<String>(ref cm, "@AssetCode", o.AssetCode);
            Sm.CmParam<Decimal>(ref cm, "@AssetValue", o.AssetValue);
            Sm.CmParamDt(ref cm, "@DepreciationDt", o.DepreciationDt);
            Sm.CmParam<Decimal>(ref cm, "@EcoLifeYr", o.EcoLifeYr);
            Sm.CmParam<Decimal>(ref cm, "@EcoLife", o.EcoLife);
            Sm.CmParam<String>(ref cm, "@DepreciationCode", o.DepreciationCode);
            Sm.CmParam<Decimal>(ref cm, "@PercentageAnnualDepreciation", o.PercentageAnnualDepreciation);
            Sm.CmParam<String>(ref cm, "@Remark", o.Remark);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        private int GetYrCount(string Year, ref List<DepreciationAssetDtl> l)
        {
            int c = 0;
            for (int i = 0; i < l.Count; i++)
                if (l[i].Yr == Year) c += 1;
            return c;
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mDepreciationTypeGarisLurus = Sm.GetParameter("DepreciationTypeGarisLurus");
            mDepreciationTypeSaldoMenurun = Sm.GetParameter("DepreciationTypeSaldoMenurun");
            mIsDocNoDepreciationAsset2BasedOnPurchaseDate = Sm.GetParameterBoo("IsDocNoDepreciationAsset2BasedOnPurchaseDate");
            mDepreciationSalvageValue = Sm.GetParameter("DepreciationSalvageValue");
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAsset(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmAsset(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        #endregion

        #endregion

        #region Class

        private class DepreciationAssetHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string AssetCode { get; set; }
            public decimal AssetValue { get; set; }
            public string DepreciationDt { get; set; }
            public decimal EcoLifeYr { get; set; }
            public decimal EcoLife { get; set; }
            public string DepreciationCode { get; set; }
            public string CCCode { get; set; }
            public decimal PercentageAnnualDepreciation { get; set; }
            public string Remark { get; set; }
        }

        private class DepreciationAssetDtl
        {
            public string DocNo { get; set; }
            public string MthPurchase { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public decimal DepreciationValue { get; set; }
            public string CCCode { get; set; }
            public decimal CostValue { get; set; }
            public decimal AnnualCostValue { get; set; }
        }

        #endregion
    }
}
