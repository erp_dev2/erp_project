﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpLeaveAll : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsSiteMandatory = false;

        #endregion

        #region Constructor

        public FrmRptEmpLeaveAll(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetSQL();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueLeaveCode(ref LueLeaveCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "DocType", 
                    "Document Type",
                    "Document#",
                    "",
                    "Date",
                    
                    //6-10
                    "Employee Code",
                    "",
                    "Employee Name",
                    "Site",
                    "Department",

                    //11-14
                    "Leave Name",
                    "Leave Type",
                    "Leave Date",
                    "Remark"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    0, 150, 180, 20, 80, 
                    
                    //6-10
                    120, 20, 250, 200, 200, 

                    //11-14
                    200, 150, 210, 250
                }
            );

            Sm.GrdColButton(Grd1, new int[] { 4, 7 });
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 6, 7 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 5, 6, 8, 9, 10, 11, 12, 13, 14 }, false);
            if(!mIsSiteMandatory)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 9 }, false);
            }
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocType, T1.DocTypeDesc, T1.DocNo, T1.DocDt, T1.EmpCode, T2.EmpName, T6.SiteName, T5.DeptName, T3.LeaveName, T4.OptDesc As LeaveType, ");
            SQL.AppendLine("If(T1.StartDt = T1.EndDt, T1.StartDt, Concat(T1.StartDt, ' - ', T1.EndDt)) As LeaveDt, T1.Remark ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select '1' As DocType, 'Leave Request' As DocTypeDesc, DocNo, DocDt, EmpCode, LeaveCode, LeaveType, ");
	        SQL.AppendLine("    DATE_FORMAT(StartDt, '%d/%b/%Y') As StartDt, DATE_FORMAT(EndDt, '%d/%b/%Y') As EndDt, Remark ");
            SQL.AppendLine("    From TblEmpLeaveHdr ");
            SQL.AppendLine("    Where CancelInd = 'N' ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine("    And (DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '2' As DocType, 'Group Leave Request' As DocTypeDesc, A.DocNo, A.DocDt, B.EmpCode, A.LeaveCode, A.LeaveType, ");
	        SQL.AppendLine("    DATE_FORMAT(A.StartDt, '%d/%b/%Y') As StartDt, DATE_FORMAT(A.EndDt, '%d/%b/%Y') As EndDt, A.Remark ");
            SQL.AppendLine("    From TblEmpLeave2Hdr A ");
            SQL.AppendLine("    Inner Join TblEmpLeave2Dtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Where A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.Status = 'A' ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblEmployee T2 On T1.EmpCode = T2.EmpCode ");
            SQL.AppendLine("Inner Join TblLeave T3 On T1.LeaveCode = T3.LeaveCode -- And T3.ActInd = 'Y' ");
            SQL.AppendLine("Inner Join TblOption T4 On T1.LeaveType = T4.OptCode And T4.OptCat = 'LeaveType' ");
            SQL.AppendLine("Inner Join TblDepartment T5 On T2.DeptCode = T5.DeptCode ");
            SQL.AppendLine("Left Join TblSite T6 On T2.SiteCode = T6.SiteCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            if (
                   Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                   Sm.IsDteEmpty(DteDocDt2, "End date") ||
                   Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                   ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T1.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T5.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLeaveCode), "T1.LeaveCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T1.EmpCode", "T2.EmpName", "T2.DisplayName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T1.DocNo, T1.DocDt, T2.EmpName; ",
                    new string[]
                    {
                        //0
                        "DocType", 

                        //1-5
                        "DocTypeDesc", "DocNo", "DocDt", "EmpCode", "EmpName",

                        //6-10
                        "SiteName", "DeptName", "LeaveName", "LeaveType", "LeaveDt",

                        //11
                        "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 11);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }


        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                if(Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "1")
                {
                    e.DoDefault = false;
                    var f1 = new FrmEmpLeave(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmEmpLeave' Limit 1; ");
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f1.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "2")
                {
                    e.DoDefault = false;
                    var f1 = new FrmEmpLeave2(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmEmpLeave2' Limit 1; ");
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f1.ShowDialog();
                }
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmEmployee' Limit 1; ");
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f1.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "1")
                {
                    var f1 = new FrmEmpLeave(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmEmpLeave' Limit 1; ");
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f1.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "2")
                {
                    var f1 = new FrmEmpLeave2(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmEmpLeave2' Limit 1; ");
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f1.ShowDialog();
                }
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmEmployee' Limit 1; ");
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f1.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueLeaveCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLeaveCode, new Sm.RefreshLue1(Sl.SetLueLeaveCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkLeaveCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Leave");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #endregion

    }
}
