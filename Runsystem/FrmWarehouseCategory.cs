﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWarehouseCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmWarehouseCategoryFind FrmFind;

        #endregion

        #region Constructor

        public FrmWarehouseCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWhsCtCode, TxtWhsCtName
                    }, true);
                    TxtWhsCtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWhsCtCode, TxtWhsCtName
                    }, false);
                    TxtWhsCtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtWhsCtCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWhsCtName
                    }, false);
                    TxtWhsCtName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtWhsCtCode, TxtWhsCtName
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWarehouseCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWhsCtCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWhsCtCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblWarehouseCategory Where WhsCtCode=@WhsCtCode" };
                Sm.CmParam<String>(ref cm, "@WhsCtCode", TxtWhsCtCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblWarehouseCategory(WhsCtCode, WhsCtName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@WhsCtCode, @WhsCtName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update WhsCtName=@WhsCtName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@WhsCtCode", TxtWhsCtCode.Text);
                Sm.CmParam<String>(ref cm, "@WhsCtName", TxtWhsCtName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtWhsCtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string WhsCtCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@WhsCtCode", WhsCtCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select WhsCtCode, WhsCtName From TblWarehouseCategory Where WhsCtCode=@WhsCtCode",
                        new string[]{ "WhsCtCode", "WhsCtName" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtWhsCtCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtWhsCtName.EditValue = Sm.DrStr(dr, c[1]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtWhsCtCode, "Warehouse's category code", false) ||
                Sm.IsTxtEmpty(TxtWhsCtName, "Warehouse's category name", false) ||
                IsWhsCtCodeExisted();
        }

        private bool IsWhsCtCodeExisted()
        {
            if (!TxtWhsCtCode.Properties.ReadOnly && Sm.IsDataExist("Select WhsCtCode From TblWarehouseCategory Where WhsCtCode='" + TxtWhsCtCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Warehouse's category code ( " + TxtWhsCtCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtWhsCtCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWhsCtCode);
        }

        private void TxtWhsCtName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWhsCtName);
        }

        #endregion

        #endregion
    }
}
