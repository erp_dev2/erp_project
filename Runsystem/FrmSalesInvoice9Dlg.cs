﻿#region update
/*
 * 19/04/2021 [ICA/KSM] new apps 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoice9Dlg : RunSystem.FrmBase4
    {
        #region Field

        private string mSQL = string.Empty, mCtCode = string.Empty, mMenuCode="XXX";
        private FrmSalesInvoice9 mFrmParent;

        #endregion

        #region Constructor

        public FrmSalesInvoice9Dlg(FrmSalesInvoice9 FrmParent, String CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e); 
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.DNo, C.ItCode, C.ProductName, C.ItCodeInternal, C.ItName, ");
            SQL.AppendLine("C.Qty, C.PurchaseUomCode, C.UPrice, C.UPriceAfterTax, ");
            SQL.AppendLine("B.CurCode, C.DeliveryDt ");
            SQL.AppendLine("From TblSalesContract A ");
            SQL.AppendLine("Inner Join TblSalesMemoHdr B On A.SalesMemoDocNo = B.DocNo And B.CurCode is Not Null ");
            SQL.AppendLine("INNER JOIN ( ");
            SQL.AppendLine("    SELECT A.DocNo, A.DNo, A.ItCode, A.ProductName, B.ItName, B.ItCodeInternal, ");
            SQL.AppendLine("    A.Qty, B.PurchaseUomCode, A.UPrice, A.UPriceAfterTax, A.DeliveryDt ");
            SQL.AppendLine("    FROM tblsalesmemodtl A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine(")C ON B.DocNo = C.DocNo ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.PtCode = 'CBD' ");
            SQL.AppendLine("And A.Status Not In ('M', 'F') ");
            SQL.AppendLine("And B.CtCode=@CtCode ");
            SQL.AppendLine("And Concat(A.DocNo, C.Dno) Not IN (Select Concat(DOctDocNo,DOCtDno) As keyDocNo From TblSalesInvoicehdr A Inner Join TblSalesInvoiceDtl B On A.DocNo=B.DocNo Where A.SODocNo Is Not Null And CancelInd = 'N') ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                         //1-5
                        "Document Number",
                        "DNo",
                        "Document Date",
                        "",
                        "Item Code",
                        
                        //6-10
                        "",
                        "Item Name", 
                        "Product Name", 
                        "Local Item Code",
                        "Quantity",

                        //11-15
                        "Sales" + Environment.NewLine + "UoM",
                        "Price",
                        "Total", 
                        "Unit Price" + Environment.NewLine + "(After Tax)",
                        "Total" + Environment.NewLine + "(After Tax)",

                        //16
                        "Delivery Date", 
                        "Currency", 
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        120, 0, 100, 20, 150, 
 
                        //6-10
                        20, 150, 120, 100, 100, 
                        
                        //11-15
                        100, 100, 120, 100, 120, 
                        
                        //17
                        100, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 4, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12, 13, 14, 15 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 8, 9, 16, 17 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 7, 8, 15, 16 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "C.ItCode", "D.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DNo", "DocDt", "ItCode", "ItName", "ProductName",   

                            //6-10
                            "ItCodeInternal", "Qty", "PurchaseUomCode", "UPrice", "UPriceAfterTax", 

                            //11
                            "DeliveryDt", "CurCode"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Grd.Cells[Row, 13].Value = dr.GetDecimal(c[7]) * dr.GetDecimal(c[9]);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                            Grd.Cells[Row, 15].Value = dr.GetDecimal(c[7]) * dr.GetDecimal(c[10]);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        protected override void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtSCDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.ShowSCDtl(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                mFrmParent.ComputeAmt();
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSalesContract(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {

        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {

        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion
    }
}
