﻿#region Update
/*
    25/08/2021 [DITA/KIM] New Application
    01/09/2021 [DITA/KIM] Aging days dibuat ga mandatory
    20/09/2021 [DITA/KIM] Rombak reporting
    21/09/2021 [TKG/KIM] ubah query dan proses perhitungan 
    23/09/2021 [TKG/KIM] Buq pembayaran invoice setelah bln/thn yg dipilih masih keluar
    28/09/2021 [TKG/KIM] menampilkan data voucher incoming payment before start from
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingARLedgerPaidItem2 : RunSystem.FrmBase6
    {
        #region Field

        public string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mCustomerAcNoAR = string.Empty, mAccountingRptStartFrom = string.Empty
            ;

        #endregion

        #region Constructor

        public FrmRptAgingARLedgerPaidItem2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueCtCode(ref LueCtCode);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                if (mAccountingRptStartFrom.Length > 0)
                {
                    Sl.SetLueYr(LueStartFrom, mAccountingRptStartFrom);
                    Sm.SetLue(LueStartFrom, mAccountingRptStartFrom);
                }
                else
                {
                    Sl.SetLueYr(LueStartFrom, string.Empty);
                    Sm.SetLue(LueStartFrom, CurrentDateTime.Substring(0, 4));
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            #region Old Code (remark by DITA)
            /*
            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From ( ");
            
            #region Sales Invoice
            SQL.AppendLine("Select Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, ");
            SQL.AppendLine("A.CtCode, B.CtName, C.DODocNo, C.DODocDt, C.DOAmt, A.DocNo, A.DocDt, A.Amt, ");
            SQL.AppendLine("D.ArsDocNo, D.ARSDocDt, D.ARSAmt, E.CreditAmt, E.TransactionType, E.JNDocNo, E.JNDocDt, ");
            SQL.AppendLine("ifnull(A.Amt, 0.00)-ifnull(D.ARSAmt, 0.00)- ifnull(E.CreditAmt, 0.00) Balance ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL.AppendLine("    And A.DocNo In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Distinct DocNo ");
            SQL.AppendLine("        From TblSalesInvoiceDtl ");
            SQL.AppendLine("        Where DocType = '3' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.Status = 'A' ");
            if(Sm.GetLue(LueAging) == "30")
                SQL.AppendLine("    And DateDiff(A.DocDt, Date_Format(A.DueDt, '%Y%m%d')) <= 30 ");
            if(Sm.GetLue(LueAging) == "60")
                SQL.AppendLine("    And DateDiff(A.DocDt, Date_Format(A.DueDt, '%Y%m%d')) Between 31 And 60 ");
            if(Sm.GetLue(LueAging) == "90")
                SQL.AppendLine("    And DateDiff(A.DocDt, Date_Format(A.DueDt, '%Y%m%d')) Between 61 And 90 ");
            if(Sm.GetLue(LueAging) == "180")
                SQL.AppendLine("    And DateDiff(A.DocDt, Date_Format(A.DueDt, '%Y%m%d')) > 180 ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, Group_Concat(Distinct T1.DOCtDocNo) DODocNo, Group_Concat(Distinct Date_Format(T4.DocDt, '%d/%m/%Y')) DODocDt, ");
            SQL.AppendLine("	 Sum(T2.Qty * T2.UPrice) DOAmt ");
            SQL.AppendLine("    From TblSalesInvoiceDtl T1 ");
            SQL.AppendLine("    Inner Join TblDOCtDtl T2 On T1.DOCtDocNo = T2.DocNo ");
            SQL.AppendLine("       And T1.DOCtDNo = T2.DNo ");
            SQL.AppendLine("        And T1.DocType = '3' ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceHdr T3 On T1.DocNo = T3.DocNo ");
            SQL.AppendLine("        And T3.CancelInd = 'N' ");
            SQL.AppendLine("        And T3.Status = 'A' ");
            SQL.AppendLine("    inner Join TblDOCtHdr T4 On T2.DocNo = T4.DocNo ");
            SQL.AppendLine("    Group BY T1.DocNo ");
            SQL.AppendLine(") C On A.DocNo = C.DocNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo ARSDocNo, DocDt ARSDocDt, SalesInvoiceDocNo, Sum(Amt) ARSAmt ");
            SQL.AppendLine("    From TblARSHdr ");
            SQL.AppendLine("    Where CancelInd = 'N' ");
            SQL.AppendLine("    Group By SalesInvoiceDocNo ");
            SQL.AppendLine(") D On A.DocNo = D.SalesInvoiceDocNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("	Select T1.DocNo , SUM(T6.CAmt) CreditAmt, 'Voucher' TransactionType, Group_Concat(Distinct T5.DocNo) JNDocNo, "); 
            SQL.AppendLine("	Group_Concat(Distinct Date_Format(T5.DocDt, '%d/%m/%Y')) JNDocDt ");
            SQL.AppendLine("	From TblSalesInvoiceHdr T1 ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.InvoiceDocNo  ");
            SQL.AppendLine("		And T2.InvoiceType = '1' ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentHdr T3 On T2.DocNo = T3.DocNo ");
            SQL.AppendLine("		And T3.CancelInd = 'N' ");
            SQL.AppendLine("		And IfNull(T3.Status, 'O')<>'C' ");
            SQL.AppendLine("	Inner Join TblVoucherHdr T4 On T3.VoucherRequestDocNo = T4.VoucherRequestDocNo ");
            SQL.AppendLine("		And T4.CancelInd = 'N' ");
            SQL.AppendLine("	Inner Join TblJournalHdr T5 On T4.JournalDocNo = T5.DocNo ");
            SQL.AppendLine("	Inner Join TblJournalDtl T6 On T5.DocNo = T6.DocNo And T6.AcNo Like Concat(@CustomerAcNoAR, '%') ");
            SQL.AppendLine("	Group By T1.DocNo ");
            SQL.AppendLine(") E On A.DocNo = E.DocNo ");
            #endregion 

            if (Sm.GetLue(LueAging) == "180")
            {
                SQL.AppendLine("UNION ALL ");

                #region Journal
                SQL.AppendLine("Select Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, ");
                SQL.AppendLine("C.CtCode, C.CtName, NULL As DODocNo, NULL As DODocDt, 0.00 As DOAmt, NULL As DocNo, NULL As DocDt, 0.00 As Amt, ");
                SQL.AppendLine("NULL As ArsDocNo, NULL As ARSDocDt, 0.00 As ARSAmt, 0.00 As CreditAmt, 'Journal Transaction' TransactionType, A.DocNo JNDocNo, ");
                SQL.AppendLine("A.DocDt JNDocDt, Sum(Case D.AcType When 'D' Then (B.DAmt - B.CAmt) Else (B.CAmt - B.DAmt) End) Balance ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("	And B.AcNo Like Concat(@CustomerAcNoAR, '%') ");
                SQL.AppendLine("Inner Join TblCustomer C On C.CtCode = Right(B.AcNo, Length(C.CtCode))  ");
                SQL.AppendLine("Inner Join TblCOA D ON B.AcNo = D.AcNo And D.ActInd = 'Y' ");
                SQL.AppendLine("    And D.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("Group By A.DocNo, A.DocDt, C.CtCode, C.CtName ");
                #endregion

                SQL.AppendLine("UNION ALL ");

                #region Opening Balance
                SQL.AppendLine("Select Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, ");
                SQL.AppendLine("C.CtCode, C.CtName, NULL As DODocNo, NULL As DODocDt, 0.00 As DOAmt, NULL As DocNo, NULL As DocDt, 0.00 As Amt, ");
                SQL.AppendLine("NULL As ArsDocNo, NULL As ARSDocDt, 0.00 As ARSAmt, 0.00 As CreditAmt, 'Opening Balance' TransactionType, A.DocNo JNDocNo, ");
                SQL.AppendLine("A.DocDt JNDocDt, SUM(B.Amt) Balance ");
                SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
                SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("	And  B.AcNo Like Concat(@CustomerAcNoAR, '%') ");
                SQL.AppendLine("Inner Join TblCustomer C On C.CtCode = Right(B.AcNo, Length(C.CtCode))  ");
                SQL.AppendLine("Inner Join TblCOA D On B.AcNo = D.AcNo And D.ActInd = 'Y' ");
                SQL.AppendLine("   And D.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("Group By A.DocDt, A.DocNo, C.CtCode, C.CtName ");
                #endregion
            }

            SQL.AppendLine(") T ");
            */
            #endregion

            SQL.AppendLine("Select Concat(Left(X1.Period, 4), '-', Right(X1.Period, 2)) As Period, ");
            SQL.AppendLine("X1.SalesInvoiceDocNo, X1.DocDt,  ");
            SQL.AppendLine("X2.CtName, X2.CtCode, X1.SalesInvoiceAmt, X1.TransType,  ");
            SQL.AppendLine("Case X1.TransType ");
            SQL.AppendLine("	When '1' Then 'Opening Balance' ");
            SQL.AppendLine("	When '2' Then Case SubType When '1' Then 'Voucher (Incoming Payment)' When '2' Then 'AR Settlement' End ");
            SQL.AppendLine("	When '3' Then Concat('Voucher (Incoming Payment) Before ', @StartFrom) ");
            SQL.AppendLine("	When '4' Then  ");
            SQL.AppendLine("		Case SubType  ");
            SQL.AppendLine("			When '1' Then 'Deposit (Incoming Payment)'  ");
            SQL.AppendLine("			When '2' Then 'Journal Transaction'  ");
            SQL.AppendLine("			When '3' Then 'Journal Voucher'  ");
            SQL.AppendLine("		End ");
            SQL.AppendLine("End As TransTypeDesc,  ");
            SQL.AppendLine("X1.DocNo, X1.DocDt2, X1.Amt  ");
            SQL.AppendLine("From ( ");

            // Type 1 : coa opening balance
            SQL.AppendLine("Select Concat(@StartFrom, '0101') As Period, '1' As TransType, '0' As SubType, Null As SalesInvoiceDocNo, Null As DocDt, "); 
            SQL.AppendLine("Right(B.AcNo, Length(B.AcNo)-@AcNoLength) As CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("A.DocNo, A.DocDt As DocDt2,  ");
            SQL.AppendLine("Case When C.AcType='D' Then 1.00 Else -1.00 End * B.Amt As Amt  ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo=B.DocNo And Left(B.AcNo, @AcNoLength)=@AcNo And B.Amt<>0.00  ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And Right(B.AcNo, Length(B.AcNo)-@AcNoLength)=@CtCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo ");
            SQL.AppendLine("Where A.Yr=@StartFrom  ");
            SQL.AppendLine("And A.CancelInd='N' ");

            SQL.AppendLine("Union All ");

            // Type 2 : sales invoice (voucher (incoming payment) + ar settlement
            SQL.AppendLine("Select Left(A.DocDt, 6) As Period, '2' As TransType, D.SubType, ");
            SQL.AppendLine("B.DocNo As SalesInvoiceDocNo, A.DocDt, B.CtCode, C.DAmt As SalesInvoiceAmt,  ");
            SQL.AppendLine("D.DocNo, D.DocDt2, IfNull(D.Amt, 0.00) As Amt  ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr B On A.DocNo=B.JournalDocNo And B.JournalDocNo is Not Null And B.CancelInd='N'  ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And B.CtCode=@CtCode ");
            SQL.AppendLine("Inner Join TblJournalDtl C On A.DocNo=C.DocNo And Left(C.AcNo, @AcNoLength)=@AcNo And C.DAmt>0.00 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select '1' As SubType, T2.DocNo As SalesInvoiceDocNo, T6.DocNo, T6.DocDt As DocDt2, T4.Amt As Amt  ");
            SQL.AppendLine("	From TblJournalHdr T1 ");
            SQL.AppendLine("	Inner Join TblSalesInvoiceHdr T2 On T1.DocNo=T2.JournalDocNo And T2.JournalDocNo is Not Null ");
            if (Filter!=" ") SQL.AppendLine(Filter.Replace("X.", "T2."));
            if (ChkCtCode.Checked) SQL.AppendLine(" And T2.CtCode=@CtCode ");
            SQL.AppendLine("	Inner Join TblJournalDtl T3 On T1.DocNo=T3.DocNo And Left(T3.AcNo, @AcNoLength)=@AcNo And T3.DAmt>0.00 ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentDtl T4 On T2.DocNo=T4.InvoiceDocNo ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentHdr T5 On T4.DocNo=T5.DocNo And T5.CancelInd='N' And T5.Status='A' ");
            SQL.AppendLine("	Inner Join TblVoucherHdr T6 On T5.VoucherRequestDocNo=T6.VoucherRequestDocNo And T6.CancelInd='N' ");
            SQL.AppendLine("	Inner Join TblJournalHdr T7 On T6.JournalDocNo=T7.DocNo And T7.DocDt<=@Dt2  ");
            SQL.AppendLine("	Where T1.DocDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("	Union All ");
            SQL.AppendLine("	Select '2' As SubType, T2.DocNo As SalesInvoiceDocNo, T3.DocNo, T3.DocDt As DocDt2, (-1.00*T4.DAmt)+T4.CAmt As Amt "); 
            SQL.AppendLine("	From TblJournalHdr T1 ");
            SQL.AppendLine("	Inner Join TblSalesInvoiceHdr T2 On T1.DocNo=T2.JournalDocNo And T2.JournalDocNo is Not Null  ");
            if (Filter != " ") SQL.AppendLine(Filter.Replace("X.", "T2."));
            if (ChkCtCode.Checked) SQL.AppendLine(" And T2.CtCode=@CtCode ");
            SQL.AppendLine("	Inner Join TblARSHdr T3 On T2.DocNo=T3.SalesInvoiceDocNo And T3.CancelInd='N' ");
            SQL.AppendLine("	Inner Join TblJournalDtl T4 On T3.JournalDocNo=T4.DocNo And Left(T4.AcNo, @AcNoLength)=@AcNo  ");
            SQL.AppendLine("	Inner Join TblJournalHdr T5 On T4.DocNo=T5.DocNo And T5.DocDt<=@Dt2  ");
            SQL.AppendLine("	Where T1.DocDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine(") D On B.DocNo=D.SalesInvoiceDocNo ");
            SQL.AppendLine("Where A.DocDt Between @Dt1 And @Dt2 ");

            SQL.AppendLine("Union All ");

            // Type 3 : sales invoice before start date (voucher (incoming payment))
            SQL.AppendLine("Select Left(A.DocDt, 6) As Period, '3' As TransType, '1' As SubType, ");
            SQL.AppendLine("Null As SalesInvoiceDocNo, Null As DocDt, A.CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("D.DocNo, D.DocDt As DocDt2, IfNull(B.Amt, 0.00) As Amt  ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl B On A.DocNo=B.InvoiceDocNo ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr C On B.DocNo=C.DocNo And C.CancelInd='N' And C.Status='A' ");
            SQL.AppendLine("Inner Join TblVoucherHdr D On C.VoucherRequestDocNo=D.VoucherRequestDocNo And D.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblJournalHdr E On D.JournalDocNo=E.DocNo And E.DocDt<=@Dt2  ");
            SQL.AppendLine("Where A.CancelInd='N'  ");
            SQL.AppendLine("And A.DocDt<@Dt1 ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And A.CtCode=@CtCode ");
            
            SQL.AppendLine("Union All ");

            //Type 4 : journal + journal voucher
            SQL.AppendLine("Select Left(A.DocDt, 6) As Period, '4' As TransType,  ");
            SQL.AppendLine("Case A.MenuCode  ");
            SQL.AppendLine("	When '0102023003' Then '1' ");
            SQL.AppendLine("	When '0102010106' Then '2' ");
            SQL.AppendLine("	When '0102010107' Then '3' ");
            SQL.AppendLine("End As SubType,  ");
            SQL.AppendLine("Null As SalesInvoiceDocNo, Null As DocDt,  ");
            SQL.AppendLine("Right(B.AcNo, Length(B.AcNo)-@AcNoLength) As CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("A.DocNo, A.DocDt As DocDt2,  ");
            SQL.AppendLine("Case When B.DAmt<>0.00 Then  ");
            SQL.AppendLine("	Case When C.AcType='D' Then 1.00 Else -1.00 End * B.DAmt ");
            SQL.AppendLine("Else  ");
            SQL.AppendLine("	Case When C.AcType='D' Then -1.00 Else 1.00 End * B.CAmt ");
            SQL.AppendLine("End As Amt  ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo=B.DocNo And Left(B.AcNo, @AcNoLength)=@AcNo And (B.DAmt<>0.00 Or B.CAmt<>0.00) ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And Right(B.AcNo, Length(B.AcNo)-@AcNoLength)=@CtCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo ");
            SQL.AppendLine("Where A.DocDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("And A.MenuCode In ('0102023003', '0102010106', '0102010107') ");
            SQL.AppendLine(") X1, TblCustomer X2 ");
            SQL.AppendLine("Where X1.CtCode=X2.CtCode ");
            SQL.AppendLine("Order By X2.CtName, X1.CtCode, X1.TransType, X1.DocDt, X1.SalesInvoiceDocNo, X1.SubType;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Period",
                        "Customer", 
                        "Invoice#", 
                        "Invoice Date", 
                        "Invoice Amount",

                        //6-10
                        "Type Code",
                        "Type",  
                        "Document#", 
                        "Document Date",
                        "Document Amount",
                       

                        //11-13
                        "Balance",
                        "AR Balance",
                        "Customer's Code"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 200, 150, 120, 150, 
                        
                        //6-10
                        0, 200, 150, 100, 150,   

                        //11-13
                        150, 150, 130
                    }
                );

            Sm.GrdFormatDec(Grd1, new int[] {5, 10, 11, 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 13 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Grd1.Cols[13].Move(2);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
           if (Sm.IsLueEmpty(LueYr, "Year") || 
               Sm.IsLueEmpty(LueMth, "Month") ||
               Sm.IsLueEmpty(LueStartFrom, "Start From")
               )return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();
                
                Sm.FilterStr(ref Filter, ref cm, TxtSLIDocNo.Text, "X.DocNo", false);
                if (ChkCtCode.Checked) Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@StartFrom", Sm.GetLue(LueStartFrom));
                Sm.CmParam<String>(ref cm, "@AcNo", mCustomerAcNoAR);
                Sm.CmParam<String>(ref cm, "@AcNoLength", mCustomerAcNoAR.Length.ToString());
                Sm.CmParam<String>(ref cm, "@Dt1", string.Concat(Sm.GetLue(LueStartFrom), "0101"));
                Sm.CmParam<String>(ref cm, "@Dt2", Sm.GetValue("Select DATE_FORMAT(Date_Add(Date_Add((Select Str_To_Date(Concat('" + Sm.GetLue(LueYr) + "','" + Sm.GetLue(LueMth) + "', '01'), '%Y%m%d')), Interval 1 Month), Interval -1 Day), '%Y%m%d');"));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[]
                        { 
                            //0
                            "Period", 

                            //1-5
                            "CtName", "SalesInvoiceDocNo", "DocDt", "SalesInvoiceAmt", "TransType",

                            //6-10
                             "TransTypeDesc", "DocNo", "DocDt2", "Amt", "CtCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdNumValueZero(ref Grd1, Row, new int[] { 11, 12 });
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        }, true, false, false, false
                    );
                ProcessRecomputeAmt();
                ProcessBalance();
                ProcessARBalance();
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[]{ 5, 10 });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mCustomerAcNoAR = Sm.GetParameter("CustomerAcNoAR");
            mAccountingRptStartFrom = Sm.GetParameter("AccountingRptStartFrom");
        }

        private void ProcessRecomputeAmt()
        {
            var SalesInvoiceDocNo = string.Empty;
            var Amt = 0m;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 6) == "2" && Sm.CompareStr(SalesInvoiceDocNo, Sm.GetGrdStr(Grd1, i, 3)))
                    Grd1.Cells[i, 5].Value = Sm.GetGrdDec(Grd1, i, 5) - Amt;
                SalesInvoiceDocNo = Sm.GetGrdStr(Grd1, i, 3);
                Amt = Sm.GetGrdDec(Grd1, i, 10);
            }
        }

        private void ProcessBalance()
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 6) == "1") //Opening Balance
                    Grd1.Cells[i, 11].Value = Sm.GetGrdDec(Grd1, i, 10);
                else
                {
                    if (Sm.GetGrdStr(Grd1, i, 6) == "3")
                        Grd1.Cells[i, 11].Value = 0m;
                    else
                        Grd1.Cells[i, 11].Value = Sm.GetGrdDec(Grd1, i, 5) - Sm.GetGrdDec(Grd1, i, 10);
                }
            }
        }

        private void ProcessARBalance()
        {
            string CtCode = string.Empty, TransType = string.Empty, SalesInvoiceDocNo = string.Empty;
            var ARBalance = 0m;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                TransType = Sm.GetGrdStr(Grd1, i, 6);
                if (Sm.CompareStr(CtCode, Sm.GetGrdStr(Grd1, i, 13)))
                {
                    if (TransType == "1")
                        ARBalance += Sm.GetGrdDec(Grd1, i, 10);
                    if (TransType == "2")
                    {
                        if (Sm.CompareStr(SalesInvoiceDocNo, Sm.GetGrdStr(Grd1, i, 3)))
                            ARBalance -= Sm.GetGrdDec(Grd1, i, 10);
                        else
                            ARBalance += Sm.GetGrdDec(Grd1, i, 11);
                    }
                    if (TransType == "3")
                        ARBalance -= Sm.GetGrdDec(Grd1, i, 10);
                    if (TransType == "4")
                        ARBalance += Sm.GetGrdDec(Grd1, i, 10);
                }
                else
                {
                    ARBalance = 0m;
                    if (TransType == "1")
                        ARBalance += Sm.GetGrdDec(Grd1, i, 10);
                    if (TransType == "2")
                        ARBalance += Sm.GetGrdDec(Grd1, i, 11);
                    if (TransType == "3")
                        ARBalance -= Sm.GetGrdDec(Grd1, i, 10);
                    if (TransType == "4")
                        ARBalance += Sm.GetGrdDec(Grd1, i, 10);
                }
                Grd1.Cells[i, 12].Value = ARBalance;
                CtCode = Sm.GetGrdStr(Grd1, i, 13);
                SalesInvoiceDocNo = Sm.GetGrdStr(Grd1, i, 3);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtSLIDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSLIDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Sales Invoice#");
        }

        #endregion

        #endregion
    }
}
