﻿namespace RunSystem
{
    partial class FrmRawMaterialVerifyOld
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRawMaterialVerifyOld));
            this.BtnRecvRawMaterialDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtRecvRawMaterialDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtVdCode = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtQueueNo = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnLegalDocVerify = new DevExpress.XtraEditors.SimpleButton();
            this.TxtLegalDocVerifyDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnRawMaterialOpnameDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtRawMaterialOpnameDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnRecvRawMaterialDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecvRawMaterialDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQueueNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLegalDocVerifyDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRawMaterialOpnameDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnRecvRawMaterialDocNo2);
            this.panel2.Controls.Add(this.BtnRawMaterialOpnameDocNo);
            this.panel2.Controls.Add(this.TxtRawMaterialOpnameDocNo);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.BtnLegalDocVerify);
            this.panel2.Controls.Add(this.TxtLegalDocVerifyDocNo);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtQueueNo);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtVdCode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.BtnRecvRawMaterialDocNo);
            this.panel2.Controls.Add(this.TxtRecvRawMaterialDocNo);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            // 
            // BtnRecvRawMaterialDocNo
            // 
            this.BtnRecvRawMaterialDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRecvRawMaterialDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRecvRawMaterialDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRecvRawMaterialDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRecvRawMaterialDocNo.Appearance.Options.UseBackColor = true;
            this.BtnRecvRawMaterialDocNo.Appearance.Options.UseFont = true;
            this.BtnRecvRawMaterialDocNo.Appearance.Options.UseForeColor = true;
            this.BtnRecvRawMaterialDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnRecvRawMaterialDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRecvRawMaterialDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRecvRawMaterialDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnRecvRawMaterialDocNo.Image")));
            this.BtnRecvRawMaterialDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRecvRawMaterialDocNo.Location = new System.Drawing.Point(422, 67);
            this.BtnRecvRawMaterialDocNo.Name = "BtnRecvRawMaterialDocNo";
            this.BtnRecvRawMaterialDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnRecvRawMaterialDocNo.TabIndex = 16;
            this.BtnRecvRawMaterialDocNo.ToolTip = "Find Receiving Raw Material Document";
            this.BtnRecvRawMaterialDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRecvRawMaterialDocNo.ToolTipTitle = "Run System";
            this.BtnRecvRawMaterialDocNo.Click += new System.EventHandler(this.BtnRecvRawMaterialDocNo_Click);
            // 
            // TxtRecvRawMaterialDocNo
            // 
            this.TxtRecvRawMaterialDocNo.EnterMoveNextControl = true;
            this.TxtRecvRawMaterialDocNo.Location = new System.Drawing.Point(158, 67);
            this.TxtRecvRawMaterialDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRecvRawMaterialDocNo.Name = "TxtRecvRawMaterialDocNo";
            this.TxtRecvRawMaterialDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRecvRawMaterialDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRecvRawMaterialDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRecvRawMaterialDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtRecvRawMaterialDocNo.Properties.MaxLength = 16;
            this.TxtRecvRawMaterialDocNo.Size = new System.Drawing.Size(261, 20);
            this.TxtRecvRawMaterialDocNo.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(26, 71);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 14);
            this.label6.TabIndex = 14;
            this.label6.Text = "Dokumen Penerimaan";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(158, 19);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Size = new System.Drawing.Size(261, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(53, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Nomor Dokumen";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(158, 43);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(163, 20);
            this.DteDocDt.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(46, 46);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 14);
            this.label4.TabIndex = 12;
            this.label4.Text = "Tanggal Dokumen";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(424, 19);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Batal";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.ShowToolTips = false;
            this.ChkCancelInd.Size = new System.Drawing.Size(67, 22);
            this.ChkCancelInd.TabIndex = 11;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(160, 189);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(498, 20);
            this.MeeRemark.TabIndex = 29;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(83, 192);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 14);
            this.label8.TabIndex = 28;
            this.label8.Text = "Keterangan";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdCode
            // 
            this.TxtVdCode.EnterMoveNextControl = true;
            this.TxtVdCode.Location = new System.Drawing.Point(159, 117);
            this.TxtVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdCode.Name = "TxtVdCode";
            this.TxtVdCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtVdCode.Properties.MaxLength = 16;
            this.TxtVdCode.Size = new System.Drawing.Size(261, 20);
            this.TxtVdCode.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(106, 120);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 14);
            this.label2.TabIndex = 21;
            this.label2.Text = "Vendor";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQueueNo
            // 
            this.TxtQueueNo.EnterMoveNextControl = true;
            this.TxtQueueNo.Location = new System.Drawing.Point(159, 141);
            this.TxtQueueNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQueueNo.Name = "TxtQueueNo";
            this.TxtQueueNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtQueueNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQueueNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQueueNo.Properties.Appearance.Options.UseFont = true;
            this.TxtQueueNo.Properties.MaxLength = 16;
            this.TxtQueueNo.Size = new System.Drawing.Size(261, 20);
            this.TxtQueueNo.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(67, 144);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 14);
            this.label3.TabIndex = 23;
            this.label3.Text = "Nomor Antrian";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnLegalDocVerify
            // 
            this.BtnLegalDocVerify.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnLegalDocVerify.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnLegalDocVerify.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLegalDocVerify.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnLegalDocVerify.Appearance.Options.UseBackColor = true;
            this.BtnLegalDocVerify.Appearance.Options.UseFont = true;
            this.BtnLegalDocVerify.Appearance.Options.UseForeColor = true;
            this.BtnLegalDocVerify.Appearance.Options.UseTextOptions = true;
            this.BtnLegalDocVerify.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnLegalDocVerify.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnLegalDocVerify.Image = ((System.Drawing.Image)(resources.GetObject("BtnLegalDocVerify.Image")));
            this.BtnLegalDocVerify.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnLegalDocVerify.Location = new System.Drawing.Point(423, 163);
            this.BtnLegalDocVerify.Name = "BtnLegalDocVerify";
            this.BtnLegalDocVerify.Size = new System.Drawing.Size(24, 21);
            this.BtnLegalDocVerify.TabIndex = 27;
            this.BtnLegalDocVerify.ToolTip = "Show Dokumen Legalitas";
            this.BtnLegalDocVerify.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnLegalDocVerify.ToolTipTitle = "Run System";
            this.BtnLegalDocVerify.Click += new System.EventHandler(this.BtnLegalDocVerify_Click);
            // 
            // TxtLegalDocVerifyDocNo
            // 
            this.TxtLegalDocVerifyDocNo.EnterMoveNextControl = true;
            this.TxtLegalDocVerifyDocNo.Location = new System.Drawing.Point(159, 165);
            this.TxtLegalDocVerifyDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLegalDocVerifyDocNo.Name = "TxtLegalDocVerifyDocNo";
            this.TxtLegalDocVerifyDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLegalDocVerifyDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLegalDocVerifyDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLegalDocVerifyDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLegalDocVerifyDocNo.Properties.MaxLength = 16;
            this.TxtLegalDocVerifyDocNo.Size = new System.Drawing.Size(261, 20);
            this.TxtLegalDocVerifyDocNo.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(44, 169);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 14);
            this.label5.TabIndex = 25;
            this.label5.Text = "Dokumen Legalitas";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnRawMaterialOpnameDocNo
            // 
            this.BtnRawMaterialOpnameDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRawMaterialOpnameDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRawMaterialOpnameDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRawMaterialOpnameDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRawMaterialOpnameDocNo.Appearance.Options.UseBackColor = true;
            this.BtnRawMaterialOpnameDocNo.Appearance.Options.UseFont = true;
            this.BtnRawMaterialOpnameDocNo.Appearance.Options.UseForeColor = true;
            this.BtnRawMaterialOpnameDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnRawMaterialOpnameDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRawMaterialOpnameDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRawMaterialOpnameDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnRawMaterialOpnameDocNo.Image")));
            this.BtnRawMaterialOpnameDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRawMaterialOpnameDocNo.Location = new System.Drawing.Point(422, 91);
            this.BtnRawMaterialOpnameDocNo.Name = "BtnRawMaterialOpnameDocNo";
            this.BtnRawMaterialOpnameDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnRawMaterialOpnameDocNo.TabIndex = 20;
            this.BtnRawMaterialOpnameDocNo.ToolTip = "Show Dokumen Opname";
            this.BtnRawMaterialOpnameDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRawMaterialOpnameDocNo.ToolTipTitle = "Run System";
            this.BtnRawMaterialOpnameDocNo.Click += new System.EventHandler(this.BtnRawMaterialOpnameDocNo_Click);
            // 
            // TxtRawMaterialOpnameDocNo
            // 
            this.TxtRawMaterialOpnameDocNo.EnterMoveNextControl = true;
            this.TxtRawMaterialOpnameDocNo.Location = new System.Drawing.Point(158, 92);
            this.TxtRawMaterialOpnameDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRawMaterialOpnameDocNo.Name = "TxtRawMaterialOpnameDocNo";
            this.TxtRawMaterialOpnameDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRawMaterialOpnameDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRawMaterialOpnameDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRawMaterialOpnameDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtRawMaterialOpnameDocNo.Properties.MaxLength = 16;
            this.TxtRawMaterialOpnameDocNo.Size = new System.Drawing.Size(261, 20);
            this.TxtRawMaterialOpnameDocNo.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(43, 96);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 14);
            this.label7.TabIndex = 18;
            this.label7.Text = "Dokumen Opname";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnRecvRawMaterialDocNo2
            // 
            this.BtnRecvRawMaterialDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRecvRawMaterialDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRecvRawMaterialDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRecvRawMaterialDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRecvRawMaterialDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnRecvRawMaterialDocNo2.Appearance.Options.UseFont = true;
            this.BtnRecvRawMaterialDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnRecvRawMaterialDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnRecvRawMaterialDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRecvRawMaterialDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRecvRawMaterialDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnRecvRawMaterialDocNo2.Image")));
            this.BtnRecvRawMaterialDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRecvRawMaterialDocNo2.Location = new System.Drawing.Point(448, 67);
            this.BtnRecvRawMaterialDocNo2.Name = "BtnRecvRawMaterialDocNo2";
            this.BtnRecvRawMaterialDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnRecvRawMaterialDocNo2.TabIndex = 17;
            this.BtnRecvRawMaterialDocNo2.ToolTip = "Show Dokumen Penerimaan Bahan Baku";
            this.BtnRecvRawMaterialDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRecvRawMaterialDocNo2.ToolTipTitle = "Run System";
            this.BtnRecvRawMaterialDocNo2.Click += new System.EventHandler(this.BtnRecvRawMaterialDocNo2_Click);
            // 
            // FrmRawMaterialVerify
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmRawMaterialVerify";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecvRawMaterialDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQueueNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLegalDocVerifyDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRawMaterialOpnameDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton BtnRecvRawMaterialDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtRecvRawMaterialDocNo;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtVdCode;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtQueueNo;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.SimpleButton BtnLegalDocVerify;
        internal DevExpress.XtraEditors.TextEdit TxtLegalDocVerifyDocNo;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.SimpleButton BtnRawMaterialOpnameDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtRawMaterialOpnameDocNo;
        private System.Windows.Forms.Label label7;
        public DevExpress.XtraEditors.SimpleButton BtnRecvRawMaterialDocNo2;
    }
}