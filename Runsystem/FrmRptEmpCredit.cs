﻿#region update
/*
    03/11/2020 [IBL/PHT] : New Application
 *  24/11/2020 [ICA/PHT] Menambah filter Employee
 *  27/11/2020 [ICA/PHT] Menambah kolom Installment Month dan Installment Count
 *  05/05/2021 [VIN/PHT] Bug: filter belum berfungsi
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpCredit : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptEmpCredit(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueSiteCode(ref LueSiteCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.* From( ");

            SQL.AppendLine("Select A.EmpCode, A.PayrunCode, B.EmpCodeOld, B.EmpName, D.SiteName, E.DivisionName, F.DeptName, C.CreditName, A.Amt, H.Mth as InstallmentMonth, IfNull(H.Counter, 0) as InstallmentCount, ");
            SQL.AppendLine("D.SiteCode, G.PayrunName, A.CreateDt ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select EmpCode, PayrunCode, CreditCode1 As CreditCode, CreditAdvancePayment1 As Amt, CreateDt ");
	        SQL.AppendLine("    From TblPayrollProcess1 ");
	        SQL.AppendLine("    Where CreditCode1 Is Not Null ");
	        SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select EmpCode, PayrunCode, CreditCode2 As CreditCode, CreditAdvancePayment2 As Amt, CreateDt ");
	        SQL.AppendLine("    From TblPayrollProcess1 ");
	        SQL.AppendLine("    Where CreditCode2 Is Not Null ");
	        SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select EmpCode, PayrunCode, CreditCode3 As CreditCode, CreditAdvancePayment3 As Amt, CreateDt ");
	        SQL.AppendLine("    From TblPayrollProcess1 ");
	        SQL.AppendLine("    Where CreditCode3 Is Not Null ");
	        SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select EmpCode, PayrunCode, CreditCode4 As CreditCode, CreditAdvancePayment4 As Amt, CreateDt ");
	        SQL.AppendLine("    From TblPayrollProcess1 ");
	        SQL.AppendLine("    Where CreditCode4 Is Not Null ");
	        SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select EmpCode, PayrunCode, CreditCode5 As CreditCode, CreditAdvancePayment5 As Amt, CreateDt ");
	        SQL.AppendLine("    From TblPayrollProcess1 ");
	        SQL.AppendLine("    Where CreditCode5 Is Not Null ");
	        SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select EmpCode, PayrunCode, CreditCode6 As CreditCode, CreditAdvancePayment6 As Amt, CreateDt ");
	        SQL.AppendLine("    From TblPayrollProcess1 ");
	        SQL.AppendLine("    Where CreditCode6 Is Not Null ");
	        SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select EmpCode, PayrunCode, CreditCode7 As CreditCode, CreditAdvancePayment7 As Amt, CreateDt ");
	        SQL.AppendLine("    From TblPayrollProcess1 ");
	        SQL.AppendLine("    Where CreditCode7 Is Not Null ");
	        SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select EmpCode, PayrunCode, CreditCode8 As CreditCode, CreditAdvancePayment8 As Amt, CreateDt ");
	        SQL.AppendLine("    From TblPayrollProcess1 ");
	        SQL.AppendLine("    Where CreditCode8 Is Not Null ");
	        SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select EmpCode, PayrunCode, CreditCode9 As CreditCode, CreditAdvancePayment9 As Amt, CreateDt ");
	        SQL.AppendLine("    From TblPayrollProcess1 ");
	        SQL.AppendLine("    Where CreditCode9 Is Not Null ");
	        SQL.AppendLine("    Union All ");
	        SQL.AppendLine("    Select EmpCode, PayrunCode, CreditCode10 As CreditCode, CreditAdvancePayment10 As Amt, CreateDt ");
	        SQL.AppendLine("    From TblPayrollProcess1 ");
	        SQL.AppendLine("    Where CreditCode10 Is Not Null ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Inner Join TblCredit C On A.CreditCode = C.CreditCode ");
            SQL.AppendLine("Left Join TblSite D On B.SiteCode = D.SiteCode ");
            SQL.AppendLine("Left Join TblDivision E On B.DivisionCode = E.DivisionCode ");
            SQL.AppendLine("Left Join TblDepartment F On B.DeptCode = F.DeptCode ");
            SQL.AppendLine("Inner Join TblPayrun G On A.PayrunCode = G.PayrunCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select EmpCode, PayrunCode, Group_Concat(Distinct IfNull(Mth, '')) Mth, Count(*) as Counter ");
            SQL.AppendLine("    From TblAdvancePaymentPROCESS  ");
            SQL.AppendLine("    Group By EmpCode, PayrunCode ");
            SQL.AppendLine(") H On A.EmpCode = H.EmpCode And A.PayrunCode = H.PayrunCode ");
            SQL.AppendLine(")A ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Employee's Code",
                    "Employee's Old Code", 
                    "Employee's Name",
                    "Site",
                    "Division",

                    //6-10
                    "Department",
                    "Credit",
                    "Amount",
                    "Installment"+Environment.NewLine+"Month", 
                    "Installment"+Environment.NewLine+"Count", 

                    //11
                    "Payrun Code",
                },
                new int[] 
                {
                    //0
                    60,

                    //1-5
                    120, 150, 150, 130, 130,
                    
                    //6-10
                    130, 180, 150, 100, 100,

                    //11
                    100
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10 }, 0);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColInvisible(Grd1, new int[] {11} );
            Grd1.Cols[11].Move(2);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 11 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName", "A.EmpCodeOld" });

                Sm.FilterStr(ref Filter, ref cm, TxtPayrunCode.Text, new string[] { "A.PayrunCode", "A.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.EmpCode, A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "EmpCode",

                            //1-5
                            "EmpCodeOld", "EmpName", "SiteName", "DivisionName", "DeptName", 

                            //6-7
                            "CreditName", "Amt", "InstallmentMonth", "InstallmentCount", "PayrunCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        }, true, false, false, false
                    );
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ForeColor = Color.Black;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.HideSubtotals(Grd1);
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 10 });
            
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtPayrunCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPayrunCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");

        }
        #endregion

    }
}
