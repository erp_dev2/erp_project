﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAdditionalCostPI : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptAdditionalCostPI(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                Sl.SetLueVdCode(ref LueVdCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select X.DocNo, X.DocDt, X.ProcessInd, X.VdCode, X.VdName, X.VdInvNo, X.CurCode, ");
            SQL.AppendLine("X.Recv, X.PO, ");
            SQL.AppendLine("X.AcNo, X.AcDesc, X.Amt, X.AcType, X.Damt, X.Camt, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When X.AcType = 'D' Then X.Damt+(-1*X.CAmt)");
            SQL.AppendLine("When X.AcType = 'C' Then X.CAmt+(-1*X.DAmt) ");
            SQL.AppendLine("End As Amount, X.remark ");
            SQL.AppendLine("From (");
	        SQL.AppendLine("    Select A.DocNo, A.DocDt, ");
	        SQL.AppendLine("    Case When A.Processind = 'F' Then 'FullFiled'");
	        SQL.AppendLine("    When A.ProcessInd = 'O' Then 'Outstanding'");
	        SQL.AppendLine("    End As ProcessInd, A.VdCode, F.VdName, A.VdInvNo, A.CurCode,");
	        SQL.AppendLine("    Group_concat(distinct C.RecvVdDocNo separator '; ') As Recv,");
	        SQL.AppendLine("    Group_concat(distinct E.PODocNo separator '; ') As PO,");
	        SQL.AppendLine("    B.AcNo, G.AcDesc, A.Amt, G.AcType,  B.DAmt, B.CAmt, B.remark ");
	        SQL.AppendLine("    From TblPurchaseInvoiceHdr A");
	        SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl4 B On A.DocNo = B.docNo");
	        SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl C On A.DocNo = C.DocNo");
	        SQL.AppendLine("    Inner Join TblRecvVdHdr D On C.RecvVdDocNo = D.DocNo ");
	        SQL.AppendLine("    Inner Join TblRecvVdDtl E On D.DocNo = E.DocNo And E.CancelInd = 'N'");
	        SQL.AppendLine("    Inner Join TblVendor F On A.VdCode = F.VdCode");
	        SQL.AppendLine("    Inner Join tblCoa G On B.AcNo = G.AcNo");
	        SQL.AppendLine("    Where A.CancelInd = 'N' And Left(B.AcNo, 1)='5' ");
	        SQL.AppendLine("    Group by A.DocNo");
            SQL.AppendLine(")X");


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "",
                        "Date",
                        "Status",
                        "Vendor",

                        //6-10
                        "",
                        "Vendor"+Environment.NewLine+"Name",
                        "Invoice",
                        "Currency",
                        "Receiving",
                        
                        //11-15
                        "Purchase"+Environment.NewLine+"Order",
                        "Total"+Environment.NewLine+"Amount",
                        "Account"+Environment.NewLine+"Number",
                        "Account"+Environment.NewLine+"Description",
                        "Value",

                        //16
                        "Remark"
                    },
                    new int[] 
                    {
                        50,
                        150, 20, 100, 100, 80, 
                        20, 200, 150,  80, 150, 
                        150, 150, 80, 250, 150,
                        150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 12, 15 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 2, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 13 });
            Sm.GrdColReadOnly(Grd1, new int[] {0, 1, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16});
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "X.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "X.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "X.VdCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By X.DocDt, X.DocNo;",
                        new string[]
                        { 
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "ProcessInd", "VdCode", "VdName", "VdInvNo",

                            //6-10
                            "CurCode", "Recv", "PO", "Amt", "AcNo", 
 
                            //11-13
                            "AcDesc", "Amount", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVendor(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mVdCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmVendor(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mVdCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion


        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

      
        private void TxtDocNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

       
    }
}
