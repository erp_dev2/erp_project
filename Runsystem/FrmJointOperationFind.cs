﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmJointOperationFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmJointOperation mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmJointOperationFind(FrmJointOperation FrmParent) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Joint Operation"+Environment.NewLine+"Code", 
                        "Joint Operation"+Environment.NewLine+"Name",
                        "Active",
                        "Contact Person"+Environment.NewLine+"Name",
                        "ID Number",

                        //6-10
                        "Contact Number",
                        "Remark",
                        "Created"+Environment.NewLine+"By",   
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 

                        //11-13
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[]
                    {
                        //0
                        60,

                        //1-5
                        100, 150, 50, 150, 100,

                        //6-10
                        100, 150, 100, 100, 100,

                        //11-13
                        100, 100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 9, 12 });
            Sm.GrdFormatTime(Grd1, new int[] { 10,11 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                if (ChkActInd.Checked) Filter = " And A.ActInd='Y' ";
                Sm.FilterStr(ref Filter, ref cm, TxtJOName.Text, new string[] { "A.JOCode", "A.JOName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.JOCode, A.JOName, A.ActInd,  " +
                    "B.ContactPersonName, B.IDNumber, B.ContactNumber, A.Remark,A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt " +
                    "From TblJointOperationHdr A Inner Join TblJointOperationDtl B On A.JOCode = B.JOCode " +
                    Filter + " Order By A.JOCode, A.JOName ",
                    new string[]
                    {
                        //0
                        "JOCode", 
                            
                        //1-5
                        "JOName", "ActInd", "ContactPersonName", "IDNumber", "ContactNumber",

                        //6-10
                        "Remark", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("T", Grd1, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("T", Grd1, dr, c, Row, 13, 10);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Button Method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtJOName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkJOName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Joint Operation");
        }

        #endregion

       
        #endregion

       
    }
}
