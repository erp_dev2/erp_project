﻿#region Update
/*
    11/04/2023 [SET/BBT] Manu Baru
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryCostMultiFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPropertyInventoryCostMulti mFrmParent;
        private string mSQL = string.Empty;


        #endregion

        #region Constructor

        public FrmPropertyInventoryCostMultiFind(FrmPropertyInventoryCostMulti FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueCCCode(ref LueCCCode);
                SetSQL();
                SetGrd();
                ChkExcludedCancelledDoc.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DNo, A.DocDt, A.CancelInd, A.SiteCode, A.CCCode, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As `Status`, ");
            SQL.AppendLine("A.PropertyCode, A.PropertyName, A.PropertyCategoryName, A.SiteName, A.CCName, A.`Type`, A.RemStockQty, A.UomCode, ");
            SQL.AppendLine("A.ValueBeforeAdd, A.CostComponentAmt, A.ValueAfterAdd, A.CostComponentReason, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.DocNo, B.DNo, A.DocDt, A.CancelInd, A.`Status`, ");
            SQL.AppendLine("    B.PropertyCode, D.PropertyName, E.PropertyCategoryName, F.SiteName, H.CCName, G.OptDesc `Type`, B.RemStockQty, D.UomCode, ");
            SQL.AppendLine("    B.ValueBeforeAdd, B.CostComponentAmt, B.ValueAfterAdd, A.CostComponentReason, B.SiteCode, B.CCCode, ");
            SQL.AppendLine("    A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("    From TblPropertyInventoryCostComponentHdr A ");
            SQL.AppendLine("    INNER JOIN TblPropertyInventoryCostComponentDtl4 B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER Join TblPropertyInventoryCostComponentDtl C On A.DocNo = C.DocNo  ");
            SQL.AppendLine("    INNER Join TblPropertyInventoryHdr D On B.PropertyCode = D.PropertyCode ");
            SQL.AppendLine("    INNER Join TblPropertyInventoryCategory E On B.PropertyCategoryCode = E.PropertyCategoryCode ");
            SQL.AppendLine("    INNER Join TblSite F On B.SiteCode = F.SiteCode ");
            SQL.AppendLine("    INNER Join TblOption G On G.OptCat = 'PropertyInventoryDocType' And C.DocType = G.OptCode ");
            SQL.AppendLine("    INNER JOIN TblCostCenter H ON B.CCCode = H.CCCode ");
            SQL.AppendLine("    GROUP BY A.DocNo, B.DNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.DocNo, B.DNo, A.DocDt, A.CancelInd, A.`Status`, ");
            SQL.AppendLine("    B.PropertyCode, D.PropertyName, E.PropertyCategoryName, F.SiteName, H.CCName, G.OptDesc `Type`, B.RemStockQty, D.UomCode, ");
            SQL.AppendLine("    B.ValueBeforeAdd, B.CostComponentAmt, B.ValueAfterAdd, A.CostComponentReason, B.SiteCode, B.CCCode, ");
            SQL.AppendLine("    A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("    From TblPropertyInventoryCostComponentHdr A ");
            SQL.AppendLine("    INNER JOIN TblPropertyInventoryCostComponentDtl4 B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryCostComponentDtl2 C On A.DocNo = C.DocNo ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryHdr D On B.PropertyCode = D.PropertyCode ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryCategory E On B.PropertyCategoryCode = E.PropertyCategoryCode ");
            SQL.AppendLine("    Inner Join TblSite F On B.SiteCode = F.SiteCode ");
            SQL.AppendLine("    Inner Join TblOption G On G.OptCat = 'PropertyInventoryDocType' And C.DocType = G.OptCode ");
            SQL.AppendLine("    INNER JOIN TblCostCenter H ON B.CCCode = H.CCCode ");
            SQL.AppendLine("    GROUP BY A.DocNo, B.DNo ");
            SQL.AppendLine(") A Where 0=0 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
            Grd1, new string[]
            {
                //0
                "No.",

                //1-5
                "Document#",
                "Date",
                "Cancel",
                "Status",
                "Property Code",

                //6-10
                "Property Name",
                "Property Category",
                "Site",
                "Cost Center",
                "Type",

                //11-15
                "Remaining Stock"+Environment.NewLine+"Quantity",
                "UoM",
                "Property Inventory Value"+Environment.NewLine+"Before Addition",
                "Cost Component Value",
                "Property Inventory Value"+Environment.NewLine+"After Addition",

                //16-20
                "Add. Cost Component Reason",
                "Created By",
                "Created"+Environment.NewLine+"Date",
                "Created"+Environment.NewLine+"Time",
                "Last"+Environment.NewLine+"Updated By",

                //21-22
                "Last"+Environment.NewLine+"Updated Date",
                "Last"+Environment.NewLine+"Updated Time",
            },
            new int[]
            {
                //0
                50,

                //1-5
                150, 100, 50, 100, 150, 

                //6-10
                150, 150, 150, 150, 150, 

                //11-15
                100, 100, 200, 150, 200, 
                
                //16-20
                200, 100, 100, 100, 100,

                //21-22
                100, 100
            }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 18, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 13, 14, 15 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
            
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                if (ChkExcludedCancelledDoc.Checked)
                    Filter = " And A.Status In ('O', 'A') And A.CancelInd='N' ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "A.CCCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Group By A.DocNo, A.DNo Order By A.DocNo; ",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            //1-5
                            "DocDt", "CancelInd", "Status", "PropertyCode", "PropertyName", 
                            //6-10
                            "PropertyCategoryName", "SiteName", "CCName", "Type", "RemStockQty", 
                            //11-15
                            "UomCode", "ValueBeforeAdd", "CostComponentAmt", "ValueAfterAdd", "CostComponentReason", 
                            //16-19
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 15);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 22, 19);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Event

        #region Misc Control Event

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }
        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }
        private void TxtProperty_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProperty_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Property");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(Sl.SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");
        }

        #endregion

        #endregion
    }
}
