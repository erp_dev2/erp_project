﻿#region Update
    // 16/10/2020 [ICA/MGI] New Application
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmUpdatePurchaseInvoiceDueDt : RunSystem.FrmBase12
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmUpdatePurchaseInvoiceDueDt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.SetControlReadOnly(new List<BaseEdit> { TxtDocNo, DteDueDtOld }, true);
                DteDueDtNew.Focus();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<BaseEdit> 
            {
               TxtDocNo, DteDueDtOld, DteDueDtNew
            });
            DteDueDtNew.Focus();
        }

        #endregion

        #region Button Methods

        override protected void BtnProcessClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false)||
                Sm.IsDteEmpty(DteDueDtNew, "Due Date New") ||
                Sm.StdMsgYN("Process", "") == DialogResult.No)
                return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ProcessData();
                ClearData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        private void ProcessData()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("UPDATE tblpurchaseinvoicehdr ");
            SQL.AppendLine("SET duedt = @DocDt ");
            SQL.AppendLine("WHERE DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDueDtNew));

            cm.CommandText = SQL.ToString();

            Sm.ExecCommand(cm);
            Sm.StdMsg(mMsgType.Info, " Due Date has been Updated.");
        }

        #endregion

        private void BtnPurchaseInvoice_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmUpdatePurchaseInvoiceDueDtDlg(this));
        }

        private void BtnPurchaseInvoice2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDocNo, "Purchase Invoice#", false))
            {
                try
                {
                    var f = new FrmPurchaseInvoice("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.StdMsg(mMsgType.Warning, Exc.Message);
                }
            }
        }
    }
}
