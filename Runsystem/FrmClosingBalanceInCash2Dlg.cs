﻿#region Update
/*
    18/02/2022 [DITA/PHT] new apps -> fasilitas untuk memilih bank account yg mau diproses.
    28/03/2022 [DITA/PHT] bank account yg muncul juga anak-anak nya profit center yg difilter
    25/11/2022 [DITA/PHT] profit center di detail perlu jadi patokan karna didetail terdapat profit center child
    13/03/2023 [MYA/BBT] Membuat penyesuaian di Closing Balance In Cash/Bank Account
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmClosingBalanceInCash2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmClosingBalanceInCash2 mFrmParent;
        private string mSQL = string.Empty, mMth = string.Empty, mYr = string.Empty, mProfitCenterCodes = string.Empty;
        
        #endregion

        #region Constructor

        public FrmClosingBalanceInCash2Dlg(FrmClosingBalanceInCash2 FrmParent, string Mth, string Yr)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mMth = Mth;
            mYr = Yr;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                //SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetSQL(ref MySqlCommand cm)
        {
            var SQL = new StringBuilder();
            var Filter = string.Empty;
            int i = 0;
            string Query = string.Empty;
            if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
            {
                mFrmParent.SetCCbProfitCenterCodeWithChild();
                foreach (var x in mFrmParent.mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (A.ProfitCenterCode=@ProfitCenter" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@ProfitCenter" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    Query += "    And 1=0 ";
                else
                    Query += "    And (" + Filter + ") ";

            }
            SQL.AppendLine("Select T5.ProfitCenterCode, T5.ProfitCenterName, T1.BankAcCode, T3.BankName, T2.BankAcNo, T2.BankAcNm, T1.Amt, ");
            SQL.AppendLine("T4.EntName ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T.ProfitCenterCode, T.BankAcCode, Sum(T.Amt) As Amt From (");
            if(mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                SQL.AppendLine("        Select B.ProfitCenterCode, B.BankAcCode, B.Amt ");
            else
                SQL.AppendLine("        Select null as ProfitCenterCode, B.BankAcCode, B.Amt ");
            SQL.AppendLine("        From TblClosingBalanceInCashHdr A ");
            SQL.AppendLine("        Inner Join TblClosingBalanceInCashDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Where A.DocNo=B.DocNo And Concat(A.Yr, A.Mth)=@YrMth2 ");
            SQL.AppendLine(Query.Replace("A.", "B."));
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select D.ProfitCenterCode, A.BankAcCode, Case A.Actype When 'C' Then -1 Else 1 End * B.Amt As Amt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode=C.BankAcCode ");
            if(mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                SQL.AppendLine("        Inner Join TblCostCenter D On C.CCCode = D.CCCode ");
            else
                SQL.AppendLine("        left Join TblCostCenter D On C.CCCode = D.CCCode ");
            SQL.AppendLine("        Where A.DocNo = B.DocNo And A.CancelInd='N' And Left(A.DocDt, 6)=@YrMth1 ");
            SQL.AppendLine(Query.Replace("A.", "D."));
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select D.ProfitCenterCode, A.BankAcCode2 As BankAcCode, (Case A.Actype2 When 'C' Then -1 Else 1 End) * B.Amt * A.ExcRate As Amt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode2=C.BankAcCode ");
            if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                SQL.AppendLine("        Inner Join TblCostCenter D On C.CCCode = D.CCCode ");
            else
                SQL.AppendLine("        left Join TblCostCenter D On C.CCCode = D.CCCode ");
            SQL.AppendLine("        Where A.DocNo=B.DocNo And A.CancelInd='N' And Left(A.DocDt, 6)=@YrMth1 And A.AcType2 Is Not Null ");
            SQL.AppendLine(Query.Replace("A.", "D."));
            SQL.AppendLine("    ) T Group By T.ProfitCenterCode, T.BankAcCode ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblBankAccount T2 On T1.BankAcCode=T2.BankAcCode ");
            SQL.AppendLine("Left Join TblBank T3 On T2.BankCode=T3.BankCode ");
            SQL.AppendLine("Left Join TblEntity T4 On T2.EntCode=T4.EntCode ");
            if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
            {
                SQL.AppendLine("Inner Join TblProfitCenter T5 On T1.ProfitCenterCode=T5.ProfitCenterCode ");
                SQL.AppendLine("Where Concat(T1.ProfitCenterCode, T1.BankAcCode) Not In ( ");
                SQL.AppendLine("    Select ifnull(Concat(B.ProfitCenterCode, B.BankAcCode), '') ");
                SQL.AppendLine("    From TblClosingBalanceInCashHdr A ");
                SQL.AppendLine("    Inner Join TblClosingBalanceInCashDtl B On A.DocNo=B.DocNo And A.CancelInd = 'N' ");
                SQL.AppendLine("    Where A.Mth=@Mth ");
                SQL.AppendLine("    And A.Yr=@Yr ");
                SQL.AppendLine("    ) ");
            }
            else
                SQL.AppendLine("Left Join TblProfitCenter T5 On T1.ProfitCenterCode=T5.ProfitCenterCode ");



            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Profit Center Code",
                        "Profit Center Name",
                        "Bank Account Code",
                        "Bank",
                       
                        //6-9
                        "Account#",
                        "Bank Account Name",
                        "Amount",
                        "Entity"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 200, 100, 180, 
                        
                        //6-9
                        180, 180, 180, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, false);
            if (!mFrmParent.mIsEntityMandatory) Sm.GrdColInvisible(Grd1, new int[] { 9 }, false);
            if (!mFrmParent.mIsFicoUseMultiProfitCenterFilter) Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, false);
            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;
                string BankAcCode = string.Empty;
                string ProfitCenterCode = string.Empty;
                var cm = new MySqlCommand();
                
                string
                     Mth2 = (mMth == "01") ? "12" : Sm.Right("0" + (int.Parse(mMth) - 1).ToString(), 2),
                     Yr2 = (mMth == "01") ? ((int.Parse(mYr)) - 1).ToString() : mYr;

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@Yr", mYr);
                Sm.CmParam<String>(ref cm, "@Mth", mMth);
                Sm.CmParam<String>(ref cm, "@YrMth1", mYr + mMth);
                Sm.CmParam<String>(ref cm, "@YrMth2", Yr2 + Mth2);
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", mProfitCenterCodes);

                GetSQL(ref cm);

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        ProfitCenterCode = Sm.GetGrdStr(mFrmParent.Grd1, r, 2);
                        BankAcCode = Sm.GetGrdStr(mFrmParent.Grd1, r, 4);
                        string Filter2 = string.Concat(ProfitCenterCode, BankAcCode);
                        if (Filter2.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += "(Concat(T5.ProfitCenterCode, T1.BankAcCode)=@Filter0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@Filter0" + r.ToString(), Filter2);
                        }
                    }
                }
                if (Filter.Length > 0)
                    Filter = " And Not(" + Filter + ") ";
                else
                    Filter = " ";


                Sm.FilterStr(ref Filter, ref cm, TxtBankAcCode.Text, new string[] { "T2.BankAcCode", "T2.BankAcNm" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T2.BankAcNm;",
                        new string[]
                     { 
                        //0
                        "ProfitCenterCode", 

                        //1-5
                        "ProfitCenterName", "BankAcCode", "BankName", "BankAcNo", "BankAcNm",
                        
                        //6-7
                         "Amt", "EntName"
                     },
                     (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                     {
                         Grd.Cells[Row, 0].Value = Row + 1;
                         Grd.Cells[Row, 1].Value = false;
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                         Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                     }, false, false, true, false
                 );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 8 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 Bank Account.");
        }

        private bool IsDataAlreadyChosen(int r)
        {
            var ProfitCenterCode = Sm.GetGrdStr(Grd1, r, 2);
            var BankAcCode = Sm.GetGrdStr(Grd1, r, 4);
            string Filter2 = string.Concat(ProfitCenterCode, BankAcCode);
            for (int i = 0; i < mFrmParent.Grd1.Rows.Count - 1; i++)
                if (Sm.CompareStr(string.Concat(Sm.GetGrdStr(mFrmParent.Grd1, i, 2), Sm.GetGrdStr(mFrmParent.Grd1, i, 4)), Filter2)) return true;
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        
        #region Misc Control Event

        private void ChkBankAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bank Account");
        }

        private void TxtBankAcCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
