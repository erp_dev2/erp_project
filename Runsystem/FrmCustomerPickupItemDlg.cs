﻿#region Update
/*
    19/08/2020 [IBL/MGI] new apps
    24/08/2020 [WED/MGI] ambil DO#
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCustomerPickupItemDlg : RunSystem.FrmBase4
    {
        #region Field
        private FrmCustomerPickupItem mFrmParent;
        private string mSQL = string.Empty, mCtCode = string.Empty;

        #endregion

        #region Constructor

        public FrmCustomerPickupItemDlg(FrmCustomerPickupItem FrmParent, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueItCtCode(ref LueItCtCode);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.DocNo As DOCtDocNo, A.DNo As DOCtDNo, A.CancelInd, C.ActInd, C.ItCode, C.ItName, ");
	        SQL.AppendLine("    IfNull(A.Qty, 0) As DOQty, IfNull(B.Qty, 0) As PickupQty, ");
	        SQL.AppendLine("    IfNull( ");
		    SQL.AppendLine("        IfNull(A.Qty, 0) - IfNull(B.Qty, 0), 0 ");
	        SQL.AppendLine("    ) As Balance, A.BatchNo, D.ItCtCode ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select X1.DocNo, X1.DocDt, X2.DNo, X2.CancelInd, X2.ItCode, X2.Qty, X2.BatchNo ");
            SQL.AppendLine("        From TblDOCt2Hdr X1 ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("            And X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            And X1.CtCode = @CtCode ");
            SQL.AppendLine("            And X2.CancelInd = 'N' ");
            SQL.AppendLine("            And Not Find_In_Set(Concat(X2.DocNo, X2.DNo), @DOCtDNo) ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select X1.DocNo, X1.DocDt, X2.DNo, X2.CancelInd, X2.ItCode, X2.Qty, X2.BatchNo ");
            SQL.AppendLine("        From TblDOCtHdr X1 ");
            SQL.AppendLine("        Inner join TblDOCtDtl X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("            And X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            And X1.CtCode = @CtCode ");
            SQL.AppendLine("            And X1.Status = 'A' And X2.CancelInd = 'N' ");
            SQL.AppendLine("            And Not Find_In_Set(Concat(X2.DocNo, X2.DNo), @DOCtDNo) ");
            SQL.AppendLine("    ) A ");
	        SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select DOCtDocNo, DOCtDNo, Sum(Qty) Qty ");
            SQL.AppendLine("        From TblCustomerPickupItemDtl ");
            SQL.AppendLine("        Where CancelInd = 'N' ");
            SQL.AppendLine("        Group By DOCtDocNo, DOCtDNo ");
            SQL.AppendLine("    ) B On A.DocNo = B.DOCtDocNo And A.DNo = B.DOCtDNo ");
	        SQL.AppendLine("    Inner Join TblItem C On A.ItCode = C.ItCode And C.ActInd = 'Y' ");
	        SQL.AppendLine("    Left Join TblItemCategory D On C.ItCtCode = D.ItCtCode ");
            SQL.AppendLine("    Order By A.DocDt, A.DocNo ");
            SQL.AppendLine(") As Tbl ");
            SQL.AppendLine("Where Tbl.Balance > 0 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
               Grd1, new string[] 
               { 
                    //0
                    "No",

                    //1-5
                    "",
                    "DO#",
                    "DODNo",
                    "Item Code",
                    "Item Name",

                    //6-9
                    "DO Qty",
                    "Pickup Qty",
                    "Balance",
                    "Batch#",
                }, new int[]
                {
                    //0
                    50, 

                    //1-5
                    20, 150, 0, 100, 200, 

                    //6-9
                    150, 150, 150, 120
                }
           );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var cm = new MySqlCommand();
                string Filter = " ", mDOCtDNo = " ";

                if (mFrmParent.Grd1.Rows.Count > 1)
                {
                    for (int i = 0; i < mFrmParent.Grd1.Rows.Count - 1; i++)
                    {
                        if (mDOCtDNo.Length > 0) mDOCtDNo += ",";
                        mDOCtDNo += string.Concat(Sm.GetGrdStr(mFrmParent.Grd1, i, 18), Sm.GetGrdStr(mFrmParent.Grd1, i, 11));
                    }
                }
                
                Sm.CmParam<String>(ref cm, "@DOCtDNo", mDOCtDNo);
                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "Tbl.DOCtDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "Tbl.ItCode", "Tbl.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "Tbl.ItCtCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter,
                    new string[]
                    {
                        //0
                        "DOCtDocNo", 

                        //1-5
                        "DOCtDNo", "ItCode", "ItName", "DOQty", "PickupQty", 

                        //6-7
                        "Balance", "BatchNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            string DOCtDNo = string.Empty;

            if (Grd1.Rows.Count != 0)
            {
                if (IsItemAlreadyChosen()) return;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (DOCtDNo.Length > 0) DOCtDNo += ",";
                        DOCtDNo += string.Concat(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 3));
                    }
                }
            }

            mFrmParent.ShowDOCt(DOCtDNo);
        }

        private bool IsItemAlreadyChosen()
        {
            string mMasterData = string.Empty, mDlgData = string.Empty;

            for (int i = 0; i < mFrmParent.Grd1.Rows.Count - 1; ++i)
            {
                for (int j = 0; j < Grd1.Rows.Count; ++j)
                {
                    if (Sm.GetGrdBool(Grd1, i, 1))
                    {
                        mMasterData = string.Concat(Sm.GetGrdStr(mFrmParent.Grd1, i, 18), Sm.GetGrdStr(mFrmParent.Grd1, i, 11));
                        mDlgData = string.Concat(Sm.GetGrdStr(Grd1, i, 2), Sm.GetGrdStr(Grd1, i, 3));

                        if (mMasterData == mDlgData)
                        {
                            Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        #endregion

        #endregion
    }
}
