﻿#region Update
/*
    18/05/2018 [TKG] tambah local document#
    03/07/2018 [HAR] tambah informasi packaging Uom dan quantitynya
    24/08/2018 [WED] RFC IOK : 
                    - Ubah nama kolom Local Document --> Sales Contract# berdasarkan parameter LocalDocument
                    - Tambah highlight merah untuk Outstanding SO yang mendekati Delivery Date, berdasarkan parameter NoOfDayForSOWarning
                    - Tambah informasi kolom Stuffing Date, Status Shipment Planning, Jml Kontainer, Container's Group, berdasarkan parameter IsRptSO2DisplayShipmentData
    12/11/2018 [WED] tambah kolom Remark SO, tampilkan loop button berdasarkan parameter IsRptSO2ShowSOLoop
    07/01/2019 [DITA] Tambah kolom Container's Group Quantity & Delivery date
    04/02/2019 [DITA] Tambah Filter Delivery date
    07/05/2021 [VIN/KSM] Sales Contract dapat ditampilkan
    18/11/2021 [TYO/IOK] Penambahan kolom Item's Actual Name di samping kolom Item's Name
    25/03/2022 [VIN/KSM] BUG: Penambahan kolom Item's Actual Name di samping kolom Item's Name
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSO2 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mMenuCodeForRptSO2WithPrice = string.Empty,
            mLocalDocument = string.Empty,
            mLblLocalDocument = string.Empty,
            mNoOfDayForSOWarning = string.Empty,
            mDocTitle = string.Empty;
        private bool 
            mIsShowUoMInfo = false,
            mIsRptSO2DisplayShipmentData = false,
            mIsRptSO2ShowSOLoop = false,
            mIsSO2UseActualItem= false;
        private int mInitColsCount = 0, mAfterSetUomColCount = 0;

        #endregion

        #region Constructor

        public FrmRptSO2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                if (mLocalDocument == "1") mLblLocalDocument = "Sales" + Environment.NewLine + "Contract#";
                else mLblLocalDocument = "Local" + Environment.NewLine + "Document#";
                SetGrd();
                
                Sl.SetLueCtCode(ref LueCtCode);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mMenuCodeForRptSO2WithPrice = Sm.GetParameter("MenuCodeForRptSO2WithPrice");
            mLocalDocument = Sm.GetParameter("LocalDocument");
            mNoOfDayForSOWarning = Sm.GetParameter("NoOfDayForSOWarning");
            mIsRptSO2DisplayShipmentData = Sm.GetParameterBoo("IsRptSO2DisplayShipmentData");
            mIsRptSO2ShowSOLoop = Sm.GetParameterBoo("IsRptSO2ShowSOLoop");
            mDocTitle = Sm.GetParameter("DocTitle");
            mIsSO2UseActualItem = Sm.GetParameterBoo("IsSO2UseActualItem");
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.* From( ");

            SQL.AppendLine("Select X1.DocNo, X1.LocalDocNo, X1.DocDt, X1.CtCode, X5.CtName, X1.SAAddress, ");
            SQL.AppendLine("X8.ItCode, X9.ItCodeInternal, X9.ItName, X7.PriceUomCode As SalesUomCode, ");
            SQL.AppendLine("X2.Qty-IfNull(X3.LocalDOQty, 0)-IfNull(X4.ExportDOQty, 0) As Outstanding, ");
            SQL.AppendLine("X2.QtypackagingUnit-IfNull(X3.LocalDOQtypackaging, 0)-IfNull(X4.ExportDOQtypackaging, 0) As QtypackagingUnit, X2.packagingunituomcode, ");
            SQL.AppendLine("X2.ContainerGroup,X2.ContainerGroupQty, X2.DeliveryDt, X11.ItName ItName2, ");
            if (mIsRptSO2DisplayShipmentData)
                SQL.AppendLine("X10.ShpStatus, ");
            else
                SQL.AppendLine("null as ShpStatus, ");
            SQL.AppendLine("X2.DNo, X1.Remark ");

            SQL.AppendLine("From TblSOHdr X1 ");
            SQL.AppendLine("Inner Join TblSODtl X2 On X1.DocNo=X2.DocNo And X2.ProcessInd2<>'F' ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.DocNO, T.DNo, T.localDOQty, (T.LocalDOQty / (T.QtySO / T.QtyPackagingUnit)) As LocalDOQtypackaging ");
            SQL.AppendLine("    From( ");
   	        SQL.AppendLine("        Select T1.DocNo, T1.DNo, "); 
		    SQL.AppendLine("        Case When IfNull(T2.QtyInventory, 0)=0 Then 0 "); 
            SQL.AppendLine("        Else (IfNull(T3.Qty, 0)/IfNull(T2.QtyInventory, 0))*IfNull(T2.Qty, 0) ");
            SQL.AppendLine("        End As LocalDOQty, T1.QtypackagingUnit, T1.Qty As QtySO "); 
            SQL.AppendLine("        From TblSODtl T1 "); 
            SQL.AppendLine("        Left Join ( "); 
            SQL.AppendLine("            Select B.SODocNo, B.SODNo, "); 
            SQL.AppendLine("            Sum(B.Qty) As Qty, Sum(B.QtyInventory) As QtyInventory "); 
            SQL.AppendLine("            From TblDRHdr A "); 
            SQL.AppendLine("            Inner Join TblDRDtl B "); 
			SQL.AppendLine("                On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo=C.DocNo And C.CancelInd='N' ");
            SQL.AppendLine("                And (C.DocDt Between @DocDt1 And @DocDt2) ");
		    SQL.AppendLine("            Inner Join TblSODtl D "); 
			SQL.AppendLine("                On D.DocNo=C.DocNo And D.ProcessInd2<>'F' ");
			SQL.AppendLine("                And D.DocNo=B.SODocNo And D.DNo=B.SODNo "); 
            SQL.AppendLine("            Where A.CancelInd='N' "); 
            SQL.AppendLine("            Group By B.SODocNo, B.SODNo "); 
		    SQL.AppendLine("        ) T2 On T1.DocNo=T2.SODocNo And T1.DNo=T2.SODNo "); 
            SQL.AppendLine("        Left Join ( "); 
      	    SQL.AppendLine("            Select C.SODocNo, C.SODNo, Sum(B.Qty) As Qty "); 
            SQL.AppendLine("            From TblDOCt2Hdr A "); 
            SQL.AppendLine("            Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo "); 
            SQL.AppendLine("            Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo "); 
            SQL.AppendLine("            Inner Join TblSOHdr D On C.SODocNo=D.DocNo And D.CancelInd='N' ");
            SQL.AppendLine("                And (D.DocDt Between @DocDt1 And @DocDt2) ");
		    SQL.AppendLine("            Inner Join TblSODtl E "); 
			SQL.AppendLine("                On E.DocNo=D.DocNo And E.ProcessInd2<>'F' ");
			SQL.AppendLine("                And E.DocNo=C.SODocNo And E.DNo=C.SODNo "); 
            SQL.AppendLine("            Group By C.SODocNo, C.SODNo "); 
		    SQL.AppendLine("        ) T3 On T1.DocNo=T3.SODocNo And T1.DNo=T3.SODNo ");
            SQL.AppendLine("        Inner Join TblSOHdr T4 On T1.DocNo=T4.DocNo And T4.CancelInd='N' ");
            SQL.AppendLine("                And (T4.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    ) T Where T.LocalDOQty <>0 "); 
            SQL.AppendLine(") X3 On X2.DocNo=X3.DocNo And X2.DNo=X3.DNo "); 
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.DocNO, T.DNo, T.ExportDOQty, (T.ExportDOQty / (T.QtySO / T.QtyPackagingUnit)) As ExportDOQtypackaging From ( ");
            SQL.AppendLine("        Select T1.DocNo, T1.DNo, "); 
		    SQL.AppendLine("        Case When IfNull(T2.QtyInventory, 0)=0 Then 0 "); 
            SQL.AppendLine("        Else (IfNull(T3.Qty, 0)/IfNull(T2.QtyInventory, 0))*IfNull(T2.Qty, 0) ");
            SQL.AppendLine("        End As ExportDOQty, T1.QtypackagingUnit, T1.Qty As QtySO  "); 
            SQL.AppendLine("        From TblSODtl T1 "); 
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select B.SODocNo, B.SODNo, "); 
            SQL.AppendLine("            Sum(B.Qty) As Qty, Sum(B.QtyInventory) As QtyInventory "); 
            SQL.AppendLine("            From TblPLHdr A "); 
            SQL.AppendLine("            Inner Join TblPLDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo=C.DocNo And C.CancelInd='N' And C.Status<>'F' And C.Status<>'M' ");
            SQL.AppendLine("                And (C.DocDt Between @DocDt1 And @DocDt2) ");
		   	SQL.AppendLine("            Inner Join TblSODtl D "); 
			SQL.AppendLine("                On D.DocNo=C.DocNo And D.ProcessInd2<>'F' ");
            SQL.AppendLine("                And D.DocNo=B.SODocNo And D.DNo=B.SODNo "); 
            SQL.AppendLine("            Group By B.SODocNo, B.SODNo "); 
            SQL.AppendLine("        ) T2 On T1.DocNo=T2.SODocNo And T1.DNo=T2.SODNo "); 
            SQL.AppendLine("        Left Join ( "); 
            SQL.AppendLine("            Select C.SODocNo, C.SODNo, Sum(B.Qty) As Qty "); 
            SQL.AppendLine("            From TblDOCt2Hdr A "); 
            SQL.AppendLine("            Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo "); 
            SQL.AppendLine("            Inner Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PLDNo=C.DNo ");
            SQL.AppendLine("            Inner Join TblSOHdr D On C.SODocNo=D.DocNo And D.CancelInd='N' And D.Status<>'F' And D.Status<>'M' ");
            SQL.AppendLine("                And (D.DocDt Between @DocDt1 And @DocDt2) ");
		   	SQL.AppendLine("            Inner Join TblSODtl E "); 
			SQL.AppendLine("                On E.DocNo=D.DocNo And E.ProcessInd2<>'F' ");
			SQL.AppendLine("                And E.DocNo=C.SODocNo And E.DNo=C.SODNo "); 
            SQL.AppendLine("            Group By C.SODocNo, C.SODNo "); 
            SQL.AppendLine("        ) T3 On T1.DocNo=T3.SODocNo And T1.DNo=T3.SODNo ");
            SQL.AppendLine("    ) T Where T.ExportDOQty <> 0 "); 
            SQL.AppendLine(") X4 On X2.DocNo=X4.DocNo And X2.DNo=X4.DNo "); 
            SQL.AppendLine("Inner Join TblCustomer X5 On X1.CtCode=X5.CtCode ");
            SQL.AppendLine("Inner Join TblCtQtDtl X6 On X1.CtQtDocNo=X6.DocNo And X2.CtQtDNo=X6.DNo ");
            SQL.AppendLine("Inner Join TblItemPriceHdr X7 On X6.ItemPriceDocNo=X7.DocNo ");
            SQL.AppendLine("Inner Join TblItemPriceDtl X8 On X6.ItemPriceDocNo=X8.DocNo And X6.ItemPriceDNo=X8.DNo ");
            SQL.AppendLine("Inner Join TblItem X9 On X8.ItCode=X9.ItCode ");
            if (mIsRptSO2DisplayShipmentData)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.DocNo As SODocNo, T2.DNo As SODNo, ");
                SQL.AppendLine("Group_Concat( ");
                SQL.AppendLine("Concat( ");
                SQL.AppendLine("    Case T5.Status When 'P' Then 'Planning' When 'R' Then 'Released' When 'C' Then 'Cancelled' When 'F' Then 'Final' End,  ");
                SQL.AppendLine("    ' (', ");
                SQL.AppendLine("    T4.DocNo, ");
                SQL.AppendLine("    ')' ");
                SQL.AppendLine(") Separator ' | ') ");
                SQL.AppendLine("As ShpStatus ");
                SQL.AppendLine("    From TblSOHdr T1 ");
                SQL.AppendLine("    Inner Join TblSODtl T2 On T1.DocNo = T2.DocNo And T2.ProcessInd <> 'F'  ");
                SQL.AppendLine("         And (T1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("     Inner Join TblSIDtl T3 On T2.DocNo = T3.SODocNo And T2.DNo = T3.SODNo ");
                SQL.AppendLine("     Inner Join TblSIHdr T4 On T3.DocNo = T4.DocNo ");
                SQL.AppendLine("     Inner Join TblSP T5 On T4.SPDocNo = T5.DocNo ");
                SQL.AppendLine("     Group By T1.DocNo, T2.DNo ");
                SQL.AppendLine(") X10 On X1.DocNo = X10.SODocNo And X2.DNo = X10.SODNo ");
            }
            SQL.AppendLine("Left join TblItem X11 On X11.itcode = X2.itcode2 ");
            SQL.AppendLine("Where X1.CancelInd='N' ");
            SQL.AppendLine("And (X1.DocDt Between @DocDt1 And @DocDt2) ");
            if(DteDeliveryDt1.EditValue!=null && DteDeliveryDt2.EditValue != null) SQL.AppendLine("And (X2.DeliveryDt Between @DeliveryDt1 And @DeliveryDt2) "); 
            SQL.AppendLine("And X2.Qty-IfNull(X3.LocalDOQty, 0)-IfNull(X4.ExportDOQty, 0)<>0 ");
            SQL.AppendLine("And X1.Status<>'F' And X1.Status<>'M' ");

            if (mDocTitle == "KSM")
            {
                SQL.AppendLine("UNION ALL  ");

                SQL.AppendLine("SELECT T6.DocNo, T6.LocalDocNo, T6.DocDt, T7.CtCode, T7.CtName, null SAAddress, ");
                SQL.AppendLine("T8.ItCode, T8.ItCodeInternal, T8.ItName, T8.SalesUOMCode As SalesUomCode, ");
                SQL.AppendLine("T1.Qty-IFNULL(T4.Qty, 0) As Outstanding, ");
                SQL.AppendLine("T1.Qty-IFNULL(T4.Qty, 0) As QtypackagingUnit, T8.SalesUOMCode, ");
                SQL.AppendLine("null ContainerGroup, null ContainerGroupQty, T1.DeliveryDt, Null As ItCode2,  ");
                SQL.AppendLine("null as ShpStatus,  ");
                SQL.AppendLine("T1.DNo, T6.Remark  ");
                SQL.AppendLine("From TblSalesMemoDtl T1  ");
                SQL.AppendLine("Inner Join (  ");
                SQL.AppendLine("  Select Distinct C.SODocNo, C.SODNo , A.DocDt ");
                SQL.AppendLine("  From TblDOCt2Hdr A  ");
                SQL.AppendLine("  Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo ");
                SQL.AppendLine("  Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo ");
                SQL.AppendLine("  Where A.DocDt  BETWEEN @DocDt1 AND @DocDt2 ");
                SQL.AppendLine(") T2 On T1.DocNo=T2.SODocNo And T1.DNo=T2.SODNo ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("  Select B.SODocNo, B.SODNo,  ");
                SQL.AppendLine("  Sum(B.Qty) As Qty, Sum(B.QtyInventory) As QtyInventory ");
                SQL.AppendLine("  From TblDRHdr A  ");
                SQL.AppendLine("Inner Join TblDRDtl B On A.DocNo=B.DocNo  ");
                SQL.AppendLine("Inner Join (  ");
                SQL.AppendLine("  Select Distinct C3.SODocNo, C3.SODNo  ");
                SQL.AppendLine("  From TblDOCt2Hdr C1 ");
                SQL.AppendLine("  Inner Join TblDOCt2Dtl2 C2 On C1.DocNo=C2.DocNo  ");
                SQL.AppendLine("  Inner Join TblDRDtl C3 On C1.DRDocNo=C3.DocNo And C2.DRDNo=C3.DNo ");
                SQL.AppendLine("  Where C1.DocDt  BETWEEN @DocDt1 AND @DocDt2 ");
                SQL.AppendLine("      ) C On B.SODocNo=C.SODocNo And B.SODNo=C.SODNo ");
                SQL.AppendLine("  Where A.CancelInd='N' ");
                SQL.AppendLine("  Group By B.SODocNo, B.SODNo ");
                SQL.AppendLine(") T3 On T1.DocNo=T3.SODocNo And T1.DNo=T3.SODNo ");
                SQL.AppendLine("Left Join (  ");
                SQL.AppendLine("  Select C.SODocNo, C.SODNo, Sum(B.Qty) As Qty   ");
                SQL.AppendLine("  From TblDOCt2Hdr A  ");
                SQL.AppendLine("  Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo  ");
                SQL.AppendLine("  Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo  ");
                SQL.AppendLine("  Inner Join ( ");
                SQL.AppendLine("      Select Distinct D3.SODocNo, D3.SODNo  ");
                SQL.AppendLine("      From TblDOCt2Hdr D1  ");
                SQL.AppendLine("      Inner Join TblDOCt2Dtl2 D2 On D1.DocNo=D2.DocNo  ");
                SQL.AppendLine("      Inner Join TblDRDtl D3 On D1.DRDocNo=D3.DocNo And D2.DRDNo=D3.DNo  ");
                SQL.AppendLine("      Where D1.DocDt  BETWEEN @DocDt1 AND @DocDt2  ");
                SQL.AppendLine("  ) D On C.SODocNo=D.SODocNo And C.SODNo=D.SODNo  ");
                SQL.AppendLine("  Group By C.SODocNo, C.SODNo  ");
                SQL.AppendLine("  ) T4 On T1.DocNo=T4.SODocNo And T1.DNo=T4.SODNo  ");
                SQL.AppendLine("INNER JOIN tblsalesmemohdr T5 ON T1.DocNo=T5.DocNo  ");
                SQL.AppendLine("INNER JOIN tblsalescontract T6 ON T5.DocNo=T6.SalesMemoDocNo  ");
                SQL.AppendLine("INNER JOIN tblcustomer T7 ON T5.CtCode=T7.CtCode  ");
                SQL.AppendLine("INNER JOIN tblitem T8 ON T1.ItCode=T8.ItCode  ");
                SQL.AppendLine("WHERE T6.DocDt BETWEEN @DocDt1 AND @DocDt2  ");
                SQL.AppendLine("AND T6.CancelInd='N' ");
                if (DteDeliveryDt1.EditValue != null && DteDeliveryDt2.EditValue != null) SQL.AppendLine("And (T1.DeliveryDt Between @DeliveryDt1 And @DeliveryDt2) ");
                SQL.AppendLine("AND T1.Qty-T4.Qty <> 0 ");
            }
            SQL.AppendLine(")A ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "",
                    mLblLocalDocument,
                    "Date",
                    "Customer",
                    
                    //6-10
                    "Shipping Address",
                    "Item's"+Environment.NewLine+"Code",
                    "Local"+Environment.NewLine+"Code",
                    "",
                    "Item's Name",
                    
                    //11-15
                    "Outstanding"+Environment.NewLine+"Quantity",
                    "UoM",
                    "Outstanding"+Environment.NewLine+"Packaging"+Environment.NewLine+"Quantity",
                    "Packaging"+Environment.NewLine+"UoM",
                    "",

                    //16-20
                    "Shipment Planning's" + Environment.NewLine + "Status",
                    "Container's Group",
                    "Container's Group " + Environment.NewLine + " Quantity",
                    "Delivery " + Environment.NewLine + "Date",
                    "SODNo",

                    //21 -22
                    "Remark",
                    "Item's Actual Name"

                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 20, 130, 80, 250, 
                    
                    //6-10
                    300, 100, 80, 20, 250, 

                    //11-15
                    120, 80, 120, 80, 20,

                    //16-20
                    220, 120,120, 80, 0,

                    //21-22
                    200, 150
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2, 9, 15 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 13, 18 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 4,19 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 16, 17, 18, 19,20, 21 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 20 }, false);
            if (!Sm.CompareStr(mMenuCodeForRptSO2WithPrice, mMenuCode))
                Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            if (!mIsRptSO2DisplayShipmentData) 
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 20 });

            if (!mIsSO2UseActualItem)
                Sm.GrdColInvisible(Grd1, new int[] { 22 });
            Grd1.Cols[22].Move(11);
            mInitColsCount = Grd1.Cols.Count;
            SetUoM();
            mAfterSetUomColCount = Grd1.Cols.Count;
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "Where 0=0 ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParamDt(ref cm, "@DeliveryDt1", Sm.GetDte(DteDeliveryDt1));
                Sm.CmParamDt(ref cm, "@DeliveryDt2", Sm.GetDte(DteDeliveryDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SetSQL() + Filter + " Order By A.CtName, A.DocDt, A.DocNo, A.ItName;",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "LocalDocNo", "DocDt", "CtName", "SAAddress", "ItCode", 
                        
                        //6-10
                        "ItCodeInternal", "ItName", "Outstanding", "SalesUomCode", "QtypackagingUnit",

                        //11-15
                        "PackagingUnituomcode", "ShpStatus", "ContainerGroup", "ContainerGroupQty", "DeliveryDt",

                        //16-18
                        "DNo", "Remark", "ItName2"


                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                    }, true, false, false, false
                );

                DisplayWarningDeliveryDate();

                if (mIsShowUoMInfo)
                {
                    var l = new List<ItUoM>();
                    GetItemUoMInfo(ref l);
                    if (l.Count > 0)
                    {
                        ShowItemUoMInfo(ref l);
                    }
                    l.Clear();

                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 13 });
                    if (mInitColsCount != mAfterSetUomColCount)
                    {
                        //for (int c = 21; c < Grd1.Cols.Count; c++)
                        for (int c = (mAfterSetUomColCount - (mAfterSetUomColCount - mInitColsCount)); c < Grd1.Cols.Count; c++)
                            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { c });
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 7));

            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 18).Length != 0)
            {
                Sm.FormShowDialog(new FrmRptSO2Dlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 1), Sm.GetGrdStr(Grd1, e.RowIndex, 18)));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 7));

            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 18).Length != 0)
            {
                Sm.FormShowDialog(new FrmRptSO2Dlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 1), Sm.GetGrdStr(Grd1, e.RowIndex, 18)));
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void DisplayWarningDeliveryDate()
        {
            var lD = new List<DateDiff>();
            if (mNoOfDayForSOWarning.Length > 0)
            {
                if (Decimal.Parse(mNoOfDayForSOWarning) > 0)
                {
                    string mDocNo = string.Empty;

                    if (Grd1.Rows.Count > 0)
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0 && Sm.GetGrdStr(Grd1, Row, 20).Length > 0)
                            {                                
                                if (mDocNo.Length > 0) mDocNo += ",";
                                mDocNo += string.Concat(Sm.GetGrdStr(Grd1, Row, 1) , Sm.GetGrdStr(Grd1, Row, 20));
                            }
                        }
                    }

                    if (mDocNo.Length > 0)
                    {
                        var SQL = new StringBuilder();
                        var cm = new MySqlCommand();

                        SQL.AppendLine("Select A.DocNo, B.DNo, B.DeliveryDt, ");
                        SQL.AppendLine("DateDiff(IfNull(DATE_FORMAT(B.DeliveryDt, '%Y-%m-%d'), DATE_FORMAT(now(), '%Y-%m-%d')), DATE_FORMAT(now(), '%Y-%m-%d')) As Diff ");
                        SQL.AppendLine("From TblSOHdr A ");
                        SQL.AppendLine("Inner Join TblSODtl B On A.DocNo = B.DocNo ");
                        SQL.AppendLine("    And Find_In_Set(Concat(B.DocNo, B.DNo), @SODocNo); ");

                        using (var cn = new MySqlConnection(Gv.ConnectionString))
                        {
                            cn.Open();
                            cm.Connection = cn;
                            cm.CommandText = SQL.ToString();
                            Sm.CmParam<String>(ref cm, "@SODocNo", mDocNo);
                            var dr = cm.ExecuteReader();
                            var c = Sm.GetOrdinal(dr, new string[] 
                            {
                                //0
                                "DocNo",
                                //1-3
                                "DNo",
                                "DeliveryDt",
                                "Diff"
                            });
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    lD.Add(new DateDiff()
                                    {
                                        DocNo = Sm.DrStr(dr, c[0]),
                                        DNo = Sm.DrStr(dr, c[1]),
                                        DeliveryDt = Sm.DrStr(dr, c[2]),
                                        Diff = Sm.DrDec(dr, c[3])
                                    });
                                }
                            }
                            dr.Close();
                        }

                        if (lD.Count > 0)
                        {
                            if (Grd1.Rows.Count > 0)
                            {
                                for (int i = 0; i < Grd1.Rows.Count; i++)
                                {
                                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                                    {
                                        for (int j = 0; j < lD.Count; j++)
                                        {
                                            if (Sm.GetGrdStr(Grd1, i, 1) == lD[j].DocNo && Sm.GetGrdStr(Grd1, i, 20) == lD[j].DNo)
                                            {
                                                if (lD[j].DeliveryDt.Length > 0)
                                                {
                                                    if (lD[j].Diff <= Decimal.Parse(mNoOfDayForSOWarning))
                                                    {
                                                        Grd1.Rows[i].BackColor = Color.LightCoral;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void SetUoM()
        {
            int ColCount = Grd1.Cols.Count;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandTimeout = 600,
                    CommandText = "Select OptDesc From TblOption Where OptCat='OutstandingSOInventory' Order By OptDesc;"
                };
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OptDesc" });
                if (dr.HasRows)
                {
                    mIsShowUoMInfo = true;
                    while (dr.Read())
                    {
                        ColCount += 1;
                        Grd1.Cols.Count = ColCount;
                        Grd1.Cols[ColCount-1].Text = Sm.DrStr(dr, 0);
                        Grd1.Cols[ColCount-1].Width = 130;
                        Grd1.Cols[ColCount-1].CellStyle.ReadOnly = iGBool.True;
                        Grd1.Header.Cells[0, ColCount-1].TextAlign = iGContentAlignment.TopCenter;
                        Sm.GrdFormatDec(Grd1, new int[] { ColCount - 1 }, 2);
                    }
                }
                dr.Close();
            }
        }

        private void GetItemUoMInfo(ref List<ItUoM> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string ItCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    ItCode = Sm.GetGrdStr(Grd1, r, 7);
                    if (ItCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(ItCode=@ItCode" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@itCode" + r.ToString(), ItCode);
                    }
                }
            }

            if (Filter.Length==0)
                Filter = " Where 0=1 ";
            else
                Filter = " Where (" + Filter + ") ";

            SQL.AppendLine("Select ItCode, SalesUomCode, SalesUomCode2, SalesUomCodeConvert12, SalesUomCodeConvert21 ");
            SQL.AppendLine("From TblItem ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By ItCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "ItCode", "SalesUomCode", "SalesUomCode2", "SalesUomCodeConvert12", "SalesUomCodeConvert21" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ItUoM()
                        {
                            ItCode = Sm.DrStr(dr, c[0]),
                            UoM1 = Sm.DrStr(dr, c[1]),
                            UoM2 = Sm.DrStr(dr, c[2]),
                            Convert12 = Sm.DrDec(dr, c[3]),
                            Convert21 = Sm.DrDec(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }
        
        private void ShowItemUoMInfo(ref List<ItUoM> l)
        {
            if (Grd1.Rows.Count >= 1)
            {
                var ItCodeTemp = string.Empty;
                var UoMTemp = string.Empty;
                var UoMTitle = string.Empty;
                var QtyTemp = 0m;
                Grd1.BeginUpdate();
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    ItCodeTemp = Sm.GetGrdStr(Grd1, r, 7);
                    QtyTemp = Sm.GetGrdDec(Grd1, r, 11);
                    UoMTemp = Sm.GetGrdStr(Grd1, r, 12);
                    if (ItCodeTemp.Length != 0)
                    {
                        foreach (var i in l.Where(x => Sm.CompareStr(x.ItCode, ItCodeTemp))) 
                        {
                            for (int c = 23; c < Grd1.Cols.Count; c++)
                            {
                                Grd1.Cells[r, c].Value = 0m;
                                UoMTitle = Grd1.Cols[c].Text.ToString();
                                if (Sm.CompareStr(UoMTemp, UoMTitle))
                                    Grd1.Cells[r, c].Value = QtyTemp;
                                else
                                {
                                    if (Sm.CompareStr(i.UoM1, UoMTemp))
                                    {
                                        if (Sm.CompareStr(i.UoM2, UoMTitle))
                                            Grd1.Cells[r, c].Value = QtyTemp*i.Convert12;
                                    }
                                    if (Sm.CompareStr(i.UoM2, UoMTemp))
                                    {
                                        if (Sm.CompareStr(i.UoM1, UoMTitle))
                                            Grd1.Cells[r, c].Value = QtyTemp * i.Convert21;
                                    }
                                }
                            }
                        }
                    }
                }
                Grd1.EndUpdate();
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void chkDeliveryDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Delivery date");
        }

        private void DteDeliveryDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDeliveryDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }


        #endregion

        #region Class

        private class ItUoM
        {
            public string ItCode { get; set; }
            public string UoM1 { get; set; }
            public string UoM2 { get; set; }
            public decimal Convert12 { get; set; }
            public decimal Convert21 { get; set; }
        }

        private class NoOfCnt
        {
            public string SODocNo { get; set; }
            public string SODNo { get; set; }
        }

        private class DateDiff
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string DeliveryDt { get; set; }
            public decimal Diff { get; set; }
        }

        #endregion
       
    }
}
