﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;


#endregion

namespace RunSystem
{
    public partial class FrmEmpPD : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        public string DeptCodeOld = string.Empty, PosCodeOld = string.Empty, GrdLvlCodeOld = string.Empty;
        internal bool mIsNotFilterByAuthorization = false;
        internal FrmEmpPDFind FrmFind;

        #endregion

        #region Constructor

        public FrmEmpPD(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Mutation, Promotion, And Demotion";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            GetParameter();
            SetFormControl(mState.View);

            base.FrmLoad(sender, e);
            Sl.SetLueDeptCode(ref LueDeptCode);
            Sl.SetLuePosCode(ref LuePosCode);
            SetLueType(ref LueType);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueType, TxtEmpCode, TxtEmpName, TxtDeptName, TxtPosition, 
                        TxtGrade, LueDeptCode, LueGrdLvlCode, LuePosCode,  MeeRemark
                    }, true);
                    BtnEmpCode.Enabled = false;
                    BtnEmpCode2.Enabled = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueType, LueDeptCode, LueGrdLvlCode, LuePosCode, MeeRemark 
                    }, false);
                    BtnEmpCode.Enabled = true;
                    BtnEmpCode2.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:

                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
              TxtDocNo, LueType, DteDocDt, TxtEmpCode, TxtEmpName, TxtDeptName, TxtPosition, 
              TxtGrade, LueDeptCode, LueGrdLvlCode, LuePosCode,  MeeRemark
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpPDFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueGrdLvlCode(ref LueGrdLvlCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpPD", "TblEmpPD");
            string EmpCode = TxtEmpCode.Text;
            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpPD(DocNo));
            cml.Add(UpdateEmployee(EmpCode));
            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueType, "Type") || 
                Sm.IsDteEmpty(DteDocDt, "Document date") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee", false);
        }

        private MySqlCommand SaveEmpPD(string DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblEmpPD(DocNo, DocDt, JobTransfer, EmpCode, DeptCodeOld, PosCodeOld, GrdLvlCodeOld, DeptCode, PosCode, GrdLvlCode, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @JobTransfer, @EmpCode, @DeptCodeOld, @PosCodeOld, @GrdLvlCodeOld, @DeptCode, @PosCode, @GrdLvlCode, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@JobTransfer", Sm.GetLue(LueType));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@DeptCodeOld", DeptCodeOld);
            Sm.CmParam<String>(ref cm, "@PosCodeOld", PosCodeOld);
            Sm.CmParam<String>(ref cm, "@GrdLvlCodeOld", GrdLvlCodeOld);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetLue(LueGrdLvlCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateEmployee(string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmployee SET DeptCode=@DeptCode, PosCode=@PosCode, GrdLvlCode=@GrdLvlCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where EmpCode=@EmpCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetLue(LueGrdLvlCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowEmpPD(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpPD(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.JobTransfer, A.EmpCode, B.EmpName, ");
            SQL.AppendLine("C.DeptName As DeptNameOld, D.PosName As PosNameOld, E.GrdLvlName As GrdLvlNameOld, A.DeptCode, ");
            SQL.AppendLine("A.PosCode, A.GrdLvlCode, A.Remark ");
            SQL.AppendLine("From TblEmpPD A ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCodeOld = C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On A.PosCodeOld = D.PosCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On A.GrdLvlCodeOld = E.GrdLvlCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "JobTransfer", "EmpCode", "EmpName",  "DeptNameOld",  

                        //6-10
                        "PosNameOld",  "GrdLvlNameOld", "DeptCode", "PosCode", "GrdLvlCode", 

                        //11
                        "Remark"
                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);

                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    Sm.SetLue(LueType, Sm.DrStr(dr, c[2]));
                    TxtEmpCode.EditValue = Sm.DrStr(dr, c[3]);
                    TxtEmpName.EditValue = Sm.DrStr(dr, c[4]);
                    TxtDeptName.EditValue = Sm.DrStr(dr, c[5]);
                    TxtPosition.EditValue = Sm.DrStr(dr, c[6]);
                    TxtGrade.EditValue = Sm.DrStr(dr, c[7]);
                    Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[8]));
                    Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[9]));
                    SetLueGrdLvlCode(ref LueGrdLvlCode, Sm.DrStr(dr, c[10]));
                    Sm.SetLue(LueGrdLvlCode, Sm.DrStr(dr, c[10]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                }, true
        );

        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
        }

        private void SetLueType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From tblOption Where OptCat = 'EmpJobTransfer' ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueGrdLvlCode(ref DXE.LookUpEdit Lue, string GrdLvlCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.GrdLvlCode As Col1, T.GrdLvlName As Col2 ");
                SQL.AppendLine("From TblGradeLevelHdr T ");
                if (GrdLvlCode.Length != 0)
                    SQL.AppendLine("Where T.GrdLvlCode='" + GrdLvlCode + "' ");
                else
                {
                    if (!mIsNotFilterByAuthorization)
                    {
                        SQL.AppendLine("Where GrdLvlCode In ( ");
                        SQL.AppendLine("    Select T2.GrdLvlCode ");
                        SQL.AppendLine("    From TblPPAHdr T1 ");
                        SQL.AppendLine("    Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                        SQL.AppendLine("    Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                        SQL.AppendLine(") ");
                    }
                }
                SQL.AppendLine("Order By GrdLvlName; ");
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.SetLue2(
                    ref Lue, ref cm,
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue1(SetLueType));
        }

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmEmpPDDlg(this));
        }

        private void BtnEmpCode2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtEmpCode, "Employee", false))
            {
                var f = new FrmEmployee(this.mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = TxtEmpCode.Text;
                f.ShowDialog();
            }
        }

        private void LueGrdLvlCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGrdLvlCode, new Sm.RefreshLue2(SetLueGrdLvlCode), string.Empty);
        }

        #endregion
    }
}
