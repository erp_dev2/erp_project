﻿#region Update
/*
    24/09/2019 [DITA/VIR] reporting baru
    07/10/2019 [WED/VIR] tambah BOQ# dan SOC# di 1st quarter 1st semester
    10/10/2019 [DITA/VIR] reporting Semester 2
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthlyProject : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        internal bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmRptMonthlyProject(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySite ? "Y" : "N");
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocDt, A.DocNo, D.OptDesc AS ProjectScope, A.CtCode, B.CtName, ");
            SQL.AppendLine("C.SiteName, IfNull(E.BOQDocNoCount1st, 0) BOQDocNoCount1st, IfNull(E.BOQDocNo1st, '') BOQDocNo1st, ");
            SQL.AppendLine("IfNull(F.BOQDocNoCount2nd, 0) BOQDocNoCount2nd, G.BOQAmt1st, H.BOQAmt2nd, ");
            SQL.AppendLine("IfNull(I.SOCOntractDocNoCount1st, 0) SOCOntractDocNoCount1st, IfNull(I.SOContractDocNo1st, '') SOContractDocNo1st, ");
            SQL.AppendLine("IfNull(J.SOCOntractDocNoCount2nd, 0) SOCOntractDocNoCount2nd, ");
            SQL.AppendLine("K.SOCOntractAmt1st , L.SOCOntractAmt2nd, ");
            SQL.AppendLine("IfNull(E2.BOQDocNoCount1st2, 0) BOQDocNoCount1st2, IFNULL(E2.BOQDocNo1st2, '') BOQDocNo1st2,  ");
            SQL.AppendLine("IfNull(F2.BOQDocNoCount2nd2, 0) BOQDocNoCount2nd2, G2.BOQAmt1st2, H2.BOQAmt2nd2,  ");
            SQL.AppendLine("IfNull(I2.SOCOntractDocNoCount1st2, 0) SOCOntractDocNoCount1st2, IFNULL(I2.SOContractDocNo1st2, '') SOContractDocNo1st2, ");
            SQL.AppendLine("IfNull(J2.SOCOntractDocNoCount2nd2, 0) SOCOntractDocNoCount2nd2,  ");
            SQL.AppendLine("K2.SOCOntractAmt1st2 , L2.SOCOntractAmt2nd2   ");
            SQL.AppendLine("From TblLOPHdr A  ");

            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode And A.CancelInd = 'N' AND A.DocDt Between Concat(@Yr, '0101') And Concat(@Yr, '1231') ");
            SQL.AppendLine("Inner Join TblSite C On A.SiteCode = C.SiteCode  ");
            SQL.AppendLine("Inner Join TblOption D ON A.ProjectScope = D.OptCode AND D.OptCat = 'ProjectScope' ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("   Select X1.DocNo, COUNT(X2.DocNo) BOQDocNoCount1st, Group_Concat(Distinct X2.DocNo) BOQDocNo1st ");
            SQL.AppendLine("    From TblLOPHdr X1  ");
            SQL.AppendLine("    Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo  ");
            SQL.AppendLine("    Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '0101') And Concat(@Yr, '0331')) And X2.ActInd = 'Y'  ");
            SQL.AppendLine("    GROUP BY X1.DocNo  ");
            SQL.AppendLine(") E On A.DocNo = E.DocNo  ");

            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("   Select X1.DocNo, COUNT(X2.DocNo) BOQDocNoCount2nd  ");
            SQL.AppendLine("  From TblLOPHdr X1  ");
            SQL.AppendLine("  Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo  ");
            SQL.AppendLine("   Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '0401') And Concat(@Yr, '0630')) And X2.ActInd = 'Y'  ");
            SQL.AppendLine("   GROUP BY X1.DocNo  ");
            SQL.AppendLine(") F On A.DocNo = F.DocNo  ");

            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("   Select X1.DocNo, SUM(X2.GrandTotal) BOQAmt1st ");
            SQL.AppendLine("  From TblLOPHdr X1  ");
            SQL.AppendLine("   Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo  ");
            SQL.AppendLine("    Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '0101') And Concat(@Yr, '0331')) And X2.ActInd = 'Y'  ");
            SQL.AppendLine("    GROUP BY X1.DocNo  ");
            SQL.AppendLine(") G On A.DocNo = G.DocNo  ");

            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    Select X1.DocNo, SUM(X2.GrandTotal) BOQAmt2nd ");
            SQL.AppendLine("    From TblLOPHdr X1  ");
            SQL.AppendLine("    Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo  ");
            SQL.AppendLine("    Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '0401') And Concat(@Yr, '0630')) And X2.ActInd = 'Y'  ");
            SQL.AppendLine("    GROUP BY X1.DocNo  ");
            SQL.AppendLine(") H On A.DocNo = H.DocNo  ");

            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("   Select X1.DocNo, COUNT(X3.DocNo) SOCOntractDocNoCount1st, Group_Concat(Distinct X3.DocNo) SOContractDocNo1st ");
            SQL.AppendLine("   From TblLOPHdr X1  ");
            SQL.AppendLine("    Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo  ");
            SQL.AppendLine("    Inner Join TblSOCOntractHdr X3 On X2.DocNo = X3.BOQDocNo  ");
            SQL.AppendLine("   Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '0101') And Concat(@Yr, '0331')) And X2.ActInd = 'Y' And X3.CancelInd = 'N' "); 
            SQL.AppendLine("   GROUP BY X1.DocNo  ");
            SQL.AppendLine(") I On A.DocNo = I.DocNo  ");

            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    Select X1.DocNo, COUNT(X3.DocNo) SOCOntractDocNoCount2nd  ");
            SQL.AppendLine("    From TblLOPHdr X1  ");
            SQL.AppendLine("    Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo  ");
            SQL.AppendLine("   Inner Join TblSOCOntractHdr X3 On X2.DocNo = X3.BOQDocNo  ");
            SQL.AppendLine("   Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '0401') And Concat(@Yr, '0630')) And X2.ActInd = 'Y' And X3.CancelInd = 'N'  ");
            SQL.AppendLine("   GROUP BY X1.DocNo  ");
            SQL.AppendLine(") J On A.DocNo = J.DocNo  ");

            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    Select X1.DocNo, SUM(X3.Amt) SOCOntractAmt1st ");
            SQL.AppendLine("   From TblLOPHdr X1  ");
            SQL.AppendLine("  Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo  ");
            SQL.AppendLine("  Inner Join TblSOCOntractHdr X3 On X2.DocNo = X3.BOQDocNo  ");
            SQL.AppendLine("  Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '0101') And Concat(@Yr, '0331')) And X2.ActInd = 'Y' And X3.CancelInd = 'N'  ");
            SQL.AppendLine("  GROUP BY X1.DocNo  ");
            SQL.AppendLine(") K On A.DocNo = K.DocNo  ");

            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("   Select X1.DocNo, SUM(X3.Amt) SOCOntractAmt2nd ");
            SQL.AppendLine("   From TblLOPHdr X1  ");
            SQL.AppendLine("  Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo  ");
            SQL.AppendLine("  Inner Join TblSOCOntractHdr X3 On X2.DocNo = X3.BOQDocNo  ");
            SQL.AppendLine("  Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '0401') And Concat(@Yr, '0630')) And X2.ActInd = 'Y' And X3.CancelInd = 'N'  ");
            SQL.AppendLine("  GROUP BY X1.DocNo  ");
            SQL.AppendLine(") L On A.DocNo = L.DocNo  ");

            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("( ");
            SQL.AppendLine("       Select X1.DocNo, COUNT(X2.DocNo) BOQDocNoCount1st2, Group_Concat(Distinct X2.DocNo) BOQDocNo1st2  ");
            SQL.AppendLine("       From TblLOPHdr X1   ");
            SQL.AppendLine("Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo   ");
            SQL.AppendLine("Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '0701') And Concat(@Yr, '0930')) And X2.ActInd = 'Y'   ");
            SQL.AppendLine("GROUP BY X1.DocNo ");  
            SQL.AppendLine(") E2 On A.DocNo = E2.DocNo   ");

            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("(   ");
            SQL.AppendLine("Select X1.DocNo, COUNT(X2.DocNo) BOQDocNoCount2nd2  "); 
            SQL.AppendLine("From TblLOPHdr X1 ");
            SQL.AppendLine("Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo ");
            SQL.AppendLine("Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '1001') And Concat(@Yr, '1231')) And X2.ActInd = 'Y'   ");
            SQL.AppendLine("GROUP BY X1.DocNo ");
            SQL.AppendLine(") F2 On A.DocNo = F2.DocNo ");

            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("(   ");
            SQL.AppendLine("Select X1.DocNo, SUM(X2.GrandTotal) BOQAmt1st2  ");
            SQL.AppendLine("From TblLOPHdr X1 ");
            SQL.AppendLine("Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo ");
            SQL.AppendLine("Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '0701') And Concat(@Yr, '0930')) And X2.ActInd = 'Y'   ");
            SQL.AppendLine("GROUP BY X1.DocNo ");
            SQL.AppendLine("    ) G2 On A.DocNo = G2.DocNo ");

            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select X1.DocNo, SUM(X2.GrandTotal) BOQAmt2nd2 ");
            SQL.AppendLine("From TblLOPHdr X1 ");
            SQL.AppendLine("Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo ");
            SQL.AppendLine("Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '1001') And Concat(@Yr, '1231')) And X2.ActInd = 'Y'  "); 
            SQL.AppendLine("GROUP BY X1.DocNo ");
            SQL.AppendLine(") H2 On A.DocNo = H2.DocNo ");

            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select X1.DocNo, COUNT(X3.DocNo) SOCOntractDocNoCount1st2, Group_Concat(Distinct X3.DocNo) SOContractDocNo1st2  ");
            SQL.AppendLine("From TblLOPHdr X1 ");
            SQL.AppendLine("Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo ");
            SQL.AppendLine("Inner Join TblSOCOntractHdr X3 On X2.DocNo = X3.BOQDocNo ");
            SQL.AppendLine("Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '0701') And Concat(@Yr, '0930')) And X2.ActInd = 'Y' And X3.CancelInd = 'N'  "); 
            SQL.AppendLine("GROUP BY X1.DocNo ");
            SQL.AppendLine(") I2 On A.DocNo = I2.DocNo ");

            SQL.AppendLine("Left Join   ");
            SQL.AppendLine(" ( ");
            SQL.AppendLine("Select X1.DocNo, COUNT(X3.DocNo) SOCOntractDocNoCount2nd2 ");
            SQL.AppendLine("From TblLOPHdr X1 ");
            SQL.AppendLine("Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo ");
            SQL.AppendLine("Inner Join TblSOCOntractHdr X3 On X2.DocNo = X3.BOQDocNo ");
            SQL.AppendLine("Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '1001') And Concat(@Yr, '1231')) And X2.ActInd = 'Y' And X3.CancelInd = 'N'  "); 
            SQL.AppendLine("GROUP BY X1.DocNo ");
            SQL.AppendLine(") J2 On A.DocNo = J2.DocNo ");

            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select X1.DocNo, SUM(X3.Amt) SOCOntractAmt1st2  ");
            SQL.AppendLine("From TblLOPHdr X1 ");
            SQL.AppendLine("Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo ");
            SQL.AppendLine("Inner Join TblSOCOntractHdr X3 On X2.DocNo = X3.BOQDocNo ");
            SQL.AppendLine("Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '0701') And Concat(@Yr, '0930')) And X2.ActInd = 'Y' And X3.CancelInd = 'N'   ");
            SQL.AppendLine("GROUP BY X1.DocNo ");
            SQL.AppendLine(") K2 On A.DocNo = K2.DocNo ");

            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select X1.DocNo, SUM(X3.Amt) SOCOntractAmt2nd2  ");
            SQL.AppendLine("From TblLOPHdr X1 ");
            SQL.AppendLine("Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo ");
            SQL.AppendLine("Inner Join TblSOCOntractHdr X3 On X2.DocNo = X3.BOQDocNo ");
            SQL.AppendLine("Where X1.CancelInd = 'N' And (X1.DocDt Between Concat(@Yr, '1001') And Concat(@Yr, '1231')) And X2.ActInd = 'Y' And X3.CancelInd = 'N'   ");
            SQL.AppendLine("GROUP BY X1.DocNo ");
            SQL.AppendLine(") L2 On A.DocNo = L2.DocNo ");

            if (mIsFilterBySite)
            {
                SQL.AppendLine("Where (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 2;

            Grd1.Cols[0].Width = 50;
            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 150;
            Grd1.Header.Cells[0, 1].Value = "Document#";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 250;
            Grd1.Header.Cells[0, 2].Value = "Scope of Work";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Cols[3].Width = 150;
            Grd1.Header.Cells[0, 3].Value = "Site";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Header.Cells[1, 4].Value = "BOQ 1st Semester";
            Grd1.Header.Cells[1, 4].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 4].SpanCols = 5;
            Grd1.Header.Cells[0, 4].Value = "1st Quarter";
            Grd1.Header.Cells[0, 5].Value = "2nd Quarter";
            Grd1.Header.Cells[0, 6].Value = "1st Quarter" + Environment.NewLine + "Amount";
            Grd1.Header.Cells[0, 7].Value = "2nd Quarter" + Environment.NewLine + "Amount";
            Grd1.Header.Cells[0, 8].Value = "1st Quarter" + Environment.NewLine + "Document#";
            Grd1.Cols[4].Width = 90;
            Grd1.Cols[5].Width = 90;
            Grd1.Cols[6].Width = 150;
            Grd1.Cols[7].Width = 150;
            Grd1.Cols[8].Width = 150;

            Grd1.Header.Cells[1, 9].Value = "SO Contract 1st Semester";
            Grd1.Header.Cells[1, 9].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 9].SpanCols = 5;
            Grd1.Header.Cells[0, 9].Value = "1st Quarter";
            Grd1.Header.Cells[0, 10].Value = "2nd Quarter";
            Grd1.Header.Cells[0, 11].Value = "1st Quarter" + Environment.NewLine + "Amount";
            Grd1.Header.Cells[0, 12].Value = "2nd Quarter" + Environment.NewLine + "Amount";
            Grd1.Header.Cells[0, 13].Value = "1st Quarter" + Environment.NewLine + "Document#";
            Grd1.Cols[9].Width = 90;
            Grd1.Cols[10].Width = 90;
            Grd1.Cols[11].Width = 150;
            Grd1.Cols[12].Width = 150;
            Grd1.Cols[13].Width = 150;

            Grd1.Header.Cells[1, 14].Value = "BOQ 2nd Semester";
            Grd1.Header.Cells[1, 14].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 14].SpanCols = 5;
            Grd1.Header.Cells[0, 14].Value = "1st Quarter";
            Grd1.Header.Cells[0, 15].Value = "2nd Quarter";
            Grd1.Header.Cells[0, 16].Value = "1st Quarter" + Environment.NewLine + "Amount";
            Grd1.Header.Cells[0, 17].Value = "2nd Quarter" + Environment.NewLine + "Amount";
            Grd1.Header.Cells[0, 18].Value = "1st Quarter" + Environment.NewLine + "Document#";
            Grd1.Cols[14].Width = 90;
            Grd1.Cols[15].Width = 90;
            Grd1.Cols[16].Width = 150;
            Grd1.Cols[17].Width = 150;
            Grd1.Cols[18].Width = 150;

            Grd1.Header.Cells[1, 19].Value = "SO Contract 2nd Semester";
            Grd1.Header.Cells[1, 19].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 19].SpanCols = 5;
            Grd1.Header.Cells[0, 19].Value = "1st Quarter";
            Grd1.Header.Cells[0, 20].Value = "2nd Quarter";
            Grd1.Header.Cells[0, 21].Value = "1st Quarter" + Environment.NewLine + "Amount";
            Grd1.Header.Cells[0, 22].Value = "2nd Quarter" + Environment.NewLine + "Amount";
            Grd1.Header.Cells[0, 23].Value = "1st Quarter" + Environment.NewLine + "Document#";
            Grd1.Cols[19].Width = 90;
            Grd1.Cols[20].Width = 90;
            Grd1.Cols[21].Width = 150;
            Grd1.Cols[22].Width = 150;
            Grd1.Cols[23].Width = 150;

            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 11, 12, 16, 17, 21, 22 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 8, 13, 18, 23 });
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 13, 18, 23 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;

            try
            {
                Sm.ClearGrd(Grd1, false);
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.CmParam(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, new string[] { "A.ProjectName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + " Order By A.DocNo; ",
                    new string[]
                    {
                        //0
                        "DocNo", 
                        //1-5
                        "ProjectScope", "SiteName", "BOQDocNoCount1st", "BOQDocNoCount2nd", "BOQAmt1st", 
                        //6-10
                        "BOQAmt2nd", "BOQDocNo1st", "SOCOntractDocNoCount1st", "SOCOntractDocNoCount2nd", "SOCOntractAmt1st", 
                        //11-15
                        "SOCOntractAmt2nd", "SOContractDocNo1st", "BOQDocNoCount1st2", "BOQDocNoCount2nd2", "BOQAmt1st2", 
                        //16-20
                        "BOQAmt2nd2", "BOQDocNo1st2", "SOCOntractDocNoCount1st2", "SOCOntractDocNoCount2nd2", "SOCOntractAmt1st2", 
                        //21-22
                        "SOCOntractAmt2nd2", "SOContractDocNo1st2"
                       
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                       
                       
                    }, true, false, false, false
                );

                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 6, 7, 9, 10, 11, 12, 14, 15, 16, 17, 19, 20, 21, 22 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }
        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Name");
        }
        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }
        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion
    }
}
