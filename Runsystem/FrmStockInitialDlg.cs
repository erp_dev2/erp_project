﻿#region Update
/*
    07/08/2017 [WED] tambah kolom foreign name berdasarkan parameter IsShowForeignName
    07/01/2020 [VIN/SIER] Parameter baru (mIsFilterByItCt) untuk Item Category per masing-masing Group
    23/03/2020 [TKG/IMS] berdasarkan parameter IsInvTrnShowItSpec, menampilkan specifikasi item
    10/01/2020 [TKG/IMS] tambah local code
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmStockInitialDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmStockInitial mFrmParent;
        private string mSQL = string.Empty;
        internal bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmStockInitialDlg(FrmStockInitial FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sl.SetLueItCtCode(ref LueItCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "Item's Name", 
                        "Local Code",
                        "Foreign Name",

                        //6-10
                        "Category",
                        "UoM",
                        "UoM",
                        "UoM",
                        "Group",

                        //11
                        "Specification"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 250, 130, 150, 
                        
                        //6-10
                        200, 0, 0, 0, 100,
  
                        //11
                        200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11 }, false);
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[10].Visible = true;
                Grd1.Cols[10].Move(6);
            }
            if (mFrmParent.mIsInvTrnShowItSpec)
            {
                Grd1.Cols[11].Visible = true;
                Grd1.Cols[11].Move(7);
            }
            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.ItCode, A.ItName, A.ItCodeInternal, A.ForeignName, B.ItCtName, A.InventoryUomCode, A.InventoryUomCode2, A.InventoryUomCode3, A.ItGrpCode, A.Specification ");
                SQL.AppendLine("From TblItem A  ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode  ");
                SQL.AppendLine("Where A.ActInd='Y'  ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                
                mSQL = SQL.ToString();

                var cm = new MySqlCommand();
                string 
                    Filter = " And 1=1 ", 
                    Filter2 = string.Empty;
                if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                if (ChkUnselectedItem.Checked)
                {
                    if (mFrmParent.Grd1.Rows.Count >= 1)
                    {
                        var No = "0001";
                        for (int Row = 0; Row < mFrmParent.Grd1.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(mFrmParent.Grd1, Row, 4).Length != 0)
                            {
                                if (Filter2.Length > 0) Filter2 += " And ";
                                Filter2 += "(A.ItCode<>@ItCode" + No.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@ItCode" + No.ToString(), Sm.GetGrdStr(mFrmParent.Grd1, Row, 4));
                                No = ("000" + (int.Parse(No) + 1).ToString()).ToString();
                            }
                        }
                    }
                    if (Filter2.Length != 0) Filter += " And (" + Filter2 + ")";
                }

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName", "A.ItCodeInternal", "A.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, 
                        mSQL + 
                        Filter + " Order By A.ItName;",
                        
                        new string[] 
                        { 
                            //0
                            "ItCode", 

                            //1-5
                            "ItName", "ItCodeInternal", "ForeignName", "ItCtName", "InventoryUomCode", 
                            
                            //6-9
                            "InventoryUomCode2", "InventoryUomCode3", "ItGrpCode", "Specification"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 11);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 11, 13, 15 });
                        
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 11, 13, 15, 17 });
                    }
                }
                mFrmParent.ComputeTotalQty();
                mFrmParent.Grd1.EndUpdate();
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 2));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 2));
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0 && Sm.GetGrdStr(Grd1, 0, 2).Length>0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for(int Row = 0;Row<Grd1.Rows.Count;Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method
        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");

        }
        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion

    }
}
