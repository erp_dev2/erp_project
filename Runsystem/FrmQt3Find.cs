﻿#region Update
/*
    08/04/2020 [WED/SRN] new apps, Qt pakai discount rate 5 biji
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmQt3Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmQt3 mFrmParent;
        private string mSQL = string.Empty;
        internal bool
           mIsFilterByItCt = false;
        #endregion

        #region Constructor

        public FrmQt3Find(FrmQt3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mFrmParent.mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLuePtCode(ref LuePtCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.VdName, D.PtName, B.ItCode, E.ItName, E.ForeignName, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("B.ActInd, E.PurchaseUomCode, A.CurCode, B.UPrice, B.Remark As DRemark, A.Remark As HRemark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, E.ItCodeInternal, E.Specification ");
            SQL.AppendLine("From TblQtHdr A ");
            SQL.AppendLine("Inner Join TblQtDtl B On A.DocNo=B.DocNo And A.DocType = 2 ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("Left Join TblPaymentTerm D On A.PtCode=D.PtCode ");
            SQL.AppendLine("Inner Join TblItem E On B.ItCode=E.ItCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("And A.PtCode Is Not Null ");
                SQL.AppendLine("And A.PtCode In (");
                SQL.AppendLine("    Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=E.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Vendor",
                        "Term of"+Environment.NewLine+"Payment",
                        "Item's"+Environment.NewLine+"Code",
                        
                        //6-10
                        "Item's Name",
                        "Foreign Name",
                        "Status",
                        "Active",
                        "UoM",
                        
                        //11-15
                        "Currency",
                        "Price",
                        "Item's Remark",
                        "Document's Remark",
                        "Created"+Environment.NewLine+"By",
                        
                        //15-20
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time",

                        //21-22
                        "Item's Code"+Environment.NewLine+"Internal",
                        "Item's Specification"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        150, 80, 200, 200, 100,
                        
                        //6-10
                        200, 100, 100, 80, 100,
                        
                        //11-15
                        100, 100, 250, 250, 100, 

                        //16-20
                        100, 100, 100, 100, 100,

                        //21-22
                        180, 300
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 16, 19 });
            Sm.GrdFormatTime(Grd1, new int[] { 17, 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 15, 16, 17, 18, 19, 20, 21 }, false);
            if (!mFrmParent.mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 7 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 22 });
            Grd1.Cols[21].Move(14);
            Grd1.Cols[22].Move(15);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 15, 16, 17, 18, 19, 20, 21 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePtCode), "A.PtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "E.ItName", "E.ForeignName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "VdName", "PtName", "ItCode", "ItName", 
                            
                            //6-10
                            "ForeignName", "StatusDesc", "ActInd", "PurchaseUomCode", "CurCode", 
                            
                            //11-15
                            "UPrice", "DRemark", "HRemark", "CreateBy", "CreateDt", 
                            
                            //16-19
                            "LastUpBy", "LastUpDt", "ItCodeInternal", "Specification"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 19);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }
        #region Additional Method
        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }
        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Methods

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue2(Sl.SetLuePtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Term of payment");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
