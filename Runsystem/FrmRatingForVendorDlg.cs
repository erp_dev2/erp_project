﻿#region Update
/*
    19/12/2022 [TYO/MNET] New apps
    03/01/2022 [ TYO/MNET] modifikasi query SetSQL

*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRatingForVendorDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRatingForVendor mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRatingForVendorDlg(FrmRatingForVendor FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
 
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = ""+ mFrmParent.mMenuCode + "-List of PO";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -360);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Document#",
                    "Date",
                    "Outstanding"+Environment.NewLine+"Quantity",
                    "PO's Remark",
                }
            );


            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 4 });
           
            Sm.SetGrdProperty(Grd1 , true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            var VdCode = Sm.GetLue(mFrmParent.LueVendor);

            SQL.AppendLine("SELECT * FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT A.DocNo, A.DocDt,  ");
            if(mFrmParent.mIsRecvVdUsePORevision)
                SQL.AppendLine("    (IFNULL(E.Qty,B.Qty) - IFNULL(D.Qty,0)) AS OutstandingQty,  ");
            else
                SQL.AppendLine("    (B.Qty - D.Qty) AS OutstandingQty,  ");
            SQL.AppendLine("    A.Remark PORemark ");
            SQL.AppendLine("    FROM tblpohdr A ");
            SQL.AppendLine("    INNER JOIN tblpodtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN tblporequestdtl C ON B.PORequestDocNo = C.DocNo ");
            SQL.AppendLine("    LEFT JOIN tblrecvvddtl D ON B.DocNo = D.PODocNo ");
            SQL.AppendLine("    LEFT JOIN  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT A.DocNo AS PODoc, A.DNo AS PODNo, B.DocNo AS POREV, B.Qty, B.`Status`  ");
            SQL.AppendLine("        FROM tblpodtl A  ");
            SQL.AppendLine("        INNER JOIN tblporevision B ON A.DocNo = B.PODocNo AND A.DNo = B.PODNo AND B.status = 'A'  ");
            SQL.AppendLine("        WHERE B.DocNo = (SELECT MAX(docno) FROM tblporevision WHERE podocno = B.podocno AND status = 'A') ");
            SQL.AppendLine("    ) E ON B.DocNo = E.PODoc AND B.DNo = E.PODNo ");
            SQL.AppendLine("    WHERE A.`Status` != 'C' ");
            SQL.AppendLine("    AND A.DocNo NOT IN  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT PODocNo FROM TblRatingForVendorHdr ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And A.VdCode = '"+VdCode+"'");
            SQL.AppendLine(") X ");
            SQL.AppendLine("WHERE X.OutstandingQty = 0 ");
            SQL.AppendLine("Group By X.DocNo ");
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "X.DocNo" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By X.DocDt, X.DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo",
                        
                        //1-3
                        "DocDt", "OutstandingQty", "PORemark",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    }, true, false, false, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 =  0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;
                        Row1 = Row;

                        mFrmParent.TxtPODocNo.EditValue = Sm.GetGrdStr(Grd1, Row1, 2 );
                        mFrmParent.MeeRemark.EditValue = Sm.GetGrdStr(Grd1, Row1, 5);
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
            this.Close();

        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            //if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            //{
            //    e.DoDefault = false;
            //    var f = new FrmItem(mFrmParent.mMenuCode);
            //    f.Tag = mFrmParent.mMenuCode;
            //    f.WindowState = FormWindowState.Normal;
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
            //    f.ShowDialog();
            //}
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            //if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            //{
            //    var f = new FrmItem(mFrmParent.mMenuCode);
            //    f.Tag = mFrmParent.mMenuCode;
            //    f.WindowState = FormWindowState.Normal;
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
            //    f.ShowDialog();
            //}
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            //if (Grd1.Rows.Count > 0)
            //{
            //    bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
            //    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //        Grd1.Cells[Row, 1].Value = !IsSelected;
            //}
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }


        #endregion

        #endregion
    }
}
