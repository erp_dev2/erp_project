﻿#region Update
/*
    [ICA/PHT] new apps
    20/06/2021 [TKG/PHT]
        profit center dan cost center divalidasi berdasarkan otorisasi group terhadap cost center,
        cost center yg dimunculkan adalah cost center yang tidak menjadi parent di cost center yg lain.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucher mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherDlg3(FrmVoucher FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = "List Of Budget Category";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BCCode, A.BCName ");
            SQL.AppendLine("From TblBudgetCategory A ");
            SQL.AppendLine("Inner Join TblCostCenter B On A.CCCode=B.CCCode ");
            SQL.AppendLine("    And B.NotParentInd='Y' ");
            SQL.AppendLine("    And B.ActInd='Y' ");
            SQL.AppendLine("    And B.ProfitCenterCode Is Not Null ");
            SQL.AppendLine("Inner Join TblGroupProfitCenter C On B.ProfitCenterCode=C.ProfitCenterCode ");
            SQL.AppendLine("Inner Join TblUser D On C.GrpCode=D.GrpCode And D.UserCode=@UserCode ");
            SQL.AppendLine("Where A.ActInd='Y' ");
            SQL.AppendLine("And A.CCCode Is Not Null ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                         //0
                        "No.",

                        //1-2
                        "Code", 
                        "Budget Category" 
                    }, new int[]
                    {
                        //0
                        50, 

                        //1-2
                        150, 600
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtBCCode.Text, new string[] { "BCCode", "BCName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By BCName;",
                        new string[]{ "BCCode", "BCName" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                int r = Grd1.CurRow.Index;
                mFrmParent.TxtBCCode.Text = Sm.GetGrdStr(Grd1, r, 2);
                mFrmParent.mBCCode = Sm.GetGrdStr(Grd1, r, 1);
                this.Close();
            }

        }

        private void Grd1_CellDoubleClick_1(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #region Event

        private void TxtBCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Budget category");
        }

        #endregion

    }
}
