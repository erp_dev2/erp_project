﻿#region Update
/*
 * 03/08/2017 [WED] Tambah filter dan kolom Location From dan Location To
 * 18/08/2022 [TYO/PRODUCT] filter cost center berdasarkan parameter IsFilterByCC
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmAssetTransferRequestFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmAssetTransferRequest mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmAssetTransferRequestFind(FrmAssetTransferRequest FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
            Sl.SetLueCCCode(ref LueCCCode, mFrmParent.mIsFilterByCC ? "Y" : "N");
            Sl.SetLueCCCode(ref LueCCCode2);
            Sl.SetLueLocCode(ref LueLocCode);
            Sl.SetLueLocCode(ref LueLocCode2);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select  A.DocNo, A.DocDt, F.DocNo As AssetTransferDocNo, C.CCName As CCNameFrom, D.CCName As CCNameTo, A.CancelInd, B.AssetCode, E.AssetName, E.DisplayName, B.Remark, ");
            SQL.AppendLine("G.LocName As LocNameFrom, H.LocName As LocNameTo, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblAssetTransferRequestHdr A ");
            SQL.AppendLine("Inner Join tblAssetTransferRequestDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblCostCenter C On A.CCCodeFrom  = C.CCCode ");
            SQL.AppendLine("Inner Join TblCostCenter D On A.CCCodeTo = D.CCCode  ");
            SQL.AppendLine("Inner Join Tblasset E On B.AssetCode = E.AssetCode ");
            SQL.AppendLine("Left Join TblAssetTransfer F On F.AssetTransferRequestDocNo = A.DocNo");
            SQL.AppendLine("Left Join TblLocation G On A.LocCodeFrom = G.LocCode ");
            SQL.AppendLine("Left Join TblLocation H On A.LocCodeTo = H.LocCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Cancel",
                    "Date",
                    "Asset Transfer#",
                    "Cost Center From",

                    //6-10
                    "Cost Center To",
                    "Location From",
                    "Location To",
                    "Asset Code",
                    "",

                    //11-15
                    "Asset Name",
                    "Display Name",
                    "Remark",
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date", 
                    
                    //16-19
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time"
                },
                new int[]
                {
                    //0
                    30,

                    //1-5
                    120, 60, 80, 120, 200, 

                    //6-10
                    200, 200, 200, 100, 20, 

                    //11-15
                    120, 200, 200, 100, 100, 

                    //16-19
                    100, 100, 100, 100
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 10 });
            Sm.GrdColCheck(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3, 15, 18 });
            Sm.GrdFormatTime(Grd1, new int[] { 16, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 14, 15, 16, 17, 18, 19 }, !ChkHideInfoInGrd.Checked);
            if (!mFrmParent.mIsAssetTransferUseLocation)
                Sm.GrdColInvisible(Grd1, new int[] { 7, 8 } );
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 13, 14, 15, 16, 17, 18, 19 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 14, 15, 16, 17, 18, 19 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "A.CCCodeFrom", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode2), "A.CCCodeTo", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode), "A.LocCodeFrom", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode2), "A.LocCodeTo", true);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "B.AssetCode", "E.AssetName", "E.DisplayName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo, B.DNo",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "CancelInd", "DocDt", "AssetTransferDocNo", "CCNameFrom", "CCnameTo",   
                            
                            //6-10
                            "LocNameFrom", "LocNameTo", "AssetCode", "AssetName", "DisplayName", 
                            
                            //11-15
                            "Remark", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);

                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 10);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 14);

                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 19, 15);
                        }, true, true, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 1, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmAsset(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmAsset(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center From");
        }

        private void ChkCCCode2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center To");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(Sl.SetLueCCCode), mFrmParent.mIsFilterByCC ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueCCCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode2, new Sm.RefreshLue1(Sl.SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueLocCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocCode, new Sm.RefreshLue1(Sl.SetLueLocCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueLocCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocCode2, new Sm.RefreshLue1(Sl.SetLueLocCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkLocCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location From");
        }

        private void ChkLocCode2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location To");
        }

        #endregion

        #endregion

    }
}
