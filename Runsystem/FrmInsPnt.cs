﻿#region Update
/*
    03/06/2020 [WED/SRN] tambah cost center group
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmInsPnt : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mInspntCode = "";
        internal FrmInsPntFind FrmFind;
       

        #endregion

        #region Constructor

        public FrmInsPnt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Insentif/Penalty";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);

            base.FrmLoad(sender, e);
            SetLueInspntCtCode(ref LueInspntCtCode);
            Sl.SetLueOption(ref LueCostCenterGroup, "CostCenterGroup");
            //if this application is called from other application
            if (mInspntCode.Length != 0)
            {
                ShowData(mInspntCode);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
           if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtInspntCode, ChkActInd, TxtInspntName, TxtAmt, LueInspntCtCode, LueCostCenterGroup
                    }, true);
                    TxtInspntCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtInspntCode, TxtInspntName, TxtAmt, LueInspntCtCode, LueCostCenterGroup
                    }, false);
                    TxtInspntCode.Focus();
                    break;
                case mState.Edit:
                    ChkActInd.Properties.ReadOnly = false;
                    TxtInspntCode.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtInspntCode, TxtInspntName,  LueInspntCtCode, LueCostCenterGroup
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
               TxtAmt
            }, 0); 
            ChkActInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 2);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmInsPntFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtInspntCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into Tblinspnt(InspntCode, InspntName, ActInd, Amt, InspntCtCode, CCGrpCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@InspntCode, @InspntName, @ActInd, @Amt, @InspntCtCode, @CCGrpCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("Update InspntName=@InspntName, ActInd=@ActInd, Amt=@Amt, InspntCtCode=@InspntCtCode, CCGrpCode = @CCGrpCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@InspntCode", TxtInspntCode.Text);
                Sm.CmParam<String>(ref cm, "@InspntName", TxtInspntName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
                Sm.CmParam<String>(ref cm, "@InspntCtCode", Sm.GetLue(LueInspntCtCode));
                Sm.CmParam<String>(ref cm, "@CCGrpCode", Sm.GetLue(LueCostCenterGroup));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtInspntCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string InspntCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@InspntCode", InspntCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select InspntCode, InspntName, ActInd, Amt, InspntCtCode, CCGrpCode From Tblinspnt Where InspntCode=@InspntCode ",
                        new string[] 
                        {
                            "InspntCode", "InspntName", "ActInd", "Amt", "InspntCtCode", "CCGrpCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtInspntCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtInspntName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                            TxtAmt.EditValue = Sm.DrDec(dr, c[3]);
                            Sm.SetLue(LueInspntCtCode, Sm.DrStr(dr, c[4]));
                            Sm.SetLue(LueCostCenterGroup, Sm.DrStr(dr, c[5]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtInspntCode, "Insentif/penalty code", false) ||
                Sm.IsTxtEmpty(TxtInspntName, "Insentif/penalty name", false) ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsLueEmpty(LueInspntCtCode, "Insentif/penalty Category")||
                IsCityCodeExisted()||
                IsDataInactiveAlready();
        }

        private bool IsCityCodeExisted()
        {
            if (!TxtInspntCode.Properties.ReadOnly && Sm.IsDataExist("Select InspntCode From Tblinspnt Where InspntCode='" + TxtInspntCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Insentif/penalty code ( " + TxtInspntCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsDataInactiveAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select InspntCode From Tblinspnt " +
                    "Where ActInd='N' And InspntCode=@InspntCode;"
            };
            Sm.CmParam<String>(ref cm, "@InspntCode", TxtInspntCode.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already not actived.");
                return true;
            }
            return false;
        }

        #endregion

        #region Additional Method

        private void SetLueInspntCtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select InspntCtCode As Col1, InspntCtName As Col2 From TblInspntCategory",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void TxtInspntCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtInspntCode);
        }
        private void TxtInspntName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtInspntName);
        }

        private void TxtAmt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void LueInspntCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueInspntCtCode, new Sm.RefreshLue1(SetLueInspntCtCode));
        }

        private void LueCostCenterGroup_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCostCenterGroup, new Sm.RefreshLue2(Sl.SetLueOption), "CostCenterGroup");
        }

        #endregion

    }
}
