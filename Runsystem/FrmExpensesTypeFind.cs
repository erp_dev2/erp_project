﻿#region Update
/*
 * 14/04/2022 [RDA/PRODUCT] new find
 * 27/05/2022 [RDA/PRODUCT] penyesuaian find sesuai requirement terbaru
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmExpensesTypeFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmExpensesType mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmExpensesTypeFind(FrmExpensesType FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetSQL();
            SetGrd();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.ExpensesCode, A.DocDt, A.ActInd, A.TransactionCode, B.OptDesc AS TransactionName, A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("FROM tblexpensestypehdr A ");
            SQL.AppendLine("INNER JOIN tbloption B ON A.TransactionCode = B.OptCode AND B.OptCat = 'InvestmentTransaction' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Setting Code",
                        "Date",
                        "Active",
                        "Transaction"+Environment.NewLine+"Code",
                        "Transaction Name",

                        //6-10
                        "Remark",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By",

                        //11-12
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"

                    },
                    new int[]
                    {
                        //0
                        25,

                        //1-5
                        80, 80, 60, 75, 180,

                        //6-10
                        200, 100, 100, 100, 100,

                        //11-12
                        100, 100,
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 8, 11 });
            Sm.GrdFormatTime(Grd1, new int[] { 9, 12 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7, 8, 9, 10, 11, 12 }, false);
            //Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7, 8 }, !ChkHideInfoInGrd.Checked);
            //Sm.SetGrdAutoSize(Grd1);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();
                if (!ChkActInd.Checked) Filter = " Where A.ActInd='Y' ";
                Sm.FilterStr(ref Filter, ref cm, TxtSettingCode.Text, new string[] { "A.ExpensesCode" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By A.ExpensesCode;",
                        new string[]
                        {
                            //0
                            "ExpensesCode", 

                            //1-5
                            "DocDt", "ActInd", "TransactionCode", "TransactionName", "Remark", 

                            //6-9
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 9);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtSettingCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSettingCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Setting's Code");
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion
    }
}
