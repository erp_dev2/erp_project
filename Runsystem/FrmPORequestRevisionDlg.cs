﻿#region Update
/*
    05/09/2019 [WED] bug query alias
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPORequestRevisionDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPORequestRevision mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPORequestRevisionDlg(FrmPORequestRevision FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "No",

                        //1-5
                        "",
                        "PO Request#",
                        "PO Request"+Environment.NewLine+"DNo",
                        "Item's"+Environment.NewLine+"Code",
                        "",

                        //6-10
                        "Item's Name",  
                        "UoM",
                        "Quantity",
                        "Outstanding"+Environment.NewLine+"Quantity",
                        "Quotation#",

                        //11-15
                        "Quotation"+Environment.NewLine+"DNo",
                        "Vendor Code",
                        "Vendor Name",
                        "Term of"+Environment.NewLine+"Payment",
                        "Currency",

                        //16-18
                        "Unit"+Environment.NewLine+"Price",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20,
                        //1-5
                        100, 100, 20, 100, 50,
                        //6-10
                        100, 20, 100, 100, 80,  
                        //11-15
                        100, 80, 100, 130, 100, 
                        //16-20
                        80, 100, 80, 
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 16 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 1, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DocNo, C.Dno, ");
            SQL.AppendLine("E.ItCode, H.ItName, H.PurchaseUomCode, ifNull(C.Qty, 0) As QtyPOR, ");
            SQL.AppendLine("C.Qty-IfNull(( ");
            SQL.AppendLine("     Select Sum(T.Qty) From TblRecvVdDtl T ");
            SQL.AppendLine("     Where T.CancelInd='N' And T.PODocNo=L.DocNo And T.PODNo=K.DNo ");
            SQL.AppendLine("     ), 0)  ");
            SQL.AppendLine(" - ifnull((Select Sum(T.Qty) From TblPOQtyCancel T ");
            SQL.AppendLine("     Where T.CancelInd='N' And T.PODocNo=L.DocNo And T.PODNo=K.DNo),0) As OutstandingQty, ");
            SQL.AppendLine("C.QtDocNo, C.QtDNo, F.VdCode, I.VdName, J.PtName, F.CurCode, ");
            SQL.AppendLine("G.UPrice, C.Remark ");
            SQL.AppendLine("From TblPORequestHdr B  ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.DocNo=C.DocNo And C.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr D On C.MaterialRequestDocNo=D.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And (D.SiteCode Is Null Or ( ");
                SQL.AppendLine("    D.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(D.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On C.MaterialRequestDocNo=E.DocNo And C.MaterialRequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr F On C.QtDocNo=F.DocNo ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("    And (F.PtCode Is Null Or ( ");
                SQL.AppendLine("    And F.PtCode Is Not Null ");
                SQL.AppendLine("    And F.PtCode In (");
                SQL.AppendLine("        Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo ");
            SQL.AppendLine("Inner Join TblItem H On E.ItCode=H.ItCode ");
            SQL.AppendLine("Inner Join TblVendor I On F.VdCode=I.VdCode ");
            SQL.AppendLine("Inner Join TblPaymentTerm J On F.PtCode=J.PtCode ");
            SQL.AppendLine("Left Join TblPoDtl K On B.DocNo = K.PoRequestDocNo And C.Dno = K.POrequestDNo And K.CancelInd = 'N' ");
            SQL.AppendLine("Left Join TblPoHdr L On K.DocNo = L.DocNo ");
            SQL.AppendLine("Left Join TblrecvVdDtl M On M.PODocNo = L.DocNo And  M.PODNo = K.Dno And M.CancelInd= 'N' ");
            SQL.AppendLine("Left Join TblRecvVdHdr N On N.DocNo = M.DocNo ");
            SQL.AppendLine("left Join TblPoQtyCancel O On O.PODocNo = L.DocNo And O.PODno = K.DNo ");
            SQL.AppendLine("Where B.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "B.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "H.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By B.CreateDt;",
                        new string[] 
                        { 
                        //0
                        "DocNo",  

                        //1-5
                        "DNo", "ItCode", "ItName", "PurchaseUomCode", "QtyPOR", 
                        
                        //6-10
                        "OutstandingQty", "QtDocNo", "QtDNo", "VdCode", "VdName",  
                        
                        //11-14
                        "PtName", "CurCode", "UPrice", "Remark"
                        
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 14);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtPORDocNo.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.DNoPOR = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                mFrmParent.TxtVdCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 13);
                mFrmParent.TxtItCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                mFrmParent.TxtQtyOld.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9), 0);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_EllipsisButtonClick_1(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmPORequest(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit_1(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPORequest(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO request#");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
