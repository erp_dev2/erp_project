﻿#region Update
/*
    19/09/2019 [TKG/IMS] tambah informasi dimensions
    08/10/2019 [WED/IMS] tambah informasi specification
    18/11/2019 [WED/IMS] tambah item local code
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBom2Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmBom2 mFrmParent;
        private string mSQL = string.Empty, mDocNoWC ="" ;

        #endregion

        #region Constructor

        public FrmBom2Dlg2(FrmBom2 FrmParent, string DocNoWC)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocNoWC = DocNoWC;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueOption(ref LueDocType, "BomDocType");
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Type",
                    "",
                    "Document#",
                    "Name",
                    
                    //6-10
                    "Source",
                    "Capacity",
                    "UoM",
                    "UoM 2",
                    "Old Code",

                    //11-13
                    "Dimensions",
                    "Specification",
                    "Item's" +Environment.NewLine + "Local Code",
                }
            );
            Sm.GrdFormatDec(Grd1, new[] { 7 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 4, 5, 6, 8, 9, 10, 11, 12, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 7, 8, 9 });
            if (!mFrmParent.mIsBOMShowDimensions) Sm.GrdColInvisible(Grd1, new int[] { 11 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 12 });
            Grd1.Cols[10].Move(6);
            Grd1.Cols[13].Move(6);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocType, A.DocNo, A.DocName, B.OptDesc, A.Capacity, A.Uom, A.Uom2, A.OldCode, A.Dimensions, A.Specification, A.ItCodeInternal ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Cast('3' As char) As DocType, A.EmpCode As DocNo, ");
            SQL.AppendLine("    Concat(A.EmpName, ' (', IfNull(B.PosName, '-'), ' / ', IfNull(C.DeptName, '-'), ')') As DocName, ");
            SQL.AppendLine("    Cast('1' As char) As Capacity, Null As Uom, Null As Uom2, EmpCodeOld As OldCode, Null As Dimensions, Null as Specification, Null As ItCodeInternal ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("    Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("	Where A.ResignDt is null ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Cast('2' As char), DocNo, DocName, Cast('1' As char), Null As Uom, Null As Uom2, Null As OldCode, Null As Dimensions, Null As Specification, Null As ItCodeInternal ");
            SQL.AppendLine("    From TblFormulaHdr  ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Cast('1' As char), ItCode, ItName, null, PlanningUomCode As Uom, PlanningUomCode2 As Uom2, Null As OldCode, ");
            if (mFrmParent.mIsBOMShowDimensions)
            {
                SQL.AppendLine("    Trim(Concat( ");
                SQL.AppendLine("    Case When IfNull(Length, 0.00)<>0.00 Then ");
                SQL.AppendLine("        Concat('Length : ', Trim(Concat(Convert(Format(IfNull(Length, 0.00), 2) Using utf8), ' ', IfNull(LengthUomCode, '')))) ");
                SQL.AppendLine("    Else '' End, ' ', ");
                SQL.AppendLine("    Case When IfNull(Height, 0.00)<>0.00 Then ");
                SQL.AppendLine("        Concat('Height : ', Trim(Concat(Convert(Format(IfNull(Height, 0.00), 2) Using utf8), ' ', IfNull(HeightUomCode, '')))) ");
                SQL.AppendLine("    Else '' End, ' ', ");
                SQL.AppendLine("    Case When IfNull(Width, 0.00)<>0.00 Then ");
                SQL.AppendLine("        Concat('Width : ', Trim(Concat(Convert(Format(IfNull(Width, 0.00), 2) Using utf8), ' ', IfNull(WidthUomCode, '')))) ");
                SQL.AppendLine("    Else '' End ");
                SQL.AppendLine("    )) As Dimensions ");
            }
            else
                SQL.AppendLine("    Null As Dimensions ");
            SQL.AppendLine(", Specification, ItCodeInternal ");
            SQL.AppendLine("    From TblItem ");
            SQL.AppendLine("    Where PlanningItemInd = 'Y' And ActInd = 'Y' ");
            SQL.AppendLine(") A, TblOption B ");
            SQL.AppendLine("Where B.OptCat='BomDocType' And A.DocType=B.OptCode ");

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 9 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And Concat(A.DocType, A.DocNo) Not In (" + mFrmParent.GetSelectedDocument() + ") ";
                

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", mDocNoWC);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.DocName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "A.DocType", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocType, A.DocNo;",
                        new string[]
                        {
                            //0
                            "DocType",

                            //1-5
                            "DocNo", 
                            "DocName",
                            "OptDesc",
                            "Capacity",
                            "UoM",

                            //6-10
                            "UoM2",
                            "OldCode",
                            "Dimensions",
                            "Specification",
                            "ItCodeInternal"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd2.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDocumentAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 1, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 4);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 4, Grd1, Row2, 5);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 6);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 6, Grd1, Row2, 7);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 7, Grd1, Row2, 8);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 11, Grd1, Row2, 9);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 12, Grd1, Row2, 10);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 13, Grd1, Row2, 11);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 14, Grd1, Row2, 12);
                            mFrmParent.Grd2.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 6, 10 });
                    }
                }
                mFrmParent.Grd2.EndUpdate();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account.");
        }

        private bool IsDocumentAlreadyChosen(int Row)
        {
            var key = Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 4);
            for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd2, Index, 1)+Sm.GetGrdStr(mFrmParent.Grd2, Index, 2), key
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 2), "1"))
                {
                    var f1 = new FrmItem(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 2), "2"))
                {
                    var f2 = new FrmFormula(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f2.ShowDialog();
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 2), "3"))
                {
                    var f3 = new FrmWorkCenter(mFrmParent.mMenuCode);
                    f3.Tag = mFrmParent.mMenuCode;
                    f3.WindowState = FormWindowState.Normal;
                    f3.StartPosition = FormStartPosition.CenterScreen;
                    f3.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f3.ShowDialog();
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 2), "1"))
                {
                    var f1 = new FrmItem(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 2), "2"))
                {
                    var f2 = new FrmFormula(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f2.ShowDialog();
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 2), "3"))
                {
                    var f3 = new FrmWorkCenter(mFrmParent.mMenuCode);
                    f3.Tag = mFrmParent.mMenuCode;
                    f3.WindowState = FormWindowState.Normal;
                    f3.StartPosition = FormStartPosition.CenterScreen;
                    f3.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f3.ShowDialog();
                }
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue2(Sl.SetLueOption), "BomDocType");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Document type");
        }

        #endregion

    }
}
