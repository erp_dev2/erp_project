﻿#region Update
/*
    08/03/2021 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestPayroll16Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherRequestPayroll16 mFrmParent;
        private string mSQL = string.Empty;
        private byte mDocType = 0;

        #endregion

        #region Constructor

        public FrmVoucherRequestPayroll16Dlg2(FrmVoucherRequestPayroll16 FrmParent, byte DocType)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocType = DocType;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.CCName ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Left Join TblCostCenter B On A.CCCode = B.CCCode ");
            SQL.AppendLine("Where A.DocNo In ");
            SQL.AppendLine("( ");

            if (mDocType == 1)
                SQL.AppendLine("    Select JournalDocNo ");
            else
                SQL.AppendLine("    Select JournalDocNo2 ");

            SQL.AppendLine("    From TblVoucherRequestPayrollJournal ");
            SQL.AppendLine("    Where VRPDocNo = @DocNo ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-3
                    "Document#",
                    "", 
                    "Cost Center"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-3
                    180, 20, 200
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", mFrmParent.TxtDocNo.Text);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + " Order By A.DocNo;",
                    new string[] { "DocNo", "CCName" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmJournal(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmJournal(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {            
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
        }


        #endregion

        #endregion

    }
}
