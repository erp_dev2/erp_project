﻿#region Update
/*
    22/08/2017 [TKG] menggunakan tax group
    02/09/2017 [TKG] bug fixing saat menampilkan memilih data purchase invoice
    03/09/2017 [TKG] tambah informasi outgoing payment# dan date, vr tax hanya yg sudah divoucherkan
    28/11/2017 [TKG] perubahan perhitungan vr tax
    02/04/2019 [WED] PI, PRI, VRTax tidak perlu di voucher kan untuk bisa diproses di VRVAT
    13/05/2019 [TKG] tambah PI raw material
    01/07/2019 [WED] BUG filter query saat menggunakan Vendor
    07/08/2019 [TKG] tambah fasilitas select multiple record, untuk PI raw material menampilkan data yg sudah diproses menjadi voucher dengan tanggal filter tanggal voucher.
    26/11/2021 [YOG] Penambahan filter range Tax Invoice Date di header pada List of Document ketika klik loop pada Tab “IN” yang letaknya di detail menu Voucher Request (VAT) (10329), agar memudahkan memilih dokumen berdasar range Tax Invoice Date.
    03/02/2022 [BRI/PHT] tambah filter multi profit center berdasarkan param IsFicoUseMultiProfitCenterFilter
    10/02/2022 [BRI/PHT] multi profit center get child
    07/06/2022 [DITA/IOK] tambah taxcode PI untuk keperluan jika ada update tax di PI 
    06/07/2022 [VIN/IOK] bug Union All
    28/09/2022 [ICA/PHT] bisa menarik lebih dari satu tax dari purchase invoice, source amount bisa dari detail berdasarkan param PurchaseInvoiceTaxCalculationFormula
    10/10/2022 [ICA/PHT] Bug Filter2 di tambah dengan filter berdasarkan TaxCode
    21/10/2022 [VIN/IOK] bug Filter vendor tidak berfungsi
    22/10/2022 [VIN/IOK] bug Filter document tidak berfungsi
    24/10/2022 [VIN/IOK] bug dokumen yang sudah dipilih tidak dapat dipilih kembali
    03/11/2022 [ICA/IOK] bug source taxinvoice2 dan taxinvoice3 masih dari taxinvoice1
    05/11/2022 [TYO/PHT] bug merubah alias di getSQL
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestPPNDlg : RunSystem.FrmBase4
    {
        #region Field

        private List<String> mlProfitCenter = null;
        private FrmVoucherRequestPPN mFrmParent;
        private string 
            mPPNTaxCode = string.Empty, 
            mDocNo = string.Empty, 
            mTaxGrpCode = string.Empty;
        private bool 
            mIsPIRawMaterialShown = false,
            mIsAllProfitCenterSelected = false;

        #endregion

        #region Constructor

        public FrmVoucherRequestPPNDlg(FrmVoucherRequestPPN FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "List of Document";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sm.SetDefaultPeriod(ref DteTaxInvDt1, ref DteTaxInvDt2, -30);
                Sl.SetLueVdCode(ref LueVdCode);
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                mDocNo = mFrmParent.TxtDocNo.Text;
                mTaxGrpCode = Sm.GetLue(mFrmParent.LueTaxGrpCode);
                mIsPIRawMaterialShown = Sm.CompareStr(mTaxGrpCode, mFrmParent.mPurchaseInvoiceRawMaterialTaxGrpCode);
                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                else
                {
                    LblMultiProfitCenterCode.Visible = false;
                    CcbProfitCenterCode.Visible = false;
                    ChkProfitCenterCode.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL(string Filter, string Filter2, string Filter3, string Filter4, string Filter5, string Filter6, string Filter7, string Filter8, string Filter9)
        {
            var SQL = new StringBuilder();

            #region Old Purchase Invoice
            //SQL.AppendLine("Select X.* From ( ");
            //SQL.AppendLine("Select '1' As DocType, 'Purchase Invoice' As DocTypeDesc, A.DocNo, A.DocDt, ");
            //SQL.AppendLine("E.VdName, F.PODocNo, ");
            //SQL.AppendLine("Case ");
            //SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo ");
            //SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo2 ");
            //SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo3 ");
            //SQL.AppendLine("Else Null End As TaxInvoiceNo, ");
            //SQL.AppendLine("Case ");
            //SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceDt ");
            //SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceDt2 ");
            //SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceDt3 ");
            //SQL.AppendLine("Else Null End As TaxInvoiceDt, ");
            //SQL.AppendLine("Case ");
            //SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxCode1 ");
            //SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxCode2 ");
            //SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxCode3 ");
            //SQL.AppendLine("Else Null End As TaxCode, ");
            //SQL.AppendLine("Case ");
            //SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then B.TaxRate*A.Amt*0.01 ");
            //SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then C.TaxRate*A.Amt*0.01 ");
            //SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then D.TaxRate*A.Amt*0.01 ");
            //SQL.AppendLine("Else 0.00 End As Amt, ");
            //SQL.AppendLine("A.TaxRateAmt As ExcRate, A.CurCode ");
            //SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            //SQL.AppendLine("Left Join TblTax B On A.TaxCode1=B.TaxCode ");
            //SQL.AppendLine("Left Join TblTax C On A.TaxCode2=C.TaxCode ");
            //SQL.AppendLine("Left Join TblTax D On A.TaxCode3=D.TaxCode ");
            //SQL.AppendLine("Inner Join TblVendor E On A.VdCode=E.VdCode ");
            //SQL.AppendLine("Left Join ( ");

            //SQL.AppendLine("    Select Tbl1.DocNo, Concat(Tbl1.PODocNo, ");
            //SQL.AppendLine("    Case When Tbl2.OP Is Null Then '' Else Concat(', ', Tbl2.OP) End ");
            //SQL.AppendLine("    ) As PODocNo From ( ");
            //SQL.AppendLine("    Select T1.DocNo, Group_Concat(Distinct T3.PODocNo Order By T3.PODocNo ASC Separator ', ') As PODocNo ");
            //SQL.AppendLine("    From TblPurchaseInvoiceHdr T1 ");
            //SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("    Inner Join TblRecvVdDtl T3 On T2.RecvVdDocNo=T3.DocNo And T2.RecvVdDNo=T3.DNo ");
            //SQL.AppendLine("    Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            //SQL.AppendLine("    And T1.CancelInd='N' ");
            //SQL.AppendLine("And ( ");

            //SQL.AppendLine("(T1.TaxCode1 Is Not Null ");
            //SQL.AppendLine("And ( ");
            //SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
            //if (mDocNo.Length > 0)
            //    SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
            //SQL.AppendLine(") ");
            //SQL.AppendLine("And T1.VATSettlementDocNo Is Null ");
            //SQL.AppendLine("And IfNull(T1.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            //SQL.AppendLine(") ");

            //SQL.AppendLine("Or ");
            //SQL.AppendLine("(T1.TaxCode2 Is Not Null ");
            //SQL.AppendLine("And ( ");
            //SQL.AppendLine("    T1.VoucherRequestPPNDocNo2 Is Null ");
            //if (mDocNo.Length > 0)
            //    SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo2, '')=@DocNoTemp ");
            //SQL.AppendLine(") ");
            //SQL.AppendLine("And T1.VATSettlementDocNo2 Is Null ");
            //SQL.AppendLine("And IfNull(T1.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            //SQL.AppendLine(") ");

            //SQL.AppendLine("Or ");
            //SQL.AppendLine("(T1.TaxCode3 Is Not Null ");
            //SQL.AppendLine("And ( ");
            //SQL.AppendLine("    T1.VoucherRequestPPNDocNo3 Is Null ");
            //if (mDocNo.Length > 0)
            //    SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo3, '')=@DocNoTemp ");
            //SQL.AppendLine(") ");
            //SQL.AppendLine("And T1.VATSettlementDocNo3 Is Null ");
            //SQL.AppendLine("And IfNull(T1.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            //SQL.AppendLine(") ");

            //SQL.AppendLine(") ");
            //SQL.AppendLine("    Group By T1.DocNo ");

            //SQL.AppendLine("    ) Tbl1 ");
            //SQL.AppendLine("    Left Join ( ");
            //SQL.AppendLine("        Select X3.DocNo, ");
            //SQL.AppendLine("        Group_Concat(Distinct Concat(X1.DocNo, ' (', DATE_FORMAT(X1.DocDt, '%d/%m/%Y'), ')') Order By X1.DocDt, X1.DocNo ASC Separator ', ') As OP ");
            //SQL.AppendLine("        From TblOutgoingPaymentHdr X1 ");
            //SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl X2 On X1.DocNo=X2.DocNo And X2.InvoiceType='1' ");
            //SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr X3 ");
            //SQL.AppendLine("            On X2.InvoiceDocNo=X3.DocNo ");
            //SQL.AppendLine("            And X3.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("        Where X1.CancelInd='N' And X1.Status In ('O', 'A') ");
            //SQL.AppendLine("        Group By X3.DocNo ");
            //SQL.AppendLine("    ) Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");
            //SQL.AppendLine(") F On A.DocNo=F.DocNo ");
            //if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
            //{
            //    SQL.AppendLine("Inner Join TblCostCenter G On A.DeptCode=G.DeptCode ");
            //    SQL.AppendLine("Inner Join TblProfitCenter H On G.ProfitCenterCode=H.ProfitCenterCode ");
            //    SQL.AppendLine(Filter7);
            //}
            //SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            //SQL.AppendLine("And A.CancelInd='N' ");
            //SQL.AppendLine("And ( ");

            //SQL.AppendLine("(A.TaxCode1 Is Not Null ");
            //SQL.AppendLine("And ( ");
            //SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
            //if (mDocNo.Length > 0)
            //    SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
            //SQL.AppendLine(") ");
            //SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
            //SQL.AppendLine("And IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            //SQL.AppendLine(") ");

            //SQL.AppendLine("Or ");
            //SQL.AppendLine("(A.TaxCode2 Is Not Null ");
            //SQL.AppendLine("And ( ");
            //SQL.AppendLine("    A.VoucherRequestPPNDocNo2 Is Null ");
            //if (mDocNo.Length > 0)
            //    SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo2, '')=@DocNoTemp ");
            //SQL.AppendLine(") ");
            //SQL.AppendLine("And A.VATSettlementDocNo2 Is Null ");
            //SQL.AppendLine("And IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            //SQL.AppendLine(") ");

            //SQL.AppendLine("Or ");
            //SQL.AppendLine("(A.TaxCode3 Is Not Null ");
            //SQL.AppendLine("And ( ");
            //SQL.AppendLine("    A.VoucherRequestPPNDocNo3 Is Null ");
            //if (mDocNo.Length > 0)
            //    SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo3, '')=@DocNoTemp ");
            //SQL.AppendLine(") ");
            //SQL.AppendLine("And A.VATSettlementDocNo3 Is Null ");
            //SQL.AppendLine("And IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            //SQL.AppendLine(") ");
            //SQL.AppendLine(") ");

            //SQL.AppendLine(Filter);
            //SQL.AppendLine(Filter2);
            //SQL.AppendLine(") X  ");
            //if (ChkTaxInvDt.Checked)
            //    SQL.AppendLine("Where (TaxInvoiceDt Between @DocDt3 And @DocDt4 And TaxInvoiceDt Is Not Null) ");
            #endregion

            #region Purchase Invoice
            SQL.AppendLine("Select X.* From ( ");
            SQL.AppendLine("Select '1' As DocType, 'Purchase Invoice' As DocTypeDesc, A.DocNo, A.DocDt, C.VdCode, ");
            SQL.AppendLine("C.VdName, D.PODocNo, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When IfNull(A.TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo ");
            SQL.AppendLine("Else Null End As TaxInvoiceNo, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When IfNull(A.TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceDt ");
            SQL.AppendLine("Else Null End As TaxInvoiceDt, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When IfNull(A.TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxCode ");
            SQL.AppendLine("Else Null End As TaxCode, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When IfNull(A.TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            if (mFrmParent.mPurchaseInvoiceTaxCalculationFormula == "2")
                SQL.AppendLine("Then A.TaxRate*B.Amt*0.01 ");
            else
                SQL.AppendLine("Then A.TaxRate*A.Amt*0.01 ");
            SQL.AppendLine("Else 0.00 End As Amt, ");
            SQL.AppendLine("A.TaxRateAmt As ExcRate, A.CurCode, A.TaxName, A.TaxRate ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("	Select '1' TaxType, A.DocNo, A.DocDt, A.Amt, A.CurCode, B.TaxCode, A.TaxInvoiceNo, A.TaxInvoiceDt, A.TaxRateAmt, B.TaxName, B.TaxRate, ");
            SQL.AppendLine("    A.VdCode, A.DeptCode ");
            SQL.AppendLine("	From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("	Inner Join TblTax B On A.TaxCode1 = B.TaxCode ");
            SQL.AppendLine("	Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("		And A.CancelInd='N' ");
            SQL.AppendLine("		AND A.TaxCode1 Is Not Null ");
            SQL.AppendLine("			And (A.VoucherRequestPPNDocNo Is Null  "); 
            if(mDocNo.Length > 0)
                SQL.AppendLine("        Or IfNull(A.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("			And A.VATSettlementDocNo Is Null ");
            SQL.AppendLine("			And IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            SQL.AppendLine("	Union All ");
            SQL.AppendLine("	Select '2' TaxType, A.DocNo, A.DocDt, A.Amt, A.CurCode, B.TaxCode, A.TaxInvoiceNo2, A.TaxInvoiceDt2, A.TaxRateAmt, B.TaxName, B.TaxRate, ");
            SQL.AppendLine("    A.VdCode, A.DeptCode ");
            SQL.AppendLine("	From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("	Inner Join TblTax B On A.TaxCode2 = B.TaxCode ");
            SQL.AppendLine("	Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("		And A.CancelInd='N' ");
            SQL.AppendLine("		AND A.TaxCode2 Is Not Null ");
            SQL.AppendLine("			And (A.VoucherRequestPPNDocNo2 Is Null ");
            if (mDocNo.Length > 0)
                SQL.AppendLine("        Or IfNull(A.VoucherRequestPPNDocNo2, '')=@DocNoTemp ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("			And A.VATSettlementDocNo2 Is Null ");
            SQL.AppendLine("			And IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            SQL.AppendLine("	Union All ");
            SQL.AppendLine("	Select '3' TaxType, A.DocNo, A.DocDt, A.Amt, A.CurCode, B.TaxCode, A.TaxInvoiceNo3, A.TaxInvoiceDt3, A.TaxRateAmt, B.TaxName, B.TaxRate, ");
            SQL.AppendLine("    A.VdCode, A.DeptCode ");
            SQL.AppendLine("	From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("	Inner Join TblTax B On A.TaxCode3 = B.TaxCode ");
            SQL.AppendLine("	Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("		And A.CancelInd='N' ");
            SQL.AppendLine("		AND A.TaxCode3 Is Not Null ");
            SQL.AppendLine("			And (A.VoucherRequestPPNDocNo3 Is Null ");
            if (mDocNo.Length > 0)
                SQL.AppendLine("        Or IfNull(A.VoucherRequestPPNDocNo3, '')=@DocNoTemp ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("			And A.VATSettlementDocNo3 Is Null ");
            SQL.AppendLine("			And IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            SQL.AppendLine(")A ");
            if (mFrmParent.mPurchaseInvoiceTaxCalculationFormula == "2")
            {
                SQL.AppendLine("Inner Join  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select DocNo, Sum(Amt) Amt, TaxType ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("	    Select A.DocNo, ");
                if (!mFrmParent.mIsPurchaseInvoiceUsePORevision)
                    SQL.AppendLine("        C.QtyPurchase*H.UPrice Amt, ");
                else
                    SQL.AppendLine("        C.QtyPurchase*IfNull(D1.Uprice, H.UPrice) Amt,");
                SQL.AppendLine("	    Case ");
                SQL.AppendLine("		    When B.TaxInd1 = 'Y' then '1' ");
                SQL.AppendLine("		    When B.TaxInd2 = 'Y' then '2' ");
                SQL.AppendLine("		    When B.TaxInd3 = 'Y' then '3' ");
                SQL.AppendLine("		    else Null ");
                SQL.AppendLine("	    End TaxType ");
                SQL.AppendLine("	    from TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("	    Inner Join TblPurchaseInvoiceDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("	    Inner Join TblRecvVdDtl C On B.RecvVdDocNo = C.DocNo And B.RecvVdDNo = C.DNo ");
                SQL.AppendLine("	    Inner Join TblPODtl D On C.PODocNo = D.DocNo And C.PODNo = D.DNo ");
                if (mFrmParent.mIsPurchaseInvoiceUsePORevision)
                {
                    SQL.AppendLine("    Left Join  ");
                    SQL.AppendLine("    (  ");
                    SQL.AppendLine("        Select T4.DocNo, T4.DNo, T1.DocNo PIDocNo, T2.DNo PIDNo, T2.RecvVdDocNo, T5.PODocNo, T5.PODNo, T5.Discount, T5.DiscountAmt, T5.RoundingValue, T5.Qty, T6.Uprice  ");
                    SQL.AppendLine("        From TblPurchaseInvoiceHdr T1 ");
                    SQL.AppendLine("        Inner Join TblPurchaseInvoiceDtl T2 On T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("        Inner Join TblRecvVdDtl T3 On T2.RecvVdDocNo = T3.DocNo And T2.RecvVdDNo = T3.DNo ");
                    SQL.AppendLine("        Inner Join TblPODtl T4 On T3.PODocNo = T4.DocNo And T3.PODNo = T4.DNo  ");
                    SQL.AppendLine("        Inner Join TblPORevision T5 On T4.DocNo = T5.PODocNo And T4.DNo = T5.PODNo  ");
                    SQL.AppendLine("        Inner Join TblQtDtl T6 On T5.QtDocNo = T6.DocNo And T5.QtDNo = T6.DNo  ");
                    SQL.AppendLine("        Where T5.PODocNo Is Not Null ");
                    SQL.AppendLine("	    And T5.DocNo = ( ");
                    SQL.AppendLine("	 	    Select Max(DocNo) From TblPORevision Where PODocNo = T5.PODocNo And PODNo = T5.PODNo ");
                    SQL.AppendLine("	 	    Group By PODocNo, PODNo ");
                    SQL.AppendLine("	    )   ");
                    SQL.AppendLine("        And T1.DocNo = @DocNo ");
                    SQL.AppendLine("    ) D1 On A.DocNo=D1.PIDocNo And B.DNo=D1.PIDNo ");
                }
                SQL.AppendLine("	    Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo ");
                SQL.AppendLine("	    Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo=F.DocNo And E.MaterialRequestDNo=F.DNo ");
                SQL.AppendLine("	    Inner Join TblQtHdr G On E.QtDocNo=G.DocNo ");
                SQL.AppendLine("	    Inner Join TblQtDtl H On E.QtDocNo=H.DocNo And E.QtDNo=H.DNo ");
                SQL.AppendLine("	    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("	        And A.CancelInd='N' ");
                SQL.AppendLine("	        AND A.TaxCode1 Is Not Null ");
                SQL.AppendLine("		    And A.VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("		    And A.VATSettlementDocNo Is Null ");
                SQL.AppendLine("		    And IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
                SQL.AppendLine("    )Tbl Group By DocNo, TaxType ");
                SQL.AppendLine(")B On A.DocNo = B.DocNo And A.TaxType = B.TaxType ");
            }
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("	Select Tbl1.DocNo, Concat(Tbl1.PODocNo, ");
            SQL.AppendLine("	Case When Tbl2.OP Is Null Then '' Else Concat(', ', Tbl2.OP) End ");
            SQL.AppendLine("	) As PODocNo ");
            SQL.AppendLine("	From ( ");
            SQL.AppendLine("		Select T1.DocNo, Group_Concat(Distinct T3.PODocNo Order By T3.PODocNo ASC Separator ', ') As PODocNo ");
            SQL.AppendLine("		From TblPurchaseInvoiceHdr T1 ");
            SQL.AppendLine("		Inner Join TblPurchaseInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("		Inner Join TblRecvVdDtl T3 On T2.RecvVdDocNo=T3.DocNo And T2.RecvVdDNo=T3.DNo ");
            SQL.AppendLine("		Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("		And T1.CancelInd='N' ");
            SQL.AppendLine("		AND ( ");
            SQL.AppendLine("			(T1.TaxCode1 Is Not Null ");
            SQL.AppendLine("            And (T1.VoucherRequestPPNDocNo Is Null ");
            if (mDocNo.Length > 0)
                SQL.AppendLine("            Or IfNull(T1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("			And T1.VATSettlementDocNo Is Null ");
            SQL.AppendLine("			And IfNull(T1.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            SQL.AppendLine("            ) ");

            SQL.AppendLine("            Or ");
            SQL.AppendLine("			(T1.TaxCode2 Is Not Null ");
            SQL.AppendLine("            And (T1.VoucherRequestPPNDocNo2 Is Null ");
            if (mDocNo.Length > 0)
                SQL.AppendLine("            Or IfNull(T1.VoucherRequestPPNDocNo2, '')=@DocNoTemp ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("			And T1.VATSettlementDocNo2 Is Null ");
            SQL.AppendLine("			And IfNull(T1.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            SQL.AppendLine("            ) ");

            SQL.AppendLine("            Or ");
            SQL.AppendLine("			(T1.TaxCode3 Is Not Null ");
            SQL.AppendLine("            And (T1.VoucherRequestPPNDocNo3 Is Null ");
            if (mDocNo.Length > 0)
                SQL.AppendLine("            Or IfNull(T1.VoucherRequestPPNDocNo3, '')=@DocNoTemp ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("			And T1.VATSettlementDocNo3 Is Null ");
            SQL.AppendLine("			And IfNull(T1.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("		GROUP BY T1.DocNo ");
            SQL.AppendLine("	)Tbl1 ");
            SQL.AppendLine("	Left Join ( ");
            SQL.AppendLine("		Select X3.DocNo, ");
            SQL.AppendLine("		Group_Concat(Distinct Concat(X1.DocNo, ' (', DATE_FORMAT(X1.DocDt, '%d/%m/%Y'), ')') Order By X1.DocDt, X1.DocNo ASC Separator ', ') As OP ");
            SQL.AppendLine("		From TblOutgoingPaymentHdr X1 ");
            SQL.AppendLine("		Inner Join TblOutgoingPaymentDtl X2 On X1.DocNo=X2.DocNo And X2.InvoiceType='1' ");
            SQL.AppendLine("		Inner Join TblPurchaseInvoiceHdr X3 On X2.InvoiceDocNo=X3.DocNo ");
            SQL.AppendLine("			And X3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("		Where X1.CancelInd='N' And X1.Status In ('O', 'A') ");
            SQL.AppendLine("		Group By X3.DocNo ");
            SQL.AppendLine("	) Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo ");
            if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
            {
                SQL.AppendLine("Inner Join TblCostCenter G On A.DeptCode=G.DeptCode ");
                SQL.AppendLine("Inner Join TblProfitCenter H On G.ProfitCenterCode=H.ProfitCenterCode ");
                SQL.AppendLine(Filter7);
            }
            SQL.AppendLine(Filter);
            //SQL.AppendLine(Filter2);
            SQL.AppendLine(") X  ");
            SQL.AppendLine("Where 0=0 ");
            SQL.AppendLine(Filter2);
            if (ChkTaxInvDt.Checked)
                SQL.AppendLine("And (TaxInvoiceDt Between @DocDt3 And @DocDt4 And TaxInvoiceDt Is Not Null) ");
			if (ChkVdCode.Checked)
                SQL.AppendLine("And VdCode = '" + Sm.GetLue(LueVdCode) + "' ");
            if (ChkDocNo.Checked)
                SQL.AppendLine("And DocNo Like '%" + TxtDocNo.Text + "%' ");
            SQL.AppendLine(Filter2);

            #endregion

            #region Purchase Return Invoice
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Y.* From ( ");
            SQL.AppendLine("Select '2' As DocType, 'Purchase Return Invoice' As DocTypeDesc, A.DocNo, A.DocDt, B.VdCode,");
            SQL.AppendLine("B.VdName, C.PODocNo, ");

            SQL.AppendLine("Case ");
            SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo ");
            SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo2 ");
            SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo3 ");
            SQL.AppendLine("Else Null End As TaxInvoiceNo, ");

            SQL.AppendLine("Case ");
            SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceDt ");
            SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceDt2 ");
            SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceDt3 ");
            SQL.AppendLine("Else Null End As TaxInvoiceDt, ");

            SQL.AppendLine("Case ");
            SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxCode1 ");
            SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxCode2 ");
            SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxCode3 ");
            SQL.AppendLine("Else Null End As TaxCode, ");

            SQL.AppendLine("Case ");
            SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxAmt1 ");
            SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxAmt2 ");
            SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxAmt3 ");
            SQL.AppendLine("Else 0.00 End As Amt, ");
            if (mFrmParent.mIsPurchaseReturnInvoiceCanOnlyHave1Record)
                SQL.AppendLine("G.TaxRateAmt As ExcRate, ");
            else
                SQL.AppendLine("0.00 As ExcRate, ");
            SQL.AppendLine("A.CurCode, Null TaxName, Null TaxRate ");
            SQL.AppendLine("From TblPurchaseReturnInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Tbl1.DocNo, Concat(Tbl1.PODocNo, ");
            SQL.AppendLine("    Case When Tbl2.OP Is Null Then '' Else Concat(', ', Tbl2.OP) End ");
            SQL.AppendLine("    ) As PODocNo From ( ");
            SQL.AppendLine("    Select T1.DocNo, Group_Concat(Distinct T4.PODocNo Order By T4.PODocNo ASC Separator ', ') As PODocNo ");
            SQL.AppendLine("    From TblPurchaseReturnInvoiceHdr T1 ");
            SQL.AppendLine("    Inner Join TblPurchaseReturnInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblDOVdDtl T3 On T2.DOVdDocNo=T3.DocNo And T2.DOVdDNo=T3.DNo ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T4 On T3.RecvVdDocNo=T4.DocNo And T3.RecvVdDNo=T4.DNo ");
            SQL.AppendLine("    Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("And ( ");

            SQL.AppendLine("(T1.TaxCode1 Is Not Null ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
            if (mDocNo.Length > 0)
                SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And T1.VATSettlementDocNo Is Null ");
            SQL.AppendLine("And IfNull(T1.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            SQL.AppendLine(") ");

            SQL.AppendLine("Or ");
            SQL.AppendLine("(T1.TaxCode2 Is Not Null ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    T1.VoucherRequestPPNDocNo2 Is Null ");
            if (mDocNo.Length > 0)
                SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo2, '')=@DocNoTemp ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And T1.VATSettlementDocNo2 Is Null ");
            SQL.AppendLine("And IfNull(T1.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            SQL.AppendLine(") ");

            SQL.AppendLine("Or ");
            SQL.AppendLine("(T1.TaxCode3 Is Not Null ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    T1.VoucherRequestPPNDocNo3 Is Null ");
            if (mDocNo.Length > 0)
                SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo3, '')=@DocNoTemp ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And T1.VATSettlementDocNo3 Is Null ");
            SQL.AppendLine("And IfNull(T1.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            SQL.AppendLine(") ");

            SQL.AppendLine(") ");
            SQL.AppendLine("    Group By T1.DocNo ");

            SQL.AppendLine("    ) Tbl1 ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select X3.DocNo, ");
            SQL.AppendLine("        Group_Concat(Distinct Concat(X1.DocNo, ' (', DATE_FORMAT(X1.DocDt, '%d/%m/%Y'), ')') Order By X1.DocDt, X1.DocNo ASC Separator ', ') As OP ");
            SQL.AppendLine("        From TblOutgoingPaymentHdr X1 ");
            SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl X2 On X1.DocNo=X2.DocNo And X2.InvoiceType='2' ");
            SQL.AppendLine("        Inner Join TblPurchaseReturnInvoiceHdr X3 ");
            SQL.AppendLine("            On X2.InvoiceDocNo=X3.DocNo ");
            SQL.AppendLine("            And X3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Where X1.CancelInd='N' And X1.Status In ('O', 'A') ");
            SQL.AppendLine("        Group By X3.DocNo ");
            SQL.AppendLine("    ) Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");

            SQL.AppendLine(") C On A.DocNo=C.DocNo ");
            SQL.AppendLine("And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("Inner Join TblPurchaseReturnInvoiceDtl D On A.DocNo=D.DocNo And D.DNo='001' ");
            SQL.AppendLine("Inner Join TblDOVdDtl E On D.DOVdDocNo=E.DocNo And D.DOVdDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl F On E.RecvVdDocNo=F.RecvVdDocNo And E.RecvVdDNo=F.RecvVdDNo ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr G On F.DocNo=G.DocNo And G.CancelInd='N' ");
            if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
            {
                SQL.AppendLine("Inner Join TblCostCenter H On A.DeptCode=H.DeptCode ");
                SQL.AppendLine("Inner Join TblProfitCenter I On H.ProfitCenterCode=I.ProfitCenterCode ");
                SQL.AppendLine(Filter8);
            }
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And ( ");

            SQL.AppendLine("(A.TaxCode1 Is Not Null ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
            if (mDocNo.Length > 0)
                SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
            SQL.AppendLine("And IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            SQL.AppendLine(") ");

            SQL.AppendLine("Or ");
            SQL.AppendLine("(A.TaxCode2 Is Not Null ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    A.VoucherRequestPPNDocNo2 Is Null ");
            if (mDocNo.Length > 0)
                SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo2, '')=@DocNoTemp ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.VATSettlementDocNo2 Is Null ");
            SQL.AppendLine("And IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            SQL.AppendLine(") ");

            SQL.AppendLine("Or ");
            SQL.AppendLine("(A.TaxCode3 Is Not Null ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    A.VoucherRequestPPNDocNo3 Is Null ");
            if (mDocNo.Length > 0)
                SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo3, '')=@DocNoTemp ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.VATSettlementDocNo3 Is Null ");
            SQL.AppendLine("And IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ");
            SQL.AppendLine(") ");

            SQL.AppendLine(") ");

            //SQL.AppendLine(Filter);
			SQL.AppendLine(") Y Where 0=0 ");
            if (ChkTaxInvDt.Checked)
                SQL.AppendLine("And (TaxInvoiceDt Between @DocDt3 And @DocDt4 And TaxInvoiceDt Is Not Null) ");
            if (ChkVdCode.Checked)
                SQL.AppendLine("And VdCode = '" + Sm.GetLue(LueVdCode) + "' ");
            if (ChkDocNo.Checked)
                SQL.AppendLine("And DocNo Like '%" + TxtDocNo.Text + "%' ");
            SQL.AppendLine(Filter3);
           
            #endregion

            #region Voucher Request Tax
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '3' As DocType, 'Voucher Request Tax' As DocTypeDesc, DocNo, DocDt, Null As VdCode, ");
            SQL.AppendLine("Null As VdName, Null As PODocNo, TaxInvNo, TaxInvDt As TaxInvoiceDt, TaxCode, ");
            SQL.AppendLine("Case When IfNull(TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            SQL.AppendLine("Then ");
            SQL.AppendLine("    Case When AcType='D' Then -1.00 Else 1.00 End*Amt ");
            SQL.AppendLine("Else 0.00 End As Amt, ");
            SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("    Select Amt From TblCurrencyRate ");
            SQL.AppendLine("    Where RateDt<=A.DocDt ");
            SQL.AppendLine("    And CurCode1=A.CurCode ");
            SQL.AppendLine("    And CurCode2=@MainCurCode ");
            SQL.AppendLine("    Order By RateDt Desc ");
            SQL.AppendLine("    Limit 1 ");
            SQL.AppendLine("), 0.00) ");
            SQL.AppendLine("End As ExcRate, A.CurCode, Null TaxName, Null TaxRate ");
            SQL.AppendLine("From TblVoucherRequestTax A ");
            SQL.AppendLine("Where (DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And CancelInd='N' ");
            //SQL.AppendLine("And Exists( ");
            //SQL.AppendLine("    Select 1 ");
            //SQL.AppendLine("    From TblVoucherHdr ");
            //SQL.AppendLine("    Where DocType='21' ");
            //SQL.AppendLine("    And CancelInd='N' ");
            //SQL.AppendLine("    And VoucherRequestDocNo=A.VoucherRequestDocNo ");
            //SQL.AppendLine(") ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
            if (mDocNo.Length > 0)
                SQL.AppendLine("    Or (IfNull(A.VoucherRequestPPNDocNo, '')=@DocNoTemp) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
            SQL.AppendLine("And IfNull(A.TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            if (ChkDocNo.Checked) SQL.AppendLine(" And Upper(A.DocNo) Like @DocNo ");
			if (ChkTaxInvDt.Checked)
                SQL.AppendLine("And (TaxInvDt Between @DocDt3 And @DocDt4 And TaxInvDt Is Not Null) ");
            if (ChkVdCode.Checked)
                SQL.AppendLine("And VdCode = '" + Sm.GetLue(LueVdCode) + "' ");
            if (ChkDocNo.Checked)
                SQL.AppendLine("And A.DocNo Like '%" + TxtDocNo.Text + "%' ");

            
            SQL.AppendLine(Filter4);
            #endregion

            if (mIsPIRawMaterialShown)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select '4' As DocType, 'Purchase Invoice Raw Material' As DocTypeDesc, A.DocNo, A.DocDt, D.VdCode,  ");
                SQL.AppendLine("D.VdName, Null As PODocNo, Null As TaxInvNo, Null As TaxInvDt, NULL AS TaxCode, ");
                SQL.AppendLine("A.Tax As Amt, 1.00 As ExcRate, A.CurCode, Null TaxName, Null TaxRate ");
                SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr A ");
                SQL.AppendLine("Inner Join TblRawMaterialVerify B On A.RawMaterialVerifyDocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblLegalDocVerifyHdr C On B.LegalDocVerifyDocNo=C.DocNo ");
                SQL.AppendLine("Inner Join TblVendor D On C.VdCode=D.VdCode ");
                SQL.AppendLine("Inner Join TblVoucherHdr E On A.VoucherRequestDocNo=E.VoucherRequestDocNo ");
                SQL.AppendLine("    And E.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("    And E.CancelInd='N' ");
                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    SQL.AppendLine("Inner Join TblCostCenter F On A.DeptCode=F.DeptCode ");
                    SQL.AppendLine("Inner Join TblProfitCenter G On F.ProfitCenterCode=G.ProfitCenterCode ");
                    SQL.AppendLine(Filter9);
                }
                SQL.AppendLine("Where A.CancelInd='N' ");
                SQL.AppendLine("And A.TaxInd='Y' ");
                SQL.AppendLine("And ( ");
                SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
                if (mDocNo.Length > 0)
                    SQL.AppendLine("    Or (IfNull(A.VoucherRequestPPNDocNo, '')=@DocNoTemp) ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                if (ChkDocNo.Checked)
                    SQL.AppendLine("And A.DocNo Like '%" + TxtDocNo.Text + "%' ");
                SQL.AppendLine(Filter5);
                if (ChkVdCode.Checked)
                    SQL.AppendLine("And D.VdCode = '" + Sm.GetLue(LueVdCode) + "' ");
            }
            
            SQL.AppendLine(" Order By DocNo; ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Type",
                        "Type",
                        "Document#", 
                        "",

                        //6-10
                        "Date",
                        "Vendor",
                        "PO/" + Environment.NewLine + "OutgoingPayment",
                        "Tax Invoice#",
                        "Tax Invoice Date",
                        
                        //11-15
                        "Amount",
                        "Rate",
                        "Amount" + Environment.NewLine + "(" + mFrmParent.mMainCurCode + ")",
                        "Currency",
                        "TaxCode",

                        //16-17
                        "Tax Invoice Name",
                        "Tax Invoice Rate"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 0, 180, 130, 20,

                        //6-10
                        80, 180, 400, 130, 100,

                        //11-15
                        120, 100, 120, 60, 0,

                        //16
                        140, 120
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 5 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13, 17 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 15 }, false);
            Grd1.Cols[14].Move(11);
            Grd1.Cols[16].Move(11);
            Grd1.Cols[17].Move(12);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                Sm.IsFilterByDateInvalid(ref DteTaxInvDt1, ref DteTaxInvDt2) ||
                (mFrmParent.mIsFicoUseMultiProfitCenterFilter && IsProfitCenterInvalid())
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ", Filter2 = string.Empty, Filter3 = string.Empty, Filter4 = string.Empty, Filter5 = string.Empty, Filter6 = string.Empty,
                    Filter7 = string.Empty, Filter8 = string.Empty, Filter9 = string.Empty;
                var cm = new MySqlCommand();

                SetProfitCenter();

                if (mFrmParent.Grd3.Rows.Count >= 1)
                {
                    string DocType = string.Empty, DocNo = string.Empty, TaxCode = string.Empty;

                    for (int r = 0; r < mFrmParent.Grd3.Rows.Count; r++)
                    {
                        DocType = Sm.GetGrdStr(mFrmParent.Grd3, r, 2);
                        DocNo = Sm.GetGrdStr(mFrmParent.Grd3, r, 4);
                        TaxCode = Sm.GetGrdStr(mFrmParent.Grd3, r, 14);
                        if (DocNo.Length != 0)
                        {
                            if (DocType == "1")
                            {
                                if (Filter2.Length > 0) Filter2 += " And ";
                                Filter2 += "(Concat(X.DocNo,'-',X.TaxCode) <> Concat(@DocNo0" + r.ToString() + ",'-',@TaxCode0" + r.ToString() + "))";
                                //Filter2 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                                Sm.CmParam<String>(ref cm, "@TaxCode0" + r.ToString(), TaxCode);
                            }
                            if (DocType == "2")
                            {
                                if (Filter3.Length > 0) Filter3 += " And ";
                                Filter3 += "(Y.DocNo<>@DocNo0" + r.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                            }
                            if (DocType == "3")
                            {
                                if (Filter4.Length > 0) Filter4 += " And ";
                                Filter4 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                            }
                            if (DocType == "4")
                            {
                                if (Filter5.Length > 0) Filter5 += " And ";
                                Filter5 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                            }
                        }
                    
                    }
                }

                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter_ = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter_.Length > 0) Filter_ += " Or ";
                            Filter_ += " (H.ProfitCenterCode=@ProfitCenter1_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenter1_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter_.Length == 0)
                            Filter7 += "    And 1=0 ";
                        else
                            Filter7 += "    And (" + Filter_ + ") ";
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            Filter7 += "    And Find_In_Set(H.ProfitCenterCode, @ProfitCenterCode2) ";
                            if (ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            Filter7 += "    And H.ProfitCenterCode In ( ";
                            Filter7 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            Filter7 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            Filter7 += "    ) ";
                        }
                    }
                }

                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter_ = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter_.Length > 0) Filter_ += " Or ";
                            Filter_ += " (I.ProfitCenterCode=@ProfitCenter2_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenter2_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter_.Length == 0)
                            Filter8 += "    And 1=0 ";
                        else
                            Filter8 += "    And (" + Filter_ + ") ";
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            Filter8 += "    And Find_In_Set(I.ProfitCenterCode, @ProfitCenterCode2) ";
                            if (ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            Filter8 += "    And I.ProfitCenterCode In ( ";
                            Filter8 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            Filter8 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            Filter8 += "    ) ";
                        }
                    }
                }

                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter_ = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter_.Length > 0) Filter_ += " Or ";
                            Filter_ += " (G.ProfitCenterCode=@ProfitCenter3_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenter3_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter_.Length == 0)
                            Filter9 += "    And 1=0 ";
                        else
                            Filter9 += "    And (" + Filter_ + ") ";
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            Filter9 += "    And Find_In_Set(G.ProfitCenterCode, @ProfitCenterCode2) ";
                            if (ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            Filter9 += "    And G.ProfitCenterCode In ( ";
                            Filter9 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            Filter9 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            Filter9 += "    ) ";
                        }
                    }
                }

                if (Filter2.Length > 0) Filter2 = " And ( " + Filter2 + ") ";
                if (Filter3.Length > 0) Filter3 = " And ( " + Filter3 + ") ";
                if (Filter4.Length > 0) Filter4 = " And ( " + Filter4 + ") ";
                if (Filter5.Length > 0) Filter5 = " And ( " + Filter5 + ") ";

                Sm.CmParam<String>(ref cm, "@MainCurCode", mFrmParent.mMainCurCode);
                Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(mFrmParent.LueCurCode));
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParamDt(ref cm, "@DocDt3", Sm.GetDte(DteTaxInvDt1));
                Sm.CmParamDt(ref cm, "@DocDt4", Sm.GetDte(DteTaxInvDt2));
                //Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteTaxInvDt1), Sm.GetDte(DteTaxInvDt2), "TaxInvoiceDt");
                //Sm.CmParam<String>(ref cm, "@PPNTaxCode", mFrmParent.mPPNTaxCode);
                Sm.CmParam<String>(ref cm, "@DocNoTemp", mDocNo);
                Sm.CmParam<String>(ref cm, "@TaxGrpCode", mTaxGrpCode);
                if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());

                //Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                //Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);

                Sm.FilterStr(ref Filter6, ref cm, TxtDocNo.Text, "A.DocNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter, Filter2, Filter3, Filter4, Filter5, Filter6, Filter7, Filter8, Filter9),
                        new string[] 
                        { 
                            //0
                            "DocType",
                            
                            //1-5
                            "DocTypeDesc", "DocNo", "DocDt", "VdName", "PODocNo", 
                            
                            //6-10
                            "TaxInvoiceNo", "TaxInvoiceDt", "Amt", "ExcRate", "CurCode",

                            //11
                            "TaxCode", "TaxName", "TaxRate"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Grd.Cells[Row, 13].Value = Sm.GetGrdDec(Grd, Row, 11) * Sm.GetGrdDec(Grd, Row, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd3.Rows.Count - 1;
                        Row2 = Row;
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 5, Grd1, Row2, 6);
                        mFrmParent.Grd3.Cells[Row1, 6].Value = Sm.GetGrdStr(Grd1, Row2, 7) + " - " + Sm.GetGrdStr(Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 7, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 8, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 9, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 10, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 11, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 12, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 14, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 16, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 17, Grd1, Row2, 17);

                        mFrmParent.Grd3.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd3, mFrmParent.Grd3.Rows.Count - 1, new int[] { 9, 10, 11, 17 });
                        mFrmParent.ComputeAmt();
                        mFrmParent.SetNo(ref mFrmParent.Grd3);
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 document.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string 
                DocType = Sm.GetGrdStr(Grd1, Row, 2),
                DocNo = Sm.GetGrdStr(Grd1, Row, 4),
                TaxCode = Sm.GetGrdStr(Grd1, Row, 15);
            
            for (int r = 0; r < mFrmParent.Grd3.Rows.Count; r++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd3, r, 2), DocType) && Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd3, r, 4), DocNo) &&
                    (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd3, r, 14), TaxCode)))
                    return true;
            
            return false;
        }

        #endregion

        #region Grid Method

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 2) == "1")
                {
                    var f = new FrmPurchaseInvoice("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 2) == "2")
                {
                    var f = new FrmPurchaseReturnInvoice("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 2) == "3")
                {
                    var f = new FrmVoucherRequestTax("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 2) == "4")
                {
                    var f = new FrmPurchaseInvoiceRawMaterial("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.ShowDialog();
                }
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 5)
            {
                e.DoDefault = false;
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 2) == "1")
                {
                    var f = new FrmPurchaseInvoice("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 2) == "2")
                {
                    var f = new FrmPurchaseReturnInvoice("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 2) == "3")
                {
                    var f = new FrmVoucherRequestTax("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 2) == "4")
                {
                    var f = new FrmPurchaseInvoiceRawMaterial("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                        Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mFrmParent.mIsFicoUseMultiProfitCenterFilter) return;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T.ProfitCenterName As Col, T.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T ");
                SQL.AppendLine("WHERE Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
                SQL.AppendLine("        Where ProfitCenterCode=T.ProfitCenterCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(") ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Method

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void DteTaxInvDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteTaxInvDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkTaxInvDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Tax Invoice Date");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit center");
        }

        #endregion

        #endregion
    }
}
