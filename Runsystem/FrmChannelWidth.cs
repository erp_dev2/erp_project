﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmChannelWidth : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmChannelWidthFind FrmFind;
        
        #endregion

        #region Constructor

        public FrmChannelWidth(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtChannelWidthCode, TxtChannelWidthName, TxtLowerMHz, TxtUpperMHz, ChkActInd }, true);
                    TxtChannelWidthCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtChannelWidthCode, TxtChannelWidthName, TxtLowerMHz, TxtUpperMHz }, false);
                    TxtChannelWidthCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtChannelWidthName, TxtLowerMHz, TxtUpperMHz, ChkActInd }, false);
                    TxtChannelWidthName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtChannelWidthCode, TxtChannelWidthName });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtLowerMHz, TxtUpperMHz }, 1 );
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmChannelWidthFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtChannelWidthCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtChannelWidthCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblChannelWidth Where ChannelWidthCode=@ChannelWidthCode" };
                Sm.CmParam<String>(ref cm, "@ChannelWidthCode", TxtChannelWidthCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblChannelWidth(ChannelWidthCode, ChannelWidthName, LowerMHz, UpperMHz, ActInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ChannelWidthCode, @ChannelWidthName, @LowerMHz, @UpperMHz, @ActInd, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ChannelWidthName=@ChannelWidthName, LowerMHz=@LowerMHz, UpperMHz=@UpperMHz, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ChannelWidthCode", TxtChannelWidthCode.Text);
                Sm.CmParam<String>(ref cm, "@ChannelWidthName", TxtChannelWidthName.Text);
                Sm.CmParam<Decimal>(ref cm, "@LowerMHz", decimal.Parse(TxtLowerMHz.Text));
                Sm.CmParam<Decimal>(ref cm, "@UpperMHz", decimal.Parse(TxtUpperMHz.Text));
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtChannelWidthCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ChannelWidthCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ChannelWidthCode", ChannelWidthCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select ChannelWidthCode, ChannelWidthName, LowerMHz, UpperMHz, ActInd From TblChannelWidth Where ChannelWidthCode=@ChannelWidthCode;",
                        new string[] { "ChannelWidthCode", "ChannelWidthName", "LowerMHz", "UpperMHz", "ActInd" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtChannelWidthCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtChannelWidthName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtLowerMHz.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 1);
                            TxtUpperMHz.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 1);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtChannelWidthCode, "Channel width code", false) ||
                Sm.IsTxtEmpty(TxtChannelWidthName, "Channel width name", false) ||
                Sm.IsTxtEmpty(TxtLowerMHz, "Lower MHz", true) ||
                Sm.IsTxtEmpty(TxtUpperMHz, "Upper MHz", true) ||
                IsCodeAlreadyExisted();
        }


        private bool IsCodeAlreadyExisted()
        {
            return 
                !TxtChannelWidthCode.Properties.ReadOnly && 
                Sm.IsDataExist(
                    "Select ChannelWidthCode From TblChannelWidth Where ChannelWidthCode=@Param;",
                    TxtChannelWidthCode.Text,
                    "Channel width code ( " + TxtChannelWidthCode.Text + " ) already existed."
                    );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtChannelWidthCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtChannelWidthCode);
        }

        private void TxtChannelWidthName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtChannelWidthName);
        }

        private void TxtLowerMHz_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLowerMHz, 1);
        }

        private void TxtUpperMHz_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtUpperMHz, 1);
        }

        #endregion

        #endregion
    }
}
