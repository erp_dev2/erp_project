﻿#region Update
/*
 * 04/11/2021 [NJP] Menambahkan Master News
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;
using System.Net;

#endregion

namespace RunSystem
{
    public partial class FrmNews : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mType = "",
            mNoOfNewsPriority = string.Empty,
            mNoOfActiveNews = string.Empty;
        internal FrmNewsFind FrmFind;
        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty
            ;
        #endregion

        #region Constructor

        public FrmNews(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            GetParameter();
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtNewsCode, TxtNewsTitle, MeeNewsLead, TxtPhoto, MeeNewsLink, DtePostingDt, DteExpiredDt, ChkPriorityInd
                    }, true);
                    BtnPhoto.Enabled = false;
                    TxtNewsCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtNewsCode, TxtNewsTitle, MeeNewsLead, TxtPhoto, MeeNewsLink, DtePostingDt, DteExpiredDt, ChkPriorityInd
                    }, false);
                    BtnPhoto.Enabled = true;
                    mType = "INSERT";
                    TxtNewsCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtNewsCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtNewsTitle, MeeNewsLead, TxtPhoto, MeeNewsLink, DtePostingDt, DteExpiredDt, ChkPriorityInd
                    }, false);
                    BtnPhoto.Enabled = true;
                    mType = "EDIT";
                    TxtNewsTitle.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtNewsCode, TxtNewsTitle, MeeNewsLead, TxtPhoto, MeeNewsLink, DtePostingDt, DteExpiredDt
            });
            ChkPriorityInd.Checked = false;

            PbUpload.Value = 0;
        }

        private void GetParameter()
        {
            mNoOfNewsPriority = Sm.GetParameter("NoOfNewsPriority");
            mNoOfActiveNews = Sm.GetParameter("NoOfActiveNews");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");

            //Default
            if (mNoOfNewsPriority.Length == 0) mNoOfNewsPriority = "3";
            if (mNoOfActiveNews.Length == 0) mNoOfActiveNews = "20";
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmNewsFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtNewsCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                    InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnPhoto_Click(object sender, EventArgs e)
        {
            try
            {
                OD.InitialDirectory = "c:";
                OD.Filter = "Image files (*.jpeg;*.png;*.BMP)|*.JPEG;*.JPG;*.PNG;*.BMP";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtPhoto.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string NewsCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@NewsCode", NewsCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblNews Where NewsCode=@NewsCode",
                        new string[] 
                        {
                            //0
                            "NewsCode", 
                            //1-5
                            "NewsTitle", "NewsLead", "Photo", "NewsLink", "PostingDt",

                            //6-10
                            "PriorityInd", "ExpiredDt" //, "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtNewsCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtNewsTitle.EditValue = Sm.DrStr(dr, c[1]);
                            MeeNewsLead.EditValue = Sm.DrStr(dr, c[2]);
                            TxtPhoto.EditValue = Sm.DrStr(dr, c[3]);
                            MeeNewsLink.EditValue = Sm.DrStr(dr, c[4]);
                            Sm.SetDte(DtePostingDt, Sm.DrStr(dr, c[5]));
                            Sm.SetDte(DteExpiredDt, Sm.DrStr(dr, c[7]));
                            ChkPriorityInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[6]), "Y") ? true : false;
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtNewsCode, "News code", false) ||
                Sm.IsTxtEmpty(TxtNewsTitle, "News Title", false) ||
                Sm.IsTxtEmpty(MeeNewsLead, "News Lead", false) ||
                Sm.IsTxtEmpty(MeeNewsLink, "News Link", false) ||
                Sm.IsTxtEmpty(DtePostingDt, "Date Posting", false) ||
                Sm.IsTxtEmpty(DteExpiredDt, "Date Expired", false) ||
                IsNewsCodeExisted() ||
                IsNewsPriorityFull() ||
                IsNewsTotalFull() ||
                IsPostingOverExpired() ||
                IsUploadFileNotValid();
        }

        private bool IsNewsCodeExisted()
        {
            if (!TxtNewsCode.Properties.ReadOnly && Sm.IsDataExist("Select NewsCode From TblNews Where NewsCode='" + TxtNewsCode.Text + "' Limit 1;"))
            {
                Sm.StdMsg(mMsgType.Warning, "News Code ( " + TxtNewsCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsNewsPriorityFull()
        {
            if (ChkPriorityInd.Checked)
            {
                int max = Int16.Parse(mNoOfNewsPriority);
                int count = 0;
                if (mType == "INSERT")
                count = Int16.Parse(Sm.GetValue("Select COUNT(NewsCode) From TblNews Where PostingDt >='" + Sm.Left(Sm.GetDte(DtePostingDt), 8) +
                        "' AND ExpiredDt <='" + Sm.Left(Sm.GetDte(DteExpiredDt), 8) + "' AND PriorityInd = 'Y' ;"));
                else if (mType == "EDIT")
                    count = Int16.Parse(Sm.GetValue("Select COUNT(NewsCode) From TblNews Where PostingDt >='" + Sm.Left(Sm.GetDte(DtePostingDt), 8) +
                        "' AND ExpiredDt <='" + Sm.Left(Sm.GetDte(DteExpiredDt), 8) + "' AND PriorityInd = 'Y' AND NewsCode <> '"+TxtNewsCode.Text+"';"));
                if (count >= max)
                {
                    Sm.StdMsg(mMsgType.Warning, "Priority News is already full.");
                    return true;
                }
                else
                    return false;
            }
            return false;
        }

        private bool IsPostingOverExpired()
        {
            string PosDt = Sm.Left(Sm.GetDte(DtePostingDt), 8);
            string ExpDt = Sm.Left(Sm.GetDte(DteExpiredDt), 8);

            if (Sm.CompareDtTm(ExpDt, PosDt)<0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Posting Date More Than Expired Date.");
                    return true;
                }
                else
                    return false;
        }

        

        private bool IsNewsTotalFull()
        {
            int max = Int16.Parse(mNoOfActiveNews);
            int count = 0;

            if (mType == "INSERT")
                count = Int16.Parse(Sm.GetValue("Select COUNT(NewsCode) From TblNews Where PostingDt >='" + Sm.Left(Sm.GetDte(DtePostingDt), 8) +
                        "' AND ExpiredDt <='" + Sm.Left(Sm.GetDte(DteExpiredDt), 8) + "';"));
            else if (mType == "EDIT")
                count = Int16.Parse(Sm.GetValue("Select COUNT(NewsCode) From TblNews Where PostingDt >='" + Sm.Left(Sm.GetDte(DtePostingDt), 8) +
                    "' AND ExpiredDt <='" + Sm.Left(Sm.GetDte(DteExpiredDt), 8) + "' AND NewsCode <> '" + TxtNewsCode.Text + "';"));

            if (count >= max)
            {

                Sm.StdMsg(mMsgType.Warning, "Priority News is already full.");
                return true;
            }
            else
                return false;
        }

        

        private bool IsUploadFileNotValid()
        {
            return
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }


        private void InsertData(object sender, EventArgs e)
        {
            string LocalDocNo = string.Empty;

            if (Sm.StdMsgYN("Question",
                    "Do you want to save this data ?"
                    ) == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblNews(  ");
            SQL.AppendLine("NewsCode, PriorityInd, NewsTitle, NewsLead, NewsLink, PostingDt, ExpiredDt, "); 
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values( ");
            SQL.AppendLine("@NewsCode, @PriorityInd, @NewsTitle, @NewsLead, @NewsLink, @PostingDt, @ExpiredDt, ");
            SQL.AppendLine(" @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update PriorityInd=@PriorityInd, NewsTitle=@NewsTitle, NewsLead=@NewsLead, ");
            SQL.AppendLine("    NewsLink=@NewsLink, PostingDt=@PostingDt, ExpiredDt=@ExpiredDt, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@NewsCode", TxtNewsCode.Text);
            Sm.CmParam<String>(ref cm, "@PriorityInd", ChkPriorityInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@NewsTitle", TxtNewsTitle.Text);
            Sm.CmParam<String>(ref cm, "@NewsLead", MeeNewsLead.Text);
            //Sm.CmParam<String>(ref cm, "@Photo", TxtPhoto.Text);
            Sm.CmParam<String>(ref cm, "@NewsLink", MeeNewsLink.Text);
            Sm.CmParamDt(ref cm, "@PostingDt", Sm.GetDte(DtePostingDt));
            Sm.CmParamDt(ref cm, "@ExpiredDt", Sm.GetDte(DteExpiredDt));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
            if (TxtPhoto.Text.Length > 0 && 
                TxtPhoto.Text != "openFileDialog1" &&
                TxtPhoto.Text != Sm.GetValue("Select Photo from tblNews where NewsCode = '"+TxtNewsCode.Text+"';"))
                UploadFile(TxtNewsCode.Text);
            Sm.StdMsg(mMsgType.Info, "News saved successfully");
            ShowData(TxtNewsCode.Text);

        }

        private void UploadFile(string Code)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtPhoto.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtPhoto.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateNews(Code, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsFTPClientDataNotValid()
        {

            if (TxtPhoto.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (TxtPhoto.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (TxtPhoto.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (TxtPhoto.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (TxtPhoto.Text.Length > 0 && TxtPhoto.Text != Sm.GetValue("Select Photo from tblNews where NewsCode = '" + TxtNewsCode.Text + "';"))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtPhoto.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select Photo From TblNews ");
                SQL.AppendLine("Where Photo=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (TxtPhoto.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtPhoto.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtPhoto.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand UpdateNews(string Code, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblNews Set ");
            SQL.AppendLine("    Photo=@Photo ");
            SQL.AppendLine("Where NewsCode=@NewsCode  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@NewsCode", Code);
            Sm.CmParam<String>(ref cm, "@Photo", FileName);

            return cm;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtNewsCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtNewsCode);
        }

        private void TxtNewsTitle_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtNewsTitle);
        }

        private void MeeNewsLead_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(MeeNewsLead);
        }

        private void MeeNewsLink_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(MeeNewsLink);
        }

        #endregion

        #endregion

    }
}
