﻿#region Update
/*
    27/09/2017 [HAR] tambah voucher dari ap dp 
    27/11/2017 [HAR] 1 PO banyak PI, maka muncul sesuai jumlah PI nya 
    13/08/2018 [TKG] tambah vendor, PO without Tax
    27/08/2018 [TKG] tambah grand total PO
    30/10/2018 [TKG] giro# dan due date diambil dari voucher.
    19/11/2018 [WED] RFC KMI minta kolom amount nya, muncul nominal cuma sekali untuk nomor PO yang sama berdasarkan parameter IsRptPOToVoucherDistinctAmt
    30/11/2018 [TKG] tambah currency
    19/02/2020 [HAR/TWC] PO yang berasal dari receiving without PO yang PO ind = N (tergenerate otomatis) minta dimunculkan, button untuk ke form receiving
    21/05/2020 [TKG/SRN] bug PO yg dicancel masih muncul
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPOToVoucher : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsRptPOToVoucherDistinctAmt = false;

        #endregion

        #region Constructor

        public FrmRptPOToVoucher(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select X.PoDocNo, X.PODocDt, X1.DocNo As APDocNo, X1.DocDt As APDocDt, ");
            SQL.AppendLine("    X2.VCDocnoAP, X2.VCDocDtAP, X.RecvVdDocno, X.RecvVdDocDt, X.PIDocNo, X.PIDocDt, X.OpDocNo, X.OpDocDt, X.VCDocNo, X.VcDocDt,  ");
            SQL.AppendLine("    X4.VdName, (X3.Amt-X3.TaxAmt) As AmtBeforeTax, X3.Amt, X.PurchaseInvoiceAmt, X2.GiroNo As DPGiroNo, X2.DueDt As DPDueDt, X5.GiroNo As OPGiroNo, X5.DueDt As OPDueDt, ");
            SQL.AppendLine("    X3.CurCode As POCurCode, X.PurchaseInvoiceCurCode ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        select Distinct A.DocNo PODocNo, A.DocDt PODocDt, B.RecvVdDocNo, B.RecvVdDocDt, C.PIDocNo,  C.PIDocDt, C.PurchaseInvoiceAmt, ");
            SQL.AppendLine("        C.OpDocNo, C.OPDocDt, C.VrDocNo, C.VRDocDt, C.VCDocNo, C.VCDocDt, C.CurCode As PurchaseInvoiceCurCode ");
            SQL.AppendLine("        From TblPOHdr A ");
            SQL.AppendLine("        left Join  ");
            SQL.AppendLine("        ( ");
	        SQL.AppendLine("            Select Distinct T2.PoDocNo, T1.DoCNo As RecvVdDocNo, T1.DocDt As recvVdDocDt  ");
	        SQL.AppendLine("            from TblRecvVdHdr T1  ");
            SQL.AppendLine("            Inner Join TblRecvVdDtl T2 On T1.Docno=T2.DocNo And T2.Cancelind = 'N' And T2.Status In ('O', 'A') ");
            SQL.AppendLine("            Inner Join TblPOHdr T3 On T2.PODocNo=T3.DocNo And T3.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("            Where A.PoInd = 'Y' And ");
            SQL.AppendLine("        )B On A.DocNo = B.PODocNo ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
	        SQL.AppendLine("            Select W2.RecvVdDocno, W.DocNo As PIDocNo, W.DocDt As PIDocDt, W.CurCode, X.OpDocNo, X.OpDocDt,  ");
	        SQL.AppendLine("            X.VrDocNo, X.VRDocDt, X.VCDocNo,  X.VCDocDt, W.Amt+W.TaxAmt-W.Downpayment As PurchaseInvoiceAmt   ");
	        SQL.AppendLine("            From TblPurchaseinvoicehdr W ");
	        SQL.AppendLine("            Inner Join  ");
	        SQL.AppendLine("            (	 ");
		    SQL.AppendLine("                select distinct RecvVdDocno, DocNo  from TblPurchaseinvoicedtl  ");
	        SQL.AppendLine("            )W2 On W.DocNo = W2.DocNO  ");
	        SQL.AppendLine("            left Join  ");
	        SQL.AppendLine("            ( ");
		    SQL.AppendLine("                Select Y2.invoiceDocNo, Y.DocNo As OpDocNo,  Y.DocDt As OPDocDt, Z.VRDocno, Z.VRDocDt, Z.VCDocno, Z.VCDocDt "); 
		    SQL.AppendLine("                From tbloutgoingpaymentHdr Y ");
		    SQL.AppendLine("                Inner Join TblOutgoingpaymentDtl Y2  on Y.Docno = Y2.DocNo  ");
		    SQL.AppendLine("                Left Join  ");
		    SQL.AppendLine("                ( ");
            SQL.AppendLine("                    Select A.DocNo As VRDocNo, A.DocDt VRDocDt, B.Docno As VCDocno, B.DocDt As VCDocDt ");
			SQL.AppendLine("                    From tblVoucherRequesthdr A ");
			SQL.AppendLine("                    Left Join TblVoucherHdr B On A.VoucherDocno = B.DocNo And B.Cancelind = 'N'  ");
			SQL.AppendLine("                    Where A.CancelInd = 'N' And Status = 'A' ");
		    SQL.AppendLine("                ) Z On Y.VoucherRequestDocNo = Z.VRDocNo ");
		    SQL.AppendLine("                Where Y.cancelInd = 'N' ");
	        SQL.AppendLine("            )X On W.DocNo = X.InvoiceDocno ");
	        SQL.AppendLine("            Where W.CancelInd = 'N' ");
            SQL.AppendLine("        )C On B.recvVdDocNo = C.RecvVdDocno ");
            SQL.AppendLine("        Inner Join (");
            SQL.AppendLine("            Select T1.DocNo From TblPOHdr T1, TblPODtl T2 ");
            SQL.AppendLine("            Where T1.DocNo=T2.DocNo And T2.CancelInd='N' And T1.Status In ('O', 'A') And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        ) D On A.DocNo=D.DocNo ");
            SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) "); 
            SQL.AppendLine("    )X ");
            SQL.AppendLine("    Left Join TblApDownpayment X1 On X.PODocNo = X1.PODocNo And X1.CancelInd = 'N' And X1.Status = 'A' ");
            SQL.AppendLine("    Left Join  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.DocNo VrDocNo, B.DocNo VcDocnoAP, B.DocDt VCDocDtAP, B.GiroNo, B.DueDt ");
	        SQL.AppendLine("        From tblVoucherRequestHdr A ");
	        SQL.AppendLine("        Inner Join TblVoucherhdr B On A.VoucherDocNo = B.Docno ");
	        SQL.AppendLine("        Where A.CancelInd = 'N' And A.DocType='04' ");
            SQL.AppendLine("    ) X2 On X1.VoucherRequestDocNo = X2.VrDocno  ");
            SQL.AppendLine("    Inner Join TblPOHdr X3 On X.PODocNo=X3.DocNo And X3.Status In ('O', 'A') ");
            SQL.AppendLine("    Inner Join TblVendor X4 On X3.VdCode=X4.VdCode ");
            SQL.AppendLine("    Left Join TblVoucherHdr X5 On X.VcDocNo=X5.DocNo ");
            SQL.AppendLine(") Y ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 32;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "PO#",
                        "",
                        "PO"+Environment.NewLine+"Date",
                        "AP DP",
                        "",

                        //6-10
                        "AP DP"+Environment.NewLine+"Date",
                        "Voucher"+Environment.NewLine+"(AP DP)",
                        "",
                        "Voucher Date"+Environment.NewLine+"(AP DP)",
                        "Received#",

                        //11-15
                        "",
                        "Received Date",
                        "PI",
                        "",
                        "PI"+Environment.NewLine+"Date",

                        //16-20
                        "OP",
                        "",
                        "OP"+Environment.NewLine+"Date",
                        "Voucher"+Environment.NewLine+"(OP)",
                        "",

                        //21-25
                        "Voucher Date"+Environment.NewLine+"(OP)",
                        "Vendor",
                        "PO's"+Environment.NewLine+"Total Before Tax",
                        "PO's"+Environment.NewLine+"Total After Tax",
                        "PI's Amount",

                        //26-30
                        "DP Voucher's BG/Cheque",
                        "DP Voucher's BG/Cheque"+Environment.NewLine+"Due Date",
                        "OP Voucher's BG/Cheque",
                        "OP Voucher's BG/Cheque"+Environment.NewLine+"Due Date",
                        "Currency",

                        //31
                        "Currency"
                    },
                    new int[] 
                    {
                        50,
                        150, 20, 120, 150, 20, 
                        120, 150, 20, 120, 150, 
                        20, 100, 150, 20, 120, 
                        150, 20, 120, 150, 20, 
                        120, 200, 150, 150, 150,
                        150, 150, 150, 150, 60,
                        60
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 5, 8, 11, 14, 17, 20});
            Sm.GrdFormatDate(Grd1, new int[] { 3, 6, 9, 12, 15, 18, 21, 27, 29 });
            Sm.GrdFormatDec(Grd1, new int[] { 23, 24, 25 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 6, 7, 9, 10, 12, 13, 15, 16, 18, 19, 21, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 }, false);
            Grd1.Cols[22].Move(4);
            Grd1.Cols[23].Move(5);
            Grd1.Cols[24].Move(6);
            Grd1.Cols[26].Move(10);
            Grd1.Cols[27].Move(11);
            Grd1.Cols[28].Move(12);
            Grd1.Cols[29].Move(13);
            Grd1.Cols[30].Move(5);
            Grd1.Cols[31].Move(30);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 1=1 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtPODocNo.Text, "Y.PODocNo", false);
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + " Order By Y.PODocDt, Y.PODocNo;",
                    new string[]
                    { 
                        //0
                        "PODocNo", 
                        //1-5
                        "PODocDt", "APDocNo", "APDocDt", "VCDocnoAP", "VCDocDtAP", 
                        //6-10
                        "RecvvdDocNo", "RecvVdDocDt", "PIDocNo", "PIDocDt", "OPDocNo",  
                        //11-15
                        "OPDocDt", "VCDocNo", "VcDocDt", "VdName", "AmtBeforeTax",
                        //16-20
                        "Amt", "PurchaseInvoiceAmt", "DPGiroNo", "DPDueDt", "OPGiroNo", 
                        //21-23
                        "OPDueDt", "POCurCode", "PurchaseInvoiceCurCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 8);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 10);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 12);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 18);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 27, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 20);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 29, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 23);
                    }, true, false, false, false
                );
                
                if(mIsRptPOToVoucherDistinctAmt) ProcessDuplicateAmt();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPO(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAPDownpayment(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucher(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 10).Contains("RVPO"))
                {
                    var f = new FrmRecvVd(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmRecvVd2(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                f.ShowDialog();
            }

            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmOutgoingPayment(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 16);
                f.ShowDialog();
            }
            if (e.ColIndex == 20 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucher(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }

        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPO(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmAPDownpayment(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 10).Contains("RVPO"))
                {
                    var f = new FrmRecvVd(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmRecvVd2(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length != 0)
            {
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                f.ShowDialog();
            }
            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length != 0)
            {
                var f = new FrmOutgoingPayment(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 16);
                f.ShowDialog();
            }
            if (e.ColIndex == 20 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length != 0)
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsRptPOToVoucherDistinctAmt = Sm.GetParameterBoo("IsRptPOToVoucherDistinctAmt");
        }

        private void ProcessDuplicateAmt()
        {
            if (Grd1.Rows.Count > 0)
            {
                string mDocNo = string.Empty;
                int mIndex = 0;

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    {
                        if (mDocNo.Length <= 0) mDocNo = Sm.GetGrdStr(Grd1, Row, 1);

                        if (mDocNo != Sm.GetGrdStr(Grd1, Row, 1))
                        {
                            mDocNo = Sm.GetGrdStr(Grd1, Row, 1);
                            mIndex = 0;
                        }

                        if (mIndex != 0)
                        {
                            Grd1.Cells[Row, 23].Value =
                            Grd1.Cells[Row, 24].Value =
                            Grd1.Cells[Row, 25].Value = 0m;
                        }

                        mIndex += 1;
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtPODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Purchase Order#");
        }

        #endregion

        #endregion
    }
}
