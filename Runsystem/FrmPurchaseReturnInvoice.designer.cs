﻿namespace RunSystem
{
    partial class FrmPurchaseReturnInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TcPurchaseReturnInvoice = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.label28 = new System.Windows.Forms.Label();
            this.BtnVoucher = new DevExpress.XtraEditors.SimpleButton();
            this.BtnPurchaseInvoice = new DevExpress.XtraEditors.SimpleButton();
            this.BtnRecvVd = new DevExpress.XtraEditors.SimpleButton();
            this.TxtRecvVd = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.TxtVoucher = new DevExpress.XtraEditors.TextEdit();
            this.TxtPurchaseInvoice = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.LueServiceCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.LueServiceCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.LueServiceCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtTaxInvoiceNo3 = new DevExpress.XtraEditors.TextEdit();
            this.DteTaxInvoiceDt3 = new DevExpress.XtraEditors.DateEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtTaxInvoiceNo2 = new DevExpress.XtraEditors.TextEdit();
            this.DteTaxInvoiceDt2 = new DevExpress.XtraEditors.DateEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtVoucherRequestPPNDocNo3 = new DevExpress.XtraEditors.TextEdit();
            this.TxtVoucherRequestPPNDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtVoucherRequestPPNDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtJournalDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtJournalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtAmtBefTax = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtTaxAmt3 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTaxAmt2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTaxAmt1 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTaxCode3 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTaxCode2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTaxCode1 = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtCurCode = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtTaxInvoiceNo = new DevExpress.XtraEditors.TextEdit();
            this.DteTaxInvoiceDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtSiteCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LueVdCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp4 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcPurchaseReturnInvoice)).BeginInit();
            this.TcPurchaseReturnInvoice.SuspendLayout();
            this.Tp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecvVd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPurchaseInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueServiceCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueServiceCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueServiceCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestPPNDocNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestPPNDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestPPNDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmtBefTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.Tp3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.Tp4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Location = new System.Drawing.Point(955, 0);
            this.panel1.Size = new System.Drawing.Size(70, 473);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcPurchaseReturnInvoice);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(955, 473);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(955, 32);
            this.panel3.TabIndex = 10;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(85, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(226, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(355, 6);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(118, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(317, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TcPurchaseReturnInvoice
            // 
            this.TcPurchaseReturnInvoice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcPurchaseReturnInvoice.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcPurchaseReturnInvoice.Location = new System.Drawing.Point(0, 32);
            this.TcPurchaseReturnInvoice.Name = "TcPurchaseReturnInvoice";
            this.TcPurchaseReturnInvoice.SelectedTabPage = this.Tp1;
            this.TcPurchaseReturnInvoice.Size = new System.Drawing.Size(955, 441);
            this.TcPurchaseReturnInvoice.TabIndex = 15;
            this.TcPurchaseReturnInvoice.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2,
            this.Tp3,
            this.Tp4});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.label28);
            this.Tp1.Controls.Add(this.BtnVoucher);
            this.Tp1.Controls.Add(this.BtnPurchaseInvoice);
            this.Tp1.Controls.Add(this.BtnRecvVd);
            this.Tp1.Controls.Add(this.TxtRecvVd);
            this.Tp1.Controls.Add(this.label25);
            this.Tp1.Controls.Add(this.TxtVoucher);
            this.Tp1.Controls.Add(this.TxtPurchaseInvoice);
            this.Tp1.Controls.Add(this.label26);
            this.Tp1.Controls.Add(this.label51);
            this.Tp1.Controls.Add(this.LueServiceCode3);
            this.Tp1.Controls.Add(this.label50);
            this.Tp1.Controls.Add(this.LueServiceCode2);
            this.Tp1.Controls.Add(this.label49);
            this.Tp1.Controls.Add(this.LueServiceCode1);
            this.Tp1.Controls.Add(this.label21);
            this.Tp1.Controls.Add(this.label22);
            this.Tp1.Controls.Add(this.TxtTaxInvoiceNo3);
            this.Tp1.Controls.Add(this.DteTaxInvoiceDt3);
            this.Tp1.Controls.Add(this.label23);
            this.Tp1.Controls.Add(this.label24);
            this.Tp1.Controls.Add(this.label14);
            this.Tp1.Controls.Add(this.label15);
            this.Tp1.Controls.Add(this.TxtTaxInvoiceNo2);
            this.Tp1.Controls.Add(this.DteTaxInvoiceDt2);
            this.Tp1.Controls.Add(this.label18);
            this.Tp1.Controls.Add(this.label20);
            this.Tp1.Controls.Add(this.label13);
            this.Tp1.Controls.Add(this.TxtVoucherRequestPPNDocNo3);
            this.Tp1.Controls.Add(this.TxtVoucherRequestPPNDocNo2);
            this.Tp1.Controls.Add(this.TxtVoucherRequestPPNDocNo);
            this.Tp1.Controls.Add(this.label12);
            this.Tp1.Controls.Add(this.label17);
            this.Tp1.Controls.Add(this.TxtJournalDocNo2);
            this.Tp1.Controls.Add(this.TxtJournalDocNo);
            this.Tp1.Controls.Add(this.label11);
            this.Tp1.Controls.Add(this.TxtAmtBefTax);
            this.Tp1.Controls.Add(this.label10);
            this.Tp1.Controls.Add(this.TxtTaxAmt3);
            this.Tp1.Controls.Add(this.TxtTaxAmt2);
            this.Tp1.Controls.Add(this.TxtTaxAmt1);
            this.Tp1.Controls.Add(this.TxtTaxCode3);
            this.Tp1.Controls.Add(this.TxtTaxCode2);
            this.Tp1.Controls.Add(this.TxtTaxCode1);
            this.Tp1.Controls.Add(this.label9);
            this.Tp1.Controls.Add(this.TxtCurCode);
            this.Tp1.Controls.Add(this.label7);
            this.Tp1.Controls.Add(this.TxtTaxInvoiceNo);
            this.Tp1.Controls.Add(this.DteTaxInvoiceDt);
            this.Tp1.Controls.Add(this.label4);
            this.Tp1.Controls.Add(this.TxtSiteCode);
            this.Tp1.Controls.Add(this.label3);
            this.Tp1.Controls.Add(this.label27);
            this.Tp1.Controls.Add(this.MeeRemark);
            this.Tp1.Controls.Add(this.label5);
            this.Tp1.Controls.Add(this.TxtAmt);
            this.Tp1.Controls.Add(this.label6);
            this.Tp1.Controls.Add(this.label19);
            this.Tp1.Controls.Add(this.MeeCancelReason);
            this.Tp1.Controls.Add(this.label16);
            this.Tp1.Controls.Add(this.LueDeptCode);
            this.Tp1.Controls.Add(this.ChkCancelInd);
            this.Tp1.Controls.Add(this.label8);
            this.Tp1.Controls.Add(this.LueVdCode);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(949, 413);
            this.Tp1.Text = "General";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(706, 50);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 14);
            this.label28.TabIndex = 77;
            this.label28.Text = "Voucher";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnVoucher
            // 
            this.BtnVoucher.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucher.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucher.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucher.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucher.Appearance.Options.UseBackColor = true;
            this.BtnVoucher.Appearance.Options.UseFont = true;
            this.BtnVoucher.Appearance.Options.UseForeColor = true;
            this.BtnVoucher.Appearance.Options.UseTextOptions = true;
            this.BtnVoucher.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucher.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucher.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnVoucher.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnVoucher.Location = new System.Drawing.Point(919, 45);
            this.BtnVoucher.Name = "BtnVoucher";
            this.BtnVoucher.Size = new System.Drawing.Size(24, 20);
            this.BtnVoucher.TabIndex = 79;
            this.BtnVoucher.ToolTip = "Show List of Voucher";
            this.BtnVoucher.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucher.ToolTipTitle = "Run System";
            this.BtnVoucher.Click += new System.EventHandler(this.BtnVoucher_Click);
            // 
            // BtnPurchaseInvoice
            // 
            this.BtnPurchaseInvoice.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPurchaseInvoice.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPurchaseInvoice.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPurchaseInvoice.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPurchaseInvoice.Appearance.Options.UseBackColor = true;
            this.BtnPurchaseInvoice.Appearance.Options.UseFont = true;
            this.BtnPurchaseInvoice.Appearance.Options.UseForeColor = true;
            this.BtnPurchaseInvoice.Appearance.Options.UseTextOptions = true;
            this.BtnPurchaseInvoice.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPurchaseInvoice.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPurchaseInvoice.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnPurchaseInvoice.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnPurchaseInvoice.Location = new System.Drawing.Point(919, 24);
            this.BtnPurchaseInvoice.Name = "BtnPurchaseInvoice";
            this.BtnPurchaseInvoice.Size = new System.Drawing.Size(24, 20);
            this.BtnPurchaseInvoice.TabIndex = 76;
            this.BtnPurchaseInvoice.ToolTip = "Show Purchase Invoice";
            this.BtnPurchaseInvoice.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPurchaseInvoice.ToolTipTitle = "Run System";
            this.BtnPurchaseInvoice.Click += new System.EventHandler(this.BtnPurchaseInvoice_Click);
            // 
            // BtnRecvVd
            // 
            this.BtnRecvVd.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRecvVd.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRecvVd.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRecvVd.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRecvVd.Appearance.Options.UseBackColor = true;
            this.BtnRecvVd.Appearance.Options.UseFont = true;
            this.BtnRecvVd.Appearance.Options.UseForeColor = true;
            this.BtnRecvVd.Appearance.Options.UseTextOptions = true;
            this.BtnRecvVd.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRecvVd.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRecvVd.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnRecvVd.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnRecvVd.Location = new System.Drawing.Point(919, 3);
            this.BtnRecvVd.Name = "BtnRecvVd";
            this.BtnRecvVd.Size = new System.Drawing.Size(24, 20);
            this.BtnRecvVd.TabIndex = 73;
            this.BtnRecvVd.ToolTip = "Show Receiving Item From Vendor";
            this.BtnRecvVd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRecvVd.ToolTipTitle = "Run System";
            this.BtnRecvVd.Click += new System.EventHandler(this.BtnRecvVd_Click);
            // 
            // TxtRecvVd
            // 
            this.TxtRecvVd.EnterMoveNextControl = true;
            this.TxtRecvVd.Location = new System.Drawing.Point(761, 5);
            this.TxtRecvVd.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRecvVd.Name = "TxtRecvVd";
            this.TxtRecvVd.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRecvVd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRecvVd.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRecvVd.Properties.Appearance.Options.UseFont = true;
            this.TxtRecvVd.Properties.MaxLength = 30;
            this.TxtRecvVd.Properties.ReadOnly = true;
            this.TxtRecvVd.Size = new System.Drawing.Size(155, 20);
            this.TxtRecvVd.TabIndex = 72;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(595, 8);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(163, 14);
            this.label25.TabIndex = 71;
            this.label25.Text = "Receiving Item From Vendor";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucher
            // 
            this.TxtVoucher.EnterMoveNextControl = true;
            this.TxtVoucher.Location = new System.Drawing.Point(761, 47);
            this.TxtVoucher.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucher.Name = "TxtVoucher";
            this.TxtVoucher.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucher.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucher.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucher.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucher.Properties.MaxLength = 30;
            this.TxtVoucher.Properties.ReadOnly = true;
            this.TxtVoucher.Size = new System.Drawing.Size(155, 20);
            this.TxtVoucher.TabIndex = 78;
            // 
            // TxtPurchaseInvoice
            // 
            this.TxtPurchaseInvoice.EnterMoveNextControl = true;
            this.TxtPurchaseInvoice.Location = new System.Drawing.Point(761, 26);
            this.TxtPurchaseInvoice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPurchaseInvoice.Name = "TxtPurchaseInvoice";
            this.TxtPurchaseInvoice.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPurchaseInvoice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPurchaseInvoice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPurchaseInvoice.Properties.Appearance.Options.UseFont = true;
            this.TxtPurchaseInvoice.Properties.MaxLength = 30;
            this.TxtPurchaseInvoice.Properties.ReadOnly = true;
            this.TxtPurchaseInvoice.Size = new System.Drawing.Size(155, 20);
            this.TxtPurchaseInvoice.TabIndex = 75;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(660, 29);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(99, 14);
            this.label26.TabIndex = 74;
            this.label26.Text = "Purchase Invoice";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(551, 217);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(46, 14);
            this.label51.TabIndex = 53;
            this.label51.Text = "Service";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueServiceCode3
            // 
            this.LueServiceCode3.EnterMoveNextControl = true;
            this.LueServiceCode3.Location = new System.Drawing.Point(602, 214);
            this.LueServiceCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueServiceCode3.Name = "LueServiceCode3";
            this.LueServiceCode3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueServiceCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode3.Properties.Appearance.Options.UseBackColor = true;
            this.LueServiceCode3.Properties.Appearance.Options.UseFont = true;
            this.LueServiceCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueServiceCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueServiceCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueServiceCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueServiceCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueServiceCode3.Properties.DropDownRows = 25;
            this.LueServiceCode3.Properties.NullText = "[Empty]";
            this.LueServiceCode3.Properties.PopupWidth = 200;
            this.LueServiceCode3.Properties.ReadOnly = true;
            this.LueServiceCode3.Size = new System.Drawing.Size(147, 20);
            this.LueServiceCode3.TabIndex = 54;
            this.LueServiceCode3.ToolTip = "F4 : Show/hide list";
            this.LueServiceCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueServiceCode3.EditValueChanged += new System.EventHandler(this.LueServiceCode3_EditValueChanged);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(551, 174);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(46, 14);
            this.label50.TabIndex = 43;
            this.label50.Text = "Service";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueServiceCode2
            // 
            this.LueServiceCode2.EnterMoveNextControl = true;
            this.LueServiceCode2.Location = new System.Drawing.Point(602, 172);
            this.LueServiceCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueServiceCode2.Name = "LueServiceCode2";
            this.LueServiceCode2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueServiceCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode2.Properties.Appearance.Options.UseBackColor = true;
            this.LueServiceCode2.Properties.Appearance.Options.UseFont = true;
            this.LueServiceCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueServiceCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueServiceCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueServiceCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueServiceCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueServiceCode2.Properties.DropDownRows = 25;
            this.LueServiceCode2.Properties.NullText = "[Empty]";
            this.LueServiceCode2.Properties.PopupWidth = 200;
            this.LueServiceCode2.Properties.ReadOnly = true;
            this.LueServiceCode2.Size = new System.Drawing.Size(147, 20);
            this.LueServiceCode2.TabIndex = 44;
            this.LueServiceCode2.ToolTip = "F4 : Show/hide list";
            this.LueServiceCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueServiceCode2.EditValueChanged += new System.EventHandler(this.LueServiceCode2_EditValueChanged);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(551, 131);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(46, 14);
            this.label49.TabIndex = 33;
            this.label49.Text = "Service";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueServiceCode1
            // 
            this.LueServiceCode1.EnterMoveNextControl = true;
            this.LueServiceCode1.Location = new System.Drawing.Point(602, 130);
            this.LueServiceCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueServiceCode1.Name = "LueServiceCode1";
            this.LueServiceCode1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueServiceCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode1.Properties.Appearance.Options.UseBackColor = true;
            this.LueServiceCode1.Properties.Appearance.Options.UseFont = true;
            this.LueServiceCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueServiceCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueServiceCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueServiceCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueServiceCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueServiceCode1.Properties.DropDownRows = 25;
            this.LueServiceCode1.Properties.NullText = "[Empty]";
            this.LueServiceCode1.Properties.PopupWidth = 200;
            this.LueServiceCode1.Properties.ReadOnly = true;
            this.LueServiceCode1.Size = new System.Drawing.Size(147, 20);
            this.LueServiceCode1.TabIndex = 34;
            this.LueServiceCode1.ToolTip = "F4 : Show/hide list";
            this.LueServiceCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueServiceCode1.EditValueChanged += new System.EventHandler(this.LueServiceCode1_EditValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(373, 238);
            this.label21.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(51, 14);
            this.label21.TabIndex = 57;
            this.label21.Text = "Amount";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(134, 238);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 14);
            this.label22.TabIndex = 55;
            this.label22.Text = "Tax";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxInvoiceNo3
            // 
            this.TxtTaxInvoiceNo3.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo3.Location = new System.Drawing.Point(166, 214);
            this.TxtTaxInvoiceNo3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo3.Name = "TxtTaxInvoiceNo3";
            this.TxtTaxInvoiceNo3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo3.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo3.Size = new System.Drawing.Size(197, 20);
            this.TxtTaxInvoiceNo3.TabIndex = 50;
            // 
            // DteTaxInvoiceDt3
            // 
            this.DteTaxInvoiceDt3.EditValue = null;
            this.DteTaxInvoiceDt3.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt3.Location = new System.Drawing.Point(428, 214);
            this.DteTaxInvoiceDt3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt3.Name = "DteTaxInvoiceDt3";
            this.DteTaxInvoiceDt3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteTaxInvoiceDt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.Appearance.Options.UseBackColor = true;
            this.DteTaxInvoiceDt3.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt3.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt3.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt3.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt3.Properties.ReadOnly = true;
            this.DteTaxInvoiceDt3.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt3.Size = new System.Drawing.Size(118, 20);
            this.DteTaxInvoiceDt3.TabIndex = 52;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(82, 218);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(79, 14);
            this.label23.TabIndex = 49;
            this.label23.Text = "Tax Invoice#";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(367, 217);
            this.label24.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(57, 14);
            this.label24.TabIndex = 51;
            this.label24.Text = "Tax Date";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(373, 196);
            this.label14.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 14);
            this.label14.TabIndex = 47;
            this.label14.Text = "Amount";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(134, 197);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 14);
            this.label15.TabIndex = 45;
            this.label15.Text = "Tax";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxInvoiceNo2
            // 
            this.TxtTaxInvoiceNo2.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo2.Location = new System.Drawing.Point(166, 172);
            this.TxtTaxInvoiceNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo2.Name = "TxtTaxInvoiceNo2";
            this.TxtTaxInvoiceNo2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo2.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo2.Size = new System.Drawing.Size(197, 20);
            this.TxtTaxInvoiceNo2.TabIndex = 40;
            // 
            // DteTaxInvoiceDt2
            // 
            this.DteTaxInvoiceDt2.EditValue = null;
            this.DteTaxInvoiceDt2.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt2.Location = new System.Drawing.Point(428, 172);
            this.DteTaxInvoiceDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt2.Name = "DteTaxInvoiceDt2";
            this.DteTaxInvoiceDt2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteTaxInvoiceDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.Appearance.Options.UseBackColor = true;
            this.DteTaxInvoiceDt2.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt2.Properties.ReadOnly = true;
            this.DteTaxInvoiceDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt2.Size = new System.Drawing.Size(118, 20);
            this.DteTaxInvoiceDt2.TabIndex = 42;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(82, 176);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(79, 14);
            this.label18.TabIndex = 39;
            this.label18.Text = "Tax Invoice#";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(367, 175);
            this.label20.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 14);
            this.label20.TabIndex = 41;
            this.label20.Text = "Tax Date";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(373, 154);
            this.label13.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(51, 14);
            this.label13.TabIndex = 37;
            this.label13.Text = "Amount";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherRequestPPNDocNo3
            // 
            this.TxtVoucherRequestPPNDocNo3.EnterMoveNextControl = true;
            this.TxtVoucherRequestPPNDocNo3.Location = new System.Drawing.Point(166, 341);
            this.TxtVoucherRequestPPNDocNo3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestPPNDocNo3.Name = "TxtVoucherRequestPPNDocNo3";
            this.TxtVoucherRequestPPNDocNo3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestPPNDocNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestPPNDocNo3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestPPNDocNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestPPNDocNo3.Properties.MaxLength = 30;
            this.TxtVoucherRequestPPNDocNo3.Properties.ReadOnly = true;
            this.TxtVoucherRequestPPNDocNo3.Size = new System.Drawing.Size(197, 20);
            this.TxtVoucherRequestPPNDocNo3.TabIndex = 66;
            // 
            // TxtVoucherRequestPPNDocNo2
            // 
            this.TxtVoucherRequestPPNDocNo2.EnterMoveNextControl = true;
            this.TxtVoucherRequestPPNDocNo2.Location = new System.Drawing.Point(166, 320);
            this.TxtVoucherRequestPPNDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestPPNDocNo2.Name = "TxtVoucherRequestPPNDocNo2";
            this.TxtVoucherRequestPPNDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestPPNDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestPPNDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestPPNDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestPPNDocNo2.Properties.MaxLength = 30;
            this.TxtVoucherRequestPPNDocNo2.Properties.ReadOnly = true;
            this.TxtVoucherRequestPPNDocNo2.Size = new System.Drawing.Size(197, 20);
            this.TxtVoucherRequestPPNDocNo2.TabIndex = 65;
            // 
            // TxtVoucherRequestPPNDocNo
            // 
            this.TxtVoucherRequestPPNDocNo.EnterMoveNextControl = true;
            this.TxtVoucherRequestPPNDocNo.Location = new System.Drawing.Point(166, 299);
            this.TxtVoucherRequestPPNDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestPPNDocNo.Name = "TxtVoucherRequestPPNDocNo";
            this.TxtVoucherRequestPPNDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestPPNDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestPPNDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestPPNDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestPPNDocNo.Properties.MaxLength = 30;
            this.TxtVoucherRequestPPNDocNo.Properties.ReadOnly = true;
            this.TxtVoucherRequestPPNDocNo.Size = new System.Drawing.Size(197, 20);
            this.TxtVoucherRequestPPNDocNo.TabIndex = 64;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(22, 301);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(139, 14);
            this.label12.TabIndex = 63;
            this.label12.Text = "Voucher Request VAT#";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(368, 385);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 14);
            this.label17.TabIndex = 70;
            this.label17.Text = "(Cancel Data)";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtJournalDocNo2
            // 
            this.TxtJournalDocNo2.EnterMoveNextControl = true;
            this.TxtJournalDocNo2.Location = new System.Drawing.Point(166, 383);
            this.TxtJournalDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo2.Name = "TxtJournalDocNo2";
            this.TxtJournalDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo2.Properties.MaxLength = 30;
            this.TxtJournalDocNo2.Properties.ReadOnly = true;
            this.TxtJournalDocNo2.Size = new System.Drawing.Size(197, 20);
            this.TxtJournalDocNo2.TabIndex = 69;
            // 
            // TxtJournalDocNo
            // 
            this.TxtJournalDocNo.EnterMoveNextControl = true;
            this.TxtJournalDocNo.Location = new System.Drawing.Point(166, 362);
            this.TxtJournalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo.Name = "TxtJournalDocNo";
            this.TxtJournalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo.Properties.MaxLength = 30;
            this.TxtJournalDocNo.Properties.ReadOnly = true;
            this.TxtJournalDocNo.Size = new System.Drawing.Size(197, 20);
            this.TxtJournalDocNo.TabIndex = 68;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(107, 365);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 14);
            this.label11.TabIndex = 67;
            this.label11.Text = "Journal#";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmtBefTax
            // 
            this.TxtAmtBefTax.EnterMoveNextControl = true;
            this.TxtAmtBefTax.Location = new System.Drawing.Point(166, 109);
            this.TxtAmtBefTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmtBefTax.Name = "TxtAmtBefTax";
            this.TxtAmtBefTax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmtBefTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmtBefTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmtBefTax.Properties.Appearance.Options.UseFont = true;
            this.TxtAmtBefTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmtBefTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmtBefTax.Properties.ReadOnly = true;
            this.TxtAmtBefTax.Size = new System.Drawing.Size(197, 20);
            this.TxtAmtBefTax.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(14, 113);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(147, 14);
            this.label10.TabIndex = 27;
            this.label10.Text = "Total Amount Before Tax";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxAmt3
            // 
            this.TxtTaxAmt3.EnterMoveNextControl = true;
            this.TxtTaxAmt3.Location = new System.Drawing.Point(428, 235);
            this.TxtTaxAmt3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt3.Name = "TxtTaxAmt3";
            this.TxtTaxAmt3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt3.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt3.Properties.ReadOnly = true;
            this.TxtTaxAmt3.Size = new System.Drawing.Size(118, 20);
            this.TxtTaxAmt3.TabIndex = 58;
            // 
            // TxtTaxAmt2
            // 
            this.TxtTaxAmt2.EnterMoveNextControl = true;
            this.TxtTaxAmt2.Location = new System.Drawing.Point(428, 193);
            this.TxtTaxAmt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt2.Name = "TxtTaxAmt2";
            this.TxtTaxAmt2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt2.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt2.Properties.ReadOnly = true;
            this.TxtTaxAmt2.Size = new System.Drawing.Size(118, 20);
            this.TxtTaxAmt2.TabIndex = 48;
            // 
            // TxtTaxAmt1
            // 
            this.TxtTaxAmt1.EnterMoveNextControl = true;
            this.TxtTaxAmt1.Location = new System.Drawing.Point(428, 151);
            this.TxtTaxAmt1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt1.Name = "TxtTaxAmt1";
            this.TxtTaxAmt1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt1.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt1.Properties.ReadOnly = true;
            this.TxtTaxAmt1.Size = new System.Drawing.Size(118, 20);
            this.TxtTaxAmt1.TabIndex = 38;
            // 
            // TxtTaxCode3
            // 
            this.TxtTaxCode3.EnterMoveNextControl = true;
            this.TxtTaxCode3.Location = new System.Drawing.Point(166, 235);
            this.TxtTaxCode3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxCode3.Name = "TxtTaxCode3";
            this.TxtTaxCode3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxCode3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxCode3.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxCode3.Properties.MaxLength = 80;
            this.TxtTaxCode3.Properties.ReadOnly = true;
            this.TxtTaxCode3.Size = new System.Drawing.Size(197, 20);
            this.TxtTaxCode3.TabIndex = 56;
            // 
            // TxtTaxCode2
            // 
            this.TxtTaxCode2.EnterMoveNextControl = true;
            this.TxtTaxCode2.Location = new System.Drawing.Point(166, 193);
            this.TxtTaxCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxCode2.Name = "TxtTaxCode2";
            this.TxtTaxCode2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxCode2.Properties.MaxLength = 80;
            this.TxtTaxCode2.Properties.ReadOnly = true;
            this.TxtTaxCode2.Size = new System.Drawing.Size(197, 20);
            this.TxtTaxCode2.TabIndex = 46;
            // 
            // TxtTaxCode1
            // 
            this.TxtTaxCode1.EnterMoveNextControl = true;
            this.TxtTaxCode1.Location = new System.Drawing.Point(166, 151);
            this.TxtTaxCode1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxCode1.Name = "TxtTaxCode1";
            this.TxtTaxCode1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxCode1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxCode1.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxCode1.Properties.MaxLength = 80;
            this.TxtTaxCode1.Properties.ReadOnly = true;
            this.TxtTaxCode1.Size = new System.Drawing.Size(197, 20);
            this.TxtTaxCode1.TabIndex = 36;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(134, 155);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 14);
            this.label9.TabIndex = 35;
            this.label9.Text = "Tax";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCurCode
            // 
            this.TxtCurCode.EnterMoveNextControl = true;
            this.TxtCurCode.Location = new System.Drawing.Point(166, 88);
            this.TxtCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode.Name = "TxtCurCode";
            this.TxtCurCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode.Properties.MaxLength = 30;
            this.TxtCurCode.Properties.ReadOnly = true;
            this.TxtCurCode.Size = new System.Drawing.Size(118, 20);
            this.TxtCurCode.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(106, 92);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 14);
            this.label7.TabIndex = 25;
            this.label7.Text = "Currency";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxInvoiceNo
            // 
            this.TxtTaxInvoiceNo.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo.Location = new System.Drawing.Point(166, 130);
            this.TxtTaxInvoiceNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo.Name = "TxtTaxInvoiceNo";
            this.TxtTaxInvoiceNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo.Size = new System.Drawing.Size(197, 20);
            this.TxtTaxInvoiceNo.TabIndex = 30;
            // 
            // DteTaxInvoiceDt
            // 
            this.DteTaxInvoiceDt.EditValue = null;
            this.DteTaxInvoiceDt.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt.Location = new System.Drawing.Point(428, 130);
            this.DteTaxInvoiceDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt.Name = "DteTaxInvoiceDt";
            this.DteTaxInvoiceDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteTaxInvoiceDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteTaxInvoiceDt.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt.Properties.ReadOnly = true;
            this.DteTaxInvoiceDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt.Size = new System.Drawing.Size(118, 20);
            this.DteTaxInvoiceDt.TabIndex = 32;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(82, 134);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 14);
            this.label4.TabIndex = 29;
            this.label4.Text = "Tax Invoice#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSiteCode
            // 
            this.TxtSiteCode.EnterMoveNextControl = true;
            this.TxtSiteCode.Location = new System.Drawing.Point(166, 67);
            this.TxtSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSiteCode.Name = "TxtSiteCode";
            this.TxtSiteCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSiteCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSiteCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSiteCode.Properties.MaxLength = 16;
            this.TxtSiteCode.Properties.ReadOnly = true;
            this.TxtSiteCode.Size = new System.Drawing.Size(319, 20);
            this.TxtSiteCode.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(133, 71);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 14);
            this.label3.TabIndex = 23;
            this.label3.Text = "Site";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(367, 133);
            this.label27.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(57, 14);
            this.label27.TabIndex = 31;
            this.label27.Text = "Tax Date";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(166, 277);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 500;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(498, 20);
            this.MeeRemark.TabIndex = 62;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(114, 280);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 61;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(166, 256);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.Size = new System.Drawing.Size(197, 20);
            this.TxtAmt.TabIndex = 60;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(22, 260);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 14);
            this.label6.TabIndex = 59;
            this.label6.Text = "Total Amount After Tax";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(26, 8);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(135, 14);
            this.label19.TabIndex = 16;
            this.label19.Text = "Reason For Cancellation";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(166, 4);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 350;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(319, 20);
            this.MeeCancelReason.TabIndex = 17;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(88, 50);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 14);
            this.label16.TabIndex = 21;
            this.label16.Text = "Department";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(166, 46);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 350;
            this.LueDeptCode.Size = new System.Drawing.Size(319, 20);
            this.LueDeptCode.TabIndex = 22;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(489, 4);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(76, 22);
            this.ChkCancelInd.TabIndex = 18;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(114, 29);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 19;
            this.label8.Text = "Vendor";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVdCode
            // 
            this.LueVdCode.EnterMoveNextControl = true;
            this.LueVdCode.Location = new System.Drawing.Point(166, 25);
            this.LueVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdCode.Name = "LueVdCode";
            this.LueVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.Appearance.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdCode.Properties.DropDownRows = 30;
            this.LueVdCode.Properties.NullText = "[Empty]";
            this.LueVdCode.Properties.PopupWidth = 350;
            this.LueVdCode.Size = new System.Drawing.Size(319, 20);
            this.LueVdCode.TabIndex = 20;
            this.LueVdCode.ToolTip = "F4 : Show/hide list";
            this.LueVdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdCode.EditValueChanged += new System.EventHandler(this.LueVdCode_EditValueChanged);
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.Grd1);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 173);
            this.Tp2.Text = "Returned Item List";
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(766, 173);
            this.Grd1.TabIndex = 31;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.Grd3);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(766, 173);
            this.Tp3.Text = "COA\'s Account List (Discount, Additional Cost, Etc)";
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(766, 173);
            this.Grd3.TabIndex = 31;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // Tp4
            // 
            this.Tp4.Appearance.Header.Options.UseTextOptions = true;
            this.Tp4.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp4.Controls.Add(this.Grd2);
            this.Tp4.Name = "Tp4";
            this.Tp4.PageVisible = false;
            this.Tp4.Size = new System.Drawing.Size(766, 173);
            this.Tp4.Text = "PO (Discount Amount/Custom Tax)";
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 173);
            this.Grd2.TabIndex = 31;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 431);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 8;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            this.LueFontSize.EditValueChanged += new System.EventHandler(this.LueFontSize_EditValueChanged);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 451);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 9;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // FrmPurchaseReturnInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 473);
            this.Name = "FrmPurchaseReturnInvoice";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcPurchaseReturnInvoice)).EndInit();
            this.TcPurchaseReturnInvoice.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.Tp1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecvVd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPurchaseInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueServiceCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueServiceCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueServiceCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestPPNDocNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestPPNDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestPPNDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmtBefTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.Tp3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.Tp4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraTab.XtraTabControl TcPurchaseReturnInvoice;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        private DevExpress.XtraTab.XtraTabPage Tp4;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit LueVdCode;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtTaxCode3;
        internal DevExpress.XtraEditors.TextEdit TxtTaxCode2;
        internal DevExpress.XtraEditors.TextEdit TxtTaxCode1;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtCurCode;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtSiteCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtAmtBefTax;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt3;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt2;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt1;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestPPNDocNo;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestPPNDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestPPNDocNo3;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo3;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt3;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo2;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label51;
        private DevExpress.XtraEditors.LookUpEdit LueServiceCode3;
        private System.Windows.Forms.Label label50;
        private DevExpress.XtraEditors.LookUpEdit LueServiceCode2;
        private System.Windows.Forms.Label label49;
        private DevExpress.XtraEditors.LookUpEdit LueServiceCode1;
        private System.Windows.Forms.Label label28;
        public DevExpress.XtraEditors.SimpleButton BtnVoucher;
        public DevExpress.XtraEditors.SimpleButton BtnPurchaseInvoice;
        public DevExpress.XtraEditors.SimpleButton BtnRecvVd;
        internal DevExpress.XtraEditors.TextEdit TxtRecvVd;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit TxtVoucher;
        internal DevExpress.XtraEditors.TextEdit TxtPurchaseInvoice;
        private System.Windows.Forms.Label label26;
    }
}