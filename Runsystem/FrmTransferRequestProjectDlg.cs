﻿#region Update
/*
    19/05/2020 [DITA/IMS] new apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTransferRequestProjectDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmTransferRequestProject mFrmParent;
        private string mSQL = string.Empty;
        internal int mNumberOfInventoryUomCode = 1;

        #endregion

        #region Constructor

        public FrmTransferRequestProjectDlg(FrmTransferRequestProject FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtBom }, true);
                SetNumberOfInventoryUomCode();
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode);
                Sl.SetLueWhsCode(ref LueWhsCode);
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL(string Filter, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, D.ItName, E.ItCtName, G.CCtName, ");
            SQL.AppendLine("D.InventoryUOMCode, D.InventoryUOMCode2, D.InventoryUOMCode3, A.WhsCode, H.WhsName, ");
            if (mFrmParent.mIsItGrpCodeShow)
                SQL.AppendLine("D.ItGrpCode, ");
            else
                SQL.AppendLine("Null As ItGrpCode, ");
            SQL.AppendLine("IfNull(A.StockQty, 0)-IfNull(B.TransferredQty, 0)-IfNull(C.DOQty, 0) As Qty, ");
            SQL.AppendLine("IfNull(A.StockQty2, 0)-IfNull(B.TransferredQty2, 0)-IfNull(C.DOQty2, 0) As Qty2, ");
            SQL.AppendLine("IfNull(A.StockQty3, 0)-IfNull(B.TransferredQty3, 0)-IfNull(C.DOQty3, 0) As Qty3, ");
            SQL.AppendLine("D.ItCodeInternal, D.Specification ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select ItCode, WhsCode, ");
            SQL.AppendLine("    Sum(Qty) As StockQty, Sum(Qty2) As StockQty2, Sum(Qty3) As StockQty3 ");
            SQL.AppendLine("    From TblStockSummary ");
            SQL.AppendLine("    Where Qty<>0 ");
           // SQL.AppendLine("    And WhsCode=@WhsCode ");
            SQL.AppendLine(Filter.Replace("X.", "").Replace("Y.", ""));
            if (TxtBom.Text.Length > 0)
            {
                SQL.AppendLine(" And ItCode In ( ");
                SQL.AppendLine("    Select DocCode From TblBomDtl ");
                SQL.AppendLine("    Where DocType = '1' ");
                SQL.AppendLine("    And DocNo=@BomDocNo ");
                SQL.AppendLine(Filter.Replace("X.", "").Replace("Y.", ""));
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Group By ItCode ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select T2.ItCode, ");
            SQL.AppendLine("    Sum(T2.Qty) As TransferredQty, ");
            SQL.AppendLine("    Sum(T2.Qty2) As TransferredQty2, ");
            SQL.AppendLine("    Sum(T2.Qty3) As TransferredQty3 ");
            SQL.AppendLine("    From TblTransferRequestWhsHdr T1 ");
            SQL.AppendLine("    Inner Join TblTransferRequestWhsDtl T2 On T1.DocNo=T2.DocNo And T2.ProcessInd ='O' ");
            SQL.AppendLine(Filter.Replace("X.", "T2.").Replace("Y.", "T1."));
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O') In ('O', 'A') ");
           // SQL.AppendLine("    And T1.WhsCode=@WhsCode ");
            SQL.AppendLine("    Group By T2.ItCode ");
            SQL.AppendLine(") B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select T4.ItCode, ");
            SQL.AppendLine("    Sum(T4.Qty) As DOQty, ");
            SQL.AppendLine("    Sum(T4.Qty2) As DOQty2, ");
            SQL.AppendLine("    Sum(T4.Qty3) As DOQty3 ");
            SQL.AppendLine("    From TblTransferRequestWhsHdr T1 ");
            SQL.AppendLine("    Inner Join TblTransferRequestWhsDtl T2 On T1.DocNo=T2.DocNo And T2.ProcessInd='F' ");
            SQL.AppendLine(Filter.Replace("X.", "T2.").Replace("Y.", "T1."));
            SQL.AppendLine("    Inner Join TblDOWhsHdr T3 ");
            SQL.AppendLine("        On T3.TransferRequestWhsDocNo Is Not Null ");
            SQL.AppendLine("        And T2.DocNo=IfNull(T3.TransferRequestWhsDocNo, '') ");
            SQL.AppendLine("        And T3.CancelInd ='N' ");
            SQL.AppendLine("        And T3.Status In ('A', 'O') ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl T4 ");
            SQL.AppendLine("        On T3.DocNo=T4.DocNo ");
            SQL.AppendLine("        And T2.ItCode=T4.ItCode ");
            SQL.AppendLine("        And T4.ProcessInd='O' ");
            SQL.AppendLine("        And T4.CancelInd='N' ");
            SQL.AppendLine(Filter.Replace("X.", "T4.").Replace("Y.", "T1."));
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O')='A' ");
           // SQL.AppendLine("    And T1.WhsCode=@WhsCode ");
            SQL.AppendLine("    Group By T4.ItCode ");
            SQL.AppendLine(") C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join TblItem D On A.ItCode=D.ItCode " );
            SQL.AppendLine("Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
            SQL.AppendLine("Left Join TblItemCostCategory F On A.ItCode=F.ItCode And F.CCCode=@CCCode ");
            SQL.AppendLine("Left Join TblCostCategory G On F.CCCode=G.CCCode And F.CCtCode=G.CCtCode ");
            SQL.AppendLine("Inner Join TblWarehouse H On H.WhsCode = A.WhsCode " + Filter2.Replace("X.", "D.").Replace("Y.", "H."));
            SQL.AppendLine("Where (IfNull(A.StockQty, 0)-IfNull(B.TransferredQty, 0)-IfNull(C.DOQty, 0))>0 ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "", 
                        "Item's Name", 
                        "Item's Category",

                        //6-10
                        "Stock",
                        "UoM ",
                        "Stock",
                        "UoM",
                        "Stock",
                        
                        //11-15
                        "UoM",
                        "Cost Category",
                        "Group",
                        "Local Code",
                        "Specification",

                        //16-17
                        "Warehouse Code",
                        "Warehouse"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 20, 250, 180,
                        
                        //6-10
                        100, 80, 100, 80, 100,
                        
                        //11-15
                        80, 200, 150, 180, 300,

                        //16-17
                        0, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8, 10 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17});
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 8, 9, 10, 11, 13, 14, 16 }, false);
            ShowInventoryUomCode();
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[13].Visible = true;
                Grd1.Cols[13].Move(5);
            }
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 15 });
            Grd1.Cols[14].Move(2);
            Grd1.Cols[17].Move(6);

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 14 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11 }, true);
        }

        override protected void ShowData()
        {
            try
            {
               
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty, Filter2 = " And 1=1 ";

                var cm = new MySqlCommand();

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    var No = "0001";
                    for (int Row = 0; Row < mFrmParent.Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(mFrmParent.Grd1, Row, 3).Length != 0)
                        {
                            Filter += (" And (X.ItCode<>@ItCode" + No.ToString() + "And Y.WhsCode<>@WhsCode  "+ No.ToString()+  ") ");
                            Sm.CmParam<String>(ref cm, "@ItCode" + No.ToString(), Sm.GetGrdStr(mFrmParent.Grd1, Row, 3));
                            No = ("000" + (int.Parse(No) + 1).ToString()).ToString();
                        }
                    }
                }
                if (Filter.Length>0) Filter2 = Filter;
                //Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "BomDocNo", TxtBom.Text);
                Sm.FilterStr(ref Filter2, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "X.ItName" });
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueItCtCode), "X.ItCtCode", true);
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueWhsCode), "Y.WhsCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter, Filter2) + " Order By D.ItName, H.WhsName ; ",
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItName", "ItCtName", "Qty", "InventoryUomCode", "Qty2", 
                            
                            //6-10
                            "InventoryUomCode2", "Qty3", "InventoryUomCode3", "CCtName", "ItGrpCode",

                            //11-14
                            "ItCodeInternal", "Specification", "WhsCode", "WhsName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 17);

                        mFrmParent.Grd1.Rows.Add();

                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 5, 6, 8, 9, 11, 12 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            string ItCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 3), ItCode))
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        #endregion

        #region Event

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void BtnBom_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmTransferRequestProjectDlg2(this));
        }

        private void TxtBom_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkBom_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bom");
        }
        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }
        #endregion
    }
}
