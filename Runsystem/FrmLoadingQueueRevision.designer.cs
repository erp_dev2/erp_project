﻿namespace RunSystem
{
    partial class FrmLoadingQueueRevision
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLoadingQueueRevision));
            this.BtnLoadingQueue = new DevExpress.XtraEditors.SimpleButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtDocNoLoadingQueue = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtDriverNameOld = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtLicenceNoOld = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtItem = new DevExpress.XtraEditors.TextEdit();
            this.LueAreaBongkar = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDriverName = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtLicenceNo = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtAreaBongkarOld = new DevExpress.XtraEditors.TextEdit();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.LueTTCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtTTCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNoLoadingQueue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDriverNameOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLicenceNoOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAreaBongkar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDriverName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLicenceNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAreaBongkarOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTTCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTTCodeOld.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(780, 0);
            this.panel1.Size = new System.Drawing.Size(70, 320);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.TxtTTCodeOld);
            this.panel2.Controls.Add(this.LueTTCode);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtAreaBongkarOld);
            this.panel2.Controls.Add(this.LueAreaBongkar);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDriverName);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtLicenceNo);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtItem);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.TxtDocNoLoadingQueue);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.TxtDriverNameOld);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.TxtLicenceNoOld);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.BtnLoadingQueue);
            this.panel2.Size = new System.Drawing.Size(780, 320);
            // 
            // BtnLoadingQueue
            // 
            this.BtnLoadingQueue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnLoadingQueue.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnLoadingQueue.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLoadingQueue.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnLoadingQueue.Appearance.Options.UseBackColor = true;
            this.BtnLoadingQueue.Appearance.Options.UseFont = true;
            this.BtnLoadingQueue.Appearance.Options.UseForeColor = true;
            this.BtnLoadingQueue.Appearance.Options.UseTextOptions = true;
            this.BtnLoadingQueue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnLoadingQueue.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnLoadingQueue.Image = ((System.Drawing.Image)(resources.GetObject("BtnLoadingQueue.Image")));
            this.BtnLoadingQueue.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnLoadingQueue.Location = new System.Drawing.Point(409, 64);
            this.BtnLoadingQueue.Name = "BtnLoadingQueue";
            this.BtnLoadingQueue.Size = new System.Drawing.Size(35, 36);
            this.BtnLoadingQueue.TabIndex = 15;
            this.BtnLoadingQueue.ToolTip = "Find Item";
            this.BtnLoadingQueue.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnLoadingQueue.ToolTipTitle = "Run System";
            this.BtnLoadingQueue.Click += new System.EventHandler(this.BtnLoadingQueue_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(37, 203);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 14);
            this.label3.TabIndex = 24;
            this.label3.Text = "Area Bongkar";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(48, 79);
            this.label13.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 14);
            this.label13.TabIndex = 13;
            this.label13.Text = "No. Antrian";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNoLoadingQueue
            // 
            this.TxtDocNoLoadingQueue.EnterMoveNextControl = true;
            this.TxtDocNoLoadingQueue.Location = new System.Drawing.Point(122, 65);
            this.TxtDocNoLoadingQueue.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNoLoadingQueue.Name = "TxtDocNoLoadingQueue";
            this.TxtDocNoLoadingQueue.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNoLoadingQueue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNoLoadingQueue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNoLoadingQueue.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNoLoadingQueue.Properties.MaxLength = 16;
            this.TxtDocNoLoadingQueue.Size = new System.Drawing.Size(279, 36);
            this.TxtDocNoLoadingQueue.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(41, 131);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 14);
            this.label11.TabIndex = 18;
            this.label11.Text = "Jenis Antrian";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDriverNameOld
            // 
            this.TxtDriverNameOld.EnterMoveNextControl = true;
            this.TxtDriverNameOld.Location = new System.Drawing.Point(122, 177);
            this.TxtDriverNameOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDriverNameOld.Name = "TxtDriverNameOld";
            this.TxtDriverNameOld.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDriverNameOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDriverNameOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDriverNameOld.Properties.Appearance.Options.UseFont = true;
            this.TxtDriverNameOld.Properties.MaxLength = 16;
            this.TxtDriverNameOld.Size = new System.Drawing.Size(321, 20);
            this.TxtDriverNameOld.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(83, 179);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 14);
            this.label7.TabIndex = 22;
            this.label7.Text = "Sopir";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLicenceNoOld
            // 
            this.TxtLicenceNoOld.EnterMoveNextControl = true;
            this.TxtLicenceNoOld.Location = new System.Drawing.Point(122, 153);
            this.TxtLicenceNoOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLicenceNoOld.Name = "TxtLicenceNoOld";
            this.TxtLicenceNoOld.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLicenceNoOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLicenceNoOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLicenceNoOld.Properties.Appearance.Options.UseFont = true;
            this.TxtLicenceNoOld.Properties.MaxLength = 16;
            this.TxtLicenceNoOld.Size = new System.Drawing.Size(321, 20);
            this.TxtLicenceNoOld.TabIndex = 21;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(66, 156);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 14);
            this.label5.TabIndex = 20;
            this.label5.Text = "No Polisi";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(23, 107);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 14);
            this.label6.TabIndex = 16;
            this.label6.Text = "Jenis Kendaraan";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItem
            // 
            this.TxtItem.EnterMoveNextControl = true;
            this.TxtItem.Location = new System.Drawing.Point(122, 129);
            this.TxtItem.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItem.Name = "TxtItem";
            this.TxtItem.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItem.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItem.Properties.Appearance.Options.UseFont = true;
            this.TxtItem.Properties.MaxLength = 16;
            this.TxtItem.Size = new System.Drawing.Size(321, 20);
            this.TxtItem.TabIndex = 19;
            // 
            // LueAreaBongkar
            // 
            this.LueAreaBongkar.EnterMoveNextControl = true;
            this.LueAreaBongkar.Location = new System.Drawing.Point(553, 200);
            this.LueAreaBongkar.Margin = new System.Windows.Forms.Padding(5);
            this.LueAreaBongkar.Name = "LueAreaBongkar";
            this.LueAreaBongkar.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAreaBongkar.Properties.Appearance.Options.UseFont = true;
            this.LueAreaBongkar.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAreaBongkar.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAreaBongkar.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAreaBongkar.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAreaBongkar.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAreaBongkar.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAreaBongkar.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAreaBongkar.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAreaBongkar.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAreaBongkar.Properties.DropDownRows = 12;
            this.LueAreaBongkar.Properties.NullText = "[Empty]";
            this.LueAreaBongkar.Properties.PopupWidth = 500;
            this.LueAreaBongkar.Size = new System.Drawing.Size(218, 20);
            this.LueAreaBongkar.TabIndex = 33;
            this.LueAreaBongkar.ToolTip = "F4 : Show/hide list";
            this.LueAreaBongkar.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(469, 203);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 14);
            this.label2.TabIndex = 32;
            this.label2.Text = "Area Bongkar";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDriverName
            // 
            this.TxtDriverName.EnterMoveNextControl = true;
            this.TxtDriverName.Location = new System.Drawing.Point(553, 176);
            this.TxtDriverName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDriverName.Name = "TxtDriverName";
            this.TxtDriverName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDriverName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDriverName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDriverName.Properties.Appearance.Options.UseFont = true;
            this.TxtDriverName.Properties.MaxLength = 16;
            this.TxtDriverName.Size = new System.Drawing.Size(218, 20);
            this.TxtDriverName.TabIndex = 31;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(515, 181);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 14);
            this.label8.TabIndex = 30;
            this.label8.Text = "Sopir";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLicenceNo
            // 
            this.TxtLicenceNo.EnterMoveNextControl = true;
            this.TxtLicenceNo.Location = new System.Drawing.Point(553, 151);
            this.TxtLicenceNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLicenceNo.Name = "TxtLicenceNo";
            this.TxtLicenceNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLicenceNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLicenceNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLicenceNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLicenceNo.Properties.MaxLength = 16;
            this.TxtLicenceNo.Size = new System.Drawing.Size(218, 20);
            this.TxtLicenceNo.TabIndex = 29;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(498, 159);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 14);
            this.label9.TabIndex = 28;
            this.label9.Text = "No Polisi";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAreaBongkarOld
            // 
            this.TxtAreaBongkarOld.EnterMoveNextControl = true;
            this.TxtAreaBongkarOld.Location = new System.Drawing.Point(122, 201);
            this.TxtAreaBongkarOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAreaBongkarOld.Name = "TxtAreaBongkarOld";
            this.TxtAreaBongkarOld.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAreaBongkarOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAreaBongkarOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAreaBongkarOld.Properties.Appearance.Options.UseFont = true;
            this.TxtAreaBongkarOld.Properties.MaxLength = 16;
            this.TxtAreaBongkarOld.Size = new System.Drawing.Size(321, 20);
            this.TxtAreaBongkarOld.TabIndex = 25;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(122, 16);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(321, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(18, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Nomor Dokumen";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(122, 41);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(166, 20);
            this.DteDocDt.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(6, 44);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 14);
            this.label4.TabIndex = 11;
            this.label4.Text = "Tanggal Dokumen";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(122, 225);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(648, 20);
            this.MeeRemark.TabIndex = 27;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(69, 227);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 14);
            this.label10.TabIndex = 26;
            this.label10.Text = "Catatan";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTTCode
            // 
            this.LueTTCode.EnterMoveNextControl = true;
            this.LueTTCode.Location = new System.Drawing.Point(553, 108);
            this.LueTTCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTTCode.Name = "LueTTCode";
            this.LueTTCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTTCode.Properties.Appearance.Options.UseFont = true;
            this.LueTTCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTTCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTTCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTTCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTTCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTTCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTTCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTTCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTTCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTTCode.Properties.DropDownRows = 12;
            this.LueTTCode.Properties.NullText = "[Empty]";
            this.LueTTCode.Properties.PopupWidth = 500;
            this.LueTTCode.Size = new System.Drawing.Size(218, 20);
            this.LueTTCode.TabIndex = 17;
            this.LueTTCode.ToolTip = "F4 : Show/hide list";
            this.LueTTCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTTCode.EditValueChanged += new System.EventHandler(this.LueTTCode_EditValueChanged);
            // 
            // TxtTTCodeOld
            // 
            this.TxtTTCodeOld.EnterMoveNextControl = true;
            this.TxtTTCodeOld.Location = new System.Drawing.Point(122, 106);
            this.TxtTTCodeOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTTCodeOld.Name = "TxtTTCodeOld";
            this.TxtTTCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTTCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTTCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTTCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtTTCodeOld.Properties.MaxLength = 16;
            this.TxtTTCodeOld.Size = new System.Drawing.Size(321, 20);
            this.TxtTTCodeOld.TabIndex = 34;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(455, 110);
            this.label12.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 14);
            this.label12.TabIndex = 35;
            this.label12.Text = "Jenis Kendaraan";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmLoadingQueueRevision
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 320);
            this.Name = "FrmLoadingQueueRevision";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNoLoadingQueue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDriverNameOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLicenceNoOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAreaBongkar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDriverName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLicenceNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAreaBongkarOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTTCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTTCodeOld.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton BtnLoadingQueue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtDocNoLoadingQueue;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtDriverNameOld;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtLicenceNoOld;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtItem;
        private DevExpress.XtraEditors.LookUpEdit LueAreaBongkar;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDriverName;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtLicenceNo;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtAreaBongkarOld;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label10;
        public DevExpress.XtraEditors.LookUpEdit LueTTCode;
        private System.Windows.Forms.Label label12;
        public DevExpress.XtraEditors.TextEdit TxtTTCodeOld;
    }
}