﻿#region Update
/*
    02/09/2019 [WED] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmUserForceLogoff : RunSystem.FrmBase12
    {
        #region Field, Property

        private string mMenuCode = string.Empty, mSQL = string.Empty;
        internal string mAccessInd = string.Empty;
        private bool mIsUseLoginSession = false;

        #endregion

        #region Constructor

        public FrmUserForceLogoff(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                GetParameter();
                TxtUserCode.Focus();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Button Methods

        override protected void BtnProcessClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtUserCode, "UserCode", false) ||
                Sm.IsTxtEmpty(TxtUserName, "UserName", false) ||
                Sm.StdMsgYN("Question", "Make sure this user is not currently using the application. Do you proceed ?") == DialogResult.No)
                return;

            try
            {
                if (mIsUseLoginSession) ProcessData();
                else Sm.StdMsg(mMsgType.Info, "Make sure 'IsUseLoginSession' parameter is enabled.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Standard Methods

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<BaseEdit> 
            {
               TxtUserCode, TxtUserName
            });
            TxtUserCode.Focus();
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsUseLoginSession = Sm.GetParameterBoo("IsUseLoginSession");
        }

        private void ProcessData()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblLog ");
            SQL.AppendLine("Set LogOut = CurrentDateTime(), LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where LogOut Is Null ");
            SQL.AppendLine("And UserCode = @UserCode; ");

            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text);

            cm.CommandText = SQL.ToString();

            Sm.ExecCommand(cm);

            Sm.StdMsg(mMsgType.Info, "User " + TxtUserCode.Text + " has successfuly forced to log off.");
            ClearData();
        }

        #endregion

        #endregion

        #region Events

        #region Button Click

        private void BtnUserCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmUserForceLogoffDlg(this));
        }

        #endregion

        #region Misc Control Events

        private void TxtUserCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtUserCode);
        }

        private void TxtUserName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtUserName);
        }

        #endregion

        #endregion
    }
}
