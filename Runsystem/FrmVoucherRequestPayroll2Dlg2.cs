﻿#region Update
/*
    25/04/2017 [TKG] send csv per employee
    21/05/2019 [TKG] remark kalau ada /r/n diganti jadi ' '.
    27/06/2019 [WED] BUG saat IsGrdEmpty
    13/03/2020 [WED/HIN] decimal point menjadi 2 untuk CSV nya, berdasarkan parameter DecimalPointForMandiriPayroll
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using System.IO;
using System.Net;
using Renci.SshNet;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestPayroll2Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherRequestPayroll2 mFrmParent;
        private string 
            mDocNo = string.Empty, 
            mDocDt = string.Empty, 
            mBankCode = string.Empty, 
            mSQL = string.Empty;

        #region untuk csv

        private string
            mHostAddrForMandiriPayroll = string.Empty,
            mSharedFolderForMandiriPayroll = string.Empty,
            mUserNameForMandiriPayroll = string.Empty,
            mPasswordForMandiriPayroll = string.Empty,
            mPortForMandiriPayroll = string.Empty,
            mHostAddrForBNIPayroll = string.Empty,
            mSharedFolderForBNIPayroll = string.Empty,
            mUserNameForBNIPayroll = string.Empty,
            mPasswordForBNIPayroll = string.Empty,
            mPortForBNIPayroll = string.Empty,
            mProtocolForMandiriPayroll = string.Empty,
            mProtocolForBNIPayroll = string.Empty,
            mCompanyCodeForMandiriPayroll = string.Empty,
            mPathToSaveExportedMandiriPayroll = string.Empty,
            mPathToSaveExportedBNIPayroll = string.Empty;

        private bool 
            mIsCSVUseRealAmt = false,
            mIsVoucherRequestPayrollAbleToProcessCSVMoreThanOnce = false;

        #endregion
   
        #endregion

        #region Constructor

        public FrmVoucherRequestPayroll2Dlg2(
            FrmVoucherRequestPayroll2 FrmParent, 
            string DocNo, 
            string DocDt, 
            string BankCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocNo = DocNo;
            mDocDt = DocDt;
            mBankCode = BankCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsVoucherRequestPayrollAbleToProcessCSVMoreThanOnce = Sm.GetParameterBoo("IsVoucherRequestPayrollAbleToProcessCSVMoreThanOnce");
            mHostAddrForMandiriPayroll = Sm.GetParameter("HostAddrForMandiriPayroll");
            mSharedFolderForMandiriPayroll = Sm.GetParameter("SharedFolderForMandiriPayroll");
            mUserNameForMandiriPayroll = Sm.GetParameter("UserNameForMandiriPayroll");
            mPasswordForMandiriPayroll = Sm.GetParameter("PasswordForMandiriPayroll");
            mPortForMandiriPayroll = Sm.GetParameter("PortForMandiriPayroll");
            mHostAddrForBNIPayroll = Sm.GetParameter("HostAddrForBNIPayroll");
            mSharedFolderForBNIPayroll = Sm.GetParameter("SharedFolderForBNIPayroll");
            mUserNameForBNIPayroll = Sm.GetParameter("UserNameForBNIPayroll");
            mPasswordForBNIPayroll = Sm.GetParameter("PasswordForBNIPayroll");
            mPortForBNIPayroll = Sm.GetParameter("PortForBNIPayroll");
            mProtocolForBNIPayroll = Sm.GetParameter("ProtocolForBNIPayroll");
            mProtocolForMandiriPayroll = Sm.GetParameter("ProtocolForMandiriPayroll");
            mCompanyCodeForMandiriPayroll = Sm.GetParameter("CompanyCodeForMandiriPayroll");
            mPathToSaveExportedBNIPayroll = Sm.GetParameter("PathToSaveExportedBNIPayroll");
            mPathToSaveExportedMandiriPayroll = Sm.GetParameter("PathToSaveExportedMandiriPayroll");
            mIsCSVUseRealAmt = Sm.GetParameterBoo("IsCSVUseRealAmt");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Bank",
                        "Bank"+Environment.NewLine+"Account#",

                        //6
                        "Transferred"
                      },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 200, 150, 150,   
                        
                        //6
                        130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select D.EmpCode, D.EmpName, E.BankName, D.BankAcNo, Sum(C.Amt) As Amt ");
            SQL.AppendLine("From TblVoucherRequestPayrollHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestPayrollDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblPayrollProcess1 C ON B.PayrunCode=C.PayrunCode ");
            SQL.AppendLine("Inner Join TblEmployee D On C.EmpCode=D.EmpCode And IfNull(D.PayrollType, 'T')='T' "); 
            SQL.AppendLine("Inner join TblBank E On D.BankCode = E.BankCode ");
            SQL.AppendLine("Inner join TblVoucherRequestHdr F On A.VoucherRequestDocNo=F.DocNo And F.CancelInd='N' And F.Status='A' And IfNull(F.VoucherDocNo, '')='' ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And A.Status = 'A' ");
            if (!mIsVoucherRequestPayrollAbleToProcessCSVMoreThanOnce)
                SQL.AppendLine("And A.CSVInd = 'N' ");
            SQL.AppendLine("And C.Amt>0 ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            
            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "D.EmpCode", "D.EmpName" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, 
                        mSQL + Filter + 
                        "Group By D.EmpCode, D.EmpName, E.BankName, D.BankAcNo " +
                        "Order By D.EmpName;",
                        new string[]
                        {
                            //0
                            "EmpCode",
                            
                            //1-5
                            "EmpName", "BankName", "BankAcNo", "Amt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsDataNotValid()
        {
            return IsGrdEmpty() || IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length>0 &&
                    Sm.GetGrdBool(Grd1, r, 1)) return false;
            }
            Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 employee.");
            return true;
        }

        override protected void ChooseData()
        {
            if (Sm.StdMsgYN("Question", "Do you want to process this data ?") == DialogResult.No ||
                IsDataNotValid()
                ) return;

                var lBank = new List<Bank>();
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                string mCreditTo = Sm.GetValue(
                    "Select B.BankAcNo " +
                    "From TblVoucherRequestPayrollHdr A " +
                    "Inner Join TblBankAccount B On A.BankAcCode = B.BankAcCode " +
                    "Where A.DocNo=@Param;", mDocNo);
                string EmpCode = string.Empty, Filter = string.Empty;

                if (Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                        if (Sm.GetGrdBool(Grd1, r, 1) && EmpCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += "(D.EmpCode=@EmpCode0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                        }
                    }
                }

                if (Filter.Length > 0)
                    Filter = " And (" + Filter + ") ";
                else
                    Filter = " And 1=0 ";

                SQL.AppendLine("Select T.EmpCode, T.EmpName, T.BankAcNo, T.BankAcName, T.BankCode, T.BankName, ");
                SQL.AppendLine("T.CurCode, Round(Sum(T.THP), " + mFrmParent.mDecimalPointForMandiriPayroll + ") As THP, T.Remark, T.FTS, T.Beneficiary, T.Email, T.DocNo, T.Periode, ");
                SQL.AppendLine("T.VoucherRequestDocNo, T.BankBranch, T.ReferenceNo, T.SwiftBeneCode, T.KliringCode, T.RTGSCode ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("  Select C.EmpCode, D.EmpName, D.BankAcNo, D.BankAcName, D.BankCode, F.BankName, ");
                SQL.AppendLine("  A.CurCode, C.Amt as THP, A.Remark, 'IBU' as FTS, 'N' As Beneficiary, ");
                SQL.AppendLine("  null As Email, A.DocNo, IfNull(DATE_FORMAT(Left(G.EndDt, 8), '%d/%m/%Y'), '') As Periode, A.VoucherRequestDocNo, ");
                SQL.AppendLine("  D.BankBranch, Concat(Left(B.PayrunCode, 6), C.EmpCode) As ReferenceNo, F.SwiftBeneCode, F.KliringCode, F.RTGSCode ");
                SQL.AppendLine("  From tblvoucherrequestpayrollhdr A ");
                SQL.AppendLine("  inner join tblvoucherrequestpayrolldtl2 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("  inner join tblpayrollprocess1 C ON B.PayrunCode = C.PayrunCode ");
                SQL.AppendLine("  inner join tblemployee D On C.EmpCode = D.EmpCode And IfNull(D.PayrollType, 'T') = 'T' "); // employee yang payroll type nya bank transfer (option EmployeePayrollType)
                SQL.AppendLine(Filter);
                SQL.AppendLine("  inner join tblvoucherrequesthdr E On A.VoucherRequestDocNo = E.DocNo ");
                SQL.AppendLine("  left join tblbank F On D.BankCode = F.BankCode ");
                SQL.AppendLine("  inner join tblpayrun G On C.PayrunCode = G.PayrunCode ");
                SQL.AppendLine("  Where A.CancelInd = 'N' ");
                SQL.AppendLine("  And A.Status = 'A' ");
                if (!mIsVoucherRequestPayrollAbleToProcessCSVMoreThanOnce)
                    SQL.AppendLine("And A.CSVInd = 'N' ");
                SQL.AppendLine("  And C.Amt > 0 ");
                SQL.AppendLine("  And A.DocNo = @DocNo ");
                SQL.AppendLine("  And (E.VoucherDocNo Is Null Or Length(Trim(E.VoucherDocNo)) <= 0 ) ");
                SQL.AppendLine(") T ");
                SQL.AppendLine("Group By T.EmpCode, T.EmpName, T.BankAcNo, T.BankAcName, T.BankCode, T.BankName, T.CurCode, T.Remark, T.FTS, ");
                SQL.AppendLine("T.Beneficiary, T.Email, T.DocNo, T.Periode, T.VoucherRequestDocNo, T.BankBranch, T.ReferenceNo, T.SwiftBeneCode, T.KliringCode, T.RTGSCode; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "EmpCode",

                         //1-5
                         "EmpName",
                         "BankAcNo",
                         "BankAcName",
                         "BankCode",
                         "BankName",

                         //6-10
                         "CurCode",
                         "THP", 
                         "Remark", 
                         "FTS",
                         "Beneficiary",

                         //11-15
                         "Email",
                         "DocNo",
                         "Periode",
                         "VoucherRequestDocNo",
                         "BankBranch",

                         //16-19
                         "ReferenceNo",
                         "SwiftBeneCode",
                         "KliringCode",
                         "RTGSCode"

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lBank.Add(new Bank()
                            {
                                EmpCode = Sm.DrStr(dr, c[0]),
                                EmpName = Sm.DrStr(dr, c[1]),
                                EmpAcNo = Sm.DrStr(dr, c[2]),
                                EmpAcName = Sm.DrStr(dr, c[3]),
                                EmpBankCode = Sm.DrStr(dr, c[4]),
                                EmpBankName = Sm.DrStr(dr, c[5]),
                                CurCode = mIsCSVUseRealAmt ? Sm.DrStr(dr, c[6]) : "IDR",
                                THP = mIsCSVUseRealAmt ? Sm.DrDec(dr, c[7]) : 1,
                                Remark = Sm.DrStr(dr, c[8]).Replace(Environment.NewLine, " "),
                                FTS = Sm.DrStr(dr, c[9]),
                                BeneficiaryNotFlag = Sm.DrStr(dr, c[10]),
                                Email = Sm.DrStr(dr, c[11]),
                                DocNo = Sm.DrStr(dr, c[12]),
                                VRDocNo = Sm.DrStr(dr, c[14]),
                                BankBranch = Sm.DrStr(dr, c[15]),
                                ReferenceNo = Sm.DrStr(dr, c[16]),
                                SwiftBeneCode = Sm.DrStr(dr, c[17]),
                                KliringCode = Sm.DrStr(dr, c[18]),
                                RTGSCode = Sm.DrStr(dr, c[19]),
                            });
                        }
                    }
                    dr.Close();
                }

                if (lBank.Count > 0)
                {
                    if (mCompanyCodeForMandiriPayroll.Length <= 0 && mBankCode == "008") // kalau header Mandiri tapi parameter Company Code masih kosong
                    {
                        Sm.StdMsg(mMsgType.Warning, "Parameter for Company Code (Mandiri) is empty.");
                        return;
                    }

                    for (int i = 0; i < lBank.Count; i++)
                    {
                        if (lBank[i].EmpAcNo.Length <= 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, lBank[i].EmpName + " (" + lBank[i].EmpCode + ") has no Account Number.");
                            return;
                        }

                        if (lBank[i].EmpAcName.Length <= 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, lBank[i].EmpName + " (" + lBank[i].EmpCode + ") has no Account Name.");
                            return;
                        }

                        if (lBank[i].EmpBankCode.Length <= 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, lBank[i].EmpName + " (" + lBank[i].EmpCode + ") has no Bank Account.");
                            return;
                        }

                        if (lBank[i].SwiftBeneCode.Length <= 0 && mBankCode == "009") // kalau header BNI, tapi bank si employee nya nggak ada informasi Swift Code nya
                        {
                            Sm.StdMsg(mMsgType.Warning, lBank[i].EmpName + " (" + lBank[i].EmpCode + "). His/her bank account (" + lBank[i].EmpBankName + ") has no Beneficiary Bank Code information.");
                            return;
                        }

                        if (lBank[i].KliringCode.Length <= 0 && mBankCode == "008") // kalau header Mandiri, tapi bank employee nya nggak ada informasi Kliring Code nya
                        {
                            Sm.StdMsg(mMsgType.Warning, lBank[i].EmpName + " (" + lBank[i].EmpCode + "). His/her bank account (" + lBank[i].EmpBankName + ") has no Kliring Bank Code information.");
                            return;
                        }

                        if (lBank[i].RTGSCode.Length <= 0 && mBankCode == "008") // kalau header Mandiri, tapi bank employee nya nggak ada informasi RTGS Code nya
                        {
                            Sm.StdMsg(mMsgType.Warning, lBank[i].EmpName + " (" + lBank[i].EmpCode + "). His/her bank account (" + lBank[i].EmpBankName + ") has no RTGS Bank Code information.");
                            return;
                        }

                        if (lBank[i].BankBranch.Length <= 0 && mBankCode == "009") // kalau header BNI tapi nggak ada informasi cabang mana
                        {
                            Sm.StdMsg(mMsgType.Warning, lBank[i].EmpName + " (" + lBank[i].EmpCode + ") has no Bank Branch information.");
                            return;
                        }
                    }

                    var lHMandiri = new List<HMandiri>();
                    var lDMandiri = new List<DMandiri>();
                    var lHBNI = new List<HBNI>();
                    var lDBNI = new List<DBNI>();

                    for (int x = 0; x < lBank.Count; x++)
                    {
                        // 008 = Mandiri
                        // 009 = BNI                        

                        if (mBankCode == "008")//if (lBank[x].BankCode == "008")
                        {
                            lDMandiri.Add(new DMandiri()
                            {
                                EmpAcNo = lBank[x].EmpAcNo,
                                EmpAcName = lBank[x].EmpAcName,
                                CurCode = lBank[x].CurCode,
                                THP = lBank[x].THP,
                                Remark = lBank[x].Remark,
                                FTS = lBank[x].FTS,
                                BankName = lBank[x].EmpBankName,
                                BeneficiaryNotFlag = lBank[x].BeneficiaryNotFlag,
                                Email = lBank[x].Email,
                                DocNo = lBank[x].DocNo,
                                EmpBankCode = lBank[x].EmpBankCode,
                                SwiftBeneCode = lBank[x].SwiftBeneCode,
                                KliringCode = lBank[x].KliringCode,
                                RTGSCode = lBank[x].RTGSCode,
                            });
                        }
                        else if (mBankCode == "009") // if (lBank[x].BankCode == "009")
                        {
                            lDBNI.Add(new DBNI()
                            {
                                EmpAcNo = lBank[x].EmpAcNo,
                                EmpAcName = lBank[x].EmpAcName,
                                CurCode = lBank[x].CurCode,
                                THP = lBank[x].THP,
                                VRDocNo = lBank[x].VRDocNo,
                                EmpBankBranch = lBank[x].BankBranch,
                                EmpBankCode = lBank[x].EmpBankCode,
                                EmpBankName = lBank[x].EmpBankName,
                                ReferenceNo = lBank[x].ReferenceNo,
                                CreditTo = mCreditTo,
                                SwiftBeneCode = lBank[x].SwiftBeneCode,
                                ReportTitle = Sm.GetParameter("ReportTitle1"),
                                Remark = lBank[x].Remark,
                                Email = lBank[x].Email
                            });
                        }
                        else
                        {
                            Sm.StdMsg(mMsgType.Info, "Bank Name field only support Mandiri or BNI for CSV process.");
                            return;
                        }
                    }

                    if (lDMandiri.Count > 0)
                    {
                        decimal mTotAmt = 0m;

                        for (int y = 0; y < lDMandiri.Count; y++)
                        {
                            mTotAmt += lDMandiri[y].THP;
                        }

                        lHMandiri.Add(new HMandiri()
                        {
                            VRDt = mDocDt.Substring(0, 8),
                            CreditTo = mCreditTo,
                            CountEmp = lDMandiri.Count,
                            TotalAmt = mTotAmt
                        });

                        ExportCSVMandiri(ref lDMandiri, ref lHMandiri);
                    }

                    if (lDBNI.Count > 0)
                    {
                        decimal mTotAmt2 = 0m;
                        string Dt = mDocDt.Substring(0, 8);

                        for (int y = 0; y < lDBNI.Count; y++)
                        {
                            mTotAmt2 += lDBNI[y].THP;
                        }

                        lHBNI.Add(new HBNI()
                        {
                            TotalAmt = mTotAmt2,
                            CreditTo = mCreditTo,
                            ReportTitle = Sm.GetParameter("ReportTitle1")
                        });

                        ExportCSVBNI(ref lDBNI, ref lHBNI, Dt);
                    }
                }
        }

        //private void UpdateCSVInd(string DocNo)
        //{
        //    var cml = new List<MySqlCommand>();

        //    cml.Add(UpdateCSVInd2(DocNo));

        //    Sm.ExecCommands(cml);

        //    //BtnCSV.Enabled = false;
        //    //BtnCSV.Visible = false;
        //}

        //private MySqlCommand UpdateCSVInd2(string DocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblVoucherRequestPayrollHdr ");
        //    SQL.AppendLine("    Set CSVInd = 'Y', LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
        //    SQL.AppendLine("Where DocNo = @DocNo; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
        //    return cm;
        //}


        private void ExportCSVMandiri(ref List<DMandiri> lD, ref List<HMandiri> lH)
        {
            try
            {
                // format : company code + 12 (payroll mix) + yyyymmdd hari ini + sequence trx (max 6 digit, aku isi HHiiss)
                var FileName = mCompanyCodeForMandiriPayroll + "12" + /*Sm.Left(Sm.ServerCurrentDateTime(), 8)*/mDocDt.Substring(0, 8) +Sm.Right(Sm.GetValue("Select Date_Format(Now(), '%H%i%s') As Tm;"), 6) + ".txt";
                if (lD.Count > 0)
                {
                    CreateCSVFileMandiri(ref lD, ref lH, FileName);

                    if (IsBankMandiriDataNotValid() || Sm.StdMsgYN("Question", "Bank Mandiri" + Environment.NewLine + "Do you want to send the exported data ?") == DialogResult.No) return;

                    SendDataMandiri(mPathToSaveExportedMandiriPayroll, FileName);

                    //UpdateCSVInd(mDocNo);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ExportCSVBNI(ref List<DBNI> lD, ref List<HBNI> lH, string Dt)
        {
            try
            {
                var Doctitle = Sm.GetParameter("DocTitle");
                var FileName = Doctitle + "_" + /*Sm.ServerCurrentDateTime()*/Sm.Right(Sm.GetValue("Select Date_Format(Now(), '%Y%m%d%H%i%s') As Tm;"), 14) + ".csv";
                if (lD.Count > 0)
                {
                    CreateCSVFileBNI(ref lD, ref lH, FileName, Dt);

                    if (IsBankBNIDataNotValid() || Sm.StdMsgYN("Question", "Bank BNI" + Environment.NewLine + "Do you want to send the exported data ?") == DialogResult.No) return;

                    SendDataBNI(mPathToSaveExportedBNIPayroll, FileName);

                    //UpdateCSVInd(mDocNo);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsBankMandiriDataNotValid()
        {
            if (mProtocolForMandiriPayroll.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Protocol (Mandiri) is empty.");
                return true;
            }

            if (mHostAddrForMandiriPayroll.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address (Mandiri) is empty.");
                return true;
            }

            if (mSharedFolderForMandiriPayroll.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder (Mandiri) is empty.");
                return true;
            }

            if (mUserNameForMandiriPayroll.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username (Mandiri) is empty.");
                return true;
            }

            if (mPortForMandiriPayroll.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number (Mandiri) is empty.");
                return true;
            }

            if (mCompanyCodeForMandiriPayroll.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for company code (Mandiri) is empty.");
                return true;
            }

            return false;
        }

        private bool IsBankBNIDataNotValid()
        {
            if (mProtocolForBNIPayroll.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Protocol (BNI) is empty.");
                return true;
            }

            if (mHostAddrForBNIPayroll.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address (BNI) is empty.");
                return true;
            }

            if (mSharedFolderForBNIPayroll.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder (BNI) is empty.");
                return true;
            }

            if (mUserNameForBNIPayroll.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username (BNI) is empty.");
                return true;
            }

            if (mPortForBNIPayroll.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number (BNI) is empty.");
                return true;
            }

            return false;
        }

        private void CreateCSVFileMandiri(ref List<DMandiri> lD, ref List<HMandiri> lH, string FileName)
        {
            StreamWriter sw = new StreamWriter(mPathToSaveExportedMandiriPayroll + FileName, false, Encoding.GetEncoding(1252));

            foreach (var i in lH)
            {
                #region CSV
                //sw.Write(
                //    "p," + i.VRDt + "," + i.CreditTo + "," + i.CountEmp + "," + i.TotalAmt
                //);
                #endregion

                #region TXT
                sw.Write(
                    "P;" + /*Sm.Left(Sm.ServerCurrentDateTime(), 8)*/i.VRDt + ";" + i.CreditTo + ";" + i.CountEmp + ";" + i.TotalAmt
                );
                #endregion
            }

            foreach (var x in lD)
            {
                #region CSV
                //sw.Write(sw.NewLine);
                //sw.Write(
                //    x.EmpAcNo + "," + x.EmpAcName + ",,,," + x.CurCode + "," + x.THP + "," + x.Remark + ",," + x.FTS + ",," + x.BankName +
                //    ",,,,," + x.BeneficiaryNotFlag + "," + x.Email + ",,,,,,,,,,,,,,,,,,,,,,," + x.DocNo
                //);
                #endregion

                #region TXT
                sw.Write(sw.NewLine);
                if (x.EmpBankCode == "008") // kalau employee itu Mandiri
                {
                    sw.Write(
                        x.EmpAcNo + ";" + x.EmpAcName.Replace(';', ' ') + ";;;;" + x.CurCode + ";" + x.THP + ";" + x.Remark.Replace(';', ' ') + ";;" + x.FTS + ";;" + x.BankName.Replace(';', ' ') +
                        ";;;;;" + x.BeneficiaryNotFlag + ";" + x.Email + ";;;;;;;;;;;;;;;;;;;;;OUR;;" + x.DocNo
                    );
                }
                else // kalau employee itu non Mandiri
                {
                    if (x.THP > 500000000) // kalau THP diatas 500jt
                    {
                        sw.Write(
                            x.EmpAcNo + ";" + x.EmpAcName.Replace(';', ' ') + ";;;;" + x.CurCode + ";" + x.THP + ";" + x.Remark.Replace(';', ' ') + ";;" + "RBU" + ";" + x.RTGSCode.Replace(';', ' ') + ";" + x.BankName.Replace(';', ' ') +
                            ";;;;;" + x.BeneficiaryNotFlag + ";" + x.Email + ";;;;;;;;;;;;;;;;;;;;;OUR;;" + x.DocNo
                        );
                    }
                    else
                    {
                        sw.Write(
                            x.EmpAcNo + ";" + x.EmpAcName.Replace(';', ' ') + ";;;;" + x.CurCode + ";" + x.THP + ";" + x.Remark.Replace(';', ' ') + ";;" + "LBU" + ";" + x.KliringCode.Replace(';', ' ') + ";" + x.BankName.Replace(';', ' ') +
                            ";;;;;" + x.BeneficiaryNotFlag + ";" + x.Email + ";;;;;;;;;;;;;;;;;;;;;OUR;;" + x.DocNo
                        );
                    }
                }

                #endregion

            }
            sw.Close();
            Sm.StdMsg(mMsgType.Info, "Successfully export file to" + Environment.NewLine + mPathToSaveExportedMandiriPayroll + FileName);
            lD.Clear();
            lH.Clear();
        }

        private void SendDataMandiri(string sourceDrive, string FileName)
        {
            if (mProtocolForMandiriPayroll.ToUpper() == "FTP")
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", sourceDrive + FileName));
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForMandiriPayroll, mPortForMandiriPayroll, mSharedFolderForMandiriPayroll, toUpload.Name));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(mUserNameForMandiriPayroll, mPasswordForMandiriPayroll);

                Stream ftpStream = request.GetRequestStream();

                FileStream file = File.OpenRead(string.Format(@"{0}", sourceDrive + FileName));

                int length = 1024;
                byte[] buffer = new byte[length];
                int bytesRead = 0;

                do
                {
                    bytesRead = file.Read(buffer, 0, length);
                    ftpStream.Write(buffer, 0, bytesRead);
                }
                while (bytesRead != 0);

                file.Close();
                ftpStream.Close();
            }
            else if (mProtocolForMandiriPayroll.ToUpper() == "SFTP")
            {
                using (SftpClient client = new SftpClient(mHostAddrForMandiriPayroll, Int32.Parse(mPortForMandiriPayroll), mUserNameForMandiriPayroll, mPasswordForMandiriPayroll))
                {
                    string destinationPath = string.Format(@"{0}{1}{0}", "/", mSharedFolderForMandiriPayroll);
                    client.Connect();
                    client.ChangeDirectory(destinationPath);
                    using (FileStream fs = new FileStream(string.Format(@"{0}", sourceDrive + FileName), FileMode.Open))
                    {
                        client.BufferSize = 4 * 1024;
                        client.UploadFile(fs, Path.GetFileName(string.Format(@"{0}", sourceDrive + FileName)), null);
                    }
                    client.Dispose();
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "Unknown Protocol.");
                return;
            }

            Sm.StdMsg(mMsgType.Info, "File uploaded to Mandiri Server.");
        }

        private void CreateCSVFileBNI(ref List<DBNI> lD, ref List<HBNI> lH, string FileName, string Dt)
        {
            StreamWriter sw = new StreamWriter(mPathToSaveExportedBNIPayroll + FileName, false, Encoding.GetEncoding(1252));

            foreach (var x in lD)
            {
                sw.WriteLine(
                    "MT100," + x.ReferenceNo + "," + Sm.Left(Sm.ServerCurrentDateTime(), 8) + "," + x.CurCode + "," +
                    Math.Round(x.THP, 0) + "," + x.ReportTitle + "," + x.CreditTo + ",,,,,,," + x.SwiftBeneCode + "," + x.EmpBankName.Replace(',', ' ') + "," +
                    x.EmpBankBranch.Replace(',', ' ') + ",," + x.EmpAcNo + "," + x.EmpAcName.Replace(',', ' ') + "," + x.Remark.Replace(',', ' ') + ",,OUR," + x.Email
                );
            }
            sw.Close();
            Sm.StdMsg(mMsgType.Info, "Successfully export file to" + Environment.NewLine + mPathToSaveExportedBNIPayroll + FileName);
            lD.Clear();
            lH.Clear();
        }

        private void SendDataBNI(string sourceDrive, string FileName)
        {
            if (mProtocolForBNIPayroll.ToUpper() == "FTP")
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", sourceDrive + FileName));
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForBNIPayroll, mPortForBNIPayroll, mSharedFolderForBNIPayroll, toUpload.Name));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(mUserNameForBNIPayroll, mPasswordForBNIPayroll);

                Stream ftpStream = request.GetRequestStream();

                FileStream file = File.OpenRead(string.Format(@"{0}", sourceDrive + FileName));

                int length = 1024;
                byte[] buffer = new byte[length];
                int bytesRead = 0;

                do
                {
                    bytesRead = file.Read(buffer, 0, length);
                    ftpStream.Write(buffer, 0, bytesRead);
                }
                while (bytesRead != 0);

                file.Close();
                ftpStream.Close();
            }
            else if (mProtocolForBNIPayroll.ToUpper() == "SFTP")
            {

                using (SftpClient client = new SftpClient(mHostAddrForBNIPayroll, Int32.Parse(mPortForBNIPayroll), mUserNameForBNIPayroll, mPasswordForBNIPayroll))
                {
                    string destinationPath = string.Format(@"{0}{1}{0}", "/", mSharedFolderForBNIPayroll);
                    client.Connect();
                    client.ChangeDirectory(destinationPath);
                    using (FileStream fs = new FileStream(string.Format(@"{0}", sourceDrive + FileName), FileMode.Open))
                    {
                        client.BufferSize = 4 * 1024;
                        client.UploadFile(fs, Path.GetFileName(string.Format(@"{0}", sourceDrive + FileName)), null);
                    }
                    client.Dispose();
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "Unknown protocol.");
                return;
            }

            Sm.StdMsg(mMsgType.Info, "File uploaded to BNI Server.");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
       
        #endregion

        #region Class

        private class Bank
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpAcNo { get; set; }
            public string EmpAcName { get; set; }
            public string CurCode { get; set; }
            public string EmpBankCode { get; set; }
            public string EmpBankName { get; set; }
            public decimal THP { get; set; }
            public string Remark { get; set; }
            public string FTS { get; set; }
            public string BeneficiaryNotFlag { get; set; }
            public string Email { get; set; }
            public string DocNo { get; set; }
            public string Periode { get; set; }
            public string VRDocNo { get; set; }
            public string BankBranch { get; set; }
            public string ReferenceNo { get; set; }
            public string SwiftBeneCode { get; set; }
            public string KliringCode { get; set; }
            public string RTGSCode { get; set; }
        }

        private class HMandiri
        {
            public string VRDt { get; set; }
            public string CreditTo { get; set; }
            public decimal CountEmp { get; set; }
            public decimal TotalAmt { get; set; }
        }

        private class DMandiri
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpAcNo { get; set; }
            public string EmpAcName { get; set; }
            public string CurCode { get; set; }
            public decimal THP { get; set; }
            public string Remark { get; set; }
            public string FTS { get; set; }
            public string BankCode { get; set; }
            public string BankName { get; set; }
            public string BeneficiaryNotFlag { get; set; }
            public string Email { get; set; }
            public string DocNo { get; set; }
            public string EmpBankCode { get; set; }
            public string SwiftBeneCode { get; set; }
            public string KliringCode { get; set; }
            public string RTGSCode { get; set; }
        }

        private class HBNI
        {
            public decimal TotalAmt { get; set; }
            public string CreditTo { get; set; }
            public string Periode { get; set; }
            public string ReportTitle { get; set; }
        }

        private class DBNI
        {
            public string EmpAcNo { get; set; }
            public string EmpAcName { get; set; }
            public string CurCode { get; set; }
            public decimal THP { get; set; }
            public string VRDocNo { get; set; }
            public string EmpBankBranch { get; set; }
            public string EmpBankName { get; set; }
            public string EmpBankCode { get; set; }
            public string ReferenceNo { get; set; }
            public string CreditTo { get; set; }
            public string SwiftBeneCode { get; set; }
            public string ReportTitle { get; set; }
            public string Remark { get; set; }
            public string Email { get; set; }
        }


        #endregion

    }
}
