﻿#region Update
    // 15/06/2017 [TKG] tambah informasi lot pada saat pencarian bin
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBinTransfer : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mWhsCode = string.Empty, mWhsCode2 = string.Empty,
            mDocType = "20", mDocType2 = "19",
            mDocNo = string.Empty; //if this application is called from other application;
        private bool mIsInventoryShowTotalQty = false;
        internal int mNumberOfInventoryUomCode = 1;
        internal FrmBinTransferFind FrmFind;

        #endregion

        #region Constructor

        public FrmBinTransfer(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Bin Transfer";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                SetNumberOfInventoryUomCode();
                mIsInventoryShowTotalQty = (Sm.GetParameter("IsInventoryShowTotalQty") == "Y");
                SetGrd();
                SetFormControl(mState.View);

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Sequence#",
                        
                        //1-5
                        "Requested"+Environment.NewLine+"DNo",
                        "Bin",
                        "Item's Code",
                        "",
                        "Local Code",
                        
                        //6-10
                        "Item's Name",
                        "Batch#",
                        "Source",
                        "Lot",
                        "Stock",

                        //11-15
                        "Requested"+Environment.NewLine+"Quantity",
                        "Transferred"+Environment.NewLine+"Quantity",
                        "UoM",
                        "Stock",
                        "Requested"+Environment.NewLine+"Quantity",
                        
                        //16-20
                        "Transferred"+Environment.NewLine+"Quantity",
                        "UoM",
                        "Stock",
                        "Requested"+Environment.NewLine+"Quantity",
                        "Transferred"+Environment.NewLine+"Quantity",
                        
                        //21-22
                        "UoM",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        80, 60, 100, 20, 100, 
                        
                        //6-10
                        300, 150, 150, 60, 100, 
                        
                        //11-15
                        100, 100, 100, 100, 100, 
                        
                        //16-20
                        100, 100, 100, 100, 100, 
                        
                        //21-22
                        100, 400
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 12, 14, 15, 16, 18, 19, 20 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 3, 4, 5, 8, 14, 15, 16, 17, 18, 19, 20, 21 }, false);
            Grd1.Cols[9].Move(3);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 8;
            Sm.GrdHdrWithColWidth(Grd2, 
                new string[] { 
                    //0
                    "",
                    
                    //1-5
                    "Bin",
                    "Quantity",
                    "UoM",
                    "Quantity",
                    "UoM",
                    
                    //6-7
                    "Quantity",
                    "UoM"
                }, 
                new int[] { 
                    //0
                    20, 
 
                    //1-5
                    60, 80, 80, 80, 80, 

                    //6-7
                    80, 80
                });
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdFormatDec(Grd2, new int[] { 2, 4, 6 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 4, 5, 6, 7 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 2, 3, 4, 5, 6, 7 });

            #endregion

            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 5, 8 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 4, 5 }, true);
                PanelTotal2.Visible = true;
            }
            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19, 20, 21 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 4, 5, 6, 7 }, true);
                PanelTotal2.Visible = true;
                PanelTotal3.Visible = true;
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, MeeRemark }, true);
                    BtnBinTransferRequestDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 5, 6, 7 });
                    
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, MeeRemark }, false);
                    BtnBinTransferRequestDocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 12, 16, 20, 22 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0 });
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtBinTransferRequestDocNo, TxtWhsCode, TxtWhsCode2, 
                MeeRemark, TxtInventoryUomCode, TxtInventoryUomCode2, TxtInventoryUomCode3 
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtTotal, TxtTotal2, TxtTotal3 
            }, 0);

            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.FocusGrd(Grd2, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10, 11, 12, 14, 15, 16, 18, 19, 20 });
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 2, 4, 6 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBinTransferFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                BtnBinTransferRequestDocNo_Click(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 12, 16, 20 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 22 }, e);

            if (e.ColIndex == 12 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 13), Sm.GetGrdStr(Grd1, e.RowIndex, 17)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 16, Grd1, e.RowIndex, 12);

            if (e.ColIndex == 12 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 13), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 20, Grd1, e.RowIndex, 12);

            if (e.ColIndex == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 20, Grd1, e.RowIndex, 16);

            if (Sm.IsGrdColSelected(new int[] { 12, 16, 20 }, e.ColIndex)) ShowBinInfo();
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10, 11, 12, 14, 15, 16, 18, 19, 20 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total +=
                        Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Additional Method

        private void SetNumberOfInventoryUomCode()
        {
            try
            {
                string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
                if (NumberOfInventoryUomCode.Length == 0)
                    mNumberOfInventoryUomCode = 1;
                else
                    mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 5) +
                            Sm.GetGrdStr(Grd1, Row, 9) +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            Sm.GetGrdStr(Grd1, Row, 11) +
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GetSelectedItem2()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            Sm.GetGrdStr(Grd1, Row, 11) +
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedBin()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode2);
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 10).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 10);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {

                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 10), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 11), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 4), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 12, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 15, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 18, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        //internal void ShowInfo(string DocNo)
        //{
        //    ClearGrd();

        //    var SQL = new StringBuilder();

        //    //SQL.AppendLine("Select A.DNo, A.Bin, A.ItCode, B.ItCodeInternal, B.ItName, A.BatchNo, A.Source, A.Lot, ");
        //    //SQL.AppendLine("IfNull(C.Stock, 0) As Stock, A.Qty, B.InventoryUomCode, ");
        //    //SQL.AppendLine("IfNull(C.Stock2, 0) As Stock2, A.Qty2, B.InventoryUomCode2, ");
        //    //SQL.AppendLine("IfNull(C.Stock3, 0) As Stock3, A.Qty3, B.InventoryUomCode3 ");
        //    //SQL.AppendLine("From TblBinTransferRequestDtl2 A ");
        //    //SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
        //    //SQL.AppendLine("Left Join ( ");
        //    //SQL.AppendLine("    Select ItCode, BatchNo, Source, Lot, Bin, ");
        //    //SQL.AppendLine("    Sum(Qty) Stock, Sum(Qty2) Stock2, Sum(Qty3) Stock3  ");
        //    //SQL.AppendLine("    From TblStockSummary ");
        //    //SQL.AppendLine("    Where WhsCode=@WhsCode ");
        //    //SQL.AppendLine("    And Bin In (Select Bin From TblBinTransferRequestDtl Where DocNo=@DocNo And CancelInd='N') ");
        //    //SQL.AppendLine("    Group By ItCode, BatchNo, Source, Lot, Bin ");
        //    //SQL.AppendLine(") C ");
        //    //SQL.AppendLine("    On A.ItCode=C.ItCode ");
        //    //SQL.AppendLine("    And A.BatchNo=C.BatchNo ");
        //    //SQL.AppendLine("    And A.Source=C.Source ");
        //    //SQL.AppendLine("    And A.Lot=C.Lot ");
        //    //SQL.AppendLine("    And A.Bin=C.Bin ");
        //    //SQL.AppendLine("Where A.DocNo=@DocNo And A.CancelInd='N' ");
        //    //SQL.AppendLine("Order By A.Bin, B.ItName, A.DNo;");

        //    //SQL.AppendLine("Select A.DNo, A.Bin, A.ItCode, B.ItCodeInternal, B.ItName, A.BatchNo, A.Source, A.Lot, ");
        //    //SQL.AppendLine("IfNull(C.Stock, 0) As Stock, A.Qty, B.InventoryUomCode, ");
        //    //SQL.AppendLine("IfNull(C.Stock2, 0) As Stock2, A.Qty2, B.InventoryUomCode2, ");
        //    //SQL.AppendLine("IfNull(C.Stock3, 0) As Stock3, A.Qty3, B.InventoryUomCode3 ");
        //    //SQL.AppendLine("From TblBinTransferRequestDtl2 A ");
        //    //SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
        //    //SQL.AppendLine("Left Join ( ");
        //    //SQL.AppendLine("    Select Source, Lot, Bin, ");
        //    //SQL.AppendLine("    Sum(Qty) Stock, Sum(Qty2) Stock2, Sum(Qty3) Stock3  ");
        //    //SQL.AppendLine("    From TblStockSummary ");
        //    //SQL.AppendLine("    Where WhsCode=@WhsCode ");
        //    //SQL.AppendLine("    And Bin In (Select Bin From TblBinTransferRequestDtl Where DocNo=@DocNo And CancelInd='N') ");
        //    //SQL.AppendLine("    Group By Source, Lot, Bin ");
        //    //SQL.AppendLine(") C ");
        //    //SQL.AppendLine("    On A.Source=C.Source ");
        //    //SQL.AppendLine("    And A.Lot=C.Lot ");
        //    //SQL.AppendLine("    And A.Bin=C.Bin ");
        //    //SQL.AppendLine("Where A.DocNo=@DocNo And A.CancelInd='N' ");
        //    //SQL.AppendLine("Order By A.Bin, B.ItName, A.DNo;");

        //    SQL.AppendLine("Select A.DNo, A.Bin, A.ItCode, B.ItCodeInternal, B.ItName, A.BatchNo, A.Source, A.Lot, ");
        //    SQL.AppendLine("IfNull(C.Qty, 0) As Stock, A.Qty, B.InventoryUomCode, ");
        //    SQL.AppendLine("IfNull(C.Qty2, 0) As Stock2, A.Qty2, B.InventoryUomCode2, ");
        //    SQL.AppendLine("IfNull(C.Qty3, 0) As Stock3, A.Qty3, B.InventoryUomCode3 ");
        //    SQL.AppendLine("From TblBinTransferRequestDtl2 A ");
        //    SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
        //    SQL.AppendLine("Left Join TblStockSummary C ");
        //    SQL.AppendLine("    On A.Source=C.Source ");
        //    SQL.AppendLine("    And A.Lot=C.Lot ");
        //    SQL.AppendLine("    And A.Bin=C.Bin ");
        //    SQL.AppendLine("    And C.WhsCode=@WhsCode ");
        //    SQL.AppendLine("    And C.Bin In (Select Bin From TblBinTransferRequestDtl Where DocNo=@DocNo And CancelInd='N') ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo And A.CancelInd='N' ");
        //    SQL.AppendLine("Order By A.Bin, B.ItName, A.DNo;");

        //    var cm = new MySqlCommand();

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);

        //    Sm.ShowDataInGrid(
        //        ref Grd1, ref cm, SQL.ToString(),
        //        new string[] 
        //        { 
        //            //0
        //            "DNo",  

        //            //1-5
        //            "Bin", "ItCode", "ItCodeInternal", "ItName", "BatchNo", 
                    
        //            //6-10
        //            "Source", "Lot", "Stock", "Qty", "InventoryUomCode", 
                    
        //            //11-15
        //            "Stock2", "Qty2", "InventoryUomCode2", "Stock3", "Qty3", 
                    
        //            //16
        //            "InventoryUomCode3"                             
        //        },
        //        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
        //        {
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
        //            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
        //            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
        //            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
        //            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
        //            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
        //            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 12);
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
        //            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
        //            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 15);
        //            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 15);
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 16);

        //            InsertSummary(Sm.GetGrdStr(Grd, Row, 2));
        //        }, false, false, true, false
        //    );
        //    ShowBinInfo();
        //    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 11, 12, 14, 15, 16, 18, 19, 20 });
        //    Sm.FocusGrd(Grd1, 0, 1);
        //    if (Sm.GetGrdStr(Grd2, Grd2.Rows.Count - 1, 1).Length != 0)
        //    {
        //        Grd2.Rows.Add();
        //        Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 2, 4, 6 });
        //    }
        //}

        internal void InsertSummary(string Bin)
        {
            bool NotFound = true;
            int index = 0;

            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd2, row, 1), Bin))
                {
                    NotFound = false;
                    index = row;
                    break;
                }
            }
            if (NotFound)
            {
                if (!(Grd2.Rows.Count == 1 && Sm.GetGrdStr(Grd2, 0, 1).Length == 0)) Grd2.Rows.Add();
                Grd2.Cells[Grd2.Rows.Count - 1, 1].Value = Bin;
                Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 2, 4, 6 });
            }
        }

        private void ShowBinInfo()
        {
            decimal Total = 0m, Total2 = 0m, Total3 = 0m;
            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd2, row, 1).Length != 0)
                {
                    ShowBinInfo(Sm.GetGrdStr(Grd2, row, 1), row);
                    Total += Sm.GetGrdDec(Grd2, row, 2);
                    Total2 += Sm.GetGrdDec(Grd2, row, 4);
                    Total3 += Sm.GetGrdDec(Grd2, row, 6);
                }
            }
            TxtTotal.EditValue = Sm.FormatNum(Total, 0);
            TxtTotal2.EditValue = Sm.FormatNum(Total2, 0);
            TxtTotal3.EditValue = Sm.FormatNum(Total3, 0);
        }

        private void ShowBinInfo(string Bin, int Row)
        {
            bool IsFirst = true;
            decimal Qty = 0m, Qty2 = 0m, Qty3 = 0m;
            for (int Index = 0; Index < Grd1.Rows.Count; Index++)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Index, 2), Bin))
                { 
                    if (IsFirst)
                    {
                        Grd2.Cells[Row, 3].Value = Sm.GetGrdStr(Grd1, Index, 13);
                        Grd2.Cells[Row, 5].Value = Sm.GetGrdStr(Grd1, Index, 17);
                        Grd2.Cells[Row, 6].Value = Sm.GetGrdStr(Grd1, Index, 21);
                        TxtInventoryUomCode.Text = Sm.GetGrdStr(Grd1, Index, 13);
                        TxtInventoryUomCode2.Text = Sm.GetGrdStr(Grd1, Index, 17);
                        TxtInventoryUomCode3.Text = Sm.GetGrdStr(Grd1, Index, 21);
                        IsFirst = false;
                    }
                    Qty += Sm.GetGrdDec(Grd1, Index, 12);
                    Qty2 += Sm.GetGrdDec(Grd1, Index, 16);
                    Qty3 += Sm.GetGrdDec(Grd1, Index, 20);
                }
            }
            Grd2.Cells[Row, 2].Value = Qty;
            Grd2.Cells[Row, 4].Value = Qty2;
            Grd2.Cells[Row, 6].Value = Qty3;
        }

        internal void InsertItem(string Bin)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select A.DNo, A.Bin, A.ItCode, B.ItCodeInternal, B.ItName, A.BatchNo, A.Source, A.Lot, ");
            //SQL.AppendLine("IfNull(C.Stock, 0) As Stock, A.Qty, B.InventoryUomCode, ");
            //SQL.AppendLine("IfNull(C.Stock2, 0) As Stock2, A.Qty2, B.InventoryUomCode2, ");
            //SQL.AppendLine("IfNull(C.Stock3, 0) As Stock3, A.Qty3, B.InventoryUomCode3 ");
            //SQL.AppendLine("From TblBinTransferRequestDtl2 A ");
            //SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select ItCode, BatchNo, Source, Lot, Bin, ");
            //SQL.AppendLine("    Sum(Qty) Stock, Sum(Qty2) Stock2, Sum(Qty3) Stock3  ");
            //SQL.AppendLine("    From TblStockSummary ");
            //SQL.AppendLine("    Where Bin=@Bin And WhsCode=@WhsCode ");
            //SQL.AppendLine("    And (Qty<>0 Or Qty2<>0 Or Qty3<>0) ");
            //SQL.AppendLine("    Group By ItCode, BatchNo, Source, Lot, Bin ");
            //SQL.AppendLine(") C ");
            //SQL.AppendLine("    On A.ItCode=C.ItCode ");
            //SQL.AppendLine("    And A.BatchNo=C.BatchNo ");
            //SQL.AppendLine("    And A.Source=C.Source ");
            //SQL.AppendLine("    And A.Lot=C.Lot ");
            //SQL.AppendLine("    And A.Bin=C.Bin ");
            //SQL.AppendLine("Where A.DocNo=@BinTransferRequestDocNo And A.CancelInd='N' And A.Bin=@Bin ");
            //SQL.AppendLine("Order By A.Bin, B.ItName, A.DNo;");

            SQL.AppendLine("Select A.DNo, A.Bin, A.ItCode, B.ItCodeInternal, B.ItName, A.BatchNo, A.Source, A.Lot, ");
            SQL.AppendLine("IfNull(C.Qty, 0) As Stock, A.Qty, B.InventoryUomCode, ");
            SQL.AppendLine("IfNull(C.Qty2, 0) As Stock2, A.Qty2, B.InventoryUomCode2, ");
            SQL.AppendLine("IfNull(C.Qty3, 0) As Stock3, A.Qty3, B.InventoryUomCode3 ");
            SQL.AppendLine("From TblBinTransferRequestDtl2 A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblStockSummary C ");
            SQL.AppendLine("    On A.Source=C.Source ");
            SQL.AppendLine("    And A.Lot=C.Lot ");
            SQL.AppendLine("    And C.Bin=@Bin ");
            SQL.AppendLine("    And C.WhsCode=@WhsCode ");
            SQL.AppendLine("Where A.DocNo=@BinTransferRequestDocNo And A.CancelInd='N' And A.Bin=@Bin ");
            SQL.AppendLine("Order By B.ItName, A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@BinTransferRequestDocNo", TxtBinTransferRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
            Sm.CmParam<String>(ref cm, "@Bin", Bin);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                        new string[] 
                        { 
                            //0
                            "DNo",  

                            //1-5
                            "Bin", "ItCode", "ItCodeInternal", "ItName", "BatchNo", 
                            
                            //6-10
                            "Source", "Lot", "Stock", "Qty", "InventoryUomCode", 
                            
                            //11-15
                            "Stock2", "Qty2", "InventoryUomCode2", "Stock3", "Qty3", 
                            
                            //16
                            "InventoryUomCode3"       
                        }
                        );
                    if (!dr.HasRows)
                    {
                        Sm.StdMsg(mMsgType.NoData, "");
                    }
                    else
                    {
                        if (Grd1.Rows.Count >= 1)
                        {
                            if (Sm.GetGrdStr(Grd1, Grd1.Rows.Count - 1, 3).Length != 0)
                                Grd1.Rows.Add();
                        }
                        else
                            Grd1.Rows.Add();
                        int Row = Grd1.Rows.Count - 1;
                        Grd1.ProcessTab = true;
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 13);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 19, 15);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 20, 15);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 16);
                            Row++;
                        }
                        dr.Dispose();
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 11, 12, 14, 15, 16, 18, 19, 20 });
                        Sm.FocusGrd(Grd1, 0, 1);
                        Grd1.EndUpdate();
                        ShowBinInfo();
                    }
                }
                cm.Dispose();
            }
        }

        private bool IsBinAlreadyExist()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select Distinct Bin From TblBinTransferRequestDtl Where DocNo=@BinTransferRequestDocNo And CancelInd='N' " +
                              "And Locate(Concat('##', Bin, '##'), @SelectedBin)<1"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SelectedBin", GetSelectedBin());
            
            if (Sm.IsDataExist(cm))
            {
                return false;
            }
            return false;
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BinTransfer", "TblBinTransferHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveBinTransferHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveBinTransferDtl(DocNo, Row));

            cml.Add(SaveStock1(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    cml.Add(SaveStock2(Row));
                    cml.Add(SaveStock3(Row));
                }
            }

            cml.Add(SaveBinStatus(DocNo));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            ReComputeStock();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtBinTransferRequestDocNo, "Request document#", false) ||
                IsBinTransferRequestDocNoNotValid() ||
                IsBinNotValid() ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid();
        }

        private bool IsBinTransferRequestDocNoNotValid()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblBinTransferRequestHdr " +
                    "Where IfNull(ProcessInd, 'O')='O' And DocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtBinTransferRequestDocNo.Text);

            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Requested document is not valid.");
                return true;
            }
            return false;
        }

        private bool IsBinNotValid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Bin From TblBinTransferRequestDtl ");
            SQL.AppendLine("Where CancelInd='Y' ");
            SQL.AppendLine("And DocNo=@DocNo ");
            SQL.AppendLine("And Locate(Concat('##', Bin, '##'), @SelectedBin)>0 Limit 1; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString()};
            Sm.CmParam<String>(ref cm, "@DocNo", TxtBinTransferRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SelectedBin", GetSelectedBin());

            string Bin = Sm.GetValue(cm);
            if (Bin.Length!=0)
            {
                Sm.StdMsg(mMsgType.Warning, "Bin ("+Bin+") is not valid.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Bin is empty.")) return true;

                Msg =
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine + 
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                    "Local Code : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 12) > Sm.GetGrdDec(Grd1, Row, 10))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Transferred quantity should not be bigger than available stock.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 12) > Sm.GetGrdDec(Grd1, Row, 11))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Transferred quantity should not be bigger than requested quantity.");
                    return true;
                }

                if (Grd1.Cols[16].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 16) > Sm.GetGrdDec(Grd1, Row, 14))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Transferred quantity (2) should not be bigger than available stock (2).");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 16) > Sm.GetGrdDec(Grd1, Row, 15))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Transferred quantity (2) should not be bigger than requested quantity (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[20].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 20) > Sm.GetGrdDec(Grd1, Row, 18))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Transferred quantity (3) should not be bigger than available stock (3).");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 20) > Sm.GetGrdDec(Grd1, Row, 19))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Transferred quantity (3) should not be bigger than requested quantity (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveBinTransferHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblBinTransferHdr(DocNo, DocDt, BinTransferRequestDocNo, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @BinTransferRequestDocNo, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@BinTransferRequestDocNo", TxtBinTransferRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveBinTransferDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblBinTransferDtl(DocNo, DNo, BinTransferRequestDNo, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @BinTransferRequestDNo, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BinTransferRequestDNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBinStatus(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBinTransferRequestDtl T1 ");
            SQL.AppendLine("Inner Join ( ");
	        SQL.AppendLine("    Select Distinct C.DocNo, C.Bin ");
	        SQL.AppendLine("    From TblBinTransferHdr A ");
	        SQL.AppendLine("    Inner Join TblBinTransferDtl B On A.DocNo=B.DocNo ");
	        SQL.AppendLine("    Inner Join TblBinTransferRequestDtl2 C On A.BinTransferRequestDocNo=C.DocNo And B.BinTransferRequestDNo=C.DNo ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.Bin=T2.Bin ");
            SQL.AppendLine("Set T1.ProcessInd='F' ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And IfNull(T1.ProcessInd, 'O')='O'; ");

            SQL.AppendLine("Update TblBinTransferRequestHdr T Set ");
            SQL.AppendLine("    T.ProcessInd='F', T.LastUpBy=@UserCode, T.LastUpDt=CurrentDateTime()  ");
            SQL.AppendLine("Where T.DocNo=@BinTransferRequestDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblBinTransferRequestDtl ");
            SQL.AppendLine("    Where DocNo=@BinTransferRequestDocNo ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And ProcessInd='O'); ");

            SQL.AppendLine("Update TblBin Set Status='1' ");
            SQL.AppendLine("Where Bin In (");
            SQL.AppendLine("    Select Distinct C.Bin ");
            SQL.AppendLine("    From TblBinTransferHdr A ");
            SQL.AppendLine("    Inner Join TblBinTransferDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblBinTransferRequestDtl2 C ");
            SQL.AppendLine("        On A.BinTransferRequestDocNo=C.DocNo ");
            SQL.AppendLine("        And B.BinTransferRequestDNo=C.DNo ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@BinTransferRequestDocNo", TxtBinTransferRequestDocNo.Text);

            return cm;
        }

        //private MySqlCommand SaveStock(string DocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblStockMovement ");
        //    SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
        //    SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
        //    SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, D.Source, 'N', '', ");
        //    SQL.AppendLine("A.DocDt, C.WhsCode2, D.Lot, D.Bin, D.ItCode, D.BatchNo, ");
        //    SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
        //    SQL.AppendLine("From TblBinTransferHdr A ");
        //    SQL.AppendLine("Inner Join TblBinTransferDtl B On A.DocNo=B.DocNo And (B.Qty<>0 Or B.Qty2<>0 Or B.Qty3<>0) ");
        //    SQL.AppendLine("Inner Join TblBinTransferRequestHdr C On A.BinTransferRequestDocNo=C.DocNo ");
        //    SQL.AppendLine("Inner Join TblBinTransferRequestDtl2 D On A.BinTransferRequestDocNo=D.DocNo And B.BinTransferRequestDNo=D.DNo And D.CancelInd='N' ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo; ");

        //    SQL.AppendLine("Insert Into TblStockMovement ");
        //    SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
        //    SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
        //    SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @DocType2, A.BinTransferRequestDocNo, B.BinTransferRequestDNo, D.Source, 'N', '', ");
        //    SQL.AppendLine("A.DocDt, C.WhsCode, D.Lot, D.Bin, D.ItCode, D.BatchNo, ");
        //    SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, Null, @UserCode, CurrentDateTime() ");
        //    SQL.AppendLine("From TblBinTransferHdr A ");
        //    SQL.AppendLine("Inner Join TblBinTransferDtl B On A.DocNo=B.DocNo And (B.Qty<>0 Or B.Qty2<>0 Or B.Qty3<>0) ");
        //    SQL.AppendLine("Inner Join TblBinTransferRequestHdr C On A.BinTransferRequestDocNo=C.DocNo ");
        //    SQL.AppendLine("Inner Join TblBinTransferRequestDtl2 D On A.BinTransferRequestDocNo=D.DocNo And B.BinTransferRequestDNo=D.DNo And D.CancelInd='N' ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo; ");

        //    SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select C.WhsCode2, D.Lot, D.Bin, D.ItCode, D.BatchNo, D.Source, 0, 0, 0, Null, @UserCode, CurrentDateTime() ");
        //    SQL.AppendLine("From TblBinTransferHdr A ");
        //    SQL.AppendLine("Inner Join TblBinTransferDtl B On A.DocNo=B.DocNo And (B.Qty<>0 Or B.Qty2<>0 Or B.Qty3<>0) ");
        //    SQL.AppendLine("Inner Join TblBinTransferRequestHdr C On A.BinTransferRequestDocNo=C.DocNo ");
        //    SQL.AppendLine("Inner Join TblBinTransferRequestDtl2 D On A.BinTransferRequestDocNo=D.DocNo And B.BinTransferRequestDNo=D.DNo And D.CancelInd='N' ");
        //    SQL.AppendLine("Left Join TblStockSummary E ");
        //    SQL.AppendLine("    On C.WhsCode2=E.WhsCode ");
        //    SQL.AppendLine("    And D.Lot=E.Lot ");
        //    SQL.AppendLine("    And D.Bin=E.Bin ");
        //    SQL.AppendLine("    And D.Source=E.Source ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo And E.WhsCode Is Null; ");

        //    SQL.AppendLine("Update TblStockSummary As T1  ");
        //    SQL.AppendLine("Inner Join ( ");
        //    SQL.AppendLine("    Select C.WhsCode2 As WhsCode, D.Lot, D.Bin, D.Source, ");
        //    SQL.AppendLine("    Sum(B.Qty) As Qty, Sum(B.Qty2) As Qty2, Sum(B.Qty3) As Qty3 ");
        //    SQL.AppendLine("    From TblBinTransferHdr A ");
        //    SQL.AppendLine("    Inner Join TblBinTransferDtl B On A.DocNo=B.DocNo And (B.Qty<>0 Or B.Qty2<>0 Or B.Qty3<>0) ");
        //    SQL.AppendLine("    Inner Join TblBinTransferRequestHdr C On A.BinTransferRequestDocNo=C.DocNo ");
        //    SQL.AppendLine("    Inner Join TblBinTransferRequestDtl2 D On A.BinTransferRequestDocNo=D.DocNo And B.BinTransferRequestDNo=D.DNo And D.CancelInd='N' ");
        //    SQL.AppendLine("    Where A.DocNo=@DocNo ");
        //    SQL.AppendLine("    Group By C.WhsCode2, D.Lot, D.Bin, D.Source ");
        //    SQL.AppendLine(") T2 ");
        //    SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
        //    SQL.AppendLine("    And T1.Source=T2.Source ");
        //    SQL.AppendLine("    And T1.Lot=T2.Lot ");
        //    SQL.AppendLine("    And T1.Bin=T2.Bin ");
        //    SQL.AppendLine("Set ");
        //    SQL.AppendLine("    T1.Qty=T1.Qty+T2.Qty, T1.Qty2=T1.Qty2+T2.Qty2, T1.Qty3=T1.Qty3+T2.Qty3, ");
        //    SQL.AppendLine("    T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime(); ");

        //    SQL.AppendLine("Update TblStockSummary As T1  ");
        //    SQL.AppendLine("Inner Join ( ");
        //    SQL.AppendLine("    Select C.WhsCode, D.Lot, D.Bin, D.Source, ");
        //    SQL.AppendLine("    Sum(B.Qty) As Qty, Sum(B.Qty2) As Qty2, Sum(B.Qty3) As Qty3 ");
        //    SQL.AppendLine("    From TblBinTransferHdr A ");
        //    SQL.AppendLine("    Inner Join TblBinTransferDtl B On A.DocNo=B.DocNo And (B.Qty<>0 Or B.Qty2<>0 Or B.Qty3<>0) ");
        //    SQL.AppendLine("    Inner Join TblBinTransferRequestHdr C On A.BinTransferRequestDocNo=C.DocNo ");
        //    SQL.AppendLine("    Inner Join TblBinTransferRequestDtl2 D On A.BinTransferRequestDocNo=D.DocNo And B.BinTransferRequestDNo=D.DNo And D.CancelInd='N' ");
        //    SQL.AppendLine("    Where A.DocNo=@DocNo; ");
        //    SQL.AppendLine("    Group By C.WhsCode, D.Lot, D.Bin, D.Source, ");
        //    SQL.AppendLine(") T2 ");
        //    SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
        //    SQL.AppendLine("    And T1.Source=T2.Source ");
        //    SQL.AppendLine("    And T1.Lot=T2.Lot ");
        //    SQL.AppendLine("    And T1.Bin=T2.Bin ");
        //    SQL.AppendLine("Set ");
        //    SQL.AppendLine("    T1.Qty=T1.Qty-T2.Qty, T1.Qty2=T1.Qty2-T2.Qty2, T1.Qty3=T1.Qty3-T2.Qty3, ");
        //    SQL.AppendLine("    T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime(); ");


        //    //SQL.AppendLine("Update TblStockSummary As T1  ");
        //    //SQL.AppendLine("Inner Join ( ");
        //    //SQL.AppendLine("    Select T.WhsCode, T.Source, T.Lot, T.Bin, ");
        //    //SQL.AppendLine("    Sum(T.Qty) As Qty, Sum(T.Qty2) As Qty2, Sum(T.Qty3) As Qty3 ");
        //    //SQL.AppendLine("    From TblStockMovement T ");
        //    //SQL.AppendLine("    Where (T.DocType=@DocType And T.DocNo=@DocNo) ");
        //    //SQL.AppendLine("    Group By T.WhsCode, T.Source, T.Lot, T.Bin ");
        //    //SQL.AppendLine("    ) T2 ");
        //    //SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
        //    //SQL.AppendLine("    And T1.Source=T2.Source ");
        //    //SQL.AppendLine("    And T1.Lot=T2.Lot ");
        //    //SQL.AppendLine("    And T1.Bin=T2.Bin ");
        //    //SQL.AppendLine("Set ");
        //    //SQL.AppendLine("    T1.Qty=T1.Qty+T2.Qty, T1.Qty2=T1.Qty2+T2.Qty2, T1.Qty3=T1.Qty3+T2.Qty3, ");
        //    //SQL.AppendLine("    T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime(); ");

        //    //SQL.AppendLine("Update TblStockSummary As T1  ");
        //    //SQL.AppendLine("Inner Join ( ");
        //    //SQL.AppendLine("    Select T.WhsCode, T.ItCode, T.BatchNo, T.Source, T.Lot, T.Bin, ");
        //    //SQL.AppendLine("    Sum(T.Qty) As Qty, Sum(T.Qty2) As Qty2, Sum(T.Qty3) As Qty3 ");
        //    //SQL.AppendLine("    From TblStockMovement T ");
        //    //SQL.AppendLine("    Where T.DocType=@DocType2 ");
        //    //SQL.AppendLine("    And T.DocNo=@DocNo2 ");
        //    //SQL.AppendLine("    And T.Bin In (Select Bin from TblStockMovement Where DocType=@DocType And DocNo=@DocNo) ");
        //    //SQL.AppendLine("    Group By T.WhsCode, T.ItCode, T.BatchNo, T.Source, T.Lot, T.Bin ");
        //    //SQL.AppendLine("    ) T2 ");
        //    //SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
        //    //SQL.AppendLine("    And T1.ItCode=T2.ItCode ");
        //    //SQL.AppendLine("    And T1.BatchNo=T2.BatchNo ");
        //    //SQL.AppendLine("    And T1.Source=T2.Source ");
        //    //SQL.AppendLine("    And T1.Lot=T2.Lot ");
        //    //SQL.AppendLine("    And T1.Bin=T2.Bin ");
        //    //SQL.AppendLine("Set ");
        //    //SQL.AppendLine("    T1.Qty=T1.Qty+T2.Qty, T1.Qty2=T1.Qty2+T2.Qty2, T1.Qty3=T1.Qty3+T2.Qty3, ");
        //    //SQL.AppendLine("    T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime(); ");


        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DocNo2", TxtBinTransferRequestDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@DocType", mDocType);
        //    Sm.CmParam<String>(ref cm, "@DocType2", mDocType2);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveStock1(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, D.Source, 'N', '', ");
            SQL.AppendLine("A.DocDt, C.WhsCode2, D.Lot, D.Bin, D.ItCode, D.BatchNo, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblBinTransferHdr A ");
            SQL.AppendLine("Inner Join TblBinTransferDtl B On A.DocNo=B.DocNo And (B.Qty<>0 Or B.Qty2<>0 Or B.Qty3<>0) ");
            SQL.AppendLine("Inner Join TblBinTransferRequestHdr C On A.BinTransferRequestDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblBinTransferRequestDtl2 D On A.BinTransferRequestDocNo=D.DocNo And B.BinTransferRequestDNo=D.DNo And D.CancelInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType2, A.BinTransferRequestDocNo, B.BinTransferRequestDNo, D.Source, 'N', '', ");
            SQL.AppendLine("A.DocDt, C.WhsCode, D.Lot, D.Bin, D.ItCode, D.BatchNo, ");
            SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblBinTransferHdr A ");
            SQL.AppendLine("Inner Join TblBinTransferDtl B On A.DocNo=B.DocNo And (B.Qty<>0 Or B.Qty2<>0 Or B.Qty3<>0) ");
            SQL.AppendLine("Inner Join TblBinTransferRequestHdr C On A.BinTransferRequestDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblBinTransferRequestDtl2 D On A.BinTransferRequestDocNo=D.DocNo And B.BinTransferRequestDNo=D.DNo And D.CancelInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select C.WhsCode2, D.Lot, D.Bin, D.ItCode, D.BatchNo, D.Source, 0, 0, 0, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblBinTransferHdr A ");
            SQL.AppendLine("Inner Join TblBinTransferDtl B On A.DocNo=B.DocNo And (B.Qty<>0 Or B.Qty2<>0 Or B.Qty3<>0) ");
            SQL.AppendLine("Inner Join TblBinTransferRequestHdr C On A.BinTransferRequestDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblBinTransferRequestDtl2 D On A.BinTransferRequestDocNo=D.DocNo And B.BinTransferRequestDNo=D.DNo And D.CancelInd='N' ");
            SQL.AppendLine("Left Join TblStockSummary E ");
            SQL.AppendLine("    On C.WhsCode2=E.WhsCode ");
            SQL.AppendLine("    And D.Lot=E.Lot ");
            SQL.AppendLine("    And D.Bin=E.Bin ");
            //SQL.AppendLine("    And D.ItCode=E.ItCode ");
            //SQL.AppendLine("    And D.BatchNo=E.BatchNo ");
            SQL.AppendLine("    And D.Source=E.Source ");
            SQL.AppendLine("Where A.DocNo=@DocNo And E.WhsCode Is Null; ");
 
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocNo2", TxtBinTransferRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@DocType2", mDocType2);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock2(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WHsCode=@WhsCode And Lot=@Lot And Bin=@Bin ");
            SQL.AppendLine("And ItCode=@ItCode And BatchNo=@BatchNo And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock3(int Row)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select @WhsCode, @Lot, @Bin, @ItCode, @BatchNo, @Source, @Qty, @Qty2, @Qty3, @Remark, @UserCode, CurrentDateTime() ");
            //SQL.AppendLine("On Duplicate Key ");
            //SQL.AppendLine("Update ");
            //SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
            //SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WHsCode=@WhsCode And Lot=@Lot And Bin=@Bin ");
            SQL.AppendLine("And ItCode=@ItCode And BatchNo=@BatchNo And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode2);
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand UpdateBin()
        //{
        //    var SQL = new StringBuilder();

        //    var cm = new MySqlCommand()
        //    {
        //        CommandText = "Select A.DocNo, B.Bin From tblbintransferrequestHdr A " +
        //                      "Inner Join TblBinTransferrequestDtl B On A.DocNo = B.DocNo And B.CancelInd = 'N' " +
        //                      "Where Concat(A.DocNo, B.Bin) not In ( Select Distinct Concat(A.BinTransferRequestDocNo,D.Bin) As Keyword " +
        //                      "From TblBintransferHdr A " +
        //                      "Inner Join TblBinTransferDtl B ON A.DocNo = B.DocNo " +
        //                      "Inner Join TblBinTransferRequestHdr C On A.BinTransferRequestDocNo = C.DocNo " +
        //                      "Inner Join TblBinTransferRequestDtl2 D On A.BinTransferRequestDocNo = D.DOcNo And B.BinTransferRequestDNo = D.Dno) " +
        //                      "And B.CancelInd = 'N' And A.DocNo = @BTR And Locate(Concat('##', B.Bin, '##'), @SelectedBin)<1 "
        //    };
        //    Sm.CmParam<String>(ref cm, "@BTR", TxtBinTransferRequestDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@SelectedBin", GetSelectedBin());

        //    if (Sm.IsDataExist(cm)) 
        //    { }
        //    else
        //    {
        //        SQL.AppendLine("Update TblBinTransferRequestHdr Set ");
        //        SQL.AppendLine("    ProcessInd='F', LastUpBy=@UserCode, LastUpDt=CurrentDateTime()  ");
        //        SQL.AppendLine("Where DocNo=@DocNo2; ");

        //        var cm2 = new MySqlCommand() { CommandText = SQL.ToString() };

        //        Sm.CmParam<String>(ref cm2, "@DocNo2", TxtBinTransferRequestDocNo.Text);
        //        Sm.CmParam<String>(ref cm2, "@UserCode", Gv.CurrentUserCode);
        //        Sm.ExecCommand(cm2);
        //    }
        //    return cm; 
        //}

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowBinTransferHdr(DocNo);
                ShowBinTransferDtl(DocNo);
                ShowBinInfo();
                if (Sm.GetGrdStr(Grd2, Grd2.Rows.Count - 1, 1).Length != 0)
                {
                    Grd2.Rows.Add();
                    Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 2, 4, 6 });
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBinTransferHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.BinTransferRequestDocNo, ");
            SQL.AppendLine("B.WhsCode2 As WhsCode, B.WhsCode As WhsCode2, C.WhsName, D.WhsName As WhsName2,  A.Remark ");
            SQL.AppendLine("From TblBinTransferHdr A ");
            SQL.AppendLine("Inner Join TblBinTransferRequestHdr B On A.BinTransferRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On B.WhsCode2=C.WhsCode ");
            SQL.AppendLine("Inner Join TblWarehouse D On B.WhsCode=D.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "BinTransferRequestDocNo", "WhsCode", "WhsCode2", "WhsName", 
                        //6-7
                        "WhsName2", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtBinTransferRequestDocNo.Text = Sm.DrStr(dr, c[2]);
                        mWhsCode = Sm.DrStr(dr, c[3]);
                        mWhsCode2 = Sm.DrStr(dr, c[4]);
                        TxtWhsCode.Text = Sm.DrStr(dr, c[5]);
                        TxtWhsCode2.Text = Sm.DrStr(dr, c[6]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
        }

        private void ShowBinTransferDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.BinTransferRequestDNo, ");
            SQL.AppendLine("C.Bin, C.ItCode, D.ItCodeInternal, D.ItName, C.BatchNo, C.Source, C.Lot, ");
            SQL.AppendLine("C.Qty As BinTransferRequestQty, B.Qty, C.Qty2 As BinTransferRequestQty2, B.Qty2, C.Qty3 As BinTransferRequestQty3, B.Qty3, ");
            SQL.AppendLine("D.InventoryUomCode, D.InventoryUomCode2, D.InventoryUomCode3, B.Remark ");
            SQL.AppendLine("From TblBinTransferHdr A ");
            SQL.AppendLine("Inner Join TblBinTransferDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblBinTransferRequestDtl2 C On A.BinTransferRequestDocNo=C.DocNo And B.BinTransferRequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By C.Bin, B.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "BinTransferRequestDNo", "Bin", "ItCode", "itCodeInternal", "ItName", 
                        
                        //6-10
                        "BatchNo", "Source", "Lot", "BinTransferRequestQty", "Qty",  
                        
                        //11-15
                        "InventoryUomCode", "BinTransferRequestQty2", "Qty2", "InventoryUomCode2", "BinTransferRequestQty3", 
                        
                        //16-18
                        "Qty3", "InventoryUomCode3", "Remark"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                    Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 10, 14, 18 });
                    InsertSummary(Sm.GetGrdStr(Grd, Row, 2));
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 11, 12, 14, 15, 16, 18, 19, 20 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnBinTransferRequestDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBinTransferDlg(this));
        }

        private void BtnBinTransferRequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtBinTransferRequestDocNo, "Bin transfer request document", false))
            {
                var f = new FrmBinTransferRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtBinTransferRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid Event

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (BtnSave.Enabled && 
                e.ColIndex == 0 && 
                !Sm.IsTxtEmpty(TxtBinTransferRequestDocNo, "Requested document#", false) && 
                e.KeyChar == Char.Parse(" "))
                Sm.FormShowDialog(new FrmBinTransferDlg2(this, TxtBinTransferRequestDocNo.Text));
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && 
                e.ColIndex == 0 && 
                !Sm.IsTxtEmpty(TxtBinTransferRequestDocNo, "Requested document#", false))
                Sm.FormShowDialog(new FrmBinTransferDlg2(this, TxtBinTransferRequestDocNo.Text));
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                if (Grd2.SelectedRows.Count > 0)
                {
                    if (Grd2.Rows[Grd2.Rows[Grd2.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd2.SelectedRows.Count - 1; Index >= 0; Index--)
                            {
                                int Row = Grd2.SelectedRows[Index].Index;
                                for (int Index2 = Grd1.Rows.Count - 1; Index2 >= 0; Index2--)
                                {
                                    if (Sm.CompareStr(
                                            Sm.GetGrdStr(Grd1, Index2, 2),
                                            Sm.GetGrdStr(Grd2, Row, 1)
                                            ))
                                        Grd1.Rows.RemoveAt(Index2);
                                }
                                if (Grd1.Rows.Count <= 0) Grd1.Rows.Add();
                                Grd2.Rows.RemoveAt(Row);
                            }
                            if (Grd2.Rows.Count <= 0) Grd2.Rows.Add();
                            ShowBinInfo();
                        }
                    }
                }
            }
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion
    }
}
