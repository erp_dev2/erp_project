﻿#region Update
/*
    12/04/2018 [HAR] validasi entity berdasarkan entity di group user
    20/07/2020 [TKG/IMS] merubah tampilan description spy kalau bank account# di bank account kosong, datanya tetap tampil.
    16/06/2022 [TKG/GSS] menambah data voucher switching bank account dengan account type 2
    08/07/2022 [HAR/GSS] bug saat ambil closing balance, saat januari harusnya ambil nilai balance bulan sebelumnya (tahun seblmnya)
    14/12/2022 [DITA/ALL] bug ketika menampilan data voucher tipe switching bank account di cash in cash out belum balance
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using CB = RunSystem.FrmRptCashBook;

#endregion

namespace RunSystem
{
    public partial class FrmRptCashFlow : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private bool mIsReportingFilterByEntity = false;


        #endregion

        #region Constructor

        public FrmRptCashFlow(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                DteDocDt1.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteDocDt2.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                GetParameter();
                SetGrd();

                string CurrentDateTime = Sm.ServerCurrentDateTime();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code",
                        "Account",
                        "Description", 
                        "Debet / Credit",
                        "Debet Credit to",

                        //6
                        "Amount",
                        " "
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 80, 400, 150, 150, 
                        //6
                        150, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7});
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2 });
        }

        private void GetParameter()
        {
            mIsReportingFilterByEntity = Sm.GetParameterBoo("IsReportingFilterByEntity");
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Sm.GetDte(DteDocDt1).Length == 0 || Sm.GetDte(DteDocDt2).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Filter date not valid");
            }
            else
            {
                if (Sm.GetDte(DteDocDt1).Substring(0, 4) == Sm.GetDte(DteDocDt2).Substring(0, 4))
                {
                    ShowDataBank();
                }
                else
                {
                    Sm.StdMsg(mMsgType.Warning, "Filter Year is different");
                }
            }
        }


        private void ShowDataBank()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '0' as Code, 'BANK/KAS' As Account, ZZ.bankAcCode, ZZ.BankAcNm,  ((Sum(ZZ.Debet)-Sum(ZZ.Credit))+ Sum(ZZ.Opening))  As AMt ");
            SQL.AppendLine("From ( ");
	        SQL.AppendLine("    Select T.BankAcCode, ");
            SQL.AppendLine("    Case When T2.BankAcNo Is Null Then T2.BankAcNm Else Concat(T2.BankAcNm, '( ', T2.BankAcNo, ' )') End As BankAcNm, ");
            SQL.AppendLine("    T.DocNo, T.DNo, T.VcYear, T.VcDate, T.VcMonth, T.Description,  ");
	        SQL.AppendLine("    Round(T.Debet, 2) As Debet, Round(T.Credit, 2) Credit, Round(T.Opening, 2) Opening, if(@prev != T.BankAcCode, @bal:=((T.Opening+T.Debet)-T.Credit), @bal:= @bal+(Debet-Credit)) As Balanced,  @prev:=T.BankAcCode, T1.Username ");
	        SQL.AppendLine("    From(  ");
	        SQL.AppendLine("          Select A.BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
	        SQL.AppendLine("          B.Description, Round(B.Amt, 2) As Credit, 0.00 As Debet, 0.00 As Opening, A.PIC, A.CreateDt  ");
	        SQL.AppendLine("          From TblVoucherHdr A  ");
	        SQL.AppendLine("          Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("          Where A.AcType = 'C' ");
            SQL.AppendLine("          And Left(A.DocDt, 4) = @Year And A.CancelInd='N'  ");
            SQL.AppendLine("          And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("And A.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EntCode From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=IfNull(A.EntCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }               
	        SQL.AppendLine("          UNION ALL ");
	        SQL.AppendLine("          Select A.BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
	        SQL.AppendLine("          B.Description, 0.00 As Credit, Round(B.Amt, 2) As Debet, 0.00, A.PIC, A.CreateDt ");
	        SQL.AppendLine("          From TblVoucherHdr A ");
	        SQL.AppendLine("          Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("          Where A.AcType = 'D' ");
            SQL.AppendLine("          And Left(A.DocDt, 4)=@Year And A.CancelInd='N' And ");
            SQL.AppendLine("          A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("And A.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=IfNull(A.EntCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }               
	        SQL.AppendLine("          Union All ");
            SQL.AppendLine("          Select A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
            SQL.AppendLine("          B.Description, Round(B.Amt, 2) As Credit, 0.00 As Debet, 0.00 As Opening, A.PIC, A.CreateDt  ");
            SQL.AppendLine("          From TblVoucherHdr A  ");
            SQL.AppendLine("          Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("          Where A.AcType2 Is Not Null ");
            SQL.AppendLine("          And A.AcType2 = 'C' ");
            SQL.AppendLine("          And Left(A.DocDt, 4) = @Year And A.CancelInd='N'  ");
            SQL.AppendLine("          And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("And A.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EntCode From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=IfNull(A.EntCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("          UNION ALL ");
            SQL.AppendLine("          Select A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
            SQL.AppendLine("          B.Description, 0.00 As Credit, Round(B.Amt, 2) As Debet, 0.00, A.PIC, A.CreateDt ");
            SQL.AppendLine("          From TblVoucherHdr A ");
            SQL.AppendLine("          Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("          Where A.AcType2 Is Not Null ");
            SQL.AppendLine("          And A.AcType2 = 'D' ");
            SQL.AppendLine("          And Left(A.DocDt, 4)=@Year ");
            SQL.AppendLine("          And A.CancelInd='N' ");
            SQL.AppendLine("          And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("And A.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=IfNull(A.EntCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("          Union All ");

            SQL.AppendLine("          Select B.BankAcCode, A.DocNo, '001', '01',  @MonthFilter As VcMonth,  @Year,  ");
	        SQL.AppendLine("          'Opening Balance', 0.00, 0.00, ifnull(B.Amt, 0.00) As Opening, A.Createby, concat(@year,@MonthFilter,'01','0002') ");
	        SQL.AppendLine("          From tblClosingBalanceIncashHdr A  ");
	        SQL.AppendLine("          Inner Join TblClosingBalanceIncashDtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("          where A.yr = @year and ");
            //   SQL.AppendLine("          A.Mth =  Cast(@MonthFilter As Decimal(10,2)) -1  ");
            SQL.AppendLine(" Where A.yr = (case When(@monthFilter = '01') Then @year-1 ELSE @year END) ");
            SQL.AppendLine("And A.Mth = (case When (@monthFilter='01') Then 12 ELSE @MonthFilter-1 END) ");

            SQL.AppendLine("          Order By BankAcCode, VcDate, VcMonth, CreateDt  ");
	        SQL.AppendLine("    )T ");
	        SQL.AppendLine("    Left Join TblUser T1 On T.Pic = T1.UserCode  ");
	        SQL.AppendLine("    Inner Join TblBankAccount T2 On T.BankAcCode=T2.BankAcCode And T2.HiddenInd='N'  ");
	        SQL.AppendLine("    Inner Join (  ");
	        SQL.AppendLine("           Select @bal := 0, @prev:=''  ");
	        SQL.AppendLine("    ) B On 1=1  ");
	        SQL.AppendLine("    )ZZ ");
            SQL.AppendLine("Group By BankAcCode, BankAcNm ");
            

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@Year", Sm.GetDte(DteDocDt1).Substring(0, 4));
                Sm.CmParam<String>(ref cm, "@MonthFilter", Sm.GetDte(DteDocDt1).Substring(4, 2));
              

                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    {
                        //0
                        "Code",
                        //1-5
                        "Account", "BankAcNm", "Amt"
                    });
                if (dr.HasRows)
                {
                    int Row = 0;
                    while (dr.Read())
                    {
                        Grd1.Rows.Add();
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 3);
                        Grd1.Cells[Row, 7].BackColor = Color.Transparent;
                        Grd1.Cells[Row, 7].ForeColor = Color.Transparent;
                        Row = Row + 1;
                    }
                 }
                dr.Close();
            }
            ShowDataAR();
        }

        private void ShowDataAR()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Code, ");
            SQL.AppendLine("'Cash In' As Account, ");
            SQL.AppendLine("concat(A.DocNo, ' ', ifnull(A.remark, ''), ' ',ifnull(B.Description, ''))As description, if(A.AcType='D', 'Debet', 'Credit') As AcType, ifnull(C.BankAcNm, '-') As BankAcNm, Round(B.Amt, 2) Amt ");
            SQL.AppendLine("From TblVoucherRequesthdr A ");
            SQL.AppendLine("Inner join TblVoucherRequestDtl B On A.Docno = B.DocNo ");
            SQL.AppendLine("Left Join TblbankAccount C On A.bankAcCode = C.BankAcCode ");
            SQL.AppendLine("Where A.AcType = 'D' And A.CancelInd = 'N' And A.Status = 'A' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("And A.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EntCode From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=IfNull(A.EntCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '1' As Code, ");
            SQL.AppendLine("'Cash In' As Account, ");
            SQL.AppendLine("concat(A.DocNo, ' ', ifnull(A.remark, ''), ' ',ifnull(B.Description, ''))As description, if(A.AcType2='D', 'Debet', 'Credit') As AcType, ifnull(C.BankAcNm, '-') As BankAcNm, Round(B.Amt, 2) Amt ");
            SQL.AppendLine("From TblVoucherRequesthdr A ");
            SQL.AppendLine("Inner join TblVoucherRequestDtl B On A.Docno = B.DocNo ");
            SQL.AppendLine("Left Join TblbankAccount C On A.bankAcCode2 = C.BankAcCode ");
            SQL.AppendLine("Where A.AcType2 = 'D' And A.CancelInd = 'N' And A.Status = 'A' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("And A.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EntCode From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=IfNull(A.EntCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2).Substring(0, 8));

                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    {
                        //0
                        "Code",
                        //1-5
                        "Account", "Description", "AcType", "BankAcNm", "Amt"
                    });
                if (dr.HasRows)
                {
                    int Row = Grd1.Rows.Count;
                    while (dr.Read())
                    {
                        Grd1.Rows.Add();
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 5);
                        Grd1.Cells[Row, 7].BackColor = Color.Transparent;
                        Grd1.Cells[Row, 7].ForeColor = Color.Transparent;

                        Row = Row + 1;
                    }
                }
                dr.Close();
            }
            ShowDataAP();
        }

        private void ShowDataAP()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '2' As Code, ");
            SQL.AppendLine("'Cash Out' As Account, ");
            SQL.AppendLine("concat(A.DocNo, ' ', ifnull(A.remark, ''), ' ',ifnull(B.Description, ''))As description, if(A.AcType='D', 'Debet', 'Credit') As AcType, ifnull(C.BankAcNm, '-') As BankAcNm, round(B.Amt, 2) Amt ");
            SQL.AppendLine("From TblVoucherRequesthdr A ");
            SQL.AppendLine("Inner join TblVoucherRequestDtl B On A.Docno = B.DocNo ");
            SQL.AppendLine("Left Join TblbankAccount C On A.bankAcCode = C.BankAcCode ");
            SQL.AppendLine("Where A.AcType = 'C' And A.CancelInd = 'N' And A.Status = 'A' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("And A.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EntCode From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=IfNull(A.EntCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Code, ");
            SQL.AppendLine("'Cash Out' As Account, ");
            SQL.AppendLine("concat(A.DocNo, ' ', ifnull(A.remark, ''), ' ',ifnull(B.Description, ''))As description, if(A.AcType2='D', 'Debet', 'Credit') As AcType, ifnull(C.BankAcNm, '-') As BankAcNm, round(B.Amt, 2) Amt ");
            SQL.AppendLine("From TblVoucherRequesthdr A ");
            SQL.AppendLine("Inner join TblVoucherRequestDtl B On A.Docno = B.DocNo ");
            SQL.AppendLine("Left Join TblbankAccount C On A.bankAcCode2 = C.BankAcCode ");
            SQL.AppendLine("Where A.AcType2 = 'C' And A.CancelInd = 'N' And A.Status = 'A' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("And A.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EntCode From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=IfNull(A.EntCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2).Substring(0, 8));

                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    {
                        //0
                        "Code",
                        //1-5
                        "Account", "Description", "AcType", "BankAcNm", "Amt"
                    });
                if (dr.HasRows)
                {
                    int Row = Grd1.Rows.Count;
                    while (dr.Read())
                    {
                        Grd1.Rows.Add();
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                        Grd1.Cells[Row, 7].Value = (-1 *dr.GetDecimal(c[5]));
                        Grd1.Cells[Row, 7].BackColor = Color.Transparent;
                        Grd1.Cells[Row, 7].ForeColor = Color.Transparent;
                        Row = Row + 1;
                    }
                }
                Grd1.GroupObject.Add(2);
                Grd1.Group();
                AdjustSubtotals();
                dr.Close();
                int MaxRow = Grd1.Rows.Count;
                if (MaxRow > 1)
                    Grd1.Cells[MaxRow-1, 3].Value = "Balance";
                Grd1.Rows.CollapseAll();
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion
    }
}
