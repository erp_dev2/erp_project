﻿#region Update
/*
    21/09/2018 [TKG] New application
    21/09/2020 [IBL/SRN] check all saat double click di kolom header
    10/01/2021 [VIN/SIER] check all Compute Balance
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLeaveSummaryDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmLeaveSummary mFrmParent;
        private string mSiteCode = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmLeaveSummaryDlg(FrmLeaveSummary FrmParent, string SiteCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mSiteCode = SiteCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueLeaveCode(ref LueLeaveCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Year",
                        "Leave Code", 
                        "Leave Name",
                        "Employee's"+Environment.NewLine+"Code",
                        
                        //6-10
                        "Old Code",
                        "Employee's Name",
                        "Department",
                        "Position",
                        "Join",
                        
                        //11-15
                        "Resign",
                        "Permanent",
                        "Start",
                        "End",
                        "Quota",
                        
                        //16-18
                        "Used",
                        "Unused",
                        "Balance"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 0, 200, 100, 
                        
                        //6-10
                        100, 200, 200, 200, 100,

                        //11-15
                        100, 100, 100, 100, 100,

                        //16-18
                        100, 100, 100
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
            Sm.GrdFormatDate(Grd1, new int[] { 10, 11, 12, 13, 14 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 15, 16, 17, 18 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 6, 9, 10, 11 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Yr, A.LeaveCode, B.LeaveName, ");
            SQL.AppendLine("A.EmpCode, C.EmpCodeOld, C.EmpName, D.DeptName, E.PosName, ");
            SQL.AppendLine("C.JoinDt, C.ResignDt,  A.InitialDt,  A.StartDt, A.EndDt, ");
            SQL.AppendLine("NoOfDays1, NoOfDays2, NoOfDays3, Balance ");
            SQL.AppendLine("From TblLeaveSummary A ");
            SQL.AppendLine("Inner Join TblLeave B On A.LeaveCode = B.LeaveCode ");
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode And C.SiteCode=@SiteCode ");
            SQL.AppendLine("Inner Join TblDepartment D On C.DeptCode=D.DeptCode ");
            SQL.AppendLine("Inner Join TblPosition E On C.PosCode=E.PosCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty, Yr = string.Empty, LeaveCode = string.Empty, EmpCode = string.Empty;

                var cm = new MySqlCommand();

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        Yr = Sm.GetGrdStr(mFrmParent.Grd1, r, 1);
                        LeaveCode = Sm.GetGrdStr(mFrmParent.Grd1, r, 2);
                        EmpCode = Sm.GetGrdStr(mFrmParent.Grd1, r, 4);
                        if (Yr.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " (A.Yr=@Yr0" + r.ToString() + " And A.LeaveCode=@LeaveCode0" + r.ToString() + " And A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@Yr0" + r.ToString(), Yr);
                            Sm.CmParam<String>(ref cm, "@LeaveCode0" + r.ToString(), LeaveCode);
                            Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                        }
                    }
                }
                if (Filter.Length > 0) Filter = " And Not ( " + Filter + ") ";

                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.FilterStr(ref Filter, ref cm, TxtYr.Text, new string[] { "A.Yr" });
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "C.EmpCodeOld", "C.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLeaveCode), "A.LeaveCode", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.Yr, B.LeaveName, C.EmpName;",
                        new string[] 
                        { 
                             //0
                             "Yr", 
                             
                             //1-5
                             "LeaveCode",  
                             "LeaveName",
                             "EmpCode",
                             "EmpCodeOld",
                             "EmpName",

                             //6-10
                             "DeptName",
                             "PosName",
                             "JoinDt",
                             "ResignDt",
                             "InitialDt",

                             //11-15
                             "StartDt",
                             "EndDt",
                             "NoOfDays1",
                             "NoOfDays2",
                             "NoOfDays3",

                             //16
                             "Balance"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 17);
                        //Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 18);
                        mFrmParent.Grd1.Cells[Row1, 17].Value = Sm.GetGrdDec(mFrmParent.Grd1, Row1, 15) - Sm.GetGrdDec(mFrmParent.Grd1, Row1, 16) - Sm.GetGrdDec(mFrmParent.Grd1, Row1, 17);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 14, 15, 16, 17 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 data.");
        }

        private bool IsDataAlreadyChosen(int r)
        {
            string 
                Yr = Sm.GetGrdStr(Grd1, r, 2),
                LeaveCode = Sm.GetGrdStr(Grd1, r, 3),
                EmpCode = Sm.GetGrdStr(Grd1, r, 5)
                ;
            for (int row = 0; row < mFrmParent.Grd1.Rows.Count - 1; row++)
                if (
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, row, 1), Yr) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, row, 2), LeaveCode) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, row, 4), EmpCode) 
                    ) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }
        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtYr_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkYr_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Year");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueLeaveCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLeaveCode, new Sm.RefreshLue1(Sl.SetLueLeaveCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkLeaveCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Leave");
        }

        #endregion

        #endregion
    }
}
