﻿#region Update
/*
    18/07/2022 [RDA/PRODUCT] new apps based on FrmrptAgingARInv
    27/01/2023 [TYO/MNET] merubah format overdue berdasarkan parameter OverdueFormat
    12/04/2023 [WED/MNET] SLI Project belum muncul
    02/05/2023 [BRI/MNET] Bug Filter Profit Center
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingARInv3 : RunSystem.FrmBase6
    {
        #region Field

        private List<String> mlProfitCenter = null;
        private string  mSQL = string.Empty, mDocTitle = string.Empty,
            mRptAgingARInvDlgFormat = string.Empty,
            mQueryProfitCenter = string.Empty,
            mOverdueFormat = string.Empty;
        private bool 
            mMInd = false, 
            mIsRptAgingARInvShowDOCt = false,
            mIsRptAgingInvoiceARShowFulfilledDocument = false,
            mIsFilterByCtCt = false,
            mIsFilterByDept = false,
            mIsFicoUseMultiProfitCenterFilter = false,
            mIsFilterByProfitCenter = false,
            mIsAllProfitCenterSelected = false;
        internal bool mIsUseMInd = false, mIsRptAgingARInvUseDocDtFilter = false, mIsRptShowVoucherLoop = false;
        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptAgingARInv3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                if (!mIsRptAgingARInvUseDocDtFilter)
                {
                    label2.Visible = DteDocumentDt1.Visible = label6.Visible = DteDocumentDt2.Visible = ChkDocumentDt.Visible = false;
                }
                if (!mIsFicoUseMultiProfitCenterFilter)
                {
                    LblMultiProfitCenterCode.Visible = false;
                    CcbProfitCenterCode.Visible = false;
                    ChkProfitCenterCode.Visible = false;
                }
                mlProfitCenter = new List<String>();
                SetGrd();
                Sl.SetLueCtCode(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                Sl.SetLueCtCtCode(ref LueCtCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                SetLueStatus();
                Sm.SetLue(LueStatus, "OP");
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                LblStatus.Visible = LueStatus.Visible = ChkStatus.Visible = mIsRptAgingInvoiceARShowFulfilledDocument;
                SetLueType(ref LueType);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(ref MySqlCommand cm)
        {
            var SQL = new StringBuilder();
            string Filter = " ", Filter2 = " ", Filter3 = " ", Filter4 = " ", Filter5 = " ", Filter6 = " ", Filter7 = " ";

            SetProfitCenter();

            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
            
            Filter2 = Filter;

            Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DueDt");
            Sm.FilterDt(ref Filter2, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
            Sm.FilterDt(ref Filter3, ref cm, Sm.GetDte(DteDocumentDt1), Sm.GetDte(DteDocumentDt2), "A.DocDt");
            Sm.FilterStr(ref Filter4, ref cm, Sm.GetLue(LueCtCtCode), "T2.CtCtCode", true);
            if (Sm.GetLue(LueStatus) == "OP")
                Filter5 += " And T1.Status In ('O', 'P') ";
            else
                Sm.FilterStr(ref Filter5, ref cm, Sm.GetLue(LueStatus), "T1.Status", true);

            Sm.CmParam<String>(ref cm, "@MInd", mMInd ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.FilterStr(ref Filter6, ref cm, Sm.GetLue(LueType), "T1.Type", true);

            if (DteInvDt1.Text.Length > 0 && DteInvDt2.Text.Length > 0)
                Filter7 += " And A.DocDt Between '"+ Sm.Left(Sm.GetDte(DteInvDt1), 8) +"' And '"+ Sm.Left(Sm.GetDte(DteInvDt2), 8) + "' ";

            if (DteBalanceDt.Text.Length > 0) Sm.CmParam<String>(ref cm, "@BalanceDt", Sm.Left(Sm.GetDte(DteBalanceDt), 8));

            //if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());

            if (ChkProfitCenterCode.Checked)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter_ = string.Empty;
                    mQueryProfitCenter = null;
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter_.Length > 0) Filter_ += " Or ";
                        Filter_ += " (K2.ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter_.Length == 0)
                    {
                        mQueryProfitCenter += "    And 1=0 ";
                    }
                    else
                    {
                        mQueryProfitCenter += "    And (" + Filter_ + ") ";
                    }
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        mQueryProfitCenter += "    And Find_In_Set(K.ProfitCenterCode, @ProfitCenterCode2) ";
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        mQueryProfitCenter += "    And K.ProfitCenterCode In ( ";
                        mQueryProfitCenter += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                        mQueryProfitCenter += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter += "    ) ";
                    }
                }
            }

            SQL.AppendLine("Select T1.Type, T1.Period, T1.DueDt, T1.AgingDays, T1.CtCode, T1.DocNo, T1.DocDt, T2.CtName, T4.CtCtName, T1.CurCode, T1.LocalDocNo, T1.TaxInvDocument, ");
            SQL.AppendLine("T1.TotalAmt, T1.TotalTax, T1.Downpayment, T1.Amt, T1.PaidAmt, T1.VoucherDocNo,T1.Balance, ");
            SQL.AppendLine("T1.DueDt, T1.AgingDays, ");
            SQL.AppendLine("Case T1.Status When 'O' Then 'Outstanding' When 'F' Then 'Fulfilled' When 'P' Then 'Partial' End As StatusDesc, ");
            if (mOverdueFormat == "1")
            {
                SQL.AppendLine("If (AgingDays < 0, Balance, 0) As AgingCurrent, ");
                SQL.AppendLine("If (AgingDays > 0 And AgingDays < 31, Balance, 0) As Aging1To30, ");
                SQL.AppendLine("If (AgingDays > 30 And AgingDays < 61, Balance, 0) As Aging31To60, ");
                SQL.AppendLine("If (AgingDays > 60 And AgingDays < 91, Balance, 0) As Aging61To90, ");
                SQL.AppendLine("If (AgingDays > 90 And AgingDays < 121, Balance, 0) As Aging91To120, ");
                SQL.AppendLine("If (AgingDays > 120, Balance, 0) As AgingOver120,  ");
                SQL.AppendLine("Null As AgingOver720, ");
            }

            else
            {
                SQL.AppendLine("If (AgingDays < 0, Balance, 0) As AgingCurrent, ");
                SQL.AppendLine("If (AgingDays > 0 And AgingDays < 31, Balance, 0) As Aging1To30, ");
                SQL.AppendLine("If (AgingDays > 30 And AgingDays < 91, Balance, 0) As Aging31To90, ");
                SQL.AppendLine("If (AgingDays > 90 And AgingDays < 181, Balance, 0) As Aging91To180, ");
                SQL.AppendLine("If (AgingDays > 181 And AgingDays < 361, Balance, 0) As Aging181To360, ");
                SQL.AppendLine("If (AgingDays > 360 And AgingDays < 721, Balance, 0) As Aging361To720, ");
                SQL.AppendLine("If (AgingDays > 720, Balance, 0) As AgingOver720,  ");
            }
            SQL.AppendLine("T1.Remark, T1.WhsName, T1.EntName, T1.DOCtDocNo, T1.taxCode1, ifnull(T1.taxAmt1, 0) As TaxAmt1, T1.taxCode2, ifnull(T1.taxAmt2, 0)  As TaxAmt2, T1.taxCode3, ifnull(T1.taxAmt3, 0) As TaxAmt3, ");
            if (mIsRptAgingARInvShowDOCt)
                SQL.AppendLine("T3.DOCtDocNo, ");
            else
                SQL.AppendLine("Null As DOCtDocNo, ");
            SQL.AppendLine(" T5.DeptName As DeptName ");
            SQL.AppendLine("From ( ");

            #region Sales Invoice

            SQL.AppendLine("    Select '1' AS Type, Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, A.DueDt, ");
            if (DteBalanceDt.Text.Length > 0)
                SQL.AppendLine("     DateDiff(@BalanceDt, A.DueDt) As AgingDays,  ");
            else
                SQL.AppendLine("     DateDiff(Left(currentdatetime(), 8), A.DueDt) As AgingDays,  ");
            SQL.AppendLine("    A.CtCode, A.DocNo, A.Docdt, A.LocalDocNo, A.TaxInvDocument, A.CurCode,  ");
            SQL.AppendLine("    A.TotalAmt, A.TotalTax, A.Downpayment, A.Amt, IfNull(B.PaidAmt, 0)+IfNull(C.ARSAmt, 0) As PaidAmt, B.VoucherDocNo,  ");
            SQL.AppendLine("    A.Amt-IfNull(B.PaidAmt, 0)-IfNull(C.ARSAmt, 0) As Balance, A.Remark, ");
            SQL.AppendLine("    g.WhsName, G.EntName, G.DOCtDocNo, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("        When A.Amt-IfNull(B.PaidAmt, 0)-IfNull(C.ARSAmt, 0) = A.Amt then 'O' ");
            SQL.AppendLine("        When A.Amt-IfNull(B.PaidAmt, 0)-IfNull(C.ARSAmt, 0) = 0 then 'F' ");
            SQL.AppendLine("        else 'P' ");
            SQL.AppendLine("    End Status, ");
		    SQL.AppendLine("    A.TaxCode1, ((D.Taxrate/100) * A.TotalAmt) taxAmt1, A.TaxCode2, ((E.Taxrate/100) * A.TotalAmt) taxAmt2, A.TaxCode3 , ((F.Taxrate/100) * A.TotalAmt) As taxAmt3 ");
            SQL.AppendLine("    From TblSalesInvoiceHdr A  ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select B1.DocNo, Sum(B2.Amt) As PaidAmt, Group_Concat(IfNull(B5.DocNo, '')) VoucherDocNo  ");
            SQL.AppendLine("        From TblSalesInvoiceHdr B1  ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl B2 On B1.DocNo=B2.InvoiceDocNo And B2.InvoiceType='1'  ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentHdr B3 On B2.DocNo=B3.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherRequestHdr B4 On B3.VoucherRequestDocNo=B4.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherHdr B5 On B4.VoucherDocNo=B5.DocNo And B5.CancelInd='N'  ");
            SQL.AppendLine("        Where B1.CancelInd='N' " + Filter.Replace("A.", "B1."));
            if (DteBalanceDt.Text.Length > 0) SQL.AppendLine("        And B5.DocDt <= @BalanceDt ");
            SQL.AppendLine("        Group By B1.DocNo  ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select T1.DocNo, Sum(T2.Amt) As ARSAmt  ");
            SQL.AppendLine("        From TblSalesInvoiceHdr T1  ");
            SQL.AppendLine("        Inner Join TblARSHdr T2 On T1.DocNo=T2.SalesInvoiceDocNo And T2.CancelInd='N'  ");
            SQL.AppendLine("        Where T1.CancelInd='N' " + Filter.Replace("A.", "T1."));
            if (DteBalanceDt.Text.Length > 0) SQL.AppendLine("        And T2.DocDt <= @BalanceDt ");
            SQL.AppendLine("        Group By T1.DocNo  ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo  ");
            SQL.AppendLine("    Left Join Tbltax D On A.TaxCode1 = D.TaxCode ");
            SQL.AppendLine("    Left Join Tbltax E On A.TaxCode2 = E.TaxCode ");
            SQL.AppendLine("    Left Join Tbltax F On A.TaxCode3 = F.TaxCode ");
            SQL.AppendLine("    Left Join  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("	        Select A.DocNo, ");
            SQL.AppendLine("            Group_Concat(Distinct D.WhsName Separator ', ') As WhsName, ");
            SQL.AppendLine("            Group_Concat(Distinct G.EntName Separator ', ') As EntName, ");
            SQL.AppendLine("            Group_Concat(Distinct C.DocNo Separator ', ') As DOCtDocNo ");
            SQL.AppendLine("	        From tblSalesInvoiceHdr A ");
            SQL.AppendLine("	        Inner Join TblsalesInvoiceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("	        Inner Join TblDOCthdr C On B.DOCtDocNo = C.DocNo ");
            SQL.AppendLine("	        Inner Join TblWarehouse D On C.WhSCode = D.WhsCode ");
            SQL.AppendLine("	        Left Join TblCostcenter E On D.CCCode = E.CCCode ");
            SQL.AppendLine("	        left Join TblProfitcenter F On E.ProfitCenterCode = F.ProfitcenterCode ");
            SQL.AppendLine("	        left Join TblEntity G On F.EntCode = G.EntCode ");
            SQL.AppendLine("	        Group By A.DocNo ");
            SQL.AppendLine("    )G On A.DocNo = G.DocNo ");
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                SQL.AppendLine("    Inner Join TblsalesInvoiceDtl H On A.DocNo = H.DocNo ");
                SQL.AppendLine("    Inner Join TblDOCthdr I On H.DOCtDocNo = I.DocNo  ");
                SQL.AppendLine("    Inner Join TblWarehouse J ON I.WhSCode = J.WhsCode ");
                SQL.AppendLine("    Left Join TblCostcenter K2 On J.CCCode = K2.CCCode ");
                SQL.AppendLine("    Left Join TblProfitcenter L On K2.ProfitCenterCode = L.ProfitcenterCode ");
            }
            if (!mIsRptAgingARInvUseDocDtFilter)
                SQL.AppendLine("    Where A.CancelInd='N'  ");
            else
                SQL.AppendLine("    Where A.CancelInd='N' " + Filter3);

            if (mIsUseMInd)
            {
                if (!mMInd)
                    SQL.AppendLine("        And A.MInd='N' ");
            }
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                //SQL.AppendLine("AND FIND_IN_SET(K.ProfitCenterCode, @ProfitCenterCode) ");                
                SQL.AppendLine(mQueryProfitCenter);
            }
            SQL.AppendLine(Filter);
            SQL.AppendLine(Filter7);
            if (DteBalanceDt.Text.Length > 0) SQL.AppendLine("        And A.DocDt <= @BalanceDt ");

            #endregion

            #region Sales Return Invoice

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select '2' AS Type, Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period,  ");
			SQL.AppendLine("        A.DocDt As DueDt,  ");
            if (DteBalanceDt.Text.Length > 0)
                SQL.AppendLine("        DateDiff(@BalanceDt, A.DocDt) As AgingDays,  ");
            else
                SQL.AppendLine("        DateDiff(Left(currentdatetime(), 8), A.DocDt) As AgingDays,  ");
            SQL.AppendLine("        A.CtCode, A.DocNo, A.DocDt,  A.LocalDocNo, A.TaxInvDocument, A.CurCode,  ");
			SQL.AppendLine("        -1*A.TotalAmt As TotalAmt,  ");
			SQL.AppendLine("        0 As TotalTax,  ");
			SQL.AppendLine("        0 As Downpayment,  ");
			SQL.AppendLine("        -1*A.TotalAmt As Amt,  ");
			SQL.AppendLine("        IfNull(B.PaidAmt, 0) As PaidAmt, B.VoucherDocNo,   ");
			SQL.AppendLine("        (-1*A.TotalAmt)+IfNull(B.PaidAmt, 0) As Balance, A.Remark, ");
            SQL.AppendLine("        C.WhsName, C.EntName, C.DOCtDocNo, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("        When (-1*A.TotalAmt)+IfNull(B.PaidAmt, 0) = (-1*A.TotalAmt) then 'O' ");
            SQL.AppendLine("        When (-1*A.TotalAmt)+IfNull(B.PaidAmt, 0) = 0 then 'F' ");
            SQL.AppendLine("        else 'P' ");
            SQL.AppendLine("    End Status, ");
			SQL.AppendLine("        null, 0 taxAmt1, null, 0 taxAmt2, null, 0 As taxAmt3 ");
			SQL.AppendLine("        From TblSalesReturnInvoiceHdr A  ");
			SQL.AppendLine("        Left Join (  ");
            SQL.AppendLine("            Select B1.DocNo, Sum(B2.Amt) As PaidAmt, Group_Concat(IfNull(B5.DocNo, '')) VoucherDocNo  ");
			SQL.AppendLine("            From TblSalesReturnInvoiceHdr B1  ");
			SQL.AppendLine("            Inner Join TblIncomingPaymentDtl B2 On B1.DocNo=B2.InvoiceDocNo And B2.InvoiceType='2'  ");
			SQL.AppendLine("            Inner Join TblIncomingPaymentHdr B3 On B2.DocNo=B3.DocNo  ");
			SQL.AppendLine("            Inner Join TblVoucherRequestHdr B4 On B3.VoucherRequestDocNo=B4.DocNo  ");
			SQL.AppendLine("            Inner Join TblVoucherHdr B5 On B4.VoucherDocNo=B5.DocNo And B5.CancelInd='N'  ");
            SQL.AppendLine("            Where B1.CancelInd='N' " + Filter2.Replace("A.", "B1."));
            if (DteBalanceDt.Text.Length > 0) SQL.AppendLine("        And B5.DocDt <= @BalanceDt ");
            SQL.AppendLine("            Group By B1.DocNo  ");
			SQL.AppendLine("        ) B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("	        Select A.DocNo, ");
            SQL.AppendLine("            Group_Concat(Distinct D.WhsName Separator ', ') As WhsName, ");
            SQL.AppendLine("            Group_Concat(Distinct G.EntName Separator ', ') As EntName, ");
            SQL.AppendLine("            Group_Concat(Distinct C.DocNo Separator ', ') As DOCtDocNo ");
            SQL.AppendLine("	        From tblSalesReturnInvoiceDtl A  ");
            SQL.AppendLine("	        Inner Join tblRecvCtHdr B On A.RecvCtDocno = B.DocNo ");
            SQL.AppendLine("	        Inner Join TblDOCthdr C On B.DOCtDocNo = C.DocNo ");
            SQL.AppendLine("	        Inner Join TblWarehouse D On C.WhSCode = D.WhsCode ");
            SQL.AppendLine("	        Left Join TblCostcenter E On D.CCCode = E.CCCode ");
            SQL.AppendLine("	        Left Join TblProfitcenter F On E.ProfitCenterCode = F.ProfitcenterCode ");
            SQL.AppendLine("	        Left Join TblEntity G On F.EntCode = G.EntCode ");
            SQL.AppendLine("	        Group By A.DocNo ");
            SQL.AppendLine("        )C On A.DocNo = C.Docno ");
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                SQL.AppendLine("    Inner Join TblsalesInvoiceDtl H On A.DocNo = H.DocNo ");
                SQL.AppendLine("    Inner Join TblDOCthdr I On H.DOCtDocNo = I.DocNo  ");
                SQL.AppendLine("    Inner Join TblWarehouse J ON I.WhSCode = J.WhsCode ");
                SQL.AppendLine("    Left Join TblCostcenter K2 On J.CCCode = K2.CCCode ");
                SQL.AppendLine("    Left Join TblProfitcenter L On K2.ProfitCenterCode = L.ProfitcenterCode ");
            }
            SQL.AppendLine("        Where A.CancelInd='N'  ");
            if (DteBalanceDt.Text.Length > 0) SQL.AppendLine("        And A.DocDt <= @BalanceDt ");
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                //SQL.AppendLine("AND FIND_IN_SET(K.ProfitCenterCode, @ProfitCenterCode) ");
                SQL.AppendLine(mQueryProfitCenter);
            }
            SQL.AppendLine(Filter2);
            SQL.AppendLine(Filter7);

            #endregion

            #region Sales Invoice for Project

            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select '3' AS Type, Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, A.DueDt, ");
            if (DteBalanceDt.Text.Length > 0)
                SQL.AppendLine("     DateDiff(@BalanceDt, A.DueDt) As AgingDays,  ");
            else
                SQL.AppendLine("     DateDiff(Left(currentdatetime(), 8), A.DueDt) As AgingDays,  ");
            SQL.AppendLine("    A.CtCode, A.DocNo, A.Docdt, A.LocalDocNo, A.TaxInvoiceNo As TaxInvDocument, A.CurCode, ");
            SQL.AppendLine("    A.TotalAmt, A.TotalTax, A.Downpayment, A.Amt, IfNull(B.PaidAmt, 0)+IfNull(C.ARSAmt, 0) As PaidAmt, B.VoucherDocNo,  ");
            SQL.AppendLine("    A.Amt-IfNull(B.PaidAmt, 0)-IfNull(C.ARSAmt, 0) As Balance, A.Remark, ");
            SQL.AppendLine("    G.WhsName, G.EntName, G.DOCtDocNo, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("        When A.Amt-IfNull(B.PaidAmt, 0)-IfNull(C.ARSAmt, 0) = A.Amt then 'O' ");
            SQL.AppendLine("        When A.Amt-IfNull(B.PaidAmt, 0)-IfNull(C.ARSAmt, 0) = 0 then 'F' ");
            SQL.AppendLine("        else 'P' ");
            SQL.AppendLine("    End Status, ");
            SQL.AppendLine("    A.TaxCode1, ((D.Taxrate/100) * A.TotalAmt) taxAmt1, A.TaxCode2, ((E.Taxrate/100) * A.TotalAmt) taxAmt2, A.TaxCode3 , ((F.Taxrate/100) * A.TotalAmt) As taxAmt3 ");
            SQL.AppendLine("    From TblSalesInvoice5Hdr A  ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select B1.DocNo, Sum(B2.Amt) As PaidAmt, Group_Concat(IfNull(B5.DocNo, '')) VoucherDocNo  ");
            SQL.AppendLine("        From TblSalesInvoice5Hdr B1  ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl B2 On B1.DocNo=B2.InvoiceDocNo And B2.InvoiceType='1'  ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentHdr B3 On B2.DocNo=B3.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherRequestHdr B4 On B3.VoucherRequestDocNo=B4.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherHdr B5 On B4.VoucherDocNo=B5.DocNo And B5.CancelInd='N'  ");
            SQL.AppendLine("        Where B1.CancelInd='N' " + Filter.Replace("A.", "B1."));
            if (DteBalanceDt.Text.Length > 0) SQL.AppendLine("        And B5.DocDt <= @BalanceDt ");
            SQL.AppendLine("        Group By B1.DocNo  ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select T1.DocNo, Sum(T2.Amt) As ARSAmt  ");
            SQL.AppendLine("        From TblSalesInvoice5Hdr T1  ");
            SQL.AppendLine("        Inner Join TblARSHdr T2 On T1.DocNo=T2.SalesInvoiceDocNo And T2.CancelInd='N'  ");
            SQL.AppendLine("        Where T1.CancelInd='N' " + Filter.Replace("A.", "T1."));
            if (DteBalanceDt.Text.Length > 0) SQL.AppendLine("        And T2.DocDt <= @BalanceDt ");
            SQL.AppendLine("        Group By T1.DocNo  ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo  ");
            SQL.AppendLine("    Left Join Tbltax D On A.TaxCode1 = D.TaxCode ");
            SQL.AppendLine("    Left Join Tbltax E On A.TaxCode2 = E.TaxCode ");
            SQL.AppendLine("    Left Join Tbltax F On A.TaxCode3 = F.TaxCode ");
            SQL.AppendLine("    Left Join  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.DocNo, Null As WhsName, Group_Concat(Distinct IfNull(K.EntName, '') Separator ', ') EntName, Group_Concat(Distinct B.ProjectImplementationDocNo Separator ', ') DOCtDocNo ");
            SQL.AppendLine("        From TblSalesInvoice5Hdr A ");
            SQL.AppendLine("        Inner Join TblSalesInvoice5Dtl B On A.DocNo = B.DocNo And A.CancelInd = 'N' ");
            SQL.AppendLine("        Inner Join TblProjectImplementationHdr C On B.ProjectImplementationDocNo = C.DocNo ");
            SQL.AppendLine("        Inner Join TblSOContractRevisionHdr D On C.SOContractDocNo = D.DocNo ");
            SQL.AppendLine("        Inner Join TblSOContractHdr E On D.SOCDocNo = E.DocNo ");
            SQL.AppendLine("        Inner Join TblBOQHdr F On E.BOQDocNO = F.DocNo ");
            SQL.AppendLine("        Inner Join TblLOPHdr G On F.LOPDocNo = G.DocNo ");
            SQL.AppendLine("        Left Join TblCostCenter I On G.CCCode = I.CCCode ");
            SQL.AppendLine("        Left Join TblProfitCenter J On I.ProfitCenterCode = J.ProfitCenterCode ");
            SQL.AppendLine("        Left Join TblEntity K ON J.EntCode = K.EntCode ");
            SQL.AppendLine("        Group By A.DocNo ");
            SQL.AppendLine("    )G On A.DocNo = G.DocNo ");
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                SQL.AppendLine("    Inner Join TblSalesInvoice5Dtl H On A.DocNo = H.DocNo And A.CancelInd = 'N' ");
                SQL.AppendLine("    Inner Join TblProjectImplementationHdr I On H.ProjectImplementationDocNo = I.DocNo ");
                SQL.AppendLine("    Inner Join TblSOContractRevisionHdr J On I.SOContractDocNo = J.DocNo ");
                SQL.AppendLine("    Inner Join TblSOContractHdr K On J.SOCDocNo = K.DocNo ");
                SQL.AppendLine("    Inner Join TblBOQHdr L On K.BOQDocNO = L.DocNo ");
                SQL.AppendLine("    Inner Join TblLOPHdr M On L.LOPDocNo = M.DocNo ");
                SQL.AppendLine("    Left Join TblCostCenter K2 On M.CCCode = K2.CCCode ");
                SQL.AppendLine("    Left Join TblProfitCenter O On K2.ProfitCenterCode = O.ProfitCenterCode ");
            }
            if (!mIsRptAgingARInvUseDocDtFilter)
                SQL.AppendLine("    Where A.CancelInd='N'  ");
            else
                SQL.AppendLine("    Where A.CancelInd='N' " + Filter3);

            if (mIsUseMInd)
            {
                if (!mMInd)
                    SQL.AppendLine("        And A.MInd='N' ");
            }
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                //SQL.AppendLine("AND FIND_IN_SET(K.ProfitCenterCode, @ProfitCenterCode) ");                
                SQL.AppendLine(mQueryProfitCenter);
            }
            SQL.AppendLine(Filter);
            SQL.AppendLine(Filter7);
            if (DteBalanceDt.Text.Length > 0) SQL.AppendLine("        And A.DocDt <= @BalanceDt ");

            #endregion

            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblCustomer T2 On T1.CtCode=T2.CtCode ");
            SQL.AppendLine(Filter4);
            if (mIsRptAgingARInvShowDOCt)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select A.DocNo, ");
                SQL.AppendLine("    Group_Concat(Distinct B.DOCtDocNo Order By B.DOCtDocNo Separator ', ') As DOCtDocNo ");
                SQL.AppendLine("    From TblSalesInvoiceHdr A, TblSalesInvoiceDtl B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo ");
                SQL.AppendLine("    And B.DOCtDocNo Is Not Null ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("    Group By A.DocNo ");
                SQL.AppendLine(") T3 On T1.DocNo=T3.DocNo ");
            }
                SQL.AppendLine("Left Join TblCustomerCategory T4 On T2.CtCtCode=T4.CtCtCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("       Select A.DocNo, ");
                SQL.AppendLine("       Group_Concat(Distinct IfNull(E.DeptName, '') SEPARATOR ', ') AS DeptName,  E.DeptCode ");
                SQL.AppendLine("       FROM tblSalesInvoiceDtl A ");
                SQL.AppendLine("       Inner Join tblDoctHdr B ON B.DocNo = A.DOCtDocNo ");
                SQL.AppendLine("       Inner Join tblWarehouse C ON C.WhsCode = B.WhsCode ");
                SQL.AppendLine("       Inner Join tblCostCenter D ON D.CCCode = C.CCCode ");
                SQL.AppendLine("       Inner Join tblDepartment E ON E.DeptCode = D.DeptCode ");
                SQL.AppendLine("       Group By A.DocNo, E.DeptCode ");
                SQL.AppendLine("       Union All ");
                SQL.AppendLine("       Select A.DocNo, ");
                SQL.AppendLine("       Group_Concat(Distinct IfNull(E.DeptName, '') SEPARATOR ', ') AS DeptName,  E.DeptCode ");
                SQL.AppendLine("       FROM tblSalesInvoiceDtl A ");
                SQL.AppendLine("       Inner Join tblDoct2Hdr B ON B.DocNo = A.DOCtDocNo ");
                SQL.AppendLine("       Inner Join tblWarehouse C ON C.WhsCode = B.WhsCode ");
                SQL.AppendLine("       Inner Join tblCostCenter D ON D.CCCode = C.CCCode ");
                SQL.AppendLine("       Inner Join tblDepartment E ON E.DeptCode = D.DeptCode ");
                SQL.AppendLine("       Group By A.DocNo, E.DeptCode ");
                SQL.AppendLine("       Union All ");
                SQL.AppendLine("       Select A.DocNo, ");
                SQL.AppendLine("Group_Concat(Distinct IfNull(F.DeptName, '') SEPARATOR ', ') AS DeptName, F.DeptCode ");
                SQL.AppendLine("FROM tblSalesreturnInvoiceDtl A ");
                SQL.AppendLine("INNER JOIN tblrecvctdtl B ON A.RecvCtDocNo = B.DocNo AND A.RecvCtDNo = B.DNo ");
                SQL.AppendLine("INNER JOIN tbldocthdr C ON B.DOCtDocNo = C.DocNo ");
                SQL.AppendLine("Inner Join tblWarehouse D ON C.WhsCode = D.WhsCode ");
                SQL.AppendLine("Inner Join tblCostCenter E ON D.CCCode = E.CCCode ");
                SQL.AppendLine("Inner Join tblDepartment F ON E.DeptCode = F.DeptCode ");
                SQL.AppendLine("Group By A.DocNo, F.DeptCode ");
            SQL.AppendLine(") T5 ON T5.DocNo = T1.DocNo ");
            if (!mIsRptAgingInvoiceARShowFulfilledDocument)
                SQL.AppendLine("Where T1.Balance<>0.00 ");
            else
            {
                SQL.AppendLine("Where 0 = 0 ");
                SQL.AppendLine(Filter5);
            }
            if (DteBalanceDt.Text.Length > 0) SQL.AppendLine("And T1.Status in ('O','P')");
            if (mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T5.DeptCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByCtCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                SQL.AppendLine("    Where CtCtCode=T4.CtCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine(Filter6);
            SQL.AppendLine("Order By T1.Period, T1.CurCode, T2.CtName, T1.DocNo; ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 41;
            Grd1.ReadOnly = false;
            Grd1.FrozenArea.ColCount = 7;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No", 
                        //1-5
                        "Period",
                        "Currency",
                        "Customer", 
                        "Invoice#",
                        "",

                        //6-10
                        "Local"+Environment.NewLine+"Document#",
                        "Document"+Environment.NewLine+"Date",
                        "Remark",
                        "Entity",
                        "DO",

                        //11-15
                        "Warehouse",
                        "Tax"+Environment.NewLine+"Invoice#",
                        "Amount"+Environment.NewLine+"Before Tax",
                        "Tax"+Environment.NewLine+"Amount",
                        "Downpayment",

                        //16-20
                        "Invoice"+Environment.NewLine+"Amount",
                        "Paid/"+Environment.NewLine+"Settled",
                        "Balance",
                        "Due"+Environment.NewLine+"Date",
                        "Aging"+Environment.NewLine+"(Days)",

                        //21-25
                        mOverdueFormat == "1" ? "Aging"+Environment.NewLine+"Current AR" : "Non Over Due",
                        "Over Due"+Environment.NewLine+"1-30 Days",
                        mOverdueFormat == "1" ? "Over Due"+Environment.NewLine+"31-60 Days" : "Over Due"+Environment.NewLine+"31-90 Days",
                        mOverdueFormat == "1" ? "Over Due"+Environment.NewLine+"61-90 Days" : "Over Due"+Environment.NewLine+"91-180 Days",
                        mOverdueFormat == "1" ? "Over Due"+Environment.NewLine+"91-120 Days" : "Over Due"+Environment.NewLine+"181-360 Days",

                        //26-30
                        mOverdueFormat == "1"? "Over 120 Days" : "Over Due"+Environment.NewLine+"361-720 Days",
                        "Over 720 Days",
                        "Tax 1",
                        "Tax Amount 1",
                        "Tax 2",
                        

                        //31-35
                        "Tax Amount 2",
                        "Tax 3",
                        "Tax Amount 3",
                        "DO To Customer",
                        "Voucher#",
                        

                        //36-40
                        "",
                        "Customer's Category",
                        "Status",
                        "Department Name",
                        "DocType"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        100, 60, 250, 145, 20,
                        //6-10
                        130, 130, 200, 180, 130, 
                        //11-15
                        130, 130, 130, 80, 130, 
                        //16-20
                        130, 130, 130, 130, 130,
                        //21-25
                        130, 130, 130, 130, 130,
                        //26-30
                        130, 130, 100, 120, 100, 
                        //31-35
                        120, 100, 120, 150, 150, 
                        //36-40
                        20, 200, 100, 150, 0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 5, 36 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,  29,  31,  33 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 7, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 28, 29, 30, 31, 32, 33, 35, 40 }, false);
            if (!mIsRptAgingARInvShowDOCt)
                Sm.GrdColInvisible(Grd1, new int[] { 34 }, false);
            if (!mIsRptShowVoucherLoop) Sm.GrdColInvisible(Grd1, new int[] { 36 }, false);
            if (mRptAgingARInvDlgFormat == "1")
                Grd1.Cols[39].Visible = false;
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 37, 38, 39, 40 });
            Grd1.Cols[37].Move(4);
            Grd1.Cols[36].Move(19);
            Grd1.Cols[38].Move(21);
            Grd1.Cols[39].Move(5);  
            if(mOverdueFormat == "1")
                Sm.GrdColInvisible(Grd1, new int[] { 27 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 38 }, mIsRptAgingInvoiceARShowFulfilledDocument);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 28, 29, 30, 31, 32, 33, 35 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (mIsFicoUseMultiProfitCenterFilter)
                if (IsProfitCenterInvalid()) return;

            if (ChkBalanceDt.Checked == false && ChkInvDt.Checked == false)
            {
                Sm.StdMsg(mMsgType.Warning, "Invoice Date or Balance As Of Date is empty"); return;
            } 

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(ref cm),
                    new string[]
                    { 
                        //0
                        "Period", 
                        //1-5
                        "CurCode", "CtName", "DocNo", "LocalDocNo", "DocDt", 
                        //6-10
                        "Remark", "EntName", "DOCtDocNo", "WhsName", "TaxInvDocument", 
                        //11-15
                        "TotalAmt", "TotalTax", "Downpayment", "Amt", "PaidAmt",   
                        //16-20
                        "Balance", "DueDt", "AgingDays", "AgingCurrent", "Aging1To30",      
                                
                        //21-25
                        mOverdueFormat=="1" ? "Aging31To60" : "Aging31To90", 
                        mOverdueFormat=="1" ? "Aging61To90" : "Aging91To180", 
                        mOverdueFormat=="1" ? "Aging91To120" : "Aging181To360", 
                        mOverdueFormat=="1" ? "AgingOver120" : "Aging361To720", 
                        "AgingOver720", 

                        //26-30
                        "TaxCode1", "TaxAmt1", "TaxCode2", "TaxAmt2", "TaxCode3", 
                        //31-35
                        "TaxAmt3", "DOCtDocNo", "VoucherDocNo", "CtCtName", "StatusDesc", 
                        //36-37
                        "DeptName", "Type"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 22);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 24);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 26);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 28);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 29);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 30);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 31);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 32);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 33);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 34);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 35);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 36);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 37);
                    }, true, false, false, false
                );
                Grd1.GroupObject.Add(1);
                Grd1.GroupObject.Add(2);
                Grd1.GroupObject.Add(3);
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                switch (mDocTitle)
                {
                    case "KMI":
                    case "KIM":
                        {
                            var f1 = new FrmSalesInvoice3(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                    case "AMKA":
                        {
                            var f1 = new FrmSalesInvoice3(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                    default:
                        {
                            string SLIType = Sm.GetGrdStr(Grd1, e.RowIndex, 40);
                            if (SLIType == "1")
                            {
                                string SLIDocType = Sm.GetValue("Select DocType From TblSalesInvoiceDtl where DocNo=@Param", Sm.GetGrdStr(Grd1, e.RowIndex, 4));

                                if (SLIDocType == "1")
                                {
                                    var f1 = new FrmSalesInvoice(mMenuCode);
                                    f1.Tag = mMenuCode;
                                    f1.WindowState = FormWindowState.Normal;
                                    f1.StartPosition = FormStartPosition.CenterScreen;
                                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                                    f1.ShowDialog();
                                }
                                else if (SLIDocType == "3")
                                {
                                    var f1 = new FrmSalesInvoice3(mMenuCode);
                                    f1.Tag = mMenuCode;
                                    f1.WindowState = FormWindowState.Normal;
                                    f1.StartPosition = FormStartPosition.CenterScreen;
                                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                                    f1.ShowDialog();
                                }
                            }
                            else if (SLIType == "2")
                            {
                                var f1 = new FrmSalesReturnInvoice(mMenuCode);
                                f1.Tag = mMenuCode;
                                f1.WindowState = FormWindowState.Normal;
                                f1.StartPosition = FormStartPosition.CenterScreen;
                                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                                f1.ShowDialog();
                            } 
                            else if (SLIType == "3")
                            {
                                var f1 = new FrmSalesInvoice5(mMenuCode);
                                f1.Tag = mMenuCode;
                                f1.WindowState = FormWindowState.Normal;
                                f1.StartPosition = FormStartPosition.CenterScreen;
                                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                                f1.ShowDialog();
                            }
                            break;
                        }
                }
            }
                if (mRptAgingARInvDlgFormat == "1" && (e.ColIndex == 36 && Sm.GetGrdStr(Grd1, e.RowIndex, 35).Length != 0))
                {
                    var f = new FrmRptAgingARInv3Dlg(this);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.VoucherDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 35);
                    f.ShowDialog();
                }
                if (mRptAgingARInvDlgFormat == "2" && (e.ColIndex == 36 && Sm.GetGrdStr(Grd1, e.RowIndex, 17).Length != 0))
                {
                    var f = new FrmRptAgingARInv3Dlg2(this);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.SalesInvoiceDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.ShowDialog();
                }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetLueType(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Col1, 'Sales Invoice' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Col1, 'Sales Return Invoice' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '3' As Col1, 'Sales Invoice For Project' As Col2; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Type", "Col2", "Col1");
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T.ProfitCenterName As Col, T.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T ");
            if (mIsFilterByProfitCenter)
            {
                SQL.AppendLine("WHERE Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
                SQL.AppendLine("        Where ProfitCenterCode=T.ProfitCenterCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param)) T; ",
                    Value.Replace(", ", ","));
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        private void GetParameter()
        {
            //string MenuCodeForDocWithMInd = Sm.GetParameter("MenuCodeForDocWithMInd");
            //if (MenuCodeForDocWithMInd.Length > 0)
            //    mMInd = MenuCodeForDocWithMInd.IndexOf("##" + mMenuCode + "##") != -1;
            //mIsUseMInd = Sm.GetParameterBoo("IsUseMInd");
            //mIsRptAgingARInvShowDOCt = Sm.GetParameterBoo("IsRptAgingARInvShowDOCt");
            //mIsRptAgingARInvUseDocDtFilter = Sm.GetParameterBoo("IsRptAgingARInvUseDocDtFilter");
            //mDocTitle = Sm.GetParameter("DocTitle");
            //mIsRptShowVoucherLoop = Sm.GetParameterBoo("IsRptShowVoucherLoop");
           

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'DocTitle', 'IsUseMInd', 'IsRptAgingARInvShowDOCt', 'IsRptAgingARInvUseDocDtFilter', 'IsRptShowVoucherLoop', ");
            SQL.AppendLine("'MenuCodeForDocWithMInd', 'IsRptAgingInvoiceARShowFulfilledDocument', 'RptAgingARInvDlgFormat', 'IsFilterByCtCt', 'IsFilterByDept', ");
            SQL.AppendLine("'IsFicoUseMultiProfitCenterFilter', 'IsFilterByProfitCenter', 'OverdueFormat' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsUseMInd": mIsUseMInd = ParValue == "Y"; break;
                            case "IsRptAgingARInvShowDOCt": mIsRptAgingARInvShowDOCt = ParValue == "Y"; break;
                            case "IsRptAgingARInvUseDocDtFilter": mIsRptAgingARInvUseDocDtFilter = ParValue == "Y"; break;
                            case "IsRptShowVoucherLoop": mIsRptShowVoucherLoop = ParValue == "Y"; break;
                            case "IsRptAgingInvoiceARShowFulfilledDocument": mIsRptAgingInvoiceARShowFulfilledDocument = ParValue == "Y"; break;
                            case "IsFilterByCtCt": mIsFilterByCtCt = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsFicoUseMultiProfitCenterFilter": mIsFicoUseMultiProfitCenterFilter = ParValue == "Y"; break;
                            case "IsFilterByProfitCenter": mIsFilterByProfitCenter = ParValue == "Y"; break;

                            //string
                            case "DocTitle": mDocTitle = ParValue; break;
                            case "MenuCodeForDocWithMInd":
                                string MenuCodeForDocWithMInd = ParValue;
                                if (MenuCodeForDocWithMInd.Length > 0)
                                    mMInd = MenuCodeForDocWithMInd.IndexOf("##" + mMenuCode + "##") != -1;
                                break;
                            case "RptAgingARInvDlgFormat": mRptAgingARInvDlgFormat = ParValue;
                                if (mRptAgingARInvDlgFormat.Length == 0) mRptAgingARInvDlgFormat = "1";
                                break;
                            case "OverdueFormat": mOverdueFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 13, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25, 26, 29, 31, 33 });
        }

        private void SetLueStatus()
        {
            Sm.SetLue2(
                ref LueStatus,
                "Select 'O' As Col1, 'Outstanding' As Col2 " +
                "Union All Select 'P' As Col1, 'Partial' As Col2 " +
                "Union All Select 'F' As Col1, 'Fulfilled' As Col2 " +
                "Union All Select 'OP' As Col1, 'Outstanding+Partial' As Col2;",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit Center");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue3(Sl.SetLueCtCode), string.Empty, mIsFilterByCtCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Due date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocumentDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocumentDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocumentDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document's date");
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue3(Sl.SetLueCtCtCode), string.Empty, mIsFilterByCtCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer's category");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueStatus), "<Refresh>")) LueStatus.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueType), "<Refresh>")) LueType.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void ChkInvDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Invoice date");
        }

        private void DteInvDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);

            if (DteInvDt1.EditValue != null)
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteBalanceDt, ChkBalanceDt }, true);
            else
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteBalanceDt, ChkBalanceDt }, false);

            if (Sm.CompareDtTm(Sm.GetDte(DteInvDt2),Sm.GetDte(DteInvDt1)) < 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Start invoice date is later than end invoice date.");
                Sm.SetDte(DteInvDt1, Sm.GetDte(DteInvDt2));
            }

        }

        private void DteInvDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);

            if (DteInvDt2.EditValue != null)
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteBalanceDt, ChkBalanceDt }, true);
            else
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteBalanceDt, ChkBalanceDt }, false);

            if (Sm.CompareDtTm(Sm.GetDte(DteInvDt1), Sm.GetDte(DteInvDt2)) > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "End invoice date is earlier than start invoice date.");
                Sm.SetDte(DteInvDt2, Sm.GetDte(DteInvDt1));
            }
        }

        private void ChkBalanceDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSingleSetDateEdit(this, sender, "Balance as of date");
        }

        private void DteBalanceDt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterSingleDteSetCheckEdit(this, sender);

            if (DteBalanceDt.EditValue != null)
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ DteInvDt1, DteInvDt2, ChkInvDt }, true);
            else
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteInvDt1, DteInvDt2, ChkInvDt }, false);
        }

        #endregion

        #endregion

    }
}
