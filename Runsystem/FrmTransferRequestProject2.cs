﻿#region Update
/*
    30/09/2020 [WED/IMS] new apps, di detail tanpa warehouse
    13/10/2020 [WED/IMS] Project mandatory, item nya ambil dari warehouse yg tersedia & batchnya sesuai dengan project code terpilih
    05/11/2020 [TKG/IMS] tambah printout
    11/11/2020 [VIN/IMS] penyesuaian printout tambah CCName dan Item Name 
    14/12/2020 [WED/IMS] ada cancel dokumen
    15/12/2020 [WED/IMS] approval berdasarkan parameter IsTransferRequestProjectApprovalBasedOnWhs
    15/12/2020 [WED/IMS] bug cancel nya itu per item, bukan per document
    29/12/2020 [IBL/IMS] bug printout. item yang dicancel masih muncul di printout
    04/07/2021 [TKG/IMS] difilter berdasarkan item category
    19/10/2021 [VIN/IMS] TR bisa di cancel kalau DOWHS nya sdh di cancel
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTransferRequestProject2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
           mMenuCode = string.Empty, mAccessInd = string.Empty,
           mDocNo = string.Empty, //if this application is called from other application;
           mPGCode = string.Empty; 
        internal FrmTransferRequestProject2Find FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        internal bool mIsSystemUseCostCenter = false;
        internal bool
            mIsFilterByItCt = false,
            mIsTransferRequestWhsProjectEnabled = false,
            mIsItGrpCodeShow = false,
            mIsBOMShowSpecifications= false;
        private bool mIsTransferRequestProjectApprovalBasedOnWhs = false;
        #endregion

        #region Constructor

        public FrmTransferRequestProject2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Transfer Request for Project";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Tp2.PageVisible = mIsTransferRequestWhsProjectEnabled;
                SetGrd();
                Tc1.SelectedTabPage = Tp2;
                SetLueItCode(ref LueItCode, string.Empty);
                Tc1.SelectedTabPage = Tp1;
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "",
                        "Item's Code",
                        "Item's Name",
                        "Stock",

                        //6-10
                        "Requested",
                        "UoM",
                        "Stock",
                        "Requested",
                        "UoM",

                        //11-15
                        "Stock",
                        "Requested",
                        "UoM",
                        "Remark",
                        "Cost Category",

                        //16-20
                        "Group",
                        "Local Code",
                        "Specification",
                        "Cancel",
                        "Old Cancel",

                        //21
                        "Status"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        20, 20, 100, 200, 0, 

                        //6-10
                        100, 80, 0, 100, 80, 

                        //11-15
                        0, 100, 80, 200, 150, 
 
                        //16-20
                        150, 180, 200, 50, 50,

                        //21
                        120
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 19, 20 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 8, 9, 11, 12 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 9, 10, 11, 12, 13, 17, 20 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 11 });
            Grd1.Cols[19].Move(1);
            Grd1.Cols[21].Move(4);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 3, 4, 5, 7, 8, 10, 11, 13, 15, 16, 17, 18, 20, 21 });

            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[16].Visible = true;
                Grd1.Cols[16].Move(5);
            }
            ShowInventoryUomCode();
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 18 });
            Grd1.Cols[17].Move(4);
            Grd1.Cols[18].Move(7);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 17 }, !ChkHideInfoInGrd.Checked);
            ShowInventoryUomCode();
            if (!(BtnSave.Enabled && TxtDocNo.Text.Length == 0))
            {
                if (mNumberOfInventoryUomCode == 2)
                    Sm.GrdColInvisible(Grd1, new int[] { 10 }, true);
                if (mNumberOfInventoryUomCode == 3)
                    Sm.GrdColInvisible(Grd1, new int[] { 13, 15 }, true);
            }
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 12, 13 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueWhsCode2, MeeRemark, LueItCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 6, 9, 12, 14, 19 });
                    BtnPGCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueWhsCode2, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] {  6, 9, 12, 14 });
                    BtnPGCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 19 });
                    break;
            }
        }

        private void ClearData()
        {
            mPGCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtLocalDocNo, LueWhsCode2, 
                MeeRemark, TxtProjectCode, TxtProjectName, TxtSOCDocNo, LueItCode
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 6, 8, 9, 11, 12 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTransferRequestProject2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode2, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }
        
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1 && !Sm.IsTxtEmpty(TxtProjectCode, "Project code", false))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                            Sm.FormShowDialog(new FrmTransferRequestProject2Dlg(this));
                    }
                }
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
          
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtProjectCode, "Project code", false))
                Sm.FormShowDialog(new FrmTransferRequestProject2Dlg(this));

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }

        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 6, 9, 12 }, e);

            if (e.ColIndex == 6)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 3, 6, 9, 12, 7, 10, 13);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 3, 6, 12, 9, 7, 13, 10);
            }

            if (e.ColIndex == 9)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 3, 9, 6, 12, 10, 7, 13);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 3, 9, 12, 6, 10, 13, 7);
            }

            if (e.ColIndex == 12)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 3, 12, 6, 9, 13, 7, 10);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 3, 12, 9, 6, 13, 10, 7);
            }

            if (e.ColIndex == 6 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 7), Sm.GetGrdStr(Grd1, e.RowIndex, 10)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 9, Grd1, e.RowIndex, 6);

            if (e.ColIndex == 6 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 7), Sm.GetGrdStr(Grd1, e.RowIndex, 13)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 12, Grd1, e.RowIndex, 6);

            if (e.ColIndex == 9 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 10), Sm.GetGrdStr(Grd1, e.RowIndex, 13)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 12, Grd1, e.RowIndex, 9);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "TransferRequestProject", "TblTransferRequestProjectHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveTransferRequestProjectHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0)
                    cml.Add(SaveTransferRequestProjectDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode2, "Warehouse to") ||
                Sm.IsTxtEmpty(TxtProjectCode, "Project Code", false) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Item is empty.")) return true;
                for (int j = (Row + 1); j < Grd1.Rows.Count - 1; ++j)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 3) == Sm.GetGrdStr(Grd1, j, 3))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Duplicate item found.");
                        Sm.FocusGrd(Grd1, j, 4);
                        return true;
                    }
                }
            }
            return false;
        }

        void GenerateSQLConditionForInventory(
            ref MySqlCommand cm, ref string Filter, int No,
            ref iGrid Grd, int Row, int Col)
        {
            if (Filter.Length > 0) Filter += " Or ";
            Filter += "(X.ItCode=@ItCode" + No + ") ";
            Sm.CmParam<String>(ref cm, "@ItCode" + No, Sm.GetGrdStr(Grd, Row, Col));
        }

        private MySqlCommand SaveTransferRequestProjectHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTransferRequestProjectHdr(DocNo, DocDt, LocalDocNo, WhsCode, PGCode, SOContractDocNo, ItCode,  Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @LocalDocNo, @WhsCode, @PGCode, @SOContractDocNo, @ItCode,  @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@PGCode", mPGCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOCDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetLue(LueItCode));
            return cm;
        }

        private MySqlCommand SaveTransferRequestProjectDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTransferRequestProjectDtl(DocNo, DNo, Status, ItCode, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'O', @ItCode, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, @DNo, DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='TransferRequestProject' ");
            if (mIsTransferRequestProjectApprovalBasedOnWhs)
                SQL.AppendLine("And WhsCode = @WhsCode ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblTransferRequestProjectDtl Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo = @DNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And DNo = @DNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Edit Data

        private bool IsEditedDataNotValid(string DNo)
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsDataNotCancelled(DNo) ||
                IsDataAlreadyCancelled(DNo) ||
                IsDataAlreadyProcessed(DNo)
                ;
        }

        private bool IsDataNotCancelled(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyCancelled(string DNo)
        {
            //var SQL = new StringBuilder();

            //SQL.AppendLine("Select 1 ");
            //SQL.AppendLine("From TblTransferRequestProjectHdr ");
            //SQL.AppendLine("Where DocNo = @Param ");
            //SQL.AppendLine("And (CancelInd = 'Y' Or Status = 'C') ");
            //SQL.AppendLine("Limit 1; ");

            //if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            //{
            //    Sm.StdMsg(mMsgType.Warning, "This document is already cancelled.");
            //    return true;
            //}

            return false;
        }

        private bool IsDataAlreadyProcessed(string DNo)
        {
            var SQL = new StringBuilder();
            string DODocNo = string.Empty;

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblDOWhs4Hdr A ");
            SQL.AppendLine("Inner Join  TblDOWhs4Dtl B on A.DocNo=B.DocNo ");
            SQL.AppendLine("Where B.TransferRequestProjectDocNo = @Param And B.TransferRequestProjectDNo In (" + DNo + ") ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Limit 1; ");

            DODocNo = Sm.GetValue(SQL.ToString(), TxtDocNo.Text);

            if (DODocNo.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already processed in #" + DODocNo);
                return true;
            }

            return false;
        }

        private void EditData()
        {
            UpdateCancelledItem();
            string DNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 19) && !Sm.GetGrdBool(Grd1, Row, 20) && Sm.GetGrdStr(Grd1, Row, 3).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditTransferRequestProjectDtl(TxtDocNo.Text, DNo));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, CancelInd ");
            SQL.AppendLine("From TblTransferRequestProjectDtl ");
            SQL.AppendLine("Where DocNo = @DocNo Order By DNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 19, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 20, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private MySqlCommand EditTransferRequestProjectDtl(string DocNo, string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTransferRequestProjectDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And (CancelInd='N' Or Status='C') And DNo In (" + DNo + "); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowTransferRequestProjectHdr(DocNo);
                ShowTransferRequestProjectDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTransferRequestProjectHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("A.LocalDocNo, A.WhsCode, A.Remark, B.PGCode, B.ProjectCode, B.ProjectName, A.SOContractDocNo, A.ItCode ");
            SQL.AppendLine("From TblTransferRequestProjectHdr A ");
            SQL.AppendLine("Left Join TblProjectGroup B On A.PGCode=B.PGCode ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "LocalDocNo", "WhsCode",  "Remark", "PGCode",
                    
                    //6-9
                    "ProjectCode", "ProjectName", "SOContractDocNo", "ItCode"
                   
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[2]);
                    Sl.SetLueWhsCode(ref LueWhsCode2, Sm.DrStr(dr, c[3]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    mPGCode = Sm.DrStr(dr, c[5]);
                    TxtProjectCode.EditValue = Sm.DrStr(dr, c[6]);
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[7]);
                    TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[8]);
                    SetLueItCode(ref LueItCode, Sm.DrStr(dr, c[9]));
                    Sm.SetLue(LueItCode, Sm.DrStr(dr, c[9]));
                }, true
            );
        }

        private void ShowTransferRequestProjectDtl(string DocNo)
        {
            try
            {
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DNo, A.CancelInd, ");
                SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc, ");
                SQL.AppendLine("A.ItCode, B.ItName, ");
                SQL.AppendLine("A.Qty, B.InventoryUOMCode, ");
                SQL.AppendLine("A.Qty2, B.InventoryUOMCode2, ");
                SQL.AppendLine("A.Qty3, B.InventoryUOMCode3, ");
                SQL.AppendLine("A.Remark, E.CCtName, B.ItGrpCode, B.ItCodeInternal, B.Specification ");
                SQL.AppendLine("From TblTransferRequestProjectDtl A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
                SQL.AppendLine("Left Join TblItemCostCategory D On A.ItCode=D.ItCode And D.CCCode=@CCCode ");
                SQL.AppendLine("Left Join TblCostCategory E On D.CCCode=E.CCCode And D.CCtCode=E.CCtCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "ItCode", "ItName", "Qty",  "InventoryUomCode", "Qty2", 
                        
                        //6-10
                        "InventoryUomCode2", "Qty3", "InventoryUomCode3", "Remark",  "CCtName",   
                        
                        //11-15
                        "ItGrpCode", "ItCodeInternal", "Specification", "CancelInd", "StatusDesc"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 13);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 19, 14);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 20, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 15);
                    }, false, false, true, false
                );
                Sm.FocusGrd(Grd1, 0, 1);
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 6, 8, 9, 11, 12 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            
                string ParValue = Sm.GetValue("Select ParValue From TblParameter Where Parcode='NumberOfInventoryUomCode';");

                if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || 
                    (Sm.GetParameter("DocTitle") == "MSI") || 
                    Sm.StdMsgYN("Print", "") == DialogResult.No) return;

                string[] TableName = { "TransferReqHdr", "TransferReqHdr2", "TransferReqHdr3", "TransferReqDtl" };
                string mDocTitle = Sm.GetParameter("DocTitle");
                var l = new List<TransferReqHdr>();
                var l1 = new List<TransferReqHdr2>();
                var l2 = new List<TransferReqHdr3>();
                var ldtl = new List<TransferReqDtl>();

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header
                var SQL = new StringBuilder();

                SQL.AppendLine("Select @CompanyLogo As CompanyLogo, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As Phone, ");
                SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y') As DocDt, B.WhsName, ");
                SQL.AppendLine("Null As WhsName2, ");
                SQL.AppendLine("F.UserName As DoReqCreateBy, F1.UserName As DoToDeptCreateby, ");
                SQL.AppendLine("Concat(IfNull(G.ParValue, ''), F.UserCode, '.JPG') As EmpPict, ");
                SQL.AppendLine("Concat(IfNull(G.ParValue, ''), F1.UserCode, '.JPG') As EmpPict2, ");
                SQL.AppendLine("A.Remark, NULL AS ProjectCode, H.ProjectName AS ProjectName, NULL AS Workshop, I.CCName  ");
                SQL.AppendLine("From TblTransferRequestProjectHdr A ");
                SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode  ");
                //SQL.AppendLine("Inner Join TblWarehouse B1 On A.WhsCode2=B1.WhsCode ");
                SQL.AppendLine("Inner Join tbluser F On A.CreateBy=F.UserCode  ");
                SQL.AppendLine("Inner Join tbluser F1 On A.CreateBy=F1.UserCode ");
                SQL.AppendLine("Left Join TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine("Left Join TblProjectGroup H On A.PGCode=H.PGCode ");
                SQL.AppendLine("Left Join TblCostCenter I On B.CCCode=I.CCCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "Company",
                         "Address",
                         "Phone",
                         "DocNo",
                         "DocDt",

                         //6-10
                         "WhsName",
                         "WhsName2",
                         "DoReqCreateBy",
                         "DoToDeptCreateby",
                         "EmpPict",

                         //11-15
                         "EmpPict2",
                         "Remark",
                         "ProjectCode",
                         "ProjectName",
                         "WorkShop",

                         //16
                         "CCName"

                         
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new TransferReqHdr()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                Company = Sm.DrStr(dr, c[1]),
                                Address = Sm.DrStr(dr, c[2]),
                                Phone = Sm.DrStr(dr, c[3]),
                                DocNo = Sm.DrStr(dr, c[4]),
                                DocDt = Sm.DrStr(dr, c[5]),

                                WhsName = Sm.DrStr(dr, c[6]),
                                WhsName2 = Sm.DrStr(dr, c[7]),
                                DoReqCreateBy = Sm.DrStr(dr, c[8]),
                                DoToDeptCreateby = Sm.DrStr(dr, c[9]),
                                EmpPict = Sm.DrStr(dr, c[10]),

                                EmpPict2 = Sm.DrStr(dr, c[11]),
                                Remark = Sm.DrStr(dr, c[12]),
                                ProjectCode = Sm.DrStr(dr, c[13]),
                                ProjectName = Sm.DrStr(dr, c[14]),
                                Workshop = Sm.DrStr(dr, c[15]),

                                CCName = Sm.DrStr(dr, c[16]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                PrintDt = String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }

                    dr.Close();
                }
                myLists.Add(l);
                #endregion

                #region Header2
                var cm1 = new MySqlCommand();
                var SQL1 = new StringBuilder();

                SQL1.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
                SQL1.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict ");
                SQL1.AppendLine("from TblDocApproval A ");
                SQL1.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
                SQL1.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQL1.AppendLine("Where DocType = 'TransferRequestProject' ");
                SQL1.AppendLine("And DocNo =@DocNo ");
                SQL1.AppendLine("Group by ApprovalDno limit 1");

                using (var cn1 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn1.Open();
                    cm1.Connection = cn1;
                    cm1.CommandText = SQL1.ToString();
                    Sm.CmParam<String>(ref cm1, "@DocNo", TxtDocNo.Text);
                    var dr1 = cm1.ExecuteReader();
                    var c1 = Sm.GetOrdinal(dr1, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-3
                         "UserCode",
                         "UserName",
                         "EmpPict"

                        
                        });
                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            l1.Add(new TransferReqHdr2()
                            {
                                ApprovalDno = Sm.DrStr(dr1, c1[0]),
                                UserCode = Sm.DrStr(dr1, c1[1]),
                                UserName = Sm.DrStr(dr1, c1[2]),
                                EmpPict = Sm.DrStr(dr1, c1[3]),
                            });
                        }
                    }

                    dr1.Close();
                }
                myLists.Add(l1);
                #endregion

                #region Header3
                var cm2 = new MySqlCommand();
                var SQL2 = new StringBuilder();

                SQL2.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
                SQL2.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict ");
                SQL2.AppendLine("from TblDocApproval A ");
                SQL2.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
                SQL2.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQL2.AppendLine("Where DocType = 'TransferRequestProject' ");
                SQL2.AppendLine("And DocNo =@DocNo ");
                SQL2.AppendLine("Group by ApprovalDno Order by ApprovalDno Desc limit 1");

                using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn2.Open();
                    cm2.Connection = cn2;
                    cm2.CommandText = SQL2.ToString();
                    Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                    var dr2 = cm2.ExecuteReader();
                    var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "ApprovalDno",

                         //1-3
                         "UserCode",
                         "UserName",
                         "EmpPict"
                        });
                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            l2.Add(new TransferReqHdr3()
                            {
                                ApprovalDno = Sm.DrStr(dr2, c2[0]),
                                UserCode = Sm.DrStr(dr2, c2[1]),
                                UserName = Sm.DrStr(dr2, c2[2]),
                                EmpPict = Sm.DrStr(dr2, c2[3]),
                            });
                        }
                    }

                    dr2.Close();
                }
                myLists.Add(l2);

                #endregion

                #region Detail

                var cmDtl = new MySqlCommand();

                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select A.ItCode, B.ItName, B.ItCodeInternal, B.Specification, B.ItGrpCode, ");
                    SQLDtl.AppendLine("A.Qty, A.Qty2, A.Qty3, B.InventoryUOMCode, B.InventoryUOMCode2, B.InventoryUOMCode3, A.Remark, ");
                    SQLDtl.AppendLine("NULL As ProjectCode ");
                    SQLDtl.AppendLine("From TblTransferRequestProjectDtl A ");
                    SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNo ");
                    SQLDtl.AppendLine("And A.CancelInd = 'N' ");
                    SQLDtl.AppendLine("Order By A.DNo;");

                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                             //0
                             "ItCode",
                             
                             //1-5
                             "ItName",
                             "ItGrpCode",
                             "Qty",
                             "Qty2",
                             "Qty3",

                             //6-10
                             "InventoryUOMCode",
                             "InventoryUOMCode2",
                             "InventoryUOMCode3",
                             "Remark",
                             "ProjectCode",

                             //11-12
                             "ItCodeInternal",
                             "Specification"
                        });
                    int nomor = 0;
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            nomor = nomor + 1;
                            ldtl.Add(new TransferReqDtl()
                            {
                                No = nomor,
                                ItCode = Sm.DrStr(drDtl, cDtl[0]),

                                ItName = Sm.DrStr(drDtl, cDtl[1]),
                                ItGrpCode = Sm.DrStr(drDtl, cDtl[2]),
                                Qty1 = Sm.DrDec(drDtl, cDtl[3]),
                                Qty2 = Sm.DrDec(drDtl, cDtl[4]),
                                Qty3 = Sm.DrDec(drDtl, cDtl[5]),

                                InventoryUomCode = Sm.DrStr(drDtl, cDtl[6]),
                                InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[7]),
                                InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[8]),
                                Remark = Sm.DrStr(drDtl, cDtl[9]),
                                ProjectCode = Sm.DrStr(drDtl, cDtl[10]),

                                ItCodeInternal = Sm.DrStr(drDtl, cDtl[11]),
                                Specification = Sm.DrStr(drDtl, cDtl[12])
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                Sm.PrintReport("TransferRequestProject2IMS", myLists, TableName, false);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            mIsSystemUseCostCenter = Sm.GetParameterBoo("IsSystemUseCostCenter");
            mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsTransferRequestWhsProjectEnabled = Sm.GetParameterBoo("IsTransferRequestWhsProjectEnabled");
            mIsTransferRequestProjectApprovalBasedOnWhs = Sm.GetParameterBoo("IsTransferRequestProjectApprovalBasedOnWhs");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        //private void ComputeQtyBasedOnConvertionFormula(
        //    string ConvertType, iGrid Grd, int Row, int ColItCode,
        //    int ColQty1, int ColQty2, int ColUom1, int ColUom2)
        //{
        //    try
        //    {
        //        if (!Sm.CompareGrdStr(Grd, Row, ColUom1, Grd, Row, ColUom2))
        //        {
        //            decimal Convert = GetInventoryUomCodeConvert(ConvertType, Sm.GetGrdStr(Grd, Row, ColItCode));
        //            if (Convert != 0)
        //            {
        //                Grd.Cells[Row, ColQty2].Value = Convert * Sm.GetGrdDec(Grd, Row, ColQty1);
        //            }
        //        }
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        //private decimal GetInventoryUomCodeConvert(string ConvertType, string ItCode)
        //{
        //    var cm = new MySqlCommand
        //    {
        //        CommandText =
        //            "Select InventoryUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
        //    };
        //    Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
        //    return Sm.GetValueDec(cm);
        //}

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 3).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ",";
                        SQL += Sm.GetGrdStr(Grd1, Row, 3);
                    }
                }
            }
            return (SQL.Length == 0 ? "XXX" : SQL);
        }

        public static void SetLueCCCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select T.CCCode As Col1, T.CCName As Col2 " +
                "From TblCostCenter T " +
                "Where Not Exists(Select Parent From TblCostCenter Where CCCode=T.CCCode And Parent Is Not Null) " +
                "Order By T.CCName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueItCode(ref DXE.LookUpEdit Lue, string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode As Col1, B.ItName As Col2 ");
            SQL.AppendLine("From TblSOContractDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            if(ItCode.Length > 0)
                SQL.AppendLine("Where A.ItCode=@ItCode ");
            else
                SQL.AppendLine("Where A.DocNo=@SOCDocNo ");
            SQL.AppendLine("Order By B.ItName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
        }
        private void LueItCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode, new Sm.RefreshLue2(SetLueItCode), string.Empty);
        }

        private void BtnPGCode_Click(object sender, EventArgs e)
        {
            var f = new FrmProjectGroupStdDlg();
            f.TopLevel = true;
            f.ShowDialog();
            if (f.mPGCode.Length > 0)
            {
                mPGCode = f.mPGCode;
                TxtProjectCode.EditValue = f.mProjectCode;
                TxtProjectName.EditValue = f.mProjectName;
                TxtSOCDocNo.EditValue = f.mSOCDocNo;
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueItCode }, false);
                SetLueItCode(ref LueItCode, string.Empty);
            }
            else
            {
                mPGCode = string.Empty;
                TxtProjectCode.EditValue = null;
                TxtProjectName.EditValue = null;
                TxtSOCDocNo.EditValue = null;
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueItCode }, true);
                LueItCode.EditValue = null;
            }
            f.Close();
        }

        #endregion

        #region Printout Class

        #region Printout Class

        class TransferReqProject
        {
            public int Index { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string LocalDocNo { get; set; }
            public string WhsCode { get; set; }
            public string WhsCode2 { get; set; }
            public string SOContractDocNo { get; set; }
            public string PGCode { get; set; }
            public string Remark { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string Remark2 { get; set; }
        }

        class TransferReqProjectHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string LocalDocNo { get; set; }
            public string WhsCode { get; set; }
            public string WhsCode2 { get; set; }
            public string SOContractDocNo { get; set; }
            public string PGCode { get; set; }
            public string Remark { get; set; }
        }

        class TransferReqProjectDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string Remark2 { get; set; }
        }

        #endregion

        #endregion
    }
}
