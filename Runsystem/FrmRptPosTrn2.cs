﻿#region Update
/*
    03/06/2020 [HAR/SRN] bug filter
 *  20/07/2020 [HAR/SRN] ada parameter untuk menampung nama database perantara
 *  07/12/2020 [HAR/SRN] data transaksi yang muncul berdasarkan data journal terkahir yang di proses
 *  08/12/2020 [IBL/SRN] data item nyambung ke itCode, bukan itcodeinternal
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPosTrn2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private string mDBNamePOS = string.Empty;

        #endregion

        #region Constructor

        public FrmRptPosTrn2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDate = Sm.ServerCurrentDateTime();
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mDBNamePOS = Sm.GetParameter("DBNamePOS");
        }

        override protected void SetSQL()
        {
            string processDt = Sm.GetValue("Select cast(max(cast(processdt AS Decimal)) AS CHAR) From TblSRNJournalProcess; ");
            
            var SQL = new StringBuilder();

            SQL.AppendLine("SET @ProcessDt = '"+processDt+"' ;");
            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select Distinct A.ItCode As ItCodePos, B.ItCode As ItCodeRS, B.ItName, B.ItCodeInternal, ");
            SQL.AppendLine("    C.ItCtName, C.Acno, C.AcNo4, C.AcNo5 ");
            SQL.AppendLine("    From " + mDBNamePOS + ".TblPosTrn A ");
            SQL.AppendLine("    Left Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("    Left Join TblItemCategory C On B.ItCtCode = C.ItCtCode ");
            SQL.AppendLine("    Where A.BsDate>@ProcessDt ");
            SQL.AppendLine(")T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item Code POS",
                        "Item Code RS", 
                        "Item"+Environment.NewLine+"Name RS",
                        "Item"+Environment.NewLine+"Category RS",
                        "Acount (COA)"+Environment.NewLine+"Stock",
                        
                        //6-10
                        "Acount (COA)"+Environment.NewLine+"Sales",
                        "Acount (COA)"+Environment.NewLine+"COGS",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 150, 180, 150, 120, 

                        //6-10
                        120, 120
                    }
                );
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "Where 0=0 ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCodePOS", "T.ItCodeRS", "T.ItName", "T.ItCodeInternal" });

                
                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter 
                +"And (  "
                +"T.AcNo is null  "
                +"Or T.Acno4 is null  "
                +"Or T.Acno5 is null  "
                +"Or T.ItCtName is null) "
                +" Order By  T.ItCodePOS ;",
                new string[]
                    {
                        //0
                       "ItCodePos", 
                       //1-5
                       "ItCodeRS", "ItName", "ItCtName", "Acno", "AcNo4", 
                       //6-10
                       "AcNo5"
                    },
                (
                    MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion 

        #region Event
        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion 
    }
}
