﻿#region Update
/*
    20/09/17 [ARI] tambah kolom Remark.
    20/08/18 [TKG] mempercepat proses data.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptWorkInProgressListing : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptWorkInProgressListing(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Z.*, ifnull(Z1.Qty1, 0) As Qty1, If(Z1.Qty1>=Z.Qty, 'Finished', 'On Proggress') As status From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select O.DocNo As PPDocNo, A.Docno AS ProdOrderDocNo, A.DocDt, C.Sequence, Concat('(Sequence : ', C.Sequence, ')', ' ', G.ItName ) As WIPName, C.WorkCenterDocNo, D.DocName As WorkCenterDocName,  ");
            SQL.AppendLine("    F.ItCode, G.Itname, F.Qty, O.Remark ");
            SQL.AppendLine("    From TblPPHdr O ");
            SQL.AppendLine("    Inner Join TblPPDtl P On O.DocNo = P.DocNo ");
            SQL.AppendLine("    Inner join TblProductionOrderHdr A On P.ProductionOrderDocNo = A.DOcNo ");
            SQL.AppendLine("        And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Inner Join TblProductionOrderDtl B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("    Inner Join TblProductionRoutingDtl C On A.ProductionRoutingDocNo=C.DocNo And B.ProductionRoutingDNo=C.DNo ");
            SQL.AppendLine("    Left Join TblWorkCenterHdr D On C.WorkCenterDocNo=D.DocNo ");
            SQL.AppendLine("    Left Join TblBomHdr E On B.BomDocNo=E.DocNo ");
            SQL.AppendLine("    Left Join TblBomDtl2 F On E.DocNo = F.DocNo ");
            SQL.AppendLine("    Inner Join Tblitem G On F.ItCode = G.ItCode ");
            SQL.AppendLine(")Z ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.PPDocNo, A.WorkCenterDocNo, B.ItCode, Sum(Qty) As Qty1 ");
            SQL.AppendLine("    From TblShopFloorControlHdr A ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Where A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.PPDocNo In ( ");
            SQL.AppendLine("        Select T1.DocNo From TblPPHdr T1 ");
            SQL.AppendLine("        Inner Join TblPPDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        Inner join TblProductionOrderHdr T3 On T2.ProductionOrderDocNo = T3.DocNo ");
            SQL.AppendLine("            And T3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By A.PPDocNo, A.WorkCenterDocNo, B.ItCode ");
            SQL.AppendLine(") Z1 On Z.PPDocNo = Z1.PPDocNo  ");
            SQL.AppendLine("    And Z.WorkcenterDocNo = Z1.WorkcenterDocNo ");
            SQL.AppendLine("    And Z.ItCode = Z1.ItCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 3;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Production Order", 
                        "",
                        "Production Order"+Environment.NewLine+"Date",
                        "WIP",
                        "Workcenter"+Environment.NewLine+"Number",

                        //6-9
                        "",
                        "Workcenter",
                        "Status",
                        "Remark"
                       
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 20, 100, 250, 130,    
                        
                        //6-9
                        20, 200, 100, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 6 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 5, 7, 8, 9 });
            Sm.GrdColInvisible(Grd1, new int[] {2, 5, 6 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "Z.ProdOrderDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenterDocNo.Text, new string[] { "Z.WorkCenterDocNo", "Z.WorkCenterDocName" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By Z.ProdOrderDocNo, Z.Sequence Desc ",
                        new string[]
                        {
                            //0
                            "ProdOrderDocNo", 

                            //1-5
                            "DocDt", "WIPName", "WorkcenterDocNo", "WorkCenterDocName", "Status",

                            //6
                            "Remark"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        }, true, false, false, false
                    );
                Grd1.BeginUpdate();
                Grd1.GroupObject.Add(1);
                //Grd1.GroupObject.Add(4);
                Grd1.Group();
                Grd1.EndUpdate();
                //if (Grd1.CurCell != null)
                //{
                //    Grd1.SortObject.Clear();
                //    Grd1.SortObject.Add(4, iGSortOrder.Descending, iGSortType.ByValue);
                //    Grd1.Sort();
                //}
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProductionOrder(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmProductionOrder(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Production Order");
        }

        private void ChkWorkCenterDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work Center");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtWorkCenterDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
