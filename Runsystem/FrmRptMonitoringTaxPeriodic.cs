﻿#region update
/*
    27/08.2021 [RDH/AMKA] Membuat menu baru reporting tax periodic
    30/08/2021 [IBL/AMKA] Feedback : perhitungan ditambah dengan OB
    14/10/2021 [RDA/AMKA] Menambah kolom PPH 24 dan PPH 26
    10/11/2021 [MYA/AMKA] Membuat filter profit center di header reporting Monitoring Tax Periodic terfilter berdasarkan tab profit center group
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DXE = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonitoringTaxPeriodic : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private List<String> mlProfitCenter;
        private bool mIsAllProfitCenterSelected = false,
            mIsFilterByProfitCenter = false;

        #endregion

        #region Constructor

        public FrmRptMonitoringTaxPeriodic(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnPrint.Visible = false;
                mlProfitCenter = new List<String>();
                var CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueMth(LueMth1);
                Sm.SetLue(LueMth1, CurrentDateTime.Substring(4, 2));
                Sl.SetLueMth(LueMth2);
                Sm.SetLue(LueMth2, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                GetParameter();
                SetGrd();
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterByProfitCenter = Sm.GetParameterBoo("IsFilterByProfitCenter");
        }

        private string GetSQL(string Alias, string subSQL1, string subSQL2, bool IsNeedOpeningBalance)
        {
            var SQL = new StringBuilder();

            if (IsNeedOpeningBalance)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select Mth, Sum(Amt) As Amt ");
                SQL.AppendLine("    From (");
                SQL.AppendLine("        Select Date_Format(T1.DocDt, '%m') As Mth,");
                SQL.AppendLine("        Sum(Case When T4.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End) As Amt ");
                SQL.AppendLine("        From TblJournalHdr T1, TblJournalDtl T2, TblCostCenter T3, TblCOA T4 ");
                SQL.AppendLine("        Where T1.DocNo=T2.DocNo And T1.CCCode Is Not Null ");
                SQL.AppendLine("        And T1.CCCode=T3.CCCode And T3.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("        And Left(T1.DocDt, 6) Between @YrMth1 And @YrMth2 ");
                SQL.AppendLine("        And T2.AcNo=T4.AcNo ");
                SQL.AppendLine("        And T4.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine(subSQL1);
                SQL.AppendLine(subSQL2);
                SQL.AppendLine("        Group By Substr(T1.DocDt, 5,2) ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Date_Format(T1.DocDt, '%m') As Mth, T2.Amt As Amt ");
                SQL.AppendLine("        From TblCOAOpeningBalanceHdr T1, TblCOAOpeningBalanceDtl T2, TblCOA T3 ");
                SQL.AppendLine("        Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        And T1.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And Left(T1.DocDt, 6) Between @YrMth1 And @YrMth2 ");
                SQL.AppendLine("        And T2.AcNo=T3.AcNo ");
                SQL.AppendLine("        And T3.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine(subSQL1);
                SQL.AppendLine(subSQL2.Replace("T3.", "T1."));
                SQL.AppendLine("        Group By Substr(T1.DocDt, 5, 2) ");
                SQL.AppendLine("    ) T Group By T.Mth ");
                SQL.AppendLine(") " + Alias + " On A.Mth=" + Alias + ".Mth ");
            }
            else
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select Date_Format(T1.DocDt, '%m') As Mth,");
                SQL.AppendLine("    Sum(Case When T4.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End) As Amt ");
                SQL.AppendLine("    From TblJournalHdr T1, TblJournalDtl T2, TblCostCenter T3, TblCOA T4 ");
                SQL.AppendLine("    Where T1.DocNo=T2.DocNo And T1.CCCode Is Not Null ");
                SQL.AppendLine("    And T1.CCCode=T3.CCCode And T3.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("    And T2.AcNo=T4.AcNo ");
                SQL.AppendLine("    And T4.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("    And Left(T1.DocDt, 6) Between @YrMth1 And @YrMth2 ");
                SQL.AppendLine(subSQL1);
                SQL.AppendLine(subSQL2);
                SQL.AppendLine("    Group By Substr(T1.DocDt, 5,2) ");
                SQL.AppendLine(") " + Alias + " On A.Mth=" + Alias + ".Mth ");
            }

            return SQL.ToString();
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();

            if (!mIsAllProfitCenterSelected)
            {
                var Filter = string.Empty;
                int i = 0;
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (T3.ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                    i++;
                }
                if (Filter.Length == 0)
                    SQL2.AppendLine("    And 1=0 ");
                else
                    SQL2.AppendLine("    And (" + Filter + ") ");
            }
            else
            {
                if (ChkProfitCenterCode.Checked) SQL2.AppendLine("    And Find_In_Set(T3.ProfitCenterCode, @ProfitCenterCode) ");
            }

            SQL.AppendLine("Select A.MthName, ");
            SQL.AppendLine("IfNull(B.Amt, 0.00) As Value1, ");
            SQL.AppendLine("IfNull(C.Amt, 0.00) As Value2, ");
            SQL.AppendLine("IfNull(D.Amt, 0.00) As Value3, ");
            SQL.AppendLine("IfNull(E.Amt, 0.00) As Value4, ");
            SQL.AppendLine("IfNull(F.Amt, 0.00) As Value5, ");
            SQL.AppendLine("IfNull(G.Amt, 0.00) As Value6, ");
            SQL.AppendLine("IfNull(H.Amt, 0.00) As Value7, ");
            SQL.AppendLine("IfNull(I.Amt, 0.00) As Value8, ");
            SQL.AppendLine("IfNull(J.Amt, 0.00) As Value9, ");
            SQL.AppendLine("IfNull(K.Amt, 0.00) As Value10, ");
            SQL.AppendLine("IfNull(L.Amt, 0.00) As Value11 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select '01' As Mth, 'January' As MthName Union All ");
            SQL.AppendLine("    Select '02' As Mth, 'February' As MthName Union All ");
            SQL.AppendLine("    Select '03' As Mth, 'March' As MthName Union All ");
            SQL.AppendLine("    Select '04' As Mth, 'April' As MthName Union All ");
            SQL.AppendLine("    Select '05' As Mth, 'May' As MthName Union All ");
            SQL.AppendLine("    Select '06' As Mth, 'June' As MthName Union All ");
            SQL.AppendLine("    Select '07' As Mth, 'July' As MthName Union All ");
            SQL.AppendLine("    Select '08' As Mth, 'August' As MthName Union All ");
            SQL.AppendLine("    Select '09' As Mth, 'September' As MthName Union All ");
            SQL.AppendLine("    Select '10' As Mth, 'October' As MthName Union All ");
            SQL.AppendLine("    Select '11' As Mth, 'November' As MthName Union All ");
            SQL.AppendLine("    Select '12' As Mth, 'December' As MthName ");
            SQL.AppendLine(")A ");
            SQL.AppendLine(GetSQL("B", " And T2.AcNo Like '2.1.4.2%' ", SQL2.ToString(), true));
            SQL.AppendLine(GetSQL("C", " And T2.AcNo Like '2.1.4.3%' ", SQL2.ToString(), true));
            SQL.AppendLine(GetSQL("D", " And T2.AcNo Like '2.1.4.1.3%' ", SQL2.ToString(), true));
            SQL.AppendLine(GetSQL("E", " And T2.AcNo Like '2.1.4.1.1%' ", SQL2.ToString(), true));
            SQL.AppendLine(GetSQL("F", " And T2.AcNo Like '2.1.4.1.5%' ", SQL2.ToString(), true));
            SQL.AppendLine(GetSQL("G", " And T2.AcNo Like '2.1.4.1.2%' ", SQL2.ToString(), true));
            SQL.AppendLine(GetSQL("H", " And T2.AcNo Like '2.1.4.1.4%' ", SQL2.ToString(), true));
            SQL.AppendLine(GetSQL("I", " And T2.AcNo Like '2.1.4.4%' ", SQL2.ToString(), true));
            SQL.AppendLine(GetSQL("J", " And T2.AcNo Like '2.1.4.5%' ", SQL2.ToString(), true));
            SQL.AppendLine(GetSQL("K", " And T2.AcNo Like '2.1.4.1.6%' ", SQL2.ToString(), true));
            SQL.AppendLine(GetSQL("L", " And T2.AcNo Like '2.1.4.1.7%' ", SQL2.ToString(), true));
            SQL.AppendLine("Where A.Mth Between @Mth1 And @Mth2 ");

            return SQL.ToString();
        }


        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (IsProfitCenterInvalid() ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth1, "Start Month") ||
                Sm.IsLueEmpty(LueMth2, "End Month") ||
                IsFilterMonthInvalid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cm = new MySqlCommand();

            string YrMth1 = Sm.GetLue(LueYr) + Sm.GetLue(LueMth1),
                   YrMth2 = Sm.GetLue(LueYr) + Sm.GetLue(LueMth2);

            Sm.CmParam<String>(ref cm, "@YrMth1", YrMth1);
            Sm.CmParam<String>(ref cm, "@YrMth2", YrMth2);
            Sm.CmParam<String>(ref cm, "@Mth1", Sm.GetLue(LueMth1));
            Sm.CmParam<String>(ref cm, "@Mth2", Sm.GetLue(LueMth2));
            try
            {
                SetProfitCenter();

                if (!mIsAllProfitCenterSelected)
                {
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                        i++;
                    }
                }
                else
                {
                    if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                }

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(),
                    new string[]
                    {
                        //0
                        "MthName",

                        //1-5
                        "Value1", "Value2", "Value3", "Value4", "Value5", 

                        //6-10
                        "Value6", "Value7", "Value8", "Value9", "Value10", 
                        
                        //11
                        "Value11"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                        Grd.Cells[Row, 11].Value = Sm.DrDec(dr, c[8]) + Sm.DrDec(dr, c[9]);
                        Grd.Cells[Row, 12].Value = Sm.DrDec(dr, c[1]) + Sm.DrDec(dr, c[2]) + Sm.DrDec(dr, c[3]) +
                                                   Sm.DrDec(dr, c[4]) + Sm.DrDec(dr, c[5]) + Sm.DrDec(dr, c[6]) +
                                                   Sm.DrDec(dr, c[7]) + Sm.DrDec(dr, c[8]) + Sm.DrDec(dr, c[9]) +
                                                   Sm.DrDec(dr, c[10]) + Sm.DrDec(dr, c[11]);
                    }, true, false, false, false
                );
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;

            SetGrdHdr(ref Grd1, 0, 0, "No", 2, 50);
            SetGrdHdr(ref Grd1, 0, 1, "Month", 2, 100);
            SetGrdHdr2(ref Grd1, 1, 2, "PPN", 2, 240, new string[] { "Output Tax", "WAPU" });
            SetGrdHdr(ref Grd1, 0, 4, "PPH Badan", 2, 120);
            SetGrdHdr2(ref Grd1, 1, 5, "PPH", 6, 540, new string[] { "PPH 21", "PPH 22", "PPH 23", "PPH 24", "PPH 26", "Final" });
            SetGrdHdr(ref Grd1, 0, 11, "STP SKPKB", 2, 120);
            SetGrdHdr(ref Grd1, 0, 12, "Amount Of Arrears", 2, 120);

            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 }, 0); 
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
        }

        override protected void HideInfoInGrd()
        {

        }

        #endregion

        #region Additional Method

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T1.ProfitCenterName As Col, T1.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T1 ");
            if (mIsFilterByProfitCenter)
            {
                SQL.AppendLine("WHERE Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
                SQL.AppendLine("        Where ProfitCenterCode=T1.ProfitCenterCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Multi profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }
            return false;
        }

        private bool IsFilterMonthInvalid()
        {
            if (Convert.ToInt32(Sm.GetLue(LueMth1)) > Convert.ToInt32(Sm.GetLue(LueMth2)))
            {
                Sm.StdMsg(mMsgType.Warning, "End month is earlier than start month.");
                return true;
            }
            return false;
        }

        private void SetGrdHdr(ref iGrid Grd, int row, int col, string Title, int SpanRows, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = SpanRows;
            Grd.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols, int Width, string[] Subtitle)
        {
            int mCount = Subtitle.Count();
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = Width;

            for (int i = 0; i < mCount; i++)
                SetGrdHdr(ref Grd1, 0, col+i, Subtitle[i], 1, Width/mCount);
        }

        private void ParPrint()
        {
            var l = new List<RptMonitoringTaxPeriodicHdr>();
            var l2 = new List<RptMonitoringTaxPeriodicDtl>();
            string[] TableName = { "RptMonitoringTaxPeriodicHdr", "RptMonitoringTaxPeriodicDtl" };
            List<IList> myLists = new List<IList>();

            #region Header

            l.Add(new RptMonitoringTaxPeriodicHdr()
            {
                CompanyName = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'ReportTitle1'"),
                Year = Sm.GetLue(LueYr),
            });
            myLists.Add(l);

            #endregion

            #region Detail

            for (int row = 0; row < (Grd1.Rows.Count-1); row++)
            {
                l2.Add(new RptMonitoringTaxPeriodicDtl()
                {
                    Nomor = row + 1,
                    Month = Sm.GetGrdStr(Grd1, row, 1),
                    OutputTax = Sm.GetGrdDec(Grd1, row, 2),
                    WAPU = Sm.GetGrdDec(Grd1, row, 3),
                    PPH = Sm.GetGrdDec(Grd1, row, 4),
                    PPH21 = Sm.GetGrdDec(Grd1, row, 5),
                    PPH22 = Sm.GetGrdDec(Grd1, row, 6),
                    PPH23 = Sm.GetGrdDec(Grd1, row, 7),
                    Final = Sm.GetGrdDec(Grd1, row, 8),
                    STP = Sm.GetGrdDec(Grd1, row, 9),
                    ArrearsAmt = Sm.GetGrdDec(Grd1, row, 10),
                });
            }
            myLists.Add(l2);

            #endregion

            Sm.PrintReport("MonitoringTaxPeriodic", myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit center");
        }

        private void LueMth1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueMth2).Length == 0) Sm.SetLue(LueMth2, Sm.GetLue(LueMth1));
        }

        private void BtnPrint2_Click(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #endregion

        #region Class

        private class RptMonitoringTaxPeriodicHdr
        {
            public string CompanyName { get; set; }
            public string Year { get; set; }
        }

        private class RptMonitoringTaxPeriodicDtl
        {
            public int Nomor { get; set; }
            public string Month { get; set; }
            public decimal OutputTax { get; set; }
            public decimal WAPU { get; set; }
            public decimal PPH { get; set; }
            public decimal PPH21 { get; set; }
            public decimal PPH22 { get; set; }
            public decimal PPH23 { get; set; }
            public decimal Final { get; set; }
            public decimal STP { get; set; }
            public decimal ArrearsAmt { get; set; }
        }

        #endregion
    }
}
