﻿#region Update
/*
    04/12/2017 (HAR) tambah Local Document filter
    09/05/2017 [WED] tambah kolom Term Of Payment
    14/11/2017 [WED] pass nilai VdCode
    29/05/2018 [TKG] tambah status PO.
    20/07/2018 [TKG] Berdasarkan IsPartialInvoicedPOUsedInAPDownpayment, PO yg sebagian sudah diproses menjadi invoice masih bisa dipilih.
    11/01/2022 [RDA/IMS] Penyesuaian data untuk PO for Service yang muncul berdasarkan param IsUsePOForServiceDocNo
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAPDownpayment2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmAPDownpayment2 mFrmParent;
        private bool mIsPartialInvoicedPOUsedInAPDownpayment = false;
        #endregion

        #region Constructor

        public FrmAPDownpayment2Dlg(FrmAPDownpayment2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                Sl.SetLueVdCode(ref LueVdCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsPartialInvoicedPOUsedInAPDownpayment = Sm.GetParameterBoo("IsPartialInvoicedPOUsedInAPDownpayment");
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();

           
            if (mFrmParent.mIsUsePOForServiceDocNo)
                SQL.AppendLine("select case when F.MaterialRequestServiceDocNo IS NULL then A.DocNo ELSE F.POSDocNo end AS DocNo, ");
            else
                SQL.AppendLine("Select A.DocNo, ");
            SQL.AppendLine("A.DocNo as PODocNo, A.LocalDocNo, A.DocDt, A.VdCode, E.Vdname, F.PtName, A.CurCode, ");
            SQL.AppendLine("(Select Count(DocNo) As NoOfDownpayment From TblAPDownpayment  Where CancelInd='N' And PODocNo=A.DocNo ) As NoOfDownpayment, ");
            SQL.AppendLine("F.Total+ if(length(A.taxCode1>0), (F.Total*B.Taxrate*0.01), 0)+ ");
            SQL.AppendLine("if(length(A.taxCode2>0), (F.Total*C.Taxrate*0.01), 0)+ ");
            SQL.AppendLine("if(length(A.taxCode3>0), (F.Total*D.Taxrate*0.01), 0)- ifnull(A.DiscountAmt, 0)+ifnull(A.CustomsTaxAmt, 0) As Amt, ");
            SQL.AppendLine("A.Remark ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Left Join TblTax B On A.TaxCode1 = B.taxCode ");
            SQL.AppendLine("Left Join TblTax C On A.TaxCode2 = C.taxCode ");
            SQL.AppendLine("Left Join TblTax D On A.TaxCode3 = D.taxCode ");
            SQL.AppendLine("Inner Join TblVendor E On A.VdCode = E.VdCode ");
            SQL.AppendLine("Inner join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select B.DocNo, F.DocNo as POSDocNo, F.MaterialRequestServiceDocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct N.PtName Separator ', ') As PtName, ");
            SQL.AppendLine("    SUM((H.Uprice * (B.Qty-ifnull(M.Qty, 0))) - (H.Uprice*(B.Qty-ifnull(M.Qty, 0))*B.Discount/100) - B.DiscountAmt + B.Roundingvalue) As Total ");
            SQL.AppendLine("    From TblPODtl B  ");
            SQL.AppendLine("    Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo  ");
            SQL.AppendLine("    Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo  ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And (E.DeptCode Is Null Or (E.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=E.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo  ");
            SQL.AppendLine("    Inner Join TblQtHdr G On D.QtDocNo=G.DocNo  ");
            SQL.AppendLine("    Inner Join TblQtDtl H On D.QtDocNo=H.DocNo And D.QtDNo=H.DNo  ");
            SQL.AppendLine("    Left Join TblPOQtyCancel M On B.Docno = M.PODocno And B.DNo = M.PODno And M.CancelInd='N'  ");
            SQL.AppendLine("    Left Join TblPaymentTerm N On G.PtCode = N.PtCode ");
            SQL.AppendLine("    Where B.CancelInd='N'   ");
            SQL.AppendLine("    Group By B.DocNo ");
            SQL.AppendLine(") F On A.Docno = F.DocNo ");
            SQL.AppendLine("Where A.Amt>0 ");

            if (mIsPartialInvoicedPOUsedInAPDownpayment)
            {
                SQL.AppendLine("And A.DocNo In (");
                SQL.AppendLine("    Select Distinct X2.DocNo ");
                SQL.AppendLine("    From TblPOHdr X1 ");
                SQL.AppendLine("    Inner Join TblPODtl X2 On X1.DocNo=X2.DocNo And X2.CancelInd='N' ");
                SQL.AppendLine("    Where X1.Status='A' ");
                SQL.AppendLine("    And X1.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("    And Not Exists( ");
	            SQL.AppendLine("        Select 1 ");
	            SQL.AppendLine("        From TblPOHdr T1 ");
	            SQL.AppendLine("        Inner Join TblPODtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.ProcessInd='F' ");
	            SQL.AppendLine("        Inner Join TblRecvVdDtl T3 On T2.DocNo=T3.PODocNo And T2.DNo=T3.PODNo And T3.CancelInd='N' And T3.Status='A' ");
	            SQL.AppendLine("        Inner Join TblRecvVdHdr T4 On T3.DocNo=T4.DocNo  ");
	            SQL.AppendLine("        Inner Join TblPurchaseInvoiceDtl T5 On T3.DocNo=T5.RecvVdDocNo And T3.DNo=T5.RecvVdDNo  ");
	            SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T6 On T5.DocNo=T6.DocNo And T6.CancelInd='N'  ");
	            SQL.AppendLine("        Where T1.Status='A' ");
	            SQL.AppendLine("        And T2.DocNo=X2.DocNo ");
                SQL.AppendLine("        And T2.DNo=X2.DNo ");
                SQL.AppendLine("        And T1.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("    )) ");
            }
            else
            {
                SQL.AppendLine("And A.DocNo Not In (");
                SQL.AppendLine("    Select Distinct T3.PODocNo ");
                SQL.AppendLine("    From TblPurchaseInvoiceHdr T1 ");
                SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblRecvVdDtl T3 On T2.RecvVdDocNo=T3.DocNo And T2.RecvVdDNo=T3.DNo ");
                SQL.AppendLine("    Inner Join TblPOHdr T4 On T3.PODocNo=T4.DocNo ");
                SQL.AppendLine("        And T4.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("        And T4.Status='A' ");
                SQL.AppendLine("    Where T1.CancelInd = 'N' ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And A.Status='A' ");
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "PO#", 
                    "",
                    "Local Document",
                    "Date",
                    "Vendor",
                    
                    //6-10
                    "Term of Payment",
                    "Currency", 
                    "Amount",
                    "Number of"+Environment.NewLine+"Downpayment",
                    "VdCode",

                    //11-12
                    "Remark", "PODocNo"
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 10, 12 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] {"A.DocNo", "A.LocalDocNo"});
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SetSQL() + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "LocalDocNo", "DocDt", "VdName", "CurCode", "Amt", 
                            
                            //6-10
                            "NoOfDownpayment" , "PtName", "VdCode", "Remark", "PODocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtPODocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtPOLocalDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                mFrmParent.TxtLocalDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                mFrmParent.TxtVdCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                mFrmParent.TxtPTCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                mFrmParent.TxtPOCurCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                Sm.SetLue(mFrmParent.LueCurCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7));
                mFrmParent.TxtPOAmt.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 8), 0);
                mFrmParent.TxtNoOfDownpayment.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 9), 2);
                mFrmParent.mVdCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 10);
                mFrmParent.MeePORemark.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 11);
                mFrmParent.mPODocNo = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 12);
                mFrmParent.ComputeOtherAmt();
                mFrmParent.SetForm(mFrmParent.mVdCode);
                this.Close();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPO(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPO(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        #endregion

        #region Grid Event

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion
    }
}
