﻿#region
/*
    18/07/2019 [DITA] fitur baru untuk find transferbinreq find
    10/09/2019 [TKG] tambahan beberapa informasi
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmIMMTransferBinFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmIMMTransferBin mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmIMMTransferBinFind(FrmIMMTransferBin FrmParent) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetSQL();
                SetGrd();
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select Concat(A.DocNo, A.DocSeqNo) As BinTransfer, A.DocNo, A.DocSeqNo, A.DocDt, ");
            SQL.AppendLine("    Concat(B.TransferBinReqDocNo, B.TransferBinReqDocSeqNo) As TransferBinReq, ");
            SQL.AppendLine("    E.WhsName As WhsNameFrom, B.BinFrom, F.LocName As LocNameFrom, ");
            SQL.AppendLine("    G.WhsName As WhsNameTo, B.BinTo, H.LocName As LocNameTo, ");
            SQL.AppendLine("    B.Source, C.ProdCode, D.ProdDesc, D.Color, ");
            SQL.AppendLine("    A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("    From TblIMMTransferBinHdr A ");
            SQL.AppendLine("    Inner Join TblIMMTransferBinDtl B On A.DocNo=B.DocNo And A.DocSeqNo=B.DocSeqNo ");
            SQL.AppendLine("    Inner Join TblIMMStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("    Inner Join TblIMMProduct D On C.ProdCode=D.ProdCode ");
            SQL.AppendLine("    Inner Join TblWarehouse E On B.WhsCodeFrom=E.WhsCode ");
            SQL.AppendLine("    Left Join TblIMMLocation F On B.LocCodeFrom=F.LocCode ");
            SQL.AppendLine("    Inner Join TblWarehouse G On B.WhsCodeTo=G.WhsCode ");
            SQL.AppendLine("    Left Join TblIMMLocation H On B.LocCodeTo=H.LocCode ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=A.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") T ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "DocNo",
                        "DocSeqNo",
                        "Date",
                        "Request#",

                        //6-10
                        "Warehouse (From)",
                        "Bin (From)",
                        "Location (From)",
                        "Warehouse (To)",
                        "Bin (To)",

                        //11-15
                        "Location (To)",
                        "Source",
                        "Product Code",
                        "Product Description",
                        "Color",

                        //16-20
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 

                        //21
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 0, 0, 100, 150, 
                        
                        //6-10
                        180, 150, 150, 180, 150,  
                        
                        //11-15
                        150, 180, 100, 200, 100,

                        //16-20
                        130, 130, 130, 130, 130,

                        //21
                        130
                    }
                );

            Sm.GrdFormatDate(Grd1, new int[] { 4, 17, 20 });
            Sm.GrdFormatTime(Grd1, new int[] { 18, 21 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 16, 17, 18, 19, 20, 21 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 20, 21 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 1=1 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.BinTransfer", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "T.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtSource.Text, "T.Source", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProdCode.Text, new string[] { "T.ProdCode", "T.ProdDesc" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "BinTransfer", 

                            //1-5
                            "DocNo", "DocSeqNo", "DocDt", "TransferBinReq", "WhsNameFrom", 

                            //6-10
                            "BinFrom", "LocNameFrom", "WhsNameTo", "BinTo", "LocNameTo", 

                            //11-15
                            "Source", "ProdCode", "ProdDesc", "Color", "CreateBy", 
                            
                            //16-18
                            "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 18);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void TxtSource_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSource_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Source");
        }

        private void TxtProdCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Product");
        }

        #endregion

        #endregion
    }
}
