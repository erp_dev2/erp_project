﻿#region Update
/*
    28/10/2019 [WED/IMS] new apps
    19/02/2020 [WED/IMS] settle by bobot
    15/06/2021 [VIN/IMS] tambah Update Percentage
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectDelivery2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        private string mProjectDeliveryDocType = "2";
        private bool mIsAutoJournalActived = false, mIsWBSBobotColumnAutoCompute = false;
        internal bool mIsFilterBySite = false;
        internal FrmProjectDelivery2Find FrmFind;

        #endregion

        #region Constructor

        public FrmProjectDelivery2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmProjectDelivery2");

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                LblAmt.Visible = TxtAmt.Visible = false;
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "PRJIDNo",
                    "Stage",
                    "Task",
                    "Bobot (%)",
                    "Remark",
                    
                    //6-10
                    "SO Contract",
                    "SO Revision" + Environment.NewLine + "(Existing)",
                    "SO Revision" + Environment.NewLine + "(Latest)",
                    "Amount",
                    "Outstanding Amount",

                    //11-15
                    "Estimated Amount",
                    "Bobot",
                    "Outstanding",
                    "Item's Code",
                    "Item's Name",

                    //16
                    "Local Code"
                },
                new int[] 
                {
                    //0
                    20,
                    //1-5
                    0, 180, 180, 120, 200, 
                    //6-10
                    150, 150, 150, 150, 150,
                    //11-15
                    150, 150, 150, 150, 200,
                    //16
                    150
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 9, 10, 11, 12, 13 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 6, 7, 8, 10, 11, 13, 14, 15, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 9, 10, 11, 14 });
            Grd1.Cols[13].Move(5);
            Grd1.Cols[12].Move(5);
            Grd1.Cols[16].Move(5);
            Grd1.Cols[15].Move(5);
            Grd1.Cols[14].Move(5);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 14 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, MeeCancelReason, DteDocDt, MeeRemark, TxtStatus, TxtAmt,
                        TxtPRJIDocNo
                    }, true);
                    Grd1.ReadOnly = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnPRJIDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    BtnPRJIDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, MeeCancelReason, DteDocDt, MeeRemark, TxtStatus,
                 TxtPRJIDocNo
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt, TxtBobot }, 0);
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 9, 10, 11, 12, 13 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProjectDelivery2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
                ChkCancelInd.Checked = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if(BtnSave.Enabled)
            {
                ComputeBobot();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                ComputeAmt();
                ComputeBobot();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false) && e.ColIndex == 0)
                {
                    Sm.FormShowDialog(new FrmProjectDelivery2Dlg2(this, TxtPRJIDocNo.Text));
                }
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false) && e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    Sm.FormShowDialog(new FrmProjectDelivery2Dlg2(this, TxtPRJIDocNo.Text));
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            UpdateLatestSOContractRevision();
            UpdateOutstandingAmt();
            UpdateOutstandingBobot();
            RecomputeBobot();
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProjectDelivery", "TblProjectDeliveryHdr");
            decimal mTotalBobot = 0m;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveProjectDeliveryHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SaveProjectDeliveryDtl(DocNo, Row));
                    cml.Add(SettledTask(
                        TxtPRJIDocNo.Text, 
                        Sm.GetGrdStr(Grd1, Row, 1), 
                        Sm.GetGrdStr(Grd1, Row, 5),
                        "I",
                        Sm.GetGrdDec(Grd1, Row, 9),
                        Sm.GetGrdDec(Grd1, Row, 12)
                        ));
                    mTotalBobot += Sm.GetGrdDec(Grd1, Row, 4);
                }
            }

            cml.Add(UpdateAchievement(TxtPRJIDocNo.Text, mTotalBobot, "I"));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {

            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false) ||
                IsProjectAlreadyCancelled() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsAmtNotValid() ||
                IsBobotNotValid();
        }

        private bool IsBobotNotValid()
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if(Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (Sm.GetGrdDec(Grd1, i, 12) > Sm.GetGrdDec(Grd1, i, 13))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Bobot should not be bigger than Outstanding.");
                        Sm.FocusGrd(Grd1, i, 12);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsAmtNotValid()
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (Sm.GetGrdDec(Grd1, i, 9) > Sm.GetGrdDec(Grd1, i, 10))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Amount should not be bigger than Outstanding Amount.");
                        Sm.FocusGrd(Grd1, i, 9);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsSOContractRevisionOutdated()
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, i, 7) != Sm.GetGrdStr(Grd1, i, 8))
                    {
                        Sm.StdMsg(mMsgType.Warning, "This document needs revision, due to outdated SO Contract Revision.");
                        Sm.FocusGrd(Grd1, i, 1);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsProjectAlreadyCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblProjectImplementationHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And (CancelInd = 'Y' Or Status = 'C'); ");

            if (Sm.IsDataExist(SQL.ToString(), TxtPRJIDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This project is already cancelled.");
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string mPRJIDNo = string.Empty, mStageName = string.Empty, mTaskName = string.Empty, mItName = string.Empty;
            bool mIsSettled = false;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                //if (Sm.IsGrdValueEmpty(Grd1, Row, 9, true, "Amount is zero.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 12, true, "Bobot is zero.")) return true;
                if (mPRJIDNo.Length > 0) mPRJIDNo += ",";
                mPRJIDNo += Sm.GetGrdStr(Grd1, Row, 1);
            }

            if (mPRJIDNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select B.StageName, C.TaskName, D.ItName ");
                SQL.AppendLine("From TblProjectImplementationDtl A ");
                SQL.AppendLine("Inner Join TblProjectStage B On A.StageCode = B.StageCode ");
                SQL.AppendLine("Inner Join TblProjectTask C On A.TaskCode = C.TaskCode ");
                SQL.AppendLine("Left Join TblItem D On A.ItCode = D.ItCode ");
                SQL.AppendLine("Where A.DocNo = @DocNo ");
                SQL.AppendLine("And Find_In_Set(A.DNo, @DNo) ");
                SQL.AppendLine("And A.SettledInd = 'Y' ");
                SQL.AppendLine("Limit 1; ");

                Sm.CmParam<String>(ref cm, "@DocNo", TxtPRJIDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DNo", mPRJIDNo);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "StageName", "TaskName", "ItName" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            mIsSettled = true;
                            mStageName = Sm.DrStr(dr, c[0]);
                            mTaskName = Sm.DrStr(dr, c[1]);
                            mItName = Sm.DrStr(dr, c[2]);
                        }
                    }
                    dr.Close();
                }

                if (mIsSettled)
                {
                    var mMsg = new StringBuilder();

                    mMsg.AppendLine("Stage : " + mStageName);
                    mMsg.AppendLine("Task : " + mTaskName);
                    mMsg.AppendLine("Item : " + mItName);
                    mMsg.AppendLine("This data is already settled. ");

                    Sm.StdMsg(mMsgType.Warning, mMsg.ToString());
                    return true;
                }
            }

            return false;
        }

        private MySqlCommand SaveProjectDeliveryHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProjectDeliveryHdr(DocNo, DocDt, DocType, Status, CancelInd, PRJIDocNo, Remark, Amt, Bobot, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @DocType, 'O', 'N', @PRJIDocNo, @Remark, @Amt, @Bobot, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblProjectDeliveryHdr Set ");
            SQL.AppendLine("    Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And DocType = 'ProjectDelivery' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", mProjectDeliveryDocType);
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", TxtPRJIDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Bobot", decimal.Parse(TxtBobot.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectDeliveryDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProjectDeliveryDtl(DocNo, DNo, PRJIDocNo, PRJIDNo, Amt, Bobot, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @PRJIDocNo, @PRJIDNo, @Amt, @Bobot, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", TxtPRJIDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PRJIDNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Bobot", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateAchievement(string DocNo, decimal Bobot, string StateInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    Select DocNo, Sum(If((SettledBobot >= Bobot), 1, (SettledBobot / Bobot)) * BobotPercentage) BobotPercentage ");
            SQL.AppendLine("    From TblProjectImplementationDtl ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And SettledBobot != 0 ");
            SQL.AppendLine("    Group By DocNo ");
            SQL.AppendLine(") B On A.DocNO = B.DocNo ");
            SQL.AppendLine("Set A.Achievement = B.BobotPercentage, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And A.CancelInd = 'N' And A.Status = 'A'; ");

            if (StateInd == "I")
            {
                //SQL.AppendLine("Update TblProjectImplementationHdr ");
                //SQL.AppendLine("    Set Achievement = Achievement + @Bobot, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                //SQL.AppendLine("Where DocNo = @DocNo ");
                //SQL.AppendLine("And CancelInd = 'N' And Status = 'A'; ");

                SQL.AppendLine("Update TblProjectImplementationHdr ");
                SQL.AppendLine("    Set Achievement = 100 ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And Achievement > 99.9; ");
            }
            else
            {
                //SQL.AppendLine("Update TblProjectImplementationHdr ");
                //SQL.AppendLine("    Set Achievement = Achievement - @Bobot, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                //SQL.AppendLine("Where DocNo = @DocNo ");
                //SQL.AppendLine("And CancelInd = 'N' And Status = 'A'; ");

                SQL.AppendLine("Update TblProjectImplementationHdr ");
                SQL.AppendLine("    Set Achievement = 0 ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And Achievement < 0.00; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@Bobot", Bobot);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SettledTask(string DocNo, string DNo, string Remark, string StateInd, decimal Amt, decimal Bobot)
        {
            var SQL = new StringBuilder();

            if (StateInd == "I")
            {
                SQL.AppendLine("Update TblProjectImplementationDtl Set ");
                SQL.AppendLine("    SettledAmt = SettledAmt + @Amt, ");
                SQL.AppendLine("    SettledBobot = SettledBobot + @Bobot, ");
                SQL.AppendLine("    Remark=@Remark, ");
                SQL.AppendLine("    LastUpBy=@UserCode, ");
                SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And DNo = @DNo And SettledInd='N' ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Update TblProjectImplementationDtl Set ");
                SQL.AppendLine("    SettledAmt = EstimatedAmt ");
                SQL.AppendLine("Where DocNo = @DocNo And DNo = @DNo And (SettledAmt > EstimatedAmt Or ((EstimatedAmt - SettledAmt) <= 1) ) ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Update TblProjectImplementationDtl Set ");
                SQL.AppendLine("    SettledInd = 'Y', ");
                SQL.AppendLine("    SettleDt = Replace(CurDate(), '-', '') ");
                SQL.AppendLine("Where DocNo = @DocNo And DNo = @DNo And SettledAmt = EstimatedAmt And EstimatedAmt != 0 ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Update TblProjectImplementationDtl Set ");
                SQL.AppendLine("    SettledInd = 'Y', ");
                SQL.AppendLine("    SettleDt = Replace(CurDate(), '-', '') ");
                SQL.AppendLine("Where DocNo = @DocNo And DNo = @DNo And SettledBobot >= Bobot And SettledBobot != 0 ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Update TblProjectImplementationDtl Set ");
                SQL.AppendLine("    BobotPercentage = (Bobot - (Bobot - SettledBobot))/Bobot * 100 ");
                SQL.AppendLine("Where DocNo = @DocNo And DNo = @DNo ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                SQL.AppendLine("; ");
            }
            else
            {
                SQL.AppendLine("Update TblProjectImplementationDtl Set ");
                SQL.AppendLine("    SettledAmt = SettledAmt - @Amt, ");
                SQL.AppendLine("    SettledBobot = SettledBobot - @Bobot, ");
                SQL.AppendLine("    SettledInd = 'N', ");
                SQL.AppendLine("    SettleDt = null, ");
                SQL.AppendLine("    Remark = null, ");
                SQL.AppendLine("    LastUpBy=@UserCode, ");
                SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo = @DocNo And DNo = @DNo ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Update TblProjectImplementationDtl Set ");
                SQL.AppendLine("    SettledAmt = 0.00 ");
                SQL.AppendLine("Where DocNo = @DocNo And DNo = @DNo And SettledAmt <= 1 ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Update TblProjectImplementationDtl Set ");
                SQL.AppendLine("    SettledBobot = 0.00 ");
                SQL.AppendLine("Where DocNo = @DocNo And DNo = @DNo And SettledBobot <= 1 ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Update TblProjectImplementationDtl Set ");
                SQL.AppendLine("    BobotPercentage = (Bobot - (Bobot - SettledBobot))/Bobot * 100 ");
                SQL.AppendLine("Where DocNo = @DocNo And DNo = @DNo ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                SQL.AppendLine("; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@Remark", Remark);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Amt);
            Sm.CmParam<Decimal>(ref cm, "@Bobot", Bobot);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            UpdateOutstandingAmt();
            RecomputeBobot();
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            decimal mTotalBobot = 0m;

            cml.Add(EditProjectDeliveryHdr());

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    mTotalBobot += Sm.GetGrdDec(Grd1, Row, 4);
                    cml.Add(SettledTask(
                        TxtPRJIDocNo.Text,
                        Sm.GetGrdStr(Grd1, Row, 1),
                        Sm.GetGrdStr(Grd1, Row, 5),
                        "E",
                        Sm.GetGrdDec(Grd1, Row, 9),
                        Sm.GetGrdDec(Grd1, Row, 12)
                        ));
                }
            }

            cml.Add(UpdateAchievement(TxtPRJIDocNo.Text, mTotalBobot, "E"));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false) ||
                IsDataNotCancelled() ||
                IsDataAlreadyCancelled() ||
                IsProjectAlreadyInvoiced() ||
                IsNotTheLastData();
        }

        private bool IsNotTheLastData()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblProjectDeliveryHdr ");
            SQL.AppendLine("Where CancelInd = 'N' ");
            SQL.AppendLine("And Status In ('O', 'A') ");
            SQL.AppendLine("And PRJIDocNo = @Param ");
            SQL.AppendLine("Order By CreateDt Desc ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtPRJIDocNo.Text))
            {
                if (Sm.GetValue(SQL.ToString(), TxtPRJIDocNo.Text) != TxtDocNo.Text)
                {
                    Sm.StdMsg(mMsgType.Warning, "You should cancel the last data #" + Sm.GetValue(SQL.ToString(), TxtPRJIDocNo.Text));
                    return true;
                }
            }

            return false;
        }

        private bool IsProjectAlreadyInvoiced()
        {
            string mPRJIDNo = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (mPRJIDNo.Length > 0) mPRJIDNo += ",";
                    mPRJIDNo += Sm.GetGrdStr(Grd1, i, 1);
                }
            }
            
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblProjectImplementationDtl ");
            SQL.AppendLine("Where DocNo = @Param1 ");
            SQL.AppendLine("And InvoicedInd = 'Y' ");
            SQL.AppendLine("And Find_In_Set(DNo, @Param2) ");
            SQL.AppendLine("Limit 1; ");

            return false;
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You should cancel this document.");
                ChkCancelInd.Focus();
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblProjectDeliveryHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And CancelInd = 'Y'; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditProjectDeliveryHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProjectDeliveryHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason = @CancelReason, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowProjectDeliveryHdr(DocNo);
                ShowProjectDeliveryDtl(DocNo);
                UpdateOutstandingAmt();
                UpdateOutstandingBobot();
                RecomputeBobot();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProjectDeliveryHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.PRJIDocNo, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' Else '' End As StatusDesc, ");
            SQL.AppendLine("A.Remark, A.Amt, A.Bobot ");
            SQL.AppendLine("From TblProjectDeliveryHdr A ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "PRJIDocNo", "Remark", 
                    
                    //6-8
                    "Amt", "StatusDesc", "Bobot"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    TxtPRJIDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    TxtStatus.EditValue = Sm.DrStr(dr, c[7]);
                    TxtBobot.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                }, true
            );
        }

        private void ShowProjectDeliveryDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PRJIDNo, C.StageName, D.TaskName, B.BobotPercentage, A.Remark, ");
            SQL.AppendLine("F.SOCDocNo, F.DocNo As SOCRDocNo, A.Amt, B.EstimatedAmt, A.Bobot, B.ItCode, G.ItName, G.ItCodeInternal ");
            SQL.AppendLine("From TblProjectDeliveryDtl A ");
            SQL.AppendLine("Inner Join TblProjectImplementationDtl B On A.PRJIDocNo = B.DocNo And A.PRJIDNo = B.DNo ");
            SQL.AppendLine("Inner Join TblProjectStage C On B.StageCode = C.StageCode ");
            SQL.AppendLine("Inner Join TblProjectTask D On B.TaskCode = D.TaskCode ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr E On B.DocNo = E.DocNo ");
            SQL.AppendLine("Inner JOin TblSOContractRevisionHdr F On E.SOContractDocNo = F.Docno ");
            SQL.AppendLine("Left Join TblItem G On B.ItCode = G.ItCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "PRJIDNo", 
                    //1-5
                    "StageName", "TaskName", "BobotPercentage","Remark", "SOCDocNo", 
                    //6-10
                    "SOCRDocNo", "Amt", "EstimatedAmt", "Bobot", "ItCode", 
                    //11-12
                    "ItName", "ItCodeInternal"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Grd.Cells[Row, 13].Value = 0m;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 9, 10, 11, 12, 13 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void RecomputeBobot()
        {
            if (mIsWBSBobotColumnAutoCompute)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    {
                        decimal mEstimatedPrice = Sm.GetGrdDec(Grd1, i, 11);
                        decimal mAmt = Sm.GetGrdDec(Grd1, i, 9);
                        decimal mBobotPercentage = Sm.GetGrdDec(Grd1, i, 4);
                        decimal mBobot = Sm.GetGrdDec(Grd1, i, 12);
                        decimal mRealBobot = Decimal.Parse(Sm.GetValue("Select Bobot From TblProjectImplementationDtl Where DocNo = @Param1 And DNo = @Param2; ", TxtPRJIDocNo.Text, Sm.GetGrdStr(Grd1, i, 1), string.Empty));

                        //if (mEstimatedPrice != mAmt)
                        //{
                        //    mBobotPercentage = Math.Round(((mAmt / mEstimatedPrice) * mBobotPercentage), 4);
                        //}

                        if(mBobot != mRealBobot)
                        {
                            mBobotPercentage = Math.Round((((mRealBobot - mBobot) / mRealBobot) * mBobotPercentage), 4);
                        }

                        Grd1.Cells[i, 4].Value = mBobotPercentage;
                    }
                }
            }
        }

        private void UpdateOutstandingAmt()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mPRJIDNo = string.Empty, mPRJIDocNo = TxtPRJIDocNo.Text;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (mPRJIDNo.Length > 0) mPRJIDNo += ",";
                    mPRJIDNo += Sm.GetGrdStr(Grd1, i, 1);
                }
            }

            if (mPRJIDNo.Length > 0)
            {
                //SQL.AppendLine("Select A.DNo, (A.SettledAmt - IfNull(B.Amt, 0)) OutstandingAmt ");
                SQL.AppendLine("Select A.DNo, (A.EstimatedAmt - A.SettledAmt) OutstandingAmt ");
                SQL.AppendLine("From TblProjectImplementationDtl A ");
                //SQL.AppendLine("Left Join ");
                //SQL.AppendLine("( ");
                //SQL.AppendLine("    Select T1.PRJIDocNo, T2.PRJIDNo, Sum(T2.Amt) Amt ");
                //SQL.AppendLine("    From TblProjectDeliveryHdr T1 ");
                //SQL.AppendLine("    Inner Join TblProjectDeliveryDtl T2 On T1.DocNo = T2.DocNo ");
                //SQL.AppendLine("        And T1.CancelInd = 'N' ");
                //SQL.AppendLine("        And T1.PRJIDocNo = @DocNo ");
                //SQL.AppendLine("        And T1.Status In ('O', 'A') ");
                //SQL.AppendLine("        And Find_In_Set(T2.PRJIDNo, @DNo) ");
                //SQL.AppendLine("    Group By T1.PRJIDocNo, T2.PRJIDNo ");
                //SQL.AppendLine(") B On A.DocNo = B.PRJIDocNo And A.DNo = B.PRJIDNo ");
                SQL.AppendLine("Where A.DocNo = @DocNo ");
                SQL.AppendLine("And Find_In_Set(A.DNo, @DNo); ");

                Sm.CmParam<String>(ref cm, "@DocNo", mPRJIDocNo);
                Sm.CmParam<String>(ref cm, "@DNo", mPRJIDNo);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DNo", "OutstandingAmt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            for (int i = 0; i < Grd1.Rows.Count; i++)
                            {
                                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                                {
                                    if (Sm.GetGrdStr(Grd1, i, 1) == Sm.DrStr(dr, c[0]))
                                    {
                                        decimal mOutstandingAmt = Sm.DrDec(dr, c[1]);
                                        if (mOutstandingAmt <= 0) mOutstandingAmt = 0m;
                                        Grd1.Cells[i, 10].Value = mOutstandingAmt;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    dr.Close();
                }
            }
        }

        private void UpdateOutstandingBobot()
        {
            var cm = new MySqlCommand();
            string mPRJIDNo = string.Empty, mPRJIDocNo = TxtPRJIDocNo.Text, sSQL = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (mPRJIDNo.Length > 0) mPRJIDNo += ",";
                    mPRJIDNo += Sm.GetGrdStr(Grd1, i, 1);
                }
            }

            if (mPRJIDNo.Length > 0)
            {
                sSQL += "Select A.DNo, (A.Bobot - IfNull(B.DeliveryBobot, 0.00)) OutstandingBobot ";
                sSQL += "From TblProjectImplementationDtl A ";
                sSQL += "Inner Join ";
                sSQL += "( ";
                sSQL += "    Select T2.PRJIDocNo, T2.PRJIDNo, Sum(T2.Bobot) DeliveryBobot ";
                sSQL += "    From TblProjectDeliveryHdr T1 ";
                sSQL += "    Inner Join TblProjectDeliveryDtl T2 On T1.DocNo = T2.DocNo ";
                sSQL += "        And T1.CancelInd = 'N' ";
                sSQL += "        And T1.Status In ('O', 'A') ";
                sSQL += "        And T1.PRJIDocNo = @DocNo ";
                sSQL += "        And Find_In_Set(T2.PRJIDNo, @DNo) ";
                sSQL += "    Group By T2.PRJIDocNo, T2.PRJIDNo ";
                sSQL += ") B On A.DocNo = B.PRJIDocNo And A.Dno = B.PRJIDNo ";
                sSQL += "    And A.DocNo = @DocNo ";
                sSQL += "    And Find_In_Set(A.DNo, @DNo); ";

                Sm.CmParam<String>(ref cm, "@DocNo", mPRJIDocNo);
                Sm.CmParam<String>(ref cm, "@DNo", mPRJIDNo);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = sSQL;
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DNo", "OutstandingBobot" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            for (int i = 0; i < Grd1.Rows.Count; i++)
                            {
                                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                                {
                                    if (Sm.GetGrdStr(Grd1, i, 1) == Sm.DrStr(dr, c[0]))
                                    {
                                        decimal mOutstandingAmt = Sm.DrDec(dr, c[1]);
                                        if (mOutstandingAmt <= 0) mOutstandingAmt = 0m;
                                        Grd1.Cells[i, 13].Value = mOutstandingAmt;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    dr.Close();
                }
            }
        }

        internal void SetOutstandingAmt(int Row)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mPRJIDNo = string.Empty, mPRJIDocNo = TxtPRJIDocNo.Text;

            mPRJIDNo = Sm.GetGrdStr(Grd1, Row, 1);

            if (mPRJIDNo.Length > 0)
            {
                //SQL.AppendLine("Select A.DNo, (A.SettledAmt - IfNull(B.Amt, 0)) OutstandingAmt ");
                SQL.AppendLine("Select A.DNo, (A.EstimatedAmt - A.SettledAmt) OutstandingAmt ");
                SQL.AppendLine("From TblProjectImplementationDtl A ");
                //SQL.AppendLine("Left Join ");
                //SQL.AppendLine("( ");
                //SQL.AppendLine("    Select T1.PRJIDocNo, T2.PRJIDNo, Sum(T2.Amt) Amt ");
                //SQL.AppendLine("    From TblProjectDeliveryHdr T1 ");
                //SQL.AppendLine("    Inner Join TblProjectDeliveryDtl T2 On T1.DocNo = T2.DocNo ");
                //SQL.AppendLine("        And T1.CancelInd = 'N' ");
                //SQL.AppendLine("        And T1.PRJIDocNo = @DocNo ");
                //SQL.AppendLine("        And T1.Status In ('O', 'A') ");
                //SQL.AppendLine("        And Find_In_Set(T2.PRJIDNo, @DNo) ");
                //SQL.AppendLine("    Group By T1.PRJIDocNo, T2.PRJIDNo ");
                //SQL.AppendLine(") B On A.DocNo = B.PRJIDocNo And A.DNo = B.PRJIDNo ");
                SQL.AppendLine("Where A.DocNo = @DocNo ");
                SQL.AppendLine("And Find_In_Set(A.DNo, @DNo); ");

                Sm.CmParam<String>(ref cm, "@DocNo", mPRJIDocNo);
                Sm.CmParam<String>(ref cm, "@DNo", mPRJIDNo);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DNo", "OutstandingAmt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            decimal mOutstandingAmt = Sm.DrDec(dr, c[1]);
                            if (mOutstandingAmt <= 0) mOutstandingAmt = 0m;
                            Grd1.Cells[Row, 10].Value = mOutstandingAmt;
                            Grd1.Cells[Row, 9].Value = mOutstandingAmt;
                            break;
                        }
                    }
                    dr.Close();
                }
            }
        }

        internal string GetSelectedPRJIData()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += "##" + TxtPRJIDocNo.Text + Sm.GetGrdStr(Grd1, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsWBSBobotColumnAutoCompute = Sm.GetParameterBoo("IsWBSBobotColumnAutoCompute");
        }

        internal void ComputeAmt()
        {
            decimal mAmt = 0m;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    mAmt += Sm.GetGrdDec(Grd1, i, 9);
                }
            }

            TxtAmt.EditValue = Sm.FormatNum(mAmt, 0);
        }

        internal void ComputeBobot()
        {
            decimal mBobot = 0m;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    mBobot += Sm.GetGrdDec(Grd1, i, 12);
                }
            }

            TxtBobot.EditValue = Sm.FormatNum(mBobot, 0);
        }

        private void UpdateLatestSOContractRevision()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.SOCDocNo ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @Param; ");

            string mSOCDocNo = Sm.GetValue(SQL.ToString(), TxtPRJIDocNo.Text);
            string mSOCR = Sm.GetValue("Select Max(DocNo) From TblSOContractRevisionHdr Where SOCDocNo = @Param And Status = 'A';", mSOCDocNo);

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    Grd1.Cells[i, 8].Value = mSOCR;
                }
            }
        }

        #endregion

        #endregion

        #region Events

        #region Button Click

        private void BtnPRJIDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmProjectDelivery2Dlg(this));
            }
        }

        private void BtnPRJIDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false))
            {
                var f = new FrmProjectImplementation2(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmProjectImplementation2");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPRJIDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void TxtBobot_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtBobot, 0);
        }

        #endregion

        #endregion

    }
}
