﻿namespace RunSystem
{
    partial class FrmMaterialRequest5Dlg7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtEstPrice = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtCurCode = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtLocation = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.A = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtGrpName1 = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtSubTotal1 = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.B = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal2 = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.TxtGrpName2 = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.C = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal3 = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.TxtGrpName3 = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.D = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode4 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode4 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal4 = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.TxtGrpName4 = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.panel6 = new System.Windows.Forms.Panel();
            this.E = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode5 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode5 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal5 = new DevExpress.XtraEditors.TextEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.TxtGrpName5 = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.panel7 = new System.Windows.Forms.Panel();
            this.F = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode6 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode6 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal6 = new DevExpress.XtraEditors.TextEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.TxtGrpName6 = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.G = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode7 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode7 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal7 = new DevExpress.XtraEditors.TextEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.TxtGrpName7 = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.H = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode8 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode8 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal8 = new DevExpress.XtraEditors.TextEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.TxtGrpName8 = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.Grd8 = new TenTec.Windows.iGridLib.iGrid();
            this.I = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode9 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode9 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal9 = new DevExpress.XtraEditors.TextEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.TxtGrpName9 = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.Grd9 = new TenTec.Windows.iGridLib.iGrid();
            this.J = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode10 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode10 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal10 = new DevExpress.XtraEditors.TextEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.TxtGrpName10 = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.Grd10 = new TenTec.Windows.iGridLib.iGrid();
            this.K = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode11 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode11 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal11 = new DevExpress.XtraEditors.TextEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.TxtGrpName11 = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.Grd11 = new TenTec.Windows.iGridLib.iGrid();
            this.L = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode12 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode12 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal12 = new DevExpress.XtraEditors.TextEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.TxtGrpName12 = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.Grd12 = new TenTec.Windows.iGridLib.iGrid();
            this.M = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode13 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode13 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal13 = new DevExpress.XtraEditors.TextEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.TxtGrpName13 = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.Grd13 = new TenTec.Windows.iGridLib.iGrid();
            this.N = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode14 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode14 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal14 = new DevExpress.XtraEditors.TextEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.TxtGrpName14 = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.Grd14 = new TenTec.Windows.iGridLib.iGrid();
            this.O = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode15 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode15 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal15 = new DevExpress.XtraEditors.TextEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.TxtGrpName15 = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.Grd15 = new TenTec.Windows.iGridLib.iGrid();
            this.P = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode16 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode16 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal16 = new DevExpress.XtraEditors.TextEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.TxtGrpName16 = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.Grd16 = new TenTec.Windows.iGridLib.iGrid();
            this.Q = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode17 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode17 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal17 = new DevExpress.XtraEditors.TextEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.TxtGrpName17 = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.Grd17 = new TenTec.Windows.iGridLib.iGrid();
            this.R = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode18 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode18 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal18 = new DevExpress.XtraEditors.TextEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.TxtGrpName18 = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.Grd18 = new TenTec.Windows.iGridLib.iGrid();
            this.S = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode19 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode19 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal19 = new DevExpress.XtraEditors.TextEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.TxtGrpName19 = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.Grd19 = new TenTec.Windows.iGridLib.iGrid();
            this.T = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode20 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode20 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal20 = new DevExpress.XtraEditors.TextEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.TxtGrpName20 = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.Grd20 = new TenTec.Windows.iGridLib.iGrid();
            this.U = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode21 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode21 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal21 = new DevExpress.XtraEditors.TextEdit();
            this.label52 = new System.Windows.Forms.Label();
            this.TxtGrpName21 = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.Grd21 = new TenTec.Windows.iGridLib.iGrid();
            this.V = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode22 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode22 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal22 = new DevExpress.XtraEditors.TextEdit();
            this.label53 = new System.Windows.Forms.Label();
            this.TxtGrpName22 = new DevExpress.XtraEditors.TextEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.Grd22 = new TenTec.Windows.iGridLib.iGrid();
            this.W = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode23 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode23 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal23 = new DevExpress.XtraEditors.TextEdit();
            this.label54 = new System.Windows.Forms.Label();
            this.TxtGrpName23 = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.Grd23 = new TenTec.Windows.iGridLib.iGrid();
            this.X = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode24 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode24 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal24 = new DevExpress.XtraEditors.TextEdit();
            this.label55 = new System.Windows.Forms.Label();
            this.TxtGrpName24 = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.Grd24 = new TenTec.Windows.iGridLib.iGrid();
            this.Y = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode25 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode25 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal25 = new DevExpress.XtraEditors.TextEdit();
            this.label56 = new System.Windows.Forms.Label();
            this.TxtGrpName25 = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.Grd25 = new TenTec.Windows.iGridLib.iGrid();
            this.Z = new DevExpress.XtraTab.XtraTabPage();
            this.LueCurCode26 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUOMCode26 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSubTotal26 = new DevExpress.XtraEditors.TextEdit();
            this.label57 = new System.Windows.Forms.Label();
            this.TxtGrpName26 = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.Grd26 = new TenTec.Windows.iGridLib.iGrid();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEstPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.A.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal1.Properties)).BeginInit();
            this.B.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.C.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.D.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.E.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.panel7.SuspendLayout();
            this.F.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.G.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.H.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).BeginInit();
            this.I.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).BeginInit();
            this.J.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd10)).BeginInit();
            this.K.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).BeginInit();
            this.L.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).BeginInit();
            this.M.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).BeginInit();
            this.N.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).BeginInit();
            this.O.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).BeginInit();
            this.P.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).BeginInit();
            this.Q.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).BeginInit();
            this.R.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd18)).BeginInit();
            this.S.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd19)).BeginInit();
            this.T.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd20)).BeginInit();
            this.U.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd21)).BeginInit();
            this.V.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd22)).BeginInit();
            this.W.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd23)).BeginInit();
            this.X.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd24)).BeginInit();
            this.Y.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd25)).BeginInit();
            this.Z.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd26)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(760, 0);
            this.panel1.Size = new System.Drawing.Size(82, 473);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 451);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtLocation);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtEstPrice);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.TxtCurCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtItName);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Size = new System.Drawing.Size(760, 118);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // TxtEstPrice
            // 
            this.TxtEstPrice.EnterMoveNextControl = true;
            this.TxtEstPrice.Location = new System.Drawing.Point(101, 92);
            this.TxtEstPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEstPrice.Name = "TxtEstPrice";
            this.TxtEstPrice.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEstPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEstPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEstPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtEstPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEstPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEstPrice.Properties.ReadOnly = true;
            this.TxtEstPrice.Size = new System.Drawing.Size(134, 20);
            this.TxtEstPrice.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(6, 95);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 14);
            this.label10.TabIndex = 19;
            this.label10.Text = "Estimated Price";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCurCode
            // 
            this.TxtCurCode.EnterMoveNextControl = true;
            this.TxtCurCode.Location = new System.Drawing.Point(101, 70);
            this.TxtCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode.Name = "TxtCurCode";
            this.TxtCurCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode.Properties.MaxLength = 3;
            this.TxtCurCode.Properties.ReadOnly = true;
            this.TxtCurCode.Size = new System.Drawing.Size(134, 20);
            this.TxtCurCode.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(42, 73);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 14);
            this.label4.TabIndex = 17;
            this.label4.Text = "Currency";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(101, 26);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 250;
            this.TxtItName.Properties.ReadOnly = true;
            this.TxtItName.Size = new System.Drawing.Size(544, 20);
            this.TxtItName.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(21, 30);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "Item\'s Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(101, 4);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 30;
            this.TxtItCode.Properties.ReadOnly = true;
            this.TxtItCode.Size = new System.Drawing.Size(199, 20);
            this.TxtItCode.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(24, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Item\'s Code";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLocation
            // 
            this.TxtLocation.EnterMoveNextControl = true;
            this.TxtLocation.Location = new System.Drawing.Point(101, 48);
            this.TxtLocation.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocation.Name = "TxtLocation";
            this.TxtLocation.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtLocation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocation.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocation.Properties.Appearance.Options.UseFont = true;
            this.TxtLocation.Properties.ReadOnly = true;
            this.TxtLocation.Size = new System.Drawing.Size(134, 20);
            this.TxtLocation.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(44, 51);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 14);
            this.label1.TabIndex = 21;
            this.label1.Text = "Location";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 118);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.A;
            this.Tc1.Size = new System.Drawing.Size(760, 355);
            this.Tc1.TabIndex = 23;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.A,
            this.B,
            this.C,
            this.D,
            this.E,
            this.F,
            this.G,
            this.H,
            this.I,
            this.J,
            this.K,
            this.L,
            this.M,
            this.N,
            this.O,
            this.P,
            this.Q,
            this.R,
            this.S,
            this.T,
            this.U,
            this.V,
            this.W,
            this.X,
            this.Y,
            this.Z});
            // 
            // A
            // 
            this.A.Appearance.Header.Options.UseTextOptions = true;
            this.A.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.A.Controls.Add(this.LueCurCode1);
            this.A.Controls.Add(this.LueUOMCode1);
            this.A.Controls.Add(this.TxtGrpName1);
            this.A.Controls.Add(this.label5);
            this.A.Controls.Add(this.Grd1);
            this.A.Controls.Add(this.panel3);
            this.A.Name = "A";
            this.A.Size = new System.Drawing.Size(754, 327);
            this.A.Text = "A";
            // 
            // LueCurCode1
            // 
            this.LueCurCode1.EnterMoveNextControl = true;
            this.LueCurCode1.Location = new System.Drawing.Point(263, 69);
            this.LueCurCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode1.Name = "LueCurCode1";
            this.LueCurCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode1.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode1.Properties.DropDownRows = 12;
            this.LueCurCode1.Properties.NullText = "[Empty]";
            this.LueCurCode1.Properties.PopupWidth = 500;
            this.LueCurCode1.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode1.TabIndex = 69;
            this.LueCurCode1.ToolTip = "F4 : Show/hide list";
            this.LueCurCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode1.EditValueChanged += new System.EventHandler(this.LueCurCode1_EditValueChanged);
            this.LueCurCode1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode1_KeyDown);
            this.LueCurCode1.Leave += new System.EventHandler(this.LueCurCode1_Leave);
            this.LueCurCode1.Validated += new System.EventHandler(this.LueCurCode1_Validated);
            // 
            // LueUOMCode1
            // 
            this.LueUOMCode1.EnterMoveNextControl = true;
            this.LueUOMCode1.Location = new System.Drawing.Point(65, 69);
            this.LueUOMCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode1.Name = "LueUOMCode1";
            this.LueUOMCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode1.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode1.Properties.DropDownRows = 12;
            this.LueUOMCode1.Properties.NullText = "[Empty]";
            this.LueUOMCode1.Properties.PopupWidth = 500;
            this.LueUOMCode1.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode1.TabIndex = 68;
            this.LueUOMCode1.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode1.EditValueChanged += new System.EventHandler(this.LueUOMCode1_EditValueChanged);
            this.LueUOMCode1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode1_KeyDown);
            this.LueUOMCode1.Leave += new System.EventHandler(this.LueUOMCode1_Leave);
            this.LueUOMCode1.Validated += new System.EventHandler(this.LueUOMCode1_Validated);
            // 
            // TxtGrpName1
            // 
            this.TxtGrpName1.EnterMoveNextControl = true;
            this.TxtGrpName1.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName1.Name = "TxtGrpName1";
            this.TxtGrpName1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName1.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName1.Properties.ReadOnly = true;
            this.TxtGrpName1.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName1.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(5, 15);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 14);
            this.label5.TabIndex = 23;
            this.label5.Text = "Group Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 44);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(754, 283);
            this.Grd1.TabIndex = 17;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1KeyDown);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TxtSubTotal1);
            this.panel3.Controls.Add(this.label32);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(754, 44);
            this.panel3.TabIndex = 70;
            // 
            // TxtSubTotal1
            // 
            this.TxtSubTotal1.EnterMoveNextControl = true;
            this.TxtSubTotal1.Location = new System.Drawing.Point(523, 12);
            this.TxtSubTotal1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal1.Name = "TxtSubTotal1";
            this.TxtSubTotal1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal1.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal1.Properties.ReadOnly = true;
            this.TxtSubTotal1.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal1.TabIndex = 26;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(460, 15);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 14);
            this.label32.TabIndex = 25;
            this.label32.Text = "Sub Total";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // B
            // 
            this.B.Controls.Add(this.LueCurCode2);
            this.B.Controls.Add(this.LueUOMCode2);
            this.B.Controls.Add(this.TxtSubTotal2);
            this.B.Controls.Add(this.label33);
            this.B.Controls.Add(this.TxtGrpName2);
            this.B.Controls.Add(this.label6);
            this.B.Controls.Add(this.Grd2);
            this.B.Controls.Add(this.panel4);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(754, 327);
            this.B.Text = "B";
            // 
            // LueCurCode2
            // 
            this.LueCurCode2.EnterMoveNextControl = true;
            this.LueCurCode2.Location = new System.Drawing.Point(263, 68);
            this.LueCurCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode2.Name = "LueCurCode2";
            this.LueCurCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode2.Properties.DropDownRows = 12;
            this.LueCurCode2.Properties.NullText = "[Empty]";
            this.LueCurCode2.Properties.PopupWidth = 500;
            this.LueCurCode2.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode2.TabIndex = 71;
            this.LueCurCode2.ToolTip = "F4 : Show/hide list";
            this.LueCurCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode2.EditValueChanged += new System.EventHandler(this.LueCurCode2_EditValueChanged);
            this.LueCurCode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode2_KeyDown);
            this.LueCurCode2.Leave += new System.EventHandler(this.LueCurCode2_Leave);
            this.LueCurCode2.Validated += new System.EventHandler(this.LueCurCode2_Validated);
            // 
            // LueUOMCode2
            // 
            this.LueUOMCode2.EnterMoveNextControl = true;
            this.LueUOMCode2.Location = new System.Drawing.Point(65, 68);
            this.LueUOMCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode2.Name = "LueUOMCode2";
            this.LueUOMCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode2.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode2.Properties.DropDownRows = 12;
            this.LueUOMCode2.Properties.NullText = "[Empty]";
            this.LueUOMCode2.Properties.PopupWidth = 500;
            this.LueUOMCode2.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode2.TabIndex = 70;
            this.LueUOMCode2.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode2.EditValueChanged += new System.EventHandler(this.LueUOMCode2_EditValueChanged);
            this.LueUOMCode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode2_KeyDown);
            this.LueUOMCode2.Leave += new System.EventHandler(this.LueUOMCode2_Leave);
            this.LueUOMCode2.Validated += new System.EventHandler(this.LueUOMCode2_Validated);
            // 
            // TxtSubTotal2
            // 
            this.TxtSubTotal2.EnterMoveNextControl = true;
            this.TxtSubTotal2.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal2.Name = "TxtSubTotal2";
            this.TxtSubTotal2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal2.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal2.Properties.ReadOnly = true;
            this.TxtSubTotal2.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal2.TabIndex = 28;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(454, 15);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(60, 14);
            this.label33.TabIndex = 27;
            this.label33.Text = "Sub Total";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName2
            // 
            this.TxtGrpName2.EnterMoveNextControl = true;
            this.TxtGrpName2.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName2.Name = "TxtGrpName2";
            this.TxtGrpName2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName2.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName2.Properties.ReadOnly = true;
            this.TxtGrpName2.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName2.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(5, 15);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 14);
            this.label6.TabIndex = 25;
            this.label6.Text = "Group Name";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 44);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(754, 283);
            this.Grd2.TabIndex = 18;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2KeyDown);
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(754, 44);
            this.panel4.TabIndex = 72;
            // 
            // C
            // 
            this.C.Controls.Add(this.LueCurCode3);
            this.C.Controls.Add(this.LueUOMCode3);
            this.C.Controls.Add(this.TxtSubTotal3);
            this.C.Controls.Add(this.label34);
            this.C.Controls.Add(this.TxtGrpName3);
            this.C.Controls.Add(this.label7);
            this.C.Controls.Add(this.Grd3);
            this.C.Controls.Add(this.panel5);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(754, 327);
            this.C.Text = "C";
            // 
            // LueCurCode3
            // 
            this.LueCurCode3.EnterMoveNextControl = true;
            this.LueCurCode3.Location = new System.Drawing.Point(263, 68);
            this.LueCurCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode3.Name = "LueCurCode3";
            this.LueCurCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode3.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode3.Properties.DropDownRows = 12;
            this.LueCurCode3.Properties.NullText = "[Empty]";
            this.LueCurCode3.Properties.PopupWidth = 500;
            this.LueCurCode3.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode3.TabIndex = 71;
            this.LueCurCode3.ToolTip = "F4 : Show/hide list";
            this.LueCurCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode3.EditValueChanged += new System.EventHandler(this.LueCurCode3_EditValueChanged);
            this.LueCurCode3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode3_KeyDown);
            this.LueCurCode3.Leave += new System.EventHandler(this.LueCurCode3_Leave);
            this.LueCurCode3.Validated += new System.EventHandler(this.LueCurCode3_Validated);
            // 
            // LueUOMCode3
            // 
            this.LueUOMCode3.EnterMoveNextControl = true;
            this.LueUOMCode3.Location = new System.Drawing.Point(65, 68);
            this.LueUOMCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode3.Name = "LueUOMCode3";
            this.LueUOMCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode3.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode3.Properties.DropDownRows = 12;
            this.LueUOMCode3.Properties.NullText = "[Empty]";
            this.LueUOMCode3.Properties.PopupWidth = 500;
            this.LueUOMCode3.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode3.TabIndex = 70;
            this.LueUOMCode3.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode3.EditValueChanged += new System.EventHandler(this.LueUOMCode3_EditValueChanged);
            this.LueUOMCode3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode3_KeyDown);
            this.LueUOMCode3.Leave += new System.EventHandler(this.LueUOMCode3_Leave);
            this.LueUOMCode3.Validated += new System.EventHandler(this.LueUOMCode3_Validated);
            // 
            // TxtSubTotal3
            // 
            this.TxtSubTotal3.EnterMoveNextControl = true;
            this.TxtSubTotal3.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal3.Name = "TxtSubTotal3";
            this.TxtSubTotal3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal3.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal3.Properties.ReadOnly = true;
            this.TxtSubTotal3.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal3.TabIndex = 28;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(454, 15);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(60, 14);
            this.label34.TabIndex = 27;
            this.label34.Text = "Sub Total";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName3
            // 
            this.TxtGrpName3.EnterMoveNextControl = true;
            this.TxtGrpName3.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName3.Name = "TxtGrpName3";
            this.TxtGrpName3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName3.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName3.Properties.ReadOnly = true;
            this.TxtGrpName3.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName3.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(5, 15);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 14);
            this.label7.TabIndex = 25;
            this.label7.Text = "Group Name";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 44);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(754, 283);
            this.Grd3.TabIndex = 19;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3KeyDown);
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(754, 44);
            this.panel5.TabIndex = 72;
            // 
            // D
            // 
            this.D.Controls.Add(this.LueCurCode4);
            this.D.Controls.Add(this.LueUOMCode4);
            this.D.Controls.Add(this.TxtSubTotal4);
            this.D.Controls.Add(this.label35);
            this.D.Controls.Add(this.TxtGrpName4);
            this.D.Controls.Add(this.label8);
            this.D.Controls.Add(this.Grd4);
            this.D.Controls.Add(this.panel6);
            this.D.Name = "D";
            this.D.Size = new System.Drawing.Size(754, 327);
            this.D.Text = "D";
            // 
            // LueCurCode4
            // 
            this.LueCurCode4.EnterMoveNextControl = true;
            this.LueCurCode4.Location = new System.Drawing.Point(263, 70);
            this.LueCurCode4.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode4.Name = "LueCurCode4";
            this.LueCurCode4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode4.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode4.Properties.DropDownRows = 12;
            this.LueCurCode4.Properties.NullText = "[Empty]";
            this.LueCurCode4.Properties.PopupWidth = 500;
            this.LueCurCode4.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode4.TabIndex = 71;
            this.LueCurCode4.ToolTip = "F4 : Show/hide list";
            this.LueCurCode4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode4.EditValueChanged += new System.EventHandler(this.LueCurCode4_EditValueChanged);
            this.LueCurCode4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode4_KeyDown);
            this.LueCurCode4.Leave += new System.EventHandler(this.LueCurCode4_Leave);
            this.LueCurCode4.Validated += new System.EventHandler(this.LueCurCode4_Validated);
            // 
            // LueUOMCode4
            // 
            this.LueUOMCode4.EnterMoveNextControl = true;
            this.LueUOMCode4.Location = new System.Drawing.Point(65, 70);
            this.LueUOMCode4.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode4.Name = "LueUOMCode4";
            this.LueUOMCode4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode4.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode4.Properties.DropDownRows = 12;
            this.LueUOMCode4.Properties.NullText = "[Empty]";
            this.LueUOMCode4.Properties.PopupWidth = 500;
            this.LueUOMCode4.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode4.TabIndex = 70;
            this.LueUOMCode4.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode4.EditValueChanged += new System.EventHandler(this.LueUOMCode4_EditValueChanged);
            this.LueUOMCode4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode4_KeyDown);
            this.LueUOMCode4.Leave += new System.EventHandler(this.LueUOMCode4_Leave);
            this.LueUOMCode4.Validated += new System.EventHandler(this.LueUOMCode4_Validated);
            // 
            // TxtSubTotal4
            // 
            this.TxtSubTotal4.EnterMoveNextControl = true;
            this.TxtSubTotal4.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal4.Name = "TxtSubTotal4";
            this.TxtSubTotal4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal4.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal4.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal4.Properties.ReadOnly = true;
            this.TxtSubTotal4.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal4.TabIndex = 28;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(454, 15);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(60, 14);
            this.label35.TabIndex = 27;
            this.label35.Text = "Sub Total";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName4
            // 
            this.TxtGrpName4.EnterMoveNextControl = true;
            this.TxtGrpName4.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName4.Name = "TxtGrpName4";
            this.TxtGrpName4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName4.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName4.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName4.Properties.ReadOnly = true;
            this.TxtGrpName4.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName4.TabIndex = 26;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(5, 15);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 14);
            this.label8.TabIndex = 25;
            this.label8.Text = "Group Name";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 44);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(754, 283);
            this.Grd4.TabIndex = 20;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd4_EllipsisButtonClick);
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd4_AfterCommitEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4KeyDown);
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(754, 44);
            this.panel6.TabIndex = 72;
            // 
            // E
            // 
            this.E.Controls.Add(this.LueCurCode5);
            this.E.Controls.Add(this.LueUOMCode5);
            this.E.Controls.Add(this.TxtSubTotal5);
            this.E.Controls.Add(this.label36);
            this.E.Controls.Add(this.TxtGrpName5);
            this.E.Controls.Add(this.label9);
            this.E.Controls.Add(this.Grd5);
            this.E.Controls.Add(this.panel7);
            this.E.Name = "E";
            this.E.Size = new System.Drawing.Size(754, 327);
            this.E.Text = "E";
            // 
            // LueCurCode5
            // 
            this.LueCurCode5.EnterMoveNextControl = true;
            this.LueCurCode5.Location = new System.Drawing.Point(263, 69);
            this.LueCurCode5.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode5.Name = "LueCurCode5";
            this.LueCurCode5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode5.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode5.Properties.DropDownRows = 12;
            this.LueCurCode5.Properties.NullText = "[Empty]";
            this.LueCurCode5.Properties.PopupWidth = 500;
            this.LueCurCode5.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode5.TabIndex = 71;
            this.LueCurCode5.ToolTip = "F4 : Show/hide list";
            this.LueCurCode5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode5.EditValueChanged += new System.EventHandler(this.LueCurCode5_EditValueChanged);
            this.LueCurCode5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode5_KeyDown);
            this.LueCurCode5.Leave += new System.EventHandler(this.LueCurCode5_Leave);
            this.LueCurCode5.Validated += new System.EventHandler(this.LueCurCode5_Validated);
            // 
            // LueUOMCode5
            // 
            this.LueUOMCode5.EnterMoveNextControl = true;
            this.LueUOMCode5.Location = new System.Drawing.Point(65, 69);
            this.LueUOMCode5.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode5.Name = "LueUOMCode5";
            this.LueUOMCode5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode5.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode5.Properties.DropDownRows = 12;
            this.LueUOMCode5.Properties.NullText = "[Empty]";
            this.LueUOMCode5.Properties.PopupWidth = 500;
            this.LueUOMCode5.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode5.TabIndex = 70;
            this.LueUOMCode5.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode5.EditValueChanged += new System.EventHandler(this.LueUOMCode5_EditValueChanged);
            this.LueUOMCode5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode5_KeyDown);
            this.LueUOMCode5.Leave += new System.EventHandler(this.LueUOMCode5_Leave);
            this.LueUOMCode5.Validated += new System.EventHandler(this.LueUOMCode5_Validated);
            // 
            // TxtSubTotal5
            // 
            this.TxtSubTotal5.EnterMoveNextControl = true;
            this.TxtSubTotal5.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal5.Name = "TxtSubTotal5";
            this.TxtSubTotal5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal5.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal5.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal5.Properties.ReadOnly = true;
            this.TxtSubTotal5.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal5.TabIndex = 28;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(454, 15);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(60, 14);
            this.label36.TabIndex = 27;
            this.label36.Text = "Sub Total";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName5
            // 
            this.TxtGrpName5.EnterMoveNextControl = true;
            this.TxtGrpName5.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName5.Name = "TxtGrpName5";
            this.TxtGrpName5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName5.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName5.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName5.Properties.ReadOnly = true;
            this.TxtGrpName5.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName5.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(5, 15);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 14);
            this.label9.TabIndex = 25;
            this.label9.Text = "Group Name";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 44);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(754, 283);
            this.Grd5.TabIndex = 20;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd5_EllipsisButtonClick);
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd5_AfterCommitEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5KeyDown);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(754, 44);
            this.panel7.TabIndex = 72;
            // 
            // F
            // 
            this.F.Controls.Add(this.LueCurCode6);
            this.F.Controls.Add(this.LueUOMCode6);
            this.F.Controls.Add(this.TxtSubTotal6);
            this.F.Controls.Add(this.label37);
            this.F.Controls.Add(this.TxtGrpName6);
            this.F.Controls.Add(this.label11);
            this.F.Controls.Add(this.Grd6);
            this.F.Controls.Add(this.panel9);
            this.F.Name = "F";
            this.F.Size = new System.Drawing.Size(754, 327);
            this.F.Text = "F";
            // 
            // LueCurCode6
            // 
            this.LueCurCode6.EnterMoveNextControl = true;
            this.LueCurCode6.Location = new System.Drawing.Point(263, 69);
            this.LueCurCode6.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode6.Name = "LueCurCode6";
            this.LueCurCode6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode6.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode6.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode6.Properties.DropDownRows = 12;
            this.LueCurCode6.Properties.NullText = "[Empty]";
            this.LueCurCode6.Properties.PopupWidth = 500;
            this.LueCurCode6.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode6.TabIndex = 71;
            this.LueCurCode6.ToolTip = "F4 : Show/hide list";
            this.LueCurCode6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode6.EditValueChanged += new System.EventHandler(this.LueCurCode6_EditValueChanged);
            this.LueCurCode6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode6_KeyDown);
            this.LueCurCode6.Leave += new System.EventHandler(this.LueCurCode6_Leave);
            this.LueCurCode6.Validated += new System.EventHandler(this.LueCurCode6_Validated);
            // 
            // LueUOMCode6
            // 
            this.LueUOMCode6.EnterMoveNextControl = true;
            this.LueUOMCode6.Location = new System.Drawing.Point(65, 69);
            this.LueUOMCode6.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode6.Name = "LueUOMCode6";
            this.LueUOMCode6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode6.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode6.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode6.Properties.DropDownRows = 12;
            this.LueUOMCode6.Properties.NullText = "[Empty]";
            this.LueUOMCode6.Properties.PopupWidth = 500;
            this.LueUOMCode6.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode6.TabIndex = 70;
            this.LueUOMCode6.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode6.EditValueChanged += new System.EventHandler(this.LueUOMCode6_EditValueChanged);
            this.LueUOMCode6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode6_KeyDown);
            this.LueUOMCode6.Leave += new System.EventHandler(this.LueUOMCode6_Leave);
            this.LueUOMCode6.Validated += new System.EventHandler(this.LueUOMCode6_Validated);
            // 
            // TxtSubTotal6
            // 
            this.TxtSubTotal6.EnterMoveNextControl = true;
            this.TxtSubTotal6.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal6.Name = "TxtSubTotal6";
            this.TxtSubTotal6.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal6.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal6.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal6.Properties.ReadOnly = true;
            this.TxtSubTotal6.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal6.TabIndex = 28;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(454, 15);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(60, 14);
            this.label37.TabIndex = 27;
            this.label37.Text = "Sub Total";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName6
            // 
            this.TxtGrpName6.EnterMoveNextControl = true;
            this.TxtGrpName6.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName6.Name = "TxtGrpName6";
            this.TxtGrpName6.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName6.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName6.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName6.Properties.ReadOnly = true;
            this.TxtGrpName6.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName6.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(5, 15);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 14);
            this.label11.TabIndex = 25;
            this.label11.Text = "Group Name";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(0, 44);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(754, 283);
            this.Grd6.TabIndex = 20;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd6_EllipsisButtonClick);
            this.Grd6.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd6_RequestEdit);
            this.Grd6.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd6_AfterCommitEdit);
            this.Grd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd6KeyDown);
            // 
            // G
            // 
            this.G.Controls.Add(this.LueCurCode7);
            this.G.Controls.Add(this.LueUOMCode7);
            this.G.Controls.Add(this.TxtSubTotal7);
            this.G.Controls.Add(this.label38);
            this.G.Controls.Add(this.TxtGrpName7);
            this.G.Controls.Add(this.label12);
            this.G.Controls.Add(this.Grd7);
            this.G.Controls.Add(this.panel10);
            this.G.Name = "G";
            this.G.Size = new System.Drawing.Size(754, 327);
            this.G.Text = "G";
            // 
            // LueCurCode7
            // 
            this.LueCurCode7.EnterMoveNextControl = true;
            this.LueCurCode7.Location = new System.Drawing.Point(263, 69);
            this.LueCurCode7.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode7.Name = "LueCurCode7";
            this.LueCurCode7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode7.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode7.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode7.Properties.DropDownRows = 12;
            this.LueCurCode7.Properties.NullText = "[Empty]";
            this.LueCurCode7.Properties.PopupWidth = 500;
            this.LueCurCode7.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode7.TabIndex = 71;
            this.LueCurCode7.ToolTip = "F4 : Show/hide list";
            this.LueCurCode7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode7.EditValueChanged += new System.EventHandler(this.LueCurCode7_EditValueChanged);
            this.LueCurCode7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode7_KeyDown);
            this.LueCurCode7.Leave += new System.EventHandler(this.LueCurCode7_Leave);
            this.LueCurCode7.Validated += new System.EventHandler(this.LueCurCode7_Validated);
            // 
            // LueUOMCode7
            // 
            this.LueUOMCode7.EnterMoveNextControl = true;
            this.LueUOMCode7.Location = new System.Drawing.Point(65, 69);
            this.LueUOMCode7.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode7.Name = "LueUOMCode7";
            this.LueUOMCode7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode7.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode7.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode7.Properties.DropDownRows = 12;
            this.LueUOMCode7.Properties.NullText = "[Empty]";
            this.LueUOMCode7.Properties.PopupWidth = 500;
            this.LueUOMCode7.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode7.TabIndex = 70;
            this.LueUOMCode7.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode7.EditValueChanged += new System.EventHandler(this.LueUOMCode7_EditValueChanged);
            this.LueUOMCode7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode7_KeyDown);
            this.LueUOMCode7.Leave += new System.EventHandler(this.LueUOMCode7_Leave);
            this.LueUOMCode7.Validated += new System.EventHandler(this.LueUOMCode7_Validated);
            // 
            // TxtSubTotal7
            // 
            this.TxtSubTotal7.EnterMoveNextControl = true;
            this.TxtSubTotal7.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal7.Name = "TxtSubTotal7";
            this.TxtSubTotal7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal7.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal7.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal7.Properties.ReadOnly = true;
            this.TxtSubTotal7.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal7.TabIndex = 28;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(454, 15);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(60, 14);
            this.label38.TabIndex = 27;
            this.label38.Text = "Sub Total";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName7
            // 
            this.TxtGrpName7.EnterMoveNextControl = true;
            this.TxtGrpName7.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName7.Name = "TxtGrpName7";
            this.TxtGrpName7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName7.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName7.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName7.Properties.ReadOnly = true;
            this.TxtGrpName7.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName7.TabIndex = 26;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(5, 15);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 14);
            this.label12.TabIndex = 25;
            this.label12.Text = "Group Name";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(0, 44);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(754, 283);
            this.Grd7.TabIndex = 20;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd7_EllipsisButtonClick);
            this.Grd7.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd7_RequestEdit);
            this.Grd7.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd7_AfterCommitEdit);
            this.Grd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd7KeyDown);
            // 
            // H
            // 
            this.H.Controls.Add(this.LueCurCode8);
            this.H.Controls.Add(this.LueUOMCode8);
            this.H.Controls.Add(this.TxtSubTotal8);
            this.H.Controls.Add(this.label39);
            this.H.Controls.Add(this.TxtGrpName8);
            this.H.Controls.Add(this.label13);
            this.H.Controls.Add(this.Grd8);
            this.H.Controls.Add(this.panel11);
            this.H.Name = "H";
            this.H.Size = new System.Drawing.Size(754, 327);
            this.H.Text = "H";
            // 
            // LueCurCode8
            // 
            this.LueCurCode8.EnterMoveNextControl = true;
            this.LueCurCode8.Location = new System.Drawing.Point(263, 69);
            this.LueCurCode8.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode8.Name = "LueCurCode8";
            this.LueCurCode8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode8.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode8.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode8.Properties.DropDownRows = 12;
            this.LueCurCode8.Properties.NullText = "[Empty]";
            this.LueCurCode8.Properties.PopupWidth = 500;
            this.LueCurCode8.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode8.TabIndex = 71;
            this.LueCurCode8.ToolTip = "F4 : Show/hide list";
            this.LueCurCode8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode8.EditValueChanged += new System.EventHandler(this.LueCurCode8_EditValueChanged);
            this.LueCurCode8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode8_KeyDown);
            this.LueCurCode8.Leave += new System.EventHandler(this.LueCurCode8_Leave);
            this.LueCurCode8.Validated += new System.EventHandler(this.LueCurCode8_Validated);
            // 
            // LueUOMCode8
            // 
            this.LueUOMCode8.EnterMoveNextControl = true;
            this.LueUOMCode8.Location = new System.Drawing.Point(65, 69);
            this.LueUOMCode8.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode8.Name = "LueUOMCode8";
            this.LueUOMCode8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode8.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode8.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode8.Properties.DropDownRows = 12;
            this.LueUOMCode8.Properties.NullText = "[Empty]";
            this.LueUOMCode8.Properties.PopupWidth = 500;
            this.LueUOMCode8.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode8.TabIndex = 70;
            this.LueUOMCode8.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode8.EditValueChanged += new System.EventHandler(this.LueUOMCode8_EditValueChanged);
            this.LueUOMCode8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode8_KeyDown);
            this.LueUOMCode8.Leave += new System.EventHandler(this.LueUOMCode8_Leave);
            this.LueUOMCode8.Validated += new System.EventHandler(this.LueUOMCode8_Validated);
            // 
            // TxtSubTotal8
            // 
            this.TxtSubTotal8.EnterMoveNextControl = true;
            this.TxtSubTotal8.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal8.Name = "TxtSubTotal8";
            this.TxtSubTotal8.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal8.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal8.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal8.Properties.ReadOnly = true;
            this.TxtSubTotal8.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal8.TabIndex = 28;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(454, 15);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(60, 14);
            this.label39.TabIndex = 27;
            this.label39.Text = "Sub Total";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName8
            // 
            this.TxtGrpName8.EnterMoveNextControl = true;
            this.TxtGrpName8.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName8.Name = "TxtGrpName8";
            this.TxtGrpName8.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName8.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName8.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName8.Properties.ReadOnly = true;
            this.TxtGrpName8.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName8.TabIndex = 26;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(5, 15);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 14);
            this.label13.TabIndex = 25;
            this.label13.Text = "Group Name";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd8
            // 
            this.Grd8.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd8.DefaultRow.Height = 20;
            this.Grd8.DefaultRow.Sortable = false;
            this.Grd8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd8.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd8.Header.Height = 21;
            this.Grd8.Location = new System.Drawing.Point(0, 44);
            this.Grd8.Name = "Grd8";
            this.Grd8.RowHeader.Visible = true;
            this.Grd8.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd8.SingleClickEdit = true;
            this.Grd8.Size = new System.Drawing.Size(754, 283);
            this.Grd8.TabIndex = 20;
            this.Grd8.TreeCol = null;
            this.Grd8.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd8.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd8_EllipsisButtonClick);
            this.Grd8.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd8_RequestEdit);
            this.Grd8.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd8_AfterCommitEdit);
            this.Grd8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd8KeyDown);
            // 
            // I
            // 
            this.I.Controls.Add(this.LueCurCode9);
            this.I.Controls.Add(this.LueUOMCode9);
            this.I.Controls.Add(this.TxtSubTotal9);
            this.I.Controls.Add(this.label40);
            this.I.Controls.Add(this.TxtGrpName9);
            this.I.Controls.Add(this.label14);
            this.I.Controls.Add(this.Grd9);
            this.I.Controls.Add(this.panel12);
            this.I.Name = "I";
            this.I.Size = new System.Drawing.Size(754, 327);
            this.I.Text = "I";
            // 
            // LueCurCode9
            // 
            this.LueCurCode9.EnterMoveNextControl = true;
            this.LueCurCode9.Location = new System.Drawing.Point(263, 70);
            this.LueCurCode9.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode9.Name = "LueCurCode9";
            this.LueCurCode9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode9.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode9.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode9.Properties.DropDownRows = 12;
            this.LueCurCode9.Properties.NullText = "[Empty]";
            this.LueCurCode9.Properties.PopupWidth = 500;
            this.LueCurCode9.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode9.TabIndex = 71;
            this.LueCurCode9.ToolTip = "F4 : Show/hide list";
            this.LueCurCode9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode9.EditValueChanged += new System.EventHandler(this.LueCurCode9_EditValueChanged);
            this.LueCurCode9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode9_KeyDown);
            this.LueCurCode9.Leave += new System.EventHandler(this.LueCurCode9_Leave);
            this.LueCurCode9.Validated += new System.EventHandler(this.LueCurCode9_Validated);
            // 
            // LueUOMCode9
            // 
            this.LueUOMCode9.EnterMoveNextControl = true;
            this.LueUOMCode9.Location = new System.Drawing.Point(65, 70);
            this.LueUOMCode9.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode9.Name = "LueUOMCode9";
            this.LueUOMCode9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode9.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode9.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode9.Properties.DropDownRows = 12;
            this.LueUOMCode9.Properties.NullText = "[Empty]";
            this.LueUOMCode9.Properties.PopupWidth = 500;
            this.LueUOMCode9.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode9.TabIndex = 70;
            this.LueUOMCode9.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode9.EditValueChanged += new System.EventHandler(this.LueUOMCode9_EditValueChanged);
            this.LueUOMCode9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode9_KeyDown);
            this.LueUOMCode9.Leave += new System.EventHandler(this.LueUOMCode9_Leave);
            this.LueUOMCode9.Validated += new System.EventHandler(this.LueUOMCode9_Validated);
            // 
            // TxtSubTotal9
            // 
            this.TxtSubTotal9.EnterMoveNextControl = true;
            this.TxtSubTotal9.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal9.Name = "TxtSubTotal9";
            this.TxtSubTotal9.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal9.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal9.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal9.Properties.ReadOnly = true;
            this.TxtSubTotal9.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal9.TabIndex = 28;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(454, 15);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(60, 14);
            this.label40.TabIndex = 27;
            this.label40.Text = "Sub Total";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName9
            // 
            this.TxtGrpName9.EnterMoveNextControl = true;
            this.TxtGrpName9.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName9.Name = "TxtGrpName9";
            this.TxtGrpName9.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName9.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName9.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName9.Properties.ReadOnly = true;
            this.TxtGrpName9.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName9.TabIndex = 26;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(5, 15);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 14);
            this.label14.TabIndex = 25;
            this.label14.Text = "Group Name";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd9
            // 
            this.Grd9.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd9.DefaultRow.Height = 20;
            this.Grd9.DefaultRow.Sortable = false;
            this.Grd9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd9.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd9.Header.Height = 21;
            this.Grd9.Location = new System.Drawing.Point(0, 44);
            this.Grd9.Name = "Grd9";
            this.Grd9.RowHeader.Visible = true;
            this.Grd9.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd9.SingleClickEdit = true;
            this.Grd9.Size = new System.Drawing.Size(754, 283);
            this.Grd9.TabIndex = 20;
            this.Grd9.TreeCol = null;
            this.Grd9.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd9.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd9_EllipsisButtonClick);
            this.Grd9.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd9_RequestEdit);
            this.Grd9.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd9_AfterCommitEdit);
            this.Grd9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd9KeyDown);
            // 
            // J
            // 
            this.J.Controls.Add(this.LueCurCode10);
            this.J.Controls.Add(this.LueUOMCode10);
            this.J.Controls.Add(this.TxtSubTotal10);
            this.J.Controls.Add(this.label41);
            this.J.Controls.Add(this.TxtGrpName10);
            this.J.Controls.Add(this.label15);
            this.J.Controls.Add(this.Grd10);
            this.J.Controls.Add(this.panel13);
            this.J.Name = "J";
            this.J.Size = new System.Drawing.Size(754, 327);
            this.J.Text = "J";
            // 
            // LueCurCode10
            // 
            this.LueCurCode10.EnterMoveNextControl = true;
            this.LueCurCode10.Location = new System.Drawing.Point(263, 69);
            this.LueCurCode10.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode10.Name = "LueCurCode10";
            this.LueCurCode10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode10.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode10.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode10.Properties.DropDownRows = 12;
            this.LueCurCode10.Properties.NullText = "[Empty]";
            this.LueCurCode10.Properties.PopupWidth = 500;
            this.LueCurCode10.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode10.TabIndex = 71;
            this.LueCurCode10.ToolTip = "F4 : Show/hide list";
            this.LueCurCode10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode10.EditValueChanged += new System.EventHandler(this.LueCurCode10_EditValueChanged);
            this.LueCurCode10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode10_KeyDown);
            this.LueCurCode10.Leave += new System.EventHandler(this.LueCurCode10_Leave);
            this.LueCurCode10.Validated += new System.EventHandler(this.LueCurCode10_Validated);
            // 
            // LueUOMCode10
            // 
            this.LueUOMCode10.EnterMoveNextControl = true;
            this.LueUOMCode10.Location = new System.Drawing.Point(65, 69);
            this.LueUOMCode10.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode10.Name = "LueUOMCode10";
            this.LueUOMCode10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode10.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode10.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode10.Properties.DropDownRows = 12;
            this.LueUOMCode10.Properties.NullText = "[Empty]";
            this.LueUOMCode10.Properties.PopupWidth = 500;
            this.LueUOMCode10.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode10.TabIndex = 70;
            this.LueUOMCode10.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode10.EditValueChanged += new System.EventHandler(this.LueUOMCode10_EditValueChanged);
            this.LueUOMCode10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode10_KeyDown);
            this.LueUOMCode10.Leave += new System.EventHandler(this.LueUOMCode10_Leave);
            this.LueUOMCode10.Validated += new System.EventHandler(this.LueUOMCode10_Validated);
            // 
            // TxtSubTotal10
            // 
            this.TxtSubTotal10.EnterMoveNextControl = true;
            this.TxtSubTotal10.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal10.Name = "TxtSubTotal10";
            this.TxtSubTotal10.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal10.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal10.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal10.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal10.Properties.ReadOnly = true;
            this.TxtSubTotal10.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal10.TabIndex = 28;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(454, 15);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(60, 14);
            this.label41.TabIndex = 27;
            this.label41.Text = "Sub Total";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName10
            // 
            this.TxtGrpName10.EnterMoveNextControl = true;
            this.TxtGrpName10.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName10.Name = "TxtGrpName10";
            this.TxtGrpName10.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName10.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName10.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName10.Properties.ReadOnly = true;
            this.TxtGrpName10.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName10.TabIndex = 26;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(5, 15);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 14);
            this.label15.TabIndex = 25;
            this.label15.Text = "Group Name";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd10
            // 
            this.Grd10.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd10.DefaultRow.Height = 20;
            this.Grd10.DefaultRow.Sortable = false;
            this.Grd10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd10.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd10.Header.Height = 21;
            this.Grd10.Location = new System.Drawing.Point(0, 44);
            this.Grd10.Name = "Grd10";
            this.Grd10.RowHeader.Visible = true;
            this.Grd10.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd10.SingleClickEdit = true;
            this.Grd10.Size = new System.Drawing.Size(754, 283);
            this.Grd10.TabIndex = 21;
            this.Grd10.TreeCol = null;
            this.Grd10.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd10.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd10_EllipsisButtonClick);
            this.Grd10.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd10_RequestEdit);
            this.Grd10.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd10_AfterCommitEdit);
            this.Grd10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd10KeyDown);
            // 
            // K
            // 
            this.K.Controls.Add(this.LueCurCode11);
            this.K.Controls.Add(this.LueUOMCode11);
            this.K.Controls.Add(this.TxtSubTotal11);
            this.K.Controls.Add(this.label42);
            this.K.Controls.Add(this.TxtGrpName11);
            this.K.Controls.Add(this.label16);
            this.K.Controls.Add(this.Grd11);
            this.K.Controls.Add(this.panel14);
            this.K.Name = "K";
            this.K.Size = new System.Drawing.Size(754, 327);
            this.K.Text = "K";
            // 
            // LueCurCode11
            // 
            this.LueCurCode11.EnterMoveNextControl = true;
            this.LueCurCode11.Location = new System.Drawing.Point(263, 70);
            this.LueCurCode11.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode11.Name = "LueCurCode11";
            this.LueCurCode11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode11.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode11.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode11.Properties.DropDownRows = 12;
            this.LueCurCode11.Properties.NullText = "[Empty]";
            this.LueCurCode11.Properties.PopupWidth = 500;
            this.LueCurCode11.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode11.TabIndex = 71;
            this.LueCurCode11.ToolTip = "F4 : Show/hide list";
            this.LueCurCode11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode11.EditValueChanged += new System.EventHandler(this.LueCurCode11_EditValueChanged);
            this.LueCurCode11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode11_KeyDown);
            this.LueCurCode11.Leave += new System.EventHandler(this.LueCurCode11_Leave);
            this.LueCurCode11.Validated += new System.EventHandler(this.LueCurCode11_Validated);
            // 
            // LueUOMCode11
            // 
            this.LueUOMCode11.EnterMoveNextControl = true;
            this.LueUOMCode11.Location = new System.Drawing.Point(65, 70);
            this.LueUOMCode11.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode11.Name = "LueUOMCode11";
            this.LueUOMCode11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode11.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode11.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode11.Properties.DropDownRows = 12;
            this.LueUOMCode11.Properties.NullText = "[Empty]";
            this.LueUOMCode11.Properties.PopupWidth = 500;
            this.LueUOMCode11.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode11.TabIndex = 70;
            this.LueUOMCode11.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode11.EditValueChanged += new System.EventHandler(this.LueUOMCode11_EditValueChanged);
            this.LueUOMCode11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode11_KeyDown);
            this.LueUOMCode11.Leave += new System.EventHandler(this.LueUOMCode11_Leave);
            this.LueUOMCode11.Validated += new System.EventHandler(this.LueUOMCode11_Validated);
            // 
            // TxtSubTotal11
            // 
            this.TxtSubTotal11.EnterMoveNextControl = true;
            this.TxtSubTotal11.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal11.Name = "TxtSubTotal11";
            this.TxtSubTotal11.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal11.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal11.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal11.Properties.ReadOnly = true;
            this.TxtSubTotal11.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal11.TabIndex = 28;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(454, 15);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(60, 14);
            this.label42.TabIndex = 27;
            this.label42.Text = "Sub Total";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName11
            // 
            this.TxtGrpName11.EnterMoveNextControl = true;
            this.TxtGrpName11.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName11.Name = "TxtGrpName11";
            this.TxtGrpName11.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName11.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName11.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName11.Properties.ReadOnly = true;
            this.TxtGrpName11.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName11.TabIndex = 26;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(5, 15);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 14);
            this.label16.TabIndex = 25;
            this.label16.Text = "Group Name";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd11
            // 
            this.Grd11.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd11.DefaultRow.Height = 20;
            this.Grd11.DefaultRow.Sortable = false;
            this.Grd11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd11.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd11.Header.Height = 21;
            this.Grd11.Location = new System.Drawing.Point(0, 44);
            this.Grd11.Name = "Grd11";
            this.Grd11.RowHeader.Visible = true;
            this.Grd11.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd11.SingleClickEdit = true;
            this.Grd11.Size = new System.Drawing.Size(754, 283);
            this.Grd11.TabIndex = 21;
            this.Grd11.TreeCol = null;
            this.Grd11.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd11.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd11_EllipsisButtonClick);
            this.Grd11.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd11_RequestEdit);
            this.Grd11.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd11_AfterCommitEdit);
            this.Grd11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd11KeyDown);
            // 
            // L
            // 
            this.L.Controls.Add(this.LueCurCode12);
            this.L.Controls.Add(this.LueUOMCode12);
            this.L.Controls.Add(this.TxtSubTotal12);
            this.L.Controls.Add(this.label43);
            this.L.Controls.Add(this.TxtGrpName12);
            this.L.Controls.Add(this.label17);
            this.L.Controls.Add(this.Grd12);
            this.L.Controls.Add(this.panel15);
            this.L.Name = "L";
            this.L.Size = new System.Drawing.Size(754, 327);
            this.L.Text = "L";
            // 
            // LueCurCode12
            // 
            this.LueCurCode12.EnterMoveNextControl = true;
            this.LueCurCode12.Location = new System.Drawing.Point(263, 70);
            this.LueCurCode12.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode12.Name = "LueCurCode12";
            this.LueCurCode12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode12.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode12.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode12.Properties.DropDownRows = 12;
            this.LueCurCode12.Properties.NullText = "[Empty]";
            this.LueCurCode12.Properties.PopupWidth = 500;
            this.LueCurCode12.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode12.TabIndex = 71;
            this.LueCurCode12.ToolTip = "F4 : Show/hide list";
            this.LueCurCode12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode12.EditValueChanged += new System.EventHandler(this.LueCurCode12_EditValueChanged);
            this.LueCurCode12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode12_KeyDown);
            this.LueCurCode12.Leave += new System.EventHandler(this.LueCurCode12_Leave);
            this.LueCurCode12.Validated += new System.EventHandler(this.LueCurCode12_Validated);
            // 
            // LueUOMCode12
            // 
            this.LueUOMCode12.EnterMoveNextControl = true;
            this.LueUOMCode12.Location = new System.Drawing.Point(65, 70);
            this.LueUOMCode12.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode12.Name = "LueUOMCode12";
            this.LueUOMCode12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode12.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode12.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode12.Properties.DropDownRows = 12;
            this.LueUOMCode12.Properties.NullText = "[Empty]";
            this.LueUOMCode12.Properties.PopupWidth = 500;
            this.LueUOMCode12.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode12.TabIndex = 70;
            this.LueUOMCode12.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode12.EditValueChanged += new System.EventHandler(this.LueUOMCode12_EditValueChanged);
            this.LueUOMCode12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode12_KeyDown);
            this.LueUOMCode12.Leave += new System.EventHandler(this.LueUOMCode12_Leave);
            this.LueUOMCode12.Validated += new System.EventHandler(this.LueUOMCode12_Validated);
            // 
            // TxtSubTotal12
            // 
            this.TxtSubTotal12.EnterMoveNextControl = true;
            this.TxtSubTotal12.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal12.Name = "TxtSubTotal12";
            this.TxtSubTotal12.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal12.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal12.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal12.Properties.ReadOnly = true;
            this.TxtSubTotal12.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal12.TabIndex = 28;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(454, 15);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(60, 14);
            this.label43.TabIndex = 27;
            this.label43.Text = "Sub Total";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName12
            // 
            this.TxtGrpName12.EnterMoveNextControl = true;
            this.TxtGrpName12.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName12.Name = "TxtGrpName12";
            this.TxtGrpName12.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName12.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName12.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName12.Properties.ReadOnly = true;
            this.TxtGrpName12.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName12.TabIndex = 26;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(5, 15);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 14);
            this.label17.TabIndex = 25;
            this.label17.Text = "Group Name";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd12
            // 
            this.Grd12.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd12.DefaultRow.Height = 20;
            this.Grd12.DefaultRow.Sortable = false;
            this.Grd12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd12.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd12.Header.Height = 21;
            this.Grd12.Location = new System.Drawing.Point(0, 44);
            this.Grd12.Name = "Grd12";
            this.Grd12.RowHeader.Visible = true;
            this.Grd12.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd12.SingleClickEdit = true;
            this.Grd12.Size = new System.Drawing.Size(754, 283);
            this.Grd12.TabIndex = 21;
            this.Grd12.TreeCol = null;
            this.Grd12.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd12.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd12_EllipsisButtonClick);
            this.Grd12.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd12_RequestEdit);
            this.Grd12.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd12_AfterCommitEdit);
            this.Grd12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd12KeyDown);
            // 
            // M
            // 
            this.M.Controls.Add(this.LueCurCode13);
            this.M.Controls.Add(this.LueUOMCode13);
            this.M.Controls.Add(this.TxtSubTotal13);
            this.M.Controls.Add(this.label44);
            this.M.Controls.Add(this.TxtGrpName13);
            this.M.Controls.Add(this.label18);
            this.M.Controls.Add(this.Grd13);
            this.M.Controls.Add(this.panel16);
            this.M.Name = "M";
            this.M.Size = new System.Drawing.Size(754, 327);
            this.M.Text = "M";
            // 
            // LueCurCode13
            // 
            this.LueCurCode13.EnterMoveNextControl = true;
            this.LueCurCode13.Location = new System.Drawing.Point(263, 70);
            this.LueCurCode13.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode13.Name = "LueCurCode13";
            this.LueCurCode13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode13.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode13.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode13.Properties.DropDownRows = 12;
            this.LueCurCode13.Properties.NullText = "[Empty]";
            this.LueCurCode13.Properties.PopupWidth = 500;
            this.LueCurCode13.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode13.TabIndex = 71;
            this.LueCurCode13.ToolTip = "F4 : Show/hide list";
            this.LueCurCode13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode13.EditValueChanged += new System.EventHandler(this.LueCurCode13_EditValueChanged);
            this.LueCurCode13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode13_KeyDown);
            this.LueCurCode13.Leave += new System.EventHandler(this.LueCurCode13_Leave);
            this.LueCurCode13.Validated += new System.EventHandler(this.LueCurCode13_Validated);
            // 
            // LueUOMCode13
            // 
            this.LueUOMCode13.EnterMoveNextControl = true;
            this.LueUOMCode13.Location = new System.Drawing.Point(65, 70);
            this.LueUOMCode13.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode13.Name = "LueUOMCode13";
            this.LueUOMCode13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode13.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode13.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode13.Properties.DropDownRows = 12;
            this.LueUOMCode13.Properties.NullText = "[Empty]";
            this.LueUOMCode13.Properties.PopupWidth = 500;
            this.LueUOMCode13.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode13.TabIndex = 70;
            this.LueUOMCode13.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode13.EditValueChanged += new System.EventHandler(this.LueUOMCode13_EditValueChanged);
            this.LueUOMCode13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode13_KeyDown);
            this.LueUOMCode13.Leave += new System.EventHandler(this.LueUOMCode13_Leave);
            this.LueUOMCode13.Validated += new System.EventHandler(this.LueUOMCode13_Validated);
            // 
            // TxtSubTotal13
            // 
            this.TxtSubTotal13.EnterMoveNextControl = true;
            this.TxtSubTotal13.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal13.Name = "TxtSubTotal13";
            this.TxtSubTotal13.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal13.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal13.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal13.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal13.Properties.ReadOnly = true;
            this.TxtSubTotal13.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal13.TabIndex = 28;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(454, 15);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(60, 14);
            this.label44.TabIndex = 27;
            this.label44.Text = "Sub Total";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName13
            // 
            this.TxtGrpName13.EnterMoveNextControl = true;
            this.TxtGrpName13.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName13.Name = "TxtGrpName13";
            this.TxtGrpName13.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName13.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName13.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName13.Properties.ReadOnly = true;
            this.TxtGrpName13.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName13.TabIndex = 26;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(5, 15);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 14);
            this.label18.TabIndex = 25;
            this.label18.Text = "Group Name";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd13
            // 
            this.Grd13.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd13.DefaultRow.Height = 20;
            this.Grd13.DefaultRow.Sortable = false;
            this.Grd13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd13.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd13.Header.Height = 21;
            this.Grd13.Location = new System.Drawing.Point(0, 44);
            this.Grd13.Name = "Grd13";
            this.Grd13.RowHeader.Visible = true;
            this.Grd13.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd13.SingleClickEdit = true;
            this.Grd13.Size = new System.Drawing.Size(754, 283);
            this.Grd13.TabIndex = 21;
            this.Grd13.TreeCol = null;
            this.Grd13.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd13.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd13_EllipsisButtonClick);
            this.Grd13.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd13_RequestEdit);
            this.Grd13.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd13_AfterCommitEdit);
            this.Grd13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd13KeyDown);
            // 
            // N
            // 
            this.N.Controls.Add(this.LueCurCode14);
            this.N.Controls.Add(this.LueUOMCode14);
            this.N.Controls.Add(this.TxtSubTotal14);
            this.N.Controls.Add(this.label45);
            this.N.Controls.Add(this.TxtGrpName14);
            this.N.Controls.Add(this.label19);
            this.N.Controls.Add(this.Grd14);
            this.N.Controls.Add(this.panel17);
            this.N.Name = "N";
            this.N.Size = new System.Drawing.Size(754, 327);
            this.N.Text = "N";
            // 
            // LueCurCode14
            // 
            this.LueCurCode14.EnterMoveNextControl = true;
            this.LueCurCode14.Location = new System.Drawing.Point(263, 70);
            this.LueCurCode14.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode14.Name = "LueCurCode14";
            this.LueCurCode14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode14.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode14.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode14.Properties.DropDownRows = 12;
            this.LueCurCode14.Properties.NullText = "[Empty]";
            this.LueCurCode14.Properties.PopupWidth = 500;
            this.LueCurCode14.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode14.TabIndex = 71;
            this.LueCurCode14.ToolTip = "F4 : Show/hide list";
            this.LueCurCode14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode14.EditValueChanged += new System.EventHandler(this.LueCurCode14_EditValueChanged);
            this.LueCurCode14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode14_KeyDown);
            this.LueCurCode14.Leave += new System.EventHandler(this.LueCurCode14_Leave);
            this.LueCurCode14.Validated += new System.EventHandler(this.LueCurCode14_Validated);
            // 
            // LueUOMCode14
            // 
            this.LueUOMCode14.EnterMoveNextControl = true;
            this.LueUOMCode14.Location = new System.Drawing.Point(65, 70);
            this.LueUOMCode14.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode14.Name = "LueUOMCode14";
            this.LueUOMCode14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode14.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode14.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode14.Properties.DropDownRows = 12;
            this.LueUOMCode14.Properties.NullText = "[Empty]";
            this.LueUOMCode14.Properties.PopupWidth = 500;
            this.LueUOMCode14.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode14.TabIndex = 70;
            this.LueUOMCode14.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode14.EditValueChanged += new System.EventHandler(this.LueUOMCode14_EditValueChanged);
            this.LueUOMCode14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode14_KeyDown);
            this.LueUOMCode14.Leave += new System.EventHandler(this.LueUOMCode14_Leave);
            this.LueUOMCode14.Validated += new System.EventHandler(this.LueUOMCode14_Validated);
            // 
            // TxtSubTotal14
            // 
            this.TxtSubTotal14.EnterMoveNextControl = true;
            this.TxtSubTotal14.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal14.Name = "TxtSubTotal14";
            this.TxtSubTotal14.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal14.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal14.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal14.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal14.Properties.ReadOnly = true;
            this.TxtSubTotal14.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal14.TabIndex = 28;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(454, 15);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(60, 14);
            this.label45.TabIndex = 27;
            this.label45.Text = "Sub Total";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName14
            // 
            this.TxtGrpName14.EnterMoveNextControl = true;
            this.TxtGrpName14.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName14.Name = "TxtGrpName14";
            this.TxtGrpName14.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName14.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName14.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName14.Properties.ReadOnly = true;
            this.TxtGrpName14.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName14.TabIndex = 26;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(5, 15);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 14);
            this.label19.TabIndex = 25;
            this.label19.Text = "Group Name";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd14
            // 
            this.Grd14.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd14.DefaultRow.Height = 20;
            this.Grd14.DefaultRow.Sortable = false;
            this.Grd14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd14.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd14.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd14.Header.Height = 21;
            this.Grd14.Location = new System.Drawing.Point(0, 44);
            this.Grd14.Name = "Grd14";
            this.Grd14.RowHeader.Visible = true;
            this.Grd14.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd14.SingleClickEdit = true;
            this.Grd14.Size = new System.Drawing.Size(754, 283);
            this.Grd14.TabIndex = 21;
            this.Grd14.TreeCol = null;
            this.Grd14.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd14.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd14_EllipsisButtonClick);
            this.Grd14.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd14_RequestEdit);
            this.Grd14.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd14_AfterCommitEdit);
            this.Grd14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd14KeyDown);
            // 
            // O
            // 
            this.O.Controls.Add(this.LueCurCode15);
            this.O.Controls.Add(this.LueUOMCode15);
            this.O.Controls.Add(this.TxtSubTotal15);
            this.O.Controls.Add(this.label46);
            this.O.Controls.Add(this.TxtGrpName15);
            this.O.Controls.Add(this.label20);
            this.O.Controls.Add(this.Grd15);
            this.O.Controls.Add(this.panel18);
            this.O.Name = "O";
            this.O.Size = new System.Drawing.Size(754, 327);
            this.O.Text = "O";
            // 
            // LueCurCode15
            // 
            this.LueCurCode15.EnterMoveNextControl = true;
            this.LueCurCode15.Location = new System.Drawing.Point(263, 69);
            this.LueCurCode15.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode15.Name = "LueCurCode15";
            this.LueCurCode15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode15.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode15.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode15.Properties.DropDownRows = 12;
            this.LueCurCode15.Properties.NullText = "[Empty]";
            this.LueCurCode15.Properties.PopupWidth = 500;
            this.LueCurCode15.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode15.TabIndex = 71;
            this.LueCurCode15.ToolTip = "F4 : Show/hide list";
            this.LueCurCode15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode15.EditValueChanged += new System.EventHandler(this.LueCurCode15_EditValueChanged);
            this.LueCurCode15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode15_KeyDown);
            this.LueCurCode15.Leave += new System.EventHandler(this.LueCurCode15_Leave);
            this.LueCurCode15.Validated += new System.EventHandler(this.LueCurCode15_Validated);
            // 
            // LueUOMCode15
            // 
            this.LueUOMCode15.EnterMoveNextControl = true;
            this.LueUOMCode15.Location = new System.Drawing.Point(65, 69);
            this.LueUOMCode15.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode15.Name = "LueUOMCode15";
            this.LueUOMCode15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode15.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode15.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode15.Properties.DropDownRows = 12;
            this.LueUOMCode15.Properties.NullText = "[Empty]";
            this.LueUOMCode15.Properties.PopupWidth = 500;
            this.LueUOMCode15.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode15.TabIndex = 70;
            this.LueUOMCode15.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode15.EditValueChanged += new System.EventHandler(this.LueUOMCode15_EditValueChanged);
            this.LueUOMCode15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode15_KeyDown);
            this.LueUOMCode15.Leave += new System.EventHandler(this.LueUOMCode15_Leave);
            this.LueUOMCode15.Validated += new System.EventHandler(this.LueUOMCode15_Validated);
            // 
            // TxtSubTotal15
            // 
            this.TxtSubTotal15.EnterMoveNextControl = true;
            this.TxtSubTotal15.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal15.Name = "TxtSubTotal15";
            this.TxtSubTotal15.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal15.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal15.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal15.Properties.ReadOnly = true;
            this.TxtSubTotal15.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal15.TabIndex = 28;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(454, 15);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(60, 14);
            this.label46.TabIndex = 27;
            this.label46.Text = "Sub Total";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName15
            // 
            this.TxtGrpName15.EnterMoveNextControl = true;
            this.TxtGrpName15.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName15.Name = "TxtGrpName15";
            this.TxtGrpName15.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName15.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName15.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName15.Properties.ReadOnly = true;
            this.TxtGrpName15.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName15.TabIndex = 26;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(5, 15);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(75, 14);
            this.label20.TabIndex = 25;
            this.label20.Text = "Group Name";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd15
            // 
            this.Grd15.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd15.DefaultRow.Height = 20;
            this.Grd15.DefaultRow.Sortable = false;
            this.Grd15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd15.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd15.Header.Height = 21;
            this.Grd15.Location = new System.Drawing.Point(0, 44);
            this.Grd15.Name = "Grd15";
            this.Grd15.RowHeader.Visible = true;
            this.Grd15.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd15.SingleClickEdit = true;
            this.Grd15.Size = new System.Drawing.Size(754, 283);
            this.Grd15.TabIndex = 21;
            this.Grd15.TreeCol = null;
            this.Grd15.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd15.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd15_EllipsisButtonClick);
            this.Grd15.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd15_RequestEdit);
            this.Grd15.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd15_AfterCommitEdit);
            this.Grd15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd15KeyDown);
            // 
            // P
            // 
            this.P.Controls.Add(this.LueCurCode16);
            this.P.Controls.Add(this.LueUOMCode16);
            this.P.Controls.Add(this.TxtSubTotal16);
            this.P.Controls.Add(this.label47);
            this.P.Controls.Add(this.TxtGrpName16);
            this.P.Controls.Add(this.label21);
            this.P.Controls.Add(this.Grd16);
            this.P.Controls.Add(this.panel19);
            this.P.Name = "P";
            this.P.Size = new System.Drawing.Size(754, 327);
            this.P.Text = "P";
            // 
            // LueCurCode16
            // 
            this.LueCurCode16.EnterMoveNextControl = true;
            this.LueCurCode16.Location = new System.Drawing.Point(263, 69);
            this.LueCurCode16.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode16.Name = "LueCurCode16";
            this.LueCurCode16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode16.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode16.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode16.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode16.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode16.Properties.DropDownRows = 12;
            this.LueCurCode16.Properties.NullText = "[Empty]";
            this.LueCurCode16.Properties.PopupWidth = 500;
            this.LueCurCode16.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode16.TabIndex = 71;
            this.LueCurCode16.ToolTip = "F4 : Show/hide list";
            this.LueCurCode16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode16.EditValueChanged += new System.EventHandler(this.LueCurCode16_EditValueChanged);
            this.LueCurCode16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode16_KeyDown);
            this.LueCurCode16.Leave += new System.EventHandler(this.LueCurCode16_Leave);
            this.LueCurCode16.Validated += new System.EventHandler(this.LueCurCode16_Validated);
            // 
            // LueUOMCode16
            // 
            this.LueUOMCode16.EnterMoveNextControl = true;
            this.LueUOMCode16.Location = new System.Drawing.Point(65, 69);
            this.LueUOMCode16.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode16.Name = "LueUOMCode16";
            this.LueUOMCode16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode16.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode16.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode16.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode16.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode16.Properties.DropDownRows = 12;
            this.LueUOMCode16.Properties.NullText = "[Empty]";
            this.LueUOMCode16.Properties.PopupWidth = 500;
            this.LueUOMCode16.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode16.TabIndex = 70;
            this.LueUOMCode16.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode16.EditValueChanged += new System.EventHandler(this.LueUOMCode16_EditValueChanged);
            this.LueUOMCode16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode16_KeyDown);
            this.LueUOMCode16.Leave += new System.EventHandler(this.LueUOMCode16_Leave);
            this.LueUOMCode16.Validated += new System.EventHandler(this.LueUOMCode16_Validated);
            // 
            // TxtSubTotal16
            // 
            this.TxtSubTotal16.EnterMoveNextControl = true;
            this.TxtSubTotal16.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal16.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal16.Name = "TxtSubTotal16";
            this.TxtSubTotal16.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal16.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal16.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal16.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal16.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal16.Properties.ReadOnly = true;
            this.TxtSubTotal16.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal16.TabIndex = 28;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(454, 15);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(60, 14);
            this.label47.TabIndex = 27;
            this.label47.Text = "Sub Total";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName16
            // 
            this.TxtGrpName16.EnterMoveNextControl = true;
            this.TxtGrpName16.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName16.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName16.Name = "TxtGrpName16";
            this.TxtGrpName16.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName16.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName16.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName16.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName16.Properties.ReadOnly = true;
            this.TxtGrpName16.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName16.TabIndex = 26;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(5, 15);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(75, 14);
            this.label21.TabIndex = 25;
            this.label21.Text = "Group Name";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd16
            // 
            this.Grd16.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd16.DefaultRow.Height = 20;
            this.Grd16.DefaultRow.Sortable = false;
            this.Grd16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd16.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd16.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd16.Header.Height = 21;
            this.Grd16.Location = new System.Drawing.Point(0, 44);
            this.Grd16.Name = "Grd16";
            this.Grd16.RowHeader.Visible = true;
            this.Grd16.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd16.SingleClickEdit = true;
            this.Grd16.Size = new System.Drawing.Size(754, 283);
            this.Grd16.TabIndex = 21;
            this.Grd16.TreeCol = null;
            this.Grd16.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd16.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd16_EllipsisButtonClick);
            this.Grd16.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd16_RequestEdit);
            this.Grd16.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd16_AfterCommitEdit);
            this.Grd16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd16KeyDown);
            // 
            // Q
            // 
            this.Q.Controls.Add(this.LueCurCode17);
            this.Q.Controls.Add(this.LueUOMCode17);
            this.Q.Controls.Add(this.TxtSubTotal17);
            this.Q.Controls.Add(this.label48);
            this.Q.Controls.Add(this.TxtGrpName17);
            this.Q.Controls.Add(this.label22);
            this.Q.Controls.Add(this.Grd17);
            this.Q.Controls.Add(this.panel20);
            this.Q.Name = "Q";
            this.Q.Size = new System.Drawing.Size(754, 327);
            this.Q.Text = "Q";
            // 
            // LueCurCode17
            // 
            this.LueCurCode17.EnterMoveNextControl = true;
            this.LueCurCode17.Location = new System.Drawing.Point(263, 69);
            this.LueCurCode17.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode17.Name = "LueCurCode17";
            this.LueCurCode17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode17.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode17.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode17.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode17.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode17.Properties.DropDownRows = 12;
            this.LueCurCode17.Properties.NullText = "[Empty]";
            this.LueCurCode17.Properties.PopupWidth = 500;
            this.LueCurCode17.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode17.TabIndex = 71;
            this.LueCurCode17.ToolTip = "F4 : Show/hide list";
            this.LueCurCode17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode17.EditValueChanged += new System.EventHandler(this.LueCurCode17_EditValueChanged);
            this.LueCurCode17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode17_KeyDown);
            this.LueCurCode17.Leave += new System.EventHandler(this.LueCurCode17_Leave);
            this.LueCurCode17.Validated += new System.EventHandler(this.LueCurCode17_Validated);
            // 
            // LueUOMCode17
            // 
            this.LueUOMCode17.EnterMoveNextControl = true;
            this.LueUOMCode17.Location = new System.Drawing.Point(65, 69);
            this.LueUOMCode17.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode17.Name = "LueUOMCode17";
            this.LueUOMCode17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode17.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode17.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode17.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode17.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode17.Properties.DropDownRows = 12;
            this.LueUOMCode17.Properties.NullText = "[Empty]";
            this.LueUOMCode17.Properties.PopupWidth = 500;
            this.LueUOMCode17.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode17.TabIndex = 70;
            this.LueUOMCode17.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode17.EditValueChanged += new System.EventHandler(this.LueUOMCode17_EditValueChanged);
            this.LueUOMCode17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode17_KeyDown);
            this.LueUOMCode17.Leave += new System.EventHandler(this.LueUOMCode17_Leave);
            this.LueUOMCode17.Validated += new System.EventHandler(this.LueUOMCode17_Validated);
            // 
            // TxtSubTotal17
            // 
            this.TxtSubTotal17.EnterMoveNextControl = true;
            this.TxtSubTotal17.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal17.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal17.Name = "TxtSubTotal17";
            this.TxtSubTotal17.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal17.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal17.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal17.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal17.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal17.Properties.ReadOnly = true;
            this.TxtSubTotal17.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal17.TabIndex = 28;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(454, 15);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(60, 14);
            this.label48.TabIndex = 27;
            this.label48.Text = "Sub Total";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName17
            // 
            this.TxtGrpName17.EnterMoveNextControl = true;
            this.TxtGrpName17.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName17.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName17.Name = "TxtGrpName17";
            this.TxtGrpName17.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName17.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName17.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName17.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName17.Properties.ReadOnly = true;
            this.TxtGrpName17.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName17.TabIndex = 26;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(5, 15);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(75, 14);
            this.label22.TabIndex = 25;
            this.label22.Text = "Group Name";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd17
            // 
            this.Grd17.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd17.DefaultRow.Height = 20;
            this.Grd17.DefaultRow.Sortable = false;
            this.Grd17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd17.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd17.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd17.Header.Height = 21;
            this.Grd17.Location = new System.Drawing.Point(0, 44);
            this.Grd17.Name = "Grd17";
            this.Grd17.RowHeader.Visible = true;
            this.Grd17.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd17.SingleClickEdit = true;
            this.Grd17.Size = new System.Drawing.Size(754, 283);
            this.Grd17.TabIndex = 21;
            this.Grd17.TreeCol = null;
            this.Grd17.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd17.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd17_EllipsisButtonClick);
            this.Grd17.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd17_RequestEdit);
            this.Grd17.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd17_AfterCommitEdit);
            this.Grd17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd17KeyDown);
            // 
            // R
            // 
            this.R.Controls.Add(this.LueCurCode18);
            this.R.Controls.Add(this.LueUOMCode18);
            this.R.Controls.Add(this.TxtSubTotal18);
            this.R.Controls.Add(this.label49);
            this.R.Controls.Add(this.TxtGrpName18);
            this.R.Controls.Add(this.label23);
            this.R.Controls.Add(this.Grd18);
            this.R.Controls.Add(this.panel21);
            this.R.Name = "R";
            this.R.Size = new System.Drawing.Size(754, 327);
            this.R.Text = "R";
            // 
            // LueCurCode18
            // 
            this.LueCurCode18.EnterMoveNextControl = true;
            this.LueCurCode18.Location = new System.Drawing.Point(263, 71);
            this.LueCurCode18.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode18.Name = "LueCurCode18";
            this.LueCurCode18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode18.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode18.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode18.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode18.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode18.Properties.DropDownRows = 12;
            this.LueCurCode18.Properties.NullText = "[Empty]";
            this.LueCurCode18.Properties.PopupWidth = 500;
            this.LueCurCode18.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode18.TabIndex = 71;
            this.LueCurCode18.ToolTip = "F4 : Show/hide list";
            this.LueCurCode18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode18.EditValueChanged += new System.EventHandler(this.LueCurCode18_EditValueChanged);
            this.LueCurCode18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode18_KeyDown);
            this.LueCurCode18.Leave += new System.EventHandler(this.LueCurCode18_Leave);
            this.LueCurCode18.Validated += new System.EventHandler(this.LueCurCode18_Validated);
            // 
            // LueUOMCode18
            // 
            this.LueUOMCode18.EnterMoveNextControl = true;
            this.LueUOMCode18.Location = new System.Drawing.Point(65, 71);
            this.LueUOMCode18.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode18.Name = "LueUOMCode18";
            this.LueUOMCode18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode18.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode18.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode18.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode18.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode18.Properties.DropDownRows = 12;
            this.LueUOMCode18.Properties.NullText = "[Empty]";
            this.LueUOMCode18.Properties.PopupWidth = 500;
            this.LueUOMCode18.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode18.TabIndex = 70;
            this.LueUOMCode18.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode18.EditValueChanged += new System.EventHandler(this.LueUOMCode18_EditValueChanged);
            this.LueUOMCode18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode18_KeyDown);
            this.LueUOMCode18.Leave += new System.EventHandler(this.LueUOMCode18_Leave);
            this.LueUOMCode18.Validated += new System.EventHandler(this.LueUOMCode18_Validated);
            // 
            // TxtSubTotal18
            // 
            this.TxtSubTotal18.EnterMoveNextControl = true;
            this.TxtSubTotal18.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal18.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal18.Name = "TxtSubTotal18";
            this.TxtSubTotal18.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal18.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal18.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal18.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal18.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal18.Properties.ReadOnly = true;
            this.TxtSubTotal18.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal18.TabIndex = 28;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(454, 15);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(60, 14);
            this.label49.TabIndex = 27;
            this.label49.Text = "Sub Total";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName18
            // 
            this.TxtGrpName18.EnterMoveNextControl = true;
            this.TxtGrpName18.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName18.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName18.Name = "TxtGrpName18";
            this.TxtGrpName18.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName18.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName18.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName18.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName18.Properties.ReadOnly = true;
            this.TxtGrpName18.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName18.TabIndex = 26;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(5, 15);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 14);
            this.label23.TabIndex = 25;
            this.label23.Text = "Group Name";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd18
            // 
            this.Grd18.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd18.DefaultRow.Height = 20;
            this.Grd18.DefaultRow.Sortable = false;
            this.Grd18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd18.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd18.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd18.Header.Height = 21;
            this.Grd18.Location = new System.Drawing.Point(0, 44);
            this.Grd18.Name = "Grd18";
            this.Grd18.RowHeader.Visible = true;
            this.Grd18.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd18.SingleClickEdit = true;
            this.Grd18.Size = new System.Drawing.Size(754, 283);
            this.Grd18.TabIndex = 21;
            this.Grd18.TreeCol = null;
            this.Grd18.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd18.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd18_EllipsisButtonClick);
            this.Grd18.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd18_RequestEdit);
            this.Grd18.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd18_AfterCommitEdit);
            this.Grd18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd18KeyDown);
            // 
            // S
            // 
            this.S.Controls.Add(this.LueCurCode19);
            this.S.Controls.Add(this.LueUOMCode19);
            this.S.Controls.Add(this.TxtSubTotal19);
            this.S.Controls.Add(this.label50);
            this.S.Controls.Add(this.TxtGrpName19);
            this.S.Controls.Add(this.label24);
            this.S.Controls.Add(this.Grd19);
            this.S.Controls.Add(this.panel22);
            this.S.Name = "S";
            this.S.Size = new System.Drawing.Size(754, 327);
            this.S.Text = "S";
            // 
            // LueCurCode19
            // 
            this.LueCurCode19.EnterMoveNextControl = true;
            this.LueCurCode19.Location = new System.Drawing.Point(263, 70);
            this.LueCurCode19.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode19.Name = "LueCurCode19";
            this.LueCurCode19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode19.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode19.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode19.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode19.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode19.Properties.DropDownRows = 12;
            this.LueCurCode19.Properties.NullText = "[Empty]";
            this.LueCurCode19.Properties.PopupWidth = 500;
            this.LueCurCode19.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode19.TabIndex = 71;
            this.LueCurCode19.ToolTip = "F4 : Show/hide list";
            this.LueCurCode19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode19.EditValueChanged += new System.EventHandler(this.LueCurCode19_EditValueChanged);
            this.LueCurCode19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode19_KeyDown);
            this.LueCurCode19.Leave += new System.EventHandler(this.LueCurCode19_Leave);
            this.LueCurCode19.Validated += new System.EventHandler(this.LueCurCode19_Validated);
            // 
            // LueUOMCode19
            // 
            this.LueUOMCode19.EnterMoveNextControl = true;
            this.LueUOMCode19.Location = new System.Drawing.Point(65, 70);
            this.LueUOMCode19.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode19.Name = "LueUOMCode19";
            this.LueUOMCode19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode19.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode19.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode19.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode19.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode19.Properties.DropDownRows = 12;
            this.LueUOMCode19.Properties.NullText = "[Empty]";
            this.LueUOMCode19.Properties.PopupWidth = 500;
            this.LueUOMCode19.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode19.TabIndex = 70;
            this.LueUOMCode19.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode19.EditValueChanged += new System.EventHandler(this.LueUOMCode19_EditValueChanged);
            this.LueUOMCode19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode19_KeyDown);
            this.LueUOMCode19.Leave += new System.EventHandler(this.LueUOMCode19_Leave);
            this.LueUOMCode19.Validated += new System.EventHandler(this.LueUOMCode19_Validated);
            // 
            // TxtSubTotal19
            // 
            this.TxtSubTotal19.EnterMoveNextControl = true;
            this.TxtSubTotal19.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal19.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal19.Name = "TxtSubTotal19";
            this.TxtSubTotal19.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal19.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal19.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal19.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal19.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal19.Properties.ReadOnly = true;
            this.TxtSubTotal19.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal19.TabIndex = 28;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(454, 15);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(60, 14);
            this.label50.TabIndex = 27;
            this.label50.Text = "Sub Total";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName19
            // 
            this.TxtGrpName19.EnterMoveNextControl = true;
            this.TxtGrpName19.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName19.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName19.Name = "TxtGrpName19";
            this.TxtGrpName19.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName19.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName19.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName19.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName19.Properties.ReadOnly = true;
            this.TxtGrpName19.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName19.TabIndex = 26;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(5, 15);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(75, 14);
            this.label24.TabIndex = 25;
            this.label24.Text = "Group Name";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd19
            // 
            this.Grd19.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd19.DefaultRow.Height = 20;
            this.Grd19.DefaultRow.Sortable = false;
            this.Grd19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd19.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd19.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd19.Header.Height = 21;
            this.Grd19.Location = new System.Drawing.Point(0, 44);
            this.Grd19.Name = "Grd19";
            this.Grd19.RowHeader.Visible = true;
            this.Grd19.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd19.SingleClickEdit = true;
            this.Grd19.Size = new System.Drawing.Size(754, 283);
            this.Grd19.TabIndex = 21;
            this.Grd19.TreeCol = null;
            this.Grd19.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd19.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd19_EllipsisButtonClick);
            this.Grd19.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd19_RequestEdit);
            this.Grd19.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd19_AfterCommitEdit);
            this.Grd19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd19KeyDown);
            // 
            // T
            // 
            this.T.Controls.Add(this.LueCurCode20);
            this.T.Controls.Add(this.LueUOMCode20);
            this.T.Controls.Add(this.TxtSubTotal20);
            this.T.Controls.Add(this.label51);
            this.T.Controls.Add(this.TxtGrpName20);
            this.T.Controls.Add(this.label25);
            this.T.Controls.Add(this.Grd20);
            this.T.Controls.Add(this.panel23);
            this.T.Name = "T";
            this.T.Size = new System.Drawing.Size(754, 327);
            this.T.Text = "T";
            // 
            // LueCurCode20
            // 
            this.LueCurCode20.EnterMoveNextControl = true;
            this.LueCurCode20.Location = new System.Drawing.Point(263, 68);
            this.LueCurCode20.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode20.Name = "LueCurCode20";
            this.LueCurCode20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode20.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode20.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode20.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode20.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode20.Properties.DropDownRows = 12;
            this.LueCurCode20.Properties.NullText = "[Empty]";
            this.LueCurCode20.Properties.PopupWidth = 500;
            this.LueCurCode20.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode20.TabIndex = 71;
            this.LueCurCode20.ToolTip = "F4 : Show/hide list";
            this.LueCurCode20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode20.EditValueChanged += new System.EventHandler(this.LueCurCode20_EditValueChanged);
            this.LueCurCode20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode20_KeyDown);
            this.LueCurCode20.Leave += new System.EventHandler(this.LueCurCode20_Leave);
            this.LueCurCode20.Validated += new System.EventHandler(this.LueCurCode20_Validated);
            // 
            // LueUOMCode20
            // 
            this.LueUOMCode20.EnterMoveNextControl = true;
            this.LueUOMCode20.Location = new System.Drawing.Point(65, 68);
            this.LueUOMCode20.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode20.Name = "LueUOMCode20";
            this.LueUOMCode20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode20.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode20.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode20.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode20.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode20.Properties.DropDownRows = 12;
            this.LueUOMCode20.Properties.NullText = "[Empty]";
            this.LueUOMCode20.Properties.PopupWidth = 500;
            this.LueUOMCode20.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode20.TabIndex = 70;
            this.LueUOMCode20.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode20.EditValueChanged += new System.EventHandler(this.LueUOMCode20_EditValueChanged);
            this.LueUOMCode20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode20_KeyDown);
            this.LueUOMCode20.Leave += new System.EventHandler(this.LueUOMCode20_Leave);
            this.LueUOMCode20.Validated += new System.EventHandler(this.LueUOMCode20_Validated);
            // 
            // TxtSubTotal20
            // 
            this.TxtSubTotal20.EnterMoveNextControl = true;
            this.TxtSubTotal20.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal20.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal20.Name = "TxtSubTotal20";
            this.TxtSubTotal20.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal20.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal20.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal20.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal20.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal20.Properties.ReadOnly = true;
            this.TxtSubTotal20.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal20.TabIndex = 28;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(454, 15);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(60, 14);
            this.label51.TabIndex = 27;
            this.label51.Text = "Sub Total";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName20
            // 
            this.TxtGrpName20.EnterMoveNextControl = true;
            this.TxtGrpName20.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName20.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName20.Name = "TxtGrpName20";
            this.TxtGrpName20.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName20.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName20.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName20.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName20.Properties.ReadOnly = true;
            this.TxtGrpName20.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName20.TabIndex = 26;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(5, 15);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(75, 14);
            this.label25.TabIndex = 25;
            this.label25.Text = "Group Name";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd20
            // 
            this.Grd20.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd20.DefaultRow.Height = 20;
            this.Grd20.DefaultRow.Sortable = false;
            this.Grd20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd20.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd20.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd20.Header.Height = 21;
            this.Grd20.Location = new System.Drawing.Point(0, 44);
            this.Grd20.Name = "Grd20";
            this.Grd20.RowHeader.Visible = true;
            this.Grd20.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd20.SingleClickEdit = true;
            this.Grd20.Size = new System.Drawing.Size(754, 283);
            this.Grd20.TabIndex = 21;
            this.Grd20.TreeCol = null;
            this.Grd20.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd20.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd20_EllipsisButtonClick);
            this.Grd20.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd20_RequestEdit);
            this.Grd20.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd20_AfterCommitEdit);
            this.Grd20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd20KeyDown);
            // 
            // U
            // 
            this.U.Controls.Add(this.LueCurCode21);
            this.U.Controls.Add(this.LueUOMCode21);
            this.U.Controls.Add(this.TxtSubTotal21);
            this.U.Controls.Add(this.label52);
            this.U.Controls.Add(this.TxtGrpName21);
            this.U.Controls.Add(this.label26);
            this.U.Controls.Add(this.Grd21);
            this.U.Controls.Add(this.panel24);
            this.U.Name = "U";
            this.U.Size = new System.Drawing.Size(754, 327);
            this.U.Text = "U";
            // 
            // LueCurCode21
            // 
            this.LueCurCode21.EnterMoveNextControl = true;
            this.LueCurCode21.Location = new System.Drawing.Point(263, 68);
            this.LueCurCode21.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode21.Name = "LueCurCode21";
            this.LueCurCode21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode21.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode21.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode21.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode21.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode21.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode21.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode21.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode21.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode21.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode21.Properties.DropDownRows = 12;
            this.LueCurCode21.Properties.NullText = "[Empty]";
            this.LueCurCode21.Properties.PopupWidth = 500;
            this.LueCurCode21.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode21.TabIndex = 71;
            this.LueCurCode21.ToolTip = "F4 : Show/hide list";
            this.LueCurCode21.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode21.EditValueChanged += new System.EventHandler(this.LueCurCode21_EditValueChanged);
            this.LueCurCode21.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode21_KeyDown);
            this.LueCurCode21.Leave += new System.EventHandler(this.LueCurCode21_Leave);
            this.LueCurCode21.Validated += new System.EventHandler(this.LueCurCode21_Validated);
            // 
            // LueUOMCode21
            // 
            this.LueUOMCode21.EnterMoveNextControl = true;
            this.LueUOMCode21.Location = new System.Drawing.Point(65, 68);
            this.LueUOMCode21.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode21.Name = "LueUOMCode21";
            this.LueUOMCode21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode21.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode21.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode21.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode21.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode21.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode21.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode21.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode21.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode21.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode21.Properties.DropDownRows = 12;
            this.LueUOMCode21.Properties.NullText = "[Empty]";
            this.LueUOMCode21.Properties.PopupWidth = 500;
            this.LueUOMCode21.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode21.TabIndex = 70;
            this.LueUOMCode21.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode21.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode21.EditValueChanged += new System.EventHandler(this.LueUOMCode21_EditValueChanged);
            this.LueUOMCode21.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode21_KeyDown);
            this.LueUOMCode21.Leave += new System.EventHandler(this.LueUOMCode21_Leave);
            this.LueUOMCode21.Validated += new System.EventHandler(this.LueUOMCode21_Validated);
            // 
            // TxtSubTotal21
            // 
            this.TxtSubTotal21.EnterMoveNextControl = true;
            this.TxtSubTotal21.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal21.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal21.Name = "TxtSubTotal21";
            this.TxtSubTotal21.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal21.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal21.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal21.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal21.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal21.Properties.ReadOnly = true;
            this.TxtSubTotal21.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal21.TabIndex = 30;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(454, 15);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(60, 14);
            this.label52.TabIndex = 29;
            this.label52.Text = "Sub Total";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName21
            // 
            this.TxtGrpName21.EnterMoveNextControl = true;
            this.TxtGrpName21.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName21.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName21.Name = "TxtGrpName21";
            this.TxtGrpName21.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName21.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName21.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName21.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName21.Properties.ReadOnly = true;
            this.TxtGrpName21.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName21.TabIndex = 26;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(5, 15);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(75, 14);
            this.label26.TabIndex = 25;
            this.label26.Text = "Group Name";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd21
            // 
            this.Grd21.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd21.DefaultRow.Height = 20;
            this.Grd21.DefaultRow.Sortable = false;
            this.Grd21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd21.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd21.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd21.Header.Height = 21;
            this.Grd21.Location = new System.Drawing.Point(0, 44);
            this.Grd21.Name = "Grd21";
            this.Grd21.RowHeader.Visible = true;
            this.Grd21.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd21.SingleClickEdit = true;
            this.Grd21.Size = new System.Drawing.Size(754, 283);
            this.Grd21.TabIndex = 21;
            this.Grd21.TreeCol = null;
            this.Grd21.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd21.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd21_EllipsisButtonClick);
            this.Grd21.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd21_RequestEdit);
            this.Grd21.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd21_AfterCommitEdit);
            this.Grd21.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd21KeyDown);
            // 
            // V
            // 
            this.V.Controls.Add(this.LueCurCode22);
            this.V.Controls.Add(this.LueUOMCode22);
            this.V.Controls.Add(this.TxtSubTotal22);
            this.V.Controls.Add(this.label53);
            this.V.Controls.Add(this.TxtGrpName22);
            this.V.Controls.Add(this.label27);
            this.V.Controls.Add(this.Grd22);
            this.V.Controls.Add(this.panel25);
            this.V.Name = "V";
            this.V.Size = new System.Drawing.Size(754, 327);
            this.V.Text = "V";
            // 
            // LueCurCode22
            // 
            this.LueCurCode22.EnterMoveNextControl = true;
            this.LueCurCode22.Location = new System.Drawing.Point(263, 70);
            this.LueCurCode22.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode22.Name = "LueCurCode22";
            this.LueCurCode22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode22.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode22.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode22.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode22.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode22.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode22.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode22.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode22.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode22.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode22.Properties.DropDownRows = 12;
            this.LueCurCode22.Properties.NullText = "[Empty]";
            this.LueCurCode22.Properties.PopupWidth = 500;
            this.LueCurCode22.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode22.TabIndex = 71;
            this.LueCurCode22.ToolTip = "F4 : Show/hide list";
            this.LueCurCode22.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode22.EditValueChanged += new System.EventHandler(this.LueCurCode22_EditValueChanged);
            this.LueCurCode22.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode22_KeyDown);
            this.LueCurCode22.Leave += new System.EventHandler(this.LueCurCode22_Leave);
            this.LueCurCode22.Validated += new System.EventHandler(this.LueCurCode22_Validated);
            // 
            // LueUOMCode22
            // 
            this.LueUOMCode22.EnterMoveNextControl = true;
            this.LueUOMCode22.Location = new System.Drawing.Point(65, 70);
            this.LueUOMCode22.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode22.Name = "LueUOMCode22";
            this.LueUOMCode22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode22.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode22.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode22.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode22.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode22.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode22.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode22.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode22.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode22.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode22.Properties.DropDownRows = 12;
            this.LueUOMCode22.Properties.NullText = "[Empty]";
            this.LueUOMCode22.Properties.PopupWidth = 500;
            this.LueUOMCode22.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode22.TabIndex = 70;
            this.LueUOMCode22.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode22.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode22.EditValueChanged += new System.EventHandler(this.LueUOMCode22_EditValueChanged);
            this.LueUOMCode22.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode22_KeyDown);
            this.LueUOMCode22.Leave += new System.EventHandler(this.LueUOMCode22_Leave);
            this.LueUOMCode22.Validated += new System.EventHandler(this.LueUOMCode22_Validated);
            // 
            // TxtSubTotal22
            // 
            this.TxtSubTotal22.EnterMoveNextControl = true;
            this.TxtSubTotal22.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal22.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal22.Name = "TxtSubTotal22";
            this.TxtSubTotal22.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal22.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal22.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal22.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal22.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal22.Properties.ReadOnly = true;
            this.TxtSubTotal22.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal22.TabIndex = 30;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(454, 15);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(60, 14);
            this.label53.TabIndex = 29;
            this.label53.Text = "Sub Total";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName22
            // 
            this.TxtGrpName22.EnterMoveNextControl = true;
            this.TxtGrpName22.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName22.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName22.Name = "TxtGrpName22";
            this.TxtGrpName22.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName22.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName22.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName22.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName22.Properties.ReadOnly = true;
            this.TxtGrpName22.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName22.TabIndex = 26;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(5, 15);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(75, 14);
            this.label27.TabIndex = 25;
            this.label27.Text = "Group Name";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd22
            // 
            this.Grd22.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd22.DefaultRow.Height = 20;
            this.Grd22.DefaultRow.Sortable = false;
            this.Grd22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd22.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd22.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd22.Header.Height = 21;
            this.Grd22.Location = new System.Drawing.Point(0, 44);
            this.Grd22.Name = "Grd22";
            this.Grd22.RowHeader.Visible = true;
            this.Grd22.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd22.SingleClickEdit = true;
            this.Grd22.Size = new System.Drawing.Size(754, 283);
            this.Grd22.TabIndex = 21;
            this.Grd22.TreeCol = null;
            this.Grd22.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd22.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd22_EllipsisButtonClick);
            this.Grd22.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd22_RequestEdit);
            this.Grd22.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd22_AfterCommitEdit);
            this.Grd22.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd22KeyDown);
            // 
            // W
            // 
            this.W.Controls.Add(this.LueCurCode23);
            this.W.Controls.Add(this.LueUOMCode23);
            this.W.Controls.Add(this.TxtSubTotal23);
            this.W.Controls.Add(this.label54);
            this.W.Controls.Add(this.TxtGrpName23);
            this.W.Controls.Add(this.label28);
            this.W.Controls.Add(this.Grd23);
            this.W.Controls.Add(this.panel26);
            this.W.Name = "W";
            this.W.Size = new System.Drawing.Size(754, 327);
            this.W.Text = "W";
            // 
            // LueCurCode23
            // 
            this.LueCurCode23.EnterMoveNextControl = true;
            this.LueCurCode23.Location = new System.Drawing.Point(263, 69);
            this.LueCurCode23.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode23.Name = "LueCurCode23";
            this.LueCurCode23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode23.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode23.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode23.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode23.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode23.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode23.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode23.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode23.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode23.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode23.Properties.DropDownRows = 12;
            this.LueCurCode23.Properties.NullText = "[Empty]";
            this.LueCurCode23.Properties.PopupWidth = 500;
            this.LueCurCode23.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode23.TabIndex = 71;
            this.LueCurCode23.ToolTip = "F4 : Show/hide list";
            this.LueCurCode23.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode23.EditValueChanged += new System.EventHandler(this.LueCurCode23_EditValueChanged);
            this.LueCurCode23.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode23_KeyDown);
            this.LueCurCode23.Leave += new System.EventHandler(this.LueCurCode23_Leave);
            this.LueCurCode23.Validated += new System.EventHandler(this.LueCurCode23_Validated);
            // 
            // LueUOMCode23
            // 
            this.LueUOMCode23.EnterMoveNextControl = true;
            this.LueUOMCode23.Location = new System.Drawing.Point(65, 69);
            this.LueUOMCode23.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode23.Name = "LueUOMCode23";
            this.LueUOMCode23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode23.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode23.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode23.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode23.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode23.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode23.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode23.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode23.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode23.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode23.Properties.DropDownRows = 12;
            this.LueUOMCode23.Properties.NullText = "[Empty]";
            this.LueUOMCode23.Properties.PopupWidth = 500;
            this.LueUOMCode23.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode23.TabIndex = 70;
            this.LueUOMCode23.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode23.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode23.EditValueChanged += new System.EventHandler(this.LueUOMCode23_EditValueChanged);
            this.LueUOMCode23.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode23_KeyDown);
            this.LueUOMCode23.Leave += new System.EventHandler(this.LueUOMCode23_Leave);
            this.LueUOMCode23.Validated += new System.EventHandler(this.LueUOMCode23_Validated);
            // 
            // TxtSubTotal23
            // 
            this.TxtSubTotal23.EnterMoveNextControl = true;
            this.TxtSubTotal23.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal23.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal23.Name = "TxtSubTotal23";
            this.TxtSubTotal23.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal23.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal23.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal23.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal23.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal23.Properties.ReadOnly = true;
            this.TxtSubTotal23.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal23.TabIndex = 30;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(454, 15);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(60, 14);
            this.label54.TabIndex = 29;
            this.label54.Text = "Sub Total";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName23
            // 
            this.TxtGrpName23.EnterMoveNextControl = true;
            this.TxtGrpName23.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName23.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName23.Name = "TxtGrpName23";
            this.TxtGrpName23.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName23.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName23.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName23.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName23.Properties.ReadOnly = true;
            this.TxtGrpName23.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName23.TabIndex = 26;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(5, 15);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(75, 14);
            this.label28.TabIndex = 25;
            this.label28.Text = "Group Name";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd23
            // 
            this.Grd23.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd23.DefaultRow.Height = 20;
            this.Grd23.DefaultRow.Sortable = false;
            this.Grd23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd23.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd23.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd23.Header.Height = 21;
            this.Grd23.Location = new System.Drawing.Point(0, 44);
            this.Grd23.Name = "Grd23";
            this.Grd23.RowHeader.Visible = true;
            this.Grd23.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd23.SingleClickEdit = true;
            this.Grd23.Size = new System.Drawing.Size(754, 283);
            this.Grd23.TabIndex = 21;
            this.Grd23.TreeCol = null;
            this.Grd23.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd23.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd23_EllipsisButtonClick);
            this.Grd23.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd23_RequestEdit);
            this.Grd23.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd23_AfterCommitEdit);
            this.Grd23.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd23KeyDown);
            // 
            // X
            // 
            this.X.Controls.Add(this.LueCurCode24);
            this.X.Controls.Add(this.LueUOMCode24);
            this.X.Controls.Add(this.TxtSubTotal24);
            this.X.Controls.Add(this.label55);
            this.X.Controls.Add(this.TxtGrpName24);
            this.X.Controls.Add(this.label29);
            this.X.Controls.Add(this.Grd24);
            this.X.Controls.Add(this.panel27);
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(754, 327);
            this.X.Text = "X";
            // 
            // LueCurCode24
            // 
            this.LueCurCode24.EnterMoveNextControl = true;
            this.LueCurCode24.Location = new System.Drawing.Point(263, 69);
            this.LueCurCode24.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode24.Name = "LueCurCode24";
            this.LueCurCode24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode24.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode24.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode24.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode24.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode24.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode24.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode24.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode24.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode24.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode24.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode24.Properties.DropDownRows = 12;
            this.LueCurCode24.Properties.NullText = "[Empty]";
            this.LueCurCode24.Properties.PopupWidth = 500;
            this.LueCurCode24.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode24.TabIndex = 71;
            this.LueCurCode24.ToolTip = "F4 : Show/hide list";
            this.LueCurCode24.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode24.EditValueChanged += new System.EventHandler(this.LueCurCode24_EditValueChanged);
            this.LueCurCode24.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode24_KeyDown);
            this.LueCurCode24.Leave += new System.EventHandler(this.LueCurCode24_Leave);
            this.LueCurCode24.Validated += new System.EventHandler(this.LueCurCode24_Validated);
            // 
            // LueUOMCode24
            // 
            this.LueUOMCode24.EnterMoveNextControl = true;
            this.LueUOMCode24.Location = new System.Drawing.Point(65, 69);
            this.LueUOMCode24.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode24.Name = "LueUOMCode24";
            this.LueUOMCode24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode24.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode24.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode24.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode24.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode24.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode24.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode24.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode24.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode24.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode24.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode24.Properties.DropDownRows = 12;
            this.LueUOMCode24.Properties.NullText = "[Empty]";
            this.LueUOMCode24.Properties.PopupWidth = 500;
            this.LueUOMCode24.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode24.TabIndex = 70;
            this.LueUOMCode24.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode24.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode24.EditValueChanged += new System.EventHandler(this.LueUOMCode24_EditValueChanged);
            this.LueUOMCode24.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode24_KeyDown);
            this.LueUOMCode24.Leave += new System.EventHandler(this.LueUOMCode24_Leave);
            this.LueUOMCode24.Validated += new System.EventHandler(this.LueUOMCode24_Validated);
            // 
            // TxtSubTotal24
            // 
            this.TxtSubTotal24.EnterMoveNextControl = true;
            this.TxtSubTotal24.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal24.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal24.Name = "TxtSubTotal24";
            this.TxtSubTotal24.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal24.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal24.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal24.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal24.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal24.Properties.ReadOnly = true;
            this.TxtSubTotal24.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal24.TabIndex = 30;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(454, 15);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(60, 14);
            this.label55.TabIndex = 29;
            this.label55.Text = "Sub Total";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName24
            // 
            this.TxtGrpName24.EnterMoveNextControl = true;
            this.TxtGrpName24.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName24.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName24.Name = "TxtGrpName24";
            this.TxtGrpName24.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName24.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName24.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName24.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName24.Properties.ReadOnly = true;
            this.TxtGrpName24.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName24.TabIndex = 26;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(5, 15);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(75, 14);
            this.label29.TabIndex = 25;
            this.label29.Text = "Group Name";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd24
            // 
            this.Grd24.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd24.DefaultRow.Height = 20;
            this.Grd24.DefaultRow.Sortable = false;
            this.Grd24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd24.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd24.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd24.Header.Height = 21;
            this.Grd24.Location = new System.Drawing.Point(0, 44);
            this.Grd24.Name = "Grd24";
            this.Grd24.RowHeader.Visible = true;
            this.Grd24.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd24.SingleClickEdit = true;
            this.Grd24.Size = new System.Drawing.Size(754, 283);
            this.Grd24.TabIndex = 21;
            this.Grd24.TreeCol = null;
            this.Grd24.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd24.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd24_EllipsisButtonClick);
            this.Grd24.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd24_RequestEdit);
            this.Grd24.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd24_AfterCommitEdit);
            this.Grd24.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd24KeyDown);
            // 
            // Y
            // 
            this.Y.Controls.Add(this.LueCurCode25);
            this.Y.Controls.Add(this.LueUOMCode25);
            this.Y.Controls.Add(this.TxtSubTotal25);
            this.Y.Controls.Add(this.label56);
            this.Y.Controls.Add(this.TxtGrpName25);
            this.Y.Controls.Add(this.label30);
            this.Y.Controls.Add(this.Grd25);
            this.Y.Controls.Add(this.panel28);
            this.Y.Name = "Y";
            this.Y.Size = new System.Drawing.Size(754, 327);
            this.Y.Text = "Y";
            // 
            // LueCurCode25
            // 
            this.LueCurCode25.EnterMoveNextControl = true;
            this.LueCurCode25.Location = new System.Drawing.Point(263, 70);
            this.LueCurCode25.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode25.Name = "LueCurCode25";
            this.LueCurCode25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode25.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode25.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode25.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode25.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode25.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode25.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode25.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode25.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode25.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode25.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode25.Properties.DropDownRows = 12;
            this.LueCurCode25.Properties.NullText = "[Empty]";
            this.LueCurCode25.Properties.PopupWidth = 500;
            this.LueCurCode25.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode25.TabIndex = 71;
            this.LueCurCode25.ToolTip = "F4 : Show/hide list";
            this.LueCurCode25.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode25.EditValueChanged += new System.EventHandler(this.LueCurCode25_EditValueChanged);
            this.LueCurCode25.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode25_KeyDown);
            this.LueCurCode25.Leave += new System.EventHandler(this.LueCurCode25_Leave);
            this.LueCurCode25.Validated += new System.EventHandler(this.LueCurCode25_Validated);
            // 
            // LueUOMCode25
            // 
            this.LueUOMCode25.EnterMoveNextControl = true;
            this.LueUOMCode25.Location = new System.Drawing.Point(65, 70);
            this.LueUOMCode25.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode25.Name = "LueUOMCode25";
            this.LueUOMCode25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode25.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode25.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode25.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode25.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode25.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode25.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode25.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode25.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode25.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode25.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode25.Properties.DropDownRows = 12;
            this.LueUOMCode25.Properties.NullText = "[Empty]";
            this.LueUOMCode25.Properties.PopupWidth = 500;
            this.LueUOMCode25.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode25.TabIndex = 70;
            this.LueUOMCode25.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode25.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode25.EditValueChanged += new System.EventHandler(this.LueUOMCode25_EditValueChanged);
            this.LueUOMCode25.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode25_KeyDown);
            this.LueUOMCode25.Leave += new System.EventHandler(this.LueUOMCode25_Leave);
            this.LueUOMCode25.Validated += new System.EventHandler(this.LueUOMCode25_Validated);
            // 
            // TxtSubTotal25
            // 
            this.TxtSubTotal25.EnterMoveNextControl = true;
            this.TxtSubTotal25.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal25.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal25.Name = "TxtSubTotal25";
            this.TxtSubTotal25.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal25.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal25.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal25.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal25.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal25.Properties.ReadOnly = true;
            this.TxtSubTotal25.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal25.TabIndex = 30;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(454, 15);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(60, 14);
            this.label56.TabIndex = 29;
            this.label56.Text = "Sub Total";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName25
            // 
            this.TxtGrpName25.EnterMoveNextControl = true;
            this.TxtGrpName25.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName25.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName25.Name = "TxtGrpName25";
            this.TxtGrpName25.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName25.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName25.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName25.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName25.Properties.ReadOnly = true;
            this.TxtGrpName25.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName25.TabIndex = 28;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(5, 15);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(75, 14);
            this.label30.TabIndex = 27;
            this.label30.Text = "Group Name";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd25
            // 
            this.Grd25.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd25.DefaultRow.Height = 20;
            this.Grd25.DefaultRow.Sortable = false;
            this.Grd25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd25.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd25.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd25.Header.Height = 21;
            this.Grd25.Location = new System.Drawing.Point(0, 44);
            this.Grd25.Name = "Grd25";
            this.Grd25.RowHeader.Visible = true;
            this.Grd25.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd25.SingleClickEdit = true;
            this.Grd25.Size = new System.Drawing.Size(754, 283);
            this.Grd25.TabIndex = 21;
            this.Grd25.TreeCol = null;
            this.Grd25.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd25.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd25_EllipsisButtonClick);
            this.Grd25.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd25_RequestEdit);
            this.Grd25.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd25_AfterCommitEdit);
            this.Grd25.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd25KeyDown);
            // 
            // Z
            // 
            this.Z.Controls.Add(this.LueCurCode26);
            this.Z.Controls.Add(this.LueUOMCode26);
            this.Z.Controls.Add(this.TxtSubTotal26);
            this.Z.Controls.Add(this.label57);
            this.Z.Controls.Add(this.TxtGrpName26);
            this.Z.Controls.Add(this.label31);
            this.Z.Controls.Add(this.Grd26);
            this.Z.Controls.Add(this.panel29);
            this.Z.Name = "Z";
            this.Z.Size = new System.Drawing.Size(754, 327);
            this.Z.Text = "Z";
            // 
            // LueCurCode26
            // 
            this.LueCurCode26.EnterMoveNextControl = true;
            this.LueCurCode26.Location = new System.Drawing.Point(263, 69);
            this.LueCurCode26.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode26.Name = "LueCurCode26";
            this.LueCurCode26.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode26.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode26.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode26.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode26.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode26.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode26.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode26.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode26.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode26.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode26.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode26.Properties.DropDownRows = 12;
            this.LueCurCode26.Properties.NullText = "[Empty]";
            this.LueCurCode26.Properties.PopupWidth = 500;
            this.LueCurCode26.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode26.TabIndex = 71;
            this.LueCurCode26.ToolTip = "F4 : Show/hide list";
            this.LueCurCode26.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode26.EditValueChanged += new System.EventHandler(this.LueCurCode26_EditValueChanged);
            this.LueCurCode26.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode26_KeyDown);
            this.LueCurCode26.Leave += new System.EventHandler(this.LueCurCode26_Leave);
            this.LueCurCode26.Validated += new System.EventHandler(this.LueCurCode26_Validated);
            // 
            // LueUOMCode26
            // 
            this.LueUOMCode26.EnterMoveNextControl = true;
            this.LueUOMCode26.Location = new System.Drawing.Point(65, 69);
            this.LueUOMCode26.Margin = new System.Windows.Forms.Padding(5);
            this.LueUOMCode26.Name = "LueUOMCode26";
            this.LueUOMCode26.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode26.Properties.Appearance.Options.UseFont = true;
            this.LueUOMCode26.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode26.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUOMCode26.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode26.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUOMCode26.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode26.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUOMCode26.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUOMCode26.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUOMCode26.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUOMCode26.Properties.DropDownRows = 12;
            this.LueUOMCode26.Properties.NullText = "[Empty]";
            this.LueUOMCode26.Properties.PopupWidth = 500;
            this.LueUOMCode26.Size = new System.Drawing.Size(171, 20);
            this.LueUOMCode26.TabIndex = 70;
            this.LueUOMCode26.ToolTip = "F4 : Show/hide list";
            this.LueUOMCode26.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUOMCode26.EditValueChanged += new System.EventHandler(this.LueUOMCode26_EditValueChanged);
            this.LueUOMCode26.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUOMCode26_KeyDown);
            this.LueUOMCode26.Leave += new System.EventHandler(this.LueUOMCode26_Leave);
            this.LueUOMCode26.Validated += new System.EventHandler(this.LueUOMCode26_Validated);
            // 
            // TxtSubTotal26
            // 
            this.TxtSubTotal26.EnterMoveNextControl = true;
            this.TxtSubTotal26.Location = new System.Drawing.Point(517, 12);
            this.TxtSubTotal26.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal26.Name = "TxtSubTotal26";
            this.TxtSubTotal26.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal26.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal26.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal26.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal26.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal26.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal26.Properties.ReadOnly = true;
            this.TxtSubTotal26.Size = new System.Drawing.Size(201, 20);
            this.TxtSubTotal26.TabIndex = 30;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(454, 15);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(60, 14);
            this.label57.TabIndex = 29;
            this.label57.Text = "Sub Total";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrpName26
            // 
            this.TxtGrpName26.EnterMoveNextControl = true;
            this.TxtGrpName26.Location = new System.Drawing.Point(82, 12);
            this.TxtGrpName26.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrpName26.Name = "TxtGrpName26";
            this.TxtGrpName26.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrpName26.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrpName26.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrpName26.Properties.Appearance.Options.UseFont = true;
            this.TxtGrpName26.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrpName26.Properties.ReadOnly = true;
            this.TxtGrpName26.Size = new System.Drawing.Size(154, 20);
            this.TxtGrpName26.TabIndex = 28;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(5, 15);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(75, 14);
            this.label31.TabIndex = 27;
            this.label31.Text = "Group Name";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd26
            // 
            this.Grd26.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd26.DefaultRow.Height = 20;
            this.Grd26.DefaultRow.Sortable = false;
            this.Grd26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd26.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd26.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd26.Header.Height = 21;
            this.Grd26.Location = new System.Drawing.Point(0, 44);
            this.Grd26.Name = "Grd26";
            this.Grd26.RowHeader.Visible = true;
            this.Grd26.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd26.SingleClickEdit = true;
            this.Grd26.Size = new System.Drawing.Size(754, 283);
            this.Grd26.TabIndex = 21;
            this.Grd26.TreeCol = null;
            this.Grd26.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd26.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd26_EllipsisButtonClick);
            this.Grd26.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd26_RequestEdit);
            this.Grd26.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd26_AfterCommitEdit);
            this.Grd26.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd26KeyDown);
            // 
            // panel8
            // 
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(754, 44);
            this.panel8.TabIndex = 0;
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(754, 44);
            this.panel9.TabIndex = 72;
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(754, 44);
            this.panel10.TabIndex = 72;
            // 
            // panel11
            // 
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(754, 44);
            this.panel11.TabIndex = 72;
            // 
            // panel12
            // 
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(754, 44);
            this.panel12.TabIndex = 72;
            // 
            // panel13
            // 
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(754, 44);
            this.panel13.TabIndex = 72;
            // 
            // panel14
            // 
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(754, 44);
            this.panel14.TabIndex = 72;
            // 
            // panel15
            // 
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(754, 44);
            this.panel15.TabIndex = 72;
            // 
            // panel16
            // 
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(754, 44);
            this.panel16.TabIndex = 72;
            // 
            // panel17
            // 
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(754, 44);
            this.panel17.TabIndex = 72;
            // 
            // panel18
            // 
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(754, 44);
            this.panel18.TabIndex = 72;
            // 
            // panel19
            // 
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(754, 44);
            this.panel19.TabIndex = 72;
            // 
            // panel20
            // 
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(754, 44);
            this.panel20.TabIndex = 72;
            // 
            // panel21
            // 
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(754, 44);
            this.panel21.TabIndex = 72;
            // 
            // panel22
            // 
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(754, 44);
            this.panel22.TabIndex = 72;
            // 
            // panel23
            // 
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(754, 44);
            this.panel23.TabIndex = 72;
            // 
            // panel24
            // 
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(754, 44);
            this.panel24.TabIndex = 72;
            // 
            // panel25
            // 
            this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(754, 44);
            this.panel25.TabIndex = 72;
            // 
            // panel26
            // 
            this.panel26.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel26.Location = new System.Drawing.Point(0, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(754, 44);
            this.panel26.TabIndex = 72;
            // 
            // panel27
            // 
            this.panel27.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel27.Location = new System.Drawing.Point(0, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(754, 44);
            this.panel27.TabIndex = 72;
            // 
            // panel28
            // 
            this.panel28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel28.Location = new System.Drawing.Point(0, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(754, 44);
            this.panel28.TabIndex = 72;
            // 
            // panel29
            // 
            this.panel29.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(754, 44);
            this.panel29.TabIndex = 72;
            // 
            // FrmMaterialRequest5Dlg7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Controls.Add(this.Tc1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Name = "FrmMaterialRequest5Dlg7";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "BOQ";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.Tc1, 0);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEstPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.A.ResumeLayout(false);
            this.A.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal1.Properties)).EndInit();
            this.B.ResumeLayout(false);
            this.B.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.C.ResumeLayout(false);
            this.C.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.D.ResumeLayout(false);
            this.D.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.E.ResumeLayout(false);
            this.E.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.panel7.ResumeLayout(false);
            this.F.ResumeLayout(false);
            this.F.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.G.ResumeLayout(false);
            this.G.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.H.ResumeLayout(false);
            this.H.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).EndInit();
            this.I.ResumeLayout(false);
            this.I.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).EndInit();
            this.J.ResumeLayout(false);
            this.J.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd10)).EndInit();
            this.K.ResumeLayout(false);
            this.K.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).EndInit();
            this.L.ResumeLayout(false);
            this.L.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).EndInit();
            this.M.ResumeLayout(false);
            this.M.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).EndInit();
            this.N.ResumeLayout(false);
            this.N.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).EndInit();
            this.O.ResumeLayout(false);
            this.O.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).EndInit();
            this.P.ResumeLayout(false);
            this.P.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).EndInit();
            this.Q.ResumeLayout(false);
            this.Q.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).EndInit();
            this.R.ResumeLayout(false);
            this.R.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd18)).EndInit();
            this.S.ResumeLayout(false);
            this.S.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd19)).EndInit();
            this.T.ResumeLayout(false);
            this.T.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd20)).EndInit();
            this.U.ResumeLayout(false);
            this.U.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd21)).EndInit();
            this.V.ResumeLayout(false);
            this.V.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd22)).EndInit();
            this.W.ResumeLayout(false);
            this.W.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd23)).EndInit();
            this.X.ResumeLayout(false);
            this.X.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd24)).EndInit();
            this.Y.ResumeLayout(false);
            this.Y.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd25)).EndInit();
            this.Z.ResumeLayout(false);
            this.Z.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUOMCode26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrpName26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd26)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        internal DevExpress.XtraEditors.TextEdit TxtEstPrice;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtCurCode;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtItName;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtLocation;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage A;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private DevExpress.XtraTab.XtraTabPage B;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraTab.XtraTabPage C;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private DevExpress.XtraTab.XtraTabPage D;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private DevExpress.XtraTab.XtraTabPage E;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        private DevExpress.XtraTab.XtraTabPage F;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        private DevExpress.XtraTab.XtraTabPage G;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        private DevExpress.XtraTab.XtraTabPage H;
        protected internal TenTec.Windows.iGridLib.iGrid Grd8;
        private DevExpress.XtraTab.XtraTabPage I;
        protected internal TenTec.Windows.iGridLib.iGrid Grd9;
        private DevExpress.XtraTab.XtraTabPage J;
        protected internal TenTec.Windows.iGridLib.iGrid Grd10;
        private DevExpress.XtraTab.XtraTabPage K;
        protected internal TenTec.Windows.iGridLib.iGrid Grd11;
        private DevExpress.XtraTab.XtraTabPage L;
        protected internal TenTec.Windows.iGridLib.iGrid Grd12;
        private DevExpress.XtraTab.XtraTabPage M;
        protected internal TenTec.Windows.iGridLib.iGrid Grd13;
        private DevExpress.XtraTab.XtraTabPage N;
        protected internal TenTec.Windows.iGridLib.iGrid Grd14;
        private DevExpress.XtraTab.XtraTabPage O;
        protected internal TenTec.Windows.iGridLib.iGrid Grd15;
        private DevExpress.XtraTab.XtraTabPage P;
        protected internal TenTec.Windows.iGridLib.iGrid Grd16;
        private DevExpress.XtraTab.XtraTabPage Q;
        protected internal TenTec.Windows.iGridLib.iGrid Grd17;
        private DevExpress.XtraTab.XtraTabPage R;
        protected internal TenTec.Windows.iGridLib.iGrid Grd18;
        private DevExpress.XtraTab.XtraTabPage S;
        protected internal TenTec.Windows.iGridLib.iGrid Grd19;
        private DevExpress.XtraTab.XtraTabPage T;
        protected internal TenTec.Windows.iGridLib.iGrid Grd20;
        private DevExpress.XtraTab.XtraTabPage U;
        protected internal TenTec.Windows.iGridLib.iGrid Grd21;
        private DevExpress.XtraTab.XtraTabPage V;
        protected internal TenTec.Windows.iGridLib.iGrid Grd22;
        private DevExpress.XtraTab.XtraTabPage W;
        protected internal TenTec.Windows.iGridLib.iGrid Grd23;
        private DevExpress.XtraTab.XtraTabPage X;
        protected internal TenTec.Windows.iGridLib.iGrid Grd24;
        private DevExpress.XtraTab.XtraTabPage Y;
        protected internal TenTec.Windows.iGridLib.iGrid Grd25;
        private DevExpress.XtraTab.XtraTabPage Z;
        protected internal TenTec.Windows.iGridLib.iGrid Grd26;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName1;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName2;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName3;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName4;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName5;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName6;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName7;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName8;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName9;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName10;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName11;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName12;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName13;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName14;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName15;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName16;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName17;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName18;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName19;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName20;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName21;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName22;
        private System.Windows.Forms.Label label27;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName23;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName24;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName25;
        private System.Windows.Forms.Label label30;
        internal DevExpress.XtraEditors.TextEdit TxtGrpName26;
        private System.Windows.Forms.Label label31;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal1;
        private System.Windows.Forms.Label label32;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal2;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal3;
        private System.Windows.Forms.Label label34;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal4;
        private System.Windows.Forms.Label label35;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal5;
        private System.Windows.Forms.Label label36;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal6;
        private System.Windows.Forms.Label label37;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal7;
        private System.Windows.Forms.Label label38;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal8;
        private System.Windows.Forms.Label label39;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal9;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal10;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal11;
        private System.Windows.Forms.Label label42;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal12;
        private System.Windows.Forms.Label label43;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal13;
        private System.Windows.Forms.Label label44;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal14;
        private System.Windows.Forms.Label label45;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal15;
        private System.Windows.Forms.Label label46;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal16;
        private System.Windows.Forms.Label label47;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal17;
        private System.Windows.Forms.Label label48;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal18;
        private System.Windows.Forms.Label label49;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal19;
        private System.Windows.Forms.Label label50;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal20;
        private System.Windows.Forms.Label label51;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal21;
        private System.Windows.Forms.Label label52;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal22;
        private System.Windows.Forms.Label label53;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal23;
        private System.Windows.Forms.Label label54;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal24;
        private System.Windows.Forms.Label label55;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal25;
        private System.Windows.Forms.Label label56;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal26;
        private System.Windows.Forms.Label label57;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode1;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode1;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode2;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode2;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode3;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode3;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode4;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode4;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode5;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode5;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode6;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode6;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode7;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode7;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode8;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode8;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode9;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode9;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode10;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode10;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode11;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode11;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode12;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode12;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode13;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode13;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode14;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode14;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode15;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode15;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode16;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode16;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode17;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode17;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode18;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode18;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode19;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode19;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode20;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode20;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode21;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode21;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode22;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode22;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode23;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode23;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode24;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode24;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode25;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode25;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode26;
        private DevExpress.XtraEditors.LookUpEdit LueUOMCode26;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel29;
    }
}