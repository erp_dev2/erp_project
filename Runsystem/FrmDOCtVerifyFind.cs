﻿#region Update
/*
    10/03/2020 [TKG/IMS] New Application
    30/09/2020 [DITA/IMS] Tambah kolom no lppb,nama proyek, kode proyek, nomor po
    29/01/2021 [WED/IMS] tambah tarik data dari DO To Customer
    12/03/2021 [DITA/IMS] rombak query untuk link socontract->doct->doctverify karena data yang tampil sebelumnya terdobel banyak dan project code, po , dll tidak muncul
    10/06/2021 [SET/IMS] Menambah ceklist exclude cancel ketika find do to cust verification
    14/06/2021 [VIN/IMS] bug filter DO To Customer#
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion


namespace RunSystem
{
    public partial class FrmDOCtVerifyFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDOCtVerify mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDOCtVerifyFind(FrmDOCtVerify FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("IfNull(A.DOCt2DocNo, A.DOCtDocNo) DOCt2DocNo, B.LocalDocNo, D.CtName, C.WhsName,  ");
            SQL.AppendLine("E.ItCode, E.ItName, E.ItCodeInternal, E.Specification,  ");
            SQL.AppendLine("B.BatchNo, B.Source,  ");
            SQL.AppendLine("B.Qty, E.InventoryUomCode, B.Qty2, E.InventoryUomCode2, B.Qty3, E.InventoryUomCode3, "); 
            SQL.AppendLine("A.LPPBNo, F.PONo, F.ProjectCode, F.ProjectName,  ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblDOCtVerifyHdr A  ");
            SQL.AppendLine("Inner Join (  ");
            SQL.AppendLine("    Select Distinct T1.DocNo, T1.LocalDocNo, T3.SOContractDocNo, T3.SOContractDNo, T2.WhsCode, T2.CtCode, T4.ItCode, ");
            SQL.AppendLine("    T4.BatchNo, T4.Source, T4.Qty, T4.Qty2, T4.Qty3 ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1  ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr T2 On T1.DOCt2DocNo = T2.DocNo  ");
            SQL.AppendLine("      And T1.DOCt2DocNo Is Not Null  ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl T3 On T2.DocNo = T3.DocNo  ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T4 On T1.DocNo = T4.DocNo And T4.DOCt2DNo = T3.DNo ");
            SQL.AppendLine("    Union All  ");
            SQL.AppendLine("    Select Distinct T1.DocNo, T1.LocalDocNo, T2.SOContractDocNo, T3.SOContractDNo, T2.WhsCode, T2.CtCode, T4.ItCode, ");
            SQL.AppendLine("    T4.BatchNo, T4.Source, T4.Qty, T4.Qty2, T4.Qty3 ");
            SQL.AppendLine("	From TblDOCtVerifyHdr T1  ");
            SQL.AppendLine("    Inner Join TblDOCtHdr T2 On T1.DOCtDocNo = T2.DocNo  ");
            SQL.AppendLine("        And T1.DOCtDocNo Is Not Null  ");
            SQL.AppendLine("    Inner JOin TblDOCtDtl T3 On T2.DocNo = T3.DocNo  ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T4 On T1.DocNo = T4.DocNo And T4.DOCtDNo = T3.DNo ");
            SQL.AppendLine(") B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("Inner Join TblWarehouse C On B.WhsCode=C.WhsCode  ");
            SQL.AppendLine("Inner Join TblCustomer D On B.CtCode=D.CtCode  ");
            SQL.AppendLine("Inner Join TblItem E On B.ItCode=E.ItCode  ");
            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("(   ");
            SQL.AppendLine("  Select X1.SOContractDocNo, X1.SOContractDNo,  ");
            SQL.AppendLine("  Group_Concat(Distinct IfNull(X2.PONo, '')) PONo,  ");
            SQL.AppendLine("  Group_Concat(Distinct IfNull(X5.ProjectCode, '')) ProjectCode, "); 
            SQL.AppendLine("  Group_Concat(Distinct IfNull(X5.ProjectName, '')) ProjectName  ");
            SQL.AppendLine("  From  ");
            SQL.AppendLine("  (  ");
            SQL.AppendLine("      Select Distinct T2.SOContractDocNo, T2.SOContractDNo  ");
            SQL.AppendLine("      From TblDOCt2Hdr T1  ");
            SQL.AppendLine("      Inner Join TblDOCt2Dtl T2 On T1.DocNo = T2.DocNo  ");
            SQL.AppendLine("          And T2.SOContractDocNo Is Not Null  ");
            SQL.AppendLine("       Union All  ");
            SQL.AppendLine("       Select Distinct T1.SOContractDocNo, T2.SOContractDNo  ");
            SQL.AppendLine("       From TblDOCtHdr T1  ");
            SQL.AppendLine("       Inner Join TblDOCtDtl T2 On T1.DocNo = T2.DocNo  ");
            SQL.AppendLine("            Where T1.SOContractDocNo Is Not Null  ");
            SQL.AppendLine("   ) X1  ");
            SQL.AppendLine("    Inner Join TblSOContractHdr X2 On X1.SOContractDocNo = X2.DocNo  ");
            SQL.AppendLine("    Inner Join TblBOQHdr X3 On X2.BOQDocNo = X3.DocNo  ");
            SQL.AppendLine("    Inner Join TblLOPHdr X4 On X3.LOPDocNo = X4.DocNo  ");
            SQL.AppendLine("    Left Join TblProjectGroup X5 On X4.PGCode = X5.PGCode  ");
            SQL.AppendLine("    Inner Join TblSOContractDtl X6 On X1.SOContractDocNo = X6.DocNo And X1.SOContractDNo = X6.DNo ");
            SQL.AppendLine("    Group By X1.SOContractDocNo , X1.SOContractDNo ");
            SQL.AppendLine(") F On B.SOContractDocNo = F.SOContractDocNo And B.SOContractDNo = F.SOContractDNo  ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "DO To Customer#",
                    "DO's Local#",
                    
                    //6-10
                    "Customer",
                    "Warehouse",
                    "Item's Code",
                    "Item's Name",
                    "Local Code",
                    
                    //11-15
                    "Specification",
                    "Batch#",
                    "Source",
                    "Quantity",
                    "UoM",

                    //16-20
                    "Quantity",
                    "UoM",
                    "Quantity",
                    "UoM",
                    "LPPB#",

                    //21-25
                    "PO#",
                    "Project Code",
                    "Project Name",
                    "Created By",
                    "Created Date",
                    
                    //26-29 
                    "Created Time", 
                    "Last Updated By", 
                    "Last Updated Date",
                    "Last Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 60, 150, 130, 
                    
                    //6-10
                    200, 200, 130, 200, 100,  
                    
                    //11-15
                    200, 180, 180, 100, 100, 

                    //16-20
                    100, 100, 100, 100, 130,

                    //21-25
                    100, 130, 200, 130, 130,
                    //26-29
                    130, 130, 130, 130
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 16, 18 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 25, 28 });
            Sm.GrdFormatTime(Grd1, new int[] { 26, 29 });
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 24, 25, 26, 27, 28, 29 }, false);
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17 }, true);
            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19 }, true);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 24, 25, 26, 27, 28, 29 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                if (ChkExcCancel.Checked)
                {
                    var mSQL2 = new StringBuilder();

                    mSQL2.AppendLine("And A.CancelInd = 'N' ");

                    Filter += mSQL2.ToString();
                }

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtDOCt2DocNo.Text, new string[] { "A.DOCt2DocNo", "A.DoctDocNo", "B.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "B.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "B.WhsCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "DOCt2DocNo", "LocalDocNo", "CtName", 

                            //6-10
                            "WhsName", "ItCode", "ItName", "ItCodeInternal", "Specification", 

                            //11-15
                            "BatchNo", "Source", "Qty", "InventoryUomCode", "Qty2", 

                            //16-20
                            "InventoryUomCode2", "Qty3", "InventoryUomCode3", "LPPBNo", "PONo",

                            //21-25
                            "ProjectCode", "ProjectName", "CreateBy", "CreateDt", "LastUpBy",
                           
                            //26
                             "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 26, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 25);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 28, 26);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 29, 26);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Event

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_Validated(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtDOCt2DocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDOCt2DocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO To Customer#");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "warehouse");
        }

        #endregion

        #endregion
    }
}
