﻿#region Update
/*
    24/03/2023 [WED/PHT] validasi pas edit-save parameter AccTypeCashAndBankAccNotForCashBook kalau value nya tidak match dengan kondisi berdasarkan parameter IsModuleInvestmentActive
    26/04/2023 [VIN/ALL] value max length update jadi 9999
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmParameter : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmParameterFind FrmFind;

        #endregion

        #region Constructor

        public FrmParameter(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            ExecQuery();
            SetFormControl(mState.View);
            SetLueParCtCode(ref LueParCtCode, string.Empty);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtParCode, TxtParDesc, TxtParValue, TxtCustomize, LueParCtCode
                    }, true);
                    TxtParCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtParCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtParDesc, TxtParValue, TxtCustomize, LueParCtCode
                    }, false);
                    TxtParCode.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtParCode, TxtParDesc, TxtParValue, TxtCustomize
            });
        }

        #endregion

        #region Additional Method

        private bool IsValueNotValid(string ParCode, string ParValue)
        {
            string 
                TableTarget = string.Empty, 
                ColumnTarget = string.Empty, ColumnTargetValue = string.Empty, 
                ColumnTarget2 = string.Empty, ColumnTarget2Value = string.Empty,
                WarningDesc = string.Empty
                ;

            switch (ParCode)
            {
                #region AccTypeCashAndBankAccNotForCashBook
                case "AccTypeCashAndBankAccNotForCashBook":
                    if (ParValue.Trim().Length == 0) break;

                    bool IsModuleInvestmentActive = Sm.GetParameterBoo("IsModuleInvestmentActive");
                    if (IsModuleInvestmentActive)
                    {
                        TableTarget = "TblOption";
                        ColumnTarget = "OptCat";
                        ColumnTargetValue = "InvestmentAccount";
                        ColumnTarget2 = "OptCode";
                        ColumnTarget2Value = ParValue;
                        WarningDesc = "Option with category : " + ColumnTargetValue;
                    }
                    else
                    {
                        TableTarget = "TblBankAccount";
                        ColumnTarget = "BankAcTp";
                        ColumnTargetValue = ParValue;
                        WarningDesc = "Bank account field : Account Type";
                    }

                    break;
                #endregion

                default: break;
            }

            if (TableTarget.Length > 0)
            {
                string invalidVal = string.Empty;
                bool IsValueValid = IsParValueValid(ref invalidVal, TableTarget, ColumnTarget, ColumnTargetValue, ColumnTarget2, ColumnTarget2Value);
                if (!IsValueValid)
                {
                    if (invalidVal.Length > 0) // dibikin gini, karena di method IsParValueValid saat ini ada warning dari sisi developer terkait belom bisanya ColTarget dan ColTarget2 sama2 berkoma
                    {
                        Sm.StdMsg(mMsgType.Warning, "There is/are invalid value from your parameter, which is : " + invalidVal + ". You could check the validity source from : " + WarningDesc);
                    }
                    return true;
                }
            }

            return false;
        }

        private bool IsParValueValid(ref string invalidVal, string TableTarget, string ColumnTarget, string ColumnTargetValue, string ColumnTarget2, string ColumnTarget2Value)
        {
            var SQL = new StringBuilder();
            string[] datas = { };
            bool IsFirst = true;
            bool IsCol2Exists = ColumnTarget2.Trim().Length > 0;
            string cols = string.Empty;

            if (IsCol2Exists)
            {
                if (ColumnTargetValue.Contains(","))
                {
                    Sm.StdMsg(mMsgType.Warning, "For the time being, its not supported for first column target to have multiple value. Please re-consider using this function.");
                    return false;
                }

                datas = ColumnTarget2Value.Split(',');
                cols = ColumnTarget2;
            }
            else
            {
                datas = ColumnTargetValue.Split(',');
                cols = ColumnTarget;
            }                

            SQL.AppendLine("Select Group_Concat(Distinct T.val) As val From ( ");

            foreach (var x in datas)
            {
                if (IsFirst) IsFirst = false;
                else SQL.AppendLine("Union  ");

                SQL.AppendLine("    Select '" + x + "' As val ");
                SQL.AppendLine("    From TblCurrency "); // ini tabel random agar query Select - From - Where bisa dicapai
                SQL.AppendLine("    Where '" + x + "' Not In ( ");
                SQL.AppendLine("        Select " + cols + " ");
                SQL.AppendLine("        From " + TableTarget + " ");
                SQL.AppendLine("        Where 0 = 0 ");
                if (IsCol2Exists) SQL.AppendLine("        And " + ColumnTarget + " ='" + ColumnTargetValue + "' ");
                SQL.AppendLine("        And " + cols + " Is Not Null ");
                SQL.AppendLine("    ) ");
            }

            SQL.AppendLine(") T; ");

            invalidVal = Sm.GetValue(SQL.ToString());

            return invalidVal.Length == 0;
        }

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('AccTypeCashAndBankAccNotForCashBook', 'account type pada master cash and bank account yang tidak muncul dalam reporting cashbook', NULL, 'PHT', NULL, 'Y', 'WEDHA', '202303241100', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsModuleInvestmentActive', 'Module investment active', 'N', 'GSS', NULL, 'Y', 'HAR', '202205270922', NULL, NULL); ");

            Sm.ExecQuery(SQL.ToString());
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmParameterFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtParCode, "", false)) return;
            SetFormControl(mState.Edit);
            //SetLueParCtName(ref LueParCtName, string.Empty);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtParCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblParameter Where ParCode=@ParCode" };
                Sm.CmParam<String>(ref cm, "@ParCode", TxtParCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblParameter(ParCode, ParDesc, ParValue, Customize, ParCtCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ParCode, @ParDesc, @ParValue, @Customize, @ParCtCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ParDesc=@ParDesc, ParValue=@ParValue, Customize=@Customize, ParCtCode=@ParCtCode,  LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ParCode", TxtParCode.Text);
                Sm.CmParam<String>(ref cm, "@ParDesc", TxtParDesc.Text);
                Sm.CmParam<String>(ref cm, "@ParValue", TxtParValue.Text);
                Sm.CmParam<String>(ref cm, "@Customize", TxtCustomize.Text);
                Sm.CmParam<String>(ref cm, "@ParCtCode", Sm.GetLue(LueParCtCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtParCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
            SetLueParCtCode(ref LueParCtCode, string.Empty);
        }

        #endregion

        #region Show Data

        public void ShowData(string ParCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ParCode", ParCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        /*"SELECT a.ParCode, a.ParDesc, a.ParValue, a.Customize, b.ParCtName "+
                        "FROM tblparameter a LEFT JOIN tblparametercategory b "+
                        "ON a.ParCtCode = b.ParCtCode WHERE a.ParCode=@ParCode",*/
                        "SELECT * FROM tblparameter WHERE ParCode=@ParCode",
                        new string[] 
                        {
                            "ParCode", "ParDesc", "ParValue", "Customize", "ParCtCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtParCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtParDesc.EditValue = Sm.DrStr(dr, c[1]);
                            TxtParValue.EditValue = Sm.DrStr(dr, c[2]);
                            TxtCustomize.EditValue = Sm.DrStr(dr, c[3]);
                            SetLueParCtCode(ref LueParCtCode, Sm.DrStr(dr, c[4]));
                            Sm.SetLue(LueParCtCode, Sm.DrStr(dr, c[4]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtParCode, "Parameter code", false) ||
                Sm.IsTxtEmpty(TxtParDesc, "Description", false) ||
                IsParCodeExisted() ||
                IsNotEditable() ||
                IsValueNotValid(TxtParCode.Text, TxtParValue.Text);
        }
        private bool IsNotEditable()
        {
            return 
                TxtParCode.Properties.ReadOnly &&
                Sm.IsDataExist(
                    "Select ParCode From TblParameter Where ParCode=@Param And Editable='N';",
                    TxtParCode.Text,
                    "You can't edit this parameter."
                    );
        }

        private bool IsParCodeExisted()
        {
            if (!TxtParCode.Properties.ReadOnly && Sm.IsDataExist("Select ParCode From TblParameter Where ParCode='" + TxtParCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter Code ( " + TxtParCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        internal void SetLueParCtCode(ref DXE.LookUpEdit Lue, string ParCtCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT ParCtCode AS Col1, ParCtName AS Col2");
            SQL.AppendLine("FROM TblParameterCategory ");

            if (ParCtCode.Length != 0)
                SQL.AppendLine("WHERE ParCtCode=@ParCtCode OR ActInd='Y'");
            else
            {
                SQL.AppendLine("WHERE ActInd='Y' ");
            }

            SQL.AppendLine("ORDER BY ParCtName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@ParCtCode", ParCtCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc control Event

        private void TxtParCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtParCode);
        }      

        private void TxtParDesc_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtParDesc);
        }

        private void TxtParValue_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtParValue);
        }

        private void TxtCustomize_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCustomize);
        }

        private void LueParCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueParCtCode, new Sm.RefreshLue2(SetLueParCtCode), string.Empty);
        }
       
        #endregion
        
        #endregion
    }
}
