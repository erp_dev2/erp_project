﻿#region
    /*
     *  23/08/2021 [ICA/GKS] new apps
     * */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpAttendanceLogSummary : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsSiteMandatory = false;
        private bool
            mIsFilterBySiteHR = false,
            mIsFilterByDeptHR = false,
            mIsAttendanceLogUseLatLngTable = false;

        #endregion

        #region Constructor

        public FrmRptEmpAttendanceLogSummary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueOption(ref LueEmploymentStatus, "EmploymentStatus");
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsAttendanceLogUseLatLngTable = Sm.GetParameterBoo("IsAttendanceLogUseLatLngTable");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code", 
                        "",
                        "Employee's Name",
                        "Department", 
                        "Site",
                        
                        //6-10
                        "Total Late",
                        "Total Early"+Environment.NewLine+"Go Home",
                        "Total Leave",
                        "Total Working"+Environment.NewLine+"Days",
                        "Location",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 20, 180, 150, 150, 
                        
                        //6-10
                        100, 100, 100, 100, 300, 
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4 }, false);
            Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 9 }, 0);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            var lWS = new List<WS>();
            var lLog = new List<Log>();
            var lLogSummary = new List<LogSummary>();
            var lLeave = new List<Leave>();
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref lWS);
                if (lWS.Count > 0)
                {
                    Process2(ref lLog);
                    Process3(ref lWS, ref lLog);
                    Process4(ref lLogSummary, ref lWS);
                    Process5(ref lLeave, ref lLogSummary);
                    Process6(ref lLogSummary);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }


        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }


        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void Process1(ref List<WS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " ";

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteDocDt2));
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "B.EmpCode", "B.EmpCodeOld", "B.EmpName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "B.SiteCode", true);

            SQL.AppendLine("Select A.Dt, A.EmpCode, B.EmpName, B.EmpCodeOld, D.PosName, C.DeptName, F.SiteName, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, G.WSName, ");
            SQL.AppendLine("G.In1 As LogIn, G.bIn1 As bLogIn, G.aIn1 As aLogIn,  ");
            SQL.AppendLine("Case When G.Out3 Is Null Then G.Out1 Else G.Out3 End As LogOut, ");
            SQL.AppendLine("Case When G.bOut3 Is Null Then G.bOut1 Else G.bOut3 End As bLogOut, ");
            SQL.AppendLine("Case When G.aOut3 Is Null Then G.aOut1 Else G.aOut3 End As aLogOut ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode " + Filter);
            SQL.AppendLine("    And B.JoinDt<=@EndDt ");
            SQL.AppendLine("    And (B.ResignDt is Null Or (B.ResignDt Is Not Null And B.ResignDt>@StartDt)) ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And B.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode=D.PosCode ");
            SQL.AppendLine("Inner Join TblWorkSchedule G On A.WsCode=G.WsCode ");
            SQL.AppendLine("Left Join TblSite F On B.SiteCode=F.SiteCode ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("Order By A.EmpCode, A.Dt;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Dt",
 
                    //1-5
                    "EmpCode", "EmpName", "LogIn", "bLogIn", "aLogIn", 
                    
                    //6-8
                    "LogOut", "bLogOut", "aLogOut"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new WS()
                        {
                            Dt = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            EmpName = Sm.DrStr(dr, c[2]),
                            LogIn = Sm.DrStr(dr, c[3]),
                            bLogIn = Sm.DrStr(dr, c[4]),
                            aLogIn = Sm.DrStr(dr, c[5]),
                            LogOut = Sm.DrStr(dr, c[6]),
                            bLogOut = Sm.DrStr(dr, c[7]),
                            aLogOut = Sm.DrStr(dr, c[8]),

                            IsOneDay = true,
                            Dt2 = string.Empty,
                            InDtL = string.Empty,
                            InTmL = string.Empty,
                            OutDtL = string.Empty,
                            OutTmL = string.Empty,
                            City = string.Empty,
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Log> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " ";

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            //Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteDocDt1)).AddDays(-1)));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteDocDt2)).AddDays(1)));
            //Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "B.EmpCode", "B.EmpCodeOld", "B.EmpName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "B.SiteCode", true);

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.Tm, A.City ");
            SQL.AppendLine("From TblAttendanceLog A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode " + Filter);
            SQL.AppendLine("    And B.JoinDt<=@EndDt ");
            SQL.AppendLine("    And (B.ResignDt is Null Or (B.ResignDt is Not Null And B.ResignDt>=@StartDt)) ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And B.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt Order By A.EmpCode, A.Dt, A.Tm;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Tm", "City" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Log()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            DtTm = Sm.Left(Sm.DrStr(dr, c[1]) + Sm.DrStr(dr, c[2]), 12),
                            City = Sm.DrStr(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<WS> lWS, ref List<Log> lLog)
        {
            string
                EmpCode = string.Empty,
                Dt0 = string.Empty,
                Dt = string.Empty,
                Dt2 = string.Empty,
                LogIn = string.Empty,
                bLogIn = string.Empty,
                aLogIn = string.Empty,
                LogOut = string.Empty,
                bLogOut = string.Empty,
                aLogOut = string.Empty
                ;
            bool IsOneDay = true;

            string Now = Sm.ServerCurrentDateTime();

            if (Now.Length > 0) Now = Sm.Left(Now, 12);

            for (var i = 0; i < lWS.Count; i++)
            {
                EmpCode = lWS[i].EmpCode;
                Dt = lWS[i].Dt;
                Dt0 = Sm.Left(Sm.FormatDate(Sm.ConvertDate(Dt).AddDays(-1)), 8);
                Dt2 = Sm.Left(Sm.FormatDate(Sm.ConvertDate(Dt).AddDays(1)), 8);

                IsOneDay = (Sm.CompareDtTm(lWS[i].LogIn, lWS[i].LogOut) <= 0);

                lWS[i].Dt2 = Dt2;
                lWS[i].IsOneDay = IsOneDay;

                LogIn = Dt + lWS[i].LogIn;
                decimal cek = Sm.CompareDtTm(lWS[i].bLogIn, lWS[i].LogIn);
                if (Sm.CompareDtTm(lWS[i].bLogIn, lWS[i].LogIn) <= 0)
                    bLogIn = Dt + lWS[i].bLogIn;
                else
                    bLogIn = Dt0 + lWS[i].bLogIn;

                if (Sm.CompareDtTm(lWS[i].LogIn, lWS[i].aLogIn) <= 0)
                    aLogIn = Dt + lWS[i].aLogIn;
                else
                    aLogIn = Dt2 + lWS[i].aLogIn;

                if (IsOneDay)
                    LogOut = Dt + lWS[i].LogOut;
                else
                    LogOut = Dt2 + lWS[i].LogOut;

                if (IsOneDay)
                    bLogOut = Dt + lWS[i].bLogOut;
                else
                {
                    if (Sm.CompareDtTm(lWS[i].bLogOut, lWS[i].LogOut) <= 0)
                        bLogOut = Dt2 + lWS[i].bLogOut;
                    else
                        bLogOut = Dt + lWS[i].bLogOut;
                }

                if (IsOneDay)
                {
                    if (Sm.CompareDtTm(lWS[i].LogOut, lWS[i].aLogOut) <= 0)
                        aLogOut = Dt + lWS[i].aLogOut;
                    else
                        aLogOut = Dt2 + lWS[i].aLogOut;
                }
                else
                    aLogOut = Dt2 + lWS[i].aLogOut;

                if (lLog.Count > 0)
                {
                    foreach (var index in lLog
                        .Where(x =>
                                string.Compare(x.EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(x.DtTm, bLogIn) >= 0 &&
                                Sm.CompareDtTm(x.DtTm, aLogIn) <= 0
                            )
                        .OrderBy(o => o.DtTm)
                        .Take(1)
                        )
                    {
                        lWS[i].InDtL = Sm.Left(index.DtTm, 8);
                        lWS[i].InTmL = Sm.Right(index.DtTm, 4);
                        lWS[i].City = index.City;
                    }

                    foreach (var index in lLog
                        .Where(x =>
                                string.Compare(x.EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(x.DtTm, bLogOut) >= 0 &&
                                Sm.CompareDtTm(x.DtTm, aLogOut) <= 0
                            )
                        .OrderByDescending(o => o.DtTm)
                        .Take(1)
                        )
                    {
                        lWS[i].OutDtL = Sm.Left(index.DtTm, 8);
                        lWS[i].OutTmL = Sm.Right(index.DtTm, 4);
                        lWS[i].City = index.City;
                    }
                }
            }
        }

        private void Process4(ref List<LogSummary> lLogSummary, ref List<WS> lWS)
        {
            lLogSummary.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " ";

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteDocDt1)).AddDays(-1)));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteDocDt2)).AddDays(1)));
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpCodeOld", "A.EmpName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);

            SQL.AppendLine("Select A.EmpCode, A.EmpName, B.DeptName, C.SiteName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("Inner Join TblSite C On A.SiteCode = C.SiteCode ");
            SQL.AppendLine("Where A.JoinDt<=@EndDt " + Filter);
            SQL.AppendLine("    And (A.ResignDt is Null Or (A.ResignDt is Not Null And A.ResignDt>=@StartDt)) ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And A.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "EmpName", "DeptName", "SiteName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lLogSummary.Add(new LogSummary()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            EmpName = Sm.DrStr(dr, c[1]),
                            DeptName = Sm.DrStr(dr, c[2]),
                            SiteName = Sm.DrStr(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }

            string EmpCode = string.Empty;
            for (int i = 0; i < lLogSummary.Count(); i++)
            {
                EmpCode = lLogSummary[i].EmpCode;
                lLogSummary[i].TotalWorkDays = lWS.Where( x => string.Compare(x.EmpCode, EmpCode) == 0 && 
                                                            ((x.InDtL.Length > 0 && x.InTmL.Length > 0) ||
                                                            (x.OutDtL.Length > 0 && x.OutDtL.Length > 0))).Count();

                lLogSummary[i].TotalLate = lWS.Where(x => string.Compare(x.EmpCode, EmpCode) == 0 && x.InTmL.Length > 0 && Sm.CompareDtTm(x.InTmL, x.LogIn) >= 0).Count();
                lLogSummary[i].TotalEarly = lWS.Where(x => string.Compare(x.EmpCode, EmpCode) == 0 && x.OutTmL.Length > 0 && Sm.CompareDtTm(x.OutTmL, x.LogOut) <= 0).Count();

                foreach (var index in lWS
                        .Where(x =>
                                string.Compare(x.EmpCode, EmpCode) == 0 && 
                                x.InTmL.Length > 0
                            )
                        .OrderByDescending(o => o.Dt)
                        .Take(1)
                        )
                {
                    lLogSummary[i].Location = index.City;
                }
            }
        }

        private void Process5(ref List<Leave> lLeave, ref List<LogSummary> lLogSummary)
        {
            lLeave.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " ";

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteDocDt1)).AddDays(-1)));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteDocDt2)).AddDays(1)));
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpCodeOld", "C.EmpName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "C.SiteCode", true);

            SQL.AppendLine("Select A.EmpCode, Count(B.DocNo) LeaveDays ");
            SQL.AppendLine("From TblEmpLeaveHdr A ");
            SQL.AppendLine("Inner Join TblEmpLeaveDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' And A.Status = 'A' ");
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL.AppendLine("And C.JoinDt<=@EndDt " + Filter);
            SQL.AppendLine("    And (C.ResignDt is Null Or (C.ResignDt is Not Null And C.ResignDt>=@StartDt)) ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And C.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And C.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=C.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Where B.LeaveDt Between @StartDt And @EndDt Group By A.EmpCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "LeaveDays" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lLeave.Add(new Leave()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            LeaveDays = Sm.DrDec(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }

            for (var i = 0; i < lLeave.Count; i++)
            {
                string EmpCode = lLeave[i].EmpCode;
                foreach (var index in lLogSummary.Where(x =>string.Compare(x.EmpCode, EmpCode) == 0))
                {
                    index.TotalLeave = lLeave[i].LeaveDays;
                }
            }
            
        }

        private void Process6(ref List<LogSummary> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {

                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[1].Value = l[i].EmpCode;
                r.Cells[3].Value = l[i].EmpName;
                r.Cells[4].Value = l[i].DeptName;
                r.Cells[5].Value = l[i].SiteName;
                r.Cells[6].Value = l[i].TotalLate;
                r.Cells[7].Value = l[i].TotalEarly;
                r.Cells[8].Value = l[i].TotalLeave;
                r.Cells[9].Value = l[i].TotalWorkDays;
                r.Cells[10].Value = l[i].Location;
            }
            Grd1.EndUpdate();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DteDocDt2.DateTime = DteDocDt1.DateTime;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueEmploymentStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmploymentStatus, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEmploymentStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employment status");
        }

        #endregion

        #endregion

        #region Class

        private class WS
        {
            public string Dt { get; set; }
            public string Dt2 { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public bool IsOneDay { get; set; }
            public string LogIn { get; set; }
            public string bLogIn { get; set; }
            public string aLogIn { get; set; }
            public string LogOut { get; set; }
            public string bLogOut { get; set; }
            public string aLogOut { get; set; }
            public string InDtL { get; set; }
            public string InTmL { get; set; }
            public string OutDtL { get; set; }
            public string OutTmL { get; set; }
            public string City { get; set; }
        }

        private class Log
        {
            public string EmpCode { get; set; }
            public string DtTm { get; set; }
            public string City { get; set; }
        }

        private class Leave
        {
            public string EmpCode { get; set; }
            public decimal LeaveDays { get; set; }
        }

        private class LogSummary
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string SiteName { get; set; }
            public decimal TotalLate { get; set; }
            public decimal TotalEarly { get; set; }
            public decimal TotalLeave { get; set; }
            public decimal TotalWorkDays { get; set; }
            public string Location { get; set; }
        }

        #endregion
    }
}
