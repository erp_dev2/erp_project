﻿#region Update
/*
    03/02/2020 [HAR/TWC] POS TWC ticketing
    23/07/2021 [TKG/TWC] tambah entity
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTWCJournalSettingDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmTWCJournalSetting mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmTWCJournalSettingDlg(FrmTWCJournalSetting FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    "No.",

                    "Account#", 
                    "Description",
                    "Type",
                    "Alias",
                    "Entity"
                },
                new int[] { 50, 150, 350, 100, 150, 200 }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.AcNo, T2.AcDesc, Case T2.AcType When 'D' Then 'Debit' When 'C' Then 'Credit' End As AcType, T2.Alias, T1.Entity ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select A.AcNo,  ");
            SQL.AppendLine("    Group_Concat(C.EntName Order By C.EntName Separator ', ') As Entity ");
            SQL.AppendLine("    From TblCOA A ");
            SQL.AppendLine("    Left Join TblCOADtl B On A.AcNo=B.AcNo ");
            SQL.AppendLine("    Left Join TblEntity C On B.EntCode=C.EntCode ");
            SQL.AppendLine("    Where A.Parent Is Not Null ");
            SQL.AppendLine("    And A.AcNo Not In (Select Parent From TblCoa Where Parent Is Not Null) ");
            SQL.AppendLine("    Group By A.AcNo ");
            SQL.AppendLine(") T1, TblCOA T2 Where T1.AcNo=T2.AcNo ");
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "T2.AcNo", "T2.AcDesc", "T2.Alias" });
                Sm.FilterStr(ref Filter, ref cm, TxtEntCode.Text, "T1.Entity", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T2.AcNo;",
                        new string[] 
                        { 
                            "AcNo",

                            "AcDesc",
                            "AcType",
                            "Alias",
                            "Entity"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                 mFrmParent.TxtAcNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                 mFrmParent.TxtAcDesc.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2) + " [" + Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4) + "]";
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "COA's Account");
        }

        private void TxtEntCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Entity");
        }

        #endregion

        #endregion
    }
}
