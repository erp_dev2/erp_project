﻿#region Update
/*
    14/02/2017 [TKG] New Application
    22/02/2017 [ARI] tambah printout
    23/02/2018 [ARI] tambah NPWP customer, remark header, keterangan, nama user dan moble number user 
    02/04/2018 [HAR] Rounding tax ke bawah berdasarkan parameter
    03/04/2018 [HAR] Rounding DO amount ke atas berdasarkan parameter
    12/04/2018 [HAR] shipping address ambil dari master customer tab asset (location) 
    07/05/2018 [ARI] Feedback printout 
    17/01/2020 [TKG/PHT] ubah GenerateReceipt
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoice4 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty, //if this application is called from other application;
            mIsFormPrintOutInvoice = string.Empty;
        private string mMainCurCode = string.Empty, mEmpCodeSI = string.Empty, mEmpCodeTaxCollector = string.Empty;
        private bool 
            mIsAutoJournalActived = false,
            mIsRemarkForJournalMandatory = false,
            mSITaxRoundingDown = false,
            mAmtRoundingUp = false
            ;
        internal bool mIsCustomerItemNameMandatory = false;
        internal FrmSalesInvoice4Find FrmFind;
        iGCell fCell;
        bool fAccept;
        
        #endregion

        #region Constructor

        public FrmSalesInvoice4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Sales Invoice";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");

                TcSalesInvoice4.SelectedTabPage = Tp4;
                Sl.SetLueOption(ref LueOption, "AccountDescriptionOnSalesInvoice");
                LueOption.Visible = false;

                TcSalesInvoice4.SelectedTabPage = Tp1;
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueTaxCode(new List<DevExpress.XtraEditors.LookUpEdit> { LueTaxCode1, LueTaxCode2 });
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                SetLueSPCode(ref LueSPCode);
                
                SetGrd();
                SetFormControl(mState.View);
                if (mIsRemarkForJournalMandatory) LblRemark.ForeColor = Color.Red;
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "DO#",
                        "DO D#",
                        "",
                        "Item/Asset"+Environment.NewLine+"Code", 

                        //6-10
                        "Item/Asset Name", 
                        "Asset", 
                        "Quantity",
                        "Packaging"+Environment.NewLine+"Unit",
                        "Quantity",

                        //11-15
                        "UoM",
                        "Price", 
                        "Discount", 
                        "Price After"+Environment.NewLine+"Discount", 
                        "Amount Before"+Environment.NewLine+"Tax (1)", 
                        
                        //16-20
                        "Tax"+Environment.NewLine+"(1)", 
                        "Amount After"+Environment.NewLine+"Tax (1)", 
                        "Amount Before"+Environment.NewLine+"Tax (2)", 
                        "Tax"+Environment.NewLine+"(2)", 
                        "Amount After"+Environment.NewLine+"Tax (2)", 

                        //21-24
                        "Amount"+Environment.NewLine+"Before Tax", 
                        "Tax", 
                        "Amount"+Environment.NewLine+"After Tax", 
                        "Total"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 130, 0, 20, 100,
 
                        //6-10
                        200, 100, 0, 0, 100, 
                        
                        //11-15
                        100, 130, 130, 130, 130, 
                        
                        //16-20
                        130, 130, 130, 130, 130, 

                        //21-24
                        130, 130, 130, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 7 });
            Sm.GrdColButton(Grd1, new int[] { 1, 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 4, 5, 7, 8, 9 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 2;
            Sm.GrdHdrWithColWidth(Grd2, new string[] { "Currency", "Deposit Amount" }, new int[] { 100, 200 });
            Sm.GrdFormatDec(Grd2, new int[] { 1 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1 });

            #endregion

            #region Grd3

            Grd3.Cols.Count = 11;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "",
                        "Debit"+Environment.NewLine+"Amount",
                        "",
                        //6-10
                        "Credit"+Environment.NewLine+"Amount",
                        "OptCode",
                        "Option",
                        "Remark",
                        ""
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 20, 100, 20, 
                        //6-10
                        100, 50, 150, 400, 20
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColCheck(Grd3, new int[] { 3, 5, 10 });
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 4, 6, 7, 8, 9, 10 });
            Sm.GrdFormatDec(Grd3, new int[] { 4, 6 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 7, 10 }, false);

            #endregion
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 7, 15, 16, 17, 18, 19, 20 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, MeeCancelReason, ChkCancelInd, TxtLocalDocNo, 
                        DteDueDt, LueSPCode, LueCurCode, TxtTaxInvDocument1, DteTaxInvDt1,
                        LueTaxCode1, TxtTaxInvDocument2, DteTaxInvDt2, LueTaxCode2, TxtDownpayment, 
                        LueBankAcCode, MeeRemark 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, TxtLocalDocNo, DteDueDt, LueSPCode, 
                        LueCurCode, TxtTaxInvDocument1, DteTaxInvDt1, LueTaxCode1, TxtTaxInvDocument2, 
                        DteTaxInvDt2, LueTaxCode2, TxtDownpayment, LueBankAcCode, MeeRemark 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5, 6, 8, 9 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, LueCtCode, MeeCancelReason, DteDueDt, 
                 TxtLocalDocNo, TxtTaxInvDocument1, DteTaxInvDt1, LueTaxCode1, TxtTaxInvDocument2, 
                 DteTaxInvDt2, LueTaxCode2, LueCurCode, MeeRemark, LueSPCode, 
                 LueBankAcCode, TxtJournalDocNo, TxtJournalDocNo2, TxtReceiptNo
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { 
                TxtTaxAmt1, TxtTaxAmt2, TxtTotalAmt, TxtTotalTax, TxtDownpayment, 
                TxtAmt 
            }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        #region Clear Grid

        private void ClearGrd()
        {
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
        }

        private void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
        }

        private void ClearGrd2()
        {
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 1 });
        }

        private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4, 6 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesInvoice4Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                SetLueCtCode(ref LueCtCode, string.Empty);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From TblSalesPerson Order by CreateDt limit 1;"));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalesInvoice4", "TblSalesInvoice2Hdr");
            string ReceiptNo = GenerateReceipt(Sm.GetDte(DteDocDt));
            
            var lDepositSummary = new List<DepositSummary>();

            ProcessDepositSummary(ref lDepositSummary);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSalesInvoice2Hdr(DocNo, ReceiptNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveSalesInvoice2Dtl(DocNo, Row));

            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveSalesInvoice2Dtl2(DocNo, Row));
            }

            if (decimal.Parse(TxtDownpayment.Text) != 0m)
            {
                cml.Add(SaveCustomerDeposit(DocNo));
                if (lDepositSummary.Count > 0)
                {
                    for (int i = 0; i < lDepositSummary.Count; i++)
                        cml.Add(SaveSalesInvoice2Dtl3(DocNo, lDepositSummary[i]));
                }
            }

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsDteEmpty(DteDueDt, "Due date") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                (mIsRemarkForJournalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsDOCtAlreadyCancelled() ||
                IsDOCtAlreadyProcessed() ||
                IsDownpaymentNotValid() ||
                IsJournalAmtNotBalanced();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 DO.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "DO entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            if (Grd3.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Additional cost, discount information entered (" + (Grd3.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd3.Rows.Count > 1)
            {
                var AcType = string.Empty;

                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 1, false, "COA's account is empty.")) return true;
                    if (Sm.GetGrdDec(Grd3, Row, 4) == 0m && Sm.GetGrdDec(Grd3, Row, 6) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Acount No : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount can't be 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd3, Row, 4) != 0m && Sm.GetGrdDec(Grd3, Row, 6) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Acount No : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount both can't be bigger than 0.");
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsDOCtAlreadyCancelled()
        {
            string Msg = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        Msg =
                            "DO# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Item/Asset Code : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                            "Item/Asset Name : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine + Environment.NewLine;

                        if (IsDOCtAlreadyCancelled(Row))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "This data already cancelled.");
                            Sm.FocusGrd(Grd1, Row, 2);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsDOCtAlreadyCancelled(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblDOCt4Dtl ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo And CancelInd='Y';");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 3));
            return Sm.IsDataExist(cm);
        }

        private bool IsDOCtAlreadyProcessed()
        {
            string Msg = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        Msg =
                            "DO# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Item/Asset Code : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                            "Item/Asset Name : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine + Environment.NewLine;

                        if (IsDOCtAlreadyProcessed(Row))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "This document already processed.");
                            Sm.FocusGrd(Grd1, Row, 2);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsDOCtAlreadyProcessed(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblDOCt4Dtl ");
            SQL.AppendLine("Where SalesInvoice2DocNo Is Not Null And DocNo=@DocNo And DNo=@DNo;");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 3));
            return Sm.IsDataExist(cm);
        }

        private bool IsDownpaymentNotValid()
        {
            decimal Downpayment = decimal.Parse(TxtDownpayment.Text);

            if (Downpayment == 0m) return false;

            //Recompute Deposit
            ShowCustomerDepositSummary(Sm.GetLue(LueCtCode));

            //Get Currency
            decimal Deposit = 0m;
            string CurCode = Sm.GetLue(LueCurCode);

            //Get Deposit Amount Based on currency
            if (Grd2.Rows.Count > 0)
            {
                for (int row = 0; row < Grd2.Rows.Count - 1; row++)
                {
                    if (Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd2, row, 0)))
                    {
                        Deposit = Sm.GetGrdDec(Grd2, row, 1);
                        break;
                    }
                }
            }

            if (Downpayment > Deposit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Currency : " + CurCode + Environment.NewLine +
                    "Deposit Amount: " + Sm.FormatNum(Deposit, 0) + Environment.NewLine +
                    "Downpayment Amount: " + Sm.FormatNum(Downpayment, 0) + Environment.NewLine + Environment.NewLine +
                    "Downpayment is bigger than existing deposit."
                    );
                return true;
            }
            return false;
        }

        private bool IsJournalAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0) Debit += Sm.GetGrdDec(Grd3, Row, 4);
                if (Sm.GetGrdStr(Grd3, Row, 6).Length > 0) Credit += Sm.GetGrdDec(Grd3, Row, 6);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }
            return false;
        }

        private MySqlCommand SaveCustomerDeposit(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCustomerDepositMovement(DocNo, DocType, DocDt, CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, ");
            SQL.AppendLine("(Case @CancelInd When 'Y' Then '04' Else '03' End) As DocType, ");
            SQL.AppendLine("DocDt, CtCode, CurCode, (Case @CancelInd When 'Y' Then 1 Else -1 End)*@Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblSalesInvoiceHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblCustomerDepositSummary(CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @CtCode, @CurCode, @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update Amt=Amt+((Case @CancelInd When 'Y' Then 1 Else -1 End)*@Amt), LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtDownpayment.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSalesInvoice2Hdr(string DocNo, string ReceiptNo)
        {
            string Doctitle = Sm.GetParameter("Doctitle");
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesInvoice2Hdr ");
            SQL.AppendLine("(DocNo, ReceiptNo, DocDt, CancelReason, CancelInd, ProcessInd, ");
            SQL.AppendLine("CtCode, LocalDocNo, DueDt, SalesName, ");
            SQL.AppendLine("TaxInvDocument1, TaxInvDt1, TaxCode1, TaxAmt1, ");
            SQL.AppendLine("TaxInvDocument2, TaxInvDt2, TaxCode2, TaxAmt2, ");
            SQL.AppendLine("BankAcCode, TotalAmt, TotalTax, Downpayment, Amt, ");
            SQL.AppendLine("VoucherRequestPPNDocNo, VATSettlementDocNo, VoucherRequestPPNCompleteInd, JournalDocNo, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values");
            SQL.AppendLine("(@DocNo, @ReceiptNo, @DocDt, Null, 'N', 'O', ");
            SQL.AppendLine("@CtCode, @LocalDocNo, @DueDt, (Select SpName From TblSalesPerson Where SpCode=@SpCode), ");
            SQL.AppendLine("@TaxInvDocument1, @TaxInvDt1, @TaxCode1, @TaxAmt1, ");
            SQL.AppendLine("@TaxInvDocument2, @TaxInvDt2, @TaxCode2, @TaxAmt2, ");
            SQL.AppendLine("@BankAcCode, @TotalAmt, @TotalTax, @Downpayment, @Amt, ");
            SQL.AppendLine("Null, Null, 'N', Null, @Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");
            
            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (Doctitle == "KIM")
                Sm.CmParam<String>(ref cm, "@ReceiptNo", ReceiptNo);
            else
                Sm.CmParam<String>(ref cm, "@ReceiptNo", string.Empty);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@SpCode", Sm.GetLue(LueSPCode));
            Sm.CmParam<String>(ref cm, "@TaxInvDocument1", TxtTaxInvDocument1.Text);
            Sm.CmParamDt(ref cm, "@TaxInvDt1", Sm.GetDte(DteTaxInvDt1));
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", decimal.Parse(TxtTaxAmt1.Text));
            Sm.CmParam<String>(ref cm, "@TaxInvDocument2", TxtTaxInvDocument2.Text);
            Sm.CmParamDt(ref cm, "@TaxInvDt2", Sm.GetDte(DteTaxInvDt2));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtTaxAmt2.Text));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", decimal.Parse(TxtTotalAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalTax", decimal.Parse(TxtTotalTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@Downpayment", decimal.Parse(TxtDownpayment.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSalesInvoice2Dtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesInvoice2Dtl ");
            SQL.AppendLine("(DocNo, DNo, DOCt4DocNo, DOCt4DNo, ItCode, AssetInd, ");
            SQL.AppendLine("QtyPackagingUnit, PackagingUnitUomCode, Qty, PriceUomCode, ");
            SQL.AppendLine("UPrice, Discount, UPriceAfDisc, ");
            SQL.AppendLine("AmtBeforeTax1, TaxAmt1, AmtAfterTax1, ");
            SQL.AppendLine("AmtBeforeTax2, TaxAmt2, AmtAfterTax2, ");
            SQL.AppendLine("AmtBeforeTax, TaxAmt, AmtAfterTax, ");
            SQL.AppendLine("Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @DOCt4DocNo, @DOCt4DNo, @ItCode, @AssetInd, ");
            SQL.AppendLine("@QtyPackagingUnit, @PackagingUnitUomCode, @Qty, @PriceUomCode, ");
            SQL.AppendLine("@UPrice, @Discount, @UPriceAfDisc, ");
            SQL.AppendLine("@AmtBeforeTax1, @TaxAmt1, @AmtAfterTax1, ");
            SQL.AppendLine("@AmtBeforeTax2, @TaxAmt2, @AmtAfterTax2, ");
            SQL.AppendLine("@AmtBeforeTax, @TaxAmt, @AmtAfterTax, ");
            SQL.AppendLine("@Amt, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Update TblDOCt4Dtl Set SalesInvoice2DocNo=@DocNo ");
            SQL.AppendLine("Where DocNo=@DOCt4DocNo And DNo=@DOCt4DNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DOCt4DocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@DOCt4DNo", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@AssetInd", Sm.GetGrdBool(Grd1, Row, 7)?"Y":"N");
            Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@PriceUomCode", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Discount", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@UPriceAfDisc", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@AmtBeforeTax1", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@AmtAfterTax1", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@AmtBeforeTax2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@AmtAfterTax2", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@AmtBeforeTax", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<Decimal>(ref cm, "@AmtAfterTax", Sm.GetGrdDec(Grd1, Row, 23));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSalesInvoice2Dtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblSalesInvoice2Dtl2(DocNo, DNo, AcNo, DAmt, CAmt, OptAcDesc,  AcInd,  Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @OptAcDesc,  @AcInd, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd3, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd3, Row, 6));
            Sm.CmParam<String>(ref cm, "@OptAcDesc", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 9));
            Sm.CmParam<String>(ref cm, "@AcInd", Sm.GetGrdBool(Grd3, Row, 10) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSalesInvoice2Dtl3(string DocNo, DepositSummary i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesInvoice2Dtl3 ");
            SQL.AppendLine("(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, @CurCode, @ExcRate, @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblSalesInvoiceHdr Where DocNo=@DocNo; ");

            SQL.AppendLine("Update TblCustomerDepositSummary2 Set ");
            SQL.AppendLine("    Amt=Amt-@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where CtCode=@CtCode ");
            SQL.AppendLine("And CurCode=@CurCode ");
            SQL.AppendLine("And ExcRate=@ExcRate; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", i.ExcRate);
            Sm.CmParam<Decimal>(ref cm, "@Amt", i.UsedAmt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            
            var lDepositSummary = new List<DepositSummary>();

            if (ChkCancelInd.Checked &&
              decimal.Parse(TxtDownpayment.Text) != 0 &&
              !Sm.CompareStr(Sm.GetLue(LueCurCode), mMainCurCode)
              )
                GetDepositSummary2(ref lDepositSummary);

            var cml = new List<MySqlCommand>();

            cml.Add(EditSalesInvoice2Hdr());

            if (ChkCancelInd.Checked &&
                decimal.Parse(TxtDownpayment.Text) != 0)
            {
                cml.Add(SaveCustomerDeposit(TxtDocNo.Text));
                if (lDepositSummary.Count > 0)
                {
                    for (int i = 0; i < lDepositSummary.Count; i++)
                        cml.Add(SaveCustomerDepositSummary2(lDepositSummary[i]));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private MySqlCommand SaveCustomerDepositSummary2(DepositSummary i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCustomerDepositSummary2 Set ");
            SQL.AppendLine("    Amt=Amt+@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where CtCode=@CtCode ");
            SQL.AppendLine("And CurCode=@CurCode ");
            SQL.AppendLine("And ExcRate=@ExcRate; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", i.ExcRate);
            Sm.CmParam<Decimal>(ref cm, "@Amt", i.UsedAmt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToIncomingPayment() ||
                (ChkCancelInd.Checked && IsVoucherRequestPPNExisted()) ||
                (ChkCancelInd.Checked && IsVATSettlementExisted());
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblSalesInvoice2Hdr Where CancelInd='Y' And DocNo=@Param;", 
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private bool IsDataAlreadyProcessedToIncomingPayment()
        {
            return Sm.IsDataExist(
                "Select 1 From TblSalesInvoice2Hdr Where ProcessInd<>'O' And DocNo=@Param Limit 1;", 
                TxtDocNo.Text,
                "This document already processed to incoming payment.");
        }

        private bool IsVoucherRequestPPNExisted()
        {
            return Sm.IsDataExist(
                "Select 1 From TblSalesInvoice2Hdr Where DocNo=@Param And VoucherRequestPPNDocNo Is Not Null;",
                TxtDocNo.Text, 
                "This data is already processed to Voucher Request VAT.");
        }

        private bool IsVATSettlementExisted()
        {
            return Sm.IsDataExist(
                "Select 1 From TblSalesInvoice2Hdr Where DocNo=@Param And VATSettlementDocNo Is Not Null;",
                TxtDocNo.Text, 
                "This document already processed to VAT settlement.");
        }

        private MySqlCommand EditSalesInvoice2Hdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesInvoice2Hdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason = @CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N';");

            SQL.AppendLine("Update TblDOCt4Dtl Set SalesInvoice2DocNo=Null ");
            SQL.AppendLine("Where SalesInvoice2DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSalesInvoice2Hdr(DocNo);
                ShowSalesInvoice2Dtl(DocNo);
                ShowSalesInvoice2Dtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSalesInvoice2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.ReceiptNo, A.DocDt, A.CancelReason, A.CancelInd, A.CtCode, A.LocalDocNo, A.DueDt, B.SPCode, ");
            SQL.AppendLine("A.TaxInvDocument1, A.TaxInvDt1, A.TaxCode1, A.TaxAmt1, ");
            SQL.AppendLine("A.TaxInvDocument2, A.TaxInvDt2, A.TaxCode2, A.TaxAmt2, ");
            SQL.AppendLine("A.CurCode, A.TotalAmt, A.TotalTax, A.DownPayment, A.Amt, A.BankAcCode, A.Remark, ");
            SQL.AppendLine("A.JournalDocNo, A.JournalDocNo2 ");
            SQL.AppendLine("From TblSalesInvoice2Hdr A ");
            SQL.AppendLine("Left Join TblSalesPerson B On A.SalesName=B.SPName ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "ReceiptNo", "DocDt",  "CancelReason", "CancelInd", "CtCode", 
                        
                        //6-10
                        "LocalDocNo", "DueDt", "SPCode", "TaxInvDocument1", "TaxInvDt1", 
                        
                        //11-15
                        "TaxCode1", "TaxAmt1", "TaxInvDocument2", "TaxInvDt2", "TaxCode2",  
                        
                        //16-20
                        "TaxAmt2", "CurCode", "TotalAmt", "TotalTax", "DownPayment", 
                        
                        //21-25
                        "Amt", "BankAcCode", "Remark", "JournalDocNo", "JournalDocNo2"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtReceiptNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]); 
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[4]) == "Y" ? true : false;
                        SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[5]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[8]));
                        TxtTaxInvDocument1.EditValue = Sm.DrStr(dr, c[9]);
                        Sm.SetDte(DteTaxInvDt1, Sm.DrStr(dr, c[10]));
                        Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[11]));
                        TxtTaxAmt1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                        TxtTaxInvDocument2.EditValue = Sm.DrStr(dr, c[13]);
                        Sm.SetDte(DteTaxInvDt2, Sm.DrStr(dr, c[14]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[15]));
                        TxtTaxAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[17]));
                        TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                        TxtTotalTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[19]), 0);
                        TxtDownpayment.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[20]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[21]), 0);
                        Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[22]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[23]);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[24]);
                        TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[25]);
                    }, true
                );
        }

        private void ShowSalesInvoice2Dtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.DOCt4DocNo, A.DOCt4DNo, A.ItCode, ");
            SQL.AppendLine("Case A.AssetInd When 'Y' Then C.AssetName Else B.ItName End As ItName, ");
            SQL.AppendLine("A.AssetInd, A.QtyPackagingUnit, A.PackagingUnitUomCode, A.Qty, A.PriceUomCode, ");
            SQL.AppendLine("A.UPrice, A.Discount, A.UPriceAfDisc, ");
            SQL.AppendLine("A.AmtBeforeTax1, A.TaxAmt1, A.AmtAfterTax1, ");
            SQL.AppendLine("A.AmtBeforeTax2, A.TaxAmt2, A.AmtAfterTax2, ");
            SQL.AppendLine("A.AmtBeforeTax, A.TaxAmt, A.AmtAfterTax, ");
            SQL.AppendLine("A.Amt ");
            SQL.AppendLine("From TblSalesInvoice2Dtl A ");
            SQL.AppendLine("Left Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblAsset C On A.ItCode=C.AssetCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo; ");
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "DOCt4DocNo", "DOCt4DNo", "ItCode", "ItName", "AssetInd", 
                    
                    //6-10
                    "QtyPackagingUnit", "PackagingUnitUomCode", "Qty", "PriceUomCode", "UPrice", 
                    
                    //11-15
                    "Discount", "UPriceAfDisc", "AmtBeforeTax1", "TaxAmt1", "AmtAfterTax1", 
                    
                    //16-20
                    "AmtBeforeTax2", "TaxAmt2", "AmtAfterTax2", "AmtBeforeTax", "TaxAmt", 
                    
                    //21-22
                    "AmtAfterTax", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowSalesInvoice2Dtl2(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.AcInd, A.DAmt, A.CAmt, ");
            SQL.AppendLine("A.OptAcDesc, C.OptDesc, A.Remark ");
            SQL.AppendLine("From TblSalesInvoice2Dtl2 A ");
            SQL.AppendLine("Inner Join TblCOA B On A.AcNo=B.AcNo ");
            SQL.AppendLine("Left Join TblOption C On A.OptAcDesc=C.OptCode And C.OptCat='AccountDescriptionOnSalesInvoice' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo", 
                    "AcDesc", "AcInd", "DAmt", "CAmt", "OptAcDesc",
                    "OptDesc", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);

                    if (Sm.GetGrdBool(Grd3, Row, 10))
                    {
                        if (Sm.GetGrdDec(Grd3, Row, 4) > 0)
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        else
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 2);
                    }
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 5 });
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowCustomerDepositSummary(string CtCode)
        {
            ClearGrd2();

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select CurCode, Amt ");
            SQL.AppendLine("From TblCustomerDepositSummary ");
            SQL.AppendLine("Where CtCode=@CtCode Order By CurCode;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] { "CurCode", "Amt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 1 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void ProcessDepositSummary(ref List<DepositSummary> l)
        {
            if (Sm.CompareStr(Sm.GetLue(LueCurCode), mMainCurCode)) return;

            decimal Downpayment = 0m;

            if (TxtDownpayment.Text.Length > 0)
                Downpayment = decimal.Parse(TxtDownpayment.Text);

            if (Downpayment <= 0m) return;

            GetDepositSummary1(ref l);

            if (l.Count > 0m)
            {
                for (int i = 0; i < l.Count; i++)
                {
                    if (Downpayment > 0m)
                    {
                        if (Downpayment >= l[i].Amt)
                        {
                            l[i].UsedAmt = l[i].Amt;
                            Downpayment -= l[i].Amt;
                        }
                        else
                        {
                            l[i].UsedAmt = Downpayment;
                            Downpayment = 0m;
                            break;
                        }
                    }
                    else
                        break;
                }

                for (int i = l.Count - 1; i >= 0; i--)
                    if (l[i].UsedAmt == 0m) l.RemoveAt(i);
            }
        }

        private void GetDepositSummary1(ref List<DepositSummary> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ExcRate, Amt ");
            SQL.AppendLine("From TblCustomerDepositSummary2 ");
            SQL.AppendLine("Where CtCode=@CtCode ");
            SQL.AppendLine("And CurCode=@CurCode ");
            SQL.AppendLine("And Amt>0.00 ");
            SQL.AppendLine("Order By CreateDt; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ExcRate", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DepositSummary()
                        {
                            ExcRate = Sm.DrDec(dr, c[0]),
                            Amt = Sm.DrDec(dr, c[1]),
                            UsedAmt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetDepositSummary2(ref List<DepositSummary> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ExcRate, Amt ");
            SQL.AppendLine("From TblSalesInvoiceDtl3 ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By CreateDt; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ExcRate", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DepositSummary()
                        {
                            ExcRate = Sm.DrDec(dr, c[0]),
                            UsedAmt = Sm.DrDec(dr, c[1]),
                            Amt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsFormPrintOutInvoice = Sm.GetParameter("FormPrintOutInvoice");
            mIsCustomerItemNameMandatory = Sm.GetParameter("IsCustomerItemNameMandatory") == "Y";
            mIsRemarkForJournalMandatory = Sm.GetParameter("IsRemarkForJournalMandatory") == "Y";
            mEmpCodeSI = Sm.GetParameter("EmpCodeSI");
            mEmpCodeTaxCollector = Sm.GetParameter("EmpCodeTaxCollector");
            mSITaxRoundingDown = Sm.GetParameterBoo("SITaxRoundingDown");
            mAmtRoundingUp = Sm.GetParameterBoo("AmtRoundingUp");
        }

        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union All Select 'All' As Col1, 'All' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        private void LueRequestEdit(
         iGrid Grd,
         DevExpress.XtraEditors.LookUpEdit Lue,
         ref iGCell fCell,
         ref bool fAccept,
         TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueCtCode(ref DXE.LookUpEdit Lue, string CtCode)
        {
            try
            {
                var SQL = new StringBuilder();

                if (CtCode.Length == 0)
                {
                    SQL.AppendLine("Select Distinct A.CtCode As Col1, C.CtName As Col2 ");
                    SQL.AppendLine("From TblDOCt4Hdr A ");
                    SQL.AppendLine("Inner Join TblDOCt4Dtl B On A.DocNo=B.DocNo And B.SalesInvoice2DocNo Is Null And B.CancelInd='N' ");
                    SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode ");
                    SQL.AppendLine("Order By C.CtName; ");
                }
                else
                {
                    SQL.AppendLine("Select CtCode As Col1, CtName As Col2 ");
                    SQL.AppendLine("From TblCustomer Where CtCode=@CtCode;");
                }

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (CtCode.Length > 0) Sm.SetLue(Lue, CtCode);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeAmt()
        { 
            decimal Amt = 0m, TotalTax = 0m, Downpayment = 0m;
            
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                string AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", Sm.GetGrdStr(Grd3, Row, 1));
                if (Sm.GetGrdBool(Grd3, Row, 3))
                {
                    if (AcType == "D")
                        Amt += Sm.GetGrdDec(Grd3, Row, 4);
                    else
                        Amt -= Sm.GetGrdDec(Grd3, Row, 4);
                }
                if (Sm.GetGrdBool(Grd3, Row, 5))
                {
                    if (AcType == "C")
                        Amt += Sm.GetGrdDec(Grd3, Row, 6);
                    else
                        Amt -= Sm.GetGrdDec(Grd3, Row, 6);
                }
            }

            string 
                TaxCode1 = Sm.GetLue(LueTaxCode1), 
                TaxCode2 = Sm.GetLue(LueTaxCode2);
            decimal TaxRate1 = 0m, TaxRate2 = 0m;

            if (TaxCode1.Length > 0)
                TaxRate1 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode1);

            if (TaxCode2.Length > 0)
                TaxRate2 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode2);

            decimal
                Qty = 0m,
                UPrice = 0m,
                Discount = 0m,
                UPriceAfDisc = 0m,
                AmtBeforeTax1 = 0m,
                TaxAmt1 = 0m,
                AmtAfterTax1 = 0m,
                AmtBeforeTax2 = 0m,
                TaxAmt2 = 0m,
                AmtAfterTax2 = 0m,
                AmtBeforeTax = 0m,
                TaxAmt = 0m,
                AmtAfterTax = 0m,
                TotalTaxAmt1 = 0m,
                TotalTaxAmt2 = 0m;


            for (int r = 0; r<Grd1.Rows.Count; r++)
            { 
                if (Sm.GetGrdStr(Grd1, r, 2).Length>0)
                {
                    Qty = Sm.GetGrdDec(Grd1, r, 10);
                    UPrice = Sm.GetGrdDec(Grd1, r, 12);
                    Discount = Sm.GetGrdDec(Grd1, r, 13);
                    UPriceAfDisc = Sm.GetGrdDec(Grd1, r, 14);

                    AmtBeforeTax1 = UPriceAfDisc*Qty;
                    if (TaxRate1 > 0)
                    {
                        TaxAmt1 = mSITaxRoundingDown == true ? Math.Floor(AmtBeforeTax1 * TaxRate1 * 0.01m) : (AmtBeforeTax1 * TaxRate1 * 0.01m);
                    }
                    else
                    {
                        TaxRate1 = -1 * TaxRate1;
                        TaxAmt1 = mSITaxRoundingDown == true ? (-1 * Math.Floor(AmtBeforeTax1 * TaxRate1 * 0.01m)) : (-1 * (AmtBeforeTax1 * TaxRate1 * 0.01m));
                    }
                    AmtAfterTax1 = AmtBeforeTax1 + TaxAmt1;
                    AmtBeforeTax2 = AmtAfterTax1;
                    if (TaxRate2 > 0)
                    {
                        TaxAmt2 = mSITaxRoundingDown == true ? Math.Floor(AmtBeforeTax1 * TaxRate2 * 0.01m) : (AmtBeforeTax1 * TaxRate2 * 0.01m);
                    }
                    else
                    {
                        TaxRate2 = -1 * TaxRate2;
                        TaxAmt2 = mSITaxRoundingDown == true ? (-1 * Math.Floor(AmtBeforeTax1 * TaxRate2 * 0.01m)) : (-1 * (AmtBeforeTax1 * TaxRate2 * 0.01m));
                    }
                    AmtAfterTax2 = AmtBeforeTax2 + TaxAmt2;
                    AmtBeforeTax = AmtBeforeTax1;
                    TaxAmt = TaxAmt1 + TaxAmt2;
                    AmtAfterTax = AmtBeforeTax+TaxAmt;

                    Amt += AmtBeforeTax;
                    TotalTaxAmt1 += TaxAmt1;
                    TotalTaxAmt2 += TaxAmt2;
                    TotalTax += TaxAmt;

                    Grd1.Cells[r, 15].Value = AmtBeforeTax1;
                    Grd1.Cells[r, 16].Value = TaxAmt1;
                    Grd1.Cells[r, 17].Value = AmtAfterTax1;
                    Grd1.Cells[r, 18].Value = AmtBeforeTax2;
                    Grd1.Cells[r, 19].Value = TaxAmt2;
                    Grd1.Cells[r, 20].Value = AmtAfterTax2;
                    Grd1.Cells[r, 21].Value = AmtBeforeTax;
                    Grd1.Cells[r, 22].Value = TaxAmt;
                    if (mAmtRoundingUp)
                    {
                        Grd1.Cells[r, 23].Value = Math.Ceiling(AmtAfterTax);
                        Grd1.Cells[r, 24].Value = Math.Ceiling(AmtAfterTax);
                    }
                    else
                    {
                        Grd1.Cells[r, 23].Value = AmtAfterTax;
                        Grd1.Cells[r, 24].Value = AmtAfterTax;
                    }
                }
            }

            TxtTotalAmt.EditValue = Sm.FormatNum(Amt, 0);
            TxtTaxAmt1.EditValue = Sm.FormatNum(TotalTaxAmt1, 0);
            TxtTaxAmt2.EditValue = Sm.FormatNum(TotalTaxAmt2, 0);
            TxtTotalTax.EditValue = Sm.FormatNum(TotalTax, 0);

            if (TxtDownpayment.Text.Length > 0) Downpayment = decimal.Parse(TxtDownpayment.Text);

            TxtAmt.EditValue = Sm.FormatNum(Amt+TotalTax-Downpayment, 0);
        }

        private void ParPrint()
        {
            if (Sm.GetParameter("Doctitle") == "KIM")
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
                {
                    var l2 = new List<InvoiceHdr>();
                    var ldtl = new List<InvoiceDtl>();
                    var ldtl2 = new List<InvoiceDtl2>();
                    var l3 = new List<Employee>();
                    var l4 = new List<EmployeeTaxCollector>();

                    string[] TableName = { "InvoiceHdr", "InvoiceDtl", "InvoiceDtl2", "Employee", "EmployeeTaxCollector" };

                    List<IList> myLists = new List<IList>();
                    var cm2 = new MySqlCommand();

                    #region Header KIM
                    var SQL2 = new StringBuilder();

                    SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
                    SQL2.AppendLine("A.DocNo,");
                    // Date_Format(A.DocDt,'%d %M %Y')As DocDt, Date_Format(A.DueDt,'%d %M %Y')As DueDt, ");
                    SQL2.AppendLine("Concat(Right(A.DocDt,2),' ',Case substring(A.DocDt,5,2) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' ");
                    SQL2.AppendLine("WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' ");
                    SQL2.AppendLine("WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END, ' ', Left(A.DocDt,4))As DocDt, ");

                    SQL2.AppendLine("Concat(Right(A.DueDt,2),' ',Case substring(A.DueDt,5,2) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' ");
                    SQL2.AppendLine("WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' ");
                    SQL2.AppendLine("WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END, ' ', Left(A.DueDt,4))As DueDt, ");

                    SQL2.AppendLine("A.CurCode, A.TotalTax, A.TotalAmt, A.DownPayment, B.CtName, ifnull(G.AssetLocation, B.Address) Address, D.SAName, D.SAAddress, D.Remark, ");
                    SQL2.AppendLine("IfNull(A.TaxCode1, null) As TaxCode1,IfNull(A.TaxCode2, null) As TaxCode2, A.Amt, E.CityName,F.CityName As SACityName, ");

                    SQL2.AppendLine("(Select Z.TaxName From TblTax Z ");
                    SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxName1, ");
                    SQL2.AppendLine("(Select Z.TaxName From TblTax Z ");
                    SQL2.AppendLine("Inner Join TblSalesInvoice2Hdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxName2, ");
                    SQL2.AppendLine("(Select Z.TaxRate From TblTax Z ");
                    SQL2.AppendLine("Inner Join TblSalesInvoice2Hdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxRate1, ");
                    SQL2.AppendLine("(Select Z.TaxRate From TblTax Z ");
                    SQL2.AppendLine("Inner Join TblSalesInvoice2Hdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxRate2, ");
                    SQL2.AppendLine("A.Remark As RemarkSI, B.NPWP, A.ReceiptNo ");

                    SQL2.AppendLine("From TblSalesInvoice2Hdr A ");
                    SQL2.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                    SQL2.AppendLine("Inner join ( ");
                    SQL2.AppendLine("		Select A.DocNo, B.DOCt4DocNo ");
                    SQL2.AppendLine("		From TblSalesInvoice2Hdr A ");
                    SQL2.AppendLine("		Inner join TblSalesInvoice2dtl B On A.DocNo=B.DocNo ");
                    SQL2.AppendLine("		group by A.DocNo ");
                    SQL2.AppendLine("	) C On A.DocNo=C.DocNo ");
                    SQL2.AppendLine("Inner Join TblDOCt4Hdr D On C.DOCt4DocNo= D.DocNo ");
                    SQL2.AppendLine("Left Join TblCity E On B.CityCode= E.CityCode ");
                    SQL2.AppendLine("Left Join TblCity F On D.SACityCode= F.CityCode ");
                    SQL2.AppendLine("Left Join  ");
                    SQL2.AppendLine("( ");
	                SQL2.AppendLine("    Select A.DocNo, B.AssetLocation  From TblSalesInvoice2Dtl A ");
	                SQL2.AppendLine("    Inner Join TblCustomerAsset B On A.ItCode = B.AssetCode ");
	                SQL2.AppendLine("    Where A.Dno = '001' ");
                    SQL2.AppendLine(")G On A.DocNo = G.DocNo ");
                    SQL2.AppendLine("Where A.DocNo=@DocNo ");

                    using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn2.Open();
                        cm2.Connection = cn2;
                        cm2.CommandText = SQL2.ToString();
                        Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                        Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                        var dr2 = cm2.ExecuteReader();
                        var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressFull",
                         "CompanyPhone",
                         "CompanyFax",

                         //6-10
                         "DocNo",
                         "DocDt",
                         "DueDt",
                         "CurCode",
                         "TotalTax",

                         //11-15
                         "TotalAmt",
                         "DownPayment", 
                         "CtName",
                         "Address",
                         "SAName",

                         //16-20
                         "SAAddress",
                         "Remark",
                         "TaxCode1",
                         "TaxCode2",
                         "Amt",

                         //21-25
                         "CityName",
                         "SACityName",
                         "TaxName1",
                         "TaxName2",
                         "TaxRate1",

                         //26-29
                         "TaxRate2",
                         "RemarkSI",
                         "NPWP",
                         "ReceiptNo"
                         

                        });
                        if (dr2.HasRows)
                        {
                            while (dr2.Read())
                            {
                                l2.Add(new InvoiceHdr()
                                {
                                    CompanyLogo = Sm.DrStr(dr2, c2[0]),

                                    CompanyName = Sm.DrStr(dr2, c2[1]),
                                    CompanyAddress = Sm.DrStr(dr2, c2[2]),
                                    CompanyAddressFull = Sm.DrStr(dr2, c2[3]),
                                    CompanyPhone = Sm.DrStr(dr2, c2[4]),
                                    CompanyFax = Sm.DrStr(dr2, c2[5]),

                                    DocNo = Sm.DrStr(dr2, c2[6]),
                                    DocDt = Sm.DrStr(dr2, c2[7]),
                                    DueDt = Sm.DrStr(dr2, c2[8]),
                                    CurCode = Sm.DrStr(dr2, c2[9]),
                                    TotalTax = Sm.DrDec(dr2, c2[10]),

                                    TotalAmt = Sm.DrDec(dr2, c2[11]),
                                    DownPayment = Sm.DrDec(dr2, c2[12]),
                                    CtName = Sm.DrStr(dr2, c2[13]),
                                    Address = Sm.DrStr(dr2, c2[14]),
                                    SAName = Sm.DrStr(dr2, c2[15]),

                                    SAAddress = Sm.DrStr(dr2, c2[16]),
                                    Remark = Sm.DrStr(dr2, c2[17]),
                                    TaxCode1 = Sm.DrStr(dr2, c2[18]),
                                    TaxCode2 = Sm.DrStr(dr2, c2[19]),
                                    Amt = Sm.DrDec(dr2, c2[20]),

                                    Terbilang = Sm.Terbilang(Sm.DrDec(dr2, c2[20])),
                                    CityName = Sm.DrStr(dr2, c2[21]),
                                    SACityName = Sm.DrStr(dr2, c2[22]),
                                    TaxName1 = Sm.DrStr(dr2, c2[23]),
                                    TaxName2 = Sm.DrStr(dr2, c2[24]),
                                    TaxRate1 = Sm.DrDec(dr2, c2[25]),

                                    TaxRate2 = Sm.DrDec(dr2, c2[26]),
                                    RemarkSI = Sm.DrStr(dr2, c2[27]),
                                    NPWP = Sm.DrStr(dr2, c2[28]),
                                    ReceiptNo = Sm.DrStr(dr2, c2[29]),

                                   

                                });
                            }
                        }
                        dr2.Close();
                    }
                    myLists.Add(l2);
                    #endregion

                    #region Detail

                    var cmDtl = new MySqlCommand();
                    var SQLDtl = new StringBuilder();
                    using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtl.Open();
                        cmDtl.Connection = cnDtl;
                        SQLDtl.AppendLine("Select A.ItCode, B.ItName, A.Qty, A.UPriceAfDisc,(A.Qty*A.UPriceAfDisc)As Amt, B.SalesuomCode As PriceUomCode, C.Remark ");
                        SQLDtl.AppendLine("From TblSalesInvoice2Dtl A ");
                        SQLDtl.AppendLine("Inner join tblitem B On A.ItCode=B.ItCode ");
                        SQLDtl.AppendLine("Inner join tbldoct4dtl C On A.Doct4Docno=C.DocNo And A.DOCt4DNo=C.DNo ");
                        SQLDtl.AppendLine("Where A.DocNo=@DocNo ");

                        cmDtl.CommandText = SQLDtl.ToString();
                        Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                        var drDtl = cmDtl.ExecuteReader();
                        var cDtl = Sm.GetOrdinal(drDtl, new string[]
                {
                    //0
                    "ItCode" ,

                    //1-5
                    "ItName" ,
                    "Qty",
                    "UPriceAfDisc",
                    "Amt",
                    "PriceUomCode",

                    "Remark"

                });

                        if (drDtl.HasRows)
                        {
                            while (drDtl.Read())
                            {
                                ldtl.Add(new InvoiceDtl()
                                {
                                    ItCode = Sm.DrStr(drDtl, cDtl[0]),
                                    ItName = Sm.DrStr(drDtl, cDtl[1]),
                                    Qty = Sm.DrDec(drDtl, cDtl[2]),
                                    UPriceAfDisc = Sm.DrDec(drDtl, cDtl[3]),
                                    Amt = Sm.DrDec(drDtl, cDtl[4]),
                                    PriceUomCode = Sm.DrStr(drDtl, cDtl[5]),

                                    Remark = Sm.DrStr(drDtl, cDtl[6]),
                                });
                            }
                        }

                        drDtl.Close();
                    }

                    myLists.Add(ldtl);

                    #endregion

                    #region Detail

                    var cmDtl2 = new MySqlCommand();
                    var SQLDtl2 = new StringBuilder();
                    using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtl2.Open();
                        cmDtl2.Connection = cnDtl2;
                        SQLDtl2.AppendLine("Select A.DocNo, ifnull(sum(B.DAmt),0)As DAmt, ifnull(sum(B.CAmt),0) As CAmt, ifnull(sum(B.DAmt),0)+ ifnull(sum(B.CAmt),0) As TAmt ");
                        SQLDtl2.AppendLine("From tblsalesinvoice2hdr A ");
                        SQLDtl2.AppendLine("Left join tblsalesinvoice2dtl2 B On A.DocNo=B.DocNo");
                        SQLDtl2.AppendLine("Where A.DocNo=@DocNo And B.AcInd='Y' ");
                        SQLDtl2.AppendLine("Group by B.Docno ");

                        cmDtl2.CommandText = SQLDtl2.ToString();
                        Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                        var drDtl2 = cmDtl2.ExecuteReader();
                        var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                {
                    //0
                    "DocNo" ,

                    //1-2
                    "DAmt" ,
                    "CAmt",
                    "TAmt",

                });

                        if (drDtl2.HasRows)
                        {
                            while (drDtl2.Read())
                            {
                                ldtl2.Add(new InvoiceDtl2()
                                {
                                    DocNo = Sm.DrStr(drDtl2, cDtl2[0]),
                                    DAmt = Sm.DrDec(drDtl2, cDtl2[1]),
                                    CAmt = Sm.DrDec(drDtl2, cDtl2[2]),
                                    TAmt = Sm.DrDec(drDtl2, cDtl2[3]),
                                });
                            }
                        }

                        drDtl2.Close();
                    }

                    myLists.Add(ldtl2);

                    #endregion

                    #region Signature KIM
                    var cm3 = new MySqlCommand();
                    var SQL3 = new StringBuilder();

                    SQL3.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict ");
                    SQL3.AppendLine("From TblEmployee A ");
                    SQL3.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
                    SQL3.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
                    SQL3.AppendLine("Where A.EmpCode=@EmpCode ");

                    using (var cn3 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn3.Open();
                        cm3.Connection = cn3;
                        cm3.CommandText = SQL3.ToString();
                        Sm.CmParam<String>(ref cm3, "@EmpCode", mEmpCodeSI);
                        var dr3 = cm3.ExecuteReader();
                        var c3 = Sm.GetOrdinal(dr3, new string[] 
                        {
                         //0-3
                         "EmpCode",
                         "EmpName",
                         "PosName",
                         "EmpPict"
                        
                        });
                        if (dr3.HasRows)
                        {
                            while (dr3.Read())
                            {
                                l3.Add(new Employee()
                                {
                                    EmpCode = Sm.DrStr(dr3, c3[0]),

                                    EmpName = Sm.DrStr(dr3, c3[1]),
                                    Position = Sm.DrStr(dr3, c3[2]),
                                    EmpPict = Sm.DrStr(dr3, c3[3]),
                                });
                            }
                        }
                        dr3.Close();
                    }
                    myLists.Add(l3);

                    #endregion

                    #region Signature2 KIM
                    var cm4 = new MySqlCommand();
                    var SQL4 = new StringBuilder();

                    SQL4.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, A.Mobile ");
                    SQL4.AppendLine("From TblEmployee A ");
                    SQL4.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
                    SQL4.AppendLine("Where A.EmpCode=@EmpCode ");

                    using (var cn4 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn4.Open();
                        cm4.Connection = cn4;
                        cm4.CommandText = SQL4.ToString();
                        Sm.CmParam<String>(ref cm4, "@EmpCode", mEmpCodeTaxCollector);
                        var dr4 = cm4.ExecuteReader();
                        var c4 = Sm.GetOrdinal(dr4, new string[] 
                        {
                         //0-3
                         "EmpCode",
                         "EmpName",
                         "PosName",
                         "Mobile"
                        
                        });
                        if (dr4.HasRows)
                        {
                            while (dr4.Read())
                            {
                                l4.Add(new EmployeeTaxCollector()
                                {
                                    EmpCode = Sm.DrStr(dr4, c4[0]),

                                    EmpName = Sm.DrStr(dr4, c4[1]),
                                    Position = Sm.DrStr(dr4, c4[2]),
                                    Mobile = Sm.DrStr(dr4, c4[3]),
                                });
                            }
                        }
                        dr4.Close();
                    }
                    myLists.Add(l4);

                    #endregion

                    Sm.PrintReport("Invoice4", myLists, TableName, false);
                    Sm.PrintReport("ReceiptSIKIM4", myLists, TableName, false);

                }

            }
        }

        internal string GenerateReceipt(string DocDt)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From TblSalesInvoiceHdr ");
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblSalesInvoiceHdr ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
            //SQL.Append("   ), '0001') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + "KWT" + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As ReceiptNo");

            return Sm.GetValue(SQL.ToString());
        }

        //internal string GenerateReceipt(string DocDt)
        //{
        //    string
        //        Yr = DocDt.Substring(2, 2),
        //        Mth = DocDt.Substring(4, 2),
        //        DocTitle = Sm.GetParameter("DocTitle");
        //    //  DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");

        //    var SQL = new StringBuilder();

        //    SQL.Append("Select Concat( ");
        //    SQL.Append("IfNull(( ");
        //    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
        //    SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblSalesInvoice2Hdr ");
        //    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
        //    SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
        //    SQL.Append("       ) As Temp ");
        //    SQL.Append("   ), '0001') ");
        //    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
        //    SQL.Append("', '/', '" + "KWT" + "', '/', '" + Mth + "','/', '" + Yr + "'");
        //    SQL.Append(") As ReceiptNo");

        //    return Sm.GetValue(SQL.ToString());
        //}

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            DteDueDt.EditValue = null;
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                var TheFont = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd1.Font = TheFont;
                Grd2.Font = TheFont;
            }
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ClearGrd();
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(SetLueCtCode), string.Empty);
                if (Sm.GetLue(LueCtCode).Length > 0) ShowCustomerDepositSummary(Sm.GetLue(LueCtCode));
                ComputeAmt();
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ClearGrd();
                Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
                ComputeAmt();
            }
        }

        private void TxtDownpayment_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDownpayment, 0);
                ComputeAmt();
            }
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void LueOption_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOption, new Sm.RefreshLue2(Sl.SetLueOption), "AccountDescriptionOnSalesInvoice");
        }

        private void LueOption_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueOption_Leave(object sender, EventArgs e)
        {
            if (LueOption.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LueOption).Length == 0)
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value =
                    Grd3.Cells[fCell.RowIndex, 8].Value = null;
                }
                else
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LueOption);
                    Grd3.Cells[fCell.RowIndex, 8].Value = LueOption.GetColumnValue("Col2");
                }
                LueOption.Visible = false;
            }
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtTaxInvDocument1_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvDocument1);
        }

        private void TxtTaxInvDocument2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvDocument2);
        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        #endregion

        #region Button Event

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo2, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo2.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid

        #region Grd1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDOCt4("X");
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer") && !Sm.IsLueEmpty(LueCurCode, "Currency"))
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmSalesInvoice4Dlg(this, Sm.GetLue(LueCtCode), Sm.GetLue(LueCurCode)));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            ComputeAmt();
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1 && !Sm.IsLueEmpty(LueCtCode, "Customer") && !Sm.IsLueEmpty(LueCurCode, "Currency"))
                Sm.FormShowDialog(new FrmSalesInvoice4Dlg(this, Sm.GetLue(LueCtCode), Sm.GetLue(LueCurCode)));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmDOCt4("X");
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #region Grd3

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdDec(Grd3, e.RowIndex, 4)>0)
            {
                Grd3.Cells[e.RowIndex, 6].Value = 0;
            }

            if (e.ColIndex == 6 && Sm.GetGrdDec(Grd3, e.RowIndex, 6)>0)
            {
                Grd3.Cells[e.RowIndex, 4].Value = 0;
            }

            if (e.ColIndex == 3 || e.ColIndex == 5)
                Grd3.Cells[e.RowIndex, 10].Value = Sm.GetGrdBool(Grd3, e.RowIndex, e.ColIndex);

            if (Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6 }, e.ColIndex))
                ComputeAmt();
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSalesInvoice4Dlg2(this));
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
            {
                LueRequestEdit(Grd3, LueOption, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
                Sl.SetLueOption(ref LueOption, "AccountDescriptionOnSalesInvoice");
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmSalesInvoice4Dlg2(this));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeAmt();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #endregion

        #region Class

        #region Report Class

        private class InvoiceHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string DueDt { get; set; }
            public string CurCode { get; set; }
            public decimal TotalTax { get; set; }
            public decimal TotalAmt { get; set; }
            public decimal DownPayment { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string Remark { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public decimal TaxRate1 { get; set; }
            public decimal TaxRate2 { get; set; }
            public decimal Amt { get; set; }
            public string Terbilang { get; set; }
            public string CityName { get; set; }
            public string SACityName { get; set; }
            public string NPWP { get; set; }
            public string RemarkSI { get; set; }
            public string ReceiptNo { get; set; }
        }

        class InvoiceDtl
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public decimal UPriceAfDisc { get; set; }
            public decimal Amt { get; set; }
            public string PriceUomCode { get; set; }
            public string Remark { get; set; }
        }

        class InvoiceDtl2
        {
            public string DocNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal TAmt { get; set; }
        }

        private class Employee
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string EmpPict { get; set; }
        }

        private class EmployeeTaxCollector
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string Mobile { get; set; }
        }

        #endregion             

        #region Rate Class

        private class DepositSummary
        {
            public decimal ExcRate { get; set; }
            public decimal Amt { get; set; }
            public decimal UsedAmt { get; set; }
        }

        #endregion

        #endregion
    }
}
