﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPosFunction : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmPosFunctionFind FrmFind;
        
        #endregion

        #region Constructor

        public FrmPosFunction(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtFuncCode, TxtFuncName, TxtFuncKey, TxtPassword }, true);
                    TxtFuncCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtFuncCode, TxtFuncName, TxtFuncKey, TxtPassword }, false);
                    TxtFuncCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtFuncName, TxtFuncKey, TxtPassword }, false);
                    TxtFuncName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtFuncCode, TxtFuncName, TxtFuncKey, TxtPassword });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPosFunctionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtFuncCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtFuncCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblPosFunction Where FuncCode=@FuncCode;" };
                Sm.CmParam<String>(ref cm, "@FuncCode", TxtFuncCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblPosFunction(FuncCode, FuncName, FuncKey, Password, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@FuncCode, @FuncName, @FuncKey, @Password, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update FuncName=@FuncName, FuncKey=@FuncKey, Password=@Password, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FuncCode", TxtFuncCode.Text);
                Sm.CmParam<String>(ref cm, "@FuncName", TxtFuncName.Text);
                Sm.CmParam<String>(ref cm, "@FuncKey", TxtFuncKey.Text);
                Sm.CmParam<String>(ref cm, "@Password", TxtPassword.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtFuncCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string FuncCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@FuncCode", FuncCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select FuncCode, FuncName, FuncKey, Password From TblPosFunction Where FuncCode=@FuncCode;",
                        new string[] { "FuncCode", "FuncName", "FuncKey", "Password" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtFuncCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtFuncName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtFuncKey.EditValue = Sm.DrStr(dr, c[2]);
                            TxtPassword.EditValue = Sm.DrStr(dr, c[3]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtFuncCode, "Function code", false) ||
                Sm.IsTxtEmpty(TxtFuncName, "Function name", false) ||
                Sm.IsTxtEmpty(TxtFuncKey, "Function key", false) ||
                IsCodeAlreadyExisted();
        }

        private bool IsCodeAlreadyExisted()
        {
            return 
                !TxtFuncCode.Properties.ReadOnly && 
                Sm.IsDataExist(
                    "Select FuncCode From TblPosFunction Where FuncCode=@Param;",
                    TxtFuncCode.Text,
                    "Function code ( " + TxtFuncCode.Text + " ) already existed."
                    );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtFuncCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtFuncCode);
        }

        private void TxtFuncName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtFuncName);
        }

        private void TxtFuncKey_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtFuncKey);
        }

        private void TxtPassword_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPassword);
        }

        #endregion

        #endregion
    }
}
