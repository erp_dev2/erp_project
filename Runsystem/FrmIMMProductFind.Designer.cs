﻿namespace RunSystem
{
    partial class FrmIMMProductFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.TxtProdCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkProdCode = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtColor = new DevExpress.XtraEditors.TextEdit();
            this.ChkColor = new DevExpress.XtraEditors.CheckEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.ChkProdCtCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueProdCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtColor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkColor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProdCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProdCtCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.ChkProdCtCode);
            this.panel2.Controls.Add(this.LueProdCtCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtColor);
            this.panel2.Controls.Add(this.ChkColor);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtProdCode);
            this.panel2.Controls.Add(this.ChkProdCode);
            this.panel2.Size = new System.Drawing.Size(672, 76);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 397);
            this.Grd1.TabIndex = 15;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 14);
            this.label6.TabIndex = 9;
            this.label6.Text = "Product";
            // 
            // TxtProdCode
            // 
            this.TxtProdCode.EnterMoveNextControl = true;
            this.TxtProdCode.Location = new System.Drawing.Point(68, 5);
            this.TxtProdCode.Name = "TxtProdCode";
            this.TxtProdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProdCode.Properties.MaxLength = 80;
            this.TxtProdCode.Size = new System.Drawing.Size(301, 20);
            this.TxtProdCode.TabIndex = 10;
            this.TxtProdCode.Validated += new System.EventHandler(this.TxtProdCode_Validated);
            // 
            // ChkProdCode
            // 
            this.ChkProdCode.Location = new System.Drawing.Point(372, 3);
            this.ChkProdCode.Name = "ChkProdCode";
            this.ChkProdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProdCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProdCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProdCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProdCode.Properties.Caption = " ";
            this.ChkProdCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProdCode.Size = new System.Drawing.Size(21, 22);
            this.ChkProdCode.TabIndex = 11;
            this.ChkProdCode.ToolTip = "Remove filter";
            this.ChkProdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProdCode.ToolTipTitle = "Run System";
            this.ChkProdCode.CheckedChanged += new System.EventHandler(this.ChkProdCode_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Color";
            // 
            // TxtColor
            // 
            this.TxtColor.EnterMoveNextControl = true;
            this.TxtColor.Location = new System.Drawing.Point(68, 26);
            this.TxtColor.Name = "TxtColor";
            this.TxtColor.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtColor.Properties.Appearance.Options.UseFont = true;
            this.TxtColor.Properties.MaxLength = 80;
            this.TxtColor.Size = new System.Drawing.Size(301, 20);
            this.TxtColor.TabIndex = 13;
            this.TxtColor.Validated += new System.EventHandler(this.TxtColor_Validated);
            // 
            // ChkColor
            // 
            this.ChkColor.Location = new System.Drawing.Point(372, 24);
            this.ChkColor.Name = "ChkColor";
            this.ChkColor.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkColor.Properties.Appearance.Options.UseFont = true;
            this.ChkColor.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkColor.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkColor.Properties.Caption = " ";
            this.ChkColor.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkColor.Size = new System.Drawing.Size(21, 22);
            this.ChkColor.TabIndex = 14;
            this.ChkColor.ToolTip = "Remove filter";
            this.ChkColor.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkColor.ToolTipTitle = "Run System";
            this.ChkColor.CheckedChanged += new System.EventHandler(this.ChkColor_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(7, 50);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 14);
            this.label5.TabIndex = 15;
            this.label5.Text = "Category";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkProdCtCode
            // 
            this.ChkProdCtCode.Location = new System.Drawing.Point(372, 46);
            this.ChkProdCtCode.Name = "ChkProdCtCode";
            this.ChkProdCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProdCtCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProdCtCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProdCtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProdCtCode.Properties.Caption = " ";
            this.ChkProdCtCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProdCtCode.Size = new System.Drawing.Size(21, 22);
            this.ChkProdCtCode.TabIndex = 17;
            this.ChkProdCtCode.ToolTip = "Remove filter";
            this.ChkProdCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProdCtCode.ToolTipTitle = "Run System";
            this.ChkProdCtCode.CheckedChanged += new System.EventHandler(this.ChkProdCtCode_CheckedChanged);
            // 
            // LueProdCtCode
            // 
            this.LueProdCtCode.EnterMoveNextControl = true;
            this.LueProdCtCode.Location = new System.Drawing.Point(68, 47);
            this.LueProdCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueProdCtCode.Name = "LueProdCtCode";
            this.LueProdCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProdCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueProdCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProdCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProdCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProdCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProdCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProdCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProdCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProdCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProdCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProdCtCode.Properties.DropDownRows = 30;
            this.LueProdCtCode.Properties.NullText = "[Empty]";
            this.LueProdCtCode.Properties.PopupWidth = 250;
            this.LueProdCtCode.Size = new System.Drawing.Size(301, 20);
            this.LueProdCtCode.TabIndex = 16;
            this.LueProdCtCode.ToolTip = "F4 : Show/hide list";
            this.LueProdCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProdCtCode.EditValueChanged += new System.EventHandler(this.LueProdCtCode_EditValueChanged);
            // 
            // FrmIMMProductFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmIMMProductFind";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtColor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkColor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProdCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProdCtCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtColor;
        private DevExpress.XtraEditors.CheckEdit ChkColor;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtProdCode;
        private DevExpress.XtraEditors.CheckEdit ChkProdCode;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.CheckEdit ChkProdCtCode;
        private DevExpress.XtraEditors.LookUpEdit LueProdCtCode;
    }
}