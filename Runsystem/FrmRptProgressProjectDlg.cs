﻿#region Update
/*
    01/03/2023 [WED/MNET] new dialog
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProgressProjectDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptProgressProject mFrmParent;
        string mPDDocNos = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProgressProjectDlg(FrmRptProgressProject FrmParent, string PDDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPDDocNos = PDDocNo;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            BtnChoose.Visible = BtnRefresh.Visible = false;
            SetGrd();
            ShowData();
        }

        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-2
                    "Document#",
                    ""
                }, new int[]
                {
                    //0
                    50,

                    //1-2
                    180, 20
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
        }
        #endregion

        #region Grid Method

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmProjectDelivery(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Show Data
        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string[] docs = mPDDocNos.Split(',');
                Sm.ClearGrd(Grd1, false);

                Grd1.BeginUpdate();
                int Row = 0;

                foreach (var x in docs.Where(w => w.Length > 0))
                {
                    Grd1.Rows.Add();

                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Grd1.Cells[Row, 1].Value = x;
                    
                    Row++;
                }
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
        }
        #endregion



        #endregion

    }
}
