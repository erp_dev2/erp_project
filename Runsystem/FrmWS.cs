﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWS : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty;
        private string mLGCodeForAnnualLeave = string.Empty;
        internal FrmWSFind FrmFind;

        #endregion

        #region Constructor

        public FrmWS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Working Sheet";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    TxtDocNo, TxtATDDocNo, TxtPeriodYr, TxtPeriodMth, DteStartDt, 
                    DteEndDt, TxtDeptCode
                }, true);
                GetParameter();
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    //ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's"+Environment.NewLine+"Name",
                        "Employee's"+Environment.NewLine+"Old Code", 
                        "Position",
                        "Join"+Environment.NewLine+"Date",

                        //6    
                        "Resign"+Environment.NewLine+"Date"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        100, 250, 100, 150, 100,
                        
                        //6
                        100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6 });
            Sm.GrdFormatDate(Grd1, new int[] { 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ DteDocDt, ChkCancelInd, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ DteDocDt, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtATDDocNo, TxtPeriodYr, TxtPeriodMth, 
                DteStartDt, DteEndDt, TxtDeptCode, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 0 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWSFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.FormShowDialog(new FrmWSDlg(this));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex!=0) e.DoDefault = false;
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0 && Sm.GetGrdStr(Grd1, 0, 1).Length != 0)
            {
                var IsSelected = Sm.GetGrdBool(Grd1, 0, 0);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0) Grd1.Cells[Row, 0].Value = IsSelected;
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "WS", "TblWSHdr");

            var lhdr = new List<WSSummaryHdr>();
            var ldtl = new List<WSSummaryDtl>();

            GenerateData(DocNo, ref lhdr, ref ldtl);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveWSHdr(DocNo));

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdBool(Grd1, Row, 0)) cml.Add(SaveWSDtl(DocNo, Row));
            }

            lhdr.ForEach(i => { cml.Add(SaveWSSummaryHdr(DocNo, ref i)); });
            ldtl.ForEach(i => { cml.Add(SaveWSSummaryDtl(DocNo, ref i)); });

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtATDDocNo, "Attention#", false) ||
                IsATDDocNoAlreadyCancelled() ||
                IsGrdEmpty() ||
                IsEmployeeNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No employee selected.");
                return true;
            }
            return false;
        }

        private bool IsEmployeeNotValid()
        {
            string EmpCode = GetSelectedEmployee();

            if (EmpCode.Length==0)
            {
                Sm.StdMsg(mMsgType.Warning, "No employee selected.");
                return true;
            }

            if (IsEmployeeAlreadyProcessed(EmpCode)) return true;
            return false;
        }

        private bool IsEmployeeAlreadyProcessed(string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.EmpName ");
            SQL.AppendLine("From TblWSHdr A ");
            SQL.AppendLine("Inner Join TblWSDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And Position(Concat('##', B.EmpCode, '##') In @SelectedEmployee)>0 ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode=C.EmpCode ");
            SQL.AppendLine("Where A.ATDDocNo=@ATDDocNo ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ATDDocNo", TxtATDDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SelectedEmployee", EmpCode);

            var EmpName = Sm.GetValue(cm);
            if (EmpName.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Employee : " + EmpName + Environment.NewLine +
                    "Employee already processed in another working sheet."
                    );
                return true;
            }
            return false;
        }

        private bool IsATDDocNoAlreadyCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblATDHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@ATDDocNo ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ATDDocNo", TxtATDDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Attendance already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveWSHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText = 
                    "Insert Into TblWSHdr(DocNo, DocDt, CancelInd, ATDDocNo, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'N', @ATDDocNo, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ATDDocNo", TxtATDDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }   

        private MySqlCommand SaveWSDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand() 
            { 
                CommandText =  
                    "Insert Into TblWSDtl(DocNo, EmpCode, CreateBy, CreateDt) " +
                    "Values(@DocNo, @EmpCode, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveWSSummaryHdr(string DocNo, ref WSSummaryHdr i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWSSummaryHdr(DocNo, EmpCode, PosCode, JoinDt, ResignDt, ");
            SQL.AppendLine("TotalWorkingDay, TotalWorkingHr, TotalHolidayDay, TotalUnknownDay, TotalAnnualLeaveDay, ");
            SQL.AppendLine("TotalPaidLeaveDay, TotalPaidLeaveHr, TotalUnpaidLeaveDay, TotalUnpaidLeaveHr, TotalOTHr,");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @EmpCode, @PosCode, @JoinDt, @ResignDt, ");
            SQL.AppendLine("@TotalWorkingDay, @TotalWorkingHr, @TotalHolidayDay, @TotalUnknownDay, @TotalAnnualLeaveDay, ");
            SQL.AppendLine("@TotalPaidLeaveDay, @TotalPaidLeaveHr, @TotalUnpaidLeaveDay, @TotalUnpaidLeaveHr, @TotalOTHr, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", i.EmpCode);
            Sm.CmParam<String>(ref cm, "@PosCode", i.PosCode);
            Sm.CmParam<String>(ref cm, "@JoinDt", i.JoinDt);
            Sm.CmParam<String>(ref cm, "@ResignDt", i.ResignDt);
            Sm.CmParam<Decimal>(ref cm, "@TotalWorkingDay", i.TotalWorkingDay);
            Sm.CmParam<Decimal>(ref cm, "@TotalWorkingHr", i.TotalWorkingHr);
            Sm.CmParam<Decimal>(ref cm, "@TotalHolidayDay", i.TotalHolidayDay);
            Sm.CmParam<Decimal>(ref cm, "@TotalUnknownDay", i.TotalUnknownDay);
            Sm.CmParam<Decimal>(ref cm, "@TotalAnnualLeaveDay", i.TotalAnnualLeaveDay);
            Sm.CmParam<Decimal>(ref cm, "@TotalPaidLeaveDay", i.TotalPaidLeaveDay);
            Sm.CmParam<Decimal>(ref cm, "@TotalPaidLeaveHr", i.TotalPaidLeaveHr);
            Sm.CmParam<Decimal>(ref cm, "@TotalUnpaidLeaveDay", i.TotalUnpaidLeaveDay);
            Sm.CmParam<Decimal>(ref cm, "@TotalUnpaidLeaveHr", i.TotalUnpaidLeaveHr);
            Sm.CmParam<Decimal>(ref cm, "@TotalOTHr", i.TotalOTHr);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveWSSummaryDtl(string DocNo, ref WSSummaryDtl i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWSSummaryDtl(DocNo, EmpCode, Dt, WTInd, TmIn, TmOut, ");
            SQL.AppendLine("FullDayWorkingHr, HalfDayWorkingHr, FullDayStartTm, FullDayEndTm, HalfDayStartTm, HalfDayEndTm, ");
            SQL.AppendLine("WSRType, LeaveCode, LGCode, PaidInd, DurationHour, OTStartTm, OTEndTm, HolidayInd, ");
            SQL.AppendLine("WorkingHr, PaidLeaveHr, UnPaidLeaveHr, OTHr, Symbol, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @EmpCode, @Dt, @WTInd, @TmIn, @TmOut, ");
            SQL.AppendLine("@FullDayWorkingHr, @HalfDayWorkingHr, @FullDayStartTm, @FullDayEndTm, @HalfDayStartTm, @HalfDayEndTm, ");
            SQL.AppendLine("@WSRType, @LeaveCode, @LGCode, @PaidInd, @DurationHour, @OTStartTm, @OTEndTm, @HolidayInd, ");
            SQL.AppendLine("@WorkingHr, @PaidLeaveHr, @UnPaidLeaveHr, @OTHr, @Symbol, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", i.EmpCode);
            Sm.CmParam<String>(ref cm, "@Dt", i.Dt);
            Sm.CmParam<String>(ref cm, "@WTInd", i.WTInd);
            Sm.CmParam<String>(ref cm, "@TmIn", i.TmIn);
            Sm.CmParam<String>(ref cm, "@TmOut", i.TmOut);
            Sm.CmParam<Decimal>(ref cm, "@FullDayWorkingHr", i.FullDayWorkingHr);
            Sm.CmParam<Decimal>(ref cm, "@HalfDayWorkingHr", i.HalfDayWorkingHr);
            Sm.CmParam<String>(ref cm, "@FullDayStartTm", i.FullDayStartTm);
            Sm.CmParam<String>(ref cm, "@FullDayEndTm", i.FullDayEndTm);
            Sm.CmParam<String>(ref cm, "@HalfDayStartTm", i.HalfDayStartTm);
            Sm.CmParam<String>(ref cm, "@HalfDayEndTm", i.HalfDayEndTm);
            Sm.CmParam<String>(ref cm, "@WSRType", i.WSRType);
            Sm.CmParam<String>(ref cm, "@LeaveCode", i.LeaveCode);
            Sm.CmParam<String>(ref cm, "@LGCode", i.LGCode);
            Sm.CmParam<String>(ref cm, "@PaidInd", i.PaidInd.Length==0?"N":i.PaidInd);
            Sm.CmParam<Decimal>(ref cm, "@DurationHour", i.DurationHour);
            Sm.CmParam<String>(ref cm, "@OTStartTm", i.OTStartTm);
            Sm.CmParam<String>(ref cm, "@OTEndTm", i.OTEndTm);
            Sm.CmParam<String>(ref cm, "@HolidayInd", i.HolidayInd.Length==0?"N":i.HolidayInd);
            Sm.CmParam<Decimal>(ref cm, "@WorkingHr", i.WorkingHr);
            Sm.CmParam<Decimal>(ref cm, "@PaidLeaveHr", i.PaidLeaveHr);
            Sm.CmParam<Decimal>(ref cm, "@UnPaidLeaveHr", i.UnPaidLeaveHr);
            Sm.CmParam<Decimal>(ref cm, "@OTHr", i.OTHr);
            Sm.CmParam<String>(ref cm, "@Symbol", i.Symbol);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var lhdr = new List<WSSummaryHdr>();
            var ldtl = new List<WSSummaryDtl>();

            if (!ChkCancelInd.Checked)
                GenerateData(TxtDocNo.Text, ref lhdr, ref ldtl);
           
            var cml = new List<MySqlCommand>();

            cml.Add(EditWSHdr());

            if (!ChkCancelInd.Checked)
            {
                lhdr.ForEach(i => { cml.Add(SaveWSSummaryHdr(TxtDocNo.Text, ref i)); });
                ldtl.ForEach(i => { cml.Add(SaveWSSummaryDtl(TxtDocNo.Text, ref i)); });
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return 
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocNoAlreadyCancelled();
        }

        private bool IsDocNoAlreadyCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblWSHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditWSHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblWSHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Delete From TblWSSummaryHdr Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblWSSummaryDtl Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowWSHdr(DocNo);
                ShowWSDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWSHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.ATDDocNo, A.Remark, ");
            SQL.AppendLine("B.Period, B.StartDt, B.EndDt, C.DeptName ");
            SQL.AppendLine("From TblWSHdr A ");
            SQL.AppendLine("Inner Join TblATDHdr B On A.ATDDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "ATDDocNo", "Period", "StartDt", 
                        
                        //6-8
                        "EndDt", "DeptName", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtATDDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtPeriodYr.EditValue = Sm.Left(dr.GetString(c[4]), 4);
                        TxtPeriodMth.EditValue = Sm.Right(dr.GetString(c[4]), 2);
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[5]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[6]));
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[7]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                    }, true
                );
        }

        private void ShowWSDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, B.JoinDt, B.ResignDt ");
            SQL.AppendLine("From TblWSDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.EmpName; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "JoinDt", "ResignDt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = true;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mLGCodeForAnnualLeave = Sm.GetParameter("LGCodeForAnnualLeave");
        }

        private string GetSelectedEmployee()
        {
            string EmpCode = string.Empty;
            for (int row = 0; row < Grd1.Rows.Count - 1; row++)
            {
                if (Sm.GetGrdBool(Grd1, row, 0) && Sm.GetGrdStr(Grd1, row, 1).Length > 0)
                    EmpCode += "##" + Sm.GetGrdStr(Grd1, row, 1) + "##";
            }
            return EmpCode;
        }

        internal void ShowEmployeeInfo(string ATDDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, B.JoinDt, B.ResignDt ");
            SQL.AppendLine("From TblATDDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Where A.DocNo=@ATDDocNo ");
            SQL.AppendLine("And A.EmpCode Not In (");
            SQL.AppendLine("    Select B.EmpCode ");
            SQL.AppendLine("    From TblWSHdr A ");
            SQL.AppendLine("    Inner Join TblWSDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.ATDDocNo=@ATDDocNo ");
            SQL.AppendLine(") Order By B.EmpName; ");
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ATDDocNo", ATDDocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                   { 
                       //0
                       "EmpCode",

                       //1-5
                       "EmpName", "EmpCodeOld", "PosName", "JoinDt", "ResignDt"
                   },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = true;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void GenerateData(string DocNo, ref List<WSSummaryHdr> lhdr, ref List<WSSummaryDtl> ldtl)
        {
            PrepareDataHdr(DocNo, ref lhdr);
            PrepareDataDtl(DocNo, ref ldtl);
            
            ldtl.ForEach(i => { ProcessDataDtl(ref i); });

            foreach (WSSummaryHdr i in lhdr) 
            {
                foreach (WSSummaryDtl d in ldtl)
                {
                    if (Sm.CompareStr(i.EmpCode, d.EmpCode))
                    {

                        i.TotalWorkingHr += d.WorkingHr;
                        if (d.HolidayInd == "Y") i.TotalHolidayDay += 1;
                        if (d.Symbol == "U") i.TotalUnknownDay += 1;
                        if (Sm.CompareStr(d.LGCode, mLGCodeForAnnualLeave)) i.TotalAnnualLeaveDay += 1;
                        if (d.PaidInd == "Y") i.TotalPaidLeaveHr += d.PaidLeaveHr;
                        if (d.PaidInd == "N") i.TotalUnpaidLeaveHr += d.UnPaidLeaveHr;
                        i.TotalOTHr += d.OTHr;
                        switch (d.WTInd)
                        {
                            case "F":
                                if (d.FullDayWorkingHr != 0)
                                {
                                    i.TotalWorkingDay += (d.WorkingHr / d.FullDayWorkingHr);
                                    if (d.PaidInd == "Y")
                                        i.TotalPaidLeaveDay = (d.PaidLeaveHr / d.FullDayWorkingHr);
                                    if (d.PaidInd == "N")
                                        i.TotalUnpaidLeaveDay = (d.UnPaidLeaveHr / d.FullDayWorkingHr);
                                }
                                break;
                            case "H":
                                if (d.HalfDayWorkingHr != 0)
                                {
                                    i.TotalWorkingDay += (d.WorkingHr / d.HalfDayWorkingHr);
                                    if (d.PaidInd == "Y")
                                        i.TotalPaidLeaveDay = (d.PaidLeaveHr / d.HalfDayWorkingHr);
                                    if (d.PaidInd == "N")
                                        i.TotalUnpaidLeaveDay = (d.UnPaidLeaveHr / d.HalfDayWorkingHr);
                                }
                                break;
                            case "O":
                                break;
                        }
                    }
                }
            }
        }

        private void PrepareDataDtl(string DocNo, ref List<WSSummaryDtl> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
                        
            SQL.AppendLine("Select A.StartDt, A.EndDt, ");
            SQL.AppendLine("B.EmpCode As EmpCode, I.JoinDt, I.ResignDt, ");   
            SQL.AppendLine("B.Dt As Dt, ");
            SQL.AppendLine("Case DAYOFWEEK(B.Dt2) ");
	        SQL.AppendLine("    When 1 Then E.Value7 ");
	        SQL.AppendLine("    When 2 Then E.Value1 ");
	        SQL.AppendLine("    When 3 Then E.Value2 ");
	        SQL.AppendLine("    When 4 Then E.Value3 ");
	        SQL.AppendLine("    When 5 Then E.Value4 ");
	        SQL.AppendLine("    When 6 Then E.Value5 ");
	        SQL.AppendLine("    When 7 Then E.Value6 ");
            SQL.AppendLine("End As WTInd, ");
            SQL.AppendLine("B.TmIn, B.TmOut, ");
            SQL.AppendLine("Case When B.TmIn<=B.TmOut Then '1' Else '2' End As OneDayInd, ");
            SQL.AppendLine("F.DailyWorkingHr As FullDayWorkingHr, ");
            SQL.AppendLine("Case When F.DailyWorkingHr>0 Then (F.WeeklyWorkingHr mod F.DailyWorkingHr) Else 0 End As HalfDayWorkingHr, ");
            SQL.AppendLine("E.StartTm As FullDayStartTm, ");
            SQL.AppendLine("E.EndTm As FullDayEndTm, ");
            SQL.AppendLine("E.StartTm2 As HalfDayStartTm, ");
            SQL.AppendLine("E.EndTm2 As HalfDayEndTm, ");
            SQL.AppendLine("E.WSRType, ");
            SQL.AppendLine("G.LeaveCode, G.LGCode, G.PaidInd, G.DurationHour, "); 
            SQL.AppendLine("H.OTStartTm, H.OTEndTm, ");
            SQL.AppendLine("If(IfNull(J.HolCode, '')='', 'N', 'Y') As HolidayInd ");
            SQL.AppendLine("From TblATDHdr A ");
            SQL.AppendLine("Inner Join ( ");
	        SQL.AppendLine("    Select DocNo, EmpCode, Dt, ");
	        SQL.AppendLine("    STR_TO_DATE(Concat(Right(Dt, 2), ',', Substring(Dt, 5, 2), ',', Left(Dt, 4)),'%d,%m,%Y') As Dt2, ");
	        SQL.AppendLine("    If(Length(IfNull(InOutA1, ''))<>8, Null, Left(InOutA1, 4)) As TmIn, ");
	        SQL.AppendLine("    If(Length(IfNull(InOutA1, ''))<>8, Null, Right(InOutA1, 4)) As TmOut ");
	        SQL.AppendLine("    From TblATDDtl  ");
            SQL.AppendLine("    Where DocNo=@ATDDocNo ");
            SQL.AppendLine("    And Position(Concat('##', EmpCode, '##') In @SelectedEmployee)>0 ");
            SQL.AppendLine("    And Dt Between @StartDt And @EndDt ");
	        SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWTGDtl C On B.EmpCode=C.EmpCode ");
            SQL.AppendLine("Inner Join TblWTGHdr D On C.WTGCode=D.WTGCode And D.ActInd='Y' ");
            SQL.AppendLine("Inner Join TblWSRHdr E On D.WSRCode=E.WSRCode And E.WSRType='1' ");
            SQL.AppendLine("Inner Join TblWT F On D.WTCode=F.WTCode "); 
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T1.EmpCode, T2.LeaveDt, T1.LeaveCode, T3.PaidInd, T3.LGCode, T1.DurationHour ");
	        SQL.AppendLine("    From TblEmpLeaveHdr T1 ");
	        SQL.AppendLine("    Inner Join TblEmpLeaveDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.LeaveDt Between @StartDt And @EndDt ");
	        SQL.AppendLine("    Inner Join TblLeave T3 On T1.LeaveCode=T3.LeaveCode ");
	        SQL.AppendLine("    Where T1.CancelInd='N' ");
	        SQL.AppendLine("    And T1.Status='A' ");
            SQL.AppendLine("    And Position(Concat('##', T1.EmpCode, '##') In @SelectedEmployee)>0 ");
            SQL.AppendLine(") G On B.EmpCode=G.EmpCode And B.Dt=G.LeaveDt ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.EmpCode, T1.OTDt, T2.OTStartTm, T2.OTEndTm ");
	        SQL.AppendLine("    From TblOTRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblOTRequestDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.Status='A' ");
            SQL.AppendLine("        And Position(Concat('##', T2.EmpCode, '##') In @SelectedEmployee)>0 ");
            SQL.AppendLine("    Where T1.OTDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    ) H On B.EmpCode=H.EmpCode And B.Dt=H.OTDt ");
            SQL.AppendLine("Inner Join TblEmployee I On B.EmpCode=I.EmpCode ");
            SQL.AppendLine("Left Join TblHoliday J ");
            SQL.AppendLine("    On B.Dt=J.HolDt ");
            SQL.AppendLine("    And J.HolDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Where A.DocNo=@ATDDocNo ");
            
            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.StartDt, A.EndDt, ");
            SQL.AppendLine("B.EmpCode, I.JoinDt, I.ResignDt, ");
            SQL.AppendLine("B.Dt, ");
            SQL.AppendLine("K.Value As WTInd, ");
            SQL.AppendLine("B.TmIn, B.TmOut, ");
            SQL.AppendLine("Case When B.TmIn<=B.TmOut Then '1' Else '2' End As OneDayInd, ");
            SQL.AppendLine("F.DailyWorkingHr As FullDayWorkingHr, ");
            SQL.AppendLine("Case When F.DailyWorkingHr>0 Then (F.WeeklyWorkingHr mod F.DailyWorkingHr) Else 0 End As HalfDayWorkingHr, ");
            SQL.AppendLine("E.StartTm As FullDayStartTm, ");
            SQL.AppendLine("E.EndTm As FullDayEndTm, ");
            SQL.AppendLine("E.StartTm2 As HalfDayStartTm, ");
            SQL.AppendLine("E.EndTm2 As HalfDayEndTm, ");
            SQL.AppendLine("E.WSRType, ");
            SQL.AppendLine("G.LeaveCode, G.LGCode, G.PaidInd, G.DurationHour, ");
            SQL.AppendLine("H.OTStartTm, H.OTEndTm, ");
            SQL.AppendLine("If(IfNull(J.HolCode, '')='', 'N', 'Y') As HolidayInd ");
            SQL.AppendLine("From TblATDHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select DocNo, EmpCode, Dt, ");
            SQL.AppendLine("    STR_TO_DATE(Concat(Right(Dt, 2), ',', Substring(Dt, 5, 2), ',', Left(Dt, 4)),'%d,%m,%Y') As Dt2, ");
            SQL.AppendLine("    If(Length(IfNull(InOutA1, ''))<>8, Null, Left(InOutA1, 4)) As TmIn, ");
            SQL.AppendLine("    If(Length(IfNull(InOutA1, ''))<>8, Null, Right(InOutA1, 4)) As TmOut ");
            SQL.AppendLine("    From TblATDDtl  ");
            SQL.AppendLine("    Where DocNo=@ATDDocNo ");
            SQL.AppendLine("    And Position(Concat('##', EmpCode, '##') In @SelectedEmployee)>0 ");
            SQL.AppendLine("    And Dt Between @StartDt And @EndDt ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWTGDtl C On B.EmpCode=C.EmpCode ");
            SQL.AppendLine("Inner Join TblWTGHdr D On C.WTGCode=D.WTGCode And D.ActInd='Y' ");
            SQL.AppendLine("Inner Join TblWSRHdr E On D.WSRCode=E.WSRCode And E.WSRType='2' ");
            SQL.AppendLine("Inner Join TblWT F On D.WTCode=F.WTCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.EmpCode, T2.LeaveDt, T1.LeaveCode, T3.PaidInd, T3.LGCode, T1.DurationHour ");
            SQL.AppendLine("    From TblEmpLeaveHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpLeaveDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.LeaveDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    Inner Join TblLeave T3 On T1.LeaveCode=T3.LeaveCode ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status='A' ");
            SQL.AppendLine("    And Position(Concat('##', T1.EmpCode, '##') In @SelectedEmployee)>0 ");
            SQL.AppendLine(") G On B.EmpCode=G.EmpCode And B.Dt=G.LeaveDt ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, T1.OTDt, T2.OTStartTm, T2.OTEndTm ");
            SQL.AppendLine("    From TblOTRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblOTRequestDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.Status='A' ");
            SQL.AppendLine("        And Position(Concat('##', T2.EmpCode, '##') In @SelectedEmployee)>0 ");
            SQL.AppendLine("    Where T1.OTDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    ) H On B.EmpCode=H.EmpCode And B.Dt=H.OTDt ");
            SQL.AppendLine("Inner Join TblEmployee I On B.EmpCode=I.EmpCode ");
            SQL.AppendLine("Left Join TblHoliday J ");
            SQL.AppendLine("    On B.Dt=J.HolDt ");
            SQL.AppendLine("    And J.HolDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Left Join TblWSRDtl K On E.WSRCode=K.WSRCode And B.Dt=K.WSRDt ");
            SQL.AppendLine("Where A.DocNo=@ATDDocNo ");
            SQL.AppendLine("Order By EmpCode, Dt; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                
                Sm.CmParam<String>(ref cm, "@ATDDocNo", TxtATDDocNo.Text);
                Sm.CmParam<String>(ref cm, "@SelectedEmployee", GetSelectedEmployee());
                Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
                Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "StartDt",

                    //1-5
                    "EndDt",
                    "EmpCode",
                    "JoinDt",
                    "ResignDt",
                    "Dt",
                    
                    //6-10
                    "WTInd",
                    "TmIn",
                    "TmOut",
                    "OneDayInd",
                    "FullDayWorkingHr",
                    
                    //11-15
                    "HalfDayWorkingHr",
                    "FullDayStartTm",
                    "FullDayEndTm",
                    "HalfDayStartTm",
                    "HalfDayEndTm",
                    
                    //16-20
                    "WSRType",
                    "LeaveCode",
                    "LGCode",
                    "PaidInd",
                    "DurationHour", 
                    
                    //21-23
                    "OTStartTm",
                    "OTEndTm",
                    "HolidayInd"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new WSSummaryDtl()
                        {
                            DocNo = DocNo,
                            StartDt = Sm.DrStr(dr, c[0]),
                            EndDt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            JoinDt = Sm.DrStr(dr, c[3]),
                            ResignDt = Sm.DrStr(dr, c[4]),   
                            Dt = Sm.DrStr(dr, c[5]),
                            WTInd = Sm.DrStr(dr, c[6]),
                            TmIn = Sm.DrStr(dr, c[7]),
                            TmOut = Sm.DrStr(dr, c[8]),
                            OneDayInd = Sm.DrStr(dr, c[9]),
                            FullDayWorkingHr = Sm.DrDec(dr, c[10]),
                            HalfDayWorkingHr = Sm.DrDec(dr, c[11]),
                            FullDayStartTm = Sm.DrStr(dr, c[12]),
                            FullDayEndTm = Sm.DrStr(dr, c[13]),
                            HalfDayStartTm = Sm.DrStr(dr, c[14]),
                            HalfDayEndTm = Sm.DrStr(dr, c[15]),
                            WSRType = Sm.DrStr(dr, c[16]),
                            LeaveCode = Sm.DrStr(dr, c[17]),
                            LGCode = Sm.DrStr(dr, c[18]),
                            PaidInd = Sm.DrStr(dr, c[19]),
                            DurationHour = Sm.DrDec(dr, c[20]), 
                            OTStartTm = Sm.DrStr(dr, c[21]),
                            OTEndTm = Sm.DrStr(dr, c[22]),
                            HolidayInd = Sm.DrStr(dr, c[23]),
                            WorkingHr = 0m,
                            PaidLeaveHr = 0m,
                            UnPaidLeaveHr = 0m,
                            OTHr = 0m,
                            Symbol = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessDataDtl(ref WSSummaryDtl i)
        {
            //Set Join Date and Resign Date
            var JoinDt = Sm.ConvertDateTime(i.JoinDt + "0000");
            var ResignDt = Sm.ConvertDateTime("900012312359");
            if (i.ResignDt.Length>0) ResignDt = Sm.ConvertDateTime(i.ResignDt + "2359");
            
            //Set Actual In And Actual Out
            var ActualIn = Sm.ConvertDateTime(i.Dt + i.TmIn);
            DateTime ActualOut = Sm.ConvertDateTime(i.Dt + i.TmOut);
            if (i.OneDayInd=="2") ActualOut = ActualOut.AddDays(1);
            
            //Set Full-Day default In And Out
            bool FullInd = false;
            var FullIn = ActualIn;
            var FullOut = ActualOut;
            if (i.FullDayStartTm.Length>0 && i.FullDayEndTm.Length>0)
            {
                FullIn = Sm.ConvertDateTime(i.Dt + i.FullDayStartTm );
                FullOut = Sm.ConvertDateTime(i.Dt + i.FullDayEndTm);
                if (decimal.Parse(i.FullDayStartTm)>decimal.Parse(i.FullDayEndTm)) 
                    FullOut = FullOut.AddDays(1);
                FullInd = true;
            }
             
            //Set Half-Day default In And Out
            bool HalfInd = false;
            var HalfIn = ActualIn;
            var HalfOut = ActualOut;
            if (i.HalfDayStartTm.Length>0 && i.HalfDayEndTm.Length>0)
            {
                HalfIn = Sm.ConvertDateTime(i.Dt + i.HalfDayStartTm);
                HalfOut = Sm.ConvertDateTime(i.Dt + i.HalfDayEndTm);
                if (decimal.Parse(i.HalfDayStartTm)>decimal.Parse(i.HalfDayEndTm)) 
                    HalfOut = HalfOut.AddDays(1);
                HalfInd = true;
            }

            //Set OT hours
            if (i.OTStartTm.Length > 0 && i.OTEndTm.Length > 0)
            {
                var OTIn = Sm.ConvertDateTime(i.Dt + i.OTStartTm);
                var OTOut = Sm.ConvertDateTime(i.Dt + i.OTEndTm);
                if (decimal.Parse(i.OTStartTm) > decimal.Parse(i.OTEndTm))
                    OTOut = OTOut.AddDays(1);
                i.OTHr = (decimal)((OTOut - OTIn).TotalHours);
            }

            var In = ActualIn;
            var Out = ActualOut;

            if (i.WTInd=="F" && FullInd)
            {   
                if (In<FullIn) In = FullIn;
                if (Out>FullOut) Out = FullOut;
            }
            
            if (i.WTInd=="H" && HalfInd)
            {
                if (In<HalfIn) In = HalfIn;
                if (Out>HalfOut) Out = HalfOut;
            }

            if (i.WTInd=="O") Out=In;
                         
            //Compute Working Hours, Leave and OT

            if (!(In < JoinDt || Out < JoinDt || In > ResignDt))
            {
                decimal ActualWorkingHr = (decimal)((Out - In).TotalHours);

                if (i.WSRType == "1" && i.HolidayInd == "Y")
                {
                    i.WorkingHr = 0m;
                    i.Symbol = "I";
                }
                else
                {
                    switch (i.WTInd)
                    {
                        case "F":
                            i.WorkingHr = i.FullDayWorkingHr;
                            i.Symbol = "F";
                            break;
                        case "H":
                            i.WorkingHr = i.HalfDayWorkingHr;
                            i.Symbol = "H";
                            break;
                        case "O":
                            i.WorkingHr = 0m;
                            i.Symbol = "O";
                            break;
                    }
                }

                //dikurangi leave
                if (i.LeaveCode.Length > 0 && i.DurationHour == 0)
                    i.DurationHour = i.WorkingHr;

                i.WorkingHr -= i.DurationHour;
                if (ActualWorkingHr < i.WorkingHr) i.WorkingHr = ActualWorkingHr;

                if (i.PaidInd == "Y")
                    i.PaidLeaveHr = i.DurationHour;
                else
                    i.UnPaidLeaveHr = i.DurationHour;

                //ditambah OT
                if (ActualWorkingHr <= i.WorkingHr)
                {
                    i.WorkingHr = ActualWorkingHr;
                    i.OTHr = 0m;
                }
                else
                {
                    if (ActualWorkingHr < i.WorkingHr + i.OTHr)
                    {
                        if (ActualWorkingHr > i.WorkingHr) i.OTHr = ActualWorkingHr - i.WorkingHr;
                    }
                }

                if (i.WorkingHr < 0) i.WorkingHr = 0m;
                if (i.PaidLeaveHr < 0) i.PaidLeaveHr = 0m;
                if (i.UnPaidLeaveHr < 0) i.UnPaidLeaveHr = 0m;
                if (i.OTHr < 0) i.OTHr = 0;

                if (i.Symbol != "O" && i.Symbol != "I" &&
                    i.WorkingHr == 0m && i.PaidLeaveHr == 0m && i.UnPaidLeaveHr == 0m)
                {
                    i.Symbol = "U";
                }
            }
            else
            {
                i.Symbol = "N"; 
            }
        }

        private void PrepareDataHdr(string DocNo, ref List<WSSummaryHdr> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select EmpCode, PosCode, DeptCode, JoinDt, ResignDt ");
            SQL.AppendLine("From TblEmployee ");
            SQL.AppendLine("Where Position(Concat('##', EmpCode, '##') In @SelectedEmployee)>0 ");
            SQL.AppendLine("Order By EmpCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedEmployee", GetSelectedEmployee());
                
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "EmpCode",

                    //1-4
                    "PosCode",
                    "DeptCode",
                    "JoinDt",
                    "ResignDt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new WSSummaryHdr()
                        {
                            DocNo= DocNo,
                            EmpCode = Sm.DrStr(dr, c[0]),
                            PosCode = Sm.DrStr(dr, c[1]),
                            DeptCode = Sm.DrStr(dr, c[2]),
                            JoinDt = Sm.DrStr(dr, c[3]),
                            ResignDt = Sm.DrStr(dr, c[4]),
                            TotalWorkingDay = 0m,
                            TotalWorkingHr = 0m,
                            TotalHolidayDay = 0m,
                            TotalUnknownDay = 0m,
                            TotalAnnualLeaveDay = 0m,
                            TotalPaidLeaveDay = 0m,
                            TotalPaidLeaveHr = 0m,
                            TotalUnpaidLeaveDay = 0m,
                            TotalUnpaidLeaveHr = 0m,
                            TotalOTHr = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnATDDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmWSDlg(this));
        }

        #endregion

        #endregion

        #region Class

        private class WSSummaryHdr
        {
            public string DocNo { get; set; }
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string PosCode { get; set; }
            public string DeptCode { get; set; }
            public decimal TotalWorkingDay { get; set; }
            public decimal TotalWorkingHr { get; set; }
            public decimal TotalHolidayDay { get; set; }
            public decimal TotalUnknownDay { get; set; }
            public decimal TotalAnnualLeaveDay { get; set; }
            public decimal TotalPaidLeaveDay { get; set; }
            public decimal TotalPaidLeaveHr { get; set; }
            public decimal TotalUnpaidLeaveDay { get; set; }
            public decimal TotalUnpaidLeaveHr { get; set; }
            public decimal TotalOTHr { get; set; }
        }

        private class WSSummaryDtl
        {
            public string DocNo { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string Dt { get; set; }
            public string WTInd { get; set; }
            public string TmIn { get; set; }
            public string TmOut { get; set; }
            public string OneDayInd { get; set; }
            public decimal FullDayWorkingHr { get; set; }
            public decimal HalfDayWorkingHr { get; set; }
            public string FullDayStartTm { get; set; }
            public string FullDayEndTm { get; set; }
            public string HalfDayStartTm { get; set; }
            public string HalfDayEndTm { get; set; }
            public string WSRType { get; set; }
            public string LeaveCode { get; set; }
            public string LGCode { get; set; }
            public string PaidInd { get; set; }
            public decimal DurationHour { get; set; }
            public string OTStartTm { get; set; }
            public string OTEndTm { get; set; }
            public string HolidayInd { get; set; }
            public decimal WorkingHr { get; set; }
            public decimal PaidLeaveHr { get; set; }
            public decimal UnPaidLeaveHr { get; set; }
            public decimal OTHr { get; set; }
            public string Symbol { get; set; }
        }

        #endregion
    }
}
