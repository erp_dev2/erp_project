﻿#region Update
/*
   02/12/2020 [DITA/IMS] New Apps
 * */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;


#endregion

namespace RunSystem
{
    public partial class FrmProjectBudgetResourceFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmProjectBudgetResource mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmProjectBudgetResourceFind(FrmProjectBudgetResource FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            SetLueCtCode(ref LueCtCode);
            SetGrd();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Status",
                        "Process",
                        "SO Contract#",

                        //6-10
                        "Customer",
                        "Customer's PO",
                        "Account#",
                        "Account Name",
                        "Bid & Pricing"+Environment.NewLine+"Quotation", 

                        //11-15
                        "Financial"+Environment.NewLine+"Budget",
                        "Project Code",
                        "Project Name",
                        "Created"+Environment.NewLine+"By", 
                        "Created"+Environment.NewLine+"Date", 

                        //16-19
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time", 
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 100, 100, 180, 
                        
                        //6-10
                        200, 150, 150, 200, 100,
                        
                        //11-15
                        100, 150, 150, 100, 100,

                        //16-19
                        100, 100, 100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 15, 18 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11 }, 0);
            Sm.GrdFormatTime(Grd1, new int[] { 16, 19});
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19}, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "C.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtSOContractDocNo.Text, "A.SOContractDocNo", false);

                SQL.AppendLine("Select A.DocNo, A.DocDt,  ");
                SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, G.OptDesc Process,  ");
                SQL.AppendLine("A.SOContractDocNo, D.CtName, B.PONo, H.AcNo, H.AcDesc, A1.BidPricingQtAmt, A1.FinancialBudgetAmt,  ");
                SQL.AppendLine("F.ProjectCode,  F.ProjectName, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblProjectBudgetResourceHdr A ");
                SQL.AppendLine("Inner Join TblProjectBudgetResourceDtl A1 On A.DocNo = A1.DocNo ");
                SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOContractDocNo = B.DocNo ");
                SQL.AppendLine("Inner Join TblBOQHdr C On B.BOQDocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblCustomer D On C.CtCode = D.CtCode ");
                SQL.AppendLine("LEFT JOIN tblnoticetoproceed C2 ON C2.LOPDocNo = C.LOPDocNo");
                SQL.AppendLine("Inner Join TblLopHdr E On E.DocNo = C.LOPDocNo ");
                SQL.AppendLine("LEFT JOIN tblprojectgroup F ON F.PGCode=E.PGCode");
                SQL.AppendLine("Left Join TblOption G On A.ProcessInd = G.OptCode And G.OptCat = 'ProjectBudgetResourceProcessInd' ");
                SQL.AppendLine("Inner Join TblCOA H On H.AcNo = A1.AcNo ");
                SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString() + Filter + " Order By A.DocDt Desc, A.DocNo; ",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "StatusDesc", "Process", "SOContractDocNo", "CtName", 

                            //6-10
                            "PONo", "AcNo", "AcDesc", "BidPricingQtAmt", "FinancialBudgetAmt", 

                            //11-15
                            "ProjectCode", "ProjectName", "CreateBy", "CreateDt", "LastUpBy",

                            //16
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 16);
                        }, true, false, false, false
                    );

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ForeColor = Color.Black;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11});
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void SetLueCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CtCode As Col1, B.CtName As Col2 ");
            SQL.AppendLine("From TblBOQHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL.AppendLine("Order By B.CtName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtSOContractDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSOContractDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Contract#");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        #endregion

        #endregion
    }
}
