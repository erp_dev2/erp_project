﻿#region Update
/*
    16/12/2020 [ICA/IMS] New App
    22/02/2021 [TKG/IMS] tambah filter cost center
    03/06/2021 [VIN/PHT] bug saat chose data
    25/06/2021 [MYA/IMS] budget category, di otomatiskan untuk terisi cost center nya sesuai cost category yg dipilih
    13/09/2022 [MYA/PRODUCT] Otomatisasi coa ketika di budget category dengan menggunakan IsBudgetCategoryUseCOA
    26/09/2022 [VIN/SIER] BUG Otomatisasi IsBudgetCategoryUseCOA : belum copy acdesc
 * 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetCategoryDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmBudgetCategory mFrmParent;
        private bool 
            mIsCostCategoryUseJoinCost = false,
            mIsCostCategoryFilterByEntity = false;

        #endregion

        #region Constructor

        public FrmBudgetCategoryDlg3(FrmBudgetCategory FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsCostCategoryUseJoinCost = Sm.GetParameterBoo("IsCostCategoryUseJoinCost");
            mIsCostCategoryFilterByEntity = Sm.GetParameterBoo("IsCostCategoryFilterByEntity");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Join Cost",
                        "Cost Center",
                        "COA Account#",

                        //6-10
                        "COA Description",
                        "Group",
                        "Entity",
                        "Cost of Group",
                        "Product",

                        //11
                        "CCCode"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 250, 80, 200, 150,  
                        
                        //6-10
                        250,150, 180, 200, 200,

                        //11
                        100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 11 }, false);
            Sm.SetGrdProperty(Grd1, false);
            if (mIsCostCategoryUseJoinCost) Grd1.Cols[3].Visible = false;
        }

        override protected void HideInfoInGrd()
        {
            
        }

        override protected void ShowData()
        {
            Cursor.Current = Cursors.WaitCursor;

            Sm.ClearGrd(Grd1, false);

            try
            {
                var cm = new MySqlCommand();
                var Filter = string.Empty;
                var SQL = new StringBuilder();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtCCtCode.Text, "A.CCtName", false);
                Sm.FilterStr(ref Filter, ref cm, TxtCCCode.Text, "C.CCName", false);

                SQL.AppendLine("Select A.CCtCode, A.CCtName, C.CCCode, C.CCName, A.AcNo, A.JoinCostInd, ");
                if (mFrmParent.mIsCOAUseAlias)
                    SQL.AppendLine("Concat(B.AcDesc, Case When B.Alias Is Null Then '' Else Concat(' [', B.Alias, ']') End) As AcDesc, ");
                else
                    SQL.AppendLine("B.AcDesc, ");
                SQL.AppendLine("D.OptDesc, E.EntName, F.OptDesc As CostOfGroupDesc, G.OptDesc As CCtProductDesc, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblCostCategory A  ");
                SQL.AppendLine("Left Join TblCOA B on A.AcNo=B.AcNo ");
                if (mIsCostCategoryFilterByEntity)
                {
                    SQL.AppendLine("And B.EntCode is Not Null ");
                    SQL.AppendLine("And B.EntCode In ( ");
                    SQL.AppendLine("    Select T1.EntCode From TblProfitCenter T1, TblSite T2 ");
                    SQL.AppendLine("    Where T1.ProfitCenterCode=T2.ProfitCenterCode ");
                    SQL.AppendLine("    And T1.EntCode Is Not Null ");
                    SQL.AppendLine("    And T2.ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("    And T2.SiteCode In (");
                    SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join TblCostCenter C On A.CCCode = C.CCCode ");
                SQL.AppendLine("Left Join TblOption D On A.CCGrpCode = D.OptCode And D.OptCat='CostCenterGroup' ");
                SQL.AppendLine("Left Join TblEntity E On B.EntCode=E.EntCode ");
                SQL.AppendLine("Left Join TblOption F On A.CostOfGroupCode=F.OptCode And F.OptCat='CostofGroup' ");
                SQL.AppendLine("Left Join TblOption G On A.CCtProductCode=G.OptCode And G.OptCat='CCtProduct' ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(" Order By C.CCName, A.CCtName;");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "CCtCode",

                            //1-5
                            "CCtName",
                            "JoinCostInd",
                            "CCName",
                            "AcNo",
                            "AcDesc",
                            
                            //6-10
                            "OptDesc",
                            "EntName",
                            "CostOfGroupDesc",
                            "CCtProductDesc",
                            "CCCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.mCCtCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtCCtName.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.TxtCostCategoryCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                if (mFrmParent.mIsCostCenterBasedOnCostCategory)
                {
                    mFrmParent.mCCCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 11);
                    mFrmParent.TxtCCCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                    mFrmParent.TxtCostCenterCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 11);
                    if (mFrmParent.mIsBudgetCategoryUseCOA)
                    {
                        mFrmParent.TxtAcNo.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                        mFrmParent.TxtAcDesc.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                    }
                }
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtCCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cost center");
        }

        private void ChkCCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cost category");
        }

        private void TxtCCtCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void Grd1_DoubleClick(object sender, EventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion
    }
}
