﻿#region Update
/* 
    11/02/2019 [TKG] tambah document approval group
    16/03/2019 [TKG] ubah SetLueSiteCode menjadi SetLueSiteCode2
    11/04/2019 [WED] SetLueDeptCode2
    17/09/2020 [WED/SRN] tambah inputan kode Leave, berdasarkan parameter IsDocApprovalSettingUseLeaveCode dan DocApprovalSettingTypeAllowedToInputLeaveCode
    10/02/2021 [VIN/PHT] tambah karakter di designer txtleavecode
    11/02/2021 [TKG/KIM] tambah end amount
    07/07/2021 [IBL/PHT] tambah field employee level code berdasarkan IsDocApprovalSettingUseEmpLevelCode
    07/07/2021 [TKG/PHT] ganti level code menggunakan lookupedit
    16/07/2021 [TKG/PHT] Berdasarkan parameter IsDocApprovalSettingAutoGenerateLevel, approval level digenerate otomatis.
    03/08/2021 [TKG/PHT] mengganti level dengan textbox
    13/10/2021 [AJH/PHT] Menambahkan kolom employee's level dan leave code berdasarkan parameter
    25/11/2021 [NJP/ALL] Approval setting tidak bisa di edit ketika masih ada dokumen yang outstanding terhadap type trx tersebut
    30/01/2022 [TKG/ALL] ubah validasi saat menambah data baru dan menghapus data lama.
    11/02/2022 [VIN/ALL] Bug validasi saat menambah data baru dan menghapus data lama.-> hapus limit di group_concat
    17/02/2022 [TKG/PHT] DocType menjadi 80 karakter
    20/02/2023 [IBL/HEX] tambah field item category berdasarkan param IsDocApprovalSettingUseItemCt
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDocApprovalSetting : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmDocApprovalSettingFind FrmFind;
        internal bool
            mIsDocApprovalSettingUseEmpLevelCode = false,
            mIsDocApprovalSettingUseLeaveCode = false,
            mIsDocApprovalSettingUseItemCt = false;
        private bool
            mIsDocApprovalSettingAutoGenerateLevel = false,
            mIsInsert = false;
        private string mDocApprovalSettingTypeAllowedToInputLeaveCode = string.Empty;

        #endregion

        #region Constructor

        public FrmDocApprovalSetting(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (!mIsDocApprovalSettingUseLeaveCode) LblLeaveCode.Visible = TxtLeaveCode.Visible = false;
                if (!mIsDocApprovalSettingUseEmpLevelCode) LblLevelCode.Visible = TxtLevelCode.Visible = false;
                if (!mIsDocApprovalSettingUseItemCt) LblItCtCode.Visible = TxtItCtCode.Visible = TxtItCtName.Visible = BtnItCtCode.Visible = false;
                SetFormControl(mState.View);
                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocType, TxtDNo, TxtUserCode, TxtLevel, LueDeptCode, 
                        LueSiteCode, LueWhsCode, TxtStartAmt, TxtEndAmt, LuePaymentType, 
                        LueDAGCode, TxtLeaveCode, TxtLevelCode, TxtItCtCode, TxtItCtName
                    }, true);
                    BtnItCtCode.Enabled = false;
                    TxtDocType.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocType, TxtDNo, TxtUserCode, LueDeptCode, LueSiteCode, 
                        LueWhsCode, TxtStartAmt, TxtEndAmt, LuePaymentType, LueDAGCode,
                        TxtLeaveCode, TxtLevelCode
                    }, false);
                    if (!mIsDocApprovalSettingAutoGenerateLevel)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtLevel }, false);
                    BtnItCtCode.Enabled = true;
                    TxtDocType.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtUserCode, LueDeptCode, LueSiteCode, LueWhsCode, TxtStartAmt, 
                         TxtEndAmt, LuePaymentType, LueDAGCode, TxtLeaveCode, TxtLevelCode
                    }, false);
                    if (!mIsDocApprovalSettingAutoGenerateLevel)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLevel }, false);
                    BtnItCtCode.Enabled = true;
                    TxtUserCode.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocType, TxtDNo, TxtUserCode, TxtLevel, LueDeptCode, 
                LueSiteCode, LueWhsCode, LuePaymentType, LueDAGCode, TxtLeaveCode, 
                TxtLevelCode, TxtItCtCode, TxtItCtName
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtLevel, TxtStartAmt, TxtEndAmt }, 2);
        }

        #endregion
        
        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDocApprovalSettingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                mIsInsert = true;
                ClearData();
                SetFormControl(mState.Insert);
                Sl.SetLueDAGCode(ref LueDAGCode, string.Empty);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                Sl.SetLueDeptCode2(ref LueDeptCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            mIsInsert = false;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Delete", "") == DialogResult.No || 
            Sm.IsTxtEmpty(TxtDocType, "", false) ||
            IsOutstandingDataForDeleteExist()) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblDocApprovalSetting Where DocType=@DocType And DNo=@DNo;" };
                Sm.CmParam<String>(ref cm, "@DocType", TxtDocType.Text);
                Sm.CmParam<String>(ref cm, "@DNo", TxtDNo.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                if (mIsInsert)
                {
                    if (mIsDocApprovalSettingAutoGenerateLevel)
                        SQL.AppendLine("Set @Level:=IfNull((Select Max(Level) From TblDocApprovalSetting Where DocType=@DocType), 0.00)+1.00;");
                    SQL.AppendLine("Insert Into TblDocApprovalSetting(DocType, DNo, UserCode, Level, DeptCode, SiteCode, WhsCode, StartAmt, EndAmt, PaymentType, DAGCode, LevelCode, LeaveCode, ItCtCode, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@DocType, @DNo, @UserCode, @Level, @DeptCode, @SiteCode, @WhsCode, @StartAmt, @EndAmt, @PaymentType, @DAGCode, @LevelCode, @LeaveCode, @ItCtCode, @UserCode2, CurrentDateTime());");
                }
                else
                {

                    SQL.AppendLine("Update TblDocApprovalSetting Set ");
                    SQL.AppendLine("    UserCode=@UserCode, Level=@Level, DeptCode=@DeptCode, SiteCode=@SiteCode, WhsCode=@WhsCode, StartAmt=@StartAmt, EndAmt=@EndAmt, PaymentType=@PaymentType, DAGCode=@DAGCode, LeaveCode=@LeaveCode, LevelCode=@LevelCode, ItCtCode = @ItCtCode, LastUpBy=@UserCode2, LastUpDt=CurrentDateTime() ");
                    SQL.AppendLine("Where DocType=@DocType And DNo=@DNo; ");
                }
                
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocType", TxtDocType.Text);
                Sm.CmParam<String>(ref cm, "@DNo", TxtDNo.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text);
                if (!(mIsInsert && mIsDocApprovalSettingAutoGenerateLevel))
                    Sm.CmParam<Decimal>(ref cm, "@Level", decimal.Parse(TxtLevel.Text));
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<Decimal>(ref cm, "@StartAmt", decimal.Parse(TxtStartAmt.Text));
                Sm.CmParam<Decimal>(ref cm, "@EndAmt", decimal.Parse(TxtEndAmt.Text));
                Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
                Sm.CmParam<String>(ref cm, "@DAGCode", Sm.GetLue(LueDAGCode));
                Sm.CmParam<String>(ref cm, "@LevelCode", TxtLevelCode.Text);
                Sm.CmParam<String>(ref cm, "@LeaveCode", TxtLeaveCode.Text);
                Sm.CmParam<String>(ref cm, "@ItCtCode", TxtItCtCode.Text);
                Sm.CmParam<String>(ref cm, "@UserCode2", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtDocType.Text, TxtDNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string DocType, string DNo )
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select A.*, B.ItCtName From TblDocApprovalSetting A ");
                SQL.AppendLine("Left Join TblItemCategory B On A.ItCtCode = B.ItCtCode ");
                SQL.AppendLine("Where DocType=@DocType And DNo=@DNo; ");

                Sm.CmParam<String>(ref cm, "@DocType", DocType);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);
                
                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "DocType", 

                        //1-5
                        "DNo", "UserCode", "Level", "DeptCode", "SiteCode", 
                        
                        //6-10
                        "WhsCode", "StartAmt", "EndAmt", "PaymentType", "DAGCode", 
                        
                        //11-13
                        "LeaveCode", "LevelCode", "ItCtCode", "ItCtName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocType.EditValue = Sm.DrStr(dr, c[0]);
                        TxtDNo.EditValue = Sm.DrStr(dr, c[1]);
                        TxtUserCode.EditValue = Sm.DrStr(dr, c[2]);
                        TxtLevel.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 2);
                        Sl.SetLueDeptCode2(ref LueDeptCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[4]));
                        Sl.SetLueSiteCode2(ref LueSiteCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[6]));
                        TxtStartAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        TxtEndAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[9]));
                        Sl.SetLueDAGCode(ref LueDAGCode, Sm.DrStr(dr, c[10]));
                        TxtLeaveCode.EditValue = Sm.DrStr(dr, c[11]);
                        TxtLevelCode.EditValue = Sm.DrStr(dr, c[12]);
                        TxtItCtCode.EditValue = Sm.DrStr(dr, c[13]);
                        TxtItCtName.EditValue = Sm.DrStr(dr, c[14]);
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            //mDocApprovalSettingTypeAllowedToInputLeaveCode = Sm.GetParameter("DocApprovalSettingTypeAllowedToInputLeaveCode");
            //mIsDocApprovalSettingUseLeaveCode = Sm.GetParameterBoo("IsDocApprovalSettingUseLeaveCode");
            //mIsDocApprovalSettingUseEmpLevelCode = Sm.GetParameterBoo("IsDocApprovalSettingUseEmpLevelCode");
            //mIsDocApprovalSettingAutoGenerateLevel = Sm.GetParameterBoo("IsDocApprovalSettingAutoGenerateLevel");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'DocApprovalSettingTypeAllowedToInputLeaveCode', 'IsDocApprovalSettingUseLeaveCode', 'IsDocApprovalSettingUseEmpLevelCode', 'IsDocApprovalSettingAutoGenerateLevel', 'IsDocApprovalSettingUseItemCt' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsDocApprovalSettingUseLeaveCode": mIsDocApprovalSettingUseLeaveCode = ParValue == "Y"; break;
                            case "IsDocApprovalSettingUseEmpLevelCode": mIsDocApprovalSettingUseEmpLevelCode = ParValue == "Y"; break;
                            case "IsDocApprovalSettingAutoGenerateLevel": mIsDocApprovalSettingAutoGenerateLevel = ParValue == "Y"; break;
                            case "IsDocApprovalSettingUseItemCt": mIsDocApprovalSettingUseItemCt = ParValue == "Y"; break;

                            //string
                            case "DocApprovalSettingTypeAllowedToInputLeaveCode": mDocApprovalSettingTypeAllowedToInputLeaveCode = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocType, "Document's type", false) ||
                Sm.IsTxtEmpty(TxtDNo, "Number", false) ||
                Sm.IsTxtEmpty(TxtUserCode, "User", false) ||
                (!mIsDocApprovalSettingAutoGenerateLevel && Sm.IsTxtEmpty(TxtLevel, "Approval's level", true)) ||
                IsDocApprovalSettingExisted() ||
                IsLeaveCodeInvalid() ||
                IsDocTypeForLeaveCodeInvalid() ||
                IsOutstandingDataForInsertExist();
        }

        private bool IsDocTypeForLeaveCodeInvalid()
        {
            if (!mIsDocApprovalSettingUseLeaveCode) return false;

            if (TxtLeaveCode.Text.Trim().Length > 0)
            {
                if (mDocApprovalSettingTypeAllowedToInputLeaveCode.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Leave code could not be added in this type.");
                    TxtLeaveCode.Focus();
                    return true;
                }

                string[] split = mDocApprovalSettingTypeAllowedToInputLeaveCode.Split(',');
                bool isNotValid = true;

                foreach (var x in split)
                {
                    if (x == TxtDocType.Text)
                    {
                        isNotValid = false;
                        break;
                    }
                }

                if (isNotValid)
                {
                    Sm.StdMsg(mMsgType.Warning, "Leave code could not be added in this type.");
                    TxtDocType.Focus();
                    return true;
                }
            }

            return false;
        }

        private bool IsLeaveCodeInvalid()
        {
            if (!mIsDocApprovalSettingUseLeaveCode) return false;

            if (TxtLeaveCode.Text.Trim().Length == 0) return false;

            if (TxtLeaveCode.Text.Trim().Contains(", "))
            {
                Sm.StdMsg(mMsgType.Warning, "Make sure your leave code could not contain a space after comma.");
                TxtLeaveCode.Focus();
                return true;
            }

            return false;
        }

        private bool IsDocApprovalSettingExisted()
        {
            if (!TxtDocType.Properties.ReadOnly && Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType ='" + TxtDocType.Text + "' And DNo='" + TxtDNo.Text + "' Limit 1; "))
            {
                Sm.StdMsg(mMsgType.Warning, "This setting already existed.");
                return true;
            }
            return false;
        }

        private bool IsOutstandingDataForDeleteExist()
        {
            if (TxtDocType.Properties.ReadOnly)
            {
                var SQL = new StringBuilder();
                SQL.AppendLine("SET SESSION group_concat_max_len = 10000; ");
                SQL.AppendLine("Select Group_Concat(Distinct DocNo SEPARATOR ',') From TblDocApproval T ");
                SQL.AppendLine("Where T.DocType=@Param1 ");
                SQL.AppendLine("And T.ApprovalDNo=@Param2 ");
                SQL.AppendLine("And T.Status Is Null ");
                SQL.AppendLine("And T.UserCode Is Null ");
                SQL.AppendLine("And Not Exists( ");
                SQL.AppendLine("    Select 1 From TblDocApproval ");
                SQL.AppendLine("    Where DocType=T.DocType ");
                SQL.AppendLine("    And DocNo=T.DocNo ");
                SQL.AppendLine("    And DNo=T.DNo ");
                SQL.AppendLine("    And Status Is Not Null ");
                SQL.AppendLine("    And Status='C' ");
                SQL.AppendLine("    ); ");
                var DocNo = Sm.GetValue(SQL.ToString(), TxtDocType.Text, TxtDNo.Text, string.Empty);
                if (DocNo.Length>0)
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        "You need to process outstanding document using document approval application first before you delete this data." + Environment.NewLine +
                        "Example : " + Environment.NewLine + 
                        DocNo.Replace(",", Environment.NewLine)
                        );
                    return true;
                }
            }
            return false;
        }

        private bool IsOutstandingDataForInsertExist()
        {
            if (!TxtDocType.Properties.ReadOnly)
            {
                bool
                    IsDeptCodeEnabled = Sm.GetLue(LueDeptCode).Length > 0,
                    IsSiteCodeEnabled = Sm.GetLue(LueSiteCode).Length > 0,
                    IsWhsCodeEnabled = Sm.GetLue(LueWhsCode).Length > 0,
                    IsDAGCodeEnabled = Sm.GetLue(LueDAGCode).Length > 0,
                    IsPaymentTypeEnabled = Sm.GetLue(LuePaymentType).Length > 0;
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                SQL.AppendLine("SET SESSION group_concat_max_len = 10000; ");
                SQL.AppendLine("Select Group_Concat(Distinct DocNo SEPARATOR ',') ");
                SQL.AppendLine("From TblDocApproval T ");
                SQL.AppendLine("Where T.DocType=@DocType ");
                SQL.AppendLine("And T.Status Is Null ");
                SQL.AppendLine("And T.UserCode Is Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblDocApprovalSetting ");
                SQL.AppendLine("    Where DocType=T.DocType ");
                SQL.AppendLine("    And DNo=T.ApprovalDNo ");
                if (IsDeptCodeEnabled)
                    SQL.AppendLine("And DeptCode Is Not Null And DeptCode=@DeptCode ");
                if (IsSiteCodeEnabled)
                    SQL.AppendLine("And SiteCode Is Not Null And SiteCode=@SiteCode ");
                if (IsWhsCodeEnabled)
                    SQL.AppendLine("And WhsCode Is Not Null And WhsCode=@WhsCode ");
                if (IsDAGCodeEnabled)
                    SQL.AppendLine("And DAGCode Is Not Null And DAGCode=@DAGCode ");
                if (IsPaymentTypeEnabled)
                    SQL.AppendLine("And PaymentType Is Not Null And PaymentType=@PaymentType ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("And Not Exists( ");
                SQL.AppendLine("    Select 1 From TblDocApproval ");
                SQL.AppendLine("    Where DocType=T.DocType ");
                SQL.AppendLine("    And DocNo=T.DocNo ");
                SQL.AppendLine("    And DNo=T.DNo ");
                SQL.AppendLine("    And Status Is Not Null ");
                SQL.AppendLine("    And Status='C' ");
                SQL.AppendLine("    ); ");
                
                cm.CommandText = SQL.ToString();

                Sm.CmParam<String>(ref cm, "@DocType", TxtDocType.Text);
                if (IsDeptCodeEnabled) Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                if (IsSiteCodeEnabled) Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                if (IsWhsCodeEnabled) Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (IsDAGCodeEnabled) Sm.CmParam<String>(ref cm, "@DAGCode", Sm.GetLue(LueDAGCode));
                if (IsPaymentTypeEnabled) Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));

                var DocNo = Sm.GetValue(cm);
                if (DocNo.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "You need to process outstanding document using document approval application first before you insert this data." + Environment.NewLine +
                        "Example : " + Environment.NewLine +
                        DocNo.Replace(",", Environment.NewLine)
                        );
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocType_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocType);
        }

        private void TxtDNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDNo);
        }

        private void TxtUserCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtUserCode);
        }

        private void TxtLevel_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLevel, 2);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(Sl.SetLueDeptCode2), string.Empty);
        }

        private void TxtStartAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtStartAmt, 0);
        }

        private void TxtEndAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtEndAmt, 0);
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue2(Sl.SetLueOption), "VoucherPaymentType");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        private void LueDAGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDAGCode, new Sm.RefreshLue2(Sl.SetLueDAGCode), string.Empty);
        }

        private void TxtLevelCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLevelCode);
        }

        private void BtnItCtCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDocApprovalSettingDlg(this));
        }

        #endregion

        #endregion
    }
}
