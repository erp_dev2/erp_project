﻿namespace RunSystem
{
    partial class FrmRptPurchaserPerformance2Dlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel4 = new System.Windows.Forms.Panel();
            this.BtnExcel = new DevExpress.XtraEditors.SimpleButton();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtUPriceBeforeTax = new DevExpress.XtraEditors.TextEdit();
            this.TxtUPriceAfterTax = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceBeforeTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceAfterTax.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 94);
            this.panel3.Size = new System.Drawing.Size(842, 379);
            // 
            // Grd1
            // 
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(842, 379);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtUPriceAfterTax);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtUPriceBeforeTax);
            this.panel2.Controls.Add(this.TxtItName);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Size = new System.Drawing.Size(842, 94);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.SteelBlue;
            this.panel4.Controls.Add(this.BtnExcel);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(758, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(84, 94);
            this.panel4.TabIndex = 26;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnExcel.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnExcel.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnExcel.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnExcel.Location = new System.Drawing.Point(0, 0);
            this.BtnExcel.Name = "BtnExcel";
            this.BtnExcel.Size = new System.Drawing.Size(84, 31);
            this.BtnExcel.TabIndex = 4;
            this.BtnExcel.Text = "  &Excel";
            this.BtnExcel.ToolTip = "Export To Excel (Alt-E)";
            this.BtnExcel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnExcel.ToolTipTitle = "Run System";
            this.BtnExcel.Click += new System.EventHandler(this.BtnExcel_Click);
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(107, 5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(253, 20);
            this.TxtDocNo.TabIndex = 28;
            this.TxtDocNo.Validated += new System.EventHandler(this.TxtDocNo_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(70, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 14);
            this.label2.TabIndex = 27;
            this.label2.Text = "PO#";
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(107, 26);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.ReadOnly = true;
            this.TxtItCode.Size = new System.Drawing.Size(100, 20);
            this.TxtItCode.TabIndex = 30;
            this.TxtItCode.Validated += new System.EventHandler(this.TxtItCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(69, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 14);
            this.label1.TabIndex = 29;
            this.label1.Text = "Item";
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(212, 26);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.ReadOnly = true;
            this.TxtItName.Size = new System.Drawing.Size(253, 20);
            this.TxtItName.TabIndex = 32;
            this.TxtItName.Validated += new System.EventHandler(this.TxtItName_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(69, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 14);
            this.label3.TabIndex = 31;
            this.label3.Text = "Price";
            // 
            // TxtUPriceBeforeTax
            // 
            this.TxtUPriceBeforeTax.EnterMoveNextControl = true;
            this.TxtUPriceBeforeTax.Location = new System.Drawing.Point(107, 47);
            this.TxtUPriceBeforeTax.Name = "TxtUPriceBeforeTax";
            this.TxtUPriceBeforeTax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUPriceBeforeTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPriceBeforeTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUPriceBeforeTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUPriceBeforeTax.Properties.ReadOnly = true;
            this.TxtUPriceBeforeTax.Size = new System.Drawing.Size(253, 20);
            this.TxtUPriceBeforeTax.TabIndex = 33;
            this.TxtUPriceBeforeTax.Validated += new System.EventHandler(this.TxtUPriceBeforeTax_Validated);
            // 
            // TxtUPriceAfterTax
            // 
            this.TxtUPriceAfterTax.EnterMoveNextControl = true;
            this.TxtUPriceAfterTax.Location = new System.Drawing.Point(107, 68);
            this.TxtUPriceAfterTax.Name = "TxtUPriceAfterTax";
            this.TxtUPriceAfterTax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUPriceAfterTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPriceAfterTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUPriceAfterTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUPriceAfterTax.Properties.ReadOnly = true;
            this.TxtUPriceAfterTax.Size = new System.Drawing.Size(253, 20);
            this.TxtUPriceAfterTax.TabIndex = 35;
            this.TxtUPriceAfterTax.Validated += new System.EventHandler(this.TxtUPriceAfterTax_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 14);
            this.label4.TabIndex = 34;
            this.label4.Text = "Price (Incl. Tax)";
            // 
            // FrmRptPurchaserPerformance2Dlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmRptPurchaserPerformance2Dlg";
            this.Text = "Purchase Invoice Document";
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceBeforeTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceAfterTax.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.TextEdit TxtUPriceAfterTax;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit TxtUPriceBeforeTax;
        private DevExpress.XtraEditors.TextEdit TxtItName;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label2;
        protected DevExpress.XtraEditors.SimpleButton BtnExcel;
    }
}