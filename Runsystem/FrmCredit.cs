﻿#region Update
/*
    28/11/2017 [WED] bisa edit site
    09/03/2018 [ARI] tambah internal indikator, tambah dialog coa
    07/06/2020 [TKG/TWC] tambah cost center group
 *  18/06/2020 [HAR/SRN] master credit tidak bisa save costcenter group saat edi
 *  08/12/2021 [ISD/PHT] site tidak mandatory based on param mIsCreditSiteMandatory
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCredit : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mCreditCode = string.Empty;
        internal FrmCreditFind FrmFind;
        internal bool
            mIsSiteMandatory = false,
            mIsFilterBySiteHR = false,
            mIsCreditSiteMandatory = false;

        #endregion

        #region Constructor

        public FrmCredit(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
                GetParameter();
                if (Sm.GetParameter("DocTitle") != "PHT")
                {
                    if (mIsSiteMandatory) LblSiteCode.ForeColor = Color.Red;
                }
                else
                {
                    if (mIsCreditSiteMandatory) LblSiteCode.ForeColor = Color.Red;
                }

                Sl.SetLueOption(ref LueCCGrpCode, "CostCenterGroup");

                //if this application is called from other application
                if (mCreditCode.Length != 0)
                {
                    ShowData(mCreditCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCreditCode, TxtCreditName, LueSiteCode, ChkInternalInd, LueCCGrpCode
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    TxtCreditCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtCreditName, LueSiteCode, ChkInternalInd, LueCCGrpCode }, false);
                    TxtCreditName.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueSiteCode, LueCCGrpCode, ChkInternalInd }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    ChkActInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtCreditCode, TxtCreditName, LueSiteCode, TxtAcNo, TxtAcDesc,
               LueCCGrpCode
            });
            ChkActInd.Checked = false;
            ChkInternalInd.Checked = ChkInternalInd.Checked = false;
        }

        internal void ShowData(string CreditCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@CreditCode", CreditCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select A.CreditCode, A.CreditName, A.ActInd, A.SiteCode,  A.AcNo, B.AcDesc, A.InternalInd, A.CCGrpCode " +
                        "From TblCredit A " +
                        "Left Join TblCOA B On A.AcNo=B.AcNo " +
                        "Where A.CreditCode=@CreditCode; ",
                        new string[] 
                        {
                            //0
                            "CreditCode",
                            
                            //1-5
                            "CreditName", "ActInd", "SiteCode", "AcNo",  "AcDesc",  
                            
                            //6-7
                            "InternalInd", "CCGrpCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtCreditCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtCreditName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                            Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[3]), string.Empty);
                            TxtAcNo.EditValue = Sm.DrStr(dr, c[4]);
                            TxtAcDesc.EditValue = Sm.DrStr(dr, c[5]);
                            ChkInternalInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[6]), "Y");
                            Sm.SetLue(LueCCGrpCode, Sm.DrStr(dr, c[7]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCreditFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                //if (mIsSiteMandatory) LblSiteCode.ForeColor = Color.Red;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCreditCode, "", false)) return;
            SetFormControl(mState.Edit);
            IfDataInactive();
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCreditCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblCredit Where CreditCode=@CreditCode" };
                Sm.CmParam<String>(ref cm, "@CreditCode", TxtCreditCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            if (TxtCreditCode.Text.Length == 0)
            {
                InsertData();
            }
            else
            {
                EditData();
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtCreditName, "Credit Name", false) ||
                (((Sm.GetParameter("DocTitle") !="PHT" && mIsSiteMandatory) || (Sm.GetParameter("DocTitle") == "PHT" && mIsCreditSiteMandatory)) && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                IsCreditCodeExisted();
        }

        private bool IsCreditCodeExisted()
        {
            if (!TxtCreditCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText =
                        "Select CreditCode From TblCredit " +
                        "Where CreditCode =@CreditCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@CreditCode", TxtCreditCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Credit code ( " + TxtCreditCode.Text + " ) already existed.");
                    TxtCreditCode.Focus();
                    return true;
                }
            }
            return false;
        }

        private void InsertData()
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                string CreditCode = string.Empty;

                CreditCode = GenerateCreditCode();

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblCredit(CreditCode, CreditName, ActInd, SiteCode, AcNo, InternalInd, CCGrpCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@CreditCode, @CreditName, 'Y', @SiteCode, @AcNo, @InternalInd, @CCGrpCode, @UserCode, CurrentDateTime()); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@CreditCode", CreditCode);
                Sm.CmParam<String>(ref cm, "@CreditName", TxtCreditName.Text);
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
                Sm.CmParam<String>(ref cm, "@InternalInd", ChkInternalInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@CCGrpCode", Sm.GetLue(LueCCGrpCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
              
                Sm.ExecCommand(cm);

                ShowData(CreditCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Edit

        private bool IsEditDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtCreditCode, "Credit Code", false) ||
                Sm.IsTxtEmpty(TxtCreditName, "Credit Name", false) ||
                (((Sm.GetParameter("DocTitle") != "PHT" && mIsSiteMandatory) || (Sm.GetParameter("DocTitle") == "PHT" && mIsCreditSiteMandatory)) && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                IsCreditAlreadyInactive(); 
        }

        private bool IsCreditAlreadyInactive()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CreditCode From TblCredit ");
            SQL.AppendLine("Where ActInd='N' And CreditCode=@CreditCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CreditCode", TxtCreditCode.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already inactive.");
                return true;
            }

            return false;
        }

        private void EditData()
        {
            try
            {
                if (IsEditDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Update TblCredit ");
                SQL.AppendLine("  Set CreditName=@CreditName, ActInd = @ActInd, SiteCode=@SiteCode, AcNo=@AcNo, CCGrpCode=@CCGrpCode, InternalInd=@InternalInd, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where CreditCode = @CreditCode; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@CreditCode", TxtCreditCode.Text);
                Sm.CmParam<String>(ref cm, "@CreditName", TxtCreditName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@CCGrpCode", Sm.GetLue(LueCCGrpCode));
                Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
                Sm.CmParam<String>(ref cm, "@InternalInd", ChkInternalInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtCreditCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void IfDataInactive()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CreditCode From TblCredit ");
            SQL.AppendLine("Where ActInd='N' And CreditCode=@CreditCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CreditCode", TxtCreditCode.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueSiteCode }, true);
            }
        }

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsCreditSiteMandatory = Sm.GetParameterBoo("IsCreditSiteMandatory");
        }

        private string GenerateCreditCode()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("   Select Right(Concat('0000', Convert(CreditCode+1, Char)), 4) From ( ");
            SQL.AppendLine("       Select Convert(Left(CreditCode, 4), Decimal) As CreditCode From TblCredit ");
            SQL.AppendLine("       Order By Left(CreditCode, 4) Desc Limit 1 ");
            SQL.AppendLine("       ) As Temp ");
            SQL.AppendLine("   ), '0001') As CreditCode ");

            return Sm.GetValue(SQL.ToString());
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtCreditCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtCreditCode);
        }

        private void TxtCreditName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtCreditName);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
        }

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmCreditDlg(this));
        }

        private void LueCCGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCCGrpCode, new Sm.RefreshLue2(Sl.SetLueOption), "CostCenterGroup");
        }

        #endregion

        #endregion
    }
}
