﻿#region Update
/*
    23/04/2020 [VIN/KBN] new apps
    19/05/2020 [TKG/IMS] tambah cost center
    08/07/2020 [IBL/SIER] cost category filter berdasarkan department
    14/12/2020 [VIN/SIER] copy AcNo dan AcDesc ke Parent
    19/01/2021 [WED/PHT] ketika ubah data, item, qty, rate, di clear berdasarkan parameter IsCASUseItemRate
    11/02/2021 [TKG/PHT] perubahan filter saat memilih cost category
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmCashAdvanceSettlementDlg4 : RunSystem.FrmBase2
    {
        #region Field

        private FrmCashAdvanceSettlement mFrmParent;
        private string mSQL = string.Empty;
        private int mCurRow = 0;

        #endregion

        #region Constructor

        public FrmCashAdvanceSettlementDlg4(FrmCashAdvanceSettlement FrmParent, int Row)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCurRow = Row;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Cost Category";

                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetLueCCCode(ref LueCCCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Cost Category Code", 
                        "Cost Category Name",
                        "COA",
                        "COA Description",
                        "Cost Center"

                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        120, 300, 130, 250, 200
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.CCtCode, CONCAT(A.CCtName, ' (', IfNull(C.CCName, '-'), ')') CCtName, A.AcNo, B.AcDesc, C.CCName  ");
            SQL.AppendLine("FROM TblCostCategory A ");
            SQL.AppendLine("INNER JOIN TblCOA B ON A.AcNo = B.AcNo ");
            SQL.AppendLine("INNER JOIN TblCostCenter C ON A.CCCode = C.CCCode ");
            SQL.AppendLine("Where A.CCCode=@CCCode ");
            if (mFrmParent.mIsSystemUseCostCenter)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                SQL.AppendLine("    Where CCCode=A.CCCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            if (mFrmParent.mIsCostCategoryFilteredInCashAdvanceSettlement)
                SQL.AppendLine("And C.DeptCode = @DeptCode ");


            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (Sm.IsLueEmpty(LueCCCode, "Cost center")) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DeptCode", mFrmParent.mDeptCode);
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "B.AcNo", "B.AcDesc" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.CCtCode;",
                        new string[] 
                        { 
                        
                            //0
                            "CCtCode",
                            
                            //1-4
                            "CCtName", "AcNo", "AcDesc", "CCName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 3, Grd1, Row, 1);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 4, Grd1, Row, 2);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 21, Grd1, Row, 3);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 22, Grd1, Row, 4);
                //Sm.CopyGrdValue(mFrmParent.Grd2, mCurRow, 1, Grd1, Row, 2);
                mFrmParent.ComputeAmt1();
                mFrmParent.SetCostCategoryInfo();
                mFrmParent.SetVoucherInfo();
                mFrmParent.ComputeAmt2();
                mFrmParent.ClearItem();
                this.Close();
            }
        }

        private void SetLueCCCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CCCode As Col1, CCName As Col2 From TblCostCenter Where ActInd='Y' And CCCode In (Select Distinct CCCode From TblCostCategory) Order By CCName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        

        #endregion

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event 

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "COA");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(SetLueCCCode));
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost center");
        }

        #endregion
    }
    
}
