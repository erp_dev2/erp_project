﻿/*
    02/05/2018 [TKG] bug perhitungan loan, perubahan perhitungan brutto
    14/05/2018 [TKG] untuk ho brutto ditambah sci
    10/07/2018 [TKG] ubah rumus brutto
*/

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptSiteBruttoComponent : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool
            mIsNotFilterByAuthorization = false,
            mIsFilterBySiteHR = false,
            mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmRptSiteBruttoComponent(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");

        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var subSQL = string.Empty;

            SQL2.AppendLine("From TblPayrollProcess1 T1 ");
            SQL2.AppendLine("Inner Join TblPayrun T2 ");
            SQL2.AppendLine("    On T1.PayrunCode=T2.PayrunCode ");
            SQL2.AppendLine("    And T2.CancelInd='N' ");
            SQL2.AppendLine(Filter.Replace("B.","T2."));
            if (mIsFilterBySiteHR)
            {
                SQL2.AppendLine("And T2.SiteCode Is Not Null ");
                SQL2.AppendLine("And Exists( ");
                SQL2.AppendLine("    Select 1 From TblGroupSite ");
                SQL2.AppendLine("    Where SiteCode=IfNull(T2.SiteCode, '') ");
                SQL2.AppendLine("    And GrpCode In ( ");
                SQL2.AppendLine("        Select GrpCode From TblUser ");
                SQL2.AppendLine("        Where UserCode=@UserCode ");
                SQL2.AppendLine("    ) ");
                SQL2.AppendLine(") ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL2.AppendLine("And T2.DeptCode Is Not Null ");
                SQL2.AppendLine("And Exists( ");
                SQL2.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL2.AppendLine("    Where DeptCode=IfNull(T2.DeptCode, '') ");
                SQL2.AppendLine("    And GrpCode In ( ");
                SQL2.AppendLine("        Select GrpCode From TblUser ");
                SQL2.AppendLine("        Where UserCode=@UserCode ");
                SQL2.AppendLine("    ) ");
                SQL2.AppendLine(") ");
            }
            subSQL = SQL2.ToString();

            SQL.AppendLine("Select A.PayrunCode, B.PayrunName, C.SiteName, D.DeptName, E.Component, E.Amt ");
            SQL.AppendLine("From ( " );
            SQL.AppendLine("    Select Distinct T1.PayrunCode, T2.SiteCode, T2.DeptCode ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblPayrun B ");
            SQL.AppendLine("    On A.PayrunCode=B.PayrunCode ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine(Filter);
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("And B.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("And B.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(B.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Inner Join TblSite C On B.SiteCode=C.SiteCode ");
            SQL.AppendLine("Inner Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join (");
            
            SQL.AppendLine("    Select '0' As Seq, T1.PayrunCode, 'Basic Salary' As Component, Sum(T1.Salary) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.Salary)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, T4.ADName As Component, Sum(T3.Amt) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Inner Join TblEmployeeAllowanceDeduction T3  ");
            SQL.AppendLine("    On T1.EmpCode=T3.EmpCode ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (T3.StartDt Is Null And T3.EndDt Is Null) Or ");
            SQL.AppendLine("    (T3.StartDt Is Not Null And T3.EndDt Is Null And T3.StartDt<=T2.EndDt) Or ");
            SQL.AppendLine("    (T3.StartDt Is Null And T3.EndDt Is Not Null And T2.EndDt<=T3.EndDt) Or ");
            SQL.AppendLine("    (T3.StartDt Is Not Null And T3.EndDt Is Not Null And T3.StartDt<=T2.EndDt And T2.EndDt<=T3.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction T4 On T3.ADCode=T4.ADCode And T4.ADType='A' And T4.AmtType='1' ");
            SQL.AppendLine("    Group By T1.PayrunCode, T4.ADName ");
            SQL.AppendLine("    Having Sum(T3.Amt)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, T4.ADName As Component, Sum(T3.Amt) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Inner Join TblEmployeeAllowanceDeduction T3  ");
            SQL.AppendLine("    On T1.EmpCode=T3.EmpCode ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (T3.StartDt Is Null And T3.EndDt Is Null) Or ");
            SQL.AppendLine("    (T3.StartDt Is Not Null And T3.EndDt Is Null And T3.StartDt<=T2.EndDt) Or ");
            SQL.AppendLine("    (T3.StartDt Is Null And T3.EndDt Is Not Null And T2.EndDt<=T3.EndDt) Or ");
            SQL.AppendLine("    (T3.StartDt Is Not Null And T3.EndDt Is Not Null And T3.StartDt<=T2.EndDt And T2.EndDt<=T3.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction T4 On T3.ADCode=T4.ADCode And T4.ADType='D' And T4.AmtType='1' ");
            SQL.AppendLine("    Group By T1.PayrunCode, T4.ADName ");
            SQL.AppendLine("    Having Sum(T3.Amt)<>0.00 ");
            SQL.AppendLine("Union All ");


            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Transport' As Component, Sum(T1.Transport) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.Transport)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Meal' As Component, Sum(T1.Meal) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.Meal)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Service Charge' As Component, Sum(T1.ServiceChargeIncentive) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.ServiceChargeIncentive)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Salary Adjustment' As Component, Sum(T1.SalaryAdjustment) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.SalaryAdjustment)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Annual Leave Allowance' As Component, Sum(T1.ADLeave) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.ADLeave)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, Concat('Loan ', IfNull(T5.CreditName, '')) As Component, Sum(T3.Amt) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Inner Join TblAdvancePaymentProcess T3 On T1.PayrunCode=T3.PayrunCode And T1.EmpCode=T3.EmpCode ");
            SQL.AppendLine("    Inner Join TblAdvancePaymentHdr T4 On T3.DocNo=T4.DocNo ");
            SQL.AppendLine("    Inner Join TblCredit T5 On T4.CreditCode=T5.CreditCode ");
            SQL.AppendLine("    Group By T1.PayrunCode, IfNull(T5.CreditName, '') ");
            SQL.AppendLine("    Having Sum(T3.Amt)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Employer BPJS (Health)' As Component, Sum(T1.SSEmployerHealth) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.SSEmployerHealth)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Employer BPJS (Employment)' As Component, Sum(T1.SSEmployerEmployment) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.SSEmployerEmployment)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Employer BPJS (Pension)' As Component, Sum(T1.SSErPension) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.SSErPension)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Employer Pension (HII)' As Component, Sum(T1.SSEmployerPension) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.SSEmployerPension)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Employer Pension (Natour)' As Component, Sum(T1.SSEmployerPension2) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.SSEmployerPension2)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Employee BPJS (Health)' As Component, Sum(T1.SSEmployeeHealth) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.SSEmployeeHealth)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Employee BPJS (Employment)' As Component, Sum(T1.SSEmployeeEmployment) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.SSEmployeeEmployment)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Employee BPJS (Pension)' As Component, Sum(T1.SSEePension) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.SSEePension)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Employee Pension (HII)' As Component, Sum(T1.SSEmployeePension) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.SSEmployeePension)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '1' As Seq, T1.PayrunCode, 'Employee Pension (Natour)' As Component, Sum(T1.SSEmployeePension2) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.SSEmployeePension2)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '2' As Seq, T1.PayrunCode, 'Brutto' As Component, ");
            SQL.AppendLine("Sum( ");
            SQL.AppendLine("T1.Salary + ");
            SQL.AppendLine("T1.TaxableFixAllowance + ");
            SQL.AppendLine("T1.Transport + ");
            SQL.AppendLine("T1.Meal + ");
            SQL.AppendLine("T1.ADLeave + ");
            SQL.AppendLine("T1.SalaryAdjustment + ");
            SQL.AppendLine("T1.ServiceChargeIncentive + ");
            SQL.AppendLine("T1.SSEmployerHealth + ");
            SQL.AppendLine("T1.SSErLifeInsurance + ");
            SQL.AppendLine("T1.SSErWorkingAccident ");
            
            //SQL.AppendLine("Case When T3.HOInd='Y' Then ");
            //SQL.AppendLine("    T1.Salary ");
            //SQL.AppendLine("    +T1.TaxableFixAllowance ");
            //SQL.AppendLine("    -T1.TaxableFixDeduction ");
            //SQL.AppendLine("    +T1.Transport ");
            //SQL.AppendLine("    +T1.Meal ");
            //SQL.AppendLine("    +T1.ADLeave ");
            //SQL.AppendLine("    +T1.SalaryAdjustment ");
            //SQL.AppendLine("    +T1.ServiceChargeIncentive ");
            //SQL.AppendLine("    - (T1.SSEmployeeHealth ");
            //SQL.AppendLine("    + T1.SSEmployeeEmployment ");
            //SQL.AppendLine("    + T1.SSEePension ");
            //SQL.AppendLine("    + T1.SSEmployeePension ");
            //SQL.AppendLine("    + T1.SSEmployeePension2 ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine("Else ");
            //SQL.AppendLine("    T1.Salary ");
            //SQL.AppendLine("    +T1.TaxableFixAllowance ");
            //SQL.AppendLine("    -T1.TaxableFixDeduction ");
            //SQL.AppendLine("    +T1.Transport ");
            //SQL.AppendLine("    +T1.Meal ");
            //SQL.AppendLine("    +T1.ADLeave ");
            //SQL.AppendLine("    +T1.SalaryAdjustment ");
            //SQL.AppendLine("    +T1.ServiceChargeIncentive ");
            //SQL.AppendLine("    - (T1.SSEmployeeHealth ");
            //SQL.AppendLine("    + T1.SSEmployeeEmployment ");
            //SQL.AppendLine("    + T1.SSEePension ");
            //SQL.AppendLine("    + T1.SSEmployeePension ");
            //SQL.AppendLine("    + T1.SSEmployeePension2 ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine("End ");
            SQL.AppendLine(") As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Inner Join TblSite T3 On T2.SiteCode=T3.SiteCode ");
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having ");
            SQL.AppendLine("Sum( ");

            SQL.AppendLine("T1.Salary + ");
            SQL.AppendLine("T1.TaxableFixAllowance + ");
            SQL.AppendLine("T1.Transport + ");
            SQL.AppendLine("T1.Meal + ");
            SQL.AppendLine("T1.ADLeave + ");
            SQL.AppendLine("T1.SalaryAdjustment + ");
            SQL.AppendLine("T1.ServiceChargeIncentive + ");
            SQL.AppendLine("T1.SSEmployerHealth + ");
            SQL.AppendLine("T1.SSErLifeInsurance + ");
            SQL.AppendLine("T1.SSErWorkingAccident ");

            //SQL.AppendLine("Case When T3.HOInd='Y' Then ");
            //SQL.AppendLine("    T1.Salary ");
            //SQL.AppendLine("    +T1.TaxableFixAllowance ");
            //SQL.AppendLine("    -T1.TaxableFixDeduction ");
            //SQL.AppendLine("    +T1.Transport ");
            //SQL.AppendLine("    +T1.Meal ");
            //SQL.AppendLine("    +T1.ADLeave ");
            //SQL.AppendLine("    +T1.SalaryAdjustment ");
            //SQL.AppendLine("    - (T1.SSEmployeeHealth ");
            //SQL.AppendLine("    + T1.SSEmployeeEmployment ");
            //SQL.AppendLine("    + T1.SSEePension ");
            //SQL.AppendLine("    + T1.SSEmployeePension ");
            //SQL.AppendLine("    + T1.SSEmployeePension2 ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine("Else ");
            //SQL.AppendLine("    T1.Salary ");
            //SQL.AppendLine("    +T1.TaxableFixAllowance ");
            //SQL.AppendLine("    -T1.TaxableFixDeduction ");
            //SQL.AppendLine("    +T1.Transport ");
            //SQL.AppendLine("    +T1.Meal ");
            //SQL.AppendLine("    +T1.ADLeave ");
            //SQL.AppendLine("    +T1.SalaryAdjustment ");
            //SQL.AppendLine("    +T1.ServiceChargeIncentive ");
            //SQL.AppendLine("    - (T1.SSEmployeeHealth ");
            //SQL.AppendLine("    + T1.SSEmployeeEmployment ");
            //SQL.AppendLine("    + T1.SSEePension ");
            //SQL.AppendLine("    + T1.SSEmployeePension ");
            //SQL.AppendLine("    + T1.SSEmployeePension2 ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine("End ");
            SQL.AppendLine(")<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '3' As Seq, T1.PayrunCode, 'Total Tax' As Component, Sum(T1.Tax) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.Tax)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '4' As Seq, T1.PayrunCode, 'Tax Allowance' As Component, Sum(T1.TaxAllowance) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.TaxAllowance)<>0.00 ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '5' As Seq, T1.PayrunCode, 'Tax-Tax Allowance' As Component, Sum(T1.Tax-T1.TaxAllowance) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Where (T1.Tax<>0.00 Or T1.TaxAllowance<>0.00) ");
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select '6' As Seq, T1.PayrunCode, 'Take Home Pay' As Component, Sum(T1.Amt) As Amt ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine("    Having Sum(T1.Amt)<>0.00 ");
            
            SQL.AppendLine(") E On A.PayrunCode=E.PayrunCode ");
            SQL.AppendLine("Where 1=1 ");
            SQL.AppendLine("And (Seq='5' Or (Seq<>'5' And IfNull(E.Amt, 0.00)<>0.00)) ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(" Order By A.PayrunCode, E.Seq, E.Component;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Payrun"+Environment.NewLine+"Code",
                        "Payrun Name",
                        "Unit",
                        "Department",
                        "Component",

                        //6
                        "Amount"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 200, 200, 200,
                        
                        //6
                        130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
            
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = " And 1=1 ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtPayrunCode.Text, new string[] { "B.PayrunCode", "B.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "B.SiteCode", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter),
                        new string[]
                        {
                            //0
                            "PayrunCode",

                            //1-5
                            "PayrunName", "SiteName", "DeptName", "Component", "Amt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void TxtPayrunCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPayrunCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun");
        }

        #endregion

        #endregion
    }
}
