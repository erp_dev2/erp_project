﻿namespace RunSystem
{
    partial class FrmPosNo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtPosNo = new DevExpress.XtraEditors.TextEdit();
            this.BtnCopy = new System.Windows.Forms.Button();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueCCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblCCCode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtPosName = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(531, 0);
            this.panel1.Size = new System.Drawing.Size(70, 282);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtPosName);
            this.panel2.Controls.Add(this.LueCCCode);
            this.panel2.Controls.Add(this.LblCCCode);
            this.panel2.Controls.Add(this.LblSiteCode);
            this.panel2.Controls.Add(this.LueSiteCode);
            this.panel2.Controls.Add(this.BtnCopy);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.LueWhsCode);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.LueCtCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtPosNo);
            this.panel2.Size = new System.Drawing.Size(531, 282);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(57, 87);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Warehouse";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(133, 84);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 25;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 300;
            this.LueWhsCode.Size = new System.Drawing.Size(300, 20);
            this.LueWhsCode.TabIndex = 14;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(67, 65);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 14);
            this.label5.TabIndex = 11;
            this.label5.Text = "Customer";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(133, 62);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 25;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(300, 20);
            this.LueCtCode.TabIndex = 12;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(53, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 14);
            this.label6.TabIndex = 9;
            this.label6.Text = "Pos Number";
            // 
            // TxtPosNo
            // 
            this.TxtPosNo.EnterMoveNextControl = true;
            this.TxtPosNo.Location = new System.Drawing.Point(133, 40);
            this.TxtPosNo.Name = "TxtPosNo";
            this.TxtPosNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPosNo.Properties.MaxLength = 30;
            this.TxtPosNo.Size = new System.Drawing.Size(118, 20);
            this.TxtPosNo.TabIndex = 10;
            // 
            // BtnCopy
            // 
            this.BtnCopy.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCopy.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnCopy.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCopy.ForeColor = System.Drawing.Color.White;
            this.BtnCopy.Location = new System.Drawing.Point(133, 189);
            this.BtnCopy.Name = "BtnCopy";
            this.BtnCopy.Size = new System.Drawing.Size(178, 25);
            this.BtnCopy.TabIndex = 21;
            this.BtnCopy.Text = "Copy Pos Number Setting";
            this.BtnCopy.UseVisualStyleBackColor = false;
            this.BtnCopy.Click += new System.EventHandler(this.BtnCopy_Click);
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(98, 131);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 17;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(133, 128);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 400;
            this.LueSiteCode.Size = new System.Drawing.Size(300, 20);
            this.LueSiteCode.TabIndex = 18;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // LueCCCode
            // 
            this.LueCCCode.EnterMoveNextControl = true;
            this.LueCCCode.Location = new System.Drawing.Point(133, 151);
            this.LueCCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCCCode.Name = "LueCCCode";
            this.LueCCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.Appearance.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCCCode.Properties.DropDownRows = 30;
            this.LueCCCode.Properties.NullText = "[Empty]";
            this.LueCCCode.Properties.PopupWidth = 300;
            this.LueCCCode.Size = new System.Drawing.Size(300, 20);
            this.LueCCCode.TabIndex = 20;
            this.LueCCCode.ToolTip = "F4 : Show/hide list";
            this.LueCCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCCCode.EditValueChanged += new System.EventHandler(this.LueCCCode_EditValueChanged);
            // 
            // LblCCCode
            // 
            this.LblCCCode.AutoSize = true;
            this.LblCCCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCCCode.ForeColor = System.Drawing.Color.Black;
            this.LblCCCode.Location = new System.Drawing.Point(54, 154);
            this.LblCCCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCCCode.Name = "LblCCCode";
            this.LblCCCode.Size = new System.Drawing.Size(72, 14);
            this.LblCCCode.TabIndex = 19;
            this.LblCCCode.Text = "Cost Center";
            this.LblCCCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(65, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 14);
            this.label1.TabIndex = 15;
            this.label1.Text = "Pos Name";
            // 
            // TxtPosName
            // 
            this.TxtPosName.EnterMoveNextControl = true;
            this.TxtPosName.Location = new System.Drawing.Point(133, 106);
            this.TxtPosName.Name = "TxtPosName";
            this.TxtPosName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosName.Properties.Appearance.Options.UseFont = true;
            this.TxtPosName.Properties.MaxLength = 30;
            this.TxtPosName.Size = new System.Drawing.Size(300, 20);
            this.TxtPosName.TabIndex = 16;
            // 
            // FrmPosNo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 282);
            this.Name = "FrmPosNo";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueCtCode;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtPosNo;
        private System.Windows.Forms.Button BtnCopy;
        private System.Windows.Forms.Label LblSiteCode;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private DevExpress.XtraEditors.LookUpEdit LueCCCode;
        private System.Windows.Forms.Label LblCCCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtPosName;
    }
}