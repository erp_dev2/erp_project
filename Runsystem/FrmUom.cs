﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmUom : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmUomFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmUom(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetGrd();
            SetFormControl(mState.View);

            Sl.SetLueUomCode(ref LueUomCode);
            LueUomCode.Visible = false;

            base.FrmLoad(sender, e);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtUomCode, TxtUomName
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2 }); 
                    TxtUomCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtUomCode, TxtUomName
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2 }); 
                    TxtUomCode.Focus();
                    break;

                case mState.Edit:
                    Sm.SetControlReadOnly(TxtUomName, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2 });
                    TxtUomName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtUomCode, TxtUomName
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]{ "Uom Code", "Convert To UoM", "Converted Quantity" },
                    new int[]{ 0, 200, 200 }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 8);
        }

        #endregion

        #region Button Method

       override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmUomFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtUomCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtUomCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() 
                { 
                    CommandText =
                        "Delete From TblUomConvertion Where UomCode=@UomCode Or UomCode2=@UomCode; " +
                        "Delete From TblUom Where UomCode=@UomCode; "
                };
                Sm.CmParam<String>(ref cm, "@UomCode", TxtUomCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveUom());
                
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveUomConvertion(Row));

                Sm.ExecCommands(cml);

                ShowData(TxtUomCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1) LueRequestEdit(Grd1, LueUomCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = 0m;
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 2 }, e);
        }

        #endregion

        #region Show Data

        public void ShowData(string UomCode)
        {
            try
            {
                ClearData();
                ShowUom(UomCode);
                ShowUomConvertion(UomCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowUom(string UomCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@UomCode", UomCode);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select UomCode, UomName From TblUom Where UomCode=@UomCode",
                    new string[]{ "UomCode", "UomName" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtUomCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtUomName.EditValue = Sm.DrStr(dr, c[1]);
                    }, true
                );
        }

        private void ShowUomConvertion(string UomCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@UomCode", UomCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.UomCode2, B.UomName, A.Qty " +
                    "From TblUomConvertion A, TblUom B " +
                    "Where A.UomCode=@UomCode And A.UomCode2=B.UomCode " +
                    "Order By B.UomName",
                    new string[] 
                    { 
                        "UomCode2", "UomName", "Qty",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtUomCode, "Uom code", false) ||
                Sm.IsTxtEmpty(TxtUomName, "Uom name", false) ||
                IsUomCodeExisted() ||
                IsGrdValueNotValid() ||
                IsDoubleEntry()
                ;
        }

        private bool IsUomCodeExisted()
        {
            if (!TxtUomCode.Properties.ReadOnly) 
            {
                var cm = new MySqlCommand() { CommandText = "Select UomCode From TblUom Where UomCode=@UomCode Limit 1" };
                Sm.CmParam<String>(ref cm, "@UomCode", TxtUomCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Uom code ( " + TxtUomCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Uom is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, true, "Quantity should be greater than 0.")) return true;
                }
            }
            return false;
        }

        private bool IsDoubleEntry()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row1 = 0; Row1 < Grd1.Rows.Count - 1; Row1++)
                {
                    if (Sm.GetGrdStr(Grd1, Row1, 0).Length != 0)
                    {
                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            if (Row1 != Row2 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row1, 0), Sm.GetGrdStr(Grd1, Row2, 0)))
                            {
                                Sm.StdMsg(mMsgType.Warning,
                                    "Uom : " + TxtUomName.Text + Environment.NewLine +
                                    "Converted Uom : " + Sm.GetGrdStr(Grd1, Row2, 1) + Environment.NewLine + Environment.NewLine +
                                    "Double entry. ");
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveUom()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblUom(UomCode,UomName, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@UomCode, @UomName, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update UomName=@UomName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
            
            SQL.AppendLine("Delete From TblUomConvertion Where UomCode=@UomCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@UomCode", TxtUomCode.Text);
            Sm.CmParam<String>(ref cm, "@UomName", TxtUomName.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveUomConvertion(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblUomConvertion(UomCode,UomCode2, Qty, CreateBy, CreateDt, LastUpBy, LastUpDt) " +
                    "Select UomCode, @UomCode2, @Qty, CreateBy, CreateDt, LastUpBy, LastUpDt " +
                    "From TblUom Where UomCode=@UomCode; "
            };
            Sm.CmParam<String>(ref cm, "@UomCode", TxtUomCode.Text);
            Sm.CmParam<String>(ref cm, "@UomCode2", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2 });
        }

        private void LueRequestEdit(
            iGrid Grd,
            DevExpress.XtraEditors.LookUpEdit Lue,
            ref iGCell fCell,
            ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueUomCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUomCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueUomCode_Leave(object sender, EventArgs e)
        {
            if (LueUomCode.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueUomCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 0].Value =
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueUomCode);
                    Grd1.Cells[fCell.RowIndex, 1].Value = LueUomCode.GetColumnValue("Col2");
                }
            }
        }

        private void LueUomCode_Validated(object sender, EventArgs e)
        {
            LueUomCode.Visible = false;
        }

        private void TxtUomCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtUomCode);
        }

        private void TxtUomName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtUomName);
        }

        #endregion

        #endregion
    }
}
