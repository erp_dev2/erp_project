﻿#region Update
/*
    23/08/2017 [HAR] tambah source KPI, edit cuma bisa menonaktifkan document
    19/12/2017 [HAR] tambah info site berdasarkan parameter isfilterbysite
    27/03/2018 [HAR] responsibility wajib di isi, jobdesc di hide
    23/04/2018 [HAR] target bulanan bisa di isi minus
    23/08/2018 [WED] KPIDtl tambah ControlInd, untuk keperluan rumus di Performance Review. ControlInd == "N", makin besar makin baik. ControlInd == "Y", makin kecil makin baik
    18/01/2019 [HAR] KPIDtl tidak bisa di delete jika pake source
    21/01/2019 [WED] Validasi Result name tidak boleh sama / duplicate
    22/01/2019 [HAR] tambah inputan year mandatory 
    29/01/2020 [HAR/HIN] waktu edit dan save nilai KPI Dno berubah
    25/09/2020 [VIN/SIER] bug: gagal save dno KPI detail
    12/11/2021 [VIN/ALL] bug: row+1 terhapus saat edit row sebelumnya
    21/12/2021 [MYA/ALL] Adanya Approval setelah mengisi data pada menu KPI
    23/12/2021 [TYO/ALL] Menambah kolom Sequence berdasarkan parameter IsKPIProcessUseSequence
    28/12/2021 [RIS/ALL] Menambah Kolom Maximum/Minimum dengan param IsKPIUseMaxMin
    26/08/2022 [ICA/TWC] bug : warning sequence cannot same muncul terus menyebabkan app stuck. Menambah validasi ketika sequence sama ketika save (validasi baru di grdaftercommitedit). 
    29/08/2022 [ICA/TWC] menambah validasi Row != row di method IsSequenceNotValid
    13/09/2022 [HPH/TWC] menambah kolom total bobot pada header dan menjumlahkan kolom bobot pada detail ditampilkan pada header menggunakan parameter IsKPIUseTotalBobot
    19/09/2022 [HPH/TWC] bug : kolom total bobot tidak hilang saat parameter IsKPIUseTotalBobot bernilai N
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;


#endregion

namespace RunSystem
{
    public partial class FrmKPI : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal string mDocNo = string.Empty;
        iGCell fCell;
        bool fAccept;
        internal FrmKPIFind FrmFind;
        internal bool mIsFilterBySite = false,
        mIsKPIProcessUseSequence = false,
        misKPIUseMaxMin = false,
        mIsKPIUseTotalBobot = false;

        #endregion

        #region Constructor

        public FrmKPI(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "KPI";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetLueOption(ref LueOption);
                SetLueType(ref LueType);
                SetLueKPIParentCode(ref LueParentKPI);
                Sl.SetLueOption(ref LueMaxMin, "KPIMaxMin");
                SetFormControl(mState.View);
                LueOption.Visible = false;
                LueType.Visible = false;
                LueJobDesc.Visible = false;
                LueMaxMin.Visible = false;
                Sl.SetLuePosCode(ref LuePosCode);
                if (mIsFilterBySite) LblSiteCode.ForeColor = Color.Red;                              
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                base.FrmLoad(sender, e);
                TxtTotalBobot.Visible = mIsKPIUseTotalBobot ? true : false;
                label11.Visible = mIsKPIUseTotalBobot ? true : false;
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo, 0);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 4;

            SetGrdHdr(ref Grd1, 0, 0, "", 2, 0);
            SetGrdHdr(ref Grd1, 0, 1, "Result" + Environment.NewLine + "Indicator", 2, 250);
            SetGrdHdr(ref Grd1, 0, 2, "Bobot", 2, 80);
            SetGrdHdr(ref Grd1, 0, 3, "Unit of Measurement", 2, 150);
            SetGrdHdr(ref Grd1, 0, 4, "Year / Month", 2, 100);
            SetGrdHdr(ref Grd1, 0, 5, "Target of Year", 2, 150);
            SetGrdHdr2(ref Grd1, 1, 6, "Month", 12);
            SetGrdHdr(ref Grd1, 0, 18, "Type", 2, 100);
            SetGrdHdr(ref Grd1, 0, 19, "Job Desc", 2, 250);
            SetGrdHdr(ref Grd1, 0, 20, "KPI Present DocNo", 2, 100);
            SetGrdHdr(ref Grd1, 0, 21, "KPI Present DNo", 2, 100);
            SetGrdHdr(ref Grd1, 0, 22, "KPI Present DTbl", 2, 100);
            SetGrdHdr(ref Grd1, 0, 23, "Control", 2, 80);
            SetGrdHdr(ref Grd1, 0, 24, "Sequence", 2, 80);
            SetGrdHdr(ref Grd1, 0, 25, "Maximum/Minimum", 2, 120);
            SetGrdHdr(ref Grd1, 0, 26, "Maximum/MinimumCode", 2, 0);

            Sm.GrdFormatDec(Grd1, new int[] { 2, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 24 }, 1);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 19, 20, 21, 22, 24, 25 }, false);
            Sm.GrdColCheck(Grd1, new int[] { 23 });

            Grd1.Cols[18].Move(5);
            Grd1.Cols[19].Move(3);

            if (mIsKPIProcessUseSequence)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 24 }, true);
                Grd1.Cols[24].Move(1);
            }

            if (misKPIUseMaxMin)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 25, 26 }, true);
                Grd1.Cols[25].Move(8);
            }

        }

        private void SetGrdHdr(ref iGrid Grd, int row, int col, string Title, int SpanRows, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = SpanRows;
            Grd.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = 160;

            SetGrdHdr(ref Grd1, 0, col, "01", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 1, "02", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 2, "03", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 3, "04", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 4, "05", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 5, "06", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 6, "07", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 7, "08", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 8, "09", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 9, "10", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 10, "11", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 11, "12", 1, 80);
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, DteDocDt, TxtKPIName, TxtPICCode, TxtPICName, LueParentKPI, LuePosCode, LueSiteCode, MeeRemark,
                        TxtSourceKPI, LueYr, TxtTotalBobot,
                    }, true);
                    ChkActiveInd.Properties.ReadOnly = true;
                    BtnPIC.Enabled = false;
                    BtnSourceKPI.Enabled = false;
                    Grd1.ReadOnly = true;
                    Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 });
                    DteDocDt.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                      DteDocDt, TxtKPIName, LueParentKPI, LuePosCode, LueSiteCode, MeeRemark, LueYr
                    }, false);
                    ChkActiveInd.Checked = true;
                    BtnPIC.Enabled = true;
                    BtnSourceKPI.Enabled = true;
                    Grd1.ReadOnly = false;
                    Sm.GrdColReadOnly(false, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 });
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        // DteDocDt, TxtKPIName, LueParentKPI, LuePosCode, MeeRemark
                    }, false);
                    ChkActiveInd.Properties.ReadOnly = false;
                    Grd1.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 23 });
                    Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 25, 26 });
                    BtnPIC.Enabled = false;
                    BtnSourceKPI.Enabled = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, TxtKPIName, TxtPICCode, TxtPICName, LueParentKPI, LuePosCode,
                LueYr, MeeRemark, TxtSourceKPI, LueSiteCode, TxtStatus, TxtTotalBobot,
            });
            ClearGrd();
            ChkActiveInd.Checked = false;
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 24 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmKPIFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            //ParPrint();
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 23 && BtnSave.Enabled)
            {
                if (Grd1.Rows.Count > 0)
                {
                    bool IsSelected = Sm.GetGrdBool(Grd1, 0, 23);
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        Grd1.Cells[Row, 23].Value = !IsSelected;
                }
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 24 && mIsKPIProcessUseSequence)
                IsSequenceNotValid(e.RowIndex);
            if (e.ColIndex == 2 && mIsKPIUseTotalBobot) 
                ComputeBobot();
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {

            if (Grd1.SelectedRows.Count > 0 && TxtSourceKPI.Text.Length == 0)
            {

                if (BtnSave.Enabled)
                {
                    if (mIsKPIUseTotalBobot) ComputeBobot();
                    Sm.GrdRemoveRow(Grd1, e, BtnSave);
                }
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);                
            }
            if (mIsKPIUseTotalBobot)
                ComputeBobot();
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
            
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4)
            {
                SetLueOption(ref LueOption);
                LueRequestEdit(Grd1, LueOption, ref fCell, ref fAccept, e);
            }

            if (e.ColIndex == 19)
            {
                SetLueJobDesc(ref LueJobDesc);
                LueRequestEdit(Grd1, LueJobDesc, ref fCell, ref fAccept, e);
            }

            if (e.ColIndex == 18)
            {
                SetLueType(ref LueType);
                LueRequestEdit(Grd1, LueType, ref fCell, ref fAccept, e);
            }

            if (e.ColIndex == 25)
            {
                Sl.SetLueOption(ref LueMaxMin, "KPIMaxMin");
                LueRequestEdit(Grd1, LueMaxMin, ref fCell, ref fAccept, e);
            }

            if (e.ColIndex == 2 && mIsKPIUseTotalBobot)
            {
                ComputeBobot();
            }

            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 24 });
                
                //Sm.SetGrdNumValueZero(ref Grd1, e.RowIndex + 1, new int[] { 2, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "KPI", "TblKPIHdr");
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SaveKPIHdr(DocNo));


            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveKPIDtl(DocNo, Row));
            }

            Sm.ExecCommands(cml);
            ShowData(DocNo, 0);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsTxtEmpty(TxtKPIName, "KPI Name", false) ||
                Sm.IsTxtEmpty(TxtPICCode, "PIC ", false) ||
                Sm.IsLueEmpty(LuePosCode, "Responsibility") ||
                //IsDocNoAlreadyProcess()||
                IsGrdEmpty() ||
                (mIsKPIProcessUseSequence && IsGrdValueNotValid()) ||
                IsResultNameDuplicate() ||
                IsValidateSite();

        }

        private bool IsResultNameDuplicate()
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    for (int j = i + 1; j < Grd1.Rows.Count; j++)
                    {
                        if (Sm.GetGrdStr(Grd1, j, 1).Length > 0)
                        {
                            if (Sm.GetGrdStr(Grd1, i, 1) == Sm.GetGrdStr(Grd1, j, 1))
                            {
                                Sm.StdMsg(mMsgType.Warning, "Duplicate result indicator found at row #" + (j + 1) + " : " + Sm.GetGrdStr(Grd1, j, 1));
                                Sm.FocusGrd(Grd1, j, 1);
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        private bool IsDocNoAlreadyProcess()
        {
            var ActInd = ChkActiveInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select KPIDocNo From TblKPIProcessHdr Where KPIDocNo=@DocNo And CancelInd='N'"
            };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already Process.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 city.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 24, false, "Sequence is empty.")) return true;
                else
                {
                    for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                    {
                        if (Row != row)
                        {
                            if (Sm.GetGrdDec(Grd1, Row, 24) == Sm.GetGrdDec(Grd1, row, 24))
                            {
                                Sm.StdMsg(mMsgType.Warning, "Cannot use same sequence number for the same item!");
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        private bool IsValidateSite()
        {
            if (mIsFilterBySite && Sm.IsLueEmpty(LueSiteCode, "Site"))
            {
                return true;
            }
            return false;
        }

        private MySqlCommand SaveKPIHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblKPIHdr(DocNo, DocDt, ActInd, Yr, KPIName, PICCode, KPIParent, PosCode, SiteCode, Status, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @ActInd, @Yr, @KPIName, @PICCode, @KPIParent, @PosCode, @SiteCode, 'O', @Remark, @UserCode, CurrentDateTime()) ");
            //region bisa edit hdr dan detail
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update DocDt=@DocDt, ActInd=@ActInd, SiteCode=@SiteCode, KPIName=@KPIName, PICCode=@PICCode, KPIParent=@KPIParent, PosCode=@PosCode, Status='O', Remark=@Remark, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            SQL.AppendLine("Delete From TblKPIDtl Where DocNo = @DocNo ; ");

            //Document Approval
            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='KPI' ");
            SQL.AppendLine("And (T.StartAmt=0.00 Or T.StartAmt<=@Amt); ");

            SQL.AppendLine("Update TblKPIHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='KPI' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@KPIName", TxtKPIName.Text);
            Sm.CmParam<String>(ref cm, "@PICCode", TxtPICCode.Text);
            Sm.CmParam<String>(ref cm, "@KPIParent", Sm.GetLue(LueParentKPI));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveKPIDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblKPIDtl(DocNo, DNo,  ");
            if (mIsKPIProcessUseSequence)
                SQL.AppendLine("Sequence, ");
            SQL.AppendLine("Result, bobot, uom, option, target, mth1, ");
            if (misKPIUseMaxMin)
                SQL.AppendLine("MaxMin, ");
            SQL.AppendLine("mth2, mth3, mth4, mth5, mth6, mth7, mth8, mth9, mth10, mth11, mth12, type, JobDesc, KPIPerspectiveDocNo, KPIPerspectiveDNo, KPIPerspectiveTblDtl, ControlInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, ");
            if (mIsKPIProcessUseSequence)
                SQL.AppendLine("@Sequence, ");
            SQL.AppendLine("@Result, @bobot, @uom, @option, @target, @mth1, ");
            if (misKPIUseMaxMin)
                SQL.AppendLine("@MaxMin, ");
            SQL.AppendLine("@mth2, @mth3, @mth4, @mth5, @mth6, @mth7, @mth8, @mth9, @mth10, @mth11, @mth12, @type, @JobDesc, @KPIPerspectiveDocNo, @KPIPerspectiveDNo, @KPIPerspectiveTblDtl, @ControlInd, @CreateBy, CurrentDateTime()); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            //Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0).Length > 0 ? Sm.GetGrdStr(Grd1, Row, 0) : Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Result", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@bobot", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@uom", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@option", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@target", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@mth1", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@mth2", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@mth3", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@mth4", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@mth5", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@mth6", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@mth7", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@mth8", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@mth9", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@mth10", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@mth11", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@mth12", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<String>(ref cm, "@type", Sm.GetGrdStr(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@JobDesc", Sm.GetGrdStr(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@KPIPerspectiveDocNo", Sm.GetGrdStr(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@KPIPerspectiveDNo", Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@KPIPerspectiveTblDtl", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@ControlInd", Sm.GetGrdBool(Grd1, Row, 23) ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@Sequence", Sm.GetGrdDec(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@MaxMin", Sm.GetGrdStr(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (IsActIndDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(EditKPIActive(TxtDocNo.Text));
            if (ChkActiveInd.Checked)
            {
                cml.Add(DeleteKPIDtl(TxtDocNo.Text));

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveKPIDtl(TxtDocNo.Text, Row));
                }
            }

            Sm.ExecCommands(cml);
            ShowData(TxtDocNo.Text, 0);
        }

        private bool IsActIndDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document Number", false) ||
                IsActIndEditedAlready();
        }

        private bool IsActIndEditedAlready()
        {
            var ActInd = ChkActiveInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblKPIHdr Where DocNo=@DocNo And ActInd='N' "
            };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ActInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already non-active.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditKPIActive(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblKPIHdr Set ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeleteKPIDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblKPIDtl Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo, int SourceNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowKPIHdr(DocNo, SourceNo);
                ShowKPIDtl(DocNo);
                if (SourceNo != 0) Sm.SetDteCurrentDate(DteDocDt);
                if (mIsKPIUseTotalBobot) ComputeBobot();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                if (SourceNo == 0)
                {
                    SetFormControl(mState.View);
                }
                else
                {
                    SetFormControl(mState.Insert);
                }
                Cursor.Current = Cursors.Default;

            }
        }

        private void ShowKPIHdr(string DocNo, int SourceNO)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.ActInd, A.Yr, A.KPIName, A.PICCode,  B.EmpName, A.KPIParent,  A.PosCode, A.SiteCode,  A.Remark, " +
                    "Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc " +
                    "From TblKPIHdr  A " +
                    "Inner Join TblEmployee B On A.PICCode=B.EmpCode " +
                    "Where A.DocNo=@DocNo",
                    new string[]
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "ActInd", "Yr", "KPIName", "PICCode",   
                        
                        //6-10
                        "EmpName", "KPIParent", "PosCode", "SiteCode", "Remark",

                        //11
                        "StatusDesc"

                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (SourceNO == 0)
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        }
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[3]));
                        TxtKPIName.EditValue = Sm.DrStr(dr, c[4]);
                        TxtPICCode.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPICName.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetLue(LueParentKPI, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[8]));
                        Sl.SetLueSiteCode2(ref LueSiteCode, Sm.DrStr(dr, c[9]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[11]);
                    }, true
                );
        }

        private void ShowKPIDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DNo, A.Result, A.bobot, A.uom, A.option, A.target, A.mth1, ");
            if (misKPIUseMaxMin)
                SQL.AppendLine("B.OptDesc As MaxMin, ");
            else
                SQL.AppendLine("Null As MaxMin, ");
            SQL.AppendLine("A.mth2, A.mth3, A.mth4, A.mth5, A.mth6, A.mth7, A.mth8, A.mth9, A.mth10, A.mth11, A.mth12, A.type, ");
            SQL.AppendLine("A.JobDesc, A.KPIPerspectiveDocNo, A.KPIPerspectiveDNo, A.KPIPerspectiveTblDtl, A.ControlInd, ");
            if (mIsKPIProcessUseSequence)
                SQL.AppendLine("A.Sequence ");
            else
                SQL.AppendLine("Null As Sequence ");
            SQL.AppendLine("From TblKPIDtl A ");
            if (misKPIUseMaxMin)
                SQL.AppendLine("Left Join TblOption B on A.MaxMin = B.OptCode AND B.OptCat = 'KPIMaxMin' ");
            SQL.AppendLine("Where DocNo=@DocNo Order By DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "Result", "bobot", "uom", "option", "target",
                    //6-10
                    "mth1", "mth2", "mth3", "mth4", "mth5", 
                    //11-15
                    "mth6", "mth7", "mth8", "mth9", "mth10", 
                    //16-20
                    "mth11", "mth12", "type", "JobDesc", "KPIPerspectiveDocNo", 
                    //21-24
                    "KPIPerspectiveDNo", "KPIPerspectiveTblDtl", "ControlInd", "Sequence", "MaxMin"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 14);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 15);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 16);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 17);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 18);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 19);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 20);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 21);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 22);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 23, 23);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 24, 24);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 25, 25);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 24 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowKPIJobDesc()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT Docno, DNo, '1' As DtlInd, Initiative ");
            SQL.AppendLine("FROM tblkpiperspectivedtl ");
            SQL.AppendLine("Where PosCode = @PosCode ");
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("SELECT Docno, DNo, '2' As DtlInd, Initiative ");
            SQL.AppendLine("FROM tblkpiperspectivedtl2 ");
            SQL.AppendLine("Where PosCode = @PosCode ");
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("SELECT Docno, DNo, '3' As DtlInd, Initiative ");
            SQL.AppendLine("FROM tblkpiperspectivedtl3 ");
            SQL.AppendLine("Where PosCode = @PosCode ");
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("SELECT Docno, DNo, '4' As DtlInd, Initiative ");
            SQL.AppendLine("FROM tblkpiperspectivedtl4 ");
            SQL.AppendLine("Where PosCode = @PosCode ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo", 
                    //1-5
                    "Dno", "DtlInd", "Initiative"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 3);
                    Grd1.Cells[Row, 2].Value = 0;
                    Grd1.Cells[Row, 5].Value = 0;
                    Grd1.Cells[Row, 6].Value = 0;
                    Grd1.Cells[Row, 7].Value = 0;
                    Grd1.Cells[Row, 8].Value = 0;
                    Grd1.Cells[Row, 9].Value = 0;
                    Grd1.Cells[Row, 10].Value = 0;
                    Grd1.Cells[Row, 11].Value = 0;
                    Grd1.Cells[Row, 12].Value = 0;
                    Grd1.Cells[Row, 13].Value = 0;
                    Grd1.Cells[Row, 14].Value = 0;
                    Grd1.Cells[Row, 15].Value = 0;
                    Grd1.Cells[Row, 16].Value = 0;
                    Grd1.Cells[Row, 17].Value = 0;
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
            mIsKPIProcessUseSequence = Sm.GetParameterBoo("IsKPIProcessUseSequence");
            misKPIUseMaxMin = Sm.GetParameterBoo("IsKPIUseMaxMin");
            mIsKPIUseTotalBobot = Sm.GetParameterBoo("IsKPIUseTotalBobot");
        }

        private void SetLueJobDesc(ref LookUpEdit Lue)
        {
            Sm.SetLue1(
                ref Lue,
                "SELECT B.Description As Col1 " +
                "FROM tblorganizationalstructure A " +
                "Inner Join tblorganizationalstructuredtl2 B On A.PosCode = B.PosCode " +
                "Where A.PosCode = '" + Sm.GetLue(LuePosCode) + "' ", "Job Desc");
        }

        private void SetLueOption(ref LookUpEdit Lue)
        {
            Sm.SetLue1(
                ref Lue,
                "Select Col1 From ( " +
                "Select 'Year' As Col1 " +
                "union all " +
                "Select 'Month' " +
                ")X ",
                "Name");
        }

        private void SetLueType(ref LookUpEdit Lue)
        {
            Sm.SetLue1(
                ref Lue,
                "Select Col1 From ( " +
                "Select 'SUM' As Col1 " +
                "union all " +
                "Select 'AVG' " +
                ")X ",
                "Name");
        }

        public static void SetLueKPIParentCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DocNo As Col1, KPIName As Col2 From TblKPIHdr Where ActInd = 'Y' Order By Col2;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueMaxMin(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='KPIMaxMin' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col2");
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void IsSequenceNotValid(int Row)
        {
            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                if (row != Row && Sm.GetGrdDec(Grd1, Row, 24) == Sm.GetGrdDec(Grd1, row, 24))
                {
                    Sm.StdMsg(mMsgType.Warning, "Cannot use same sequence number for the same item!");
                    Grd1.Cells[Row, 24].Value = null;
                    break;
                }
            }
        }

        
        internal void ComputeBobot()
        {
            decimal Bobot = 0m;

            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                        Bobot += Sm.GetGrdDec(Grd1, Row, 2);
                }
            }
            
                TxtTotalBobot.EditValue = Sm.FormatNum(Bobot, 0);
        }

        #endregion


        #endregion

        #region Event       

        private void LueOption_Leave(object sender, EventArgs e)
        {
            if (LueOption.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LueOption).Length == 0)
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                else
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = Sm.GetLue(LueOption);
            }
        }

        private void LueOption_Validated(object sender, EventArgs e)
        {
            LueOption.Visible = false;
        }

        private void LueOption_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOption, new Sm.RefreshLue1(SetLueOption));
        }


        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue1(SetLueType));
        }

        private void LueType_Leave(object sender, EventArgs e)
        {
            if (LueType.Visible && fAccept && fCell.ColIndex == 18)
            {
                if (Sm.GetLue(LueType).Length == 0)
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                else
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = Sm.GetLue(LueType);
            }
        }

        private void LueType_Validated(object sender, EventArgs e)
        {
            LueType.Visible = false;
        }

       
        private void BtnPIC_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmKPIDlg(this));
        }

        private void LueParentKPI_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueParentKPI, new Sm.RefreshLue1(SetLueKPIParentCode));
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
            if (Sm.GetLue(LuePosCode).Length > 0)
            {
                ShowKPIJobDesc();
                if (mIsKPIUseTotalBobot) ComputeBobot();
            }
        }

        private void LueJobDesc_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueJobDesc, new Sm.RefreshLue1(SetLueJobDesc));
        }

        private void LueJobDesc_Leave(object sender, EventArgs e)
        {
            if (LueJobDesc.Visible && fAccept && fCell.ColIndex == 19)
            {
                if (Sm.GetLue(LueJobDesc).Length == 0)
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                else
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = Sm.GetLue(LueJobDesc);
            }
            LueJobDesc.Visible = false;
        }

        private void BtnSourceKPI_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmKPIDlg2(this));
        }

        

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
        }

        private void LueMaxMin_Click(object sender, EventArgs e)
        {
            Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = Sm.GetLue(LueMaxMin);
        }

        private void LueMaxMin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMaxMin, new Sm.RefreshLue2(Sl.SetLueOption), "KPIMaxMin");
        }

        private void LueMaxMin_Leave(object sender, EventArgs e)
        {
            if (LueMaxMin.Visible && fAccept && fCell.ColIndex == 25)
            {
                if (Sm.GetLue(LueMaxMin).Length == 0)
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value =
                    Grd1.Cells[fCell.RowIndex, 26].Value  =  null;
                else
                    Grd1.Cells[fCell.RowIndex, 26].Value = Sm.GetLue(LueMaxMin);
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = LueMaxMin.GetColumnValue("Col2");
            }
        }

        private void LueMaxMin_Validated(object sender, EventArgs e)
        {
            LueMaxMin.Visible = false;
        }

        private void TxtTotalBobot_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTotalBobot, 0);
        }

        #endregion



    }
}
