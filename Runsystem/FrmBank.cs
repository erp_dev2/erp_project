﻿#region Update
/*
    19/12/2017 [WED] tambah field Swift Beneficiary Code
    09/02/2018 [WED] tambah kode Kliring dan RTGS
    25/11/2020 [DITA/PHT] tambah input : Address, City, dan Province di master Bank
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Drawing.Imaging;

using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmBank : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmBankFind FrmFind;

        #endregion

        #region Constructor

        public FrmBank(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetLueCityCode(ref LueCityCode, string.Empty, string.Empty);
            Sl.SetLueProvCode(ref LueProvCode);
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtBankCode, TxtBankName, TxtBankShNm, TxtSwiftBeneCode, TxtKliringCode, TxtRTGSCode,
                        LueProvCode, LueCityCode, MeeAddress
                    }, true);
                    TxtBankCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtBankCode, TxtBankName, TxtBankShNm, TxtSwiftBeneCode, TxtKliringCode, TxtRTGSCode,
                         LueProvCode, MeeAddress
                    }, false);
                    TxtBankCode.Focus();
                    break;
                case mState.Edit:
                   Sm.SetControlReadOnly(TxtBankCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtBankName,TxtBankShNm, TxtSwiftBeneCode, TxtKliringCode, TxtRTGSCode,
                        LueProvCode, MeeAddress
                    }, false);
                    TxtBankName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtBankCode, TxtBankName, TxtBankShNm, TxtSwiftBeneCode, TxtKliringCode, TxtRTGSCode,
                 LueProvCode, LueCityCode, MeeAddress
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBankFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtBankCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtBankCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblBank Where BankCode=@BankCode" };
                Sm.CmParam<String>(ref cm, "@BankCode", TxtBankCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblBank(BankCode, BankName, BankShNm, SwiftBeneCode, KliringCode, RTGSCode, ProvCode, CityCode, Address, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@BankCode, @BankName, @BankShNm, @SwiftBeneCode, @KliringCode, @RTGSCode, @ProvCode, @CityCode, @Address,  @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update BankName=@BankName, BankShNm=@BankShNm, SwiftBeneCode=@SwiftBeneCode, KliringCode=@KliringCode, RTGSCode=@RTGSCode, ProvCode=@ProvCode, CityCode=@CityCode, Address=@Address, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@BankCode", TxtBankCode.Text);
                Sm.CmParam<String>(ref cm, "@BankName", TxtBankName.Text);
                Sm.CmParam<String>(ref cm, "@BankShNm", TxtBankShNm.Text);
                Sm.CmParam<String>(ref cm, "@SwiftBeneCode", TxtSwiftBeneCode.Text);
                Sm.CmParam<String>(ref cm, "@KliringCode", TxtKliringCode.Text);
                Sm.CmParam<String>(ref cm, "@RTGSCode", TxtRTGSCode.Text);
                Sm.CmParam<String>(ref cm, "@ProvCode", Sm.GetLue(LueProvCode));
                Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCityCode));
                Sm.CmParam<String>(ref cm, "@Address", MeeAddress.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtBankCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string BankCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@BankCode", BankCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblBank Where BankCode = @BankCode",
                        new string[] 
                        {
                            "BankCode", 
                            "BankName","BankShNm", "SwiftBeneCode", "KliringCode", "RTGSCode",
                            "ProvCode", "CityCode", "Address"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtBankCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtBankName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtBankShNm.EditValue = Sm.DrStr(dr, c[2]);
                            TxtSwiftBeneCode.EditValue = Sm.DrStr(dr, c[3]);
                            TxtKliringCode.EditValue = Sm.DrStr(dr, c[4]);
                            TxtRTGSCode.EditValue = Sm.DrStr(dr, c[5]);
                            Sm.SetLue(LueProvCode, Sm.DrStr(dr, c[6]));
                            SetLueCityCode(ref LueCityCode, Sm.DrStr(dr, c[6]), Sm.DrStr(dr, c[7]));
                            MeeAddress.EditValue = Sm.DrStr(dr, c[8]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtBankCode, "Bank code", false) ||
                Sm.IsTxtEmpty(TxtBankName, "Bank Name", false) ||
                Sm.IsTxtEmpty(TxtBankShNm, "Bank Short Name", false) ||
                IsBankCodeExisted();
        }

        private bool IsBankCodeExisted()
        {
            if (!TxtBankCode.Properties.ReadOnly && Sm.IsDataExist("Select BankCode From TblBank Where BankCode ='" + TxtBankCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Bank code ( " + TxtBankCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }


        #endregion

        #region Additional Method

        public static void SetLueCityCode(ref LookUpEdit Lue, string ProvCode, string CityCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CityCode As Col1, A.CityName As Col2 ");
            SQL.AppendLine("From TblCity A ");
            if (CityCode.Length > 0)
            {
                SQL.AppendLine("Left Join TblProvince B On A.ProvCode=B.ProvCode ");
                SQL.AppendLine("Where A.CityCode=@CityCode ");
                SQL.AppendLine("Or A.CityCode In ( ");
                SQL.AppendLine("    Select T1.CityCode ");
                SQL.AppendLine("    From TblCity T1, TblProvince T2 ");
                SQL.AppendLine("    Where T1.ProvCode=T2.ProvCode ");
                SQL.AppendLine("    And T1.ProvCode=@ProvCode ");
                SQL.AppendLine(")  ");
                SQL.AppendLine("Order By A.CityName;");
            }
            else
            {
                SQL.AppendLine("Inner Join TblProvince B On A.ProvCode=B.ProvCode And A.ProvCode = @ProvCode ");
                SQL.AppendLine("Order By A.CityName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ProvCode", ProvCode);
            if (CityCode.Length > 0) Sm.CmParam<String>(ref cm, "@CityCode", CityCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (CityCode.Length > 0) Sm.SetLue(Lue, CityCode);
        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        private void TxtBankCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBankCode);
        }
        private void TxtBankName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBankName);
        }

        private void TxtBankShNm_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBankShNm);
        }

        private void TxtSwiftBeneCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSwiftBeneCode);
        }

        private void TxtKliringCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtKliringCode);
        }

        private void TxtRTGSCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtRTGSCode);
        }

        private void LueProvCode_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueProvCode).Length > 0) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCityCode}, false);
            else Sm.SetControlReadOnly(new List<DXE.BaseEdit> {LueCityCode}, true);
            Sm.RefreshLookUpEdit(LueProvCode, new Sm.RefreshLue1(Sl.SetLueProvCode));
            SetLueCityCode(ref LueCityCode, Sm.GetLue(LueProvCode), string.Empty);
        }

        private void LueCityCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue3(SetLueCityCode), Sm.GetLue(LueProvCode), string.Empty);
        }
        private void MeeAddress_Validated(object sender, EventArgs e)
        {
            Sm.MeeTrim(MeeAddress);
        }

        #endregion       
      
        #endregion


       
    }
}
