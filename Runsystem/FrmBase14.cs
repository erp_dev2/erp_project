﻿#region Update
/*
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
 */
#endregion

#region Namespace
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Syncfusion.Windows.Forms.Diagram.Controls;
using Syncfusion.Windows.Forms.Tools;
using Syncfusion.Windows.Forms.Diagram;

using Sm = RunSystem.StdMtd;
using Gv = RunSystem.GlobalVar;
#endregion

namespace RunSystem
{
    public partial class FrmBase14 : Form
    {
        #region Constructor

        public FrmBase14()
        {
            InitializeComponent();
        }
        #endregion

        #region Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {
            Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
            Sm.SetLue(LueFontSize, "9");
        }

        virtual protected void SetSQL()
        {
        }

        virtual protected string SetReportName()
        {
            return "";
        }

        virtual protected void ShowData()
        {

        }

        virtual protected void HideInfoInGrd()
        {

        }

        virtual protected void ExportToExcel()
        {

        }


        virtual protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {

        }


       
        #endregion

        #region Event

        #region Form Event

        private void FrmBase14_Load(object sender, EventArgs e)
        {
            FrmLoad(sender, e);
        }

        #endregion

        #region Button Event

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ShowData();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {

        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        #endregion

        #region Misc Control event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            LueFontSizeEditValueChanged(sender, e);
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        #endregion

        #region Diagram Event

    
        #endregion


        #endregion
    }
}
