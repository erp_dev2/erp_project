﻿#region Update
/*
    16/11/2020 [TKG/PHT] New Application
    24/11/2020 [TKG/PHT] hanya outstanding vr yg perlu diproses
    30/11/2020 [IBL/PHT] Bug: validasi IsGrdEmpty(). Grid row count < 1
    28/12/2020 [VIN/PHT] narik VR multi (CASBA)
    26/01/2021 [HAR/PHT] generate docno
    08/03/2021 [DITA/PHT] dapat menarik VR multi (switching cost center)
    08/03/2021 [DITA/PHT] jurnal untuk VC tipe multi switching cost center
    08/03/2021 [DITA/PHT] rombak jurnal untuk VC tipe CASBA
    08/03/2021 [DITA/PHT] rombak jurnal untuk VC tipe switching bank account
    15/03/2021 [WED/PHT] cost center ambil dari bank account nya
    16/03/2021 [DITA/PHT] penyesuaian coa untuk switching bank account -> ambil dari ayat silang
    18/03/2021 [DITA/PHT] bug saat menampilkan bacnkaccount Credit dan Debit belum sesuai
    25/03/2021 [WED/PHT] rumus journal switching bank account dirubah, melihat ke site nya Bank Account nya
    08/04/2021 [ICA/PHT] menyamakan Jornal seperti di Voucher untuk type CASBA
    29/04/2021 [IBL/PHT] narik CashType dari multi VR berdasarkan parameter IsVoucherUseCashType
    07/05/2021 [BRI/PHT] tambah remark
    31/05/2021 [DITA/PHT] Remark hasil import juga disimpan di remark detail journal transaction
    11/06/2021 [DITA/PHT] remark detail journal ambil dari voucher detail
    17/06/2021 [TKG/PHT] Berdasarkan parameter IsVoucherMultiUseProfitCenter, cost center difilter berdasarkan profit center dan cost center yg muncul adalah cost center yg tidak menjadi parent di cost center yg lain
    12/07/2021 [IQA/PHT] Memindah cash type di multi VC, yang awalnya di multi VR   
    16/07/2021 [TKG/PHT] ubah GetParameter(), tambah filter profit center, cost center, budget category, remark
    21/02/2022 [DITA/PHT] Voucher Switching Bank Account ada InterOffice berdasarkan parameter IsVoucherCreateInterOfficeJournal, VoucherInterOfficeJournalFormat
    24/02/2022 [DITA/PHT] Voucher Switching Cost Center ada InterOffice berdasarkan parameter IsVoucherCreateInterOfficeJournal, VoucherInterOfficeJournalFormat
    14/03/2022 [TKG/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    21/03/2022 [ISD/PHT] tambah field cash type group dengan parameter IsVoucherUseCashTypeGroup
    28/03/2022 [IBL/PHT] BUG: Cash Type Group tidak tersimpan di Voucher
    20/04/2022 [TKG/PHT] mempercepat proses save
    21/04/2022 [TKG/PHT] mempercepat proses save
    01/06/2022 [DITA/PHT] masih ada bug untuk debit credit jurnal RAK di switch cc dan bank acc
    09/08/2022 [BRI/PHT] Penyesuaian filter by group bank acc
    21/09/2022 [DITA/PHT] untuk tipe switch bank account saat cost center nya sama saat ini ga bentuk jurnal default
    10/01/2023 [WED/PHT] penomoran journal berdasarkan parameter JournalDocNoFormat
    13/01/2023 [WED/PHT] bug fix ketika parameter JournalDocNoFormat value nya default
    19/01/2023 [TKG/PHT] bug journal interoffice (nomor urut tidak berdasarkan profit center, dll)
    04/04/2023 [SET/PHT] menghilangkan validasi filter By Bank Account pada showdata()
    27/04/2024 [TKG/PHT] type Switching Cost Center, ubah proses journal interoffice seperti semula, tambah save voucher pasangannya.
    28/04/2024 [TKG/PHT] bug saat proses journal switching cost center tick all semua data, kombinasi saat tick satu persatu 
    28/04/2024 [TKG/PHT] hanya account type yang credit yg generate journal untuk switching cost center
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Globalization;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherMulti : RunSystem.FrmBase7
    {
        #region Field

        internal string mMenuCode = string.Empty, mDocNoFormat = string.Empty;
        private bool
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsFilterByDept = false,
            mIsFilterBySite = false,
            mIsFilterByEnt = false,
            mIsFilterByBankAccount = false,
            mIsAutoJournalActived = false,
            mIsVoucherUseCashType = false,
            mIsVoucherMultiUseProfitCenter = false,
            mIsAllProfitCenterSelected = false,
            mIsVoucherCreateInterOfficeJournal = false,
            IsCostCenterSwitchingBankDifferent = false,
            mIsVoucherUseCashTypeGroup = false;
        private string mMainCurCode = string.Empty, mVoucherInterOfficeJournalFormat = string.Empty,
            CCCodeBankAccount = string.Empty,
            CCCodeBankAccount2 = string.Empty,
            mDocType = string.Empty,
            mDefaultEntity = string.Empty,
            mJournalDocNoFormat = string.Empty
            ;
        private List<String> mlProfitCenter = null;

        #endregion

        #region Constructor

        public FrmVoucherMulti(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                SetGrd();
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                SetLueCashTypeCode(ref LueCashTypeCode, string.Empty, string.Empty);
                if (!mIsVoucherUseCashType) LblCashType.ForeColor = Color.Black;
                if (mIsVoucherMultiUseProfitCenter)
                {
                    panel3.Visible = true;
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                int ypoint = 22;
                if (mIsVoucherUseCashTypeGroup)
                {
                    LblCashTypeGroup.ForeColor = Color.Red;
                    SetLueCashTypeGroup(ref LueCashTypeGroup);
                }
                else
                {
                    LblCashTypeGroup.Visible = LueCashTypeGroup.Visible = false;
                    LblCashType.Top = LblCashType.Top - ypoint; LueCashTypeCode.Top = LueCashTypeCode.Top - ypoint;
                    panel2.Size = new Size(785, 96);
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            //mMainCurCode = Sm.GetParameter("MainCurCode");
            //mDocNoFormat = Sm.GetParameter("DocNoFormat");
            //mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            //mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            //mIsFilterByEnt = Sm.GetParameterBoo("IsFilterByEnt");
            //mIsFilterByBankAccount = Sm.GetParameterBoo("IsFilterByBankAccount");
            //mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            //mIsVoucherUseCashType = Sm.GetParameterBoo("IsVoucherUseCashType");
            //mIsVoucherMultiUseProfitCenter = Sm.GetParameterBoo("IsVoucherMultiUseProfitCenter");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'MainCurCode', 'DocNoFormat', 'IsFilterByDept', 'IsFilterBySite', 'IsFilterByEnt', ");
            SQL.AppendLine("'IsFilterByBankAccount', 'IsAutoJournalActived', 'IsVoucherUseCashType', 'IsVoucherMultiUseProfitCenter', 'IsVoucherCreateInterOfficeJournal', ");
            SQL.AppendLine("'VoucherInterOfficeJournalFormat', 'IsClosingJournalBasedOnMultiProfitCenter', 'IsVoucherUseCashTypeGroup', 'DefaultEntity', 'JournalDocNoFormat' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsFilterByEnt": mIsFilterByEnt = ParValue == "Y"; break;
                            case "IsFilterByBankAccount": mIsFilterByBankAccount = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsVoucherUseCashType": mIsVoucherUseCashType = ParValue == "Y"; break;
                            case "IsVoucherMultiUseProfitCenter": mIsVoucherMultiUseProfitCenter = ParValue == "Y"; break;
                            case "IsVoucherCreateInterOfficeJournal": mIsVoucherCreateInterOfficeJournal = ParValue == "Y"; break;
                            case "IsVoucherUseCashTypeGroup": mIsVoucherUseCashTypeGroup = ParValue == "Y"; break;

                            //string
                            case "DefaultEntity": mDefaultEntity = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "DocNoFormat": mDocNoFormat = ParValue; break;
                            case "VoucherInterOfficeJournalFormat":
                                mVoucherInterOfficeJournalFormat = ParValue;
                                if (mVoucherInterOfficeJournalFormat.Length == 0) mVoucherInterOfficeJournalFormat = "1";
                                break;
                            case "JournalDocNoFormat": mJournalDocNoFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }

            if (mJournalDocNoFormat.Length == 0) mJournalDocNoFormat = "1";
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Voucher Request#",
                        "",
                        "Date",
                        "Entity",
                        "Bank Account (C)",

                        //6-10
                        "Bank Account (D)",
                        "Currency",
                        "Amount",
                        "Remark",
                        "Voucher#",

                        //11-15
                        "Entity Code",
                        "Type",
                        "DocType",
                        "Budget Category Code",
                        "BankAcCode",

                        //16-20
                        "BankAcCode2",
                        "AcType",
                        "AcType2",
                        "PairedVRDocNo",
                        "ParentOfPairedInd",

                        //21-24
                        "BankAcCodeTemp",
                        "BankAcTp",
                        "code1",
                        "ProfitCenterCode"
                    },
                     new int[] 
                    {
                        //0
                        20, 
                        //1-5
                        150, 20, 100, 200, 300, 
                        //6-10
                        300, 80, 130, 200, 130,
                        //11-15
                        0, 200, 0, 0, 0,
                        //16-20
                        0, 100, 100, 100, 100,
                        //21-24
                        0,0,0,0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20,
                21, 22, 23, 24 }, false);
            Grd1.Cols[12].Move(4);
            Grd1.Cols[13].Move(5);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 10 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                SetProfitCenter();
                
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo, A.DocDt, B.EntName, ");
                SQL.AppendLine("If (A.AcType = 'C',  ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When E.BankName Is Not Null Then Concat(E.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("    Case When C.BankAcNo Is Not Null Then Concat(C.BankAcNo) Else IfNull(C.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When C.Remark Is Not Null Then Concat(' ', '(', C.Remark, ')') Else '' End ");
                SQL.AppendLine(")), ");
                SQL.AppendLine("If (A.AcType2 = 'C',  ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When F.BankName Is Not Null Then Concat(F.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("    Case When D.BankAcNo Is Not Null Then Concat(D.BankAcNo) Else IfNull(D.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When D.Remark Is Not Null Then Concat(' ', '(', D.Remark, ')') Else '' End ");
                SQL.AppendLine(")), '')) As BankAcName, ");

                SQL.AppendLine("If (A.AcType = 'D',  ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When E.BankName Is Not Null Then Concat(E.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("    Case When C.BankAcNo Is Not Null Then Concat(C.BankAcNo) Else IfNull(C.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When C.Remark Is Not Null Then Concat(' ', '(', C.Remark, ')') Else '' End ");
                SQL.AppendLine(")), ");
                SQL.AppendLine("If (A.AcType2 = 'D',  ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When F.BankName Is Not Null Then Concat(F.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("    Case When D.BankAcNo Is Not Null Then Concat(D.BankAcNo) Else IfNull(D.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When D.Remark Is Not Null Then Concat(' ', '(', D.Remark, ')') Else '' End ");
                SQL.AppendLine(")), '')) As BankAcName2, ");

                SQL.AppendLine("A.CurCode, A.Amt, A.Remark, A.EntCode, G.OptCode DocType, G.OptDesc Type, A.BCCode, A.BankAcCode, ");
                SQL.AppendLine("A.BankAcCode2, A.AcType, A.AcType2, A.PairedVRDocNo, H.BankAcCodeTemp, C.BankAcTp, I.ProfitCenterCode ");
                SQL.AppendLine("From TblVoucherRequestHdr A ");
                SQL.AppendLine("Left Join TblEntity B On A.EntCode=B.EntCode ");
                if (mIsVoucherMultiUseProfitCenter)
                {
                    SQL.AppendLine("Inner Join TblBankAccount C On A.BankAcCode=C.BankAcCode ");
                    SQL.AppendLine("    And (C.CCCode Is Null Or (C.CCCode Is Not Null And C.CCCode In ( ");
                    SQL.AppendLine("        Select CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And NotParentInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In (");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        )))) ");
                }
                else
                    SQL.AppendLine("Left Join TblBankAccount C On A.BankAcCode=C.BankAcCode ");
                SQL.AppendLine("Left Join TblBankAccount D On A.BankAcCode2=D.BankAcCode ");
                SQL.AppendLine("Left Join TblBank E On C.BankCode=E.BankCode ");
                SQL.AppendLine("Left Join TblBank F On D.BankCode=F.BankCode ");
                SQL.AppendLine("Left Join TblOption G On A.DocType=G.OptCode and G.OptCat ='VoucherDocType' ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select DocNo, BankAcCode BankAcCodeTemp ");
                SQL.AppendLine("    From TblVoucherRequestHdr ");
                SQL.AppendLine(")H On A.PairedVRDocNo = H.DocNo  ");
                SQL.AppendLine("Left Join TblCostCenter I On C.CCCode = I.CCCode ");

                SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("And A.VoucherDocNo Is Null ");
                SQL.AppendLine("And A.DocType in ('16', '70', 73) ");
                SQL.AppendLine("And A.MultiInd='Y' ");
                SQL.AppendLine("And A.CancelInd='N' ");
                SQL.AppendLine("And A.Status='A' ");
                if (mIsFilterByDept)
                {
                    SQL.AppendLine("And (A.DeptCode Is Null ");
                    SQL.AppendLine("    Or (A.DeptCode Is Not Null And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("        )))) ");
                }
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And (A.SiteCode Is Null ");
                    SQL.AppendLine("    Or (A.SiteCode Is Not Null And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupSite ");
                    SQL.AppendLine("        Where SiteCode=A.SiteCode ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("        )))) ");
                }
                if (mIsFilterByEnt)
                {
                    SQL.AppendLine("And (A.EntCode Is Null ");
                    SQL.AppendLine("    Or (A.EntCode Is Not Null And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupEntity ");
                    SQL.AppendLine("        Where EntCode=A.EntCode ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("        )))) ");
                }

                #region filter validasi By Bank Account
                /*if (mIsFilterByBankAccount)
                {
                    SQL.AppendLine("And (A.BankAcCode Is Null ");
                    SQL.AppendLine("    Or (A.BankAcCode Is Not Null And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    )))) ");

                    SQL.AppendLine("And (A.BankAcCode2 Is Null ");
                    SQL.AppendLine("    Or (A.BankAcCode2 Is Not Null And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=A.BankAcCode2 ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ))) Or Find_In_Set(A.DocType,'16,73') ");
                    SQL.AppendLine("    ) ");
                }*/
                #endregion

                if (TxtRemark.Text.Length > 0)
                {
                    SQL.AppendLine("And A.Remark Like @Remark ");
                    Sm.CmParam<String>(ref cm, "@Remark", "%" + TxtRemark.Text + "%");
                }
                if (mIsVoucherMultiUseProfitCenter)
                {
                    SQL.AppendLine("And A.DeptCode Is Not Null ");
                    SQL.AppendLine("And A.DeptCode In ( ");

                    SQL.AppendLine("    Select Distinct X3.DeptCode ");
                    SQL.AppendLine("    From TblUser X1 ");
                    SQL.AppendLine("    Inner Join TblGroupProfitCenter X2 On X1.GrpCode=X2.GrpCode ");

                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter = string.Empty;
                        int i = 0;

                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " (X2.ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                            i++;
                        }
                        if (Filter.Length == 0)
                            SQL.AppendLine("    And 1=0 ");
                        else
                            SQL.AppendLine("    And (" + Filter + ") ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("        And Find_In_Set(X2.ProfitCenterCode, @ProfitCenterCode) ");
                            if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                        }
                    }
                    SQL.AppendLine("Inner Join TblCostCenter X3 On X2.ProfitCenterCode=X3.ProfitCenterCode And X3.DeptCode Is Not Null ");
                    if (TxtCCCode.Text.Length > 0)
                    {
                        SQL.AppendLine("    And X3.CCName Like @CCCode ");
                        Sm.CmParam<String>(ref cm, "@CCCode", "%" + TxtCCCode.Text + "%");
                    }
                    if (TxtBCCode.Text.Length > 0)
                    {
                        SQL.AppendLine("Inner Join TblBudgetCategory X4 On X3.CCCode=X4.CCCode And X4.BCName Like @BCCode ");
                        Sm.CmParam<String>(ref cm, "@BCCode", "%" + TxtCCCode.Text + "%");
                    }
                    SQL.AppendLine("Where X1.UserCode=@UserCode ");

                    SQL.AppendLine("    ) ");
                }

                SQL.AppendLine("Order By A.DocDt, A.DocNo; ");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[]
                    {
                        //0
                        "DocNo",
                            
                        //1-5
                        "DocDt", "EntName", "BankAcName", "BankAcName2", "CurCode", 
                            
                        //6-10
                        "Amt", "Remark", "EntCode", "Type", "DocType",

                        //11-15
                        "BCCode", "BankAcCode", "BankAcCode2", "AcType", "AcType2", 

                        //16-19
                        "PairedVRDocNo", "BankAcCodeTemp", "BankAcTp", "ProfitCenterCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Grd1.Cells[Row, 10].Value = null;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                        Grd.Cells[Row, 23].Value = Sm.GetCode1ForJournalDocNo("FrmVoucher", Sm.DrStr(dr, c[18]), Sm.DrStr(dr, c[14]), mJournalDocNoFormat);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 19);
                        Grd1.Cells[Row, 20].Value = "N";
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private string GetJournalSQL(string DocType, int VoucherIndex, bool IsMainCurCode)
        {
            //16 : Switching Bank Account
            //70 : Cash Advance Switching Bank Account
            //73 : Switching Cost Center

            var SQL = string.Empty;
            switch (DocType)
            {
                case "16":
                    SQL = GetJournalSQL16(VoucherIndex);
                    break;
                case "70":
                    SQL = GetJournalSQL70(VoucherIndex, IsMainCurCode);
                    break;
                case "73":
                    SQL = GetJournalSQL73(VoucherIndex);
                    break;
            }
            return SQL;
        }

        #region New Code
        override protected void SaveData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                    IsSaveDataNotValid()) return;

                //16 : Switching Bank Account
                //70 : Cash Advance Switching Bank Account
                //73 : Switching Cost Center

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                var cml = new List<MySqlCommand>();
                string
                    DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8),
                    YrMth = Sm.Left(Sm.GetDte(DteDocDt), 6),
                    Yr = YrMth.Substring(2, 2),
                    Mth = YrMth.Substring(4, 2),
                    BankAcCodeCr = string.Empty, 
                    BankAcCodeDb = string.Empty,
                    SQLForNewVoucherDocNo = string.Empty,
                    SQLForNewJournalDocNo = string.Empty,
                    SQLForNewJournalDocNo2 = string.Empty;
                decimal Amt = 0m;
                int VoucherIndex = 0, JournalIndex = 0;
                bool
                    //IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled"),
                    IsMonthlyClosingValidationEnabled = Sm.IsDataExist(
                        "Select 1 From TblClosingJournal " +
                        "Where CancelInd='N' And @Param<=ClosingDt " +
                        "And Exists(Select 1 From TblParameter Where ParCode = 'IsClosingJournalActived' And ParValue='Y') Limit 1;", DocDt);

                SQLForNewVoucherDocNo = Sm.GetNewVoucherDocNo(DocDt, "Voucher", "1");

                if (mDocNoFormat == "1")
                {
                    SQLForNewJournalDocNo = Sm.GetNewJournalDocNo(DocDt, 1);
                    SQLForNewJournalDocNo2 = Sm.GetNewJournalDocNo(DocDt, 2);
                }
                
                var l1 = new List<FrmJournalInterOffice.CC1>();
                var l2 = new List<FrmJournalInterOffice.CC2>();
                var l3 = new List<FrmJournalInterOffice.CCSeq>();
                var l4 = new List<SwitchingCC>();
                var l5 = new List<SwitchingBankAc>();

                Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
                Sm.CmParam<String>(ref cm, "@EntCode", mDefaultEntity);
                Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
                Sm.CmParam<String>(ref cm, "@CashTypeCode", Sm.GetLue(LueCashTypeCode));
                Sm.CmParam<String>(ref cm, "@CashTypeGrpCode", Sm.GetLue(LueCashTypeGroup));
                
                SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");
                SQL.AppendLine("Set @Row:=0; ");

                string DocTypeDesc = string.Empty;
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdBool(Grd1, r, 0))
                    {
                        var code1 = Sm.GetGrdStr(Grd1, r, 23);
                        var code2 = mJournalDocNoFormat == "1" ? string.Empty : string.Concat(Sm.Left(code1, 1), Sm.Right(code1, 1) == "K" ? "M" : "K");
                        var profitCenterCode = Sm.GetGrdStr(Grd1, r, 24);

                        VoucherIndex++;
                        mDocType = Sm.GetGrdStr(Grd1, r, 13);
                        DocTypeDesc = string.Empty;
                        if (mJournalDocNoFormat == "1")
                        {
                            if (mDocNoFormat != "1")
                            {
                                SQLForNewJournalDocNo = Sm.GetNewJournalDocNo3(DocDt, Sm.GetGrdStr(Grd1, r, 11), "1");
                                SQLForNewJournalDocNo2 = Sm.GetNewJournalDocNo3(DocDt, Sm.GetGrdStr(Grd1, r, 11), "2");
                            }
                        }
                        else
                        {
                            SQLForNewJournalDocNo = Sm.GetNewJournalDocNoWithAddCodes(DocDt, 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty);
                            SQLForNewJournalDocNo2 = Sm.GetNewJournalDocNoWithAddCodes(DocDt, 2, code1, profitCenterCode, string.Empty, string.Empty, string.Empty);
                        }
                        l1.Clear();
                        l2.Clear();
                        l3.Clear();
                        l4.Clear();
                        l5.Clear();

                        switch (mDocType)
                        {
                            case "16":
                                DocTypeDesc = "Switching Bank Account";
                                break;
                            case "70":
                                DocTypeDesc = "Cash Advance Switching Bank Account";
                                break;
                            case "73":
                                DocTypeDesc = "Switching Cost Center";
                                break;
                        }

                        if (mDocType == "16") 
                            IsCostCenterSwitchingBankDifferent = GetCostCenterSwitchingBankAccount(Sm.GetGrdStr(Grd1, r, 15), Sm.GetGrdStr(Grd1, r, 16));

                        #region Prepare Data + validation

                        if (mIsAutoJournalActived)
                        {
                            // Journal InterOffice untuk 16+73
                            if (mDocType == "73" &&
                                mIsVoucherCreateInterOfficeJournal &&
                                mVoucherInterOfficeJournalFormat == "2" && 
                                Sm.GetGrdStr(Grd1, r, 20) == "Y"
                                )
                            {
                                // InterOffice by Cost Center

                                CCCodeBankAccount = GetCCCodeBankAccount(Sm.GetGrdStr(Grd1, r, 15));
                                CCCodeBankAccount2 = GetCCCodeBankAccount(Sm.GetGrdStr(Grd1, r, 21));

                                FrmJournalInterOffice.PrepC1(ref l1, CCCodeBankAccount);
                                FrmJournalInterOffice.PrepC2(ref l2, CCCodeBankAccount2);
                                FrmJournalInterOffice.PrepSeq(ref l1, ref l2, ref l3);

                                if (l3.Count > 0)
                                {
                                    if (mIsClosingJournalBasedOnMultiProfitCenter && IsMonthlyClosingValidationEnabled)
                                    {
                                        var lProfitCenterCode73 = new List<string>();
                                        GetProfitCenterCode(ref l3, ref lProfitCenterCode73);
                                        foreach (var x in lProfitCenterCode73)
                                        {
                                            if (Sm.IsClosingJournalInvalid(true, false, DocDt, x))
                                            {
                                                cml.Clear();
                                                return;
                                            }
                                        }
                                        lProfitCenterCode73.Clear();
                                    }
                                }
                            }

                            if (mDocType == "16" && IsCostCenterSwitchingBankDifferent)
                            {
                                // InterOffice by Cost Center

                                FrmJournalInterOffice.PrepC1(ref l1, CCCodeBankAccount);
                                FrmJournalInterOffice.PrepC2(ref l2, CCCodeBankAccount2);
                                FrmJournalInterOffice.PrepSeq(ref l1, ref l2, ref l3);

                                if (l3.Count > 0)
                                {
                                    if (mIsClosingJournalBasedOnMultiProfitCenter && IsMonthlyClosingValidationEnabled)
                                    {
                                        var lProfitCenterCode16 = new List<string>();
                                        GetProfitCenterCode(ref l3, ref lProfitCenterCode16);
                                        foreach (var x in lProfitCenterCode16)
                                        {
                                            if (Sm.IsClosingJournalInvalid(true, false, DocDt, x))
                                            {
                                                cml.Clear();
                                                return;
                                            }
                                        }
                                        lProfitCenterCode16.Clear();
                                    }
                                }
                            }

                            if (!mIsVoucherCreateInterOfficeJournal ||
                                (mIsVoucherCreateInterOfficeJournal && mDocType == "70"))
                            {
                                if (mIsClosingJournalBasedOnMultiProfitCenter &&
                                    mIsVoucherCreateInterOfficeJournal &&
                                    IsMonthlyClosingValidationEnabled &&
                                    mDocType == "70")
                                {
                                    if (Sm.IsClosingJournalInvalid(true, false, DocDt, GetProfitCenterCode(r)))
                                    {
                                        cml.Clear();
                                        return;
                                    }
                                }
                            }
                        }

                        #endregion

                        if (mJournalDocNoFormat == "2")
                            SQLForNewVoucherDocNo = Sm.GetNewVoucherDocNoWithAddCodes(DocDt, 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty);

                        SaveVoucher(ref cm, ref SQL, VoucherIndex, r, SQLForNewVoucherDocNo);

                        if (mIsAutoJournalActived)
                        {
                            // Journal InterOffice untuk 16+73
                            if (mDocType == "73" && 
                                mIsVoucherCreateInterOfficeJournal && 
                                mVoucherInterOfficeJournalFormat == "2" && 
                                Sm.GetGrdStr(Grd1, r, 20) == "Y"
                                )
                            {
                                // InterOffice by Cost Center

                                if (l3.Count > 0)
                                {
                                    BankAcCodeCr = Sm.GetGrdStr(Grd1, r, 17) == "C" ? Sm.GetGrdStr(Grd1, r, 15) : Sm.GetGrdStr(Grd1, r, 21);
                                    BankAcCodeDb = Sm.GetGrdStr(Grd1, r, 17) == "C" ? Sm.GetGrdStr(Grd1, r, 21) : Sm.GetGrdStr(Grd1, r, 15);
                                    Amt = Sm.GetGrdDec(Grd1, r, 8);

                                    l4.Clear();
                                    for (int i = 0; i < l3.Count; ++i)
                                    {
                                        var code3 = code1;
                                        var profitCenterCode1 = mJournalDocNoFormat == "2" ? Sm.GetValue("Select ProfitCenterCode From TblCostCenter Where CCCode = @Param ", l3[i].CCCode) : "";
                                        if (i == 0 || i == l3.Count - 1)
                                        {
                                            JournalIndex += 1;
                                            if (i != 0) code3 = code2;
                                            else code3 = code1;

                                            if (mJournalDocNoFormat == "2")
                                            {
                                                // TKG 1
                                                // SQLForNewJournalDocNo = Sm.GetNewJournalDocNoWithAddCodes(DocDt, JournalIndex, code3, profitCenterCode1, string.Empty, string.Empty, string.Empty);
                                                SQLForNewJournalDocNo = Sm.GetNewJournalDocNoWithAddCodes(DocDt, 1, code3, profitCenterCode1, string.Empty, string.Empty, string.Empty);
                                            }
                                                
                                            l4.Add(new SwitchingCC()
                                            {
                                                Journal = JournalIndex,
                                                Voucher = VoucherIndex
                                            });
                                            SaveJournalInterOfficeCostCenterSwitchingCC(
                                                    ref SQL, ref cm, VoucherIndex, JournalIndex, r, true, i, l3[i].CCCode, (i==0?BankAcCodeCr: BankAcCodeDb), Amt, SQLForNewJournalDocNo);
                                        }
                                        else
                                        {
                                            for (int j = 1; j <= 2; ++j)
                                            {
                                                JournalIndex += 1;
                                                if (JournalIndex % 2 == 0) code3 = code2;
                                                else code3 = code1;

                                                if (mJournalDocNoFormat == "2")
                                                {
                                                    // TKG 2
                                                    // SQLForNewJournalDocNo = Sm.GetNewJournalDocNoWithAddCodes(DocDt, JournalIndex, code3, profitCenterCode1, string.Empty, string.Empty, string.Empty);
                                                    SQLForNewJournalDocNo = Sm.GetNewJournalDocNoWithAddCodes(DocDt, 1, code3, profitCenterCode1, string.Empty, string.Empty, string.Empty);

                                                }

                                                l4.Add(new SwitchingCC()
                                                {
                                                    Journal = JournalIndex,
                                                    Voucher = VoucherIndex
                                                });
                                                SaveJournalInterOfficeCostCenterSwitchingCC(
                                                        ref SQL, ref cm, VoucherIndex, JournalIndex, r, false, j, l3[i].CCCode, BankAcCodeCr, Amt, SQLForNewJournalDocNo);
                                            }
                                        }
                                    }
                                }

                                if (l4.Count > 1)
                                    SaveVoucherDtl3(ref SQL, ref cm, mDocType, null, l4);
                            }

                            if (mDocType == "16" && IsCostCenterSwitchingBankDifferent)
                            {
                                BankAcCodeCr = Sm.GetGrdStr(Grd1, r, 17) == "C" ? Sm.GetGrdStr(Grd1, r, 15) : Sm.GetGrdStr(Grd1, r, 16);
                                BankAcCodeDb = Sm.GetGrdStr(Grd1, r, 17) == "C" ? Sm.GetGrdStr(Grd1, r, 16) : Sm.GetGrdStr(Grd1, r, 15);
                                Amt = Sm.GetGrdDec(Grd1, r, 8);

                                l5.Clear();

                                for (int i = 0; i < l3.Count; ++i)
                                {
                                    var code3 = code1;
                                    var profitCenterCode1 = mJournalDocNoFormat == "2" ? Sm.GetValue("Select ProfitCenterCode From TblCostCenter Where CCCode = @Param ", l3[i].CCCode) : "";

                                    if (i == 0 || i == l3.Count - 1)
                                    {
                                        JournalIndex += 1;
                                        if (i != 0) code3 = code2;
                                        else code3 = code1;

                                        if (mJournalDocNoFormat == "2")
                                        {
                                            // TKG
                                            // SQLForNewJournalDocNo = Sm.GetNewJournalDocNoWithAddCodes(DocDt, JournalIndex, code3, profitCenterCode1, string.Empty, string.Empty, string.Empty);
                                            SQLForNewJournalDocNo = Sm.GetNewJournalDocNoWithAddCodes(DocDt, 1, code3, profitCenterCode1, string.Empty, string.Empty, string.Empty);
                                        }

                                        l5.Add(new SwitchingBankAc()
                                        {
                                            Journal = JournalIndex,
                                            Voucher = VoucherIndex
                                        });

                                        SaveJournalInterOfficeCostCenterSwitchingBank(
                                            ref SQL, ref cm, VoucherIndex, JournalIndex, r, true, i, l3[i].CCCode, (i == 0 ? BankAcCodeCr : BankAcCodeDb), Amt, SQLForNewJournalDocNo);
                                    }
                                    else
                                    {
                                        for (int j = 1; j <= 2; ++j)
                                        {
                                            JournalIndex += 1;

                                            if (JournalIndex % 2 == 0) code3 = code2;
                                            else code3 = code1;

                                            if (mJournalDocNoFormat == "2")
                                            {
                                                // SQLForNewJournalDocNo = Sm.GetNewJournalDocNoWithAddCodes(DocDt, JournalIndex, code3, profitCenterCode1, string.Empty, string.Empty, string.Empty);
                                                SQLForNewJournalDocNo = Sm.GetNewJournalDocNoWithAddCodes(DocDt, 1, code3, profitCenterCode1, string.Empty, string.Empty, string.Empty);
                                            }

                                            l5.Add(new SwitchingBankAc()
                                            {
                                                Journal = JournalIndex,
                                                Voucher = VoucherIndex
                                            });
                                            SaveJournalInterOfficeCostCenterSwitchingBank(
                                                ref SQL, ref cm, VoucherIndex, JournalIndex, r, false, j, l3[i].CCCode, BankAcCodeCr, Amt, SQLForNewJournalDocNo);
                                        }
                                    }
                                }
                            }

                            if (l5.Count > 1)
                                SaveVoucherDtl3(ref SQL, ref cm, mDocType, l5, null);
                        }

                        if(!mIsVoucherCreateInterOfficeJournal || 
                            (mIsVoucherCreateInterOfficeJournal && mDocType == "70") ||
                            (mIsVoucherCreateInterOfficeJournal && mDocType == "16" && !IsCostCenterSwitchingBankDifferent)
                            )
                        {
                            // Journal Standard

                            JournalIndex += 1;

                            SQL.AppendLine("Update TblVoucherHdr Set ");
                            SQL.AppendLine("    JournalDocNo=");
                            SQL.AppendLine(SQLForNewJournalDocNo);
                            //if (mDocNoFormat == "1")
                            //    SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), JournalIndex));
                            //else
                            //    SQL.AppendLine(Sm.GetNewJournalDocNo3(Sm.GetDte(DteDocDt), Sm.GetGrdStr(Grd1, r, 11), JournalIndex.ToString()));
                            if (mDocType == "16")
                            {
                                JournalIndex += 1;

                                SQL.AppendLine("    ,JournalDocNo3=");
                                SQL.AppendLine(SQLForNewJournalDocNo2);
                                //if (mDocNoFormat == "1")
                                //    SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), JournalIndex));
                                //else
                                //    SQL.AppendLine(Sm.GetNewJournalDocNo3(Sm.GetDte(DteDocDt), Sm.GetGrdStr(Grd1, r, 11), JournalIndex.ToString()));
                            }
                            SQL.AppendLine("Where DocNo=@VoucherDocNo_" + VoucherIndex.ToString() + ";");

                            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
                            SQL.AppendLine("Select A.JournalDocNo, A.DocDt, ");
                            SQL.AppendLine("Concat('Voucher (', IfNull( '" + DocTypeDesc + "' , 'None'), ') : ', A.DocNo) As JnDesc, ");
                            SQL.AppendLine("@MenuCode As MenuCode, ");
                            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
                            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
                            SQL.AppendLine("From TblVoucherHdr A ");
                            SQL.AppendLine("Left Join TblBankAccount B On A.BankAcCode = B.BankAcCode ");
                            SQL.AppendLine("Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString() + " And A.JournalDocNo Is Not Null; ");

                            SQL.AppendLine("Set @Row=0; ");

                            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
                            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                            SQL.AppendLine("B.AcNo, B.DAmt, B.CAmt, ");
                            SQL.AppendLine("EntCode, D.Remark, A.CreateBy, A.CreateDt ");
                            SQL.AppendLine("From TblJournalHdr A ");
                            SQL.AppendLine("Inner Join ( ");
                            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

                            SQL.AppendLine(GetJournalSQL(mDocType, VoucherIndex, Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 7), mMainCurCode)));

                            SQL.AppendLine(") B On 1=1 ");
                            SQL.AppendLine("Inner Join tblVoucherHdr C On A.DocNo=C.JournalDocNo And C.JournalDocNo is Not Null And C.DocNo=@VoucherDocNo_" + VoucherIndex.ToString() + " ");
                            SQL.AppendLine("Inner Join tblVoucherDtl D On C.DocNo=D.DocNo ; ");


                            if (mDocType == "16")
                            {
                                SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
                                SQL.AppendLine("Select A.JournalDocNo3, A.DocDt, ");
                                SQL.AppendLine("Concat('Voucher (', IfNull('" + DocTypeDesc + " ', 'None'), ') : ', A.DocNo) As JnDesc, ");
                                SQL.AppendLine("@MenuCode As MenuCode, ");
                                SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
                                SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
                                SQL.AppendLine("From TblVoucherHdr A ");
                                SQL.AppendLine("Left Join TblBankAccount B On A.BankAcCode2 = B.BankAcCode ");
                                SQL.AppendLine("Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString() + " And A.JournalDocNo3 Is Not Null; ");

                                SQL.AppendLine("Set @Row=0; ");

                                SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
                                SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                                SQL.AppendLine("B.AcNo, B.DAmt, B.CAmt, ");
                                SQL.AppendLine("EntCode, D.Remark, A.CreateBy, A.CreateDt ");
                                SQL.AppendLine("From TblJournalHdr A ");
                                SQL.AppendLine("Inner Join ( ");
                                SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From ( ");

                                #region Site Bank Account sama

                                SQL.AppendLine("        Select B.ParValue As AcNo, ");
                                SQL.AppendLine("        A.Amt As DAmt, ");
                                SQL.AppendLine("        0.00 As CAmt ");
                                SQL.AppendLine("        From TblVoucherHdr A ");
                                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForCrossAccount' And B.ParValue Is Not Null ");
                                // tambahan wed
                                SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
                                SQL.AppendLine("        Inner Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
                                // end tambahan wed
                                SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                                SQL.AppendLine("        And A.AcType2='C' ");
                                // tambahan wed
                                SQL.AppendLine("        And C.SiteCode = D.SiteCode ");
                                // end tambahan wed
                                SQL.AppendLine("    Union All ");
                                SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
                                SQL.AppendLine("        0.00 As DAmt, ");
                                SQL.AppendLine("        A.Amt As CAmt ");
                                SQL.AppendLine("        From TblVoucherHdr A ");
                                SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
                                // tambahan wed
                                SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
                                // end tambahan wed
                                SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                                SQL.AppendLine("        And A.AcType2='C' ");
                                // tambahan wed
                                SQL.AppendLine("        And C.SiteCode = B.SiteCode ");
                                // end tambahan wed
                                SQL.AppendLine("    Union All ");
                                SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
                                SQL.AppendLine("        A.Amt As DAmt, ");
                                SQL.AppendLine("        0.00 As CAmt ");
                                SQL.AppendLine("        From TblVoucherHdr A ");
                                SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
                                // tambahan wed
                                SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
                                // end tambahan wed
                                SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                                SQL.AppendLine("        And A.AcType2='D' ");
                                // tambahan wed
                                SQL.AppendLine("        And C.SiteCode = B.SiteCode ");
                                // end tambahan wed
                                SQL.AppendLine("    Union All ");
                                SQL.AppendLine("        Select B.ParValue As AcNo, ");
                                SQL.AppendLine("        0.00 As DAmt, ");
                                SQL.AppendLine("        A.Amt As CAmt ");
                                SQL.AppendLine("        From TblVoucherHdr A ");
                                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForCrossAccount' And B.ParValue Is Not Null ");
                                // tambahan wed
                                SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
                                SQL.AppendLine("        Inner Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
                                // end tambahan wed
                                SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                                SQL.AppendLine("        And A.AcType2='D' ");
                                // tambahan wed
                                SQL.AppendLine("        And C.SiteCode = D.SiteCode ");
                                // end tambahan wed

                                #endregion

                                #region Site Bank Account beda

                                SQL.AppendLine("    Union All ");
                                SQL.AppendLine("        Select D.COAAcNoInterOffice As AcNo, ");
                                SQL.AppendLine("        A.Amt As DAmt, ");
                                SQL.AppendLine("        0.00 As CAmt ");
                                SQL.AppendLine("        From TblVoucherHdr A ");
                                SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
                                SQL.AppendLine("        Inner Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode And D.COAAcNoInterOffice Is Not Null ");
                                SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                                SQL.AppendLine("        And A.AcType2='C' ");
                                SQL.AppendLine("        And C.SiteCode != D.SiteCode ");
                                SQL.AppendLine("    Union All ");
                                SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
                                SQL.AppendLine("        0.00 As DAmt, ");
                                SQL.AppendLine("        A.Amt As CAmt ");
                                SQL.AppendLine("        From TblVoucherHdr A ");
                                SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
                                SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
                                SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                                SQL.AppendLine("        And A.AcType2='C' ");
                                SQL.AppendLine("        And C.SiteCode != B.SiteCode ");
                                SQL.AppendLine("    Union All ");
                                SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
                                SQL.AppendLine("        A.Amt As DAmt, ");
                                SQL.AppendLine("        0.00 As CAmt ");
                                SQL.AppendLine("        From TblVoucherHdr A ");
                                SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
                                SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
                                SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                                SQL.AppendLine("        And A.AcType2='D' ");
                                SQL.AppendLine("        And C.SiteCode != B.SiteCode ");
                                SQL.AppendLine("    Union All ");
                                SQL.AppendLine("        Select D.COAAcNoInterOffice As AcNo, ");
                                SQL.AppendLine("        0.00 As DAmt, ");
                                SQL.AppendLine("        A.Amt As CAmt ");
                                SQL.AppendLine("        From TblVoucherHdr A ");
                                SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
                                SQL.AppendLine("        Inner Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode And D.COAAcNoInterOffice Is Not Null ");
                                SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                                SQL.AppendLine("        And A.AcType2='D' ");
                                SQL.AppendLine("        And C.SiteCode != D.SiteCode ");

                                #endregion

                                SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");

                                SQL.AppendLine(") B On 1=1 ");
                                SQL.AppendLine("Inner Join tblVoucherHdr C On A.DocNo=C.JournalDocNo3 And C.JournalDocNo3 is Not Null And C.DocNo=@VoucherDocNo_" + VoucherIndex.ToString() + " ");
                                SQL.AppendLine("Inner Join tblVoucherDtl D On C.DocNo=D.DocNo ; ");
                            }
                        }

                        l1.Clear(); l2.Clear(); l3.Clear(); l4.Clear(); l5.Clear();
                    }
                }
                cm.CommandText = SQL.ToString();
                cml.Add(cm);
                Sm.ExecCommands(cml);

                Sm.StdMsg(mMsgType.Info, "Process is completed.");
                Sm.ClearGrd(Grd1, true);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private string GetProfitCenterCode(int r)
        {
            var Value = Sm.GetGrdStr(Grd1, r, 1);
            if (Value.Length == 0) return string.Empty;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.ProfitCenterCode ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.CCCode Is Not Null ");
            SQL.AppendLine("Inner Join TblCostCenter C On B.CCCode=C.CCCode And C.ProfitCenterCode Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@Param;");

            return Sm.GetValue(SQL.ToString(), Value);
        }

        private void GetProfitCenterCode(ref List<FrmJournalInterOffice.CCSeq> lCostCenter, ref List<string> lProfitCenterCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Query = new StringBuilder();
            int i = 0;

            foreach (var x in lCostCenter.Distinct())
            {
                Query.AppendLine(" Or CCCode=@CCCode_" + i.ToString());
                Sm.CmParam<String>(ref cm, "@CCCode_" + i.ToString(), x.CCCode);
                i++;
            }

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblCostCenter ");
            SQL.AppendLine("Where ProfitCenterCode Is Not Null And (CCCode='***' ");
            SQL.AppendLine(Query.ToString());
            SQL.AppendLine("); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read()) lProfitCenterCode.Add(Sm.DrStr(dr, c[0]));
                }
                dr.Close();
            }
        }

        private string GetJournalSQL16(int VoucherIndex)
        {
            //16 : Switching Bank Account

            var SQL = new StringBuilder();

            #region Site code sama di bank account nya

            SQL.AppendLine("        Select B.Parvalue As AcNo, ");
            SQL.AppendLine("        A.Amt As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForCrossAccount' And B.ParValue Is Not Null ");
            // tambahan wed
            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
            SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode And C.SiteCode = D.SiteCode ");
            // end tambahan wed
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
            SQL.AppendLine("        And A.AcType='C' ");

            // end tambahan wed

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        A.Amt As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            // tambahan wed
            SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode And B.SiteCode = D.SiteCode ");
            // end tambahan wed
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
            SQL.AppendLine("        And A.AcType='C' ");

            // end tambahan wed
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
            SQL.AppendLine("        A.Amt As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            // tambahan wed
            SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode And B.SiteCode = D.SiteCode ");
            // end tambahan wed
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
            SQL.AppendLine("        And A.AcType='D' ");
            // end tambahan wed
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.ParValue As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        A.Amt As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForCrossAccount' And B.ParValue Is Not Null ");
            // tambahan wed
            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
            SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode And C.SiteCode = D.SiteCode ");
            // end tambahan wed
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
            SQL.AppendLine("        And A.AcType='D' ");
            // end tambahan wed

            #endregion

            #region Site Code beda di bank account

            SQL.AppendLine("    Union All ");

            SQL.AppendLine("        Select C.COAAcNoInterOffice As AcNo, ");
            SQL.AppendLine("        A.Amt As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode And C.COAAcNoInteroffice Is Not Null ");
            SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
            SQL.AppendLine("        And A.AcType='C' ");
            SQL.AppendLine("        And C.SiteCode<>D.SiteCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        A.Amt As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
            SQL.AppendLine("        And A.AcType='C' ");
            SQL.AppendLine("        And B.SiteCode<>D.SiteCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
            SQL.AppendLine("        A.Amt As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
            SQL.AppendLine("        And A.AcType='D' ");
            SQL.AppendLine("        And B.SiteCode<>D.SiteCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select C.COAAcNoInterOffice As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        A.Amt As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode And C.COAAcNoInterOffice Is Not Null ");
            SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
            SQL.AppendLine("        And A.AcType='D' ");
            SQL.AppendLine("        And C.SiteCode<>D.SiteCode ");

            #endregion

            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");

            return SQL.ToString();

        }

        private string GetJournalSQL70(int VoucherIndex, bool IsMainCurCode)
        {
            //70 : Cash Advance Switching Bank Account

            var SQL = new StringBuilder();

            if (!IsMainCurCode)
            {
                SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Amt From TblCurrencyRate ");
                SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("    ), 0)*A.Amt ");
                SQL.AppendLine("    As DAmt, ");
                SQL.AppendLine("    0 As CAmt ");
                SQL.AppendLine("    From TblVoucherHdr A ");
                SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                SQL.AppendLine("    And A.AcType='D' ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
                SQL.AppendLine("    0 As DAmt, ");
                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Amt From TblCurrencyRate ");
                SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("    ), 0)*A.Amt ");
                SQL.AppendLine("    As CAmt ");
                SQL.AppendLine("    From TblVoucherHdr A ");
                SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                SQL.AppendLine("    And A.AcType='C' ");
                SQL.AppendLine("    Union All ");

                SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Amt From TblCurrencyRate ");
                SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("    ), 0)*A.Amt ");
                SQL.AppendLine("    As DAmt, ");
                SQL.AppendLine("    0 As CAmt ");
                SQL.AppendLine("    From TblVoucherHdr A ");
                SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                SQL.AppendLine("    And A.AcType2='D' ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
                SQL.AppendLine("    0 As DAmt, ");
                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Amt From TblCurrencyRate ");
                SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("    ), 0)*A.Amt ");
                SQL.AppendLine("    As CAmt ");
                SQL.AppendLine("    From TblVoucherHdr A ");
                SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                SQL.AppendLine("    And A.AcType2='C' ");
                SQL.AppendLine(") Tbl Where AcNo is Not Null Group By AcNo ");

            }
            else
            {
                SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
                SQL.AppendLine("    Case When A.CurCode=@MainCurCode Then A.Amt Else A.Amt*A.ExcRate End As DAmt, ");
                SQL.AppendLine("    0 As CAmt ");
                SQL.AppendLine("    From TblVoucherHdr A ");
                SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                SQL.AppendLine("    And A.AcType='D' ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
                SQL.AppendLine("    0 As DAmt, ");
                SQL.AppendLine("    Case When A.CurCode=@MainCurCode Then A.Amt Else A.Amt*A.ExcRate End As CAmt ");
                SQL.AppendLine("    From TblVoucherHdr A ");
                SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                SQL.AppendLine("    And A.AcType='C' ");
                SQL.AppendLine("    Union All ");

                SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
                SQL.AppendLine("    Case When A.CurCode2=@MainCurCode Then A.Amt*A.ExcRate Else A.Amt End As DAmt, ");
                SQL.AppendLine("    0 As CAmt ");
                SQL.AppendLine("    From TblVoucherHdr A ");
                SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                SQL.AppendLine("    And A.AcType2='D' ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
                SQL.AppendLine("    0 As DAmt, ");
                SQL.AppendLine("    Case When A.CurCode2=@MainCurCode Then A.Amt*A.ExcRate Else A.Amt End As CAmt ");
                SQL.AppendLine("    From TblVoucherHdr A ");
                SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                SQL.AppendLine("    And A.AcType2='C' ");
                SQL.AppendLine(") Tbl Where AcNo is Not Null Group By AcNo ");

            }

            return SQL.ToString();
        }

        private string GetJournalSQL73(int VoucherIndex)
        {
            //73: Switching Cost Center

            var SQL = new StringBuilder();

            SQL.AppendLine("        Select B.COAAcNoInterOffice As AcNo, ");
            SQL.AppendLine("        A.Amt As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
            SQL.AppendLine("        And A.AcType='C' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        A.Amt As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
            SQL.AppendLine("        And A.AcType='C' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
            SQL.AppendLine("        A.Amt As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
            SQL.AppendLine("        And A.AcType='D' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.COAAcNoInterOffice As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        A.Amt As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
            SQL.AppendLine("        And A.AcType='D' ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");

            return SQL.ToString();

        }

        private void SaveVoucher(ref MySqlCommand cm, ref StringBuilder SQL, int VoucherIndex, int row, string SQLForNewVoucherDocNo)
        {
            mDocType = Sm.GetGrdStr(Grd1, row, 13);

            SQL.Append("Set @VoucherDocNo_" + VoucherIndex.ToString() + ":=" + SQLForNewVoucherDocNo + ";");

            SQL.Append("Insert Into TblVoucherHdr ");
            SQL.Append("(DocNo, DocDt, CancelInd, MInd, VoucherRequestDocNo, DocType, ");
            SQL.Append("CurCode, CurCode2, ExcRate, ");
            if (mIsVoucherUseCashType)
                SQL.Append("CashTypeCode, ");
            if (mIsAutoJournalActived && ((mDocType == "16" && IsCostCenterSwitchingBankDifferent) || (mDocType == "73" && mIsVoucherCreateInterOfficeJournal && mVoucherInterOfficeJournalFormat == "2")))
                SQL.Append("InterOfficeInd, ");
            if (mIsVoucherUseCashTypeGroup)
                SQL.Append("CashTypeGrpCode, ");
            SQL.Append("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, BankCode, PIC, Amt, DeptCode, EntCode, Remark, BCCode, ");
            SQL.Append("CreateBy, CreateDt) ");
            SQL.Append("Select @VoucherDocNo_" + VoucherIndex.ToString() + ", @DocDt, 'N', MInd, @VoucherRequestDocNo_" + VoucherIndex.ToString() + ", DocType, ");
            SQL.Append("CurCode, CurCode2, ExcRate, ");
            if (mIsVoucherUseCashType)
                SQL.Append("@CashTypeCode, ");
            if (mIsAutoJournalActived && ((mDocType == "16" && IsCostCenterSwitchingBankDifferent) || (mDocType == "73" && mIsVoucherCreateInterOfficeJournal && mVoucherInterOfficeJournalFormat == "2")))
                SQL.Append("'Y', ");
            if (mIsVoucherUseCashTypeGroup)
                SQL.Append("@CashTypeGrpCode, ");
            SQL.Append("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, BankCode, PIC, Amt, DeptCode, EntCode, Remark, BCCode, ");
            SQL.Append("@UserCode, @Dt ");
            SQL.Append("From TblVoucherRequestHdr ");
            SQL.Append("Where DocNo=@VoucherRequestDocNo_" + VoucherIndex.ToString() + "; ");

            SQL.Append("Update TblVoucherRequestHdr Set ");
            SQL.Append("    VoucherDocNo=@VoucherDocNo_" + VoucherIndex.ToString());
            SQL.Append(" Where DocNo=@VoucherRequestDocNo_" + VoucherIndex.ToString() + ";");

            if (mDocType == "73")
            {
                SQL.Append("Update TblVoucherHdr Set ");
                SQL.Append("    PairedVoucherDocNo=( ");
                SQL.Append("        Select VoucherDocNo ");
                SQL.Append("        From TblVoucherRequestHdr ");
                SQL.Append("        Where VoucherDocNo Is Not Null And PairedVRDocNo=@VoucherRequestDocNo_" + VoucherIndex.ToString());
                SQL.Append("        Limit 1) ");
                SQL.Append("Where PairedVoucherDocNo Is Null And DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                SQL.Append(";");

                SQL.Append("Update TblVoucherHdr Set ");
                SQL.Append("    PairedVoucherDocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                SQL.Append(" Where PairedVoucherDocNo Is Null And DocNo In (");
                SQL.Append("    Select PairedVoucherDocNo ");
                SQL.Append("    From TblVoucherHdr ");
                SQL.Append("    Where PairedVoucherDocNo Is Not Null ");
                SQL.Append("    And DocNo=@VoucherDocNo_" + VoucherIndex.ToString());
                SQL.Append("    );");
            }

            SQL.Append("Insert Into TblVoucherDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.Append("Select @VoucherDocNo_" + VoucherIndex.ToString() + ", DNo, 'Multi Voucher', Amt, Remark, @UserCode, @Dt ");
            SQL.Append("From TblVoucherRequestDtl Where DocNo=@VoucherRequestDocNo_" + VoucherIndex.ToString() + ";");

            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo_" + VoucherIndex.ToString(), Sm.GetGrdStr(Grd1, row, 1));
        }

        private void SaveVoucherDtl3(ref StringBuilder SQL, ref MySqlCommand cm, string DocType, List<SwitchingBankAc> l, List<SwitchingCC> l2)
        {
            SQL.AppendLine("/* SaveVoucherDtl3 - Voucher Multi */ ");

            SQL.AppendLine("Insert Into TblVoucherDtl3(DocNo, DNo, JournalDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");

            int DNo = 1;
            int i = 0;
            int Temp = -1;

            if (DocType == "16")
            {
                foreach (var x in l.OrderBy(o => o.Voucher).ThenBy(o => o.Journal))
                {
                    if (Temp != x.Voucher) DNo = 1;
                    if (i != 0) SQL.AppendLine(", ");

                    SQL.AppendLine(
                        "(@VoucherDocNo_" + x.Voucher.ToString() +
                        ", @DNo_" + x.Voucher.ToString() + "_" + x.Journal.ToString() + "_" + i.ToString() +
                        ", @JournalDocNo_" + x.Journal.ToString() + ", ");
                    SQL.AppendLine("@UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + x.Voucher.ToString() + "_" + x.Journal.ToString() + "_" + i.ToString(), Sm.Right(string.Concat("000", (DNo).ToString()), 3));

                    i += 1;
                    DNo += 1;
                    Temp = x.Voucher;
                }
            }
            else if (DocType == "73")
            {
                foreach (var x in l2.OrderBy(o => o.Voucher).ThenBy(o => o.Journal))
                {
                    if (Temp != x.Voucher) DNo = 1;
                    if (i != 0) SQL.AppendLine(", ");

                    SQL.AppendLine(
                        "(@VoucherDocNo_" + x.Voucher.ToString() +
                        ", @DNo_" + x.Voucher.ToString() + "_" + x.Journal.ToString() + "_" + i.ToString() +
                        ", @JournalDocNo_" + x.Journal.ToString() + ", ");
                    SQL.AppendLine("@UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + x.Voucher.ToString() + "_" + x.Journal.ToString() + "_" + i.ToString(), Sm.Right(string.Concat("000", (DNo).ToString()), 3));

                    i += 1;
                    DNo += 1;
                    Temp = x.Voucher;
                }
            }

            SQL.AppendLine("; ");
        }

        private void SaveJournalInterOfficeCostCenterSwitchingBank(
           ref StringBuilder SQL, ref MySqlCommand cm, int VoucherIndex, int JournalIndex, int row, bool IsEdgeData,
            int index, string CCCode, string BankAcCode, decimal Amt, string SQLForNewJournalDocNo
           )
        {
            SQL.AppendLine("/*SaveJournalInterOfficeCostCenterSwitchingBank - Voucher Multi*/ ");
            SQL.AppendLine("Set @Row=0; ");
            SQL.AppendLine("Set @JournalDocNo_" + JournalIndex + ":=");
            SQL.AppendLine(SQLForNewJournalDocNo);
            //if (mDocNoFormat == "1")
            //    SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            //else
            //    SQL.AppendLine(Sm.GetNewJournalDocNo3(Sm.GetDte(DteDocDt), Sm.GetGrdStr(Grd1, row, 11), "1"));
            SQL.AppendLine("; ");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, ");
            SQL.AppendLine("CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo_" + JournalIndex + ", ");
            SQL.AppendLine("DocDt, Concat('Journal Inter Office : ', @VoucherDocNo_" + VoucherIndex.ToString() + "), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@CCCode_" + JournalIndex + ", Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblVoucherHdr Where DocNo=@VoucherDocNo_" + VoucherIndex.ToString() + "; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, DAmt DAmt, CAmt CAmt From ( ");

            if (IsEdgeData)
            {
                if (index == 0)
                {
                    //RAK debit, Bank credit
                    SQL.AppendLine("Select COAAcNoInterOffice As AcNo, @Amt_" + JournalIndex + " As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex);
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("Select COAAcNo As AcNo, 0.00 As DAmt, @Amt_" + JournalIndex + " As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex);
                }
                else // data index terakhir
                {
                    //bank debit, RAK credit
                    SQL.AppendLine("Select COAAcNo As AcNo, @Amt_" + JournalIndex + " As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex);
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("Select COAAcNoInterOffice As AcNo, 0.00 As DAmt, @Amt_" + JournalIndex + " As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex);
                }
            }
            else
            {
                if (index % 2 == 0) //data baris genap
                {
                    //RAK debit, bank credit
                    SQL.AppendLine("Select COAAcNoInterOffice As AcNo, @Amt_" + JournalIndex + " As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex);
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("Select COAAcNo As AcNo, 0.00 As DAmt, @Amt_" + JournalIndex + " As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex);
                }
                else //data baris ganjil
                {
                    //bank debit, RAK credit
                    SQL.AppendLine("Select COAAcNo As AcNo, @Amt_" + JournalIndex + " As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex);
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("Select COAAcNoInterOffice As AcNo, 0.00 As DAmt, @Amt_" + JournalIndex + " As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode=@BankAcCode_" + JournalIndex);
                }
            }

            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo = @JournalDocNo_" + JournalIndex + "; ");

            Sm.CmParam<String>(ref cm, "@CCCode_" + JournalIndex, CCCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt_" + JournalIndex, Amt);
            Sm.CmParam<String>(ref cm, "@BankAcCode_" + JournalIndex, BankAcCode);
        }

        private void SaveJournalInterOfficeCostCenterSwitchingCC(
            ref StringBuilder SQL, ref MySqlCommand cm, int VoucherIndex, int JournalIndex, int row, bool IsEdgeData, 
            int index, string CCCode, string BankAcCode, decimal Amt, string SQLForNewJournalDocNo)
        {
            SQL.AppendLine("/*SaveJournalInterOfficeCostCenterSwitchingCC - Voucher Multi*/ ");

            SQL.AppendLine("Set @Row=0; ");
            SQL.AppendLine("Set @JournalDocNo_"+JournalIndex+":=");
            SQL.AppendLine(SQLForNewJournalDocNo);
            //if (mDocNoFormat == "1")
            //    SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            //else
            //    SQL.AppendLine(Sm.GetNewJournalDocNo3(Sm.GetDte(DteDocDt), Sm.GetGrdStr(Grd1, row, 11), "1"));
            SQL.AppendLine("; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, ");
            SQL.AppendLine("CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo_" + JournalIndex + ", DocDt, ");
            SQL.AppendLine("Concat('Journal Inter Office : ', @VoucherDocNo_" + VoucherIndex.ToString() + "), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@CCCode_" + JournalIndex + ", Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblVoucherHdr ");
            SQL.AppendLine("Where DocNo=@VoucherDocNo_" + VoucherIndex.ToString() + "; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, DAmt DAmt, CAmt CAmt From ( ");

            if (IsEdgeData)
            {
                if (index == 0)
                {
                    //RAK debit, Bank credit
                    SQL.AppendLine("Select COAAcNoInterOffice As AcNo, @Amt_" + JournalIndex + " As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex);
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("Select COAAcNo As AcNo, 0.00 As DAmt, @Amt_" + JournalIndex + " As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex );
                }
                else // data index terakhir
                {
                    //bank debit, RAK credit
                    SQL.AppendLine("Select COAAcNo As AcNo, @Amt_" + JournalIndex + " As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex);
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("Select COAAcNoInterOffice As AcNo, 0.00 As DAmt, @Amt_" + JournalIndex + " As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex);
                }
            }
            else
            {
                if (index % 2 == 0) //data baris genap
                {
                    //RAK debit, bank credit
                    SQL.AppendLine("Select COAAcNoInterOffice As AcNo, @Amt_" + JournalIndex + " As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex);
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("Select COAAcNo As AcNo, 0.00 As DAmt, @Amt_" + JournalIndex + " As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex);
                }
                else //data baris ganjil
                {
                    //bank debit, RAK credit
                    SQL.AppendLine("Select COAAcNo As AcNo, @Amt_" + JournalIndex + " As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex);
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("Select COAAcNoInterOffice As AcNo, 0.00 As DAmt, @Amt_" + JournalIndex + " As CAmt ");
                    SQL.AppendLine("From TblBankAccount ");
                    SQL.AppendLine("Where BankAcCode = @BankAcCode_" + JournalIndex);
                }
            }

            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo_" + JournalIndex + "; ");

            Sm.CmParam<String>(ref cm, "@CCCode_" + JournalIndex, CCCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt_" + JournalIndex, Amt);
            Sm.CmParam<String>(ref cm, "@BankAcCode_" + JournalIndex, BankAcCode);
        }


        #endregion

        #region Old Code

        //override protected void SaveData()
        //{
        //    try
        //    {
        //        if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
        //            IsSaveDataNotValid()) return;

        //        //16 : Switching Bank Account
        //        //70 : Cash Advance Switching Bank Account
        //        //73 : Switching Cost Center

        //        Cursor.Current = Cursors.WaitCursor;

        //        var SQL = new StringBuilder();
        //        var cm = new MySqlCommand();
        //        var cml = new List<MySqlCommand>();

        //        bool
        //        IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");

        //        string
        //            YrMth = Sm.Left(Sm.GetDte(DteDocDt), 6),
        //            Yr = YrMth.Substring(2, 2),
        //            Mth = YrMth.Substring(4, 2),
        //            DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Voucher'"),
        //            DocSeqNo = "4";

        //        if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");
        //        string IterasiNum = string.Empty;
        //        for (int i = 0; i < Convert.ToInt32(DocSeqNo); i++)
        //        {
        //            IterasiNum = string.Concat(IterasiNum, "0");
        //        }

        //        int val = 0;

        //        var v = Sm.GetValueDec(
        //            "Select Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp " +
        //            "From TblVoucherHdr " +
        //            "Where Left(DocDt, 6)=@Param " +
        //            "Order By Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) Desc Limit 1;",
        //            YrMth
        //            );

        //        SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");
        //        SQL.AppendLine("Set @Row:=0; ");

        //        string DocTypeDesc = string.Empty;
        //        for (int r = 0; r < Grd1.Rows.Count; r++)
        //        {
        //            if (Sm.GetGrdBool(Grd1, r, 0))
        //            {
        //                mDocType = Sm.GetGrdStr(Grd1, r, 13);
        //                DocTypeDesc = string.Empty;
        //                switch (mDocType)
        //                {
        //                    case "16":
        //                        DocTypeDesc = "Switching Bank Account";
        //                        break;
        //                    case "70":
        //                        DocTypeDesc = "Cash Advance Switching Bank Account";
        //                        break;
        //                    case "73":
        //                        DocTypeDesc = "Switching Cost Center";
        //                        break;
        //                }

        //                if (mDocType == "16")
        //                    IsCostCenterSwitchingBankDifferent = GetCostCenterSwitchingBankAccount(Sm.GetGrdStr(Grd1, r, 15), Sm.GetGrdStr(Grd1, r, 16));

        //                v++;
        //                Grd1.Cells[r, 10].Value = string.Concat(Yr, "/", Mth, "/", Sm.Right(string.Concat(IterasiNum, v.ToString()), Convert.ToInt32(DocSeqNo)), "/", DocAbbr);

        //                cml.Add(SaveVoucher(r));

        //                if (mIsAutoJournalActived)
        //                {
        //                    // Journal InterOffice untuk 16+73
        //                    if (mDocType == "73" && mIsVoucherCreateInterOfficeJournal && mVoucherInterOfficeJournalFormat == "2" && Sm.GetGrdStr(Grd1, r, 20) == "Y")
        //                    {
        //                        #region InterOffice by Cost Center

        //                        CCCodeBankAccount = GetCCCodeBankAccount(Sm.GetGrdStr(Grd1, r, 15));
        //                        CCCodeBankAccount2 = GetCCCodeBankAccount(Sm.GetGrdStr(Grd1, r, 21));

        //                        var l1 = new List<FrmJournalInterOffice.CC1>();
        //                        var l2 = new List<FrmJournalInterOffice.CC2>();
        //                        var l3 = new List<FrmJournalInterOffice.CCSeq>();
        //                        var l4 = new List<SwitchingCC>();

        //                        FrmJournalInterOffice.PrepC1(ref l1, CCCodeBankAccount);
        //                        FrmJournalInterOffice.PrepC2(ref l2, CCCodeBankAccount2);
        //                        FrmJournalInterOffice.PrepSeq(ref l1, ref l2, ref l3);

        //                        if (l3.Count > 0)
        //                        {
        //                            if (mIsClosingJournalBasedOnMultiProfitCenter)
        //                            {
        //                                var lProfitCenterCode73 = new List<string>();
        //                                GetProfitCenterCode(ref l3, ref lProfitCenterCode73);
        //                                foreach (var x in lProfitCenterCode73)
        //                                {
        //                                    if (Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDocDt), x))
        //                                    {
        //                                        cml.Clear();
        //                                        return;
        //                                    }
        //                                }
        //                                lProfitCenterCode73.Clear();
        //                            }

        //                            string JNDocNo = string.Empty;
        //                            string BankAcCodeCr = Sm.GetGrdStr(Grd1, r, 17) == "C" ? Sm.GetGrdStr(Grd1, r, 15) : Sm.GetGrdStr(Grd1, r, 21);
        //                            string BankAcCodeDb = Sm.GetGrdStr(Grd1, r, 17) == "C" ? Sm.GetGrdStr(Grd1, r, 21) : Sm.GetGrdStr(Grd1, r, 15);
        //                            decimal AmtJN = 0m;

        //                            AmtJN = Sm.GetGrdDec(Grd1, r, 8);

        //                            for (int i = 0; i < l3.Count; ++i)
        //                            {
        //                                if (i == 0 || i == l3.Count - 1)
        //                                {
        //                                    val += 1;
        //                                    JNDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), val);

        //                                    l4.Add(new SwitchingCC()
        //                                    {
        //                                        JournalDocNo = JNDocNo,
        //                                        VoucherDocNo = Sm.GetGrdStr(Grd1, r, 10)
        //                                    });

        //                                    if (i == 0)
        //                                    {
        //                                        cml.Add(SaveJournalInterOfficeCostCenterSwitchingCC(
        //                                            Sm.GetGrdStr(Grd1, r, 10), JNDocNo, true, i, l3[i].CCCode, 0,
        //                                            Sm.GetGrdStr(Grd1, r, 1), BankAcCodeCr, Sm.GetDte(DteDocDt), mMenuCode,
        //                                            AmtJN
        //                                            ));
        //                                    }
        //                                    else
        //                                    {
        //                                        cml.Add(SaveJournalInterOfficeCostCenterSwitchingCC(
        //                                            Sm.GetGrdStr(Grd1, r, 10), JNDocNo, true, i, l3[i].CCCode, 0,
        //                                            Sm.GetGrdStr(Grd1, r, 1), BankAcCodeDb, Sm.GetDte(DteDocDt), mMenuCode,
        //                                            AmtJN
        //                                            ));
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    for (int j = 1; j <= 2; ++j)
        //                                    {
        //                                        val += 1;
        //                                        JNDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), val);

        //                                        l4.Add(new SwitchingCC()
        //                                        {
        //                                            JournalDocNo = JNDocNo,
        //                                            VoucherDocNo = Sm.GetGrdStr(Grd1, r, 10)
        //                                        });

        //                                        cml.Add(SaveJournalInterOfficeCostCenterSwitchingCC(
        //                                            Sm.GetGrdStr(Grd1, r, 10), JNDocNo, false, j, l3[i].CCCode, 0,
        //                                            Sm.GetGrdStr(Grd1, r, 1), BankAcCodeCr, Sm.GetDte(DteDocDt), mMenuCode,
        //                                            AmtJN
        //                                            ));
        //                                    }
        //                                }
        //                            }
        //                        }


        //                        if (l4.Count > 1)
        //                        {
        //                            cml.Add(SaveVoucherDtl3(mDocType, null, l4));
        //                        }

        //                        l1.Clear(); l2.Clear(); l3.Clear(); l4.Clear();

        //                        #endregion
        //                    }
        //                    if (mDocType == "16" && IsCostCenterSwitchingBankDifferent)
        //                    {
        //                        #region InterOffice by Cost Center

        //                        var l1 = new List<FrmJournalInterOffice.CC1>();
        //                        var l2 = new List<FrmJournalInterOffice.CC2>();
        //                        var l3 = new List<FrmJournalInterOffice.CCSeq>();
        //                        var l4 = new List<SwitchingBankAc>();

        //                        FrmJournalInterOffice.PrepC1(ref l1, CCCodeBankAccount);
        //                        FrmJournalInterOffice.PrepC2(ref l2, CCCodeBankAccount2);
        //                        FrmJournalInterOffice.PrepSeq(ref l1, ref l2, ref l3);

        //                        if (l3.Count > 0)
        //                        {
        //                            if (mIsClosingJournalBasedOnMultiProfitCenter)
        //                            {
        //                                var lProfitCenterCode16 = new List<string>();
        //                                GetProfitCenterCode(ref l3, ref lProfitCenterCode16);
        //                                foreach (var x in lProfitCenterCode16)
        //                                {
        //                                    if (Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDocDt), x))
        //                                    {
        //                                        cml.Clear();
        //                                        return;
        //                                    }
        //                                }
        //                                lProfitCenterCode16.Clear();
        //                            }

        //                            string JNDocNo = string.Empty;
        //                            string BankAcCodeCr = Sm.GetGrdStr(Grd1, r, 17) == "C" ? Sm.GetGrdStr(Grd1, r, 15) : Sm.GetGrdStr(Grd1, r, 16);
        //                            string BankAcCodeDb = Sm.GetGrdStr(Grd1, r, 17) == "C" ? Sm.GetGrdStr(Grd1, r, 16) : Sm.GetGrdStr(Grd1, r, 15);
        //                            decimal AmtJN = 0m;

        //                            AmtJN = Sm.GetGrdDec(Grd1, r, 8);

        //                            for (int i = 0; i < l3.Count; ++i)
        //                            {
        //                                if (i == 0 || i == l3.Count - 1)
        //                                {
        //                                    val += 1;
        //                                    JNDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), val);

        //                                    l4.Add(new SwitchingBankAc()
        //                                    {
        //                                        JournalDocNo = JNDocNo,
        //                                        VoucherDocNo = Sm.GetGrdStr(Grd1, r, 10)
        //                                    });

        //                                    if (i == 0)
        //                                    {
        //                                        cml.Add(SaveJournalInterOfficeCostCenterSwitchingBank(
        //                                            Sm.GetGrdStr(Grd1, r, 10), JNDocNo, true, i, l3[i].CCCode, 0,
        //                                            Sm.GetGrdStr(Grd1, r, 1), BankAcCodeCr, Sm.GetDte(DteDocDt), mMenuCode,
        //                                            AmtJN
        //                                            ));
        //                                    }
        //                                    else
        //                                    {
        //                                        cml.Add(SaveJournalInterOfficeCostCenterSwitchingBank(
        //                                            Sm.GetGrdStr(Grd1, r, 10), JNDocNo, true, i, l3[i].CCCode, 0,
        //                                            Sm.GetGrdStr(Grd1, r, 1), BankAcCodeDb, Sm.GetDte(DteDocDt), mMenuCode,
        //                                            AmtJN
        //                                            ));
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    for (int j = 1; j <= 2; ++j)
        //                                    {
        //                                        val += 1;
        //                                        JNDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), val);

        //                                        l4.Add(new SwitchingBankAc()
        //                                        {
        //                                            JournalDocNo = JNDocNo,
        //                                            VoucherDocNo = Sm.GetGrdStr(Grd1, r, 10)
        //                                        });

        //                                        cml.Add(SaveJournalInterOfficeCostCenterSwitchingBank(
        //                                            Sm.GetGrdStr(Grd1, r, 10), JNDocNo, false, j, l3[i].CCCode, 0,
        //                                            Sm.GetGrdStr(Grd1, r, 1), BankAcCodeCr, Sm.GetDte(DteDocDt), mMenuCode,
        //                                            AmtJN
        //                                            ));
        //                                    }
        //                                }
        //                            }
        //                        }


        //                        if (l4.Count > 1)
        //                        {
        //                            cml.Add(SaveVoucherDtl3(mDocType, l4, null));
        //                        }

        //                        l1.Clear(); l2.Clear(); l3.Clear(); l4.Clear();

        //                        #endregion
        //                    }

        //                    if (!mIsVoucherCreateInterOfficeJournal ||
        //                        (mIsVoucherCreateInterOfficeJournal && mDocType == "70"))
        //                    {
        //                        if (mIsClosingJournalBasedOnMultiProfitCenter &&
        //                            mIsVoucherCreateInterOfficeJournal &&
        //                            mDocType == "70")
        //                        {
        //                            if (Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDocDt), GetProfitCenterCode(r)))
        //                            {
        //                                cml.Clear();
        //                                return;
        //                            }
        //                        }

        //                        // Journal Standard
        //                        SQL.AppendLine("Update TblVoucherHdr Set ");
        //                        SQL.AppendLine("    JournalDocNo=");
        //                        if (mDocNoFormat == "1")
        //                            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
        //                        else
        //                            SQL.AppendLine(Sm.GetNewJournalDocNo3(Sm.GetDte(DteDocDt), Sm.GetGrdStr(Grd1, r, 11), "1"));
        //                        if (mDocType == "16")
        //                        {
        //                            SQL.AppendLine("    ,JournalDocNo3=");
        //                            if (mDocNoFormat == "1")
        //                                SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 2));
        //                            else
        //                                SQL.AppendLine(Sm.GetNewJournalDocNo3(Sm.GetDte(DteDocDt), Sm.GetGrdStr(Grd1, r, 11), "2"));
        //                        }
        //                        SQL.AppendLine("Where DocNo=@VoucherDocNo_" + r.ToString() + ";");

        //                        SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
        //                        SQL.AppendLine("Select A.JournalDocNo, A.DocDt, ");
        //                        SQL.AppendLine("Concat('Voucher (', IfNull( '" + DocTypeDesc + "' , 'None'), ') : ', A.DocNo) As JnDesc, ");
        //                        SQL.AppendLine("@MenuCode As MenuCode, ");
        //                        SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
        //                        SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
        //                        SQL.AppendLine("From TblVoucherHdr A ");
        //                        SQL.AppendLine("Left Join TblBankAccount B On A.BankAcCode = B.BankAcCode ");
        //                        SQL.AppendLine("Where A.DocNo=@VoucherDocNo_" + r.ToString() + " And A.JournalDocNo Is Not Null; ");

        //                        SQL.AppendLine("Set @Row=0; ");

        //                        SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
        //                        SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
        //                        SQL.AppendLine("B.AcNo, B.DAmt, B.CAmt, ");
        //                        SQL.AppendLine("EntCode, D.Remark, A.CreateBy, A.CreateDt ");
        //                        SQL.AppendLine("From TblJournalHdr A ");
        //                        SQL.AppendLine("Inner Join ( ");
        //                        SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

        //                        SQL.AppendLine(GetJournalSQL(ref cm, mDocType, r));

        //                        SQL.AppendLine(") B On 1=1 ");
        //                        SQL.AppendLine("Inner Join tblVoucherHdr C On A.DocNo=C.JournalDocNo And C.JournalDocNo is Not Null And C.DocNo=@VoucherDocNo_" + r.ToString() + " ");
        //                        SQL.AppendLine("Inner Join tblVoucherDtl D On C.DocNo=D.DocNo ; ");


        //                        if (mDocType == "16")
        //                        {
        //                            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
        //                            SQL.AppendLine("Select A.JournalDocNo3, A.DocDt, ");
        //                            SQL.AppendLine("Concat('Voucher (', IfNull('" + DocTypeDesc + " ', 'None'), ') : ', A.DocNo) As JnDesc, ");
        //                            SQL.AppendLine("@MenuCode As MenuCode, ");
        //                            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
        //                            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
        //                            SQL.AppendLine("From TblVoucherHdr A ");
        //                            SQL.AppendLine("Left Join TblBankAccount B On A.BankAcCode2 = B.BankAcCode ");
        //                            SQL.AppendLine("Where A.DocNo=@VoucherDocNo_" + r.ToString() + " And A.JournalDocNo3 Is Not Null; ");

        //                            SQL.AppendLine("Set @Row=0; ");

        //                            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
        //                            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
        //                            SQL.AppendLine("B.AcNo, B.DAmt, B.CAmt, ");
        //                            SQL.AppendLine("EntCode, D.Remark, A.CreateBy, A.CreateDt ");
        //                            SQL.AppendLine("From TblJournalHdr A ");
        //                            SQL.AppendLine("Inner Join ( ");
        //                            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From ( ");

        //                            #region Site Bank Account sama

        //                            SQL.AppendLine("        Select B.ParValue As AcNo, ");
        //                            SQL.AppendLine("        A.Amt As DAmt, ");
        //                            SQL.AppendLine("        0.00 As CAmt ");
        //                            SQL.AppendLine("        From TblVoucherHdr A ");
        //                            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForCrossAccount' And B.ParValue Is Not Null ");
        //                            // tambahan wed
        //                            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
        //                            SQL.AppendLine("        Inner Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
        //                            // end tambahan wed
        //                            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + r.ToString());
        //                            SQL.AppendLine("        And A.AcType2='C' ");
        //                            // tambahan wed
        //                            SQL.AppendLine("        And C.SiteCode = D.SiteCode ");
        //                            // end tambahan wed
        //                            SQL.AppendLine("    Union All ");
        //                            SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
        //                            SQL.AppendLine("        0.00 As DAmt, ");
        //                            SQL.AppendLine("        A.Amt As CAmt ");
        //                            SQL.AppendLine("        From TblVoucherHdr A ");
        //                            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
        //                            // tambahan wed
        //                            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
        //                            // end tambahan wed
        //                            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + r.ToString());
        //                            SQL.AppendLine("        And A.AcType2='C' ");
        //                            // tambahan wed
        //                            SQL.AppendLine("        And C.SiteCode = B.SiteCode ");
        //                            // end tambahan wed
        //                            SQL.AppendLine("    Union All ");
        //                            SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
        //                            SQL.AppendLine("        A.Amt As DAmt, ");
        //                            SQL.AppendLine("        0.00 As CAmt ");
        //                            SQL.AppendLine("        From TblVoucherHdr A ");
        //                            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
        //                            // tambahan wed
        //                            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
        //                            // end tambahan wed
        //                            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + r.ToString());
        //                            SQL.AppendLine("        And A.AcType2='D' ");
        //                            // tambahan wed
        //                            SQL.AppendLine("        And C.SiteCode = B.SiteCode ");
        //                            // end tambahan wed
        //                            SQL.AppendLine("    Union All ");
        //                            SQL.AppendLine("        Select B.ParValue As AcNo, ");
        //                            SQL.AppendLine("        0.00 As DAmt, ");
        //                            SQL.AppendLine("        A.Amt As CAmt ");
        //                            SQL.AppendLine("        From TblVoucherHdr A ");
        //                            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForCrossAccount' And B.ParValue Is Not Null ");
        //                            // tambahan wed
        //                            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
        //                            SQL.AppendLine("        Inner Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
        //                            // end tambahan wed
        //                            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + r.ToString());
        //                            SQL.AppendLine("        And A.AcType2='D' ");
        //                            // tambahan wed
        //                            SQL.AppendLine("        And C.SiteCode = D.SiteCode ");
        //                            // end tambahan wed

        //                            #endregion

        //                            #region Site Bank Account beda

        //                            SQL.AppendLine("    Union All ");
        //                            SQL.AppendLine("        Select D.COAAcNoInterOffice As AcNo, ");
        //                            SQL.AppendLine("        A.Amt As DAmt, ");
        //                            SQL.AppendLine("        0.00 As CAmt ");
        //                            SQL.AppendLine("        From TblVoucherHdr A ");
        //                            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
        //                            SQL.AppendLine("        Inner Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode And D.COAAcNoInterOffice Is Not Null ");
        //                            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + r.ToString());
        //                            SQL.AppendLine("        And A.AcType2='C' ");
        //                            SQL.AppendLine("        And C.SiteCode != D.SiteCode ");
        //                            SQL.AppendLine("    Union All ");
        //                            SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
        //                            SQL.AppendLine("        0.00 As DAmt, ");
        //                            SQL.AppendLine("        A.Amt As CAmt ");
        //                            SQL.AppendLine("        From TblVoucherHdr A ");
        //                            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
        //                            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
        //                            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + r.ToString());
        //                            SQL.AppendLine("        And A.AcType2='C' ");
        //                            SQL.AppendLine("        And C.SiteCode != B.SiteCode ");
        //                            SQL.AppendLine("    Union All ");
        //                            SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
        //                            SQL.AppendLine("        A.Amt As DAmt, ");
        //                            SQL.AppendLine("        0.00 As CAmt ");
        //                            SQL.AppendLine("        From TblVoucherHdr A ");
        //                            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
        //                            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
        //                            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + r.ToString());
        //                            SQL.AppendLine("        And A.AcType2='D' ");
        //                            SQL.AppendLine("        And C.SiteCode != B.SiteCode ");
        //                            SQL.AppendLine("    Union All ");
        //                            SQL.AppendLine("        Select D.COAAcNoInterOffice As AcNo, ");
        //                            SQL.AppendLine("        0.00 As DAmt, ");
        //                            SQL.AppendLine("        A.Amt As CAmt ");
        //                            SQL.AppendLine("        From TblVoucherHdr A ");
        //                            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
        //                            SQL.AppendLine("        Inner Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode And D.COAAcNoInterOffice Is Not Null ");
        //                            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + r.ToString());
        //                            SQL.AppendLine("        And A.AcType2='D' ");
        //                            SQL.AppendLine("        And C.SiteCode != D.SiteCode ");

        //                            #endregion

        //                            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");

        //                            SQL.AppendLine(") B On 1=1 ");
        //                            SQL.AppendLine("Inner Join tblVoucherHdr C On A.DocNo=C.JournalDocNo3 And C.JournalDocNo3 is Not Null And C.DocNo=@VoucherDocNo_" + r.ToString() + " ");
        //                            SQL.AppendLine("Inner Join tblVoucherDtl D On C.DocNo=D.DocNo ; ");
        //                        }
        //                    }
        //                }
        //                Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
        //                Sm.CmParam<String>(ref cm, "@VoucherDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 10));

        //            }
        //        }
        //        Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //        Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
        //        Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
        //        Sm.CmParam<String>(ref cm, "@CashTypeCode", Sm.GetLue(LueCashTypeCode));

        //        cm.CommandText = SQL.ToString();
        //        cml.Add(cm);
        //        Sm.ExecCommands(cml);

        //        Sm.StdMsg(mMsgType.Info, "Process is completed.");
        //        Sm.ClearGrd(Grd1, true);
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //    finally
        //    {
        //        Cursor.Current = Cursors.Default;
        //    }
        //}

        //private string GetProfitCenterCode(int r)
        //{
        //    var Value = Sm.GetGrdStr(Grd1, r, 1);
        //    if (Value.Length == 0) return string.Empty;

        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select C.ProfitCenterCode ");
        //    SQL.AppendLine("From TblVoucherRequestHdr A ");
        //    SQL.AppendLine("Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.CCCode Is Not Null ");
        //    SQL.AppendLine("Inner Join TblCostCenter C On B.CCCode=C.CCCode And C.ProfitCenterCode Is Not Null ");
        //    SQL.AppendLine("Where A.DocNo=@Param;");

        //    return Sm.GetValue(SQL.ToString(), Value);
        //}

        //private void GetProfitCenterCode(ref List<FrmJournalInterOffice.CCSeq> lCostCenter, ref List<string> lProfitCenterCode)
        //{
        //    var cm = new MySqlCommand();
        //    var SQL = new StringBuilder();
        //    var Query = new StringBuilder();
        //    int i = 0;

        //    foreach (var x in lCostCenter.Distinct())
        //    {
        //        Query.AppendLine(" Or CCCode=@CCCode_" + i.ToString());
        //        Sm.CmParam<String>(ref cm, "@CCCode_" + i.ToString(), x.CCCode);
        //        i++;
        //    }

        //    SQL.AppendLine("Select Distinct ProfitCenterCode From TblCostCenter ");
        //    SQL.AppendLine("Where ProfitCenterCode Is Not Null And (CCCode='***' ");
        //    SQL.AppendLine(Query.ToString());
        //    SQL.AppendLine("); ");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandTimeout = 600;
        //        cm.CommandText = SQL.ToString();
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read()) lProfitCenterCode.Add(Sm.DrStr(dr, c[0]));
        //        }
        //        dr.Close();
        //    }
        //}

        //private string GetJournalSQL16(ref MySqlCommand cm, int Row)
        //{
        //    //16 : Switching Bank Account

        //    var SQL = new StringBuilder();

        //    #region Site code sama di bank account nya

        //    SQL.AppendLine("        Select B.Parvalue As AcNo, ");
        //    SQL.AppendLine("        A.Amt As DAmt, ");
        //    SQL.AppendLine("        0.00 As CAmt ");
        //    SQL.AppendLine("        From TblVoucherHdr A ");
        //    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForCrossAccount' And B.ParValue Is Not Null ");
        //    // tambahan wed
        //    SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
        //    SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
        //    // end tambahan wed
        //    SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //    SQL.AppendLine("        And A.AcType='C' ");
        //    // tambahan wed
        //    SQL.AppendLine("        And C.SiteCode = D.SiteCode ");
        //    // end tambahan wed

        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
        //    SQL.AppendLine("        0.00 As DAmt, ");
        //    SQL.AppendLine("        A.Amt As CAmt ");
        //    SQL.AppendLine("        From TblVoucherHdr A ");
        //    SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
        //    // tambahan wed
        //    SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
        //    // end tambahan wed
        //    SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //    SQL.AppendLine("        And A.AcType='C' ");
        //    // tambahan wed
        //    SQL.AppendLine("        And B.SiteCode = D.SiteCode ");
        //    // end tambahan wed
        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
        //    SQL.AppendLine("        A.Amt As DAmt, ");
        //    SQL.AppendLine("        0.00 As CAmt ");
        //    SQL.AppendLine("        From TblVoucherHdr A ");
        //    SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
        //    // tambahan wed
        //    SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
        //    // end tambahan wed
        //    SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //    SQL.AppendLine("        And A.AcType='D' ");
        //    // tambahan wed
        //    SQL.AppendLine("        And B.SiteCode = D.SiteCode ");
        //    // end tambahan wed
        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("        Select B.ParValue As AcNo, ");
        //    SQL.AppendLine("        0.00 As DAmt, ");
        //    SQL.AppendLine("        A.Amt As CAmt ");
        //    SQL.AppendLine("        From TblVoucherHdr A ");
        //    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForCrossAccount' And B.ParValue Is Not Null ");
        //    // tambahan wed
        //    SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
        //    SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
        //    // end tambahan wed
        //    SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //    SQL.AppendLine("        And A.AcType='D' ");
        //    // tambahan wed
        //    SQL.AppendLine("        And C.SiteCode = D.SiteCode ");
        //    // end tambahan wed

        //    #endregion

        //    #region Site Code beda di bank account

        //    SQL.AppendLine("    Union All ");

        //    SQL.AppendLine("        Select C.COAAcNoInterOffice As AcNo, ");
        //    SQL.AppendLine("        A.Amt As DAmt, ");
        //    SQL.AppendLine("        0.00 As CAmt ");
        //    SQL.AppendLine("        From TblVoucherHdr A ");
        //    SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode And C.COAAcNoInteroffice Is Not Null ");
        //    SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
        //    SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //    SQL.AppendLine("        And A.AcType='C' ");
        //    SQL.AppendLine("        And C.SiteCode != D.SiteCode ");

        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
        //    SQL.AppendLine("        0.00 As DAmt, ");
        //    SQL.AppendLine("        A.Amt As CAmt ");
        //    SQL.AppendLine("        From TblVoucherHdr A ");
        //    SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
        //    SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
        //    SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //    SQL.AppendLine("        And A.AcType='C' ");
        //    SQL.AppendLine("        And B.SiteCode != D.SiteCode ");
        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
        //    SQL.AppendLine("        A.Amt As DAmt, ");
        //    SQL.AppendLine("        0.00 As CAmt ");
        //    SQL.AppendLine("        From TblVoucherHdr A ");
        //    SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
        //    SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
        //    SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //    SQL.AppendLine("        And A.AcType='D' ");
        //    SQL.AppendLine("        And B.SiteCode != D.SiteCode ");
        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("        Select C.COAAcNoInterOffice As AcNo, ");
        //    SQL.AppendLine("        0.00 As DAmt, ");
        //    SQL.AppendLine("        A.Amt As CAmt ");
        //    SQL.AppendLine("        From TblVoucherHdr A ");
        //    SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode And C.COAAcNoInterOffice Is Not Null ");
        //    SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2 = D.BankAcCode ");
        //    SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //    SQL.AppendLine("        And A.AcType='D' ");
        //    SQL.AppendLine("        And C.SiteCode != D.SiteCode ");

        //    #endregion

        //    SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");

        //    return SQL.ToString();

        //}

        //private string GetJournalSQL70(ref MySqlCommand cm, int Row)
        //{
        //    //70 : Cash Advance Switching Bank Account

        //    var SQL = new StringBuilder();

        //    //SQL.AppendLine("        Select B.COAAcNoInterOffice As AcNo, ");
        //    //SQL.AppendLine("        A.Amt As DAmt, ");
        //    //SQL.AppendLine("        0.00 As CAmt ");
        //    //SQL.AppendLine("        From TblVoucherHdr A ");
        //    //SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
        //    //SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //    //SQL.AppendLine("    Union All ");
        //    //SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
        //    //SQL.AppendLine("        0.00 As DAmt, ");
        //    //SQL.AppendLine("        A.Amt As CAmt ");
        //    //SQL.AppendLine("        From TblVoucherHdr A ");
        //    //SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
        //    //SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //    //SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");

        //    if (!Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 7), mMainCurCode))
        //    {
        //        SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
        //        SQL.AppendLine("    IfNull(( ");
        //        SQL.AppendLine("        Select Amt From TblCurrencyRate ");
        //        SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
        //        SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
        //        SQL.AppendLine("    ), 0)*A.Amt ");
        //        SQL.AppendLine("    As DAmt, ");
        //        SQL.AppendLine("    0 As CAmt ");
        //        SQL.AppendLine("    From TblVoucherHdr A ");
        //        SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
        //        SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //        SQL.AppendLine("    And A.AcType='D' ");
        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
        //        SQL.AppendLine("    0 As DAmt, ");
        //        SQL.AppendLine("    IfNull(( ");
        //        SQL.AppendLine("        Select Amt From TblCurrencyRate ");
        //        SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
        //        SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
        //        SQL.AppendLine("    ), 0)*A.Amt ");
        //        SQL.AppendLine("    As CAmt ");
        //        SQL.AppendLine("    From TblVoucherHdr A ");
        //        SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
        //        SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //        SQL.AppendLine("    And A.AcType='C' ");
        //        SQL.AppendLine("    Union All ");

        //        SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
        //        SQL.AppendLine("    IfNull(( ");
        //        SQL.AppendLine("        Select Amt From TblCurrencyRate ");
        //        SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
        //        SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
        //        SQL.AppendLine("    ), 0)*A.Amt ");
        //        SQL.AppendLine("    As DAmt, ");
        //        SQL.AppendLine("    0 As CAmt ");
        //        SQL.AppendLine("    From TblVoucherHdr A ");
        //        SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
        //        SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //        SQL.AppendLine("    And A.AcType2='D' ");
        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
        //        SQL.AppendLine("    0 As DAmt, ");
        //        SQL.AppendLine("    IfNull(( ");
        //        SQL.AppendLine("        Select Amt From TblCurrencyRate ");
        //        SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
        //        SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
        //        SQL.AppendLine("    ), 0)*A.Amt ");
        //        SQL.AppendLine("    As CAmt ");
        //        SQL.AppendLine("    From TblVoucherHdr A ");
        //        SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
        //        SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //        SQL.AppendLine("    And A.AcType2='C' ");
        //        SQL.AppendLine(") Tbl Where AcNo is Not Null Group By AcNo ");

        //    }
        //    else
        //    {
        //        SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
        //        SQL.AppendLine("    Case When A.CurCode=@MainCurCode Then A.Amt Else A.Amt*A.ExcRate End As DAmt, ");
        //        SQL.AppendLine("    0 As CAmt ");
        //        SQL.AppendLine("    From TblVoucherHdr A ");
        //        SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
        //        SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //        SQL.AppendLine("    And A.AcType='D' ");
        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
        //        SQL.AppendLine("    0 As DAmt, ");
        //        SQL.AppendLine("    Case When A.CurCode=@MainCurCode Then A.Amt Else A.Amt*A.ExcRate End As CAmt ");
        //        SQL.AppendLine("    From TblVoucherHdr A ");
        //        SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
        //        SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //        SQL.AppendLine("    And A.AcType='C' ");
        //        SQL.AppendLine("    Union All ");

        //        SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
        //        SQL.AppendLine("    Case When A.CurCode2=@MainCurCode Then A.Amt*A.ExcRate Else A.Amt End As DAmt, ");
        //        SQL.AppendLine("    0 As CAmt ");
        //        SQL.AppendLine("    From TblVoucherHdr A ");
        //        SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
        //        SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //        SQL.AppendLine("    And A.AcType2='D' ");
        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select B.COAAcNo As AcNo, ");
        //        SQL.AppendLine("    0 As DAmt, ");
        //        SQL.AppendLine("    Case When A.CurCode2=@MainCurCode Then A.Amt*A.ExcRate Else A.Amt End As CAmt ");
        //        SQL.AppendLine("    From TblVoucherHdr A ");
        //        SQL.AppendLine("    Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode And B.COAAcNo Is Not Null ");
        //        SQL.AppendLine("    Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //        SQL.AppendLine("    And A.AcType2='C' ");
        //        SQL.AppendLine(") Tbl Where AcNo is Not Null Group By AcNo ");

        //    }

        //    return SQL.ToString();
        //}

        //private string GetJournalSQL73(ref MySqlCommand cm, int Row)
        //{
        //    //73: Switching Cost Center

        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("        Select B.COAAcNoInterOffice As AcNo, ");
        //    SQL.AppendLine("        A.Amt As DAmt, ");
        //    SQL.AppendLine("        0.00 As CAmt ");
        //    SQL.AppendLine("        From TblVoucherHdr A ");
        //    SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
        //    SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //    SQL.AppendLine("        And A.AcType='C' ");
        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
        //    SQL.AppendLine("        0.00 As DAmt, ");
        //    SQL.AppendLine("        A.Amt As CAmt ");
        //    SQL.AppendLine("        From TblVoucherHdr A ");
        //    SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
        //    SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //    SQL.AppendLine("        And A.AcType='C' ");
        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
        //    SQL.AppendLine("        A.Amt As DAmt, ");
        //    SQL.AppendLine("        0.00 As CAmt ");
        //    SQL.AppendLine("        From TblVoucherHdr A ");
        //    SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
        //    SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //    SQL.AppendLine("        And A.AcType='D' ");
        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("        Select B.COAAcNoInterOffice As AcNo, ");
        //    SQL.AppendLine("        0.00 As DAmt, ");
        //    SQL.AppendLine("        A.Amt As CAmt ");
        //    SQL.AppendLine("        From TblVoucherHdr A ");
        //    SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
        //    SQL.AppendLine("        Where A.DocNo=@VoucherDocNo_" + Row.ToString());
        //    SQL.AppendLine("        And A.AcType='D' ");
        //    SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");

        //    return SQL.ToString();

        //}

        //private MySqlCommand SaveVoucher(int r)
        //{
        //    var SQL = new StringBuilder();

        //    mDocType = Sm.GetGrdStr(Grd1, r, 13);
        //    SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

        //    SQL.Append("Insert Into TblVoucherHdr ");
        //    SQL.Append("(DocNo, DocDt, CancelInd, MInd, VoucherRequestDocNo, DocType, ");
        //    SQL.Append("CurCode, CurCode2, ExcRate, ");
        //    if (mIsVoucherUseCashType)
        //        SQL.Append("CashTypeCode, ");
        //    if (mIsAutoJournalActived && ((mDocType == "16" && IsCostCenterSwitchingBankDifferent) || (mDocType == "73" && mIsVoucherCreateInterOfficeJournal && mVoucherInterOfficeJournalFormat == "2")))
        //        SQL.Append("InterOfficeInd, ");
        //    if (mIsVoucherUseCashTypeGroup)
        //        SQL.Append("CashTypeGrpCode, ");
        //    SQL.Append("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, BankCode, PIC, Amt, DeptCode, EntCode, Remark, BCCode, CreateBy, CreateDt) ");
        //    SQL.Append("Select @VoucherDocNo_" + r.ToString() + ", @DocDt, 'N', MInd, @VoucherRequestDocNo_" + r.ToString() + ", DocType, ");
        //    SQL.Append("CurCode, CurCode2, ExcRate, ");
        //    if (mIsVoucherUseCashType)
        //        SQL.Append("@CashTypeCode, ");
        //    if (mIsAutoJournalActived && ((mDocType == "16" && IsCostCenterSwitchingBankDifferent) || (mDocType == "73" && mIsVoucherCreateInterOfficeJournal && mVoucherInterOfficeJournalFormat == "2")))
        //        SQL.Append("'Y', ");
        //    if (mIsVoucherUseCashTypeGroup)
        //        SQL.Append("@CashTypeGrpCode, ");
        //    SQL.Append("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, BankCode, PIC, Amt, DeptCode, EntCode, Remark, BCCode, @UserCode, @Dt ");
        //    SQL.Append("From TblVoucherRequestHdr ");
        //    SQL.Append("Where DocNo=@VoucherRequestDocNo_" + r.ToString() + "; ");

        //    SQL.Append("Update TblVoucherRequestHdr Set ");
        //    SQL.Append("    VoucherDocNo=@VoucherDocNo_" + r.ToString());
        //    SQL.Append(" Where DocNo=@VoucherRequestDocNo_" + r.ToString() + ";");

        //    SQL.Append("Insert Into TblVoucherDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
        //    SQL.Append("Select @VoucherDocNo_" + r.ToString() + ", DNo, 'Multi Voucher', Amt, Remark, @UserCode, @Dt ");
        //    SQL.Append("From TblVoucherRequestDtl Where DocNo=@VoucherRequestDocNo_" + r.ToString() + ";");


        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
        //    Sm.CmParam<String>(ref cm, "@VoucherDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 10));
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
        //    Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
        //    Sm.CmParam<String>(ref cm, "@CashTypeCode", Sm.GetLue(LueCashTypeCode));
        //    Sm.CmParam<String>(ref cm, "@CashTypeGrpCode", Sm.GetLue(LueCashTypeGroup));

        //    return cm;
        //}

        //private MySqlCommand SaveVoucherDtl3(string DocType, List<SwitchingBankAc> l, List<SwitchingCC> l2)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();

        //    SQL.AppendLine("/*SaveVoucherDtl3 - Voucher*/ ");
        //    SQL.AppendLine("Insert Into TblVoucherDtl3(DocNo, DNo, JournalDocNo, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values ");

        //    int DNo = 1;
        //    int i = 0;
        //    string CurrVoucherDocNo = string.Empty;

        //    if (DocType == "16")
        //    {
        //        foreach (var x in l.OrderBy(o => o.VoucherDocNo).ThenBy(o => o.JournalDocNo))
        //        {
        //            if (CurrVoucherDocNo.Length == 0) CurrVoucherDocNo = x.VoucherDocNo;

        //            if (CurrVoucherDocNo != x.VoucherDocNo) DNo = 1;

        //            if (i != 0) SQL.AppendLine(", ");

        //            SQL.AppendLine("(@DocNo__" + i.ToString() + ", @DNo__" + i.ToString() + ", @JournalDocNo__" + i.ToString() + ", ");
        //            SQL.AppendLine("@CreateBy, CurrentDateTime() ");
        //            SQL.AppendLine(") ");

        //            Sm.CmParam<String>(ref cm, "@DocNo__" + i.ToString(), x.VoucherDocNo);
        //            Sm.CmParam<String>(ref cm, "@DNo__" + i.ToString(), Sm.Right(string.Concat("000", (DNo).ToString()), 3));
        //            Sm.CmParam<String>(ref cm, "@JournalDocNo__" + i.ToString(), x.JournalDocNo);

        //            i += 1;
        //            DNo += 1;
        //        }
        //    }
        //    else if (DocType == "73")
        //    {
        //        foreach (var x in l2.OrderBy(o => o.VoucherDocNo).ThenBy(o => o.JournalDocNo))
        //        {
        //            if (CurrVoucherDocNo.Length == 0) CurrVoucherDocNo = x.VoucherDocNo;

        //            if (CurrVoucherDocNo != x.VoucherDocNo) DNo = 1;

        //            if (i != 0) SQL.AppendLine(", ");

        //            SQL.AppendLine("(@DocNo__" + i.ToString() + ", @DNo__" + i.ToString() + ", @JournalDocNo__" + i.ToString() + ", ");
        //            SQL.AppendLine("@CreateBy, CurrentDateTime() ");
        //            SQL.AppendLine(") ");

        //            Sm.CmParam<String>(ref cm, "@DocNo__" + i.ToString(), x.VoucherDocNo);
        //            Sm.CmParam<String>(ref cm, "@DNo__" + i.ToString(), Sm.Right(string.Concat("000", (DNo).ToString()), 3));
        //            Sm.CmParam<String>(ref cm, "@JournalDocNo__" + i.ToString(), x.JournalDocNo);

        //            i += 1;
        //            DNo += 1;
        //        }

        //    }

        //    SQL.AppendLine("; ");


        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    cm.CommandText = SQL.ToString();

        //    return cm;
        //}

        //private MySqlCommand SaveJournalInterOfficeCostCenterSwitchingBank(
        //   string DocNo, string JNDocNo, bool IsEdgeData, int index, string CCCode, int iteration,
        //   string VoucherRequestDocNo, string BankAcCode, string DocDt, string MenuCode,
        //   decimal AmtJN
        //   )
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("/*SaveJournalInterOfficeCostCenterSwitchingBank - Voucher Multi*/ ");
        //    SQL.AppendLine("Set @Row:=0; ");

        //    SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, ");
        //    SQL.AppendLine("CCCode, ");
        //    SQL.AppendLine("Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @JNDocNo, ");
        //    SQL.AppendLine("DocDt, ");
        //    SQL.AppendLine("Concat('Journal Inter Office : ', @DocNo) As JnDesc, ");
        //    SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
        //    SQL.AppendLine("@CCCode As CCCode, ");
        //    SQL.AppendLine("Remark, CreateBy, CreateDt ");
        //    SQL.AppendLine("From TblVoucherHdr Where DocNo=@DocNo; ");

        //    SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
        //    SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
        //    SQL.AppendLine("From TblJournalHdr A ");
        //    SQL.AppendLine("Inner Join ( ");
        //    SQL.AppendLine("    Select AcNo, DAmt DAmt, CAmt CAmt From ( ");

        //    if (IsEdgeData)
        //    {
        //        if (index == 0)
        //        {
        //            //RAK debit, Bank credit
        //            SQL.AppendLine("Select COAAcNoInterOffice AcNo, @AmtJN As DAmt, 0.00 As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //            SQL.AppendLine("Union All ");
        //            SQL.AppendLine("Select COAAcNo AcNo, 0.00 As DAmt, @AmtJN As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //        }
        //        else // data index terakhir
        //        {
        //            //bank debit, RAK credit
        //            SQL.AppendLine("Select COAAcNo AcNo, @AmtJN DAmt, 0.00 As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //            SQL.AppendLine("Union All ");
        //            SQL.AppendLine("Select COAAcNoInterOffice AcNo, 0.00 As DAmt, @AmtJN As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //        }
        //    }
        //    else
        //    {
        //        if (index % 2 == 0) //data baris genap
        //        {
        //            //RAK debit, bank credit
        //            SQL.AppendLine("Select COAAcNoInterOffice AcNo, @AmtJN As DAmt, 0.00 As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //            SQL.AppendLine("Union All ");
        //            SQL.AppendLine("Select COAAcNo AcNo, 0.00 As DAmt, @AmtJN As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //        }
        //        else //data baris ganjil
        //        {
        //            //bank debit, RAK credit
        //            SQL.AppendLine("Select COAAcNo AcNo, @AmtJN DAmt, 0.00 As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //            SQL.AppendLine("Union All ");
        //            SQL.AppendLine("Select COAAcNoInterOffice AcNo, 0.00 As DAmt, @AmtJN As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //        }
        //    }

        //    SQL.AppendLine("    ) Tbl ");
        //    SQL.AppendLine("    Where AcNo Is Not Null ");
        //    SQL.AppendLine(") B On 1=1 ");
        //    SQL.AppendLine("Where A.DocNo = @JNDocNo; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@JNDocNo", JNDocNo);
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
        //    Sm.CmParam<Decimal>(ref cm, "@AmtJN", AmtJN);
        //    Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);
        //    Sm.CmParamDt(ref cm, "@DocDt", DocDt);
        //    Sm.CmParam<String>(ref cm, "@MenuCode", MenuCode);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveJournalInterOfficeCostCenterSwitchingCC(
        //  string DocNo, string JNDocNo, bool IsEdgeData, int index, string CCCode, int iteration,
        //  string VoucherRequestDocNo, string BankAcCode, string DocDt, string MenuCode,
        //  decimal AmtJN
        //  )
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("/*SaveJournalInterOfficeCostCenterSwitchingCC - Voucher Multi*/ ");
        //    SQL.AppendLine("Set @Row:=0; ");

        //    SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, ");
        //    SQL.AppendLine("CCCode, ");
        //    SQL.AppendLine("Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @JNDocNo, ");
        //    SQL.AppendLine("DocDt, ");
        //    SQL.AppendLine("Concat('Journal Inter Office : ', @DocNo) As JnDesc, ");
        //    SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
        //    SQL.AppendLine("@CCCode As CCCode, ");
        //    SQL.AppendLine("Remark, CreateBy, CreateDt ");
        //    SQL.AppendLine("From TblVoucherHdr Where DocNo=@DocNo; ");

        //    SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
        //    SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
        //    SQL.AppendLine("From TblJournalHdr A ");
        //    SQL.AppendLine("Inner Join ( ");
        //    SQL.AppendLine("    Select AcNo, DAmt DAmt, CAmt CAmt From ( ");

        //    if (IsEdgeData)
        //    {
        //        if (index == 0)
        //        {
        //            //RAK debit, Bank credit
        //            SQL.AppendLine("Select COAAcNoInterOffice AcNo, @AmtJN As DAmt, 0.00 As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //            SQL.AppendLine("Union All ");
        //            SQL.AppendLine("Select COAAcNo AcNo, 0.00 As DAmt, @AmtJN As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //        }
        //        else // data index terakhir
        //        {
        //            //bank debit, RAK credit
        //            SQL.AppendLine("Select COAAcNo AcNo, @AmtJN DAmt, 0.00 As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //            SQL.AppendLine("Union All ");
        //            SQL.AppendLine("Select COAAcNoInterOffice AcNo, 0.00 As DAmt, @AmtJN As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //        }
        //    }
        //    else
        //    {
        //        if (index % 2 == 0) //data baris genap
        //        {
        //            //RAK debit, bank credit
        //            SQL.AppendLine("Select COAAcNoInterOffice AcNo, @AmtJN As DAmt, 0.00 As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //            SQL.AppendLine("Union All ");
        //            SQL.AppendLine("Select COAAcNo AcNo, 0.00 As DAmt, @AmtJN As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //        }
        //        else //data baris ganjil
        //        {
        //            //bank debit, RAK credit
        //            SQL.AppendLine("Select COAAcNo AcNo, @AmtJN DAmt, 0.00 As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //            SQL.AppendLine("Union All ");
        //            SQL.AppendLine("Select COAAcNoInterOffice AcNo, 0.00 As DAmt, @AmtJN As CAmt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            SQL.AppendLine("Where BankAcCode = @BankAcCode ");
        //        }
        //    }

        //    SQL.AppendLine("    ) Tbl ");
        //    SQL.AppendLine("    Where AcNo Is Not Null ");
        //    SQL.AppendLine(") B On 1=1 ");
        //    SQL.AppendLine("Where A.DocNo = @JNDocNo; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@JNDocNo", JNDocNo);
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
        //    Sm.CmParam<Decimal>(ref cm, "@AmtJN", AmtJN);
        //    Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);
        //    Sm.CmParamDt(ref cm, "@DocDt", DocDt);
        //    Sm.CmParam<String>(ref cm, "@MenuCode", MenuCode);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand UpdateInterOfficeInd(string DocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("/*Update InterOfficeInd - Voucher*/ ");
        //    SQL.AppendLine("Update TblVoucherHdr Set ");
        //    SQL.AppendLine("    InterOfficeInd = 'Y' ");
        //    SQL.AppendLine("Where DocNo = @DocNo; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

        //    return cm;
        //}

        #endregion

        private bool IsSaveDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Voucher's date") ||
                (mIsVoucherUseCashType && Sm.IsLueEmpty(LueCashTypeCode, "Cash Type")) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count < 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 voucher request.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 0)) return false;
            }
            Sm.StdMsg(mMsgType.Warning, "No voucher request selected.");
            return true;
        }

        #endregion

        #endregion

        #region Additional Method

        //internal void SetLueCashTypeCode(ref LookUpEdit Lue, string CashTypeCode)
        //{
        //    Sm.SetLue2(ref Lue, "Select CashTypeCode As Col1, CashTypeName As Col2 From TblCashType Order By CashTypeName;", 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //    if (CashTypeCode.Length != 0) Sm.SetLue(Lue, CashTypeCode);
        //}

        internal void SetLueCashTypeCode(ref LookUpEdit Lue, string CashTypeCode, string CashTypeGrp)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select CashTypeCode As Col1, CashTypeName As Col2 ");
            SQL.AppendLine("From TblCashType A ");
            if (CashTypeCode.Length > 0)
                SQL.AppendLine("Where A.CashTypeCode = @CashTypeCode ");
            else
            {
                if (mIsVoucherUseCashTypeGroup)
                    SQL.AppendLine("Where CashTypeGrpCode = @CashTypeGrp And A.`Level` = (SELECT MAX(`Level`) FROM tblcashtype WHERE CashTypeGrpCode = @CashTypeGrp) ");
            }
            SQL.AppendLine("Order By CashTypeName ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CashTypeGrp", CashTypeGrp);
            Sm.CmParam<String>(ref cm, "@CashTypeCode", CashTypeCode);

            Sm.SetLue2(
                ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");

            if (CashTypeCode.Length != 0) Sm.SetLue(Lue, CashTypeCode);
        }

        public static void SetLueCashTypeGroup(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CashTypeGrpCode As Col1, CashTypeGrpName As Col2 From TblCashTypeGroup Order By CashTypeGrpName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsVoucherMultiUseProfitCenter) return;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                    SQL.AppendLine("    And (" + Filter + ") ");
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private bool GetCostCenterSwitchingBankAccount(string BankAcCode, string BankAcCode2)
        {
            if (mIsVoucherCreateInterOfficeJournal && mVoucherInterOfficeJournalFormat == "2")
            {
                CCCodeBankAccount = GetCCCodeBankAccount(BankAcCode);
                CCCodeBankAccount2 = GetCCCodeBankAccount(BankAcCode2);

                if (CCCodeBankAccount.Trim().Length == 0 || CCCodeBankAccount2.Trim().Length == 0) return false;

                return CCCodeBankAccount != CCCodeBankAccount2;
            }

            return false;
        }
        private string GetCCCodeBankAccount(string BankAcCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode ");
            SQL.AppendLine("From TblBankAccount ");
            SQL.AppendLine("Where BankAcCode = @Param ");
            SQL.AppendLine("; ");

            return Sm.GetValue(SQL.ToString(), BankAcCode);
        }


        #endregion 

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                if (mIsVoucherCreateInterOfficeJournal && mVoucherInterOfficeJournalFormat == "2")
                {
                    string PairedVRDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                    if (e.ColIndex == 0)
                    {
                        if (Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length > 0)
                        {
                            for (int r = 0; r < Grd1.Rows.Count; r++)
                            {
                                if (Sm.GetGrdStr(Grd1, r, 1) == PairedVRDocNo)
                                {
                                    Grd1.Cells[r, 0].Value = Sm.GetGrdBool(Grd1, e.RowIndex, 0);
                                    if (Sm.GetGrdStr(Grd1, r, 17) != "C" ||
                                        !Sm.GetGrdBool(Grd1, r, 0))
                                        Grd1.Cells[r, 20].Value = "N";
                                    if (Sm.GetGrdStr(Grd1, r, 17) == "C" &&
                                        Sm.GetGrdBool(Grd1, r, 0))
                                        Grd1.Cells[r, 20].Value = "Y";
                                }
                            }

                            if (Sm.GetGrdBool(Grd1, e.RowIndex, 0) && Sm.GetGrdStr(Grd1, e.RowIndex, 17)=="C") 
                                Grd1.Cells[e.RowIndex, 20].Value = "Y";

                            if (!(Sm.GetGrdBool(Grd1, e.RowIndex, 0) &&
                                   Sm.GetGrdStr(Grd1, e.RowIndex, 17) == "C"))
                                Grd1.Cells[e.RowIndex, 20].Value = "N";
                        }

                    }
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                string mVRMenuCode = string.Empty, mDocType = string.Empty;
                int Row = Grd1.CurRow.Index;

                mDocType = Sm.GetGrdStr(Grd1, Row, 13);
                //if (mDocType == "70")
                //    mVRMenuCode = Sm.GetParameter("MenuCodeForVRBudget");
                //else
                //    mVRMenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmVoucherRequest' And MenuCode Not In (Select ParValue From TblParameter Where ParCode = 'MenuCodeForVRBudget') Limit 1;");
                var f = new FrmVoucherRequest("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                string mVRMenuCode = string.Empty, mDocType = string.Empty;
                int Row = Grd1.CurRow.Index;

                mDocType = Sm.GetGrdStr(Grd1, Row, 13);
                //if (mDocType == "70")
                //    mVRMenuCode = Sm.GetParameter("MenuCodeForVRBudget");
                //else
                //    mVRMenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmVoucherRequest' And MenuCode Not In (Select ParValue From TblParameter Where ParCode = 'MenuCodeForVRBudget') Limit 1;");
                var f = new FrmVoucherRequest("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 0);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    Grd1.Cells[r, 0].Value = !IsSelected;
                    if (mIsVoucherCreateInterOfficeJournal && mVoucherInterOfficeJournalFormat == "2")
                    {
                        string PairedVRDocNo = Sm.GetGrdStr(Grd1, r, 19);
                        if (e.ColIndex == 0)
                        {
                            if (Sm.GetGrdStr(Grd1, r, 19).Length > 0)
                            {
                                for (int i = 0; i< Grd1.Rows.Count; i++)
                                {
                                    if (Sm.GetGrdStr(Grd1, i, 1) == PairedVRDocNo)
                                    {
                                        Grd1.Cells[i, 0].Value = Sm.GetGrdBool(Grd1, r, 0);
                                        if (Sm.GetGrdStr(Grd1, i, 17) != "C" ||
                                        !Sm.GetGrdBool(Grd1, i, 0))
                                            Grd1.Cells[i, 20].Value = "N";
                                        if (Sm.GetGrdStr(Grd1, i, 17) == "C" &&
                                        Sm.GetGrdBool(Grd1, i, 0))
                                            Grd1.Cells[i, 20].Value = "Y";
                                    }
                                }
                                if (Sm.GetGrdBool(Grd1, r, 0) && 
                                    Sm.GetGrdStr(Grd1, r, 17) == "C")
                                    Grd1.Cells[r, 20].Value = "Y";
                                if (!(Sm.GetGrdBool(Grd1, r, 0) &&
                                    Sm.GetGrdStr(Grd1, r, 17) == "C"))
                                    Grd1.Cells[r, 20].Value = "N";
                            }
                        }
                    }
                }   
            }
        }

        private void LueCashTypeGroup_EditValueChanged(object sender, EventArgs e)
        {
            if (!BtnSave.Enabled) return;
            try
            {
                if (mIsVoucherUseCashTypeGroup)
                {
                    Sm.RefreshLookUpEdit(LueCashTypeGroup, new Sm.RefreshLue1(SetLueCashTypeGroup));
                    LueCashTypeCode.EditValue = null;
                    string CashTypeGrp = Sm.GetLue(LueCashTypeGroup);

                    if (CashTypeGrp.Length != 0)
                    {
                        LueCashTypeCode.EditValue = "<Refresh>";
                        Sm.RefreshLookUpEdit(LueCashTypeCode, new Sm.RefreshLue3(SetLueCashTypeCode), "", CashTypeGrp);
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCashTypeCode }, false);
                    }
                    else
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCashTypeCode }, true);
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        //private void LueCashTypeCode_EditValueChanged(object sender, EventArgs e)
        //{
        //    Sm.RefreshLookUpEdit(LueCashTypeCode, new Sm.RefreshLue2(SetLueCashTypeCode), string.Empty);
        //}

        private void LueCashTypeCode_EditValueChanged(object sender, EventArgs e)
        {
            if (!BtnSave.Enabled) return;
            try
            {
                if (mIsVoucherUseCashTypeGroup)
                    Sm.RefreshLookUpEdit(LueCashTypeCode, new Sm.RefreshLue3(SetLueCashTypeCode), string.Empty, Sm.GetLue(LueCashTypeGroup));
                else
                    Sm.RefreshLookUpEdit(LueCashTypeCode, new Sm.RefreshLue3(SetLueCashTypeCode), string.Empty, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit Center");
        }

        private void TxtCCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cost center");
        }

        private void TxtBCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Budget category");
        }

        private void TxtRemark_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkRemark_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Remark");
        }

        #endregion

        #endregion

        #region Class
        private class SwitchingBankAc
        {
            public int Voucher { get; set; }
            public int Journal { get; set; }
        }

        private class SwitchingCC
        {
            public int Voucher { get; set; }
            public int Journal { get; set; }
        }
        #endregion
    }
}
