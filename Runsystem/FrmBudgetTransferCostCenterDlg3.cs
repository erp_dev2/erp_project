﻿#region Update 
/*
    16/11/2021 [ICA/PHT] new apps
    13/05/2022 [WED/PHt] bug pembagian dengan angka 0    
 * */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetTransferCostCenterDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmBudgetTransferCostCenter mFrmParent;
        private string
            mSQL = string.Empty,
            mCCCode = string.Empty,
            mBCCode = string.Empty, 
            mMth = string.Empty, 
            mYr = string.Empty;
        private byte mDocType = 0;
        private int mCurRow = 0;

        #endregion

        #region Constructor

        public FrmBudgetTransferCostCenterDlg3(
            FrmBudgetTransferCostCenter FrmParent, 
            byte DocType,
            string CCCode, 
            string BCCode, 
            int CurRow, 
            string Mth, 
            string Yr)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocType = DocType;
            mCCCode = CCCode;
            mBCCode = BCCode;
            mCurRow = CurRow;
            mMth = Mth;
            mYr = Yr;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No",

                    //1-5
                    "Item's Code", 
                    "Item's Name", 
                    "Rate", 
                    "Quantity"
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    150, 250, 100, 100
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT B.ItCode, E.ItName, B.Rate"+mMth+" as Rate, B.Qty"+mMth+" As Qty ");
            SQL.AppendLine("FROM TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("INNER JOIN TblCompanyBudgetPlanDtl2 B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    AND A.CancelInd = 'N' ");
            SQL.AppendLine("    AND A.CCCode IS NOT NULL ");
            SQL.AppendLine("    AND A.CCCode = @CCCode ");
            SQL.AppendLine("    AND A.CompletedInd = 'Y' ");
            SQL.AppendLine("    AND A.Yr = @Yr ");
            SQL.AppendLine("INNER JOIN TblCostCategory C ON B.AcNo = C.AcNo AND C.CCCode = @CCCode ");
            SQL.AppendLine("INNER JOIN TblBudgetCategory D ON C.CCtCode = D.CCtCode ");
            SQL.AppendLine("INNER JOIN TblItem E ON B.ItCode = E.ItCode ");
            SQL.AppendLine("WHERE D.BCCode = @BCCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
                Sm.CmParam<String>(ref cm, "@BCCode", mBCCode);
                Sm.CmParam<String>(ref cm, "@Yr", mYr);

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "E.ItName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By E.ItName",
                    new string[]
                    {
                        "ItCode",
                        "ItName", "Rate", "Qty", 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                if (mDocType == 1)
                {
                    Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 15, Grd1, Row, 1);
                    Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 16, Grd1, Row, 2);
                    Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 17, Grd1, Row, 3);
                    Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 19, Grd1, Row, 4);
                    if (Sm.GetGrdDec(Grd1, Row, 3) != 0m)
                        mFrmParent.Grd1.Cells[mCurRow, 18].Value = Sm.GetGrdDec(mFrmParent.Grd1, mCurRow, 5) / Sm.GetGrdDec(Grd1, Row, 3);
                    else mFrmParent.Grd1.Cells[mCurRow, 18].Value = 0m;
                }
                else
                {
                    Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 21, Grd1, Row, 1);
                    Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 22, Grd1, Row, 2);
                    Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 23, Grd1, Row, 3);
                    Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 25, Grd1, Row, 4);
                    if (Sm.GetGrdDec(Grd1, Row, 3) != 0m)
                        mFrmParent.Grd1.Cells[mCurRow, 24].Value = Sm.GetGrdDec(mFrmParent.Grd1, mCurRow, 5) / Sm.GetGrdDec(Grd1, Row, 3);
                    else mFrmParent.Grd1.Cells[mCurRow, 24].Value = 0m;
                }
                this.Close();
            }
        }


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #region Additional Methods

        #endregion

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Items");
        }

        #endregion

        #endregion

    }
}
