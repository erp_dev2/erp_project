﻿#region Update
/*
    
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentSekuritas : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mSekuritasCode = string.Empty; //if this application is called from other application
        internal FrmInvestmentSekuritasFind FrmFind;
        internal bool
            mIsAutoJournalActived = false,
            mIsWarehouseUseSite = false,
            mIsWarehouseUsePOS = false,
            mIsMovingAvgEnabled = false;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmInvestmentSekuritas(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Finanvial Institution";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                Sl.SetLueCityCode(ref LueCityCode);

                //if this application is called from other application
                if (mSekuritasCode.Length != 0)
                {
                    ShowData(mSekuritasCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWhsCode, TxtWhsName, MeeAddress, LueCityCode, 
                        TxtPostalCd, TxtPhone, TxtFax, TxtEmail, TxtMobile, 
                        MeeContactPerson, TxtAcNo, TxtAcDesc, MeeRemark
                    }, true);
                    TxtWhsCode.Focus();                  
                    BtnAcNo.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWhsCode, TxtWhsName, MeeAddress, LueCityCode, 
                        TxtPostalCd, TxtPhone, TxtFax, TxtEmail, TxtMobile, 
                        MeeContactPerson,  MeeRemark, 
                    }, false);
                    TxtWhsCode.Focus();
                    BtnAcNo.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtWhsCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWhsName, MeeAddress, LueCityCode, TxtPostalCd, 
                        TxtPhone, TxtFax, TxtEmail, TxtMobile, MeeContactPerson, 
                        MeeRemark,
                    }, false);
                    TxtWhsName.Focus();
                    BtnAcNo.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtWhsCode, TxtWhsName, MeeAddress, LueCityCode, 
                TxtPostalCd, TxtPhone, TxtFax, TxtEmail, TxtMobile, 
                MeeContactPerson, TxtAcNo, TxtAcDesc, MeeRemark
            });
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAutoJournalActived', 'IsWarehouseUseSite', 'IsWarehouseUsePOS', 'IsMovingAvgEnabled' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsWarehouseUseSite": mIsWarehouseUseSite = ParValue == "Y"; break;
                            case "IsWarehouseUsePOS": mIsWarehouseUsePOS = ParValue == "Y"; break;
                            case "IsMovingAvgEnabled": mIsMovingAvgEnabled = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }
        
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmInvestmentSekuritasFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
          
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWhsCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWhsCode, string.Empty, false) || 
                Sm.StdMsgYN("Delete", string.Empty) == DialogResult.No )
                return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblInvestmentSekuritas Where SekuritasCode=@SekuritasCode" };
                Sm.CmParam<String>(ref cm, "@SekuritasCode", TxtWhsCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveInvestmentSekuritas());

                Sm.ExecCommands(cml);

                ShowData(TxtWhsCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }


        #endregion

        #region Show Data

        public void ShowData(string SekuritasCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();
                
                Sm.CmParam<String>(ref cm, "@SekuritasCode", SekuritasCode);

                SQL.AppendLine("Select A.SekuritasCode, A.SekuritasName, A.Address, A.CityCode, A.PostalCd, ");
                SQL.AppendLine("A.Phone, A.Fax, A.Email, A.Mobile, A.ContactPerson, A.Acno, B.AcDesc, A.Remark ");
                SQL.AppendLine("From TblInvestmentSekuritas A ");
                SQL.AppendLine("Left Join TblCoa B On A.AcNo=B.AcNo ");
                SQL.AppendLine("Where A.SekuritasCode=@SekuritasCode;");

                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                            "SekuritasCode",
                            "SekuritasName", "Address", "CityCode", "PostalCd", "Phone",
                            "Fax", "Email", "Mobile", "ContactPerson", "AcNo",
                            "AcDesc", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtWhsCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtWhsName.EditValue = Sm.DrStr(dr, c[1]);
                            MeeAddress.EditValue = Sm.DrStr(dr, c[2]);
                            Sm.SetLue(LueCityCode, Sm.DrStr(dr, c[3]));
                            TxtPostalCd.EditValue = Sm.DrStr(dr, c[4]);
                            TxtPhone.EditValue = Sm.DrStr(dr, c[5]);
                            TxtFax.EditValue = Sm.DrStr(dr, c[6]);
                            TxtEmail.EditValue = Sm.DrStr(dr, c[7]);
                            TxtMobile.EditValue = Sm.DrStr(dr, c[8]);
                            MeeContactPerson.EditValue = Sm.DrStr(dr, c[9]);
                            TxtAcNo.EditValue = Sm.DrStr(dr, c[10]);
                            TxtAcDesc.EditValue = Sm.DrStr(dr, c[11]);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                        }, true
                    );

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtWhsCode, "Institution code", false) ||
                Sm.IsTxtEmpty(TxtWhsName, "Institution name", false) ||
                Sm.IsLueEmpty(LueCityCode, "City") ||
                IsSekuritasCodeExisted()
                ;
        }

        private bool IsSekuritasCodeExisted()
        {
            if (!TxtWhsCode.Properties.ReadOnly && 
                Sm.IsDataExist("Select 1 From TblInvestmentSekuritas Where SekuritasCode='" + TxtWhsCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Institution code ( " + TxtWhsCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }


        private MySqlCommand SaveInvestmentSekuritas()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblInvestmentSekuritas(SekuritasCode, SekuritasName, ");
            SQL.AppendLine("Address, CityCode, PostalCd, Phone, fax, Email, Mobile, ContactPerson,  AcNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SekuritasCode, @SekuritasName, ");
            SQL.AppendLine("@Address, @CityCode, @PostalCd, @Phone, @fax, @Email, @Mobile, @ContactPerson, @AcNo,  @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update ");
            SQL.AppendLine("   SekuritasName=@SekuritasName, ");
            SQL.AppendLine("   Address=@Address, CityCode=@CityCode, PostalCd=@PostalCd, Phone=@Phone, fax=@fax, Email=@Email, Mobile=@Mobile, ");
            SQL.AppendLine("   ContactPerson=@ContactPerson, AcNo=@AcNo, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SekuritasCode", TxtWhsCode.Text);
            Sm.CmParam<String>(ref cm, "@SekuritasName", TxtWhsName.Text);
            Sm.CmParam<String>(ref cm, "@Address", MeeAddress.Text);
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCityCode));
            Sm.CmParam<String>(ref cm, "@PostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@Phone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@Fax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@Email", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@Mobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@ContactPerson", MeeContactPerson.Text);
            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
           
            return cm;
        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtWhsCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWhsCode);
        }

        private void TxtWhsName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWhsName);
        }

        private void LueCityCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue1(Sl.SetLueCityCode));
        }

        private void TxtPostalCd_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPostalCd);
        }

        private void TxtPhone_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPhone);
        }

        private void TxtFax_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtFax);
        }

        private void TxtEmail_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtEmail);
        }

        private void TxtMobile_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtMobile);
        }



        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmInvestmentSekuritasDlg(this, true, 0));
        }


        #endregion

        #endregion
      
    }
}
