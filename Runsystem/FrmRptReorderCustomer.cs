﻿#region Update
// 21/07/2017 [HAR] tambah informasi sipping name
#endregion


#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptReorderCustomer : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptReorderCustomer(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                //GetParameter();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, Sm.ServerCurrentDateTime().Substring(0, 4));
                Sl.SetLueSPCode(ref LueSPCode);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "Sales Code", 
                       
                        //1-5
                        "Sales person",
                        "Customer",
                        "Shipping Name",
                        "Shipping Address",
                        "City",
                       
                        //6-10
                        "Total",
                        "January",
                        "February",
                        "March",
                        "April",
                        //11-15
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        //16-19
                        "October",
                        "November",
                        "December"
                    },
                    new int[] 
                    {
                        //0
                        50, 
                        //1-5
                        120, 180, 200, 350, 150,  
                        //6-10
                        80, 100, 100, 100, 100,  
                        //11-15
                        100, 100, 100, 100, 100,  
                        //16-18
                        100, 100, 100, 
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Sm.IsLueEmpty(LueYr, "Year")) return;

            var lReoCustomer = new List<ReoCustomer>();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref lReoCustomer);
                if (lReoCustomer.Count> 0)
                {
                    Process3(ref lReoCustomer);
                    Process4(ref lReoCustomer);
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                lReoCustomer.Clear();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void Process1(ref List<ReoCustomer> l)
        {
            string Filter = " And 0=0 ";

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct X.SpCode, X.Spname, X.CtCode, X.CtName, X.SACityCode, X.CiTyname, X.SAAddress, X.SAName, X.Yr ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct * From (  ");
            SQL.AppendLine("        Select A.SpCode, B.Spname, A.CtCode, C.CtName, A.SACityCode, D.CiTyname, A.SAAddress, A.Yr, A.mth, A.SAname, Count(A.DocNo) CountData ");
            SQL.AppendLine("        From ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select A.SpCode, A.SAAddress, A.SACityCode, A.CtCode, left(A.DocDt, 4) As Yr, SubString(A.DocDt, 5, 2) As mth,  ");
            SQL.AppendLine("            A.DocNo, A.SAName  ");
            SQL.AppendLine("            From TblSOHdr A ");
            SQL.AppendLine("            Inner Join ");
            SQL.AppendLine("            ( ");
            SQL.AppendLine("                Select Distinct DocNo  ");
            SQL.AppendLine("                From TblSoDtl A ");
            SQL.AppendLine("                Inner Join ( ");
            SQL.AppendLine("                    Select C.SoDocNo, C.SODno  ");
            SQL.AppendLine("                    From TblDocT2hdr A ");
            SQL.AppendLine("                    Inner Join TblDoCt2Dtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("                    Inner Join TblDRDtl C On A.DRDocNo = C.DocNo And B.DRDno = C.Dno  ");
            SQL.AppendLine("                )B On A.DocNo = B.SoDocNo And A.Dno = B.SoDno ");
            SQL.AppendLine("                Where A.processind2 = 'F' ");
            SQL.AppendLine("            )B On A.Docno = B.DocNo ");
            SQL.AppendLine("            Where A.CancelInd = 'N'  ");
            SQL.AppendLine("        )A ");
            SQL.AppendLine("        Inner Join TblSalesperson B On A.SpCode = B.SpCode ");
            SQL.AppendLine("        Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            SQL.AppendLine("        Inner JOin TblCity D On A.SaCityCode = D.CityCode ");
            SQL.AppendLine("        Where A.Yr=@Yr ");
            SQL.AppendLine("        Group By A.SpCode, A.SAAddress, D.CiTYName, A.SACityCode, A.CtCode, C.Ctname, A.SAName,  A.yr, A.Mth ");
            SQL.AppendLine("        Order By A.SPCode, A.SAAddress ");
            SQL.AppendLine("    )Z having CountData > 1 ");
            SQL.AppendLine(")X ");
            SQL.AppendLine("Where X.SpCode In (Select SpCode From tblSalesPersonDtl Where UserCode = @UserCode ) ");
            
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "X.CtCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSPCode), "X.SPCode", true);

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString() + Filter + " Order By X.SPCode, X.SAAddress;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "SpCode",

                    //1-5
                    "Spname", "CtName", "SAName", "SAAddress", "CiTyname" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ReoCustomer()
                        {
                            SPCode = Sm.DrStr(dr, c[0]),
                            SPName = Sm.DrStr(dr, c[1]),
                            CtName = Sm.DrStr(dr, c[2]),
                            SAName = Sm.DrStr(dr, c[3]),
                            Address = Sm.DrStr(dr, c[4]),
                            CityName = Sm.DrStr(dr, c[5]),
                            Total = 0m,
                            Mth01 = 0m,
                            Mth02 = 0m,
                            Mth03 = 0m,
                            Mth04 = 0m,
                            Mth05 = 0m,
                            Mth06 = 0m,
                            Mth07 = 0m,
                            Mth08 = 0m,
                            Mth09 = 0m,
                            Mth10 = 0m,
                            Mth11 = 0m,
                            Mth12 = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<ReoCustomer> l)
        {
            string Filter = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.SpCode, B.Spname, A.CtCode, C.CtName, A.SACityCode, D.CiTyname, A.SAAddress, A.Yr, A.mth, Count(A.DocNo) CountData ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.SpCode, A.SAAddress, A.SACityCode, A.CtCode, left(A.DocDt, 4) As Yr, SubString(A.DocDt, 5, 2) As mth,  ");
            SQL.AppendLine("        A.DocNo  ");
            SQL.AppendLine("        From TblSOHdr A ");
            SQL.AppendLine("        Inner Join ");
	        SQL.AppendLine("        ( ");
		    SQL.AppendLine("            Select Distinct DocNo  ");
		    SQL.AppendLine("            From TblSoDtl A ");
		    SQL.AppendLine("            Inner Join ( ");
			SQL.AppendLine("                Select C.SoDocNo, C.SODno  ");
			SQL.AppendLine("                From TblDocT2hdr A ");
			SQL.AppendLine("                Inner Join TblDoCt2Dtl2 B On A.DocNo = B.DocNo ");
			SQL.AppendLine("                Inner Join TblDRDtl C On A.DRDocNo = C.DocNo And B.DRDno = C.Dno  ");
		    SQL.AppendLine("            )B On A.DocNo = B.SoDocNo And A.Dno = B.SoDno ");
		    SQL.AppendLine("            Where A.processind2 = 'F' ");
            SQL.AppendLine("        )B On A.Docno = B.DocNo ");
            SQL.AppendLine("        Where A.CancelInd = 'N' -- And A.Status = 'F' ");
            SQL.AppendLine("    )A ");
            SQL.AppendLine("    Inner Join TblSalesperson B On A.SpCode = B.SpCode ");
            SQL.AppendLine("    Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            SQL.AppendLine("    Inner JOin TblCity D On A.SaCityCode = D.CityCode ");
            SQL.AppendLine("    Where A.Yr=@Yr ");
            SQL.AppendLine("    Group By A.SpCode, A.SAAddress, D.CiTYName, A.SACityCode, A.CtCode, C.Ctname,  A.yr, A.Mth ");
            SQL.AppendLine("    Order By A.SPCode, A.SAAddress ");
            SQL.AppendLine(")Z having CountData > 1 ");

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SPCode", "SAAddress", "Yr", "Mth", "CountData" });
                if (dr.HasRows)
                {
                    var SpCode = string.Empty;
                    var SAAddress = string.Empty;
                    var Yr = string.Empty;
                    var Mth = string.Empty;
                    //var CountDays = 0m;
                    int Temp = 0;
                    while (dr.Read())
                    {
                        SpCode = Sm.DrStr(dr, c[0]);
                        SAAddress = Sm.DrStr(dr, c[1]);
                        Yr = Sm.DrStr(dr, c[2]);
                        for (int i = Temp; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].SPCode, SpCode) && Sm.CompareStr(l[i].Address, SAAddress) && Sm.CompareStr(Sm.GetLue(LueYr), Yr))
                            {
                                switch (Sm.DrStr(dr, c[3]))
                                {
                                    case "01":
                                        l[i].Mth01 = Sm.DrDec(dr, c[4]);
                                        l[i].Total += Sm.DrDec(dr, c[4]);
                                        break;
                                    case "02":
                                        l[i].Mth02 = Sm.DrDec(dr, c[4]);
                                        l[i].Total += Sm.DrDec(dr, c[4]);
                                        break;
                                    case "03":
                                        l[i].Mth03 = Sm.DrDec(dr, c[4]);
                                        l[i].Total += Sm.DrDec(dr, c[4]);
                                        break;
                                    case "04":
                                        l[i].Mth04 = Sm.DrDec(dr, c[4]);
                                        l[i].Total += Sm.DrDec(dr, c[4]);
                                        break;
                                    case "05":
                                        l[i].Mth05 = Sm.DrDec(dr, c[4]);
                                        l[i].Total += Sm.DrDec(dr, c[4]);
                                        break;
                                    case "06":
                                        l[i].Mth06 = Sm.DrDec(dr, c[4]);
                                        l[i].Total += Sm.DrDec(dr, c[4]);
                                        break;
                                    case "07":
                                        l[i].Mth07 = Sm.DrDec(dr, c[4]);
                                        l[i].Total += Sm.DrDec(dr, c[4]);
                                        break;
                                    case "08":
                                        l[i].Mth08 = Sm.DrDec(dr, c[4]);
                                        l[i].Total += Sm.DrDec(dr, c[4]);
                                        break;
                                    case "09":
                                        l[i].Mth09 = Sm.DrDec(dr, c[4]);
                                        l[i].Total += Sm.DrDec(dr, c[4]);
                                        break;
                                    case "10":
                                        l[i].Mth10 = Sm.DrDec(dr, c[4]);
                                        l[i].Total += Sm.DrDec(dr, c[4]);
                                        break;
                                    case "11":
                                        l[i].Mth11 = Sm.DrDec(dr, c[4]);
                                        l[i].Total += Sm.DrDec(dr, c[4]);
                                        break;
                                    case "12":
                                        l[i].Mth12 = Sm.DrDec(dr, c[4]);
                                        l[i].Total += Sm.DrDec(dr, c[4]);
                                        break;
                                }
                                Temp = i;
                                break;
                            }
                        }

                    }
                }
                dr.Close();
            }
        }

        private void Process4(ref List<ReoCustomer> l)
        {
            var Yr = Sm.GetLue(LueYr);
            iGRow r;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                //r.Cells[0].Value = i + 1;
                r.Cells[0].Value = l[i].SPCode;
                r.Cells[1].Value = l[i].SPName;
                r.Cells[2].Value = l[i].CtName;
                r.Cells[3].Value = l[i].SAName;
                r.Cells[4].Value = l[i].Address;
                r.Cells[5].Value = l[i].CityName;
                r.Cells[6].Value = l[i].Total;
                r.Cells[7].Value = l[i].Mth01;
                r.Cells[8].Value = l[i].Mth02;
                r.Cells[9].Value = l[i].Mth03;
                r.Cells[10].Value = l[i].Mth04;
                r.Cells[11].Value = l[i].Mth05;
                r.Cells[12].Value = l[i].Mth06;
                r.Cells[13].Value = l[i].Mth07;
                r.Cells[14].Value = l[i].Mth08;
                r.Cells[15].Value = l[i].Mth09;
                r.Cells[16].Value = l[i].Mth10;
                r.Cells[17].Value = l[i].Mth11;
                r.Cells[18].Value = l[i].Mth12;
            }
            Grd1.EndUpdate();
            l.Clear();
            AdjustSubtotals();
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6,7,8,9,10,11,12,13,14,15,16,17,18 });
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion
   

        #endregion

        #endregion

        #region Event
        private void ChkSPCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Sales person");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(Sl.SetLueSPCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

        #region class

        private class ReoCustomer
        {
            public string SPCode { get; set; }
            public string SPName { get; set; }
            public string CtName { get; set; }
            public string CityName { get; set; }
            public string Address { get; set; }
            public decimal Total { get; set; }
            public decimal Mth01 { get; set; }
            public decimal Mth02 { get; set; }
            public decimal Mth03 { get; set; }
            public decimal Mth04 { get; set; }
            public decimal Mth05 { get; set; }
            public decimal Mth06 { get; set; }
            public decimal Mth07 { get; set; }
            public decimal Mth08 { get; set; }
            public decimal Mth09 { get; set; }
            public decimal Mth10 { get; set; }
            public decimal Mth11 { get; set; }
            public decimal Mth12 { get; set; }
            public string SAName { get; set; }
        }

        #endregion
    }
}
