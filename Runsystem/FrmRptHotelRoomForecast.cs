﻿#region Update
    // 21/09/2017 [TKG] reporting Hotel's Other Forecast.
    // 25/09/2017 [TKG] Revisi tampilan kolom
    // 29/09/2017 [ARI] tambah Printout
    // 17/10/2017 [ARI] tambah kolom month
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptHotelRoomForecast : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptHotelRoomForecast(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Date",
                        "Month",
                        "Posting",
                        "Section",
                        "Name",
                        
                        //6-10
                        "Room Type",
                        "Total Room",
                        "Reservation",
                        "Room"+Environment.NewLine+"Night",
                        "ADR",

                        //11-15
                        "ARR",
                        "Gross Revenue",
                        "Nett Revenue",
                        "RevPar",
                        "Cost",

                        //16
                        "Average Cost"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 100, 200, 200, 150,

                        //6-10
                        150, 120, 120, 120, 120, 

                        //11-15
                        120, 120, 120, 120, 120, 
                      
                        //16
                        120
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9 }, 3);
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 12, 13, 14, 15, 16 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 4 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteDocDt2));

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select Date, Date_Format(date,'%M')As Month, Posting, Section, Name, Room_Type, Total_Room, RSV, " +
                    "Room_Night, Adr, Arr, Revenue, Nett_Revenue, RevPar, Cost, Average_Cost " +
                    "From TblRoomForecasts Where Date Between @Dt1 And @Dt2 Order By Date Desc;",
                    new string[]
                    { 
                        //0
                        "Date", 

                        //1-5
                        "Month", "Posting", "Section", "Name", "Room_Type", 

                        //6-10
                        "Total_Room", "RSV", "Room_Night", "Adr", "Arr", 

                        //11-15
                        "Revenue", "Nett_Revenue", "RevPar", "Cost", "Average_Cost"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional

        override protected void PrintData()
        {
            ParPrint();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "No Data displayed");
                return true;
            }
            return false;
        }

        private bool IsGrdValueInvalid()
        {
            if (Grd1.Rows.Count > 0)
            {
                if (Sm.GetGrdStr(Grd1, 0, 1).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "No Data displayed");
                    return true;
                }
            }
            return false;
        }

        private void ParPrint()
        {
            if (Sm.IsDteEmpty(DteDocDt1, "Start Date") || Sm.IsDteEmpty(DteDocDt2, "End Date") || Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) || IsGrdEmpty() || IsGrdValueInvalid() || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<RoomForecast>();
            var ldtl = new List<RoomForecastDtl>();
            string[] TableName = { "RoomForecast", "RoomForecastDtl" };

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2',");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where Menucode=@MenuCode) As MenuDesc ");
           
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
                //Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteDocDt1));
                //Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteDocDt2));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                        "CompanyLogo",

                         //1-5
                        "CompanyName",
                        "CompanyAddress",
                        "CompanyPhone",
                        "MenuDesc",
                       

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RoomForecast()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            MnuDesc = Sm.DrStr(dr, c[4]),
                            DocDt = String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDocDt1))),
                            DocDt2 = String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDocDt2))),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select Date, Date_Format(Date,'%d/%b/%Y')As Dates, Name, Room_Type, Total_Room, Rsv, Room_Night, Adr, ");
                SQLDtl.AppendLine("Arr, Revenue, Nett_Revenue, RevPar, Cost, Average_Cost From TblRoomForecasts ");
                SQLDtl.AppendLine("Where Date Between @Dt1 And @Dt2 Order by Date Desc; ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParamDt(ref cmDtl, "@Dt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cmDtl, "@Dt2", Sm.GetDte(DteDocDt2));
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                {
                    //0
                    "Dates",

                    //1-5
                    "Name",
                    "Room_Type",
                    "Total_Room",
                    "Rsv",
                    "Room_Night",
                    
                    //11-15
                    "Adr",
                    "Arr",
                    "Revenue",
                    "Nett_Revenue",
                    "RevPar",
                    
                    //16
                    "Cost",
                    "Average_Cost"
                   
                });

                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new RoomForecastDtl()
                        {
                            nomor = nomor,
                            Date = Sm.DrStr(drDtl, cDtl[0]),
                            Name = Sm.DrStr(drDtl, cDtl[1]),

                            RoomType = Sm.DrStr(drDtl, cDtl[2]),
                            TotalRoom = Sm.DrDec(drDtl, cDtl[3]),
                            Reservation = Sm.DrDec(drDtl, cDtl[4]),
                            RoomNight = Sm.DrDec(drDtl, cDtl[5]),
                            ADR = Sm.DrDec(drDtl, cDtl[6]),

                            ARR = Sm.DrDec(drDtl, cDtl[7]),
                            GrossRevenue = Sm.DrDec(drDtl, cDtl[8]),
                            NettRevenue = Sm.DrDec(drDtl, cDtl[9]),
                            RevPar = Sm.DrDec(drDtl, cDtl[10]),
                            Cost = Sm.DrDec(drDtl, cDtl[11]),

                            AverageCost = Sm.DrDec(drDtl, cDtl[12]),
                        });
                    }
                }

                drDtl.Close();
            }

            myLists.Add(ldtl);

            #endregion

            Sm.PrintReport("RoomForecast", myLists, TableName, false);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion

        #region Report Class

        class RoomForecast
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string MnuDesc { get; set; }
            public string DocDt { get; set; }
            public string DocDt2 { get; set; }
            public string PrintBy { get; set; }
           
        }

        class RoomForecastDtl
        {
            public int nomor { get; set; }
            public string Date { get; set; }
            public string Name { get; set; }
            public string RoomType { get; set; }
            public decimal TotalRoom { get; set; }
            public decimal Reservation { get; set; }
            public decimal RoomNight { get; set; }
            public decimal ADR { get; set; }
            public decimal ARR { get; set; }
            public decimal GrossRevenue { get; set; }
            public decimal NettRevenue { get; set; }
            public decimal RevPar { get; set; }
            public decimal Cost { get; set; }
            public decimal AverageCost { get; set; }
           

        }

        #endregion
    }
}
