﻿#region Update
/*
 * 20/04/2022 [RDA/PRODUCT] new apps
 * 24/05/2022 [SET/PRODUCT] tambahan Last coupon date & Next coupon date
 *                          merubah Annual days assumption menjadi Lue
 * 27/06/2022 [IBL/PRODUCT] tambah parameter DebtInvestmentCtCode
 * 27/06/2022 [SET/PRODUCT] Tambah % untuk Interest Rate
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentItemDebt : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mDebtInvestmentCtCode = string.Empty;
        internal FrmInvestmentItemDebtFind FrmFind;
        internal bool mIsInsert = false;

        #endregion

        #region Constructor

        public FrmInvestmentItemDebt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Debt Securities Investment Item";

            try
            {
                GetParameter();

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnDelete.Visible = false;
                BtnPrint.Visible = false;
                SetFormControl(mState.View);

                base.FrmLoad(sender, e);
                //if this application is called from other application
                //if (mInvestmentCode.Length != 0)
                //{
                //    ShowData(mInvestmentCode);
                //    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible =
                //    BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = mIsAccessUsed;
                //}
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtInvestmentDebtCode, ChkActInd, TxtPortofolioId, TxtInvestmentName,
                        TxtForeignName, LueInvestmentCtCode, LueCurCode, TxtIssuer, LueInvestmentUomCode,
                        TxtIssuedAmt, TxtSpecification, DteUpdateDt, DteListingDt, DteMaturityDt,
                        TxtInterestRateAmt, TxtInterestType, TxtInterestFreq, MeeRemark, LueAnnualDays
                    }, true);
                    BtnPortofolio1.Enabled = false;
                    BtnPortofolio2.Enabled = false;
                    TxtInvestmentDebtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtForeignName, LueInvestmentUomCode, TxtSpecification, MeeRemark, LueAnnualDays
                    }, false);
                    BtnPortofolio1.Focus();
                    BtnPortofolio1.Enabled = true;
                    BtnPortofolio2.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        ChkActInd
                    }, false);
                    ChkActInd.Focus();
                    break;
            }
        }

        public void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtInvestmentDebtCode, ChkActInd, TxtPortofolioId, TxtInvestmentName,
                TxtForeignName, LueInvestmentCtCode, LueCurCode, TxtIssuer, LueInvestmentUomCode,
                TxtIssuedAmt, TxtSpecification, DteUpdateDt, DteListingDt, DteMaturityDt,
                TxtInterestRateAmt, TxtInterestType, TxtInterestFreq, MeeRemark, LueAnnualDays,
                DteLastCouponDt, DteNextCouponDt
            });

            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmInvestmentItemDebtFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                InsertDataClick();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtInvestmentDebtCode, "", false)) return;
            mIsInsert = false;
            SetFormControl(mState.Edit);
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (mIsInsert && TxtInvestmentDebtCode.Text == string.Empty)
                {
                    InsertData();
                }
                else
                    EditData(TxtInvestmentDebtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void BtnPortofolio_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmInvestmentItemDebtDlg(this));
        }

        private void BtnPortofolio2_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmInvestmentItemDebtDlg(this));
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPortofolioId, "Investment code", false) ||
                Sm.IsTxtEmpty(TxtInvestmentName, "Investment name", false) ||
                //Sm.IsTxtEmpty(TxtDaysAssumption, "Annual day assumption", true)
                Sm.IsLueEmpty(LueAnnualDays, "Annual days assumption");
                //Sm.IsLueEmpty(LueInvestmentCtCode, "Investment's category") ||
                //IsItCodeExisted();
        }

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;

            var SQL = new StringBuilder();
            var InvestmentDebtCode = GenerateInvestmentDebtCode();

            SQL.AppendLine("Insert Into TblInvestmentItemDebt( ");
            SQL.AppendLine("    InvestmentDebtCode,PortofolioId,PortofolioName,ActInd,ForeignName, ");
            SQL.AppendLine("    InvestmentCtCode,CurCode,Issuer,UomCode,IssuedAmt, ");
            SQL.AppendLine("    Specification,UpdatedDt,ListingDt,MaturityDt,InterestRateAmt, ");
            SQL.AppendLine("    InterestType,InterestFreq,AnnualDays, LastCouponDt, NextCouponDt, Remark, ");
            SQL.AppendLine("    CreateBy,CreateDt) ");
            SQL.AppendLine("Values( ");
            SQL.AppendLine("    @InvestmentDebtCode, @PortofolioId, @PortofolioName, @ActInd, @ForeignName,  ");
            SQL.AppendLine("    @InvestmentCtCode, @CurCode, @Issuer, @UomCode, @IssuedAmt,  ");
            SQL.AppendLine("    @Specification, @UpdatedDt, @ListingDt, @MaturityDt, @InterestRateAmt,  ");
            SQL.AppendLine("    @InterestType, @InterestFreq, @AnnualDays, @LastCouponDt, @NextCouponDt, @Remark,  ");
            SQL.AppendLine("    @CreateBy, CurrentDateTime())  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@InvestmentDebtCode", InvestmentDebtCode);
            Sm.CmParam<String>(ref cm, "@PortofolioId", TxtPortofolioId.Text);
            Sm.CmParam<String>(ref cm, "@PortofolioName", TxtInvestmentName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ForeignName", TxtForeignName.Text);
            Sm.CmParam<String>(ref cm, "@InvestmentCtCode", Sm.GetLue(LueInvestmentCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@Issuer", TxtIssuer.Text);
            Sm.CmParam<String>(ref cm, "@UomCode", Sm.GetLue(LueInvestmentUomCode));
            Sm.CmParam<Decimal>(ref cm, "@IssuedAmt", Decimal.Parse(TxtIssuedAmt.Text));
            Sm.CmParam<String>(ref cm, "@Specification", TxtSpecification.Text);
            Sm.CmParamDt(ref cm, "@UpdatedDt", Sm.GetDte(DteUpdateDt));
            Sm.CmParamDt(ref cm, "@ListingDt", Sm.GetDte(DteListingDt));
            Sm.CmParamDt(ref cm, "@MaturityDt", Sm.GetDte(DteMaturityDt));
            Sm.CmParam<Decimal>(ref cm, "@InterestRateAmt", Decimal.Parse(TxtInterestRateAmt.Text));
            Sm.CmParam<String>(ref cm, "@InterestType", TxtInterestType.Text);
            Sm.CmParam<String>(ref cm, "@InterestFreq", TxtInterestFreq.Text);
            Sm.CmParam<String>(ref cm, "@AnnualDays", Sm.GetLue(LueAnnualDays));
            Sm.CmParamDt(ref cm, "@LastCouponDt", Sm.GetDte(DteLastCouponDt));
            Sm.CmParamDt(ref cm, "@NextCouponDt", Sm.GetDte(DteNextCouponDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);

            ShowData(InvestmentDebtCode);
        }

        private void EditData(string InvestmentDebtCode)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsNonActiveDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblInvestmentItemDebt Set ");
            SQL.AppendLine("    ActInd=@ActInd, LastUpBy=@LastUpBy, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where InvestmentDebtCode=@InvestmentDebtCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LastUpBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@InvestmentDebtCode", InvestmentDebtCode);

            Sm.ExecCommand(cm);

            ShowData(InvestmentDebtCode);
        }

        private bool IsNonActiveDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtInvestmentDebtCode, "Investment Debt Code", false) ||
                IsDataInactiveAlready();
        }

        private bool IsDataInactiveAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblInvestmentItemDebt Where ActInd='N' And InvestmentDebtCode=@Param;",
                TxtInvestmentDebtCode.Text,
                "This data already inactive.");
        }

        private string GenerateInvestmentDebtCode()
        {
            var DocCode = string.Empty;
            var InvestmentCtCode = Sm.GetLue(LueInvestmentCtCode);
            if (mIsInsert && TxtInvestmentDebtCode.Text.Length == 0)
            {
                {
                    DocCode = Sm.GetValue(
                         "Select Concat(@Param1,'-', " +
                         "   IfNull((Select Right(Concat('00000', Convert(InvestmentDebtCode+1, Char)), 5) From ( " +
                         "       Select Convert(Right(InvestmentDebtCode, 5), Decimal) As InvestmentDebtCode " +
                         "      From TblInvestmentItemDebt " +
                         "      Where Left(InvestmentDebtCode, @Param2)=Concat(@Param1, '-') " +
                         "      Order By Right(InvestmentDebtCode, 5) Desc Limit 1 " +
                         "       ) As TblItemTemp), '00001')) As InvestmentDebtCode; ",
                         InvestmentCtCode, (InvestmentCtCode.Length + 1).ToString(), string.Empty);
                }
            }

            return DocCode;
        }

        #endregion

        #region Show Data

        public void ShowData(string InvestmentDebtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowItem(InvestmentDebtCode);

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowItem(string InvestmentDebtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.InvestmentDebtCode,A.PortofolioId,A.PortofolioName,A.ForeignName,A.ActInd, ");
            SQL.AppendLine("A.InvestmentCtCode,A.CurCode,A.Issuer,A.UomCode,A.IssuedAmt, ");
            SQL.AppendLine("A.Specification,A.UpdatedDt,A.ListingDt,A.MaturityDt,A.InterestRateAmt, ");
            SQL.AppendLine("A.InterestType,A.InterestFreq,A.AnnualDays, A.LastCouponDt, A.NextCouponDt, A.Remark ");
            SQL.AppendLine("FROM tblinvestmentitemdebt A ");
            SQL.AppendLine("WHERE A.InvestmentDebtCode=@InvestmentDebtCode ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@InvestmentDebtCode", InvestmentDebtCode);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    {
                        //0
                        "InvestmentDebtCode",
                        
                        //1-5
                        "PortofolioId","PortofolioName","ForeignName","ActInd","InvestmentCtCode",

                        //6-10
                        "CurCode","Issuer","UomCode","IssuedAmt","Specification",

                        //11-15
                        "UpdatedDt","ListingDt","MaturityDt","InterestRateAmt","InterestType",

                        //16-20
                        "InterestFreq","AnnualDays", "LastCouponDt", "NextCouponDt", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtInvestmentDebtCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtPortofolioId.EditValue = Sm.DrStr(dr, c[1]);
                        TxtInvestmentName.EditValue = Sm.DrStr(dr, c[2]);
                        TxtForeignName.EditValue = Sm.DrStr(dr, c[3]);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        SetLueInvestmentCtCode(ref LueInvestmentCtCode, Sm.DrStr(dr, c[5]));
                        SetLueCurCode(ref LueCurCode, Sm.DrStr(dr, c[6]));
                        TxtIssuer.EditValue = Sm.DrStr(dr, c[7]);
                        Sm.SetLue(LueInvestmentUomCode, Sm.DrStr(dr, c[8]));
                        TxtIssuedAmt.EditValue = Sm.DrDec(dr, c[9]);
                        TxtSpecification.EditValue = Sm.DrStr(dr, c[10]);
                        Sm.SetDte(DteUpdateDt, Sm.DrStr(dr, c[11]));
                        Sm.SetDte(DteListingDt, Sm.DrStr(dr, c[12]));
                        Sm.SetDte(DteMaturityDt, Sm.DrStr(dr, c[13]));
                        TxtInterestRateAmt.EditValue = Sm.DrDec(dr, c[14]);
                        TxtInterestType.EditValue = Sm.DrStr(dr, c[15]);
                        TxtInterestFreq.EditValue = Sm.DrStr(dr, c[16]);
                        Sm.SetLue(LueAnnualDays, Sm.DrStr(dr, c[17]));
                        Sm.SetDte(DteLastCouponDt, Sm.DrStr(dr, c[18]));
                        Sm.SetDte(DteNextCouponDt, Sm.DrStr(dr, c[19]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[20]);
                    }, true
                );
        }


        #endregion

        #region Additional Method

        internal protected void InsertDataClick()
        {
            mIsInsert = true;
            ClearData();
            SetFormControl(mState.Insert);

            SetLueInvestmentCtCode(ref LueInvestmentCtCode, string.Empty);
            SetLueCurCode(ref LueCurCode, string.Empty);
            Sl.SetLueUomCode(ref LueInvestmentUomCode);
            Sl.SetLueOption(ref LueAnnualDays, "AnnualDaysAssumption");
            Sm.SetLue(LueAnnualDays, "2");
            //Sm.SetDte(DteLastCouponDt, "00000000");
            //Sm.SetDte(DteNextCouponDt, "00000000");
            ChkActInd.Checked = true;
            //Sm.FormatNumTxt(TxtDaysAssumption, 0);
            Sm.FormatNumTxt(TxtInterestRateAmt, 0);
            Sm.FormatNumTxt(TxtIssuedAmt, 0);
        }

        internal void SetLueInvestmentCtCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.InvestmentCtCode As Col1, T.InvestmentCtName As Col2 From TblInvestmentCategory T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where (T.InvestmentCtCode=@Code Or ");
                SQL.AppendLine("(T.ActInd='Y' ");
                SQL.AppendLine(")) ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
            }
            SQL.AppendLine("Order By T.InvestmentCtName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        internal void SetLueCurCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.CurCode As Col1, T.CurName As Col2 From TblCurrency T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where T.CurCode=@Code ");
            }
            SQL.AppendLine("Order By T.CurCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void GetParameter()
        {
            mDebtInvestmentCtCode = Sm.GetParameter("DebtInvestmentCtCode");
        }


        #endregion

        #endregion

        #region Event

        private void LueAnnualDays_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAnnualDays, new Sm.RefreshLue2(Sl.SetLueOption), "AnnualDaysAssumption");
        }

        //private void TxtDaysAssumption_Validated(object sender, EventArgs e)
        //{
        //    Sm.FormatNumTxt(TxtDaysAssumption, 0);
        //}

        #endregion

    }
}
