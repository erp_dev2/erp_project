﻿namespace RunSystem
{
    partial class FrmVendor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgContactPerson = new System.Windows.Forms.TabPage();
            this.LueType = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgBank = new System.Windows.Forms.TabPage();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.LueLevel = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgItemCategory = new System.Windows.Forms.TabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgVdSector = new System.Windows.Forms.TabPage();
            this.LueSubSectorCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueQualification = new DevExpress.XtraEditors.LookUpEdit();
            this.LueSector = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgVdRating = new System.Windows.Forms.TabPage();
            this.LueVdRating = new DevExpress.XtraEditors.LookUpEdit();
            this.LueRating = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgUploadedFile = new System.Windows.Forms.TabPage();
            this.Grd8 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgProfile = new System.Windows.Forms.TabPage();
            this.LueCountry = new DevExpress.XtraEditors.LookUpEdit();
            this.LueGender = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeAddress2 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeExperience = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeLanguage = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCertificate = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEducation = new DevExpress.XtraEditors.MemoExEdit();
            this.LueBirthPlace = new DevExpress.XtraEditors.LookUpEdit();
            this.DteBirthDt = new DevExpress.XtraEditors.DateEdit();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgProject = new System.Windows.Forms.TabPage();
            this.LueProgressCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteContractEndDt = new DevExpress.XtraEditors.DateEdit();
            this.DteContractDt = new DevExpress.XtraEditors.DateEdit();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueProfile = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeProject = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgVdDeed = new System.Windows.Forms.TabPage();
            this.DteDeedDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteDeedDt = new DevExpress.XtraEditors.DateEdit();
            this.Grd9 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgVehicle = new System.Windows.Forms.TabPage();
            this.LueTransportType = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd10 = new TenTec.Windows.iGridLib.iGrid();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtRMVdCode = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtVdCodeInternal = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtEstablishedYr = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.LueParent = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtWebsite = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtShortName = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.BtnVendorCode = new DevExpress.XtraEditors.SimpleButton();
            this.LueSDCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueVilCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtPostalCd = new DevExpress.XtraEditors.TextEdit();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueCityCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.LblVdCtCode = new System.Windows.Forms.Label();
            this.LueVdCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtVdName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtVdCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSelectionScore = new DevExpress.XtraEditors.TextEdit();
            this.LblSelectionScore = new System.Windows.Forms.Label();
            this.TxtVdExternalCode = new DevExpress.XtraEditors.TextEdit();
            this.LblVdExternalCode = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.LueEntityType = new DevExpress.XtraEditors.LookUpEdit();
            this.LblProcurementStatus = new System.Windows.Forms.Label();
            this.LueProcurementStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtNIB = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.ChkCreateUserVendor = new DevExpress.XtraEditors.CheckEdit();
            this.BtnVendorRegister = new DevExpress.XtraEditors.SimpleButton();
            this.LblVendorRegister = new System.Windows.Forms.Label();
            this.ChkTaxInd = new DevExpress.XtraEditors.CheckEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtTIN = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtIdentityNo = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtCreditLimit = new DevExpress.XtraEditors.TextEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtMobile = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtPhone = new DevExpress.XtraEditors.TextEdit();
            this.TxtEmail = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtFax = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.TpgContactPerson.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.TpgBank.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).BeginInit();
            this.TpgItemCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpgVdSector.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSubSectorCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueQualification.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.TpgVdRating.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdRating.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueRating.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.TpgUploadedFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).BeginInit();
            this.TpgProfile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeExperience.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeLanguage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCertificate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEducation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBirthPlace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.TpgProject.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProgressCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProfile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.TpgVdDeed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeedDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeedDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeedDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeedDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).BeginInit();
            this.TpgVehicle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueTransportType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd10)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRMVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCodeInternal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEstablishedYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWebsite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSDCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVilCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSelectionScore.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdExternalCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntityType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcurementStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNIB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCreateUserVendor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTIN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdentityNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreditLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(889, 0);
            this.panel1.Size = new System.Drawing.Size(70, 547);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Location = new System.Drawing.Point(0, 120);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnCancel.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Location = new System.Drawing.Point(0, 96);
            this.BtnSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnSave.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Location = new System.Drawing.Point(0, 72);
            this.BtnDelete.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnDelete.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Location = new System.Drawing.Point(0, 48);
            this.BtnEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnEdit.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Location = new System.Drawing.Point(0, 24);
            this.BtnInsert.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnInsert.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnFind.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Location = new System.Drawing.Point(0, 144);
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnPrint.Size = new System.Drawing.Size(70, 24);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(889, 547);
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgContactPerson);
            this.tabControl1.Controls.Add(this.TpgBank);
            this.tabControl1.Controls.Add(this.TpgItemCategory);
            this.tabControl1.Controls.Add(this.TpgVdSector);
            this.tabControl1.Controls.Add(this.TpgVdRating);
            this.tabControl1.Controls.Add(this.TpgUploadedFile);
            this.tabControl1.Controls.Add(this.TpgProfile);
            this.tabControl1.Controls.Add(this.TpgProject);
            this.tabControl1.Controls.Add(this.TpgVdDeed);
            this.tabControl1.Controls.Add(this.TpgVehicle);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 332);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(889, 215);
            this.tabControl1.TabIndex = 63;
            // 
            // TpgContactPerson
            // 
            this.TpgContactPerson.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgContactPerson.Controls.Add(this.LueType);
            this.TpgContactPerson.Controls.Add(this.Grd1);
            this.TpgContactPerson.Location = new System.Drawing.Point(4, 26);
            this.TpgContactPerson.Name = "TpgContactPerson";
            this.TpgContactPerson.Size = new System.Drawing.Size(881, 185);
            this.TpgContactPerson.TabIndex = 3;
            this.TpgContactPerson.Text = "Contact Person";
            this.TpgContactPerson.UseVisualStyleBackColor = true;
            // 
            // LueType
            // 
            this.LueType.EnterMoveNextControl = true;
            this.LueType.Location = new System.Drawing.Point(130, 37);
            this.LueType.Margin = new System.Windows.Forms.Padding(5);
            this.LueType.Name = "LueType";
            this.LueType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.Appearance.Options.UseFont = true;
            this.LueType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueType.Properties.DropDownRows = 12;
            this.LueType.Properties.NullText = "[Empty]";
            this.LueType.Properties.PopupWidth = 500;
            this.LueType.Size = new System.Drawing.Size(171, 20);
            this.LueType.TabIndex = 64;
            this.LueType.ToolTip = "F4 : Show/hide list";
            this.LueType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueType.EditValueChanged += new System.EventHandler(this.LueType_EditValueChanged);
            this.LueType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueType_KeyDown);
            this.LueType.Leave += new System.EventHandler(this.LueType_Leave);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(881, 185);
            this.Grd1.TabIndex = 63;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // TpgBank
            // 
            this.TpgBank.Controls.Add(this.LueBankCode);
            this.TpgBank.Controls.Add(this.Grd2);
            this.TpgBank.Controls.Add(this.LueLevel);
            this.TpgBank.Location = new System.Drawing.Point(4, 26);
            this.TpgBank.Name = "TpgBank";
            this.TpgBank.Size = new System.Drawing.Size(764, 185);
            this.TpgBank.TabIndex = 4;
            this.TpgBank.Text = "Bank";
            this.TpgBank.UseVisualStyleBackColor = true;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(23, 41);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 12;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 500;
            this.LueBankCode.Size = new System.Drawing.Size(171, 20);
            this.LueBankCode.TabIndex = 64;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            this.LueBankCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueBankCode_KeyDown);
            this.LueBankCode.Leave += new System.EventHandler(this.LueBankCode_Leave);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(764, 185);
            this.Grd2.TabIndex = 63;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // LueLevel
            // 
            this.LueLevel.EnterMoveNextControl = true;
            this.LueLevel.Location = new System.Drawing.Point(72, 22);
            this.LueLevel.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel.Name = "LueLevel";
            this.LueLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.Appearance.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel.Properties.DropDownRows = 12;
            this.LueLevel.Properties.NullText = "[Empty]";
            this.LueLevel.Properties.PopupWidth = 500;
            this.LueLevel.Size = new System.Drawing.Size(210, 20);
            this.LueLevel.TabIndex = 37;
            this.LueLevel.ToolTip = "F4 : Show/hide list";
            this.LueLevel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TpgItemCategory
            // 
            this.TpgItemCategory.Controls.Add(this.Grd3);
            this.TpgItemCategory.Location = new System.Drawing.Point(4, 26);
            this.TpgItemCategory.Name = "TpgItemCategory";
            this.TpgItemCategory.Size = new System.Drawing.Size(764, 185);
            this.TpgItemCategory.TabIndex = 10;
            this.TpgItemCategory.Text = "Item\'s Category";
            this.TpgItemCategory.UseVisualStyleBackColor = true;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(764, 185);
            this.Grd3.TabIndex = 63;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // TpgVdSector
            // 
            this.TpgVdSector.Controls.Add(this.LueSubSectorCode);
            this.TpgVdSector.Controls.Add(this.LueQualification);
            this.TpgVdSector.Controls.Add(this.LueSector);
            this.TpgVdSector.Controls.Add(this.Grd4);
            this.TpgVdSector.Location = new System.Drawing.Point(4, 26);
            this.TpgVdSector.Name = "TpgVdSector";
            this.TpgVdSector.Size = new System.Drawing.Size(764, 185);
            this.TpgVdSector.TabIndex = 11;
            this.TpgVdSector.Text = "Vendor\'s Sector";
            this.TpgVdSector.UseVisualStyleBackColor = true;
            // 
            // LueSubSectorCode
            // 
            this.LueSubSectorCode.EnterMoveNextControl = true;
            this.LueSubSectorCode.Location = new System.Drawing.Point(421, 27);
            this.LueSubSectorCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSubSectorCode.Name = "LueSubSectorCode";
            this.LueSubSectorCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubSectorCode.Properties.Appearance.Options.UseFont = true;
            this.LueSubSectorCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubSectorCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSubSectorCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubSectorCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSubSectorCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubSectorCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSubSectorCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubSectorCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSubSectorCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSubSectorCode.Properties.DropDownRows = 12;
            this.LueSubSectorCode.Properties.NullText = "[Empty]";
            this.LueSubSectorCode.Properties.PopupWidth = 500;
            this.LueSubSectorCode.Size = new System.Drawing.Size(171, 20);
            this.LueSubSectorCode.TabIndex = 66;
            this.LueSubSectorCode.ToolTip = "F4 : Show/hide list";
            this.LueSubSectorCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSubSectorCode.EditValueChanged += new System.EventHandler(this.LueSubSectorCode_EditValueChanged);
            this.LueSubSectorCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueSubSectorCode_KeyDown);
            this.LueSubSectorCode.Leave += new System.EventHandler(this.LueSubSectorCode_Leave);
            // 
            // LueQualification
            // 
            this.LueQualification.EnterMoveNextControl = true;
            this.LueQualification.Location = new System.Drawing.Point(240, 27);
            this.LueQualification.Margin = new System.Windows.Forms.Padding(5);
            this.LueQualification.Name = "LueQualification";
            this.LueQualification.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQualification.Properties.Appearance.Options.UseFont = true;
            this.LueQualification.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQualification.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueQualification.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQualification.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueQualification.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQualification.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueQualification.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQualification.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueQualification.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueQualification.Properties.DropDownRows = 12;
            this.LueQualification.Properties.NullText = "[Empty]";
            this.LueQualification.Properties.PopupWidth = 500;
            this.LueQualification.Size = new System.Drawing.Size(171, 20);
            this.LueQualification.TabIndex = 65;
            this.LueQualification.ToolTip = "F4 : Show/hide list";
            this.LueQualification.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueQualification.EditValueChanged += new System.EventHandler(this.LueQualification_EditValueChanged);
            this.LueQualification.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueQualification_KeyDown);
            this.LueQualification.Leave += new System.EventHandler(this.LueQualification_Leave);
            // 
            // LueSector
            // 
            this.LueSector.EnterMoveNextControl = true;
            this.LueSector.Location = new System.Drawing.Point(59, 27);
            this.LueSector.Margin = new System.Windows.Forms.Padding(5);
            this.LueSector.Name = "LueSector";
            this.LueSector.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSector.Properties.Appearance.Options.UseFont = true;
            this.LueSector.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSector.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSector.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSector.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSector.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSector.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSector.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSector.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSector.Properties.DropDownRows = 12;
            this.LueSector.Properties.NullText = "[Empty]";
            this.LueSector.Properties.PopupWidth = 500;
            this.LueSector.Size = new System.Drawing.Size(171, 20);
            this.LueSector.TabIndex = 64;
            this.LueSector.ToolTip = "F4 : Show/hide list";
            this.LueSector.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSector.EditValueChanged += new System.EventHandler(this.LueSector_EditValueChanged);
            this.LueSector.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueSector_KeyDown);
            this.LueSector.Leave += new System.EventHandler(this.LueSector_Leave);
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(764, 185);
            this.Grd4.TabIndex = 63;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // TpgVdRating
            // 
            this.TpgVdRating.Controls.Add(this.LueVdRating);
            this.TpgVdRating.Controls.Add(this.LueRating);
            this.TpgVdRating.Controls.Add(this.Grd5);
            this.TpgVdRating.Location = new System.Drawing.Point(4, 26);
            this.TpgVdRating.Name = "TpgVdRating";
            this.TpgVdRating.Size = new System.Drawing.Size(764, 185);
            this.TpgVdRating.TabIndex = 12;
            this.TpgVdRating.Text = "Rating";
            this.TpgVdRating.UseVisualStyleBackColor = true;
            // 
            // LueVdRating
            // 
            this.LueVdRating.EnterMoveNextControl = true;
            this.LueVdRating.Location = new System.Drawing.Point(245, 27);
            this.LueVdRating.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdRating.Name = "LueVdRating";
            this.LueVdRating.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdRating.Properties.Appearance.Options.UseFont = true;
            this.LueVdRating.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdRating.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdRating.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdRating.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdRating.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdRating.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdRating.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdRating.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdRating.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdRating.Properties.DropDownRows = 12;
            this.LueVdRating.Properties.NullText = "[Empty]";
            this.LueVdRating.Properties.PopupWidth = 500;
            this.LueVdRating.Size = new System.Drawing.Size(171, 20);
            this.LueVdRating.TabIndex = 65;
            this.LueVdRating.ToolTip = "F4 : Show/hide list";
            this.LueVdRating.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdRating.EditValueChanged += new System.EventHandler(this.LueVdRating_EditValueChanged);
            this.LueVdRating.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueVdRating_KeyDown);
            this.LueVdRating.Leave += new System.EventHandler(this.LueVdRating_Leave);
            // 
            // LueRating
            // 
            this.LueRating.EnterMoveNextControl = true;
            this.LueRating.Location = new System.Drawing.Point(57, 27);
            this.LueRating.Margin = new System.Windows.Forms.Padding(5);
            this.LueRating.Name = "LueRating";
            this.LueRating.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRating.Properties.Appearance.Options.UseFont = true;
            this.LueRating.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRating.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueRating.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRating.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueRating.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRating.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueRating.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRating.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueRating.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueRating.Properties.DropDownRows = 12;
            this.LueRating.Properties.NullText = "[Empty]";
            this.LueRating.Properties.PopupWidth = 500;
            this.LueRating.Size = new System.Drawing.Size(171, 20);
            this.LueRating.TabIndex = 64;
            this.LueRating.ToolTip = "F4 : Show/hide list";
            this.LueRating.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueRating.EditValueChanged += new System.EventHandler(this.LueRating_EditValueChanged);
            this.LueRating.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueRating_KeyDown);
            this.LueRating.Leave += new System.EventHandler(this.LueRating_Leave);
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(764, 185);
            this.Grd5.TabIndex = 63;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // TpgUploadedFile
            // 
            this.TpgUploadedFile.Controls.Add(this.Grd8);
            this.TpgUploadedFile.Location = new System.Drawing.Point(4, 26);
            this.TpgUploadedFile.Name = "TpgUploadedFile";
            this.TpgUploadedFile.Size = new System.Drawing.Size(764, 185);
            this.TpgUploadedFile.TabIndex = 15;
            this.TpgUploadedFile.Text = "Uploaded File";
            this.TpgUploadedFile.UseVisualStyleBackColor = true;
            // 
            // Grd8
            // 
            this.Grd8.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd8.DefaultRow.Height = 20;
            this.Grd8.DefaultRow.Sortable = false;
            this.Grd8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd8.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd8.Header.Height = 21;
            this.Grd8.Location = new System.Drawing.Point(0, 0);
            this.Grd8.Name = "Grd8";
            this.Grd8.RowHeader.Visible = true;
            this.Grd8.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd8.SingleClickEdit = true;
            this.Grd8.Size = new System.Drawing.Size(764, 185);
            this.Grd8.TabIndex = 63;
            this.Grd8.TreeCol = null;
            this.Grd8.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd8.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd8_EllipsisButtonClick);
            this.Grd8.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd8_RequestEdit);
            // 
            // TpgProfile
            // 
            this.TpgProfile.Controls.Add(this.LueCountry);
            this.TpgProfile.Controls.Add(this.LueGender);
            this.TpgProfile.Controls.Add(this.MeeAddress2);
            this.TpgProfile.Controls.Add(this.MeeExperience);
            this.TpgProfile.Controls.Add(this.MeeLanguage);
            this.TpgProfile.Controls.Add(this.MeeCertificate);
            this.TpgProfile.Controls.Add(this.MeeEducation);
            this.TpgProfile.Controls.Add(this.LueBirthPlace);
            this.TpgProfile.Controls.Add(this.DteBirthDt);
            this.TpgProfile.Controls.Add(this.Grd6);
            this.TpgProfile.Location = new System.Drawing.Point(4, 26);
            this.TpgProfile.Name = "TpgProfile";
            this.TpgProfile.Padding = new System.Windows.Forms.Padding(3);
            this.TpgProfile.Size = new System.Drawing.Size(764, 185);
            this.TpgProfile.TabIndex = 13;
            this.TpgProfile.Text = "Vendor Expertise";
            this.TpgProfile.UseVisualStyleBackColor = true;
            // 
            // LueCountry
            // 
            this.LueCountry.EnterMoveNextControl = true;
            this.LueCountry.Location = new System.Drawing.Point(467, 186);
            this.LueCountry.Margin = new System.Windows.Forms.Padding(5);
            this.LueCountry.Name = "LueCountry";
            this.LueCountry.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCountry.Properties.Appearance.Options.UseFont = true;
            this.LueCountry.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCountry.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCountry.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCountry.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCountry.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCountry.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCountry.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCountry.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCountry.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCountry.Properties.DropDownRows = 12;
            this.LueCountry.Properties.NullText = "[Empty]";
            this.LueCountry.Properties.PopupWidth = 500;
            this.LueCountry.Size = new System.Drawing.Size(171, 20);
            this.LueCountry.TabIndex = 71;
            this.LueCountry.ToolTip = "F4 : Show/hide list";
            this.LueCountry.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCountry.EditValueChanged += new System.EventHandler(this.LueCountry_EditValueChanged);
            this.LueCountry.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCountry_KeyDown);
            this.LueCountry.Leave += new System.EventHandler(this.LueCountry_Leave);
            // 
            // LueGender
            // 
            this.LueGender.EnterMoveNextControl = true;
            this.LueGender.Location = new System.Drawing.Point(506, 209);
            this.LueGender.Margin = new System.Windows.Forms.Padding(5);
            this.LueGender.Name = "LueGender";
            this.LueGender.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.Appearance.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGender.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGender.Properties.DropDownRows = 12;
            this.LueGender.Properties.NullText = "[Empty]";
            this.LueGender.Properties.PopupWidth = 500;
            this.LueGender.Size = new System.Drawing.Size(171, 20);
            this.LueGender.TabIndex = 70;
            this.LueGender.ToolTip = "F4 : Show/hide list";
            this.LueGender.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGender.EditValueChanged += new System.EventHandler(this.LueGender_EditValueChanged);
            this.LueGender.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueGender_KeyDown);
            this.LueGender.Leave += new System.EventHandler(this.LueGender_Leave);
            // 
            // MeeAddress2
            // 
            this.MeeAddress2.EnterMoveNextControl = true;
            this.MeeAddress2.Location = new System.Drawing.Point(357, 164);
            this.MeeAddress2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAddress2.Name = "MeeAddress2";
            this.MeeAddress2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress2.Properties.Appearance.Options.UseFont = true;
            this.MeeAddress2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddress2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddress2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddress2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddress2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddress2.Properties.MaxLength = 400;
            this.MeeAddress2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAddress2.Properties.ShowIcon = false;
            this.MeeAddress2.Size = new System.Drawing.Size(263, 20);
            this.MeeAddress2.TabIndex = 70;
            this.MeeAddress2.ToolTip = "F4 : Show/hide text";
            this.MeeAddress2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddress2.ToolTipTitle = "Run System";
            this.MeeAddress2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeAddress2_KeyDown);
            this.MeeAddress2.Leave += new System.EventHandler(this.MeeAddress2_Leave);
            // 
            // MeeExperience
            // 
            this.MeeExperience.EnterMoveNextControl = true;
            this.MeeExperience.Location = new System.Drawing.Point(318, 142);
            this.MeeExperience.Margin = new System.Windows.Forms.Padding(5);
            this.MeeExperience.Name = "MeeExperience";
            this.MeeExperience.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeExperience.Properties.Appearance.Options.UseFont = true;
            this.MeeExperience.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeExperience.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeExperience.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeExperience.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeExperience.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeExperience.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeExperience.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeExperience.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeExperience.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeExperience.Properties.MaxLength = 400;
            this.MeeExperience.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeExperience.Properties.ShowIcon = false;
            this.MeeExperience.Size = new System.Drawing.Size(263, 20);
            this.MeeExperience.TabIndex = 69;
            this.MeeExperience.ToolTip = "F4 : Show/hide text";
            this.MeeExperience.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeExperience.ToolTipTitle = "Run System";
            this.MeeExperience.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeExperience_KeyDown);
            this.MeeExperience.Leave += new System.EventHandler(this.MeeExperience_Leave);
            // 
            // MeeLanguage
            // 
            this.MeeLanguage.EnterMoveNextControl = true;
            this.MeeLanguage.Location = new System.Drawing.Point(263, 120);
            this.MeeLanguage.Margin = new System.Windows.Forms.Padding(5);
            this.MeeLanguage.Name = "MeeLanguage";
            this.MeeLanguage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLanguage.Properties.Appearance.Options.UseFont = true;
            this.MeeLanguage.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLanguage.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeLanguage.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLanguage.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeLanguage.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLanguage.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeLanguage.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLanguage.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeLanguage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeLanguage.Properties.MaxLength = 400;
            this.MeeLanguage.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeLanguage.Properties.ShowIcon = false;
            this.MeeLanguage.Size = new System.Drawing.Size(263, 20);
            this.MeeLanguage.TabIndex = 68;
            this.MeeLanguage.ToolTip = "F4 : Show/hide text";
            this.MeeLanguage.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeLanguage.ToolTipTitle = "Run System";
            this.MeeLanguage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeLanguage_KeyDown);
            this.MeeLanguage.Leave += new System.EventHandler(this.MeeLanguage_Leave);
            // 
            // MeeCertificate
            // 
            this.MeeCertificate.EnterMoveNextControl = true;
            this.MeeCertificate.Location = new System.Drawing.Point(219, 98);
            this.MeeCertificate.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCertificate.Name = "MeeCertificate";
            this.MeeCertificate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertificate.Properties.Appearance.Options.UseFont = true;
            this.MeeCertificate.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertificate.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCertificate.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertificate.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCertificate.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertificate.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCertificate.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertificate.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCertificate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCertificate.Properties.MaxLength = 400;
            this.MeeCertificate.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCertificate.Properties.ShowIcon = false;
            this.MeeCertificate.Size = new System.Drawing.Size(263, 20);
            this.MeeCertificate.TabIndex = 67;
            this.MeeCertificate.ToolTip = "F4 : Show/hide text";
            this.MeeCertificate.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCertificate.ToolTipTitle = "Run System";
            this.MeeCertificate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCertificate_KeyDown);
            this.MeeCertificate.Leave += new System.EventHandler(this.MeeCertificate_Leave);
            // 
            // MeeEducation
            // 
            this.MeeEducation.EnterMoveNextControl = true;
            this.MeeEducation.Location = new System.Drawing.Point(162, 76);
            this.MeeEducation.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEducation.Name = "MeeEducation";
            this.MeeEducation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEducation.Properties.Appearance.Options.UseFont = true;
            this.MeeEducation.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEducation.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEducation.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEducation.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEducation.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEducation.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEducation.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEducation.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEducation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEducation.Properties.MaxLength = 400;
            this.MeeEducation.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEducation.Properties.ShowIcon = false;
            this.MeeEducation.Size = new System.Drawing.Size(263, 20);
            this.MeeEducation.TabIndex = 66;
            this.MeeEducation.ToolTip = "F4 : Show/hide text";
            this.MeeEducation.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEducation.ToolTipTitle = "Run System";
            this.MeeEducation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEducation_KeyDown);
            this.MeeEducation.Leave += new System.EventHandler(this.MeeEducation_Leave);
            // 
            // LueBirthPlace
            // 
            this.LueBirthPlace.EnterMoveNextControl = true;
            this.LueBirthPlace.Location = new System.Drawing.Point(155, 53);
            this.LueBirthPlace.Margin = new System.Windows.Forms.Padding(5);
            this.LueBirthPlace.Name = "LueBirthPlace";
            this.LueBirthPlace.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBirthPlace.Properties.Appearance.Options.UseFont = true;
            this.LueBirthPlace.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBirthPlace.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBirthPlace.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBirthPlace.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBirthPlace.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBirthPlace.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBirthPlace.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBirthPlace.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBirthPlace.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBirthPlace.Properties.DropDownRows = 12;
            this.LueBirthPlace.Properties.NullText = "[Empty]";
            this.LueBirthPlace.Properties.PopupWidth = 500;
            this.LueBirthPlace.Size = new System.Drawing.Size(171, 20);
            this.LueBirthPlace.TabIndex = 65;
            this.LueBirthPlace.ToolTip = "F4 : Show/hide list";
            this.LueBirthPlace.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBirthPlace.EditValueChanged += new System.EventHandler(this.LueBirthPlace_EditValueChanged);
            this.LueBirthPlace.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueBirthPlace_KeyDown);
            this.LueBirthPlace.Leave += new System.EventHandler(this.LueBirthPlace_Leave);
            // 
            // DteBirthDt
            // 
            this.DteBirthDt.EditValue = null;
            this.DteBirthDt.EnterMoveNextControl = true;
            this.DteBirthDt.Location = new System.Drawing.Point(23, 53);
            this.DteBirthDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteBirthDt.Name = "DteBirthDt";
            this.DteBirthDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.Appearance.Options.UseFont = true;
            this.DteBirthDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBirthDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBirthDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBirthDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBirthDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBirthDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBirthDt.Size = new System.Drawing.Size(125, 20);
            this.DteBirthDt.TabIndex = 64;
            this.DteBirthDt.Visible = false;
            this.DteBirthDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteBirthDt_KeyDown);
            this.DteBirthDt.Leave += new System.EventHandler(this.DteBirthDt_Leave);
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(3, 3);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(758, 179);
            this.Grd6.TabIndex = 63;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd6_EllipsisButtonClick);
            this.Grd6.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd6_RequestEdit);
            this.Grd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd6_KeyDown);
            // 
            // TpgProject
            // 
            this.TpgProject.Controls.Add(this.LueProgressCode);
            this.TpgProject.Controls.Add(this.DteContractEndDt);
            this.TpgProject.Controls.Add(this.DteContractDt);
            this.TpgProject.Controls.Add(this.LueCurCode);
            this.TpgProject.Controls.Add(this.LueProfile);
            this.TpgProject.Controls.Add(this.MeeProject);
            this.TpgProject.Controls.Add(this.Grd7);
            this.TpgProject.Location = new System.Drawing.Point(4, 26);
            this.TpgProject.Name = "TpgProject";
            this.TpgProject.Padding = new System.Windows.Forms.Padding(3);
            this.TpgProject.Size = new System.Drawing.Size(764, 185);
            this.TpgProject.TabIndex = 14;
            this.TpgProject.Text = "Project Historical";
            this.TpgProject.UseVisualStyleBackColor = true;
            // 
            // LueProgressCode
            // 
            this.LueProgressCode.EnterMoveNextControl = true;
            this.LueProgressCode.Location = new System.Drawing.Point(619, 55);
            this.LueProgressCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueProgressCode.Name = "LueProgressCode";
            this.LueProgressCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProgressCode.Properties.Appearance.Options.UseFont = true;
            this.LueProgressCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProgressCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProgressCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProgressCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProgressCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProgressCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProgressCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProgressCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProgressCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProgressCode.Properties.DropDownRows = 12;
            this.LueProgressCode.Properties.NullText = "[Empty]";
            this.LueProgressCode.Properties.PopupWidth = 500;
            this.LueProgressCode.Size = new System.Drawing.Size(171, 20);
            this.LueProgressCode.TabIndex = 69;
            this.LueProgressCode.ToolTip = "F4 : Show/hide list";
            this.LueProgressCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProgressCode.EditValueChanged += new System.EventHandler(this.LueProgressCode_EditValueChanged);
            this.LueProgressCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueProgressCode_KeyDown);
            this.LueProgressCode.Leave += new System.EventHandler(this.LueProgressCode_Leave);
            // 
            // DteContractEndDt
            // 
            this.DteContractEndDt.EditValue = null;
            this.DteContractEndDt.EnterMoveNextControl = true;
            this.DteContractEndDt.Location = new System.Drawing.Point(708, 104);
            this.DteContractEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteContractEndDt.Name = "DteContractEndDt";
            this.DteContractEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteContractEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteContractEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteContractEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteContractEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteContractEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteContractEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteContractEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteContractEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteContractEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteContractEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteContractEndDt.Size = new System.Drawing.Size(156, 20);
            this.DteContractEndDt.TabIndex = 68;
            this.DteContractEndDt.Visible = false;
            this.DteContractEndDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteContractEndDt_KeyDown);
            this.DteContractEndDt.Leave += new System.EventHandler(this.DteContractEndDt_Leave);
            // 
            // DteContractDt
            // 
            this.DteContractDt.EditValue = null;
            this.DteContractDt.EnterMoveNextControl = true;
            this.DteContractDt.Location = new System.Drawing.Point(546, 104);
            this.DteContractDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteContractDt.Name = "DteContractDt";
            this.DteContractDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteContractDt.Properties.Appearance.Options.UseFont = true;
            this.DteContractDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteContractDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteContractDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteContractDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteContractDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteContractDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteContractDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteContractDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteContractDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteContractDt.Size = new System.Drawing.Size(156, 20);
            this.DteContractDt.TabIndex = 67;
            this.DteContractDt.Visible = false;
            this.DteContractDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteContractDt_KeyDown);
            this.DteContractDt.Leave += new System.EventHandler(this.DteContractDt_Leave);
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(357, 104);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 12;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 500;
            this.LueCurCode.Size = new System.Drawing.Size(171, 20);
            this.LueCurCode.TabIndex = 66;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            this.LueCurCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode_KeyDown);
            this.LueCurCode.Leave += new System.EventHandler(this.LueCurCode_Leave);
            // 
            // LueProfile
            // 
            this.LueProfile.EnterMoveNextControl = true;
            this.LueProfile.Location = new System.Drawing.Point(64, 75);
            this.LueProfile.Margin = new System.Windows.Forms.Padding(5);
            this.LueProfile.Name = "LueProfile";
            this.LueProfile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfile.Properties.Appearance.Options.UseFont = true;
            this.LueProfile.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfile.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProfile.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfile.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProfile.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfile.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProfile.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfile.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProfile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProfile.Properties.DropDownRows = 12;
            this.LueProfile.Properties.NullText = "[Empty]";
            this.LueProfile.Properties.PopupWidth = 500;
            this.LueProfile.Size = new System.Drawing.Size(171, 20);
            this.LueProfile.TabIndex = 64;
            this.LueProfile.ToolTip = "F4 : Show/hide list";
            this.LueProfile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProfile.EditValueChanged += new System.EventHandler(this.LueProfile_EditValueChanged);
            this.LueProfile.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueProfile_KeyDown);
            this.LueProfile.Leave += new System.EventHandler(this.LueProfile_Leave);
            // 
            // MeeProject
            // 
            this.MeeProject.EnterMoveNextControl = true;
            this.MeeProject.Location = new System.Drawing.Point(332, 55);
            this.MeeProject.Margin = new System.Windows.Forms.Padding(5);
            this.MeeProject.Name = "MeeProject";
            this.MeeProject.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProject.Properties.Appearance.Options.UseFont = true;
            this.MeeProject.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProject.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeProject.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProject.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeProject.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProject.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeProject.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProject.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeProject.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeProject.Properties.MaxLength = 400;
            this.MeeProject.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeProject.Properties.ShowIcon = false;
            this.MeeProject.Size = new System.Drawing.Size(263, 20);
            this.MeeProject.TabIndex = 65;
            this.MeeProject.ToolTip = "F4 : Show/hide text";
            this.MeeProject.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeProject.ToolTipTitle = "Run System";
            this.MeeProject.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeProject_KeyDown);
            this.MeeProject.Leave += new System.EventHandler(this.MeeProject_Leave);
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(3, 3);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(758, 179);
            this.Grd7.TabIndex = 63;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd7_EllipsisButtonClick);
            this.Grd7.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd7_RequestEdit);
            this.Grd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd7_KeyDown);
            // 
            // TpgVdDeed
            // 
            this.TpgVdDeed.Controls.Add(this.DteDeedDt2);
            this.TpgVdDeed.Controls.Add(this.DteDeedDt);
            this.TpgVdDeed.Controls.Add(this.Grd9);
            this.TpgVdDeed.Location = new System.Drawing.Point(4, 26);
            this.TpgVdDeed.Name = "TpgVdDeed";
            this.TpgVdDeed.Padding = new System.Windows.Forms.Padding(3);
            this.TpgVdDeed.Size = new System.Drawing.Size(764, 185);
            this.TpgVdDeed.TabIndex = 16;
            this.TpgVdDeed.Text = "Deed";
            this.TpgVdDeed.UseVisualStyleBackColor = true;
            // 
            // DteDeedDt2
            // 
            this.DteDeedDt2.EditValue = null;
            this.DteDeedDt2.EnterMoveNextControl = true;
            this.DteDeedDt2.Location = new System.Drawing.Point(192, 38);
            this.DteDeedDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDeedDt2.Name = "DteDeedDt2";
            this.DteDeedDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeedDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDeedDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeedDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDeedDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDeedDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDeedDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeedDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDeedDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeedDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDeedDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDeedDt2.Size = new System.Drawing.Size(125, 20);
            this.DteDeedDt2.TabIndex = 65;
            this.DteDeedDt2.Visible = false;
            this.DteDeedDt2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteDeedDt2_KeyDown);
            this.DteDeedDt2.Leave += new System.EventHandler(this.DteDeedDt2_Leave);
            // 
            // DteDeedDt
            // 
            this.DteDeedDt.EditValue = null;
            this.DteDeedDt.EnterMoveNextControl = true;
            this.DteDeedDt.Location = new System.Drawing.Point(60, 37);
            this.DteDeedDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDeedDt.Name = "DteDeedDt";
            this.DteDeedDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeedDt.Properties.Appearance.Options.UseFont = true;
            this.DteDeedDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeedDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDeedDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDeedDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDeedDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeedDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDeedDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeedDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDeedDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDeedDt.Size = new System.Drawing.Size(125, 20);
            this.DteDeedDt.TabIndex = 64;
            this.DteDeedDt.Visible = false;
            this.DteDeedDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteDeedDt_KeyDown);
            this.DteDeedDt.Leave += new System.EventHandler(this.DteDeedDt_Leave);
            // 
            // Grd9
            // 
            this.Grd9.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd9.DefaultRow.Height = 20;
            this.Grd9.DefaultRow.Sortable = false;
            this.Grd9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd9.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd9.Header.Height = 21;
            this.Grd9.Location = new System.Drawing.Point(3, 3);
            this.Grd9.Name = "Grd9";
            this.Grd9.RowHeader.Visible = true;
            this.Grd9.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd9.SingleClickEdit = true;
            this.Grd9.Size = new System.Drawing.Size(758, 179);
            this.Grd9.TabIndex = 63;
            this.Grd9.TreeCol = null;
            this.Grd9.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd9.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd9_RequestEdit);
            this.Grd9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd9_KeyDown);
            // 
            // TpgVehicle
            // 
            this.TpgVehicle.Controls.Add(this.LueTransportType);
            this.TpgVehicle.Controls.Add(this.Grd10);
            this.TpgVehicle.Location = new System.Drawing.Point(4, 26);
            this.TpgVehicle.Name = "TpgVehicle";
            this.TpgVehicle.Padding = new System.Windows.Forms.Padding(3);
            this.TpgVehicle.Size = new System.Drawing.Size(764, 185);
            this.TpgVehicle.TabIndex = 17;
            this.TpgVehicle.Text = "Vehicle";
            this.TpgVehicle.UseVisualStyleBackColor = true;
            // 
            // LueTransportType
            // 
            this.LueTransportType.EnterMoveNextControl = true;
            this.LueTransportType.Location = new System.Drawing.Point(139, 30);
            this.LueTransportType.Margin = new System.Windows.Forms.Padding(5);
            this.LueTransportType.Name = "LueTransportType";
            this.LueTransportType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransportType.Properties.Appearance.Options.UseFont = true;
            this.LueTransportType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransportType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTransportType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransportType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTransportType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransportType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTransportType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransportType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTransportType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTransportType.Properties.DropDownRows = 12;
            this.LueTransportType.Properties.NullText = "[Empty]";
            this.LueTransportType.Properties.PopupWidth = 500;
            this.LueTransportType.Size = new System.Drawing.Size(171, 20);
            this.LueTransportType.TabIndex = 65;
            this.LueTransportType.ToolTip = "F4 : Show/hide list";
            this.LueTransportType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTransportType.EditValueChanged += new System.EventHandler(this.LueTransportType_EditValueChanged_1);
            this.LueTransportType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTransportType_KeyDown_1);
            this.LueTransportType.Leave += new System.EventHandler(this.LueTransportType_Leave_1);
            // 
            // Grd10
            // 
            this.Grd10.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd10.DefaultRow.Height = 20;
            this.Grd10.DefaultRow.Sortable = false;
            this.Grd10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd10.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd10.Header.Height = 21;
            this.Grd10.Location = new System.Drawing.Point(3, 3);
            this.Grd10.Name = "Grd10";
            this.Grd10.RowHeader.Visible = true;
            this.Grd10.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd10.SingleClickEdit = true;
            this.Grd10.Size = new System.Drawing.Size(758, 179);
            this.Grd10.TabIndex = 64;
            this.Grd10.TreeCol = null;
            this.Grd10.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd10.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd10_RequestEdit);
            this.Grd10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd10_KeyDown);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TxtRMVdCode);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.TxtVdCodeInternal);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.TxtEstablishedYr);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.LueParent);
            this.panel3.Controls.Add(this.TxtWebsite);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.TxtShortName);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.BtnVendorCode);
            this.panel3.Controls.Add(this.LueSDCode);
            this.panel3.Controls.Add(this.LueVilCode);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.TxtPostalCd);
            this.panel3.Controls.Add(this.ChkActInd);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.LueCityCode);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.LblVdCtCode);
            this.panel3.Controls.Add(this.LueVdCtCode);
            this.panel3.Controls.Add(this.MeeAddress);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.TxtVdName);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtVdCode);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(889, 331);
            this.panel3.TabIndex = 64;
            // 
            // TxtRMVdCode
            // 
            this.TxtRMVdCode.EnterMoveNextControl = true;
            this.TxtRMVdCode.Location = new System.Drawing.Point(142, 67);
            this.TxtRMVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRMVdCode.Name = "TxtRMVdCode";
            this.TxtRMVdCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRMVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRMVdCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRMVdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtRMVdCode.Properties.MaxLength = 40;
            this.TxtRMVdCode.Size = new System.Drawing.Size(319, 20);
            this.TxtRMVdCode.TabIndex = 48;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8F);
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(5, 70);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(131, 13);
            this.label22.TabIndex = 47;
            this.label22.Text = "RunMarket\'s Vendor Code";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdCodeInternal
            // 
            this.TxtVdCodeInternal.EnterMoveNextControl = true;
            this.TxtVdCodeInternal.Location = new System.Drawing.Point(142, 88);
            this.TxtVdCodeInternal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdCodeInternal.Name = "TxtVdCodeInternal";
            this.TxtVdCodeInternal.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVdCodeInternal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdCodeInternal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdCodeInternal.Properties.Appearance.Options.UseFont = true;
            this.TxtVdCodeInternal.Properties.MaxLength = 40;
            this.TxtVdCodeInternal.Size = new System.Drawing.Size(319, 20);
            this.TxtVdCodeInternal.TabIndex = 50;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(26, 90);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(110, 14);
            this.label21.TabIndex = 49;
            this.label21.Text = "Vendor Local Code";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEstablishedYr
            // 
            this.TxtEstablishedYr.EnterMoveNextControl = true;
            this.TxtEstablishedYr.Location = new System.Drawing.Point(142, 277);
            this.TxtEstablishedYr.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEstablishedYr.Name = "TxtEstablishedYr";
            this.TxtEstablishedYr.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEstablishedYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEstablishedYr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEstablishedYr.Properties.Appearance.Options.UseFont = true;
            this.TxtEstablishedYr.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEstablishedYr.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEstablishedYr.Properties.MaxLength = 4;
            this.TxtEstablishedYr.Size = new System.Drawing.Size(154, 20);
            this.TxtEstablishedYr.TabIndex = 69;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(25, 280);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(111, 14);
            this.label20.TabIndex = 67;
            this.label20.Text = "Establishment Year";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(65, 258);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(71, 14);
            this.label19.TabIndex = 65;
            this.label19.Text = "Head Office";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueParent
            // 
            this.LueParent.EnterMoveNextControl = true;
            this.LueParent.Location = new System.Drawing.Point(142, 256);
            this.LueParent.Margin = new System.Windows.Forms.Padding(5);
            this.LueParent.Name = "LueParent";
            this.LueParent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.Appearance.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueParent.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueParent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueParent.Properties.DropDownRows = 30;
            this.LueParent.Properties.NullText = "[Empty]";
            this.LueParent.Properties.PopupWidth = 350;
            this.LueParent.Size = new System.Drawing.Size(319, 20);
            this.LueParent.TabIndex = 66;
            this.LueParent.ToolTip = "F4 : Show/hide list";
            this.LueParent.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueParent.EditValueChanged += new System.EventHandler(this.LueParent_EditValueChanged);
            // 
            // TxtWebsite
            // 
            this.TxtWebsite.EnterMoveNextControl = true;
            this.TxtWebsite.Location = new System.Drawing.Point(142, 235);
            this.TxtWebsite.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWebsite.Name = "TxtWebsite";
            this.TxtWebsite.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWebsite.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWebsite.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWebsite.Properties.Appearance.Options.UseFont = true;
            this.TxtWebsite.Properties.MaxLength = 40;
            this.TxtWebsite.Size = new System.Drawing.Size(319, 20);
            this.TxtWebsite.TabIndex = 64;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(84, 238);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 14);
            this.label18.TabIndex = 63;
            this.label18.Text = "Website";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtShortName
            // 
            this.TxtShortName.EnterMoveNextControl = true;
            this.TxtShortName.Location = new System.Drawing.Point(142, 46);
            this.TxtShortName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShortName.Name = "TxtShortName";
            this.TxtShortName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShortName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShortName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShortName.Properties.Appearance.Options.UseFont = true;
            this.TxtShortName.Properties.MaxLength = 40;
            this.TxtShortName.Size = new System.Drawing.Size(319, 20);
            this.TxtShortName.TabIndex = 46;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(64, 49);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 14);
            this.label17.TabIndex = 45;
            this.label17.Text = "Short Name";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnVendorCode
            // 
            this.BtnVendorCode.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnVendorCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVendorCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVendorCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVendorCode.Appearance.Options.UseBackColor = true;
            this.BtnVendorCode.Appearance.Options.UseFont = true;
            this.BtnVendorCode.Appearance.Options.UseForeColor = true;
            this.BtnVendorCode.Appearance.Options.UseTextOptions = true;
            this.BtnVendorCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVendorCode.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnVendorCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnVendorCode.Location = new System.Drawing.Point(300, 3);
            this.BtnVendorCode.Name = "BtnVendorCode";
            this.BtnVendorCode.Size = new System.Drawing.Size(31, 20);
            this.BtnVendorCode.TabIndex = 41;
            this.BtnVendorCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVendorCode.ToolTipTitle = "Latest vendor code";
            // 
            // LueSDCode
            // 
            this.LueSDCode.EnterMoveNextControl = true;
            this.LueSDCode.Location = new System.Drawing.Point(142, 172);
            this.LueSDCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSDCode.Name = "LueSDCode";
            this.LueSDCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSDCode.Properties.Appearance.Options.UseFont = true;
            this.LueSDCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSDCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSDCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSDCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSDCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSDCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSDCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSDCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSDCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSDCode.Properties.DropDownRows = 30;
            this.LueSDCode.Properties.NullText = "[Empty]";
            this.LueSDCode.Properties.PopupWidth = 350;
            this.LueSDCode.Size = new System.Drawing.Size(319, 20);
            this.LueSDCode.TabIndex = 58;
            this.LueSDCode.ToolTip = "F4 : Show/hide list";
            this.LueSDCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSDCode.EditValueChanged += new System.EventHandler(this.LueSDCode_EditValueChanged);
            // 
            // LueVilCode
            // 
            this.LueVilCode.EnterMoveNextControl = true;
            this.LueVilCode.Location = new System.Drawing.Point(142, 193);
            this.LueVilCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVilCode.Name = "LueVilCode";
            this.LueVilCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVilCode.Properties.Appearance.Options.UseFont = true;
            this.LueVilCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVilCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVilCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVilCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVilCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVilCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVilCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVilCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVilCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVilCode.Properties.DropDownRows = 30;
            this.LueVilCode.Properties.NullText = "[Empty]";
            this.LueVilCode.Properties.PopupWidth = 350;
            this.LueVilCode.Size = new System.Drawing.Size(319, 20);
            this.LueVilCode.TabIndex = 60;
            this.LueVilCode.ToolTip = "F4 : Show/hide list";
            this.LueVilCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVilCode.EditValueChanged += new System.EventHandler(this.LueVilCode_EditValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(67, 176);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 14);
            this.label16.TabIndex = 57;
            this.label16.Text = "Sub District";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(95, 197);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 14);
            this.label14.TabIndex = 59;
            this.label14.Text = "Village";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPostalCd
            // 
            this.TxtPostalCd.EnterMoveNextControl = true;
            this.TxtPostalCd.Location = new System.Drawing.Point(142, 214);
            this.TxtPostalCd.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPostalCd.Name = "TxtPostalCd";
            this.TxtPostalCd.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPostalCd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPostalCd.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPostalCd.Properties.Appearance.Options.UseFont = true;
            this.TxtPostalCd.Properties.MaxLength = 20;
            this.TxtPostalCd.Size = new System.Drawing.Size(319, 20);
            this.TxtPostalCd.TabIndex = 62;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(336, 3);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(76, 22);
            this.ChkActInd.TabIndex = 42;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(109, 155);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 14);
            this.label4.TabIndex = 55;
            this.label4.Text = "City";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCityCode
            // 
            this.LueCityCode.EnterMoveNextControl = true;
            this.LueCityCode.Location = new System.Drawing.Point(142, 151);
            this.LueCityCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCityCode.Name = "LueCityCode";
            this.LueCityCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.Appearance.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCityCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCityCode.Properties.DropDownRows = 30;
            this.LueCityCode.Properties.NullText = "[Empty]";
            this.LueCityCode.Properties.PopupWidth = 350;
            this.LueCityCode.Size = new System.Drawing.Size(319, 20);
            this.LueCityCode.TabIndex = 56;
            this.LueCityCode.ToolTip = "F4 : Show/hide list";
            this.LueCityCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCityCode.EditValueChanged += new System.EventHandler(this.LueCityCode_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(65, 218);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 14);
            this.label6.TabIndex = 61;
            this.label6.Text = "Postal Code";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblVdCtCode
            // 
            this.LblVdCtCode.AutoSize = true;
            this.LblVdCtCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblVdCtCode.ForeColor = System.Drawing.Color.Black;
            this.LblVdCtCode.Location = new System.Drawing.Point(80, 112);
            this.LblVdCtCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblVdCtCode.Name = "LblVdCtCode";
            this.LblVdCtCode.Size = new System.Drawing.Size(56, 14);
            this.LblVdCtCode.TabIndex = 51;
            this.LblVdCtCode.Text = "Category";
            this.LblVdCtCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVdCtCode
            // 
            this.LueVdCtCode.EnterMoveNextControl = true;
            this.LueVdCtCode.Location = new System.Drawing.Point(142, 109);
            this.LueVdCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdCtCode.Name = "LueVdCtCode";
            this.LueVdCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdCtCode.Properties.DropDownRows = 30;
            this.LueVdCtCode.Properties.NullText = "[Empty]";
            this.LueVdCtCode.Properties.PopupWidth = 350;
            this.LueVdCtCode.Size = new System.Drawing.Size(319, 20);
            this.LueVdCtCode.TabIndex = 52;
            this.LueVdCtCode.ToolTip = "F4 : Show/hide list";
            this.LueVdCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdCtCode.EditValueChanged += new System.EventHandler(this.LueVdCtCode_EditValueChanged);
            // 
            // MeeAddress
            // 
            this.MeeAddress.EnterMoveNextControl = true;
            this.MeeAddress.Location = new System.Drawing.Point(142, 130);
            this.MeeAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAddress.Name = "MeeAddress";
            this.MeeAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddress.Properties.MaxLength = 400;
            this.MeeAddress.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAddress.Properties.ShowIcon = false;
            this.MeeAddress.Size = new System.Drawing.Size(319, 20);
            this.MeeAddress.TabIndex = 54;
            this.MeeAddress.ToolTip = "F4 : Show/hide text";
            this.MeeAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddress.ToolTipTitle = "Run System";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(86, 133);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 14);
            this.label15.TabIndex = 53;
            this.label15.Text = "Address";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdName
            // 
            this.TxtVdName.EnterMoveNextControl = true;
            this.TxtVdName.Location = new System.Drawing.Point(142, 25);
            this.TxtVdName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdName.Name = "TxtVdName";
            this.TxtVdName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVdName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdName.Properties.Appearance.Options.UseFont = true;
            this.TxtVdName.Properties.MaxLength = 100;
            this.TxtVdName.Size = new System.Drawing.Size(319, 20);
            this.TxtVdName.TabIndex = 44;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(54, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 14);
            this.label2.TabIndex = 43;
            this.label2.Text = "Vendor Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdCode
            // 
            this.TxtVdCode.EnterMoveNextControl = true;
            this.TxtVdCode.Location = new System.Drawing.Point(142, 4);
            this.TxtVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdCode.Name = "TxtVdCode";
            this.TxtVdCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtVdCode.Properties.MaxLength = 16;
            this.TxtVdCode.Size = new System.Drawing.Size(154, 20);
            this.TxtVdCode.TabIndex = 40;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(57, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 14);
            this.label1.TabIndex = 39;
            this.label1.Text = "Vendor Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.LueDeptCode);
            this.panel4.Controls.Add(this.TxtSelectionScore);
            this.panel4.Controls.Add(this.LblSelectionScore);
            this.panel4.Controls.Add(this.TxtVdExternalCode);
            this.panel4.Controls.Add(this.LblVdExternalCode);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.LueEntityType);
            this.panel4.Controls.Add(this.LblProcurementStatus);
            this.panel4.Controls.Add(this.LueProcurementStatus);
            this.panel4.Controls.Add(this.TxtNIB);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.ChkCreateUserVendor);
            this.panel4.Controls.Add(this.BtnVendorRegister);
            this.panel4.Controls.Add(this.LblVendorRegister);
            this.panel4.Controls.Add(this.ChkTaxInd);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.TxtTIN);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.TxtIdentityNo);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.TxtCreditLimit);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.TxtMobile);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.TxtPhone);
            this.panel4.Controls.Add(this.TxtEmail);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.TxtFax);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(528, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(361, 331);
            this.panel4.TabIndex = 68;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(75, 310);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(73, 14);
            this.label24.TabIndex = 69;
            this.label24.Text = "Department";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(153, 307);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(194, 20);
            this.LueDeptCode.TabIndex = 70;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // TxtSelectionScore
            // 
            this.TxtSelectionScore.EnterMoveNextControl = true;
            this.TxtSelectionScore.Location = new System.Drawing.Point(153, 286);
            this.TxtSelectionScore.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSelectionScore.Name = "TxtSelectionScore";
            this.TxtSelectionScore.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSelectionScore.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSelectionScore.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSelectionScore.Properties.Appearance.Options.UseFont = true;
            this.TxtSelectionScore.Properties.MaxLength = 40;
            this.TxtSelectionScore.Size = new System.Drawing.Size(194, 20);
            this.TxtSelectionScore.TabIndex = 68;
            // 
            // LblSelectionScore
            // 
            this.LblSelectionScore.AutoSize = true;
            this.LblSelectionScore.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSelectionScore.ForeColor = System.Drawing.Color.Black;
            this.LblSelectionScore.Location = new System.Drawing.Point(59, 290);
            this.LblSelectionScore.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSelectionScore.Name = "LblSelectionScore";
            this.LblSelectionScore.Size = new System.Drawing.Size(92, 14);
            this.LblSelectionScore.TabIndex = 67;
            this.LblSelectionScore.Text = "Selection Score";
            this.LblSelectionScore.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdExternalCode
            // 
            this.TxtVdExternalCode.EnterMoveNextControl = true;
            this.TxtVdExternalCode.Location = new System.Drawing.Point(153, 265);
            this.TxtVdExternalCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdExternalCode.Name = "TxtVdExternalCode";
            this.TxtVdExternalCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVdExternalCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdExternalCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdExternalCode.Properties.Appearance.Options.UseFont = true;
            this.TxtVdExternalCode.Properties.MaxLength = 40;
            this.TxtVdExternalCode.Size = new System.Drawing.Size(194, 20);
            this.TxtVdExternalCode.TabIndex = 66;
            // 
            // LblVdExternalCode
            // 
            this.LblVdExternalCode.AutoSize = true;
            this.LblVdExternalCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblVdExternalCode.ForeColor = System.Drawing.Color.Black;
            this.LblVdExternalCode.Location = new System.Drawing.Point(24, 269);
            this.LblVdExternalCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblVdExternalCode.Name = "LblVdExternalCode";
            this.LblVdExternalCode.Size = new System.Drawing.Size(127, 14);
            this.LblVdExternalCode.TabIndex = 65;
            this.LblVdExternalCode.Text = "Vendor External Code";
            this.LblVdExternalCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(77, 247);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(71, 14);
            this.label23.TabIndex = 63;
            this.label23.Text = "Entity Type";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEntityType
            // 
            this.LueEntityType.EnterMoveNextControl = true;
            this.LueEntityType.Location = new System.Drawing.Point(153, 244);
            this.LueEntityType.Margin = new System.Windows.Forms.Padding(5);
            this.LueEntityType.Name = "LueEntityType";
            this.LueEntityType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityType.Properties.Appearance.Options.UseFont = true;
            this.LueEntityType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEntityType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEntityType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEntityType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEntityType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEntityType.Properties.DropDownRows = 30;
            this.LueEntityType.Properties.NullText = "[Empty]";
            this.LueEntityType.Properties.PopupWidth = 350;
            this.LueEntityType.Size = new System.Drawing.Size(194, 20);
            this.LueEntityType.TabIndex = 64;
            this.LueEntityType.ToolTip = "F4 : Show/hide list";
            this.LueEntityType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LblProcurementStatus
            // 
            this.LblProcurementStatus.AutoSize = true;
            this.LblProcurementStatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblProcurementStatus.ForeColor = System.Drawing.Color.Black;
            this.LblProcurementStatus.Location = new System.Drawing.Point(31, 197);
            this.LblProcurementStatus.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblProcurementStatus.Name = "LblProcurementStatus";
            this.LblProcurementStatus.Size = new System.Drawing.Size(117, 14);
            this.LblProcurementStatus.TabIndex = 58;
            this.LblProcurementStatus.Text = "Procurement Status";
            this.LblProcurementStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProcurementStatus
            // 
            this.LueProcurementStatus.EnterMoveNextControl = true;
            this.LueProcurementStatus.Location = new System.Drawing.Point(152, 194);
            this.LueProcurementStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueProcurementStatus.Name = "LueProcurementStatus";
            this.LueProcurementStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementStatus.Properties.Appearance.Options.UseFont = true;
            this.LueProcurementStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProcurementStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProcurementStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProcurementStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProcurementStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProcurementStatus.Properties.DropDownRows = 30;
            this.LueProcurementStatus.Properties.NullText = "[Empty]";
            this.LueProcurementStatus.Properties.PopupWidth = 350;
            this.LueProcurementStatus.Size = new System.Drawing.Size(194, 20);
            this.LueProcurementStatus.TabIndex = 59;
            this.LueProcurementStatus.ToolTip = "F4 : Show/hide list";
            this.LueProcurementStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProcurementStatus.EditValueChanged += new System.EventHandler(this.LueProcurementStatus_EditValueChanged);
            // 
            // TxtNIB
            // 
            this.TxtNIB.EnterMoveNextControl = true;
            this.TxtNIB.Location = new System.Drawing.Point(152, 151);
            this.TxtNIB.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNIB.Name = "TxtNIB";
            this.TxtNIB.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNIB.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNIB.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNIB.Properties.Appearance.Options.UseFont = true;
            this.TxtNIB.Properties.MaxLength = 40;
            this.TxtNIB.Size = new System.Drawing.Size(194, 20);
            this.TxtNIB.TabIndex = 55;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(122, 155);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 14);
            this.label3.TabIndex = 54;
            this.label3.Text = "NIB";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCreateUserVendor
            // 
            this.ChkCreateUserVendor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCreateUserVendor.Location = new System.Drawing.Point(192, 217);
            this.ChkCreateUserVendor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCreateUserVendor.Name = "ChkCreateUserVendor";
            this.ChkCreateUserVendor.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCreateUserVendor.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCreateUserVendor.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCreateUserVendor.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCreateUserVendor.Properties.Appearance.Options.UseFont = true;
            this.ChkCreateUserVendor.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCreateUserVendor.Properties.Caption = "Create User Vendor";
            this.ChkCreateUserVendor.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCreateUserVendor.Size = new System.Drawing.Size(135, 22);
            this.ChkCreateUserVendor.TabIndex = 62;
            // 
            // BtnVendorRegister
            // 
            this.BtnVendorRegister.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnVendorRegister.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVendorRegister.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVendorRegister.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVendorRegister.Appearance.Options.UseBackColor = true;
            this.BtnVendorRegister.Appearance.Options.UseFont = true;
            this.BtnVendorRegister.Appearance.Options.UseForeColor = true;
            this.BtnVendorRegister.Appearance.Options.UseTextOptions = true;
            this.BtnVendorRegister.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVendorRegister.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnVendorRegister.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnVendorRegister.Location = new System.Drawing.Point(153, 218);
            this.BtnVendorRegister.Name = "BtnVendorRegister";
            this.BtnVendorRegister.Size = new System.Drawing.Size(31, 20);
            this.BtnVendorRegister.TabIndex = 61;
            this.BtnVendorRegister.ToolTip = "Get Vendor Register";
            this.BtnVendorRegister.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVendorRegister.ToolTipTitle = "Get Vendor Register";
            // 
            // LblVendorRegister
            // 
            this.LblVendorRegister.AutoSize = true;
            this.LblVendorRegister.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblVendorRegister.Location = new System.Drawing.Point(11, 219);
            this.LblVendorRegister.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblVendorRegister.Name = "LblVendorRegister";
            this.LblVendorRegister.Size = new System.Drawing.Size(137, 14);
            this.LblVendorRegister.TabIndex = 60;
            this.LblVendorRegister.Text = "Source Vendor Register";
            this.LblVendorRegister.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkTaxInd
            // 
            this.ChkTaxInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkTaxInd.Location = new System.Drawing.Point(297, 25);
            this.ChkTaxInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkTaxInd.Name = "ChkTaxInd";
            this.ChkTaxInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkTaxInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTaxInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkTaxInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkTaxInd.Properties.Appearance.Options.UseFont = true;
            this.ChkTaxInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkTaxInd.Properties.Caption = "Tax";
            this.ChkTaxInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTaxInd.Size = new System.Drawing.Size(50, 22);
            this.ChkTaxInd.TabIndex = 43;
            this.ChkTaxInd.ToolTip = "Tax Indicator";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(7, 30);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(141, 14);
            this.label13.TabIndex = 41;
            this.label13.Text = "Taxpayer Identification#";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTIN
            // 
            this.TxtTIN.EnterMoveNextControl = true;
            this.TxtTIN.Location = new System.Drawing.Point(152, 25);
            this.TxtTIN.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTIN.Name = "TxtTIN";
            this.TxtTIN.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTIN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTIN.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTIN.Properties.Appearance.Options.UseFont = true;
            this.TxtTIN.Properties.MaxLength = 40;
            this.TxtTIN.Size = new System.Drawing.Size(140, 20);
            this.TxtTIN.TabIndex = 42;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(51, 8);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 14);
            this.label12.TabIndex = 39;
            this.label12.Text = "Identity Number";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtIdentityNo
            // 
            this.TxtIdentityNo.EnterMoveNextControl = true;
            this.TxtIdentityNo.Location = new System.Drawing.Point(152, 4);
            this.TxtIdentityNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIdentityNo.Name = "TxtIdentityNo";
            this.TxtIdentityNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtIdentityNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIdentityNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIdentityNo.Properties.Appearance.Options.UseFont = true;
            this.TxtIdentityNo.Properties.MaxLength = 40;
            this.TxtIdentityNo.Size = new System.Drawing.Size(194, 20);
            this.TxtIdentityNo.TabIndex = 40;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(101, 176);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 56;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCreditLimit
            // 
            this.TxtCreditLimit.EnterMoveNextControl = true;
            this.TxtCreditLimit.Location = new System.Drawing.Point(152, 130);
            this.TxtCreditLimit.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCreditLimit.Name = "TxtCreditLimit";
            this.TxtCreditLimit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCreditLimit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCreditLimit.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCreditLimit.Properties.Appearance.Options.UseFont = true;
            this.TxtCreditLimit.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCreditLimit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCreditLimit.Size = new System.Drawing.Size(194, 20);
            this.TxtCreditLimit.TabIndex = 53;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(152, 172);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(194, 20);
            this.MeeRemark.TabIndex = 57;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(80, 134);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 14);
            this.label11.TabIndex = 52;
            this.label11.Text = "Credit Limit";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMobile
            // 
            this.TxtMobile.EnterMoveNextControl = true;
            this.TxtMobile.Location = new System.Drawing.Point(152, 109);
            this.TxtMobile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMobile.Name = "TxtMobile";
            this.TxtMobile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMobile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobile.Properties.Appearance.Options.UseFont = true;
            this.TxtMobile.Properties.MaxLength = 40;
            this.TxtMobile.Size = new System.Drawing.Size(194, 20);
            this.TxtMobile.TabIndex = 51;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(106, 50);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 14);
            this.label7.TabIndex = 44;
            this.label7.Text = "Phone";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(107, 113);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 14);
            this.label10.TabIndex = 50;
            this.label10.Text = "Mobile";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhone
            // 
            this.TxtPhone.EnterMoveNextControl = true;
            this.TxtPhone.Location = new System.Drawing.Point(152, 46);
            this.TxtPhone.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtPhone.Properties.MaxLength = 40;
            this.TxtPhone.Size = new System.Drawing.Size(194, 20);
            this.TxtPhone.TabIndex = 45;
            // 
            // TxtEmail
            // 
            this.TxtEmail.EnterMoveNextControl = true;
            this.TxtEmail.Location = new System.Drawing.Point(152, 88);
            this.TxtEmail.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmail.Properties.Appearance.Options.UseFont = true;
            this.TxtEmail.Properties.MaxLength = 40;
            this.TxtEmail.Size = new System.Drawing.Size(194, 20);
            this.TxtEmail.TabIndex = 49;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(67, 92);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 14);
            this.label8.TabIndex = 48;
            this.label8.Text = "Email Address";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(123, 71);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 14);
            this.label9.TabIndex = 46;
            this.label9.Text = "Fax";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFax
            // 
            this.TxtFax.EnterMoveNextControl = true;
            this.TxtFax.Location = new System.Drawing.Point(152, 67);
            this.TxtFax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFax.Name = "TxtFax";
            this.TxtFax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFax.Properties.Appearance.Options.UseFont = true;
            this.TxtFax.Properties.MaxLength = 40;
            this.TxtFax.Size = new System.Drawing.Size(194, 20);
            this.TxtFax.TabIndex = 47;
            // 
            // FrmVendor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 547);
            this.Name = "FrmVendor";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.TpgContactPerson.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.TpgBank.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).EndInit();
            this.TpgItemCategory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpgVdSector.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueSubSectorCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueQualification.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.TpgVdRating.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueVdRating.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueRating.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.TpgUploadedFile.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).EndInit();
            this.TpgProfile.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeExperience.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeLanguage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCertificate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEducation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBirthPlace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.TpgProject.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueProgressCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProfile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.TpgVdDeed.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDeedDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeedDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeedDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeedDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).EndInit();
            this.TpgVehicle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueTransportType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd10)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRMVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCodeInternal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEstablishedYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWebsite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSDCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVilCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSelectionScore.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdExternalCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntityType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcurementStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNIB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCreateUserVendor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTIN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdentityNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreditLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgContactPerson;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.TabPage TpgBank;
        private DevExpress.XtraEditors.LookUpEdit LueLevel;
        private System.Windows.Forms.TabPage TpgItemCategory;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private System.Windows.Forms.TabPage TpgVdSector;
        private DevExpress.XtraEditors.LookUpEdit LueSector;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private System.Windows.Forms.TabPage TpgVdRating;
        private DevExpress.XtraEditors.LookUpEdit LueVdRating;
        private DevExpress.XtraEditors.LookUpEdit LueRating;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        private System.Windows.Forms.TabPage TpgProfile;
        private System.Windows.Forms.TabPage TpgProject;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        internal DevExpress.XtraEditors.DateEdit DteBirthDt;
        private DevExpress.XtraEditors.LookUpEdit LueBirthPlace;
        private DevExpress.XtraEditors.MemoExEdit MeeEducation;
        private DevExpress.XtraEditors.MemoExEdit MeeExperience;
        private DevExpress.XtraEditors.MemoExEdit MeeLanguage;
        private DevExpress.XtraEditors.MemoExEdit MeeCertificate;
        private DevExpress.XtraEditors.MemoExEdit MeeProject;
        private DevExpress.XtraEditors.LookUpEdit LueProfile;
        private System.Windows.Forms.TabPage TpgUploadedFile;
        protected internal TenTec.Windows.iGridLib.iGrid Grd8;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.TabPage TpgVdDeed;
        protected internal TenTec.Windows.iGridLib.iGrid Grd9;
        internal DevExpress.XtraEditors.DateEdit DteDeedDt2;
        internal DevExpress.XtraEditors.DateEdit DteDeedDt;
        private DevExpress.XtraEditors.LookUpEdit LueType;
        private DevExpress.XtraEditors.MemoExEdit MeeAddress2;
        private DevExpress.XtraEditors.LookUpEdit LueGender;
        private DevExpress.XtraEditors.LookUpEdit LueCountry;
        private DevExpress.XtraEditors.LookUpEdit LueQualification;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        internal DevExpress.XtraEditors.DateEdit DteContractDt;
        internal DevExpress.XtraEditors.DateEdit DteContractEndDt;
        private DevExpress.XtraEditors.LookUpEdit LueProgressCode;
        private System.Windows.Forms.TabPage TpgVehicle;
        protected internal TenTec.Windows.iGridLib.iGrid Grd10;
        private DevExpress.XtraEditors.LookUpEdit LueTransportType;
        private DevExpress.XtraEditors.LookUpEdit LueSubSectorCode;
        private System.Windows.Forms.Panel panel3;
        protected internal DevExpress.XtraEditors.TextEdit TxtVdCodeInternal;
        private System.Windows.Forms.Label label21;
        protected internal DevExpress.XtraEditors.TextEdit TxtEstablishedYr;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        protected internal DevExpress.XtraEditors.LookUpEdit LueParent;
        protected internal DevExpress.XtraEditors.TextEdit TxtWebsite;
        private System.Windows.Forms.Label label18;
        protected internal DevExpress.XtraEditors.TextEdit TxtShortName;
        private System.Windows.Forms.Label label17;
        public DevExpress.XtraEditors.SimpleButton BtnVendorCode;
        private DevExpress.XtraEditors.LookUpEdit LueSDCode;
        private DevExpress.XtraEditors.LookUpEdit LueVilCode;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        protected internal DevExpress.XtraEditors.TextEdit TxtPostalCd;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private System.Windows.Forms.Label label4;
        protected internal DevExpress.XtraEditors.LookUpEdit LueCityCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LblVdCtCode;
        protected internal DevExpress.XtraEditors.LookUpEdit LueVdCtCode;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeAddress;
        private System.Windows.Forms.Label label15;
        protected internal DevExpress.XtraEditors.TextEdit TxtVdName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtVdCode;
        private System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label23;
        private DevExpress.XtraEditors.LookUpEdit LueEntityType;
        private System.Windows.Forms.Label LblProcurementStatus;
        private DevExpress.XtraEditors.LookUpEdit LueProcurementStatus;
        protected internal DevExpress.XtraEditors.TextEdit TxtNIB;
        private System.Windows.Forms.Label label3;
        protected internal DevExpress.XtraEditors.CheckEdit ChkCreateUserVendor;
        public DevExpress.XtraEditors.SimpleButton BtnVendorRegister;
        private System.Windows.Forms.Label LblVendorRegister;
        private DevExpress.XtraEditors.CheckEdit ChkTaxInd;
        private System.Windows.Forms.Label label13;
        protected internal DevExpress.XtraEditors.TextEdit TxtTIN;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.TextEdit TxtIdentityNo;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtCreditLimit;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label11;
        protected internal DevExpress.XtraEditors.TextEdit TxtMobile;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        protected internal DevExpress.XtraEditors.TextEdit TxtPhone;
        protected internal DevExpress.XtraEditors.TextEdit TxtEmail;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        protected internal DevExpress.XtraEditors.TextEdit TxtFax;
        protected internal DevExpress.XtraEditors.TextEdit TxtRMVdCode;
        private System.Windows.Forms.Label label22;
        protected internal DevExpress.XtraEditors.TextEdit TxtSelectionScore;
        private System.Windows.Forms.Label LblSelectionScore;
        protected internal DevExpress.XtraEditors.TextEdit TxtVdExternalCode;
        private System.Windows.Forms.Label LblVdExternalCode;
        private System.Windows.Forms.Label label24;
        public DevExpress.XtraEditors.LookUpEdit LueDeptCode;
    }
}