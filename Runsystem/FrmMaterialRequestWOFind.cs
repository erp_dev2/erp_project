﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequestWOFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmMaterialRequestWO mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmMaterialRequestWOFind(FrmMaterialRequestWO FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                string CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-1);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
                mFrmParent.SetLueDeptCode(ref LueDeptCode);
                if (mFrmParent.mIsFilterBySite)
                    Sl.SetLueSiteCode(ref LueSiteCode, string.Empty);
                else
                    Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
            mFrmParent.mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.DeptName, B.CancelInd, A.WODocNo, ");
            SQL.AppendLine("Case B.Status When 'A' Then 'Approved' When 'C' Then 'Cancel' When 'O' Then 'Outstanding' Else 'Unknown' End As StatusDesc, ");
            SQL.AppendLine("B.ItCode, B.Qty, D.PurchaseUomCode, ");
            SQL.AppendLine("D.ItName, E.SiteName, B.UsageDt, A.CreateBy, A.CreateDt, B.LastUpBy, B.LastUpDt, D.ForeignName, D.ItCodeInternal ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=C.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Inner Join TblItem D ");
            SQL.AppendLine("    On B.ItCode=D.ItCode ");
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
            SQL.AppendLine("Where A.WODocNo Is Not Null ");
            if (mFrmParent.mIsSiteMandatory && mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(")) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Status",
                        "Department",

                        //6-10
                        "Site",
                        "Item's"+Environment.NewLine+"Code",
                        "",
                        "Item's Name",
                        "Local Code",

                        //11-15
                        "Foreign Name",
                        "Quantity",
                        "UoM",
                        "Usage"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"By",
                        
                        //16-20
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time",

                        //21
                        "WO#"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 60, 100, 200, 
                        
                        //6-10
                        150, 80, 20, 250, 100, 
                        
                        //11-15
                        200, 100, 80, 100, 100, 

                        //16-20
                        100, 100, 100, 100, 100,

                        //21
                        150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 14, 16, 19 });
            Sm.GrdFormatTime(Grd1, new int[] { 17, 20 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
            Grd1.Cols[21].Move(5);
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 15, 16, 17, 18, 19, 20 }, false);
            if (!mFrmParent.mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 11 }, false);
            if (!mIsFilterBySite) Sm.GrdColInvisible(Grd1, new int[] { 6 }, false);
            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 15, 16, 17, 18, 19, 20 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                if (ChkExcludedCancelledItem.Checked)
                    Filter = " And A.Status In ('O', 'A') And A.CancelInd='N' And B.Status In ('O', 'A') And B.CancelInd='N' ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "D.ItName", "D.ForeignName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "CancelInd", "StatusDesc", "DeptName", "SiteName", 

                            //6-10
                            "ItCode", "ItName", "ItCodeInternal", "ForeignName", "Qty", 
                            
                            //11-15
                            "PurchaseUomCode", "UsageDt", "CreateBy", "CreateDt", "LastUpBy", 
                            
                            //16-17
                            "LastUpDt", "WODocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 20, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 7));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 7));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0)
                DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(mFrmParent.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mFrmParent.mIsFilterBySite)
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode), string.Empty);
            else
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion
    }
}
