﻿#region Update
/*
    13/12/2021 [BRI/PHT] menu baru  
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
#endregion

namespace RunSystem
{
    public partial class FrmBalanceSheetSetting : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = "", mMaxAccountCategory = "";
        internal bool mInsert = false;

        internal FrmBalanceSheetSettingFind FrmFind;

        #endregion

        #region Constructor

        public FrmBalanceSheetSetting(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Balance Sheet Setting";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                GetParameter();
                base.FrmLoad(sender, e);

                if (mDocNo.Length != 0)
                {
                    //ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid1

            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Account#",
                        "Description",
                        "Type",
                        "Alias",
                        //6
                        "Process"
                    },
                    new int[]
                    {
                        //0
                        20,
 
                        //1-5
                        20, 150, 200, 80, 150,
                        //6
                        80
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 0 });
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 6 });
            #endregion

            #region Grid2

            Grd2.Cols.Count = 7;
            Grd2.FrozenArea.ColCount = 1;
            Grd2.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]
                    {
                        //0
                        "Dno",

                        //1-5
                        "",
                        "Account#",
                        "Description",
                        "Type",
                        "Alias",
                        //6
                        "Process"
                    },
                    new int[]
                    {
                        //0
                        20,

                        20, 150, 200, 80, 150,

                        20
                    }
                );
            Sm.GrdColCheck(Grd2, new int[] { 6 });
            Sm.GrdColInvisible(Grd2, new int[] { 0 });
            Sm.GrdColButton(Grd2, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 4, 5, 6 });
            Sm.GrdColInvisible(Grd2, new int[] { 6 });

            #endregion

            #region Grid3

            Grd3.Cols.Count = 7;
            Grd3.FrozenArea.ColCount = 1;
            Grd3.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "Dno",

                        //1-5
                        "",
                        "Account#",
                        "Description",
                        "Type",
                        "Alias",
                        //6
                        "Process"
                    },
                    new int[]
                    {
                        //0
                        20,

                        20, 150, 200, 80, 150,

                        20
                    }
                );
            Sm.GrdColCheck(Grd3, new int[] { 6 });
            Sm.GrdColInvisible(Grd3, new int[] { 0 });
            Sm.GrdColButton(Grd3, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 3, 4, 5, 6 });
            Sm.GrdColInvisible(Grd3, new int[] { 6 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtSettingCode, DteDocDt, LueYr
                    }, true);
                    ChkActiveInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    DteDocDt.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                      TxtSettingCode, DteDocDt, LueYr
                    }, false);
                    ChkActiveInd.Properties.ReadOnly = false;
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                      DteDocDt, LueYr
                    }, false);
                    ChkActiveInd.Properties.ReadOnly = false;
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    break;
            }
        }

        private void GetParameter()
        {
            mMaxAccountCategory = Sm.GetParameter("MaxAccountCategory");
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtSettingCode, DteDocDt, LueYr
            });
            ClearGrd();
            ChkActiveInd.Checked = false;
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
        }

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBalanceSheetSettingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            string CurrentDateTime = Sm.ServerCurrentDateTime();
            Sl.SetLueYr(LueYr, "");
            Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

            ChkActiveInd.Checked = true;
            mInsert = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSettingCode, "", false)) return;
            SetFormControl(mState.Edit);
            mInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Grid Method
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueYr, "Year")) //BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
            {
                Sm.FormShowDialog(new FrmBalanceSheetSettingDlg(this, "2"));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueYr, "Year")) //BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
            {
                Sm.FormShowDialog(new FrmBalanceSheetSettingDlg(this, "3"));
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueYr, "Year")) //BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
            {
                Sm.FormShowDialog(new FrmBalanceSheetSettingDlg(this, "4"));
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string SettingCode = string.Empty;

            SettingCode = TxtSettingCode.Text;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveBalanceSheetSettingHdr(SettingCode));


            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveBalanceSheetSettingDtl(SettingCode, Row));
            }
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(SaveBalanceSheetSettingDtl2(SettingCode, Row));
            }
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 2).Length > 0) cml.Add(SaveBalanceSheetSettingDtl3(SettingCode, Row));
            }
            Sm.ExecCommands(cml);
            ShowData(SettingCode);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSettingCode, "Setting Code", false) ||
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                //IsSettingCodeAlreadyProcess() ||
                IsDocNoAlreadyProcess() ||
                IsGrdEmpty();
        }

        private bool IsSettingCodeAlreadyProcess()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select SettingCode From TblBalanceSheetSettingHdr Where SettingCode=@SettingCode And ActInd='N'"
            };

            Sm.CmParam<String>(ref cm, "@SettingCode", TxtSettingCode.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "SettingCode already Exist.");
                return true;
            }
            return false;
        }

        private bool IsDocNoAlreadyProcess()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select ActInd From TblBalanceSheetSettingHdr Where ActInd ='Y' And SettingCode <> @SettingCode ;"
            };

            Sm.CmParam<String>(ref cm, "@SettingCode", TxtSettingCode.Text);
            //Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            if (Sm.IsDataExist(cm) && ChkActiveInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "Balance Sheet Active already Exist.");
                return true;
            }
            return false;
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 account.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveBalanceSheetSettingHdr(string SettingCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBalanceSheetSettingHdr(SettingCode, Dt, ActInd, Yr, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SettingCode, @Dt, @ActInd, @Yr, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update Dt=@Dt, Yr=@Yr, ActInd=@ActInd,  ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblBalanceSheetSettingDtl Where SettingCode=@SettingCode; ");
            SQL.AppendLine("Delete From TblBalanceSheetSettingDtl2 Where SettingCode=@SettingCode; ");
            SQL.AppendLine("Delete From TblBalanceSheetSettingDtl3 Where SettingCode=@SettingCode; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveBalanceSheetSettingDtl(string SettingCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBalanceSheetSettingDtl(SettingCode, DNo, AcNo, ProcessInd,  CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SettingCode, @DNo, @AcNo, @ProcessInd, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("0000000" + (Row + 1).ToString(), 6));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetGrdBool(Grd1, Row, 6) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBalanceSheetSettingDtl2(string SettingCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBalanceSheetSettingDtl2(SettingCode, DNo, AcNo, ProcessInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SettingCode, @DNo, @AcNo, @ProcessInd, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 6));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetGrdBool(Grd2, Row, 6) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBalanceSheetSettingDtl3(string SettingCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBalanceSheetSettingDtl3(SettingCode, DNo, AcNo, ProcessInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SettingCode, @DNo, @AcNo, @ProcessInd, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 6));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetGrdBool(Grd3, Row, 6) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (IsActIndDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
            Cursor.Current = Cursors.WaitCursor;
            EditBalanceSheetSettingActive(TxtSettingCode.Text);
            ShowData(TxtSettingCode.Text);
        }

        private bool IsActIndDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSettingCode, "Setting Code", false) ||
                IsActIndEditedAlready();
        }

        private bool IsActIndEditedAlready()
        {
            var ActInd = ChkActiveInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select SettingCode From TblBalanceSheetSettingHdr Where SettingCode=@SettingCode And ActInd='N' "
            };

            Sm.CmParam<String>(ref cm, "@SettingCode", TxtSettingCode.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ActInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This setting already non-active.");
                return true;
            }
            return false;
        }

        private void EditBalanceSheetSettingActive(string SettingCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblBalanceSheetSettingHdr Set ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where SettingCode=@SettingCode ;"
            };
            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string SettingCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowBalanceSheetSettingHdr(SettingCode);
                ShowBalanceSheetSettingDtl(SettingCode);
                ShowBalanceSheetSettingDtl2(SettingCode);
                ShowBalanceSheetSettingDtl3(SettingCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBalanceSheetSettingHdr(string SettingCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select SettingCode, Dt, Yr, ActInd " +
                    "From TblBalanceSheetSettingHdr " +
                    "Where SettingCode=@SettingCode",
                    new string[]
                    { 
                        //0
                        "SettingCode", 

                        //1-3
                        "Dt", "Yr", "ActInd",

                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtSettingCode.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueYr(LueYr, string.Empty);
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[2]));
                        ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    }, true
                );
        }

        private void ShowBalanceSheetSettingDtl(string SettingCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.Acno, B.AcDesc, C.OptDesc, B.Alias, A.Processind ");
            SQL.AppendLine("From tblBalanceSheetSettingdtl A ");
            SQL.AppendLine("Left Join TblCOA B On A.Acno = B.Acno ");
            SQL.AppendLine("Left Join TblOption C On B.AcType = C.OptCode And OptCat = 'AccountType' ");
            SQL.AppendLine("Where A.SettingCode=@SettingCode Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "AcNo", "AcDesc", "OptDesc", "Alias", "ProcessInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowBalanceSheetSettingDtl2(string SettingCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.Acno, B.AcDesc, C.OptDesc, B.Alias, A.Processind ");
            SQL.AppendLine("From tblBalanceSheetSettingdtl2 A ");
            SQL.AppendLine("Left Join TblCOA B On A.Acno = B.Acno ");
            SQL.AppendLine("Left Join TblOption C On B.AcType = C.OptCode And OptCat = 'AccountType' ");
            SQL.AppendLine("Where A.SettingCode=@SettingCode Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "AcNo", "AcDesc", "OptDesc", "Alias", "ProcessInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("B", Grd2, dr, c, Row, 6, 5);

                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowBalanceSheetSettingDtl3(string SettingCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.Acno, B.AcDesc, C.OptDesc, B.Alias, A.Processind ");
            SQL.AppendLine("From tblBalanceSheetSettingdtl3 A ");
            SQL.AppendLine("Left Join TblCOA B On A.Acno = B.Acno ");
            SQL.AppendLine("Left Join TblOption C On B.AcType = C.OptCode And OptCat = 'AccountType' ");
            SQL.AppendLine("Where A.SettingCode=@SettingCode Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "AcNo", "AcDesc", "OptDesc", "Alias", "ProcessInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("B", Grd3, dr, c, Row, 6, 5);

                }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 1);
        }

        #endregion

        #endregion

        #region Event

        #endregion
    }
}
