﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectSystem : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptProjectSystem(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
            base.FrmLoad(sender, e);
        }


        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "Account Desc",

                        //1-5
                        "Account Code", 
                        "Project System",
                        "Project",
                        "Amount Target",
                        "Amount",
                        //6
                        "Level",
                    },
                    new int[] 
                    {
                        400,
                        100, 150, 150, 150, 150, 
                        50
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1 });
            Sm.GrdFormatDec(Grd1, new int[] {4, 5}, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6 }, !ChkHideInfoInGrd.Checked);           

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Sm.ClearGrd(Grd1, false);
                ShowProjectSystem();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }


        private void ShowProjectSystem()
        {
            int Level = 0;
            string PrevAcCode = string.Empty;
            var cm = new MySqlCommand();

            string Filter = " ";
            Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);

            Sm.ClearGrd(Grd1, true);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText =
                    "Select D.DocDt, C.AcCode, Concat(C.AcCode, ' - ', C.AcDesc) As AcDesc, A.Docno, A.ProjectDocNo, " +
                    "A.Amt, C.level " +
                    "From TblProjectSystemDtl A  " +
                    "Left Join (  " +
                    "    Select DocNo, AcCode, AcDesc, ParentDocNo From TblProject  " +
                    "    Where DocNo Not In (  " +
                    "        Select ParentDocNo From TblProject  " +
                    "        Where ParentDocNo Is Not Null  " +
                    "        )  " +
                    ") B On A.ProjectDocno=B.DocNo   " +
                    "Inner Join TblProject C On A.projectDocNo = C.DocNo And ActInd = 'Y'  " +
                    "Inner Join TblProjectSystemHdr D On A.DocNo = D.DocNo "+
                    "Where (D.DocDt Between @DocDt1 And @DocDt2) " + Filter + " " +
                    "Order By C.AcCode, C.level  ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcDesc", "AcCode", "DocNo","ProjectDocNo", "Amt", "Level" });
                if (!dr.HasRows)
                    Sm.StdMsg(mMsgType.NoData, "");
                else
                {
                    int Row = 0;
                    Grd1.Rows.Count = 0;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Grd1.Rows.Add();
                        Level = Convert.ToInt32(Sm.DrStr(dr, 6)); //Sm.DrStr(dr, 1).Length - ((Sm.DrStr(dr, 1).Length + 1) / 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                        Grd1.Rows[Row].Level = Level;
                        Grd1.Cells[Row, 5].Value = 0m;
                        if (Row > 0)
                            Grd1.Rows[Row - 1].TreeButton =
                                (PrevAcCode.Length >= Sm.DrStr(dr, 1).Length) ?
                                    iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                        PrevAcCode = Sm.DrStr(dr, 1);
                        Row++;
                    }
                    Grd1.TreeLines.Visible = true;
                }
                Grd1.EndUpdate();
                dr.Close();
            }
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }
        #endregion
     
    }
}
