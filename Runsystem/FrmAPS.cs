﻿#region Update
/*
    12/05/2017 [TKG] journal otomatis memablik debit menjadi credit, credit menjadi debit.
    31/05/2017 [WED] perbaikan penamaan form dan grid
    06/06/2017 [TKG] Validasi amount boleh negatif (kurang dari 0)
    06/07/2017 [TKG] tambah journal. 
    20/07/2017 [HAR] Bug Fixing ada kurang di query pas ambil SiteCode
    27/07/2017 [ARI] TAMBAH PRINTOUT
    02/09/2017 [TKG] Proses journal berdasarkan parameter IsAPSettlementJournalReverse
    24/09/2017 [TKG] Apabila IsAPSettlementJournalReverse='Y', perhitungan amount dengan COA juga dibalik.
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    12/06/2020 [HAR/SRN] BUG : data header tidak muncul karena PI dicancel
    21/04/2021 [WED/ALL] COA bisa dipilih lebih dari sekali berdasarkan parameter IsCOACouldBeChosenMoreThanOnce
    23/04/2021 [ICA/IMS] Journal -> Jika param CostCenterFormulaForAPAR = 2 maka ketika ada AcNo kepala 5 dan CCCode kosong. CCCode akan ambil dari param CCCodeForJournalAPAR.
    29/07/2021 [IBL/IMS] BUG: journal cancel belum menyimpan CCCode
    16/08/2021 [RDA/ALL] validasi coa auto journal untuk menu AP Settlement
    13/10/2021 [NJP/AMKA] Menambah Parameter APSettlementAmtFormula Jika Nilainya 2 Settlement Amount Dihitung dari Detile Coa yang sesuai dengan COA's Account (AP Invoiced) dari Item Category, sesuai pada Receiving Item yang tercatat pada Purchase Invoice 
    03/01/2022 [TRI/AMKA] Menambahkan validasi agar settlement amount tidak bisa di save saat minus
    10/01/2022 [SET/AMKA] Membuat Journal AP Settlement dapat capture Cost Center yang merujuk ke Cost Center di menu Purchase Invoice
    07/02/2022 [TKG/GSS] ubah GetParameter dan proses save.
    10/01/2023 [DITA/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    10/01/2023 [RDA/PHT] perubahan generate journal docno menggunakan Sm.GetNewJournalDocNoWithAddCodes (khusus PHT)
    24/01/2023 [SET/BBT] Penambahan Local Document
    09/02/2023 [SET/MNET] Penyesuain source cost center journal
    23/03/2023 [BRI/PHT] bug save journal
    14/04/2023 [BRI/TWC] bug save journal
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAPS : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
            mVdCode = string.Empty, mMainCurCode = string.Empty;
        private string mCostCenterFormulaForAPAR = string.Empty,
            mAPSettlementAmtFormula = string.Empty,
            mJournalDocNoFormat = string.Empty;
        private bool
            mIsAutoJournalActived = false,
            mIsAPSettlementJournalReverse = false,
            mIsCheckCOAJournalNotExists = false,
            mIsAPSettlementJournalUseCCCode = false
            ;
        internal bool
            mIsCOACouldBeChosenMoreThanOnce = false,
            mIsClosingJournalBasedOnMultiProfitCenter = false
            ;
        internal FrmAPSFind FrmFind;

        #endregion

        #region Constructor

        public FrmAPS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "AP Settlement";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark",

                        //6
                        "Duplicated"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400,

                        //6
                        0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 6 });
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 6 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo, DteDocDt, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                    TxtDocNo.Focus();
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnPI.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo, DteDocDt, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 3, 4, 5 });
                    DteDocDt.Focus();
                    BtnPI.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtLocalDocNo, DteDocDt, TxtDocNoPI, TxtVdCode, DteVdInvDt, 
                TxtCurCode, TxtOutstandingInv, TxtVdInvNo, TxtAmountAPS, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmountAPS, TxtOutstandingInv }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAPSFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "APS", "APSDtl" };

            var l = new List<APSHdr>();
            var ldtl = new List<APSDtl>();

            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine(" Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
            SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y')As DocDt, A.PurchaseInvoiceDocNo, C.VdName, B.VdInvNo, Date_Format(B.DocDt,'%d %M %Y') As VdInvDt, ");
            SQL.AppendLine("(B.Amt + B.TaxAmt - B.DownPayment)-D.AmtInvOP -E.AmtAps + A.Amt As OutstandingInv, B.CurCode, A.Amt As AmtSet, A.Remark ");
            SQL.AppendLine("From TblApsHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr B On A.PurchaseinvoiceDocNo = B.DocNo  And B.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblVendor C On B.VdCode = C.VdCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("   Select A.InvoiceDocNo, SUM(A.AMT) AS AmtInvOP ");
            SQL.AppendLine("   From tblOutgoingPaymentDtl A ");
            SQL.AppendLine("Inner Join Tbloutgoingpaymenthdr B On A.Docno = B.Docno And B.CancelInd = 'N' ");
            SQL.AppendLine("Where B.Status <> 'C' ");
            SQL.AppendLine("Group By InvoiceDocNo ");
            SQL.AppendLine(")D On D.InvoiceDocNo = A.PurchaseinvoiceDocNo ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select A.PurchaseInvoiceDocno, SUM(A.Amt) As AmtAPS From TblApsHdr A Where A.CancelInd = 'N' ");
            SQL.AppendLine("    Group By  A.PurchaseInvoiceDocno ");
            SQL.AppendLine(") E On B.DocNo = E.PurchaseInvoiceDocno ");
            SQL.AppendLine(" Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocNo", 
                        
                         //6-10
                         "DocDt", 
                         "PurchaseInvoiceDocNo",
                         "VdName", 
                         "VdInvNo",
                         "VdInvDt", 
                         
                         //11-14
                         "OutstandingInv", 
                         "CurCode",
                         "AmtSet",
                         "Remark",
                         
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new APSHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyFax = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),
                            DocDt = Sm.DrStr(dr, c[6]),
                            PurchaseInvoiceDocNo = Sm.DrStr(dr, c[7]),
                            VdName = Sm.DrStr(dr, c[8]),
                            VdInvNo = Sm.DrStr(dr, c[9]),
                            VdInvDt = Sm.DrStr(dr, c[10]),
                            OutstandingInv = Sm.DrStr(dr, c[11]),
                            CurCode = Sm.DrStr(dr, c[12]),
                            AmtSet = Sm.DrStr(dr, c[13]),
                            Remark = Sm.DrStr(dr, c[14]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.Dno, A.ACNo, B.ACDesc, A.DAmt, A.CAmt, A.Remark  ");
                SQLDtl.AppendLine("From TblAPSDtl A ");
                SQLDtl.AppendLine("Inner Join TblCOA B On A.AcNo = B.ACno ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DNo",

                         //1-5
                         "ACNo",
                         "ACDesc",
                         "DAmt",
                         "CAmt",
                         "Remark"

                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new APSDtl()
                        {
                            DNo = Sm.DrStr(drDtl, cDtl[0]),
                            ACNo = Sm.DrStr(drDtl, cDtl[1]),
                            ACDesc = Sm.DrStr(drDtl, cDtl[2]),
                            DAmt = Sm.DrDec(drDtl, cDtl[3]),
                            CAmt = Sm.DrDec(drDtl, cDtl[4]),
                            Remark = Sm.DrStr(drDtl, cDtl[5])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            #endregion

            Sm.PrintReport("APS", myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmAPSDlg(this));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmtWithCOA();
                Sm.GrdEnter(Grd1, e);
                Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmAPSDlg(this));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                Grd1.Cells[e.RowIndex, 4].Value = 0;
                ComputeAmtWithCOA();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                Grd1.Cells[e.RowIndex, 3].Value = 0;
                ComputeAmtWithCOA();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (!Sm.IsTxtEmpty(TxtDocNoPI, "Purchase invoice#", false))
                RecomputeOutstandingPI(TxtDocNoPI.Text);

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Aps", "TblApsHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveAPSHdr(DocNo));
            cml.Add(UpdateProcessIndPI());
            cml.Add(SaveAPSDtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) 
            //        cml.Add(SaveAPSDtl(DocNo, Row));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                 Sm.IsDteEmpty(DteDocDt, "Date") ||
                 Sm.IsTxtEmpty(TxtAmountAPS, "Settlement Amount", true) ||
                 //Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                 (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDocDt), GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt))) ||
                 IsGrdEmpty() ||
                 IsGrdExceedMaxRecords() ||
                 IsJournalAmtNotBalanced() ||
                 IsSettlementBiggerThanOutstanding() ||
                 IsSettlementSmallerThanNull() ||
                 IsSettlementSmallgerThanNull() ||
                 IsDuplicateCOANotHaveRemark() ||
                 IsJournalSettingInvalid()
                 ;
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) return false;

            var Msg =
               "Journal's setting is invalid." + Environment.NewLine +
               "Please contact Finance/Accounting department." + Environment.NewLine;

            if (IsJournalSettingInvalid_GrdCoaEmpty(Msg)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_GrdCoaEmpty(string Msg)
        {
            string ItCOA = string.Empty, ItCOAName = string.Empty;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCOA = Sm.GetGrdStr(Grd1, r, 1);
                ItCOAName = Sm.GetGrdStr(Grd1, r, 2);

                if (ItCOA == string.Empty && ItCOAName.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Item's COA account# (" + ItCOAName + ") is empty.");
                    return true;
                }
            }

            return false;
        }

        private bool IsDuplicateCOANotHaveRemark()
        {
            if (!mIsCOACouldBeChosenMoreThanOnce) return false;
            if (Grd1.Rows.Count <= 1) return false;

            GetDuplicateCOAIndicator();

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdBool(Grd1, i, 6) && Sm.GetGrdStr(Grd1, i, 5).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to fill this remark due to account duplication.");
                    Sm.FocusGrd(Grd1, i, 5);
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 account.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "COA data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsSettlementBiggerThanOutstanding()
        {
            decimal outs = Decimal.Parse(TxtOutstandingInv.Text);
            decimal sett = Decimal.Parse(TxtAmountAPS.Text);
            if (sett > outs)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Settlemet amount is bigger than outstanding amount.");
                return true;
            }
            return false;
        }

        private bool IsSettlementSmallerThanNull()
        {
            decimal sett = Decimal.Parse(TxtAmountAPS.Text);
            if (sett <= 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Settlemet amount must be greater than 0.");
                return true;
            }
            return false;
        }

        private bool IsSettlementSmallgerThanNull()
        {
            if (Decimal.Parse(TxtAmountAPS.Text) == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Settlemet amount should not be 0.");
                return true;
            }
            return false;
        }

        private bool IsJournalAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0) Debit += Sm.GetGrdDec(Grd1, Row, 3);
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) Credit += Sm.GetGrdDec(Grd1, Row, 4);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }
            return false;
        }

        private MySqlCommand SaveAPSHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAPSHdr(DocNo, LocalDocNo, DocDt, CancelInd, PurchaseInvoiceDocNo, Amt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @LocalDocNo, @DocDt, 'N', @PurchaseInvoiceDocNo, @Amt, @Remark, @UserCode, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PurchaseInvoiceDocNo", TxtDocNoPI.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmountAPS.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveAPSDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* AP Settlement - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblAPSDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        "(@DocNo, @DNo_" + r.ToString() + 
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @Remark_" + r.ToString() + 
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 4));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code
        //private MySqlCommand SaveAPSDtl(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblAPSDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @Remark, @CreateBy, CurrentDateTime()) "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd1, Row, 3));
        //    Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd1, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}
        #endregion

        private MySqlCommand UpdateProcessIndPI()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
            if (TxtDocNo.Text.Length == 0)
            {
                if ((Decimal.Parse(TxtOutstandingInv.Text) - Decimal.Parse(TxtAmountAPS.Text)) == 0)
                {
                    SQL.AppendLine("    ProcessInd='F', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                }
                else
                {
                    SQL.AppendLine("    ProcessInd='P', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                }
            }
            else
            {
                if (IsDataPIAlreadyExist() == true)
                {
                    SQL.AppendLine("    ProcessInd='P', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                }
                else
                {
                    SQL.AppendLine("    ProcessInd='O', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                }
            }
            SQL.AppendLine("Where DocNo=@DocNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNoPI.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsDataPIAlreadyExist()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select PurchaseInvoiceDocNo From TblAPSHdr ");
            SQL.AppendLine("Where CancelInd='N' And PurchaseInvoiceDocNo=@PurchaseInvoiceDocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PurchaseInvoiceDocNo", TxtDocNoPI.Text);

            if (Sm.IsDataExist(cm))
            {
                return true;
            }

            return false;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var EntCode = Sm.GetValue(
                     "Select C.EntCode " +
                     "From TblPurchaseInvoiceHdr A, TblSite B, TblProfitCenter C " +
                     "Where A.DocNo=@Param " +
                     "And A.SiteCode=B.SiteCode " +
                     "And B.ProfitCenterCode=C.ProfitCenterCode;",
                     TxtDocNoPI.Text
                     );

            SQL.AppendLine("Update TblAPSHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, ");
            if(mIsAPSettlementJournalUseCCCode)
                SQL.AppendLine("CCCode, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('AP Settlement : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            if (mIsAPSettlementJournalUseCCCode)
                SQL.AppendLine("Ifnull(C.CCCode, O.CCCode) As CCCode, ");
            SQL.AppendLine("A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblAPSHdr A  ");
            if (mIsAPSettlementJournalUseCCCode)
            {
                SQL.AppendLine("INNER JOIN tblpurchaseinvoicehdr B ON A.PurchaseInvoiceDocNo = B.DocNo ");
                SQL.AppendLine("LEFT JOIN tblcostcenter C ON B.DeptCode = C.DeptCode ");
                SQL.AppendLine("Left JOIN tblpurchaseinvoicedtl D ON B.DocNo = D.DocNo ");
                SQL.AppendLine("Left JOIN tblrecvvddtl E ON D.RecvVdDocNo = E.DocNo AND D.RecvVdDNo = E.DNo ");
                SQL.AppendLine("LEFT JOIN tblpodtl F ON E.PODocNo = F.DocNo AND E.PODNo = F.DNo ");
                SQL.AppendLine("LEFT JOIN tblporequestdtl G ON F.PORequestDocNo = G.DocNo AND F.PORequestDNo = G.DNo ");
                SQL.AppendLine("LEFT JOIN tblmaterialrequestdtl H ON G.MaterialRequestDocNo = H.DocNo AND G.MaterialRequestDNo = H.DNo ");
                SQL.AppendLine("Left JOIN tblmaterialrequesthdr I ON H.DocNo = I.DocNo ");
                SQL.AppendLine("LEFT JOIN tbldroppingrequesthdr J ON I.DroppingRequestDocNo = J.DocNo ");
                SQL.AppendLine("LEFT JOIN tblprojectimplementationhdr K ON J.PRJIDocNo = K.DocNo ");
                SQL.AppendLine("LEFT JOIN tblsocontractRevisionhdr L ON K.SOContractDocNo = L.DocNo ");
                SQL.AppendLine("LEFT JOIN tblsocontracthdr M ON L.SOCDocNo = M.DocNo ");
                SQL.AppendLine("LEFT JOIN tblboqhdr N ON M.BOQDocNo = N.DocNo ");
                SQL.AppendLine("LEFT JOIN tbllophdr O ON N.LOPDocNo = O.DocNo ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Limit 1; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, ");
            if (mIsCOACouldBeChosenMoreThanOnce)
                SQL.AppendLine("Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, B.DNo, B.AcNo, ");
            if (mIsAPSettlementJournalReverse)
            {
                if (Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
                    SQL.AppendLine("B.CAmt As DAmt, B.DAMt As CAmt, ");
                else
                    SQL.AppendLine("B.CAmt*IfNull(C.Amt, 0.00) As DAmt, B.DAMt*IfNull(C.Amt, 0.00) As CAmt, ");
            }
            else
            {
                if (Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
                    SQL.AppendLine("B.DAmt, B.CAmt, ");
                else
                    SQL.AppendLine("B.DAmt*IfNull(C.Amt, 0.00) As DAmt, B.CAMt*IfNull(C.Amt, 0.00) As CAmt, ");
            }
            SQL.AppendLine("@EntCode, ");
            if (mIsCOACouldBeChosenMoreThanOnce)
                SQL.AppendLine("B.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblAPSDtl B On B.DocNo=@DocNo ");
            if (!Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
                SQL.AppendLine(") C On 0=0 ");
            }
            SQL.AppendLine("Where A.DocNo=@JournalDocNo Order By B.AcNo;");

            if (mCostCenterFormulaForAPAR == "2")
            {
                SQL.AppendLine("Update TblJournalHdr A ");
                SQL.AppendLine("Inner Join TblAPSHdr B On A.DocNo = B.JournalDocNo ");
                SQL.AppendLine("Inner Join TblParameter C On C.ParCode = 'CCCodeForJournalAPAR' And C.ParValue Is Not Null ");
                SQL.AppendLine("Set A.CCCode = C.ParValue ");
                SQL.AppendLine("Where B.DocNo = @DocNo ");
                SQL.AppendLine("    And A.CCCode is Null ");
                SQL.AppendLine("    And Exists ( ");
                SQL.AppendLine("        Select AcNo From TblJournalDtl ");
                SQL.AppendLine("        Where DocNo = A.DocNo And AcNo like '5.%' ");
                SQL.AppendLine("    ); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (mJournalDocNoFormat == "1")
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            }
            else if (mJournalDocNoFormat == "2")
            {
                string CCCode = Sm.GetValue("SELECT B.CCCode " +
                        "FROM tblpurchaseinvoicehdr A  " +
                        "LEFT JOIN tblcostcenter B ON A.DeptCode = B.DeptCode  " +
                        "WHERE A.DocNo=@Param ", TxtDocNoPI.Text);

                string Code1 = Sm.GetCode1ForJournalDocNo("FrmAPS", string.Empty, string.Empty, mJournalDocNoFormat);
                string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", CCCode);
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteDocDt), 1, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty)));
            }
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelAPSHdr());
            cml.Add(UpdateProcessIndPI());
            if (mIsAutoJournalActived) cml.Add(SaveJournal2());
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                //Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt))) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblAPSHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled .");
                return true;
            }

            return false;
        }

        private MySqlCommand CancelAPSHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAPSHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
           // var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);
            var IsClosingJournalUseCurrentDt = mIsClosingJournalBasedOnMultiProfitCenter ? Sm.IsClosingJournalUseCurrentDt(Sm.GetDte(DteDocDt), GetProfitCenterCode()) : Sm.IsClosingJournalUseCurrentDt(DocDt);


            SQL.AppendLine("Update TblAPSHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling AP Settlement : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblAPSHdr Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblAPSHdr Where DocNo=@DocNo);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
            {
                if (mJournalDocNoFormat == "1")
                {
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                }
                else if (mJournalDocNoFormat == "2")
                {
                    string CCCode = Sm.GetValue("SELECT B.CCCode " +
                            "FROM tblpurchaseinvoicehdr A  " +
                            "LEFT JOIN tblcostcenter B ON A.DeptCode = B.DeptCode  " +
                            "WHERE A.DocNo=@Param ", TxtDocNoPI.Text);

                    string Code1 = Sm.GetCode1ForJournalDocNo("FrmAPS", string.Empty, string.Empty, mJournalDocNoFormat);
                    string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", CCCode);
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(CurrentDt, 1, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty)));
                }
            }
            else
            {
                if (mJournalDocNoFormat == "1")
                {
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                }
                else if (mJournalDocNoFormat == "2")
                {
                    string CCCode = Sm.GetValue("SELECT B.CCCode " +
                            "FROM tblpurchaseinvoicehdr A  " +
                            "LEFT JOIN tblcostcenter B ON A.DeptCode = B.DeptCode  " +
                            "WHERE A.DocNo=@Param ", TxtDocNoPI.Text);

                    string Code1 = Sm.GetCode1ForJournalDocNo("FrmAPS", string.Empty, string.Empty, mJournalDocNoFormat);
                    string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", CCCode);
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(DocDt, 1, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty)));
                }
            }

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowAPSHdr(DocNo);
                ShowAPSDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAPSHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.CancelInd, A.PurchaseInvoiceDocNo, C.VdName, B.VdInvNo, B.DocDt As VdInvDt, ");
            SQL.AppendLine("(B.Amt + B.TaxAmt - B.DownPayment)-IfNull(D.Amt, 0.00)-IfNull(E.Amt, 0.00) As OutstandingAmt, ");
            SQL.AppendLine("B.CurCode, A.Amt, A.Remark ");
            SQL.AppendLine("From TblAPSHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr B On A.PurchaseinvoiceDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblVendor C On B.VdCode=C.VdCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Sum(T2.Amt) As Amt ");
            SQL.AppendLine("    From TblOutgoingPaymentHdr T1, TblOutgoingPaymentDtl T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.Docno ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    And T2.InvoiceDocNo=(Select PurchaseInvoiceDocNo From TblAPSHdr Where DocNo=@DocNo) ");
            SQL.AppendLine(") D On 1=1 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Sum(Amt) As Amt ");
            SQL.AppendLine("    From TblAPSHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And DocNo<>@DocNo ");
            SQL.AppendLine("    And PurchaseInvoiceDocNo=(Select PurchaseInvoiceDocNo From TblAPSHdr Where DocNo=@DocNo) ");
            SQL.AppendLine(") E On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "DocNo", 
                        "LocalDocNo", "DocDt", "CancelInd", "PurchaseInvoiceDocNo", "VdName",
                        "VdInvNo", "VdInvDt", "OutstandingAmt", "CurCode", "Amt",
                        "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y") ? true : false;
                        TxtDocNoPI.EditValue = Sm.DrStr(dr, c[4]);
                        TxtVdCode.EditValue = Sm.DrStr(dr, c[5]);
                        TxtVdInvNo.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetDte(DteVdInvDt, Sm.DrStr(dr, c[7]));
                        TxtOutstandingInv.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        TxtCurCode.EditValue = Sm.DrStr(dr, c[9]);
                        TxtAmountAPS.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                    }, true
                );
        }

        private void ShowAPSDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.ACNo, B.ACDesc, A.DAmt, A.CAmt, A.Remark  ");
            SQL.AppendLine("From TblAPSDtl A ");
            SQL.AppendLine("Inner Join TblCOA B On A.AcNo = B.ACno ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "AcNo", "AcDesc", "DAmt", "CAmt", "Remark"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 5);
                }, false, false, true, false
        );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ShowDataPurchaseInvoice(string PurchaseInvoiceDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select A.DocNo, A.DocDt, A.VdInvNo, A.VdCode, C.Vdname, A.CurCode, (A.Amt - A.Downpayment + A.TaxAmt) - ifnull(B.AmtInvOp, 0) - ifnull(D.AmtAPS, 0) As OutstandingInv ");
            SQL.AppendLine("From TblPurchaseInvoicehdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.InvoiceDocNo, SUM(A.AMT) AS AmtInvOP ");
            SQL.AppendLine("    From tblOutgoingPaymentDtl A ");
            SQL.AppendLine("    Inner Join Tbloutgoingpaymenthdr B On A.Docno = B.Docno And B.CancelInd = 'N' ");
            SQL.AppendLine("    Where B.Status <> 'C' ");
            SQL.AppendLine("    Group By InvoiceDocNo ");
            SQL.AppendLine(")B On B.InvoiceDocNo = A.DocNo ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode = C.VdCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select  A.PurchaseInvoiceDocNo, SUM(A.Amt) As AmtAps ");
            SQL.AppendLine("    From TblApsHdr A  ");
            SQL.AppendLine("    Where A.CancelInd = 'N' ");
            SQL.AppendLine("    Group by A.PurchaseInvoiceDocNo ");
            SQL.AppendLine(")D On D.PurchaseInvoiceDocNo = A.DocNo ");
            SQL.AppendLine("Where A.CancelInd = 'N' And A.ProcessInd <> 'F' ");
            SQL.AppendLine("Group By A.DocNo, A.DocDt, A.VdInvNo, C.Vdname, A.CurCode   ");
            SQL.AppendLine(")Z ");
            SQL.AppendLine("Where Z.DocNo=@DocNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", PurchaseInvoiceDocNo);
            Sm.ShowDataInCtrl(
               ref cm,
               SQL.ToString(),
               new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "VdCode", "VdName", "VdInvNo", "CurCode", 
                        
                        //6
                        "OutstandingInv",
                    },
               (MySqlDataReader dr, int[] c) =>
               {
                   TxtDocNoPI.EditValue = Sm.DrStr(dr, c[0]);
                   Sm.SetDte(DteVdInvDt, Sm.DrStr(dr, c[1]));
                   mVdCode = Sm.DrStr(dr, c[2]);
                   TxtVdCode.EditValue = Sm.DrStr(dr, c[3]);
                   TxtVdInvNo.EditValue = Sm.DrStr(dr, c[4]);
                   TxtCurCode.EditValue = Sm.DrStr(dr, c[5]);
                   TxtOutstandingInv.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
               }, false
              );
        }

        #endregion

        #region Additional Method

        private void GetDuplicateCOAIndicator()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    string AcNo1 = Sm.GetGrdStr(Grd1, i, 1);
                    for (int j = (i + 1); j < Grd1.Rows.Count - 1; ++j)
                    {
                        string AcNo2 = Sm.GetGrdStr(Grd1, j, 1);

                        if (AcNo1 == AcNo2)
                        {
                            Grd1.Cells[i, 6].Value = true;
                            Grd1.Cells[j, 6].Value = true;
                        }
                    }
                }
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAutoJournalActived', 'IsAPSettlementJournalReverse', 'IsAPSettlementJournalUseCCCode', 'IsCOACouldBeChosenMoreThanOnce', 'IsCheckCOAJournalNotExists', ");
            SQL.AppendLine("'APSettlementAmtFormula', 'MainCurCode', 'CostCenterFormulaForAPAR', 'IsClosingJournalBasedOnMultiProfitCenter', 'JournalDocNoFormat' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsCOACouldBeChosenMoreThanOnce": mIsCOACouldBeChosenMoreThanOnce = ParValue == "Y"; break;
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsAPSettlementJournalUseCCCode": mIsAPSettlementJournalUseCCCode = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsAPSettlementJournalReverse": mIsAPSettlementJournalReverse = ParValue == "Y"; break;
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;

                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "CostCenterFormulaForAPAR": mCostCenterFormulaForAPAR = ParValue; break;
                            case "APSettlementAmtFormula": mAPSettlementAmtFormula = ParValue; break;
                            case "JournalDocNoFormat": mJournalDocNoFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }

            if (mCostCenterFormulaForAPAR.Length == 0) mCostCenterFormulaForAPAR = "1";
            if (mAPSettlementAmtFormula.Length == 0) mAPSettlementAmtFormula = "1";
            if (mJournalDocNoFormat.Length == 0) mJournalDocNoFormat = "1";
        }

        internal void ComputeAmtWithCOA()
        {
            decimal Amt = 0m;
            var AcNo = string.Empty;
            var AcType = string.Empty;
            if (mAPSettlementAmtFormula.Trim() == "1")
            {
                AcNo = string.Concat(Sm.GetParameter("VendorAcNoAP"), mVdCode);
                AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", AcNo);
            }
            if (!mIsAPSettlementJournalReverse)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (mAPSettlementAmtFormula.Trim() == "2")
                    {
                        AcNo = Sm.GetValue("SELECT DISTINCT T5.AcNo FROM tblpurchaseinvoicedtl T1 " +
                            "INNER JOIN tblrecvvddtl T2 ON T1.RecvVdDocNo = T2.DocNo AND T1.RecvVdDNo = T2.DNo " +
                            "INNER JOIN tblitem T3 ON T2.ItCode = T3.ItCode " +
                            "INNER JOIN tblitemcategory T4 ON T3.ItCtCode = T4.ItCtCode " +
                            "INNER JOIN tblcoa T5 ON T4.AcNo9 = T5.AcNo " +
                            "WHERE T1.DocNo = '" + TxtDocNoPI.Text + "' AND T5.AcNo = @Param;"
                            , Sm.GetGrdStr(Grd1, Row, 1));

                        AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", AcNo);
                    }
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 1), AcNo))
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 3) != 0m)
                        {
                            if (AcType == "D")
                                Amt -= Sm.GetGrdDec(Grd1, Row, 3);
                            else
                                Amt += Sm.GetGrdDec(Grd1, Row, 3);
                        }
                        if (Sm.GetGrdDec(Grd1, Row, 4) != 0m)
                        {
                            if (AcType == "C")
                                Amt -= Sm.GetGrdDec(Grd1, Row, 4);
                            else
                                Amt += Sm.GetGrdDec(Grd1, Row, 4);
                        }
                    }
                }
            }
            else
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (mAPSettlementAmtFormula.Trim() == "2")
                    {
                        AcNo = Sm.GetValue("SELECT DISTINCT T5.AcNo FROM tblpurchaseinvoicedtl T1 " +
                           "INNER JOIN tblrecvvddtl T2 ON T1.RecvVdDocNo = T2.DocNo AND T1.RecvVdDNo = T2.DNo " +
                           "INNER JOIN tblitem T3 ON T2.ItCode = T3.ItCode " +
                           "INNER JOIN tblitemcategory T4 ON T3.ItCtCode = T4.ItCtCode " +
                           "INNER JOIN tblcoa T5 ON T4.AcNo9 = T5.AcNo " +
                           "WHERE T1.DocNo = '" + TxtDocNoPI.Text + "' AND T5.AcNo = @Param;"
                           , Sm.GetGrdStr(Grd1, Row, 1));

                        AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", AcNo);
                    }
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 1), AcNo))
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 3) != 0)
                        {
                            if (AcType == "D")
                                Amt += Sm.GetGrdDec(Grd1, Row, 3);
                            else
                                Amt -= Sm.GetGrdDec(Grd1, Row, 3);
                        }
                        if (Sm.GetGrdDec(Grd1, Row, 4) != 0)
                        {
                            if (AcType == "C")
                                Amt += Sm.GetGrdDec(Grd1, Row, 4);
                            else
                                Amt -= Sm.GetGrdDec(Grd1, Row, 4);
                        }
                    }
                }
            }
            TxtAmountAPS.EditValue = Sm.FormatNum(Amt, 0);

        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (!mIsCOACouldBeChosenMoreThanOnce)
                            SQL += "##" + Sm.GetGrdStr(Grd1, Row, 1) + "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        public void RecomputeOutstandingPI(string PurchaseInvoiceDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select A.DocNo, (A.Amt - A.Downpayment + A.TaxAmt) - ifnull(B.AmtInvOp, 0) - ifnull(D.AmtAps, 0) As OutstandingInv ");
            SQL.AppendLine("From TblPurchaseInvoicehdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.InvoiceDocNo, SUM(A.AMT) AS AmtInvOP ");
            SQL.AppendLine("    From tblOutgoingPaymentDtl A ");
            SQL.AppendLine("    Inner Join Tbloutgoingpaymenthdr B On A.Docno = B.Docno And B.CancelInd = 'N' ");
            SQL.AppendLine("    Where B.Status <> 'C' ");
            SQL.AppendLine("    Group By InvoiceDocNo ");
            SQL.AppendLine(")B On B.InvoiceDocNo = A.DocNo ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode = C.VdCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select  A.PurchaseInvoiceDocNo, SUM(A.Amt) As AmtAps ");
            SQL.AppendLine("    From TblApsHdr A  ");
            SQL.AppendLine("    Where A.CancelInd = 'N' ");
            SQL.AppendLine("    Group by A.PurchaseInvoiceDocNo ");
            SQL.AppendLine(")D On D.PurchaseInvoiceDocNo = A.DocNo ");
            SQL.AppendLine("Where A.CancelInd = 'N' And A.ProcessInd <> 'F' ");
            SQL.AppendLine("Group By A.DocNo, A.DocDt, A.VdInvNo, C.Vdname, A.CurCode   ");
            SQL.AppendLine(")Z ");
            SQL.AppendLine("Where Z.DocNo=@DocNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", PurchaseInvoiceDocNo);
            Sm.ShowDataInCtrl(
               ref cm,
               SQL.ToString(),
               new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "OutstandingInv",
                    },
               (MySqlDataReader dr, int[] c) =>
               {
                   TxtOutstandingInv.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[1]), 0);
               }, false
              );
        }

        private string GetProfitCenterCode()
        {
            var Value = TxtDocNoPI.Text;
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select B.ProfitCenterCode From TblPurchaseInvoiceHdr  A, TblCostCenter B " +
                    "Where A.DeptCode=B.DeptCode And B.ProfitCenterCode Is Not Null And A.DocNo=@Param;",
                    Value);
        }

        #endregion

        #endregion

        #region Event

        private void BtnPI_Click(object sender, EventArgs e)
        {
            try
            {
                var f = new FrmAPSDlg2(this);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnPI2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDocNoPI, "Purchase Invoice", false))
            {
                try
                {
                    var f = new FrmPurchaseInvoice(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtDocNoPI.Text;
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

        #endregion

        #region Report Class

        private class APSHdr
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string PurchaseInvoiceDocNo { get; set; }
            public string VdName { get; set; }
            public string VdInvNo { get; set; }
            public string VdInvDt { get; set; }
            public string OutstandingInv { get; set; }
            public string CurCode { get; set; }
            public string AmtSet { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }

        }

        private class APSDtl
        {
            public string DNo { get; set; }
            public string ACNo { get; set; }
            public string ACDesc { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public string Remark { get; set; }
        }

        #endregion
    }
}
