﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPLPDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPLP mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPLPDlg2(FrmPLP FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Partner#",
                    "",
                    "Partner Name",
                    "Survey#",
                    "",
                    
                    //6-9
                    "Date",
                    "Loan Amount"+Environment.NewLine+"(Without Interest)",
                    "Installment",
                    "Starting Payment",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    100, 20, 180, 160, 20, 
                    
                    //6-9
                    80, 120, 100, 180
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2, 5 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 6, 7, 8, 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 5 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.PnCode, D.PnName, A.DocNo, A.DocDt, A.Amt, A.Installment, Date_Format(Concat(A.Yr, A.StartMth, '01'), '%b %Y') StartingPayment ");
            SQL.AppendLine("From (Select * From TblSurvey Where Status = 'A' And CancelInd = 'N') A ");
            SQL.AppendLine("Inner Join TblRequestLP C On A.RQLPDocNo = C.DocNo And C.ProcessInd = 'O' ");
            SQL.AppendLine("Inner Join TblPartner D On C.PnCode = D.PnCode ");
            SQL.AppendLine("Inner Join TblVoucherHdr E On A.VoucherRequestDocNo = E.VoucherRequestDocNo And E.CancelInd = 'N' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtPnCode.Text, new string[] { "C.PnCode", "D.PnName", "D.CompanyName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter +
                    " Order By D.PnName, A.DocNo; ",
                    new string[] 
                    {                     
                        //0
                        "PnCode",
                        
                        //1-5
                        "PnName", "DocNo", "DocDt", "Amt", "Installment", 
                        
                        //6
                        "StartingPayment"
                        
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ClearData2();
                mFrmParent.mPnCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtPnName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                mFrmParent.TxtSurveyDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                this.Hide();
            }
            else
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least one voucher request.");

        }

        private bool IsSurveyAlreadyChosen(int Row)
        {
            string key = Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 13);
            for (int Index = 0; Index <= mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    key, mFrmParent.TxtSurveyDocNo.Text + Sm.GetGrdStr(mFrmParent.Grd1, Index, 6)
                    )) return true;
            return false;
        }

        private bool IsSurveyDocNoDifferent()
        {
            string mSurveyDocNo = string.Empty;
            bool mFlag = false;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1))
                {
                    if (mSurveyDocNo.Length > 0)
                    {
                        if (mSurveyDocNo != Sm.GetGrdStr(Grd1, Row, 5))
                        {
                            mFlag = true;
                            break;
                        }
                    }
                    mSurveyDocNo = Sm.GetGrdStr(Grd1, Row, 5);
                }
            }

            if (mFlag) Sm.StdMsg(mMsgType.Warning, "You can only choose from the same Survey Document#");
            return mFlag;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                ChooseData();
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f1 = new FrmPartner(mFrmParent.mMenuCode);
                f1.Text = Sm.GetMenuDesc("FrmPartner");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mPnCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f1 = new FrmSurvey(mFrmParent.mMenuCode);
                f1.Text = Sm.GetMenuDesc("FrmSurvey");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmPartner(mFrmParent.mMenuCode);
                f1.Text = Sm.GetMenuDesc("FrmPartner");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mPnCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f1 = new FrmSurvey(mFrmParent.mMenuCode);
                f1.Text = Sm.GetMenuDesc("FrmSurvey");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtPnCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPnCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Partner");
        }

        #endregion

        #endregion

    }
}
