﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptVendorAccrual : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptVendorAccrual(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -180);
                SetGrd();
                SetSQL();
                Sl.SetLueVdCode(ref LueVdCode);
                SetLueType();
                SetLueStatus();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.VdName, D.OptDesc As Doctype, C.DocNumber, ");
            SQL.AppendLine("C.Amt, E.OptDesc As DocStatus, ");
            SQL.AppendLine("H.VoucherDocNo, I.DocDt As VoucherDocDt, ");
            SQL.AppendLine("L.EntName ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl3 C On A.Docno = C.DocNo ");
            SQL.AppendLine("Inner Join TblOption D On C.Doctype = D.OptCode And D.OptCat = 'PurchaseInvoiceDocType' ");
            SQL.AppendLine("Inner Join TblOption E On C.DocInd = E.OptCode And E.optCat = 'PurchaseInvoiceDocInd' ");
            SQL.AppendLine("Left Join TblOutgoingpaymentDtl F On A.DocNo = F.InvoiceDocNo And F.InvoiceType='1' ");
            SQL.AppendLine("Left Join TblOutgoingPaymentHdr G On F.Docno = G.DocNo And G.CancelInd ='N' ");
            SQL.AppendLine("Left Join TblVoucherRequesthdr H On H.DocNo = G.VoucherrequestDocno And H.CancelInd ='N' ");
            SQL.AppendLine("Left Join TblVoucherhdr I On H.VoucherDocNo = I.DocNo  And I.CancelInd ='N' ");
            SQL.AppendLine("Left Join TblSite J On A.SiteCode = J.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter K On J.ProfitCenterCode = K.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity L On K.EntCode =  L.EntCode  ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Purchase Invoice", 
                        "",
                        "Document"+Environment.NewLine+"Date",
                        "Voucher",
                        "Voucher"+Environment.NewLine+"Date",
                        
                        //6-10
                        "Vendor",
                        "Document Accrual"+Environment.NewLine+"Type",
                        "Document Accrual"+Environment.NewLine+"Number",
                        "Amount",
                        "Document"+Environment.NewLine+"Information",
                        //11
                        "Entity"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        120, 20, 100, 100, 100, 

                        //6-10
                        250, 120, 150, 100, 120,
                        //
                        200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 11 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 11  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtPIDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "C.DocType", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueStatus), "C.DocInd", true);
               
                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order by A.DocNo, C.Dno;",
                new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "VoucherDocNo", "VoucherDocDt", "VdName", "DocType", 

                        //6
                        "DocNumber", "Amt", "DocStatus", "EntName"
                    },
                (
                    MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                }, true, false, false, false
                );
                Grd1.GroupObject.Add(6);
                Grd1.Group();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f2 = new FrmPurchaseInvoice(mMenuCode);
                f2.Tag = mMenuCode;
                f2.WindowState = FormWindowState.Normal;
                f2.StartPosition = FormStartPosition.CenterScreen;
                f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f2.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f2 = new FrmPurchaseInvoice(mMenuCode);
                f2.Tag = mMenuCode;
                f2.WindowState = FormWindowState.Normal;
                f2.StartPosition = FormStartPosition.CenterScreen;
                f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f2.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetLueType()
        {
            Sm.SetLue2(
                ref LueType,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption "+
                "Where OptCat = 'PurchaseInvoiceDocType' ",
                0, 35, false, true, "Code", "Type", "Col2", "Col1");
        }

        private void SetLueStatus()
        {
            Sm.SetLue2(
                ref LueStatus,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption " +
                "Where OptCat = 'PurchaseInvoiceDocInd' ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void TxtPIDocNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPIDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        private void TxtMRDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueType), "<Refresh>")) LueType.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueStatus), "<Refresh>")) LueStatus.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }
      #endregion
    }
}
