﻿#region Update
// 17/05/2018 [HAR] feedback : hanya item yang group nya planned, tambah lot, printout
// 07/07/2018 [HAR] Tidak menyertakan SFC yang sudah dicancel di data Work Center, Bisa klik Calculatesaat EDIT dokumen transaksi yang sudah telanjur di-save

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmWasteSpinning : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = "";
        internal FrmWasteSpinningFind FrmFind;
        private List<FSW> lFSW = null;
        private List<FSW> lFSW2 = null;
            private List<TotalCode> lTotalCode = null;

        #endregion

        #region Constructor

        public FrmWasteSpinning(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Waste Spinning";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
           
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 5;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                       //0
                        "DNo",
                        
                        //1-5
                        "Item Group"+Environment.NewLine+"Code",
                        "Item Group"+Environment.NewLine+"Name",
                        "Short"+Environment.NewLine+"Name",
                        "Production",
                        "Total"+Environment.NewLine+"Production",

                        //6-10
                        "Shift"+Environment.NewLine+"I",
                        "Shift"+Environment.NewLine+"II",
                        "Shift"+Environment.NewLine+"III",
                        "Total",
                        "Total"+Environment.NewLine+"SFC",

                        //11-13
                        "%-Age"+Environment.NewLine+"Waste",
                        "Receive To"+Environment.NewLine+"Warehouse",
                        "%-Age"+Environment.NewLine+"STB",
                    },
                     new int[] 
                    {
                        20, 
                        80, 200, 100, 100, 100,  
                        100, 100, 100, 100, 100,  
                        100, 100, 100
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 9, 10, 11, 13 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 10});

            Grd2.Cols.Count = 4;
            Grd2.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                       //0
                        "Document",
                        
                        //1-5
                        "Date",
                        "Workcenter Name",
                        "Total Production",
                    },
                     new int[] 
                    {
                        150, 
                        80, 200, 150
                    }
                );

            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 2, 3});
            Sm.GrdFormatDate(Grd2, new int[] { 1 });
            Sm.GrdFormatDec(Grd2, new int[] { 3 }, 0);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, DteSfcDt, ChkCancelInd, MeeCancelReason, MeeRemark,
                        TxtWasteDocNo, LueLotCode
                    }, true);
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    TxtDocNo.Focus();
                    BtnProcess.Enabled = false;
                    BtnCalculate.Enabled = false;
                    BtnWasteDocNo.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, DteSfcDt, MeeRemark, LueLotCode
                    }, false);
                    Grd1.ReadOnly = false;
                    DteDocDt.Focus();
                    BtnProcess.Enabled = true;
                    BtnCalculate.Enabled = true;
                    BtnWasteDocNo.Enabled = true;
                    break;
                case mState.Edit:
                    Grd1.ReadOnly = false;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       MeeCancelReason, ChkCancelInd, 
                    }, false);
                    TxtDocNo.Focus();
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    BtnCalculate.Enabled = true;
                    BtnWasteDocNo.Enabled = true;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, DteSfcDt, ChkCancelInd, MeeCancelReason, MeeRemark,
                TxtWasteDocNo, LueLotCode
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
            ChkCancelInd.Checked = false;
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 3 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWasteSpinningFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                ChkCancelInd.Checked = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
                SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            {
                ParPrint(TxtDocNo.Text);
                if (TxtWasteDocNo.Text.Length > 0)
                {
                    ParPrint(TxtWasteDocNo.Text);
                }
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
           
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 6 || e.ColIndex == 7 || e.ColIndex == 8)
            {               
                    ComputeShift(e.RowIndex);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;
            
            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "WasteSpinning", "TblWasteSpinningHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveWSPHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveWSPDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteSfcDt, "SFC Date") ||
                IsGrdEmpty()||
                IsSFCDateAlreadyExist()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 SFC Result#.");
                return true;
            }
            return false;
        }

        private bool IsSFCDateAlreadyExist()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From tblwastespinninghdr " +
                    "Where CancelInd='N' And SFCDt=@SFCDt And Lot=@Lot ;"
            };
            Sm.CmParam<String>(ref cm, "@SFCDt", Sm.GetDte(DteSfcDt).Substring(0, 8));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetLue(LueLotCode));

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "SFC and Lot for this periode already created.");
                return true;
            }
            return false;
        }


        private MySqlCommand SaveWSPHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWasteSpinningHdr ");
            SQL.AppendLine("(DocNo, DocDt, SFCDt, CancelInd, CancelReason, Lot, WasteDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @SFCDt, 'N', @CancelReason, @Lot, @WasteDocNo, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@SFCDt", Sm.GetDte(DteSfcDt));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@Lot" , Sm.GetLue(LueLotCode));
            Sm.CmParam<String>(ref cm, "@WasteDocNo", TxtWasteDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveWSPDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWasteSpinningDtl(DocNo, Dno, ItGrpCode, TotalProd, AmtShift1, AmtShift2, AmtShift3, Total, TotalSFC, ProsenAgeWaste, AmtRecv, ProsenAgeSTB, CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @Dno, @ItGrpCode, @TotalProd, @AmtShift1, @AmtShift2, @AmtShift3, @Total, @TotalSFC, @ProsenAgeWaste, @AmtRecv, @ProsenAgeSTB, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItGrpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@TotalProd", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@AmtShift1", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@AmtShift2", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@AmtShift3", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Total", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@TotalSFC", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@ProsenAgeWaste", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@AmtRecv", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@ProsenAgeSTB", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            EditWasteSpinningHdr();

            var cml = new List<MySqlCommand>();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveWSPDtl(TxtDocNo.Text, Row));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() 
                ;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblWasteSpinningHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false);
        }

        private void EditWasteSpinningHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblWasteSpinningHdr Set CancelInd=@CancelInd, CancelReason=@CancelReason, WasteDocNo=@WasteDocNo, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo And CancelInd='N'; "+
                    "Delete From TblWasteSpinningDtl Where DocNo = @DocNo ; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked==true?"Y":"N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@WasteDocNo", TxtWasteDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }

        #endregion
      

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowWSPHdr(DocNo);
                ShowWSPDtl(DocNo);
                ShowWorkcenter(Sm.GetDte(DteSfcDt));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWSPHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, SFCDt, CancelReason, CancelInd, Lot, WasteDocNo, Remark From TblWasteSpinningHdr Where DocNo=@DocNo",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "SFCDt", "CancelReason", "CancelInd", "Lot", 
                        "WasteDocNo", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetDte(DteSfcDt, Sm.DrStr(dr, c[2]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                        Sm.SetLue(LueLotCode, Sm.DrStr(dr, c[5]));
                        TxtWasteDocNo.EditValue = Sm.DrStr(dr, c[6]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
        }

        private void ShowWSPDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DNo, A.ItGrpCode, B.ItGrpName, B.ItGrpShortName, Param7, TotalProd, A.AmtShift1, A.AmtShift2, A.AmtShift3,  "); 
                SQL.AppendLine("A.Total, A.TotalSFC, A.ProsenAgeWaste, A.AmtRecv, A.ProsenAgeSTB  ");
                SQL.AppendLine("From tblwastespinningdtl A  ");
                SQL.AppendLine("Inner Join TblItemGroup B On A.ItGrpCode = B.ItGrpCode ");
                SQL.AppendLine("Left Join tblformulaspinningwaste C On B.ItgrpCode = C.ItGrpCode And FSWtype = '1' ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.Dno;");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "ItGrpCode", "ItGrpName", "ItGrpShortName", "Param7", "TotalProd",   
                        
                        //6-10
                        "AmtShift1", "AmtShift2", "AmtShift3", "Total", "TotalSFC",  
                        
                        //11-13
                        "ProsenAgeWaste",  "AmtRecv", "ProsenAgeSTB"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 13);
                    }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] {  5, 6, 7, 8, 9, 10, 11, 12, 13 });
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion        

        #region Additional Method

        internal void ProcessSFC(string SFCDt)
        {
            Sm.ClearGrd(Grd1, false);
            try
            {
                CalculateSFC();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void CalculateSFC()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParamDt(ref cm, "@SFCDt", Sm.GetDte(DteSfcDt));

            SQL.AppendLine("Select X1.ItgrpCode, X1.ItGrpName, X1.ItGrpShortName, ");
            SQL.AppendLine("X2.Qty, X2.Qty2, X2.QtyP1, X2.QtyP2, X3.FSWCode, X3.Param7  ");
            SQL.AppendLine("From TblitemGroup X1 ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select X.ItgrpCode, X.ItGrpName, X.ItGrpShortName,  ");
            SQL.AppendLine("    SUM(Qty) Qty, SUM(Qty2) Qty2, SUM(QtyP1) QtyP1, SUM(QtyP2) QtyP2 ");
            SQL.AppendLine("    from  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select D.ItgrpCode, D.ItGrpname, D.ItGrpShortName,  ");
            SQL.AppendLine("        A.productionShiftCode, B.Qty, B.Qty2, ifnull(E.Qty1, 0) QtyP1, ifnull(E.Qty2, 0) QtyP2 ");
            SQL.AppendLine("        From TblShopFloorControlHdr A ");
            SQL.AppendLine("        Inner Join TblShopFloorControlDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TbLitem C On B.itCode = C.itCode ");
            SQL.AppendLine("        Inner Join TblItemGroup D On C.ItgrpCode = D.itGrpCode ");
            SQL.AppendLine("        Left Join  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select A.DocDt, B.ItCode, SUM(C.Qty) Qty1, SUM(C.Qty2) Qty2, SUM(C.Qty3) Qty3  ");
            SQL.AppendLine("            From TblDoWhshdr A ");
            SQL.AppendLine("            inner Join tblDoWhSDtl B On A.DocNo = B.DocnO And B.CancelInd = 'N' ");
            SQL.AppendLine("            Inner join TblRecvWhsDtl C On B.Docno = C.DOWhsDocNO And B.Dno = C.DOWhsDno  ");
            SQL.AppendLine("            Inner Join TblItem D On B.ItCode = D.ItCode ");
            SQL.AppendLine("            Inner Join tblItemgroup E On D.ItgrpCode = E.ItgrpCode ");
            SQL.AppendLine("            And E.ItGrpCode In   ");
            SQL.AppendLine("            (  ");
            SQL.AppendLine("                Select ItGrpCode From tblItemgroup   ");
            SQL.AppendLine("                Where ItgrpCode between 03 And 50  ");
            SQL.AppendLine("            )  ");
            SQL.AppendLine("            Group By A.DocDt, B.ItCode ");
            SQL.AppendLine("        )E On C.ItCode = E.ItCode And A.DocDt = E.DocDt ");
            SQL.AppendLine("        Where A.CancelInd = 'N' And A.DocDt = @SFCDt ");
            SQL.AppendLine("        And D.ItGrpCode In  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select ItGrpCode From tblItemgroup  ");
            SQL.AppendLine("            Where ItgrpCode between 03 And 50 ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine("    Group By X.ItgrpCode, X.ItGrpName, X.ItGrpShortName  ");
            SQL.AppendLine(")X2 On X1.ItGrpCode = X2.itGrpCode ");
            SQL.AppendLine("Left Join tblformulaspinningwaste X3 On X1.ItGrpCode = X3.ItGrpCode And X3.FSWType = '1' ");
            SQL.AppendLine("Where X1.SeqNo > 0 order by SeqNo ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "ItgrpCode", 
                    "ItGrpName", "ItGrpShortName", "Qty", "QtyP1", "FSWCode", 
                    "Param7"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Grd.Cells[Row, 6].Value = 0;
                    Grd.Cells[Row, 7].Value = 0;
                    Grd.Cells[Row, 8].Value = 0;
                    Grd.Cells[Row, 9].Value = 0;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 3);
                    Grd.Cells[Row, 11].Value = 0;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 4);
                    Grd.Cells[Row, 13].Value = 0;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 6);
                    Grd.Cells[Row, 5].Value = CheckWorkcenter(Sm.GetGrdStr(Grd1, Row, 4));
                }, true, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ShowWorkcenter(string SFCDt)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParamDt(ref cm, "@SFCDt", SFCDt);
            Sm.CmParam<String>(ref cm, "@Lot", String.Concat(Sm.GetLue(LueLotCode), "%"));

            SQL.AppendLine("Select A.WorkcenterDocNo, A.DocDt, C.DocName, SUM(B.Qty) As QtyProd ");
            SQL.AppendLine("From TblShopFloorControlhdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join tblworkcenterhdr C On A.WorkcenterDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join Tblitem D on B.ItCode = D.itCode And D.PlanningItemind = 'Y' ");
            SQL.AppendLine("Where A.DocDt=@SFCDt And C.ActiveInd = 'Y' And A.CancelInd = 'N' ");
            SQL.AppendLine("And D.itgrpCode in ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select ItgrpCode From tblItemgroup Where ItGrpCode = '002' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.WorkcenterDocNo not in ( ");
            SQL.AppendLine("    Select DocNo From tblWorkcenterhdr ");
            SQL.AppendLine("    Where DocName = 'blowing' ");
            SQL.AppendLine(") ");
            if (Sm.GetDte(DteSfcDt).Length > 0)
            {
                SQL.AppendLine("And B.batchno like @Lot ");
            }
            SQL.AppendLine("Group By A.WorkcenterDocNo, C.DocName ");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "WorkcenterDocNo", 
                    "DocDt", "DocName", "QtyProd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        internal decimal CheckWorkcenter(string Workcenter)
        {
            decimal AmountTotalProd = 0;

            if (Workcenter == "RSF")
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 2) == "RING SPINNING")
                    {
                        AmountTotalProd = Sm.GetGrdDec(Grd2, Row, 3);
                    }
                }
            }

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Workcenter == Sm.GetGrdStr(Grd2, Row, 2))
                {
                    AmountTotalProd = Sm.GetGrdDec(Grd2, Row, 3);
                }
               
            }

            

            return AmountTotalProd;
        }

        private void ComputeShift(int Row)
        {
            if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
            {
                Grd1.Cells[Row, 9].Value = Sm.GetGrdDec(Grd1, Row, 6) + Sm.GetGrdDec(Grd1, Row, 7) + Sm.GetGrdDec(Grd1, Row, 8);
            }

        }


        private void Computasi()
        {
            string StrQuery = string.Empty;
            string StrQuery2 = string.Empty;

            decimal Param1 = 0,
                Param2 = 0, Param3 = 0,
                Param4 = 0, Param5 = 0,
                Param6 = 0;

            lFSW = new List<FSW>();
            lFSW2 = new List<FSW>();
            FSWList(ref lFSW, "1");
            FSWList(ref lFSW2, "2");

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                for (var i = 0; i < lFSW.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    {
                        StrQuery = Sm.GetValue("Select Query From TblFormulaSpinningWaste Where ItGrpCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And FSWType = '1' ");
                        StrQuery2 = Sm.GetValue("Select Query From TblFormulaSpinningWaste Where ItGrpCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And FSWType = '2' ");
                    }
                    if (lFSW[i].ItGrpCode == Sm.GetGrdStr(Grd1, Row, 1))
                    {
                        for (int j = 0; j <= Grd1.Rows.Count - 1; j++)
                        {
                            if ((lFSW[i].Param1.Length > 0? Sm.Right(lFSW[i].Param1, 3):"") == Sm.GetGrdStr(Grd1, j, 0) || 
                                (lFSW2[i].Param1.Length > 0? Sm.Right(lFSW2[i].Param1, 3):"") == Sm.GetGrdStr(Grd1, j, 0))
                            {
                                Param1 = Sm.GetGrdDec(Grd1, j, 9);
                            }
                            if ((lFSW[i].Param2.Length > 0 ? Sm.Right(lFSW[i].Param2, 3) : "") == Sm.GetGrdStr(Grd1, j, 0) ||
                                (lFSW2[i].Param2.Length > 0 ? Sm.Right(lFSW2[i].Param2, 3) : "") == Sm.GetGrdStr(Grd1, j, 0))
                            {
                                Param2 = Sm.GetGrdDec(Grd1, j, 9);
                            }
                            if ((lFSW[i].Param3.Length > 0 ? Sm.Right(lFSW[i].Param3, 3) : "") == Sm.GetGrdStr(Grd1, j, 0) ||
                                (lFSW2[i].Param3.Length > 0 ? Sm.Right(lFSW2[i].Param3, 3) : "") == Sm.GetGrdStr(Grd1, j, 0))
                            {
                                Param3 = Sm.GetGrdDec(Grd1, j, 9);
                            }
                            if ((lFSW[i].Param4.Length > 0 ? Sm.Right(lFSW[i].Param4, 3) : "") == Sm.GetGrdStr(Grd1, j, 0) ||
                                (lFSW2[i].Param4.Length > 0 ? Sm.Right(lFSW2[i].Param4, 3) : "") == Sm.GetGrdStr(Grd1, j, 0))
                            {
                                Param4 = Sm.GetGrdDec(Grd1, j, 9);
                            }
                            if ((lFSW[i].Param5.Length > 0 ? Sm.Right(lFSW[i].Param5, 3) : "") == Sm.GetGrdStr(Grd1, j, 0) ||
                                (lFSW2[i].Param5.Length > 0 ? Sm.Right(lFSW2[i].Param5, 3) : "") == Sm.GetGrdStr(Grd1, j, 0))
                            {
                                Param5 = Sm.GetGrdDec(Grd1, j, 9);
                            }
                            if ((lFSW[i].Param6.Length > 0 ? Sm.Right(lFSW[i].Param6, 3) : "") == Sm.GetGrdStr(Grd1, j, 0))
                            {
                                Param6 = Sm.GetGrdDec(Grd1, j, 9);
                            }
                        }
                      
                        string DataAgeWaste =
                            Sm.GetValue(
                            "SET @param1 = " + Param1 + "; " +
                            "SET @param2 = " + Param2 + "; " +
                            "SET @param3 = " + Param3 + "; " +
                            "SET @param4 = " + Param4 + "; " +
                            "SET @param5 = " + Param5 + "; " +
                            "SET @param6 = " + Param6 + "; " +
                            "SET @param7 = " + Sm.GetGrdDec(Grd1, Row, 5) + "; " +
                            "Select " + StrQuery + " ; ");

                        if (DataAgeWaste.Length > 0)
                        {
                            Grd1.Cells[Row, 11].Value = Math.Round(decimal.Parse(DataAgeWaste), 2);
                        }
                        else
                        {
                            Grd1.Cells[Row, 11].Value = 0;
                        }

                        string DataAgeSTB =
                           Sm.GetValue(
                           "SET @param1 = " + Param1 + "; " +
                           "SET @param2 = " + Param2 + "; " +
                           "SET @param3 = " + Param3 + "; " +
                           "SET @param4 = " + Param4 + "; " +
                           "SET @param5 = " + Param5 + "; " +
                           "SET @param6 = " + Sm.GetGrdDec(Grd1, Row, 5) + "; " +
                           "SET @param7 = " + Sm.GetGrdDec(Grd1, Row, 12) + "; " +
                           "Select " + StrQuery2 + " ; ");

                        if (DataAgeSTB.Length > 0)
                        {
                            Grd1.Cells[Row, 13].Value = Math.Round(decimal.Parse(DataAgeSTB), 2);
                        }
                        else
                        {
                            Grd1.Cells[Row, 13].Value = 0;
                        }

                    }
                }


            }
        }

        private void ComputeAgeWaste()
        {
           string StrQuery = string.Empty;

           decimal Param1 = 0,
               Param2 = 0, Param3 = 0,
               Param4 = 0, Param5 = 0,
               Param6 = 0;


           lFSW = new List<FSW>();
           FSWList(ref lFSW, "1");

            for (int Row = 0; Row<Grd1.Rows.Count-1; Row++)
            {
               // decimal[] arrayxx = new decimal[20];
                        
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                #region array
                //arrayxx[0] = Sm.GetGrdDec(Grd1, 0, 9);
                //arrayxx[1] = Sm.GetGrdDec(Grd1, 1, 9);
                //arrayxx[2] = Sm.GetGrdDec(Grd1, 2, 9);
                //arrayxx[3] = Sm.GetGrdDec(Grd1, 3, 9);
                //arrayxx[4] = Sm.GetGrdDec(Grd1, 4, 9);
                //arrayxx[5] = Sm.GetGrdDec(Grd1, 5, 9);
                //arrayxx[6] = Sm.GetGrdDec(Grd1, 6, 9);
                //arrayxx[7] = Sm.GetGrdDec(Grd1, 7, 9);
                //arrayxx[8] = Sm.GetGrdDec(Grd1, 8, 9);
                //arrayxx[9] = Sm.GetGrdDec(Grd1, 9, 9);
                //arrayxx[10] = Sm.GetGrdDec(Grd1, 10, 9);
                //arrayxx[11] = Sm.GetGrdDec(Grd1, 11, 9);
                //arrayxx[12] = Sm.GetGrdDec(Grd1, 12, 9);
                //arrayxx[13] = Sm.GetGrdDec(Grd1, 13, 9);
                //arrayxx[14] = Sm.GetGrdDec(Grd1, 14, 9);
                //arrayxx[15] = Sm.GetGrdDec(Grd1, 15, 9);
                //arrayxx[16] = Sm.GetGrdDec(Grd1, 16, 9);
                //arrayxx[17] = Sm.GetGrdDec(Grd1, 17, 9);
                //arrayxx[18] = Sm.GetGrdDec(Grd1, 18, 9);
                //arrayxx[19] = Sm.GetGrdDec(Grd1, 19, 9);
                #endregion

                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 5) == 0)
                        MessageBox.Show("You need input total production " + Sm.GetGrdStr(Grd1, Row, 4) + " " + Environment.NewLine + "for this item " + Sm.GetGrdStr(Grd1, Row, 2) + " " + Environment.NewLine + "to calculate percentage ");
                }


                #region calculate 2

                //for (var i = 0; i < lFSW.Count; i++)
                //{
                //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                //        StrQuery = Sm.GetValue("Select Query From TblFormulaSpinningWaste Where ItGrpCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And FSWType = '1' ");

                //    if (lFSW[i].ItGrpCode == Sm.GetGrdStr(Grd1, Row, 1))
                //    {
                //        for (int j = 0; j <= Grd1.Rows.Count - 1; j++)
                //        {
                //            if (lFSW[i].Param1.Length > 0)
                //            {

                //                if (Sm.Right(lFSW[i].Param1, 3) == Sm.GetGrdStr(Grd1, j, 0))
                //                {
                //                    // Param1 = arrayxx[j];
                //                    Param1 = Sm.GetGrdDec(Grd1, j, 9);
                //                }
                //            }

                //            if (lFSW[i].Param2.Length > 0)
                //            {
                //                if (Sm.Right(lFSW[i].Param2, 3) == Sm.GetGrdStr(Grd1, j, 0))
                //                    {
                //                        //  Param2 = arrayxx[j];
                //                        Param2 = Sm.GetGrdDec(Grd1, j, 9);
                //                    }
                //            }

                //            if (lFSW[i].Param3.Length > 0)
                //            {
                //                if (Sm.Right(lFSW[i].Param3, 3) == Sm.GetGrdStr(Grd1, j, 0))
                //                    {
                //                        //  Param3 = arrayxx[j];
                //                        Param3 = Sm.GetGrdDec(Grd1, j, 9);
                //                    }
                //            }

                //            if (lFSW[i].Param4.Length > 0)
                //            {
                //                if (Sm.Right(lFSW[i].Param4, 3) == Sm.GetGrdStr(Grd1, j, 0))
                //                    {
                //                        //  Param4 = arrayxx[j];
                //                        Param4 = Sm.GetGrdDec(Grd1, j, 9);
                //                    }
                //            }

                //            if (lFSW[i].Param5.Length > 0)
                //            {
                //                 if (Sm.Right(lFSW[i].Param5, 3) == Sm.GetGrdStr(Grd1, j, 0))
                //                    {
                //                        //  Param5 = arrayxx[j];
                //                        Param5 = Sm.GetGrdDec(Grd1, j, 9);
                //                    }
                //            }

                //            if (lFSW[i].Param6.Length > 0)
                //            {
                //                if (Sm.Right(lFSW[i].Param6, 3) == Sm.GetGrdStr(Grd1, j, 0))
                //                    {
                //                        //  Param6 = arrayxx[j];
                //                        Param6 = Sm.GetGrdDec(Grd1, j, 9);
                //                    }
                //            }


                //        }

                //        string DataAgeWaste =
                //            Sm.GetValue(
                //            "SET @param1 = " + Param1 + "; " +
                //            "SET @param2 = " + Param2 + "; " +
                //            "SET @param3 = " + Param3 + "; " +
                //            "SET @param4 = " + Param4 + "; " +
                //            "SET @param5 = " + Param5 + "; " +
                //            "SET @param6 = " + Param6 + "; " +
                //            "SET @param7 = " + Sm.GetGrdDec(Grd1, Row, 5) + "; " +
                //            "Select " + StrQuery + " ; ");

                //        if (DataAgeWaste.Length > 0)
                //        {
                //            Grd1.Cells[Row, 11].Value = Math.Round(decimal.Parse(DataAgeWaste), 2);
                //        }
                //        else
                //        {
                //            Grd1.Cells[Row, 11].Value = 0;
                //        }
                //    }
                //}
                #endregion


                #region calculate

                for (var i = 0; i < lFSW.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        StrQuery = Sm.GetValue("Select Query From TblFormulaSpinningWaste Where ItGrpCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And FSWType = '1' ");

                    if (lFSW[i].ItGrpCode == Sm.GetGrdStr(Grd1, Row, 1))
                    {
                        if (lFSW[i].Param1.Length > 0)
                        {
                            for (int j = 0; j <= Grd1.Rows.Count - 1; j++)
                            {
                                if (Sm.Right(lFSW[i].Param1, 3) == Sm.GetGrdStr(Grd1, j, 0))
                                {
                                    // Param1 = arrayxx[j];
                                    Param1 = Sm.GetGrdDec(Grd1, j, 9);
                                }
                            }
                        }

                        if (lFSW[i].Param2.Length > 0)
                        {
                            for (int j = 0; j <= Grd1.Rows.Count - 1; j++)
                            {
                                if (Sm.Right(lFSW[i].Param2, 3) == Sm.GetGrdStr(Grd1, j, 0))
                                {
                                    //  Param2 = arrayxx[j];
                                    Param2 = Sm.GetGrdDec(Grd1, j, 9);
                                }
                            }
                        }

                        if (lFSW[i].Param3.Length > 0)
                        {
                            for (int j = 0; j <= Grd1.Rows.Count - 1; j++)
                            {
                                if (Sm.Right(lFSW[i].Param3, 3) == Sm.GetGrdStr(Grd1, j, 0))
                                {
                                    //  Param3 = arrayxx[j];
                                    Param3 = Sm.GetGrdDec(Grd1, j, 9);
                                }
                            }
                        }
                        if (lFSW[i].Param4.Length > 0)
                        {
                            for (int j = 0; j <= Grd1.Rows.Count - 1; j++)
                            {
                                if (Sm.Right(lFSW[i].Param4, 3) == Sm.GetGrdStr(Grd1, j, 0))
                                {
                                    //  Param4 = arrayxx[j];
                                    Param4 = Sm.GetGrdDec(Grd1, j, 9);
                                }
                            }
                        }
                        if (lFSW[i].Param5.Length > 0)
                        {
                            for (int j = 0; j <= Grd1.Rows.Count - 1; j++)
                            {
                                if (Sm.Right(lFSW[i].Param5, 3) == Sm.GetGrdStr(Grd1, j, 0))
                                {
                                    //  Param5 = arrayxx[j];
                                    Param5 = Sm.GetGrdDec(Grd1, j, 9);
                                }
                            }
                        }
                        if (lFSW[i].Param6.Length > 0)
                        {
                            for (int j = 0; j <= Grd1.Rows.Count - 1; j++)
                            {
                                if (Sm.Right(lFSW[i].Param6, 3) == Sm.GetGrdStr(Grd1, j, 0))
                                {
                                    //  Param6 = arrayxx[j];
                                    Param6 = Sm.GetGrdDec(Grd1, j, 9);
                                }
                            }
                        }


                        string DataAgeWaste =
                            Sm.GetValue(
                            "SET @param1 = " + Param1 + "; " +
                            "SET @param2 = " + Param2 + "; " +
                            "SET @param3 = " + Param3 + "; " +
                            "SET @param4 = " + Param4 + "; " +
                            "SET @param5 = " + Param5 + "; " +
                            "SET @param6 = " + Param6 + "; " +
                            "SET @param7 = " + Sm.GetGrdDec(Grd1, Row, 5) + "; " +
                            "Select " + StrQuery + " ; ");

                        if (DataAgeWaste.Length > 0)
                        {
                            Grd1.Cells[Row, 11].Value = Math.Round(decimal.Parse(DataAgeWaste), 2);
                        }
                        else
                        {
                            Grd1.Cells[Row, 11].Value = 0;
                        }
                    }
                }
                    #endregion

            }

        }        

        private void ComputeAgeSTB()
        {
            string StrQuery = string.Empty;

            decimal Param1 = 0,
                Param2 = 0, Param3 = 0,
                Param4 = 0, Param5 = 0,
                Param6 = 0;

            lFSW = new List<FSW>();
            FSWList(ref lFSW, "2");

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                //decimal[] arrayxx = new decimal[20];

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                #region Array
                //arrayxx[0] = Sm.GetGrdDec(Grd1, 0, 9);
                //arrayxx[1] = Sm.GetGrdDec(Grd1, 1, 9);
                //arrayxx[2] = Sm.GetGrdDec(Grd1, 2, 9);
                //arrayxx[3] = Sm.GetGrdDec(Grd1, 3, 9);
                //arrayxx[4] = Sm.GetGrdDec(Grd1, 4, 9);
                //arrayxx[5] = Sm.GetGrdDec(Grd1, 5, 9);
                //arrayxx[6] = Sm.GetGrdDec(Grd1, 6, 9);
                //arrayxx[7] = Sm.GetGrdDec(Grd1, 7, 9);
                //arrayxx[8] = Sm.GetGrdDec(Grd1, 8, 9);
                //arrayxx[9] = Sm.GetGrdDec(Grd1, 9, 9);
                //arrayxx[10] = Sm.GetGrdDec(Grd1, 10, 9);
                //arrayxx[11] = Sm.GetGrdDec(Grd1, 11, 9);
                //arrayxx[12] = Sm.GetGrdDec(Grd1, 12, 9);
                //arrayxx[13] = Sm.GetGrdDec(Grd1, 13, 9);
                //arrayxx[14] = Sm.GetGrdDec(Grd1, 14, 9);
                //arrayxx[15] = Sm.GetGrdDec(Grd1, 15, 9);
                //arrayxx[16] = Sm.GetGrdDec(Grd1, 16, 9);
                //arrayxx[17] = Sm.GetGrdDec(Grd1, 17, 9);
                //arrayxx[18] = Sm.GetGrdDec(Grd1, 18, 9);
                //arrayxx[19] = Sm.GetGrdDec(Grd1, 19, 9);
                #endregion

                #region calculate
                for (var i = 0; i < lFSW.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        StrQuery = Sm.GetValue("Select Query From TblFormulaSpinningWaste Where ItGrpCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And FSWType = '2' ");

                    if (lFSW[i].ItGrpCode == Sm.GetGrdStr(Grd1, Row, 1))
                    {
                        if (lFSW[i].Param1.Length > 0)
                        {
                            for (int j = 0; j <= Grd1.Rows.Count - 1; j++)
                            {
                                if (Sm.Right(lFSW[i].Param1, 3) == Sm.GetGrdStr(Grd1, j, 0))
                                {
                                    //Param1 = arrayxx[j];
                                    Param1 = Sm.GetGrdDec(Grd1, j, 9);
                                }
                            }
                        }

                        if (lFSW[i].Param2.Length > 0)
                        {
                            for (int j = 0; j <= Grd1.Rows.Count - 1; j++)
                            {
                                if (Sm.Right(lFSW[i].Param2, 3) == Sm.GetGrdStr(Grd1, j, 0))
                                {
                                    //Param2 = arrayxx[j];
                                    Param2 = Sm.GetGrdDec(Grd1, j, 9);
                                }
                            }
                        }

                        if (lFSW[i].Param3.Length > 0)
                        {
                            for (int j = 0; j <= Grd1.Rows.Count - 1; j++)
                            {
                                if (Sm.Right(lFSW[i].Param3, 3) == Sm.GetGrdStr(Grd1, j, 0))
                                {
                                    //Param3 = arrayxx[j];
                                    Param3 = Sm.GetGrdDec(Grd1, j, 9);
                                }
                            }
                        }
                        if (lFSW[i].Param4.Length > 0)
                        {
                            for (int j = 0; j <= Grd1.Rows.Count - 1; j++)
                            {
                                if (Sm.Right(lFSW[i].Param4, 3) == Sm.GetGrdStr(Grd1, j, 0))
                                {
                                    //Param4 = arrayxx[j];
                                    Param4 = Sm.GetGrdDec(Grd1, j, 9);
                                }
                            }
                        }
                        if (lFSW[i].Param5.Length > 0)
                        {
                            for (int j = 0; j <= Grd1.Rows.Count - 1; j++)
                            {
                                if (Sm.Right(lFSW[i].Param5, 3) == Sm.GetGrdStr(Grd1, j, 0))
                                {
                                    //Param5 = arrayxx[j];
                                    Param5 = Sm.GetGrdDec(Grd1, j, 9);
                                }
                            }
                        }
                        //if (lFSW[i].Param6.Length > 0)
                        //{
                        //    for (int j = 0; j <= Grd1.Rows.Count - 1; j++)
                        //    {
                        //        if (Sm.Right(lFSW[i].Param6, 3) == Sm.GetGrdStr(Grd1, j, 0))
                        //        {
                        //            Param6 = Sm.GetGrdDec(Grd1, j, 9);
                        //        }
                        //    }
                        //}


                        string DataAgeSTB =
                            Sm.GetValue(
                            "SET @param1 = " + Param1 + "; " +
                            "SET @param2 = " + Param2 + "; " +
                            "SET @param3 = " + Param3 + "; " +
                            "SET @param4 = " + Param4 + "; " +
                            "SET @param5 = " + Param5 + "; " +
                            "SET @param6 = " + Sm.GetGrdDec(Grd1, Row, 5) + "; " +
                            "SET @param7 = " + Sm.GetGrdDec(Grd1, Row, 12) + "; " +
                            "Select " + StrQuery + " ; ");

                        if (DataAgeSTB.Length > 0)
                        {
                            Grd1.Cells[Row, 13].Value = Math.Round(decimal.Parse(DataAgeSTB), 2);
                        }
                        else
                        {
                            Grd1.Cells[Row, 13].Value = 0;
                        }
                    }
                }
                #endregion
            }

        }

        private void FSWList(ref List<FSW> l, string TypeTrx)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select FswCode, ItGrpCode, FSWname, FSWType, Query, Param1, ");  
            SQL.AppendLine("Param2, Param3, param4, Param5, Param6, Param7 ");
            SQL.AppendLine("From TblFormulaSpinningWaste  ");
            if (TypeTrx == "1")
            {
                SQL.AppendLine("Where FSWtype = '1' ");
            }
            else
            {
                SQL.AppendLine("Where FSWtype = '2' ");
            }
            SQL.AppendLine(";");
            

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "FswCode", 
                    "ItGrpCode", "FSWname", "FSWType", "Query", "Param1",
                    "Param2", "Param3", "param4", "Param5", "Param6", 
                    "Param7"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new FSW()
                        {
                            FSWCode = Sm.DrStr(dr, c[0]),
                            ItGrpCode = Sm.DrStr(dr, c[1]),
                            FSWName = Sm.DrStr(dr, c[2]),
                            FSWType = Sm.DrStr(dr, c[3]),
                            Query = Sm.DrStr(dr, c[4]),
                            Param1 = Sm.DrStr(dr, c[5]),
                            Param2 = Sm.DrStr(dr, c[6]),
                            Param3 = Sm.DrStr(dr, c[7]),
                            Param4 = Sm.DrStr(dr, c[8]),
                            Param5 = Sm.DrStr(dr, c[9]),
                            Param6 = Sm.DrStr(dr, c[10]),
                            Param7 = Sm.DrStr(dr, c[11]),
                        });
                    }
                }
                dr.Close();
            }
        }

        internal void SetLueLotCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Substring(B.batchNo, 1, (locate('(', B.batchNo)-1)) As Col1, Substring(B.batchNo, 1, (locate('(', B.batchNo)-1)) As Col2 ");
            SQL.AppendLine("From tblShopFloorControlhdr A ");
            SQL.AppendLine("inner Join TblShopFloorControlDtl b On A.Docno = b.Docno ");
            SQL.AppendLine("Where A.DocDt = @DocDt ");
            SQL.AppendLine("Order By B.BatchNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteSfcDt).Substring(0, 8));


            Sm.SetLue2(
             ref Lue, ref cm,
             0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ParPrint(string DocNo)
        {
            var ld = new List<WSPHdr>();
            var ldtl = new List<WSPDtl>();
            var ldtl2 = new List<WSPDtl2>();
            var ldtl3 = new List<WSPDtl3>();
            //var ld_A = new List<WSPHdr_A>();
            //var ldtl_B = new List<WSPDtl_B>();
            //var ldtl2_C = new List<WSPDtl2_C>();
            //var ldtl3_D = new List<WSPDtl3_D>();

            //string[] TableName = { "WSP", "WSPDtl", "WSPDtl2", "WSPDtl3", "WSP_A", "WSPDtl_B", "WSPDtl2_C", "WSPDtl3_D" };
            string[] TableName = { "WSP", "WSPDtl", "WSPDtl2", "WSPDtl3" };
            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', "); 
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', "); 
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax',  "); 
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%M %d, %Y') As DocDt,  ");
            SQL.AppendLine("DATE_FORMAT(A.SFcDt,'%M %d, %Y') As SfcDt, A.Remark, A.Lot  "); 
            SQL.AppendLine("From tblWasteSpinninghdr A  ");  
            SQL.AppendLine("Where A.DocNo=@DocNo  ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         //6-10
                         "DocNo",
                         "DocDt",
                         "SfcDt",
                         "Remark",
                         "Lot"
                    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ld.Add(new WSPHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),
                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            SFCDt = Sm.DrStr(dr, c[8]),
                            Remark = Sm.DrStr(dr, c[9]),
                            Lot = Sm.DrStr(dr, c[10]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(ld);

            #endregion

            #region Detail data 1
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                
                SQLDtl.AppendLine("Select A.ItgrpCode, A.Itgrpname, A.ItGrpShortName, ifnull(B.AmtShift1, 0) AmtShift1,  ");
                SQLDtl.AppendLine("ifnull(B.AmtShift2, 0) AmtShift2, ifnull(B.AmtShift3, 0) AmtShift3,  ");
                SQLDtl.AppendLine("ifnull(B.Total, 0) Total, ifnull(B.TotalSFC, 0) TotalSFC, ifnull(B.ProsenAgeWaste, 0) ProsenAgeWaste,  ");
                SQLDtl.AppendLine("ifnull(B.AmtRecv, 0) AmtRecv, ifnull(B.ProsenAgeSTB, 0) ProsenAgeSTB, ifnull(C.AccShift1, 0) AccShift1,  ");
                SQLDtl.AppendLine("ifnull(C.AccShift2, 0) AccShift2,  ifnull(C.AccShift3, 0) AccShift3, ifnull(C.AccTotal, 0) AccTotal,  ");
                SQLDtl.AppendLine("ifnull(C.AccTotalSFC, 0) AccTotalSFC, ifnull(C.AccProsenAgeWaste, 0) AccProsenAgeWaste,  ");
                SQLDtl.AppendLine("ifnull(C.AccAmtRecv, 0) AccAmtRecv, ifnull(C.AccProsenAgeSTB, 0) AccProsenAgeSTB  ");
                SQLDtl.AppendLine("from tblitemGroup A ");
                SQLDtl.AppendLine("Left Join  ");
                SQLDtl.AppendLine("( ");
	            SQLDtl.AppendLine("    Select A.DNo, A.ItGrpCode, A.AmtShift1, A.AmtShift2, A.AmtShift3,  ");
	            SQLDtl.AppendLine("    A.Total, A.TotalSFC, A.ProsenAgeWaste, A.AmtRecv, A.ProsenAgeSTB  ");
	            SQLDtl.AppendLine("    From tblwastespinningdtl A  ");
                SQLDtl.AppendLine("    Where A.DocNo=@DocNo ");
                SQLDtl.AppendLine(")B On A.ItGrpCode = B.ItgrpCode ");
                SQLDtl.AppendLine("Left Join  ");
                SQLDtl.AppendLine("( ");
	            SQLDtl.AppendLine("    Select B.ItGrpCode, SUM(B.AmtShift1) AccShift1, SUM(B.AmtShift2) AccShift2, ");
	            SQLDtl.AppendLine("    SUM(B.AmtShift3) AccShift3, SUM(B.Total) AccTotal, SUM(B.TotalSFC) AccTotalSFC,  ");
	            SQLDtl.AppendLine("    SUm(B.ProsenAgeWaste) AccProsenAgeWaste, SUM(B.AmtRecv) AccAmtRecv, SUM(B.ProsenAgeSTB) AccProsenAgeSTB  ");
                SQLDtl.AppendLine("    From tblwasteSpinninghdr A");
                SQLDtl.AppendLine("    inner Join tblwastespinningdtl B on A.DocNo = B.DocNo ");
                SQLDtl.AppendLine("    Where A.SfcDt Between '"+string.Concat(Sm.Left(Sm.GetDte(DteSfcDt), 6), "01")+"' And '"+Sm.GetDte(DteSfcDt).Substring(0, 8)+"'  And A.Cancelind = 'N' ");
	            SQLDtl.AppendLine("    Group By B.ItGrpCode ");
                SQLDtl.AppendLine(")C On A.ItGrpCode = C.ItgrpCode ");
                SQLDtl.AppendLine("Where A.ActInd = 'Y' And A.ItGrpCode between '03' And '20' ");

                

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ItGrpCode" ,
                         
                         //1-5
                         "ItGrpName",
                         "ItGrpShortName" ,
                         "AmtShift1",
                         "AmtShift2" ,
                         "AmtShift3" ,
                         
                         //6-10
                         "Total" ,
                         "TotalSFC",
                         "ProsenAgeWaste",
                         "AmtRecv" ,
                         "ProsenAgeSTB", 
                         
                         //11-15
                         "AccShift1",
                         "AccShift2" ,
                         "AccShift3" ,
                         "AccTotal" ,
                         "AccTotalSFC",
                         
                         //16-18
                         "AccProsenAgeWaste",
                         "AccAmtRecv" ,
                         "AccProsenAgeSTB" 
                        });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new WSPDtl()
                        {
                            no = nomor,
                            ItGrpCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItGrpName = Sm.DrStr(drDtl, cDtl[1]),
                            ItGrpShortName = Sm.DrStr(drDtl, cDtl[2]),
                            AmtShift1 = Sm.DrDec(drDtl, cDtl[3]),
                            AmtShift2 = Sm.DrDec(drDtl, cDtl[4]),
                            AmtShift3 = Sm.DrDec(drDtl, cDtl[5]),
                            AmtTotal = Sm.DrDec(drDtl, cDtl[6]),
                            TotalSFC = Sm.DrDec(drDtl, cDtl[7]),
                            AmtAgeWaste = Sm.DrDec(drDtl, cDtl[8]),
                            AmtRecv = Sm.DrDec(drDtl, cDtl[9]),
                            AmtAgeSTB = Sm.DrDec(drDtl, cDtl[10]),

                            AccAmtShift1 = Sm.DrDec(drDtl, cDtl[11]),
                            AccAmtShift2 = Sm.DrDec(drDtl, cDtl[12]),
                            AccAmtShift3 = Sm.DrDec(drDtl, cDtl[13]),
                            AccAmtTotal = Sm.DrDec(drDtl, cDtl[14]),
                            AccTotalSFC = Sm.DrDec(drDtl, cDtl[15]),
                            AccAmtAgeWaste = Sm.DrDec(drDtl, cDtl[16]),
                            AccAmtRecv = Sm.DrDec(drDtl, cDtl[17]),
                            AccAmtAgeSTB = Sm.DrDec(drDtl, cDtl[18]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail data 2
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select A.ItgrpCode, A.Itgrpname, A.ItGrpShortName, ifnull(B.AmtShift1, 0) AmtShift1,  ");
                SQLDtl2.AppendLine("ifnull(B.AmtShift2, 0) AmtShift2, ifnull(B.AmtShift3, 0) AmtShift3,  ");
                SQLDtl2.AppendLine("ifnull(B.Total, 0) Total, ifnull(B.TotalSFC, 0) TotalSFC, ifnull(B.ProsenAgeWaste, 0) ProsenAgeWaste,  ");
                SQLDtl2.AppendLine("ifnull(B.AmtRecv, 0) AmtRecv, ifnull(B.ProsenAgeSTB, 0) ProsenAgeSTB, ifnull(C.AccShift1, 0) AccShift1,  ");
                SQLDtl2.AppendLine("ifnull(C.AccShift2, 0) AccShift2,  ifnull(C.AccShift3, 0) AccShift3, ifnull(C.AccTotal, 0) AccTotal,  ");
                SQLDtl2.AppendLine("ifnull(C.AccTotalSFC, 0) AccTotalSFC, ifnull(C.AccProsenAgeWaste, 0) AccProsenAgeWaste,  ");
                SQLDtl2.AppendLine("ifnull(C.AccAmtRecv, 0) AccAmtRecv, ifnull(C.AccProsenAgeSTB, 0) AccProsenAgeSTB  ");
                SQLDtl2.AppendLine("from tblitemGroup A ");
                SQLDtl2.AppendLine("Left Join  ");
                SQLDtl2.AppendLine("( ");
                SQLDtl2.AppendLine("    Select A.DNo, A.ItGrpCode, A.AmtShift1, A.AmtShift2, A.AmtShift3,  ");
                SQLDtl2.AppendLine("    A.Total, A.TotalSFC, A.ProsenAgeWaste, A.AmtRecv, A.ProsenAgeSTB  ");
                SQLDtl2.AppendLine("    From tblwastespinningdtl A  ");
                SQLDtl2.AppendLine("    Where A.DocNo=@DocNo ");
                SQLDtl2.AppendLine(")B On A.ItGrpCode = B.ItgrpCode ");
                SQLDtl2.AppendLine("Left Join  ");
                SQLDtl2.AppendLine("( ");
                SQLDtl2.AppendLine("    Select B.ItGrpCode, SUM(B.AmtShift1) AccShift1, SUM(B.AmtShift2) AccShift2, ");
                SQLDtl2.AppendLine("    SUM(B.AmtShift3) AccShift3, SUM(B.Total) AccTotal, SUM(B.TotalSFC) AccTotalSFC,  ");
                SQLDtl2.AppendLine("    SUm(B.ProsenAgeWaste) AccProsenAgeWaste, SUM(B.AmtRecv) AccAmtRecv, SUM(B.ProsenAgeSTB) AccProsenAgeSTB  ");
                SQLDtl2.AppendLine("    From tblwasteSpinninghdr A");
                SQLDtl2.AppendLine("    inner Join tblwastespinningdtl B on A.DocNo = B.DocNo ");
                SQLDtl2.AppendLine("    Where A.SfcDt Between '" + string.Concat(Sm.Left(Sm.GetDte(DteSfcDt), 6), "01") + "' And '" + Sm.GetDte(DteSfcDt).Substring(0, 8) + "'  And A.Cancelind = 'N' ");
                SQLDtl2.AppendLine("    Group By B.ItGrpCode ");
                SQLDtl2.AppendLine(")C On A.ItGrpCode = C.ItgrpCode ");
                SQLDtl2.AppendLine("Where A.ActInd = 'Y' And A.ItGrpCode between '21' And '25' ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", DocNo);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "ItGrpCode" ,
                         
                         //1-5
                         "ItGrpName",
                         "ItGrpShortName" ,
                         "AmtShift1",
                         "AmtShift2" ,
                         "AmtShift3" ,
                         
                         //6-10
                         "Total" ,
                         "TotalSFC",
                         "ProsenAgeWaste",
                         "AmtRecv" ,
                         "ProsenAgeSTB", 
                         
                         //11-15
                         "AccShift1",
                         "AccShift2" ,
                         "AccShift3" ,
                         "AccTotal" ,
                         "AccTotalSFC",
                         
                         //16-18
                         "AccProsenAgeWaste",
                         "AccAmtRecv" ,
                         "AccProsenAgeSTB" 
                        });
                if (drDtl2.HasRows)
                {
                    int nomor = 0;
                    while (drDtl2.Read())
                    {
                        nomor = nomor + 1;
                        ldtl2.Add(new WSPDtl2()
                        {
                            no = nomor,
                            ItGrpCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            ItGrpName = Sm.DrStr(drDtl2, cDtl2[1]),
                            ItGrpShortName = Sm.DrStr(drDtl2, cDtl2[2]),
                            AmtShift1 = Sm.DrDec(drDtl2, cDtl2[3]),
                            AmtShift2 = Sm.DrDec(drDtl2, cDtl2[4]),
                            AmtShift3 = Sm.DrDec(drDtl2, cDtl2[5]),
                            AmtTotal = Sm.DrDec(drDtl2, cDtl2[6]),
                            TotalSFC = Sm.DrDec(drDtl2, cDtl2[7]),
                            AmtAgeWaste = Sm.DrDec(drDtl2, cDtl2[8]),
                            AmtRecv = Sm.DrDec(drDtl2, cDtl2[9]),
                            AmtAgeSTB = Sm.DrDec(drDtl2, cDtl2[10]),

                            AccAmtShift1 = Sm.DrDec(drDtl2, cDtl2[11]),
                            AccAmtShift2 = Sm.DrDec(drDtl2, cDtl2[12]),
                            AccAmtShift3 = Sm.DrDec(drDtl2, cDtl2[13]),
                            AccAmtTotal = Sm.DrDec(drDtl2, cDtl2[14]),
                            AccTotalSFC = Sm.DrDec(drDtl2, cDtl2[15]),
                            AccAmtAgeWaste = Sm.DrDec(drDtl2, cDtl2[16]),
                            AccAmtRecv = Sm.DrDec(drDtl2, cDtl2[17]),
                            AccAmtAgeSTB = Sm.DrDec(drDtl2, cDtl2[18]),
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail data 3
            var cmDtl3 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;

                SQLDtl3.AppendLine("Select A.ItgrpCode, A.Itgrpname, A.ItGrpShortName, ifnull(B.AmtShift1, 0) AmtShift1,  ");
                SQLDtl3.AppendLine("ifnull(B.AmtShift2, 0) AmtShift2, ifnull(B.AmtShift3, 0) AmtShift3,  ");
                SQLDtl3.AppendLine("ifnull(B.Total, 0) Total, ifnull(B.TotalSFC, 0) TotalSFC, ifnull(B.ProsenAgeWaste, 0) ProsenAgeWaste,  ");
                SQLDtl3.AppendLine("ifnull(B.AmtRecv, 0) AmtRecv, ifnull(B.ProsenAgeSTB, 0) ProsenAgeSTB, ifnull(C.AccShift1, 0) AccShift1,  ");
                SQLDtl3.AppendLine("ifnull(C.AccShift2, 0) AccShift2,  ifnull(C.AccShift3, 0) AccShift3, ifnull(C.AccTotal, 0) AccTotal,  ");
                SQLDtl3.AppendLine("ifnull(C.AccTotalSFC, 0) AccTotalSFC, ifnull(C.AccProsenAgeWaste, 0) AccProsenAgeWaste,  ");
                SQLDtl3.AppendLine("ifnull(C.AccAmtRecv, 0) AccAmtRecv, ifnull(C.AccProsenAgeSTB, 0) AccProsenAgeSTB  ");
                SQLDtl3.AppendLine("from tblitemGroup A ");
                SQLDtl3.AppendLine("Left Join  ");
                SQLDtl3.AppendLine("( ");
                SQLDtl3.AppendLine("    Select A.DNo, A.ItGrpCode, A.AmtShift1, A.AmtShift2, A.AmtShift3,  ");
                SQLDtl3.AppendLine("    A.Total, A.TotalSFC, A.ProsenAgeWaste, A.AmtRecv, A.ProsenAgeSTB  ");
                SQLDtl3.AppendLine("    From tblwastespinningdtl A  ");
                SQLDtl3.AppendLine("    Where A.DocNo=@DocNo ");
                SQLDtl3.AppendLine(")B On A.ItGrpCode = B.ItgrpCode ");
                SQLDtl3.AppendLine("Left Join  ");
                SQLDtl3.AppendLine("( ");
                SQLDtl3.AppendLine("    Select B.ItGrpCode, SUM(B.AmtShift1) AccShift1, SUM(B.AmtShift2) AccShift2, ");
                SQLDtl3.AppendLine("    SUM(B.AmtShift3) AccShift3, SUM(B.Total) AccTotal, SUM(B.TotalSFC) AccTotalSFC,  ");
                SQLDtl3.AppendLine("    SUm(B.ProsenAgeWaste) AccProsenAgeWaste, SUM(B.AmtRecv) AccAmtRecv, SUM(B.ProsenAgeSTB) AccProsenAgeSTB  ");
                SQLDtl3.AppendLine("    From tblwasteSpinninghdr A");
                SQLDtl3.AppendLine("    inner Join tblwastespinningdtl B on A.DocNo = B.DocNo ");
                SQLDtl3.AppendLine("    Where A.SfcDt Between '" + string.Concat(Sm.Left(Sm.GetDte(DteSfcDt), 6), "01") + "' And '" + Sm.GetDte(DteSfcDt).Substring(0, 8) + "'  And A.Cancelind = 'N' ");
                SQLDtl3.AppendLine("    Group By B.ItGrpCode ");
                SQLDtl3.AppendLine(")C On A.ItGrpCode = C.ItgrpCode ");
                SQLDtl3.AppendLine("Where A.ActInd = 'Y' And A.ItGrpCode between '26' And '50' ");

                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", DocNo);
                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                        {
                         //0
                         "ItGrpCode" ,
                         
                         //1-5
                         "ItGrpName",
                         "ItGrpShortName" ,
                         "AmtShift1",
                         "AmtShift2" ,
                         "AmtShift3" ,
                         
                         //6-10
                         "Total" ,
                         "TotalSFC",
                         "ProsenAgeWaste",
                         "AmtRecv" ,
                         "ProsenAgeSTB", 
                         
                         //11-15
                         "AccShift1",
                         "AccShift2" ,
                         "AccShift3" ,
                         "AccTotal" ,
                         "AccTotalSFC",
                         
                         //16-18
                         "AccProsenAgeWaste",
                         "AccAmtRecv" ,
                         "AccProsenAgeSTB" 
                        });
                if (drDtl3.HasRows)
                {
                    int nomor = 0;
                    while (drDtl3.Read())
                    {
                        nomor = nomor + 1;
                        ldtl3.Add(new WSPDtl3()
                        {
                            no = nomor,
                            ItGrpCode = Sm.DrStr(drDtl3, cDtl3[0]),
                            ItGrpName = Sm.DrStr(drDtl3, cDtl3[1]),
                            ItGrpShortName = Sm.DrStr(drDtl3, cDtl3[2]),
                            AmtShift1 = Sm.DrDec(drDtl3, cDtl3[3]),
                            AmtShift2 = Sm.DrDec(drDtl3, cDtl3[4]),
                            AmtShift3 = Sm.DrDec(drDtl3, cDtl3[5]),
                            AmtTotal = Sm.DrDec(drDtl3, cDtl3[6]),
                            TotalSFC = Sm.DrDec(drDtl3, cDtl3[7]),
                            AmtAgeWaste = Sm.DrDec(drDtl3, cDtl3[8]),
                            AmtRecv = Sm.DrDec(drDtl3, cDtl3[9]),
                            AmtAgeSTB = Sm.DrDec(drDtl3, cDtl3[10]),

                            AccAmtShift1 = Sm.DrDec(drDtl3, cDtl3[11]),
                            AccAmtShift2 = Sm.DrDec(drDtl3, cDtl3[12]),
                            AccAmtShift3 = Sm.DrDec(drDtl3, cDtl3[13]),
                            AccAmtTotal = Sm.DrDec(drDtl3, cDtl3[14]),
                            AccTotalSFC = Sm.DrDec(drDtl3, cDtl3[15]),
                            AccAmtAgeWaste = Sm.DrDec(drDtl3, cDtl3[16]),
                            AccAmtRecv = Sm.DrDec(drDtl3, cDtl3[17]),
                            AccAmtAgeSTB = Sm.DrDec(drDtl3, cDtl3[18]),
                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(ldtl3);
            #endregion

            #region old
            //#region Header

            //var cm_A = new MySqlCommand();
            //var SQL_A = new StringBuilder();

            //SQL_A.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            //SQL_A.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',  ");
            //SQL_A.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            //SQL_A.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',  ");
            //SQL_A.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax',  ");
            //SQL_A.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%M %d, %Y') As DocDt,  ");
            //SQL_A.AppendLine("DATE_FORMAT(A.SFcDt,'%M %d, %Y') As SfcDt, A.Remark, A.Lot  ");
            //SQL_A.AppendLine("From tblWasteSpinninghdr A  ");
            //SQL_A.AppendLine("Where A.DocNo=@DocNo_A  ");


            //using (var cn_A = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cn_A.Open();
            //    cm_A.Connection = cn_A;
            //    cm_A.CommandText = SQL_A.ToString();
            //    Sm.CmParam<String>(ref cm_A, "@DocNo_A", TxtWasteDocNo.Text);
                
            //    var dr_A = cm_A.ExecuteReader();
            //    var c_A = Sm.GetOrdinal(dr_A, new string[] 
            //        {
            //             //0
            //             "CompanyLogo",

            //             //1-5
            //             "CompanyName",
            //             "CompanyAddress",
            //             "CompanyAddressCity",
            //             "CompanyPhone",
            //             "CompanyFax",
            //             //6-10
            //             "DocNo",
            //             "DocDt",
            //             "SfcDt",
            //             "Remark",
            //             "Lot"
            //        });
            //    if (dr_A.HasRows)
            //    {
            //        while (dr_A.Read())
            //        {
            //            ld_A.Add(new WSPHdr_A()
            //            {
            //                CompanyLogo_A = Sm.DrStr(dr_A, c_A[0]),
            //                CompanyName_A = Sm.DrStr(dr_A, c_A[1]),
            //                CompanyAddress_A = Sm.DrStr(dr_A, c_A[2]),
            //                CompanyAddressCity_A = Sm.DrStr(dr_A, c_A[3]),
            //                CompanyPhone_A = Sm.DrStr(dr_A, c_A[4]),
            //                CompanyFax_A = Sm.DrStr(dr_A, c_A[5]),
            //                DocNo_A = Sm.DrStr(dr_A, c_A[6]),
            //                DocDt_A = Sm.DrStr(dr_A, c_A[7]),
            //                SFCDt_A = Sm.DrStr(dr_A, c_A[8]),
            //                Remark_A = Sm.DrStr(dr_A, c_A[9]),
            //                Lot_A = Sm.DrStr(dr_A, c_A[10]),
            //                PrintBy_A = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
            //            });
            //        }
            //    }
            //    dr_A.Close();
            //}
            //myLists.Add(ld_A);

            //#endregion

            //#region Detail data 1
            //var cmDtl_B = new MySqlCommand();

            //var SQLDtl_B = new StringBuilder();
            //using (var cnDtl_B = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cnDtl_B.Open();
            //    cmDtl_B.Connection = cnDtl_B;

            //    SQLDtl_B.AppendLine("Select A.ItgrpCode, A.Itgrpname, A.ItGrpShortName, ifnull(B.AmtShift1, 0) AmtShift1,  ");
            //    SQLDtl_B.AppendLine("ifnull(B.AmtShift2, 0) AmtShift2, ifnull(B.AmtShift3, 0) AmtShift3,  ");
            //    SQLDtl_B.AppendLine("ifnull(B.Total, 0) Total, ifnull(B.TotalSFC, 0) TotalSFC, ifnull(B.ProsenAgeWaste, 0) ProsenAgeWaste,  ");
            //    SQLDtl_B.AppendLine("ifnull(B.AmtRecv, 0) AmtRecv, ifnull(B.ProsenAgeSTB, 0) ProsenAgeSTB, ifnull(C.AccShift1, 0) AccShift1,  ");
            //    SQLDtl_B.AppendLine("ifnull(C.AccShift2, 0) AccShift2,  ifnull(C.AccShift3, 0) AccShift3, ifnull(C.AccTotal, 0) AccTotal,  ");
            //    SQLDtl_B.AppendLine("ifnull(C.AccTotalSFC, 0) AccTotalSFC, ifnull(C.AccProsenAgeWaste, 0) AccProsenAgeWaste,  ");
            //    SQLDtl_B.AppendLine("ifnull(C.AccAmtRecv, 0) AccAmtRecv, ifnull(C.AccProsenAgeSTB, 0) AccProsenAgeSTB  ");
            //    SQLDtl_B.AppendLine("from tblitemGroup A ");
            //    SQLDtl_B.AppendLine("Left Join  ");
            //    SQLDtl_B.AppendLine("( ");
            //    SQLDtl_B.AppendLine("    Select A.DNo, A.ItGrpCode, A.AmtShift1, A.AmtShift2, A.AmtShift3,  ");
            //    SQLDtl_B.AppendLine("    A.Total, A.TotalSFC, A.ProsenAgeWaste, A.AmtRecv, A.ProsenAgeSTB  ");
            //    SQLDtl_B.AppendLine("    From tblwastespinningdtl A  ");
            //    SQLDtl_B.AppendLine("    Where A.DocNo=@DocNo_B ");
            //    SQLDtl_B.AppendLine(")B On A.ItGrpCode = B.ItgrpCode ");
            //    SQLDtl_B.AppendLine("Left Join  ");
            //    SQLDtl_B.AppendLine("( ");
            //    SQLDtl_B.AppendLine("    Select B.ItGrpCode, SUM(B.AmtShift1) AccShift1, SUM(B.AmtShift2) AccShift2, ");
            //    SQLDtl_B.AppendLine("    SUM(B.AmtShift3) AccShift3, SUM(B.Total) AccTotal, SUM(B.TotalSFC) AccTotalSFC,  ");
            //    SQLDtl_B.AppendLine("    SUm(B.ProsenAgeWaste) AccProsenAgeWaste, SUM(B.AmtRecv) AccAmtRecv, SUM(B.ProsenAgeSTB) AccProsenAgeSTB  ");
            //    SQLDtl_B.AppendLine("    From tblwasteSpinninghdr A");
            //    SQLDtl_B.AppendLine("    inner Join tblwastespinningdtl B on A.DocNo = B.DocNo ");
            //    SQLDtl_B.AppendLine("    Where A.SfcDt Between '" + string.Concat(Sm.Left(Sm.GetDte(DteSfcDt), 6), "01") + "' And '" + Sm.GetDte(DteSfcDt).Substring(0, 8) + "'  And A.Cancelind = 'N' ");
            //    SQLDtl_B.AppendLine("    Group By B.ItGrpCode ");
            //    SQLDtl_B.AppendLine(")C On A.ItGrpCode = C.ItgrpCode ");
            //    SQLDtl_B.AppendLine("Where A.ActInd = 'Y' And A.ItGrpCode between '03' And '20' ");


            //    cmDtl_B.CommandText = SQLDtl_B.ToString();
            //    Sm.CmParam<String>(ref cmDtl_B, "@DocNo_B", TxtWasteDocNo.Text);
            //    var drDtl_B = cmDtl_B.ExecuteReader();
            //    var cDtl_B = Sm.GetOrdinal(drDtl_B, new string[] 
            //            {
            //             //0
            //             "ItGrpCode" ,
                         
            //             //1-5
            //             "ItGrpName",
            //             "ItGrpShortName" ,
            //             "AmtShift1",
            //             "AmtShift2" ,
            //             "AmtShift3" ,
                         
            //             //6-10
            //             "Total" ,
            //             "TotalSFC",
            //             "ProsenAgeWaste",
            //             "AmtRecv" ,
            //             "ProsenAgeSTB", 
                         
            //             //11-15
            //             "AccShift1",
            //             "AccShift2" ,
            //             "AccShift3" ,
            //             "AccTotal" ,
            //             "AccTotalSFC",
                         
            //             //16-18
            //             "AccProsenAgeWaste",
            //             "AccAmtRecv" ,
            //             "AccProsenAgeSTB" 
            //            });
            //    if (drDtl_B.HasRows)
            //    {
            //        int nomor = 0;
            //        while (drDtl_B.Read())
            //        {
            //            nomor = nomor + 1;
            //            ldtl_B.Add(new WSPDtl_B()
            //            {
            //                no_B = nomor,
            //                ItGrpCode_B = Sm.DrStr(drDtl_B, cDtl_B[0]),
            //                ItGrpName_B = Sm.DrStr(drDtl_B, cDtl_B[1]),
            //                ItGrpShortName_B = Sm.DrStr(drDtl_B, cDtl_B[2]),
            //                AmtShift1_B = Sm.DrDec(drDtl_B, cDtl_B[3]),
            //                AmtShift2_B = Sm.DrDec(drDtl_B, cDtl_B[4]),
            //                AmtShift3_B = Sm.DrDec(drDtl_B, cDtl_B[5]),
            //                AmtTotal_B = Sm.DrDec(drDtl_B, cDtl_B[6]),
            //                TotalSFC_B = Sm.DrDec(drDtl_B, cDtl_B[7]),
            //                AmtAgeWaste_B = Sm.DrDec(drDtl_B, cDtl_B[8]),
            //                AmtRecv_B = Sm.DrDec(drDtl_B, cDtl_B[9]),
            //                AmtAgeSTB_B = Sm.DrDec(drDtl_B, cDtl_B[10]),
            //                AccAmtShift1_B = Sm.DrDec(drDtl_B, cDtl_B[11]),
            //                AccAmtShift2_B = Sm.DrDec(drDtl_B, cDtl_B[12]),
            //                AccAmtShift3_B = Sm.DrDec(drDtl_B, cDtl_B[13]),
            //                AccAmtTotal_B = Sm.DrDec(drDtl_B, cDtl_B[14]),
            //                AccTotalSFC_B = Sm.DrDec(drDtl_B, cDtl_B[15]),
            //                AccAmtAgeWaste_B = Sm.DrDec(drDtl_B, cDtl_B[16]),
            //                AccAmtRecv_B = Sm.DrDec(drDtl_B, cDtl_B[17]),
            //                AccAmtAgeSTB_B = Sm.DrDec(drDtl_B, cDtl_B[18]),
            //            });
            //        }
            //    }
            //    drDtl_B.Close();
            //}
            //myLists.Add(ldtl_B);
            //#endregion

            //#region Detail data 2
            //var cmDtl2_C = new MySqlCommand();

            //var SQLDtl2_C = new StringBuilder();
            //using (var cnDtl2_C = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cnDtl2_C.Open();
            //    cmDtl2_C.Connection = cnDtl2_C;

            //    SQLDtl2_C.AppendLine("Select A.ItgrpCode, A.Itgrpname, A.ItGrpShortName, ifnull(B.AmtShift1, 0) AmtShift1,  ");
            //    SQLDtl2_C.AppendLine("ifnull(B.AmtShift2, 0) AmtShift2, ifnull(B.AmtShift3, 0) AmtShift3,  ");
            //    SQLDtl2_C.AppendLine("ifnull(B.Total, 0) Total, ifnull(B.TotalSFC, 0) TotalSFC, ifnull(B.ProsenAgeWaste, 0) ProsenAgeWaste,  ");
            //    SQLDtl2_C.AppendLine("ifnull(B.AmtRecv, 0) AmtRecv, ifnull(B.ProsenAgeSTB, 0) ProsenAgeSTB, ifnull(C.AccShift1, 0) AccShift1,  ");
            //    SQLDtl2_C.AppendLine("ifnull(C.AccShift2, 0) AccShift2,  ifnull(C.AccShift3, 0) AccShift3, ifnull(C.AccTotal, 0) AccTotal,  ");
            //    SQLDtl2_C.AppendLine("ifnull(C.AccTotalSFC, 0) AccTotalSFC, ifnull(C.AccProsenAgeWaste, 0) AccProsenAgeWaste,  ");
            //    SQLDtl2_C.AppendLine("ifnull(C.AccAmtRecv, 0) AccAmtRecv, ifnull(C.AccProsenAgeSTB, 0) AccProsenAgeSTB  ");
            //    SQLDtl2_C.AppendLine("from tblitemGroup A ");
            //    SQLDtl2_C.AppendLine("Left Join  ");
            //    SQLDtl2_C.AppendLine("( ");
            //    SQLDtl2_C.AppendLine("    Select A.DNo, A.ItGrpCode, A.AmtShift1, A.AmtShift2, A.AmtShift3,  ");
            //    SQLDtl2_C.AppendLine("    A.Total, A.TotalSFC, A.ProsenAgeWaste, A.AmtRecv, A.ProsenAgeSTB  ");
            //    SQLDtl2_C.AppendLine("    From tblwastespinningdtl A  ");
            //    SQLDtl2_C.AppendLine("    Where A.DocNo=@DocNo_C ");
            //    SQLDtl2_C.AppendLine(")B On A.ItGrpCode = B.ItgrpCode ");
            //    SQLDtl2_C.AppendLine("Left Join  ");
            //    SQLDtl2_C.AppendLine("( ");
            //    SQLDtl2_C.AppendLine("    Select B.ItGrpCode, SUM(B.AmtShift1) AccShift1, SUM(B.AmtShift2) AccShift2, ");
            //    SQLDtl2_C.AppendLine("    SUM(B.AmtShift3) AccShift3, SUM(B.Total) AccTotal, SUM(B.TotalSFC) AccTotalSFC,  ");
            //    SQLDtl2_C.AppendLine("    SUm(B.ProsenAgeWaste) AccProsenAgeWaste, SUM(B.AmtRecv) AccAmtRecv, SUM(B.ProsenAgeSTB) AccProsenAgeSTB  ");
            //    SQLDtl2_C.AppendLine("    From tblwasteSpinninghdr A");
            //    SQLDtl2_C.AppendLine("    inner Join tblwastespinningdtl B on A.DocNo = B.DocNo ");
            //    SQLDtl2_C.AppendLine("    Where A.SfcDt Between '" + string.Concat(Sm.Left(Sm.GetDte(DteSfcDt), 6), "01") + "' And '" + Sm.GetDte(DteSfcDt).Substring(0, 8) + "'  And A.Cancelind = 'N' ");
            //    SQLDtl2_C.AppendLine("    Group By B.ItGrpCode ");
            //    SQLDtl2_C.AppendLine(")C On A.ItGrpCode = C.ItgrpCode ");
            //    SQLDtl2_C.AppendLine("Where A.ActInd = 'Y' And A.ItGrpCode between '21' And '25' ");

            //    cmDtl2_C.CommandText = SQLDtl2_C.ToString();
            //    Sm.CmParam<String>(ref cmDtl2_C, "@DocNo_C", TxtWasteDocNo.Text);
            //    var drDtl2_C = cmDtl2_C.ExecuteReader();
            //    var cDtl2_C = Sm.GetOrdinal(drDtl2_C, new string[] 
            //            {
            //             //0
            //             "ItGrpCode" ,
                         
            //             //1-5
            //             "ItGrpName",
            //             "ItGrpShortName" ,
            //             "AmtShift1",
            //             "AmtShift2" ,
            //             "AmtShift3" ,
                         
            //             //6-10
            //             "Total" ,
            //             "TotalSFC",
            //             "ProsenAgeWaste",
            //             "AmtRecv" ,
            //             "ProsenAgeSTB", 
                         
            //             //11-15
            //             "AccShift1",
            //             "AccShift2" ,
            //             "AccShift3" ,
            //             "AccTotal" ,
            //             "AccTotalSFC",
                         
            //             //16-18
            //             "AccProsenAgeWaste",
            //             "AccAmtRecv" ,
            //             "AccProsenAgeSTB" 
            //            });
            //    if (drDtl2_C.HasRows)
            //    {
            //        int nomor = 0;
            //        while (drDtl2_C.Read())
            //        {
            //            nomor = nomor + 1;
            //            ldtl2_C.Add(new WSPDtl2_C()
            //            {
            //                no_C = nomor,
            //                ItGrpCode_C = Sm.DrStr(drDtl2_C, cDtl2_C[0]),
            //                ItGrpName_C = Sm.DrStr(drDtl2_C, cDtl2_C[1]),
            //                ItGrpShortName_C = Sm.DrStr(drDtl2_C, cDtl2_C[2]),
            //                AmtShift1_C = Sm.DrDec(drDtl2_C, cDtl2_C[3]),
            //                AmtShift2_C = Sm.DrDec(drDtl2_C, cDtl2_C[4]),
            //                AmtShift3_C = Sm.DrDec(drDtl2_C, cDtl2_C[5]),
            //                AmtTotal_C = Sm.DrDec(drDtl2_C, cDtl2_C[6]),
            //                TotalSFC_C = Sm.DrDec(drDtl2_C, cDtl2_C[7]),
            //                AmtAgeWaste_C = Sm.DrDec(drDtl2_C, cDtl2_C[8]),
            //                AmtRecv_C = Sm.DrDec(drDtl2_C, cDtl2_C[9]),
            //                AmtAgeSTB_C = Sm.DrDec(drDtl2_C, cDtl2_C[10]),

            //                AccAmtShift1_C = Sm.DrDec(drDtl2_C, cDtl2_C[11]),
            //                AccAmtShift2_C = Sm.DrDec(drDtl2_C, cDtl2_C[12]),
            //                AccAmtShift3_C = Sm.DrDec(drDtl2_C, cDtl2_C[13]),
            //                AccAmtTotal_C = Sm.DrDec(drDtl2_C, cDtl2_C[14]),
            //                AccTotalSFC_C = Sm.DrDec(drDtl2_C, cDtl2_C[15]),
            //                AccAmtAgeWaste_C = Sm.DrDec(drDtl2_C, cDtl2_C[16]),
            //                AccAmtRecv_C = Sm.DrDec(drDtl2_C, cDtl2_C[17]),
            //                AccAmtAgeSTB_C = Sm.DrDec(drDtl2_C, cDtl2_C[18]),
            //            });
            //        }
            //    }
            //    drDtl2_C.Close();
            //}
            //myLists.Add(ldtl2_C);
            //#endregion

            //#region Detail data 3
            //var cmDtl3_D = new MySqlCommand();

            //var SQLDtl3_D = new StringBuilder();
            //using (var cnDtl3_D = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cnDtl3_D.Open();
            //    cmDtl3_D.Connection = cnDtl3_D;

            //    SQLDtl3_D.AppendLine("Select A.ItgrpCode, A.Itgrpname, A.ItGrpShortName, ifnull(B.AmtShift1, 0) AmtShift1,  ");
            //    SQLDtl3_D.AppendLine("ifnull(B.AmtShift2, 0) AmtShift2, ifnull(B.AmtShift3, 0) AmtShift3,  ");
            //    SQLDtl3_D.AppendLine("ifnull(B.Total, 0) Total, ifnull(B.TotalSFC, 0) TotalSFC, ifnull(B.ProsenAgeWaste, 0) ProsenAgeWaste,  ");
            //    SQLDtl3_D.AppendLine("ifnull(B.AmtRecv, 0) AmtRecv, ifnull(B.ProsenAgeSTB, 0) ProsenAgeSTB, ifnull(C.AccShift1, 0) AccShift1,  ");
            //    SQLDtl3_D.AppendLine("ifnull(C.AccShift2, 0) AccShift2,  ifnull(C.AccShift3, 0) AccShift3, ifnull(C.AccTotal, 0) AccTotal,  ");
            //    SQLDtl3_D.AppendLine("ifnull(C.AccTotalSFC, 0) AccTotalSFC, ifnull(C.AccProsenAgeWaste, 0) AccProsenAgeWaste,  ");
            //    SQLDtl3_D.AppendLine("ifnull(C.AccAmtRecv, 0) AccAmtRecv, ifnull(C.AccProsenAgeSTB, 0) AccProsenAgeSTB  ");
            //    SQLDtl3_D.AppendLine("from tblitemGroup A ");
            //    SQLDtl3_D.AppendLine("Left Join  ");
            //    SQLDtl3_D.AppendLine("( ");
            //    SQLDtl3_D.AppendLine("    Select A.DNo, A.ItGrpCode, A.AmtShift1, A.AmtShift2, A.AmtShift3,  ");
            //    SQLDtl3_D.AppendLine("    A.Total, A.TotalSFC, A.ProsenAgeWaste, A.AmtRecv, A.ProsenAgeSTB  ");
            //    SQLDtl3_D.AppendLine("    From tblwastespinningdtl A  ");
            //    SQLDtl3_D.AppendLine("    Where A.DocNo=@DocNo_D ");
            //    SQLDtl3_D.AppendLine(")B On A.ItGrpCode = B.ItgrpCode ");
            //    SQLDtl3_D.AppendLine("Left Join  ");
            //    SQLDtl3_D.AppendLine("( ");
            //    SQLDtl3_D.AppendLine("    Select B.ItGrpCode, SUM(B.AmtShift1) AccShift1, SUM(B.AmtShift2) AccShift2, ");
            //    SQLDtl3_D.AppendLine("    SUM(B.AmtShift3) AccShift3, SUM(B.Total) AccTotal, SUM(B.TotalSFC) AccTotalSFC,  ");
            //    SQLDtl3_D.AppendLine("    SUm(B.ProsenAgeWaste) AccProsenAgeWaste, SUM(B.AmtRecv) AccAmtRecv, SUM(B.ProsenAgeSTB) AccProsenAgeSTB  ");
            //    SQLDtl3_D.AppendLine("    From tblwasteSpinninghdr A");
            //    SQLDtl3_D.AppendLine("    inner Join tblwastespinningdtl B on A.DocNo = B.DocNo ");
            //    SQLDtl3_D.AppendLine("    Where A.SfcDt Between '" + string.Concat(Sm.Left(Sm.GetDte(DteSfcDt), 6), "01") + "' And '" + Sm.GetDte(DteSfcDt).Substring(0, 8) + "'  And A.Cancelind = 'N' ");
            //    SQLDtl3_D.AppendLine("    Group By B.ItGrpCode ");
            //    SQLDtl3_D.AppendLine(")C On A.ItGrpCode = C.ItgrpCode ");
            //    SQLDtl3_D.AppendLine("Where A.ActInd = 'Y' And A.ItGrpCode between '26' And '50' ");

            //    cmDtl3_D.CommandText = SQLDtl3_D.ToString();
            //    Sm.CmParam<String>(ref cmDtl3_D, "@DocNo_D", TxtWasteDocNo.Text);
            //    var drDtl3_D = cmDtl3_D.ExecuteReader();
            //    var cDtl3_D = Sm.GetOrdinal(drDtl3_D, new string[] 
            //            {
            //             //0
            //             "ItGrpCode" ,
                         
            //             //1-5
            //             "ItGrpName",
            //             "ItGrpShortName" ,
            //             "AmtShift1",
            //             "AmtShift2" ,
            //             "AmtShift3" ,
                         
            //             //6-10
            //             "Total" ,
            //             "TotalSFC",
            //             "ProsenAgeWaste",
            //             "AmtRecv" ,
            //             "ProsenAgeSTB", 
                         
            //             //11-15
            //             "AccShift1",
            //             "AccShift2" ,
            //             "AccShift3" ,
            //             "AccTotal" ,
            //             "AccTotalSFC",
                         
            //             //16-18
            //             "AccProsenAgeWaste",
            //             "AccAmtRecv" ,
            //             "AccProsenAgeSTB" 
            //            });
            //    if (drDtl3_D.HasRows)
            //    {
            //        int nomor = 0;
            //        while (drDtl3_D.Read())
            //        {
            //            nomor = nomor + 1;
            //            ldtl3_D.Add(new WSPDtl3_D()
            //            {
            //                no_D = nomor,
            //                ItGrpCode_D = Sm.DrStr(drDtl3_D, cDtl3_D[0]),
            //                ItGrpName_D = Sm.DrStr(drDtl3_D, cDtl3_D[1]),
            //                ItGrpShortName_D = Sm.DrStr(drDtl3_D, cDtl3_D[2]),
            //                AmtShift1_D = Sm.DrDec(drDtl3_D, cDtl3_D[3]),
            //                AmtShift2_D = Sm.DrDec(drDtl3_D, cDtl3_D[4]),
            //                AmtShift3_D = Sm.DrDec(drDtl3_D, cDtl3_D[5]),
            //                AmtTotal_D = Sm.DrDec(drDtl3_D, cDtl3_D[6]),
            //                TotalSFC_D = Sm.DrDec(drDtl3_D, cDtl3_D[7]),
            //                AmtAgeWaste_D = Sm.DrDec(drDtl3_D, cDtl3_D[8]),
            //                AmtRecv_D = Sm.DrDec(drDtl3_D, cDtl3_D[9]),
            //                AmtAgeSTB_D = Sm.DrDec(drDtl3_D, cDtl3_D[10]),

            //                AccAmtShift1_D = Sm.DrDec(drDtl3_D, cDtl3_D[11]),
            //                AccAmtShift2_D = Sm.DrDec(drDtl3_D, cDtl3_D[12]),
            //                AccAmtShift3_D = Sm.DrDec(drDtl3_D, cDtl3_D[13]),
            //                AccAmtTotal_D = Sm.DrDec(drDtl3_D, cDtl3_D[14]),
            //                AccTotalSFC_D = Sm.DrDec(drDtl3_D, cDtl3_D[15]),
            //                AccAmtAgeWaste_D = Sm.DrDec(drDtl3_D, cDtl3_D[16]),
            //                AccAmtRecv_D = Sm.DrDec(drDtl3_D, cDtl3_D[17]),
            //                AccAmtAgeSTB_D = Sm.DrDec(drDtl3_D, cDtl3_D[18]),
            //            });
            //        }
            //    }
            //    drDtl3_D.Close();
            //}
            //myLists.Add(ldtl3_D);
            //#endregion

            #endregion

            Sm.PrintReport("WasteSpinning", myLists, TableName, false);
        }

        #endregion

        #endregion

        #region Event

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteSfcDt).Length > 0)
            {
                ShowWorkcenter(Sm.GetDte(DteSfcDt));
                ProcessSFC(Sm.GetDte(DteSfcDt));
            }
        }

        private void BtnCalculate_Click(object sender, EventArgs e)
        {
            //ComputeAgeWaste();
            //ComputeAgeSTB();
            Computasi();
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void LueLotCode_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteSfcDt).Length > 0)
            {
                Sm.RefreshLookUpEdit(LueLotCode, new Sm.RefreshLue1(SetLueLotCode));
            }
        }

        private void DteSfcDt_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteSfcDt).Length > 0)
            {
                SetLueLotCode(ref LueLotCode);
            }
        }

        private void BtnWasteDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmWasteSpinningDlg(this));
        }


        #endregion

        #region Class

        #region Class printOut
        private class WSPHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string SFCDt { get; set; }
            public string Lot { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }
        }

        private class WSPDtl
        {
            public decimal no { set; get; }
            public string ItGrpCode { set; get; }
            public string ItGrpName { get; set; }
            public string ItGrpShortName { get; set; }
            public decimal AmtShift1 { get; set; }
            public decimal AmtShift2 { get; set; }
            public decimal AmtShift3 { get; set; }
            public decimal AmtTotal { get; set; }
            public decimal TotalSFC { get; set; }
            public decimal AmtAgeWaste { get; set; }
            public decimal AmtRecv { get; set; }
            public decimal AmtAgeSTB { get; set; }
            public decimal AccAmtShift1 { get; set; }
            public decimal AccAmtShift2 { get; set; }
            public decimal AccAmtShift3 { get; set; }
            public decimal AccAmtTotal { get; set; }
            public decimal AccTotalSFC { get; set; }
            public decimal AccAmtAgeWaste { get; set; }
            public decimal AccAmtRecv { get; set; }
            public decimal AccAmtAgeSTB { get; set; }
        }

        private class WSPDtl2
        {
            public decimal no { set; get; }
            public string ItGrpCode { set; get; }
            public string ItGrpName { get; set; }
            public string ItGrpShortName { get; set; }
            public decimal AmtShift1 { get; set; }
            public decimal AmtShift2 { get; set; }
            public decimal AmtShift3 { get; set; }
            public decimal AmtTotal { get; set; }
            public decimal TotalSFC { get; set; }
            public decimal AmtAgeWaste { get; set; }
            public decimal AmtRecv { get; set; }
            public decimal AmtAgeSTB { get; set; }
            public decimal AccAmtShift1 { get; set; }
            public decimal AccAmtShift2 { get; set; }
            public decimal AccAmtShift3 { get; set; }
            public decimal AccAmtTotal { get; set; }
            public decimal AccTotalSFC { get; set; }
            public decimal AccAmtAgeWaste { get; set; }
            public decimal AccAmtRecv { get; set; }
            public decimal AccAmtAgeSTB { get; set; }
        }

        private class WSPDtl3
        {
            public decimal no { set; get; }
            public string ItGrpCode { set; get; }
            public string ItGrpName { get; set; }
            public string ItGrpShortName { get; set; }
            public decimal AmtShift1 { get; set; }
            public decimal AmtShift2 { get; set; }
            public decimal AmtShift3 { get; set; }
            public decimal AmtTotal { get; set; }
            public decimal TotalSFC { get; set; }
            public decimal AmtAgeWaste { get; set; }
            public decimal AmtRecv { get; set; }
            public decimal AmtAgeSTB { get; set; }
            public decimal AccAmtShift1 { get; set; }
            public decimal AccAmtShift2 { get; set; }
            public decimal AccAmtShift3 { get; set; }
            public decimal AccAmtTotal { get; set; }
            public decimal AccTotalSFC { get; set; }
            public decimal AccAmtAgeWaste { get; set; }
            public decimal AccAmtRecv { get; set; }
            public decimal AccAmtAgeSTB { get; set; }
        }

        private class WSPHdr_A
        {
            public string CompanyLogo_A { set; get; }
            public string CompanyName_A { get; set; }
            public string CompanyAddressCity_A { get; set; }
            public string CompanyAddress_A { get; set; }
            public string CompanyPhone_A { get; set; }
            public string CompanyFax_A { get; set; }
            public string DocNo_A { get; set; }
            public string DocDt_A { get; set; }
            public string SFCDt_A { get; set; }
            public string Remark_A { get; set; }
            public string Lot_A { get; set; }
            public string PrintBy_A { get; set; }
        }

        private class WSPDtl_B
        {
            public decimal no_B { set; get; }
            public string ItGrpCode_B { set; get; }
            public string ItGrpName_B { get; set; }
            public string ItGrpShortName_B { get; set; }
            public decimal AmtShift1_B { get; set; }
            public decimal AmtShift2_B { get; set; }
            public decimal AmtShift3_B { get; set; }
            public decimal AmtTotal_B { get; set; }
            public decimal TotalSFC_B { get; set; }
            public decimal AmtAgeWaste_B { get; set; }
            public decimal AmtRecv_B { get; set; }
            public decimal AmtAgeSTB_B { get; set; }
            public decimal AccAmtShift1_B { get; set; }
            public decimal AccAmtShift2_B { get; set; }
            public decimal AccAmtShift3_B { get; set; }
            public decimal AccAmtTotal_B { get; set; }
            public decimal AccTotalSFC_B { get; set; }
            public decimal AccAmtAgeWaste_B { get; set; }
            public decimal AccAmtRecv_B { get; set; }
            public decimal AccAmtAgeSTB_B { get; set; }
        }

        private class WSPDtl2_C
        {
            public decimal no_C { set; get; }
            public string ItGrpCode_C { set; get; }
            public string ItGrpName_C { get; set; }
            public string ItGrpShortName_C { get; set; }
            public decimal AmtShift1_C { get; set; }
            public decimal AmtShift2_C { get; set; }
            public decimal AmtShift3_C { get; set; }
            public decimal AmtTotal_C { get; set; }
            public decimal TotalSFC_C { get; set; }
            public decimal AmtAgeWaste_C { get; set; }
            public decimal AmtRecv_C { get; set; }
            public decimal AmtAgeSTB_C { get; set; }
            public decimal AccAmtShift1_C { get; set; }
            public decimal AccAmtShift2_C { get; set; }
            public decimal AccAmtShift3_C { get; set; }
            public decimal AccAmtTotal_C { get; set; }
            public decimal AccTotalSFC_C { get; set; }
            public decimal AccAmtAgeWaste_C { get; set; }
            public decimal AccAmtRecv_C { get; set; }
            public decimal AccAmtAgeSTB_C { get; set; }
        }

        private class WSPDtl3_D
        {
            public decimal no_D { set; get; }
            public string ItGrpCode_D { set; get; }
            public string ItGrpName_D { get; set; }
            public string ItGrpShortName_D { get; set; }
            public decimal AmtShift1_D { get; set; }
            public decimal AmtShift2_D { get; set; }
            public decimal AmtShift3_D { get; set; }
            public decimal AmtTotal_D { get; set; }
            public decimal TotalSFC_D { get; set; }
            public decimal AmtAgeWaste_D { get; set; }
            public decimal AmtRecv_D { get; set; }
            public decimal AmtAgeSTB_D { get; set; }
            public decimal AccAmtShift1_D { get; set; }
            public decimal AccAmtShift2_D { get; set; }
            public decimal AccAmtShift3_D { get; set; }
            public decimal AccAmtTotal_D { get; set; }
            public decimal AccTotalSFC_D { get; set; }
            public decimal AccAmtAgeWaste_D { get; set; }
            public decimal AccAmtRecv_D { get; set; }
            public decimal AccAmtAgeSTB_D { get; set; }
        }
     
        #endregion

        private class FSW
        {
            public string FSWCode { get; set; }
            public string ItGrpCode { get; set; }
            public string FSWName { get; set; }
            public string FSWType { get; set; }
            public string Query { get; set; }
            public string Param1 { get; set; }
            public string Param2 { get; set; }
            public string Param3 { get; set; }
            public string Param4 { get; set; }
            public string Param5 { get; set; }
            public string Param6 { get; set; }
            public string Param7 { get; set; }
        }

        private class TotalCode
        {
            public decimal TotalFSWCode001 { get; set; }
            public decimal TotalFSWCode002 { get; set; }
            public decimal TotalFSWCode003 { get; set; }
            public decimal TotalFSWCode004 { get; set; }
            public decimal TotalFSWCode005 { get; set; }
            public decimal TotalFSWCode006 { get; set; }
            public decimal TotalFSWCode007 { get; set; }
            public decimal TotalFSWCode008 { get; set; }
            public decimal TotalFSWCode009 { get; set; }
            public decimal TotalFSWCode010 { get; set; }
            public decimal TotalFSWCode011 { get; set; }
            public decimal TotalFSWCode012 { get; set; }
            public decimal TotalFSWCode013 { get; set; }
            public decimal TotalFSWCode014 { get; set; }
            public decimal TotalFSWCode015 { get; set; }
            public decimal TotalFSWCode016 { get; set; }

            public string FSWCode001 { get; set; }
            public string FSWCode002 { get; set; }
            public string FSWCode003 { get; set; }
            public string FSWCode004 { get; set; }
            public string FSWCode005 { get; set; }
            public string FSWCode006 { get; set; }
            public string FSWCode007 { get; set; }
            public string FSWCode008 { get; set; }
            public string FSWCode009 { get; set; }
            public string FSWCode010 { get; set; }
            public string FSWCode011 { get; set; }
            public string FSWCode012 { get; set; }
            public string FSWCode013 { get; set; }
            public string FSWCode014 { get; set; }
            public string FSWCode015 { get; set; }
            public string FSWCode016 { get; set; }
        }

        #endregion
     
    }

 
   
}
