﻿#region Update
/*
    15/12/2022 [TYO/MNET] New Apps
    02/01/2022 [TYO/MNET] Menambah print out
    09/01/2022 [TYO/MNET] Menambah validasi ketika click loop PODocNo
    19/01/2023 [TYO/MNET] bug : set parprint() masih true
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRatingForVendor : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mListofRatingforVendor = string.Empty;

        internal bool
            mIsRecvVdUsePORevision = false;
        //internal FrmFind FrmFind;

        internal bool mIsRatingforVendorUseRemarkDetail = false;

        internal FrmRatingForVendorFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmRatingForVendor(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Rating For Vendor";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                BtnPrint.Visible = true;
                SetGrd();
                Sl.SetLueVdCode(ref LueVendor);
                Sl.SetLueOption(ref LueNilai, "RatingforVendorValue");
                LueNilai.Visible = false;
                base.FrmLoad(sender, e);
            }

            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method
        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, TxtPODocNo, TxtCancelationReason, TxtPIC, ChkCancelInd, MeeRemark, DteDocDt, LueVendor, TxtAverage, TxtTotal
                    }, true);
                    Grd1.ReadOnly = true;
                    BtnPODocNo.Enabled = false;
                    break;

                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                       DteDocDt, LueVendor
                    }, false);
                    Grd1.ReadOnly = false;
                    BtnPODocNo.Enabled = true;
                    TxtPIC.Text = Sm.GetValue("Select UserName From TblUser Where UserCode = '"+Gv.CurrentUserCode+"'");
                    GetKriteriaPenilaian();
                    break;

                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtCancelationReason, ChkCancelInd
                    }, false);

                    if(ChkCancelInd.Checked == true)
                    {
                        Sm.StdMsg(mMsgType.Warning, "This document already Canceled.");
                        string DocNo = TxtDocNo.Text;
                        ShowData(DocNo);
                    }
                    Grd1.ReadOnly = true;
                    BtnPODocNo.Enabled = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, LueVendor, TxtPODocNo, TxtPIC, MeeRemark, TxtCancelationReason, TxtTotal, TxtAverage
            });
            Sm.ClearGrd(Grd1, true);
            ChkCancelInd.Checked = false;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "No",

                    //1-5
                    "Rating Code",
                    "Kriteria Penilaian",
                    "Nilai",
                    "Nilai Code",
                    "Remark"
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    200, 300, 100, 100, 250
                }
            );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4 });
            if(!mIsRatingforVendorUseRemarkDetail)
                Sm.GrdColInvisible(Grd1, new int[] { 5 });

        }

        private void GetParameter()
        {
            mIsRatingforVendorUseRemarkDetail = Sm.GetParameter("IsRatingforVendorUseRemarkDetail") == "Y";
            mListofRatingforVendor = Sm.GetParameter("ListofRatingforVendor");
            mIsRecvVdUsePORevision = Sm.GetParameter("IsRecvVdUsePORevision") == "Y";
        }

        internal void GetKriteriaPenilaian()
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Parameter", mListofRatingforVendor);
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT @no:=@no+1 AS nomor, RatingCode, Description  ");
            SQL.AppendLine("FROM tblrating ");
            SQL.AppendLine("JOIN (SELECT @no:=0) r ");
            SQL.AppendLine("WHERE FIND_IN_SET( ");
            SQL.AppendLine("RatingCode,@Parameter); ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                {
                    //0-2
                    "nomor","RatingCode", "Description"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd1, 0, 1);


        }

        #endregion

        #region Additional Method

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            string DocNo = TxtDocNo.Text;
            cml.Add(SaveRatingForVendorHdr(DocNo));
            Sm.ExecCommands(cml);
            ShowData(DocNo);

        }

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RatingForVendor", "TblRatingForVendorHdr");
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRatingForVendorHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Grd1.Rows.Count > 1)
                {
                    cml.Add(SaveRatingForVendorDtl(DocNo, Row));
                }
            }
            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private MySqlCommand SaveRatingForVendorHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblRatingForVendorHdr(DocNo, DocDt, CancelInd, CancelReason, VdCode, PODocNo, PICCode, TotalRate, AverageRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', NULL, @VdCode, @PODocNo, @PICCode, @TotalRate, @AverageRate, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("	Update CancelInd = @CancelInd, CancelReason = @CancelReason, LastUpBy = @UserCode, LastUpDt = CurrentDateTime(); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", TxtCancelationReason.Text);
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVendor));
            Sm.CmParam<String>(ref cm, "@PODocNo", TxtPODocNo.Text);
            Sm.CmParam<String>(ref cm, "@PICCode", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@TotalRate", Decimal.Parse(TxtTotal.Text));
            Sm.CmParam<Decimal>(ref cm, "@AverageRate", Decimal.Parse(TxtAverage.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;

        }

        private MySqlCommand SaveRatingForVendorDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblRatingForVendorDtl(DocNo, DNo, CriteriaCode, Rate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @CriteriaCode, @Rate, @Remark, @UserCode, CurrentDateTime()) ");

            cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@CriteriaCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Rate", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@UserCode",Gv.CurrentUserCode);


            return cm;
        }

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowRatingForVendorHdr(DocNo);
                ShowRatingForVendorDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRatingForVendorHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, A.VdCode, A.PODocNo, B.UserName PIC, A.TotalRate, A.AverageRate, A.Remark ");
            SQL.AppendLine("From TblRatingForVendorHdr A ");
            SQL.AppendLine("Inner Join TblUser B On A.PICCode = B.UserCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "DocNo",

                    //1-5
                    "DocDt",
                    "CancelInd",
                    "CancelReason",
                    "VdCode",
                    "PODocNo",

                    //6-9
                    "PIC",
                    "TotalRate",
                    "AverageRate",
                    "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                    TxtCancelationReason.EditValue = Sm.DrStr(dr, c[3]);
                    Sm.SetLue(LueVendor, Sm.DrStr(dr, c[4]));
                    TxtPODocNo.EditValue = Sm.DrStr(dr, c[5]);

                    TxtPIC.EditValue = Sm.DrStr(dr, c[6]);
                    TxtTotal.EditValue = Sm.DrDec(dr, c[7]);
                    TxtAverage.EditValue = Sm.DrDec(dr, c[8]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[9]);

                }, true
                );
        }
        private void ShowRatingForVendorDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Parameter", mListofRatingforVendor);

            SQL.AppendLine("Select @no:=@no+1 AS Nomor, B.Description CriteriaName, A.Rate, A.Remark ");
            SQL.AppendLine("From TblRatingForVendorDtl A ");
            SQL.AppendLine("Inner Join TblRating B On A.CriteriaCode = B.RatingCode ");
            SQL.AppendLine("And Find_In_Set(RatingCode, @Parameter) ");
            SQL.AppendLine("Join (Select @no:=0) r ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[]
                    {
                         //0
                        "Nomor",

                        //1-3
                        "CriteriaName",
                        "Rate",
                        "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    }, false, false, true, false
                );

            Sm.FocusGrd(Grd1, 0, 1);
        }



        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueVendor, "Vendor") ||
                Sm.IsTxtEmpty(TxtPODocNo, "Purchase Order", false) ||
                Sm.IsTxtEmpty(TxtPIC, "PIC", false) ||
                IsGrdInvalid();
                //IsDocNoAlreadyCanceled();
        }

        private bool IsDocNoAlreadyCanceled()
        {

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblRatingForVendorHdr Where DocNo=@DocNo And CancelInd='N'"
            };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already Canceled.");
                return true;
            }
            return false;
        }

        private bool IsGrdInvalid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; ++Row)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Nilai is empty.")) return true;
                }
            }
            return false;
        }

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            var l = new List<RatingForVendorHdr>();
            var lDtl = new List<RatingForVendorDtl>();

            string[] TableName = { "RatingForVendorHdr", "RatingForVendorDtl" };
            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax',  ");
            SQL.AppendLine("A.PODocNo, A.Remark, B.VdName, Date_format(A.DocDt, '%d %M %Y') DocDt, C.UserName, A.TotalRate, A.AverageRate ");
            SQL.AppendLine("From TblRatingForVendorHdr A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
            SQL.AppendLine("Inner Join TblUser C On A.CreateBy = C.UserCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressFull",
                         "CompanyPhone",
                         "CompanyFax",

                         //6-10
                         "PODocNo",
                         "Remark",
                         "VdName",
                         "DocDt",
                         "UserName",

                         //11-15
                         "TotalRate",
                         "AverageRate"
                         
                    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RatingForVendorHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressFull = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),

                            PODocNo = Sm.DrStr(dr, c[6]),
                            RemarkHdr = Sm.DrStr(dr, c[7]),
                            VendorName = Sm.DrStr(dr, c[8]),
                            DocDt = Sm.DrStr(dr, c[9]),
                            CreateBy = Sm.DrStr(dr, c[10]),

                            Total = Sm.DrDec(dr, c[11]),
                            Average = Sm.DrDec(dr, c[12]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select @no:=@no+1 AS Nomor, B.Description CriteriaName, A.Rate ");
                SQLDtl.AppendLine("From TblRatingForVendorDtl A  ");
                SQLDtl.AppendLine("Inner Join TblRating B On A.CriteriaCode = B.RatingCode ");
                SQLDtl.AppendLine("And Find_In_Set(RatingCode, @Parameter) ");
                SQLDtl.AppendLine("Join (Select @no:=0) r ");
                SQLDtl.AppendLine("Where A.DocNo = @DocNo; ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmDtl, "@Parameter", mListofRatingforVendor);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                        {
                         //0
                         "Nomor" ,

                         //1-2
                         "CriteriaName" ,
                         "Rate"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        lDtl.Add(new RatingForVendorDtl()
                        {
                            Nomor = Sm.DrInt(drDtl, cDtl[0]),
                            CriteriaName = Sm.DrStr(drDtl, cDtl[1]),
                            Rate = Sm.DrDec(drDtl, cDtl[2])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(lDtl);
            #endregion

            Sm.PrintReport("RatingForVendor", myLists, TableName, false);
        }
        #endregion

        #region Button Method
        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRatingForVendorFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }


        #endregion

        #region Grd Control Event
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                if (e.ColIndex == 3) Sm.LueRequestEdit(ref Grd1, ref LueNilai, ref fCell, ref fAccept, e);
            }
            //Sm.GrdRequestEdit(Grd1, e.RowIndex);
        }

       
        #endregion

        #region Events
        private void LueNilai_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueNilai.Visible = false;
        }

        private void LueNilai_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueNilai, new Sm.RefreshLue2(Sl.SetLueOption), "RatingforVendorValue");
        }

        private void BtnPODocNo_Click(object sender, EventArgs e)
        {
            if(Sm.GetLue(LueVendor).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Please choose vendor first.");
                LueVendor.Focus();
            }

            else
                Sm.FormShowDialog(new FrmRatingForVendorDlg(this));
        }

        private void LueNilai_Leave(object sender, EventArgs e)
        {
            if (LueNilai.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (Sm.GetLue(LueNilai).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 4].Value =
                    Grd1.Cells[fCell.RowIndex, 3].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueNilai);
                    Grd1.Cells[fCell.RowIndex, 3].Value = LueNilai.GetColumnValue("Col2");
                }
            }
            LueNilai.Visible = false;

            if (Sm.GetLue(LueNilai).Length != 0)
            {
                decimal total = 0;
                decimal average = 0;
                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                {
                    total += Sm.GetGrdDec(Grd1, i, 3);
                }

                TxtTotal.Text = total.ToString();

                average = total / (Grd1.Rows.Count - 1);
                TxtAverage.Text = average.ToString("0.00");
            }
        }

        private void LueNilai_KeyDown(object sender, KeyEventArgs e)
        {
            //Sm.LueKeyDown(Grd1, ref fAccept, e);
        }
        #endregion


        #endregion

        #region Printout class

        private class RatingForVendorHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string PODocNo { get; set; }
            public string RemarkHdr { get; set; }
            public string VendorName { get; set; }
            public string DocDt { get; set; }
            public string CreateBy { get; set; }
            public decimal Total { get; set; }
            public decimal Average { get; set; }
            public string PrintBy { get; set; }
        }

        private class RatingForVendorDtl
        {
            public int Nomor { get; set; }
            public string CriteriaName { get; set; }
            public decimal Rate { get; set; }

        }

        #endregion
    }
}
