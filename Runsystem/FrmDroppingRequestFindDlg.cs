﻿#region
/*
 *      06/02/2023 [RDA/MNET] new dialogue for find
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDroppingRequestFindDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDroppingRequestFind mFrmParent;
        internal string
            Realization = string.Empty,
            DocType = string.Empty;
            

        #endregion

        #region Constructor

        public FrmDroppingRequestFindDlg(FrmDroppingRequestFind FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                //Sm.ButtonVisible("Y", ref BtnPrint, ref BtnExcel);
                BtnChoose.Visible = BtnPrint.Visible = BtnExcel.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                  Grd1,
                  new string[]
                    {
                        //0
                        "No",
                        
                        //1-2
                        "Document#",
                        "",
                    },
                   new int[]
                    {
                        //0
                        30,
 
                        //1-2
                        150, 20
                    }
              );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1 });
            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            switch (DocType)
            {
                case "1":
                    SQL.AppendLine("Select distinct DocNo ");
                    SQL.AppendLine("From tblrecvvdhdr ");
                    SQL.AppendLine("Where POInd='Y' and Find_In_SET(DocNo ,@Realization); ");
                    break;
                case "2":
                    SQL.AppendLine("Select distinct DocNo ");
                    SQL.AppendLine("From tblrecvvdhdr ");
                    SQL.AppendLine("Where POInd='N' and Find_In_SET(DocNo ,@Realization); ");
                    break;
                case "3":
                    SQL.AppendLine("Select distinct DocNo ");
                    SQL.AppendLine("From tblcashadvancesettlementdtl ");
                    SQL.AppendLine("Where Find_In_SET(DocNo ,@Realization); ");
                    break;
                case "4":
                    SQL.AppendLine("Select distinct DocNo ");
                    SQL.AppendLine("From tblpettycashdisbursementhdr ");
                    SQL.AppendLine("Where Find_In_SET(DocNo ,@Realization); ");
                    break;
                case "5":
                    SQL.AppendLine("Select distinct DocNo ");
                    SQL.AppendLine("From tblvoucherhdr ");
                    SQL.AppendLine("Where Find_In_SET(DocNo ,@Realization); ");
                    break;
            }

            return SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {

        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = string.Empty;
                Sm.CmParam<string>(ref cm, "@Realization", Realization);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL() + Filter,
                        new string[]
                        { 
                            //0
                            "DocNo", 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        }, true, false, false, false
                    );
                //iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3 });

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        #endregion

        #region Grid Control Events

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                switch (DocType) 
                {
                    case "1":
                        var f1 = new FrmRecvVd("***");
                        f1.Tag = "***";
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f1.ShowDialog();
                        break;
                    case "2":
                        var f2 = new FrmRecvVd2("***");
                        f2.Tag = "***";
                        f2.WindowState = FormWindowState.Normal;
                        f2.StartPosition = FormStartPosition.CenterScreen;
                        f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f2.ShowDialog();
                        break;
                    case "3":
                        var f3 = new FrmCashAdvanceSettlement("***");
                        f3.Tag = "***";
                        f3.WindowState = FormWindowState.Normal;
                        f3.StartPosition = FormStartPosition.CenterScreen;
                        f3.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f3.ShowDialog();
                        break;
                    case "4":
                        var f4 = new FrmPettyCashDisbursement("***");
                        f4.Tag = "***";
                        f4.WindowState = FormWindowState.Normal;
                        f4.StartPosition = FormStartPosition.CenterScreen;
                        f4.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f4.ShowDialog();
                        break;
                    case "5":
                        var f5 = new FrmVoucher("***");
                        f5.Tag = "***";
                        f5.WindowState = FormWindowState.Normal;
                        f5.StartPosition = FormStartPosition.CenterScreen;
                        f5.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f5.ShowDialog();
                        break;
                }

            }
        }

        #endregion

        #endregion
    }
}
