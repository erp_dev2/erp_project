﻿namespace RunSystem
{
    partial class FrmWorkSchedule2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TmeD1Out2 = new DevExpress.XtraEditors.TimeEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TmeD1In2 = new DevExpress.XtraEditors.TimeEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TmeD1Out1 = new DevExpress.XtraEditors.TimeEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkD1HolidayInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtWSCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TmeD1In1 = new DevExpress.XtraEditors.TimeEdit();
            this.TxtWSName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TmeD2Out2 = new DevExpress.XtraEditors.TimeEdit();
            this.TmeD2In2 = new DevExpress.XtraEditors.TimeEdit();
            this.TmeD2Out1 = new DevExpress.XtraEditors.TimeEdit();
            this.ChkD2HolidayInd = new DevExpress.XtraEditors.CheckEdit();
            this.TmeD2In1 = new DevExpress.XtraEditors.TimeEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TmeD3Out2 = new DevExpress.XtraEditors.TimeEdit();
            this.TmeD3In2 = new DevExpress.XtraEditors.TimeEdit();
            this.TmeD3Out1 = new DevExpress.XtraEditors.TimeEdit();
            this.ChkD3HolidayInd = new DevExpress.XtraEditors.CheckEdit();
            this.TmeD3In1 = new DevExpress.XtraEditors.TimeEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TmeD6Out2 = new DevExpress.XtraEditors.TimeEdit();
            this.TmeD6In2 = new DevExpress.XtraEditors.TimeEdit();
            this.TmeD6Out1 = new DevExpress.XtraEditors.TimeEdit();
            this.ChkD6HolidayInd = new DevExpress.XtraEditors.CheckEdit();
            this.TmeD6In1 = new DevExpress.XtraEditors.TimeEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TmeD5Out2 = new DevExpress.XtraEditors.TimeEdit();
            this.TmeD5In2 = new DevExpress.XtraEditors.TimeEdit();
            this.TmeD5Out1 = new DevExpress.XtraEditors.TimeEdit();
            this.ChkD5HolidayInd = new DevExpress.XtraEditors.CheckEdit();
            this.TmeD5In1 = new DevExpress.XtraEditors.TimeEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TmeD4Out2 = new DevExpress.XtraEditors.TimeEdit();
            this.TmeD4In2 = new DevExpress.XtraEditors.TimeEdit();
            this.TmeD4Out1 = new DevExpress.XtraEditors.TimeEdit();
            this.ChkD4HolidayInd = new DevExpress.XtraEditors.CheckEdit();
            this.TmeD4In1 = new DevExpress.XtraEditors.TimeEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TmeD7Out2 = new DevExpress.XtraEditors.TimeEdit();
            this.TmeD7In2 = new DevExpress.XtraEditors.TimeEdit();
            this.TmeD7Out1 = new DevExpress.XtraEditors.TimeEdit();
            this.ChkD7HolidayInd = new DevExpress.XtraEditors.CheckEdit();
            this.TmeD7In1 = new DevExpress.XtraEditors.TimeEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD1Out2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD1In2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD1Out1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkD1HolidayInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWSCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD1In1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWSName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD2Out2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD2In2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD2Out1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkD2HolidayInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD2In1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD3Out2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD3In2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD3Out1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkD3HolidayInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD3In1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD6Out2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD6In2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD6Out1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkD6HolidayInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD6In1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD5Out2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD5In2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD5Out1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkD5HolidayInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD5In1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD4Out2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD4In2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD4Out1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkD4HolidayInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD4In1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD7Out2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD7In2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD7Out1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkD7HolidayInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD7In1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 255);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.TmeD7Out2);
            this.panel2.Controls.Add(this.TmeD7In2);
            this.panel2.Controls.Add(this.TmeD7Out1);
            this.panel2.Controls.Add(this.ChkD7HolidayInd);
            this.panel2.Controls.Add(this.TmeD7In1);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.TmeD6Out2);
            this.panel2.Controls.Add(this.TmeD6In2);
            this.panel2.Controls.Add(this.TmeD6Out1);
            this.panel2.Controls.Add(this.ChkD6HolidayInd);
            this.panel2.Controls.Add(this.TmeD6In1);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.TmeD5Out2);
            this.panel2.Controls.Add(this.TmeD5In2);
            this.panel2.Controls.Add(this.TmeD5Out1);
            this.panel2.Controls.Add(this.ChkD5HolidayInd);
            this.panel2.Controls.Add(this.TmeD5In1);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.TmeD4Out2);
            this.panel2.Controls.Add(this.TmeD4In2);
            this.panel2.Controls.Add(this.TmeD4Out1);
            this.panel2.Controls.Add(this.ChkD4HolidayInd);
            this.panel2.Controls.Add(this.TmeD4In1);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.TmeD3Out2);
            this.panel2.Controls.Add(this.TmeD3In2);
            this.panel2.Controls.Add(this.TmeD3Out1);
            this.panel2.Controls.Add(this.ChkD3HolidayInd);
            this.panel2.Controls.Add(this.TmeD3In1);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TmeD2Out2);
            this.panel2.Controls.Add(this.TmeD2In2);
            this.panel2.Controls.Add(this.TmeD2Out1);
            this.panel2.Controls.Add(this.ChkD2HolidayInd);
            this.panel2.Controls.Add(this.TmeD2In1);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.TmeD1Out2);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TmeD1In2);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TmeD1Out1);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.ChkD1HolidayInd);
            this.panel2.Controls.Add(this.TxtWSCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TmeD1In1);
            this.panel2.Controls.Add(this.TxtWSName);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Size = new System.Drawing.Size(772, 255);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(171, 50);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(451, 20);
            this.MeeRemark.TabIndex = 15;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(121, 53);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 14);
            this.label10.TabIndex = 14;
            this.label10.Text = "Remark";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeD1Out2
            // 
            this.TmeD1Out2.EditValue = null;
            this.TmeD1Out2.EnterMoveNextControl = true;
            this.TmeD1Out2.Location = new System.Drawing.Point(417, 100);
            this.TmeD1Out2.Name = "TmeD1Out2";
            this.TmeD1Out2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD1Out2.Properties.Appearance.Options.UseFont = true;
            this.TmeD1Out2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD1Out2.Properties.Mask.EditMask = "HH:mm";
            this.TmeD1Out2.Size = new System.Drawing.Size(62, 20);
            this.TmeD1Out2.TabIndex = 38;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(417, 78);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 14);
            this.label6.TabIndex = 37;
            this.label6.Text = "Break Out";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeD1In2
            // 
            this.TmeD1In2.EditValue = null;
            this.TmeD1In2.EnterMoveNextControl = true;
            this.TmeD1In2.Location = new System.Drawing.Point(353, 100);
            this.TmeD1In2.Name = "TmeD1In2";
            this.TmeD1In2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD1In2.Properties.Appearance.Options.UseFont = true;
            this.TmeD1In2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD1In2.Properties.Mask.EditMask = "HH:mm";
            this.TmeD1In2.Size = new System.Drawing.Size(62, 20);
            this.TmeD1In2.TabIndex = 36;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(358, 78);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 14);
            this.label5.TabIndex = 35;
            this.label5.Text = "Break In";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeD1Out1
            // 
            this.TmeD1Out1.EditValue = null;
            this.TmeD1Out1.EnterMoveNextControl = true;
            this.TmeD1Out1.Location = new System.Drawing.Point(288, 100);
            this.TmeD1Out1.Name = "TmeD1Out1";
            this.TmeD1Out1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD1Out1.Properties.Appearance.Options.UseFont = true;
            this.TmeD1Out1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD1Out1.Properties.Mask.EditMask = "HH:mm";
            this.TmeD1Out1.Size = new System.Drawing.Size(62, 20);
            this.TmeD1Out1.TabIndex = 34;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(288, 79);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 14);
            this.label4.TabIndex = 18;
            this.label4.Text = "Check Out";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkD1HolidayInd
            // 
            this.ChkD1HolidayInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkD1HolidayInd.Location = new System.Drawing.Point(185, 100);
            this.ChkD1HolidayInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkD1HolidayInd.Name = "ChkD1HolidayInd";
            this.ChkD1HolidayInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkD1HolidayInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkD1HolidayInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkD1HolidayInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkD1HolidayInd.Properties.Appearance.Options.UseFont = true;
            this.ChkD1HolidayInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkD1HolidayInd.Properties.Caption = "";
            this.ChkD1HolidayInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkD1HolidayInd.Size = new System.Drawing.Size(19, 22);
            this.ChkD1HolidayInd.TabIndex = 28;
            // 
            // TxtWSCode
            // 
            this.TxtWSCode.EnterMoveNextControl = true;
            this.TxtWSCode.Location = new System.Drawing.Point(171, 6);
            this.TxtWSCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWSCode.Name = "TxtWSCode";
            this.TxtWSCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWSCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWSCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWSCode.Properties.Appearance.Options.UseFont = true;
            this.TxtWSCode.Properties.MaxLength = 16;
            this.TxtWSCode.Size = new System.Drawing.Size(167, 20);
            this.TxtWSCode.TabIndex = 10;
            this.TxtWSCode.Validated += new System.EventHandler(this.TxtWSCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(46, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Work Schedule Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(43, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Work Schedule Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeD1In1
            // 
            this.TmeD1In1.EditValue = null;
            this.TmeD1In1.EnterMoveNextControl = true;
            this.TmeD1In1.Location = new System.Drawing.Point(222, 100);
            this.TmeD1In1.Name = "TmeD1In1";
            this.TmeD1In1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD1In1.Properties.Appearance.Options.UseFont = true;
            this.TmeD1In1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD1In1.Properties.Mask.EditMask = "HH:mm";
            this.TmeD1In1.Size = new System.Drawing.Size(62, 20);
            this.TmeD1In1.TabIndex = 32;
            // 
            // TxtWSName
            // 
            this.TxtWSName.EnterMoveNextControl = true;
            this.TxtWSName.Location = new System.Drawing.Point(171, 28);
            this.TxtWSName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWSName.Name = "TxtWSName";
            this.TxtWSName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWSName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWSName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWSName.Properties.Appearance.Options.UseFont = true;
            this.TxtWSName.Properties.MaxLength = 40;
            this.TxtWSName.Size = new System.Drawing.Size(451, 20);
            this.TxtWSName.TabIndex = 13;
            this.TxtWSName.Validated += new System.EventHandler(this.TxtWSName_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(227, 79);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Check In";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(346, 5);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(67, 22);
            this.ChkActInd.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(174, 79);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 14);
            this.label7.TabIndex = 16;
            this.label7.Text = "Holiday";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(119, 103);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 14);
            this.label8.TabIndex = 42;
            this.label8.Text = "Monday";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(115, 124);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 14);
            this.label9.TabIndex = 48;
            this.label9.Text = "Tuesday";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeD2Out2
            // 
            this.TmeD2Out2.EditValue = null;
            this.TmeD2Out2.EnterMoveNextControl = true;
            this.TmeD2Out2.Location = new System.Drawing.Point(417, 121);
            this.TmeD2Out2.Name = "TmeD2Out2";
            this.TmeD2Out2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD2Out2.Properties.Appearance.Options.UseFont = true;
            this.TmeD2Out2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD2Out2.Properties.Mask.EditMask = "HH:mm";
            this.TmeD2Out2.Size = new System.Drawing.Size(62, 20);
            this.TmeD2Out2.TabIndex = 47;
            // 
            // TmeD2In2
            // 
            this.TmeD2In2.EditValue = null;
            this.TmeD2In2.EnterMoveNextControl = true;
            this.TmeD2In2.Location = new System.Drawing.Point(353, 121);
            this.TmeD2In2.Name = "TmeD2In2";
            this.TmeD2In2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD2In2.Properties.Appearance.Options.UseFont = true;
            this.TmeD2In2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD2In2.Properties.Mask.EditMask = "HH:mm";
            this.TmeD2In2.Size = new System.Drawing.Size(62, 20);
            this.TmeD2In2.TabIndex = 46;
            // 
            // TmeD2Out1
            // 
            this.TmeD2Out1.EditValue = null;
            this.TmeD2Out1.EnterMoveNextControl = true;
            this.TmeD2Out1.Location = new System.Drawing.Point(288, 121);
            this.TmeD2Out1.Name = "TmeD2Out1";
            this.TmeD2Out1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD2Out1.Properties.Appearance.Options.UseFont = true;
            this.TmeD2Out1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD2Out1.Properties.Mask.EditMask = "HH:mm";
            this.TmeD2Out1.Size = new System.Drawing.Size(62, 20);
            this.TmeD2Out1.TabIndex = 45;
            // 
            // ChkD2HolidayInd
            // 
            this.ChkD2HolidayInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkD2HolidayInd.Location = new System.Drawing.Point(185, 121);
            this.ChkD2HolidayInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkD2HolidayInd.Name = "ChkD2HolidayInd";
            this.ChkD2HolidayInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkD2HolidayInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkD2HolidayInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkD2HolidayInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkD2HolidayInd.Properties.Appearance.Options.UseFont = true;
            this.ChkD2HolidayInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkD2HolidayInd.Properties.Caption = "";
            this.ChkD2HolidayInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkD2HolidayInd.Size = new System.Drawing.Size(19, 22);
            this.ChkD2HolidayInd.TabIndex = 43;
            // 
            // TmeD2In1
            // 
            this.TmeD2In1.EditValue = null;
            this.TmeD2In1.EnterMoveNextControl = true;
            this.TmeD2In1.Location = new System.Drawing.Point(222, 121);
            this.TmeD2In1.Name = "TmeD2In1";
            this.TmeD2In1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD2In1.Properties.Appearance.Options.UseFont = true;
            this.TmeD2In1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD2In1.Properties.Mask.EditMask = "HH:mm";
            this.TmeD2In1.Size = new System.Drawing.Size(62, 20);
            this.TmeD2In1.TabIndex = 44;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(97, 145);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 14);
            this.label11.TabIndex = 54;
            this.label11.Text = "Wednesday";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeD3Out2
            // 
            this.TmeD3Out2.EditValue = null;
            this.TmeD3Out2.EnterMoveNextControl = true;
            this.TmeD3Out2.Location = new System.Drawing.Point(417, 142);
            this.TmeD3Out2.Name = "TmeD3Out2";
            this.TmeD3Out2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD3Out2.Properties.Appearance.Options.UseFont = true;
            this.TmeD3Out2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD3Out2.Properties.Mask.EditMask = "HH:mm";
            this.TmeD3Out2.Size = new System.Drawing.Size(62, 20);
            this.TmeD3Out2.TabIndex = 53;
            // 
            // TmeD3In2
            // 
            this.TmeD3In2.EditValue = null;
            this.TmeD3In2.EnterMoveNextControl = true;
            this.TmeD3In2.Location = new System.Drawing.Point(353, 142);
            this.TmeD3In2.Name = "TmeD3In2";
            this.TmeD3In2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD3In2.Properties.Appearance.Options.UseFont = true;
            this.TmeD3In2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD3In2.Properties.Mask.EditMask = "HH:mm";
            this.TmeD3In2.Size = new System.Drawing.Size(62, 20);
            this.TmeD3In2.TabIndex = 52;
            // 
            // TmeD3Out1
            // 
            this.TmeD3Out1.EditValue = null;
            this.TmeD3Out1.EnterMoveNextControl = true;
            this.TmeD3Out1.Location = new System.Drawing.Point(288, 142);
            this.TmeD3Out1.Name = "TmeD3Out1";
            this.TmeD3Out1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD3Out1.Properties.Appearance.Options.UseFont = true;
            this.TmeD3Out1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD3Out1.Properties.Mask.EditMask = "HH:mm";
            this.TmeD3Out1.Size = new System.Drawing.Size(62, 20);
            this.TmeD3Out1.TabIndex = 51;
            // 
            // ChkD3HolidayInd
            // 
            this.ChkD3HolidayInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkD3HolidayInd.Location = new System.Drawing.Point(185, 142);
            this.ChkD3HolidayInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkD3HolidayInd.Name = "ChkD3HolidayInd";
            this.ChkD3HolidayInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkD3HolidayInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkD3HolidayInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkD3HolidayInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkD3HolidayInd.Properties.Appearance.Options.UseFont = true;
            this.ChkD3HolidayInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkD3HolidayInd.Properties.Caption = "";
            this.ChkD3HolidayInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkD3HolidayInd.Size = new System.Drawing.Size(19, 22);
            this.ChkD3HolidayInd.TabIndex = 49;
            // 
            // TmeD3In1
            // 
            this.TmeD3In1.EditValue = null;
            this.TmeD3In1.EnterMoveNextControl = true;
            this.TmeD3In1.Location = new System.Drawing.Point(222, 142);
            this.TmeD3In1.Name = "TmeD3In1";
            this.TmeD3In1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD3In1.Properties.Appearance.Options.UseFont = true;
            this.TmeD3In1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD3In1.Properties.Mask.EditMask = "HH:mm";
            this.TmeD3In1.Size = new System.Drawing.Size(62, 20);
            this.TmeD3In1.TabIndex = 50;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(113, 208);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 14);
            this.label12.TabIndex = 72;
            this.label12.Text = "Saturday";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeD6Out2
            // 
            this.TmeD6Out2.EditValue = null;
            this.TmeD6Out2.EnterMoveNextControl = true;
            this.TmeD6Out2.Location = new System.Drawing.Point(417, 205);
            this.TmeD6Out2.Name = "TmeD6Out2";
            this.TmeD6Out2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD6Out2.Properties.Appearance.Options.UseFont = true;
            this.TmeD6Out2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD6Out2.Properties.Mask.EditMask = "HH:mm";
            this.TmeD6Out2.Size = new System.Drawing.Size(62, 20);
            this.TmeD6Out2.TabIndex = 71;
            // 
            // TmeD6In2
            // 
            this.TmeD6In2.EditValue = null;
            this.TmeD6In2.EnterMoveNextControl = true;
            this.TmeD6In2.Location = new System.Drawing.Point(353, 205);
            this.TmeD6In2.Name = "TmeD6In2";
            this.TmeD6In2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD6In2.Properties.Appearance.Options.UseFont = true;
            this.TmeD6In2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD6In2.Properties.Mask.EditMask = "HH:mm";
            this.TmeD6In2.Size = new System.Drawing.Size(62, 20);
            this.TmeD6In2.TabIndex = 70;
            // 
            // TmeD6Out1
            // 
            this.TmeD6Out1.EditValue = null;
            this.TmeD6Out1.EnterMoveNextControl = true;
            this.TmeD6Out1.Location = new System.Drawing.Point(288, 205);
            this.TmeD6Out1.Name = "TmeD6Out1";
            this.TmeD6Out1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD6Out1.Properties.Appearance.Options.UseFont = true;
            this.TmeD6Out1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD6Out1.Properties.Mask.EditMask = "HH:mm";
            this.TmeD6Out1.Size = new System.Drawing.Size(62, 20);
            this.TmeD6Out1.TabIndex = 69;
            // 
            // ChkD6HolidayInd
            // 
            this.ChkD6HolidayInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkD6HolidayInd.Location = new System.Drawing.Point(185, 205);
            this.ChkD6HolidayInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkD6HolidayInd.Name = "ChkD6HolidayInd";
            this.ChkD6HolidayInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkD6HolidayInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkD6HolidayInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkD6HolidayInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkD6HolidayInd.Properties.Appearance.Options.UseFont = true;
            this.ChkD6HolidayInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkD6HolidayInd.Properties.Caption = "";
            this.ChkD6HolidayInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkD6HolidayInd.Size = new System.Drawing.Size(19, 22);
            this.ChkD6HolidayInd.TabIndex = 67;
            // 
            // TmeD6In1
            // 
            this.TmeD6In1.EditValue = null;
            this.TmeD6In1.EnterMoveNextControl = true;
            this.TmeD6In1.Location = new System.Drawing.Point(222, 205);
            this.TmeD6In1.Name = "TmeD6In1";
            this.TmeD6In1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD6In1.Properties.Appearance.Options.UseFont = true;
            this.TmeD6In1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD6In1.Properties.Mask.EditMask = "HH:mm";
            this.TmeD6In1.Size = new System.Drawing.Size(62, 20);
            this.TmeD6In1.TabIndex = 68;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(130, 187);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 14);
            this.label13.TabIndex = 66;
            this.label13.Text = "Friday";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeD5Out2
            // 
            this.TmeD5Out2.EditValue = null;
            this.TmeD5Out2.EnterMoveNextControl = true;
            this.TmeD5Out2.Location = new System.Drawing.Point(417, 184);
            this.TmeD5Out2.Name = "TmeD5Out2";
            this.TmeD5Out2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD5Out2.Properties.Appearance.Options.UseFont = true;
            this.TmeD5Out2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD5Out2.Properties.Mask.EditMask = "HH:mm";
            this.TmeD5Out2.Size = new System.Drawing.Size(62, 20);
            this.TmeD5Out2.TabIndex = 65;
            // 
            // TmeD5In2
            // 
            this.TmeD5In2.EditValue = null;
            this.TmeD5In2.EnterMoveNextControl = true;
            this.TmeD5In2.Location = new System.Drawing.Point(353, 184);
            this.TmeD5In2.Name = "TmeD5In2";
            this.TmeD5In2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD5In2.Properties.Appearance.Options.UseFont = true;
            this.TmeD5In2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD5In2.Properties.Mask.EditMask = "HH:mm";
            this.TmeD5In2.Size = new System.Drawing.Size(62, 20);
            this.TmeD5In2.TabIndex = 64;
            // 
            // TmeD5Out1
            // 
            this.TmeD5Out1.EditValue = null;
            this.TmeD5Out1.EnterMoveNextControl = true;
            this.TmeD5Out1.Location = new System.Drawing.Point(288, 184);
            this.TmeD5Out1.Name = "TmeD5Out1";
            this.TmeD5Out1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD5Out1.Properties.Appearance.Options.UseFont = true;
            this.TmeD5Out1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD5Out1.Properties.Mask.EditMask = "HH:mm";
            this.TmeD5Out1.Size = new System.Drawing.Size(62, 20);
            this.TmeD5Out1.TabIndex = 63;
            // 
            // ChkD5HolidayInd
            // 
            this.ChkD5HolidayInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkD5HolidayInd.Location = new System.Drawing.Point(185, 184);
            this.ChkD5HolidayInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkD5HolidayInd.Name = "ChkD5HolidayInd";
            this.ChkD5HolidayInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkD5HolidayInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkD5HolidayInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkD5HolidayInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkD5HolidayInd.Properties.Appearance.Options.UseFont = true;
            this.ChkD5HolidayInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkD5HolidayInd.Properties.Caption = "";
            this.ChkD5HolidayInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkD5HolidayInd.Size = new System.Drawing.Size(19, 22);
            this.ChkD5HolidayInd.TabIndex = 61;
            // 
            // TmeD5In1
            // 
            this.TmeD5In1.EditValue = null;
            this.TmeD5In1.EnterMoveNextControl = true;
            this.TmeD5In1.Location = new System.Drawing.Point(222, 184);
            this.TmeD5In1.Name = "TmeD5In1";
            this.TmeD5In1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD5In1.Properties.Appearance.Options.UseFont = true;
            this.TmeD5In1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD5In1.Properties.Mask.EditMask = "HH:mm";
            this.TmeD5In1.Size = new System.Drawing.Size(62, 20);
            this.TmeD5In1.TabIndex = 62;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(111, 166);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 14);
            this.label14.TabIndex = 60;
            this.label14.Text = "Thursday";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeD4Out2
            // 
            this.TmeD4Out2.EditValue = null;
            this.TmeD4Out2.EnterMoveNextControl = true;
            this.TmeD4Out2.Location = new System.Drawing.Point(417, 163);
            this.TmeD4Out2.Name = "TmeD4Out2";
            this.TmeD4Out2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD4Out2.Properties.Appearance.Options.UseFont = true;
            this.TmeD4Out2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD4Out2.Properties.Mask.EditMask = "HH:mm";
            this.TmeD4Out2.Size = new System.Drawing.Size(62, 20);
            this.TmeD4Out2.TabIndex = 59;
            // 
            // TmeD4In2
            // 
            this.TmeD4In2.EditValue = null;
            this.TmeD4In2.EnterMoveNextControl = true;
            this.TmeD4In2.Location = new System.Drawing.Point(353, 163);
            this.TmeD4In2.Name = "TmeD4In2";
            this.TmeD4In2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD4In2.Properties.Appearance.Options.UseFont = true;
            this.TmeD4In2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD4In2.Properties.Mask.EditMask = "HH:mm";
            this.TmeD4In2.Size = new System.Drawing.Size(62, 20);
            this.TmeD4In2.TabIndex = 58;
            // 
            // TmeD4Out1
            // 
            this.TmeD4Out1.EditValue = null;
            this.TmeD4Out1.EnterMoveNextControl = true;
            this.TmeD4Out1.Location = new System.Drawing.Point(288, 163);
            this.TmeD4Out1.Name = "TmeD4Out1";
            this.TmeD4Out1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD4Out1.Properties.Appearance.Options.UseFont = true;
            this.TmeD4Out1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD4Out1.Properties.Mask.EditMask = "HH:mm";
            this.TmeD4Out1.Size = new System.Drawing.Size(62, 20);
            this.TmeD4Out1.TabIndex = 57;
            // 
            // ChkD4HolidayInd
            // 
            this.ChkD4HolidayInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkD4HolidayInd.Location = new System.Drawing.Point(185, 163);
            this.ChkD4HolidayInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkD4HolidayInd.Name = "ChkD4HolidayInd";
            this.ChkD4HolidayInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkD4HolidayInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkD4HolidayInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkD4HolidayInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkD4HolidayInd.Properties.Appearance.Options.UseFont = true;
            this.ChkD4HolidayInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkD4HolidayInd.Properties.Caption = "";
            this.ChkD4HolidayInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkD4HolidayInd.Size = new System.Drawing.Size(19, 22);
            this.ChkD4HolidayInd.TabIndex = 55;
            // 
            // TmeD4In1
            // 
            this.TmeD4In1.EditValue = null;
            this.TmeD4In1.EnterMoveNextControl = true;
            this.TmeD4In1.Location = new System.Drawing.Point(222, 163);
            this.TmeD4In1.Name = "TmeD4In1";
            this.TmeD4In1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD4In1.Properties.Appearance.Options.UseFont = true;
            this.TmeD4In1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD4In1.Properties.Mask.EditMask = "HH:mm";
            this.TmeD4In1.Size = new System.Drawing.Size(62, 20);
            this.TmeD4In1.TabIndex = 56;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(121, 229);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 14);
            this.label15.TabIndex = 78;
            this.label15.Text = "Sunday";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeD7Out2
            // 
            this.TmeD7Out2.EditValue = null;
            this.TmeD7Out2.EnterMoveNextControl = true;
            this.TmeD7Out2.Location = new System.Drawing.Point(417, 226);
            this.TmeD7Out2.Name = "TmeD7Out2";
            this.TmeD7Out2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD7Out2.Properties.Appearance.Options.UseFont = true;
            this.TmeD7Out2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD7Out2.Properties.Mask.EditMask = "HH:mm";
            this.TmeD7Out2.Size = new System.Drawing.Size(62, 20);
            this.TmeD7Out2.TabIndex = 77;
            // 
            // TmeD7In2
            // 
            this.TmeD7In2.EditValue = null;
            this.TmeD7In2.EnterMoveNextControl = true;
            this.TmeD7In2.Location = new System.Drawing.Point(353, 226);
            this.TmeD7In2.Name = "TmeD7In2";
            this.TmeD7In2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD7In2.Properties.Appearance.Options.UseFont = true;
            this.TmeD7In2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD7In2.Properties.Mask.EditMask = "HH:mm";
            this.TmeD7In2.Size = new System.Drawing.Size(62, 20);
            this.TmeD7In2.TabIndex = 76;
            // 
            // TmeD7Out1
            // 
            this.TmeD7Out1.EditValue = null;
            this.TmeD7Out1.EnterMoveNextControl = true;
            this.TmeD7Out1.Location = new System.Drawing.Point(288, 226);
            this.TmeD7Out1.Name = "TmeD7Out1";
            this.TmeD7Out1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD7Out1.Properties.Appearance.Options.UseFont = true;
            this.TmeD7Out1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD7Out1.Properties.Mask.EditMask = "HH:mm";
            this.TmeD7Out1.Size = new System.Drawing.Size(62, 20);
            this.TmeD7Out1.TabIndex = 75;
            // 
            // ChkD7HolidayInd
            // 
            this.ChkD7HolidayInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkD7HolidayInd.Location = new System.Drawing.Point(185, 226);
            this.ChkD7HolidayInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkD7HolidayInd.Name = "ChkD7HolidayInd";
            this.ChkD7HolidayInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkD7HolidayInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkD7HolidayInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkD7HolidayInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkD7HolidayInd.Properties.Appearance.Options.UseFont = true;
            this.ChkD7HolidayInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkD7HolidayInd.Properties.Caption = "";
            this.ChkD7HolidayInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkD7HolidayInd.Size = new System.Drawing.Size(19, 22);
            this.ChkD7HolidayInd.TabIndex = 73;
            // 
            // TmeD7In1
            // 
            this.TmeD7In1.EditValue = null;
            this.TmeD7In1.EnterMoveNextControl = true;
            this.TmeD7In1.Location = new System.Drawing.Point(222, 226);
            this.TmeD7In1.Name = "TmeD7In1";
            this.TmeD7In1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeD7In1.Properties.Appearance.Options.UseFont = true;
            this.TmeD7In1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeD7In1.Properties.Mask.EditMask = "HH:mm";
            this.TmeD7In1.Size = new System.Drawing.Size(62, 20);
            this.TmeD7In1.TabIndex = 74;
            // 
            // FrmWorkSchedule2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 255);
            this.Name = "FrmWorkSchedule2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD1Out2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD1In2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD1Out1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkD1HolidayInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWSCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD1In1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWSName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD2Out2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD2In2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD2Out1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkD2HolidayInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD2In1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD3Out2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD3In2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD3Out1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkD3HolidayInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD3In1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD6Out2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD6In2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD6Out1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkD6HolidayInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD6In1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD5Out2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD5In2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD5Out1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkD5HolidayInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD5In1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD4Out2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD4In2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD4Out1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkD4HolidayInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD4In1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD7Out2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD7In2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD7Out1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkD7HolidayInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeD7In1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TimeEdit TmeD7Out2;
        internal DevExpress.XtraEditors.TimeEdit TmeD7In2;
        internal DevExpress.XtraEditors.TimeEdit TmeD7Out1;
        private DevExpress.XtraEditors.CheckEdit ChkD7HolidayInd;
        internal DevExpress.XtraEditors.TimeEdit TmeD7In1;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TimeEdit TmeD6Out2;
        internal DevExpress.XtraEditors.TimeEdit TmeD6In2;
        internal DevExpress.XtraEditors.TimeEdit TmeD6Out1;
        private DevExpress.XtraEditors.CheckEdit ChkD6HolidayInd;
        internal DevExpress.XtraEditors.TimeEdit TmeD6In1;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TimeEdit TmeD5Out2;
        internal DevExpress.XtraEditors.TimeEdit TmeD5In2;
        internal DevExpress.XtraEditors.TimeEdit TmeD5Out1;
        private DevExpress.XtraEditors.CheckEdit ChkD5HolidayInd;
        internal DevExpress.XtraEditors.TimeEdit TmeD5In1;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TimeEdit TmeD4Out2;
        internal DevExpress.XtraEditors.TimeEdit TmeD4In2;
        internal DevExpress.XtraEditors.TimeEdit TmeD4Out1;
        private DevExpress.XtraEditors.CheckEdit ChkD4HolidayInd;
        internal DevExpress.XtraEditors.TimeEdit TmeD4In1;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TimeEdit TmeD3Out2;
        internal DevExpress.XtraEditors.TimeEdit TmeD3In2;
        internal DevExpress.XtraEditors.TimeEdit TmeD3Out1;
        private DevExpress.XtraEditors.CheckEdit ChkD3HolidayInd;
        internal DevExpress.XtraEditors.TimeEdit TmeD3In1;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TimeEdit TmeD2Out2;
        internal DevExpress.XtraEditors.TimeEdit TmeD2In2;
        internal DevExpress.XtraEditors.TimeEdit TmeD2Out1;
        private DevExpress.XtraEditors.CheckEdit ChkD2HolidayInd;
        internal DevExpress.XtraEditors.TimeEdit TmeD2In1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TimeEdit TmeD1Out2;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TimeEdit TmeD1In2;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TimeEdit TmeD1Out1;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkD1HolidayInd;
        internal DevExpress.XtraEditors.TextEdit TxtWSCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TimeEdit TmeD1In1;
        private DevExpress.XtraEditors.TextEdit TxtWSName;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
    }
}