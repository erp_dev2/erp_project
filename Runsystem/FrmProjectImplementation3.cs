﻿#region Update
/*
 *  26/12/2022 [TYO/MNET] new app based on ProjectImplementation
    26/12/2022 [TYO/MNET] tambah tab upload file berdasarkan param IsPRJIAllowToUploadFile
    05/01/2023 [TYO/MNET] bug upload file masih save nama directory
    24/01/2023 [TYO/MNET] bug ketika edit data
    06/02/2023 [DITA/MNET] perombakan menu prji sesuai dengan kebutuhan mnet
    22/10/2023 [MAU/MNET] btn BOQ menuju pada menu BOQ baru
    03/03/2023 [MYA/MNET] Penambahan warning berupa validasi perbedaan warna untuk mengakomodir ketika task pada project sudah mendekati atau melewati plan end date di Menu Project Implementation
    03/03/2023 [WED/MNET] bug fix TotalResource saat show data
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using System.IO;
using System.Drawing.Imaging;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmProjectImplementation3 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mListItCtCodeForResource = string.Empty,
             mItGrpCodeForRemuneration = string.Empty,
            mItGrpCodeForDirectCost = string.Empty,
            mItGrpCodeForIndirectCost = string.Empty,
            mPPN10ForProjectResourceShowZero = string.Empty;
        internal FrmProjectImplementation3Find FrmFind;
        private string
          
            mProjectResourceCode = string.Empty,
            mStateInd = string.Empty,
            mWBSFormula = string.Empty,
            mRemunerationItCtCode = string.Empty,
            mDirectCostItCtCode = string.Empty,
            mPRJISourceForRBP = string.Empty;
        private bool 
            mIsProjectImplementationApprovalBySiteMandatory = false,
            mIsProjectImplementationDisplayTotalResource = false,
            mIsProjectImplementationDisplayTaxFields = false,
            mIsWBSBobotColumnAutoCompute = false,
            mIsPRJIUseWBS2 = false,
            mIsPSModuleShowApproverRemarkInfo = false,
            mIsPRJIAllowToUploadFile = false,
            mIsPRJIUploadFileMandatory = false;
        internal bool
            mIsFilterBySite = false,
            mIsWBS2SameAsWBS = false;

        private string
         mPortForFTPClient = string.Empty,
         mHostAddrForFTPClient = string.Empty,
         mSharedFolderForFTPClient = string.Empty,
         mUsernameForFTPClient = string.Empty,
         mPasswordForFTPClient = string.Empty,
         mFileSizeMaxUploadFTPClient = string.Empty,
         mFormatFTPClient = string.Empty;

        private byte[] downloadedData;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmProjectImplementation3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmProjectImplementation");

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueProcessInd, "ProjectImplementationProcessInd");

                TcProjectImplementation.SelectTab("TpgDocument");
                SetGrd4();

                TcProjectImplementation.SelectTab("TpgIssue");
                SetGrd3();

                TcProjectImplementation.SelectTab("TpgResource");
                SetGrd2();
               

                TcProjectImplementation.SelectTab("TpgApproval");
                SetGrd6();

                TcProjectImplementation.SelectTab("TpgWBS2");
                Sl.SetLueProjectStageCode(ref LueStageCode2);
                Sl.SetLueProjectTaskCode(ref LueTaskCode);
                LueStageCode2.Visible = false;
                LueTaskCode.Visible = false;
                SetGrd5();

                
                if (!mIsPRJIUseWBS2) TcProjectImplementation.TabPages.Remove(TpgWBS2);

                TcProjectImplementation.SelectTab("TpgWBS");
                Sl.SetLueProjectStageCode(ref LueStageCode);
                Sl.SetLueProjectTaskCode(ref LueTaskCode);
                LueStageCode.Visible = false;
                LueTaskCode.Visible = false;
                SetGrd();
               
                if (mIsPRJIAllowToUploadFile) SetGrd7();
                SetFormControl(mState.View);

                

                if (!mIsPRJIAllowToUploadFile)
                    TcProjectImplementation.TabPages.Remove(TpgUploadFile);

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Grid 1 - WBS

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                { 
                    //0
                    "StageCode", 
                    //1-5
                    "Stage",
                    "Description", 
                    "Plan Start Date", 
                    "Plan End Date",
                    "Price",
                    //6-10
                    "Weight %",
                    "Settled", 
                    "Remark", 
                    "Settled Weight %",
                    "Settled Revenue"+Environment.NewLine+"Amount",
                    //11-13
                    "Estimated Cost"+Environment.NewLine+"Recorded", 
                    "Project Delivery#", 
                    "Settled Date"
                },
                new int[] 
                { 
                    0, 
                    200, 100, 100, 100, 150,
                    100, 100, 100, 100, 150,
                    150, 180, 100
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] {5, 6, 9, 10,11 }, 8);
            Sm.GrdColCheck(Grd1, new int[] { 7 });
            Sm.GrdFormatDate(Grd1, new int[] { 3, 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,13 });
            Sm.GrdColInvisible(Grd1, new int[] { 0});
        }

        #endregion

        #region Grid 5 - WBS 2

        private void SetGrd5()
        {
            Grd5.Cols.Count = 14;
            Grd5.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd5, new string[] 
                { 
                     //0
                    "StageCode", 

                    //1-5
                    "Stage",
                    "Task Code",
                    "Task",
                    "Plan Start Date",
                    "Plan End Date",
                    
                    //6-10
                    "Price",
                    "Weight %",
                    "Settled",
                    "Remark",
                    "Settled Revenue"+Environment.NewLine+"Amount",

                    //11-13
                    "Estimated Cost"+Environment.NewLine+"Recorded",
                    "Project Delivery#",
                    "Settled Date"
                },
                new int[] 
                { 
                    0, 
                    200, 0, 200, 100, 100,
                    100, 100, 100, 100, 120,
                    120, 100, 100
                }
            );
            Sm.GrdFormatDec(Grd5, new int[] { 6, 7, 10, 11 }, 8);
            Sm.GrdColCheck(Grd5, new int[] { 8 });
            Sm.GrdFormatDate(Grd5, new int[] { 4, 5, 13 });
            Sm.GrdColReadOnly(Grd5, new int[] {0, 2, 8, 9, 10, 11 ,12 ,13 });
            Sm.GrdColInvisible(Grd5, new int[] { 0, 2 });
        }

        #endregion

        #region Grid 2 - Resource

        private void SetGrd2()
        {
            Grd2.Cols.Count = 16;
            Grd2.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd2, new string[] 
                { 
                    "",
                    "ResourceItCode", "Resource", "Group", "Remark", "Quantity 1"+Environment.NewLine+"(Unit)", 
                    "Quantity 2"+Environment.NewLine+"(Time)", "Unit Price", "Tax", "Total Without Tax", "Total Tax", 
                    "Total With Tax", "Group It Code", "", "Detail#", "Sequence"
                },
                new int[] 
                { 
                    20, 
                    0, 200, 200, 200, 150, 
                    150, 180, 180, 200, 200, 
                    200,150, 20, 120, 65
                }
            );
            Sm.GrdFormatDec(Grd2, new int[] { 5, 6, 7, 8, 9, 10, 11 }, 8);
            Sm.GrdColButton(Grd2, new int[] { 0, 13 });
            Sm.GrdColReadOnly(Grd2, new int[] { 1, 2, 3, 9, 10, 11, 14 });
            Sm.GrdColInvisible(Grd2, new int[] { 12 });
            Grd2.Cols[15].Move(1);
        }

        #endregion

        #region Grid 3 - Issue

        private void SetGrd3()
        {
            Grd3.Cols.Count = 2;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd3, new string[] 
                { 
                    "Remark",
                    "PIC"
                },
                new int[] 
                { 
                    200, 
                    200
                }
            );
        }

        #endregion

        #region Grid 4 - Document

        private void SetGrd4()
        {
            Grd4.Cols.Count = 5;
            Grd4.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd4, new string[] 
                { 
                    "",
                    "DocCode", "Document", "Date", "Remark"
                },
                new int[] 
                { 
                    20, 
                    0, 200, 100, 200
                }
            );
            Sm.GrdColButton(Grd4, new int[] { 0 });
            Sm.GrdColReadOnly(Grd4, new int[] { 1, 2 });
            Sm.GrdFormatDate(Grd4, new int[] { 3 });
        }

        #endregion

        #region Grid 6 - Approval

        private void SetGrd6()
        {
            Grd6.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd6,
                    new string[]
                    {
                        //0
                        "No",

                        //1-4
                        "User",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd6, new int[] { 3 });
            Sm.GrdColReadOnly(Grd6, new int[] { 0, 1, 2, 3, 4 });
        }

        #endregion

        #region Grid 7 - Upload File

        private void SetGrd7()
        {
            if(mIsPRJIAllowToUploadFile)
            {
                Grd7.Cols.Count = 7;
                Grd7.FrozenArea.ColCount = 3;

                Sm.GrdHdrWithColWidth(
                        Grd7,
                        new string[]
                        {
                        //0
                        "D No",
                        //1-5
                        "File Name",
                        "",
                        "D",
                        "Upload By",
                        "Date",

                        //6
                        "Time"
                        },
                         new int[]
                        {
                        0,
                        250, 20, 20, 100, 100,
                        100
                        }
                    );

                Sm.GrdColInvisible(Grd7, new int[] { 0 }, false);
                Sm.GrdFormatDate(Grd7, new int[] { 5 });
                Sm.GrdFormatTime(Grd7, new int[] { 6 });
                //Sm.GrdColButton(Grd7, new int[] { 2, 3 });
                Sm.GrdColButton(Grd7, new int[] { 2 }, "1");
                Sm.GrdColButton(Grd7, new int[] { 3 }, "2");
                Sm.GrdColReadOnly(true, true, Grd7, new int[] { 0, 1, 4, 5, 6 });
                if(!mIsPRJIAllowToUploadFile) Sm.GrdColReadOnly(Grd7, new int[] { 2 });
                Grd7.Cols[2].Move(1);
            }
        }

        #endregion
        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtSOContractDocNo, TxtAchievement,
                        TxtBOQDocNo, TxtLOPDocNo, LueCtCode, LueCtContactPersonName,  LueStageCode, LueTaskCode,
                        DteDocDt2, DtePlanEndDt, DtePlanStartDt, MeeRemark, LueProcessInd, TxtProjectName, TxtTotalResource,
                        TxtProjectCode, TxtType, TxtTotalResource, TxtRemunerationCost, TxtDirectCost, 
                        TxtTotalWeight,  TxtTotalPrice, TxtTotalPrice2, MeeApprovalRemark, TxtTotalRev, TxtEstimatedCost, TxtContractAmtBefTax, TxtContractAmtBefTax2, TxtTotalWeight2,
                        TxtRemunerationPerc,TxtDirectPerc, TxtIndirectPerc, TxtIndirectCost, TxtTotalResourcePerc, TxtCostPerc, TxtPICSales
                    }, true);
                    BtnSOContractDocNo.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = Grd2.ReadOnly = Grd3.ReadOnly = Grd4.ReadOnly = Grd5.ReadOnly = true;
                    if (TxtDocNo.Text.Length > 0)
                    {
                        Grd2.ReadOnly = false;
                        Sm.GrdColReadOnly(Grd2, new int[] { 4, 5, 6, 7, 8, 15 });
                    }
                    BtnImportResource.Enabled = false;

                    if(mIsPRJIAllowToUploadFile)
                    {
                        //Sm.GrdColReadOnly(true, true, Grd7, new int[] { 0, 1, 2, 3, 4, 5, 6 });

                        for (int Row = 0; Row < Grd7.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd7, Row, 1).Length == 0)
                            {
                                Grd7.Cells[Row, 2].ReadOnly = iGBool.False;
                                Grd7.Cells[Row, 3].ReadOnly = iGBool.True;
                            }
                            else
                            {
                                Grd7.Cells[Row, 2].ReadOnly = iGBool.True;
                                Grd7.Cells[Row, 3].ReadOnly = iGBool.False;
                            }
                        }
                    }

                    TxtDocNo.Focus();
                    break;

                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueStageCode2, LueTaskCode, DteDocDt2, DtePlanStartDt2, DtePlanEndDt2, MeeRemark,
                    }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProcessInd }, true);
                    BtnSOContractDocNo.Enabled = true;
                    Grd1.ReadOnly = Grd2.ReadOnly = Grd3.ReadOnly = Grd4.ReadOnly = Grd5.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 4, 5, 6, 7, 8, 15 });
                    Sm.GrdColReadOnly(Grd5, new int[] { 0, 2, 8, 9, 10, 11, 12, 13 });
                    BtnImportResource.Enabled = true;
                    if(mIsPRJIAllowToUploadFile) 
                        Sm.GrdColReadOnly(false, true, Grd7, new int[] { 2, 3 });
                    DteDocDt.Focus();
                    break;

                case mState.Edit:
                    if (Sm.GetLue(LueProcessInd) == "D") Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProcessInd }, false);
                    else Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProcessInd }, true);

                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();

                    if (Sm.GetLue(LueProcessInd) == "D")
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LueStageCode2, LueTaskCode, DtePlanStartDt2, DtePlanEndDt2
                        }, false);
                        Grd1.ReadOnly = Grd2.ReadOnly = Grd5.ReadOnly = false;
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 4, 5, 6, 7, 8, 15 });
                    }
                    else Grd1.ReadOnly = Grd2.ReadOnly = Grd5.ReadOnly = true;

                    if (mIsPRJIAllowToUploadFile)
                    {
                        if(Sm.GetLue(LueProcessInd) == "F")
                            Grd7.ReadOnly = true;

                        else if(Sm.GetLue(LueProcessInd) == "D")
                            Grd7.ReadOnly = false;
                    }

                    //if (Sm.GetLue(LueProcessInd) == "F")
                    //{
                    //    Grd7.ReadOnly = true;
                    //    Sm.GrdColReadOnly(Grd7, new int[] { 0, 1, 2, 4, 5, 6 });
                    //    Grd7.Cells[fCell.RowIndex, 3].ReadOnly = iGBool.False;
                    //}
                    //else
                    //{
                    //    Grd7.ReadOnly = false;
                    //    Sm.GrdColReadOnly(Grd7, new int[] { 0, 1, 4, 5, 6 });
                    //    Grd7.Cells[fCell.RowIndex, 3].ReadOnly = iGBool.False;
                    //    Grd7.Cells[fCell.RowIndex, 2].ReadOnly = iGBool.False;
                    //}
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt2
                    }, false);
                    Grd3.ReadOnly = Grd4.ReadOnly = false;
                    break;
            }
        }

        private void ClearData()
        {
            
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtSOContractDocNo,
                TxtBOQDocNo, TxtLOPDocNo, LueCtCode, LueCtContactPersonName, LueStageCode, LueTaskCode,
                DteDocDt2, DtePlanEndDt, DtePlanStartDt, MeeRemark, TxtStatus, LueProcessInd, TxtProjectName,
                TxtProjectCode, TxtType, MeeApprovalRemark, TxtPICSales
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtAchievement, TxtTotalResource, TxtTotalResource, TxtRemunerationCost, TxtDirectCost,  
                TxtTotalWeight,  TxtTotalRev, TxtEstimatedCost, TxtContractAmtBefTax, TxtContractAmtBefTax2, TxtTotalWeight2,
                TxtTotalPrice, TxtTotalResource, TxtRemunerationCost, TxtDirectCost, 
                TxtTotalWeight,  TxtTotalPrice2, TxtRemunerationPerc,TxtDirectPerc, TxtIndirectPerc, TxtIndirectCost, TxtTotalResourcePerc, TxtCostPerc
            }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
            mStateInd = string.Empty;
        }

        #region Clear Grid

        internal void ClearGrd()
        {
            ClearGrdWBS1();
            ClearGrdWBS2();
            ClearGrdResource();
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd4, true);
            if(mIsPRJIAllowToUploadFile)
                Sm.ClearGrd(Grd7, true);
        }

        private void ClearGrdWBS1()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 6, 9, 10, 11 });
        }
        private void ClearGrdWBS2()
        {
            Sm.ClearGrd(Grd5, true);
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 6, 7, 10, 11 });
        }
        private void ClearGrdResource()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 5, 6, 7, 8, 9, 10, 11 });
        }

        #endregion

        #endregion

        #region Additional Methods

        #region Upload File

        private MySqlCommand SaveUploadFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Project Implementation - Upload File */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("Delete From TblProjectImplementationFile Where DocNo=@DocNo; ");

            for (int r = 0; r < Grd7.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd7, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblProjectImplementationFile(DocNo, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd7, r, 1));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName), LastUpBy = @UserCode , LastUpDt = @Dt; ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void ShowUploadFile(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd7, ref cm,
                   "Select A.DNo, A.FileName, B.UserName, A.CreateDt "+
                   "From  TblProjectImplementationFile A "+
                   "Inner Join TblUser B On A.CreateBy = B.UserCode "+
                   "Where DocNo=@DocNo Order By Dno",

                    new string[]
                    {
                        "Dno",
                        "FileName", "UserName", "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd7, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("T", Grd7, dr, c, Row, 6, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd7, 0, 0);
        }
        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string EmpCode, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateUploadFile(EmpCode, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName);
        }

        private bool IsUploadFileMandatory()
        {

            if (Grd7.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No files to upload, please upload at least one file.");
                TcProjectImplementation.SelectTab("TpgUploadFile");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsPRJIAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsPRJIAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsPRJIAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsPRJIAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsPRJIAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsPRJIAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd7, Row, 4).Length == 0)
            //if (mIsPRJIAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd7, Row, 1) != Sm.GetGrdStr(Grd7, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblProjectImplementationFile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand UpdateUploadFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProjectImplementationFile Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }
        #endregion

        private void ProcessWBSSummary()
        {
            if (BtnSave.Enabled)
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<WBSSummary>();

                ClearGrdWBS1();
                try
                {
                    GetWBS2(ref l);
                    if (l.Count > 0)
                    {
                        var lsum = new List<WBSSummary>();
                        GetWBSSummary(ref l, ref lsum);
                        ProcessWBSGrid(ref lsum);
                    }
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    l.Clear();
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void GetWBS2(ref List<WBSSummary> l)
        {           
            for (int i = 0; i < Grd5.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd5, i, 0).Length > 0)
                {
                    l.Add(new WBSSummary()
                    {
                        StageCode = Sm.GetGrdStr(Grd5, i, 0),
                        StageName = Sm.GetGrdStr(Grd5, i, 1),
                        PlanStartDt = Sm.GetGrdDate(Grd5, i, 4),
                        PlanEndDt = Sm.GetGrdDate(Grd5, i, 5),
                        Price = Sm.GetGrdDec(Grd5, i, 6),
                        BobotPerc = Sm.GetGrdDec(Grd5, i, 7)
                    });
                }
            }

        }

        private void GetWBSSummary(ref List<WBSSummary> l, ref List<WBSSummary> lsum)
        {
            lsum = l.GroupBy(s => new { s.StageCode, s.StageName })
               .Select(t => new WBSSummary()
               {
                   StageCode = t.Key.StageCode,
                   StageName = t.Key.StageName,
                   PlanStartDt = t.Min(d => d.PlanStartDt),
                   PlanEndDt = t.Max(d => d.PlanEndDt),
                   Price = t.Sum(d => d.Price),
                   BobotPerc = t.Sum(d => d.BobotPerc),
                  
               }).ToList();
        }

        private void ProcessWBSGrid(ref List<WBSSummary> lsum)
        {
            Grd1.BeginUpdate();
            int i = 0;
            foreach (var x in lsum)
            {
                Grd1.Rows.Add();
                Grd1.Cells[i, 0].Value = x.StageCode;
                Grd1.Cells[i, 1].Value = x.StageName;
                if(x.PlanStartDt.Length == 0)
                    Grd1.Cells[i, 3].Value = string.Empty;
                else
                    Grd1.Cells[i, 3].Value = Sm.ConvertDate(x.PlanStartDt); ;
                if(x.PlanEndDt.Length == 0)
                    Grd1.Cells[i, 4].Value = string.Empty;
                else
                    Grd1.Cells[i, 4].Value = Sm.ConvertDate(x.PlanEndDt); ;
                Grd1.Cells[i, 5].Value = Sm.FormatNum(x.Price, 0);
                Grd1.Cells[i, 6].Value = Sm.FormatNum(x.BobotPerc, 2);
                i++;
            }
            Grd1.EndUpdate();
            ComputeTotalPrice();
        }
        private void LockGrid()
        {
            if (mPRJISourceForRBP == "1")
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    string detail = Sm.GetGrdStr(Grd2, Row, 14);

                    if (detail.Length > 0)
                    {
                        for (int Col = 0; Col < 15; Col++)
                        {
                            Grd2.Rows[Row].ReadOnly = iGBool.True;
                        }
                    }
                    else
                    {
                        int[] ColIndex = new int[] { 4, 5, 6, 7, 8, 15 };
                        for (int Col = 0; Col < ColIndex.Length; Col++)
                        {
                            Grd2.Cols[ColIndex[Col]].CellStyle.ReadOnly = iGBool.False;
                        }
                    }
                }
            }
        }


        internal void ComputeTotalResource()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd2, row, 2).Length != 0)
                {
                    Total += Sm.GetGrdDec(Grd2, row, 11);
                }
            }
            TxtTotalResource.EditValue = Sm.FormatNum(Total, 0);

            ComputeTotal();
        }

        internal void ComputeRemunerationCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd2, row, 2).Length != 0 && Sm.GetGrdStr(Grd2, row, 12).ToString() == mItGrpCodeForRemuneration && mItGrpCodeForRemuneration.Length > 0)
                {
                    Total += Sm.GetGrdDec(Grd2, row, 11);
                }
            }
            TxtRemunerationCost.EditValue = Sm.FormatNum(Total, 0);

            ComputeTotal();
        }

        internal void ComputeDirectCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd2, row, 2).Length != 0 && Sm.GetGrdStr(Grd2, row, 12).ToString() == mItGrpCodeForDirectCost && mItGrpCodeForDirectCost.Length > 0)
                {
                    Total += Sm.GetGrdDec(Grd2, row, 11);
                }
            }
            TxtDirectCost.EditValue = Sm.FormatNum(Total, 0);
            ComputeTotal();
        }

        internal void ComputeIndirectCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd2, row, 2).Length != 0 && Sm.GetGrdStr(Grd2, row, 12).ToString() == mItGrpCodeForIndirectCost && mItGrpCodeForIndirectCost.Length > 0)
                {
                    Total += Sm.GetGrdDec(Grd2, row, 11);
                }
            }
            TxtIndirectCost.EditValue = Sm.FormatNum(Total, 0);
            ComputeTotal();
        }

      

        internal void ComputeTotal()
        {
            decimal Total = 0m,
                CostPerc = 0m,
                ContractAmtBefTax = 0m,
                RemunerationCost = 0m,
                DirectCost = 0m,
                IndirectCost = 0m, RemunerationPerc = 0m, DirectCostPerc = 0m, IndirectCostPerc = 0m;
            

            RemunerationCost = Sm.GetDecValue(TxtRemunerationCost.Text);
            DirectCost = Sm.GetDecValue(TxtDirectCost.Text);
            IndirectCost = Sm.GetDecValue(TxtIndirectCost.Text);

            Total = Sm.GetDecValue(TxtRemunerationCost.Text) + Sm.GetDecValue(TxtDirectCost.Text)+ Sm.GetDecValue(TxtIndirectCost.Text);
            TxtTotalResource.EditValue = Sm.FormatNum(Total, 0);
            if (Sm.GetDecValue(TxtContractAmtBefTax.Text) != 0)
            {
                ContractAmtBefTax = Sm.GetDecValue(TxtContractAmtBefTax.Text);
                CostPerc = Total / (ContractAmtBefTax * 0.01m);
                TxtCostPerc.EditValue = Sm.FormatNum(CostPerc, 0);
            }

            if (Total != 0)
            {
                RemunerationPerc = RemunerationCost / (Total * 0.01m);
                DirectCostPerc = DirectCost / (Total * 0.01m);
                IndirectCostPerc = IndirectCost / (Total * 0.01m);
            }

            TxtRemunerationPerc.EditValue = Sm.FormatNum(RemunerationPerc, 2);
            TxtDirectPerc.EditValue = Sm.FormatNum(DirectCostPerc, 2);
            TxtIndirectPerc.EditValue = Sm.FormatNum(IndirectCostPerc, 2);
            TxtTotalResourcePerc.EditValue = Sm.FormatNum(100m, 0);


        }

        internal void CheckDueDate()
        {
            int Date1 = 0;
            int Date2 = 0;
            if(Sm.GetLue(LueProcessInd) == "F" && TxtStatus.Text == "Approved")
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    if (!Sm.GetGrdBool(Grd1, row, 7))
                    {
                        Date1 = Convert.ToInt32(Sm.GetGrdDate(Grd1, row, 4).Substring(0, 8));
                        Date2 = Convert.ToInt32(Sm.GetValue("Select Date_Format(CURDATE(),'%Y%m%d')"));
                        if ((Date1 - Date2) < 0)
                        {
                            Grd1.Rows[row].BackColor = Color.Red;
                        }
                        else if ((Date1 - Date2) <= 7 && (Date1 - Date2) >= 0)
                        {
                            Grd1.Rows[row].BackColor = Color.Yellow;
                        }
                    }
                }
            }
            
        }

        internal string GetSelectedItCode()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedDocCode()
        {
            var SQL = string.Empty;
            if (Grd4.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd4, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");                       
            mListItCtCodeForResource = Sm.GetParameter("ListItCtCodeForResource");
            mIsProjectImplementationApprovalBySiteMandatory = Sm.GetParameterBoo("IsProjectImplementationApprovalBySiteMandatory");
            mItGrpCodeForRemuneration = Sm.GetParameter("ItGrpCodeForRemuneration");
            mItGrpCodeForDirectCost = Sm.GetParameter("ItGrpCodeForDirectCost");
            mPPN10ForProjectResourceShowZero = Sm.GetParameter("PPN10%ForProjectResourceShowZero");
            mIsProjectImplementationDisplayTaxFields = Sm.GetParameterBoo("IsProjectImplementationDisplayTaxFields");
            mIsProjectImplementationDisplayTotalResource = Sm.GetParameterBoo("IsProjectImplementationDisplayTotalResource");
            mIsWBSBobotColumnAutoCompute = Sm.GetParameterBoo("IsWBSBobotColumnAutoCompute");
            mWBSFormula = Sm.GetParameter("WBSFormula");
            mRemunerationItCtCode = Sm.GetParameter("RemunerationItCtCode");
            mDirectCostItCtCode = Sm.GetParameter("DirectCostItCtCode");
            mIsPRJIUseWBS2 = Sm.GetParameterBoo("IsPRJIUseWBS2");
            mIsWBS2SameAsWBS = Sm.GetParameterBoo("IsWBS2SameAsWBS");
            mIsPSModuleShowApproverRemarkInfo = Sm.GetParameterBoo("IsPSModuleShowApproverRemarkInfo");
            mPRJISourceForRBP = Sm.GetParameter("PRJISourceForRBP");
            mIsPRJIAllowToUploadFile = Sm.GetParameterBoo("IsPRJIAllowToUploadFile");
            mIsPRJIUploadFileMandatory = Sm.GetParameterBoo("IsPRJIUploadFileMandatory");
            mItGrpCodeForIndirectCost = Sm.GetParameter("ItGrpCodeForIndirectCost");

            //Upload File
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");


            if (mWBSFormula.Length == 0) mWBSFormula = "1";
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal void ShowSOContractDocument(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select X1.DocNo, A.BOQDocNo, B.LOPDocNo, B.CtCode, B.CtContactPersonName, B.SPCode, C.ProjectName, A.Amt, D.OptDesc, C.ProjectResource, A.ProjectCode2, F.UserCode PICSales, ");
            SQL.AppendLine("A.RemunerationAmt, A.DirectCostAmt, A.IndirectCostAmt, A.TotalCost, A.CostPerc, A.RemunerationPerc, A.DirectCostPerc, A.IndirectCostPerc, A.TotalCostPerc ");
            SQL.AppendLine("From TblSOContractRevisionHdr X1 ");
            SQL.AppendLine("Inner Join TblSOContractHdr A On X1.SOCDocNo = A.DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblLopHdr C On B.LOPDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblOption D On C.ProjectType = D.OptCode And D.OptCat = 'ProjectType' ");
            SQL.AppendLine("Inner Join TblOption E On C.ProjectType = E.OptCode And E.OptCat = 'ProjectResource' ");
            SQL.AppendLine("Left Join TblUser F On C.PICCode = F.UserCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By X1.CreateDt Desc Limit 1; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "BOQDocNo", "LOPDocNo", "CtCode", "CtContactPersonName", "SPCode",

                    //6-10
                    "ProjectName", "Amt", "OptDesc", "ProjectResource", "ProjectCode2",

                    //11-15
                    "PICSales", "RemunerationAmt" , "DirectCostAmt" , "IndirectCostAmt" , "TotalCost" ,

                    //16-20
                     "CostPerc", "RemunerationPerc" ,"DirectCostPerc" , "IndirectCostPerc" , "TotalCostPerc"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    TxtLOPDocNo.EditValue = Sm.DrStr(dr, c[2]);
                    Sl.SetLueCtCode(ref LueCtCode);
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[3]));
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.DrStr(dr, c[3]));
                    Sm.SetLue(LueCtContactPersonName, Sm.DrStr(dr, c[4]));
                    //Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[5]));
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[6]);
                    TxtContractAmtBefTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                    TxtContractAmtBefTax2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                    TxtType.EditValue = Sm.DrStr(dr, c[8]);
                    mProjectResourceCode = Sm.DrStr(dr, c[9]);
                    TxtProjectCode.EditValue = Sm.DrStr(dr, c[10]);
                    TxtPICSales.EditValue = Sm.DrStr(dr, c[11]);
                    TxtRemunerationCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    TxtDirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                    TxtIndirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                    TxtTotalResource.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[15]), 0); 
                    TxtCostPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0); 
                    TxtRemunerationPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]), 0); 
                    TxtDirectPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0); 
                    TxtIndirectPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[19]), 0); 
                    TxtTotalResourcePerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[20]), 0); 
                }, true
            );

        }

        public void ShowResource(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ResourceItCode, A.Sequence, B.ItName,C.ItGrpCode, C.ItGrpName, A.Remark, A.Qty1, A.Qty2, A.UPrice, A.Tax, A.TotalWithoutTax, A.TotalTax, A.Amt ");
            SQL.AppendLine(", D.DocNo As RBPDocNo ");
            SQL.AppendLine("From TblSOContractDtl6 A ");
            SQL.AppendLine("Inner Join TblItem B On A.ResourceItCode = B.ItCode ");
            SQL.AppendLine("Left Join TblItemGroup C On B.ItGrpCode = C.ItGrpCode ");
            SQL.AppendLine("Left Join TblProjectImplementationRBPHdr D On A.DocNo = D.PRJIDocNo And D.ResourceItCode = A.ResourceItCode And D.CancelInd = 'N' ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.DNo; ");

            var cm2 = new MySqlCommand();
            Sm.CmParam<String>(ref cm2, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm2, SQL.ToString(),
                new string[]
                {
                    "ResourceItCode",
                    "ItName", "ItGrpName", "Remark", "Qty1", "Qty2",
                    "UPrice", "Tax", "TotalWithoutTax", "TotalTax", "Amt",
                    "ItGrpCode", "RBPDocNo", "Sequence"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11 });

        }

      

        //private void ComputePercentage2(decimal TotalBobot)
        //{
        //    for (int i = 0; i < Grd5.Rows.Count; ++i)
        //    {
        //        if (Sm.GetGrdStr(Grd5, i, 0).Length > 0)
        //        {
        //            Grd5.Cells[i, 8].Value = Sm.Round(((Sm.GetGrdDec(Grd5, i, 7) / TotalBobot) * 100m), 2);
        //        }
        //    }
        //}

        private void ComputeTotalPrice()
        {
            decimal mTotalPrice = 0m, mTotalWeight = 0m; ;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    mTotalPrice += Sm.GetGrdDec(Grd1, Row, 5);

            TxtTotalPrice.EditValue = Sm.FormatNum(mTotalPrice, 0);

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    mTotalWeight += Sm.GetGrdDec(Grd1, Row, 6);

            TxtTotalWeight.EditValue = Sm.FormatNum(mTotalWeight, 0);

        }

        private void ComputeTotalPrice2()
        {
            decimal mTotalPrice2 = 0m, mTotalWeight2 = 0m;
            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                    mTotalPrice2 += Sm.GetGrdDec(Grd5, Row, 6);

            TxtTotalPrice2.EditValue = Sm.FormatNum(mTotalPrice2, 0);

            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                    mTotalWeight2 += Sm.GetGrdDec(Grd5, Row, 7);

            TxtTotalWeight2.EditValue = Sm.FormatNum(mTotalWeight2, 0);

            
        }

        internal void ComputeDetail(int r, int readOnlyColumn)
        {
            decimal ContractAmtBefTax = 0m, Price = 0m, WeightPerc= 0m;
            ContractAmtBefTax = Sm.GetDecValue(TxtContractAmtBefTax2.Text);
            Price = Sm.GetGrdDec(Grd5, r, 6);
            WeightPerc = Sm.GetGrdDec(Grd5, r, 7);

            if (readOnlyColumn == 7)
            {
                if (Price > 0)
                    WeightPerc = (Price / ContractAmtBefTax) * 100m;
            }
            
            if (readOnlyColumn == 6)
            {
                if (WeightPerc > 0)
                    Price = WeightPerc * 0.01m * ContractAmtBefTax  ;
            }

            Grd5.Cells[r, 6].Value = Price; // Price
            Grd5.Cells[r, 7].Value = Sm.FormatNum(WeightPerc, 2); //Weight%
        }

        internal void ComputeSettledAmt()
        {
            decimal mTotalRev = 0m, mEstimatedCost = 0m; ;
            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                    mTotalRev += Sm.GetGrdDec(Grd5, Row, 10);
            TxtTotalRev.EditValue = Sm.FormatNum(mTotalRev, 0);

            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                    mEstimatedCost += Sm.GetGrdDec(Grd5, Row, 11);
            TxtEstimatedCost.EditValue = Sm.FormatNum(mEstimatedCost, 0);
        }


        private void SetLueCtPersonCode(ref DXE.LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblCustomerContactPerson Where CtCode= '" + CtCode + "' Order By ContactPersonName",
                "Contact Person Name");
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProjectImplementation3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetLue(LueProcessInd,"D");
                mStateInd = "I";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            mStateInd = "E";

            if(mPRJISourceForRBP == "1") LockGrid();
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;

                string[] TableName = { /*"PRJI", "PRJI2",*/  "Resource", "PRJISign", "PRJISign2", "PRJIDtlSign", "RBPRemuneration", "RBPDirectCost" };
                List<IList> myLists = new List<IList>();
                var l = new List<PRJI>();
                var l1 = new List<PRJIDummy>();
                var l2 = new List<PRJI2>();
                var lResource = new List<Resource>();
                var lSign = new List<PRJISign>();
                var lSign2 = new List<PRJISign2>();
                var lDtlSign = new List<PRJIDtlSign>();
                var lR = new List<RBPData>();
                var lR1 = new List<RBPRemuneration>();
                var lR2 = new List<RBPDirectCost>();

                #region Kurva S Old
                //l2.Add(new PRJI2()
                //{
                //    CurrentDt = String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                //    Achievment = decimal.Parse(TxtAchievement.Text)
                //});


                //for (int r = 0; r < Grd1.Rows.Count; r++)
                //{
                //    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                //    {
                //        l1.Add(new PRJIDummy()
                //        {
                //            Stage = Sm.GetGrdStr(Grd1, r, 1),
                //            Task = Sm.GetGrdStr(Grd1, r, 3),
                //            StartDt = String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.GetGrdDate(Grd1, r, 4))),
                //            EndDt = String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.GetGrdDate(Grd1, r, 5))),
                //            EndDt2 = Sm.Left(Sm.GetGrdDate(Grd1, r, 5), 8),
                //            Bobot = Sm.GetGrdDec(Grd1, r, 6),
                //            Persentase = (Sm.GetGrdDec(Grd1, r, 7)) * 100,
                //            SettledInd = Sm.GetGrdBool(Grd1, r, 8),
                //            Akumulasi = 0m
                //        });
                //    }
                //}

                //if (l1.Count > 0)
                //{
                //    foreach (var o in l1.OrderBy(x => x.EndDt2))
                //    {
                //        l.Add(new PRJI()
                //        {
                //            Stage = o.Stage,
                //            Task = o.Task,
                //            StartDt = o.StartDt,
                //            EndDt = o.EndDt,
                //            EndDt2 = o.EndDt2,
                //            Bobot = o.Bobot,
                //            Persentase = o.Persentase,
                //            SettledInd = o.SettledInd,
                //            Akumulasi = o.Akumulasi
                //        });
                //    }
                //}

                //if (l.Count > 0)
                //{
                //    decimal mAc = 0m;
                //    for (int i = 0; i < l.Count; i++)
                //    {
                //        mAc += l[i].Persentase;
                //        l[i].Akumulasi = mAc;
                //    }
                //}

                ////l.OrderBy(o => o.EndDt2).ToList();

                //myLists.Add(l);
                //myLists.Add(l2);
                //Sm.PrintReport("ProjectImplementation", myLists, TableName, false);

                //l1.Clear(); l.Clear(); l2.Clear();

                #endregion 

                #region Resource

                var cm = new MySqlCommand();

                var SQL = new StringBuilder();

                SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, ");
                SQL.AppendLine("B.DocNo As SoContractDocNo, B.Amt As ContractAmt, D.DocNo As LOPDocNo ,D.ProjectName, ");
                SQL.AppendLine(" A.RemunerationAmt, A.DirectCostAmt, A.PPNAmt, A.ExclPPNAmt, A.PPhAmt, A.ExclPPNPPhAmt, A.ExclPPNPPhPercentage, A.ExclPPNPercentage, ( A.RemunerationAmt + A.DirectCostAmt) As Total, E.SiteName, F.OptDesc As ProjectType, B.ProjectCode2 ");
                SQL.AppendLine(" From TblProjectImplementationHdr A ");
                SQL.AppendLine(" Inner Join TblSOContractRevisionHdr A1 On A.SOContractDocno = A1.DocNo ");
                SQL.AppendLine(" Inner Join Tblsocontracthdr B on A1.SOCDocNo = B.DocNo ");
                SQL.AppendLine(" Inner Join TblBOQhdr C On B.BOQDocno = C.DocNo ");
                SQL.AppendLine(" Inner Join TblLOPHdr D On C.LOPDocNo = D.DocNo ");
                SQL.AppendLine(" Inner Join TblSite E On D.SiteCode = E.SiteCode ");
                SQL.AppendLine(" Inner Join TblOption F On D.ProjectType = F.OptCode And F.OptCat = 'ProjectType' ");
                SQL.AppendLine(" Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyName",

                         //1-5
                         "CompanyAddress",
                         "DocNo", 
                         "DocDt", 
                         "LOPDocNo",
                         "ProjectName",

                         //6-10
                         "ContractAmt", 
                         "CompanyLogo",
                         "RemunerationAmt",
                         "DirectCostAmt",
                         "PPNAmt",
                         
                         //11-15
                         "ExclPPNAmt",
                         "PPhAmt",
                         "ExclPPNPPhAmt",
                         "ExclPPNPPhPercentage",
                         "ExclPPNPercentage",

                         //16-19
                         "Total",
                         "SiteName",
                         "ProjectType",
                         "ProjectCode2"
                        
                         
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lResource.Add(new Resource()
                            {
                                CompanyName = Sm.DrStr(dr, c[0]),

                                CompanyAddress = Sm.DrStr(dr, c[1]),
                                DocNo = Sm.DrStr(dr, c[2]),
                                DocDt = Sm.DrStr(dr, c[3]),
                                LOPDocNo  = Sm.DrStr(dr, c[4]),
                                ProjectName = Sm.DrStr(dr, c[5]),

                                ContractAmt = Sm.DrDec(dr, c[6]),
                                CompanyLogo = Sm.DrStr(dr, c[7]),
                                RemunerationAmt = Sm.DrDec(dr, c[8]),
                                DirectCostAmt = Sm.DrDec(dr, c[9]),
                                PPNAmt = Sm.DrDec(dr, c[10]),

                                ExclPPNAmt = Sm.DrDec(dr, c[11]),
                                PPhAmt = Sm.DrDec(dr, c[12]),
                                ExclPPNPPhAmt = Sm.DrDec(dr, c[13]),
                                ExclPPNPPhPercentage = Sm.DrDec(dr, c[14]),
                                ExclPPNPercentage = Sm.DrDec(dr, c[15]),

                                Total = Sm.DrDec(dr, c[16]),
                                SiteName = Sm.DrStr(dr, c[17]),
                                ProjectType = Sm.DrStr(dr, c[18]),
                                ProjectCode = Sm.DrStr(dr, c[19]),
                            });
                        }
                    }
                    dr.Close();
                }

                myLists.Add(lResource);
                #endregion

                #region Detail Signature

                //Dibuat Oleh
                var cmDtl = new MySqlCommand();

                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                    SQLDtl.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                    SQLDtl.AppendLine("From ( ");
                    SQLDtl.AppendLine("    Select Distinct ");
                    SQLDtl.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, '1' As DNo, 0 As Level, 'Dibuat oleh,' As Title, Left(A.CreateDt, 8) As LastUpDt  ");
                    SQLDtl.AppendLine("    From TblProjectImplementationHdr A ");
                    SQLDtl.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                    SQLDtl.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                    SQLDtl.AppendLine(") T1 ");
                    SQLDtl.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                    SQLDtl.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                    SQLDtl.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                    SQLDtl.AppendLine("Group By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Title ");
                    SQLDtl.AppendLine("Order By T1.Level; ");

                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@Space", "-------------------------");
                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",

                         //6
                         "LastupDt"
                        });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            lSign.Add(new PRJISign()
                            {
                                Signature = Sm.DrStr(drDtl, cDtl[0]),
                                UserName = Sm.DrStr(drDtl, cDtl[1]),
                                PosName = Sm.DrStr(drDtl, cDtl[2]),
                                Space = Sm.DrStr(drDtl, cDtl[3]),
                                DNo = Sm.DrStr(drDtl, cDtl[4]),
                                Title = Sm.DrStr(drDtl, cDtl[5]),
                                LastUpDt = Sm.DrStr(drDtl, cDtl[6])
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(lSign);

                //Disetujui Oleh
                var cmDtl2 = new MySqlCommand();

                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                    SQLDtl2.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                    SQLDtl2.AppendLine("From ( ");
                    SQLDtl2.AppendLine("    Select Distinct ");
                    SQLDtl2.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, B.ApprovalDNo As DNo, D.Level, 'Mengetahui Menyetujui,' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                    SQLDtl2.AppendLine("    From TblProjectImplementationHdr A ");
                    SQLDtl2.AppendLine("    Inner Join TblDocApproval B On B.DocType='ProjectImplementation' And A.DocNo=B.DocNo  ");
                    SQLDtl2.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                    SQLDtl2.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'ProjectImplementation' And Level = '1' ");
                    SQLDtl2.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                    SQLDtl2.AppendLine(") T1 ");
                    SQLDtl2.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                    SQLDtl2.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                    SQLDtl2.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                    SQLDtl2.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                    SQLDtl2.AppendLine("Order By T1.DNo desc; ");

                    cmDtl2.CommandText = SQLDtl2.ToString();
                    Sm.CmParam<String>(ref cmDtl2, "@Space", "-------------------------");
                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {

                            lSign2.Add(new PRJISign2()
                            {
                                Signature = Sm.DrStr(drDtl2, cDtl2[0]),
                                UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                                PosName = Sm.DrStr(drDtl2, cDtl2[2]),
                                Space = Sm.DrStr(drDtl2, cDtl2[3]),
                                DNo = Sm.DrStr(drDtl2, cDtl2[4]),
                                Title = Sm.DrStr(drDtl2, cDtl2[5]),
                                LastUpDt = Sm.DrStr(drDtl2, cDtl2[6])
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(lSign2);

                //Detail Signature
                var cmDtl3 = new MySqlCommand();

                var SQLDtl3 = new StringBuilder();
                using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl3.Open();
                    cmDtl3.Connection = cnDtl3;

                    SQLDtl3.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                    SQLDtl3.AppendLine(" T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt, T1.Remark  ");
                    SQLDtl3.AppendLine("From (  ");
                    SQLDtl3.AppendLine("   Select Distinct  ");
                    SQLDtl3.AppendLine("   B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName,  ");
                    SQLDtl3.AppendLine("   B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt, 'Disetujui' As Remark  ");
                    SQLDtl3.AppendLine("   From tblprojectimplementationhdr A  ");
                    SQLDtl3.AppendLine("   Inner Join TblDocApproval B On B.DocType='ProjectImplementation' And A.DocNo=B.DocNo   ");
                    SQLDtl3.AppendLine("   Inner Join TblUser C On B.UserCode=C.UserCode  ");
                    SQLDtl3.AppendLine("   Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'ProjectImplementation' ") ;
                    SQLDtl3.AppendLine("   Left Join TblGroup E On C.GrpCode=E.GrpCode  ");
                    SQLDtl3.AppendLine("     Where A.CancelInd='N' And A.DocNo=@DocNo ");
                    SQLDtl3.AppendLine(") T1  ");
                    SQLDtl3.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode  ");
                    SQLDtl3.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode  ");
                    SQLDtl3.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature'  ");
                    SQLDtl3.AppendLine("Where T1.Level >= 2  ");
                    SQLDtl3.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName  ");
                    SQLDtl3.AppendLine("Order By T1.Level "); 


                    cmDtl3.CommandText = SQLDtl3.ToString();
                    Sm.CmParam<String>(ref cmDtl3, "@Space", "-------------------------");
                    Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);
                    var drDtl3 = cmDtl3.ExecuteReader();
                    var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                    if (drDtl3.HasRows)
                    {
                        while (drDtl3.Read())
                        {

                            lDtlSign.Add(new PRJIDtlSign()
                            {
                                Signature = Sm.DrStr(drDtl3, cDtl3[0]),
                                UserName = Sm.DrStr(drDtl3, cDtl3[1]),
                                PosName = Sm.DrStr(drDtl3, cDtl3[2]),
                                Space = Sm.DrStr(drDtl3, cDtl3[3]),
                                DNo = Sm.DrStr(drDtl3, cDtl3[4]),
                                Title = Sm.DrStr(drDtl3, cDtl3[5]),
                                LastUpDt = Sm.DrStr(drDtl3, cDtl3[6])
                            });
                        }
                    }
                    drDtl3.Close();
                }
                myLists.Add(lDtlSign);
                #endregion

                #region RBP

                var cmR = new MySqlCommand();
                var SQLR = new StringBuilder();

                SQLR.AppendLine("SELECT G.ProjectCode2, H.VdName, C.ItName, C.ItCtCode, B.UPrice, B.Amt, B.Sequence, E.Yr, E.Mth, E.TotalAmt As RemunerationAmt, E.TotalAmt As DirectCostAmt ");
                SQLR.AppendLine("FROM TblProjectImplementationHdr A ");
                SQLR.AppendLine("INNER JOIN TblProjectImplementationDtl2 B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' AND A.DocNo = @DocNo ");
                SQLR.AppendLine("INNER JOIN TblItem C ON B.ResourceItCode = C.ItCode AND (C.ItCtCode = @RemunerationItCtCode OR C.ItCtCode = @DirectCostItCtCode) ");
                SQLR.AppendLine("INNER JOIN TblProjectImplementationRBPHdr D ON B.DocNo = D.PRJIDocNo AND B.ResourceItCode = D.ResourceItCode ");
                SQLR.AppendLine("INNER JOIN TblProjectImplementationRBPDtl E ON D.DocNo = E.DocNo ");
                SQLR.AppendLine("INNER JOIN TblSOCOntractRevisionHdr F ON A.SOContractDocNo = F.DocNo ");
                SQLR.AppendLine("INNER JOIN TblSOCOntractHdr G ON F.SOCDocNo = G.DocNo ");
                SQLR.AppendLine("Left JOIN TblVendor H ON E.VdCode = H.VdCode ");
                SQLR.AppendLine("Order By right(concat('00000000', ifnull(sequence, '0')), 8) Desc ");

                using (var cnR = new MySqlConnection(Gv.ConnectionString))
                {
                    cnR.Open();
                    cmR.Connection = cnR;
                    cmR.CommandText = SQLR.ToString();
                    Sm.CmParam<String>(ref cmR, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cmR, "@RemunerationItCtCode", mRemunerationItCtCode);
                    Sm.CmParam<String>(ref cmR, "@DirectCostItCtCode", mDirectCostItCtCode);
                    var drR = cmR.ExecuteReader();
                    var cR = Sm.GetOrdinal(drR, new string[] 
                    {
                        //0
                        "ProjectCode2",

                        //1-5
                        "VdName",
                        "ItName", 
                        "ItCtCode",
                        "UPrice",
                        "Amt", 

                        //6-9
                        "Yr",
                        "Mth",
                        "RemunerationAmt", 
                        "DirectCostAmt",
                        "Sequence"
                    });
                    if (drR.HasRows)
                    {
                        
                        while (drR.Read())
                        {
                          
                            lR.Add(new RBPData()
                            {
                                ProjectCode = Sm.DrStr(drR, cR[0]),
                                VdName = Sm.DrStr(drR, cR[1]),
                                ItName = Sm.DrStr(drR, cR[2]),
                                ItCtCode = Sm.DrStr(drR, cR[3]),
                                UPrice = Sm.DrDec(drR, cR[4]),
                                Amt = Sm.DrDec(drR, cR[5]),
                                Yr = Sm.DrStr(drR, cR[6]),
                                Mth = Sm.DrStr(drR, cR[7]),
                                RemunerationAmt = Sm.DrDec(drR, cR[8]),
                                DirectCostAmt = Sm.DrDec(drR, cR[9]),
                                Sequence = Sm.DrStr(drR, cR[10])
                            });
                        }
                    }
                    drR.Close();
                }
               
                if (lR.Count > 0)
                {
                    string mProjectCode = string.Empty, mVdName = string.Empty, mItName = string.Empty, mYr = string.Empty, mSequence = string.Empty;
                    decimal mUPrice = 0m, mAmt = 0m;
                    int mRow = 0;

                    string mProjectCode2 = string.Empty, mVdName2 = string.Empty, mItName2 = string.Empty, mYr2 = string.Empty, mSequence2 = string.Empty;
                    decimal mUPrice2 = 0m, mAmt2 = 0m;
                    int mRow2 = 0;
                    int nomor1 = 0, nomor2 = 0;
                   
                    for (int i = 0; i < lR.Count; i++)
                    {
                        if (lR[i].ItCtCode == mRemunerationItCtCode)
                        {
                            if (lR1.Count == 0 || 
                                !(
                                    mProjectCode == lR[i].ProjectCode &&
                                    mVdName == lR[i].VdName &&
                                    mItName == lR[i].ItName &&
                                    mYr == lR[i].Yr &&
                                    mUPrice == lR[i].UPrice &&
                                    mAmt == lR[i].Amt
                                ))
                            {
                                mProjectCode = lR[i].ProjectCode;
                                mVdName = lR[i].VdName;
                                mItName = lR[i].ItName;
                                mYr = lR[i].Yr;
                                mUPrice = lR[i].UPrice;
                                mAmt = lR[i].Amt;
                                mSequence = lR[i].Sequence;
                                mRow = lR1.Count();
                                nomor1 += 1;

                                lR1.Add(new RBPRemuneration()
                                {
                                    No = nomor1,
                                    ProjectCode = lR[i].ProjectCode,
                                    VdName = lR[i].VdName,
                                    ItName = lR[i].ItName,
                                    UPrice = lR[i].UPrice,
                                    Amt = lR[i].Amt,
                                    Sequence = lR[i].Sequence,
                                    Yr = lR[i].Yr,
                                    Mth01Amt = (lR[i].Mth == "01") ? lR[i].RemunerationAmt : 0m,
                                    Mth02Amt = (lR[i].Mth == "02") ? lR[i].RemunerationAmt : 0m,
                                    Mth03Amt = (lR[i].Mth == "03") ? lR[i].RemunerationAmt : 0m,
                                    Mth04Amt = (lR[i].Mth == "04") ? lR[i].RemunerationAmt : 0m,
                                    Mth05Amt = (lR[i].Mth == "05") ? lR[i].RemunerationAmt : 0m,
                                    Mth06Amt = (lR[i].Mth == "06") ? lR[i].RemunerationAmt : 0m,
                                    Mth07Amt = (lR[i].Mth == "07") ? lR[i].RemunerationAmt : 0m,
                                    Mth08Amt = (lR[i].Mth == "08") ? lR[i].RemunerationAmt : 0m,
                                    Mth09Amt = (lR[i].Mth == "09") ? lR[i].RemunerationAmt : 0m,
                                    Mth10Amt = (lR[i].Mth == "10") ? lR[i].RemunerationAmt : 0m,
                                    Mth11Amt = (lR[i].Mth == "11") ? lR[i].RemunerationAmt : 0m,
                                    Mth12Amt = (lR[i].Mth == "12") ? lR[i].RemunerationAmt : 0m,
                                    TotalRemunerationAmt = 0m
                                });
                            }
                            else
                            {
                                //if (mProjectCode == lR[i].ProjectCode &&
                                //    mVdName == lR[i].VdName &&
                                //    mItName == lR[i].ItName &&
                                //    mYr == lR[i].Yr &&
                                //    mUPrice == lR[i].UPrice &&
                                //    mAmt == lR[i].Amt
                                //    )
                                //{
                                    if (lR[i].Mth == "01") lR1[mRow].Mth01Amt = lR[i].RemunerationAmt;
                                    if (lR[i].Mth == "02") lR1[mRow].Mth02Amt = lR[i].RemunerationAmt;
                                    if (lR[i].Mth == "03") lR1[mRow].Mth03Amt = lR[i].RemunerationAmt;
                                    if (lR[i].Mth == "04") lR1[mRow].Mth04Amt = lR[i].RemunerationAmt;
                                    if (lR[i].Mth == "05") lR1[mRow].Mth05Amt = lR[i].RemunerationAmt;
                                    if (lR[i].Mth == "06") lR1[mRow].Mth06Amt = lR[i].RemunerationAmt;
                                    if (lR[i].Mth == "07") lR1[mRow].Mth07Amt = lR[i].RemunerationAmt;
                                    if (lR[i].Mth == "08") lR1[mRow].Mth08Amt = lR[i].RemunerationAmt;
                                    if (lR[i].Mth == "09") lR1[mRow].Mth09Amt = lR[i].RemunerationAmt;
                                    if (lR[i].Mth == "10") lR1[mRow].Mth10Amt = lR[i].RemunerationAmt;
                                    if (lR[i].Mth == "11") lR1[mRow].Mth11Amt = lR[i].RemunerationAmt;
                                    if (lR[i].Mth == "12") lR1[mRow].Mth12Amt = lR[i].RemunerationAmt;
                                //}
                            }
                        }

                        if (lR[i].ItCtCode == mDirectCostItCtCode)
                        {
                            if (lR2.Count == 0 ||
                                !(
                                    mProjectCode2 == lR[i].ProjectCode &&
                                    mVdName2 == lR[i].VdName &&
                                    mItName2 == lR[i].ItName &&
                                    mYr2 == lR[i].Yr &&
                                    mUPrice2 == lR[i].UPrice &&
                                    mAmt2 == lR[i].Amt
                                ))
                            {
                                mProjectCode2 = lR[i].ProjectCode;
                                mVdName2 = lR[i].VdName;
                                mItName2 = lR[i].ItName;
                                mYr2 = lR[i].Yr;
                                mUPrice2 = lR[i].UPrice;
                                mAmt2 = lR[i].Amt;
                                mSequence2 = lR[i].Sequence;
                                mRow2 = lR2.Count();
                                nomor2 += 1;

                                lR2.Add(new RBPDirectCost()
                                {
                                    No = nomor2,
                                    ProjectCode = lR[i].ProjectCode,
                                    VdName = lR[i].VdName,
                                    ItName = lR[i].ItName,
                                    UPrice = lR[i].UPrice,
                                    Amt = lR[i].Amt,
                                    Sequence = lR[i].Sequence,
                                    Yr = lR[i].Yr,
                                    Mth01Amt = (lR[i].Mth == "01") ? lR[i].DirectCostAmt : 0m,
                                    Mth02Amt = (lR[i].Mth == "02") ? lR[i].DirectCostAmt : 0m,
                                    Mth03Amt = (lR[i].Mth == "03") ? lR[i].DirectCostAmt : 0m,
                                    Mth04Amt = (lR[i].Mth == "04") ? lR[i].DirectCostAmt : 0m,
                                    Mth05Amt = (lR[i].Mth == "05") ? lR[i].DirectCostAmt : 0m,
                                    Mth06Amt = (lR[i].Mth == "06") ? lR[i].DirectCostAmt : 0m,
                                    Mth07Amt = (lR[i].Mth == "07") ? lR[i].DirectCostAmt : 0m,
                                    Mth08Amt = (lR[i].Mth == "08") ? lR[i].DirectCostAmt : 0m,
                                    Mth09Amt = (lR[i].Mth == "09") ? lR[i].DirectCostAmt : 0m,
                                    Mth10Amt = (lR[i].Mth == "10") ? lR[i].DirectCostAmt : 0m,
                                    Mth11Amt = (lR[i].Mth == "11") ? lR[i].DirectCostAmt : 0m,
                                    Mth12Amt = (lR[i].Mth == "12") ? lR[i].DirectCostAmt : 0m,
                                    TotalDirectCostAmt = 0m
                                });
                            }
                            else
                            {
                                //if (mProjectCode2 == lR[i].ProjectCode &&
                                //    mVdName2 == lR[i].VdName &&
                                //    mItName2 == lR[i].ItName &&
                                //    mYr2 == lR[i].Yr &&
                                //    mUPrice2 == lR[i].UPrice &&
                                //    mAmt2 == lR[i].Amt
                                //    )
                                //{
                                if (lR[i].Mth == "01") lR2[mRow2].Mth01Amt = lR[i].DirectCostAmt;
                                if (lR[i].Mth == "02") lR2[mRow2].Mth02Amt = lR[i].DirectCostAmt;
                                if (lR[i].Mth == "03") lR2[mRow2].Mth03Amt = lR[i].DirectCostAmt;
                                if (lR[i].Mth == "04") lR2[mRow2].Mth04Amt = lR[i].DirectCostAmt;
                                if (lR[i].Mth == "05") lR2[mRow2].Mth05Amt = lR[i].DirectCostAmt;
                                if (lR[i].Mth == "06") lR2[mRow2].Mth06Amt = lR[i].DirectCostAmt;
                                if (lR[i].Mth == "07") lR2[mRow2].Mth07Amt = lR[i].DirectCostAmt;
                                if (lR[i].Mth == "08") lR2[mRow2].Mth08Amt = lR[i].DirectCostAmt;
                                if (lR[i].Mth == "09") lR2[mRow2].Mth09Amt = lR[i].DirectCostAmt;
                                if (lR[i].Mth == "10") lR2[mRow2].Mth10Amt = lR[i].DirectCostAmt;
                                if (lR[i].Mth == "11") lR2[mRow2].Mth11Amt = lR[i].DirectCostAmt;
                                if (lR[i].Mth == "12") lR2[mRow2].Mth12Amt = lR[i].DirectCostAmt;
                                //}
                            }
                        }
                    }

                    if (lR1.Count > 0)
                    {
                        for (int i = 0; i < lR1.Count; i++)
                        {
                            lR1[i].TotalRemunerationAmt = lR1[i].Mth01Amt +
                                lR1[i].Mth02Amt +
                                lR1[i].Mth03Amt +
                                lR1[i].Mth04Amt +
                                lR1[i].Mth05Amt +
                                lR1[i].Mth06Amt +
                                lR1[i].Mth07Amt +
                                lR1[i].Mth08Amt +
                                lR1[i].Mth09Amt +
                                lR1[i].Mth10Amt +
                                lR1[i].Mth11Amt +
                                lR1[i].Mth12Amt;
                        }
                    }

                    if (lR2.Count > 0)
                    {
                        for (int i = 0; i < lR2.Count; i++)
                        {
                            lR2[i].TotalDirectCostAmt = lR2[i].Mth01Amt +
                                lR2[i].Mth02Amt +
                                lR2[i].Mth03Amt +
                                lR2[i].Mth04Amt +
                                lR2[i].Mth05Amt +
                                lR2[i].Mth06Amt +
                                lR2[i].Mth07Amt +
                                lR2[i].Mth08Amt +
                                lR2[i].Mth09Amt +
                                lR2[i].Mth10Amt +
                                lR2[i].Mth11Amt +
                                lR2[i].Mth12Amt;
                        }
                    }
                }

                myLists.Add(lR1);
                myLists.Add(lR2);

                #endregion

                Sm.PrintReport("ResourceYK", myLists, TableName, false);
                Sm.PrintReport("DirectCost", myLists, TableName, false);
                Sm.PrintReport("RemunerationCost", myLists, TableName, false);


            }
        }

        #endregion

        #region Grid Method

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                ComputeTotalPrice();
                //    if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
                //    {
                //        if (Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length > 0)
                //        {
                //            decimal mEstimatedAmt = 0m;
                //            if (mWBSFormula == "1") mEstimatedAmt = Sm.GetGrdDec(Grd1, e.RowIndex, 6);

                //            Grd1.Cells[e.RowIndex, 11].Value = mEstimatedAmt;
                //        }
                //    }
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            //if (BtnSave.Enabled && !Grd1.ReadOnly)
            //{
            //    if (Sm.IsGrdColSelected(new int[] { 1, 3, 4, 5, 6 }, e.ColIndex))
            //    {
            //        //e.DoDefault = false;
            //        //if (e.ColIndex == 1) LueRequestEdit(Grd1, LueStageCode, ref fCell, ref fAccept, e);
            //        //if (e.ColIndex == 3) LueRequestEdit(Grd1, LueTaskCode, ref fCell, ref fAccept, e);
            //        //if (e.ColIndex == 4) Sm.DteRequestEdit(Grd1, DtePlanStartDt, ref fCell, ref fAccept, e);
            //        //if (e.ColIndex == 5) Sm.DteRequestEdit(Grd1, DtePlanEndDt, ref fCell, ref fAccept, e);
            //        Sm.GrdRequestEdit(Grd1, e.RowIndex);
            //        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7, 8 });
            //    }
            //}
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            //if (BtnSave.Enabled && !Grd1.ReadOnly)
            //{
            //    Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            //    Sm.GrdRemoveRow(Grd1, e, BtnSave);
            //}
        }

        private void Grd5_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd5.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
                {
                    Sm.GrdColReadOnly(Grd5, new int[] { 7 });
                    if (Sm.GetGrdStr(Grd5, e.RowIndex, 0).Length > 0)
                    {
                        ComputeDetail(e.RowIndex, 7);
                        ComputeTotalPrice2();
                    }
                }
                if (Sm.IsGrdColSelected(new int[] { 7 }, e.ColIndex))
                {
                    Sm.GrdColReadOnly(Grd5, new int[] { 6 });
                    if (Sm.GetGrdStr(Grd5, e.RowIndex, 0).Length > 0)
                    {
                        ComputeDetail(e.RowIndex, 6);
                        ComputeTotalPrice2();
                    }
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 2, 4, 5, 6, 7 }, e.ColIndex)) ProcessWBSSummary();
            }
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd5.ReadOnly)
            {
                Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd5, e, BtnSave);
                ComputeTotalPrice2();
                ComputeTotalPrice();
                ProcessWBSSummary();
            }
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd5.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 1, 3, 4, 5 }, e.ColIndex))
                {
                    //e.DoDefault = false;
                    if (e.ColIndex == 1) LueRequestEdit(Grd5, LueStageCode2, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 3) LueRequestEdit(Grd5, LueTaskCode, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 4) Sm.DteRequestEdit(Grd5, DtePlanStartDt2, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 5) Sm.DteRequestEdit(Grd5, DtePlanEndDt2, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd5, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 6, 7, 10, 11 });
                }
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd2.ReadOnly)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 5, 6, 7, 8, 9, 10, 11 }, e);
                if (Sm.IsGrdColSelected(new int[] { 5, 6, 7, 8, 9, 10, 11 }, e.ColIndex))
                {
                    decimal mQty1 = 0m, mQty2 = 0m, mUPrice = 0m, mTax, mTotalWithoutTax = 0m, mTotalTax = 0m, mAmt = 0m;
                    mQty1 = Sm.GetGrdDec(Grd2, e.RowIndex, 5);
                    mQty2 = Sm.GetGrdDec(Grd2, e.RowIndex, 6);
                    mUPrice = Sm.GetGrdDec(Grd2, e.RowIndex, 7);
                    mTax = Sm.GetGrdDec(Grd2, e.RowIndex, 8);

                    mTotalWithoutTax = mQty1 * mQty2 * mUPrice;
                    mTotalTax = mQty1 * mQty2 * mTax;
                    mAmt = mTotalWithoutTax + mTotalTax;

                    Grd2.Cells[e.RowIndex, 9].Value = Math.Round(mTotalWithoutTax, 2);
                    Grd2.Cells[e.RowIndex, 10].Value = Math.Round(mTotalTax, 2);
                    Grd2.Cells[e.RowIndex, 11].Value = Math.Round(mAmt, 2);
                    ComputeTotalResource();
                    ComputeDirectCost(); ComputeRemunerationCost();ComputeIndirectCost();
                }
            }
        }
        
        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (!Grd2.ReadOnly)
            {
                if(e.ColIndex == 0 && mStateInd.Length > 0) Sm.FormShowDialog(new FrmProjectImplementation3Dlg2(this));
                if (e.ColIndex == 13 && Sm.GetGrdStr(Grd2, e.RowIndex, 14).Length > 0)
                {
                    var f = new FrmProjectImplementationRBP(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 14);
                    f.ShowDialog();
                }
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!Grd2.ReadOnly)
            {
                //e.DoDefault = false;
                if (e.ColIndex == 0 && TxtDocNo.Text.Length <= 0)
                {
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3 });
                }
                if (e.ColIndex == 13 && Sm.GetGrdStr(Grd2, e.RowIndex, 14).Length > 0)
                {
                    //e.DoDefault = false;
                    var f = new FrmProjectImplementationRBP(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 14);
                    f.ShowDialog();
                }
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd2.ReadOnly)
            {
                Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd2, e, BtnSave);
                ComputeTotalResource(); ComputeDirectCost(); ComputeRemunerationCost();ComputeIndirectCost();
            }
           
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //e.DoDefault = false;
                if (Sm.IsGrdColSelected(new int[] { 0, 1 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                }
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
            }
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if(e.ColIndex == 0)
                {
                    Sm.FormShowDialog(new FrmProjectImplementation3Dlg3(this));
                }
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //e.DoDefault = false;
                if (Sm.IsGrdColSelected(new int[] { 0, 3 }, e.ColIndex))
                {
                    if (e.ColIndex == 3) Sm.DteRequestEdit(Grd4, DteDocDt2, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd4, e.RowIndex);
                }
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd4, e, BtnSave);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProjectImplementation", "TblProjectImplementationHdr");

            var cml = new List<MySqlCommand>();
            

            cml.Add(SaveProjectImplementationHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) 
                    cml.Add(SaveProjectImplementationDtl(DocNo, Row));

            for (int Row = 0; Row < Grd2.Rows.Count ; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) 
                    cml.Add(SaveProjectImplementationDtl2(DocNo, Row));

            if (Grd3.Rows.Count >= 1)
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 0).Length > 0)
                        cml.Add(SaveProjectImplementationDtl3(DocNo, Row));

            if (Grd4.Rows.Count >= 1)
                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                        cml.Add(SaveProjectImplementationDtl4(DocNo, Row));

           for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0) 
                    cml.Add(SaveProjectImplementationDtl5(DocNo, Row));

            //upload file
            if (mIsPRJIAllowToUploadFile)
            {
                for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd7, Row, 1).Length > 0)
                    {
                        if (mIsPRJIAllowToUploadFile && Sm.GetGrdStr(Grd7, Row, 1).Length > 0 && Sm.GetGrdStr(Grd7, Row, 1) != "openFileDialog1")
                        {
                            if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd7, Row, 1))) return;
                        }
                    }
                }

                cml.Add(SaveUploadFile(DocNo));
                Sm.ExecCommands(cml);

                for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd7, Row, 1).Length > 0)
                    {
                        if (mIsPRJIAllowToUploadFile && Sm.GetGrdStr(Grd7, Row, 1).Length > 0 && Sm.GetGrdStr(Grd7, Row, 1) != "openFileDialog1")
                        {
                            UploadFile(DocNo, Row, Sm.GetGrdStr(Grd7, Row, 1));
                        }
                    }
                }
            }
            

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueProcessInd, "Process") ||
                Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false) ||
                IsTotalWeightNotValid()||
                IsSOContractAlreadyProcessed() ||
                IsSOContractCancelled() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid("I") ||
                (mIsPRJIUploadFileMandatory && IsUploadFileMandatory());
                //IsBobotPercentageNotValid();
        }

        private bool IsTotalWeightNotValid()
        {
            var SQL = new StringBuilder();

            decimal TotalWeight = 0m;
            TotalWeight = Sm.GetDecValue(TxtTotalWeight2.Text);
            if (TotalWeight != 100m)
            {
                Sm.StdMsg(mMsgType.Warning, "Total Weight% must be 100%. ");
                return true;
            }

            return false;
        }
        private bool IsSOContractCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblSOContractRevisionHdr A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOCDocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @Param ");
            SQL.AppendLine("And (B.CancelInd = 'Y' Or B.Status = 'C') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtSOContractDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This SO Contract is cancelled.");
                return true;
            }

            return false;
        }

        private bool IsSOContractAlreadyProcessed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblProjectImplementationHdr ");
            SQL.AppendLine("Where SOContractDocNo = @Param ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("And Status In ('A', 'O') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtSOContractDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This SO Contract document has already processed to Project Implementation #" + Sm.GetValue(SQL.ToString(), TxtSOContractDocNo.Text));
                TxtSOContractDocNo.Focus();
                return true;
            }

            return false;
        }

        private bool IsBobotPercentageNotValid()
        {

            decimal mPercent = 0m;
            bool mFlag = false;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    mPercent += Sm.GetGrdDec(Grd1, i, 7);
            }

            if (mPercent > 100)
            {
                for (int j = Grd1.Rows.Count-1; j >= 0; j--)
                {
                    if (Sm.GetGrdStr(Grd1, j, 1).Length > 0)
                    {
                        Grd1.Cells[j, 6].Value = Sm.GetGrdDec(Grd1, j, 6) - 0.00000001m;
                        Sm.StdMsg(mMsgType.Info, "Bobot adjusted. Please save again.");
                        mFlag = true;
                        break;
                    }
                }
            }
                
            return mFlag;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 WBS.");
                TcProjectImplementation.SelectTab("TpgWBS");
                return true;
            }
            if (Grd2.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Resource.");
                TcProjectImplementation.SelectTab("TpgResource");
                return true;
            }
            if (mIsPRJIUseWBS2)
            {
                if (Grd5.Rows.Count <= 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 WBS 2.");
                    TcProjectImplementation.SelectTab("TpgWBS2");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid(string mStateInd)
        {
            if (mStateInd == "I")
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Stage is empty.")) { TcProjectImplementation.SelectTab("TpgWBS"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Plan Start Date is empty.")) { TcProjectImplementation.SelectTab("TpgWBS"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Plan End Date is empty.")) { TcProjectImplementation.SelectTab("TpgWBS"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 5, true, "Price could not be empty or zero.")) { TcProjectImplementation.SelectTab("TpgWBS"); return true; }
                }

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.CompareDtTm(Sm.Left(Sm.GetGrdDate(Grd1, Row, 3), 8), Sm.Left(Sm.GetGrdDate(Grd1, Row, 4), 8)) > 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Plan end date could not be earlier than plan start date.");
                        TcProjectImplementation.SelectTab("TpgWBS");
                        Sm.FocusGrd(Grd1, Row, 4);
                        return true;
                    }
                }
                if (mIsPRJIUseWBS2)
                {
                    for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                    {
                        if (Sm.IsGrdValueEmpty(Grd5, Row, 1, false, "Stage is empty.")) { TcProjectImplementation.SelectTab("TpgWBS2"); return true; }
                        if (Sm.IsGrdValueEmpty(Grd5, Row, 3, false, "Task is empty.")) { TcProjectImplementation.SelectTab("TpgWBS2"); return true; }
                        if (Sm.IsGrdValueEmpty(Grd5, Row, 4, false, "Plan Start Date is empty.")) { TcProjectImplementation.SelectTab("TpgWBS2"); return true; }
                        if (Sm.IsGrdValueEmpty(Grd5, Row, 5, false, "Plan End Date is empty.")) { TcProjectImplementation.SelectTab("TpgWBS2"); return true; }
                        if (mIsWBS2SameAsWBS)
                        { 
                            if (Sm.IsGrdValueEmpty(Grd5, Row, 6, true, "Price could not be empty or zero.")) { TcProjectImplementation.SelectTab("TpgWBS2"); return true; }
                        }
                        else
                        { 
                            if (Sm.IsGrdValueEmpty(Grd5, Row, 7, true, "Weight % could not be empty or zero.")) { TcProjectImplementation.SelectTab("TpgWBS2"); return true; }
                        }
                    }

                    for (int r1 = 0; r1 < Grd5.Rows.Count - 1; r1++)
                    {
                        for (int r2 = (r1 + 1); r2 < Grd5.Rows.Count; r2++)
                        {
                            if ((Sm.GetGrdStr(Grd5, r1, 0) == Sm.GetGrdStr(Grd5, r2, 0))
                                &&
                                (Sm.GetGrdStr(Grd5, r1, 2) == Sm.GetGrdStr(Grd5, r2, 2))
                                )
                            {
                                Sm.StdMsg(mMsgType.Warning, "Duplicate task found : " + Sm.GetGrdStr(Grd5, r2, 3));
                                TcProjectImplementation.SelectTab("TpgWBS2");
                                Sm.FocusGrd(Grd5, r2, 3);
                                return true;
                            }
                        }
                    }

                    for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareDtTm(Sm.Left(Sm.GetGrdDate(Grd5, Row, 4), 8), Sm.Left(Sm.GetGrdDate(Grd5, Row, 5), 8)) > 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Plan end date could not be earlier than plan start date.");
                            TcProjectImplementation.SelectTab("TpgWBS2");
                            Sm.FocusGrd(Grd5, Row, 5);
                            return true;
                        }
                    }
                }

                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                { 
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Resource is empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 5, false, "Quantity 1 could not be empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 6, false, "Quantity 2 could not be empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 7, false, "Unit Price could not be empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 8, false, "Tax could not be empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 9, false, "Total Without Tax could not be empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 10, false, "Total Tax could not be empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 11, false, "Total With Tax could not be empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                }

                for (int r1 = 0; r1 < Grd2.Rows.Count - 1; r1++)
                {
                    for (int r2 = (r1 + 1); r2 < Grd2.Rows.Count; r2++)
                    {
                        if (Sm.GetGrdStr(Grd2, r1, 1) == Sm.GetGrdStr(Grd2, r2, 1))
                        {
                            Sm.StdMsg(mMsgType.Warning, "Duplicate resource found : " + Sm.GetGrdStr(Grd2, r2, 2));
                            TcProjectImplementation.SelectTab("TpgResource");
                            Sm.FocusGrd(Grd2, r2, 2);
                            return true;
                        }
                    }
                }
            }

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd3, Row, 0, false, "Remark is empty.")) { TcProjectImplementation.SelectTab("TpgIssue"); return true; }
                if (Sm.IsGrdValueEmpty(Grd3, Row, 1, false, "PIC is empty.")) { TcProjectImplementation.SelectTab("TpgIssue"); return true; }
            }

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd4, Row, 2, false, "Document is empty.")) { TcProjectImplementation.SelectTab("TpgDocument"); return true; }
            }

            for (int r1 = 0; r1 < Grd4.Rows.Count - 1; r1++)
            {
                for (int r2 = (r1 + 1); r2 < Grd4.Rows.Count; r2++)
                {
                    if (Sm.GetGrdStr(Grd4, r1, 1) == Sm.GetGrdStr(Grd4, r2, 1))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Duplicate document found : " + Sm.GetGrdStr(Grd4, r2, 2));
                        TcProjectImplementation.SelectTab("TpgDocument");
                        Sm.FocusGrd(Grd4, r2, 2);
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveProjectImplementationHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProjectImplementationHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status,ProcessInd, CancelInd,  SOContractDocNo, Achievement,  TotalResource, ");
            SQL.AppendLine(" RemunerationAmt, DirectCostAmt, TotalPrice, TotalBobot,  TotalPrice2, ");
            SQL.AppendLine(" IndirectCostAmt, RemunerationPerc, DirectCostPerc, IndirectCostPerc, CostPerc, TotalResourcePerc,  ");
            SQL.AppendLine(" TotalBobot2,  TotalAchievement, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'O','D', 'N',  @SOContractDocNo, 0, @TotalResource, @RemunerationAmt, ");
            SQL.AppendLine(" @DirectCostAmt, @TotalPrice, @TotalBobot,  @TotalPrice2, ");
            SQL.AppendLine(" @IndirectCostAmt, @RemunerationPerc, @DirectCostPerc, @IndirectCostPerc, @CostPerc, @TotalResourcePerc, ");
            SQL.AppendLine(" @TotalBobot2,@TotalAchievement, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@LOPDocNo", TxtLOPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@TotalResource", Sm.GetDecValue(TxtTotalResource.Text));
            Sm.CmParam<Decimal>(ref cm, "@RemunerationAmt", Sm.GetDecValue(TxtRemunerationCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCostAmt", Sm.GetDecValue(TxtDirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalPrice", Sm.GetDecValue(TxtTotalPrice.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalBobot", Sm.GetDecValue(TxtTotalWeight.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalPrice2", Sm.GetDecValue(TxtTotalPrice2.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalBobot2", Sm.GetDecValue(TxtTotalWeight2.Text));
            Sm.CmParam<Decimal>(ref cm, "@IndirectCostAmt", Sm.GetDecValue(TxtIndirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@RemunerationPerc", Sm.GetDecValue(TxtRemunerationPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCostPerc", Sm.GetDecValue(TxtDirectPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@IndirectCostPerc", Sm.GetDecValue(TxtIndirectPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@CostPerc", Sm.GetDecValue(TxtCostPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalResourcePerc", Sm.GetDecValue(TxtTotalResourcePerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalAchievement", Sm.GetDecValue(TxtAchievement.Text));
            return cm;
        }

        private MySqlCommand SaveProjectImplementationDtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationDtl(DocNo, DNo, StageCode, TaskCode, PlanStartDt, PlanEndDt, Description, ");
            SQLDtl.AppendLine("Amt, Bobot, BobotPercentage, SettledInd, EstimatedAmt, SettledAmt, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @StageCode, @TaskCode, @PlanStartDt, @PlanEndDt, @Description, ");
            SQLDtl.AppendLine("@Amt, 0, @BobotPercentage, 'N', 0, 0, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@StageCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParamDt(ref cm, "@PlanStartDt", Sm.GetGrdDate(Grd1, Row, 3));
            Sm.CmParamDt(ref cm, "@PlanEndDt", Sm.GetGrdDate(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@BobotPercentage", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationDtl2(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationDtl2(DocNo, DNo, ResourceItCode, Sequence, Remark, Qty1, Qty2, UPrice, Tax, TotalWithoutTax, TotalTax, Amt, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @ResourceItCode, @Sequence, @Remark, @Qty1, @Qty2, @UPrice, @Tax, @TotalWithoutTax, @TotalTax, @Amt, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ResourceItCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Qty1", Sm.GetGrdDec(Grd2, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Sm.GetGrdDec(Grd2, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@TotalWithoutTax", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@TotalTax", Sm.GetGrdDec(Grd2, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 11));
            Sm.CmParam<String>(ref cm, "@Sequence", Sm.GetGrdStr(Grd2, Row, 15));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);


            return cm;
        }

        private MySqlCommand SaveProjectImplementationDtl3(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationDtl3(DocNo, DNo, Remark, PIC, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @Remark, @PIC, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 0));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationDtl4(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationDtl4(DocNo, DNo, DocCode, Dt, Remark, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @DocCode, @Dt, @Remark, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DocCode", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetGrdDate(Grd4, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd4, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationDtl5(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationDtl5(DocNo, DNo, StageCode, TaskCode, PlanStartDt, PlanEndDt, ");
            SQLDtl.AppendLine("Amt, BobotPercentage,  ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @StageCode, @TaskCode, @PlanStartDt, @PlanEndDt, ");
            SQLDtl.AppendLine("@Amt, @BobotPercentage,  ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@StageCode", Sm.GetGrdStr(Grd5, Row, 0));
            Sm.CmParam<String>(ref cm, "@TaskCode", Sm.GetGrdStr(Grd5, Row, 2));
            Sm.CmParamDt(ref cm, "@PlanStartDt", Sm.GetGrdDate(Grd5, Row, 4));
            Sm.CmParamDt(ref cm, "@PlanEndDt", Sm.GetGrdDate(Grd5, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd5, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@BobotPercentage", Sm.GetGrdDec(Grd5, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
            
            Cursor.Current = Cursors.WaitCursor;
            
            var cml = new List<MySqlCommand>();
            //ComputeEstimatedAmt();

            cml.Add(EditProjectImplementationHdr());
            if (MeeCancelReason.Text.Length <= 0)
            {
                if (Sm.GetLue(LueProcessInd) == "D")
                {
                    cml.Add(DeleteProjectImplementationDtl125());
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                            cml.Add(SaveProjectImplementationDtl(TxtDocNo.Text, Row));

                    for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                            cml.Add(SaveProjectImplementationDtl2(TxtDocNo.Text, Row));

                    for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                            cml.Add(SaveProjectImplementationDtl5(TxtDocNo.Text, Row));

                    for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                            cml.Add(EditProjectImplementationDtl2(TxtDocNo.Text, Row));
                }

                cml.Add(DeleteProjectImplementationDtl34());

                if (Grd3.Rows.Count >= 1)
                    for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd3, Row, 0).Length > 0)
                            cml.Add(SaveProjectImplementationDtl3(TxtDocNo.Text, Row));

                if (Grd4.Rows.Count >= 1)
                    for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                            cml.Add(SaveProjectImplementationDtl4(TxtDocNo.Text, Row));

                if (Sm.GetLue(LueProcessInd) == "F")
                {
                    cml.Add(SaveDocApproval(TxtDocNo.Text));

                    // save Project Implementation Revision
                    //command by dita 10/02/2023 karna mnet belum ada requirement untuk perombakan prji revision, sehingga menyebabkan bug
                    //cml.Add(SaveProjectImplementationRevisionHdr(TxtDocNo.Text));

                    //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    //    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    //        cml.Add(SaveProjectImplementationRevisionDtl(TxtDocNo.Text, Row));

                    //for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    //    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                    //        cml.Add(SaveProjectImplementationRevisionDtl2(TxtDocNo.Text, Row));

                    //if (Grd3.Rows.Count >= 1)
                    //    for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    //        if (Sm.GetGrdStr(Grd3, Row, 0).Length > 0)
                    //            cml.Add(SaveProjectImplementationRevisionDtl3(TxtDocNo.Text, Row));

                    //if (Grd4.Rows.Count >= 1)
                    //    for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                    //        if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                    //            cml.Add(SaveProjectImplementationRevisionDtl4(TxtDocNo.Text, Row));

                    //for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                    //    if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                    //        cml.Add(SaveProjectImplementationRevisionDtl5(TxtDocNo.Text, Row));
                }
            }

            if (MeeCancelReason.Text.Length > 0)
            {
                cml.Add(UpdateWBSHdr(TxtDocNo.Text));
            }

            //EDIT UPLOAD

            if (mIsPRJIAllowToUploadFile)
            {
                for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd7, Row, 1).Length > 0)
                    {
                        if (mIsPRJIAllowToUploadFile && Sm.GetGrdStr(Grd7, Row, 1).Length > 0 && Sm.GetGrdStr(Grd7, Row, 1) != "openFileDialog1")
                        {
                            if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd7, Row, 1))) return;
                        }
                    }
                }

                cml.Add(SaveUploadFile(TxtDocNo.Text));
                Sm.ExecCommands(cml);

                for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd7, Row, 1).Length > 0)
                    {
                        if (mIsPRJIAllowToUploadFile && Sm.GetGrdStr(Grd7, Row, 1).Length > 0 && Sm.GetGrdStr(Grd7, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd7, Row, 4).Length == 0)
                        {
                            UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd7, Row, 1));
                        }
                    }
                }
            }

            

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsLueEmpty(LueProcessInd, "Process") ||
                IsTotalWeightNotValid()||
                //IsDataAlreadyFinal() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyInvoiced() ||
                IsDataAlreadyInvoiced2() ||
                (mPRJISourceForRBP != "1" && IsDataAlreadyRBP()) ||
                IsGrdValueNotValid("E") ||
                (mIsPRJIUploadFileMandatory && IsUploadFileMandatory());
               // IsBobotPercentageNotValid()
                ;
        }

        private bool IsDataAlreadyRBP()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblProjectImplementationRBPHdr A ");
            SQL.AppendLine("Inner Join TblProjectImplementationDtl2 B On A.PRJIDocNo = B.DocNo And A.ResourceItCode = B.ResourceItCode ");
            SQL.AppendLine("    And A.PRJIDocNo = @Param ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblParameter C On C.Parcode = 'PRJISourceForRBP' And Parvalue Is Not Null And Parvalue = '2' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already created on RBP#" + Sm.GetValue(SQL.ToString(), TxtDocNo.Text));
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyFinal()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblProjectImplementationHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And ProcessInd = 'F' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already Final.");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyInvoiced()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblProjectImplementationDtl ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And InvoicedInd = 'Y' ");
            SQL.AppendLine("Limit 1; ");

            if(Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "At least one of this WBS data has been invoiced.");
                TcProjectImplementation.SelectTab("TpgWBS");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyInvoiced2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblProjectImplementationDtl5 ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And InvoicedInd = 'Y' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "At least one of this WBS2 data has been invoiced.");
                TcProjectImplementation.SelectTab("TpgWBS2");
                return true;
            }

            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblProjectImplementationHdr ");
            SQL.AppendLine("Where (CancelInd='Y' Or Status = 'C') And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditProjectImplementationHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProjectImplementationHdr Set ");
            SQL.AppendLine(" ProcessInd=@ProcessInd,CancelReason=@CancelReason, CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(), ");
            SQL.AppendLine(" TotalResource=@TotalResource,TotalPrice=@TotalPrice, TotalBobot=@TotalBobot, ");
            SQL.AppendLine(" RemunerationAmt=@RemunerationAmt, DirectCostAmt=@DirectCostAmt, ");
            SQL.AppendLine(" TotalPrice2=@TotalPrice2, TotalBobot2=@TotalBobot2,  ");
            SQL.AppendLine(" IndirectCostAmt=@IndirectCostAmt, RemunerationPerc=@RemunerationPerc, DirectCostPerc=@DirectCostPerc, IndirectCostPerc=@IndirectCostPerc, CostPerc=@CostPerc ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status <> 'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@TotalResource", Sm.GetDecValue(TxtTotalResource.Text));
            Sm.CmParam<Decimal>(ref cm, "@RemunerationAmt", Sm.GetDecValue(TxtRemunerationCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCostAmt", Sm.GetDecValue(TxtDirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalPrice", Sm.GetDecValue(TxtTotalPrice.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalBobot", Sm.GetDecValue(TxtTotalWeight.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalPrice2", Sm.GetDecValue(TxtTotalPrice2.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalBobot2", Sm.GetDecValue(TxtTotalWeight2.Text));
            Sm.CmParam<Decimal>(ref cm, "@IndirectCostAmt", Sm.GetDecValue(TxtIndirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@RemunerationPerc", Sm.GetDecValue(TxtRemunerationPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCostPerc", Sm.GetDecValue(TxtDirectPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@IndirectCostPerc", Sm.GetDecValue(TxtIndirectPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@CostPerc", Sm.GetDecValue(TxtCostPerc.Text));

            return cm;
        }

        private MySqlCommand DeleteProjectImplementationDtl34()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblProjectImplementationDtl3 Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ( Select DocNo From TblProjectImplementationHdr Where DocNo = @DocNo And CancelInd = 'N' ); ");

            SQL.AppendLine("Delete From TblProjectImplementationDtl4 Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ( Select DocNo From TblProjectImplementationHdr Where DocNo = @DocNo And CancelInd = 'N' ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }

        private MySqlCommand DeleteProjectImplementationDtl125()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblProjectImplementationDtl Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ( Select DocNo From TblProjectImplementationHdr Where DocNo = @DocNo And CancelInd = 'N' ); ");

            SQL.AppendLine("Delete From TblProjectImplementationDtl2 Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ( Select DocNo From TblProjectImplementationHdr Where DocNo = @DocNo And CancelInd = 'N' ); ");

            SQL.AppendLine("Delete From TblProjectImplementationDtl5 Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ( Select DocNo From TblProjectImplementationHdr Where DocNo = @DocNo And CancelInd = 'N' ); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }

        private MySqlCommand UpdateWBSHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblWBSHdr Set ");
            SQL.AppendLine("    PRJIDocNo = null, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where PRJIDocNo = @DocNo ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblProjectImplementationHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And CancelInd = 'Y' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='ProjectImplementation' ");
            if (mIsProjectImplementationApprovalBySiteMandatory)
            {
                SQL.AppendLine("And T.SiteCode Is Not Null ");
                SQL.AppendLine("And T.SiteCode In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select SiteCode ");
                SQL.AppendLine("    From TblLOPHdr ");
                SQL.AppendLine("    Where DocNo = @LOPDocNo ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblProjectImplementationHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And DocType = 'ProjectImplementation' ");
            SQL.AppendLine(");     ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LOPDocNo", TxtLOPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProjectImplementationRevisionHdr(DocNo, SOCRDocNo, PRJIDocNo, ");
            SQL.AppendLine("DocDt, Status, TotalResource, TotalPrice, TotalBobot, TotalPersentase, ");
            SQL.AppendLine("RemunerationAmt, DirectCostAmt, PPNAmt, ExclPPNAmt, PPhAmt, ExclPPNPPhAmt, ");
            SQL.AppendLine("ExclPPNPPhPercentage, ExclPPNPercentage, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, @SOCRDocNo, @PRJIDocNo, ");
            SQL.AppendLine("A.DocDt, 'O', A.TotalResource, A.TotalPrice, A.TotalBobot, A.TotalPersentase, ");
            SQL.AppendLine("A.RemunerationAmt, A.DirectCostAmt, A.PPNAmt, A.ExclPPNAmt, A.PPhAmt, A.ExclPPNPPhAmt, ");
            SQL.AppendLine("A.ExclPPNPPhPercentage, A.ExclPPNPercentage, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Where A.DocNo = @PRJIDocNo ");
            SQL.AppendLine("And A.ProcessInd = 'F' ");
            SQL.AppendLine("And A.Status = 'A'; ");

            // update status jadi A, kalau ga ada PRJI yang perlu di approve
            SQL.AppendLine("Update TblProjectImplementationRevisionHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='ProjectImplementation' And DocNo=@PRJIDocNo ");
            SQL.AppendLine("); ");

            //SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
            //SQL.AppendLine("From TblDocApprovalSetting ");
            //SQL.AppendLine("Where DocType='ProjectImplementationRev'; ");            

            //SQL.AppendLine("Update TblProjectImplementationRevisionHdr Set Status='A' ");
            //SQL.AppendLine("Where DocNo=@DocNo ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select DocNo From TblDocApproval ");
            //SQL.AppendLine("    Where DocType='ProjectImplementationRev' And DocNo=@DocNo ");
            //SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo, "/0001"));
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SOCRDocNo", TxtSOContractDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionDtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRevisionDtl(DocNo, DNo, StageCode, TaskCode, PlanStartDt, PlanEndDt, ");
            SQLDtl.AppendLine("Bobot, BobotPercentage, Amt, EstimatedAmt, SettledAmt, SettledInd, SettleDt, InvoicedInd, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Select @DocNo, @DNo, @StageCode, @TaskCode, @PlanStartDt, @PlanEndDt, ");
            SQLDtl.AppendLine("@Bobot, @BobotPercentage, @Amt, @EstimatedAmt, @SettledAmt, 'N', NULL, 'N', @CreateBy, CurrentDateTime() ");
            SQLDtl.AppendLine("From TblProjectImplementationHdr A ");
            SQLDtl.AppendLine("Where A.DocNo = @PRJIDocNo ");
            SQLDtl.AppendLine("And A.ProcessInd = 'F' ");
            SQLDtl.AppendLine("And A.Status = 'A'; ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo, "/0001"));
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@StageCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@TaskCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParamDt(ref cm, "@PlanStartDt", Sm.GetGrdDate(Grd1, Row, 4));
            Sm.CmParamDt(ref cm, "@PlanEndDt", Sm.GetGrdDate(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Bobot", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@BobotPercentage", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@EstimatedAmt", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@SettledAmt", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionDtl2(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRevisionDtl2(DocNo, DNo, Sequence, ResourceItCode, Remark, Qty1, Qty2, UPrice, Tax, TotalWithoutTax, TotalTax, Amt, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Select @DocNo, @DNo, @Sequence, @ResourceItCode, @Remark, @Qty1, @Qty2, @UPrice, @Tax, @TotalWithoutTax, @TotalTax, @Amt, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime() ");
            SQLDtl.AppendLine("From TblProjectImplementationHdr A ");
            SQLDtl.AppendLine("Where A.DocNo = @PRJIDocNo ");
            SQLDtl.AppendLine("And A.ProcessInd = 'F' ");
            SQLDtl.AppendLine("And A.Status = 'A'; ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo, "/0001"));
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ResourceItCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Qty1", Sm.GetGrdDec(Grd2, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Sm.GetGrdDec(Grd2, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@TotalWithoutTax", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@TotalTax", Sm.GetGrdDec(Grd2, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 11));
            Sm.CmParam<String>(ref cm, "@Sequence", Sm.GetGrdStr(Grd2, Row, 15));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionDtl3(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRevisionDtl3(DocNo, DNo, Remark, PIC, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Select @DocNo, @DNo, @Remark, @PIC, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime() ");
            SQLDtl.AppendLine("From TblProjectImplementationHdr A ");
            SQLDtl.AppendLine("Where A.DocNo = @PRJIDocNo ");
            SQLDtl.AppendLine("And A.ProcessInd = 'F' ");
            SQLDtl.AppendLine("And A.Status = 'A'; ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo, "/0001"));
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 0));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionDtl4(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRevisionDtl4(DocNo, DNo, DocCode, Dt, Remark, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Select @DocNo, @DNo, @DocCode, @Dt, @Remark, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime() ");
            SQLDtl.AppendLine("From TblProjectImplementationHdr A ");
            SQLDtl.AppendLine("Where A.DocNo = @PRJIDocNo ");
            SQLDtl.AppendLine("And A.ProcessInd = 'F' ");
            SQLDtl.AppendLine("And A.Status = 'A'; ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo, "/0001"));
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DocCode", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetGrdDate(Grd4, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd4, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionDtl5(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRevisionDtl5(DocNo, DNo, StageCode, TaskCode, PlanStartDt, PlanEndDt, ");
            SQLDtl.AppendLine("Bobot, BobotPercentage, ");
            if(mIsWBS2SameAsWBS)
                SQLDtl.AppendLine("Amt, EstimatedAmt, ");
            SQLDtl.AppendLine("SettledAmt, SettledInd, SettleDt, InvoicedInd, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Select @DocNo, @DNo, @StageCode, @TaskCode, @PlanStartDt, @PlanEndDt, ");
            SQLDtl.AppendLine("@Bobot, @BobotPercentage, ");
            if (mIsWBS2SameAsWBS)
                SQLDtl.AppendLine("@Amt, @EstimatedAmt, ");
            SQLDtl.AppendLine(" @SettledAmt, 'N', NULL, 'N', @CreateBy, CurrentDateTime() ");
            SQLDtl.AppendLine("From TblProjectImplementationHdr A ");
            SQLDtl.AppendLine("Where A.DocNo = @PRJIDocNo ");
            SQLDtl.AppendLine("And A.ProcessInd = 'F' ");
            SQLDtl.AppendLine("And A.Status = 'A'; ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo, "/0001"));
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@StageCode", Sm.GetGrdStr(Grd5, Row, 0));
            Sm.CmParam<String>(ref cm, "@TaskCode", Sm.GetGrdStr(Grd5, Row, 2));
            Sm.CmParamDt(ref cm, "@PlanStartDt", Sm.GetGrdDate(Grd5, Row, 4));
            Sm.CmParamDt(ref cm, "@PlanEndDt", Sm.GetGrdDate(Grd5, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd5, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Bobot", Sm.GetGrdDec(Grd5, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@BobotPercentage", Sm.GetGrdDec(Grd5, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@EstimatedAmt", Sm.GetGrdDec(Grd5, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@SettledAmt", Sm.GetGrdDec(Grd5, Row, 12));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditProjectImplementationDtl2(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Update TblProjectImplementationDtl2 Set ");
            SQLDtl.AppendLine(" Sequence=@Sequence ");
            SQLDtl.AppendLine("Where DocNo = @DocNo And DNo = @DNo; ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Sequence", Sm.GetGrdStr(Grd2, Row, 15));

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ComputeSettledAmt();
                ShowProjectImplementationHdr(DocNo);
                ShowProjectImplementationDtl(DocNo);
                ShowProjectImplementationDtl2(DocNo);
                ShowProjectImplementationDtl3(DocNo);
                ShowProjectImplementationDtl4(DocNo);
                ShowProjectImplementationDtl5(DocNo);
                CheckDueDate();
                Sm.ShowDocApproval(DocNo, "ProjectImplementation", ref Grd6);
                if(mIsPRJIAllowToUploadFile) ShowUploadFile(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProjectImplementationHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.PPNInd, A.SOContractDocNo, A.Achievement, A.Remark, ");
            SQL.AppendLine("B.BOQDocNo, C.LOPDocNo, C.CtCode, C.CtContactPersonName, C.SPCode, D.ProjectName, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, A.ProcessInd, A.TotalResource, ");
            SQL.AppendLine(" A.RemunerationAmt, A.DirectCostAmt, A.PPNAmt, A.ExclPPNAmt, A.PPhAmt, A.ExclPPNPPhAmt, A.ExclPPNPPhPercentage, A.ExclPPNPercentage, ");
            SQL.AppendLine(" E.OptDesc, ( A.RemunerationAmt + A.DirectCostAmt) As Total, ( A.PPNAmt + A.ExclPPNAmt) As ContractAmt, A.TotalPrice, A.TotalBobot, A.TotalPersentase, B.ProjectCode2, ");
            SQL.AppendLine(" A.TotalPrice2, A.TotalBobot2, A.TotalPersentase2, A.TotalAchievement, D.ProjectResource, A.PPNCode, A.PPhCode, ");
            SQL.AppendLine(" B.Amt ContractAmtBefTax, A.TotalRev, A.TotalEstimatedCost, A.IndirectCostAmt, A.RemunerationPerc, A.DirectCostPerc, A.IndirectCostPerc, A.CostPerc, A.TotalResourcePerc, H.UserCode PICSales ");
            if (mIsPSModuleShowApproverRemarkInfo)
                SQL.AppendLine(", G.Remark as ApprovalRemark ");
            else
                SQL.AppendLine(", Null as ApprovalRemark ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr A1 On A.SOContractDocNo = A1.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A1.SOCDocNo = B.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr C On B.BOQDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblLopHdr D On C.LOPDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblOption E On D.ProjectType = E.OptCode And OptCat = 'ProjectType' ");

            if (mIsPSModuleShowApproverRemarkInfo)
            {
                SQL.AppendLine("Left Join (   ");
                SQL.AppendLine("	Select Max(T2.`Level`) LV, T1.DocNo   ");
                SQL.AppendLine("	From TblDocApproval T1   ");
                SQL.AppendLine("	Inner Join TblDocApprovalSetting T2 On T1.DocType = T2.DocType And T1.ApprovalDNo = T2.DNo   ");
                SQL.AppendLine("	Where T1.DocType = 'ProjectImplementation' And T1.DocNo = @DocNo ");
                SQL.AppendLine("	And (T1.UserCode Is Not Null And T1.UserCode <> '') ");
                SQL.AppendLine("	And T1.Status Is Not Null ");
                SQL.AppendLine("	GROUP BY T1.DocNo ");
                SQL.AppendLine(") F ON A.DocNo = F.DocNo ");
                SQL.AppendLine("Left Join (   ");
                SQL.AppendLine("   Select T2.`Level` As LV, T1.DocNo, T1.Remark, T3.UserName, T1.UserCode ");
                SQL.AppendLine("   From TblDocApproval T1   ");
                SQL.AppendLine("   Inner Join TblDocApprovalSetting T2 On T1.DocType = T2.DocType And T1.ApprovalDNo = T2.DNo ");
                SQL.AppendLine("   Inner Join TblUser T3 On T1.UserCode = T3.UserCode  ");
                SQL.AppendLine("   Where T1.DocType = 'ProjectImplementation' And T1.DocNo = @DocNo ");
                SQL.AppendLine("	And (T1.UserCode Is Not Null And T1.UserCode <> '')   ");
                SQL.AppendLine("	And T1.Status Is Not Null ");
                SQL.AppendLine(") G On F.DocNo = G.DocNo And F.LV = G.LV ");
            }
            SQL.AppendLine("Left Join TblUser H On D.PICCode = H.UserCode ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "SOContractDocNo", "Achievement",  
                    
                    //6-10
                    "Remark", "BOQDocNo", "LOPDocNo", "CtCode", "CtContactPersonName",   
                    
                    //11-15
                    "SPCode", "StatusDesc", "ProcessInd", "ProjectName", "TotalResource", 

                    //16-20
                    "RemunerationAmt", "DirectCostAmt", "PPNAmt", "ExclPPNAmt", "PPhAmt", 
                    
                    //21-25
                    "ExclPPNPPhAmt", "ExclPPNPPhPercentage", "ExclPPNPercentage", "OptDesc", "Total",

                    //26-30
                    "ContractAmt", "TotalPrice", "TotalBobot", "TotalPersentase", "ProjectCode2",

                    //31-35
                    "TotalPrice2", "TotalBobot2", "TotalPersentase2", "TotalAchievement", "PPNInd",

                    //36-40
                    "ProjectResource", "PPNCode", "ApprovalRemark", "PPhCode", "ContractAmtBefTax", 

                    //40-45
                    "TotalRev", "TotalEstimatedCost", "IndirectCostAmt", "RemunerationPerc", "DirectCostPerc", 
                    
                    //46-49
                    "IndirectCostPerc", "CostPerc", "PICSales", "TotalResourcePerc"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                    TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtAchievement.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[7]);
                    TxtLOPDocNo.EditValue = Sm.DrStr(dr, c[8]);
                    Sl.SetLueCtCode(ref LueCtCode);
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[9]));
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.DrStr(dr, c[9]));
                    Sm.SetLue(LueCtContactPersonName, Sm.DrStr(dr, c[10]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[12]);
                    Sm.SetLue(LueProcessInd, Sm.DrStr(dr, c[13]));
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[14]);
                    TxtTotalResource.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[15]), 0);
                    TxtRemunerationCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                    TxtDirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]), 0);
                    TxtType.EditValue = Sm.DrStr(dr, c[24]);
                    //TxtTotalResource.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[25]), 0);
                    TxtTotalPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[27]), 0);
                    TxtTotalWeight.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[28]), 2);
                    TxtProjectCode.EditValue = Sm.DrStr(dr, c[30]);
                    TxtTotalPrice2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[31]), 0);
                    TxtTotalWeight2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[32]), 0);
                    mProjectResourceCode = Sm.DrStr(dr, c[36]);
                    MeeApprovalRemark.EditValue = Sm.DrStr(dr, c[38]);
                    TxtContractAmtBefTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[40]), 0);
                    TxtContractAmtBefTax2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[40]), 0);
                    TxtTotalRev.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[41]), 0);
                    TxtEstimatedCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[42]), 0);
                    TxtIndirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[43]), 0);
                    TxtRemunerationPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[44]), 0);
                    TxtDirectPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[45]), 0);
                    TxtIndirectPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[46]), 0);
                    TxtCostPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[47]), 0);
                    TxtPICSales.EditValue = Sm.DrStr(dr, c[48]);
                    TxtTotalResourcePerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[49]), 0);

                }, true
            );
        }

        private void ShowProjectImplementationDtl(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select A.StageCode, B.StageName, A.TaskCode, C.TaskName, ");
            SQLDtl.AppendLine("A.PlanStartDt, A.PlanEndDt, A.Bobot, A.BobotPercentage, A.SettledInd,  D.Remark, A.Amt, ");
            SQLDtl.AppendLine("A.EstimatedAmt, A.SettledAmt, D.SettleDt, A.Description, A.SettledWeightPerc, A.SettledRevAmt, A.EstimatedCost, D.ProjectDeliveryDocNo ");
            SQLDtl.AppendLine("From TblProjectImplementationDtl A ");
            SQLDtl.AppendLine("Inner Join TblProjectStage B On A.StageCode = B.StageCode And A.DocNo = @DocNo ");
            SQLDtl.AppendLine("Left Join TblProjectTask C On A.TaskCode = C.TaskCode ");
            SQLDtl.AppendLine("Left Join ");
            SQLDtl.AppendLine("( ");
            SQLDtl.AppendLine("    Select Group_Concat(Distinct X1.DocNo) ProjectDeliveryDocNo, ");
            SQLDtl.AppendLine("    Group_Concat(Distinct Date_Format(X1.DocDt, '%d/%m/%Y')) SettleDt, ");
            SQLDtl.AppendLine("    Group_Concat(Distinct IfNull(X1.Remark, '')) Remark, ");
            SQLDtl.AppendLine("    X1.PRJIDocNo, X3.StageCode ");
            SQLDtl.AppendLine("    From TblProjectDeliveryHdr X1 ");
            SQLDtl.AppendLine("    Inner Join TblProjectDeliveryDtl X2 On X1.DocNo = X2.DocNo ");
            SQLDtl.AppendLine("        And X1.Status In('A') ");
            SQLDtl.AppendLine("        And X1.CancelInd = 'N' ");
            SQLDtl.AppendLine("        And X1.PRJIDocNo = @DocNo ");
            SQLDtl.AppendLine("    Inner Join TblProjectImplementationDtl5 X3 On X1.PRJIDocNo = X3.DocNo And X2.PRJIDNo = X3.DNo ");
            SQLDtl.AppendLine("    Inner Join TblProjectImplementationDtl X4 On X4.DocNo = @DocNo And X4.SettledInd = 'Y' And X3.StageCode = X4.StageCode ");
            SQLDtl.AppendLine("    Group By X1.PRJIDocNo, X3.StageCode ");
            SQLDtl.AppendLine(") D On A.DocNo = D.PRJIDocNo And A.StageCode = D.StageCode ");
            SQLDtl.AppendLine("Order By A.DNo; ");

            var cm1 = new MySqlCommand();
            Sm.CmParam<String>(ref cm1, "@DocNo", DocNo);

            SetGrd();

            Sm.ShowDataInGrid(
                ref Grd1, ref cm1, SQLDtl.ToString(),
                new string[] 
                { 
                    "StageCode", 
                    "StageName", "Description", "PlanStartDt", "PlanEndDt","Amt",
                    "BobotPercentage", "SettledInd", "Remark",  "SettledWeightPerc", "SettledRevAmt",
                    "EstimatedCost","ProjectDeliveryDocNo", "SettleDt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] {5, 6, 9, 10, 11 });

        }

        private void ShowProjectImplementationDtl2(string DocNo)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Select A.ResourceItCode, A.Sequence, B.ItName,C.ItGrpCode, C.ItGrpName, A.Remark, A.Qty1, A.Qty2, A.UPrice, A.Tax, A.TotalWithoutTax, A.TotalTax, A.Amt ");
            SQLDtl2.AppendLine(", D.DocNo As RBPDocNo ");
            SQLDtl2.AppendLine("From TblProjectImplementationDtl2 A ");
            SQLDtl2.AppendLine("Inner Join TblItem B On A.ResourceItCode = B.ItCode ");
            SQLDtl2.AppendLine("Left Join TblItemGroup C On B.ItGrpCode = C.ItGrpCode ");
            SQLDtl2.AppendLine("Left Join TblProjectImplementationRBPHdr D On A.DocNo = D.PRJIDocNo And D.ResourceItCode = A.ResourceItCode And D.CancelInd = 'N' ");
            SQLDtl2.AppendLine("Where A.DocNo = @DocNo ");
            SQLDtl2.AppendLine("Order By A.DNo; ");

            var cm2 = new MySqlCommand();
            Sm.CmParam<String>(ref cm2, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm2, SQLDtl2.ToString(),
                new string[] 
                { 
                    "ResourceItCode", 
                    "ItName", "ItGrpName", "Remark", "Qty1", "Qty2", 
                    "UPrice", "Tax", "TotalWithoutTax", "TotalTax", "Amt",
                    "ItGrpCode", "RBPDocNo", "Sequence"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11 });
               
        }

        private void ShowProjectImplementationDtl3(string DocNo)
        {
            var SQLDtl3 = new StringBuilder();

            SQLDtl3.AppendLine("Select Remark, PIC ");
            SQLDtl3.AppendLine("From TblProjectImplementationDtl3 ");
            SQLDtl3.AppendLine("Where DocNo = @DocNo ");
            SQLDtl3.AppendLine("Order By DNo; ");

            var cm3 = new MySqlCommand();
            Sm.CmParam<String>(ref cm3, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd3, ref cm3, SQLDtl3.ToString(),
                new string[] 
                { 
                    "Remark", 
                    "PIC"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowProjectImplementationDtl4(string DocNo)
        {
            var SQLDtl4 = new StringBuilder();

            SQLDtl4.AppendLine("Select A.DocCode, B.DocName, A.Dt, A.Remark ");
            SQLDtl4.AppendLine("From TblProjectImplementationDtl4 A ");
            SQLDtl4.AppendLine("Left Join TblProjectDocument B On A.DocCode = B.DocCode ");
            SQLDtl4.AppendLine("Where A.DocNo = @DocNo ");
            SQLDtl4.AppendLine("Order By A.DNo; ");

            var cm4 = new MySqlCommand();
            Sm.CmParam<String>(ref cm4, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd4, ref cm4, SQLDtl4.ToString(),
                new string[] 
                { 
                    "DocCode", 
                    "DocName", "Dt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {       
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 1);
        }

        private void ShowProjectImplementationDtl5(string DocNo)
        {
            var SQLDtl5 = new StringBuilder();

            SQLDtl5.AppendLine("Select A.StageCode, B.StageName, A.TaskCode, C.TaskName, ");
            SQLDtl5.AppendLine("A.PlanStartDt, A.PlanEndDt, A.Bobot, A.BobotPercentage, A.SettledInd, A.Remark, ");
            SQLDtl5.AppendLine("A.Amt, A.EstimatedAmt, A.SettledAmt, A.SettleDt, ");
            SQLDtl5.AppendLine("A.SettledRevAmt, A.EstimatedCost, D.ProjectDeliveryDocNo ");
            SQLDtl5.AppendLine("From TblProjectImplementationDtl5 A ");
            SQLDtl5.AppendLine("Inner Join TblProjectStage B On A.StageCode = B.StageCode And A.DocNo = @DocNo ");
            SQLDtl5.AppendLine("Inner Join TblProjectTask C On A.TaskCode = C.TaskCode ");
            SQLDtl5.AppendLine("Left Join ");
            SQLDtl5.AppendLine("( ");
            SQLDtl5.AppendLine("    Select Group_Concat(Distinct T1.DocNo) ProjectDeliveryDocNo, T2.PRJIDocNo, T2.PRJIDNo ");
            SQLDtl5.AppendLine("    From TblProjectDeliveryHdr T1 ");
            SQLDtl5.AppendLine("    Inner Join TblProjectDeliveryDtl T2 On T1.DocNo = T2.DocNo ");
            SQLDtl5.AppendLine("        And T1.CancelInd = 'N' ");
            SQLDtl5.AppendLine("        And T1.Status In('A') ");
            SQLDtl5.AppendLine("        And T2.PRJIDocNo = @DocNo ");
            SQLDtl5.AppendLine("    Group By T2.PRJIDocNo, T2.PRJIDNo ");
            SQLDtl5.AppendLine(") D On A.DocNo = D.PRJIDocNo And A.DNo = D.PRJIDNo ");
            SQLDtl5.AppendLine("Order By A.DNo; ");

            var cm5 = new MySqlCommand();
            Sm.CmParam<String>(ref cm5, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd5, ref cm5, SQLDtl5.ToString(),
                new string[] 
                { 
                    "StageCode", 
                    "StageName", "TaskCode", "TaskName", "PlanStartDt", "PlanEndDt",
                    "Amt","BobotPercentage", "SettledInd", "Remark","SettledRevAmt",
                    "EstimatedCost", "ProjectDeliveryDocNo", "SettleDt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 13);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 7, 8 });
            if (mIsWBS2SameAsWBS) Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 6, 7, 10, 11});

        }

        #endregion

        #endregion

        #region Events

        #region Button Clicks

        private void BtnImportWBS_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ResultWBS>();

                ClearGrdWBS1();
                try
                {
                    ProcessWBS1(ref l);
                    if (l.Count > 0)
                    {
                        ProcessWBS2(ref l);
                        ProcessWBS3(ref l);
                        ProcessWBS4(ref l);
                    }
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    l.Clear();
                    Cursor.Current = Cursors.Default;
                }
            }
        }


        private void ProcessWBS1(ref List<ResultWBS> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var FileName = openFileDialog1.FileName;
            bool IsFirst = true;
            var BobotTemp = 0m;

            using (var rd = new StreamReader(FileName))
            {

                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');

                    var arr = splits.ToArray();
                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            if (splits.Count() > 5)
                                BobotTemp = Decimal.Parse(splits[5].Trim());
                            else
                                BobotTemp = 0m;
                            l.Add(new ResultWBS()
                            {
                                StageCode = splits[0].Trim(),
                                TaskCode = splits[1].Trim(),
                                PlanStartDt = splits[2].Trim(),
                                PlanEndDt = splits[3].Trim(),
                                Price = Decimal.Parse(splits[4].Trim()),
                                Bobot = BobotTemp,
                                StageName = string.Empty,
                                TaskName = string.Empty
                            });
                        }
                        else
                        {
                            Sm.StdMsg(mMsgType.NoData, "");
                            return;
                        }
                    }
                }
            }
        }

        private void ProcessWBS2(ref List<ResultWBS> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var StageCode = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(StageCode=@StageCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@StageCode0" + i.ToString(), l[i].StageCode);
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select StageCode, StageName From TblProjectStage ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "StageCode", "StageName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        StageCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].StageCode, StageCode))
                                l[i].StageName = Sm.DrStr(dr, c[1]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessWBS3(ref List<ResultWBS> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var TaskCode = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(TaskCode=@TaskCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@TaskCode0" + i.ToString(), l[i].TaskCode);
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select TaskCode, TaskName From TblProjectTask ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "TaskCode", "TaskName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        TaskCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].TaskCode, TaskCode))
                                l[i].TaskName = Sm.DrStr(dr, c[1]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessWBS4(ref List<ResultWBS> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = l[i].StageCode;
                r.Cells[1].Value = l[i].StageName;
                r.Cells[2].Value = l[i].TaskCode;
                r.Cells[3].Value = l[i].TaskName;
                if (l[i].PlanStartDt.Length > 0) r.Cells[4].Value = Sm.ConvertDate(l[i].PlanStartDt);
                if (l[i].PlanEndDt.Length > 0) r.Cells[5].Value = Sm.ConvertDate(l[i].PlanEndDt);
                r.Cells[6].Value = l[i].Price;
                r.Cells[7].Value = l[i].Bobot;
                r.Cells[8].Value = 0m;
                r.Cells[9].Value = false;
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7, 8 });
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9 });
            Grd1.EndUpdate();
        }

        private void BtnImportWBS2_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ResultWBS2>();

                ClearGrdWBS2();
                try
                {
                    ProcessWBS1(ref l);
                    if (l.Count > 0)
                    {
                        ProcessWBS2(ref l);
                        ProcessWBS3(ref l);
                        ProcessWBS4(ref l);
                    }
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    l.Clear();
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void ProcessWBS1(ref List<ResultWBS2> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var FileName = openFileDialog1.FileName;
            bool IsFirst = true;
            var BobotTemp = 0m;
            var PriceTemp = 0m;

            using (var rd = new StreamReader(FileName))
            {

                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');

                    var arr = splits.ToArray();
                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            if (mIsWBS2SameAsWBS)
                            {
                                PriceTemp = Decimal.Parse(splits[4].Trim());
                                if (splits.Count() > 5)
                                    BobotTemp = Decimal.Parse(splits[5].Trim());
                                else
                                    BobotTemp = 0m;
                            }
                            else
                            {
                                PriceTemp = 0m;
                                BobotTemp = Decimal.Parse(splits[4].Trim());
                            }
                            l.Add(new ResultWBS2()
                            {
                                StageCode = splits[0].Trim(),
                                TaskCode = splits[1].Trim(),
                                PlanStartDt = splits[2].Trim(),
                                PlanEndDt = splits[3].Trim(),
                                Price = PriceTemp,
                                Bobot = BobotTemp,
                                StageName = string.Empty,
                                TaskName = string.Empty
                            });
                        }
                        else
                        {
                            Sm.StdMsg(mMsgType.NoData, "");
                            return;
                        }
                    }
                }
            }
        }

        private void ProcessWBS2(ref List<ResultWBS2> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var StageCode = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(StageCode=@StageCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@StageCode0" + i.ToString(), l[i].StageCode);
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select StageCode, StageName From TblProjectStage ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                { "StageCode", "StageName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        StageCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].StageCode, StageCode))
                                l[i].StageName = Sm.DrStr(dr, c[1]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessWBS3(ref List<ResultWBS2> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var TaskCode = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(TaskCode=@TaskCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@TaskCode0" + i.ToString(), l[i].TaskCode);
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select TaskCode, TaskName From TblProjectTask ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "TaskCode", "TaskName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        TaskCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].TaskCode, TaskCode))
                                l[i].TaskName = Sm.DrStr(dr, c[1]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessWBS4(ref List<ResultWBS2> l)
        {
            iGRow r;
            Grd5.BeginUpdate();
            Grd5.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd5.Rows.Add();
                r.Cells[0].Value = l[i].StageCode;
                r.Cells[1].Value = l[i].StageName;
                r.Cells[2].Value = l[i].TaskCode;
                r.Cells[3].Value = l[i].TaskName;
                if (l[i].PlanStartDt.Length > 0) r.Cells[4].Value = Sm.ConvertDate(l[i].PlanStartDt);
                if (l[i].PlanEndDt.Length > 0) r.Cells[5].Value = Sm.ConvertDate(l[i].PlanEndDt);
                r.Cells[6].Value = l[i].Price;
                r.Cells[7].Value = l[i].Bobot;
                r.Cells[8].Value = 0m;
                r.Cells[9].Value = false;
            }
            r = Grd5.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 6, 7, 8 });
            Sm.SetGrdBoolValueFalse(ref Grd5, Grd5.Rows.Count - 1, new int[] { 9 });
            Grd5.EndUpdate();
        }

        private void ProcessResource1(ref List<ResultResource> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var FileName = openFileDialog1.FileName;

            using (var rd = new StreamReader(FileName))
            {

                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits.Count() != 6)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid data.");
                        return;
                    }
                    else
                    {
                        var arr = splits.ToArray();
                        if (arr[0].Trim().Length > 0)
                        {
                            l.Add(new ResultResource()
                            {
                                ResourceItCode = splits[0].Trim(),
                                Remark = splits[1].Trim(),
                                Qty1 = Decimal.Parse(splits[2].Trim().Length > 0 ? splits[2].Trim() : "0"),
                                Qty2 = Decimal.Parse(splits[3].Trim().Length > 0 ? splits[3].Trim() : "0"),
                                UPrice = Decimal.Parse(splits[4].Trim().Length > 0 ? splits[4].Trim() : "0"),
                                Tax = Decimal.Parse(splits[5].Trim().Length > 0 ? splits[5].Trim() : "0"),
                                ItName = string.Empty,
                                ItGrpName = string.Empty,
                            });
                        }
                        else
                        {
                            Sm.StdMsg(mMsgType.NoData, "");
                            return;
                        }
                    }
                }
            }
        }

        private void ProcessResource2(ref List<ResultResource> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var ItCode = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.ItCode=@ItCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@ItCode0" + i.ToString(), l[i].ResourceItCode);
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select A.ItCode, A.ItName, B.ItGrpName ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left Join TblItemGroup B On A.ItGrpCode=B.ItGrpCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "ItName", "ItGrpName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].ResourceItCode, ItCode))
                            {
                                l[i].ItName = Sm.DrStr(dr, c[1]);
                                l[i].ItGrpName = Sm.DrStr(dr, c[2]);
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessResource3(ref List<ResultResource> l)
        {
            iGRow r;
            Grd2.BeginUpdate();
            Grd2.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd2.Rows.Add();
                r.Cells[1].Value = l[i].ResourceItCode;
                r.Cells[2].Value = l[i].ItName;
                r.Cells[3].Value = l[i].ItGrpName;
                r.Cells[4].Value = l[i].Remark;
                r.Cells[5].Value = l[i].Qty1;
                r.Cells[6].Value = l[i].Qty2;
                r.Cells[7].Value = l[i].UPrice;
                r.Cells[8].Value = l[i].Tax;
                r.Cells[9].Value = l[i].Qty1*l[i].Qty2*l[i].UPrice;
                r.Cells[10].Value = l[i].Qty1*l[i].Qty2*l[i].Tax;
                r.Cells[11].Value = (l[i].Qty1*l[i].Qty2*l[i].UPrice) + (l[i].Qty1*l[i].Qty2*l[i].Tax);
            }
            r = Grd2.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11 });
            Grd2.EndUpdate();
        }

        private void BtnImportResource_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ResultResource>();

                ClearGrdResource();
                TxtTotalResource.EditValue = Sm.FormatNum(0m, 0);
                try
                {
                    ProcessResource1(ref l);
                    if (l.Count > 0)
                    {
                        ProcessResource2(ref l);
                        ProcessResource3(ref l);
                        ComputeTotalResource();
                    }
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    l.Clear();
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void BtnSOContractDocNo_Click(object sender, EventArgs e)
        {
            if(BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmProjectImplementation3Dlg(this));
            }
        }

        private void BtnSOContractDocNo2_Click(object sender, EventArgs e)
        {
            if(!Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false))
            {
                var f = new FrmSOContractRevision(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmSOContractRevision");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtSOContractDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnBOQDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtBOQDocNo, "BOQ#", false))
            {
                //var f = new FrmBOQ(mMenuCode);
                //f.Tag = mMenuCode;
                //f.Text = Sm.GetMenuDesc("FrmBOQ");
                //f.WindowState = FormWindowState.Normal;
                //f.StartPosition = FormStartPosition.CenterScreen;
                //f.mDocNo = TxtBOQDocNo.Text;
                //f.ShowDialog();

                var f = new FrmBOQ3(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmBOQ3");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtBOQDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnLOPDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtLOPDocNo, "LOP#", false))
            {
                var f = new FrmLOP(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmLOP");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtLOPDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueStageCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueStageCode, new Sm.RefreshLue1(Sl.SetLueProjectStageCode));
            }
        }

        
       
        private void LueStageCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueStageCode_Leave(object sender, EventArgs e)
        {
            if (LueStageCode.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueStageCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 0].Value =
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueStageCode);
                    Grd1.Cells[fCell.RowIndex, 1].Value = LueStageCode.GetColumnValue("Col2");
                }
                LueStageCode.Visible = false;
            }
        }

        private void LueTaskCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaskCode, new Sm.RefreshLue1(Sl.SetLueProjectTaskCode));
            }
        }

        private void LueTaskCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueTaskCode_Leave(object sender, EventArgs e)
        {
            if (LueTaskCode.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (Sm.GetLue(LueTaskCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 2].Value =
                    Grd1.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueTaskCode);
                    Grd1.Cells[fCell.RowIndex, 3].Value = LueTaskCode.GetColumnValue("Col2");
                }
                LueTaskCode.Visible = false;
            }
        }

        private void DtePlanStartDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DtePlanStartDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DtePlanStartDt, ref fCell, ref fAccept);
        }

        private void DtePlanEndDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DtePlanEndDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DtePlanEndDt, ref fCell, ref fAccept);
        }

        private void DteDocDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd4, ref fAccept, e);
        }

        private void DteDocDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDocDt2, ref fCell, ref fAccept);
        }

        private void LueProcessInd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProcessInd, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectImplementationProcessInd");
        }

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            int SelectedIndex = 0;

            if (Grd7.SelectedRows.Count > 0)
            {
                for (int Index = Grd7.SelectedRows.Count - 1; Index >= 0; Index--)
                    SelectedIndex = Grd7.SelectedRows[Index].Index;
            }

            if (mStateInd == "E" && Sm.GetLue(LueProcessInd) != "F")
            {
                if (Sm.GetGrdStr(Grd7, SelectedIndex, 1).Length > 0)
                {
                    Sm.GrdRemoveRow(Grd7, e, BtnSave);
                    Sm.GrdEnter(Grd7, e);
                    Sm.GrdTabInLastCell(Grd7, e, BtnFind, BtnSave);
                }
            }
            else if (mStateInd == "I")
            {
                Sm.GrdRemoveRow(Grd7, e, BtnSave);
                Sm.GrdEnter(Grd7, e);
                Sm.GrdTabInLastCell(Grd7, e, BtnFind, BtnSave);
            }
        }

        private void LueStageCode2_Validated(object sender, EventArgs e)
        {
            ProcessWBSSummary();
        }

        private void LueTaskCode_Validated(object sender, EventArgs e)
        {
            ProcessWBSSummary();
        }

        private void DtePlanEndDt2_Validated(object sender, EventArgs e)
        {
            ProcessWBSSummary();
        }

        private void DtePlanStartDt2_Validated(object sender, EventArgs e)
        {
            ProcessWBSSummary();
        }

        private void Grd7_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

    
        private void LueStageCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueStageCode2, new Sm.RefreshLue1(Sl.SetLueProjectStageCode));
            }
        }

        private void LueStageCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void LueStageCode2_Leave(object sender, EventArgs e)
        {
            if (LueStageCode2.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueStageCode2).Length == 0)
                    Grd5.Cells[fCell.RowIndex, 0].Value =
                    Grd5.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueStageCode2);
                    Grd5.Cells[fCell.RowIndex, 1].Value = LueStageCode2.GetColumnValue("Col2");
                }
                LueStageCode2.Visible = false;
            }
        }

        private void LueTaskCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaskCode, new Sm.RefreshLue1(Sl.SetLueProjectTaskCode));
            }
        }

        private void LueTaskCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void LueTaskCode2_Leave(object sender, EventArgs e)
        {
            if (LueTaskCode.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (Sm.GetLue(LueTaskCode).Length == 0)
                    Grd5.Cells[fCell.RowIndex, 2].Value =
                    Grd5.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueTaskCode);
                    Grd5.Cells[fCell.RowIndex, 3].Value = LueTaskCode.GetColumnValue("Col2");
                }
                LueTaskCode.Visible = false;
            }
        }

        private void Grd7_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd7, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd7, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd7, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd7, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd7, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|ZIP/RAR files (*.rar;*zip)|*.rar;*zip";
                    OD.ShowDialog();
                    Grd7.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }

        private void DtePlanEndDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd5, ref fAccept, e);
        }


        private void DtePlanEndDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DtePlanEndDt2, ref fCell, ref fAccept);
        }

        private void DtePlanStartDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd5, ref fAccept, e);
        }

        private void DtePlanStartDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DtePlanStartDt2, ref fCell, ref fAccept);
        }


        #endregion

        #endregion

        #region Class

      

        private class WBSSummary
        {
            public string StageCode { get; set; }
            public string StageName { get; set; }
            public string PlanStartDt { get; set; }
            public string PlanEndDt { get; set; }
            public decimal Price { get; set; }
            public decimal BobotPerc { get; set; }
        }

        private class ResultWBS
        {
            public string StageCode { get; set; }
            public string StageName { get; set; }
            public string TaskCode { get; set; }
            public string TaskName { get; set; }
            public string PlanStartDt { get; set; }
            public string PlanEndDt { get; set; }
            public decimal Price { get; set; }
            public decimal Bobot { get; set; }
        }


        private class ResultWBS2
        {
            public string StageCode { get; set; }
            public string StageName { get; set; }
            public string TaskCode { get; set; }
            public string TaskName { get; set; }
            public string PlanStartDt { get; set; }
            public string PlanEndDt { get; set; }
            public decimal Price { get; set; }
            public decimal Bobot { get; set; }
        }

        private class ResultResource
        {
            public string ResourceItCode { get; set; }
            public string ItName { get; set; }
            public string ItGrpName { get; set; }
            public string Remark { get; set; }
            public decimal Qty1 { get; set; }
            public decimal Qty2 { get; set; }
            public decimal UPrice { get; set; }
            public decimal Tax { get; set; }
        }

        private class PRJI
        {
            public string Stage { get; set; }
            public string Task { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Bobot { get; set; }
            public decimal Persentase { get; set; }
            public decimal Akumulasi { get; set; }
            public bool SettledInd { get; set; }
            public string EndDt2 { get; set; }
        }

        private class PRJIDummy
        {
            public string Stage { get; set; }
            public string Task { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Bobot { get; set; }
            public decimal Persentase { get; set; }
            public decimal Akumulasi { get; set; }
            public bool SettledInd { get; set; }
            public string EndDt2 { get; set; }
        }

        private class PRJI2
        {
            public string CurrentDt { get; set; }
            public decimal Achievment { get; set; }

        }

        private class Resource
        {
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string LOPDocNo { get; set; }
            public string ProjectName { get; set; }
            public string CompanyLogo { get; set; }
            public decimal RemunerationAmt { get; set; }
            public decimal DirectCostAmt { get; set; }
            public decimal ExclPPNAmt { get; set; }
            public decimal PPNAmt { get; set; }
            public decimal PPhAmt { get; set; }
            public decimal ContractAmt { get; set; }
            public decimal ExclPPNPPhAmt { get; set; }
            public decimal ExclPPNPercentage { get; set; }
            public decimal ExclPPNPPhPercentage { get; set; }
            public decimal Total { get; set; }
            public string SiteName { get; set; }
            public string ProjectType { get; set; }
            public string ProjectCode { get; set; }
        }

        private class PRJISign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class PRJISign2
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class PRJIDtlSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class RBPData
        {
            public string ProjectCode { get; set; }
            public string VdName { get; set; }
            public string ItName { get; set; }
            public string ItCtCode { get; set; }
            public decimal UPrice { get; set; }
            public decimal Amt { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public decimal RemunerationAmt { get; set; }
            public decimal DirectCostAmt { get; set; }
            public string Sequence { get; set; }
        }

        private class RBPRemuneration
        {
            public int No { get; set; }
            public string ProjectCode { get; set; }
            public string VdName { get; set; }
            public string ItName { get; set; }
            public decimal UPrice { get; set; }
            public decimal Amt { get; set; }
            public decimal Mth01Amt { get; set; }
            public decimal Mth02Amt { get; set; }
            public decimal Mth03Amt { get; set; }
            public decimal Mth04Amt { get; set; }
            public decimal Mth05Amt { get; set; }
            public decimal Mth06Amt { get; set; }
            public decimal Mth07Amt { get; set; }
            public decimal Mth08Amt { get; set; }
            public decimal Mth09Amt { get; set; }
            public decimal Mth10Amt { get; set; }
            public decimal Mth11Amt { get; set; }
            public decimal Mth12Amt { get; set; }
            public string Yr { get; set; }
            public decimal TotalRemunerationAmt { get; set; }
            public string Sequence { get; set; }
        }

        private class RBPDirectCost
        {
            public int No { get; set; }
            public string ProjectCode { get; set; }
            public string VdName { get; set; }
            public string ItName { get; set; }
            public decimal UPrice { get; set; }
            public decimal Amt { get; set; }
            public decimal Mth01Amt { get; set; }
            public decimal Mth02Amt { get; set; }
            public decimal Mth03Amt { get; set; }
            public decimal Mth04Amt { get; set; }
            public decimal Mth05Amt { get; set; }
            public decimal Mth06Amt { get; set; }
            public decimal Mth07Amt { get; set; }
            public decimal Mth08Amt { get; set; }
            public decimal Mth09Amt { get; set; }
            public decimal Mth10Amt { get; set; }
            public decimal Mth11Amt { get; set; }
            public decimal Mth12Amt { get; set; }
            public string Yr { get; set; }
            public decimal TotalDirectCostAmt { get; set; }
            public string Sequence { get; set; }
        }

        #endregion   

    }
}
