﻿namespace RunSystem
{
    partial class FrmAttachmentFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAttachmentFile));
            this.label18 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.ChkFile1 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload1 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload1 = new System.Windows.Forms.ProgressBar();
            this.TxtFile1 = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.BtnUpload1 = new DevExpress.XtraEditors.SimpleButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.BtnDownload20 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload19 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload18 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload17 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload16 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload15 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload14 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload13 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload12 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload11 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnUpload20 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnUpload19 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnUpload18 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile20 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload20 = new System.Windows.Forms.ProgressBar();
            this.TxtFile20 = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.ChkFile19 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload19 = new System.Windows.Forms.ProgressBar();
            this.TxtFile19 = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.ChkFile18 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload18 = new System.Windows.Forms.ProgressBar();
            this.TxtFile18 = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.ChkFile17 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload17 = new System.Windows.Forms.ProgressBar();
            this.TxtFile17 = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.BtnUpload17 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile16 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload16 = new System.Windows.Forms.ProgressBar();
            this.TxtFile16 = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.BtnUpload16 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile15 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload15 = new System.Windows.Forms.ProgressBar();
            this.TxtFile15 = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.BtnUpload15 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile14 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload14 = new System.Windows.Forms.ProgressBar();
            this.TxtFile14 = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.BtnUpload14 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile13 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload13 = new System.Windows.Forms.ProgressBar();
            this.TxtFile13 = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.BtnUpload13 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile12 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload12 = new System.Windows.Forms.ProgressBar();
            this.TxtFile12 = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.BtnUpload12 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile11 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload11 = new System.Windows.Forms.ProgressBar();
            this.TxtFile11 = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.BtnUpload11 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile2 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload2 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload2 = new System.Windows.Forms.ProgressBar();
            this.TxtFile2 = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnUpload2 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile4 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload4 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload4 = new System.Windows.Forms.ProgressBar();
            this.TxtFile4 = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnUpload4 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile3 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload3 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload3 = new System.Windows.Forms.ProgressBar();
            this.TxtFile3 = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnUpload3 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile6 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload6 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload6 = new System.Windows.Forms.ProgressBar();
            this.TxtFile6 = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnUpload6 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile5 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload5 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload5 = new System.Windows.Forms.ProgressBar();
            this.TxtFile5 = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnUpload5 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile8 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload8 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload8 = new System.Windows.Forms.ProgressBar();
            this.TxtFile8 = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnUpload8 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile7 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload7 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload7 = new System.Windows.Forms.ProgressBar();
            this.TxtFile7 = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnUpload7 = new DevExpress.XtraEditors.SimpleButton();
            this.OD1 = new System.Windows.Forms.OpenFileDialog();
            this.ChkFile10 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload10 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload10 = new System.Windows.Forms.ProgressBar();
            this.TxtFile10 = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.BtnUpload10 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile9 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload9 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload9 = new System.Windows.Forms.ProgressBar();
            this.TxtFile9 = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.BtnUpload9 = new DevExpress.XtraEditors.SimpleButton();
            this.SFD1 = new System.Windows.Forms.SaveFileDialog();
            this.BtnSP = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile1.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile9.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(848, 0);
            this.panel1.Size = new System.Drawing.Size(70, 405);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnSP);
            this.panel2.Controls.Add(this.ChkFile10);
            this.panel2.Controls.Add(this.BtnDownload10);
            this.panel2.Controls.Add(this.PbUpload10);
            this.panel2.Controls.Add(this.TxtFile10);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.BtnUpload10);
            this.panel2.Controls.Add(this.ChkFile9);
            this.panel2.Controls.Add(this.BtnDownload9);
            this.panel2.Controls.Add(this.PbUpload9);
            this.panel2.Controls.Add(this.TxtFile9);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.BtnUpload9);
            this.panel2.Controls.Add(this.ChkFile8);
            this.panel2.Controls.Add(this.BtnDownload8);
            this.panel2.Controls.Add(this.PbUpload8);
            this.panel2.Controls.Add(this.TxtFile8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.BtnUpload8);
            this.panel2.Controls.Add(this.ChkFile7);
            this.panel2.Controls.Add(this.BtnDownload7);
            this.panel2.Controls.Add(this.PbUpload7);
            this.panel2.Controls.Add(this.TxtFile7);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.BtnUpload7);
            this.panel2.Controls.Add(this.ChkFile6);
            this.panel2.Controls.Add(this.BtnDownload6);
            this.panel2.Controls.Add(this.PbUpload6);
            this.panel2.Controls.Add(this.TxtFile6);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.BtnUpload6);
            this.panel2.Controls.Add(this.ChkFile5);
            this.panel2.Controls.Add(this.BtnDownload5);
            this.panel2.Controls.Add(this.PbUpload5);
            this.panel2.Controls.Add(this.TxtFile5);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.BtnUpload5);
            this.panel2.Controls.Add(this.ChkFile4);
            this.panel2.Controls.Add(this.BtnDownload4);
            this.panel2.Controls.Add(this.PbUpload4);
            this.panel2.Controls.Add(this.TxtFile4);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.BtnUpload4);
            this.panel2.Controls.Add(this.ChkFile3);
            this.panel2.Controls.Add(this.BtnDownload3);
            this.panel2.Controls.Add(this.PbUpload3);
            this.panel2.Controls.Add(this.TxtFile3);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.BtnUpload3);
            this.panel2.Controls.Add(this.ChkFile2);
            this.panel2.Controls.Add(this.BtnDownload2);
            this.panel2.Controls.Add(this.PbUpload2);
            this.panel2.Controls.Add(this.TxtFile2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.BtnUpload2);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.ChkFile1);
            this.panel2.Controls.Add(this.BtnDownload1);
            this.panel2.Controls.Add(this.PbUpload1);
            this.panel2.Controls.Add(this.TxtFile1);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.BtnUpload1);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Size = new System.Drawing.Size(848, 405);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(5, 10);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 14);
            this.label18.TabIndex = 14;
            this.label18.Text = "Document#";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(83, 7);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(235, 20);
            this.TxtDocNo.TabIndex = 15;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(45, 32);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 14);
            this.label17.TabIndex = 16;
            this.label17.Text = "Date";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(83, 29);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(126, 20);
            this.DteDocDt.TabIndex = 17;
            // 
            // ChkFile1
            // 
            this.ChkFile1.Location = new System.Drawing.Point(320, 49);
            this.ChkFile1.Name = "ChkFile1";
            this.ChkFile1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile1.Properties.Appearance.Options.UseFont = true;
            this.ChkFile1.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile1.Properties.Caption = " ";
            this.ChkFile1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile1.Size = new System.Drawing.Size(20, 22);
            this.ChkFile1.TabIndex = 48;
            this.ChkFile1.ToolTip = "Remove filter";
            this.ChkFile1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile1.ToolTipTitle = "Run System";
            this.ChkFile1.CheckedChanged += new System.EventHandler(this.ChkFile1_CheckedChanged);
            // 
            // BtnDownload1
            // 
            this.BtnDownload1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload1.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload1.Appearance.Options.UseBackColor = true;
            this.BtnDownload1.Appearance.Options.UseFont = true;
            this.BtnDownload1.Appearance.Options.UseForeColor = true;
            this.BtnDownload1.Appearance.Options.UseTextOptions = true;
            this.BtnDownload1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload1.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload1.Image")));
            this.BtnDownload1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload1.Location = new System.Drawing.Point(379, 49);
            this.BtnDownload1.Name = "BtnDownload1";
            this.BtnDownload1.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload1.TabIndex = 47;
            this.BtnDownload1.ToolTip = "DownloadFile";
            this.BtnDownload1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload1.ToolTipTitle = "Run System";
            this.BtnDownload1.Click += new System.EventHandler(this.BtnDownload1_Click);
            // 
            // PbUpload1
            // 
            this.PbUpload1.Location = new System.Drawing.Point(83, 72);
            this.PbUpload1.Name = "PbUpload1";
            this.PbUpload1.Size = new System.Drawing.Size(320, 10);
            this.PbUpload1.TabIndex = 46;
            // 
            // TxtFile1
            // 
            this.TxtFile1.EnterMoveNextControl = true;
            this.TxtFile1.Location = new System.Drawing.Point(83, 50);
            this.TxtFile1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile1.Name = "TxtFile1";
            this.TxtFile1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile1.Properties.Appearance.Options.UseFont = true;
            this.TxtFile1.Properties.MaxLength = 16;
            this.TxtFile1.Size = new System.Drawing.Size(234, 20);
            this.TxtFile1.TabIndex = 44;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(43, 54);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 14);
            this.label6.TabIndex = 43;
            this.label6.Text = "File 1";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload1
            // 
            this.BtnUpload1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload1.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload1.Appearance.Options.UseBackColor = true;
            this.BtnUpload1.Appearance.Options.UseFont = true;
            this.BtnUpload1.Appearance.Options.UseForeColor = true;
            this.BtnUpload1.Appearance.Options.UseTextOptions = true;
            this.BtnUpload1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload1.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload1.Image")));
            this.BtnUpload1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload1.Location = new System.Drawing.Point(348, 49);
            this.BtnUpload1.Name = "BtnUpload1";
            this.BtnUpload1.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload1.TabIndex = 45;
            this.BtnUpload1.ToolTip = "BrowseFile";
            this.BtnUpload1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload1.ToolTipTitle = "Run System";
            this.BtnUpload1.Click += new System.EventHandler(this.BtnUpload1_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.BtnDownload20);
            this.panel4.Controls.Add(this.BtnDownload19);
            this.panel4.Controls.Add(this.BtnDownload18);
            this.panel4.Controls.Add(this.BtnDownload17);
            this.panel4.Controls.Add(this.BtnDownload16);
            this.panel4.Controls.Add(this.BtnDownload15);
            this.panel4.Controls.Add(this.BtnDownload14);
            this.panel4.Controls.Add(this.BtnDownload13);
            this.panel4.Controls.Add(this.BtnDownload12);
            this.panel4.Controls.Add(this.BtnDownload11);
            this.panel4.Controls.Add(this.BtnUpload20);
            this.panel4.Controls.Add(this.BtnUpload19);
            this.panel4.Controls.Add(this.BtnUpload18);
            this.panel4.Controls.Add(this.ChkFile20);
            this.panel4.Controls.Add(this.PbUpload20);
            this.panel4.Controls.Add(this.TxtFile20);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.ChkFile19);
            this.panel4.Controls.Add(this.PbUpload19);
            this.panel4.Controls.Add(this.TxtFile19);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.ChkFile18);
            this.panel4.Controls.Add(this.PbUpload18);
            this.panel4.Controls.Add(this.TxtFile18);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.ChkFile17);
            this.panel4.Controls.Add(this.PbUpload17);
            this.panel4.Controls.Add(this.TxtFile17);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.BtnUpload17);
            this.panel4.Controls.Add(this.ChkFile16);
            this.panel4.Controls.Add(this.PbUpload16);
            this.panel4.Controls.Add(this.TxtFile16);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.BtnUpload16);
            this.panel4.Controls.Add(this.ChkFile15);
            this.panel4.Controls.Add(this.PbUpload15);
            this.panel4.Controls.Add(this.TxtFile15);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.BtnUpload15);
            this.panel4.Controls.Add(this.ChkFile14);
            this.panel4.Controls.Add(this.PbUpload14);
            this.panel4.Controls.Add(this.TxtFile14);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.BtnUpload14);
            this.panel4.Controls.Add(this.ChkFile13);
            this.panel4.Controls.Add(this.PbUpload13);
            this.panel4.Controls.Add(this.TxtFile13);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.BtnUpload13);
            this.panel4.Controls.Add(this.ChkFile12);
            this.panel4.Controls.Add(this.PbUpload12);
            this.panel4.Controls.Add(this.TxtFile12);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.BtnUpload12);
            this.panel4.Controls.Add(this.ChkFile11);
            this.panel4.Controls.Add(this.PbUpload11);
            this.panel4.Controls.Add(this.TxtFile11);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.BtnUpload11);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(437, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(411, 405);
            this.panel4.TabIndex = 49;
            // 
            // BtnDownload20
            // 
            this.BtnDownload20.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload20.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload20.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload20.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload20.Appearance.Options.UseBackColor = true;
            this.BtnDownload20.Appearance.Options.UseFont = true;
            this.BtnDownload20.Appearance.Options.UseForeColor = true;
            this.BtnDownload20.Appearance.Options.UseTextOptions = true;
            this.BtnDownload20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload20.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload20.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload20.Image")));
            this.BtnDownload20.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload20.Location = new System.Drawing.Point(373, 356);
            this.BtnDownload20.Name = "BtnDownload20";
            this.BtnDownload20.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload20.TabIndex = 128;
            this.BtnDownload20.ToolTip = "BrowseFile";
            this.BtnDownload20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload20.ToolTipTitle = "Run System";
            this.BtnDownload20.Click += new System.EventHandler(this.BtnDownload20_Click);
            // 
            // BtnDownload19
            // 
            this.BtnDownload19.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload19.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload19.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload19.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload19.Appearance.Options.UseBackColor = true;
            this.BtnDownload19.Appearance.Options.UseFont = true;
            this.BtnDownload19.Appearance.Options.UseForeColor = true;
            this.BtnDownload19.Appearance.Options.UseTextOptions = true;
            this.BtnDownload19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload19.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload19.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload19.Image")));
            this.BtnDownload19.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload19.Location = new System.Drawing.Point(373, 321);
            this.BtnDownload19.Name = "BtnDownload19";
            this.BtnDownload19.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload19.TabIndex = 127;
            this.BtnDownload19.ToolTip = "BrowseFile";
            this.BtnDownload19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload19.ToolTipTitle = "Run System";
            this.BtnDownload19.Click += new System.EventHandler(this.BtnDownload19_Click);
            // 
            // BtnDownload18
            // 
            this.BtnDownload18.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload18.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload18.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload18.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload18.Appearance.Options.UseBackColor = true;
            this.BtnDownload18.Appearance.Options.UseFont = true;
            this.BtnDownload18.Appearance.Options.UseForeColor = true;
            this.BtnDownload18.Appearance.Options.UseTextOptions = true;
            this.BtnDownload18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload18.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload18.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload18.Image")));
            this.BtnDownload18.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload18.Location = new System.Drawing.Point(374, 286);
            this.BtnDownload18.Name = "BtnDownload18";
            this.BtnDownload18.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload18.TabIndex = 126;
            this.BtnDownload18.ToolTip = "BrowseFile";
            this.BtnDownload18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload18.ToolTipTitle = "Run System";
            this.BtnDownload18.Click += new System.EventHandler(this.BtnDownload18_Click);
            // 
            // BtnDownload17
            // 
            this.BtnDownload17.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload17.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload17.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload17.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload17.Appearance.Options.UseBackColor = true;
            this.BtnDownload17.Appearance.Options.UseFont = true;
            this.BtnDownload17.Appearance.Options.UseForeColor = true;
            this.BtnDownload17.Appearance.Options.UseTextOptions = true;
            this.BtnDownload17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload17.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload17.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload17.Image")));
            this.BtnDownload17.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload17.Location = new System.Drawing.Point(375, 252);
            this.BtnDownload17.Name = "BtnDownload17";
            this.BtnDownload17.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload17.TabIndex = 125;
            this.BtnDownload17.ToolTip = "BrowseFile";
            this.BtnDownload17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload17.ToolTipTitle = "Run System";
            this.BtnDownload17.Click += new System.EventHandler(this.BtnDownload17_Click);
            // 
            // BtnDownload16
            // 
            this.BtnDownload16.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload16.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload16.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload16.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload16.Appearance.Options.UseBackColor = true;
            this.BtnDownload16.Appearance.Options.UseFont = true;
            this.BtnDownload16.Appearance.Options.UseForeColor = true;
            this.BtnDownload16.Appearance.Options.UseTextOptions = true;
            this.BtnDownload16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload16.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload16.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload16.Image")));
            this.BtnDownload16.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload16.Location = new System.Drawing.Point(375, 217);
            this.BtnDownload16.Name = "BtnDownload16";
            this.BtnDownload16.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload16.TabIndex = 124;
            this.BtnDownload16.ToolTip = "BrowseFile";
            this.BtnDownload16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload16.ToolTipTitle = "Run System";
            this.BtnDownload16.Click += new System.EventHandler(this.BtnDownload16_Click);
            // 
            // BtnDownload15
            // 
            this.BtnDownload15.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload15.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload15.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload15.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload15.Appearance.Options.UseBackColor = true;
            this.BtnDownload15.Appearance.Options.UseFont = true;
            this.BtnDownload15.Appearance.Options.UseForeColor = true;
            this.BtnDownload15.Appearance.Options.UseTextOptions = true;
            this.BtnDownload15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload15.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload15.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload15.Image")));
            this.BtnDownload15.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload15.Location = new System.Drawing.Point(374, 183);
            this.BtnDownload15.Name = "BtnDownload15";
            this.BtnDownload15.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload15.TabIndex = 123;
            this.BtnDownload15.ToolTip = "BrowseFile";
            this.BtnDownload15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload15.ToolTipTitle = "Run System";
            this.BtnDownload15.Click += new System.EventHandler(this.BtnDownload15_Click);
            // 
            // BtnDownload14
            // 
            this.BtnDownload14.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload14.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload14.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload14.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload14.Appearance.Options.UseBackColor = true;
            this.BtnDownload14.Appearance.Options.UseFont = true;
            this.BtnDownload14.Appearance.Options.UseForeColor = true;
            this.BtnDownload14.Appearance.Options.UseTextOptions = true;
            this.BtnDownload14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload14.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload14.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload14.Image")));
            this.BtnDownload14.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload14.Location = new System.Drawing.Point(372, 149);
            this.BtnDownload14.Name = "BtnDownload14";
            this.BtnDownload14.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload14.TabIndex = 122;
            this.BtnDownload14.ToolTip = "BrowseFile";
            this.BtnDownload14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload14.ToolTipTitle = "Run System";
            this.BtnDownload14.Click += new System.EventHandler(this.BtnDownload14_Click);
            // 
            // BtnDownload13
            // 
            this.BtnDownload13.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload13.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload13.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload13.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload13.Appearance.Options.UseBackColor = true;
            this.BtnDownload13.Appearance.Options.UseFont = true;
            this.BtnDownload13.Appearance.Options.UseForeColor = true;
            this.BtnDownload13.Appearance.Options.UseTextOptions = true;
            this.BtnDownload13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload13.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload13.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload13.Image")));
            this.BtnDownload13.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload13.Location = new System.Drawing.Point(373, 114);
            this.BtnDownload13.Name = "BtnDownload13";
            this.BtnDownload13.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload13.TabIndex = 121;
            this.BtnDownload13.ToolTip = "BrowseFile";
            this.BtnDownload13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload13.ToolTipTitle = "Run System";
            this.BtnDownload13.Click += new System.EventHandler(this.BtnDownload13_Click);
            // 
            // BtnDownload12
            // 
            this.BtnDownload12.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload12.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload12.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload12.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload12.Appearance.Options.UseBackColor = true;
            this.BtnDownload12.Appearance.Options.UseFont = true;
            this.BtnDownload12.Appearance.Options.UseForeColor = true;
            this.BtnDownload12.Appearance.Options.UseTextOptions = true;
            this.BtnDownload12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload12.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload12.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload12.Image")));
            this.BtnDownload12.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload12.Location = new System.Drawing.Point(373, 79);
            this.BtnDownload12.Name = "BtnDownload12";
            this.BtnDownload12.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload12.TabIndex = 120;
            this.BtnDownload12.ToolTip = "BrowseFile";
            this.BtnDownload12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload12.ToolTipTitle = "Run System";
            this.BtnDownload12.Click += new System.EventHandler(this.BtnDownload12_Click);
            // 
            // BtnDownload11
            // 
            this.BtnDownload11.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload11.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload11.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload11.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload11.Appearance.Options.UseBackColor = true;
            this.BtnDownload11.Appearance.Options.UseFont = true;
            this.BtnDownload11.Appearance.Options.UseForeColor = true;
            this.BtnDownload11.Appearance.Options.UseTextOptions = true;
            this.BtnDownload11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload11.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload11.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload11.Image")));
            this.BtnDownload11.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload11.Location = new System.Drawing.Point(372, 41);
            this.BtnDownload11.Name = "BtnDownload11";
            this.BtnDownload11.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload11.TabIndex = 119;
            this.BtnDownload11.ToolTip = "BrowseFile";
            this.BtnDownload11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload11.ToolTipTitle = "Run System";
            this.BtnDownload11.Click += new System.EventHandler(this.BtnDownload11_Click);
            // 
            // BtnUpload20
            // 
            this.BtnUpload20.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload20.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload20.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload20.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload20.Appearance.Options.UseBackColor = true;
            this.BtnUpload20.Appearance.Options.UseFont = true;
            this.BtnUpload20.Appearance.Options.UseForeColor = true;
            this.BtnUpload20.Appearance.Options.UseTextOptions = true;
            this.BtnUpload20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload20.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload20.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload20.Image")));
            this.BtnUpload20.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload20.Location = new System.Drawing.Point(338, 356);
            this.BtnUpload20.Name = "BtnUpload20";
            this.BtnUpload20.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload20.TabIndex = 118;
            this.BtnUpload20.ToolTip = "BrowseFile";
            this.BtnUpload20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload20.ToolTipTitle = "Run System";
            this.BtnUpload20.Click += new System.EventHandler(this.BtnUpload20_Click);
            // 
            // BtnUpload19
            // 
            this.BtnUpload19.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload19.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload19.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload19.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload19.Appearance.Options.UseBackColor = true;
            this.BtnUpload19.Appearance.Options.UseFont = true;
            this.BtnUpload19.Appearance.Options.UseForeColor = true;
            this.BtnUpload19.Appearance.Options.UseTextOptions = true;
            this.BtnUpload19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload19.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload19.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload19.Image")));
            this.BtnUpload19.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload19.Location = new System.Drawing.Point(340, 322);
            this.BtnUpload19.Name = "BtnUpload19";
            this.BtnUpload19.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload19.TabIndex = 117;
            this.BtnUpload19.ToolTip = "BrowseFile";
            this.BtnUpload19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload19.ToolTipTitle = "Run System";
            this.BtnUpload19.Click += new System.EventHandler(this.BtnUpload19_Click);
            // 
            // BtnUpload18
            // 
            this.BtnUpload18.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload18.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload18.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload18.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload18.Appearance.Options.UseBackColor = true;
            this.BtnUpload18.Appearance.Options.UseFont = true;
            this.BtnUpload18.Appearance.Options.UseForeColor = true;
            this.BtnUpload18.Appearance.Options.UseTextOptions = true;
            this.BtnUpload18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload18.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload18.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload18.Image")));
            this.BtnUpload18.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload18.Location = new System.Drawing.Point(342, 286);
            this.BtnUpload18.Name = "BtnUpload18";
            this.BtnUpload18.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload18.TabIndex = 116;
            this.BtnUpload18.ToolTip = "BrowseFile";
            this.BtnUpload18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload18.ToolTipTitle = "Run System";
            this.BtnUpload18.Click += new System.EventHandler(this.BtnUpload18_Click);
            // 
            // ChkFile20
            // 
            this.ChkFile20.Location = new System.Drawing.Point(314, 356);
            this.ChkFile20.Name = "ChkFile20";
            this.ChkFile20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile20.Properties.Appearance.Options.UseFont = true;
            this.ChkFile20.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile20.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile20.Properties.Caption = " ";
            this.ChkFile20.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile20.Size = new System.Drawing.Size(20, 22);
            this.ChkFile20.TabIndex = 115;
            this.ChkFile20.ToolTip = "Remove filter";
            this.ChkFile20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile20.ToolTipTitle = "Run System";
            this.ChkFile20.CheckedChanged += new System.EventHandler(this.ChkFile20_CheckedChanged);
            // 
            // PbUpload20
            // 
            this.PbUpload20.Location = new System.Drawing.Point(78, 379);
            this.PbUpload20.Name = "PbUpload20";
            this.PbUpload20.Size = new System.Drawing.Size(320, 10);
            this.PbUpload20.TabIndex = 113;
            // 
            // TxtFile20
            // 
            this.TxtFile20.EnterMoveNextControl = true;
            this.TxtFile20.Location = new System.Drawing.Point(78, 357);
            this.TxtFile20.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile20.Name = "TxtFile20";
            this.TxtFile20.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile20.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile20.Properties.Appearance.Options.UseFont = true;
            this.TxtFile20.Properties.MaxLength = 16;
            this.TxtFile20.Size = new System.Drawing.Size(234, 20);
            this.TxtFile20.TabIndex = 111;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(27, 361);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(42, 14);
            this.label19.TabIndex = 110;
            this.label19.Text = "File 20";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkFile19
            // 
            this.ChkFile19.Location = new System.Drawing.Point(315, 321);
            this.ChkFile19.Name = "ChkFile19";
            this.ChkFile19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile19.Properties.Appearance.Options.UseFont = true;
            this.ChkFile19.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile19.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile19.Properties.Caption = " ";
            this.ChkFile19.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile19.Size = new System.Drawing.Size(20, 22);
            this.ChkFile19.TabIndex = 109;
            this.ChkFile19.ToolTip = "Remove filter";
            this.ChkFile19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile19.ToolTipTitle = "Run System";
            this.ChkFile19.CheckedChanged += new System.EventHandler(this.ChkFile19_CheckedChanged);
            // 
            // PbUpload19
            // 
            this.PbUpload19.Location = new System.Drawing.Point(78, 344);
            this.PbUpload19.Name = "PbUpload19";
            this.PbUpload19.Size = new System.Drawing.Size(320, 10);
            this.PbUpload19.TabIndex = 107;
            // 
            // TxtFile19
            // 
            this.TxtFile19.EnterMoveNextControl = true;
            this.TxtFile19.Location = new System.Drawing.Point(78, 322);
            this.TxtFile19.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile19.Name = "TxtFile19";
            this.TxtFile19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile19.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile19.Properties.Appearance.Options.UseFont = true;
            this.TxtFile19.Properties.MaxLength = 16;
            this.TxtFile19.Size = new System.Drawing.Size(234, 20);
            this.TxtFile19.TabIndex = 105;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(27, 326);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(42, 14);
            this.label20.TabIndex = 104;
            this.label20.Text = "File 19";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkFile18
            // 
            this.ChkFile18.Location = new System.Drawing.Point(313, 286);
            this.ChkFile18.Name = "ChkFile18";
            this.ChkFile18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile18.Properties.Appearance.Options.UseFont = true;
            this.ChkFile18.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile18.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile18.Properties.Caption = " ";
            this.ChkFile18.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile18.Size = new System.Drawing.Size(20, 22);
            this.ChkFile18.TabIndex = 103;
            this.ChkFile18.ToolTip = "Remove filter";
            this.ChkFile18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile18.ToolTipTitle = "Run System";
            this.ChkFile18.CheckedChanged += new System.EventHandler(this.ChkFile18_CheckedChanged);
            // 
            // PbUpload18
            // 
            this.PbUpload18.Location = new System.Drawing.Point(78, 309);
            this.PbUpload18.Name = "PbUpload18";
            this.PbUpload18.Size = new System.Drawing.Size(320, 10);
            this.PbUpload18.TabIndex = 101;
            // 
            // TxtFile18
            // 
            this.TxtFile18.EnterMoveNextControl = true;
            this.TxtFile18.Location = new System.Drawing.Point(78, 287);
            this.TxtFile18.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile18.Name = "TxtFile18";
            this.TxtFile18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile18.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile18.Properties.Appearance.Options.UseFont = true;
            this.TxtFile18.Properties.MaxLength = 16;
            this.TxtFile18.Size = new System.Drawing.Size(234, 20);
            this.TxtFile18.TabIndex = 99;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(27, 291);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 14);
            this.label15.TabIndex = 98;
            this.label15.Text = "File 18";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkFile17
            // 
            this.ChkFile17.Location = new System.Drawing.Point(314, 251);
            this.ChkFile17.Name = "ChkFile17";
            this.ChkFile17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile17.Properties.Appearance.Options.UseFont = true;
            this.ChkFile17.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile17.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile17.Properties.Caption = " ";
            this.ChkFile17.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile17.Size = new System.Drawing.Size(20, 22);
            this.ChkFile17.TabIndex = 97;
            this.ChkFile17.ToolTip = "Remove filter";
            this.ChkFile17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile17.ToolTipTitle = "Run System";
            this.ChkFile17.CheckedChanged += new System.EventHandler(this.ChkFile17_CheckedChanged);
            // 
            // PbUpload17
            // 
            this.PbUpload17.Location = new System.Drawing.Point(78, 274);
            this.PbUpload17.Name = "PbUpload17";
            this.PbUpload17.Size = new System.Drawing.Size(320, 10);
            this.PbUpload17.TabIndex = 95;
            // 
            // TxtFile17
            // 
            this.TxtFile17.EnterMoveNextControl = true;
            this.TxtFile17.Location = new System.Drawing.Point(78, 252);
            this.TxtFile17.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile17.Name = "TxtFile17";
            this.TxtFile17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile17.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile17.Properties.Appearance.Options.UseFont = true;
            this.TxtFile17.Properties.MaxLength = 16;
            this.TxtFile17.Size = new System.Drawing.Size(234, 20);
            this.TxtFile17.TabIndex = 93;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(27, 256);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 14);
            this.label16.TabIndex = 92;
            this.label16.Text = "File 17";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload17
            // 
            this.BtnUpload17.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload17.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload17.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload17.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload17.Appearance.Options.UseBackColor = true;
            this.BtnUpload17.Appearance.Options.UseFont = true;
            this.BtnUpload17.Appearance.Options.UseForeColor = true;
            this.BtnUpload17.Appearance.Options.UseTextOptions = true;
            this.BtnUpload17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload17.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload17.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload17.Image")));
            this.BtnUpload17.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload17.Location = new System.Drawing.Point(342, 251);
            this.BtnUpload17.Name = "BtnUpload17";
            this.BtnUpload17.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload17.TabIndex = 94;
            this.BtnUpload17.ToolTip = "BrowseFile";
            this.BtnUpload17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload17.ToolTipTitle = "Run System";
            this.BtnUpload17.Click += new System.EventHandler(this.BtnUpload17_Click);
            // 
            // ChkFile16
            // 
            this.ChkFile16.Location = new System.Drawing.Point(313, 217);
            this.ChkFile16.Name = "ChkFile16";
            this.ChkFile16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile16.Properties.Appearance.Options.UseFont = true;
            this.ChkFile16.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile16.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile16.Properties.Caption = " ";
            this.ChkFile16.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile16.Size = new System.Drawing.Size(20, 22);
            this.ChkFile16.TabIndex = 91;
            this.ChkFile16.ToolTip = "Remove filter";
            this.ChkFile16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile16.ToolTipTitle = "Run System";
            this.ChkFile16.CheckedChanged += new System.EventHandler(this.ChkFile16_CheckedChanged);
            // 
            // PbUpload16
            // 
            this.PbUpload16.Location = new System.Drawing.Point(78, 240);
            this.PbUpload16.Name = "PbUpload16";
            this.PbUpload16.Size = new System.Drawing.Size(320, 10);
            this.PbUpload16.TabIndex = 89;
            // 
            // TxtFile16
            // 
            this.TxtFile16.EnterMoveNextControl = true;
            this.TxtFile16.Location = new System.Drawing.Point(78, 218);
            this.TxtFile16.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile16.Name = "TxtFile16";
            this.TxtFile16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile16.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile16.Properties.Appearance.Options.UseFont = true;
            this.TxtFile16.Properties.MaxLength = 16;
            this.TxtFile16.Size = new System.Drawing.Size(234, 20);
            this.TxtFile16.TabIndex = 87;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(27, 222);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 14);
            this.label13.TabIndex = 86;
            this.label13.Text = "File 16";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload16
            // 
            this.BtnUpload16.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload16.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload16.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload16.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload16.Appearance.Options.UseBackColor = true;
            this.BtnUpload16.Appearance.Options.UseFont = true;
            this.BtnUpload16.Appearance.Options.UseForeColor = true;
            this.BtnUpload16.Appearance.Options.UseTextOptions = true;
            this.BtnUpload16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload16.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload16.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload16.Image")));
            this.BtnUpload16.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload16.Location = new System.Drawing.Point(341, 217);
            this.BtnUpload16.Name = "BtnUpload16";
            this.BtnUpload16.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload16.TabIndex = 88;
            this.BtnUpload16.ToolTip = "BrowseFile";
            this.BtnUpload16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload16.ToolTipTitle = "Run System";
            this.BtnUpload16.Click += new System.EventHandler(this.BtnUpload16_Click);
            // 
            // ChkFile15
            // 
            this.ChkFile15.Location = new System.Drawing.Point(314, 182);
            this.ChkFile15.Name = "ChkFile15";
            this.ChkFile15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile15.Properties.Appearance.Options.UseFont = true;
            this.ChkFile15.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile15.Properties.Caption = " ";
            this.ChkFile15.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile15.Size = new System.Drawing.Size(20, 22);
            this.ChkFile15.TabIndex = 85;
            this.ChkFile15.ToolTip = "Remove filter";
            this.ChkFile15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile15.ToolTipTitle = "Run System";
            this.ChkFile15.CheckedChanged += new System.EventHandler(this.ChkFile15_CheckedChanged);
            // 
            // PbUpload15
            // 
            this.PbUpload15.Location = new System.Drawing.Point(78, 205);
            this.PbUpload15.Name = "PbUpload15";
            this.PbUpload15.Size = new System.Drawing.Size(320, 10);
            this.PbUpload15.TabIndex = 83;
            // 
            // TxtFile15
            // 
            this.TxtFile15.EnterMoveNextControl = true;
            this.TxtFile15.Location = new System.Drawing.Point(78, 183);
            this.TxtFile15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile15.Name = "TxtFile15";
            this.TxtFile15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile15.Properties.Appearance.Options.UseFont = true;
            this.TxtFile15.Properties.MaxLength = 16;
            this.TxtFile15.Size = new System.Drawing.Size(234, 20);
            this.TxtFile15.TabIndex = 81;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(27, 187);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 14);
            this.label14.TabIndex = 80;
            this.label14.Text = "File 15";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload15
            // 
            this.BtnUpload15.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload15.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload15.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload15.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload15.Appearance.Options.UseBackColor = true;
            this.BtnUpload15.Appearance.Options.UseFont = true;
            this.BtnUpload15.Appearance.Options.UseForeColor = true;
            this.BtnUpload15.Appearance.Options.UseTextOptions = true;
            this.BtnUpload15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload15.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload15.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload15.Image")));
            this.BtnUpload15.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload15.Location = new System.Drawing.Point(342, 182);
            this.BtnUpload15.Name = "BtnUpload15";
            this.BtnUpload15.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload15.TabIndex = 82;
            this.BtnUpload15.ToolTip = "BrowseFile";
            this.BtnUpload15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload15.ToolTipTitle = "Run System";
            this.BtnUpload15.Click += new System.EventHandler(this.BtnUpload15_Click);
            // 
            // ChkFile14
            // 
            this.ChkFile14.Location = new System.Drawing.Point(314, 148);
            this.ChkFile14.Name = "ChkFile14";
            this.ChkFile14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile14.Properties.Appearance.Options.UseFont = true;
            this.ChkFile14.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile14.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile14.Properties.Caption = " ";
            this.ChkFile14.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile14.Size = new System.Drawing.Size(20, 22);
            this.ChkFile14.TabIndex = 79;
            this.ChkFile14.ToolTip = "Remove filter";
            this.ChkFile14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile14.ToolTipTitle = "Run System";
            this.ChkFile14.CheckedChanged += new System.EventHandler(this.ChkFile14_CheckedChanged);
            // 
            // PbUpload14
            // 
            this.PbUpload14.Location = new System.Drawing.Point(78, 171);
            this.PbUpload14.Name = "PbUpload14";
            this.PbUpload14.Size = new System.Drawing.Size(320, 10);
            this.PbUpload14.TabIndex = 77;
            // 
            // TxtFile14
            // 
            this.TxtFile14.EnterMoveNextControl = true;
            this.TxtFile14.Location = new System.Drawing.Point(78, 149);
            this.TxtFile14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile14.Name = "TxtFile14";
            this.TxtFile14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile14.Properties.Appearance.Options.UseFont = true;
            this.TxtFile14.Properties.MaxLength = 16;
            this.TxtFile14.Size = new System.Drawing.Size(234, 20);
            this.TxtFile14.TabIndex = 75;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(27, 153);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 14);
            this.label11.TabIndex = 74;
            this.label11.Text = "File 14";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload14
            // 
            this.BtnUpload14.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload14.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload14.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload14.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload14.Appearance.Options.UseBackColor = true;
            this.BtnUpload14.Appearance.Options.UseFont = true;
            this.BtnUpload14.Appearance.Options.UseForeColor = true;
            this.BtnUpload14.Appearance.Options.UseTextOptions = true;
            this.BtnUpload14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload14.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload14.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload14.Image")));
            this.BtnUpload14.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload14.Location = new System.Drawing.Point(342, 148);
            this.BtnUpload14.Name = "BtnUpload14";
            this.BtnUpload14.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload14.TabIndex = 76;
            this.BtnUpload14.ToolTip = "BrowseFile";
            this.BtnUpload14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload14.ToolTipTitle = "Run System";
            this.BtnUpload14.Click += new System.EventHandler(this.BtnUpload14_Click);
            // 
            // ChkFile13
            // 
            this.ChkFile13.Location = new System.Drawing.Point(315, 113);
            this.ChkFile13.Name = "ChkFile13";
            this.ChkFile13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile13.Properties.Appearance.Options.UseFont = true;
            this.ChkFile13.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile13.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile13.Properties.Caption = " ";
            this.ChkFile13.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile13.Size = new System.Drawing.Size(20, 22);
            this.ChkFile13.TabIndex = 73;
            this.ChkFile13.ToolTip = "Remove filter";
            this.ChkFile13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile13.ToolTipTitle = "Run System";
            this.ChkFile13.CheckedChanged += new System.EventHandler(this.ChkFile13_CheckedChanged);
            // 
            // PbUpload13
            // 
            this.PbUpload13.Location = new System.Drawing.Point(78, 136);
            this.PbUpload13.Name = "PbUpload13";
            this.PbUpload13.Size = new System.Drawing.Size(320, 10);
            this.PbUpload13.TabIndex = 71;
            // 
            // TxtFile13
            // 
            this.TxtFile13.EnterMoveNextControl = true;
            this.TxtFile13.Location = new System.Drawing.Point(78, 114);
            this.TxtFile13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile13.Name = "TxtFile13";
            this.TxtFile13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile13.Properties.Appearance.Options.UseFont = true;
            this.TxtFile13.Properties.MaxLength = 16;
            this.TxtFile13.Size = new System.Drawing.Size(234, 20);
            this.TxtFile13.TabIndex = 69;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(27, 118);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 14);
            this.label12.TabIndex = 68;
            this.label12.Text = "File 13";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload13
            // 
            this.BtnUpload13.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload13.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload13.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload13.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload13.Appearance.Options.UseBackColor = true;
            this.BtnUpload13.Appearance.Options.UseFont = true;
            this.BtnUpload13.Appearance.Options.UseForeColor = true;
            this.BtnUpload13.Appearance.Options.UseTextOptions = true;
            this.BtnUpload13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload13.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload13.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload13.Image")));
            this.BtnUpload13.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload13.Location = new System.Drawing.Point(343, 113);
            this.BtnUpload13.Name = "BtnUpload13";
            this.BtnUpload13.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload13.TabIndex = 70;
            this.BtnUpload13.ToolTip = "BrowseFile";
            this.BtnUpload13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload13.ToolTipTitle = "Run System";
            this.BtnUpload13.Click += new System.EventHandler(this.BtnUpload13_Click);
            // 
            // ChkFile12
            // 
            this.ChkFile12.Location = new System.Drawing.Point(313, 78);
            this.ChkFile12.Name = "ChkFile12";
            this.ChkFile12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile12.Properties.Appearance.Options.UseFont = true;
            this.ChkFile12.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile12.Properties.Caption = " ";
            this.ChkFile12.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile12.Size = new System.Drawing.Size(20, 22);
            this.ChkFile12.TabIndex = 67;
            this.ChkFile12.ToolTip = "Remove filter";
            this.ChkFile12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile12.ToolTipTitle = "Run System";
            this.ChkFile12.CheckedChanged += new System.EventHandler(this.ChkFile12_CheckedChanged);
            // 
            // PbUpload12
            // 
            this.PbUpload12.Location = new System.Drawing.Point(78, 101);
            this.PbUpload12.Name = "PbUpload12";
            this.PbUpload12.Size = new System.Drawing.Size(320, 10);
            this.PbUpload12.TabIndex = 65;
            // 
            // TxtFile12
            // 
            this.TxtFile12.EnterMoveNextControl = true;
            this.TxtFile12.Location = new System.Drawing.Point(78, 79);
            this.TxtFile12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile12.Name = "TxtFile12";
            this.TxtFile12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile12.Properties.Appearance.Options.UseFont = true;
            this.TxtFile12.Properties.MaxLength = 16;
            this.TxtFile12.Size = new System.Drawing.Size(234, 20);
            this.TxtFile12.TabIndex = 63;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(27, 83);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 14);
            this.label9.TabIndex = 62;
            this.label9.Text = "File 12";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload12
            // 
            this.BtnUpload12.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload12.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload12.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload12.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload12.Appearance.Options.UseBackColor = true;
            this.BtnUpload12.Appearance.Options.UseFont = true;
            this.BtnUpload12.Appearance.Options.UseForeColor = true;
            this.BtnUpload12.Appearance.Options.UseTextOptions = true;
            this.BtnUpload12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload12.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload12.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload12.Image")));
            this.BtnUpload12.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload12.Location = new System.Drawing.Point(341, 78);
            this.BtnUpload12.Name = "BtnUpload12";
            this.BtnUpload12.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload12.TabIndex = 64;
            this.BtnUpload12.ToolTip = "BrowseFile";
            this.BtnUpload12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload12.ToolTipTitle = "Run System";
            this.BtnUpload12.Click += new System.EventHandler(this.BtnUpload12_Click);
            // 
            // ChkFile11
            // 
            this.ChkFile11.Location = new System.Drawing.Point(313, 42);
            this.ChkFile11.Name = "ChkFile11";
            this.ChkFile11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile11.Properties.Appearance.Options.UseFont = true;
            this.ChkFile11.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile11.Properties.Caption = " ";
            this.ChkFile11.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile11.Size = new System.Drawing.Size(20, 22);
            this.ChkFile11.TabIndex = 61;
            this.ChkFile11.ToolTip = "Remove filter";
            this.ChkFile11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile11.ToolTipTitle = "Run System";
            this.ChkFile11.CheckedChanged += new System.EventHandler(this.ChkFile11_CheckedChanged);
            // 
            // PbUpload11
            // 
            this.PbUpload11.Location = new System.Drawing.Point(78, 65);
            this.PbUpload11.Name = "PbUpload11";
            this.PbUpload11.Size = new System.Drawing.Size(320, 10);
            this.PbUpload11.TabIndex = 59;
            // 
            // TxtFile11
            // 
            this.TxtFile11.EnterMoveNextControl = true;
            this.TxtFile11.Location = new System.Drawing.Point(78, 43);
            this.TxtFile11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile11.Name = "TxtFile11";
            this.TxtFile11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile11.Properties.Appearance.Options.UseFont = true;
            this.TxtFile11.Properties.MaxLength = 16;
            this.TxtFile11.Size = new System.Drawing.Size(234, 20);
            this.TxtFile11.TabIndex = 57;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(27, 48);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 14);
            this.label10.TabIndex = 56;
            this.label10.Text = "File 11";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload11
            // 
            this.BtnUpload11.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload11.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload11.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload11.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload11.Appearance.Options.UseBackColor = true;
            this.BtnUpload11.Appearance.Options.UseFont = true;
            this.BtnUpload11.Appearance.Options.UseForeColor = true;
            this.BtnUpload11.Appearance.Options.UseTextOptions = true;
            this.BtnUpload11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload11.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload11.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload11.Image")));
            this.BtnUpload11.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload11.Location = new System.Drawing.Point(341, 42);
            this.BtnUpload11.Name = "BtnUpload11";
            this.BtnUpload11.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload11.TabIndex = 58;
            this.BtnUpload11.ToolTip = "BrowseFile";
            this.BtnUpload11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload11.ToolTipTitle = "Run System";
            this.BtnUpload11.Click += new System.EventHandler(this.BtnUpload11_Click);
            // 
            // ChkFile2
            // 
            this.ChkFile2.Location = new System.Drawing.Point(319, 84);
            this.ChkFile2.Name = "ChkFile2";
            this.ChkFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile2.Properties.Appearance.Options.UseFont = true;
            this.ChkFile2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile2.Properties.Caption = " ";
            this.ChkFile2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile2.Size = new System.Drawing.Size(20, 22);
            this.ChkFile2.TabIndex = 55;
            this.ChkFile2.ToolTip = "Remove filter";
            this.ChkFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile2.ToolTipTitle = "Run System";
            this.ChkFile2.CheckedChanged += new System.EventHandler(this.ChkFile2_CheckedChanged);
            // 
            // BtnDownload2
            // 
            this.BtnDownload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload2.Appearance.Options.UseBackColor = true;
            this.BtnDownload2.Appearance.Options.UseFont = true;
            this.BtnDownload2.Appearance.Options.UseForeColor = true;
            this.BtnDownload2.Appearance.Options.UseTextOptions = true;
            this.BtnDownload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload2.Image")));
            this.BtnDownload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload2.Location = new System.Drawing.Point(378, 84);
            this.BtnDownload2.Name = "BtnDownload2";
            this.BtnDownload2.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload2.TabIndex = 54;
            this.BtnDownload2.ToolTip = "DownloadFile";
            this.BtnDownload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload2.ToolTipTitle = "Run System";
            this.BtnDownload2.Click += new System.EventHandler(this.BtnDownload2_Click);
            // 
            // PbUpload2
            // 
            this.PbUpload2.Location = new System.Drawing.Point(83, 107);
            this.PbUpload2.Name = "PbUpload2";
            this.PbUpload2.Size = new System.Drawing.Size(320, 10);
            this.PbUpload2.TabIndex = 53;
            // 
            // TxtFile2
            // 
            this.TxtFile2.EnterMoveNextControl = true;
            this.TxtFile2.Location = new System.Drawing.Point(83, 85);
            this.TxtFile2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile2.Name = "TxtFile2";
            this.TxtFile2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile2.Properties.Appearance.Options.UseFont = true;
            this.TxtFile2.Properties.MaxLength = 16;
            this.TxtFile2.Size = new System.Drawing.Size(234, 20);
            this.TxtFile2.TabIndex = 51;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(43, 89);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 50;
            this.label1.Text = "File 2";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload2
            // 
            this.BtnUpload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload2.Appearance.Options.UseBackColor = true;
            this.BtnUpload2.Appearance.Options.UseFont = true;
            this.BtnUpload2.Appearance.Options.UseForeColor = true;
            this.BtnUpload2.Appearance.Options.UseTextOptions = true;
            this.BtnUpload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload2.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload2.Image")));
            this.BtnUpload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload2.Location = new System.Drawing.Point(347, 84);
            this.BtnUpload2.Name = "BtnUpload2";
            this.BtnUpload2.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload2.TabIndex = 52;
            this.BtnUpload2.ToolTip = "BrowseFile";
            this.BtnUpload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload2.ToolTipTitle = "Run System";
            this.BtnUpload2.Click += new System.EventHandler(this.BtnUpload2_Click);
            // 
            // ChkFile4
            // 
            this.ChkFile4.Location = new System.Drawing.Point(320, 153);
            this.ChkFile4.Name = "ChkFile4";
            this.ChkFile4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile4.Properties.Appearance.Options.UseFont = true;
            this.ChkFile4.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile4.Properties.Caption = " ";
            this.ChkFile4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile4.Size = new System.Drawing.Size(20, 22);
            this.ChkFile4.TabIndex = 67;
            this.ChkFile4.ToolTip = "Remove filter";
            this.ChkFile4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile4.ToolTipTitle = "Run System";
            this.ChkFile4.CheckedChanged += new System.EventHandler(this.ChkFile4_CheckedChanged);
            // 
            // BtnDownload4
            // 
            this.BtnDownload4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload4.Appearance.Options.UseBackColor = true;
            this.BtnDownload4.Appearance.Options.UseFont = true;
            this.BtnDownload4.Appearance.Options.UseForeColor = true;
            this.BtnDownload4.Appearance.Options.UseTextOptions = true;
            this.BtnDownload4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload4.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload4.Image")));
            this.BtnDownload4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload4.Location = new System.Drawing.Point(379, 153);
            this.BtnDownload4.Name = "BtnDownload4";
            this.BtnDownload4.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload4.TabIndex = 66;
            this.BtnDownload4.ToolTip = "DownloadFile";
            this.BtnDownload4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload4.ToolTipTitle = "Run System";
            this.BtnDownload4.Click += new System.EventHandler(this.BtnDownload4_Click);
            // 
            // PbUpload4
            // 
            this.PbUpload4.Location = new System.Drawing.Point(84, 176);
            this.PbUpload4.Name = "PbUpload4";
            this.PbUpload4.Size = new System.Drawing.Size(320, 10);
            this.PbUpload4.TabIndex = 65;
            // 
            // TxtFile4
            // 
            this.TxtFile4.EnterMoveNextControl = true;
            this.TxtFile4.Location = new System.Drawing.Point(84, 154);
            this.TxtFile4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile4.Name = "TxtFile4";
            this.TxtFile4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile4.Properties.Appearance.Options.UseFont = true;
            this.TxtFile4.Properties.MaxLength = 16;
            this.TxtFile4.Size = new System.Drawing.Size(234, 20);
            this.TxtFile4.TabIndex = 63;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(43, 158);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 14);
            this.label2.TabIndex = 62;
            this.label2.Text = "File 4";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload4
            // 
            this.BtnUpload4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload4.Appearance.Options.UseBackColor = true;
            this.BtnUpload4.Appearance.Options.UseFont = true;
            this.BtnUpload4.Appearance.Options.UseForeColor = true;
            this.BtnUpload4.Appearance.Options.UseTextOptions = true;
            this.BtnUpload4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload4.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload4.Image")));
            this.BtnUpload4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload4.Location = new System.Drawing.Point(348, 153);
            this.BtnUpload4.Name = "BtnUpload4";
            this.BtnUpload4.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload4.TabIndex = 64;
            this.BtnUpload4.ToolTip = "BrowseFile";
            this.BtnUpload4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload4.ToolTipTitle = "Run System";
            this.BtnUpload4.Click += new System.EventHandler(this.BtnUpload4_Click);
            // 
            // ChkFile3
            // 
            this.ChkFile3.Location = new System.Drawing.Point(321, 119);
            this.ChkFile3.Name = "ChkFile3";
            this.ChkFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile3.Properties.Appearance.Options.UseFont = true;
            this.ChkFile3.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile3.Properties.Caption = " ";
            this.ChkFile3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile3.Size = new System.Drawing.Size(20, 22);
            this.ChkFile3.TabIndex = 61;
            this.ChkFile3.ToolTip = "Remove filter";
            this.ChkFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile3.ToolTipTitle = "Run System";
            this.ChkFile3.CheckedChanged += new System.EventHandler(this.ChkFile3_CheckedChanged);
            // 
            // BtnDownload3
            // 
            this.BtnDownload3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload3.Appearance.Options.UseBackColor = true;
            this.BtnDownload3.Appearance.Options.UseFont = true;
            this.BtnDownload3.Appearance.Options.UseForeColor = true;
            this.BtnDownload3.Appearance.Options.UseTextOptions = true;
            this.BtnDownload3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload3.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload3.Image")));
            this.BtnDownload3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload3.Location = new System.Drawing.Point(380, 119);
            this.BtnDownload3.Name = "BtnDownload3";
            this.BtnDownload3.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload3.TabIndex = 60;
            this.BtnDownload3.ToolTip = "DownloadFile";
            this.BtnDownload3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload3.ToolTipTitle = "Run System";
            this.BtnDownload3.Click += new System.EventHandler(this.BtnDownload3_Click);
            // 
            // PbUpload3
            // 
            this.PbUpload3.Location = new System.Drawing.Point(84, 142);
            this.PbUpload3.Name = "PbUpload3";
            this.PbUpload3.Size = new System.Drawing.Size(320, 10);
            this.PbUpload3.TabIndex = 59;
            // 
            // TxtFile3
            // 
            this.TxtFile3.EnterMoveNextControl = true;
            this.TxtFile3.Location = new System.Drawing.Point(84, 120);
            this.TxtFile3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile3.Name = "TxtFile3";
            this.TxtFile3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile3.Properties.Appearance.Options.UseFont = true;
            this.TxtFile3.Properties.MaxLength = 16;
            this.TxtFile3.Size = new System.Drawing.Size(234, 20);
            this.TxtFile3.TabIndex = 57;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(43, 124);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 14);
            this.label3.TabIndex = 56;
            this.label3.Text = "File 3";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload3
            // 
            this.BtnUpload3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload3.Appearance.Options.UseBackColor = true;
            this.BtnUpload3.Appearance.Options.UseFont = true;
            this.BtnUpload3.Appearance.Options.UseForeColor = true;
            this.BtnUpload3.Appearance.Options.UseTextOptions = true;
            this.BtnUpload3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload3.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload3.Image")));
            this.BtnUpload3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload3.Location = new System.Drawing.Point(349, 119);
            this.BtnUpload3.Name = "BtnUpload3";
            this.BtnUpload3.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload3.TabIndex = 58;
            this.BtnUpload3.ToolTip = "BrowseFile";
            this.BtnUpload3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload3.ToolTipTitle = "Run System";
            this.BtnUpload3.Click += new System.EventHandler(this.BtnUpload3_Click);
            // 
            // ChkFile6
            // 
            this.ChkFile6.Location = new System.Drawing.Point(320, 221);
            this.ChkFile6.Name = "ChkFile6";
            this.ChkFile6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile6.Properties.Appearance.Options.UseFont = true;
            this.ChkFile6.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile6.Properties.Caption = " ";
            this.ChkFile6.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile6.Size = new System.Drawing.Size(20, 22);
            this.ChkFile6.TabIndex = 79;
            this.ChkFile6.ToolTip = "Remove filter";
            this.ChkFile6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile6.ToolTipTitle = "Run System";
            this.ChkFile6.CheckedChanged += new System.EventHandler(this.ChkFile6_CheckedChanged);
            // 
            // BtnDownload6
            // 
            this.BtnDownload6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload6.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload6.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload6.Appearance.Options.UseBackColor = true;
            this.BtnDownload6.Appearance.Options.UseFont = true;
            this.BtnDownload6.Appearance.Options.UseForeColor = true;
            this.BtnDownload6.Appearance.Options.UseTextOptions = true;
            this.BtnDownload6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload6.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload6.Image")));
            this.BtnDownload6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload6.Location = new System.Drawing.Point(379, 221);
            this.BtnDownload6.Name = "BtnDownload6";
            this.BtnDownload6.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload6.TabIndex = 78;
            this.BtnDownload6.ToolTip = "DownloadFile";
            this.BtnDownload6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload6.ToolTipTitle = "Run System";
            this.BtnDownload6.Click += new System.EventHandler(this.BtnDownload6_Click);
            // 
            // PbUpload6
            // 
            this.PbUpload6.Location = new System.Drawing.Point(84, 244);
            this.PbUpload6.Name = "PbUpload6";
            this.PbUpload6.Size = new System.Drawing.Size(320, 10);
            this.PbUpload6.TabIndex = 77;
            // 
            // TxtFile6
            // 
            this.TxtFile6.EnterMoveNextControl = true;
            this.TxtFile6.Location = new System.Drawing.Point(84, 222);
            this.TxtFile6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile6.Name = "TxtFile6";
            this.TxtFile6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile6.Properties.Appearance.Options.UseFont = true;
            this.TxtFile6.Properties.MaxLength = 16;
            this.TxtFile6.Size = new System.Drawing.Size(234, 20);
            this.TxtFile6.TabIndex = 75;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(43, 226);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 14);
            this.label4.TabIndex = 74;
            this.label4.Text = "File 6";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload6
            // 
            this.BtnUpload6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload6.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload6.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload6.Appearance.Options.UseBackColor = true;
            this.BtnUpload6.Appearance.Options.UseFont = true;
            this.BtnUpload6.Appearance.Options.UseForeColor = true;
            this.BtnUpload6.Appearance.Options.UseTextOptions = true;
            this.BtnUpload6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload6.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload6.Image")));
            this.BtnUpload6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload6.Location = new System.Drawing.Point(348, 221);
            this.BtnUpload6.Name = "BtnUpload6";
            this.BtnUpload6.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload6.TabIndex = 76;
            this.BtnUpload6.ToolTip = "BrowseFile";
            this.BtnUpload6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload6.ToolTipTitle = "Run System";
            this.BtnUpload6.Click += new System.EventHandler(this.BtnUpload6_Click);
            // 
            // ChkFile5
            // 
            this.ChkFile5.Location = new System.Drawing.Point(321, 187);
            this.ChkFile5.Name = "ChkFile5";
            this.ChkFile5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile5.Properties.Appearance.Options.UseFont = true;
            this.ChkFile5.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile5.Properties.Caption = " ";
            this.ChkFile5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile5.Size = new System.Drawing.Size(20, 22);
            this.ChkFile5.TabIndex = 73;
            this.ChkFile5.ToolTip = "Remove filter";
            this.ChkFile5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile5.ToolTipTitle = "Run System";
            this.ChkFile5.CheckedChanged += new System.EventHandler(this.ChkFile5_CheckedChanged);
            // 
            // BtnDownload5
            // 
            this.BtnDownload5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload5.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload5.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload5.Appearance.Options.UseBackColor = true;
            this.BtnDownload5.Appearance.Options.UseFont = true;
            this.BtnDownload5.Appearance.Options.UseForeColor = true;
            this.BtnDownload5.Appearance.Options.UseTextOptions = true;
            this.BtnDownload5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload5.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload5.Image")));
            this.BtnDownload5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload5.Location = new System.Drawing.Point(380, 187);
            this.BtnDownload5.Name = "BtnDownload5";
            this.BtnDownload5.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload5.TabIndex = 72;
            this.BtnDownload5.ToolTip = "DownloadFile";
            this.BtnDownload5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload5.ToolTipTitle = "Run System";
            this.BtnDownload5.Click += new System.EventHandler(this.BtnDownload5_Click);
            // 
            // PbUpload5
            // 
            this.PbUpload5.Location = new System.Drawing.Point(84, 210);
            this.PbUpload5.Name = "PbUpload5";
            this.PbUpload5.Size = new System.Drawing.Size(320, 10);
            this.PbUpload5.TabIndex = 71;
            // 
            // TxtFile5
            // 
            this.TxtFile5.EnterMoveNextControl = true;
            this.TxtFile5.Location = new System.Drawing.Point(84, 188);
            this.TxtFile5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile5.Name = "TxtFile5";
            this.TxtFile5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile5.Properties.Appearance.Options.UseFont = true;
            this.TxtFile5.Properties.MaxLength = 16;
            this.TxtFile5.Size = new System.Drawing.Size(234, 20);
            this.TxtFile5.TabIndex = 69;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(43, 192);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 14);
            this.label5.TabIndex = 68;
            this.label5.Text = "File 5";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload5
            // 
            this.BtnUpload5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload5.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload5.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload5.Appearance.Options.UseBackColor = true;
            this.BtnUpload5.Appearance.Options.UseFont = true;
            this.BtnUpload5.Appearance.Options.UseForeColor = true;
            this.BtnUpload5.Appearance.Options.UseTextOptions = true;
            this.BtnUpload5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload5.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload5.Image")));
            this.BtnUpload5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload5.Location = new System.Drawing.Point(349, 187);
            this.BtnUpload5.Name = "BtnUpload5";
            this.BtnUpload5.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload5.TabIndex = 70;
            this.BtnUpload5.ToolTip = "BrowseFile";
            this.BtnUpload5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload5.ToolTipTitle = "Run System";
            this.BtnUpload5.Click += new System.EventHandler(this.BtnUpload5_Click);
            // 
            // ChkFile8
            // 
            this.ChkFile8.Location = new System.Drawing.Point(319, 289);
            this.ChkFile8.Name = "ChkFile8";
            this.ChkFile8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile8.Properties.Appearance.Options.UseFont = true;
            this.ChkFile8.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile8.Properties.Caption = " ";
            this.ChkFile8.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile8.Size = new System.Drawing.Size(20, 22);
            this.ChkFile8.TabIndex = 91;
            this.ChkFile8.ToolTip = "Remove filter";
            this.ChkFile8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile8.ToolTipTitle = "Run System";
            this.ChkFile8.CheckedChanged += new System.EventHandler(this.ChkFile8_CheckedChanged);
            // 
            // BtnDownload8
            // 
            this.BtnDownload8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload8.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload8.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload8.Appearance.Options.UseBackColor = true;
            this.BtnDownload8.Appearance.Options.UseFont = true;
            this.BtnDownload8.Appearance.Options.UseForeColor = true;
            this.BtnDownload8.Appearance.Options.UseTextOptions = true;
            this.BtnDownload8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload8.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload8.Image")));
            this.BtnDownload8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload8.Location = new System.Drawing.Point(378, 289);
            this.BtnDownload8.Name = "BtnDownload8";
            this.BtnDownload8.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload8.TabIndex = 90;
            this.BtnDownload8.ToolTip = "DownloadFile";
            this.BtnDownload8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload8.ToolTipTitle = "Run System";
            this.BtnDownload8.Click += new System.EventHandler(this.BtnDownload8_Click);
            // 
            // PbUpload8
            // 
            this.PbUpload8.Location = new System.Drawing.Point(83, 312);
            this.PbUpload8.Name = "PbUpload8";
            this.PbUpload8.Size = new System.Drawing.Size(320, 10);
            this.PbUpload8.TabIndex = 89;
            // 
            // TxtFile8
            // 
            this.TxtFile8.EnterMoveNextControl = true;
            this.TxtFile8.Location = new System.Drawing.Point(83, 290);
            this.TxtFile8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile8.Name = "TxtFile8";
            this.TxtFile8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile8.Properties.Appearance.Options.UseFont = true;
            this.TxtFile8.Properties.MaxLength = 16;
            this.TxtFile8.Size = new System.Drawing.Size(234, 20);
            this.TxtFile8.TabIndex = 87;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(43, 294);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 14);
            this.label7.TabIndex = 86;
            this.label7.Text = "File 8";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload8
            // 
            this.BtnUpload8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload8.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload8.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload8.Appearance.Options.UseBackColor = true;
            this.BtnUpload8.Appearance.Options.UseFont = true;
            this.BtnUpload8.Appearance.Options.UseForeColor = true;
            this.BtnUpload8.Appearance.Options.UseTextOptions = true;
            this.BtnUpload8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload8.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload8.Image")));
            this.BtnUpload8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload8.Location = new System.Drawing.Point(347, 289);
            this.BtnUpload8.Name = "BtnUpload8";
            this.BtnUpload8.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload8.TabIndex = 88;
            this.BtnUpload8.ToolTip = "BrowseFile";
            this.BtnUpload8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload8.ToolTipTitle = "Run System";
            this.BtnUpload8.Click += new System.EventHandler(this.BtnUpload8_Click);
            // 
            // ChkFile7
            // 
            this.ChkFile7.Location = new System.Drawing.Point(320, 255);
            this.ChkFile7.Name = "ChkFile7";
            this.ChkFile7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile7.Properties.Appearance.Options.UseFont = true;
            this.ChkFile7.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile7.Properties.Caption = " ";
            this.ChkFile7.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile7.Size = new System.Drawing.Size(20, 22);
            this.ChkFile7.TabIndex = 85;
            this.ChkFile7.ToolTip = "Remove filter";
            this.ChkFile7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile7.ToolTipTitle = "Run System";
            this.ChkFile7.CheckedChanged += new System.EventHandler(this.ChkFile7_CheckedChanged);
            // 
            // BtnDownload7
            // 
            this.BtnDownload7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload7.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload7.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload7.Appearance.Options.UseBackColor = true;
            this.BtnDownload7.Appearance.Options.UseFont = true;
            this.BtnDownload7.Appearance.Options.UseForeColor = true;
            this.BtnDownload7.Appearance.Options.UseTextOptions = true;
            this.BtnDownload7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload7.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload7.Image")));
            this.BtnDownload7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload7.Location = new System.Drawing.Point(379, 255);
            this.BtnDownload7.Name = "BtnDownload7";
            this.BtnDownload7.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload7.TabIndex = 84;
            this.BtnDownload7.ToolTip = "DownloadFile";
            this.BtnDownload7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload7.ToolTipTitle = "Run System";
            this.BtnDownload7.Click += new System.EventHandler(this.BtnDownload7_Click);
            // 
            // PbUpload7
            // 
            this.PbUpload7.Location = new System.Drawing.Point(83, 278);
            this.PbUpload7.Name = "PbUpload7";
            this.PbUpload7.Size = new System.Drawing.Size(320, 10);
            this.PbUpload7.TabIndex = 83;
            // 
            // TxtFile7
            // 
            this.TxtFile7.EnterMoveNextControl = true;
            this.TxtFile7.Location = new System.Drawing.Point(83, 256);
            this.TxtFile7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile7.Name = "TxtFile7";
            this.TxtFile7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile7.Properties.Appearance.Options.UseFont = true;
            this.TxtFile7.Properties.MaxLength = 16;
            this.TxtFile7.Size = new System.Drawing.Size(234, 20);
            this.TxtFile7.TabIndex = 81;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(43, 260);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 14);
            this.label8.TabIndex = 80;
            this.label8.Text = "File 7";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload7
            // 
            this.BtnUpload7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload7.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload7.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload7.Appearance.Options.UseBackColor = true;
            this.BtnUpload7.Appearance.Options.UseFont = true;
            this.BtnUpload7.Appearance.Options.UseForeColor = true;
            this.BtnUpload7.Appearance.Options.UseTextOptions = true;
            this.BtnUpload7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload7.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload7.Image")));
            this.BtnUpload7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload7.Location = new System.Drawing.Point(348, 255);
            this.BtnUpload7.Name = "BtnUpload7";
            this.BtnUpload7.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload7.TabIndex = 82;
            this.BtnUpload7.ToolTip = "BrowseFile";
            this.BtnUpload7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload7.ToolTipTitle = "Run System";
            this.BtnUpload7.Click += new System.EventHandler(this.BtnUpload7_Click);
            // 
            // OD1
            // 
            this.OD1.FileName = "NamaFile";
            // 
            // ChkFile10
            // 
            this.ChkFile10.Location = new System.Drawing.Point(319, 358);
            this.ChkFile10.Name = "ChkFile10";
            this.ChkFile10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile10.Properties.Appearance.Options.UseFont = true;
            this.ChkFile10.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile10.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile10.Properties.Caption = " ";
            this.ChkFile10.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile10.Size = new System.Drawing.Size(20, 22);
            this.ChkFile10.TabIndex = 103;
            this.ChkFile10.ToolTip = "Remove filter";
            this.ChkFile10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile10.ToolTipTitle = "Run System";
            this.ChkFile10.CheckedChanged += new System.EventHandler(this.ChkFile10_CheckedChanged);
            // 
            // BtnDownload10
            // 
            this.BtnDownload10.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload10.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload10.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload10.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload10.Appearance.Options.UseBackColor = true;
            this.BtnDownload10.Appearance.Options.UseFont = true;
            this.BtnDownload10.Appearance.Options.UseForeColor = true;
            this.BtnDownload10.Appearance.Options.UseTextOptions = true;
            this.BtnDownload10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload10.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload10.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload10.Image")));
            this.BtnDownload10.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload10.Location = new System.Drawing.Point(378, 358);
            this.BtnDownload10.Name = "BtnDownload10";
            this.BtnDownload10.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload10.TabIndex = 102;
            this.BtnDownload10.ToolTip = "DownloadFile";
            this.BtnDownload10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload10.ToolTipTitle = "Run System";
            this.BtnDownload10.Click += new System.EventHandler(this.BtnDownload10_Click);
            // 
            // PbUpload10
            // 
            this.PbUpload10.Location = new System.Drawing.Point(83, 381);
            this.PbUpload10.Name = "PbUpload10";
            this.PbUpload10.Size = new System.Drawing.Size(320, 10);
            this.PbUpload10.TabIndex = 101;
            // 
            // TxtFile10
            // 
            this.TxtFile10.EnterMoveNextControl = true;
            this.TxtFile10.Location = new System.Drawing.Point(83, 359);
            this.TxtFile10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile10.Name = "TxtFile10";
            this.TxtFile10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile10.Properties.Appearance.Options.UseFont = true;
            this.TxtFile10.Properties.MaxLength = 16;
            this.TxtFile10.Size = new System.Drawing.Size(234, 20);
            this.TxtFile10.TabIndex = 99;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(36, 363);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(42, 14);
            this.label21.TabIndex = 98;
            this.label21.Text = "File 10";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload10
            // 
            this.BtnUpload10.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload10.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload10.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload10.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload10.Appearance.Options.UseBackColor = true;
            this.BtnUpload10.Appearance.Options.UseFont = true;
            this.BtnUpload10.Appearance.Options.UseForeColor = true;
            this.BtnUpload10.Appearance.Options.UseTextOptions = true;
            this.BtnUpload10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload10.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload10.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload10.Image")));
            this.BtnUpload10.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload10.Location = new System.Drawing.Point(347, 358);
            this.BtnUpload10.Name = "BtnUpload10";
            this.BtnUpload10.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload10.TabIndex = 100;
            this.BtnUpload10.ToolTip = "BrowseFile";
            this.BtnUpload10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload10.ToolTipTitle = "Run System";
            this.BtnUpload10.Click += new System.EventHandler(this.BtnUpload10_Click);
            // 
            // ChkFile9
            // 
            this.ChkFile9.Location = new System.Drawing.Point(320, 324);
            this.ChkFile9.Name = "ChkFile9";
            this.ChkFile9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile9.Properties.Appearance.Options.UseFont = true;
            this.ChkFile9.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile9.Properties.Caption = " ";
            this.ChkFile9.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile9.Size = new System.Drawing.Size(20, 22);
            this.ChkFile9.TabIndex = 97;
            this.ChkFile9.ToolTip = "Remove filter";
            this.ChkFile9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile9.ToolTipTitle = "Run System";
            this.ChkFile9.CheckedChanged += new System.EventHandler(this.ChkFile9_CheckedChanged);
            // 
            // BtnDownload9
            // 
            this.BtnDownload9.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload9.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload9.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload9.Appearance.Options.UseBackColor = true;
            this.BtnDownload9.Appearance.Options.UseFont = true;
            this.BtnDownload9.Appearance.Options.UseForeColor = true;
            this.BtnDownload9.Appearance.Options.UseTextOptions = true;
            this.BtnDownload9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload9.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload9.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload9.Image")));
            this.BtnDownload9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload9.Location = new System.Drawing.Point(379, 324);
            this.BtnDownload9.Name = "BtnDownload9";
            this.BtnDownload9.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload9.TabIndex = 96;
            this.BtnDownload9.ToolTip = "DownloadFile";
            this.BtnDownload9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload9.ToolTipTitle = "Run System";
            this.BtnDownload9.Click += new System.EventHandler(this.BtnDownload9_Click);
            // 
            // PbUpload9
            // 
            this.PbUpload9.Location = new System.Drawing.Point(83, 347);
            this.PbUpload9.Name = "PbUpload9";
            this.PbUpload9.Size = new System.Drawing.Size(320, 10);
            this.PbUpload9.TabIndex = 95;
            // 
            // TxtFile9
            // 
            this.TxtFile9.EnterMoveNextControl = true;
            this.TxtFile9.Location = new System.Drawing.Point(83, 325);
            this.TxtFile9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile9.Name = "TxtFile9";
            this.TxtFile9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile9.Properties.Appearance.Options.UseFont = true;
            this.TxtFile9.Properties.MaxLength = 16;
            this.TxtFile9.Size = new System.Drawing.Size(234, 20);
            this.TxtFile9.TabIndex = 93;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(43, 329);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 14);
            this.label22.TabIndex = 92;
            this.label22.Text = "File 9";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnUpload9
            // 
            this.BtnUpload9.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload9.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload9.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload9.Appearance.Options.UseBackColor = true;
            this.BtnUpload9.Appearance.Options.UseFont = true;
            this.BtnUpload9.Appearance.Options.UseForeColor = true;
            this.BtnUpload9.Appearance.Options.UseTextOptions = true;
            this.BtnUpload9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload9.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload9.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload9.Image")));
            this.BtnUpload9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload9.Location = new System.Drawing.Point(348, 324);
            this.BtnUpload9.Name = "BtnUpload9";
            this.BtnUpload9.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload9.TabIndex = 94;
            this.BtnUpload9.ToolTip = "BrowseFile";
            this.BtnUpload9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload9.ToolTipTitle = "Run System";
            this.BtnUpload9.Click += new System.EventHandler(this.BtnUpload9_Click);
            // 
            // BtnSP
            // 
            this.BtnSP.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSP.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSP.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSP.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSP.Appearance.Options.UseBackColor = true;
            this.BtnSP.Appearance.Options.UseFont = true;
            this.BtnSP.Appearance.Options.UseForeColor = true;
            this.BtnSP.Appearance.Options.UseTextOptions = true;
            this.BtnSP.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSP.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSP.Image = ((System.Drawing.Image)(resources.GetObject("BtnSP.Image")));
            this.BtnSP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSP.Location = new System.Drawing.Point(323, 6);
            this.BtnSP.Name = "BtnSP";
            this.BtnSP.Size = new System.Drawing.Size(22, 21);
            this.BtnSP.TabIndex = 104;
            this.BtnSP.ToolTip = "List of Shipment Planning";
            this.BtnSP.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSP.ToolTipTitle = "Run System";
            this.BtnSP.Click += new System.EventHandler(this.BtnSP_Click);
            // 
            // FrmAttachmentFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 405);
            this.Name = "FrmAttachmentFile";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile1.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile9.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private DevExpress.XtraEditors.CheckEdit ChkFile1;
        public DevExpress.XtraEditors.SimpleButton BtnDownload1;
        private System.Windows.Forms.ProgressBar PbUpload1;
        internal DevExpress.XtraEditors.TextEdit TxtFile1;
        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.SimpleButton BtnUpload1;
        private DevExpress.XtraEditors.CheckEdit ChkFile8;
        public DevExpress.XtraEditors.SimpleButton BtnDownload8;
        private System.Windows.Forms.ProgressBar PbUpload8;
        internal DevExpress.XtraEditors.TextEdit TxtFile8;
        private System.Windows.Forms.Label label7;
        public DevExpress.XtraEditors.SimpleButton BtnUpload8;
        private DevExpress.XtraEditors.CheckEdit ChkFile7;
        public DevExpress.XtraEditors.SimpleButton BtnDownload7;
        private System.Windows.Forms.ProgressBar PbUpload7;
        internal DevExpress.XtraEditors.TextEdit TxtFile7;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.SimpleButton BtnUpload7;
        private DevExpress.XtraEditors.CheckEdit ChkFile6;
        public DevExpress.XtraEditors.SimpleButton BtnDownload6;
        private System.Windows.Forms.ProgressBar PbUpload6;
        internal DevExpress.XtraEditors.TextEdit TxtFile6;
        private System.Windows.Forms.Label label4;
        public DevExpress.XtraEditors.SimpleButton BtnUpload6;
        private DevExpress.XtraEditors.CheckEdit ChkFile5;
        public DevExpress.XtraEditors.SimpleButton BtnDownload5;
        private System.Windows.Forms.ProgressBar PbUpload5;
        internal DevExpress.XtraEditors.TextEdit TxtFile5;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.SimpleButton BtnUpload5;
        private DevExpress.XtraEditors.CheckEdit ChkFile4;
        public DevExpress.XtraEditors.SimpleButton BtnDownload4;
        private System.Windows.Forms.ProgressBar PbUpload4;
        internal DevExpress.XtraEditors.TextEdit TxtFile4;
        private System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.SimpleButton BtnUpload4;
        private DevExpress.XtraEditors.CheckEdit ChkFile3;
        public DevExpress.XtraEditors.SimpleButton BtnDownload3;
        private System.Windows.Forms.ProgressBar PbUpload3;
        internal DevExpress.XtraEditors.TextEdit TxtFile3;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.SimpleButton BtnUpload3;
        private DevExpress.XtraEditors.CheckEdit ChkFile2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload2;
        private System.Windows.Forms.ProgressBar PbUpload2;
        internal DevExpress.XtraEditors.TextEdit TxtFile2;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.SimpleButton BtnUpload2;
        protected System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.CheckEdit ChkFile20;
        private System.Windows.Forms.ProgressBar PbUpload20;
        internal DevExpress.XtraEditors.TextEdit TxtFile20;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraEditors.CheckEdit ChkFile19;
        private System.Windows.Forms.ProgressBar PbUpload19;
        internal DevExpress.XtraEditors.TextEdit TxtFile19;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.CheckEdit ChkFile18;
        private System.Windows.Forms.ProgressBar PbUpload18;
        internal DevExpress.XtraEditors.TextEdit TxtFile18;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.CheckEdit ChkFile17;
        private System.Windows.Forms.ProgressBar PbUpload17;
        internal DevExpress.XtraEditors.TextEdit TxtFile17;
        private System.Windows.Forms.Label label16;
        public DevExpress.XtraEditors.SimpleButton BtnUpload17;
        private DevExpress.XtraEditors.CheckEdit ChkFile16;
        private System.Windows.Forms.ProgressBar PbUpload16;
        internal DevExpress.XtraEditors.TextEdit TxtFile16;
        private System.Windows.Forms.Label label13;
        public DevExpress.XtraEditors.SimpleButton BtnUpload16;
        private DevExpress.XtraEditors.CheckEdit ChkFile15;
        private System.Windows.Forms.ProgressBar PbUpload15;
        internal DevExpress.XtraEditors.TextEdit TxtFile15;
        private System.Windows.Forms.Label label14;
        public DevExpress.XtraEditors.SimpleButton BtnUpload15;
        private DevExpress.XtraEditors.CheckEdit ChkFile14;
        private System.Windows.Forms.ProgressBar PbUpload14;
        internal DevExpress.XtraEditors.TextEdit TxtFile14;
        private System.Windows.Forms.Label label11;
        public DevExpress.XtraEditors.SimpleButton BtnUpload14;
        private DevExpress.XtraEditors.CheckEdit ChkFile13;
        private System.Windows.Forms.ProgressBar PbUpload13;
        internal DevExpress.XtraEditors.TextEdit TxtFile13;
        private System.Windows.Forms.Label label12;
        public DevExpress.XtraEditors.SimpleButton BtnUpload13;
        private DevExpress.XtraEditors.CheckEdit ChkFile12;
        private System.Windows.Forms.ProgressBar PbUpload12;
        internal DevExpress.XtraEditors.TextEdit TxtFile12;
        private System.Windows.Forms.Label label9;
        public DevExpress.XtraEditors.SimpleButton BtnUpload12;
        private DevExpress.XtraEditors.CheckEdit ChkFile11;
        private System.Windows.Forms.ProgressBar PbUpload11;
        internal DevExpress.XtraEditors.TextEdit TxtFile11;
        private System.Windows.Forms.Label label10;
        public DevExpress.XtraEditors.SimpleButton BtnUpload11;
        private System.Windows.Forms.OpenFileDialog OD1;
        private DevExpress.XtraEditors.CheckEdit ChkFile10;
        public DevExpress.XtraEditors.SimpleButton BtnDownload10;
        private System.Windows.Forms.ProgressBar PbUpload10;
        internal DevExpress.XtraEditors.TextEdit TxtFile10;
        private System.Windows.Forms.Label label21;
        public DevExpress.XtraEditors.SimpleButton BtnUpload10;
        private DevExpress.XtraEditors.CheckEdit ChkFile9;
        public DevExpress.XtraEditors.SimpleButton BtnDownload9;
        private System.Windows.Forms.ProgressBar PbUpload9;
        internal DevExpress.XtraEditors.TextEdit TxtFile9;
        private System.Windows.Forms.Label label22;
        public DevExpress.XtraEditors.SimpleButton BtnUpload9;
        public DevExpress.XtraEditors.SimpleButton BtnDownload20;
        public DevExpress.XtraEditors.SimpleButton BtnDownload19;
        public DevExpress.XtraEditors.SimpleButton BtnDownload18;
        public DevExpress.XtraEditors.SimpleButton BtnDownload17;
        public DevExpress.XtraEditors.SimpleButton BtnDownload16;
        public DevExpress.XtraEditors.SimpleButton BtnDownload15;
        public DevExpress.XtraEditors.SimpleButton BtnDownload14;
        public DevExpress.XtraEditors.SimpleButton BtnDownload13;
        public DevExpress.XtraEditors.SimpleButton BtnDownload12;
        public DevExpress.XtraEditors.SimpleButton BtnDownload11;
        public DevExpress.XtraEditors.SimpleButton BtnUpload20;
        public DevExpress.XtraEditors.SimpleButton BtnUpload19;
        public DevExpress.XtraEditors.SimpleButton BtnUpload18;
        private System.Windows.Forms.SaveFileDialog SFD1;
        public DevExpress.XtraEditors.SimpleButton BtnSP;
    }
}