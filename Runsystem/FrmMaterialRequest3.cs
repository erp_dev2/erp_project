﻿#region Update
/*
    27/09/2019 [WED/IMS] new apps
    01/10/2019 [WED/IMS] NTP dan SOC tidak mandatory berdasarkan parameter IsNTPSOCInMRMandatory
    11/01/2019 [DITA/IMS] tambah informasi Specifications
    21/11/2019 [WED/IMS] ambil dari BOM Revision
    03/12/2019 [WED/IMS] print out material request
    11/12/2019 [WED/IMS] print out material request jasa (disamakan dengan yg inventory)
    16/12/2019 [WED/IMS] ada pilihan saat mau print, mau print barang atau jasa
    12/02/2020 [TKG/IMS] tambah informasi project
    15/02/2020 [TKG/IMS] menggunakan proses BOM yg baru
    28/02/2020 [TKG/IMS] tambah finished goods dan component
    19/04/2020 [TKG/IMS] tambah IsProduction (digunakan untuk membedakan nomor dokumen PO untuk umum dan produksi
    28/04/2020 [HAR/IMS] Feedback Printout
    20/05/2020 [TKG/IMS] SO contract# harus diisi
    27/05/2020 [TKG/IMS] tambah jumlah karakter remark menjadi 2000
    31/05/2020 [TKG/IMS] pilih so contract# atau project
    05/08/2020 [VIN/IMS] bug printout, project dan komponen tidak muncul
    20/09/2020 [TKG/IMS] Berdasarkan parameter IsDocNoFormatUseFullYear, dokumen# menampilkan tahun secara lengkap (Misal : 2020).
    25/09/2020 [WED/IMS] tambah cancellation qty
    30/09/2020 [IBL/IMS] tambah print out material request jasa
    07/10/2020 [IBL/IMS] item yang ketarik diprintout hanya item yang tidak cancel saja
    07/10/2020 [TKG/IMS] GenerateDocNo reset nomor urut per tahun
    05/11/2020 [WED/IMS] Item service tidak bisa ditarik di PR for Production. semua item service diarahkan di menu PR for Service.
    05/11/2020 [WED/IMS] PR For Service ditentukan berdasarkan parameter MenuCodeForPRService
    19/12/2020 [WED/IMS] PR For Service tambah input item name lagi. Item Name yg sekarang diganti jadi PR Service's Item. Ada fitur untuk import CSV ke grid nya
    01/02/2021 [IBL/IMS] memisahkan printout PR for Production dan PR for Service
    23/02/2021 [WED/IMS] tambahan parameter BatchNoForFreeStock
    06/04/2021 [WED/IMS] print out untuk menu PR For Service, kolom Specification diganti source nya dari item name + uom item turunan (ItCode2)
    06/04/2021 [WED/IMS] approval PR For Service dibedakan dengan PR For Production
    06/07/2021 [VIN/IMS] Btn PG bisa copy SOC DocNo 
    23/12/2021 [SET/IMS] Menambahkan designer pada printout dan menghubungkan dengan approval
 * 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest3 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, IsProcFormat = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            mDroppingRequestBCCode = string.Empty,
            mReqTypeForNonBudget = string.Empty,
            mPGCode = string.Empty,
            mBatchNoForFreeStock = string.Empty
            ;
        internal FrmMaterialRequest3Find FrmFind;
        private bool
            mIsUseItemConsumption = false,
            mIsRemarkForApprovalMandatory = false,
            IsAutoGeneratePurchaseLocalDocNo = false,
            mIsApprovalBySiteMandatory = false,
            mIsDocNoWithDeptShortCode = false,
            mIsDeptFilterBySite = false,
            mIsShowSeqNoInMaterialRequest = false,
            mIsMaterialRequestMaxStockValidationEnabled = false;
        internal bool 
            mIsSiteMandatory = false, 
            mIsPICInMRMandatory = false,
            mIsFilterBySite = false,
            mIsFilterByDept = false,
            mIsFilterByItCt = false,
            mIsShowForeignName = false,
            mIsBudgetActive = false,
            mIsDORequestNeedStockValidation = false,
            mIsDORequestUseItemIssued = false,
            mIsMRShowEstimatedPrice = false,
            mIsMRAllowToUploadFile = false,
            mIsMaterialRequestForDroppingRequestEnabled = false,
            mIsNTPSOCInMRMandatory = false,
            mIsBOMShowSpecifications = false,
            mMenuCodeForPRService = false
            ;
        private string
            mBudgetBasedOn = "1",
            mIsPrintOutUseDigitalSignature = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mMRAvailableBudgetSubtraction = string.Empty,
            mMRDocType = "2",
            mPrintOutFileTypeInd = string.Empty;
           
        private iGCopyPasteManager fCopyPasteManager;

        iGCell fCell;
        bool fAccept;

        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmMaterialRequest3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Material Request";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                GetParameter();
                if (mIsSiteMandatory) LblSiteCode.ForeColor = Color.Red;
                if (mIsPICInMRMandatory) LblPICCode.ForeColor = Color.Red;
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtDocNo, TxtPOQtyCancelDocNo, TxtRemainingBudget, TxtDORequestDocNo }, true);
                SetLueCurCode(ref LueCurCode);
                SetGrd();
                LueCurCode.Visible = false;
                SetFormControl(mState.View);
                SetLuePICCode(ref LuePICCode);
                if (!mIsDeptFilterBySite) Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept?"Y":"N");
                Tp2.PageVisible = mIsMaterialRequestForDroppingRequestEnabled;

                //if (mMenuCodeForPRService) BtnPrint.Visible = false;
                if (!mMenuCodeForPRService) panel7.Visible = BtnImportCSV.Visible = false;

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            IsProcFormat = Sm.GetParameter("ProcFormatDocNo");
            mIsRemarkForApprovalMandatory = Sm.GetParameterBoo("IsMaterialRequestRemarkForApprovalMandatory");
            IsAutoGeneratePurchaseLocalDocNo = Sm.GetParameterBoo("IsAutoGeneratePurchaseLocalDocNo");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsApprovalBySiteMandatory = Sm.GetParameterBoo("IsApprovalBySiteMandatory");
            mBudgetBasedOn = Sm.GetParameter("BudgetBasedOn");
            mIsBudgetActive = Sm.GetParameterBoo("IsBudgetActive");
            mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            mIsDocNoWithDeptShortCode = Sm.GetParameterBoo("IsDocNoWithDeptShortCode");
            mIsDORequestNeedStockValidation = Sm.GetParameterBoo("IsDORequestNeedStockValidation");
            mIsDORequestUseItemIssued = Sm.GetParameterBoo("IsDORequestUseItemIssued");
            mIsMRShowEstimatedPrice = Sm.GetParameterBoo("IsMRShowEstimatedPrice");
            mIsUseItemConsumption = Sm.GetParameterBoo("IsUseItemConsumption");
            mIsPICInMRMandatory = Sm.GetParameterBoo("IsPICInMRMandatory");
            if (mIsSiteMandatory)
            {
                mIsDeptFilterBySite = Sm.IsDataExist(
                    "Select 1 From TblDepartment A, TblDepartmentBudgetSite B " +
                    "Where A.DeptCode=B.DeptCode And A.ActInd='Y' " +
                    "And Exists(Select 1 from TblParameter " +
                    "Where ParCode='IsBudgetActive' And ParValue='Y') " +
                    "Limit 1;");
            }

            mIsPrintOutUseDigitalSignature = Sm.GetParameter("IsPrintOutUseDigitalSignature");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mIsMRAllowToUploadFile = Sm.GetParameterBoo("IsMRAllowToUploadFile");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsShowSeqNoInMaterialRequest = Sm.GetParameterBoo("IsShowSeqNoInMaterialRequest");
            mIsMaterialRequestMaxStockValidationEnabled = Sm.GetParameterBoo("IsMaterialRequestMaxStockValidationEnabled");
            mMRAvailableBudgetSubtraction = Sm.GetParameter("MRAvailableBudgetSubtraction");
            mIsMaterialRequestForDroppingRequestEnabled = Sm.GetParameterBoo("IsMaterialRequestForDroppingRequestEnabled");
            mIsNTPSOCInMRMandatory = Sm.GetParameterBoo("IsNTPSOCInMRMandatory");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mMenuCodeForPRService = Sm.GetParameter("MenuCodeForPRService") == mMenuCode;
            mBatchNoForFreeStock = Sm.GetParameter("BatchNoForFreeStock");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 44;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "Cancel Reason",
                        "",
                        "Status",
                        
                        
                        //6-10
                        "Cheked by",
                        "",
                        mMenuCodeForPRService ? "PR Service's"+Environment.NewLine+"Item's Code" : "Item's Code",
                        mMenuCodeForPRService ? "PR Service's"+Environment.NewLine+"Local Code" : "Local Code",
                        "",
                        
                        //11-15
                        mMenuCodeForPRService ? "PR Service's"+Environment.NewLine+"Item's Name" : "Item's Name",
                        mMenuCodeForPRService ? "PR Service's"+Environment.NewLine+"Specification" : "Specification",
                        mMenuCodeForPRService ? "PR Service's"+Environment.NewLine+"Item Sub Category code" : "Item Sub Category code",
                        mMenuCodeForPRService ? "PR Service's"+Environment.NewLine+"Sub Category" : "Sub Category",
                        "Minimum Stock",
                       
                        //16-20
                        "Reoder Point",
                        "Quantity",
                        "UoM",
                        "Usage Date",
                        "Quotation#",
                        
                        //21-25
                        "Quotation Dno",
                        "Quotation Date",
                        "Price",
                        "Total",
                        "Remark",

                        //26-30
                        "DORequestDocNo",
                        "DORequestDNo",
                        "Currency",
                        "Estimated Price",
                        "No.",

                        //31-35
                        "Dropping Request Quantity",
                        "Dropping Request's"+Environment.NewLine+"Amount",
                        "Stock",
                        "Bom#",
                        "Finished Good",

                        //36-40
                        "Component",
                        "Cancelled"+Environment.NewLine+"Quantity",
                        "ServiceItemInd",
                        "",
                        "Item's Code",

                        //41-43
                        "Local Code",
                        "Item's Name",
                        "Specification",
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        50, 50, 200, 20, 80, 
                        
                        //6-10
                        100, 20, 100, 150, 20,  
                        
                        //11-15
                        230, 200, 80, 100, 100, 
                        
                        //16-20
                        100, 120, 100, 100, 130,  
                        
                        //21-25
                        80, 100, 150, 150, 400,

                        //26-30
                        0, 0, 100, 120, 50,

                        //31-35
                        0, 130, 130, 130, 200,

                        //36-40
                        200, 120, 0, 20, 120,

                        //41-43
                        200, 150, 250
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 38 });
            Sm.GrdFormatDec(Grd1, new int[] { 15, 16, 17, 23, 24, 29, 31, 32, 33, 37 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 4, 7, 10, 39 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 19, 22 });
            if (!mIsDORequestNeedStockValidation && !mIsDORequestUseItemIssued)
                Sm.GrdColInvisible(Grd1, new int[] { 7 });
            
            if (!mIsMRShowEstimatedPrice)
                Sm.GrdColInvisible(Grd1, new int[] { 28, 29 });
            if (IsProcFormat == "1")
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 10, 13, 21, 22, 23, 24, 31, 32 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 10, 13, 14, 21, 22, 23, 24, 31, 32 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 30, 31, 32, 33, 34, 35, 36, 37, 38, 40, 41, 42, 43 });
            fCopyPasteManager = new iGCopyPasteManager(Grd1);
            Grd1.Cols[33].Move(18);
            Grd1.Cols[9].Move(12);
            if (mMenuCodeForPRService)
            {
                Grd1.Cols[43].Move(15);
                Grd1.Cols[42].Move(15);
                Grd1.Cols[41].Move(15);
                Grd1.Cols[40].Move(15);
                Grd1.Cols[39].Move(15);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 39, 40, 41, 42, 43 });
            }
            Grd1.Cols[37].Move(19);
            Grd1.Cols[34].Move(25);
            Grd1.Cols[35].Move(26);
            Grd1.Cols[36].Move(27);
            Grd1.Cols[28].Move(25);
            Grd1.Cols[29].Move(26);
            Grd1.Cols[30].Move(0);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 8, 9, 10, 22 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtLocalDocNo, DteDocDt, LueReqType, LueDeptCode, LueBCCode, 
                       LueSiteCode, MeeRemark, TxtFile, LuePICCode, TxtSOCDocNo, TxtNTPDocNo
                    }, true);
                    BtnPGCode.Enabled = false;
                    BtnPOQtyCancelDocNo.Enabled = false;
                    BtnDORequestDocNo.Enabled = false;
                    BtnDroppingRequestDocNo.Enabled = false;
                    BtnSOCDocNo.Enabled = BtnNTPDocNo.Enabled = false;
                    BtnImportCSV.Enabled = false;
                    if (!mIsDORequestNeedStockValidation)
                        LblDORequestDocNo.ForeColor = Color.Red;
                    else
                        LblDORequestDocNo.ForeColor = Color.Black;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 28, 29, 39, 40, 41, 42, 43 });
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsMRAllowToUploadFile)
                        BtnDownload.Enabled = true;
                    ChkFile.Enabled = false;
                    Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 5, 6 }, true);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDeptCode }, mIsDeptFilterBySite);                 
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { DteDocDt, LueReqType, LueSiteCode, MeeRemark, LuePICCode }, false);                 
                    if (!IsAutoGeneratePurchaseLocalDocNo) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtLocalDocNo }, false);
                    BtnPGCode.Enabled = true;
                    BtnPOQtyCancelDocNo.Enabled = true;
                    BtnSOCDocNo.Enabled = BtnNTPDocNo.Enabled = true;
                    BtnImportCSV.Enabled = true;
                    if (!mIsDORequestNeedStockValidation && !mIsDORequestUseItemIssued)
                        LblDORequestDocNo.ForeColor = Color.Red;
                    else
                        LblDORequestDocNo.ForeColor = Color.Black;
                    if (!mIsDORequestNeedStockValidation) BtnDORequestDocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 7, 17, 19, 25, 28, 29, 39 });
                    if (mIsMRAllowToUploadFile)
                    {
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = true;
                    }
                    ChkFile.Enabled = true;
                    BtnDroppingRequestDocNo.Enabled = true;
                    Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 5, 6 }, false);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3 });
                    if (!mIsDORequestNeedStockValidation)
                        LblDORequestDocNo.ForeColor = Color.Red;
                    else
                        LblDORequestDocNo.ForeColor = Color.Black;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mPrintOutFileTypeInd = string.Empty;
            mDroppingRequestBCCode = string.Empty;
            mPGCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtLocalDocNo, DteDocDt, LueReqType, LueDeptCode, TxtFile,
                LueSiteCode, TxtPOQtyCancelDocNo, LueBCCode, MeeRemark, TxtDORequestDocNo, 
                LuePICCode, TxtDroppingRequestDocNo, TxtDR_DeptCode, TxtDR_Yr, TxtDR_Mth, 
                TxtDR_PRJIDocNo, TxtDR_BCCode, MeeDR_Remark, TxtSOCDocNo, TxtNTPDocNo,
                TxtProjectCode, TxtProjectName, TxtPONo
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtRemainingBudget, TxtDR_DroppingRequestAmt, TxtDR_MRAmt, TxtDR_Balance 
            }, 0);
            ClearGrd();
            ChkFile.Checked = false;
            PbUpload.Value = 0;
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 15, 16, 17, 23, 24, 29, 31, 32, 37 });
            if (mIsBudgetActive) Grd1.Cells[0, 11].ForeColor = Color.Black;
            SetSeqNo();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmMaterialRequest3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            InsertData();
        }

        private void InsertData()
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueReqType(ref LueReqType, string.Empty);
                if (mReqTypeForNonBudget.Length > 0) Sm.SetLue(LueReqType, mReqTypeForNonBudget);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySite ? "Y" : "N");
                if (!mIsDeptFilterBySite) Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            ParPrint(TxtDocNo.Text);
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion 

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 7 && !Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode, "Department") && !IsSOContractAndProjectEmpty()) // && !IsSOContractAndNTPIsEmpty())
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) 
                            Sm.FormShowDialog(new FrmMaterialRequest3Dlg(this, Sm.GetLue(LueReqType), Sm.GetLue(LueDeptCode)));
                    }

                    if(e.ColIndex == 39 && mMenuCodeForPRService && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 11, false, "PR Service's Item's Name is empty."))
                    {
                        Sm.FormShowDialog(new FrmMaterialRequest3Dlg7(this, e.RowIndex));
                    }

                    if (e.ColIndex == 17 && TxtDroppingRequestDocNo.Text.Length>0)
                        e.DoDefault = false;
                    
                    if (Sm.IsGrdColSelected(new int[] { 3, 17, 19, 25 }, e.ColIndex))
                    {
                        if (e.ColIndex == 19) Sm.DteRequestEdit(Grd1, DteUsageDt, ref fCell, ref fAccept, e);
                        if(mIsDORequestNeedStockValidation) Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 17, 23, 24, 29, 31, 32, 33 });
                    }

                    if (e.ColIndex == 28)
                    {
                        LueRequestEdit(Grd1, LueCurCode, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        SetLueCurCode(ref LueCurCode);
                    }
                    
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0 ) ShowMRApprovalInfo(e.RowIndex);
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            //diremark oleh TKG on 14/8/2018
            //if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            //if (mIsDORequestNeedStockValidation)
            //{
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeRemainingBudget();
                ComputeDR_Balance();
                SetSeqNo();
            }
            //}
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e) //
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0) ShowMRApprovalInfo(e.RowIndex);
            if (e.ColIndex == 7 && 
                TxtDocNo.Text.Length == 0 && 
                !Sm.IsLueEmpty(LueReqType, "Request type") && 
                !Sm.IsLueEmpty(LueDeptCode, "Department") &&
                !IsSOContractAndProjectEmpty()
                //&& !IsSOContractAndNTPIsEmpty()
                )
                Sm.FormShowDialog(new FrmMaterialRequest3Dlg(this, Sm.GetLue(LueReqType), Sm.GetLue(LueDeptCode)));

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }

            if (e.ColIndex == 39 && mMenuCodeForPRService && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 11, false, "PR Service's Item's Name is empty."))
            {
                Sm.FormShowDialog(new FrmMaterialRequest3Dlg7(this, e.RowIndex));
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 17 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 25 }, e);
            if (e.ColIndex == 17)
            {
                ComputeTotal(e.RowIndex);
                ComputeDR_Balance();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 19)
            {
                if (Sm.GetGrdDate(Grd1, 0, 19).Length != 0)
                {
                    var UsageDt = Sm.ConvertDate(Sm.GetGrdDate(Grd1, 0, 19));
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0) Grd1.Cells[Row, 19].Value = UsageDt;
                }
            }

            if (e.ColIndex == 25)
            {
                var Remark = Sm.GetGrdStr(Grd1, 0, 25);
                if (Remark.Length != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0) Grd1.Cells[Row, 25].Value = Remark;
                }
            }

            if (e.ColIndex == 28)
            {
                var CurCode = Sm.GetGrdStr(Grd1, 0, 28);
                if (CurCode.Length != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0) 
                            Grd1.Cells[Row, 28].Value = CurCode;
                }
            }

            if (e.ColIndex == 29)
            {
                decimal Total = 0m;

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 29).Length != 0)
                        Total += Sm.GetGrdDec(Grd1, Row, 29);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        override protected void GrdRequestColHdrToolTipText(object sender, iGRequestColHdrToolTipTextEventArgs e)
        {
            if (e.ColIndex == 19)
                e.Text = "Double click title to copy data based on the first row's value.";
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            string LocalDocNo = string.Empty;
            var mMsg = new StringBuilder();
            if (Sm.StdMsgYN("Question",
                    "Department : " + LueDeptCode.GetColumnValue("Col2") + Environment.NewLine +
                    "Do you want to save this data ?"
                    ) == DialogResult.No ||
                IsInsertedDataNotValid()) return;            
 
            Cursor.Current = Cursors.WaitCursor;

            string DeptCode = Sm.GetLue(LueDeptCode);
            string SubCategory = Sm.GetGrdStr(Grd1, 0, 13);
            bool IsDocNeedNoApproval = true;

            IsDocNeedNoApproval = IsDocApprovalSettingNotExisted(mMenuCodeForPRService);
            
            string DocNo = string.Empty;
            if (!mMenuCodeForPRService)
                DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr", SubCategory);
            else
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "MaterialRequestService", "TblMaterialRequestServiceHdr");

            if (IsAutoGeneratePurchaseLocalDocNo)
                LocalDocNo = GenerateLocalDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr", SubCategory);
            else
                LocalDocNo = TxtLocalDocNo.Text;
            
            var cml = new List<MySqlCommand>();

            if (!mMenuCodeForPRService)
            {
                cml.Add(SaveMaterialRequestHdr(DocNo, LocalDocNo));
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 8).Length > 0)
                    {
                        cml.Add(SaveMaterialRequestDtl(
                            DocNo,
                            Row,
                            IsDocNeedNoApproval
                            ));
                    }
                }

                //cml.Add(UpdateBOQDtl(DocNo, "I"));

                Sm.ExecCommands(cml);

                if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                    UploadFile(DocNo);
            }
            else
            {
                cml.Add(SaveMaterialRequestServiceHdr(DocNo, LocalDocNo));
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 8).Length > 0)
                    {
                        cml.Add(SaveMaterialRequestServiceDtl(
                            DocNo,
                            Row,
                            IsDocNeedNoApproval
                            ));
                    }
                }

                Sm.ExecCommands(cml);

                if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                    UploadFile(DocNo);
            }

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            IsSubCategoryNull();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueReqType, "Request type") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                IsSOContractAndProjectEmpty() ||
                //IsSOContractAndNTPIsEmpty() ||
                //IsBothSOContractAndNTPFilled() ||
                (mIsPICInMRMandatory && Sm.IsLueEmpty(LuePICCode, "PIC")) ||
                (!mIsDORequestNeedStockValidation && !mIsDORequestUseItemIssued && Sm.IsTxtEmpty(TxtDORequestDocNo, "DO Request#", false)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsItemPRDuplicated() ||
                IsGrdExceedMaxRecords() ||
                IsItemInvalid() ||
                IsRemainingBudgetNotValid() ||
                IsSubcategoryDifferent() ||
                IsSubCategoryXXX() ||
                IsPOQtyCancelDocNoNotValid() ||
                (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                (mIsMRAllowToUploadFile && IsUploadFileNotValid()) ||
                IsMaxStockInvalid() ||
                IsDR_QtyInvalid() ||
                IsDR_AmtInvalid() ||
                IsDR_BalanceInvalid();
        }

        private bool IsItemPRDuplicated()
        {
            if (mMenuCodeForPRService)
            {
                string ItCode1 = string.Empty, ItCode2 = string.Empty;

                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    ItCode1 = string.Concat(Sm.GetGrdStr(Grd1, i, 8), Sm.GetGrdStr(Grd1, i, 40));

                    for (int j = i+1; j < Grd1.Rows.Count - 1; ++j)
                    {
                        ItCode2 = string.Concat(Sm.GetGrdStr(Grd1, j, 8), Sm.GetGrdStr(Grd1, j, 40));

                        if (Sm.CompareStr(ItCode1, ItCode2))
                        {
                            Sm.StdMsg(mMsgType.Warning, "Duplicate combination of PR Service item and Item is found at row #" + (j+1));
                            Sm.FocusGrd(Grd1, j, 42);
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsItemInvalid()
        {
            CheckServiceItemInd();
            bool mFlag = false;
            int Row = 0;
            string Msg = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (mMenuCodeForPRService)
                {
                    if (Sm.GetGrdStr(Grd1, i, 38) != "Y")
                    {
                        Row = i;
                        mFlag = true;
                        Msg = "Non-service";
                        break;
                    }
                }
                else
                {
                    if (Sm.GetGrdStr(Grd1, i, 38) == "Y")
                    {
                        Row = i;
                        mFlag = true;
                        Msg = "Service";
                        break;
                    }
                }
            }

            if (mFlag)
            {
                Sm.StdMsg(mMsgType.Warning, "Item at row #" + (Row+1) + " is detected as a " + Msg + " item. PR For Service is targeted for service item only, while PR For Production is targeted for non-service item only.");
                return true;
            }

            return false;
        }

        private bool IsSOContractAndProjectEmpty()
        {
            if (TxtSOCDocNo.Text.Length == 0 && TxtProjectCode.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Please fill either SO Contract# or project.");
                return true;
            }
            return false;
        }

        //private bool IsSOContractAndNTPIsEmpty()
        //{
        //    if (mIsNTPSOCInMRMandatory)
        //    {
        //        if (TxtNTPDocNo.Text.Length == 0 && TxtSOCDocNo.Text.Length == 0)
        //        {
        //            Sm.StdMsg(mMsgType.Warning, "Please fill either SO Contract# or Notice To Proceed#.");
        //            TxtSOCDocNo.Focus();
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //private bool IsBothSOContractAndNTPFilled()
        //{
        //    if (TxtNTPDocNo.Text.Length > 0 && TxtSOCDocNo.Text.Length > 0)
        //    {
        //        Sm.StdMsg(mMsgType.Warning, "Please only fill either SO Contract# or Notice To Proceed#.");
        //        TxtSOCDocNo.Focus();
        //        return true;
        //    }
        //    return false;
        //}

        private bool IsDR_AlreadyProcessed1()
        {
            if (!mIsMaterialRequestForDroppingRequestEnabled) return false;

            if (TxtDR_PRJIDocNo.Text.Length>0)
            {
                if (Sm.IsDataExist("Select 1 From TblDroppingRequestDtl Where MRDocNo Is Not Null And DocNo=@Param Limit 1;", TxtDroppingRequestDocNo.Text))    
                {
                    Sm.StdMsg(mMsgType.Warning, "This dropping request# already processed.");
                    return true;
                }
            }
            if (mDroppingRequestBCCode.Length > 0)
            {
                if (Sm.IsDataExist(
                    "Select 1 From TblDroppingRequestDtl2 Where MRDocNo Is Not Null And DocNo=@Param1 And BCCode=@Param2 Limit 1;", TxtDroppingRequestDocNo.Text, mDroppingRequestBCCode, string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning, "This dropping request# already processed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDR_QtyInvalid()
        { 
            if (TxtDroppingRequestDocNo.Text.Length <= 0) return false;

            decimal Qty1 = 0m, Qty2 = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Qty1 = 0m;  
                Qty2 = 0m;
                if (Sm.GetGrdStr(Grd1, r, 17).Length != 0) Qty1 = Sm.GetGrdDec(Grd1, r, 17);
                if (Sm.GetGrdStr(Grd1, r, 31).Length != 0) Qty2 = Sm.GetGrdDec(Grd1, r, 31);

                if (Qty1 > Qty2)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                        "Requested Quantity : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 17), 0) + Environment.NewLine +
                        "Dropping Request's Quantity : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 31), 0) + Environment.NewLine + Environment.NewLine +
                        "Requested Quantity is bigger than Dropping Request's Quantity.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDR_AmtInvalid()
        {
            if (TxtDroppingRequestDocNo.Text.Length <= 0) return false;

            decimal Amt1 = 0m, Amt2 = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Amt1 = 0m;
                Amt2 = 0m;
                if (Sm.GetGrdStr(Grd1, r, 24).Length != 0) Amt1 = Sm.GetGrdDec(Grd1, r, 24);
                if (Sm.GetGrdStr(Grd1, r, 32).Length != 0) Amt2 = Sm.GetGrdDec(Grd1, r, 32);

                if (Amt1 > Amt2)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                        "Requested Amount : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 24), 0) + Environment.NewLine +
                        "Dropping Request's Amount : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 32), 0) + Environment.NewLine + Environment.NewLine +
                        "Requested Amount is bigger than Dropping Request's Amount.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDR_BalanceInvalid()
        {
            if (TxtDroppingRequestDocNo.Text.Length <= 0) return false;

            //ComputeDR_OtherMRAmt();
            ComputeDR_Balance();
            if (decimal.Parse(TxtDR_Balance.Text) < 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                        "Dropping Request's Balance : " + TxtDR_Balance.Text + Environment.NewLine +
                        "Dropping Request's balance is less than 0.00.");
                return true;
            }
            return false;
        }

        private bool IsMaxStockInvalid()
        {
            //membandingkan maximum stock dengan quantity yg diminta + stock semua warehouse + outang MR yg belum di-PO-kan.
            //MR yg belum di-received inidikator belum ada/Uom bisa beda/proses bisa lama
            //Hanya yg maximum stocknya diisi dan uom purchase dan inventory-nya sama

            if (!mIsMaterialRequestMaxStockValidationEnabled) return false;

            string ItCode = string.Empty, Filter = string.Empty, Filter2 = string.Empty;
            var cm = new MySqlCommand();

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 8);
                if (ItCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.ItCode=@ItCode0" + r.ToString() + ") ";
                    
                    if (Filter2.Length > 0) Filter2 += " Union All ";
                    Filter2 += " Select @ItCode0" + r.ToString() + " As ItCode, @Qty0" + r.ToString() + " As Qty ";

                    Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                    Sm.CmParam<Decimal>(ref cm, "@Qty0" + r.ToString(), Sm.GetGrdDec(Grd1, r, 17));
                }
            }

            if (Filter.Length == 0)
                return false;
            else
                Filter = " And ( " + Filter + " ) ";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.MaxStock, IfNull(B.Qty, 0.00) As Stock, IfNull(C.Qty, 0.00) As OutstandingMR, IfNull(D.Qty, 0.00) As MRQty  ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select A.ItCode, Sum(A.Qty) As Qty  ");
            SQL.AppendLine("    From TblStockSummary  A ");
            SQL.AppendLine("    Where A.Qty>0.00 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("    Group By A.ItCode ");
            SQL.AppendLine("    ) B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select T2.ItCode, Sum(T2.Qty) As Qty  ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.Status In ('O', 'A') And T2.ProcessInd In ('O', 'P') ");
            SQL.AppendLine(Filter.Replace("A.", "T2."));
            SQL.AppendLine("    Where T1.CancelInd='N' And T1.Status In ('O', 'A')  ");
            SQL.AppendLine("    Group By T2.ItCode ");
            SQL.AppendLine("    ) C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("    ) D On A.ItCode=D.ItCode ");
            SQL.AppendLine("Where A.MaxStock>0.00 ");
            SQL.AppendLine("And A.PurchaseUomCode=A.InventoryUomCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("And A.MaxStock<IfNull(B.Qty, 0.00)+IfNull(C.Qty, 0.00)+IfNull(D.Qty, 0.00) ");
            SQL.AppendLine("Limit 1; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        "ItCode", 
                        "ItName", "MaxStock", "Stock", "OutstandingMR", "MRQty"
                    });

                if (dr.HasRows)
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Item's Code : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                                "Item's Name : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                                "Maximum Stock : " + Sm.FormatNum(Sm.DrDec(dr, 2), 0) + Environment.NewLine +
                                "Current Stock : " + Sm.FormatNum(Sm.DrDec(dr, 3), 0) + Environment.NewLine +
                                "Outstanding Material Request : " + Sm.FormatNum(Sm.DrDec(dr, 4), 0) + Environment.NewLine +
                                "Requested Quantity : " + Sm.FormatNum(Sm.DrDec(dr, 5), 0) + Environment.NewLine + Environment.NewLine +
                                "Invalid maximum stock."
                                );
                            return true;
                        }
                    }
                }
                dr.Close();
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var DocDt = Sm.GetDte(DteDocDt);
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (mMenuCodeForPRService)
                {
                    if (
                        Sm.IsGrdValueEmpty(Grd1, Row, 8, false, "PR Service Item is empty.") ||
                        Sm.IsGrdValueEmpty(Grd1, Row, 42, false, "Item is empty.") ||
                        Sm.IsGrdValueEmpty(Grd1, Row, 17, true, "Quantity should be bigger than 0.00.") ||
                        Sm.IsGrdValueEmpty(Grd1, Row, 19, false, "Usage date is empty.") ||
                        IsUsageDtNotValid(DocDt, Row) ||
                        (Sm.GetLue(LueReqType) == "1" && Sm.IsGrdValueEmpty(Grd1, Row, 23, true, "Quotation's price is 0.")) ||
                        (mIsRemarkForApprovalMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 25, false, "Remark is empty."))
                        ) return true;
                }
                else
                {
                    if (
                        Sm.IsGrdValueEmpty(Grd1, Row, 8, false, "Item is empty.") ||
                        Sm.IsGrdValueEmpty(Grd1, Row, 17, true, "Quantity should be bigger than 0.00.") ||
                        Sm.IsGrdValueEmpty(Grd1, Row, 19, false, "Usage date is empty.") ||
                        IsUsageDtNotValid(DocDt, Row) ||
                        (Sm.GetLue(LueReqType) == "1" && Sm.IsGrdValueEmpty(Grd1, Row, 23, true, "Quotation's price is 0.")) ||
                        (mIsRemarkForApprovalMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 25, false, "Remark is empty."))
                        ) return true;
                }
            }
            return false;
        }

        private bool IsUsageDtNotValid(string DocDt, int Row)
        {
            var UsageDt = Sm.GetGrdDate(Grd1, Row, 19);
            if (Sm.CompareDtTm(UsageDt, DocDt) < 0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Document Date : " + Sm.FormatDate2("dd/MMM/yyyy", DocDt) + Environment.NewLine +
                    "Usage Date : " + Sm.FormatDate2("dd/MMM/yyyy", UsageDt) + Environment.NewLine + Environment.NewLine +
                    "Usage date is earlier than document date.");
                Sm.FocusGrd(Grd1, Row, 19);
                return true;
            }
            return false;
        }

        private bool IsRemainingBudgetNotValid()
        {
            decimal RemainingBudget = 0m;

            if (TxtRemainingBudget.Text.Length != 0) RemainingBudget = decimal.Parse(TxtRemainingBudget.Text);

            if (RemainingBudget<0)
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid remaining budget.");
                return true;
            }
            return false;
        }

        private bool IsDocApprovalSettingNotExisted(bool IsMenuCodeForPRService)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where UserCode Is Not Null ");
            
            if (!IsMenuCodeForPRService)
                SQL.AppendLine("And DocType = 'MaterialRequest' ");
            else
                SQL.AppendLine("And DocType = 'MaterialRequestService' ");

            SQL.AppendLine("And DeptCode Is Not Null ");
            SQL.AppendLine("And DeptCode = @Param1 ");
            if (mIsApprovalBySiteMandatory)
            {
                SQL.AppendLine("And SiteCode Is Not Null ");
                SQL.AppendLine("And SiteCode=@Param2 ");
            }

            SQL.AppendLine("Limit 1; ");

            if (!Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueDeptCode), Sm.GetLue(LueSiteCode), string.Empty)) 
                return true;
            else 
                return false;
        }

        private void IsSubCategoryNull()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 13).Length == 0)
                {
                    Grd1.Cells[Row, 13].Value = Grd1.Cells[Row, 14].Value = "XXX";
                }
            }
        }

        private bool IsSubCategoryXXX()
        {
            if (IsProcFormat == "1")
            {
                string Msg = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 14) == "XXX")
                    {
                        Msg =
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine;

                        Sm.StdMsg(mMsgType.Warning, Msg + "doesn't have Sub-Category.");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsSubcategoryDifferent()
        {
            if (IsProcFormat == "1")
            {
                string SubCat = Sm.GetGrdStr(Grd1, 0, 13);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (SubCat != Sm.GetGrdStr(Grd1, Row, 13))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Item have different subcategory ");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsPOQtyCancelDocNoNotValid()
        {
            if (TxtPOQtyCancelDocNo.Text.Length == 0) return false;

            var cm = new MySqlCommand() 
            { 
                CommandText = 
                "Select DocNo From TblPOQtyCancel " +
                "Where DocNo=@Param And CancelInd='N' And ProcessInd='O' And NewMRInd='Y';"
            };
            Sm.CmParam<String>(ref cm, "@Param", TxtPOQtyCancelDocNo.Text);
            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Cancellation PO# is invalid.");
                return true;
            }
            return false;
        }

        private bool IsCancelReasonEmpty()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 3).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                        "Cancel reason still empty.");
                    Sm.FocusGrd(Grd1, Row, 3);
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveMaterialRequestHdr(string DocNo, string LocalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialRequestHdr(DocNo, DocType, LocalDocNo, POQtyCancelDocNo, DORequestDocNo, DocDt, SiteCode, DeptCode, PICCode, ReqType, BCCode, SeqNo, ItScCode, Mth, Yr, Revision, SOCDocNo, NTPDocNo, ");
            if (TxtDroppingRequestDocNo.Text.Length>0)
                SQL.AppendLine("DroppingRequestDocNo, DroppingRequestBCCode, ");
            SQL.AppendLine("PGCode, IsProduction, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocType, @LocalDocNo, @POQtyCancelDocNo, @DORequestDocNo, @DocDt, @SiteCode, @DeptCode, @PICCode, @ReqType, @BCCode, @SeqNo, @ItScCode, @Mth, @Yr, @Revision, @SOCDocNo, @NTPDocNo, ");
            if (TxtDroppingRequestDocNo.Text.Length > 0)
                SQL.AppendLine("@DroppingRequestDocNo, @DroppingRequestBCCode, ");
            SQL.AppendLine("@PGCode, 'Y', @Remark, @CreateBy, CurrentDateTime());");

            if (TxtPOQtyCancelDocNo.Text.Length > 0)
                SQL.AppendLine("Update TblPOQtyCancel Set ProcessInd='F' Where DocNo=@POQtyCancelDocNo;");        
            
            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mMRDocType);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@POQtyCancelDocNo", TxtPOQtyCancelDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DORequestDocNo", TxtDORequestDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PICCode", Sm.GetLue(LuePICCode));
            Sm.CmParam<String>(ref cm, "@ReqType", Sm.GetLue(LueReqType));
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
            if (IsAutoGeneratePurchaseLocalDocNo)
            {
                Sm.CmParam<String>(ref cm, "@SeqNo", Sm.Left(LocalDocNo, 6));
                Sm.CmParam<String>(ref cm, "@ItScCode", Sm.GetGrdStr(Grd1, 0, 13));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.Left(Sm.GetDte(DteDocDt), 4));
                Sm.CmParam<String>(ref cm, "@Revision", "");
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@SeqNo", "");
                Sm.CmParam<String>(ref cm, "@ItScCode", "");
                Sm.CmParam<String>(ref cm, "@Mth", "");
                Sm.CmParam<String>(ref cm, "@Yr", "");
                Sm.CmParam<String>(ref cm, "@Revision", "");
            }
            Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
            Sm.CmParam<String>(ref cm, "@NTPDocNo", TxtNTPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mDroppingRequestBCCode);
            Sm.CmParam<String>(ref cm, "@PGCode", mPGCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveMaterialRequestDtl(string DocNo, int Row, bool NoNeedApproval)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialRequestDtl(DocNo, DNo, CancelInd, CancelReason, ");
            SQL.AppendLine("Status, ItCode, Qty, UsageDt, QtDocNo, QtDNo, DORequestDocNo, ");
            SQL.AppendLine("BOMRDocNo, BOMRDNo, BOMDocNo, BOMDNo, ");
            SQL.AppendLine("DORequestDNo, UPrice, CurCode, EstPrice, Bom2DocNo, FinishedGood, Component, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @CancelReason, ");
            SQL.AppendLine("@Status, @ItCode, @Qty, @UsageDt, @QtDocNo, @QtDNo, @DORequestDocNo, ");
            SQL.AppendLine("@BOMRDocNo, @BOMRDNo, @BOMDocNo, @BOMDNo, ");
            SQL.AppendLine("@DORequestDNo, @UPrice, @CurCode, @EstPrice, @Bom2DocNo, @FinishedGood, @Component, @Remark, @CreateBy, CurrentDateTime()); ");

            if (TxtDroppingRequestDocNo.Text.Length > 0)
            {
                if (TxtDR_PRJIDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("Update TblDroppingRequestDtl A ");
                    SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr B On A.PRBPDocNo=B.DocNo ");
                    SQL.AppendLine("Inner Join TblMaterialRequestDtl C On B.ResourceItCode=C.ItCode And C.DocNo=@DocNo ");
                    SQL.AppendLine("Set A.MRDocNo=C.DocNo, A.MRDNo=C.DNo ");
                    SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("And A.MRDocNo Is Null; ");
                }
                if (TxtDR_BCCode.Text.Length > 0)
                {
                    SQL.AppendLine("Update TblDroppingRequestDtl2 A ");
                    SQL.AppendLine("Inner Join TblMaterialRequestHdr B On A.BCCode=B.DroppingRequestBCCode And B.DocNo=@DocNo ");
                    SQL.AppendLine("Inner Join TblMaterialRequestDtl C On A.ItCode=C.ItCode And B.DocNo=C.DocNo ");
                    SQL.AppendLine("Set A.MRDocNo=C.DocNo, A.MRDNo=C.DNo ");
                    SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("And A.ItCode=@ItCode ");
                    SQL.AppendLine("And A.MRDocNo Is Null; ");
                }
            }

            if (!NoNeedApproval)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DeptCode=@DeptCode ");
                if (mIsApprovalBySiteMandatory)
                    SQL.AppendLine("And IfNull(T.SiteCode, '')=@SiteCode ");
                SQL.AppendLine("And T.DocType='MaterialRequest'; ");
            }
        
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@Status", NoNeedApproval?"A":"O");
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetGrdDate(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@QtDocNo", Sm.GetGrdStr(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@QtDNo", Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@DORequestDocNo", Sm.GetGrdStr(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@DORequestDNo", Sm.GetGrdStr(Grd1, Row, 27));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, Row, 28));
            Sm.CmParam<Decimal>(ref cm, "@EstPrice", Sm.GetGrdDec(Grd1, Row, 29));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Bom2DocNo", Sm.GetGrdStr(Grd1, Row, 34));
            Sm.CmParam<String>(ref cm, "@FinishedGood", Sm.GetGrdStr(Grd1, Row, 35));
            Sm.CmParam<String>(ref cm, "@Component", Sm.GetGrdStr(Grd1, Row, 36));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveMaterialRequestServiceHdr(string DocNo, string LocalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialRequestServiceHdr(DocNo, DocType, LocalDocNo, POQtyCancelDocNo, DORequestDocNo, DocDt, SiteCode, DeptCode, PICCode, ReqType, BCCode, SOCDocNo, NTPDocNo, ");
            SQL.AppendLine("PGCode, IsProduction, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocType, @LocalDocNo, @POQtyCancelDocNo, @DORequestDocNo, @DocDt, @SiteCode, @DeptCode, @PICCode, @ReqType, @BCCode, @SOCDocNo, @NTPDocNo, ");
            SQL.AppendLine("@PGCode, 'Y', @Remark, @CreateBy, CurrentDateTime()); ");

            if (TxtPOQtyCancelDocNo.Text.Length > 0)
                SQL.AppendLine("Update TblPOQtyCancel Set ProcessInd='F' Where DocNo=@POQtyCancelDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mMRDocType);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@POQtyCancelDocNo", TxtPOQtyCancelDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DORequestDocNo", TxtDORequestDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PICCode", Sm.GetLue(LuePICCode));
            Sm.CmParam<String>(ref cm, "@ReqType", Sm.GetLue(LueReqType));
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
            Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
            Sm.CmParam<String>(ref cm, "@NTPDocNo", TxtNTPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PGCode", mPGCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveMaterialRequestServiceDtl(string DocNo, int Row, bool NoNeedApproval)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialRequestServiceDtl(DocNo, DNo, CancelInd, CancelReason, ");
            SQL.AppendLine("Status, ItCode, ItCode2, Qty, UsageDt, QtDocNo, QtDNo, DORequestDocNo, ");
            SQL.AppendLine("DORequestDNo, UPrice, CurCode, EstPrice, Bom2DocNo, FinishedGood, Component, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @CancelReason, ");
            SQL.AppendLine("@Status, @ItCode, @ItCode2, @Qty, @UsageDt, @QtDocNo, @QtDNo, @DORequestDocNo, ");
            SQL.AppendLine("@DORequestDNo, @UPrice, @CurCode, @EstPrice, @Bom2DocNo, @FinishedGood, @Component, @Remark, @CreateBy, CurrentDateTime()); ");

            if (!NoNeedApproval)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DeptCode=@DeptCode ");
                if (mIsApprovalBySiteMandatory)
                    SQL.AppendLine("And IfNull(T.SiteCode, '')=@SiteCode ");
                SQL.AppendLine("And T.DocType='MaterialRequestService'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@Status", NoNeedApproval ? "A" : "O");
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@ItCode2", Sm.GetGrdStr(Grd1, Row, 40));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetGrdDate(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@QtDocNo", Sm.GetGrdStr(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@QtDNo", Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@DORequestDocNo", Sm.GetGrdStr(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@DORequestDNo", Sm.GetGrdStr(Grd1, Row, 27));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, Row, 28));
            Sm.CmParam<Decimal>(ref cm, "@EstPrice", Sm.GetGrdDec(Grd1, Row, 29));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@Bom2DocNo", Sm.GetGrdStr(Grd1, Row, 34));
            Sm.CmParam<String>(ref cm, "@FinishedGood", Sm.GetGrdStr(Grd1, Row, 35));
            Sm.CmParam<String>(ref cm, "@Component", Sm.GetGrdStr(Grd1, Row, 36));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            UpdateCancelledItem();

            string DNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 8).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelMaterialRequestDtl(TxtDocNo.Text, DNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 8).Length > 0)
                    cml.Add(CancelMaterialRequestDtl2(TxtDocNo.Text, Row));
            }

            //cml.Add(UpdateBOQDtl(TxtDocNo.Text, "E"));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, CancelInd ");
            if (!mMenuCodeForPRService)
                SQL.AppendLine("From TblMaterialRequestDtl ");
            else
                SQL.AppendLine("From TblMaterialRequestServiceDtl ");
            SQL.AppendLine("Where DocNo = @DocNo Order By DNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                IsCancelledItemNotExisted(DNo) ||
                IsCancelledItemCheckedAlready(DNo)||
                IsCancelReasonEmpty() ;
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsCancelledItemCheckedAlready(string DNo)
        {
            var SQL = new StringBuilder();
            string DocNo = string.Empty;

            if (!mMenuCodeForPRService)
            {
                SQL.AppendLine("Select DocNo From TblPORequestDtl ");
                SQL.AppendLine("Where MaterialRequestDocNo=@Param ");
                SQL.AppendLine("And (CancelInd='N' And IfNull(Status, 'O')<>'C') ");
                SQL.AppendLine("And MaterialRequestDNo In (" + DNo + ") ");
                SQL.AppendLine("Limit 1; ");
            }
            else
            {
                SQL.AppendLine("Select DocNo ");
                SQL.AppendLine("From TblMaterialRequestDtl ");
                SQL.AppendLine("Where MaterialRequestServiceDocNo = @Param ");
                SQL.AppendLine("And (CancelInd = 'N' And IfNull(Status, 'O') <> 'C') ");
                SQL.AppendLine("And MaterialRequestServiceDNo In (" + DNo + ") ");
                SQL.AppendLine("Limit 1; ");
            }

            DocNo = Sm.GetValue(SQL.ToString(), TxtDocNo.Text);

            if (DocNo.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "This document has been processed to " + DocNo);
                return true;
            }
            return false;
        }

        private MySqlCommand CancelMaterialRequestDtl(string DocNo, string DNo)
        {
            var SQL = new StringBuilder();

            if (!mMenuCodeForPRService)
            {
                SQL.AppendLine("Update TblMaterialRequestDtl Set ");
                SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And (CancelInd='N' Or Status='C') And DNo In (" + DNo + "); ");

                if (!mIsDORequestNeedStockValidation)
                {
                    SQL.AppendLine("Update TblMaterialRequestDtl Set ");
                    SQL.AppendLine("  DORequestDocNo = null, ");
                    SQL.AppendLine("  DORequestDNo = null ");
                    SQL.AppendLine("Where DocNo = @DocNo ");
                    SQL.AppendLine("And DORequestDocNo Is Not Null ");
                    SQL.AppendLine("And DORequestDNo Is Not Null ");
                    SQL.AppendLine("And DNo In (" + DNo + "); ");
                }

                if (TxtPOQtyCancelDocNo.Text.Length > 0)
                    SQL.AppendLine("Update TblPOQtyCancel Set ProcessInd='O' Where DocNo=@POQtyCancelDocNo;");

                SQL.AppendLine("Update TblPRDtl A ");
                SQL.AppendLine("Inner Join TblMaterialRequestHdr B ");
                SQL.AppendLine("    On A.DocNo=IfNull(B.PRDocNo, '') ");
                SQL.AppendLine("    And B.DocNo=@DocNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl C ");
                SQL.AppendLine("    On B.DocNo=C.DocNo ");
                SQL.AppendLine("    And A.DNo=IfNull(C.PRDNo, '') ");
                SQL.AppendLine("    And (C.CancelInd='Y' Or C.Status='C') ");
                SQL.AppendLine("    And C.DNo In (" + DNo + ") ");
                SQL.AppendLine("Set A.CancelInd='Y', A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime(); ");

                SQL.AppendLine("Update TblMakeToStockDtl A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select T1.MakeToStockDocNo As DocNo, T2.MakeToStockDNo As DNo, ");
                SQL.AppendLine("    Sum(Case When T2.CancelInd='N' Then T2.Qty1 Else 0 End) As Qty1 ");
                SQL.AppendLine("    From TblPRHdr T1 ");
                SQL.AppendLine("    Inner Join TblPRDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("            Select Distinct X1.PRDocNo As DocNo, X2.PRDNo As DNo ");
                SQL.AppendLine("            From TblMaterialRequestHdr X1 ");
                SQL.AppendLine("            Inner Join TblMaterialRequestDtl X2 ");
                SQL.AppendLine("                On X1.DocNo=X2.DocNo ");
                SQL.AppendLine("                And X2.PRDNo Is Not Null ");
                SQL.AppendLine("                And X2.DNo In (" + DNo + ") ");
                SQL.AppendLine("            Where X1.DocNo=@DocNo ");
                SQL.AppendLine("            And X1.PRDocNo Is Not Null ");
                SQL.AppendLine("    ) T3 On T1.DocNo=T3.DocNo And T2.DNo=T3.DNo ");
                SQL.AppendLine("    Group By T1.MakeToStockDocNo, T2.MakeToStockDNo ");
                SQL.AppendLine(") B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
                SQL.AppendLine("Set A.ProcessInd2 = ");
                SQL.AppendLine("    Case When A.Qty<>0 Then ");
                SQL.AppendLine("        Case When IfNull(B.Qty1, 0)=0 Then 'O' ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            Case When A.Qty>IfNull(B.Qty1, 0) Then 'P' Else 'F' End ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    Else 'F' End;");

                if (TxtDroppingRequestDocNo.Text.Length > 0)
                {
                    if (TxtDR_PRJIDocNo.Text.Length > 0)
                    {
                        SQL.AppendLine("Update TblDroppingRequestDtl A ");
                        SQL.AppendLine("Inner Join TblMaterialRequestDtl B ");
                        SQL.AppendLine("    On A.MRDocNo=B.DocNo ");
                        SQL.AppendLine("    And A.MRDNo=B.DNo ");
                        SQL.AppendLine("    And (B.CancelInd='Y' Or B.Status='C') ");
                        SQL.AppendLine("Set A.MRDocNo=Null, A.MRDNo=Null ");
                        SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                        SQL.AppendLine("And A.MRDocNo Is Not Null ");
                        SQL.AppendLine("And A.MRDocNo=@DocNo;");
                    }
                    if (TxtDR_BCCode.Text.Length > 0)
                    {
                        SQL.AppendLine("Update TblDroppingRequestDtl2 A ");
                        SQL.AppendLine("Inner Join TblMaterialRequestDtl B ");
                        SQL.AppendLine("    On A.MRDocNo=B.DocNo ");
                        SQL.AppendLine("    And A.MRDNo=B.DNo ");
                        SQL.AppendLine("    And (B.CancelInd='Y' Or B.Status='C') ");
                        SQL.AppendLine("Set A.MRDocNo=Null, A.MRDNo=Null ");
                        SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                        SQL.AppendLine("And A.MRDocNo Is Not Null ");
                        SQL.AppendLine("And A.MRDocNo=@DocNo;");
                    }
                }
            }
            else
            {
                SQL.AppendLine("Update TblMaterialRequestServiceDtl Set ");
                SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And (CancelInd='N' Or Status='C') And DNo In (" + DNo + "); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (TxtPOQtyCancelDocNo.Text.Length > 0) Sm.CmParam<String>(ref cm, "@POQtyCancelDocNo", TxtPOQtyCancelDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand CancelMaterialRequestDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            if (!mMenuCodeForPRService)
            {
                SQL.AppendLine("Update TblMaterialRequestDtl Set ");
            }
            else
            {
                SQL.AppendLine("Update TblMaterialRequestServiceDtl Set ");
            }
            SQL.AppendLine("    CancelReason=@CancelReason ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@Dno ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Dno", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));

            return cm;
        }

        private MySqlCommand UpdateMRFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            if (!mMenuCodeForPRService)
            {
                SQL.AppendLine("Update TblMaterialRequesthdr Set ");
                
            }
            else
            {
                SQL.AppendLine("Update TblMaterialRequestServiceHdr Set ");
            }
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                if (mMenuCodeForPRService)
                {
                    ShowMaterialRequestServiceHdr(DocNo);
                    ShowMaterialRequestServiceDtl(DocNo);
                }
                else
                {
                    ShowMaterialRequestHdr(DocNo);
                    ShowDroppingRequestInfo();
                    ShowMaterialRequestDtl(DocNo);
                    ComputeRemainingBudget();
                    ComputeDR_Balance();
                    SetSeqNo();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowMaterialRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.POQtyCancelDocNo, ");
            SQL.AppendLine("A.SiteCode, A.DeptCode, A.PICCode, A.ReqType, A.DORequestDocNo, ");
            SQL.AppendLine("A.BCCode, A.FileName, A.Remark, A.DroppingRequestDocNo, ");
            SQL.AppendLine("A.DroppingRequestBCCode, A.SOCDocNo, A.NTPDocNo, A.PGCode, B.ProjectCode, B.ProjectName, C.PONo ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Left Join TblProjectGroup B on A.PGCode=B.PGCode ");
            SQL.AppendLine("Left Join TblSOContractHdr C on A.SOCDocNo=C.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "DocNo", 
                        "LocalDocNo", "DocDt", "POQtyCancelDocNo", "SiteCode", "DeptCode",  
                        "ReqType", "BCCode", "FileName", "Remark", "DORequestDocNo",
                        "PICCode", "DroppingRequestDocNo", "DroppingRequestBCCode", "SOCDocNo", "NTPDocNo",
                        "PGCode", "ProjectCode", "ProjectName", "PONo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        TxtPOQtyCancelDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[4]), "N");            
                        Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[5]), "N");                              
                        SetLueReqType(ref LueReqType, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueReqType, Sm.DrStr(dr, c[6]));
                        Sl.SetLueBCCode(ref LueBCCode, Sm.DrStr(dr, c[7]), string.Empty);
                        TxtFile.EditValue = Sm.DrStr(dr, c[8]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                        TxtDORequestDocNo.EditValue = Sm.DrStr(dr, c[10]);
                        Sm.SetLue(LuePICCode, Sm.DrStr(dr, c[11]));
                        TxtDroppingRequestDocNo.EditValue = Sm.DrStr(dr, c[12]);
                        mDroppingRequestBCCode = Sm.DrStr(dr, c[13]);
                        TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[14]);
                        TxtNTPDocNo.EditValue = Sm.DrStr(dr, c[15]);
                        mPGCode = Sm.DrStr(dr, c[16]);
                        TxtProjectCode.EditValue = Sm.DrStr(dr, c[17]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[18]);
                        TxtPONo.EditValue = Sm.DrStr(dr, c[19]);
                    }, true
                );
        }

        private void ShowDroppingRequestInfo()
        {
            if (!mIsMaterialRequestForDroppingRequestEnabled || TxtDroppingRequestDocNo.Text.Length == 0) return;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.DeptName, B.Yr, B.Mth, B.PRJIDocNo, D.BCName, B.Remark, ");
            SQL.AppendLine("Case When A.DroppingRequestBCCode Is Not Null Then ");
            SQL.AppendLine("(Select Sum(Amt) From TblDroppingRequestDtl2 Where DocNo=A.DroppingRequestDocNo And BCCode=A.DroppingRequestBCCode) ");
            SQL.AppendLine("Else B.Amt End As Amt ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Left Join TblDroppingRequestHdr B On A.DroppingRequestDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblBudgetCategory D On A.DroppingRequestBCCode=D.BCCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "DeptName", 
                        "Yr", "Mth", "PRJIDocNo", "BCName", "Remark",
                        "Amt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDR_DeptCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtDR_Yr.EditValue = Sm.DrStr(dr, c[1]);
                        TxtDR_Mth.EditValue = Sm.DrStr(dr, c[2]);
                        TxtDR_PRJIDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtDR_BCCode.EditValue = Sm.DrStr(dr, c[4]);
                        MeeDR_Remark.EditValue = Sm.DrStr(dr, c[5]);
                        TxtDR_DroppingRequestAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    }, true
                );
        }

        private void ShowMaterialRequestDtl(string DocNo)
        {
                
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.CancelReason, ");
            SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As StatusDesc, ");
            SQL.AppendLine("(   Select T2.UserName From TblDocApproval T1, TblUser T2 ");
            SQL.AppendLine("    Where T1.DocType='MaterialRequest' And T1.DocNo=@DocNo And T1.DNo=A.DNo And T1.UserCode=T2.UserCode And T1.UserCode Is Not Null  ");
            SQL.AppendLine("    Order By ApprovalDNo Desc Limit 1");
            SQL.AppendLine(") As UserName, A.DORequestDocNo, A.DORequestDNo,  ");
            SQL.AppendLine("A.ItCode, B.ItCodeInternal, B.ItName, B.Specification,  B.ItScCode, D.ItScName, B.MinStock, B.ReorderStock, A.Qty, B.PurchaseUomCode, IfNull(E.CancelledQty, 0.00) CancelledQty, ");
            SQL.AppendLine("A.UsageDt, A.QtDocNo, A.QtDNo, C.DocDt, A.UPrice, (A.Qty*A.UPrice) As Total, A.Remark, A.CurCode, A.EstPrice, A.Bom2DocNo, A.FinishedGood, A.Component, ");
            if (mIsMaterialRequestForDroppingRequestEnabled)
            {
                Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
                
                if (TxtDR_PRJIDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("(Select IfNull(T3.Amt, 0.00) As Amt ");
                    SQL.AppendLine("From TblDroppingRequestDtl T1, TblProjectImplementationRBPHdr T2, TblProjectImplementationDtl2 T3 ");
                    SQL.AppendLine("Where T1.PRBPDocNo=T2.DocNo And T2.ResourceItCode=A.ItCode And T2.PRJIDocNo=T3.DocNo And T2.ResourceItCode=T3.ResourceItCode ");
                    SQL.AppendLine("And T1.DocNo=@DroppingRequestDocNo) As DroppingRequestAmt ");
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mDroppingRequestBCCode);

                    SQL.AppendLine("(Select Amt ");
                    SQL.AppendLine("From TblDroppingRequestDtl2 ");
                    SQL.AppendLine("Where DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("And BCCode=@DroppingRequestBCCode ");
                    SQL.AppendLine("And ItCode=A.ItCode ");
                    SQL.AppendLine(") As DroppingRequestAmt ");
                }
            }
            else
            {
                SQL.AppendLine("0.00 As DroppingRequestAmt ");
            }

            SQL.AppendLine("From TblMaterialRequestDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblQtHdr C On A.QtDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblItemSubCategory D On B.ItScCode = D.ItScCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select MRDocNo, MRDNo, Sum(Qty) CancelledQty ");
            SQL.AppendLine("    From TblMRQtyCancel ");
            SQL.AppendLine("    Where MRDocNo = @DocNo ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    Group By MRDocNo, MRDNo ");
            SQL.AppendLine(") E On A.DocNo = E.MRDocNo And A.DNo = E.MRDNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ItCode");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "CancelInd", "CancelReason", "StatusDesc", "UserName", "ItCode",  
                    
                    //6-10
                    "ItCodeInternal", "ItName", "Specification", "ItScCode", "ItScName",  
                    
                    //11-15
                    "MinStock", "ReorderStock", "Qty", "PurchaseUomCode", "UsageDt", 
                    
                    //16-20
                    "QtDocNo", "QtDNo", "DocDt", "UPrice", "Total", 
                    
                    //21-25
                    "Remark", "DORequestDocNo", "DORequestDNo", "CurCode", "EstPrice",
                    
                    //26-30
                    "DroppingRequestAmt", "Bom2DocNo", "FinishedGood", "Component", "CancelledQty",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 25);
                    Grd.Cells[Row, 31].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 26);
                    Grd.Cells[Row, 33].Value = 0m;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 27);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 28);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 29);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 30);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 15, 16, 17, 29, 31, 32, 33, 37 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowMaterialRequestServiceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.POQtyCancelDocNo, ");
            SQL.AppendLine("A.SiteCode, A.DeptCode, A.PICCode, A.ReqType, A.DORequestDocNo, ");
            SQL.AppendLine("null As DroppingRequestDocNo, null As DroppingRequestBCCode, A.BCCode, A.FileName, A.Remark, ");
            SQL.AppendLine("A.SOCDocNo, A.NTPDocNo, A.PGCode, B.ProjectCode, B.ProjectName, C.PONo ");
            SQL.AppendLine("From TblMaterialRequestServiceHdr A ");
            SQL.AppendLine("Left Join TblProjectGroup B on A.PGCode=B.PGCode ");
            SQL.AppendLine("Left Join TblSOContractHdr C on A.SOCDocNo=C.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DocNo", 
                    "LocalDocNo", "DocDt", "POQtyCancelDocNo", "SiteCode", "DeptCode",  
                    "ReqType", "BCCode", "FileName", "Remark", "DORequestDocNo",
                    "PICCode", "DroppingRequestDocNo", "DroppingRequestBCCode", "SOCDocNo", "NTPDocNo",
                    "PGCode", "ProjectCode", "ProjectName", "PONo"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                    TxtPOQtyCancelDocNo.EditValue = Sm.DrStr(dr, c[3]);
                    Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[4]), "N");
                    Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[5]), "N");
                    SetLueReqType(ref LueReqType, Sm.DrStr(dr, c[6]));
                    Sm.SetLue(LueReqType, Sm.DrStr(dr, c[6]));
                    Sl.SetLueBCCode(ref LueBCCode, Sm.DrStr(dr, c[7]), string.Empty);
                    TxtFile.EditValue = Sm.DrStr(dr, c[8]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                    TxtDORequestDocNo.EditValue = Sm.DrStr(dr, c[10]);
                    Sm.SetLue(LuePICCode, Sm.DrStr(dr, c[11]));
                    TxtDroppingRequestDocNo.EditValue = Sm.DrStr(dr, c[12]);
                    mDroppingRequestBCCode = Sm.DrStr(dr, c[13]);
                    TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[14]);
                    TxtNTPDocNo.EditValue = Sm.DrStr(dr, c[15]);
                    mPGCode = Sm.DrStr(dr, c[16]);
                    TxtProjectCode.EditValue = Sm.DrStr(dr, c[17]);
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[18]);
                    TxtPONo.EditValue = Sm.DrStr(dr, c[19]);
                }, true
            );
        }

        private void ShowMaterialRequestServiceDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.CancelReason, ");
            SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As StatusDesc, ");
            SQL.AppendLine("Null As UserName, A.DORequestDocNo, A.DORequestDNo,  ");
            SQL.AppendLine("A.ItCode, B.ItCodeInternal, B.ItName, B.Specification, B.ItScCode, D.ItScName, B.MinStock, B.ReorderStock, A.Qty, B.PurchaseUomCode, IfNull(E.CancelledQty, 0.00) CancelledQty, ");
            SQL.AppendLine("A.UsageDt, A.QtDocNo, A.QtDNo, C.DocDt, A.UPrice, (A.Qty*A.UPrice) As Total, A.Remark, A.CurCode, A.EstPrice, A.Bom2DocNo, A.FinishedGood, A.Component, ");
            SQL.AppendLine("0.00 As DroppingRequestAmt, A.ItCode2, F.ItCodeInternal ItCodeInternal2, F.ItName ItName2, F.Specification Specification2 ");
            SQL.AppendLine("From TblMaterialRequestServiceDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblQtHdr C On A.QtDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblItemSubCategory D On B.ItScCode = D.ItScCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select MRDocNo, MRDNo, Sum(Qty) CancelledQty ");
            SQL.AppendLine("    From TblMRQtyCancel ");
            SQL.AppendLine("    Where MRDocNo = @DocNo ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    Group By MRDocNo, MRDNo ");
            SQL.AppendLine(") E On A.DocNo = E.MRDocNo And A.DNo = E.MRDNo ");
            SQL.AppendLine("Left Join TblItem F On A.ItCode2 = F.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "CancelInd", "CancelReason", "StatusDesc", "UserName", "ItCode",  
                    
                    //6-10
                    "ItCodeInternal", "ItName", "Specification", "ItScCode", "ItScName",  
                    
                    //11-15
                    "MinStock", "ReorderStock", "Qty", "PurchaseUomCode", "UsageDt", 
                    
                    //16-20
                    "QtDocNo", "QtDNo", "DocDt", "UPrice", "Total", 
                    
                    //21-25
                    "Remark", "DORequestDocNo", "DORequestDNo", "CurCode", "EstPrice",
                    
                    //26-30
                    "DroppingRequestAmt", "Bom2DocNo", "FinishedGood", "Component", "CancelledQty",

                    //31-34
                    "ItCode2", "ItCodeInternal2", "ItName2", "Specification2"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 25);
                    Grd.Cells[Row, 31].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 26);
                    Grd.Cells[Row, 33].Value = 0m;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 27);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 28);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 29);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 30);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 31);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 41, 32);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 33);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 34);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 15, 16, 17, 29, 31, 32, 33, 37 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void ProcessCSV()
        {
            if (!mMenuCodeForPRService) return;

            var l = new List<CSVItemDetail>();
            string ItCode1 = string.Empty, ItCode2 = string.Empty;
            GetCSVData(ref l);
            if (l.Count > 0)
            {
                if (CSVDataNotValid(ref l)) return;
                
                PrepItemData(ref l, ref ItCode1, ref ItCode2);
                l.RemoveAll(m => m.PRItName.Length == 0 || m.ItName.Length == 0);
                GetStockData(ref l, ref ItCode1);
                GetBom2Data(ref l, ref ItCode1);
                SetDataToGrd(ref l); //ClearGrd dulu baru ceplokin
            }
            else
                Sm.StdMsg(mMsgType.NoData, string.Empty);

            l.Clear();
        }

        private void GetStockData(ref List<CSVItemDetail> l, ref string ItCode1)
        {
            var SQL2 = new StringBuilder();
            var cm = new MySqlCommand();

            SQL2.AppendLine("    Select ItCode, Sum(Qty) Stock ");
            SQL2.AppendLine("    From TblStockSummary ");
            SQL2.AppendLine("    Where Qty <> 0 ");
            SQL2.AppendLine("    And BatchNo Is Not Null ");
            SQL2.AppendLine("    And BatchNo Not In (Select ProjectCode from TblProjectGroup Where ProjectCode Is Not Null) ");
            SQL2.AppendLine("    And Find_IN_set(ItCode, @ItCode1) ");
            SQL2.AppendLine("    And ItCode In ( ");
            SQL2.AppendLine("        Select ItCode ");
            SQL2.AppendLine("        From TblBom2Hdr ");
            SQL2.AppendLine("        Where SOContractDocNo Is Not Null ");
            SQL2.AppendLine("        And SOContractDocNo=@SOContractDocNo ");
            SQL2.AppendLine("        And CancelInd='N' ");
            SQL2.AppendLine("        And Status='A' ");
            if (mMenuCodeForPRService)
                SQL2.AppendLine("        And ServiceItemInd = 'Y' ");
            else
                SQL2.AppendLine("        And ServiceItemInd = 'N' ");
            SQL2.AppendLine("    Union All ");
            SQL2.AppendLine("        Select B.ItCode ");
            SQL2.AppendLine("        From TblBom2Hdr A, TblBom2Dtl B ");
            SQL2.AppendLine("        Where A.DocNo=B.DocNo ");
            SQL2.AppendLine("        And A.SOContractDocNo Is Not Null ");
            SQL2.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
            SQL2.AppendLine("        And A.CancelInd='N' ");
            SQL2.AppendLine("        And A.Status='A' ");
            SQL2.AppendLine("    Union All ");
            SQL2.AppendLine("        Select B.ItCode2 ");
            SQL2.AppendLine("        From TblBom2Hdr A, TblBom2Dtl2 B ");
            SQL2.AppendLine("        Where A.DocNo=B.DocNo ");
            SQL2.AppendLine("        And A.SOContractDocNo Is Not Null ");
            SQL2.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
            SQL2.AppendLine("    )  ");
            SQL2.AppendLine("    Group By ItCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm, "@ItCode1", ItCode1);
                Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOCDocNo.Text);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "Stock" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var x in l.Where(w => w.PRItCode == Sm.DrStr(dr, c[0])))
                        {
                            x.Stock = Sm.DrDec(dr, c[1]);
                        }
                    }
                }
                dr.Close();
            }

        }

        private void GetBom2Data(ref List<CSVItemDetail> l, ref string ItCode1)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.Bom2DocNo, A.ItCode, B.FinishedGood, C.Component From ( ");
            SQL.AppendLine("    Select Distinct DocNo As Bom2DocNo, ItCode From ( ");
            SQL.AppendLine("    Select DocNo, ItCode ");
            SQL.AppendLine("    From TblBom2Hdr ");
            SQL.AppendLine("    Where SOContractDocNo Is Not Null ");
            SQL.AppendLine("    And SOContractDocNo=@SOContractDocNo ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And Status='A' ");
            SQL.AppendLine("    And Find_In_Set(ItCode, @ItCode1) ");
            if (mMenuCodeForPRService)
                SQL.AppendLine("    And ServiceItemInd = 'Y' ");
            else
                SQL.AppendLine("    And ServiceITemInd = 'N' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.DocNo, B.ItCode ");
            SQL.AppendLine("    From TblBom2Hdr A, TblBom2Dtl B ");
            SQL.AppendLine("    Where A.DocNo=B.DocNo ");
            SQL.AppendLine("    And A.SOContractDocNo Is Not Null ");
            SQL.AppendLine("    And A.SOContractDocNo=@SOContractDocNo ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine("    And A.Status='A' ");
            SQL.AppendLine("    And Find_In_Set(B.ItCode, @ItCode1) ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.DocNo, B.ItCode2 as ItCode ");
            SQL.AppendLine("    From TblBom2Hdr A, TblBom2Dtl2 B ");
            SQL.AppendLine("    Where A.DocNo=B.DocNo ");
            SQL.AppendLine("    And A.SOContractDocNo Is Not Null ");
            SQL.AppendLine("    And A.SOContractDocNo=@SOContractDocNo ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine("    And A.Status='A' ");
            SQL.AppendLine("    And Find_In_Set(B.ItCode2, @ItCode1) ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select DocNo As Bom2DocNo, ItCode, ");
            SQL.AppendLine("    Group_Concat(Distinct ItName Order By ItName Separator ', ') As FinishedGood ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T1.DocNo, T1.ItCode, T2.ItName ");
            SQL.AppendLine("        From TblBom2Hdr T1, TblItem T2 ");
            SQL.AppendLine("        Where T1.SOContractDocNo Is Not Null ");
            SQL.AppendLine("        And T1.SOContractDocNo=@SOContractDocNo ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.Status='A' ");
            SQL.AppendLine("        And T1.ItCode=T2.ItCode ");
            SQL.AppendLine("        And Find_In_Set(T1.ItCode, @ItCode1) ");
            if (mMenuCodeForPRService)
                SQL.AppendLine("        And T1.ServiceItemInd = 'Y' ");
            else
                SQL.AppendLine("        And T1.ServiceItemInd = 'N' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select A.DocNo, B.ItCode, C.ItName ");
            SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl B, TblItem C ");
            SQL.AppendLine("        Where A.DocNo=B.DocNo ");
            SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
            SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
            SQL.AppendLine("        And A.CancelInd='N' ");
            SQL.AppendLine("        And A.Status='A' ");
            SQL.AppendLine("        And A.ItCode=C.ItCode ");
            SQL.AppendLine("        And Find_In_Set(A.ItCode, @ItCode1) ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select A.DocNo, B.ItCode2, C.ItName ");
            SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl2 B, TblItem C ");
            SQL.AppendLine("        Where A.DocNo=B.DocNo ");
            SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
            SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
            SQL.AppendLine("        And A.CancelInd='N' ");
            SQL.AppendLine("        And A.Status='A' ");
            SQL.AppendLine("        And A.ItCode=C.ItCode ");
            SQL.AppendLine("        And Find_In_Set(A.ItCode, @ItCode1) ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Where ItName Is Not Null ");
            SQL.AppendLine("    Group By DocNo, ItCode ");
            SQL.AppendLine(") B On A.Bom2DocNo=B.Bom2DocNo And A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select DocNo As Bom2DocNo, ItCode, ");
            SQL.AppendLine("    Group_Concat(Distinct ItName Order By ItName Separator ', ' ) As Component ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.DocNo, B.ItCode, C.ItName ");
            SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl B, TblItem C ");
            SQL.AppendLine("        Where A.DocNo=B.DocNo ");
            SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
            SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
            SQL.AppendLine("        And A.CancelInd='N' ");
            SQL.AppendLine("        And A.Status='A' ");
            SQL.AppendLine("        And B.ItCode=C.ItCode ");
            SQL.AppendLine("        And Find_In_Set(B.ItCode, @ItCode1) ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select A.DocNo, B.ItCode, C.ItName ");
            SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl B, TblItem C ");
            SQL.AppendLine("        Where A.DocNo=B.DocNo ");
            SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
            SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
            SQL.AppendLine("        And A.CancelInd='N' ");
            SQL.AppendLine("        And A.Status='A' ");
            SQL.AppendLine("        And B.ItCode=C.ItCode ");
            SQL.AppendLine("        And Find_In_Set(B.ItCode, @ItCode1) ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select A.DocNo, B.ItCode2, C.ItName ");
            SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl2 B, TblItem C ");
            SQL.AppendLine("        Where A.DocNo=B.DocNo ");
            SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
            SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
            SQL.AppendLine("        And A.CancelInd='N' ");
            SQL.AppendLine("        And A.Status='A' ");
            SQL.AppendLine("        And B.ItCode=C.ItCode ");
            SQL.AppendLine("        And Find_In_Set(B.ItCode, @ItCode1) ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Where ItName Is Not Null ");
            SQL.AppendLine("    Group By DocNo, ItCode ");
            SQL.AppendLine(") C On A.Bom2DocNo=B.Bom2DocNo And A.ItCode=B.ItCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@ItCode1", ItCode1);
                Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOCDocNo.Text);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Bom2DocNo", "ItCode", "FinishedGood", "Component" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var x in l.Where(w => w.PRItCode == Sm.DrStr(dr, c[1])))
                        {
                            x.BOM2DocNo = Sm.DrStr(dr, c[0]);
                            x.FinishedGood = Sm.DrStr(dr, c[2]);
                            x.Component = Sm.DrStr(dr, c[3]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetDataToGrd(ref List<CSVItemDetail> l)
        {
            ClearGrd();

            Grd1.BeginUpdate();
            int Row = 0;
            foreach(var x in l)
            {
                Grd1.Rows.Add();
                Grd1.Cells[Row, 8].Value = x.PRItCode;
                Grd1.Cells[Row, 9].Value = x.PRItCodeInternal;
                Grd1.Cells[Row, 11].Value = x.PRItName;
                Grd1.Cells[Row, 12].Value = x.PRSpecification;
                Grd1.Cells[Row, 13].Value = x.ItScCode;
                Grd1.Cells[Row, 14].Value = x.ItScName;
                Grd1.Cells[Row, 15].Value = Sm.FormatNum(x.MinStock, 0);
                Grd1.Cells[Row, 16].Value = Sm.FormatNum(x.ReorderPoint, 0);
                Grd1.Cells[Row, 17].Value = Sm.FormatNum(x.Qty, 0);
                Grd1.Cells[Row, 18].Value = x.Uom;
                Grd1.Cells[Row, 19].Value = Sm.ConvertDate(x.UsageDt);
                Grd1.Cells[Row, 20].Value = x.QtDocNo;
                Grd1.Cells[Row, 21].Value = x.QtDNo;
                if (x.QtDocDt.Length > 0) Grd1.Cells[Row, 22].Value = Sm.ConvertDate(x.QtDocDt);
                else Grd1.Cells[Row, 22].Value = string.Empty;
                Grd1.Cells[Row, 23].Value = Sm.FormatNum(x.QtUPrice, 0);
                Grd1.Cells[Row, 24].Value = Sm.FormatNum(x.Total, 0);
                Grd1.Cells[Row, 25].Value = x.Remark;
                Grd1.Cells[Row, 29].Value = Sm.FormatNum(0m, 0);
                Grd1.Cells[Row, 31].Value = Sm.FormatNum(0m, 0);
                Grd1.Cells[Row, 32].Value = Sm.FormatNum(0m, 0);
                Grd1.Cells[Row, 33].Value = Sm.FormatNum(x.Stock, 0);
                Grd1.Cells[Row, 34].Value = x.BOM2DocNo;
                Grd1.Cells[Row, 35].Value = x.FinishedGood;
                Grd1.Cells[Row, 36].Value = x.Component;
                Grd1.Cells[Row, 37].Value = Sm.FormatNum(0m, 0);
                Grd1.Cells[Row, 40].Value = x.ItCode;
                Grd1.Cells[Row, 41].Value = x.ItCodeInternal;
                Grd1.Cells[Row, 42].Value = x.ItName;
                Grd1.Cells[Row, 43].Value = x.Specification;

                ComputeTotal(Row);

                Row += 1;
            }
            Grd1.EndUpdate();

            SetSeqNo();
        }

        private bool CSVDataNotValid(ref List<CSVItemDetail> l)
        {
            foreach(var x in l)
            {
                if (x.PRItCode.Trim().Length == 0) { Sm.StdMsg(mMsgType.Warning, "PR Service Item Code is required."); return true; }
                if (x.ItCode.Trim().Length == 0) { Sm.StdMsg(mMsgType.Warning, "Item Code is required."); return true; }
                if (x.Qty.ToString().Trim().Length == 0) { Sm.StdMsg(mMsgType.Warning, "Quantity is required."); return true; }
                if (x.UsageDt.Trim().Length == 0) { Sm.StdMsg(mMsgType.Warning, "Usage Date is required."); return true; }
                if (x.Remark.Trim().Length == 0) { Sm.StdMsg(mMsgType.Warning, "Remark is required."); return true; }

                if (x.UsageDt.Contains("/") || x.UsageDt.Contains("-") || x.UsageDt.Contains(".") || x.UsageDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Warning, "Usage Date's format should be 'yyyymmdd'."); return true;
                }
            }

            return false;
        }

        private void GetCSVData(ref List<CSVItemDetail> l)
        {
            OD.InitialDirectory = "c:";
            OD.Filter = "CSV files (*.csv)|*.CSV";
            OD.FilterIndex = 2;
            OD.ShowDialog();

            var FileName = OD.FileName;

            using (var rd = new StreamReader(FileName))
            {
                bool IsFirst = true;
                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits.Count() != 5)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid data.");
                        return;
                    }
                    else
                    {
                        var arr = splits.ToArray();
                        if (arr[0].Trim().Length > 0)
                        {
                            if (IsFirst) IsFirst = false;
                            else
                            {
                                l.Add(new CSVItemDetail()
                                {
                                    PRItCode = splits[0].Trim(),
                                    ItCode = splits[1].Trim(),
                                    Qty = Decimal.Parse(splits[2].Trim().Length > 0 ? splits[2].Trim() : "0"),
                                    UsageDt = splits[3].Trim(),
                                    Remark = splits[4].Trim(),

                                    PRItCodeInternal = string.Empty,
                                    PRItName = string.Empty,
                                    PRSpecification = string.Empty,
                                    ItCodeInternal = string.Empty,
                                    ItName = string.Empty,
                                    Specification = string.Empty,

                                    ItScCode = string.Empty,
                                    ItScName = string.Empty,
                                    Uom = string.Empty,
                                    ReorderPoint = 0m,
                                    MinStock = 0m,
                                    Stock = 0m,
                                    QtDocNo = string.Empty,
                                    QtDNo = string.Empty,
                                    QtDocDt = string.Empty,
                                    QtUPrice = 0m,
                                    Total = 0m,

                                    BOM2DocNo = string.Empty,
                                    Component = string.Empty,
                                    FinishedGood = string.Empty
                                });
                            }
                        }
                        else
                        {
                            Sm.StdMsg(mMsgType.NoData, "");
                            return;
                        }
                    }
                }
            }
        }

        private void PrepItemData(ref List<CSVItemDetail> l, ref string ItCode1, ref string ItCode2)
        {
            foreach(var x in l)
            {
                if (ItCode1.Length > 0) ItCode1 += ",";
                if (ItCode2.Length > 0) ItCode2 += ",";

                ItCode1 += x.PRItCode;
                ItCode2 += x.ItCode;
            }

            if (ItCode1.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select A.ItCode, A.ItCodeInternal, A.ItName, A.Specification, A.PurchaseUomCode, ");
                SQL.AppendLine("A.MinStock, A.ReorderStock, A.ItScCode, B.ItScName ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Left Join TblItemSubCategory B On A.ItScCode = B.ItScCode ");
                SQL.AppendLine("Where A.ActInd = 'Y' ");
                SQL.AppendLine("And A.ServiceItemInd = 'Y' ");
                SQL.AppendLine("And Find_In_Set(A.ItCode, @ItCode1); ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@ItCode1", ItCode1);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        "ItCode", 
                        "ItCodeInternal", "ItName", "Specification", "PurchaseUomCode", "MinStock",
                        "ReorderStock", "ItScCode", "ItScName"
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            foreach(var x in l.Where(w => w.PRItCode == Sm.DrStr(dr, c[0])))
                            {
                                x.PRItCodeInternal = Sm.DrStr(dr, c[1]);
                                x.PRItName = Sm.DrStr(dr, c[2]);
                                x.PRSpecification = Sm.DrStr(dr, c[3]);
                                x.Uom = Sm.DrStr(dr, c[4]);
                                x.MinStock = Sm.DrDec(dr, c[5]);
                                x.ReorderPoint = Sm.DrDec(dr, c[6]);
                                x.ItScCode = Sm.DrStr(dr, c[7]);
                                x.ItScName = Sm.DrStr(dr, c[8]);
                            }
                        }
                    }
                    dr.Close();
                }
            }

            if (ItCode2.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select A.ItCode, A.ItCodeInternal, A.ItName, A.Specification ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Where A.ActInd = 'Y' ");
                SQL.AppendLine("And A.ServiceItemInd = 'Y' ");
                SQL.AppendLine("And Find_In_Set(A.ItCode, @ItCode2); ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@ItCode2", ItCode2);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        "ItCode", 
                        "ItCodeInternal", "ItName", "Specification"
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            foreach (var x in l.Where(w => w.ItCode == Sm.DrStr(dr, c[0])))
                            {
                                x.ItCodeInternal = Sm.DrStr(dr, c[1]);
                                x.ItName = Sm.DrStr(dr, c[2]);
                                x.Specification = Sm.DrStr(dr, c[3]);
                            }
                        }
                    }
                    dr.Close();
                }
            }
        }

        private void CheckServiceItemInd()
        {
            string ItCode = string.Empty;

            for (int i = 0; i <Grd1.Rows.Count - 1; ++i)
            {
                if (ItCode.Length > 0) ItCode += ",";
                ItCode += Sm.GetGrdStr(Grd1, i, 8);
            }

            if (ItCode.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                var l = new List<MRItemService>();

                SQL.AppendLine("Select ItCode, ServiceItemInd ");
                SQL.AppendLine("From TblItem ");
                SQL.AppendLine("Where Find_In_Set(ItCode, @ItCode); ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@ItCode", ItCode);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "ServiceItemInd" });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new MRItemService()
                            {
                                ItCode = Sm.DrStr(dr, c[0]),
                                ServiceItemInd = Sm.DrStr(dr, c[1])
                            });
                        }
                    }
                    dr.Close();
                }

                if (l.Count > 0)
                {
                    for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                    {
                        foreach (var x in l.Where(w => w.ItCode == Sm.GetGrdStr(Grd1, i, 8)))
                        {
                            Grd1.Cells[i, 38].Value = x.ServiceItemInd;
                        }
                    }
                }

                l.Clear();
            }
        }

        internal void SetSeqNo()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
                Grd1.Cells[r, 30].Value = r + 1;
        }

        public static string GetNumber(string Dno)
        {
            string number = string.Empty;
            for (int ind = 0; ind < Dno.Length; ind++)
            {
                if (Char.IsNumber(Dno[ind]) == true)
                {
                    number = number + Dno[ind];
                }

            }
            return number;
        }

        private string GenerateDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                ShortCode = string.Empty,
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            bool IsDocNoFormatUseFullYear = Sm.GetParameter("IsDocNoFormatUseFullYear")=="Y";

            if (mIsDocNoWithDeptShortCode)
                ShortCode = Sm.GetValue("Select ShortCode From TblDepartment Where DeptCode='" + Sm.GetLue(LueDeptCode) + "';");
            
            var SQL = new StringBuilder();

            if (IsDocNoFormatUseFullYear)
            {
                Yr = Sm.Left(DocDt, 4);

                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', ");
                if (mIsDocNoWithDeptShortCode && ShortCode.Length > 0)
                    SQL.Append("'" + ShortCode + "', '/', ");
                SQL.Append("'" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                if (IsProcFormat == "1")
                {
                    SQL.Append("Select Concat('" + SubCategory + "', '/', ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("         Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("      Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
                else
                {
                    SQL.Append("Select Concat( ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', ");
                    if (mIsDocNoWithDeptShortCode && ShortCode.Length > 0)
                        SQL.Append("'" + ShortCode + "', '/', ");
                    SQL.Append("'" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
            }

            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateLocalDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                Yr = DocDt.Substring(0, 4),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DeptCode = Sm.GetValue("Select ShortCode From TblDepartment Where DeptCode = '"+Sm.GetLue(LueDeptCode)+"' ");


            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('000000', Convert(LocalDocNo+1, Char)), 6) From ( ");
            SQL.Append("       Select Convert(ifnull(Max(LocalDocNo), 0), Decimal) As LocalDocNo From " + Tbl);
            SQL.Append("       Order By Convert(ifnull(Max(LocalDocNo), 0), Decimal) Desc Limit 1");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '000001'), ");
            SQL.Append(" '/', '" + DocAbbr + "', '/', '"+DeptCode+"', '/', '"+SubCategory+"', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As LocalDocNo");

            return Sm.GetValue(SQL.ToString());
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 8) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedItemBOM()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 8) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ComputeTotal(int Row)
        {
            decimal Qty = 0m, UPrice = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 17).Length!=0) Qty = Sm.GetGrdDec(Grd1, Row, 17);
            if (Sm.GetGrdStr(Grd1, Row, 23).Length!=0) UPrice = Sm.GetGrdDec(Grd1, Row, 23);

            Grd1.Cells[Row, 24].Value = Qty*UPrice;

            ComputeRemainingBudget();
            ComputeDR_Balance();
        }

        private decimal ComputeAvailableBudget()
        {
            string AvailableBudget = "0";

            if (Sm.GetLue(LueDeptCode).Length != 0 && Sm.GetDte(DteDocDt).Length!=0 && Sm.CompareStr(Sm.GetLue(LueReqType), "1"))
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select ");
                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Amt From TblBudget ");
                SQL.AppendLine("        Where DeptCode=@DeptCode ");
                SQL.AppendLine("        And Yr=Left(@DocDt, 4) ");
                SQL.AppendLine("        And Mth=Substring(@DocDt, 5, 2) ");
                //SQL.AppendLine("        And UserCode Is Not Null ");
                SQL.AppendLine("    ), 0.00)- ");
                
                if (mMRAvailableBudgetSubtraction == "1")
                {
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Sum(B.Qty*B.UPrice) ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A, TblMaterialRequestDtl B ");
                    SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                    if (mBudgetBasedOn == "1")
                        SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    if (mBudgetBasedOn == "2")
                        SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    SQL.AppendLine("        And A.ReqType='1' ");
                    SQL.AppendLine("        And B.CancelInd='N' ");
                    SQL.AppendLine("        And B.Status<>'C' ");
                    SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    SQL.AppendLine("       And A.DocNo<>@DocNo ");
                    SQL.AppendLine("    ),0.00)+");
                }
                
                if (mMRAvailableBudgetSubtraction == "2")
                {
                    SQL.AppendLine("    IfNull(( ");

                    #region Old Code

                    //SQL.AppendLine("        Select Sum(E.Qty*D.UPrice)  ");
                    //SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                    //SQL.AppendLine("        Inner Join TblMaterialRequestDtl B on A.Docno = B.DocNo ");
                    //SQL.AppendLine("        Inner Join TblPORequestDtl C  On B.DocNo=C.MaterialRequestDocNo And B.DNo = C.MaterialRequestDNo ");
                    //SQL.AppendLine("        Inner Join TblQTDtl D On D.DocNo = C.QtDocNo And D.DNo = C.QtDNo ");
                    //SQL.AppendLine("        Inner Join TblPODtl E on E.PORequestDocNo = C.DocNo And E.PORequestDNo = C.DNo And E.CancelInd = 'N' ");

                    //if (mBudgetBasedOn == "1")
                    //    SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    //if (mBudgetBasedOn == "2")
                    //    SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    //SQL.AppendLine("        And A.ReqType='1' ");
                    //SQL.AppendLine("        And B.CancelInd='N' ");
                    //SQL.AppendLine("        And B.Status<>'C' ");
                    //SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    //SQL.AppendLine("       And A.DocNo<>@DocNo ");

                    #endregion

                    SQL.AppendLine("    Select Sum( ");
                    SQL.AppendLine("    X2.Amt -  ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("      IfNull( X1.DiscountAmt *  ");
                    SQL.AppendLine("          Case When X1.CurCode = @MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("          IfNull(( ");
                    SQL.AppendLine("              Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("              Where RateDt<=X1.DocDt And CurCode1=X1.CurCode And CurCode2=@MainCurCode  ");
                    SQL.AppendLine("              Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End ");
                    SQL.AppendLine("      , 0.00) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("    From TblPOHdr X1 ");
                    SQL.AppendLine("    Inner Join ( ");
                    SQL.AppendLine("        Select A.DeptCode, E.DocNo, Sum(  ");
                    SQL.AppendLine("        ((((100.00-D.Discount)*0.01)*(D.Qty*G.UPrice))-D.DiscountAmt+D.RoundingValue)  ");
                    SQL.AppendLine("        * Case When F.CurCode=@MainCurCode Then 1.00 Else  ");
                    SQL.AppendLine("        IfNull((  ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode  ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End  ");
                    SQL.AppendLine("        ) As Amt  ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                    SQL.AppendLine("        Inner Join TblMaterialRequestDtl B  ");
                    SQL.AppendLine("            On A.DocNo=B.DocNo ");
                    SQL.AppendLine("            And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    SQL.AppendLine("            And A.DocNo <> @DocNo ");
                    SQL.AppendLine("            And B.CancelInd='N'  ");
                    SQL.AppendLine("            And B.Status In ('A', 'O')  ");
                    //SQL.AppendLine("            And A.Status='A'  ");
                    SQL.AppendLine("            And A.Reqtype='1'   ");

                    if (mBudgetBasedOn == "1")
                        SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    if (mBudgetBasedOn == "2")
                        SQL.AppendLine("        And A.SiteCode=@DeptCode ");

                    SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N'  ");
                    SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo=F.DocNo  ");
                    SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo  ");
                    SQL.AppendLine("        Group By A.DeptCode, E.DocNo ");
                    SQL.AppendLine("    ) X2 On X1.DocNo = X2.DocNo ");
                    SQL.AppendLine("    Group By X2.DeptCode ");

                    SQL.AppendLine("    ),0.00)+");
                }

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(A.Qty*D.UPrice) ");
                SQL.AppendLine("        From TblPOQtyCancel A ");
                SQL.AppendLine("        Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestHdr E ");
                SQL.AppendLine("            On D.DocNo=E.DocNo  ");
                if (mBudgetBasedOn == "1")
                    SQL.AppendLine("        And E.DeptCode=@DeptCode ");
                if (mBudgetBasedOn == "2")
                    SQL.AppendLine("        And E.SiteCode=@DeptCode ");
                SQL.AppendLine("            And E.ReqType='1' ");
                SQL.AppendLine("            And Left(E.DocDt, 6)=Left(@DocDt, 6) ");
                SQL.AppendLine("            And E.DocNo<>@DocNo ");
                SQL.AppendLine("        Where A.CancelInd='N' ");
                SQL.AppendLine("    ),0.00) ");
                SQL.AppendLine(" As RemainingBudget");

                var cm = new MySqlCommand()
                { CommandText =SQL.ToString() };
                if (mBudgetBasedOn == "1")
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                if (mBudgetBasedOn == "2")
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueSiteCode));
                
                Sm.CmParam<String>(ref cm, "@DocNo", (TxtDocNo.Text.Length!=0)?TxtDocNo.Text:"XXX");
                Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<String>(ref cm, "@MainCurCode", Sm.GetParameter("MainCurCode"));
                
                AvailableBudget = Sm.GetValue(cm);
            }
            
            return decimal.Parse(AvailableBudget);
        }

        public void ComputeRemainingBudget()
        {
            decimal AvailableBudget = 0m, RequestedBudget = 0m;
            try
            {
                if (Sm.CompareStr(Sm.GetLue(LueReqType), "1"))
                {
                    AvailableBudget = ComputeAvailableBudget();    
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 24).Length != 0)
                            RequestedBudget += Sm.GetGrdDec(Grd1, Row, 24);
                
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            TxtRemainingBudget.Text = Sm.FormatNum(AvailableBudget-RequestedBudget, 0);
        }

        public void ComputeDR_Balance()
        {
            if (!mIsMaterialRequestForDroppingRequestEnabled) return;
            decimal 
                DR_DroppingRequestAmt = decimal.Parse(TxtDR_DroppingRequestAmt.Text),
                //DR_OtherMRAmt = decimal.Parse(TxtDR_OtherMRAmt.Text),
                DR_MRAmt = 0m,
                Qty = 0m, 
                UPrice = 0m;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Qty = 0m;
                UPrice = 0m;

                if (Sm.GetGrdStr(Grd1, r, 17).Length != 0) Qty = Sm.GetGrdDec(Grd1, r, 17);
                if (Sm.GetGrdStr(Grd1, r, 23).Length != 0) UPrice = Sm.GetGrdDec(Grd1, r, 23);

                DR_MRAmt += (Qty * UPrice);
            }
            TxtDR_MRAmt.Text = Sm.FormatNum(DR_MRAmt, 0);
            //TxtDR_Balance.Text = Sm.FormatNum(DR_DroppingRequestAmt - DR_OtherMRAmt - DR_MRAmt, 0);
            TxtDR_Balance.Text = Sm.FormatNum(DR_DroppingRequestAmt - DR_MRAmt, 0);
        }

        private void ParPrint(string DocNo)
        {
            if (mMenuCodeForPRService)
                mPrintOutFileTypeInd = "S";
            else
                mPrintOutFileTypeInd = "I";

            var l = new List<MRIMS>();
            var ld = new List<MRIMSDtl>();
            var ld2 = new List<MRIMSDtl2>();
            var ls = new List<MRIMSSign>();
            var ls2 = new List<MRIMSSign2>();

            string[] TableName = { "MRIMS", "MRIMSDtl", "MRIMSDtl2", "MRIMSSign", "MRIMSSign2" };
            List<IList> myLists = new List<IList>();

            #region Header
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT Distinct @CompanyLogo As CompanyLogo, A.DocNo, ");
            if (mPrintOutFileTypeInd == "I")
                SQL.AppendLine("DATE_FORMAT(A.DocDt, '%d-%m-%Y') DocDt, ");
            else
                SQL.AppendLine("DATE_FORMAT(A.DocDt, '%d %M %Y') DocDt, ");
            SQL.AppendLine("B.SiteName, '' ProjectCode,  ");
            SQL.AppendLine("'' ProjectName, GROUP_CONCAT(Distinct T9.FinishedGood SEPARATOR ' \n') DocName  ");
            if (mPrintOutFileTypeInd == "I")
                SQL.AppendLine("FROM TblMaterialRequestHdr A  ");
            else
                SQL.AppendLine("FROM TblMaterialRequestServiceHdr A  ");
            SQL.AppendLine("LEFT JOIN TblSite B ON A.SiteCode = B.SiteCode ");
            SQL.AppendLine("INNER JOIN tblmaterialrequestdtl T9 ON A.DocNo=T9.DocNo ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo; ");
            //SQL.AppendLine("And Exists(Select 1 From TblMaterialRequestDtl Where DocNo = @DocNo And ItCode In (Select ItCode From TblItem Where InventoryItemInd ='Y')); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DocDt",
                        "SiteName",
                        "ProjectCode",
                        "ProjectName",
                        "DocName",
                        //6
                        "CompanyLogo"
                    });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new MRIMS()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            SiteName = Sm.DrStr(dr, c[2]),
                            ProjectCode = TxtProjectCode.Text,
                            ProjectName = TxtProjectName.Text,
                            DocName = Sm.DrStr(dr, c[5]),
                            CompanyLogo = Sm.DrStr(dr, c[6]),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail Inventory
            var SQLD = new StringBuilder();
            var cmd = new MySqlCommand();

            SQLD.AppendLine("SELECT A.DocNo, A.DNo, B.ItCodeInternal, B.ItName, B.Specification, A.Qty, B.InventoryUomCode Uom, ");
            SQLD.AppendLine("C.ProjectCode, DATE_FORMAT(A.UsageDt, '%d-%m-%Y') UsageDt, A.Remark ");
            SQLD.AppendLine("FROM TblMaterialRequestDtl A ");
            SQLD.AppendLine("INNER JOIN TblItem B ON A.ItCode = B.ItCode ");
            SQLD.AppendLine("    AND A.DocNo = @DocNo ");
            SQLD.AppendLine("    And B.InventoryItemInd = 'Y' ");
            SQLD.AppendLine("LEFT JOIN ");
            SQLD.AppendLine("( ");
            SQLD.AppendLine("    SELECT T1.DocNo, T1.DNo, GROUP_CONCAT(DISTINCT T5.ProjectCode) ProjectCode ");
            SQLD.AppendLine("    FROM TblMaterialRequestDtl T1 ");
            SQLD.AppendLine("    INNER JOIN TblBOMRevisionHdr T2 ON T1.BOMRDocNo = T2.DocNo ");
            SQLD.AppendLine("        AND T1.DocNo = @DocNo ");
            SQLD.AppendLine("    INNER JOIN TblBOQHdr T3 ON T2.BOQDocNo = T3.DocNo ");
            SQLD.AppendLine("    INNER JOIN TblLOPHdr T4 ON T3.LOPDocNo = T4.DocNo ");
            SQLD.AppendLine("    LEFT JOIN TblProjectGroup T5 ON T4.PGCode = T5.PGCode ");
            SQLD.AppendLine("    GROUP BY T1.DocNo, T1.DNo ");
            SQLD.AppendLine(") C ON A.DocNo = C.DocNo AND A.DNo = C.DNo ");
            SQLD.AppendLine("Where A.CancelInd = 'N' ");
            SQLD.AppendLine("Order by A.ItCode; ");

            using (var cnd = new MySqlConnection(Gv.ConnectionString))
            {
                cnd.Open();
                cmd.Connection = cnd;
                cmd.CommandText = SQLD.ToString();
                Sm.CmParam<String>(ref cmd, "@DocNo", DocNo);

                var drd = cmd.ExecuteReader();
                var cd = Sm.GetOrdinal(drd, new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DNo",
                        "ItCodeInternal",
                        "ItName",
                        "Specification",
                        "Qty",
                        //6-9
                        "Uom",
                        "ProjectCode",
                        "UsageDt",
                        "Remark"
                    });

                if (drd.HasRows)
                {
                    int mNo = 1;
                    while (drd.Read())
                    {
                        ld.Add(new MRIMSDtl()
                        {
                            No = mNo,
                            DocNo = Sm.DrStr(drd, cd[0]),
                            DNo = Sm.DrStr(drd, cd[1]),
                            ItCodeInternal = Sm.DrStr(drd, cd[2]),
                            ItName = Sm.DrStr(drd, cd[3]),
                            Specification = Sm.DrStr(drd, cd[4]),
                            Qty = Sm.DrDec(drd, cd[5]),
                            Uom = Sm.DrStr(drd, cd[6]),
                            ProjectCode = Sm.DrStr(drd, cd[7]),
                            UsageDt = Sm.DrStr(drd, cd[8]),
                            Remark = Sm.DrStr(drd, cd[9])
                        });
                        mNo += 1;
                    }
                }
                drd.Close();
            }
            myLists.Add(ld);

            #endregion

            #region Detail Service

            var SQLD2 = new StringBuilder();
            var cmd2 = new MySqlCommand();

            SQLD2.AppendLine("SELECT A.DocNo, A.DNo, B.ItCodeInternal, B.ItName, IfNull(Concat(C.ItName, '\n', C.InventoryUomCode), '') Specification, A.Qty, B.InventoryUomCode Uom, ");
            SQLD2.AppendLine("Null As ProjectCode, DATE_FORMAT(A.UsageDt, '%d-%m-%Y') UsageDt, A.Remark ");
            SQLD2.AppendLine("FROM TblMaterialRequestServiceDtl A ");
            SQLD2.AppendLine("INNER JOIN TblItem B ON A.ItCode = B.ItCode ");
            SQLD2.AppendLine("    AND A.DocNo = @DocNo ");
            //SQLD2.AppendLine("    And B.ServiceItemInd = 'Y' ");
            SQLD2.AppendLine("Left Join TblItem C On A.ItCode2 = C.ItCode ");
            SQLD2.AppendLine("Where A.CancelInd = 'N' ");
            SQLD2.AppendLine("Order by A.ItCode; ");

            using (var cnd2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnd2.Open();
                cmd2.Connection = cnd2;
                cmd2.CommandText = SQLD2.ToString();
                Sm.CmParam<String>(ref cmd2, "@DocNo", DocNo);

                var drd2 = cmd2.ExecuteReader();
                var cd2 = Sm.GetOrdinal(drd2, new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DNo",
                        "ItCodeInternal",
                        "ItName",
                        "Specification",
                        "Qty",
                        //6-9
                        "Uom",
                        "ProjectCode",
                        "UsageDt",
                        "Remark"
                    });

                if (drd2.HasRows)
                {
                    int mNo = 1;
                    while (drd2.Read())
                    {
                        ld2.Add(new MRIMSDtl2()
                        {
                            No = mNo,
                            DocNo = Sm.DrStr(drd2, cd2[0]),
                            DNo = Sm.DrStr(drd2, cd2[1]),
                            ItCodeInternal = Sm.DrStr(drd2, cd2[2]),
                            ItName = Sm.DrStr(drd2, cd2[3]),
                            Specification = Sm.DrStr(drd2, cd2[4]),
                            Qty = Sm.DrDec(drd2, cd2[5]),
                            Uom = Sm.DrStr(drd2, cd2[6]),
                            ProjectCode = Sm.DrStr(drd2, cd2[7]),
                            UsageDt = Sm.DrStr(drd2, cd2[8]),
                            Remark = Sm.DrStr(drd2, cd2[9])
                        });
                        mNo += 1;
                    }
                }
                drd2.Close();
            }
            myLists.Add(ld2);

            #endregion

            #region Signature
            var SQLS = new StringBuilder();
            var cms = new MySqlCommand();

            SQLS.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
            SQLS.AppendLine("T1.UserCode, T1.UserName, T3.PosName, ");
            SQLS.AppendLine("T1.DNo, T1.Seq As Level, @Space As Space, ");
            SQLS.AppendLine("T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
            SQLS.AppendLine("From ( ");

            SQLS.AppendLine("    Select UserCode, UserName, DNo, Seq, Title, LastUpDt From ( ");
            SQLS.AppendLine("        Select Distinct B.UserCode, C.UserName, ");
            SQLS.AppendLine("        B.ApprovalDNo As DNo, 900+D.Level As Seq, ");
            SQLS.AppendLine("        'Approved By' As Title, ");
            SQLS.AppendLine("        Left(B.LastUpDt, 8) As LastUpDt ");
            SQLS.AppendLine("        From TblMaterialRequestDtl A ");
            SQLS.AppendLine("        Inner Join TblDocApproval B On A.DocNo=B.DocNo AND A.DocNo = @DocNo ");
            SQLS.AppendLine("        Inner Join TblUser C On B.UserCode=C.UserCode ");
            SQLS.AppendLine("        Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType='MaterialRequest' ");
            SQLS.AppendLine("        Where A.Status='A' ");
            SQLS.AppendLine("        And A.DocNo=@DocNo ");
            SQLS.AppendLine("        And B.UserCode Not In (Select CreateBy From TblMaterialRequestHdr Where DocNo = @DocNo) ");
            SQLS.AppendLine("        Order By D.Level Desc ");
            SQLS.AppendLine("    ) Tbl ");

            SQLS.AppendLine("    Union All ");
            SQLS.AppendLine("    Select Distinct ");
            SQLS.AppendLine("    A.CreateBy As UserCode, B.UserName, ");
            SQLS.AppendLine("    '1' As DNo, 0 As Seq, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
            SQLS.AppendLine("    From TblMaterialRequestHdr A ");
            SQLS.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode And A.DocNo=@DocNo ");
            SQLS.AppendLine(") T1 ");
            SQLS.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
            SQLS.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
            SQLS.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
            SQLS.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
            SQLS.AppendLine("Order By T1.Seq; ");

            using (var cns = new MySqlConnection(Gv.ConnectionString))
            {
                cns.Open();
                cms.Connection = cns;
                cms.CommandText = SQLS.ToString();
                Sm.CmParam<String>(ref cms, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cms, "@DocNo", DocNo);

                var drs = cms.ExecuteReader();
                var cs = Sm.GetOrdinal(drs, new string[]
                {
                    //0
                     "Signature",
                     //1-5
                     "Username",
                     "PosName",
                     "Space",
                     "Level",
                     "Title",
                     "LastupDt"
                });

                if (drs.HasRows)
                {
                    while (drs.Read())
                    {
                        ls.Add(new MRIMSSign()
                        {
                            Signature = Sm.DrStr(drs, cs[0]),
                            UserName = Sm.DrStr(drs, cs[1]),
                            PosName = Sm.DrStr(drs, cs[2]),
                            Space = Sm.DrStr(drs, cs[3]),
                            DNo = Sm.DrStr(drs, cs[4]),
                            Title = Sm.DrStr(drs, cs[5]),
                            LastUpDt = Sm.DrStr(drs, cs[6])
                        });
                    }
                }
                drs.Close();
            }
            myLists.Add(ls);
            #endregion

            #region Signature 2
            var SQLS2 = new StringBuilder();
            var cms2 = new MySqlCommand();

            SQLS2.AppendLine("SELECT * ");
            SQLS2.AppendLine("FROM  ");
            SQLS2.AppendLine("( ");
	        SQLS2.AppendLine("    SELECT UPPER(B.UserName) CreateUserName, DATE_FORMAT(LEFT(A.CreateDt, 8), '%d-%m-%Y') CreateDocDt ");
            if (mPrintOutFileTypeInd == "I")
                SQLS2.AppendLine("    FROM TblMaterialRequestHdr A ");
            else
                SQLS2.AppendLine("    FROM TblMaterialRequestServiceHdr A ");
            SQLS2.AppendLine("    INNER JOIN TblUser B ON A.CreateBy = B.UserCode ");
	        SQLS2.AppendLine("        AND A.DocNo = @DocNo ");
            SQLS2.AppendLine(") A ");
            SQLS2.AppendLine("LEFT JOIN ");
            SQLS2.AppendLine("( ");
	        SQLS2.AppendLine("    SELECT Distinct UPPER(T3.UserName) ApproveUserName, IFNULL(DATE_FORMAT(LEFT(T1.LastUpDt, 8), '%d-%m-%Y'), '') ApproveDocDt ");
            SQLS2.AppendLine("     , Concat(IFNULL(T4.ParValue, ''), T3.UserCode, '.JPG') As SignApprove1 ");
            SQLS2.AppendLine("    FROM TblDocApproval T1 ");
	        SQLS2.AppendLine("    INNER JOIN TblDocApprovalSetting T2 ON T1.DocType = T2.DocType ");
	        SQLS2.AppendLine("        AND T1.DocNo = @DocNo ");
	        SQLS2.AppendLine("        AND T1.ApprovalDNo = T2.DNo ");
	        SQLS2.AppendLine("        AND T2.Level IN  ");
	        SQLS2.AppendLine("        ( ");
	    	SQLS2.AppendLine("	            SELECT MAX(B.Level) MaxLevel ");
			SQLS2.AppendLine("	            FROM TblDocApproval A ");
			SQLS2.AppendLine("	            INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
			SQLS2.AppendLine("	                AND A.ApprovalDNo = B.DNo ");
			SQLS2.AppendLine("	                AND A.DocNo = @DocNo ");
	        SQLS2.AppendLine("        ) ");
	        SQLS2.AppendLine("    INNER JOIN TblUser T3 ON T1.UserCode = T3.UserCode AND T1.Status = 'A' ");
	        SQLS2.AppendLine("        AND T3.UserCode NOT IN (SELECT CreateBy FROM TblMaterialRequestHdr WHERE DocNo = @DocNo) ");
            SQLS2.AppendLine("      INNER JOIN tblparameter T4 ON T4.ParCode = 'ImgFileSignature'");
            SQLS2.AppendLine(") B ON 0 = 0 ");
            SQLS2.AppendLine("LEFT JOIN  ");
            SQLS2.AppendLine("( ");
	        SQLS2.AppendLine("    SELECT Distinct UPPER(T3.UserName) Approve2UserName, IFNULL(DATE_FORMAT(LEFT(T1.LastUpDt, 8), '%d-%m-%Y'), '') Approve2DocDt ");
            SQLS2.AppendLine("     , Concat(IFNULL(T4.ParValue, ''), T3.UserCode, '.JPG') As SignApprove2 ");
            SQLS2.AppendLine("    FROM TblDocApproval T1 ");
	        SQLS2.AppendLine("    INNER JOIN TblDocApprovalSetting T2 ON T1.DocType = T2.DocType ");
	        SQLS2.AppendLine("        AND T1.DocNo = @DocNo ");
	        SQLS2.AppendLine("        AND T1.ApprovalDNo = T2.DNo ");
	        SQLS2.AppendLine("        AND T2.Level != 1 ");
	        SQLS2.AppendLine("        AND T2.Level IN  ");
	        SQLS2.AppendLine("        ( ");
	    	SQLS2.AppendLine("	            SELECT MAX(B.Level) - 1 MaxLevel ");
			SQLS2.AppendLine("	            FROM TblDocApproval A ");
			SQLS2.AppendLine("	            INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
			SQLS2.AppendLine("	                AND A.ApprovalDNo = B.DNo ");
			SQLS2.AppendLine("	                AND A.DocNo = @DocNo ");
	        SQLS2.AppendLine("        ) ");
	        SQLS2.AppendLine("    INNER JOIN TblUser T3 ON T1.UserCode = T3.UserCode AND T1.Status = 'A' ");
	        SQLS2.AppendLine("        AND T3.UserCode NOT IN (SELECT CreateBy FROM TblMaterialRequestHdr WHERE DocNo = @DocNo) ");
            SQLS2.AppendLine("      INNER JOIN tblparameter T4 ON T4.ParCode = 'ImgFileSignature'");
            SQLS2.AppendLine(") C ON 0 = 0 ");
            SQLS2.AppendLine("LEFT JOIN  ");
            SQLS2.AppendLine("( ");
            SQLS2.AppendLine("    SELECT Distinct UPPER(T3.UserName) Approve3UserName, IFNULL(DATE_FORMAT(LEFT(T1.LastUpDt, 8), '%d-%m-%Y'), '') Approve3DocDt ");
            SQLS2.AppendLine("     , Concat(IFNULL(T4.ParValue, ''), T3.UserCode, '.JPG') As SignApprove3 ");
            SQLS2.AppendLine("    FROM TblDocApproval T1 ");
            SQLS2.AppendLine("    INNER JOIN TblDocApprovalSetting T2 ON T1.DocType = T2.DocType ");
            SQLS2.AppendLine("        AND T1.DocNo = @DocNo ");
            SQLS2.AppendLine("        AND T1.ApprovalDNo = T2.DNo ");
            SQLS2.AppendLine("        AND T2.Level IN  ");
            SQLS2.AppendLine("        ( ");
	        SQLS2.AppendLine("                SELECT MAX(B.Level) - 2 MaxLevel ");
	        SQLS2.AppendLine("                FROM TblDocApproval A ");
	        SQLS2.AppendLine("                INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
	        SQLS2.AppendLine("                    AND A.ApprovalDNo = B.DNo ");
	        SQLS2.AppendLine("                    AND A.DocNo = @DocNo ");
            SQLS2.AppendLine("        ) ");
            SQLS2.AppendLine("    INNER JOIN TblUser T3 ON T1.UserCode = T3.UserCode AND T1.Status = 'A' ");
            SQLS2.AppendLine("        AND T3.UserCode NOT IN (SELECT CreateBy FROM TblMaterialRequestHdr WHERE DocNo = @DocNo) ");
            SQLS2.AppendLine("      INNER JOIN tblparameter T4 ON T4.ParCode = 'ImgFileSignature'");
            SQLS2.AppendLine(") D ON 0 = 0 ");

            using (var cns2 = new MySqlConnection(Gv.ConnectionString))
            {
                cns2.Open();
                cms2.Connection = cns2;
                cms2.CommandText = SQLS2.ToString();
                Sm.CmParam<String>(ref cms2, "@DocNo", DocNo);

                var drs2 = cms2.ExecuteReader();
                var cs2 = Sm.GetOrdinal(drs2, new string[]
                {
                    //0
                     "CreateUserName",
                     //1-5
                     "CreateDocDt",
                     "ApproveUserName",
                     "ApproveDocDt",
                     "SignApprove1",
                     "Approve2UserName",

                     //6-10
                     "Approve2DocDt",
                     "SignApprove2",
                     "Approve3UserName",
                     "Approve3DocDt",
                     "SignApprove3",
                });

                if (drs2.HasRows)
                {
                    while (drs2.Read())
                    {
                        ls2.Add(new MRIMSSign2()
                        {
                            CreateUserName = Sm.DrStr(drs2, cs2[0]),
                            CreateDocDt = Sm.DrStr(drs2, cs2[1]),
                            ApproveUserName = Sm.DrStr(drs2, cs2[2]),
                            ApproveDocDt = Sm.DrStr(drs2, cs2[3]),
                            SignApprove1 = Sm.DrStr(drs2, cs2[4]),
                            Approve2UserName = Sm.DrStr(drs2, cs2[5]),
                            Approve2DocDt = Sm.DrStr(drs2, cs2[6]),
                            SignApprove2 = Sm.DrStr(drs2, cs2[7]),
                            Approve3UserName = Sm.DrStr(drs2, cs2[8]),
                            Approve3DocDt = Sm.DrStr(drs2, cs2[9]),
                            SignApprove3 = Sm.DrStr(drs2, cs2[10]),
                        });
                    }
                }
                drs2.Close();
            }

            myLists.Add(ls2);

            #endregion

            if (l.Count == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "No data for this print out.");
            }
            else
            {
                if(mPrintOutFileTypeInd == "I")
                    Sm.PrintReport("MaterialRequest3Inventory", myLists, TableName, false);
                else
                    Sm.PrintReport("MaterialRequest3Service", myLists, TableName, false);
            }

            l.Clear();
            mPrintOutFileTypeInd = string.Empty;
        }

        internal void ShowPOQtyCancelInfo(string DocNo)
        {
            TxtRemainingBudget.EditValue = Sm.FormatNum(0m, 0);
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, D.DocDt, D.ReqType, D.DeptCode, Concat('Replacement for ', D.DocNo) As RemarkH, ");
            SQL.AppendLine("E.ItCode, F.ItName, A.Qty, F.PurchaseUomCode, E.UsageDt, E.Remark As RemarkD, ");
            SQL.AppendLine("F.ItSCCode, H.ItSCName ");
            SQL.AppendLine("From TblPOQtyCancel A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr D On C.MaterialRequestDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On C.MaterialRequestDocNo=E.DocNo And C.MaterialRequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblItem F On E.ItCode=F.ItCode ");
            SQL.AppendLine("Inner Join TblDepartment G On D.DeptCode=G.DeptCode ");
            SQL.AppendLine("Left Join TblItemSubCategory H On F.ItSCCode=H.ItSCCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "ReqType", "DeptCode", "RemarkH", "ItCode", 
                        //6-10
                        "ItName", "Qty", "PurchaseUomCode", "UsageDt", "RemarkD",
                        //11-12
                        "ItSCCode", "ItSCName"
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        TxtPOQtyCancelDocNo.EditValue = Sm.DrStr(dr, 0);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueReqType, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 8, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 11, 6);
                        Sm.SetGrdValue("N", Grd1, dr, c, 0, 17, 7);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 18, 8);
                        Sm.SetGrdValue("D", Grd1, dr, c, 0, 19, 9);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 25, 10);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 13, 11);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 14, 12);
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            Grd1.Rows.Add();
            Sm.SetGrdBoolValueFalse(ref Grd1, 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 1, new int[] { 17, 23, 24, 31, 32 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private string ItemSelection()
        {
            var SQL = new StringBuilder();

            if (Sm.CompareStr(Sm.GetLue(LueReqType), "2"))
            {
                SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, Null As DocNo, Null As DNo, Null As DocDt, 0 As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("A.ItName, A.ForeignName, ");
                SQL.AppendLine("ifnull(C.Qty01, 0) As Mth01, ifnull(C.Qty03, 0) As Mth03, ifnull(C.Qty06, 0) As Mth06, ifnull(C.Qty09, 0) As Mth09, ifnull(C.Qty12, 0) As Mth12, ");
                SQL.AppendLine("A.ItScCode, D.ItScName, A.ItCodeInternal, A.ItGrpCode, E.ItGrpName ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" ) C On C.ItCode = A.ItCode ");
                SQL.AppendLine("Left Join TblItemSubCategory D On A.ItScCode = D.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup E On A.ItGrpCode=E.ItGrpCode ");
            }
            else
            {
                SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, C.DocNo, C.DNo, C.DocDt, ");
                SQL.AppendLine("A.ItName, A.ForeignName, ");
                SQL.AppendLine("C.UPrice*");
                SQL.AppendLine("    Case When IfNull(C.CurCode, '')=D.ParValue Then 1 ");
                SQL.AppendLine("    Else IfNull(E.Amt, 0) ");
                SQL.AppendLine("    End As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("ifnull(F.Qty01, 0) As Mth01, ifnull(F.Qty03, 0) As Mth03, ifnull(F.Qty06, 0) As Mth06, ifnull(F.Qty09, 0) As Mth09, ifnull(F.Qty12, 0) As Mth12, ");
                SQL.AppendLine("A.ItScCode, G.ItScName, A.ItCodeInternal, A.ItGrpCode, H.ItGrpName ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, T2.DNo, T1.DocDt, T2.ItCode, T1.CurCode, T2.UPrice ");
                SQL.AppendLine("    From TblQtHdr T1 ");
                SQL.AppendLine("    Inner Join TblQtDtl T2 On T1.DocNo=T2.DocNo And T2.ActInd='Y' ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select T3b.ItCode, Max(T3a.SystemNo) As Key1 ");
                SQL.AppendLine("        From TblQtHdr T3a, TblQtDtl T3b ");
                SQL.AppendLine("        Where T3a.DocNo=T3b.DocNo And T3b.ActInd='Y' ");
                SQL.AppendLine("        Group By T3b.ItCode ");
                SQL.AppendLine("    ) T3 On T1.SystemNo=T3.Key1 And T2.ItCode=T3.ItCode ");
                SQL.AppendLine(") C On A.ItCode=C.ItCode ");
                SQL.AppendLine("Left Join TblParameter D On D.ParCode='MainCurCode' ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.CurCode CurCode1, T2.CurCode As CurCode2, IfNull(T3.Amt, 0) As Amt ");
                SQL.AppendLine("    From TblCurrency T1 ");
                SQL.AppendLine("    Inner Join TblCurrency T2 On T1.CurCode<>T2.CurCode ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select T3a.RateDt, T3a.CurCode1, T3a.CurCode2, T3a.Amt ");
                SQL.AppendLine("        From TblCurrencyRate T3a  ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select CurCode1, CurCode2, Max(RateDt) As RateDt  ");
                SQL.AppendLine("            From TblCurrencyRate Group By CurCode1, CurCode2 ");
                SQL.AppendLine("        ) T3b On T3a.CurCode1=T3b.CurCode1 And T3a.CurCode2=T3b.CurCode2 And T3a.RateDt=T3b.RateDt ");
                SQL.AppendLine("    ) T3 On T1.CurCode=T3.CurCode1 And T2.CurCode=T3.CurCode2 ");
                SQL.AppendLine(") E On  E.CurCode1=C.CurCode And E.CurCode2=D.ParValue ");
                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" ) F On F.ItCode = A.ItCode ");
                SQL.AppendLine("Left Join TblItemSubCategory G On A.ItScCode=G.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup H On A.ItGrpCode=H.ItGrpCode ");

            }

            SQL.AppendLine("Where A.ActInd = 'Y' ");

            if (Sm.IsDataExist("Select ParCode From TblParameter Where ParCode='ItemManagedByWhsInd' And ParValue='Y' Limit 1 "))
                SQL.AppendLine("And (IfNull(A.ControlByDeptCode, '')='' Or (IfNull(A.ControlByDeptCode, '')<>'' And A.ControlByDeptCode=@DeptCode)) ");

            return SQL.ToString();
        }

        internal void ShowDORequestItem(string DocNo)
        {
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo As DORequestDocNo, B.DNo As DORequestDNo, B.ItCode, ");
            SQL.AppendLine("X.ItCtName, X.PurchaseUomCode, X.DocNo, X.DNo, X.DocDt, X.UPrice, X.MinStock, X.ReorderStock, ");
            SQL.AppendLine("X.ItName, X.ForeignName, ");
            SQL.AppendLine("X.Mth01, X.Mth03, X.Mth06, X.Mth09, X.Mth12, ");
            SQL.AppendLine("X.ItScCode, X.ItScName, X.ItCodeInternal, X.ItGrpCode, X.ItGrpName, ");
            SQL.AppendLine("(IfNull(B.Qty, 0.00)-IfNull(D.Qty, 0.00)) As MRQty ");
            SQL.AppendLine("From TblDORequestDeptHdr A ");
            SQL.AppendLine("Inner Join TblDORequestDeptDtl B On A.DocNo = B.DocNo And B.ProcessInd = 'O' ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine(ItemSelection());

            SQL.AppendLine(")X On B.ItCode = X.ItCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  Select ItCode, Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
            SQL.AppendLine("  From TblStockSummary ");
            SQL.AppendLine("  Where WhsCode = @WhsCode ");
            SQL.AppendLine("  And Qty>0 ");
            SQL.AppendLine("  Group By ItCode ");
            SQL.AppendLine(") D On B.ItCode = D.ItCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And IfNull(B.Qty, 0.00)>IfNull(D.Qty, 0.00) ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetValue("Select WhsCode From TblDORequestDeptHdr Where DocNo = '" + DocNo + "'; "));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@DocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",

                    //1-5
                    "ItCodeInternal", "ItGrpName", "ItName", "ForeignName",  "ItCtName",   
                    
                    //6-7
                    "ItScCode", "ItScName", "PurchaseUomCode", "DocNo", "DNo", 

                    //11-15
                    "DocDt", "UPrice", "MinStock", "ReorderStock", "Mth01",   

                    //16-20
                    "Mth03", "Mth06", "Mth09", "Mth12", "DORequestDocNo",
                    
                    //21-22
                    "DORequestDNo", "MRQty"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 10);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 22);
                    Grd1.Cells[Row, 29].Value = 0;

                    if (mIsBudgetActive)
                        Grd1.Cells[Row, 11].ForeColor = Sm.GetGrdStr(Grd1, Row, 20).Length > 0 ? Color.Black : Color.Red;
                    ComputeTotal(Row);
                }, false, false, true, false
            );
            
            Sm.FocusGrd(Grd1, 0, 1);

            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 15, 16, 17, 23, 24, 29, 31, 32 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void SetLueDeptCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");
            SQL.AppendLine("Where 1=1 ");
            SQL.AppendLine("And ActInd = 'Y' ");
            if (mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsDeptFilterBySite)
            {
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select DeptCode From TblDepartmentBudgetSite ");
                SQL.AppendLine("    Where SiteCode=@SiteCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue, string SiteCode)
        {
            try
            {
                var SQL = new StringBuilder();
                
                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                if (SiteCode.Length == 0)
                    SQL.AppendLine("Where T.ActInd='Y' ");
                else
                    SQL.AppendLine("Where T.SiteCode=@SiteCode ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By SiteName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueReqType(ref DXE.LookUpEdit Lue, string ReqType)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
                SQL.AppendLine("From TblOption Where OptCat='ReqType' ");
                if (ReqType.Length > 0)
                    SQL.AppendLine("And OptCode=@ReqType ");
                else
                {
                    if (!mIsMaterialRequestForDroppingRequestEnabled)
                    {
                        if (mIsBudgetActive && mReqTypeForNonBudget.Length > 0)
                            SQL.AppendLine("And OptCode<>@ReqTypeForNonBudget ");
                    }
                }
                SQL.AppendLine("Order By OptDesc;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                Sm.CmParam<String>(ref cm, "@ReqType", ReqType);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Type", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetBudgetCategory()
        {
            LueBCCode.EditValue = null;
            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, true);

            var ReqType = Sm.GetLue(LueReqType);
            var DeptCode = Sm.GetLue(LueDeptCode);

            if (
                mReqTypeForNonBudget.Length == 0 ||
                ReqType.Length == 0 ||
                Sm.CompareStr(ReqType, mReqTypeForNonBudget) ||
                DeptCode.Length == 0
                ) return;

            Sl.SetLueBCCode(ref LueBCCode, string.Empty, DeptCode);
            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, false);
        }

        private bool ShowPrintApproval()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select Status From TblMaterialRequestDtl " +
                    "Where DocNo=@DocNo And Status ='O';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Can't print. This document has not been approved.");
                return true;
            }
            return false;
        }

        private void ShowMRApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
            SQL.AppendLine("Concat(Substring(LastUpDt, 7, 2), '/', Substring(LastUpDt, 5, 2), '/', Left(LastUpDt, 4))  ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("Remark  ");
            SQL.AppendLine("From TblDocApproval ");
            SQL.AppendLine("Where DocType='MaterialRequest' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo "); //
            SQL.AppendLine("Order By ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, new string[] { "UserCode", "StatusDesc", "LastUpDt", "Remark" });
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            "Remark : " + Sm.DrStr(dr, 3) + Environment.NewLine + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted()
             ;
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsMRAllowToUploadFile && TxtFile.Text.Length>0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                if (!mMenuCodeForPRService)
                    SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                else
                    SQL.AppendLine("Select DocNo From TblMaterialRequestServiceHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();
                
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress,":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true; 

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false; 

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        internal void SetDroppingReqProjImplItQty()
        {
            if (TxtDR_PRJIDocNo.Text.Length == 0) return;

            var SQL = new StringBuilder();
            string ItCode = string.Empty;
            decimal Qty = 0m, Amt = 0m;

            SQL.AppendLine("Select T2.ResourceItCode, T1.Qty, IfNull(T3.Amt, 0.00) As Amt ");
            SQL.AppendLine("From TblDroppingRequestDtl T1 ");
            SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr T2 On T1.PRBPDocNo=T2.DocNo ");
            SQL.AppendLine("Left Join TblProjectImplementationDtl2 T3 On T2.PRJIDocNo=T3.DocNo And T2.ResourceItCode=T3.ResourceItCode ");
            SQL.AppendLine("Where T1.DocNo=@DroppingRequestDocNo And T1.MRDocNo Is Null ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "ResourceItCode", "Qty", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        Qty = Sm.DrDec(dr, 1);
                        Amt = Sm.DrDec(dr, 2);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), ItCode) && Sm.GetGrdDec(Grd1, r, 31)==0m)
                            {
                                Grd1.Cells[r, 17].Value = Qty;
                                Grd1.Cells[r, 31].Value = Qty;
                                Grd1.Cells[r, 32].Value = Amt;
                                Grd1.Cells[r, 24].Value = Sm.GetGrdDec(Grd1, r, 17) * Sm.GetGrdDec(Grd1, r, 23);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            ComputeDR_Balance();
        }

        internal void SetDroppingReqBCItQty()
        {
            if (mDroppingRequestBCCode.Length == 0) return;

            var SQL = new StringBuilder();
            string ItCode = string.Empty;
            decimal Qty = 0m, Amt = 0m;

            SQL.AppendLine("Select ItCode, Qty, Amt ");
            SQL.AppendLine("From TblDroppingRequestDtl2 ");
            SQL.AppendLine("Where DocNo=@DroppingRequestDocNo ");
            SQL.AppendLine("And BCCode=@DroppingRequestBCCode ");
            SQL.AppendLine("And MRDocNo Is Null ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mDroppingRequestBCCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "Qty", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        Qty = Sm.DrDec(dr, 1);
                        Amt = Sm.DrDec(dr, 2);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), ItCode) &&
                                Sm.GetGrdDec(Grd1, r, 31) == 0m
                                )
                            {
                                Grd1.Cells[r, 17].Value = Qty;
                                Grd1.Cells[r, 31].Value = Qty;
                                Grd1.Cells[r, 32].Value = Amt;
                                Grd1.Cells[r, 24].Value = Sm.GetGrdDec(Grd1, r, 17) * Sm.GetGrdDec(Grd1, r, 23);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            ComputeDR_Balance();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueReqType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueReqType, new Sm.RefreshLue2(SetLueReqType), string.Empty);
                if (mIsMaterialRequestForDroppingRequestEnabled)
                {
                    mDroppingRequestBCCode = string.Empty;
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        TxtDroppingRequestDocNo, TxtDR_DeptCode, TxtDR_Yr, TxtDR_Mth, TxtDR_PRJIDocNo, 
                        TxtDR_BCCode, MeeDR_Remark
                    });
                    Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                    { 
                        TxtDR_DroppingRequestAmt, TxtDR_MRAmt, TxtDR_Balance 
                    }, 0);
                }
                else
                    SetBudgetCategory();
                ClearGrd();
                ComputeRemainingBudget();
                ComputeDR_Balance();
            }
        }

        private void DteUsageDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteUsageDt, ref fCell, ref fAccept);
        }

        private void DteUsageDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteDocDt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsMaterialRequestForDroppingRequestEnabled)
                {
                    mDroppingRequestBCCode = string.Empty;
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        TxtDroppingRequestDocNo, TxtDR_DeptCode, TxtDR_Yr, TxtDR_Mth, TxtDR_PRJIDocNo, 
                        TxtDR_BCCode, MeeDR_Remark
                    });
                    Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                    { 
                        TxtDR_DroppingRequestAmt, TxtDR_MRAmt, TxtDR_Balance 
                    }, 0);
                    ClearGrd();
                }
                ComputeRemainingBudget();
                ComputeDR_Balance();
            }
        }

        private void LueSiteCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite ? "Y" : "N");
                if (mIsDeptFilterBySite)
                {
                    if (Sm.GetLue(LueSiteCode).Length > 0)
                    {
                        Sm.SetControlReadOnly(LueDeptCode, false);
                        LueDeptCode.EditValue = "<Refresh>";
                        Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
                    }
                    else
                    {
                        LueDeptCode.EditValue = null;
                        Sm.SetControlReadOnly(LueDeptCode, true);
                    }
                }
            }
        }

        private void LueDeptCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsDeptFilterBySite)
                    Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
                else
                    Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
                if (!mIsMaterialRequestForDroppingRequestEnabled) SetBudgetCategory();
                mDroppingRequestBCCode = string.Empty;
                if (mIsMaterialRequestForDroppingRequestEnabled)
                {
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        TxtDroppingRequestDocNo, TxtDR_DeptCode, TxtDR_Yr, TxtDR_Mth, TxtDR_PRJIDocNo, 
                        TxtDR_BCCode, MeeDR_Remark
                    });
                    Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                    { 
                        TxtDR_DroppingRequestAmt, TxtDR_MRAmt, TxtDR_Balance 
                    }, 0);
                }
                ClearGrd();
                ComputeRemainingBudget();
                ComputeDR_Balance();
            }
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue3(Sl.SetLueBCCode), string.Empty, Sm.GetLue(LueDeptCode));
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        public static void SetLueCurCode(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue, "Select CurCode As Col1 From tblCurrency ", "Currency");
        }

        public static void SetLuePICCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(ref Lue, "Select UserCode As Col1, UserName As Col2 From TblUser Order By UserName", 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
   
        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode_Leave(object sender, EventArgs e)
        {
            if (LueCurCode.Visible && fAccept && fCell.ColIndex == 28)
            {
                if (Sm.GetLue(LueCurCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 28].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 28].Value = Sm.GetLue(LueCurCode);
                }
                LueCurCode.Visible = false;
            }
        }

        private void LueCurCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }
        private void LuePICCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LuePICCode, new Sm.RefreshLue1(SetLuePICCode));
            
        }

        #endregion

        #region Button  Event

        private void BtnImportCSV_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mMenuCodeForPRService && BtnImportCSV.Enabled)
            {
                ProcessCSV();
            }
        }

        private void BtnPOQtyCancelDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMaterialRequest3Dlg2(this));
        }

        private void BtnPOQtyCancelDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPOQtyCancelDocNo, "Cancellation PO#", false))
            {
                var f = new FrmPOQtyCancel(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPOQtyCancelDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnDORequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueReqType, "Request Type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmMaterialRequest3Dlg3(this, Sm.GetLue(LueDeptCode)));
        }

        private void BtnDORequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDORequestDocNo, "DO Request#", false))
            {
                var f = new FrmDORequestDept(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDORequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnDroppingRequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDroppingRequestDocNo, "Dropping request#", false))
            {
                var f = new FrmDroppingRequest(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDroppingRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnDroppingRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmMaterialRequest3Dlg4(this, Sm.GetDte(DteDocDt), Sm.GetLue(LueDeptCode)));
        }

        private void BtnSOCDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmMaterialRequest3Dlg5(this));
            }
        }

        private void BtnSOCDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSOCDocNo, "SO Contract#", false))
            {
                var f = new FrmSOContract2(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtSOCDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnNTPDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmMaterialRequest3Dlg6(this));
            }
        }

        private void BtnNTPDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtNTPDocNo, "Notice To Proceed#", false))
            {
                var f = new FrmNoticeToProceed(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtNTPDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnPGCode_Click(object sender, EventArgs e)
        {
            //if (TxtSOCDocNo.Text.Length > 0 || TxtNTPDocNo.Text.Length > 0)
            //{
            //    Sm.StdMsg(mMsgType.Info, "You can't input project group.");
            //    return;
            //}

            var f = new FrmProjectGroupStdDlg();
            f.TopLevel = true;
            f.ShowDialog();
            if (f.mPGCode.Length > 0)
            {
                mPGCode = f.mPGCode;
                TxtProjectCode.EditValue = f.mProjectCode;
                TxtProjectName.EditValue = f.mProjectName;
                TxtSOCDocNo.EditValue = f.mSOCDocNo;
                TxtPONo.EditValue = f.mPONo;
            }
            else
            {
                mPGCode = string.Empty;
                TxtProjectCode.EditValue = null;
                TxtProjectName.EditValue = null;
                TxtSOCDocNo.EditValue = null;
                TxtPONo.EditValue = null;
            }
            f.Close();
            ClearGrd();
        }

        #endregion

        #endregion

        #region Report Class

        private class MRItemService
        {
            public string ItCode { get; set; }
            public string ServiceItemInd { get; set; }
        }

        private class CSVItemDetail
        {
            public string PRItCode { get; set; }
            public string PRItCodeInternal { get; set; }
            public string PRItName { get; set; }
            public string PRSpecification { get; set; }
            public string ItScCode { get; set; }
            public string ItScName { get; set; }
            public decimal MinStock { get; set; }
            public decimal ReorderPoint { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }
            public string QtDocNo { get; set; }
            public string QtDNo { get; set; }
            public string QtDocDt { get; set; }
            public decimal QtUPrice { get; set; }
            public decimal Total { get; set; }
            public decimal Stock { get; set; }
            public string BOM2DocNo { get; set; }
            public string FinishedGood { get; set; }
            public string Component { get; set; }
            public string UsageDt { get; set; }
            public string Remark { get; set; }

            public string ItCode { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItName { get; set; }
            public string Specification { get; set; }
            
        }

        private class MRIMS
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string SiteName { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string DocName { get; set; }
            public string CompanyLogo { get; set; }
        }

        private class MRIMSDtl
        {
            public int No { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItName { get; set; }
            public string Specification { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }
            public string ProjectCode { get; set; }
            public string UsageDt { get; set; }
            public string Remark { get; set; }
        }

        private class MRIMSDtl2
        {
            public int No { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItName { get; set; }
            public string Specification { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }
            public string ProjectCode { get; set; }
            public string UsageDt { get; set; }
            public string Remark { get; set; }
        }

        private class MRIMSSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class MRIMSSign2
        {
            public string CreateUserName { get; set; }
            public string CreateDocDt { get; set; }
            public string ApproveUserName { get; set; }
            public string ApproveDocDt { get; set; }
            public string SignApprove1 { get; set; }
            public string Approve2UserName { get; set; }
            public string Approve2DocDt { get; set; }
            public string SignApprove2 { get; set; }
            public string Approve3UserName { get; set; }
            public string Approve3DocDt { get; set; }
            public string SignApprove3 { get; set; }
        }

        #endregion
    }
}
