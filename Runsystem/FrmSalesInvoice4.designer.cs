﻿namespace RunSystem
{
    partial class FrmSalesInvoice4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.LueSPCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueBankAcCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtTotalAmt = new DevExpress.XtraEditors.TextEdit();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtDownpayment = new DevExpress.XtraEditors.TextEdit();
            this.LblRemark = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtTotalTax = new DevExpress.XtraEditors.TextEdit();
            this.DteDueDt = new DevExpress.XtraEditors.DateEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.LueOption = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.TcSalesInvoice4 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.DteTaxInvDt2 = new DevExpress.XtraEditors.DateEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.TxtTaxInvDocument2 = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.DteTaxInvDt1 = new DevExpress.XtraEditors.DateEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.TxtTaxAmt2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTaxAmt1 = new DevExpress.XtraEditors.TextEdit();
            this.LueTaxCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueTaxCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtTaxInvDocument1 = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.BtnJournalDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnJournalDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtJournalDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtJournalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.Tp4 = new DevExpress.XtraTab.XtraTabPage();
            this.TxtReceiptNo = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDownpayment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueOption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcSalesInvoice4)).BeginInit();
            this.TcSalesInvoice4.SuspendLayout();
            this.Tp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvDocument2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvDocument1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.Tp3.SuspendLayout();
            this.Tp4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReceiptNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Location = new System.Drawing.Point(808, 0);
            this.panel1.Size = new System.Drawing.Size(70, 473);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcSalesInvoice4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(808, 473);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TxtReceiptNo);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.LueCtCode);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(808, 96);
            this.panel3.TabIndex = 8;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.MeeCancelReason);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.ChkCancelInd);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(461, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(347, 96);
            this.panel4.TabIndex = 16;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(7, 26);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(267, 20);
            this.MeeCancelReason.TabIndex = 21;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(4, 8);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 14);
            this.label4.TabIndex = 20;
            this.label4.Text = "Reason For Cancellation";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(9, 48);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(65, 22);
            this.ChkCancelInd.TabIndex = 11;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(20, 52);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 14);
            this.label8.TabIndex = 14;
            this.label8.Text = "Customer";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(82, 49);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 450;
            this.LueCtCode.Size = new System.Drawing.Size(342, 20);
            this.LueCtCode.TabIndex = 15;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(82, 28);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(123, 20);
            this.DteDocDt.TabIndex = 13;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(46, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(82, 7);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(186, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(109, 6);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(186, 20);
            this.TxtLocalDocNo.TabIndex = 21;
            this.TxtLocalDocNo.Validated += new System.EventHandler(this.TxtLocalDocNo_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(9, 9);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 14);
            this.label10.TabIndex = 20;
            this.label10.Text = "Local Document";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(29, 53);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 14);
            this.label16.TabIndex = 25;
            this.label16.Text = "Sales Person";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSPCode
            // 
            this.LueSPCode.EnterMoveNextControl = true;
            this.LueSPCode.Location = new System.Drawing.Point(109, 50);
            this.LueSPCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSPCode.Name = "LueSPCode";
            this.LueSPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.Appearance.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSPCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSPCode.Properties.DropDownRows = 30;
            this.LueSPCode.Properties.NullText = "[Empty]";
            this.LueSPCode.Properties.PopupWidth = 250;
            this.LueSPCode.Size = new System.Drawing.Size(409, 20);
            this.LueSPCode.TabIndex = 26;
            this.LueSPCode.ToolTip = "F4 : Show/hide list";
            this.LueSPCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSPCode.EditValueChanged += new System.EventHandler(this.LueSPCode_EditValueChanged);
            // 
            // LueBankAcCode
            // 
            this.LueBankAcCode.EnterMoveNextControl = true;
            this.LueBankAcCode.Location = new System.Drawing.Point(110, 266);
            this.LueBankAcCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode.Name = "LueBankAcCode";
            this.LueBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.LueBankAcCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode.Properties.DropDownRows = 30;
            this.LueBankAcCode.Properties.NullText = "[Empty]";
            this.LueBankAcCode.Properties.PopupWidth = 450;
            this.LueBankAcCode.Size = new System.Drawing.Size(409, 20);
            this.LueBankAcCode.TabIndex = 38;
            this.LueBankAcCode.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode.EditValueChanged += new System.EventHandler(this.LueBankAcCode_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(34, 269);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 14);
            this.label7.TabIndex = 37;
            this.label7.Text = "Transfer To";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(109, 72);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 30;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 250;
            this.LueCurCode.Size = new System.Drawing.Size(186, 20);
            this.LueCurCode.TabIndex = 28;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(49, 75);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 14);
            this.label24.TabIndex = 27;
            this.label24.Text = "Currency";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalAmt
            // 
            this.TxtTotalAmt.EnterMoveNextControl = true;
            this.TxtTotalAmt.Location = new System.Drawing.Point(110, 182);
            this.TxtTotalAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalAmt.Name = "TxtTotalAmt";
            this.TxtTotalAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalAmt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalAmt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtTotalAmt.Properties.ReadOnly = true;
            this.TxtTotalAmt.Size = new System.Drawing.Size(186, 20);
            this.TxtTotalAmt.TabIndex = 30;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(110, 245);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.Size = new System.Drawing.Size(186, 20);
            this.TxtAmt.TabIndex = 36;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(22, 185);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 14);
            this.label6.TabIndex = 29;
            this.label6.Text = "Total Amount";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(11, 248);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 14);
            this.label14.TabIndex = 35;
            this.label14.Text = "Invoice Amount";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(110, 287);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(450, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(409, 20);
            this.MeeRemark.TabIndex = 40;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // TxtDownpayment
            // 
            this.TxtDownpayment.EnterMoveNextControl = true;
            this.TxtDownpayment.Location = new System.Drawing.Point(110, 224);
            this.TxtDownpayment.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDownpayment.Name = "TxtDownpayment";
            this.TxtDownpayment.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDownpayment.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDownpayment.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDownpayment.Properties.Appearance.Options.UseFont = true;
            this.TxtDownpayment.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDownpayment.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDownpayment.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDownpayment.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtDownpayment.Properties.ReadOnly = true;
            this.TxtDownpayment.Size = new System.Drawing.Size(186, 20);
            this.TxtDownpayment.TabIndex = 34;
            this.TxtDownpayment.Validated += new System.EventHandler(this.TxtDownpayment_Validated);
            // 
            // LblRemark
            // 
            this.LblRemark.AutoSize = true;
            this.LblRemark.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRemark.Location = new System.Drawing.Point(58, 290);
            this.LblRemark.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRemark.Name = "LblRemark";
            this.LblRemark.Size = new System.Drawing.Size(47, 14);
            this.LblRemark.TabIndex = 39;
            this.LblRemark.Text = "Remark";
            this.LblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(18, 227);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(87, 14);
            this.label15.TabIndex = 33;
            this.label15.Text = "Downpayment";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(46, 206);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 14);
            this.label13.TabIndex = 31;
            this.label13.Text = "Total Tax";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalTax
            // 
            this.TxtTotalTax.EnterMoveNextControl = true;
            this.TxtTotalTax.Location = new System.Drawing.Point(110, 203);
            this.TxtTotalTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalTax.Name = "TxtTotalTax";
            this.TxtTotalTax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalTax.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalTax.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalTax.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtTotalTax.Properties.ReadOnly = true;
            this.TxtTotalTax.Size = new System.Drawing.Size(186, 20);
            this.TxtTotalTax.TabIndex = 32;
            // 
            // DteDueDt
            // 
            this.DteDueDt.EditValue = null;
            this.DteDueDt.EnterMoveNextControl = true;
            this.DteDueDt.Location = new System.Drawing.Point(109, 28);
            this.DteDueDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDt.Name = "DteDueDt";
            this.DteDueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.Appearance.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteDueDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDt.Size = new System.Drawing.Size(123, 20);
            this.DteDueDt.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(45, 31);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 14);
            this.label9.TabIndex = 17;
            this.label9.Text = "Due Date";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(766, 132);
            this.Grd1.TabIndex = 17;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 132);
            this.Grd2.TabIndex = 17;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // LueOption
            // 
            this.LueOption.EnterMoveNextControl = true;
            this.LueOption.Location = new System.Drawing.Point(160, 22);
            this.LueOption.Margin = new System.Windows.Forms.Padding(5);
            this.LueOption.Name = "LueOption";
            this.LueOption.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.Appearance.Options.UseFont = true;
            this.LueOption.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueOption.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueOption.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueOption.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueOption.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueOption.Properties.DropDownRows = 30;
            this.LueOption.Properties.NullText = "[Empty]";
            this.LueOption.Properties.PopupWidth = 300;
            this.LueOption.Size = new System.Drawing.Size(202, 20);
            this.LueOption.TabIndex = 18;
            this.LueOption.ToolTip = "F4 : Show/hide list";
            this.LueOption.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueOption.EditValueChanged += new System.EventHandler(this.LueOption_EditValueChanged);
            this.LueOption.Leave += new System.EventHandler(this.LueOption_Leave);
            this.LueOption.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueOption_KeyDown);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(766, 132);
            this.Grd3.TabIndex = 17;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 431);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 10;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            this.LueFontSize.EditValueChanged += new System.EventHandler(this.LueFontSize_EditValueChanged);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 451);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 11;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // TcSalesInvoice4
            // 
            this.TcSalesInvoice4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcSalesInvoice4.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcSalesInvoice4.Location = new System.Drawing.Point(0, 96);
            this.TcSalesInvoice4.Name = "TcSalesInvoice4";
            this.TcSalesInvoice4.SelectedTabPage = this.Tp1;
            this.TcSalesInvoice4.Size = new System.Drawing.Size(808, 377);
            this.TcSalesInvoice4.TabIndex = 16;
            this.TcSalesInvoice4.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2,
            this.Tp3,
            this.Tp4});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.label5);
            this.Tp1.Controls.Add(this.DteTaxInvDt2);
            this.Tp1.Controls.Add(this.label28);
            this.Tp1.Controls.Add(this.label29);
            this.Tp1.Controls.Add(this.TxtTaxInvDocument2);
            this.Tp1.Controls.Add(this.label30);
            this.Tp1.Controls.Add(this.label11);
            this.Tp1.Controls.Add(this.DteTaxInvDt1);
            this.Tp1.Controls.Add(this.label27);
            this.Tp1.Controls.Add(this.TxtTaxAmt2);
            this.Tp1.Controls.Add(this.TxtTaxAmt1);
            this.Tp1.Controls.Add(this.LueTaxCode2);
            this.Tp1.Controls.Add(this.label12);
            this.Tp1.Controls.Add(this.LueTaxCode1);
            this.Tp1.Controls.Add(this.TxtTaxInvDocument1);
            this.Tp1.Controls.Add(this.label17);
            this.Tp1.Controls.Add(this.BtnJournalDocNo2);
            this.Tp1.Controls.Add(this.BtnJournalDocNo);
            this.Tp1.Controls.Add(this.label21);
            this.Tp1.Controls.Add(this.TxtJournalDocNo2);
            this.Tp1.Controls.Add(this.TxtJournalDocNo);
            this.Tp1.Controls.Add(this.label22);
            this.Tp1.Controls.Add(this.LueBankAcCode);
            this.Tp1.Controls.Add(this.label16);
            this.Tp1.Controls.Add(this.label7);
            this.Tp1.Controls.Add(this.DteDueDt);
            this.Tp1.Controls.Add(this.LueCurCode);
            this.Tp1.Controls.Add(this.label24);
            this.Tp1.Controls.Add(this.TxtTotalAmt);
            this.Tp1.Controls.Add(this.label9);
            this.Tp1.Controls.Add(this.TxtAmt);
            this.Tp1.Controls.Add(this.LueSPCode);
            this.Tp1.Controls.Add(this.label6);
            this.Tp1.Controls.Add(this.label10);
            this.Tp1.Controls.Add(this.label14);
            this.Tp1.Controls.Add(this.MeeRemark);
            this.Tp1.Controls.Add(this.TxtLocalDocNo);
            this.Tp1.Controls.Add(this.TxtDownpayment);
            this.Tp1.Controls.Add(this.LblRemark);
            this.Tp1.Controls.Add(this.label15);
            this.Tp1.Controls.Add(this.TxtTotalTax);
            this.Tp1.Controls.Add(this.label13);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(802, 349);
            this.Tp1.Text = "General";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(365, 163);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 14);
            this.label5.TabIndex = 79;
            this.label5.Text = "Amount";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvDt2
            // 
            this.DteTaxInvDt2.EditValue = null;
            this.DteTaxInvDt2.EnterMoveNextControl = true;
            this.DteTaxInvDt2.Location = new System.Drawing.Point(420, 138);
            this.DteTaxInvDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvDt2.Name = "DteTaxInvDt2";
            this.DteTaxInvDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvDt2.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvDt2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvDt2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvDt2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvDt2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvDt2.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvDt2.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvDt2.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvDt2.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvDt2.Size = new System.Drawing.Size(107, 20);
            this.DteTaxInvDt2.TabIndex = 76;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(383, 141);
            this.label28.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(33, 14);
            this.label28.TabIndex = 75;
            this.label28.Text = "Date";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(79, 162);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(27, 14);
            this.label29.TabIndex = 77;
            this.label29.Text = "Tax";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxInvDocument2
            // 
            this.TxtTaxInvDocument2.EnterMoveNextControl = true;
            this.TxtTaxInvDocument2.Location = new System.Drawing.Point(109, 138);
            this.TxtTaxInvDocument2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvDocument2.Name = "TxtTaxInvDocument2";
            this.TxtTaxInvDocument2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvDocument2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvDocument2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvDocument2.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvDocument2.Properties.MaxLength = 30;
            this.TxtTaxInvDocument2.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvDocument2.TabIndex = 74;
            this.TxtTaxInvDocument2.Validated += new System.EventHandler(this.TxtTaxInvDocument2_Validated);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(27, 141);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(79, 14);
            this.label30.TabIndex = 73;
            this.label30.Text = "Tax Invoice#";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(365, 119);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 14);
            this.label11.TabIndex = 71;
            this.label11.Text = "Amount";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvDt1
            // 
            this.DteTaxInvDt1.EditValue = null;
            this.DteTaxInvDt1.EnterMoveNextControl = true;
            this.DteTaxInvDt1.Location = new System.Drawing.Point(420, 94);
            this.DteTaxInvDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvDt1.Name = "DteTaxInvDt1";
            this.DteTaxInvDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvDt1.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvDt1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvDt1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvDt1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvDt1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvDt1.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvDt1.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvDt1.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvDt1.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvDt1.Size = new System.Drawing.Size(107, 20);
            this.DteTaxInvDt1.TabIndex = 68;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(383, 97);
            this.label27.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(33, 14);
            this.label27.TabIndex = 67;
            this.label27.Text = "Date";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxAmt2
            // 
            this.TxtTaxAmt2.EnterMoveNextControl = true;
            this.TxtTaxAmt2.Location = new System.Drawing.Point(420, 160);
            this.TxtTaxAmt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt2.Name = "TxtTaxAmt2";
            this.TxtTaxAmt2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt2.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt2.Properties.ReadOnly = true;
            this.TxtTaxAmt2.Size = new System.Drawing.Size(147, 20);
            this.TxtTaxAmt2.TabIndex = 80;
            // 
            // TxtTaxAmt1
            // 
            this.TxtTaxAmt1.EnterMoveNextControl = true;
            this.TxtTaxAmt1.Location = new System.Drawing.Point(420, 116);
            this.TxtTaxAmt1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt1.Name = "TxtTaxAmt1";
            this.TxtTaxAmt1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt1.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt1.Properties.ReadOnly = true;
            this.TxtTaxAmt1.Size = new System.Drawing.Size(147, 20);
            this.TxtTaxAmt1.TabIndex = 72;
            // 
            // LueTaxCode2
            // 
            this.LueTaxCode2.EnterMoveNextControl = true;
            this.LueTaxCode2.Location = new System.Drawing.Point(109, 160);
            this.LueTaxCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode2.Name = "LueTaxCode2";
            this.LueTaxCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode2.Properties.DropDownRows = 25;
            this.LueTaxCode2.Properties.NullText = "[Empty]";
            this.LueTaxCode2.Properties.PopupWidth = 200;
            this.LueTaxCode2.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode2.TabIndex = 78;
            this.LueTaxCode2.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode2.EditValueChanged += new System.EventHandler(this.LueTaxCode2_EditValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(79, 119);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 14);
            this.label12.TabIndex = 69;
            this.label12.Text = "Tax";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaxCode1
            // 
            this.LueTaxCode1.EnterMoveNextControl = true;
            this.LueTaxCode1.Location = new System.Drawing.Point(109, 116);
            this.LueTaxCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode1.Name = "LueTaxCode1";
            this.LueTaxCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode1.Properties.DropDownRows = 25;
            this.LueTaxCode1.Properties.NullText = "[Empty]";
            this.LueTaxCode1.Properties.PopupWidth = 200;
            this.LueTaxCode1.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode1.TabIndex = 70;
            this.LueTaxCode1.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode1.EditValueChanged += new System.EventHandler(this.LueTaxCode1_EditValueChanged);
            // 
            // TxtTaxInvDocument1
            // 
            this.TxtTaxInvDocument1.EnterMoveNextControl = true;
            this.TxtTaxInvDocument1.Location = new System.Drawing.Point(109, 94);
            this.TxtTaxInvDocument1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvDocument1.Name = "TxtTaxInvDocument1";
            this.TxtTaxInvDocument1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvDocument1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvDocument1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvDocument1.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvDocument1.Properties.MaxLength = 30;
            this.TxtTaxInvDocument1.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvDocument1.TabIndex = 66;
            this.TxtTaxInvDocument1.Validated += new System.EventHandler(this.TxtTaxInvDocument1_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(27, 97);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 14);
            this.label17.TabIndex = 65;
            this.label17.Text = "Tax Invoice#";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnJournalDocNo2
            // 
            this.BtnJournalDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnJournalDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnJournalDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnJournalDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnJournalDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnJournalDocNo2.Appearance.Options.UseFont = true;
            this.BtnJournalDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnJournalDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnJournalDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnJournalDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnJournalDocNo2.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnJournalDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnJournalDocNo2.Location = new System.Drawing.Point(299, 327);
            this.BtnJournalDocNo2.Name = "BtnJournalDocNo2";
            this.BtnJournalDocNo2.Size = new System.Drawing.Size(24, 20);
            this.BtnJournalDocNo2.TabIndex = 45;
            this.BtnJournalDocNo2.ToolTip = "Show Journal Information";
            this.BtnJournalDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnJournalDocNo2.ToolTipTitle = "Run System";
            this.BtnJournalDocNo2.Click += new System.EventHandler(this.BtnJournalDocNo2_Click);
            // 
            // BtnJournalDocNo
            // 
            this.BtnJournalDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnJournalDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnJournalDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnJournalDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnJournalDocNo.Appearance.Options.UseBackColor = true;
            this.BtnJournalDocNo.Appearance.Options.UseFont = true;
            this.BtnJournalDocNo.Appearance.Options.UseForeColor = true;
            this.BtnJournalDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnJournalDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnJournalDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnJournalDocNo.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnJournalDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnJournalDocNo.Location = new System.Drawing.Point(299, 307);
            this.BtnJournalDocNo.Name = "BtnJournalDocNo";
            this.BtnJournalDocNo.Size = new System.Drawing.Size(24, 20);
            this.BtnJournalDocNo.TabIndex = 43;
            this.BtnJournalDocNo.ToolTip = "Show Journal Information";
            this.BtnJournalDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnJournalDocNo.ToolTipTitle = "Run System";
            this.BtnJournalDocNo.Click += new System.EventHandler(this.BtnJournalDocNo_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Green;
            this.label21.Location = new System.Drawing.Point(325, 331);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(81, 14);
            this.label21.TabIndex = 46;
            this.label21.Text = "(Cancel Data)";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtJournalDocNo2
            // 
            this.TxtJournalDocNo2.EnterMoveNextControl = true;
            this.TxtJournalDocNo2.Location = new System.Drawing.Point(110, 329);
            this.TxtJournalDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo2.Name = "TxtJournalDocNo2";
            this.TxtJournalDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo2.Properties.MaxLength = 30;
            this.TxtJournalDocNo2.Properties.ReadOnly = true;
            this.TxtJournalDocNo2.Size = new System.Drawing.Size(186, 20);
            this.TxtJournalDocNo2.TabIndex = 44;
            // 
            // TxtJournalDocNo
            // 
            this.TxtJournalDocNo.EnterMoveNextControl = true;
            this.TxtJournalDocNo.Location = new System.Drawing.Point(110, 308);
            this.TxtJournalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo.Name = "TxtJournalDocNo";
            this.TxtJournalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo.Properties.MaxLength = 30;
            this.TxtJournalDocNo.Properties.ReadOnly = true;
            this.TxtJournalDocNo.Size = new System.Drawing.Size(186, 20);
            this.TxtJournalDocNo.TabIndex = 42;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(51, 311);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 14);
            this.label22.TabIndex = 41;
            this.label22.Text = "Journal#";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.Grd1);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 132);
            this.Tp2.Text = "List of DO";
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.Grd2);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(766, 132);
            this.Tp3.Text = "Customer\'s Deposit Balance";
            // 
            // Tp4
            // 
            this.Tp4.Appearance.Header.Options.UseTextOptions = true;
            this.Tp4.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp4.Controls.Add(this.LueOption);
            this.Tp4.Controls.Add(this.Grd3);
            this.Tp4.Name = "Tp4";
            this.Tp4.Size = new System.Drawing.Size(766, 132);
            this.Tp4.Text = "Discount, Additional Cost, Etc";
            // 
            // TxtReceiptNo
            // 
            this.TxtReceiptNo.EnterMoveNextControl = true;
            this.TxtReceiptNo.Location = new System.Drawing.Point(82, 70);
            this.TxtReceiptNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtReceiptNo.Name = "TxtReceiptNo";
            this.TxtReceiptNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtReceiptNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtReceiptNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtReceiptNo.Properties.Appearance.Options.UseFont = true;
            this.TxtReceiptNo.Properties.MaxLength = 16;
            this.TxtReceiptNo.Properties.ReadOnly = true;
            this.TxtReceiptNo.Size = new System.Drawing.Size(198, 20);
            this.TxtReceiptNo.TabIndex = 44;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(22, 73);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 14);
            this.label3.TabIndex = 43;
            this.label3.Text = "Receipt#";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmSalesInvoice4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 473);
            this.Name = "FrmSalesInvoice4";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDownpayment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueOption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcSalesInvoice4)).EndInit();
            this.TcSalesInvoice4.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.Tp1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvDocument2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvDocument1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.Tp3.ResumeLayout(false);
            this.Tp4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtReceiptNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.DateEdit DteDueDt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit LueCtCode;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtTotalAmt;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtDownpayment;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtTotalTax;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label LblRemark;
        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label10;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private DevExpress.XtraEditors.LookUpEdit LueBankAcCode;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueOption;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.LookUpEdit LueSPCode;
        private DevExpress.XtraTab.XtraTabControl TcSalesInvoice4;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        private DevExpress.XtraTab.XtraTabPage Tp4;
        public DevExpress.XtraEditors.SimpleButton BtnJournalDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnJournalDocNo;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvDt2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvDocument2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvDt1;
        private System.Windows.Forms.Label label27;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt2;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt1;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode2;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode1;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvDocument1;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtReceiptNo;
        private System.Windows.Forms.Label label3;

    }
}