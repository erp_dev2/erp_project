﻿#region Update
/*
    27/03/2018 [WED] tambah primary PosCode2
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmOrganizationalStructureFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmOrganizationalStructure mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmOrganizationalStructureFind(FrmOrganizationalStructure FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Position"+Environment.NewLine+"Code", 
                        "Master"+Environment.NewLine+"Position",
                        "Position",
                        "Parent",
                        "Created"+Environment.NewLine+"By",   
                        
                        //6-10
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",

                        //11
                        "PosCode2"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        100, 250, 250, 250, 100, 

                        //6-10
                        100, 100, 100, 100, 100,

                        //11
                        0
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 6, 9 });
            Sm.GrdFormatTime(Grd1, new int[] { 7, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PosCode, B.PosName, A.PosCode2, A.PosName2, C.PosName As ParentName, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblOrganizationalStructure A ");
            SQL.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblPosition C On A.Parent=C.PosCode ");
            SQL.AppendLine(Filter + " Order By B.PosName, A.PosName2; ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8, 9, 10 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtPosName.Text, new string[] { "A.PosCode", "B.PosName", "A.PosCode2", "A.PosName2" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[]
                        {
                            //0
                            "PosCode", 
                                
                            //1-5
                            "PosName", "PosName2", "ParentName", "CreateBy", "CreateDt", 
                            
                            //6-8
                            "LastUpBy", "LastUpDt", "PosCode2"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 11));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPosName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPosName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Position");
        }

        #endregion

        #endregion
    }
}
