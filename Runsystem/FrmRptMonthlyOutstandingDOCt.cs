#region Update
/*
    11/02/2021 [ICA+IQA/KSM] New Apps
    10/08/2021 [TRI/KSM] menambahkan printout
    15/09/2021 [RDA/KSM] menambahkan filter item category
    24/09/2021 [VIN/KSM] Bug Printout 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using System.Collections;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthlyOutstandingDOCt : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptMonthlyOutstandingDOCt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -365);
                Sl.SetLueItCtCodeActive(ref LueItCtCode, string.Empty);
                SetLueRemark(ref LueRemark);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT B.ItName, A.*, (A.Qty - (A.DOQty + A.StockWhs + A.OtherWhsDOQty + A.StockOtherWhs)) AS RestOrder, B.ItCtCode, C.ItCtName ");
            SQL.AppendLine("FROM ( ");
            SQL.AppendLine("    SELECT T.ItCode, IFNULL(T.Qty, 0) Qty, IFNULL(T2.Qty, 0) DOQty, IFNULL(T3.StockWhs, 0) StockWhs, ");
            SQL.AppendLine("    IFNULL(T3.StockOtherWhs, 0) StockOtherWhs, IFNULL(T4.OtherWhsDOQty, 0) OtherWhsDOQty, ");
            SQL.AppendLine("    IFNULL(T2.Remark, 'NO DO') Remark ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        SELECT C.ItCode, SUM(IFNULL(C.Qty, 0)) Qty ");
            SQL.AppendLine("        FROM TblSalesContract A ");
            SQL.AppendLine("		INNER JOIN TblSalesMemoHdr B ON A.SalesMemoDocNo = B.DocNo AND B.CancelInd = 'N' ");
            SQL.AppendLine("        INNER JOIN tblSalesMemoDtl C ON B.DocNo = C.DocNo ");
            SQL.AppendLine("        WHERE (A.DocDt BETWEEN @DocDt1 AND @DocDt2) AND A.CancelInd = 'N' ");
            SQL.AppendLine("        GROUP BY C.ItCode ");
            SQL.AppendLine("    )T ");
            SQL.AppendLine("    LEFT JOIN ( ");
            SQL.AppendLine("        SELECT B.ItCode, SUM(IFNULL(B.Qty, 0)) Qty, 'DO' as Remark ");
            SQL.AppendLine("        FROM tbldoct2hdr A ");
            SQL.AppendLine("        INNER JOIN tbldoct2dtl B ON A.DocNo = B.DocNo AND B.CancelInd = 'N' ");
            SQL.AppendLine("        INNER JOIN tbldrhdr C ON A.DRDocNo = C.DocNo AND C.CancelInd = 'N' ");
            SQL.AppendLine("        INNER JOIN tbldrdtl D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("        INNER JOIN tblsalescontract E ON D.SCDocNo = E.DOcNo AND (E.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            SQL.AppendLine("        WHERE A.DocDt BETWEEN @DocDt1 AND @DocDt2 ");
            SQL.AppendLine("        GROUP BY B.ItCode ");
            SQL.AppendLine("    )T2 ON T.ItCode = T2.ItCode ");
            SQL.AppendLine("    LEFT JOIN ( ");
            SQL.AppendLine("        Select ItCode, SUM(Qty) Qty,");
            SQL.AppendLine("        SUM(CASE When WhsCode IN ('G.SPN','GBJ-K') Then Qty Else 0 End ) as StockWhs, ");
            SQL.AppendLine("        SUM(CASE When WhsCode IN ('G.SPN','GBJ-K') Then 0 Else Qty End ) as StockOtherWhs ");
            SQL.AppendLine("        FROM tblstocksummary ");
            SQL.AppendLine("        GROUP BY ItCode ");
            SQL.AppendLine("    )T3 ON T.ItCode = T3.ItCode ");
            SQL.AppendLine("    LEFT JOIN ( ");
            SQL.AppendLine("        SELECT B.ItCode, SUM(B.Qty) as OtherWhsDOQty ");
            SQL.AppendLine("        FROM tbldoct2hdr A ");
            SQL.AppendLine("        INNER JOIN tbldoct2dtl B ON A.DocNo = B.DocNo AND B.CancelInd = 'N' ");
            SQL.AppendLine("        WHERE A.WhsCode NOT IN ('G.SPN','GBJ-K') ");
            SQL.AppendLine("        And A.DocDt BETWEEN @DocDt1 AND @DocDt2 ");
            SQL.AppendLine("        GROUP BY B.ItCode ");
            SQL.AppendLine("    )T4 ON T.ItCode = T4.ItCode ");
            SQL.AppendLine(")A ");
            SQL.AppendLine("INNER JOIN tblitem B ON A.ItCode = B.ItCode ");
            SQL.AppendLine("INNER JOIN tblitemcategory C ON B.ItCtCode = C.ItCtCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Item's Code",
                    "Item's Name",
                    "Item's Category",
                    "Quantity",
                    "Warehouse DO",

                    //6-10
                    "Warehouse Stock",
                    "Other Warehouse"+Environment.NewLine+"DO",
                    "Other Warehouse"+Environment.NewLine+"Stock",
                    "Rest Order",
                    "Remark"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 150, 250, 130, 130, 

                    //6-10
                    130, 130, 130, 130, 110 
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "B.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueRemark), "A.Remark", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "B.ItCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By B.ItName;",
                        new string[]
                        { 
                            //0
                            "ItCode", 

                            //1-5
                            "ItName", "ItCtName", "Qty", "DOQty", "StockWhs", 
                            
                            //6-9
                            "OtherWhsDOQty", "StockOtherWhs", "RestOrder", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            if (Sm.GetGrdDec(Grd1, Row, 9) < 0)
                            {
                                Grd.Cells[Row, 9].ForeColor = Color.Red;
                            } 
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 6, 7, 8, 9 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            try
            {
                if (Grd1.Rows.Count == 0)
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                    return;
                }
                iGSubtotalManager.ForeColor = Color.Black;

                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Additional Method 

        private void SetLueRemark(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select 'DO' As Col1, 'DO' As Col2 ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select 'NO DO' As Col1, 'NO DO' As Col2 ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            var l = new List<Header>();
            var ldtl = new List<Detail>();

            string[] TableName = { "Header", "Detail" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            l.Add(new Header()
            {
                CompanyLogo = Sm.GetValue("Select Distinct @Param As CompanyLogo", @Sm.CompanyLogo()),
                CompanyName = Gv.CompanyName,
            });
            myLists.Add(l);

            #endregion

            #region Detail

            int nomor = 0;
            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                nomor = nomor + 1;
                ldtl.Add(new Detail()
                {
                    No = nomor,
                    ItName = Sm.GetGrdStr(Grd1, i, 2),
                    Qty = Sm.GetGrdDec(Grd1, i, 4),
                    DOQty = Sm.GetGrdDec(Grd1, i, 5),
                    StockWhs = Sm.GetGrdDec(Grd1, i, 6),
                    OtherWhsDOQty = Sm.GetGrdDec(Grd1, i, 7),
                    StockOtherWhs = Sm.GetGrdDec(Grd1, i, 8),
                    RestOrder = Sm.GetGrdDec(Grd1, i, 9),
                });

            }
            myLists.Add(ldtl);
            #endregion

            Sm.PrintReport("OutstandingRSDKSM", myLists, TableName, false);

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueRemark_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueRemark, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkRemark_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Remark");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCodeActive), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        #endregion

        #endregion

        #region Class

        private class Header
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
        }
        private class Detail
        {
            // "ItName", "Qty", "DOQty", "StockWhs", "OtherWhsDOQty", \"StockOtherWhs", "RestOrder", "Remark"
            public int No { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public decimal DOQty { get; set; }
            public decimal StockWhs { get; set; }
            public decimal OtherWhsDOQty { get; set; }
            public decimal StockOtherWhs { get; set; }
            public decimal RestOrder { get; set; }
        }
        #endregion
    }
}
