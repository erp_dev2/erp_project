﻿#region Update
/*
    07/02/2023 [WED/MNET] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmServiceDeliveryDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmServiceDelivery mFrmParent;
        private string mSQL = string.Empty, mCtCode = string.Empty;

        #endregion

        #region Constructor

        public FrmServiceDeliveryDlg3(FrmServiceDelivery FrmParent, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "DNo",
                        "Name",
                        "Address",
                        "City",
                        "City",

                        //6-10
                        "Country",
                        "Country",
                        "Postal Code",
                        "Phone",
                        "Fax",

                        //11-12
                        "Email",
                        "Mobile"
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 6 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.Name, A.Address, ");
            SQL.AppendLine("A.CityCode, B.CityName, A.CntCode, C.CntName, A.PostalCd, ");
            SQL.AppendLine("A.Phone, A.Fax, A.Email, A.Mobile ");
            SQL.AppendLine("From TblCustomerShipAddress A ");
            SQL.AppendLine("Left Join TblCity B On A.CityCode=B.CityCode ");
            SQL.AppendLine("Left Join TblCountry C On A.CntCode=C.CntCode ");
            SQL.AppendLine("Where A.CtCode=@CtCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.FilterStr(ref Filter, ref cm, TxtName.Text, "A.Name", false);
                Sm.FilterStr(ref Filter, ref cm, TxtCityName.Text, "B.CityName", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + "Order By A.Name;",
                    new string[]
                    { 
                            //0
                            "DNo", 
                             
                            //1-5
                            "Name",
                            "Address",
                            "CityCode",
                            "CityName",
                            "CntCode", 
            
                            //6-10
                            "CntName",
                            "PostalCD",
                            "Phone",
                            "Fax",
                            "Email", 
                             
                            //11
                            "Mobile"                         },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    }, true, false, false, true
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                //mFrmParent.mCtSADNo = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtSAName.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.MeeSAAddress.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                mFrmParent.mCityCode = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.TxtCity.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.mCntCode = Sm.GetGrdStr(Grd1, Row, 6);
                mFrmParent.TxtCountry.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                mFrmParent.TxtPostalCd.EditValue = Sm.GetGrdStr(Grd1, Row, 8);
                mFrmParent.TxtPhone.EditValue = Sm.GetGrdStr(Grd1, Row, 9);
                mFrmParent.TxtFax.EditValue = Sm.GetGrdStr(Grd1, Row, 10);
                mFrmParent.TxtEmail.EditValue = Sm.GetGrdStr(Grd1, Row, 11);
                mFrmParent.TxtMobile.EditValue = Sm.GetGrdStr(Grd1, Row, 12);

                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Name");
        }

        private void TxtCityName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCityName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Name");
        }

        #endregion

        #endregion

    }
}
