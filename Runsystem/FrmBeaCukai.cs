﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBeaCukai : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "",
             mDocNo = string.Empty,
             mCtCode = string.Empty;
        public string mSectionNo = string.Empty;
        internal FrmBeaCukaiFind FrmFind;

        #endregion

        #region Constructor

        public FrmBeaCukai(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Bea Cukai";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible =
                    BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible =
                    BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "SO#",
                        "SO DNo",
                        "",
                        "Agent",
                        "Item's Code",
                        
                        //6-10
                        "",
                        "Item's" + Environment.NewLine + "Local Code",
                        "Item's Name",
                        "Quantity" + Environment.NewLine + "(Packaging)",
                        "UoM" + Environment.NewLine + "(Packaging)",
                        
                        //11-15
                        "DR/PL" + Environment.NewLine + "Quantity",
                        "DO" + Environment.NewLine + "Quantity",
                        "Balance",
                        "UoM",
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 0, 20, 200, 100, 
                        
                        //6-10
                        20, 100, 250, 80, 80, 
                        
                        //11-15
                        80, 80, 80, 80
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11, 12, 13 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 15 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14 });


        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtPLDocNo, DteDocDt, TxtCtCode, TxtCntName, TxtKodePabean, TxtPabean, TxtNoPengajuan, TxtJnsEkspor, TxtKategEkspor, TxtCaraDagang, TxtCaraBayar, 
                        TxtNoPendaftaran, DtePendaftaranDt, TxtNoBC, DteBCDt, TxtSubPos, TxtBankDevisa, TxtValutaAsing, TxtPetiKemas, TxtStatusPeti, TxtJmlPeti,
                        TxtNoPeti, TxtNPWP, TxtNmNPWP, MeeAlamat, TxtNoPokokPPJK, DtePPJKDt, TxtPemeriksaan, TxtKantorPemeriksaan, TxtJnsKemasan, TxtJmlKemasan, TxtMerkKemasan,
                        TxtPengangkutan, TxtSaranaPengangkut, TxtNoPengangkut, TxtBenderaPengangkut, DteTglEksporDt

                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 7, 8 });
                    BtnPLDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtKodePabean, TxtPabean, TxtNoPengajuan, TxtJnsEkspor, TxtKategEkspor, TxtCaraDagang, TxtCaraBayar, 
                        TxtNoPendaftaran, DtePendaftaranDt, TxtNoBC, DteBCDt, TxtSubPos, TxtBankDevisa, TxtValutaAsing, TxtPetiKemas, TxtStatusPeti, TxtJmlPeti,
                        TxtNoPeti, TxtNPWP, TxtNmNPWP, MeeAlamat, TxtNoPokokPPJK, DtePPJKDt, TxtPemeriksaan, TxtKantorPemeriksaan, TxtJnsKemasan, TxtJmlKemasan, TxtMerkKemasan,
                        TxtPengangkutan, TxtSaranaPengangkut, TxtNoPengangkut, TxtBenderaPengangkut, DteTglEksporDt
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnPLDocNo.Enabled = true;
                    // Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4, 6 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtPLDocNo, TxtCtCode, TxtCntName, DteDocDt, TxtKodePabean, TxtPabean, TxtNoPengajuan, TxtJnsEkspor, TxtKategEkspor, TxtCaraDagang, TxtCaraBayar, 
                TxtNoPendaftaran, DtePendaftaranDt, TxtNoBC, DteBCDt, TxtSubPos, TxtBankDevisa, TxtValutaAsing, TxtPetiKemas, TxtStatusPeti, TxtJmlPeti,
                TxtNoPeti, TxtNPWP, TxtNmNPWP, MeeAlamat, TxtNoPokokPPJK, TxtPemeriksaan, TxtKantorPemeriksaan, TxtJnsKemasan, TxtJmlKemasan, TxtMerkKemasan,
                TxtPengangkutan, TxtSaranaPengangkut, TxtNoPengangkut, TxtBenderaPengangkut, DteTglEksporDt
            });
            ChkCancelInd.Checked = false;
            BtnPLDocNo.Enabled = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {

            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 9, 11, 12, 13 });
         
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBeaCukaiFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                DtePendaftaranDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteBCDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DtePPJKDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteTglEksporDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            var l = new List<Beacukai>();
            var l2 = new List<Beacukai2>();
            var l3 = new List<Beacukai3>();
            string[] TableName = { "BeaCukai", "Beacukai2", "Beacukai3" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header 1

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, DATE_FORMAT(A.DocDt,'%d-%m-%Y') As DocDt, A.PLDocNo, A.KodePabean, A.Pabean, A.NoPengajuan, A.JnsEkspor, ");
            SQL.AppendLine("A.KategEkspor, A.CaraDagang, A.CaraBayar, A.NoPendaftaran, DATE_FORMAT(A.PendaftaranDt,'%d-%m-%Y') As PendaftaranDt, A.NoBC, DATE_FORMAT(A.BCDt,'%d-%m-%Y')As BCDt, A.SubPos, A.BankDevisa, ");
            SQL.AppendLine("A.ValutaAsing, A.PetiKemas, A.StatusPeti, A.JmlPeti, A.NoPeti, A.NPWP, A.NmNPWP, A.Alamat, A.NoPokokPPJK, DATE_FORMAT(A.PPJKDt,'%d-%m-%Y')As PPJKDt, A.Pemeriksaan, ");
            SQL.AppendLine("A.KantorPemeriksaan, A.JnsKemasan, A.JmlKemasan, A.MerkKemasan, E.CtName, E.Address, A.Pengangkutan, A.SaranaPengangkut, A.NoPengangkut, A.BenderaPengangkut, ");
            SQL.AppendLine("DATE_FORMAT(A.TglEksporDt,'%d-%m-%Y') As TglEksporDt, B.InvoiceNo, DATE_FORMAT(B.DocDt,'%d-%m-%Y') As InvoiceDt, C.SPPlaceDelivery, ");
            SQL.AppendLine("E.CntCode, F.CntName, G.PortName As PortLoading, H.PortName As PortDischarge, C.SpPlaceReceipt, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity' ");
            SQL.AppendLine("From TblBeaCukai A ");
            SQL.AppendLine("Inner Join TblPlHdr B On A.PLDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblSIHdr C On B.SIDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblSP D On C.SPDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer E On D.CtCode=E.CtCode ");
            SQL.AppendLine("Inner Join TblCountry F On E.CntCode=F.CntCode ");
            SQL.AppendLine("Inner Join TblPort G On C.SPPortCode1 = G.PortCode ");
            SQL.AppendLine("Inner Join TblPort H On C.SPPortCode2 = H.PortCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]

                {
                    //0
                    "CompanyName",

                    //1-5
                    "CompanyAddress",
                    "CompanyAddressCity",
                    "DocNo",
                    "DocDt",
                    "PLDocNo",
                    //6-10
                    "KodePabean",
                    "Pabean",
                    "NoPengajuan", 
                    "JnsEkspor", 
                    "KategEkspor", 
                    //11-15
                    "CaraDagang", 
                    "CaraBayar",  
                    "NoPendaftaran", 
                    "PendaftaranDt", 
                    "NoBC",
                    //16-20
                    "BCDt", 
                    "SubPos",  
                    "BankDevisa", 
                    "ValutaAsing", 
                    "PetiKemas", 
                    //21-25
                    "StatusPeti", 
                    "JmlPeti", 
                    "NoPeti", 
                    "NPWP", 
                    "NmNPWP", 
                    //26-30
                    "Alamat",  
                    "NoPokokPPJK", 
                    "PPJKDt",
                    "Pemeriksaan", 
                    "KantorPemeriksaan", 
                    
                    //31-35
                    "JnsKemasan", 
                    "JmlKemasan", 
                    "MerkKemasan",
                    "CtName",
                    "Address",
                   
                    //36-40
                    "Pengangkutan",
                    "SaranaPengangkut",
                    "NoPengangkut",
                    "BenderaPengangkut",
                    "TglEksporDt",

                    //41-45
                    "InvoiceNo",
                    "InvoiceDt",
                    "SPPlaceDelivery",
                    "CntCode",
                    "CntName",
                    
                    //46-48
                    "PortLoading",
                    "PortDischarge",
                    "SpPlaceReceipt",
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Beacukai()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),

                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyAddressCity = Sm.DrStr(dr, c[2]),
                            DocNo = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            PLDocNo = Sm.DrStr(dr, c[5]),
                            //6-10
                            KodePabean = Sm.DrStr(dr, c[6]),
                            Pabean = Sm.DrStr(dr, c[7]),
                            NoPengajuan = Sm.DrStr(dr, c[8]),
                            JnsEkspor = Sm.DrStr(dr, c[9]),
                            KategEkspor = Sm.DrStr(dr, c[10]),
                            //11-15
                            CaraDagang = Sm.DrStr(dr, c[11]),
                            CaraBayar = Sm.DrStr(dr, c[12]),
                            NoPendaftaran = Sm.DrStr(dr, c[13]),
                            PendaftaranDt = Sm.DrStr(dr, c[14]),
                            NoBC = Sm.DrStr(dr, c[15]),
                            //16-20
                            BCDt = Sm.DrStr(dr, c[16]),
                            SubPos = Sm.DrStr(dr, c[17]),
                            BankDevisa = Sm.DrStr(dr, c[18]),
                            ValutaAsing = Sm.DrStr(dr, c[19]),
                            PetiKemas = Sm.DrStr(dr, c[20]),
                            //21-25
                            StatusPeti = Sm.DrStr(dr, c[21]),
                            JmlPeti = Sm.DrStr(dr, c[22]),
                            NoPeti = Sm.DrStr(dr, c[23]),
                            NPWP = Sm.DrStr(dr, c[24]),
                            NmNPWP = Sm.DrStr(dr, c[25]),
                            //26-30
                            Alamat = Sm.DrStr(dr, c[26]),
                            NoPokokPPJK = Sm.DrStr(dr, c[27]),
                            PPJKDt = Sm.DrStr(dr, c[28]),
                            Pemeriksaan = Sm.DrStr(dr, c[29]),
                            KantorPemeriksaan = Sm.DrStr(dr, c[30]),

                            //31-35
                            JnsKemasan = Sm.DrStr(dr, c[31]),
                            JmlKemasan = Sm.DrStr(dr, c[32]),
                            MerkKemasan = Sm.DrStr(dr, c[33]),
                            CtName = Sm.DrStr(dr, c[34]),
                            Address = Sm.DrStr(dr, c[35]),

                            //36-40
                            Pengangkutan = Sm.DrStr(dr, c[36]),
                            SaranaPengangkut = Sm.DrStr(dr, c[37]),
                            NoPengangkut = Sm.DrStr(dr, c[38]),
                            BenderaPengangkut = Sm.DrStr(dr, c[39]),
                            TglEksporDt = Sm.DrStr(dr, c[40]),

                            //41-43
                            InvoiceNo = Sm.DrStr(dr, c[41]),
                            InvoiceDt = Sm.DrStr(dr, c[42]),
                            SPPlaceDelivery = Sm.DrStr(dr, c[43]),
                            CntCode = Sm.DrStr(dr, c[44]),
                            CntName = Sm.DrStr(dr, c[45]),

                            //46-48
                            PortLoading = Sm.DrStr(dr, c[46]),
                            PortDischarge = Sm.DrStr(dr, c[47]),
                            SpPlaceReceipt = Sm.DrStr(dr, c[48]),
                            

                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            #endregion

            #region Header 2

            var SQL2 = new StringBuilder();
            var cm2 = new MySqlCommand();

            SQL2.AppendLine("Select A.DocNo, J.DtName ");
            SQL2.AppendLine("From TblBeaCukai A ");
            SQL2.AppendLine("Inner Join TblPlHdr B On A.PLDocNo=B.DocNo ");
            SQL2.AppendLine("Inner Join TblSIHdr C On B.SIDocNo=C.DocNo ");
            SQL2.AppendLine("Inner Join TblSIDtl F On C.DocNo=F.DocNo  ");
            SQL2.AppendLine("Inner Join TblSOdtl G On F.SoDocNo = G.DocNo And G.DNo=F.SoDNo ");
            SQL2.AppendLine("Inner Join TblSoHdr H On G.DocNo = H.DocNo ");
            SQL2.AppendLine("Inner Join TblCtQtHdr I On H.CtQtDocNo = I.DocNo ");
            SQL2.AppendLine("Inner Join TblDeliveryType J On I.ShpMCode=J.DTCode ");
            SQL2.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[]

                {
                  
                    "DocNo",
                    "DTName",
                  
                });

                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new Beacukai2()
                        {
                            DocNo = Sm.DrStr(dr2, c2[0]),
                            DTName = Sm.DrStr(dr2, c2[1]),
                            
                        });
                    }
                }
                dr2.Close();
            }

            myLists.Add(l2);

            #endregion

            #region Header3

            var SQL3 = new StringBuilder();
            var cm3 = new MySqlCommand();
           
                SQL3.AppendLine("Select H.DocNo, Sum(Round(E.UPrice * Round(A2.Qty, 4), 2))As Fob, A2.SectionNo, B.ItName, Case ");
                SQL3.AppendLine("When A2.SectionNo = '1' then G.f1 ");
                SQL3.AppendLine("When A2.SectionNo = '2' then G.f2 ");
                SQL3.AppendLine("When A2.SectionNo = '3' then G.f3 ");
                SQL3.AppendLine("When A2.SectionNo = '4' then G.f4 ");
                SQL3.AppendLine("When A2.SectionNo = '5' then G.f5 ");
                SQL3.AppendLine("When A2.SectionNo = '6' then G.f6 ");
                SQL3.AppendLine("When A2.SectionNo = '7' then G.f7 ");
                SQL3.AppendLine("When A2.SectionNo = '8' then G.f8 ");
                SQL3.AppendLine("When A2.SectionNo = '9' then G.f9 ");
                SQL3.AppendLine("When A2.SectionNo = '10' then G.f10 ");
                SQL3.AppendLine("When A2.SectionNo = '11' then G.f11 ");
                SQL3.AppendLine("When A2.SectionNo = '12' then G.f12 ");
                SQL3.AppendLine("When A2.SectionNo = '13' then G.f13 ");
                SQL3.AppendLine("When A2.SectionNo = '14' then G.f14 ");
                SQL3.AppendLine("When A2.SectionNo = '15' then G.f15 ");
                SQL3.AppendLine("When A2.SectionNo = '16' then G.f16 ");
                SQL3.AppendLine("When A2.SectionNo = '17' then G.f17 ");
                SQL3.AppendLine("When A2.SectionNo = '18' then G.f18 ");
                SQL3.AppendLine("When A2.SectionNo = '19' then G.f19 ");
                SQL3.AppendLine("When A2.SectionNo = '20' then G.f20 ");
                SQL3.AppendLine("When A2.SectionNo = '21' then G.f21 ");
                SQL3.AppendLine("When A2.SectionNo = '22' then G.f22 ");
                SQL3.AppendLine("When A2.SectionNo = '23' then G.f23 ");
                SQL3.AppendLine("When A2.SectionNo = '24' then G.f24 ");
                SQL3.AppendLine("When A2.SectionNo = '25' then G.f25 ");
                SQL3.AppendLine("end as Freight ");
                SQL3.AppendLine("From TblSInv X ");
                SQL3.AppendLine("Inner Join TblPlhdr A On A.DocNo = X.PlDocNo ");
                SQL3.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                SQL3.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode  ");
                SQL3.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno ");
                SQL3.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo ");
                SQL3.AppendLine("Inner Join TblCtQtDtl E On D.CtQtDocNo = E.DocNo And C.CtQtDNo = E.Dno ");
                SQL3.AppendLine("Inner Join TblCtQtHdr F On D.CtQtDocNo = F.DocNo ");
                SQL3.AppendLine("Inner Join TblBeaCukai H On A.DocNo=H.PLDocNo ");
                SQL3.AppendLine("Left Join ( ");
                SQL3.AppendLine("       Select S.PLDocno, ");
                SQL3.AppendLine("       S.f1,S.f2,S.f3,S.f4,S.f5,S.f6,S.F7,S.F8,S.f9,S.f10,S.f11,S.f12,S.f13,S.f14, ");
                SQL3.AppendLine("       S.f15,S.f16,S.f17,S.f18,S.f19,S.f20,S.f21,S.f22,S.f23,S.f24,S.f25 ");
                SQL3.AppendLine("       From ( ");
                SQL3.AppendLine("       Select Distinct X.PLDocNo, ");
                SQL3.AppendLine("       if(A.Freight1 != 0, (A.Freight1-B.THC), 0) As F1, ");
                SQL3.AppendLine("       if(A.Freight2 != 0, (A.Freight2-B.THC), 0) As F2, ");
                SQL3.AppendLine("       if(A.Freight3 != 0, (A.Freight3-B.THC), 0) As F3, ");
                SQL3.AppendLine("       if(A.Freight4 != 0, (A.Freight4-B.THC), 0) As F4, ");
                SQL3.AppendLine("       if(A.Freight5 != 0, (A.Freight5-B.THC), 0) As F5, ");
                SQL3.AppendLine("       if(A.Freight6 != 0, (A.Freight6-B.THC), 0) As F6, ");
                SQL3.AppendLine("       if(A.Freight7 != 0, (A.Freight7-B.THC), 0) As F7, ");
                SQL3.AppendLine("       if(A.Freight8 != 0, (A.Freight8-B.THC), 0) As F8, ");
                SQL3.AppendLine("       if(A.Freight9 != 0, (A.Freight9-B.THC), 0) As F9, ");
                SQL3.AppendLine("       if(A.Freight10 != 0, (A.Freight10-B.THC), 0) As F10, ");
                SQL3.AppendLine("       if(A.Freight11 != 0, (A.Freight11-B.THC), 0) As F11, ");
                SQL3.AppendLine("       if(A.Freight12 != 0, (A.Freight12-B.THC), 0) As F12, ");
                SQL3.AppendLine("       if(A.Freight13 != 0, (A.Freight13-B.THC), 0) As F13, ");
                SQL3.AppendLine("       if(A.Freight14 != 0, (A.Freight14-B.THC), 0) As F14, ");
                SQL3.AppendLine("       if(A.Freight15 != 0, (A.Freight15-B.THC), 0) As F15, ");
                SQL3.AppendLine("       if(A.Freight16 != 0, (A.Freight16-B.THC), 0) As F16, ");
                SQL3.AppendLine("       if(A.Freight17 != 0, (A.Freight17-B.THC), 0) As F17, ");
                SQL3.AppendLine("       if(A.Freight18 != 0, (A.Freight18-B.THC), 0) As F18, ");
                SQL3.AppendLine("       if(A.Freight19 != 0, (A.Freight19-B.THC), 0) As F19, ");
                SQL3.AppendLine("       if(A.Freight20 != 0, (A.Freight20-B.THC), 0) As F20, ");
                SQL3.AppendLine("       if(A.Freight21 != 0, (A.Freight21-B.THC), 0) As F21, ");
                SQL3.AppendLine("       if(A.Freight22 != 0, (A.Freight22-B.THC), 0) As F22, ");
                SQL3.AppendLine("       if(A.Freight23 != 0, (A.Freight23-B.THC), 0) As F23, ");
                SQL3.AppendLine("       if(A.Freight24 != 0, (A.Freight24-B.THC), 0) As F24, ");
                SQL3.AppendLine("       if(A.Freight25 != 0, (A.Freight25-B.THC), 0) As F25 ");
                SQL3.AppendLine("       From TblSInv X  ");
                SQL3.AppendLine("       Inner Join TblPlhdr A On A.DocNo = X.PlDocNo ");
                SQL3.AppendLine("       Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                SQL3.AppendLine("       Left Join ");
                SQL3.AppendLine("       ( ");
                SQL3.AppendLine("               Select ParValue As THC From tblParameter Where ParCode = 'THC' ");
                SQL3.AppendLine("       ) B On 0=0  ");
                SQL3.AppendLine("       Where X.PLDocNo = @Docno ");
                SQL3.AppendLine(")S ");
                SQL3.AppendLine("Where S.PLDocNo = @DocNo ");
                SQL3.AppendLine(")G On H.DocNo = G.PLDocNo ");
                SQL3.AppendLine("Where H.DocNo='"+TxtDocNo.Text+"' And A2.SectionNo=@SectionNo ");
                SQL3.AppendLine("Group By H.DocNo,  A2.SectionNo ");

                using (var cn3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn3.Open();
                    cm3.Connection = cn3;
                    cm3.CommandText = SQL3.ToString();
                    Sm.CmParam<String>(ref cm3, "@DocNo", TxtPLDocNo.Text);
                    Sm.CmParam<String>(ref cm3, "@SectionNo", Sm.GetValue("select SectionNo from tblbeacukai where Docno='" + TxtDocNo.Text + "'"));

                    var dr3 = cm3.ExecuteReader();
                    var c3 = Sm.GetOrdinal(dr3, new string[] 
                    {
                        //0-4
                        "DocNo",

                        "Fob",
                        "SectionNo",
                        "ItName",
                        "Freight"

                    });
                    if (dr3.HasRows)
                    {
                        int nomor = 0;
                        while (dr3.Read())
                        {
                            nomor = nomor + 1;
                            l3.Add(new Beacukai3()
                            {
                                nomor= nomor,
                                DocNo = Sm.DrStr(dr3, c3[0]),
                                Fob = Sm.DrDec(dr3, c3[1]),
                                SectionNo = Sm.DrStr(dr3, c3[2]),
                                ItName = Sm.DrStr(dr3, c3[3]),
                                Freight = Sm.DrDec(dr3, c3[4]),
                             });
                        }
                    }
                    dr3.Close();
            }
            myLists.Add(l3);

            #endregion

                Sm.PrintReport("BeaCukai", myLists, TableName, false);
            {
                Sm.PrintReport("NPE", myLists, TableName, false);
            }


        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BeaCukai", "TblBeaCukai");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveBecuk(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document Date") ||
                Sm.IsTxtEmpty(TxtPLDocNo, "Packing list", false) ||
                Sm.IsTxtEmpty(TxtPabean, "Kantor pabean", false) ||
                Sm.IsTxtEmpty(TxtKodePabean, "Kode pabean", false) ||
                Sm.IsTxtEmpty(TxtNoPengajuan, "Nomor pengajuan", false) ||
                Sm.IsTxtEmpty(TxtPengangkutan, "Cara pengangkutan", false) ||
                Sm.IsTxtEmpty(TxtSaranaPengangkut, "Sarana pengangkut", false);
        }

        private MySqlCommand SaveBecuk(string DocNo)
        {
            var SQL = new StringBuilder();

             SQL.AppendLine("Insert Into TblBeaCukai(DocNo, DocDt, PLDocNo,SectionNo, KodePabean, Pabean, NoPengajuan, JnsEkspor, KategEkspor, CaraDagang, ");
             SQL.AppendLine("CaraBayar, Pengangkutan, SaranaPengangkut, NoPengangkut, BenderaPengangkut, TglEksporDt, PetiKemas, StatusPeti, ");
             SQL.AppendLine("JmlPeti, NoPeti, BankDevisa, ValutaAsing, NoPendaftaran, PendaftaranDt, NoBC, BCDt, SubPos, NPWP, NmNPWP, ");
             SQL.AppendLine("Alamat, NoPokokPPJK, PPJKDt, Pemeriksaan, KantorPemeriksaan, JnsKemasan, JmlKemasan, MerkKemasan, CreateBy, CreateDt )");
             SQL.AppendLine("Values(@DocNo, @DocDt, @PLDocNo, @SectionNo, @KodePabean, @Pabean, @NoPengajuan, @JnsEkspor, @KategEkspor, @CaraDagang,");
             SQL.AppendLine("@CaraBayar, @Pengangkutan, @SaranaPengangkut, @NoPengangkut, @BenderaPengangkut, @TglEksporDt, @PetiKemas, @StatusPeti, ");
             SQL.AppendLine("@JmlPeti, @NoPeti, @BankDevisa, @ValutaAsing, @NoPendaftaran, @PendaftaranDt, @NoBC, @BCDt, @SubPos, @NPWP, @NmNPWP, ");
             SQL.AppendLine("@Alamat, @NoPokokPPJK, @PPJKDt, @Pemeriksaan, @KantorPemeriksaan, @JnsKemasan, @JmlKemasan, @MerkKemasan,  @CreateBy, CurrentDateTime()) ");


            var cm = new MySqlCommand()
            { CommandText = SQL.ToString()};

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));

            Sm.CmParam<String>(ref cm, "@PLDocNo",TxtPLDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SectionNo", mSectionNo);
            Sm.CmParam<String>(ref cm, "@KodePabean", TxtKodePabean.Text);
            Sm.CmParam<String>(ref cm, "@Pabean", TxtPabean.Text);
            Sm.CmParam<String>(ref cm, "@NoPengajuan", TxtNoPengajuan.Text);
            Sm.CmParam<String>(ref cm, "@JnsEkspor", TxtJnsEkspor.Text);
            Sm.CmParam<String>(ref cm, "@KategEkspor", TxtKategEkspor.Text);
            Sm.CmParam<String>(ref cm, "@CaraDagang", TxtCaraDagang.Text);
            Sm.CmParam<String>(ref cm, "@CaraBayar", TxtCaraBayar.Text);
            Sm.CmParam<String>(ref cm, "@Pengangkutan", TxtPengangkutan.Text);
            Sm.CmParam<String>(ref cm, "@SaranaPengangkut", TxtSaranaPengangkut.Text);

            Sm.CmParam<String>(ref cm, "@NoPengangkut", TxtNoPengangkut.Text);
            Sm.CmParam<String>(ref cm, "@BenderaPengangkut", TxtBenderaPengangkut.Text);
            Sm.CmParamDt(ref cm, "@TglEksporDt", Sm.GetDte(DteTglEksporDt));
            Sm.CmParam<String>(ref cm, "@PetiKemas", TxtPetiKemas.Text);
            Sm.CmParam<String>(ref cm, "@StatusPeti", TxtStatusPeti.Text);
            Sm.CmParam<String>(ref cm, "@JmlPeti", TxtJmlPeti.Text);
            Sm.CmParam<String>(ref cm, "@NoPeti", TxtNoPeti.Text);
            Sm.CmParam<String>(ref cm, "@BankDevisa", TxtBankDevisa.Text);
            Sm.CmParam<String>(ref cm, "@ValutaAsing", TxtValutaAsing.Text);
            Sm.CmParam<String>(ref cm, "@NoPendaftaran", TxtNoPendaftaran.Text);

            Sm.CmParamDt(ref cm, "@PendaftaranDt", Sm.GetDte(DtePendaftaranDt));
            Sm.CmParam<String>(ref cm, "@NoBC", TxtNoBC.Text);
            Sm.CmParamDt(ref cm, "@BCDt", Sm.GetDte(DteBCDt)); 
            Sm.CmParam<String>(ref cm, "@SubPos", TxtSubPos.Text);
            Sm.CmParam<String>(ref cm, "@NPWP", TxtNPWP.Text);
            Sm.CmParam<String>(ref cm, "@NmNPWP", TxtNmNPWP.Text);
            Sm.CmParam<String>(ref cm, "@Alamat", MeeAlamat.Text);
            Sm.CmParam<String>(ref cm, "@NoPokokPPJK", TxtNoPokokPPJK.Text);
            Sm.CmParamDt(ref cm, "@PPJKDt", Sm.GetDte(DtePPJKDt));
            Sm.CmParam<String>(ref cm, "@Pemeriksaan", TxtPemeriksaan.Text);

            Sm.CmParam<String>(ref cm, "@KantorPemeriksaan", TxtKantorPemeriksaan.Text);
            Sm.CmParam<String>(ref cm, "@JnsKemasan", TxtJnsKemasan.Text );
            Sm.CmParam<String>(ref cm, "@JmlKemasan", TxtJmlKemasan.Text);
            Sm.CmParam<String>(ref cm, "@MerkKemasan", TxtMerkKemasan.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelBeaCukai());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblBeaCukai " +
                    "Where CancelInd='Y' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelBeaCukai()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBeaCukai Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowDataHdr(DocNo);
                ShowDataDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDataHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.PLDocNo, E.CtName, A.KodePabean, A.Pabean, A.NoPengajuan, A.JnsEkspor, A.KategEkspor, A.CaraDagang, ");
            SQL.AppendLine("A.CaraBayar, A.Pengangkutan, A.SaranaPengangkut, A.NoPengangkut, A.BenderaPengangkut, A.TglEksporDt, A.PetiKemas, A.StatusPeti, ");
            SQL.AppendLine("A.JmlPeti, A.NoPeti, A.BankDevisa, A.ValutaAsing, A.NoPendaftaran, A.PendaftaranDt, A.NoBC, A.BCDt, A.SubPos, A.NPWP, A.NmNPWP, A.Alamat, ");
            SQL.AppendLine("A.NoPokokPPJK, A.PPJKDt, A.Pemeriksaan, A.KantorPemeriksaan, A.JnsKemasan, A.JmlKemasan, A.MerkKemasan,  ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When A.SectionNo = '1' Then B.Cnt1 ");
            SQL.AppendLine("When A.SectionNo = '2' Then B.Cnt2 ");
            SQL.AppendLine("When A.SectionNo = '3' Then B.Cnt3 ");
            SQL.AppendLine("When A.SectionNo = '4' Then B.Cnt4 ");
            SQL.AppendLine("When A.SectionNo = '5' Then B.Cnt5 ");
            SQL.AppendLine("When A.SectionNo = '6' Then B.Cnt6 ");
            SQL.AppendLine("When A.SectionNo = '7' Then B.Cnt7 ");
            SQL.AppendLine("When A.SectionNo = '8' Then B.Cnt8 ");
            SQL.AppendLine("When A.SectionNo = '9' Then B.Cnt9 ");
            SQL.AppendLine("When A.SectionNo = '10' Then B.Cnt10 ");
            SQL.AppendLine("When A.SectionNo = '11' Then B.Cnt11 ");
            SQL.AppendLine("When A.SectionNo = '12' Then B.Cnt12 ");
            SQL.AppendLine("When A.SectionNo = '13' Then B.Cnt13 ");
            SQL.AppendLine("When A.SectionNo = '14' Then B.Cnt14 ");
            SQL.AppendLine("When A.SectionNo = '15' Then B.Cnt15 ");
            SQL.AppendLine("When A.SectionNo = '16' Then B.Cnt16 ");
            SQL.AppendLine("When A.SectionNo = '17' Then B.Cnt17 ");
            SQL.AppendLine("When A.SectionNo = '18' Then B.Cnt18 ");
            SQL.AppendLine("When A.SectionNo = '19' Then B.Cnt19 ");
            SQL.AppendLine("When A.SectionNo = '20' Then B.Cnt20 ");
            SQL.AppendLine("When A.SectionNo = '21' Then B.Cnt21 ");
            SQL.AppendLine("When A.SectionNo = '22' Then B.Cnt22 ");
            SQL.AppendLine("When A.SectionNo = '23' Then B.Cnt23 ");
            SQL.AppendLine("When A.SectionNo = '24' Then B.Cnt24 ");
            SQL.AppendLine("When A.SectionNo = '25' Then B.Cnt25  ");
            SQL.AppendLine("End As CntName, A.CancelInd  ");
            SQL.AppendLine("From TblBeaCukai A ");
            SQL.AppendLine("Inner Join TblPlHdr B On A.PLDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblSIHdr C On B.SIDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblSP D On C.SPDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer E On D.CtCode=E.CtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "PLDocNo", "CtName", "KodePabean", "Pabean",  
                        
                        //6-10
                        "NoPengajuan", "JnsEkspor", "KategEkspor", "CaraDagang", "CaraBayar",  
                        
                        //11-15
                        "Pengangkutan", "SaranaPengangkut", "NoPengangkut", "BenderaPengangkut", "TglEksporDt",

                        //16-20
                        "PetiKemas", "StatusPeti", "JmlPeti", "NoPeti", "BankDevisa", 

                        //21-25
                        "ValutaAsing", "NoPendaftaran", "PendaftaranDt", "NoBC", "BCDt",
                        
                        //26-30
                        "SubPos", "NPWP", "NmNPWP", "Alamat", "NoPokokPPJK",

                        //31-35
                        "PPJKDt",  "Pemeriksaan", "KantorPemeriksaan", "JnsKemasan", "JmlKemasan", 
                        
                        //36
                        "MerkKemasan", "CntName", "CancelInd"

                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);

                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtPLDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        TxtCtCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtKodePabean.EditValue = Sm.DrStr(dr, c[4]);
                        TxtPabean.EditValue = Sm.DrStr(dr, c[5]);
                        TxtNoPengajuan.EditValue = Sm.DrStr(dr,c[6]);

                        TxtJnsEkspor.EditValue = Sm.DrStr(dr, c[7]);
                        TxtKategEkspor.EditValue = Sm.DrStr(dr, c[8]);
                        TxtCaraDagang.EditValue = Sm.DrStr(dr, c[9]);
                        TxtCaraBayar.EditValue = Sm.DrStr(dr, c[10]);

                        TxtPengangkutan.EditValue = Sm.DrStr(dr, c[11]);
                        TxtSaranaPengangkut.EditValue = Sm.DrStr(dr, c[12]);
                        TxtNoPengangkut.EditValue = Sm.DrStr(dr, c[13]);
                        TxtBenderaPengangkut.EditValue = Sm.DrStr(dr, c[14]);
                        Sm.SetDte(DteTglEksporDt, Sm.DrStr(dr, c[15]));

                        TxtPetiKemas.EditValue = Sm.DrStr(dr, c[16]);
                        TxtStatusPeti.EditValue = Sm.DrStr(dr, c[17]);
                        TxtJmlPeti.EditValue = Sm.DrStr(dr, c[18]);
                        TxtNoPeti.EditValue = Sm.DrStr(dr, c[19]);
                        TxtBankDevisa.EditValue = Sm.DrStr(dr, c[20]);

                        TxtValutaAsing.EditValue = Sm.DrStr(dr, c[21]);
                        TxtNoPendaftaran.EditValue = Sm.DrStr(dr, c[22]);
                        Sm.SetDte(DtePendaftaranDt, Sm.DrStr(dr, c[23]));
                        TxtNoBC.EditValue = Sm.DrStr(dr,c[24]);
                        Sm.SetDte(DteBCDt, Sm.DrStr(dr, c[25]));

                        TxtSubPos.EditValue = Sm.DrStr(dr,c[26]);
                        TxtNPWP.EditValue = Sm.DrStr(dr, c[27]);
                        TxtNmNPWP.EditValue = Sm.DrStr(dr, c[28]);
                        MeeAlamat.EditValue = Sm.DrStr(dr, c[29]);
                        TxtNoPokokPPJK.EditValue = Sm.DrStr(dr, c[30]);

                        Sm.SetDte(DtePPJKDt, Sm.DrStr(dr, c[31]));
                        TxtPemeriksaan.EditValue = Sm.DrStr(dr, c[32]);
                        TxtKantorPemeriksaan.EditValue = Sm.DrStr(dr, c[33]);
                        TxtJnsKemasan.EditValue = Sm.DrStr(dr, c[34]);
                        TxtJmlKemasan.EditValue = Sm.DrStr(dr, c[35]);

                        TxtMerkKemasan.EditValue = Sm.DrStr(dr, c[36]);
                        TxtCntName.EditValue = Sm.DrStr(dr, c[37]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[38]) == "Y";
                    }, true
                );
        }

        internal void ShowDataDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.SODocNo, A.SODNo, D.AgtName, ");
            SQL.AppendLine("A.ItCode, E.ItCodeInternal, E.ItName, ");
            SQL.AppendLine("A.QtyPackagingUnit, C.PackagingUnitUomCode, A.QtyInventory, E.InventoryUomCode, E.ItGrpCode ");
            SQL.AppendLine("From TblPLDtl A ");
            SQL.AppendLine("Inner Join TblSOHdr B On A.SODocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblSODtl C On A.SODocNo=C.DocNo And A.SODNo=C.DNo ");
            SQL.AppendLine("Left Join TblAgent D On C.AgtCode=D.AgtCode ");
            SQL.AppendLine("Inner Join TblItem E On A.ItCode=E.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.SectionNo=@SectionNo ");
            SQL.AppendLine("Order By A.ItCode, A.DNo;");

            
            Sm.CmParam<String>(ref cm, "@SectionNo", Sm.GetValue("select SectionNo from tblbeacukai where Docno='"+TxtDocNo.Text+"'"));
            Sm.CmParam<String>(ref cm, "@DocNo", TxtPLDocNo.Text);


            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "SODocNo", "SODNo", "AgtName", "ItCode", "ItCodeInternal", 
                    
                    //6-10
                    "ItName", "QtyPackagingUnit", "PackagingUnitUomCode", "QtyInventory", "InventoryUomCode",

                    //11
                    "ItGrpCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Grd.Cells[Row, 12].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 11, 12, 13 });
            Sm.FocusGrd(Grd1, 0, 1);

        }

        #endregion

        #region Additional Method

        internal void ShowSOInfo()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

                SQL.AppendLine("Select A.DNo, A.SODocNo, A.SODNo, D.AgtName, ");
                SQL.AppendLine("A.ItCode, E.ItCodeInternal, E.ItName, ");
                SQL.AppendLine("A.QtyPackagingUnit, C.PackagingUnitUomCode, A.QtyInventory, E.InventoryUomCode, E.ItGrpCode ");
                SQL.AppendLine("From TblPLDtl A ");
                SQL.AppendLine("Inner Join TblSOHdr B On A.SODocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblSODtl C On A.SODocNo=C.DocNo And A.SODNo=C.DNo ");
                SQL.AppendLine("Left Join TblAgent D On C.AgtCode=D.AgtCode ");
                SQL.AppendLine("Inner Join TblItem E On A.ItCode=E.ItCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo And A.SectionNo=@SectionNo ");
                SQL.AppendLine("Order By A.ItCode, A.DNo;");

                Sm.CmParam<String>(ref cm, "@SectionNo", mSectionNo);
                Sm.CmParam<String>(ref cm, "@DocNo", TxtPLDocNo.Text);
         

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "SODocNo", "SODNo", "AgtName", "ItCode", "ItCodeInternal", 
                    
                    //6-10
                    "ItName", "QtyPackagingUnit", "PackagingUnitUomCode", "QtyInventory", "InventoryUomCode",

                    //11
                    "ItGrpCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Grd.Cells[Row, 12].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 11, 12, 13 });
            Sm.FocusGrd(Grd1, 0, 1);
      
        }

        #endregion

        #endregion

        #region Event

        private void BtnPLDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBeaCukaiDlg(this));
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        #endregion

        #region Report Class

        class Beacukai
        {
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string PLDocNo { get; set; }
            public string KodePabean { get; set; }
            public string Pabean { get; set; }
            public string NoPengajuan { get; set; }
            public string JnsEkspor { get; set; }
            public string KategEkspor { get; set; }
            public string CaraDagang { get; set; }
            public string CaraBayar { get; set; }
            public string NoPendaftaran { get; set; }
            public string PendaftaranDt { get; set; }
            public string NoBC { get; set; }
            public string BCDt { get; set; }
            public string SubPos { get; set; }
            public string BankDevisa { get; set; }
            public string ValutaAsing { get; set; }
            public string PetiKemas { get; set; }
            public string StatusPeti { get; set; }
            public string JmlPeti { get; set; }
            public string NoPeti { get; set; }
            public string NPWP { get; set; }
            public string NmNPWP { get; set; }
            public string Alamat { get; set; }
            public string NoPokokPPJK { get; set; }
            public string PPJKDt { get; set; }
            public string Pemeriksaan { get; set; }
            public string KantorPemeriksaan { get; set; }
            public string JnsKemasan { get; set; }
            public string JmlKemasan { get; set; }
            public string MerkKemasan { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string Pengangkutan { get; set; }
            public string SaranaPengangkut { get; set; }
            public string NoPengangkut { get; set; }
            public string BenderaPengangkut { get; set; }
            public string TglEksporDt { get; set; }
            public string InvoiceNo { get; set; }
            public string InvoiceDt { get; set; }
            public string CntCode { get; set; }
            public string CntName { get; set; }
            public string PortLoading { get; set; }
            public string SPPlaceDelivery { get; set; }  
            public string PortDischarge { get; set; }
            public string SpPlaceReceipt { get; set; }
            public string PrintBy { get; set; }
        }

        class Beacukai2
        {
            public string DocNo { get; set; }
            public string DTName { get; set; }
        }

        class Beacukai3
        {
            public int nomor { get; set; }
            public string DocNo { get; set; }
            public decimal Fob { get; set; }
            public string SectionNo { get; set; }
            public string ItName { get; set; }
            public decimal Freight { get; set; }
        }


        #endregion


    }
}
