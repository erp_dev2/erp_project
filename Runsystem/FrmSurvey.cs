﻿#region Update
/*
    01/08/2018 [WED] tambah tab baru Installment Result
    14/09/2018 [WED] tambah input DueDt, maks isian 28
    16/09/2018 [WED] save ke TblLoanSummary
    26/09/2018 [WED] OutstandingPaymentAmtMain & OutstandingPaymentAmtRate udah diinput di Survey; Amt VR nya ambil dari TotalAmt
    20/12/2018 [DITA] Menambahkan year (tab instulment result) menu Form Survey
    21/12/2018 [WED] Amount VR ambil dari Amount Loan
    08/01/2019 [MEY] Approval divalidasi berdasarkan amount di approval setting.
    22/02/2019 [WED] BUG Saat cancel juga harusnya meng update ProcessInd RequestLP
    07/10/2019 [WED/TWC] nomor voucher di generate berdasarkan parameter VoucherCodeFormatType
    27/02/2020 [WED/SIER] perhitungan perubahan rate berdasarkan parameter SurveyRateChangesFormat
    04/04/2022 [ICA/TWC] perhitungan bunga dengan anuitas berdasarkan parameter FormSurveyInterestType
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmSurvey : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = String.Empty;
        private string
            mSurveyDeptCode = string.Empty,
            mPaymentType = string.Empty,
            mBankAcCode = string.Empty,
            mBankCode = string.Empty,
            mVoucherDocType = "52",
            mVRAcType = "C",
            mGiroNo = string.Empty,
            mDueDt = string.Empty,
            mDebitTo = string.Empty,
            mSurveyStartYr = string.Empty,
            mVoucherCodeFormatType = string.Empty,
            mSurveyRateChangesFormat = string.Empty,
            mFormSurveyInterestType = string.Empty;
        internal FrmSurveyFind FrmFind;
        private decimal mInterestRate = 3;
        private bool mSaveState = false;

        #endregion

        #region Constructor

        public FrmSurvey(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmSurvey");
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();

                TcSurvey.SelectedTabPage = Tp2;
                SetLueMonth(ref LueMonth);
                Sl.SetLueYr(LueYr, mSurveyStartYr);
                SetGrd();

                TcSurvey.SelectedTabPage = Tp1;
                SetFormControl(mState.View);

                Sl.SetLueBCCode(ref LueBCCode);
                Sl.SetLueBSCode(ref LueBSCode);
                Sl.SetLueOption(ref LueProposal, "Survey1");
                Sl.SetLueOption(ref LueIDNumber, "Survey2");
                Sl.SetLueOption(ref LueSIUP, "Survey2");
                Sl.SetLueOption(ref LueLicence, "Survey2");
                Sl.SetLueOption(ref LueBussinessDomicile, "Survey3");
                Sl.SetLueOption(ref LuePosting, "Survey2");
                Sl.SetLueOption(ref LueFiles, "Survey2");
                Sl.SetLueOption(ref LueDevelopment, "Survey1");

                base.FrmLoad(sender, e);
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.ReadOnly = true;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Month", "Year", "Amount", "Interest"+Environment.NewLine+"Amount", "Total"+Environment.NewLine+"Amount",
                    //6
                    "Outstanding Amt"
                },
                new int[] 
                {
                    //0
                    10, 
                    //1-5
                    150, 60, 120, 120, 120,
                    //6
                    150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 4, 5 });
        }

        protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, MeeCancelReason, TxtStatus, TxtRQLPDocNo, DteEstimatedDt, MeeRemark, 
                        LueProposal, LueIDNumber, LueSIUP, LueLicence, LueBussinessDomicile, TxtWorkExp, TxtManPower, 
                        TxtOmzet, TxtAsset, TxtInstallment, TxtAssurance, LuePosting, LueFiles, LueDevelopment, 
                        TxtNoOfLoan, TxtAmt, LueMonth, LueYr, TxtDueDt
                    }, true);
                    TxtDocNo.Focus();
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnRQLPDocNo.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, DteEstimatedDt, MeeRemark, 
                        LueProposal, LueIDNumber, LueSIUP, LueLicence, LueBussinessDomicile, TxtWorkExp, TxtManPower, 
                        TxtOmzet, TxtAsset, TxtInstallment, TxtAssurance, LuePosting, LueFiles, LueDevelopment, 
                        TxtNoOfLoan, TxtAmt, LueMonth, LueYr, TxtDueDt
                    }, false);
                    DteDocDt.Focus();
                    BtnRQLPDocNo.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtStatus, TxtRQLPDocNo, DteEstimatedDt, MeeRemark, 
                LueProposal, LueIDNumber, LueSIUP, LueLicence, LueBussinessDomicile,  
                LuePosting, LueFiles, LueDevelopment, 
                TxtPnName, MeeAddress, TxtCompanyName, MeeCompanyAddress, DteBirthDt, 
                TxtIdentityNo, TxtPhone, TxtMobile, LueBCCode, LueBSCode, TxtVoucherRequestDocNo, TxtVoucherDocNo,
                TxtDueDt
            });
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueMonth, LueYr });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtWorkExp, TxtOmzet, TxtAsset, TxtAmt, TxtInterestRateAmt, TxtTotalAmt, TxtInterestRate }, 1);
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtInstallment, TxtNoOfLoan, TxtManPower }, 11);
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 6 });
            ChkCancelInd.Checked = false;
            mSaveState = false;
        }

        #endregion    

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSurveyFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
                TxtInterestRate.EditValue = Sm.FormatNum(mInterestRate, 0);
                TxtDueDt.Text = "01";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            mSaveState = true;
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty, mMenuCode) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid())
            {
                mSaveState = false;
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            var l = new List<LoanSummary>();
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Survey", "TblSurvey");
            string VRDocNo = string.Empty;
            if (mVoucherCodeFormatType == "2")
                VRDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr");
            else 
                VRDocNo = Sm.GenerateDocNoVoucher(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mVRAcType, mBankAcCode);

            ProcessLoanSummary(DocNo, ref l);
            ProcessAmt(ref l);

            var cml = new List<MySqlCommand>();
          
            cml.Add(SaveSurvey(DocNo, VRDocNo));
            if (l.Count > 0)
            {
                for (int i = 0; i < l.Count; i++)
                {
                    cml.Add(SaveSurveyDtl(ref l, i));
                    cml.Add(SaveLoanSummary(ref l, i));
                }
            }

            cml.Add(SaveVoucherRequest(VRDocNo, DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtRQLPDocNo, "Request#", false) ||
                Sm.IsDteEmpty(DteEstimatedDt, "Estimated Date") ||
                Sm.IsLueEmpty(LueProposal, "Proposal") ||
                Sm.IsLueEmpty(LueIDNumber, "ID Number") ||
                Sm.IsLueEmpty(LueSIUP, "SIUP") ||
                Sm.IsLueEmpty(LueLicence, "Licence / Deed") ||
                Sm.IsLueEmpty(LueBussinessDomicile, "Bussiness Domicile") ||
                Sm.IsTxtEmpty(TxtWorkExp, "Work Experience", true) ||
                Sm.IsTxtEmpty(TxtManPower, "Manpower", true) ||
                Sm.IsTxtEmpty(TxtOmzet, "Monthly Omzet", true) ||
                Sm.IsTxtEmpty(TxtAsset, "Asset", true) ||
                Sm.IsTxtEmpty(TxtInstallment, "Installment Payment Capability", true) ||
                Sm.IsTxtEmpty(TxtAssurance, "Assurance Capability", false) ||
                Sm.IsLueEmpty(LuePosting, "Posting / Book keeping") ||
                Sm.IsLueEmpty(LueFiles, "Other Files") ||
                Sm.IsLueEmpty(LueDevelopment, "Development Probability") ||
                Sm.IsTxtEmpty(TxtNoOfLoan, "No. of Loan", true) ||
                Sm.IsTxtEmpty(TxtAmt, "Loan Amount", true) ||
                Sm.IsLueEmpty(LueMonth, "Start Month") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsTxtEmpty(TxtDueDt, "Due Date", false) ||
                IsDueDtNotValid();// ||
                //IsGrdEmpty() ||
                //IsGrdValueNotValid() ||
                //IsAmountNotBalance() ||
                //IsStartMonthNotBalance() ||
                //IsStartYearNotBalance() ||
                //IsStartYear() ||
                //IsAmountNotValid();
        }

        private bool IsDueDtNotValid()
        {
            if (Decimal.Parse(TxtDueDt.Text) <= 0 || Decimal.Parse(TxtDueDt.Text) > 28)
            {
                Sm.StdMsg(mMsgType.Warning, "Due Date only allowed between 01 and 28.");
                TcSurvey.SelectedTabPage = Tp2;
                TxtDueDt.Focus();
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to save at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Month is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Year is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 3, true, "Amount is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsStartYearNotBalance()
        {
            if (Sm.GetLue(LueYr) != Sm.GetGrdStr(Grd1, 0, 2))
            {
                Sm.StdMsg(mMsgType.Warning, "Year is not balance");
                return true;
            }
            return false;
        }

        private bool IsStartYear()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                int aa = int.Parse(Sm.GetLue(LueYr));
                int bb = (Sm.GetGrdInt(Grd1, Row, 2));

                if (aa > bb)
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid Year Information");
                    return true;
                }
            }

            return false;
        }

        private bool IsAmountNotBalance()
        {
            decimal amt = decimal.Parse(TxtTotalAmt.Text);
            decimal total = 0m;

            for (int i = 0; i < Grd1.Rows.Count; i++)
                if (Sm.GetGrdStr(Grd1, i, 0).Length > 0)
                    total += Sm.GetGrdDec(Grd1, i, 5);

            if (Decimal.Round(amt, 2) != Decimal.Round(total, 2))
            {
                Sm.StdMsg(mMsgType.Warning, "Total amount is not balance.");
                return true;
            }
            return false;
        }

        private bool IsAmountNotValid()
        {
            decimal Amount = 0m;
            if (TxtAmt.Text.Length != 0) Amount = decimal.Parse(TxtAmt.Text);
            if (Amount < 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid Amount, Please check again.");
                return true;
            }
            return false;
        }

        private bool IsStartMonthNotBalance()
        {
            if (Sm.GetLue(LueMonth) != Sm.GetGrdStr(Grd1, 0, 0))
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid Month");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveSurvey(string DocNo, string VRDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSurvey ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, RQLPDocNo, EstimatedDt, VoucherRequestDocNo, Remark, ");
            SQL.AppendLine("Proposal, IDNumber, SIUP, Licence, BussinessDomicile, WorkExp, ManPower, ");
            SQL.AppendLine("Omzet, Asset, Installment, Assurance, Posting, Files, Development, NoOfLoan, Amt, ");
            SQL.AppendLine("InterestRate, InterestRateAmt, TotalAmt, StartMth, Yr, DueDt, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'N', 'O', @RQLPDocNo, @EstimatedDt, @VoucherRequestDocNo, @Remark, ");
            SQL.AppendLine("@Proposal, @IDNumber, @SIUP, @Licence, @BussinessDomicile, @WorkExp, @ManPower, ");
            SQL.AppendLine("@Omzet, @Asset, @Installment, @Assurance, @Posting, @Files, @Development, @NoOfLoan, @Amt, ");
            SQL.AppendLine("@InterestRate, @InterestRateAmt, @TotalAmt, @StartMth, @Yr, @DueDt, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting ");
                SQL.AppendLine("Where DocType='Survey' ");
                SQL.AppendLine("And (StartAmt=0 ");
                SQL.AppendLine("Or StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select Amt From TblSurvey Where DocNo=@DocNo ");
                SQL.AppendLine("), 0.00)); ");
            }

            SQL.AppendLine("Update TblSurvey Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='Survey' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@RQLPDocNo", TxtRQLPDocNo.Text);
            Sm.CmParamDt(ref cm, "@EstimatedDt", Sm.GetDte(DteEstimatedDt));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VRDocNo);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Proposal", Sm.GetLue(LueProposal));
            Sm.CmParam<String>(ref cm, "@IDNumber", Sm.GetLue(LueIDNumber));
            Sm.CmParam<String>(ref cm, "@SIUP", Sm.GetLue(LueSIUP));
            Sm.CmParam<String>(ref cm, "@Licence", Sm.GetLue(LueLicence));
            Sm.CmParam<String>(ref cm, "@BussinessDomicile", Sm.GetLue(LueBussinessDomicile));
            Sm.CmParam<Decimal>(ref cm, "@WorkExp", Decimal.Parse(TxtWorkExp.Text));
            Sm.CmParam<Decimal>(ref cm, "@ManPower", Decimal.Parse(TxtManPower.Text));
            Sm.CmParam<Decimal>(ref cm, "@Omzet", Decimal.Parse(TxtOmzet.Text));
            Sm.CmParam<Decimal>(ref cm, "@Asset", Decimal.Parse(TxtAsset.Text));
            Sm.CmParam<Decimal>(ref cm, "@Installment", Decimal.Parse(TxtInstallment.Text));
            Sm.CmParam<String>(ref cm, "@Assurance", TxtAssurance.Text);
            Sm.CmParam<String>(ref cm, "@Posting", Sm.GetLue(LuePosting));
            Sm.CmParam<String>(ref cm, "@Files", Sm.GetLue(LueFiles));
            Sm.CmParam<String>(ref cm, "@Development", Sm.GetLue(LueDevelopment));
            Sm.CmParam<Decimal>(ref cm, "@NoOfLoan", Decimal.Parse(TxtNoOfLoan.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@InterestRate", Decimal.Parse(TxtInterestRate.Text));
            Sm.CmParam<Decimal>(ref cm, "@InterestRateAmt", Decimal.Parse(TxtInterestRateAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", Decimal.Parse(TxtTotalAmt.Text));
            Sm.CmParam<String>(ref cm, "@StartMth", Sm.GetLue(LueMonth));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@DueDt", TxtDueDt.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSurveyDtl(ref List<LoanSummary> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSurveyDtl(DocNo, DNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DNo, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].SurveyDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", l[Row].SurveyDNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveLoanSummary(ref List<LoanSummary> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLoanSummary(SurveyDocNo, SurveyDNo, Mth, Yr, AmtMain, AmtRate, Amt, OutstandingLoanAmt, PaymentInd, OutstandingPaymentAmtMain, OutstandingPaymentAmtRate, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DNo, @Mth, @Yr, @AmtMain, @AmtRate, @Amt, @OutstandingLoanAmt, 'O', @AmtMain, @AmtRate, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].SurveyDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", l[Row].SurveyDNo);
            Sm.CmParam<String>(ref cm, "@Mth", l[Row].Mth);
            Sm.CmParam<String>(ref cm, "@Yr", l[Row].Yr);
            Sm.CmParam<Decimal>(ref cm, "@AmtMain", l[Row].AmtMain);
            Sm.CmParam<Decimal>(ref cm, "@AmtRate", l[Row].AmtRate);
            Sm.CmParam<Decimal>(ref cm, "@Amt", l[Row].Amt);
            Sm.CmParam<Decimal>(ref cm, "@OutstandingLoanAmt", l[Row].OutstandingLoanAmt);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequest(string DocNo, string SurveyDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, LocalDocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, GiroNo, ");
            SQL.AppendLine("BankCode, OpeningDt, DueDt, PIC, CurCode, Amt, CurCode2, ExcRate, PaymentUser, ");
            SQL.AppendLine("Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, null, @DocDt, 'N', 'O', 'N', @DeptCode, @DocType, null, ");
            SQL.AppendLine("@AcType, @BankAcCode, null, null, @PaymentType, @GiroNo, ");
            SQL.AppendLine("@BankCode, null, @DueDt, @PIC, @CurCode, @Amt, null, 1, null,");
            SQL.AppendLine("@Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, '001', @Description, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='Survey' ");
            SQL.AppendLine("    And DocNo In (Select T.DocNo From TblSurvey T Where T.VoucherRequestDocNo = @DocNo) ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", mSurveyDeptCode);
            Sm.CmParam<String>(ref cm, "@DocType", mVoucherDocType);
            Sm.CmParam<String>(ref cm, "@AcType", mVRAcType);
            Sm.CmParam<String>(ref cm, "@BankAcCode", mBankAcCode);
            Sm.CmParam<String>(ref cm, "@PaymentType", mPaymentType);
            Sm.CmParam<String>(ref cm, "@GiroNo", mGiroNo);
            Sm.CmParam<String>(ref cm, "@DueDt", mDueDt);
            Sm.CmParam<String>(ref cm, "@BankCode", mBankCode);
            Sm.CmParam<String>(ref cm, "@PIC", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CurCode", "IDR");
            //Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtTotalAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Description", string.Concat("Survey #", SurveyDocNo));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditSurvey());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToVoucher();
        }

        private bool IsDataAlreadyProcessedToVoucher()
        {
            if(Sm.IsDataExist("Select DocNo From TblVoucherHdr Where VoucherRequestDocNo = @Param And CancelInd = 'N' Limit 1; ", TxtVoucherRequestDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already processed to Voucher.");
                return true;
            }

            return false;
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this survey.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblSurvey " +
                "Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already cancelled.");
        }

        private MySqlCommand EditSurvey()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSurvey Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd = 'N'; ");

            SQL.AppendLine("Update TblRequestLP Set ");
            SQL.AppendLine("    ProcessInd = 'O', LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo In (Select RQLPDocNo From TblSurvey Where DocNo = @DocNo) ");
            SQL.AppendLine("And ProcessInd = 'F'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VRDocNo And CancelInd='N' And Status In ('O', 'A'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VRDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                mSaveState = true;
                ShowSurvey(DocNo);
                TcSurvey.SelectedTabPage = Tp2;
                ShowSurveyDtl(DocNo);
                TcSurvey.SelectedTabPage = Tp1;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSurvey(string DocNo) 
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd,  ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("A.RQLPDocNo, A.EstimatedDt, A.VoucherRequestDocNo, D.DocNo As VoucherDocNo, A.Remark, C.PnName, C.Address,  ");
            SQL.AppendLine("C.CompanyName, C.CompanyAddress, C.BirthDt, C.IdentityNo, C.Phone, C.Mobile, C.BCCode, C.BSCode, ");
            SQL.AppendLine("A.Proposal, A.IDNumber, A.SIUP, A.Licence, A.BussinessDomicile, A.WorkExp, A.ManPower, A.Omzet, A.Asset, ");
            SQL.AppendLine("A.Installment, A.Assurance, A.Posting, A.Files, A.Development, A.NoOfLoan, A.Amt, ");
            SQL.AppendLine("A.InterestRate, A.InterestRateAmt, A.TotalAmt, A.StartMth, A.Yr, A.DueDt ");
            SQL.AppendLine("From TblSurvey A ");
            SQL.AppendLine("Inner Join TblRequestLP B On A.RQLPDocNo = B.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblPartner C On B.PnCode = C.PnCode ");
            SQL.AppendLine("Left Join TblVoucherHdr D On A.VoucherRequestDocNo = D.VoucherRequestDocNo; ");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "StatusDesc", "RQLPDocNo",  

                    //6-10
                    "EstimatedDt", "VoucherRequestDocNo", "VoucherDocNo", "Remark", "PnName",  

                    //11-15
                    "Address", "CompanyName", "CompanyAddress", "BirthDt", "IdentityNo", 
                     
                    //16-20
                    "Phone",  "Mobile", "BCCode", "BSCode", "Proposal", 

                    //21-25
                    "IDNumber", "SIUP", "Licence", "BussinessDomicile", "WorkExp",

                    //26-30
                    "ManPower", "Omzet", "Asset", "Installment", "Assurance", 

                    //31-35
                    "Posting", "Files", "Development", "NoOfLoan", "Amt",

                    //36-40
                    "InterestRate", "InterestRateAmt", "TotalAmt", "StartMth", "Yr",

                    //41
                    "DueDt"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                    TxtRQLPDocNo.EditValue = Sm.DrStr(dr, c[5]);
                    Sm.SetDte(DteEstimatedDt, Sm.DrStr(dr, c[6]));
                    TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[7]);
                    TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[8]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                    TxtPnName.EditValue = Sm.DrStr(dr, c[10]);
                    MeeAddress.EditValue = Sm.DrStr(dr, c[11]);
                    TxtCompanyName.EditValue = Sm.DrStr(dr, c[12]);
                    MeeCompanyAddress.EditValue = Sm.DrStr(dr, c[13]);
                    Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[14]));
                    TxtIdentityNo.EditValue = Sm.DrStr(dr, c[15]);
                    TxtPhone.EditValue = Sm.DrStr(dr, c[16]);
                    TxtMobile.EditValue = Sm.DrStr(dr, c[17]);
                    Sm.SetLue(LueBCCode, Sm.DrStr(dr, c[18]));
                    Sm.SetLue(LueBSCode, Sm.DrStr(dr, c[19]));
                    Sm.SetLue(LueProposal, Sm.DrStr(dr, c[20]));
                    Sm.SetLue(LueIDNumber, Sm.DrStr(dr, c[21]));
                    Sm.SetLue(LueSIUP, Sm.DrStr(dr, c[22]));
                    Sm.SetLue(LueLicence, Sm.DrStr(dr, c[23]));
                    Sm.SetLue(LueBussinessDomicile, Sm.DrStr(dr, c[24]));
                    TxtWorkExp.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[25]), 0);
                    TxtManPower.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[26]), 11);
                    TxtOmzet.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[27]), 0);
                    TxtAsset.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[28]), 0);
                    TxtInstallment.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[29]), 11);
                    TxtAssurance.EditValue = Sm.DrStr(dr, c[30]);
                    Sm.SetLue(LuePosting, Sm.DrStr(dr, c[31]));
                    Sm.SetLue(LueFiles, Sm.DrStr(dr, c[32]));
                    Sm.SetLue(LueDevelopment, Sm.DrStr(dr, c[33]));
                    TxtNoOfLoan.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[34]), 11);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[35]), 0);
                    TxtInterestRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[36]), 0);
                    TxtInterestRateAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[37]), 0);
                    TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[38]), 0);
                    Sm.SetLue(LueMonth, Sm.DrStr(dr, c[39]));
                    Sm.SetLue(LueYr, Sm.DrStr(dr, c[40]));
                    TxtDueDt.EditValue = Sm.DrStr(dr, c[41]);
                }, true
            );
        }

        private void ShowSurveyDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DNo, MonthName(Str_To_Date(T2.Mth , '%m')) As Mth, ");
            SQL.AppendLine("T2.Yr, T2.AmtMain, T2.AmtRate, T2.Amt, T2.OutstandingLoanAmt ");
            SQL.AppendLine("From TblSurveyDtl T1 ");
            SQL.AppendLine("Inner Join TblLoanSummary T2 On T1.DocNo = T2.SurveyDocNo And T1.DNo = T2.SurveyDNo ");
            SQL.AppendLine("Where T1.DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "DNo", 
                        "Mth", "Yr", "AmtMain", "AmtRate", "Amt", 
                        "OutstandingLoanAmt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Methods

        private void ProcessAmt(ref List<LoanSummary> l)
        {
            decimal mInterestAmt = 0m, mTotalAmt = 0m;
            //if (TxtAmt.Text.Length > 0) mAmt = Decimal.Parse(TxtAmt.Text);

            //if(mAmt > 0)
            //{
            //    mInterestAmt = mAmt * (mInterestRate / 100);
            //    mTotalAmt = mAmt + mInterestAmt;
            //}

            if (l.Count > 0)
            {
                for (int Row = 0; Row < l.Count; Row++)
                {
                    mInterestAmt += l[Row].AmtRate;
                    mTotalAmt += l[Row].Amt;
                }
            }

            TxtInterestRateAmt.EditValue = Sm.FormatNum(mInterestAmt, 0);
            TxtTotalAmt.EditValue = Sm.FormatNum(mTotalAmt, 0);
        }

        //private void ProcessGrd(ref List<LoanSummary> l)
        //{
        //    if (l.Count > 0)
        //    {
        //        string[] mMonths = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
        //        string[] mMonths2 = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

        //        for (int i = 0; i < l.Count; i++)
        //        {
        //            Grd1.Rows.Add();
        //            Grd1.Cells[i, 0].Value = l[i].Mth;
        //            Grd1.Cells[i, 1].Value = mMonths2[Int32.Parse(l[i].Mth) - 1];
        //            Grd1.Cells[i, 2].Value = l[i].Yr;
        //            Grd1.Cells[i, 3].Value = l[i].AmtMain;
        //            Grd1.Cells[i, 4].Value = l[i].AmtRate;
        //            Grd1.Cells[i, 5].Value = l[i].Amt;
        //        }
        //        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6 });
        //    }
        //}

        private void ProcessLoanSummary(string DocNo, ref List<LoanSummary> l)
        {            
            int mInstallment = 0, mStartMth = 0, mStartYr = 0;
            decimal
                mLoanAmt = 0m,
                mTotalAmt = 0m,
                mDNo = 1,
                mRate = 0m,
                mMain = Decimal.Parse(TxtAmt.Text) / Decimal.Parse(TxtInstallment.Text),
                mOutstandingLoanAmt = Decimal.Parse(TxtAmt.Text),
                mCurrLoanAmt = Decimal.Parse(TxtAmt.Text);

            if (Decimal.Parse(TxtInstallment.Text) > 0) mInstallment = Int32.Parse(TxtInstallment.Text);
            if (Sm.GetLue(LueMonth).Trim().Length > 0 && LueMonth.Text != "<Refresh>") mStartMth = Int32.Parse(Sm.GetLue(LueMonth));
            if (Sm.GetLue(LueYr).Length > 0) mStartYr = Int32.Parse(Sm.GetLue(LueYr));
            if (TxtAmt.Text.Length > 0) mLoanAmt = Decimal.Parse(TxtAmt.Text);

            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 6 });

            if (mInstallment > 0 && mLoanAmt > 0 && mStartMth > 0 && mStartYr > 0)
            {
                for (int i = 0; i < mInstallment; i++)
                {
                    if (mStartMth == 13)
                    {
                        mStartMth = 1;
                        mStartYr += 1;
                    }

                    if (mFormSurveyInterestType == "2")
                    {
                        mCurrLoanAmt = mOutstandingLoanAmt;
                        if (mOutstandingLoanAmt < 1)
                        {
                            mRate = 0;
                            mTotalAmt = 0;
                        }
                        else
                        {
                            mRate = ((mInterestRate / 12) / 100) * mCurrLoanAmt;
                            mTotalAmt = (mLoanAmt * ((mInterestRate / 12) / 100)) / (1 - ((decimal)Math.Pow((1 /(1 + ((((double)mInterestRate) / 12) / 100))), mInstallment)));
                        }

                        mMain = mTotalAmt - mRate;
                        mOutstandingLoanAmt -= mMain;
                    }
                    else
                    {

                        if (mSurveyRateChangesFormat == "1")
                        {
                            if (mStartMth == 1)
                                mCurrLoanAmt = mOutstandingLoanAmt;
                        }
                        else
                        {
                            if (Sm.Right(string.Concat("00", mStartMth.ToString()), 2) == Sm.GetLue(LueMonth))
                                mCurrLoanAmt = mOutstandingLoanAmt;
                        }

                        mRate = ((mInterestRate / 12) / 100) * mCurrLoanAmt;
                        mOutstandingLoanAmt -= mMain;
                        mTotalAmt = mMain + mRate;
                    }

                    l.Add(new LoanSummary()
                    {
                        SurveyDocNo = DocNo,
                        SurveyDNo = Sm.Right(string.Concat("000", mDNo.ToString()), 3),
                        Mth = Sm.Right(string.Concat("00", mStartMth.ToString()), 2),
                        Yr = mStartYr.ToString(),
                        AmtMain = mMain,
                        AmtRate = mRate,
                        Amt = mTotalAmt,
                        OutstandingLoanAmt = mOutstandingLoanAmt
                    });

                    mStartMth += 1;
                    mDNo += 1;
                }
            }
        }

        private static void SetLueMonth(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "select '01' As Col1, 'January' As Col2 union All " +
                "select '02','February' Union All " +
                "select '03','March' Union All " +
                "select '04','April' Union All " +
                "select '05','May' Union All " +
                "select '06','June' Union All " +
                "select '07','July' Union All " +
                "select '08','August' Union All " +
                "select '09','September' Union All " +
                "select '10','October' Union All " +
                "select '11','November' Union All " +
                "select '12','December'",
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            mSurveyDeptCode = Sm.GetParameter("SurveyDeptCode");
            mInterestRate = Sm.GetParameterDec("InterestRate");
            mSurveyStartYr = Sm.GetParameter("SurveyStartYr");
            mVoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType");
            mSurveyRateChangesFormat = Sm.GetParameter("SurveyRateChangesFormat");
            mFormSurveyInterestType = Sm.GetParameter("FormSurveyInterestType");

            if (mSurveyRateChangesFormat.Length == 0) mSurveyRateChangesFormat = "1";
        }

        internal void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtRQLPDocNo, DteEstimatedDt, 
                LueProposal, LueIDNumber, LueSIUP, LueLicence, LueBussinessDomicile,  
                LuePosting, LueFiles, LueDevelopment, 
                TxtPnName, MeeAddress, TxtCompanyName, MeeCompanyAddress, DteBirthDt, 
                TxtIdentityNo, TxtPhone, TxtMobile, LueBCCode, LueBSCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtWorkExp, TxtManPower, TxtOmzet, TxtAsset, TxtInstallment, TxtAssurance, TxtNoOfLoan, TxtAmt }, 1);
        }

        internal void ShowRQLPData(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.PnName, B.Address, B.CompanyName, B.CompanyAddress, B.BirthDt, B.IdentityNo, ");
            SQL.AppendLine("B.Phone, B.Mobile, B.BCCode, B.BSCode, B.ManPower, A.Omzet, B.Asset, A.Assurance, A.Amt ");
            SQL.AppendLine("From TblRequestLP A ");
            SQL.AppendLine("Inner Join TblPartner B On A.PnCode = B.PnCode And A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo",
                    
                    //1-5
                    "PnName", "Address", "CompanyName", "CompanyAddress", "BirthDt", 
                    
                    //6-10
                    "IdentityNo", "Phone", "Mobile", "BCCode", "BSCode", 

                    //11-15
                    "ManPower", "Omzet", "Asset", "Assurance", "Amt"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtRQLPDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtPnName.EditValue = Sm.DrStr(dr, c[1]);
                    MeeAddress.EditValue = Sm.DrStr(dr, c[2]);
                    TxtCompanyName.EditValue = Sm.DrStr(dr, c[3]);
                    MeeCompanyAddress.EditValue = Sm.DrStr(dr, c[4]);
                    Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[5]));
                    TxtIdentityNo.EditValue = Sm.DrStr(dr, c[6]);
                    TxtPhone.EditValue = Sm.DrStr(dr, c[7]);
                    TxtMobile.EditValue = Sm.DrStr(dr, c[8]);
                    Sm.SetLue(LueBCCode, Sm.DrStr(dr, c[9]));
                    Sm.SetLue(LueBSCode, Sm.DrStr(dr, c[10]));
                    TxtManPower.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 11);
                    TxtOmzet.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    TxtAsset.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                    TxtAssurance.EditValue = Sm.DrStr(dr, c[14]);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[15]), 0);
                }, true
            );
        }

        private bool IsNeedApproval()
        {
            return Sm.IsDataExist("Select Doctype From TblDocApprovalSetting Where Doctype = 'Survey' Limit 1; ");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueProposal_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueProposal, new Sm.RefreshLue2(Sl.SetLueOption), "Survey1");
        }

        private void LueIDNumber_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueIDNumber, new Sm.RefreshLue2(Sl.SetLueOption), "Survey2");
        }

        private void LueSIUP_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSIUP, new Sm.RefreshLue2(Sl.SetLueOption), "Survey2");
        }

        private void LueLicence_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueLicence, new Sm.RefreshLue2(Sl.SetLueOption), "Survey2");
        }

        private void LueBussinessDomicile_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBussinessDomicile, new Sm.RefreshLue2(Sl.SetLueOption), "Survey3");
        }

        private void TxtWorkExp_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtWorkExp, 0);
        }

        private void TxtManPower_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtManPower, 11);
        }

        private void TxtOmzet_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtOmzet, 0);
        }

        private void TxtAsset_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAsset, 0);
        }

        private void TxtInstallment_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {                
                Sm.FormatNumTxt(TxtInstallment, 11);
                //ProcessAmt();
                //ProcessGrd();                
            }
        }

        private void TxtAssurance_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtAssurance);
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LuePosting_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePosting, new Sm.RefreshLue2(Sl.SetLueOption), "Survey2");
        }

        private void LueFiles_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueFiles, new Sm.RefreshLue2(Sl.SetLueOption), "Survey2");
        }

        private void LueDevelopment_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDevelopment, new Sm.RefreshLue2(Sl.SetLueOption), "Survey1");
        }

        private void TxtNoOfLoan_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtNoOfLoan, 11);
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {                
                Sm.FormatNumTxt(TxtAmt, 0);
                //ProcessAmt();
                //ProcessGrd();                
            }
        }

        private void TxtInterestRate_Validated(object sender, EventArgs e)
        {
            if(BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtInterestRate, 0);
                //ProcessAmt();
                //ProcessGrd();
            }
        }

        private void LueMonth_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueMonth, new Sm.RefreshLue1(SetLueMonth));
                //ProcessAmt();
                //ProcessGrd();
        }
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //ProcessAmt();
                //ProcessGrd();
            }
        }

        private void TxtDueDt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                string mDueDt = "01";
                if (TxtDueDt.Text.Length != 0) mDueDt = Sm.Right(string.Concat("00", TxtDueDt.Text), 2);
                TxtDueDt.Text = mDueDt;
            }
        }

        #endregion

        #region Button Click

        private void BtnRQLPDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmSurveyDlg(this));
        }

        private void BtnRQLPDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtRQLPDocNo, "Request#", false))
            {
                var f = new FrmRequestLP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtRQLPDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "VR#", false))
            {
                var f = new FrmVoucherRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Class

        private class LoanSummary
        {
            public string SurveyDocNo { get; set; }
            public string SurveyDNo { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public decimal AmtMain { get; set; }
            public decimal AmtRate { get; set; }
            public decimal Amt { get; set; }
            public decimal OutstandingLoanAmt { get; set; }
        }

        #endregion

    }
}
