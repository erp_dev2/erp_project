﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RuniProbe.GlobalVar;
using Sm = RuniProbe.StdMtd;
using Sl = RuniProbe.SetLue;

#endregion

namespace RuniProbe
{
    public partial class FrmRptDeduction2 : RuniProbe.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmRptDeduction2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetGrd();
                SetSQL();
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSectionCode(ref LueSection);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");

        }

        override protected void SetSQL()
        {

            
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 42;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5      
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Year",
                        "Month",
                        //6-10
                        "Payrun",
                        "Site",
                        "Department",
                        "Position",
                        "Section Code",
                        //11-15
                        "Section",
                        "BPJS TK",
                        "BPJS JP",
                        "BPJS KES",
                        "PPDP",
                        //16-20
                        "SS DPLK",
                        "PPTF",
                        "THP",
                        "DE001",
                        "IUR. KOPSI",
                        //21-25
                        "DE004",
                        "Pinj. KOPSI",
                        "DE008",
                        "TOKO KOPSI",
                        "DE002",
                        //26-30
                        "IUR. KOPEN",
                        "DE005",
                        "Pinj. KOPEN",
                        "DE007",
                        "TOKO KOPEN",
                        //31-35
                        "DE006",
                        "Pinj. KOP."+Environment.NewLine+"WANITA PATRA",
                        "DE011",
                        "Ekses"+Environment.NewLine+"Claim",
                        "DE003",
                        //36-40
                        "Zakat",
                        "DE009",
                        "Shodaqoh",
                        "Total Deduction",
                        "PPNI",
                        //41
                        "Net Income",
                   },
                     new int[] 
                    {
                        //0
                        40,
                        //1-5
                        100, 220, 100, 50, 70,                        
                        //6-10
                        100, 170, 170, 220, 100,  
                        //11-15
                        180, 120, 120, 120, 120,  
                        //16-20
                        120,50, 120, 50, 120, 
                        //21-25
                        50, 120, 50, 120, 50,  
                        //26-30
                        120,50, 120, 50, 120,  
                        //31-35
                        50,120, 50, 120, 50,  
                        //36-40
                        120, 50, 120, 120, 50, 
                        //41
                        120 
                   }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,  34, 35, 36, 37, 38, 39, 40, 41 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 40 }, false);
            Grd1.Cols[17].Move(12);
            Grd1.Cols[18].Move(13);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;
            try
            {
                Sm.ClearGrd(Grd1, false);
                Process3();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion


        #region List

        private void Process1(ref List<Employee> lEmp)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, A.SiteCode, C.SiteName,  ");
            SQL.AppendLine("A.DeptCode, B.DeptName, D.posName, A.SectionCode, E.SectionName,  ");
            SQL.AppendLine("F.SSEmployeeEmployment, F.SSEmployeePension, F.SSEmployeeHealth, F.SSEeDPLK, F.PLAmt, F.payrunCode ");
            SQL.AppendLine("From TblEmployee A  ");
            SQL.AppendLine("Inner Join tblDepartment B On A.DeptCode = B.DeptCode  ");
            SQL.AppendLine("Inner Join TblSite C On A.SiteCode = C.SiteCode  ");
            SQL.AppendLine("left join TblPosition D on A.posCode = D.PosCode  ");
            SQL.AppendLine("left join Tblsection E On A.SectionCode = E.SectionCode  ");
            SQL.AppendLine("left join ( ");
            SQL.AppendLine("    Select A.PayrunCode, A.EmpCode, A.SSEmployeeEmployment, A.SSEmployeePension, A.SSEmployeeHealth, A.SSEeDPLK, A.PLAmt   ");
            SQL.AppendLine("    from tblpayrollprocess1 A ");
            SQL.AppendLine("    Inner Join Tblpayrun B On A.PayrunCode = B.payrunCode ");
            SQL.AppendLine("    Where left(B.startDt, 6) = concat(@Yr, @Mth) ");
            SQL.AppendLine(")F On A.EmpCode = F.EmpCode ");
            SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate))  ");
            if (Sm.GetLue(LueSiteCode).Length > 0)
            {
                SQL.AppendLine("And A.SiteCode = @SiteCode ");
            }
            if (Sm.GetLue(LueDeptCode).Length > 0)
            {
                SQL.AppendLine("And A.DeptCode = @DeptCode ");
            }
            if (Sm.GetLue(LueSection).Length > 0)
            {
                SQL.AppendLine("And A.SectionCode = @SectionCode ");
            }
            

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                string Filter = " ";

                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@SectionCode", Sm.GetLue(LueSection));
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpCodeOld", "A.EmpName" });

                cm.CommandText = SQL.ToString() + Filter + " Order By A.EmpCode;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", 
                    "EmpName", "EmpCodeOld", "SiteCode", "SiteName", "DeptCode", 
                    "DeptName", "PosName", "SectionCode", "SectionName", "SSEmployeeEmployment",  
                    "SSEmployeePension", "SSEmployeeHealth", "SSEeDPLK",  "PLAmt", "PayrunCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEmp.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            EmpName = Sm.DrStr(dr, c[1]),
                            EmpCodeOld = Sm.DrStr(dr, c[2]),
                            SiteCode = Sm.DrStr(dr, c[3]),
                            SiteName = Sm.DrStr(dr, c[4]),
                            DeptCode = Sm.DrStr(dr, c[5]),
                            DeptName = Sm.DrStr(dr, c[6]),
                            PosName = Sm.DrStr(dr, c[7]),
                            SectionCode = Sm.DrStr(dr, c[8]),
                            SectionName = Sm.DrStr(dr, c[9]),
                            SSTnk = Sm.DrDec(dr, c[10]),
                            SSPens = Sm.DrDec(dr, c[11]),
                            SSKes = Sm.DrDec(dr, c[12]),
                            SSDplk = Sm.DrDec(dr, c[13]),
                            PayrunCode = Sm.DrStr(dr, c[15]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<DataDeduction> lDataDed)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From (   ");
            SQL.AppendLine("Select 'Loan' As Type, A.EmpCode,   ");
            SQL.AppendLine("A.Yr, A.PayrunCode,   ");
            SQL.AppendLine("Case A.Mth  " );
            SQL.AppendLine("    When '01' Then 'January'   ");
            SQL.AppendLine("    When '02' Then 'February'   ");
            SQL.AppendLine("    When '03' Then 'March'   ");
            SQL.AppendLine("    When '04' Then 'April'   ");
            SQL.AppendLine("    When '05' Then 'May'   ");
            SQL.AppendLine("    When '06' Then 'June'  ");
            SQL.AppendLine("    When '07' Then 'July'   ");
            SQL.AppendLine("    When '08' Then 'August'   ");
            SQL.AppendLine("    When '09' Then 'September'   ");
            SQL.AppendLine("    When '10' Then 'October'   ");
            SQL.AppendLine("    When '11' Then 'November'   ");
            SQL.AppendLine("    When '12' Then 'December' End As Mth, Concat('AP', B.CreditCode) CreditCode,  ");
            SQL.AppendLine("    H.CreditName, A.Amt  ");
            SQL.AppendLine("From TblAdvancePaymentProcess A   ");
            SQL.AppendLine("Inner Join TblAdvancePaymentHdr B On A.DocNo=B.DocNo And A.EmpCode=B.EmpCode And B.Status='A' And B.CancelInd='N'  ");
            SQL.AppendLine("Inner Join TblAdvancePaymentDtl C On B.DocNo=C.DocNo And C.Yr=@Yr And C.Mth=@Mth  " );
            SQL.AppendLine("Inner Join TblEmployee D On A.EmpCode=D.EmpCode  ");
            SQL.AppendLine("Left Join TblSite E On D.SiteCode=E.SiteCode   ");
            SQL.AppendLine("Left Join TblDepartment F On D.DeptCode=F.DeptCode   ");
            SQL.AppendLine("Left Join TblPosition G On D.PosCode=G.PosCode   ");
            SQL.AppendLine("Left Join TblCredit H On B.CreditCode=H.CreditCode   ");
            SQL.AppendLine("Inner Join TblPayrun I On A.PayrunCode=I.PayrunCode And I.CancelInd='N'  " );
            SQL.AppendLine("Left Join TblSection J On D.SectionCode = J.SectionCode   ");
            SQL.AppendLine("Where A.Yr=@Yr   ");
            SQL.AppendLine("And A.Mth=@Mth   ");
            SQL.AppendLine("Union All   ");
            SQL.AppendLine("Select 'Deduction' As Type, B.EmpCode, Left(A.StartDt, 4) As Yr, B.PayrunCode, SubString(A.Startdt, 5,2) As Mth, ");
            SQL.AppendLine("Concat('DE', B.ADCode) CreditCode, C.ADName As CreditName, ifnull(B.Amt, 0) As Amt       ");
            SQL.AppendLine("From TblPayrun A   ");
            SQL.AppendLine("Inner join TblpayrollprocessAd B On A.payrunCode = B.PayrunCode   ");
            SQL.AppendLine("Inner Join tblAllowanceDeduction C On B.ADCode = C.AdCode ");
            SQL.AppendLine("Where Left(A.StartDt, 6) = Concat(@Yr, @Mth) And SubString(A.Startdt, 5,2) = @Mth  ");
            SQL.AppendLine("Union All   ");
            SQL.AppendLine("Select    ");
            SQL.AppendLine("X1.type, X1.EmpCode, X1.startDt As Yr,    ");
            SQL.AppendLine("X1.PayrunCode, x1.mth,  Concat('PP', X1.Code) CreditCode,  ");
            SQL.AppendLine("X1.type As Name, X1.Amt  ");
            SQL.AppendLine("From    ");
            SQL.AppendLine("(   ");
            SQL.AppendLine("    Select B.EmpCode, 'TF' As Code, 'Transfer' As type,  Left(A.startDt, 4) As StartDt,   ");
            SQL.AppendLine("    A.PayrunCode, SubString(A.Startdt, 5,2) As mth,   ");
            SQL.AppendLine("    (B.Salary+B.FixAllowance+B.meal+B.transport+B.VariableAllowance+B.IncEmployee+ADLeave+B.SalaryAdjustment+B.AllVariableAllowance+B.OT1Amt+B.OT2Amt+B.OTHolidayAmt) As Amt ");
            SQL.AppendLine("    From tblpayrun A   ");
            SQL.AppendLine("    inner Join TblpayrollProcess1 B On A.PayrunCode = B.payrunCode  ");
            SQL.AppendLine("    Union ALl   ");
            SQL.AppendLine("    Select B.EmpCode, 'DP' As Code, 'SS DPLK' As type,  Left(A.startDt, 4) As StartDt,   ");
            SQL.AppendLine("    A.PayrunCode, SubString(A.Startdt, 5,2) As mth,    ");
            SQL.AppendLine("    (B.SSErDPLK+ B.SSEeDPLK) as Amt   ");
            SQL.AppendLine("    From tblpayrun A   ");
            SQL.AppendLine("    inner Join TblpayrollProcess1 B On A.PayrunCode = B.payrunCode  ");
            SQL.AppendLine("    Union ALl   ");
            SQL.AppendLine("    Select B.EmpCode, 'NI' As Code, 'Net Income' As type, Left(A.startDt, 4) As StartDt,  ");
            SQL.AppendLine("    A.PayrunCode, SubString(A.Startdt, 5,2) As mth,   ");
            SQL.AppendLine("    (B.Amt) as Amt   ");
            SQL.AppendLine("    From tblpayrun A   ");
            SQL.AppendLine("    inner Join TblpayrollProcess1 B On A.PayrunCode = B.payrunCode  ");
            SQL.AppendLine(")X1  ");
            SQL.AppendLine("Where X1.startDt=@Yr   ");
            SQL.AppendLine("And X1.Mth=@Mth  ");
            SQL.AppendLine(") T ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                
                cm.CommandText = SQL.ToString() + "Order By T.EmpCode;";
                
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Type", "EmpCode", "Yr", "PayrunCode", "mth", 
                    "CreditCode", "CreditName", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lDataDed.Add(new DataDeduction()
                        {
                            Type = Sm.DrStr(dr, c[0]),
                            EmpCode2 = Sm.DrStr(dr, c[1]),
                            Yr = Sm.DrStr(dr, c[2]),
                            PayrunCode = Sm.DrStr(dr, c[3]),
                            Mth = Sm.DrStr(dr, c[4]),
                            CreditCode = Sm.DrStr(dr, c[5]),
                            CreditName = Sm.DrStr(dr, c[6]),
                            Amt = Sm.DrDec(dr, c[7]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3()
        {
            try
            {
                var lEmp = new List<Employee>();
                var lDataDed = new List<DataDeduction>();

                Process1(ref lEmp);
                Process2(ref lDataDed);
                int xRow = 0; 
                if (lEmp.Count > 0)
                {
                    if (lDataDed.Count > 0)
                    {
                        for (var j = 0; j < lEmp.Count; j++)
                        {
                            Grd1.Rows.Add();

                            Grd1.Cells[xRow, 0].Value = xRow + 1;
                            Grd1.Cells[xRow, 1].Value = lEmp[j].EmpCode;
                            Grd1.Cells[xRow, 2].Value = lEmp[j].EmpName;
                            Grd1.Cells[xRow, 3].Value = lEmp[j].EmpCodeOld;
                            Grd1.Cells[xRow, 4].Value = Sm.GetLue(LueYr);
                            Grd1.Cells[xRow, 5].Value = Sm.GetLue(LueMth);
                            Grd1.Cells[xRow, 6].Value = lEmp[j].PayrunCode;
                            Grd1.Cells[xRow, 7].Value = lEmp[j].SiteName;
                            Grd1.Cells[xRow, 8].Value = lEmp[j].DeptName;
                            Grd1.Cells[xRow, 9].Value = lEmp[j].PosName;
                            Grd1.Cells[xRow, 10].Value = lEmp[j].SectionCode;
                            Grd1.Cells[xRow, 11].Value = lEmp[j].SectionName;
                            Grd1.Cells[xRow, 12].Value = Sm.FormatNum(lEmp[j].SSTnk, 0);
                            Grd1.Cells[xRow, 13].Value = Sm.FormatNum(lEmp[j].SSPens, 0);
                            Grd1.Cells[xRow, 14].Value = Sm.FormatNum(lEmp[j].SSKes, 0);
                            Grd1.Cells[xRow, 15].Value = Sm.FormatNum(lEmp[j].SSDplk, 0);
                            Grd1.Cells[xRow, 16].Value = Sm.FormatNum(lEmp[j].SSDplk, 0);
                            Grd1.Cells[xRow, 17].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 18].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 19].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 20].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 21].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 22].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 23].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 24].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 25].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 26].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 27].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 28].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 29].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 30].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 31].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 32].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 33].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 34].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 35].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 36].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 37].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 38].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 39].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 40].Value = Sm.FormatNum(0, 0);
                            Grd1.Cells[xRow, 41].Value = Sm.FormatNum(0, 0);

                            for (var i = 0; i < lDataDed.Count; i++)
                                {
                                    if (lEmp[j].EmpCode == lDataDed[i].EmpCode2)
                                    {
                                        for (int col = 16; col < Grd1.Cols.Count; col++)
                                        {
                                            if (lDataDed[i].CreditCode == "DE010")
                                            {
                                                Grd1.Cells[xRow, 15].Value = Sm.FormatNum((lEmp[j].SSDplk + lDataDed[i].Amt), 0);
                                                Grd1.Cells[xRow, 16].Value = Sm.FormatNum((lEmp[j].SSDplk + lDataDed[i].Amt), 0);
                                            }
                                            if (Grd1.Header.Cells[0, col].Value.ToString() == lDataDed[i].CreditCode)
                                            {
                                               
                                                    Grd1.Cells[xRow, col].Value = Sm.FormatNum(lDataDed[i].Amt, 0);
                                                    Grd1.Cells[xRow, col + 1].Value = Sm.FormatNum(lDataDed[i].Amt, 0);
                                             }
                                        }
                                    }
                                }
                            Grd1.Cells[xRow, 39].Value =
                                Sm.GetGrdDec(Grd1, xRow, 20) +
                                Sm.GetGrdDec(Grd1, xRow, 22) +
                                Sm.GetGrdDec(Grd1, xRow, 24) +
                                Sm.GetGrdDec(Grd1, xRow, 26) +
                                Sm.GetGrdDec(Grd1, xRow, 28) +
                                Sm.GetGrdDec(Grd1, xRow, 30) +
                                Sm.GetGrdDec(Grd1, xRow, 32) +
                                Sm.GetGrdDec(Grd1, xRow, 34) +
                                Sm.GetGrdDec(Grd1, xRow, 36) +
                                Sm.GetGrdDec(Grd1, xRow, 38) ;
                            xRow += 1; 
                        }
                    }
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41 });
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueSection_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSection, new Sm.RefreshLue1(Sl.SetLueSectionCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkSection_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Section");
        }
        #endregion

        #region Class

        private class Employee
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
            public string DeptCode { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string SectionCode { get; set; }
            public string SectionName { get; set; }
            public decimal SSTnk { get; set; }
            public decimal SSPens { get; set; }
            public decimal SSKes { get; set; }
            public decimal SSDplk { get; set; }
            public string PayrunCode { get; set; }


            //public string TypeDoc { get; set; }
            //public string Yr { get; set; }
            //public string PayrunCode { get; set; }
            //public string Mth { get; set; }
            //public string CreditCode { get; set; }
            //public string CreditName { get; set; }
            //public decimal Amt { get; set; }
        }

        private class DataDeduction
        {
            public string Type { get; set; }
            public string EmpCode2 { get; set; }
            public string Yr { get; set; }
            public string PayrunCode { get; set; }
            public string Mth { get; set; }
            public string CreditCode { get; set; }
            public string CreditName { get; set; }
            public decimal Amt { get; set; }
        }


        #endregion

    }


 

}
