﻿namespace RunSystem
{
    partial class FrmRptPPS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChkEmpCode = new DevExpress.XtraEditors.CheckEdit();
            this.TxtEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ChkDeptCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkGradeCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueGradeCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.ChkLvlCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueLvlCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkGradeCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGradeCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLvlCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLvlCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.ChkLvlCode);
            this.panel2.Controls.Add(this.LueLvlCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.ChkGradeCode);
            this.panel2.Controls.Add(this.LueGradeCode);
            this.panel2.Controls.Add(this.ChkEmpCode);
            this.panel2.Controls.Add(this.TxtEmpCode);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.ChkDeptCode);
            this.panel2.Controls.Add(this.LueDeptCode);
            this.panel2.Size = new System.Drawing.Size(772, 93);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 380);
            this.Grd1.TabIndex = 14;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 93);
            this.panel3.Size = new System.Drawing.Size(772, 380);
            // 
            // ChkEmpCode
            // 
            this.ChkEmpCode.Location = new System.Drawing.Point(309, 2);
            this.ChkEmpCode.Name = "ChkEmpCode";
            this.ChkEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkEmpCode.Properties.Appearance.Options.UseFont = true;
            this.ChkEmpCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkEmpCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkEmpCode.Properties.Caption = " ";
            this.ChkEmpCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkEmpCode.Size = new System.Drawing.Size(19, 22);
            this.ChkEmpCode.TabIndex = 10;
            this.ChkEmpCode.ToolTip = "Remove filter";
            this.ChkEmpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkEmpCode.ToolTipTitle = "Run System";
            this.ChkEmpCode.CheckedChanged += new System.EventHandler(this.ChkEmpCode_CheckedChanged);
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.EnterMoveNextControl = true;
            this.TxtEmpCode.Location = new System.Drawing.Point(86, 3);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode.Properties.MaxLength = 250;
            this.TxtEmpCode.Size = new System.Drawing.Size(219, 20);
            this.TxtEmpCode.TabIndex = 9;
            this.TxtEmpCode.Validated += new System.EventHandler(this.TxtEmpCode_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(22, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 14);
            this.label12.TabIndex = 8;
            this.label12.Text = "Employee";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 14);
            this.label7.TabIndex = 11;
            this.label7.Text = "Department";
            // 
            // ChkDeptCode
            // 
            this.ChkDeptCode.Location = new System.Drawing.Point(309, 23);
            this.ChkDeptCode.Name = "ChkDeptCode";
            this.ChkDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDeptCode.Properties.Appearance.Options.UseFont = true;
            this.ChkDeptCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDeptCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDeptCode.Properties.Caption = " ";
            this.ChkDeptCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDeptCode.Size = new System.Drawing.Size(19, 22);
            this.ChkDeptCode.TabIndex = 13;
            this.ChkDeptCode.ToolTip = "Remove filter";
            this.ChkDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDeptCode.ToolTipTitle = "Run System";
            this.ChkDeptCode.CheckedChanged += new System.EventHandler(this.ChkDeptCode_CheckedChanged);
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(86, 24);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 25;
            this.LueDeptCode.Properties.MaxLength = 16;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 500;
            this.LueDeptCode.Size = new System.Drawing.Size(219, 20);
            this.LueDeptCode.TabIndex = 12;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 14);
            this.label1.TabIndex = 14;
            this.label1.Text = "Grade";
            // 
            // ChkGradeCode
            // 
            this.ChkGradeCode.Location = new System.Drawing.Point(309, 45);
            this.ChkGradeCode.Name = "ChkGradeCode";
            this.ChkGradeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkGradeCode.Properties.Appearance.Options.UseFont = true;
            this.ChkGradeCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkGradeCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkGradeCode.Properties.Caption = " ";
            this.ChkGradeCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkGradeCode.Size = new System.Drawing.Size(19, 22);
            this.ChkGradeCode.TabIndex = 16;
            this.ChkGradeCode.ToolTip = "Remove filter";
            this.ChkGradeCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkGradeCode.ToolTipTitle = "Run System";
            this.ChkGradeCode.CheckedChanged += new System.EventHandler(this.ChkGradeCode_CheckedChanged);
            // 
            // LueGradeCode
            // 
            this.LueGradeCode.EnterMoveNextControl = true;
            this.LueGradeCode.Location = new System.Drawing.Point(86, 46);
            this.LueGradeCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueGradeCode.Name = "LueGradeCode";
            this.LueGradeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGradeCode.Properties.Appearance.Options.UseFont = true;
            this.LueGradeCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGradeCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGradeCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGradeCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGradeCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGradeCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGradeCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGradeCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGradeCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGradeCode.Properties.DropDownRows = 25;
            this.LueGradeCode.Properties.MaxLength = 16;
            this.LueGradeCode.Properties.NullText = "[Empty]";
            this.LueGradeCode.Properties.PopupWidth = 500;
            this.LueGradeCode.Size = new System.Drawing.Size(219, 20);
            this.LueGradeCode.TabIndex = 15;
            this.LueGradeCode.ToolTip = "F4 : Show/hide list";
            this.LueGradeCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGradeCode.EditValueChanged += new System.EventHandler(this.LueGradeCode_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(47, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 14);
            this.label2.TabIndex = 17;
            this.label2.Text = "Level";
            // 
            // ChkLvlCode
            // 
            this.ChkLvlCode.Location = new System.Drawing.Point(309, 67);
            this.ChkLvlCode.Name = "ChkLvlCode";
            this.ChkLvlCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkLvlCode.Properties.Appearance.Options.UseFont = true;
            this.ChkLvlCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkLvlCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkLvlCode.Properties.Caption = " ";
            this.ChkLvlCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkLvlCode.Size = new System.Drawing.Size(19, 22);
            this.ChkLvlCode.TabIndex = 19;
            this.ChkLvlCode.ToolTip = "Remove filter";
            this.ChkLvlCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkLvlCode.ToolTipTitle = "Run System";
            this.ChkLvlCode.CheckedChanged += new System.EventHandler(this.ChkLvlCode_CheckedChanged);
            // 
            // LueLvlCode
            // 
            this.LueLvlCode.EnterMoveNextControl = true;
            this.LueLvlCode.Location = new System.Drawing.Point(86, 68);
            this.LueLvlCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueLvlCode.Name = "LueLvlCode";
            this.LueLvlCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLvlCode.Properties.Appearance.Options.UseFont = true;
            this.LueLvlCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLvlCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLvlCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLvlCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLvlCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLvlCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLvlCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLvlCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLvlCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLvlCode.Properties.DropDownRows = 25;
            this.LueLvlCode.Properties.MaxLength = 16;
            this.LueLvlCode.Properties.NullText = "[Empty]";
            this.LueLvlCode.Properties.PopupWidth = 500;
            this.LueLvlCode.Size = new System.Drawing.Size(219, 20);
            this.LueLvlCode.TabIndex = 18;
            this.LueLvlCode.ToolTip = "F4 : Show/hide list";
            this.LueLvlCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLvlCode.EditValueChanged += new System.EventHandler(this.LueLvlCode_EditValueChanged);
            // 
            // FrmRptPPS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptPPS";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkGradeCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGradeCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLvlCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLvlCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkEmpCode;
        private DevExpress.XtraEditors.TextEdit TxtEmpCode;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.CheckEdit ChkDeptCode;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.CheckEdit ChkLvlCode;
        private DevExpress.XtraEditors.LookUpEdit LueLvlCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkGradeCode;
        private DevExpress.XtraEditors.LookUpEdit LueGradeCode;
    }
}