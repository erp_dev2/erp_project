﻿#region Update
/*
    07/08/2017 [WED] tambah kolom foreign name berdasarkan parameter IsShowForeignName
    07/01/2020 [VIN/SIER] Parameter baru (mIsFilterByItCt) untuk Item Category per masing-masing Group
    23/03/2020 [TKG/IMS] berdasarkan parameter IsInvTrnShowItSpec, menampilkan specifikasi item
    10/01/2020 [TKG/IMS] tambah local code
    12/04/2022 [RDA/PRODUCT] penyesuaian data kolom untuk menu investment initial
    12/04/2022 [HAR/PRODUCT] Bitem investment equity yang nonaktf masih ketarik
    13/05/2022 [RDA/PRODUCT] penyesuaian show data untuk kolom Investment Code
    25/05/2022 [IBL/PRODUCT] membedakan antara item equity dan item debt dengan mType (1: Equity, 2: Debt)
    05/07/2022 [SET/PRODUCT] penambahan Last Coupon Date & Next Coupon Date di detail untuk digunakan di tab Debt Securities
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentStockInitialDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmInvestmentStockInitial mFrmParent;
        private string mSQL = string.Empty;
        internal bool mIsFilterByItCt = false;
        private int mType = 0;

        #endregion

        #region Constructor

        public FrmInvestmentStockInitialDlg(FrmInvestmentStockInitial FrmParent, int Type)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mType = Type;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                ChkUnselectedItem.Visible = false;
                SetGrd();
                mFrmParent.SetLueInvestmentCtCode(ref LueInvestmentCtCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Investment's"+Environment.NewLine+ (mType == 1 ? "Equity" : "Debt")+ " Code",
                        "Investment's"+Environment.NewLine+"Name", 
                        "Local Code",
                        "Foreign Name",

                        //6-10
                        "Investment Category",
                        "UoM",
                        "UoM",
                        "UoM",
                        "Group",

                        //11-15
                        "Specification",
                        "Currency",
                        "Issuer",
                        "CurCode",
                        "Investment's"+Environment.NewLine+"Code",

                        //16-17
                        "LastCouponDt",
                        "NextCouponDt",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 250, 130, 150, 
                        
                        //6-10
                        200, 0, 0, 0, 100,
  
                        //11-15
                        200, 150, 150, 150, 150,

                        //16-17
                        100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 16, 17 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 7, 8, 9, 10, 11, 14, 16, 17 }, false);
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[10].Visible = true;
                Grd1.Cols[10].Move(6);
            }
            if (mFrmParent.mIsInvTrnShowItSpec)
            {
                Grd1.Cols[11].Visible = true;
                Grd1.Cols[11].Move(7);
            }
            Grd1.Cols[15].Move(2);

            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var SQL = new StringBuilder();

                if (mType == 1)
                {
                    SQL.AppendLine("Select A.InvestmentEquityCode as InvestmentCode, B.PortofolioName, A.ForeignName, C.InvestmentCtName,  ");
                    SQL.AppendLine("A.UomCode, D.CurName, B.Issuer, A.Specification, B.CurCode, A.PortofolioId, NULL AS LastCouponDt, NULL AS NextCouponDt  ");
                    SQL.AppendLine("From TblInvestmentItemEquity A  ");
                    SQL.AppendLine("Inner Join TblInvestmentPortofolio B ON A.PortofolioId = B.PortofolioId  ");
                    SQL.AppendLine("Left Join TblInvestmentCategory C ON B.InvestmentCtCode = C.InvestmentCtCode  ");
                    SQL.AppendLine("Left Join TblCurrency D ON B.CurCode = D.CurCode Where A.ActInd = 'Y' ");
                }
                else
                {
                    SQL.AppendLine("Select A.InvestmentDebtCode as InvestmentCode, B.PortofolioName, A.ForeignName, C.InvestmentCtName,  ");
                    SQL.AppendLine("A.UomCode, D.CurName, B.Issuer, A.Specification, B.CurCode, A.PortofolioId, A.LastCouponDt, NextCouponDt ");
                    SQL.AppendLine("From TblInvestmentItemDebt A  ");
                    SQL.AppendLine("Inner Join TblInvestmentPortofolio B ON A.PortofolioId = B.PortofolioId  ");
                    SQL.AppendLine("Left Join TblInvestmentCategory C ON B.InvestmentCtCode = C.InvestmentCtCode  ");
                    SQL.AppendLine("Left Join TblCurrency D ON B.CurCode = D.CurCode Where A.ActInd = 'Y' ");
                }

                mSQL = SQL.ToString();

                var cm = new MySqlCommand();
                string 
                    Filter = " ", 
                    Filter2 = string.Empty;
                if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                /*
                if (ChkUnselectedItem.Checked)
                {
                    if (mFrmParent.Grd3.Rows.Count >= 1)
                    {
                        var No = "0001";
                        for (int Row = 0; Row < mFrmParent.Grd3.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(mFrmParent.Grd3, Row, 4).Length != 0)
                            {
                                if (Filter2.Length > 0) Filter2 += " And ";
                                Filter2 += "(A.ItCode<>@ItCode" + No.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@ItCode" + No.ToString(), Sm.GetGrdStr(mFrmParent.Grd3, Row, 4));
                                No = ("000" + (int.Parse(No) + 1).ToString()).ToString();
                            }
                        }
                    }
                    if (Filter2.Length != 0) Filter += " And (" + Filter2 + ")";
                }
                */

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.PortofolioId", "B.PortofolioName", "A.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueInvestmentCtCode), "C.InvestmentCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, 
                        mSQL + 
                        Filter + " Order By B.PortofolioName;",
                        
                        new string[] 
                        { 
                            //0
                            "InvestmentCode", 

                            //1-5
                            "PortofolioName", "ForeignName", "InvestmentCtName", "UomCode", "CurName",
                            
                            //6-9
                            "Issuer", "Specification", "CurCode", "PortofolioId", "LastCouponDt",

                            //10
                            "NextCouponDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 11);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                if (mType == 1)
                {
                    mFrmParent.Grd3.BeginUpdate();
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1))
                        {
                            if (IsChoose == false) IsChoose = true;

                            Row1 = mFrmParent.Grd3.Rows.Count - 1;
                            Row2 = Row;

                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 4, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 5, Grd1, Row2, 3);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 6, Grd1, Row2, 4);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 20, Grd1, Row2, 5);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 25, Grd1, Row2, 6);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 12, Grd1, Row2, 7);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 14, Grd1, Row2, 8);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 16, Grd1, Row2, 9);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 19, Grd1, Row2, 10);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 21, Grd1, Row2, 11);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 26, Grd1, Row2, 12);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 27, Grd1, Row2, 13);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 32, Grd1, Row2, 14);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 34, Grd1, Row2, 15);
                            Sm.SetGrdNumValueZero(mFrmParent.Grd3, Row1, new int[] { 11, 13, 15 });

                            mFrmParent.Grd3.Rows.Add();
                            Sm.SetGrdBoolValueFalse(mFrmParent.Grd3, mFrmParent.Grd3.Rows.Count - 1, new int[] { 1, 2 });
                            Sm.SetGrdNumValueZero(mFrmParent.Grd3, mFrmParent.Grd3.Rows.Count - 1, new int[] { 11, 13, 15, 17, 29, 30 });
                        }
                    }
                    mFrmParent.ComputeTotalQty();
                    mFrmParent.Grd3.EndUpdate();
                }
                else
                {
                    mFrmParent.Grd4.BeginUpdate();
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1))
                        {
                            if (IsChoose == false) IsChoose = true;

                            Row1 = mFrmParent.Grd4.Rows.Count - 1;
                            Row2 = Row;

                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 3, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 4, Grd1, Row2, 15);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 5, Grd1, Row2, 3);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 10, Grd1, Row2, 6);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 11, Grd1, Row2, 14);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 12, Grd1, Row2, 13);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 18, Grd1, Row2, 4);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 24, Grd1, Row2, 16);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 25, Grd1, Row2, 17);
                            Sm.SetGrdNumValueZero(mFrmParent.Grd4, Row1, new int[] { 15, 16, 17 });

                            mFrmParent.Grd4.Rows.Add();
                            Sm.SetGrdBoolValueFalse(mFrmParent.Grd4, mFrmParent.Grd4.Rows.Count - 1, new int[] { 1 });
                            Sm.SetGrdNumValueZero(mFrmParent.Grd4, mFrmParent.Grd4.Rows.Count - 1, new int[] { 15, 16, 17 });
                            if(Sm.GetGrdDate(Grd1, Row2, 16) != "" && Sm.GetGrdDate(Grd1, Row2, 17) != "")
                                Sm.GrdColReadOnly(true, true, mFrmParent.Grd4, new int[] { 24, 25 });
                            else
                                Sm.GrdColReadOnly(false, true, mFrmParent.Grd4, new int[] { 24, 25 });
                        }
                    }
                    mFrmParent.Grd4.EndUpdate();
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 2));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 2));
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0 && Sm.GetGrdStr(Grd1, 0, 2).Length>0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for(int Row = 0;Row<Grd1.Rows.Count;Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method
        


        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueInvestmentCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueInvestmentCtCode, new Sm.RefreshLue2(mFrmParent.SetLueInvestmentCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkInvestmentCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Investment Item's category");
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion

    }
}
