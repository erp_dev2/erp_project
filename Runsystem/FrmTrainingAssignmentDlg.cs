﻿#region Update
/*
    29/03/2018 [HAR] employee dan training code ambil dari training Request
    20/07/2018 [HAR] employee filter by site
    28/08/2018 [HAR] validasi hanya employee yang training raquestnya diapprove
    07/09/2018 [HAR] employee bisa di pilih berkali2
    26/09/2018 [HAR] employee yang sdh di assignment tidak usah dimunculin
    04/10/2018 [HAR] data date training tidak di lempar ke parent
    07/11/2018 [HAR] training request docno dilempar ke parent, emplolyee yg muncul yg training requestnya blm di assigmnetkan
    06/02/2019 [HAR] BUG ANALIS : function tick nya (employee yang sama) di hilangin 
    05/04/2019 [TKG] tambah filter training code
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTrainingAssignmentDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmTrainingAssignment mFrmParent;
        private string mSiteCode = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmTrainingAssignmentDlg(FrmTrainingAssignment FrmParent, string SiteCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mSiteCode = SiteCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Employee";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                SetLueTrainingCode(ref LueTrainingCode, mSiteCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Employee's"+Environment.NewLine+"Code",
                    "Employee's Name", 
                    "Old Code",
                    "Resign Date",

                    //6-10
                    "TrainingCode",
                    "Training",
                    "Quantity",
                    "Training Request",
                    "",

                    //11-12
                    "Date Schedule",
                    "Dt2"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 80, 180, 80, 80, 

                    //6-10
                    0, 180, 100, 150, 20,
                    //11
                    100, 100
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdColButton(Grd1, new int[] {10});
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9});
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 6, 9, 10, 11, 12 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 9, 10, }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct ");
            SQL.AppendLine("T1.EmpCode, T3.EmpName, T3.EmpCodeOld, T3.ResignDt,  ");
            SQL.AppendLine("T2.TrainingCode, T4.TrainingName, ifnull(T5.Qty, 0) Qty, T1.DocNo, T6.Dt, DATE_FORMAT(T6.Dt, '%a, %d-%b-%Y') As Dt2  ");
            SQL.AppendLine("From tbltrainingrequestdtl2 T1  ");
            SQL.AppendLine("Inner Join TblTrainingRequestHdr T2 On T1.DocNo = T2.DocNo And T2.CancelInd = 'N'  ");
            SQL.AppendLine("Inner Join tblEmployee T3 On T1.EmpCode = T3.EmpCode And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>@CurrentDate)) ");
            if (mSiteCode.Length > 0)
            {
                SQL.AppendLine("And T3.SiteCode =@SiteCode ");
            }
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("And T3.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(T3.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Inner Join TblTraining T4 On T2.TrainingCode = T4.TrainingCode  ");
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("And T4.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(T4.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select EmpCode, trainingCode,  Count(trainingCode) Qty From TblEmployeeTraining ");
            SQL.AppendLine("    Group By EmpCode, trainingCode ");
            SQL.AppendLine(")T5 On T1.EmpCode = T5.EmpCode And T2.TrainingCode = T5.TrainingCode  ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( "); 
            SQL.AppendLine("    Select trainingCode, Min(Dt) As Dt From tbltrainingschDtl ");
	        SQL.AppendLine("    Where Dt>=@CurrentDocDate And SchInd = 'Y' Group By trainingCode ");
            SQL.AppendLine(")T6 On T2.trainingCode And T6.TrainingCode  ");
            SQL.AppendLine("Where  T1.`Status` = 'A' And T2.CancelInd = 'N' ");
            SQL.AppendLine("And Concat(T1.EmpCode, T2.TrainingCode, ifnull(T1.DocNo, '-')) not in ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Concat(B.EmpCode, B.TrainingCode, ifnull(B.TrainingRequestDocNo, '-')) As Kode ");
            SQL.AppendLine("    from tblTrainingAssignmentHdr A ");
            SQL.AppendLine("    Inner Join  tblTrainingAssignmentDtl B On A.DocNo= B.DocNO ");
            SQL.AppendLine("    Where CancelInd = 'N' ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.CmParamDt(ref cm, "@CurrentDocDate", Sm.GetDte(mFrmParent.DteDocDt).Substring(0, 8));

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T1.EmpCode", "T3.EmpName", "T3.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueTrainingCode), "T2.TrainingCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Group By T1.EmpCode, T3.EmpName, T3.EmpCodeOld,T3.ResignDt, T2.TrainingCode, T4.TrainingName Order By T3.EmpName, T4.TrainingName;",
                        new string[] 
                        { 
                            //0
                            "EmpCode",
 
                            //1-5
                            "EmpName", "EmpCodeOld", "ResignDt", "TrainingCode", "TrainingName",

                            //6
                            "Qty", "DocNo", "Dt","Dt2"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        //Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 7 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 employee.");
        }

        private bool IsEmpCodeAlreadyChosen(int Row)
        {
            string EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
            string TrainingCode = Sm.GetGrdStr(Grd1, Row, 6);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    string.Concat("##", EmpCode, "##", TrainingCode, "##"),
                    string.Concat("##", Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), "##", Sm.GetGrdStr(mFrmParent.Grd1, Index, 5), "##")
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            //TickGrdCols(e.RowIndex, e.ColIndex);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmTrainingRequest("");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmTrainingRequest("");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetLueTrainingCode(ref LookUpEdit Lue, string SiteCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select TrainingCode As Col1, TrainingName As Col2 From TblTraining ");
            if (SiteCode.Length>0) SQL.AppendLine("Where SiteCode Is Not Null And SiteCode=@SiteCode ");
            SQL.AppendLine("Order By TrainingName;");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void TickGrdCols(int RowIndex, int ColIndex)
        {
            try
            {
                bool Value = Sm.GetGrdBool(Grd1, RowIndex, ColIndex);
                string EmpCode = Sm.GetGrdStr(Grd1, RowIndex, 2);

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if(Row == RowIndex + 1)
                    {
                        if (EmpCode != Sm.GetGrdStr(Grd1, Row, 2)) break;
                    }

                    if (EmpCode == Sm.GetGrdStr(Grd1, Row, 2))
                    {
                        Grd1.Cells[Row, ColIndex].Value = Value;
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control events

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueTrainingCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrainingCode, new Sm.RefreshLue2(SetLueTrainingCode), mSiteCode);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkTrainingCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Training");
        }

        #endregion

        #endregion

    }
}
