﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpTravelRequest : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmEmpTravelRequestFind FrmFind;

        #endregion

        #region Constructor

        public FrmEmpTravelRequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Master Employee Travel Request";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetGrd();
            SetFormControl(mState.View);
            base.FrmLoad(sender, e);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-4
                    "Employee"+Environment.NewLine+"Code",
                    "",
                    "Employee"+Environment.NewLine+"Name"
                },
                new int[] 
                {
                    20,
                    80, 20, 150
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0, 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 3 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtEmpCode, TxtEmpName, TxtEmpCodeOld, TxtDeptCode, TxtPosCode, TxtSiteCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3 });
                    BtnEmpCode.Enabled = false;
                    TxtEmpCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 2 });
                    BtnEmpCode.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 2 });
                    BtnEmpCode.Enabled = false;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtEmpCode, TxtEmpName, TxtEmpCodeOld, TxtDeptCode, TxtPosCode, TxtSiteCode, MeeRemark
            });
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpTravelRequestFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            BtnEmpCode_Click(sender, e);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtEmpCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && !Sm.IsTxtEmpty(TxtEmpCode, "Employee", false))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmEmpTravelRequestDlg(this, TxtEmpCode.Text));
                }
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && !Sm.IsTxtEmpty(TxtEmpCode, "Employee", false))
                Sm.FormShowDialog(new FrmEmpTravelRequestDlg(this, TxtEmpCode.Text));


            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            string EmpCode = TxtEmpCode.Text;

            cml.Add(SaveEmpTravelRequestHdr(EmpCode));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    cml.Add(SaveEmpTravelRequestDtl(EmpCode, Row));

            Sm.ExecCommands(cml);

            ShowData(EmpCode);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtEmpCode, "Employee Code", false) ||
                IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No Employee in the list.");
                return true;
            }
            return false;
        }


        private MySqlCommand SaveEmpTravelRequestHdr(string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpTravelRequestHdr(EmpCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, @Remark, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("    On Duplicate Key ");
            SQL.AppendLine("    Update Remark = @Remark, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblEmpTravelRequestDtl Where EmpCode = @EmpCode; ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpTravelRequestDtl(string EmpCode, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblEmpTravelRequestDtl(EmpCode, EmpCode2, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@EmpCode, @EmpCode2, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParam<String>(ref cm, "@EmpCode2", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string EmpCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowEmpTravelRequestHdr(EmpCode);
                ShowEmpTravelRequestDtl(EmpCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpTravelRequestHdr(string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, E.SiteName, A.Remark ");
            SQL.AppendLine("From TblEmpTravelRequestHdr A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode = C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode = D.DeptCode ");
            SQL.AppendLine("Left Join TblSite E On B.SiteCode = E.SiteCode ");
            SQL.AppendLine("Where A.EmpCode = @EmpCode; ");
            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "EmpCode", 
                        "EmpName", "EmpCodeOld", "PosName", "DeptName", "SiteName",
                        "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[1]);
                        TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[2]);
                        TxtPosCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[5]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        private void ShowEmpTravelRequestDtl(string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode2, B.EmpName ");
            SQL.AppendLine("From TblEmpTravelRequestDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode2 = B.EmpCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode2", 
                    //1
                    "EmpName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        internal string GetSelectedEmployeeCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 1) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        #endregion

        #endregion

        #region Events

        #region Button Events

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmEmpTravelRequestDlg2(this));
        }

        #endregion

        #endregion

    }
}
