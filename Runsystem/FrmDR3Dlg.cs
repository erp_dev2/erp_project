﻿#region Update
/*
    18/06/2020 [IBL/KSM] new apps
 *  28/01/2021 [VIN/KSM] tidak bisa pilih dokumen dengan status berbeda (PPN/Non PPN) tambah filter status
    09/02/2021 [ICA/KSM] Item yg muncul hanya item dari sales contract dengan status bukan fullfilled
    22/02/2021 [IBL/KSM] Tambah GenerateLocalDocNo, untuk otomatis generate localdocno di main screen saat choosedata
 *  08/03/2021 [RDH/KSM] Tambah kolom Local Document Sales Contract (0109990102) di panel detail menu DR 
 *  22/04/2021 [ICA/KSM] Mengaktifkan untuk DR CBD
 *  19/05/2021 [MYA/KSM] Tambah kolom local code pada grid ambil dari tabel Sales Contract
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDR3Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDR3 mFrmParent;
        string mSQL = string.Empty, mCtCode = "", mSiteCode = "";
        bool mCBDInd = false;
        

        #endregion

        #region Constructor

        public FrmDR3Dlg(FrmDR3 FrmParent, string CtCode, string SiteCode, bool CBDInd)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
            mSiteCode = SiteCode;
            mCBDInd = CBDInd;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Sales Contract";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetLueTaxInd(ref LueTaxInd);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            if (!mCBDInd)
            {
                SQL.AppendLine("SELECT NULL as SAName, D.ItCode, D.ItName, D.ItCodeInternal, D.ForeignName, NULL AS PackagingUnitUomCode, ");
                SQL.AppendLine("(C.Qty + ");
                SQL.AppendLine("    (C.Qty * IFNULL( ");
                SQL.AppendLine("        Case When K.ParValue = B.SiteCode Then ");
                SQL.AppendLine("            Case When LENGTH(J.ParValue) = 0 Then 0 ");
                SQL.AppendLine("            Else ");
                SQL.AppendLine("                CAST(J.ParValue AS DECIMAL(10,4)) * 0.01 ");
                SQL.AppendLine("            End ");
                SQL.AppendLine("        ELSE 0 End ");
                SQL.AppendLine("        , 0) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") AS QtyPackagingUnit, ");
                SQL.AppendLine(" 0 AS OutstandingQtyPackagingUnit, A.DocNo, A.DocDt, B.DocNo AS SalesMemoDocNo, C.DNo as SalesMemoDNo, NULL as PriceUomCode, A.LocalDocNo, NULL AS CustomerPONo, ");
                SQL.AppendLine("C.Qty, ");
                SQL.AppendLine("(C.Qty + ");
                SQL.AppendLine("    (C.Qty * IFNULL( ");
                SQL.AppendLine("        Case When K.ParValue = B.SiteCode Then ");
                SQL.AppendLine("            Case When LENGTH(J.ParValue) = 0 Then 0 ");
                SQL.AppendLine("            Else ");
                SQL.AppendLine("                CAST(J.ParValue AS DECIMAL(10,4)) * 0.01 ");
                SQL.AppendLine("            End ");
                SQL.AppendLine("        ELSE 0 End ");
                SQL.AppendLine("        , 0) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") - IFNULL(I.Qty, 0) AS OutstandingQty, ");
                SQL.AppendLine("D.SalesUoMCode, D.InventoryUoMCode, C.DeliveryDt, I.Qty AS UsedItem, ");
                SQL.AppendLine("NULL as VoucherDocNo, NULL as PriceAfterTax, NULL as CtPONo, B.TaxInd, A.LocalDocNo");
                SQL.AppendLine("FROM TblSalesContract A ");
                SQL.AppendLine("INNER JOIN TblSalesMemoHdr B ON A.SalesMemoDocNo = B.DocNo ");
                SQL.AppendLine("INNER JOIN TblSalesMemoDtl C ON B.DocNo = C.DocNo ");
                SQL.AppendLine("INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            }
            else
            {
                SQL.AppendLine("Select null as SAName, C.ItCode, D.ItName, D.ItCodeInternal, D.SalesUomCode PackagingUnitUomCode,  ");
                SQL.AppendLine("D.ItName ForeignName, C.Qty OutstandingQtyPackagingUnit, ");
                SQL.AppendLine("(C.Qty + ");
                SQL.AppendLine("    (C.Qty * IFNULL( ");
                SQL.AppendLine("        Case When K.ParValue = B.SiteCode Then ");
                SQL.AppendLine("            Case When LENGTH(J.ParValue) = 0 Then 0 ");
                SQL.AppendLine("            Else ");
                SQL.AppendLine("                CAST(J.ParValue AS DECIMAL(10,4)) * 0.01 ");
                SQL.AppendLine("            End ");
                SQL.AppendLine("        ELSE 0 End ");
                SQL.AppendLine("        , 0) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") AS QtyPackagingUnit, ");
                SQL.AppendLine("(C.Qty + ");
                SQL.AppendLine("    (C.Qty * IFNULL( ");
                SQL.AppendLine("        Case When K.ParValue = B.SiteCode Then ");
                SQL.AppendLine("            Case When LENGTH(J.ParValue) = 0 Then 0 ");
                SQL.AppendLine("            Else ");
                SQL.AppendLine("                CAST(J.ParValue AS DECIMAL(10,4)) * 0.01 ");
                SQL.AppendLine("            End ");
                SQL.AppendLine("        ELSE 0 End ");
                SQL.AppendLine("        , 0) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") - IFNULL(I.Qty, 0) AS OutstandingQty, ");
                SQL.AppendLine("A.DocNo, A.DocDt, C.DocNo AS SalesMemoDocNo, C.DNo as SalesMemoDNo, C.Qty, NULL as PriceUomCode, A.LocalDocNo, ");
                SQL.AppendLine("D.SalesUoMCode, D.InventoryUoMCode, C.DeliveryDt, I.Qty AS UsedItem, ");
                SQL.AppendLine("H.DocNo VoucherDocNo, C.UPriceAfterTax as PriceAfterTax, NULL as CtPONo, B.TaxInd, D.SalesUomCode, B.TaxInd ");
                SQL.AppendLine("From TblSalesContract A ");
                SQL.AppendLine("Inner Join TblSalesMemoHdr B On A.SalesMemoDocNo = B.DocNo ");
                SQL.AppendLine("Inner Join TblSalesMemoDtl C On B.DocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblItem D On C.ItCode = D.ItCode ");
                SQL.AppendLine("Inner Join TblSalesInvoiceHdr E On E.SODocNo = A.DocNo ");
                SQL.AppendLine("Inner Join TblIncomingPaymentDtl F On F.InvoiceDocNo = E.DocNo ");
                SQL.AppendLine("Inner Join TblIncomingPaymentHdr G On F.DocNo = G.DocNo And G.CancelInd = 'N' ");
                SQL.AppendLine("Inner Join TblVoucherHdr H On G.VoucherRequestDocNo = H.VoucherRequestDocNo And H.CancelInd = 'N' ");
            }

            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT A.SCDocNo, A.SODocNo, A.SODNo, SUM(IFNULL(A.QtyPackagingUnit,0)) AS Qty  ");
            SQL.AppendLine("    FROM tbldrdtl A ");
            SQL.AppendLine("     INNER JOIN tbldrhdr B ON A.DocNo = B.DocNo AND B.CancelInd = 'N' ");
            SQL.AppendLine("         AND B.DocType = '3' ");
            SQL.AppendLine("    GROUP BY A.SCDocNo, A.SODocNo, A.SODNo ");
            SQL.AppendLine(") I ON A.DocNo = I.SCDocNo AND B.DocNo = I.SODocNo AND C.DNo = I.SODNo ");
            SQL.AppendLine("LEFT JOIN TblParameter J ON J.ParCode = 'AllowancePercentageQtyForDRSC' ");
            SQL.AppendLine("LEFT JOIN TblParameter K ON K.ParCode = 'AllowanceSiteCodeForDRSC' ");
            SQL.AppendLine("WHERE A.DocDt BETWEEN @DocDt1 AND @DocDt2 ");
            SQL.AppendLine("AND A.Status <> 'F' ");
            SQL.AppendLine("AND B.CtCode = @CtCode ");
            SQL.AppendLine("AND B.SiteCode = @SiteCode ");
            SQL.AppendLine("AND A.CancelInd = 'N' ");
            SQL.AppendLine("AND B.Status = 'A' ");
            SQL.AppendLine("AND C.ProcessInd <> 'F' ");
            SQL.AppendLine("AND D.ActInd = 'Y' ");

            SQL.AppendLine("And Not Find_IN_set(Concat(C.ItCode, A.DocNo, B.DocNo, C.DNo), @SelectedItemSO) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Item's"+Environment.NewLine+"Code", 
                        "",
                        "Item's Name",
                        "Local Code",
                        
                        //6-10
                        "Packaging"+Environment.NewLine+"UoM",
                        "Quantity"+Environment.NewLine+"(Packaging)",
                        "Oustanding Quantity"+Environment.NewLine+"(Packaging)",
                        "Sales Contract#",
                        "",
                        
                        //11-15
                        "Sales Contract"+Environment.NewLine+"Date",
                        "Sales Memo DNo",
                        "Quantity"+Environment.NewLine+"(Sales)",
                        "Outstanding Quantity"+Environment.NewLine+"(Sales)",
                        "UoM"+Environment.NewLine+"(Sales)",
                        
                        //16-20
                        "UoM"+Environment.NewLine+"(Inventory)",
                        "Delivery"+Environment.NewLine+"Date",
                        "Voucher",
                        "Price"+Environment.NewLine+"After Tax",
                        "Local"+Environment.NewLine+"Document#",

                        //21-25
                        "Customer's PO#",
                        "Foreign Name",
                        "AllowanceQty",
                        "UsedItem",
                        "Sales Memo#",

                        //26
                        "PPN",
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        20, 150, 20, 200, 150,  
                        
                        //6-10
                        80, 80, 80, 200, 20, 
                        
                        //11-15
                        130, 100, 80, 80, 100,   
                        
                        //16-20
                        100, 130, 100, 100, 150,

                        //21-25
                        130, 150, 0, 0, 150,

                        //26
                        30
                    }
                );
            Grd1.Cols[20].Move(10);

            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 13, 14, 19 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 10 });
            Sm.GrdFormatDate(Grd1, new int[] { 11, 17 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 });
            
            if (mCBDInd == false)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 13, 18, 19, 21, 23, 24, 25, 26 }, false);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 13, 18, 19, 21, 23, 24, 25, 26 }, false);
            }
            Sm.SetGrdProperty(Grd1, false);

            if (!mFrmParent.mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 22 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6 }, !ChkHideInfoInGrd.Checked);
        }
    

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsLueEmpty(LueTaxInd, "Status") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                
                var cm = new MySqlCommand();

                string Selected = mFrmParent.GetSelectedItemSO();
                

                if (LueTaxInd.Text=="PPN")
                    Filter = " And B.TaxInd = 'Y' ";
                else
                    Filter = " And B.TaxInd = 'N' "; 

                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParam<String>(ref cm, "@SelectedItemSO", Selected);
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItCodeInternal", "D.ItName" });
                

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                        new string[]
                        {

                            //0
                            "SAName", 
                            //1-5
                            "ItCode", "ItName", "ItCodeInternal", "PackagingUnitUomCode", "QtyPackagingUnit",  
                            //6-10
                            "OutstandingQtyPackagingUnit", "DocNo", "DocDt", "SalesMemoDNo", "Qty",
                            //11-15
                            "OutstandingQty", "PriceUomCode", "InventoryUomCode", "DeliveryDt", "VoucherDocNo",
                            //16-20
                            "PriceAfterTax", "LocalDocNo", "CtPONo", "ForeignName", "UsedItem",
                            //21-24
                            "SalesMemoDocNo", "SalesUoMCode", "TaxInd","LocalDocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);//ItCode
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);//ItName
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);//LocalCode
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);//Packaging Uom
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);//QtyPackaging
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 11);//OutstandingQty
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);//SCDocNo
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 8);//SCDocDt
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);//SMDNo
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);//QtySales
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);//OutstandingQtySales
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 22);//UomSales
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);//UomInventory
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 14);//DeliveryDt
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);//VoucherDocNo
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);//PriceAfterTax
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);//LocalDocument#
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);//CtPONo
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);//ForeignName
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);//AllowQty
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);//SalesMemo#
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);//PPN
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        protected override void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;
                        if (!IsItemAlreadyChosen(Row))
                        {
                            Row1 = mFrmParent.Grd1.Rows.Count - 1;
                            Row2 = Row;
                            if (!IsDocumentPPNAlreadyChosen())
                            {
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);//itCode
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 4);//itname
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);//localcode
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 9);//sc#
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 12);//SMDNo
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 15);//packagingunit
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 8);//oustandingpackagingunit
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 7);//packagingqty
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 14);//outstandingsalesmemo
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 13);//requestQtySales
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 15);//UomSales
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 13);//RequestQtyInventory
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 16);//UomInventory
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 17);//DeliveryDt
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 22);//ForeignName
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 8);//Allowance
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 25);//SM#
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 23);//UsedItem
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 26);//TaxInd
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 38, Grd1, Row2, 20);//Local#

                                mFrmParent.Grd1.Rows.Add();
                            }
                            Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 9, 11, 10, 12, 13, 14, 17, 21 });
                        }
                        else
                        {
                            Sm.StdMsg(mMsgType.Warning,
                            "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                            "This item has already chosen.");
                        }
                    }
                }
                mFrmParent.ComputeUom();
                mFrmParent.ComputeAllowanceQty();
                mFrmParent.GenerateLocalDocNo();
                mFrmParent.Grd1.EndUpdate();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");


        }

        private bool IsItemAlreadyChosen(int Row)
        {
            var Item =
                Sm.GetGrdStr(Grd1, Row, 2) +
                Sm.GetGrdStr(Grd1, Row, 9) +
                Sm.GetGrdStr(Grd1, Row, 25) +
                Sm.GetGrdStr(Grd1, Row, 12);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 1) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 5) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 29) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 7),
                    Item
                    )) return true;
            return false;
        }

        private bool IsDocumentPPNAlreadyChosen()
        {
            int Index = 0;
            if (Sm.GetGrdStr(mFrmParent.Grd1, Index, 2).Length > 0 && Sm.GetGrdStr(mFrmParent.Grd1, Index, 31) == "Y" && LueTaxInd.Text == "Non PPN")
            {
                Sm.StdMsg(mMsgType.Warning, "You can't choose Non PPN Document If PPN Document already chosen. ");

                return true;
            }
            if (Sm.GetGrdStr(mFrmParent.Grd1, Index, 2).Length > 0 && Sm.GetGrdStr(mFrmParent.Grd1, Index, 31) == "N" && LueTaxInd.Text == "PPN")
            {
                Sm.StdMsg(mMsgType.Warning, "You can't choose PPN Document If Non PPN Document already chosen. ");

                return true;
            }
                return false;
        }
        private void SetLueTaxInd(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select 'PPN' Col1, 'PPN' Col2  Union All select 'Non PPN' Col1, 'Non PPN' Col2 ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
       

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSalesContract(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmSalesContract(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueTaxInd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaxInd, new Sm.RefreshLue1(SetLueTaxInd));
        }

        #endregion
    }
}
