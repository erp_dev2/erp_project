﻿#region Update
/*
    10/02/2023 [WED/HEX] new apps : https://docs.google.com/spreadsheets/d/1mkaGz3-9mnHk_0eWMgSVDWN40BHhUaW6/edit#gid=702018512
    14/02/2023 [WED/HEX] validasi dept sama dihilangkan
    15/02/2023 [DITA/HEX] ubah method upload file dari update jadi insert, tambah param mIsUploadFileRenamed
    15/02/2023 [WED/HEX] method update budget summary dibuat public agar bisa dipanggil di docapproval
    06/03/2023 [WED/HEX] tambahan validasi ke Closing Procurement berdasarkan tahun fiskal
    14/03/2023 [WED/HEX] compute remaining budget pakai method baru khusus HEX
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.IO;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetTransfer : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mDocNo = string.Empty
            ;
        internal FrmBudgetTransferFind FrmFind;
        internal bool
            mIsFilterByDept = false,
            mIsUploadFileRenamed = false
            ;

        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty
            ;

        private bool
            mIsBudgetTransferAllowToUploadFile = false
            ;

        iGCell fCell;
        bool fAccept;

        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmBudgetTransfer(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Budget Transfer";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                ExecQuery();
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
                Sl.SetLueDeptCode(ref LueDeptCode2, string.Empty, mIsFilterByDept ? "Y" : "N");
                Sl.SetLueBCCode(ref LueBCCode, string.Empty, Sm.GetLue(LueDeptCode));
                Sl.SetLueBCCode(ref LueBCCode2, string.Empty, Sm.GetLue(LueDeptCode2));
                LueBCCode.Visible = LueBCCode2.Visible = false;

                if (!mIsBudgetTransferAllowToUploadFile) Tc1.TabPages.Remove(Tp2);

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsFilterByDept', 'PortForFTPClient', 'HostAddrForFTPClient', 'SharedFolderForFTPClient', ");
            SQL.AppendLine("'UsernameForFTPClient', 'PasswordForFTPClient', 'FileSizeMaxUploadFTPClient', 'FormatFTPClient', ");
            SQL.AppendLine("'IsBudgetTransferAllowToUploadFile', 'IsUploadFileRenamed' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsBudgetTransferAllowToUploadFile": mIsBudgetTransferAllowToUploadFile = ParValue == "Y"; break;
                            case "IsUploadFileRenamed": mIsUploadFileRenamed = ParValue == "Y"; break;

                            //string
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;

                            //Integer
                        }
                    }
                }
                dr.Close();
            }

            if (mFormatFTPClient.Length == 0) mFormatFTPClient = "1";
        }

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "DNo",
                        
                    //1-5
                    "Cancel",
                    "Old Cancel",
                    "Reason for Cancellation",
                    "BCCode",
                    "From Category",
                        
                    //6-10
                    "BCCode2",
                    "To Category",
                    "Transfer Amount",
                    "Remark",
                    "Remaining Budget"
                }, new int[]
                {
                    //0
                    0,
                        
                    //1-5
                    50, 50, 200, 0, 180,
                        
                    //6-10
                    0, 180, 150, 200, 0
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 6, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2, 3, 4, 6, 10 });

            #endregion

            #region Grid 2 - Approval Info

            Grd2.Cols.Count = 5;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "No",

                    //1-4
                    "User",
                    "Status",
                    "Date",
                    "Remark"
                },
                new int[] { 50, 150, 200, 100, 200 }
            );
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 2, 3, 4 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, LueDeptCode, LueDeptCode2, MeeCancelReason, 
                        TxtDocNoInternal, MeeRemark, LueBCCode, LueBCCode2, TxtFile
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = ChkFile.Properties.ReadOnly = true;
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = TxtDocNo.Text.Length > 0 && mIsBudgetTransferAllowToUploadFile;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 5, 7, 8, 9 });
                    TxtDocNo.Focus();
                    break;

                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, LueDeptCode, LueDeptCode2, TxtDocNoInternal, LueBCCode, LueBCCode2, MeeRemark
                    }, false);
                    ChkCancelInd.Checked = ChkFile.Checked = false;
                    ChkFile.Properties.ReadOnly = false;
                    BtnFile.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 5, 7, 8, 9 });
                    DteDocDt.Focus();
                    break;

                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        MeeCancelReason
                    }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, TxtStatus, LueDeptCode, LueDeptCode2,
                TxtDocNoInternal, MeeRemark, LueBCCode, LueBCCode2, MeeCancelReason, TxtFile
            });
            ChkCancelInd.Checked = false;
            ChkFile.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 10 });
            Sm.ClearGrd(Grd2, false);
        }

        private void ClearGrd2()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; ++Row)
            {
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0 && Sm.GetGrdStr(Grd1, Row, 6).Length > 0)
                {
                    Grd1.Cells[Row, 6].Value = Grd1.Cells[Row, 7].Value = null;
                }
            }
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBudgetTransferFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                //if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                //    Sm.StdMsgYN("Print", "") == DialogResult.No
                //    ) return;
                //ParPrint((int)mNumberOfInventoryUomCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (Sm.IsGrdColSelected(new int[] { 5, 7, 8, 9 }, e.ColIndex))
                    {
                        if (e.ColIndex == 5) Sm.LueRequestEdit(ref Grd1, ref LueBCCode, ref fCell, ref fAccept, e);
                        if (e.ColIndex == 7) Sm.LueRequestEdit(ref Grd1, ref LueBCCode2, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8 });
                    }
                }
                else
                {
                    if (!(
                        (e.ColIndex == 1 || e.ColIndex == 3) &&
                        !Sm.GetGrdBool(Grd1, e.RowIndex, 2) &&
                        Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0
                        ))
                        e.DoDefault = false;
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmt();
            }

            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 8, 10 }, e);

                if (Sm.IsGrdColSelected(new int[] { 1, 3, 8 }, e.ColIndex))
                {
                    if (e.ColIndex == 3)
                    {
                        if (Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length > 0)
                        {
                            Grd1.Cells[e.RowIndex, 1].Value = true;
                        }
                    }

                    if (e.ColIndex == 1)
                    {
                        if (!Sm.GetGrdBool(Grd1, e.RowIndex, 1))
                        {
                            Grd1.Cells[e.RowIndex, 3].Value = string.Empty;
                        }
                    }

                    ComputeAmt();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BudgetTransfer", "TblBudgetTransferHdr");
            var cml = new List<MySqlCommand>();

            cml.Add(SaveBudgetTransferHdr(DocNo));
            cml.Add(SaveBudgetTransferDtl(DocNo));
            cml.Add(UpdateBudgetSummary(DocNo, Sm.Left(Sm.GetDte(DteDocDt), 8), true));

            Sm.ExecCommands(cml);

            ProcessUploadFile(DocNo);

            ShowData(DocNo);
        }

        private void ProcessUploadFile(string DocNo)
        {
            if (TxtFile.Text.Length > 0 && mIsBudgetTransferAllowToUploadFile)
            {
                #region init data
                string FileName = TxtFile.Text;
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
                //string fName = Path.GetFileNameWithoutExtension(toUpload.Name);
                string fExt = Path.GetExtension(toUpload.Name);
                string DNo = "000";
                string dir = "budget_transfer/";
                string RenameTo = string.Concat(dir, DocNo.Replace('/', '-'), "_", DNo, fExt);
                #endregion

                var l = new List<Sm.UploadFileClass>();

                l.Add(new Sm.UploadFileClass()
                {
                    DocNo = DocNo,
                    DNo = DNo,
                    FileName = mIsUploadFileRenamed ? RenameTo : toUpload.Name
                }) ;

                bool isSuccessUpload = Sm.UploadFile(mIsBudgetTransferAllowToUploadFile, FileName, mFormatFTPClient, RenameTo, mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, mUsernameForFTPClient, mPasswordForFTPClient, mIsUploadFileRenamed);
                if (isSuccessUpload) Sm.InsertUploadedFile(mIsBudgetTransferAllowToUploadFile, ref l, "TblBudgetTransferFile");

                l.Clear();
            }
        }

        public MySqlCommand UpdateBudgetSummary(string DocNo, string DocDt, bool IsInsert)
        {
            // DNo berkoma
            var SQL = new StringBuilder();
            string FiscalYrRange = Sm.GetParameter("FiscalYearRange");
            string YrBudget = Sm.Left(DocDt, 4);

            if (FiscalYrRange.Length > 0)
            {
                string[] period = FiscalYrRange.Split(',');
                string period1 = string.Concat(Sm.Left(DocDt, 4), period[0]);

                if (Int32.Parse(Sm.Left(DocDt, 6)) < Int32.Parse(period1))
                {
                    YrBudget = (Int32.Parse(Sm.Left(DocDt, 4)) - 1).ToString();
                }
            }

            SQL.AppendLine("Insert Ignore Into TblBudgetSummary(Yr, Mth, DeptCode, BCCode, Amt1, Amt2, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @YrBudget, '00', A.DeptCode, B.BCCode, 0.00, 0.00, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblBudgetTransferHdr A ");
            SQL.AppendLine("Inner Join TblBudgetTransferDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("    And A.Status = 'A' ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Insert Ignore Into TblBudgetSummary(Yr, Mth, DeptCode, BCCode, Amt1, Amt2, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @YrBudget, '00', A.DeptCode2, B.BCCode2, 0.00, 0.00, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblBudgetTransferHdr A ");
            SQL.AppendLine("Inner Join TblBudgetTransferDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("    And A.Status = 'A' ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblBudgetSummary A ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select @YrBudget As Yr, T1.DeptCode, T2.BCCode, Sum(T2.Amt) Amt ");
            SQL.AppendLine("    From TblBudgetTransferHdr T1 ");
            SQL.AppendLine("    Inner Join TblBudgetTransferDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DocNo = @DocNo ");
            SQL.AppendLine("        And T1.Status = 'A' ");
            SQL.AppendLine("    Group By Left(T1.DocDt, 4), T1.DeptCode, T2.BCCode ");
            SQL.AppendLine(") B On A.Yr = B.Yr And A.DeptCode = B.DeptCode And A.BCCode = B.BCCode ");

            SQL.AppendLine("Set A.Amt2 = A.Amt2 ");

            if (IsInsert) SQL.AppendLine(" - ");
            else SQL.AppendLine(" + ");
            
            SQL.AppendLine("B.Amt ");
            SQL.AppendLine(", A.LastUpBy = @CreateBy, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblBudgetSummary A ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select @YrBudget As Yr, T1.DeptCode2, T2.BCCode2, Sum(T2.Amt) Amt ");
            SQL.AppendLine("    From TblBudgetTransferHdr T1 ");
            SQL.AppendLine("    Inner Join TblBudgetTransferDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DocNo = @DocNo ");
            SQL.AppendLine("        And T1.Status = 'A' ");
            SQL.AppendLine("    Group By Left(T1.DocDt, 4), T1.DeptCode2, T2.BCCode2 ");
            SQL.AppendLine(") B On A.Yr = B.Yr And A.DeptCode = B.DeptCode2 And A.BCCode = B.BCCode2 ");

            SQL.AppendLine("Set A.Amt2 = A.Amt2 ");

            if (IsInsert) SQL.AppendLine(" + ");
            else SQL.AppendLine(" - ");

            SQL.AppendLine("B.Amt ");
            SQL.AppendLine(", A.LastUpBy = @CreateBy, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@YrBudget", YrBudget);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueDeptCode, "From Department") ||
                Sm.IsLueEmpty(LueDeptCode2, "To Department") ||
                Sm.IsClosingProcurementInvalidDefault(Sm.GetDte(DteDocDt)) ||
                //IsDepartmentDuplicate() ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsUploadFileInvalid();
        }

        private bool IsDepartmentDuplicate()
        {
            if (Sm.GetLue(LueDeptCode) == Sm.GetLue(LueDeptCode2))
            {
                Sm.StdMsg(mMsgType.Warning, "You could not transfer between the same department.");
                LueDeptCode2.Focus();
                return true;
            }

            return false;
        }

        private bool IsUploadFileInvalid()
        {
            if (!mIsBudgetTransferAllowToUploadFile) return false;

            string FileName = TxtFile.Text;

            if (FileName.Length == 0) { Sm.StdMsg(mMsgType.Warning, "There is no file to upload."); return true; }

            FileInfo f = new FileInfo(FileName);

            if (Sm.IsFTPClientDataNotValid(mIsBudgetTransferAllowToUploadFile, FileName, mHostAddrForFTPClient, mSharedFolderForFTPClient, mUsernameForFTPClient, mPortForFTPClient) ||
                Sm.IsFileSizeNotValid(mIsBudgetTransferAllowToUploadFile, FileName, false, 0, ref f, mFileSizeMaxUploadFTPClient) ||
                Sm.IsFileNameAlreadyExisted(mIsBudgetTransferAllowToUploadFile, FileName, "TblBudgetTransferFile", "FileName"))
            {
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 budget transfer detail.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {             
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 5, false, "From Category is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 7, false, "From Category is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 8, true, "Transfer amount is zero.")) return true;

                if (Sm.GetGrdStr(Grd1, Row, 4) == Sm.GetGrdStr(Grd1, Row, 6))
                {
                    Sm.StdMsg(mMsgType.Warning, "Could not transfer on the same category.");
                    Sm.FocusGrd(Grd1, Row, 7);
                    return true;
                }

                for (int Row2 = Row+1; Row2 < Grd1.Rows.Count; ++Row2)
                {
                    if (string.Concat(Sm.GetGrdStr(Grd1, Row, 4), Sm.GetGrdStr(Grd1, Row, 6)) ==
                        string.Concat(Sm.GetGrdStr(Grd1, Row2, 4), Sm.GetGrdStr(Grd1, Row2, 6))
                        )
                    {
                        Sm.StdMsg(mMsgType.Warning, "Could not transfer on duplicate data.");
                        Sm.FocusGrd(Grd1, Row2, 7);
                        return true;
                    }
                }

                ReComputeRemainingBudget(Row);
                if (Sm.GetGrdDec(Grd1, Row, 8) > Sm.GetGrdDec(Grd1, Row, 10))
                {
                    var Msg = new StringBuilder();

                    Msg.AppendLine("Transferred Amount : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 8), 0));
                    Msg.AppendLine("Remaining Amount :  " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 10), 0));
                    Msg.AppendLine(Environment.NewLine);
                    Msg.AppendLine("The amount to be transferred is exceed remaining budget. ");

                    Sm.StdMsg(mMsgType.Warning, Msg.ToString());
                    Sm.FocusGrd(Grd1, Row, 8);
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveBudgetTransferHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* SaveBudgetTransferHdr */ ");

            SQL.AppendLine("Insert Into TblBudgetTransferHdr(DocNo, DocDt, Status, DocNoInternal, ");
            SQL.AppendLine("DeptCode, DeptCode2, ");
            SQL.AppendLine("Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @DocNoInternal, ");
            SQL.AppendLine("@DeptCode, @DeptCode2, ");
            SQL.AppendLine("@Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='BudgetTransfer' ");
            SQL.AppendLine("And T.DeptCode = @DeptCode ");
            SQL.AppendLine("And (T.StartAmt = 0 ");
            SQL.AppendLine("Or T.StartAmt <= @Amt); ");

            SQL.AppendLine("Update TblBudgetTransferHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='BudgetTransfer' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@DeptCode2", Sm.GetLue(LueDeptCode2));
            Sm.CmParam<String>(ref cm, "@DocNoInternal", TxtDocNoInternal.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBudgetTransferDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* SaveBudgetTransferDtl */ ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblBudgetTransferDtl(DocNo, DNo, BCCode, BCCode2, Amt, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                    {
                        SQL.AppendLine(", ");
                    }

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @BCCode_" + r.ToString() +
                        ", @BCCode2_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @CreateBy, CurrentDateTime()) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@BCCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                    Sm.CmParam<String>(ref cm, "@BCCode2_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 8));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 9));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsEditedDataNotValid())
            {
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateBudgetTransferHdr(TxtDocNo.Text));
            cml.Add(UpdateBudgetSummary(TxtDocNo.Text, Sm.Left(Sm.GetDte(DteDocDt), 8), false));
            
            Sm.ExecCommands(cml);
            
            ShowData(TxtDocNo.Text);
            
            cml.Clear();
        }

        private bool IsEditedDataNotValid()
        {
            return
                IsBudgetTransferStatusCancelled() ||
                IsCancelledItemNotExisted() ||
                Sm.IsClosingProcurementInvalidDefault(Sm.GetDte(DteDocDt));
        }

        private bool IsBudgetTransferStatusCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 from TblBudgetTransferHdr Where DocNo=@Param And (Status='C' Or CancelInd = 'Y');",
                TxtDocNo.Text,
                "Document is already cancelled.");
        }        

        private bool IsCancelledItemNotExisted()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private MySqlCommand UpdateBudgetTransferHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBudgetTransferHdr Set ");
            SQL.AppendLine("    CancelInd = 'Y', CancelReason = @CancelReason, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo And CancelInd = 'N' ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowBudgetTransferHdr(DocNo);
                ShowBudgetTransferDtl(DocNo);
                ShowUploadedFile(DocNo);
                Sm.ShowDocApproval(DocNo, "BudgetTransfer", ref Grd2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBudgetTransferHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' Else '' End As StatusDesc, ");
            SQL.AppendLine("A.CancelReason, A.CancelInd, A.DocNoInternal, A.DeptCode, A.DeptCode2, A.Amt, A.Remark ");
            SQL.AppendLine("From TblBudgetTransferHdr A ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "StatusDesc", "CancelReason", "CancelInd", "DocNoInternal",  

                    //6-9
                    "DeptCode", "DeptCode2", "Amt", "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                    TxtDocNoInternal.EditValue = Sm.DrStr(dr, c[5]);
                    Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[6]));
                    Sm.SetLue(LueDeptCode2, Sm.DrStr(dr, c[7]));
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                }, true
            );
        }

        private void ShowBudgetTransferDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.BCCode, B.BCName, A.BCCode2, C.BCName BCName2, A.Amt, A.Remark ");
            SQL.AppendLine("From TblBudgetTransferDtl A ");
            SQL.AppendLine("Inner Join TblBudgetCategory B On A.BCCode = B.BCCode ");
            SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode2 = C.BCCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 

                    //1-5
                    "BCCode", "BCName", "BCCode2", "BCName2", "Amt", 
                    
                    //6
                    "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 10 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowUploadedFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select FileName ");
            SQL.AppendLine("From TblBudgetTransferFile ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("; ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]  { "FileName" },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtFile.EditValue = Sm.DrStr(dr, c[0]);
                }, false
            );
        }

        #endregion

        #region Additional Method

        private void ReComputeRemainingBudget(int Row)
        {
            string DocNo = TxtDocNo.Text;
            string DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8);
            Grd1.Cells[Row, 10].Value = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
            {
                string DeptCode = Sm.GetLue(LueDeptCode);
                string BCCode = Sm.GetGrdStr(Grd1, Row, 4);
                decimal AvailableBudget = Sm.ComputeAvailableBudget2(DocNo, DocDt, string.Empty, DeptCode, BCCode);

                if (AvailableBudget != 0m) Grd1.Cells[Row, 10].Value = AvailableBudget;
            }
        }

        private void ComputeAmt()
        {
            decimal Amt = 0;
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 4).Length > 0)
                {
                    Amt += Sm.GetGrdDec(Grd1, i, 8);
                }
            }

            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            //create table
            SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tblbudgettransferhdr` ( ");
            SQL.AppendLine("    `DocNo` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DocDt` VARCHAR(8) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `Status` VARCHAR(1) NOT NULL DEFAULT 'A' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CancelInd` VARCHAR(1) NOT NULL DEFAULT 'N' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CancelReason` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DocNoInternal` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DeptCode` VARCHAR(60) NOT NULL COMMENT 'Department From' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DeptCode2` VARCHAR(60) NOT NULL COMMENT 'Department To' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `Amt` DECIMAL(20, 6) NOT NULL DEFAULT '0.000000', ");
            SQL.AppendLine("    `Remark` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateBy` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateDt` VARCHAR(12) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpBy` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpDt` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    PRIMARY KEY(`DocNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `DocNoInternal` (`DocNoInternal`) USING BTREE, ");
            SQL.AppendLine("   INDEX `DocNo` (`DocNo`, `DocDt`, `Status`, `DeptCode`, `DeptCode2`, `CreateDt`, `CancelInd`) USING BTREE ");
            SQL.AppendLine(") ");
            SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            SQL.AppendLine("ENGINE = InnoDB ");
            SQL.AppendLine("; ");

            SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tblbudgettransferdtl` ( ");
            SQL.AppendLine("    `DocNo` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DNo` VARCHAR(3) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `BCCode` VARCHAR(60) NOT NULL COMMENT 'Budget Category From' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `BCCode2` VARCHAR(60) NOT NULL COMMENT 'Budget Category To' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `Amt` DECIMAL(20, 6) NOT NULL DEFAULT '0.000000' COMMENT 'Total Transferred', ");
            SQL.AppendLine("    `Remark` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateBy` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateDt` VARCHAR(12) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpBy` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpDt` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    PRIMARY KEY(`DocNo`, `DNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `DocNo` (`DocNo`, `DNo`, `BCCode`, `BCCode2`) USING BTREE ");
            SQL.AppendLine(") ");
            SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            SQL.AppendLine("ENGINE = InnoDB ");
            SQL.AppendLine("; ");

            SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tblbudgettransferfile` ( ");
            SQL.AppendLine("    `DocNo` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DNo` VARCHAR(3) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `FileName` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateBy` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateDt` VARCHAR(12) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpBy` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpDt` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    PRIMARY KEY(`DocNo`, `DNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `DocNo` (`DocNo`, `DNo`, `FileName`, `CreateDt`) USING BTREE ");
            SQL.AppendLine(") ");
            SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            SQL.AppendLine("ENGINE = InnoDB ");
            SQL.AppendLine("; ");


            //insert data
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('IsBudgetTransferAllowToUploadFile', 'Apakah Budget Transfer di ijinkan untuk upload file ? [Y = Ya, N = Tidak]', 'N', 'HEX', NULL, 'Y', 'WEDHA', '202302131400', 'HAR', '201810031151'); ");

            Sm.ExecQuery(SQL.ToString());
        }

        #endregion

        #endregion

        #region Events

        #region Button Click

        private void BtnFile_Click(object sender, EventArgs e)
        {
            Sm.ChooseUploadedFile(ref ChkFile, ref OD, ref TxtFile);
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtFile, "File", false))
            {
                bool isSuccessDownload = Sm.DownloadFileWithProgressBar(ref downloadedData, ref PbUpload, mFormatFTPClient, mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                if (isSuccessDownload) Sm.DownloadAction(ref SFD, ref downloadedData, TxtFile.Text);
            }
        }

        #endregion

        #region Misc Control Events

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
                ClearGrd();
                Sl.SetLueBCCode(ref LueBCCode, string.Empty, Sm.GetLue(LueDeptCode));
            }
        }

        private void LueDeptCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode2, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
                ClearGrd2();
                Sl.SetLueBCCode(ref LueBCCode2, string.Empty, Sm.GetLue(LueDeptCode2));
            }
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue3(Sl.SetLueBCCode), string.Empty, Sm.GetLue(LueDeptCode));
            }
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (!ChkFile.Checked)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void LueBCCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueBCCode2, new Sm.RefreshLue3(Sl.SetLueBCCode), string.Empty, Sm.GetLue(LueDeptCode2));
            }
        }

        private void LueBCCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBCCode_Leave(object sender, EventArgs e)
        {
            if (LueBCCode.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueBCCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 4].Value = Grd1.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueBCCode);
                    Grd1.Cells[fCell.RowIndex, 5].Value = LueBCCode.GetColumnValue("Col2");
                }
                LueBCCode.Visible = false;
            }
        }

        private void LueBCCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBCCode2_Leave(object sender, EventArgs e)
        {
            if (LueBCCode2.Visible && fAccept && fCell.ColIndex == 7)
            {
                if (Sm.GetLue(LueBCCode2).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 6].Value = Grd1.Cells[fCell.RowIndex, 7].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueBCCode2);
                    Grd1.Cells[fCell.RowIndex, 7].Value = LueBCCode2.GetColumnValue("Col2");
                }
                LueBCCode2.Visible = false;
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #endregion

        #region Class

        private class RemainingBudget
        {
            public string BCCode { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion
    }
}
