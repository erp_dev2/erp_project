﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptFinancePerformance : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private string DocDt = string.Empty;
        #endregion

        #region Constructor

        public FrmRptFinancePerformance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();   
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                SetLueTransaksi(ref LueTrxCode);
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSQL(string TypeTrx)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * from ( ");
            // PI
            if (TypeTrx == "1")
            {
                SQL.AppendLine("Select X.MonthFilter, X.DocDt, X.Username, group_Concat(Distinct X.DocNo)As DocNo, ");
                SQL.AppendLine("Count(X.ItCode) As TotDocument, X.ItCode, X.Itname, X.ForeignName, X.Totalday, "); 
                SQL.AppendLine("Round(Count(X.ItCode) / Right(Last_day(X.DocDt), 2), 2) AS Avgday, X.DocType ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("Select Left(A.DocDt, 6) As MonthFilter, A.DocDt, E.Username, A.DocNo, ");
                SQL.AppendLine("C.ItCode, D.Itname, D.ForeignName, Right(Last_day(A.DocDt), 2) AS Totalday, '1' as DocType");
                SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblRecvVdDtl C On B.RecvVdDocNo=C.DocNo And B.RecvVdDNo=C.DNo ");
                SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("Inner Join TblUser E On A.CreateBy = E.UserCode ");
                SQL.AppendLine("Where A.CancelInd = 'N' And Left(A.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group By A.DocNo ");
                SQL.AppendLine(")X ");
                SQL.AppendLine("Group by X.ItCode ");
            }
            // OP
            if (TypeTrx == "2")
            {
                SQL.AppendLine("Select X1.MonthFilter, X1.DocDt, X1.UserName, Group_Concat(Distinct X1.DocNo) As DocNo, ");
                SQL.AppendLine("Count(X1.ItCode)As TotDocument, X1.ItCode,X1.Itname, X1.ForeignName, X1.Totalday, ");
                SQL.AppendLine("Round(Count(X1.ItCode) / Right(Last_day(X1.DocDt), 2), 2) AS Avgday, X1.DocType ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("Select Left(Z.DocDt, 6) As MonthFilter, Z.DocDt, E.Username, Z.DocNo, ");
                SQL.AppendLine("C.ItCode, D.Itname, D.ForeignName, Right(Last_day(Z.DocDt), 2) AS Totalday, '2' as DocType "); 
                SQL.AppendLine("From tbloutgoingpaymenthdr Z ");
                SQL.AppendLine("Inner Join tbloutgoingpaymentdtl Y On Z.DocNo=Y.DocNo ");
                SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr A  On Y.InvoiceDocNo=A.DocNo "); 
                SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblRecvVdDtl C On B.RecvVdDocNo=C.DocNo And B.RecvVdDNo=C.DNo ");
                SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode "); 
                SQL.AppendLine("Inner Join TblUser E On A.CreateBy = E.UserCode ");
                SQL.AppendLine("Where A.CancelInd = 'N' And Left(Z.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group By Z.DocNo ");
                SQL.AppendLine(")X1 ");
                SQL.AppendLine("Group by X1.ItCode");
            }
            //VR
            if (TypeTrx == "3")
            {
                SQL.AppendLine("Select X1.MonthFilter, X1.DocDt, X1.Username, Group_Concat(Distinct X1.DocNo) As DocNo, ");
                SQL.AppendLine("Count(X1.ItCode)As TotDocument, X1.ItCode, X1.Itname, X1.ForeignName, ");
                SQL.AppendLine("X1.Totalday, Round(Count(X1.ItCode) / Right(Last_day(X1.DocDt), 2), 2) AS Avgday, X1.DocType ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("Select Left(A.DocDt, 6) As MonthFilter, A.DocDt, K.Username, A.DocNo,"); 
                SQL.AppendLine("E.ItCode, J.Itname, J.ForeignName, Right(Last_day(A.DocDt), 2) AS Totalday, "); 
                SQL.AppendLine("Round(Count(E.ItCode) / Right(Last_day(A.DocDt), 2), 2) AS Avgday, '3' as DocType ");
                SQL.AppendLine("From tblvoucherRequesthdr A ");
                SQL.AppendLine("Inner Join tbloutgoingpaymenthdr B On A.DocNo=B.VoucherRequestDocNo ");
                SQL.AppendLine("Inner Join tbloutgoingpaymentdtl X On B.DocNo=X.DocNo ");
                SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr C  On X.InvoiceDocNo=C.DocNo ");
                SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl D On C.DocNo=D.DocNo ");
                SQL.AppendLine("Inner Join TblRecvVdDtl E On D.RecvVdDocNo=E.DocNo And D.RecvVdDNo=E.DNo ");
                SQL.AppendLine("Inner Join TblItem J On E.ItCode=J.ItCode ");
                SQL.AppendLine("Inner Join TblUser K On A.CreateBy = K.UserCode ");
                SQL.AppendLine("Where A.CancelInd = 'N' And Left(A.DocDt, 6) = @MonthFilter And A.DocType='03' ");
                SQL.AppendLine("Group By A.DocNo ");
                SQL.AppendLine(")X1 ");
                SQL.AppendLine("Group by X1.ItCode ");
            }
            //VC
            if (TypeTrx == "4")
            {
                SQL.AppendLine("Select X1.MonthFilter, X1.DocDt, X1.Username, Group_Concat(Distinct X1.DocNo) As DocNo,");
                SQL.AppendLine("Count(X1.ItCode)As TotDocument, X1.ItCode, X1.Itname, X1.ForeignName, ");
                SQL.AppendLine("X1.Totalday, Round(Count(X1.ItCode) / Right(Last_day(X1.DocDt), 2), 2) AS Avgday, X1.DocType ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("Select Left(Z.DocDt, 6) As MonthFilter, Z.DocDt, K.Username, Z.DocNo,");
                SQL.AppendLine("E.ItCode, J.Itname, J.ForeignName, Right(Last_day(Z.DocDt), 2) AS Totalday, '4' as DocType "); 
                SQL.AppendLine("From tblVoucherhdr Z ");
                SQL.AppendLine("Inner Join tblvoucherRequesthdr A On Z.VoucherRequestDocNo=A.DocNo ");
                SQL.AppendLine("Inner Join tbloutgoingpaymenthdr B On A.DocNo=B.VoucherRequestDocNo ");
                SQL.AppendLine("Inner Join tbloutgoingpaymentdtl Y On B.DocNo=Y.DocNo ");
                SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr C  On Y.InvoiceDocNo=C.DocNo "); 
                SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl D On C.DocNo=D.DocNo ");
                SQL.AppendLine("Inner Join TblRecvVdDtl E On D.RecvVdDocNo=E.DocNo And D.RecvVdDNo=E.DNo ");
                SQL.AppendLine("Inner Join TblItem J On E.ItCode=J.ItCode ");
                SQL.AppendLine("Inner Join TblUser K On A.CreateBy = K.UserCode ");
                SQL.AppendLine("Where A.CancelInd = 'N' And Left(Z.DocDt, 6) = @MonthFilter And A.DocType='03' ");
                SQL.AppendLine("Group By Z.DocNo ");
                SQL.AppendLine(")X1 ");
                SQL.AppendLine("Group by X1.ItCode ");
            }
           
            SQL.AppendLine(")X ");
            SQL.AppendLine("Where X.MonthFilter=@MonthFilter ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item's Code",
                        "",
                        "User", 
                        "Total"+Environment.NewLine+"Document",
                        "Document",
                        
                        //6-8
                        "Item Name",
                        "Foreign Name",
                        "Average Usage Day"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        120, 20, 150, 80, 200,
                        //6-8
                        180, 200, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { }, false);
            Sm.GrdColButton(Grd1, new int[] {2}); 
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Month = Sm.GetLue(LueMth);
            DocDt = string.Concat(Year, Month);

            if (Year == string.Empty && Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month And Year is Empty.");
                return;
            }
            else if (Year == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
                return;
            }
            else if (Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
                return;
            }

            if (Sm.IsLueEmpty(LueTrxCode, "Transaction type")) return;
            
            try
            {
                
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@MonthFilter", DocDt);
                Sm.FilterStr(ref Filter, ref cm, TxtUserCode.Text, new string[] { "X.UserName" });
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "X.ItName", "X.ForeignName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By X.UserName, X.ItName;",
                    new string[] 
                    { 
                        "ItCode", 
                        "UserName", "TotDocument", "DocNo", "ItName", "ForeignName", 
                        "AvgDay" 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    }, true, false, false, false
                    );
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }

        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 8 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        public static void SetLueTransaksi(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Purchase Invoice' As Col2 " +
                "Union All " +
                "Select '2' As Col1, 'Outgoing Payment' As Col2 " +
                "Union All " +
                "Select '3' As Col1, 'Voucher Request' As Col2 " +
                "Union All " +
                "Select '4' As Col1, 'Voucher' As Col2 ",
               
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void ChkUserCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Username");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtUserCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueTrxCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrxCode, new Sm.RefreshLue1(SetLueTransaksi));
            if (Sm.GetLue(LueTrxCode).Length > 0)
                SetSQL(Sm.GetLue(LueTrxCode));
        }

       

        #endregion    
    }
}
