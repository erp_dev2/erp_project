﻿namespace RunSystem
{
    partial class FrmDirectorateGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblDirectorateGroupCode = new System.Windows.Forms.Label();
            this.LblDirectorateGroupName = new System.Windows.Forms.Label();
            this.TxtDirectorateGroupCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtDirectorateGroupName = new DevExpress.XtraEditors.TextEdit();
            this.LblParent = new System.Windows.Forms.Label();
            this.LueParentCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkActive = new DevExpress.XtraEditors.CheckEdit();
            this.ChkParent = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectorateGroupCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectorateGroupName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParentCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkParent.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(581, 0);
            this.panel1.Size = new System.Drawing.Size(70, 164);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkParent);
            this.panel2.Controls.Add(this.ChkActive);
            this.panel2.Controls.Add(this.LueParentCode);
            this.panel2.Controls.Add(this.LblParent);
            this.panel2.Controls.Add(this.TxtDirectorateGroupName);
            this.panel2.Controls.Add(this.TxtDirectorateGroupCode);
            this.panel2.Controls.Add(this.LblDirectorateGroupName);
            this.panel2.Controls.Add(this.LblDirectorateGroupCode);
            this.panel2.Size = new System.Drawing.Size(581, 164);
            // 
            // LblDirectorateGroupCode
            // 
            this.LblDirectorateGroupCode.AutoSize = true;
            this.LblDirectorateGroupCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDirectorateGroupCode.ForeColor = System.Drawing.Color.Black;
            this.LblDirectorateGroupCode.Location = new System.Drawing.Point(12, 10);
            this.LblDirectorateGroupCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDirectorateGroupCode.Name = "LblDirectorateGroupCode";
            this.LblDirectorateGroupCode.Size = new System.Drawing.Size(137, 14);
            this.LblDirectorateGroupCode.TabIndex = 9;
            this.LblDirectorateGroupCode.Text = "Directorate Group Code";
            this.LblDirectorateGroupCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblDirectorateGroupName
            // 
            this.LblDirectorateGroupName.AutoSize = true;
            this.LblDirectorateGroupName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDirectorateGroupName.ForeColor = System.Drawing.Color.Red;
            this.LblDirectorateGroupName.Location = new System.Drawing.Point(9, 31);
            this.LblDirectorateGroupName.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDirectorateGroupName.Name = "LblDirectorateGroupName";
            this.LblDirectorateGroupName.Size = new System.Drawing.Size(140, 14);
            this.LblDirectorateGroupName.TabIndex = 11;
            this.LblDirectorateGroupName.Text = "Directorate Group Name";
            this.LblDirectorateGroupName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDirectorateGroupCode
            // 
            this.TxtDirectorateGroupCode.EnterMoveNextControl = true;
            this.TxtDirectorateGroupCode.Location = new System.Drawing.Point(158, 7);
            this.TxtDirectorateGroupCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDirectorateGroupCode.Name = "TxtDirectorateGroupCode";
            this.TxtDirectorateGroupCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDirectorateGroupCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDirectorateGroupCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDirectorateGroupCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDirectorateGroupCode.Properties.MaxLength = 40;
            this.TxtDirectorateGroupCode.Size = new System.Drawing.Size(282, 20);
            this.TxtDirectorateGroupCode.TabIndex = 10;
            // 
            // TxtDirectorateGroupName
            // 
            this.TxtDirectorateGroupName.EnterMoveNextControl = true;
            this.TxtDirectorateGroupName.Location = new System.Drawing.Point(158, 28);
            this.TxtDirectorateGroupName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDirectorateGroupName.Name = "TxtDirectorateGroupName";
            this.TxtDirectorateGroupName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDirectorateGroupName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDirectorateGroupName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDirectorateGroupName.Properties.Appearance.Options.UseFont = true;
            this.TxtDirectorateGroupName.Properties.MaxLength = 400;
            this.TxtDirectorateGroupName.Size = new System.Drawing.Size(282, 20);
            this.TxtDirectorateGroupName.TabIndex = 12;
            // 
            // LblParent
            // 
            this.LblParent.AutoSize = true;
            this.LblParent.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblParent.ForeColor = System.Drawing.Color.Black;
            this.LblParent.Location = new System.Drawing.Point(105, 52);
            this.LblParent.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblParent.Name = "LblParent";
            this.LblParent.Size = new System.Drawing.Size(43, 14);
            this.LblParent.TabIndex = 13;
            this.LblParent.Text = "Parent";
            this.LblParent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueParentCode
            // 
            this.LueParentCode.EnterMoveNextControl = true;
            this.LueParentCode.Location = new System.Drawing.Point(158, 49);
            this.LueParentCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueParentCode.Name = "LueParentCode";
            this.LueParentCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParentCode.Properties.Appearance.Options.UseFont = true;
            this.LueParentCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParentCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueParentCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParentCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueParentCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParentCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueParentCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParentCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueParentCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueParentCode.Properties.DropDownRows = 30;
            this.LueParentCode.Properties.NullText = "[Empty]";
            this.LueParentCode.Properties.PopupWidth = 300;
            this.LueParentCode.Size = new System.Drawing.Size(282, 20);
            this.LueParentCode.TabIndex = 14;
            this.LueParentCode.ToolTip = "F4 : Show/hide list";
            this.LueParentCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueParentCode.EditValueChanged += new System.EventHandler(this.LueParentCode_EditValueChanged);
            // 
            // ChkActive
            // 
            this.ChkActive.Location = new System.Drawing.Point(448, 8);
            this.ChkActive.Name = "ChkActive";
            this.ChkActive.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActive.Properties.Appearance.Options.UseFont = true;
            this.ChkActive.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkActive.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkActive.Properties.Caption = "Active";
            this.ChkActive.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActive.Size = new System.Drawing.Size(71, 22);
            this.ChkActive.TabIndex = 15;
            this.ChkActive.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkActive.ToolTipTitle = "Run System";
            this.ChkActive.CheckedChanged += new System.EventHandler(this.ChkActive_CheckedChanged);
            // 
            // ChkParent
            // 
            this.ChkParent.Location = new System.Drawing.Point(448, 29);
            this.ChkParent.Name = "ChkParent";
            this.ChkParent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkParent.Properties.Appearance.Options.UseFont = true;
            this.ChkParent.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkParent.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkParent.Properties.Caption = "Not Parent";
            this.ChkParent.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkParent.Size = new System.Drawing.Size(96, 22);
            this.ChkParent.TabIndex = 16;
            this.ChkParent.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkParent.ToolTipTitle = "Run System";
            // 
            // FrmDirectorateGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 164);
            this.Name = "FrmDirectorateGroup";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectorateGroupCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectorateGroupName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParentCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkParent.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblDirectorateGroupCode;
        private System.Windows.Forms.Label LblDirectorateGroupName;
        internal DevExpress.XtraEditors.TextEdit TxtDirectorateGroupCode;
        internal DevExpress.XtraEditors.TextEdit TxtDirectorateGroupName;
        private System.Windows.Forms.Label LblParent;
        private DevExpress.XtraEditors.LookUpEdit LueParentCode;
        private DevExpress.XtraEditors.CheckEdit ChkActive;
        private DevExpress.XtraEditors.CheckEdit ChkParent;
    }
}