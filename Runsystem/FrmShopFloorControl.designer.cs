﻿namespace RunSystem
{
    partial class FrmShopFloorControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmShopFloorControl));
            this.BtnProductionOrderDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnProductionRoutingDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnItCode = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSODocNo = new DevExpress.XtraEditors.SimpleButton();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtProductionRoutingDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtProductionOrderPlanningUomCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtSODocNo = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.DteProdEndDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.DteProdStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtProductionOrderDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtProductionOrderQty = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.LueProductionShiftCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.BtnWorkCenterDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtWorkCenterDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtWorkCenterDocName = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtCapacity = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtLocation = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtUomCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtBomDocName = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.BtnBomDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtBomDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtBomQty = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtBomPlanningUomCode = new DevExpress.XtraEditors.TextEdit();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtSequence = new DevExpress.XtraEditors.TextEdit();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProductionRoutingDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProductionOrderPlanningUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSODocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteProdEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteProdEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteProdStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteProdStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProductionOrderDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProductionOrderQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionShiftCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkCenterDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkCenterDocName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCapacity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBomDocName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBomDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBomQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBomPlanningUomCode.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSequence.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.LueProductionShiftCode);
            this.panel2.Controls.Add(this.TxtProductionOrderQty);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.TxtProductionOrderDocNo);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.BtnProductionOrderDocNo);
            this.panel2.Controls.Add(this.BtnItCode);
            this.panel2.Controls.Add(this.BtnSODocNo);
            this.panel2.Controls.Add(this.TxtProductionOrderPlanningUomCode);
            this.panel2.Controls.Add(this.TxtItName);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtSODocNo);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.DteProdEndDt);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.DteProdStartDt);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 242);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.Grd2);
            this.panel3.Location = new System.Drawing.Point(0, 242);
            this.panel3.Size = new System.Drawing.Size(772, 231);
            this.panel3.Controls.SetChildIndex(this.Grd2, 0);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.Location = new System.Drawing.Point(0, 110);
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(772, 121);
            this.Grd1.TabIndex = 63;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnProductionOrderDocNo
            // 
            this.BtnProductionOrderDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnProductionOrderDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProductionOrderDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProductionOrderDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnProductionOrderDocNo.Appearance.Options.UseBackColor = true;
            this.BtnProductionOrderDocNo.Appearance.Options.UseFont = true;
            this.BtnProductionOrderDocNo.Appearance.Options.UseForeColor = true;
            this.BtnProductionOrderDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnProductionOrderDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnProductionOrderDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnProductionOrderDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnProductionOrderDocNo.Image")));
            this.BtnProductionOrderDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnProductionOrderDocNo.Location = new System.Drawing.Point(326, 91);
            this.BtnProductionOrderDocNo.Name = "BtnProductionOrderDocNo";
            this.BtnProductionOrderDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnProductionOrderDocNo.TabIndex = 19;
            this.BtnProductionOrderDocNo.ToolTip = "Show Sales Order\'s Document Number Information";
            this.BtnProductionOrderDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnProductionOrderDocNo.ToolTipTitle = "Run System";
            this.BtnProductionOrderDocNo.Click += new System.EventHandler(this.BtnProductionOrderDocNo_Click);
            // 
            // BtnProductionRoutingDocNo
            // 
            this.BtnProductionRoutingDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnProductionRoutingDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProductionRoutingDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProductionRoutingDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnProductionRoutingDocNo.Appearance.Options.UseBackColor = true;
            this.BtnProductionRoutingDocNo.Appearance.Options.UseFont = true;
            this.BtnProductionRoutingDocNo.Appearance.Options.UseForeColor = true;
            this.BtnProductionRoutingDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnProductionRoutingDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnProductionRoutingDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnProductionRoutingDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnProductionRoutingDocNo.Image")));
            this.BtnProductionRoutingDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnProductionRoutingDocNo.Location = new System.Drawing.Point(294, 7);
            this.BtnProductionRoutingDocNo.Name = "BtnProductionRoutingDocNo";
            this.BtnProductionRoutingDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnProductionRoutingDocNo.TabIndex = 38;
            this.BtnProductionRoutingDocNo.ToolTip = "Find Production Routing\'s Document Number";
            this.BtnProductionRoutingDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnProductionRoutingDocNo.ToolTipTitle = "Run System";
            this.BtnProductionRoutingDocNo.Click += new System.EventHandler(this.BtnProductionRoutingDocNo_Click);
            // 
            // BtnItCode
            // 
            this.BtnItCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnItCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnItCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnItCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnItCode.Appearance.Options.UseBackColor = true;
            this.BtnItCode.Appearance.Options.UseFont = true;
            this.BtnItCode.Appearance.Options.UseForeColor = true;
            this.BtnItCode.Appearance.Options.UseTextOptions = true;
            this.BtnItCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnItCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnItCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnItCode.Image")));
            this.BtnItCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnItCode.Location = new System.Drawing.Point(326, 174);
            this.BtnItCode.Name = "BtnItCode";
            this.BtnItCode.Size = new System.Drawing.Size(24, 21);
            this.BtnItCode.TabIndex = 29;
            this.BtnItCode.ToolTip = "Show Item Information";
            this.BtnItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnItCode.ToolTipTitle = "Run System";
            this.BtnItCode.Click += new System.EventHandler(this.BtnItCode_Click);
            // 
            // BtnSODocNo
            // 
            this.BtnSODocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSODocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSODocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSODocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSODocNo.Appearance.Options.UseBackColor = true;
            this.BtnSODocNo.Appearance.Options.UseFont = true;
            this.BtnSODocNo.Appearance.Options.UseForeColor = true;
            this.BtnSODocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSODocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSODocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSODocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSODocNo.Image")));
            this.BtnSODocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSODocNo.Location = new System.Drawing.Point(326, 152);
            this.BtnSODocNo.Name = "BtnSODocNo";
            this.BtnSODocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnSODocNo.TabIndex = 26;
            this.BtnSODocNo.ToolTip = "Find Sales Order\'s Document Number";
            this.BtnSODocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSODocNo.ToolTipTitle = "Run System";
            this.BtnSODocNo.Click += new System.EventHandler(this.BtnSODocNo_Click);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(127, 217);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(195, 20);
            this.MeeRemark.TabIndex = 61;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(76, 219);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 14);
            this.label10.TabIndex = 60;
            this.label10.Text = "Remark";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProductionRoutingDocNo
            // 
            this.TxtProductionRoutingDocNo.EnterMoveNextControl = true;
            this.TxtProductionRoutingDocNo.Location = new System.Drawing.Point(127, 7);
            this.TxtProductionRoutingDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProductionRoutingDocNo.Name = "TxtProductionRoutingDocNo";
            this.TxtProductionRoutingDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProductionRoutingDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProductionRoutingDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProductionRoutingDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtProductionRoutingDocNo.Properties.MaxLength = 16;
            this.TxtProductionRoutingDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtProductionRoutingDocNo.TabIndex = 37;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(2, 10);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 14);
            this.label9.TabIndex = 36;
            this.label9.Text = "Production Routing#";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProductionOrderPlanningUomCode
            // 
            this.TxtProductionOrderPlanningUomCode.EnterMoveNextControl = true;
            this.TxtProductionOrderPlanningUomCode.Location = new System.Drawing.Point(232, 217);
            this.TxtProductionOrderPlanningUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProductionOrderPlanningUomCode.Name = "TxtProductionOrderPlanningUomCode";
            this.TxtProductionOrderPlanningUomCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProductionOrderPlanningUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProductionOrderPlanningUomCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProductionOrderPlanningUomCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProductionOrderPlanningUomCode.Properties.MaxLength = 16;
            this.TxtProductionOrderPlanningUomCode.Size = new System.Drawing.Size(91, 20);
            this.TxtProductionOrderPlanningUomCode.TabIndex = 34;
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(157, 196);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 16;
            this.TxtItName.Size = new System.Drawing.Size(286, 20);
            this.TxtItName.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(88, 198);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 14);
            this.label7.TabIndex = 30;
            this.label7.Text = "Item Name";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(157, 175);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 16;
            this.TxtItCode.Size = new System.Drawing.Size(166, 20);
            this.TxtItCode.TabIndex = 28;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(91, 177);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 14);
            this.label6.TabIndex = 27;
            this.label6.Text = "Item Code";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSODocNo
            // 
            this.TxtSODocNo.EnterMoveNextControl = true;
            this.TxtSODocNo.Location = new System.Drawing.Point(157, 154);
            this.TxtSODocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSODocNo.Name = "TxtSODocNo";
            this.TxtSODocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSODocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSODocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSODocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSODocNo.Properties.MaxLength = 16;
            this.TxtSODocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtSODocNo.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(78, 156);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 14);
            this.label5.TabIndex = 24;
            this.label5.Text = "SO# / MTS#";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteProdEndDt
            // 
            this.DteProdEndDt.EditValue = null;
            this.DteProdEndDt.EnterMoveNextControl = true;
            this.DteProdEndDt.Location = new System.Drawing.Point(157, 133);
            this.DteProdEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteProdEndDt.Name = "DteProdEndDt";
            this.DteProdEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteProdEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteProdEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteProdEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteProdEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteProdEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteProdEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteProdEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteProdEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteProdEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteProdEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteProdEndDt.Size = new System.Drawing.Size(116, 20);
            this.DteProdEndDt.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(27, 135);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 14);
            this.label4.TabIndex = 22;
            this.label4.Text = "Production\'s End Date";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteProdStartDt
            // 
            this.DteProdStartDt.EditValue = null;
            this.DteProdStartDt.EnterMoveNextControl = true;
            this.DteProdStartDt.Location = new System.Drawing.Point(157, 112);
            this.DteProdStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteProdStartDt.Name = "DteProdStartDt";
            this.DteProdStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteProdStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteProdStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteProdStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteProdStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteProdStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteProdStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteProdStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteProdStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteProdStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteProdStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteProdStartDt.Size = new System.Drawing.Size(116, 20);
            this.DteProdStartDt.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(21, 115);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 14);
            this.label3.TabIndex = 20;
            this.label3.Text = "Production\'s Start Date";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(157, 49);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(116, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(123, 52);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(157, 7);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(83, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProductionOrderDocNo
            // 
            this.TxtProductionOrderDocNo.EnterMoveNextControl = true;
            this.TxtProductionOrderDocNo.Location = new System.Drawing.Point(157, 91);
            this.TxtProductionOrderDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProductionOrderDocNo.Name = "TxtProductionOrderDocNo";
            this.TxtProductionOrderDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProductionOrderDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProductionOrderDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProductionOrderDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtProductionOrderDocNo.Properties.MaxLength = 16;
            this.TxtProductionOrderDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtProductionOrderDocNo.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(46, 94);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 14);
            this.label8.TabIndex = 17;
            this.label8.Text = "Production Order#";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProductionOrderQty
            // 
            this.TxtProductionOrderQty.EnterMoveNextControl = true;
            this.TxtProductionOrderQty.Location = new System.Drawing.Point(157, 217);
            this.TxtProductionOrderQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProductionOrderQty.Name = "TxtProductionOrderQty";
            this.TxtProductionOrderQty.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProductionOrderQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProductionOrderQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProductionOrderQty.Properties.Appearance.Options.UseFont = true;
            this.TxtProductionOrderQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtProductionOrderQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtProductionOrderQty.Size = new System.Drawing.Size(72, 20);
            this.TxtProductionOrderQty.TabIndex = 33;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(4, 219);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(152, 14);
            this.label11.TabIndex = 32;
            this.label11.Text = "Production Order Quantity";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(124, 73);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 14);
            this.label12.TabIndex = 15;
            this.label12.Text = "Shift";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProductionShiftCode
            // 
            this.LueProductionShiftCode.EnterMoveNextControl = true;
            this.LueProductionShiftCode.Location = new System.Drawing.Point(157, 70);
            this.LueProductionShiftCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueProductionShiftCode.Name = "LueProductionShiftCode";
            this.LueProductionShiftCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.Appearance.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProductionShiftCode.Properties.DropDownRows = 12;
            this.LueProductionShiftCode.Properties.NullText = "[Empty]";
            this.LueProductionShiftCode.Properties.PopupWidth = 500;
            this.LueProductionShiftCode.Size = new System.Drawing.Size(286, 20);
            this.LueProductionShiftCode.TabIndex = 16;
            this.LueProductionShiftCode.ToolTip = "F4 : Show/hide list";
            this.LueProductionShiftCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProductionShiftCode.EditValueChanged += new System.EventHandler(this.LueProductionShiftCode_EditValueChanged);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(157, 27);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 12;
            // 
            // BtnWorkCenterDocNo
            // 
            this.BtnWorkCenterDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnWorkCenterDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnWorkCenterDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnWorkCenterDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnWorkCenterDocNo.Appearance.Options.UseBackColor = true;
            this.BtnWorkCenterDocNo.Appearance.Options.UseFont = true;
            this.BtnWorkCenterDocNo.Appearance.Options.UseForeColor = true;
            this.BtnWorkCenterDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnWorkCenterDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnWorkCenterDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnWorkCenterDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnWorkCenterDocNo.Image")));
            this.BtnWorkCenterDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnWorkCenterDocNo.Location = new System.Drawing.Point(294, 29);
            this.BtnWorkCenterDocNo.Name = "BtnWorkCenterDocNo";
            this.BtnWorkCenterDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnWorkCenterDocNo.TabIndex = 41;
            this.BtnWorkCenterDocNo.ToolTip = "Find Production Routing\'s Document Number";
            this.BtnWorkCenterDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnWorkCenterDocNo.ToolTipTitle = "Run System";
            this.BtnWorkCenterDocNo.Click += new System.EventHandler(this.BtnWorkCenterDocNo_Click);
            // 
            // TxtWorkCenterDocNo
            // 
            this.TxtWorkCenterDocNo.EnterMoveNextControl = true;
            this.TxtWorkCenterDocNo.Location = new System.Drawing.Point(127, 28);
            this.TxtWorkCenterDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWorkCenterDocNo.Name = "TxtWorkCenterDocNo";
            this.TxtWorkCenterDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWorkCenterDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWorkCenterDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWorkCenterDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtWorkCenterDocNo.Properties.MaxLength = 16;
            this.TxtWorkCenterDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtWorkCenterDocNo.TabIndex = 40;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(37, 31);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 14);
            this.label13.TabIndex = 39;
            this.label13.Text = "Work Center#";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWorkCenterDocName
            // 
            this.TxtWorkCenterDocName.EnterMoveNextControl = true;
            this.TxtWorkCenterDocName.Location = new System.Drawing.Point(127, 49);
            this.TxtWorkCenterDocName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWorkCenterDocName.Name = "TxtWorkCenterDocName";
            this.TxtWorkCenterDocName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWorkCenterDocName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWorkCenterDocName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWorkCenterDocName.Properties.Appearance.Options.UseFont = true;
            this.TxtWorkCenterDocName.Properties.MaxLength = 16;
            this.TxtWorkCenterDocName.Size = new System.Drawing.Size(166, 20);
            this.TxtWorkCenterDocName.TabIndex = 43;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(11, 51);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(112, 14);
            this.label14.TabIndex = 42;
            this.label14.Text = "Work Center Name";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCapacity
            // 
            this.TxtCapacity.EnterMoveNextControl = true;
            this.TxtCapacity.Location = new System.Drawing.Point(127, 91);
            this.TxtCapacity.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCapacity.Name = "TxtCapacity";
            this.TxtCapacity.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCapacity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCapacity.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCapacity.Properties.Appearance.Options.UseFont = true;
            this.TxtCapacity.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCapacity.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCapacity.Size = new System.Drawing.Size(81, 20);
            this.TxtCapacity.TabIndex = 47;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(61, 115);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 14);
            this.label15.TabIndex = 49;
            this.label15.Text = "Sequence";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(71, 94);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(52, 14);
            this.label19.TabIndex = 46;
            this.label19.Text = "Capacity";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLocation
            // 
            this.TxtLocation.EnterMoveNextControl = true;
            this.TxtLocation.Location = new System.Drawing.Point(127, 70);
            this.TxtLocation.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocation.Name = "TxtLocation";
            this.TxtLocation.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocation.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocation.Properties.Appearance.Options.UseFont = true;
            this.TxtLocation.Properties.MaxLength = 16;
            this.TxtLocation.Size = new System.Drawing.Size(166, 20);
            this.TxtLocation.TabIndex = 45;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(70, 73);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 14);
            this.label20.TabIndex = 44;
            this.label20.Text = "Location";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUomCode
            // 
            this.TxtUomCode.EnterMoveNextControl = true;
            this.TxtUomCode.Location = new System.Drawing.Point(210, 91);
            this.TxtUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUomCode.Name = "TxtUomCode";
            this.TxtUomCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUomCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUomCode.Properties.Appearance.Options.UseFont = true;
            this.TxtUomCode.Properties.MaxLength = 16;
            this.TxtUomCode.Size = new System.Drawing.Size(83, 20);
            this.TxtUomCode.TabIndex = 48;
            // 
            // TxtBomDocName
            // 
            this.TxtBomDocName.EnterMoveNextControl = true;
            this.TxtBomDocName.Location = new System.Drawing.Point(127, 154);
            this.TxtBomDocName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBomDocName.Name = "TxtBomDocName";
            this.TxtBomDocName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBomDocName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBomDocName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBomDocName.Properties.Appearance.Options.UseFont = true;
            this.TxtBomDocName.Properties.MaxLength = 16;
            this.TxtBomDocName.Size = new System.Drawing.Size(166, 20);
            this.TxtBomDocName.TabIndex = 54;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(57, 157);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 14);
            this.label16.TabIndex = 53;
            this.label16.Text = "Bom Name";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnBomDocNo
            // 
            this.BtnBomDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBomDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBomDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBomDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBomDocNo.Appearance.Options.UseBackColor = true;
            this.BtnBomDocNo.Appearance.Options.UseFont = true;
            this.BtnBomDocNo.Appearance.Options.UseForeColor = true;
            this.BtnBomDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnBomDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBomDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBomDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnBomDocNo.Image")));
            this.BtnBomDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBomDocNo.Location = new System.Drawing.Point(294, 134);
            this.BtnBomDocNo.Name = "BtnBomDocNo";
            this.BtnBomDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnBomDocNo.TabIndex = 52;
            this.BtnBomDocNo.ToolTip = "Find Production Routing\'s Document Number";
            this.BtnBomDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBomDocNo.ToolTipTitle = "Run System";
            this.BtnBomDocNo.Click += new System.EventHandler(this.BtnBomDocNo_Click);
            // 
            // TxtBomDocNo
            // 
            this.TxtBomDocNo.EnterMoveNextControl = true;
            this.TxtBomDocNo.Location = new System.Drawing.Point(127, 133);
            this.TxtBomDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBomDocNo.Name = "TxtBomDocNo";
            this.TxtBomDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBomDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBomDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBomDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBomDocNo.Properties.MaxLength = 16;
            this.TxtBomDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtBomDocNo.TabIndex = 51;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(83, 136);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 14);
            this.label17.TabIndex = 50;
            this.label17.Text = "Bom#";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBomQty
            // 
            this.TxtBomQty.EnterMoveNextControl = true;
            this.TxtBomQty.Location = new System.Drawing.Point(127, 175);
            this.TxtBomQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBomQty.Name = "TxtBomQty";
            this.TxtBomQty.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBomQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBomQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBomQty.Properties.Appearance.Options.UseFont = true;
            this.TxtBomQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBomQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBomQty.Size = new System.Drawing.Size(81, 20);
            this.TxtBomQty.TabIndex = 56;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(41, 178);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 14);
            this.label18.TabIndex = 55;
            this.label18.Text = "Bom Quantity";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBomPlanningUomCode
            // 
            this.TxtBomPlanningUomCode.EnterMoveNextControl = true;
            this.TxtBomPlanningUomCode.Location = new System.Drawing.Point(210, 175);
            this.TxtBomPlanningUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBomPlanningUomCode.Name = "TxtBomPlanningUomCode";
            this.TxtBomPlanningUomCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBomPlanningUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBomPlanningUomCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBomPlanningUomCode.Properties.Appearance.Options.UseFont = true;
            this.TxtBomPlanningUomCode.Properties.MaxLength = 16;
            this.TxtBomPlanningUomCode.Size = new System.Drawing.Size(83, 20);
            this.TxtBomPlanningUomCode.TabIndex = 57;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.TxtSequence);
            this.panel5.Controls.Add(this.LueWhsCode);
            this.panel5.Controls.Add(this.TxtBomDocNo);
            this.panel5.Controls.Add(this.TxtUomCode);
            this.panel5.Controls.Add(this.TxtBomQty);
            this.panel5.Controls.Add(this.TxtCapacity);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.BtnBomDocNo);
            this.panel5.Controls.Add(this.TxtLocation);
            this.panel5.Controls.Add(this.TxtBomPlanningUomCode);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.TxtWorkCenterDocName);
            this.panel5.Controls.Add(this.TxtBomDocName);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.BtnWorkCenterDocNo);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.TxtWorkCenterDocNo);
            this.panel5.Controls.Add(this.TxtProductionRoutingDocNo);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.BtnProductionRoutingDocNo);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(446, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(326, 242);
            this.panel5.TabIndex = 35;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(54, 197);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 14);
            this.label21.TabIndex = 58;
            this.label21.Text = "Warehouse";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSequence
            // 
            this.TxtSequence.EnterMoveNextControl = true;
            this.TxtSequence.Location = new System.Drawing.Point(127, 112);
            this.TxtSequence.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSequence.Name = "TxtSequence";
            this.TxtSequence.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSequence.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSequence.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSequence.Properties.Appearance.Options.UseFont = true;
            this.TxtSequence.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSequence.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSequence.Size = new System.Drawing.Size(81, 20);
            this.TxtSequence.TabIndex = 49;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(127, 196);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 20;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 200;
            this.LueWhsCode.Size = new System.Drawing.Size(195, 20);
            this.LueWhsCode.TabIndex = 59;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 20;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(772, 110);
            this.Grd2.TabIndex = 62;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // FrmShopFloorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmShopFloorControl";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProductionRoutingDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProductionOrderPlanningUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSODocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteProdEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteProdEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteProdStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteProdStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProductionOrderDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProductionOrderQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionShiftCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkCenterDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkCenterDocName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCapacity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBomDocName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBomDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBomQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBomPlanningUomCode.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSequence.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtProductionOrderQty;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtProductionOrderDocNo;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.SimpleButton BtnProductionOrderDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnProductionRoutingDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnItCode;
        public DevExpress.XtraEditors.SimpleButton BtnSODocNo;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtProductionRoutingDocNo;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtProductionOrderPlanningUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtItName;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtSODocNo;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteProdEndDt;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.DateEdit DteProdStartDt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private DevExpress.XtraEditors.DateEdit DteDocDt;
        private DevExpress.XtraEditors.LookUpEdit LueProductionShiftCode;
        internal DevExpress.XtraEditors.TextEdit TxtWorkCenterDocName;
        private System.Windows.Forms.Label label14;
        public DevExpress.XtraEditors.SimpleButton BtnWorkCenterDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtWorkCenterDocNo;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtCapacity;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtLocation;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtBomDocName;
        private System.Windows.Forms.Label label16;
        public DevExpress.XtraEditors.SimpleButton BtnBomDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtBomDocNo;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtBomQty;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtBomPlanningUomCode;
        protected System.Windows.Forms.Panel panel5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        internal DevExpress.XtraEditors.TextEdit TxtSequence;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.LookUpEdit LueWhsCode;
    }
}