﻿#region Update
/*
    22/06/2020 [WED/KBN] new apps [pilih entity untuk COA per row nya]
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptFicoSettingDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptFicoSettingDlg2 mFrmParent;
        private int mRow = 0;
        private string mSQL = string.Empty;
        private bool mIsFilterByEnt = false;

        #endregion

        #region Constructor

        public FrmRptFicoSettingDlg3(FrmRptFicoSettingDlg2 FrmParent, int Row)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                mIsFilterByEnt = Sm.GetParameterBoo("IsFilterByEnt");
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-3
                    "Entity Code",
                    "Entity Name",
                    "Short Code",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-3
                    100, 200, 80
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3 });            
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '-' EntCode, '-' EntName, '-' ShortCode Union All ");
            SQL.AppendLine("SELECT A.EntCode, A.EntName, A.ShortCode ");
            SQL.AppendLine("FROM TblEntity A ");
            SQL.AppendLine("WHERE A.ActInd = 'Y' ");
            if (mIsFilterByEnt)
            {
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode = A.EntCode ");
                SQL.AppendLine("    And GrpCode In ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select GrpCode ");
                SQL.AppendLine("        From TblUser ");
                SQL.AppendLine("        Where UserCode = @UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtEntCode.Text, new string[] { "A.EntCode", "A.EntName", "A.ShortCode" });
                Sm.CmParam<string>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " ;",
                    new string[] { "EntCode", "EntName", "ShortCode" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;
                string EntCode = Sm.GetGrdStr(Grd1, Row, 1);
                string EntName = Sm.GetGrdStr(Grd1, Row, 2);

                mFrmParent.ShowSelectedEntity(EntCode, EntName, mRow);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtEntCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Entity");
        }

        #endregion

        #endregion

    }
}
