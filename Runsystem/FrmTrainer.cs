﻿#region Update
// 03/01/2018 [HAR] hapus informasi competence
// 05/09/2018 [HAR] bisa edit hours
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTrainer : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmTrainerFind FrmFind;

        #endregion

        #region Constructor

        public FrmTrainer(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);

            Sl.SetLueTrainerType(ref LueTrainerType);
            Sl.SetLueVdCode(ref LueVdCode);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtTrainerCode, TxtTrainerName, LueTrainerType, TxtEmpCode, TxtEmpName, LueVdCode,
                        TxtTrainingCode, TxtTrainingName, TxtNoOfHr
                    }, true);
                    BtnEmpCode.Enabled = false;
                    BtnTrainingCode.Enabled = false;
                    TxtTrainerCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtTrainerCode, TxtTrainerName, LueTrainerType,
                        TxtNoOfHr
                    }, false);
                    TxtTrainerCode.Focus();
                    BtnEmpCode.Enabled = true;
                    BtnTrainingCode.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtTrainerName, TxtNoOfHr 
                    }, false);
                    TxtTrainerName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtTrainerCode, TxtTrainerName, LueTrainerType, TxtEmpCode, TxtEmpName, LueVdCode,
                TxtTrainingCode, TxtTrainingName,
            });
            Sm.SetControlNumValueZero(new List<DevExpress.XtraEditors.TextEdit>
            {
                TxtNoOfHr
            }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTrainerFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtTrainerCode, "", false)) return;
            SetFormControl(mState.Edit);

            if (Sm.GetLue(LueTrainerType).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetLue(LueTrainerType), "I"))
                    BtnEmpCode.Enabled = true;
                else
                    Sm.SetControlReadOnly(LueVdCode, false);
            }
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtTrainerCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblTrainer Where TrainerCode=@TrainerCode" };
                Sm.CmParam<String>(ref cm, "@TrainerCode", TxtTrainerCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblTrainer(TrainerCode, TrainerName, TrainingCode, TrainerType, EmpCode, VdCode, NoOfHr, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@TrainerCode, @TrainerName, @TrainingCode, @TrainerType, @EmpCode, @VdCode, @NoOfHr, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update TrainerName=@TrainerName, TrainingCode=@TrainingCode, TrainerType=@TrainerType, EmpCode=@EmpCode, VdCode=@VdCode, NoOfHr=@NoOfHr, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@TrainerCode", TxtTrainerCode.Text);
                Sm.CmParam<String>(ref cm, "@TrainingCode", TxtTrainingCode.Text);
                Sm.CmParam<String>(ref cm, "@TrainerName", TxtTrainerName.Text);
                Sm.CmParam<String>(ref cm, "@TrainerType", Sm.GetLue(LueTrainerType));
                Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
                Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
                Sm.CmParam<Decimal>(ref cm, "@NoOfHr", Decimal.Parse(TxtNoOfHr.Text));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtTrainerCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string TrainerCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@TrainerCode", TrainerCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select A.TrainerCode, A.TrainerName, A.TrainingCode, C.TrainingName,  A.TrainerType, A.EmpCode, B.EmpName, A.VdCode, A.NoOfHr " +
                        "From TblTrainer A " +
                        "Left Join TblEmployee B On A.EmpCode=B.EmpCode " +
                        "Inner Join TblTraining C On A.TrainingCode=C.TrainingCode "+
                        "Where A.TrainerCode=@TrainerCode",
                        new string[] 
                        {
                            //0
                            "TrainerCode", 
                            //1-5
                            "TrainerName", "TrainingCode", "TrainingName",  "TrainerType", "EmpCode", 
                            //6-8
                            "EmpName", "VdCode", "NoOfHr"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtTrainerCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtTrainerName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtTrainingCode.EditValue = Sm.DrStr(dr, c[2]);
                            TxtTrainingName.EditValue = Sm.DrStr(dr, c[3]);
                            Sm.SetLue(LueTrainerType, Sm.DrStr(dr, c[4]));
                            TxtEmpCode.EditValue = Sm.DrStr(dr, c[5]);
                            TxtEmpName.EditValue = Sm.DrStr(dr, c[6]);
                            Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[7]));
                            TxtNoOfHr.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[8]), 0);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtTrainerCode, "Trainer code", false) ||
                Sm.IsTxtEmpty(TxtTrainerName, "Trainer name", false) ||
                Sm.IsTxtEmpty(TxtTrainingCode, "Training", false) ||
                Sm.IsLueEmpty(LueTrainerType, "Trainer type") ||
                (Sm.CompareStr(Sm.GetLue(LueTrainerType), "I") && Sm.IsTxtEmpty(TxtEmpCode, "Employee", false)) ||
                (Sm.CompareStr(Sm.GetLue(LueTrainerType), "E") && Sm.IsLueEmpty(LueVdCode, "Vendor")) ||
                IsTrainerCodeExisted();
        }

        private bool IsTrainerCodeExisted()
        {
            if (!TxtTrainerCode.Properties.ReadOnly && Sm.IsDataExist("Select TrainerCode From TblTrainer Where TrainerCode='" + TxtTrainerCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Trainer code ( " + TxtTrainerCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmTrainerDlg(this));
        }

        private void TxtTrainerCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTrainerCode);
        }

        private void TxtTrainerName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTrainerName);
        }

        private void LueTrainerType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrainerType, new Sm.RefreshLue1(Sl.SetLueTrainerType));

            BtnEmpCode.Enabled = false;
            Sm.SetControlReadOnly(LueVdCode, true);

            if (BtnSave.Enabled && Sm.GetLue(LueTrainerType).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetLue(LueTrainerType), "I"))
                    BtnEmpCode.Enabled = true;
                else
                    Sm.SetControlReadOnly(LueVdCode, false);
            }

        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
        }

        private void BtnTrainingCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmTrainerDlg2(this));
        }

        private void TxtNoOfHr_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtNoOfHr, 0);
        }
        #endregion

       

       

        #endregion
    }
}
