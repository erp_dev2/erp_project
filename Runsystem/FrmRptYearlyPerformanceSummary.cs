﻿#region Update
/*
    28/01/2019 [DITA] ubah source filter year
    18/05/2022 [TYO/PRODUCT] Set kolom Performance Review# jika CancelInd = Y maka tidak muncul
    25/05/2022 [ICA/HIN] mengubah source performance review menjadi new performance review berdasarkan paramater IsRptKPIUseGoalsSetting
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptYearlyPerformanceSummary : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty , mYr = string.Empty;
        internal bool
            mIsFilterBySiteHR = false,
            mIsFilterByDeptHR = false;
        private bool mIsRptKPIUseGoalsSetting = false;

        #endregion

        #region Constructor

        public FrmRptYearlyPerformanceSummary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                string Yr = Sm.ServerCurrentDateTime().Substring(0, 4);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, Yr);
                Sl.SetLueOption(ref LueStatus, "EmploymentStatus");

                if (mIsRptKPIUseGoalsSetting)
                { 
                    label2.Text = "Goals Name";
                    label2.Left -= 10;
                }
 
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.PRDocNo, C.EmpCode, C.EmpName, B.Grade, B.Average, ");
            SQL.AppendLine("D.SiteName, E.DeptName, F.PosName, C.JoinDt, C.ResignDt, G.OptDesc, ");
            if (!mIsRptKPIUseGoalsSetting)
            {
                #region SourceKPI
                SQL.AppendLine("A.KPIName, Null As GoalsProcessDocNo ");
                SQL.AppendLine("From TblKPIHdr A ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine("     Select X2.KPIDocNo, X2.DocNo as PRDocNo, X2.Grade, X2.Average ");
                SQL.AppendLine("     From ");
                SQL.AppendLine("     ( ");
                SQL.AppendLine("         Select T1.KPIDocno, Max(Concat(T1.DocDt,Left(T1.DocNo,4))) as MaxDocNo ");
                SQL.AppendLine("         From TblPerformanceReviewHdr T1 ");
                SQL.AppendLine("         Inner Join TblKPIHdr T2 On T1.KPIDocNo = T2.DocNo And T2.Yr = @Yr And T2.ActInd = 'Y' ");
                SQL.AppendLine("         Group By T1.KPIDocNo ");
                SQL.AppendLine("     ) X1  ");
                SQL.AppendLine("    Inner Join TblPerformanceReviewHdr X2 on X1.KPIDocNo = X2.KPIDocno ");
                SQL.AppendLine("        And Left(X1.MaxDocNo, 8) = X2.DocDt ");
                SQL.AppendLine("        And Right(X1.MaxDocNo, 4) = Left(X2.DocNo, 4) ");
                SQL.AppendLine("        AND X2.CancelInd = 'N' ");
                SQL.AppendLine(" ) B On A.DocNo = B.KPIDocNo ");
                #endregion
            }
            else
            {
                #region SourceGoals
                SQL.AppendLine("A.GoalsName KPIName, B.GoalsProcessDocNo ");
                SQL.AppendLine("From TblGoalsSettingHdr A ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine("    SELECT X1.GoalsDocNo, X2.GoalsProcessDocNo, X2.DocNo as PRDocNo, X3.GrdStatus Grade, X2.TotalScore Average ");
                SQL.AppendLine("    From ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        SELECT T2.GoalsDocno, T1.GoalsProcessDocNo, Max(Concat(T1.DocDt,LEFT(T1.DocNo,4))) as MaxDocNo ");
                SQL.AppendLine("        From TblNewPerformanceReviewHdr T1 ");
                SQL.AppendLine("        INNER JOIN TblGoalsProcessHdr T2 ON T1.GoalsProcessDocNo = T2.DocNo AND T2.CancelInd = 'N' AND T2.Yr = @Yr ");
                SQL.AppendLine("        Inner Join TblGoalsSettingHdr T3 ON T2.GoalsDocNo = T3.DocNo AND T3.ActInd = 'Y' ");
                SQL.AppendLine("        Group BY T2.GoalsDocNo ");
                SQL.AppendLine("    ) X1 ");
                SQL.AppendLine("    Inner Join TblNewPerformanceReviewHdr X2 on X1.GoalsProcessDocNo = X2.GoalsProcessDocNo ");
                SQL.AppendLine("        And Left(X1.MaxDocNo, 8) = X2.DocDt ");
                SQL.AppendLine("        And Right(X1.MaxDocNo, 4) = Left(X2.DocNo, 4) ");
                SQL.AppendLine("        AND X2.CancelInd = 'N' ");
                SQL.AppendLine("    INNER JOIN TblPerformanceGrade X3 ON X2.GrdCode = X3.GrdCode AND X3.ActInd = 'Y' ");
                SQL.AppendLine(" ) B On A.DocNo = B.GoalsDocNo ");
                #endregion
            }
            SQL.AppendLine("Inner Join TblEmployee C On A.PICCode=C.EmpCode ");
            SQL.AppendLine("Inner Join TblSite D On C.SiteCode = D.SiteCode ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And C.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select T1.SiteCode From TblGroupSite T1 ");
                SQL.AppendLine("        Where T1.SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("        And T1.GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblDepartment E On C.DeptCode = E.DeptCode ");
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And C.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment T2 ");
                SQL.AppendLine("        Where T2.DeptCode=IfNull(C.DeptCode, '') ");
                SQL.AppendLine("        And T2.GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblPosition F On C.PosCode = F.PosCode ");
            SQL.AppendLine("Inner Join TblOption G On C.EmploymentStatus = G.OptCode And G.OptCat='EmploymentStatus' ");
            if (!mIsRptKPIUseGoalsSetting)
                SQL.AppendLine("Where A.Yr=@Yr And A.ActInd = 'Y' ");
            else
                SQL.AppendLine("Where Left(A.DocDt, 4)=@Yr And A.ActInd = 'Y' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        (!mIsRptKPIUseGoalsSetting? "KPI#" : "Goals#"),
                        (!mIsRptKPIUseGoalsSetting? "KPI Name" : "Goals Name"),
                        "Performance"+Environment.NewLine+"Review#",
                        "Employee#",
                        "Employee"+Environment.NewLine+"Name",
                        
                        //6-10
                        "Grade",
                        (!mIsRptKPIUseGoalsSetting ? "Average" : "Total Score"),
                        "Site",
                        "Department",
                        "Position",                        

                        //11-15
                        "Join"+Environment.NewLine+"Date",
                        "Resign"+Environment.NewLine+"Date",
                        "Status",
                        "Goals Process#"

                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 200,150, 100, 200,   
                        
                        //6-10
                        80,100, 200, 200, 200, 

                        //11-14
                        110, 110, 80, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdFormatDate(Grd1, new int[] { 11, 12 });
            if (mIsRptKPIUseGoalsSetting)
                Grd1.Cols[14].Move(3);
            Grd1.Cols[14].Visible = mIsRptKPIUseGoalsSetting;
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            mYr = string.Empty;
            Sm.ClearGrd(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                if (Sm.IsLueEmpty(LueYr, "Year")) return;

                Cursor.Current = Cursors.WaitCursor;
                mYr = Sm.GetLue(LueYr);
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@Yr", mYr);
                if (!mIsRptKPIUseGoalsSetting)
                    Sm.FilterStr(ref Filter, ref cm, TxtKPICode.Text, new string[] { "B.KPIDocNo", "A.KPIName" });
                else
                    Sm.FilterStr(ref Filter, ref cm, TxtKPICode.Text, new string[] { "A.DocNo", "A.GoalsName" });
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "E.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "D.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueStatus), "C.EmploymentStatus", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocDt, A.DocNo, " + (!mIsRptKPIUseGoalsSetting ? "A.KPIName;" : "A.GoalsName;"),
                    new string[]
                    {
                        //0
                        "DocNo",
                    
                        //1-5
                        "KPIName",
                        "PRDocNo",
                        "EmpCode",
                        "EmpName",
                        "Grade",                           
                        
                        //6-10
                        "Average",
                        "SiteName",
                        "DeptName",
                        "PosName",
                        "JoinDt",                             
                        
                        //11
                        "ResignDt",
                        "OptDesc",
                        "GoalsProcessDocNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsRptKPIUseGoalsSetting = Sm.GetParameterBoo("IsRptKPIUseGoalsSetting");
        }
        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtKPICode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
            Sm.FilterLueSetCheckEdit(this, sender);
            
        }

        private void ChkKPICode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "KPI");
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employment Status");
        }

        #endregion

    }
}
