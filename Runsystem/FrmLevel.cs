﻿#region Update
/*
    14/10/2020 [WED/PHT] ganti template untuk tambah tab baru, berdasarkan parameter IsLevelUsePositionAllowance
    26/10/2020 [WED/PHT] tambah QualityPoint dan Position
    16/07/2021 [VIN/PHT] Bug Amount decimal
    09/12/2021 [RIS/PHT] Allowance deduction bisa diambil lebih dari 1 kali dengan param = IsAllowanceDeductionLevelCanBeSelectedMoreThanOnce
    02/03/2022 [MYA/PHT] Membuat SMK Statis di master level yang nantinya akan berefek kepada semua karyawan mendapatkan tunjangan SMK statis sesuai levelnya
    09/03/2022 [TRI/PHT] BUG "duplicate entry" saat edit kemudian save master level
    21/03/2023 [WED/PHT] tambah inputan Position Status (param IsLevelUsePositionStatus) dan mandatory (param IsPositionStatusMandatory)
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLevel : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmLevelFind FrmFind;
        internal bool 
            mIsOTAuthorizationByLevel = false,
            mIsAllowanceDeductionLevelCanBeSelectedMoreThanOnce = false,
            mIsLevelUsePositionStatus = false,
            mIsPositionStatusMandatory = false
            ;
        private bool mIsLevelUsePositionAllowance = false;
        private string mSMKStatisAmtSource = string.Empty;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmLevel(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Level";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                ExecQuery();
                GetParameter();
                if (mIsOTAuthorizationByLevel)
                    ChkRequestOTInd.Visible = true;
                else
                    ChkRequestOTInd.Visible = false;

                LblPositionStatus.Visible = LuePositionStatusCode.Visible = mIsLevelUsePositionStatus;
                if (mIsLevelUsePositionStatus && mIsPositionStatusMandatory) LblPositionStatus.ForeColor = Color.Red;
                if (mIsLevelUsePositionStatus) Sl.SetLuePositionStatusCode(ref LuePositionStatusCode);

                SetFormControl(mState.View);
                SetGrd();
                TcLevel.SelectedTabPage = TpPositionAllowance;

                LueGrdLvlCode.Visible = false;
                LueLocationCode.Visible = false;
                LueWorkingCode.Visible = false;
                LuePosCode.Visible = false;

                if (!mIsLevelUsePositionAllowance)
                {
                    TcLevel.TabPages.Remove(TpPositionAllowance);
                    LblQualityPoint.Visible = TxtQualityPoint.Visible = false;
                }
                else
                {
                    SetLueGrdLevelCode(ref LueGrdLvlCode, string.Empty);
                    Sl.SetLueOption(ref LueLocationCode, "LevelLocation");
                    Sl.SetLueOption(ref LueWorkingCode, "PositionTeritory");
                    Sl.SetLuePosCode(ref LuePosCode);
                }

                TcLevel.SelectedTabPage = TpGeneral;

                TcLevel.SelectedTabPage = TpSMKStatis;
                if(mSMKStatisAmtSource != "2")
                {
                    TcLevel.TabPages.Remove(TpSMKStatis);
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsOTAuthorizationByLevel = Sm.GetParameterBoo("IsOTAuthorizationByLevel");
            mIsLevelUsePositionAllowance = Sm.GetParameterBoo("IsLevelUsePositionAllowance");
            mIsAllowanceDeductionLevelCanBeSelectedMoreThanOnce = Sm.GetParameterBoo("IsAllowanceDeductionLevelCanBeSelectedMoreThanOnce");
            mSMKStatisAmtSource = Sm.GetParameter("SMKStatisAmtSource");
            mIsLevelUsePositionStatus = Sm.GetParameterBoo("IsLevelUsePositionStatus");
            mIsPositionStatusMandatory = Sm.GetParameterBoo("IsPositionStatusMandatory");
        }

        private void SetGrd()
        {
            #region Grid 1 - General
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Allowance/Deduction"+Environment.NewLine+"Code",
                        "Allowance/Deduction"+Environment.NewLine+"Name",
                        "",
                        "Amount",

                    },
                     new int[] 
                    {
                        60, 
                        20, 150, 250, 20, 130
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3 });
            Sm.GrdColButton(Grd1, new int[] { 1, 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2 }, false);
            #endregion

            #region Grid 2 - Level Position Allowance
            Grd2.Cols.Count = 14;
            Grd2.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "",
                    "Allowance/Deduction"+Environment.NewLine+"Code",
                    "Allowance/Deduction"+Environment.NewLine+"Name",
                    "",
                    "Grade Code",

                    //6-10
                    "Grade",
                    "Location Code",
                    "Location", 
                    "Working Code",
                    "Working",

                    //11-13
                    "Amount",
                    "Position Code",
                    "Position"

                },
                new int[] 
                {
                    60, 
                    20, 150, 250, 20, 0, 
                    180, 0, 200, 0, 200,
                    130, 0, 250
                }
            );
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 2, 3, 5, 7, 9, 12 });
            Sm.GrdColButton(Grd2, new int[] { 1, 4 });
            Sm.GrdFormatDec(Grd2, new int[] { 11 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2, 5, 7, 9, 12 }, false);
            Grd2.Cols[13].Move(7);
            #endregion

            #region Grid 3 - SMK Statis
            Grd3.Cols.Count = 2;
            Grd3.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "Amount",

                    },
                     new int[]
                    {
                        60,
                        130
                    }
                );
            Sm.GrdColReadOnly(Grd3, new int[] { 0 });
            Sm.GrdFormatDec(Grd3, new int[] { 1 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 0 }, false);
            #endregion
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLevelCode, TxtLevelName, ChkRequestOTInd, MeeRemark, LueGrdLvlCode, LueLocationCode, LueWorkingCode, TxtQualityPoint, LuePositionStatusCode }, true);
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    TxtLevelCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLevelCode, TxtLevelName, ChkRequestOTInd, MeeRemark, LueGrdLvlCode, LueLocationCode, LueWorkingCode, TxtQualityPoint, LuePositionStatusCode }, false);
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    TxtLevelCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLevelName, ChkRequestOTInd, MeeRemark, LueGrdLvlCode, LueLocationCode, LueWorkingCode, TxtQualityPoint, LuePositionStatusCode }, false);
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    TxtLevelName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtLevelCode, TxtLevelName, MeeRemark, LueGrdLvlCode, LueLocationCode, LueWorkingCode, LuePositionStatusCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtQualityPoint
            }, 0);
            ChkRequestOTInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 11 });

            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 1 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmLevelFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtLevelCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;
                var cml = new List<MySqlCommand>();
                cml.Add(SaveLevelHdr());
                if (Grd1.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                            cml.Add(SaveLevelDtl(Row));
                }

                if (Grd2.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0)
                            cml.Add(SaveLevelDtl2(Row));
                }

                if (Grd3.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                            cml.Add(SaveLevelDtl3(Row));
                }

                Sm.ExecCommands(cml);
                ShowData(TxtLevelCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        #region Grid 1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmLevelDlg(this, 1));
                }

                if (Sm.IsGrdColSelected(new int[] { 1, 5 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
                }
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAllowanceDeduction(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mADCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmLevelDlg(this, 1));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmAllowanceDeduction(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mADCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid 2

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //e.DoDefault = false;
                if (Sm.IsGrdColSelected(new int[] { 1, 6, 8, 10, 11, 13 }, e.ColIndex))
                {
                    if (e.ColIndex == 6) LueRequestEdit(Grd2, LueGrdLvlCode, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 8) LueRequestEdit(Grd2, LueLocationCode, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 10) LueRequestEdit(Grd2, LueWorkingCode, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 13) LueRequestEdit(Grd2, LuePosCode, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 11 });
                }
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmLevelDlg(this, 2));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmAllowanceDeduction(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mADCode = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid 3

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if(e.RowIndex == 0)
                {
                    if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd3, e.RowIndex);
                        Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 1 });
                    }
                }
                else
                {
                    e.DoDefault = false;
                }
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }


        #endregion

        #endregion

        #region Show Data

        public void ShowData(string LevelCode)
        {
            try
            {
                ClearData();
                ShowLevelHdr(LevelCode);
                ShowLevelDtl(LevelCode);
                ShowLevelDtl2(LevelCode);
                ShowLevelDtl3(LevelCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowLevelHdr(string LevelCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select LevelCode, LevelName, AllowOTRequestInd, Remark, QualityPoint, ");
            if (mIsLevelUsePositionStatus) SQL.AppendLine("PositionStatusCode ");
            else SQL.AppendLine("Null As PositionStatusCode ");
            SQL.AppendLine("From TblLevelHdr Where LevelCode=@LevelCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@LevelCode", LevelCode);

            Sm.ShowDataInCtrl(
                 ref cm, SQL.ToString(),
                 new string[] { "LevelCode", "LevelName", "AllowOTRequestInd", "Remark", "QualityPoint", "PositionStatusCode" },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtLevelCode.EditValue = Sm.DrStr(dr, c[0]);
                     TxtLevelName.EditValue = Sm.DrStr(dr, c[1]);
                     ChkRequestOTInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                     MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                     TxtQualityPoint.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                     if (mIsLevelUsePositionStatus) Sm.SetLue(LuePositionStatusCode, Sm.DrStr(dr, c[5]));
                 }, true
             );
        }

        private void ShowLevelDtl(string LevelCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ADCode, B.ADName, A.Amt ");
            SQL.AppendLine("From TblLevelDtl A  ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode ");
            SQL.AppendLine("Where A.LevelCode=@LevelCode ");
            SQL.AppendLine("Order By B.ADName;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@LevelCode", LevelCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] { "DNo", "ADCode", "ADName", "Amt" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowLevelDtl2(string LevelCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ADCode, B.ADName, A.GrdLvlCode, C.GrdLvlName, A.LocationCode, ");
            SQL.AppendLine("D.OptDesc As LocationName, A.WorkingCode, E.OptDesc As WorkingName, A.Amt, A.PosCode, F.PosName ");
            SQL.AppendLine("From TblLevelDtl2 A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode = B.ADCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr C On A.GrdLvlCode = C.GrdLvlCode ");
            SQL.AppendLine("Left Join TblOption D On A.LocationCode = D.OptCode And D.OptCat = 'LevelLocation' ");
            SQL.AppendLine("Left Join TblOption E On A.WorkingCode = E.OptCode And E.OptCat = 'PositionTeritory' ");
            SQL.AppendLine("Left Join TblPosition F On A.PosCode = F.PosCode ");
            SQL.AppendLine("Where A.LevelCode = @LevelCode; ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@LevelCode", LevelCode);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "ADCode", "ADName", "GrdLvlCode", "GrdLvlName", "LocationCode", 
                    "LocationName", "WorkingCode", "WorkingName", "Amt" , "PosCode",
                    "PosName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 11 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowLevelDtl3(string LevelCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.Amt ");
            SQL.AppendLine("From TblLevelDtl3 A  ");
            SQL.AppendLine("Where A.LevelCode=@LevelCode ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@LevelCode", LevelCode);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQL.ToString(),
                    new string[] { "DNo", "Amt" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 1 });
            Sm.FocusGrd(Grd3, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtLevelCode, "Level's code", false) ||
                Sm.IsTxtEmpty(TxtLevelName, "Level's name", false) ||
                IsLevelCodeExisted() ||
                (mIsLevelUsePositionStatus && mIsPositionStatusMandatory && Sm.IsLueEmpty(LuePositionStatusCode, "Position Status")) ||
                IsGrdValueNotValid() ||
                IsAllowanceDeductionNotValid();
        }

        private bool IsLevelCodeExisted()
        {
            if (TxtLevelCode.Properties.ReadOnly) return false;
            return Sm.IsDataExist(
                "Select 1 From TblLevelHdr Where LevelCode=@Param",
                TxtLevelCode.Text,
                "This level code already existed.");
        }

        private bool IsAllowanceDeductionNotValid()
        {
            
            for (int r1 = 0; r1 < Grd2.Rows.Count - 1; r1++)
            {
                for (int r2 = 0; r2 < Grd2.Rows.Count - 1; r2++)
                    if (mIsAllowanceDeductionLevelCanBeSelectedMoreThanOnce)
                    {
                        if (r1 != r2 && Sm.CompareStr(Sm.GetGrdStr(Grd2, r1, 3), Sm.GetGrdStr(Grd2, r2, 3)))
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd2, r1, 6), Sm.GetGrdStr(Grd2, r2, 6)) && Sm.CompareStr(Sm.GetGrdStr(Grd2, r1, 8), Sm.GetGrdStr(Grd2, r2, 8)) && Sm.CompareStr(Sm.GetGrdStr(Grd2, r1, 10), Sm.GetGrdStr(Grd2, r2, 10)) && Sm.CompareStr(Sm.GetGrdStr(Grd2, r1, 13), Sm.GetGrdStr(Grd2, r2, 13)))
                            {
                                int rt1 = r1 + 1;
                                int rt2 = r2 + 1;
                                Sm.StdMsg(mMsgType.Warning, "Allowance Deduction Row " + rt2 + " Should not be Same as Row " + rt1 );
                                Sm.FocusGrd(Grd2, r2, 3);
                                return true;
                            }

                        }
                    } else
                    {
                        if (r1 != r2 && Sm.CompareStr(Sm.GetGrdStr(Grd2, r1, 3), Sm.GetGrdStr(Grd2, r2, 3)))
                        {
                            Sm.StdMsg(mMsgType.Warning, "Allowance Deduction Can't Duplicate");
                            Sm.FocusGrd(Grd2, r2, 3);
                            return true;
                        }
                        
                    }
                    
               
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 2, false, "Allowance/Deduction is empty.")) { TcLevel.SelectedTabPage = TpGeneral; return true; }
                    if (Sm.IsGrdValueEmpty(Grd1, r, 5, true, "Amount is empty.")) { TcLevel.SelectedTabPage = TpGeneral; return true; }
                }
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, i, 2, false, "Allowance/Deduction is empty.")) { TcLevel.SelectedTabPage = TpPositionAllowance; return true; }
                    if (Sm.IsGrdValueEmpty(Grd2, i, 11, true, "Amount is zero.")) { TcLevel.SelectedTabPage = TpPositionAllowance; return true; }
                }
            }

            //if (Grd3.Rows.Count > 1)
            //{
            //    for (int i = 0; i < Grd3.Rows.Count - 1; ++i)
            //    {
            //        if (Sm.IsGrdValueEmpty(Grd3, i, 1, true, "Amount is zero.")) { TcLevel.SelectedTabPage = TpSMKStatis; return true; }
            //    }
            //}

            return false;
        }

        private MySqlCommand SaveLevelHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLevelHdr ");
            SQL.AppendLine("(LevelCode, LevelName, AllowOTRequestInd, QualityPoint, Remark, ");
            if (mIsLevelUsePositionStatus) SQL.AppendLine("PositionStatusCode, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@LevelCode, @LevelName, @AllowOTRequestInd, @QualityPoint, @Remark, ");
            if (mIsLevelUsePositionStatus) SQL.AppendLine("@PositionStatusCode, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("    LevelName=@LevelName, AllowOTRequestInd=@AllowOTRequestInd, QualityPoint=@QualityPoint, Remark=@Remark, ");
            if (mIsLevelUsePositionStatus) SQL.AppendLine("    PositionStatusCode = @PositionStatusCode, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblLevelDtl Where LevelCode=@LevelCode; ");
            SQL.AppendLine("Delete From TblLevelDtl2 Where LevelCode=@LevelCode; ");
            SQL.AppendLine("Delete From TblLevelDtl3 Where LevelCode=@LevelCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@LevelCode", TxtLevelCode.Text);
            Sm.CmParam<String>(ref cm, "@LevelName", TxtLevelName.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            if (mIsLevelUsePositionStatus) Sm.CmParam<String>(ref cm, "@PositionStatusCode", Sm.GetLue(LuePositionStatusCode));
            Sm.CmParam<String>(ref cm, "@AllowOTRequestInd", ChkRequestOTInd.Checked == true ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@QualityPoint", Decimal.Parse(TxtQualityPoint.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveLevelDtl(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblLevelDtl(LevelCode, DNo, ADCode, Amt, CreateBy, CreateDt) " +
                    "Values (@LevelCode, @DNo, @ADCode, @Amt, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@LevelCode", TxtLevelCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveLevelDtl2(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLevelDtl2(LevelCode, DNo, ADCode, GrdLvlCode, LocationCode, WorkingCode, PosCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@LevelCode, @DNo, @ADCode, @GrdLvlCode, @LocationCode, @WorkingCode, @PosCode, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@LevelCode", TxtLevelCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@LocationCode", Sm.GetGrdStr(Grd2, Row, 7));
            Sm.CmParam<String>(ref cm, "@WorkingCode", Sm.GetGrdStr(Grd2, Row, 9));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetGrdStr(Grd2, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveLevelDtl3(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblLevelDtl3(LevelCode, DNo, Amt, CreateBy, CreateDt) " +
                    "Values (@LevelCode, @DNo, @Amt, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@LevelCode", TxtLevelCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Methods

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsLevelUsePositionStatus', 'Apakah master Level menggunakan Position Status ? [Y = Ya; N = Tidak]', 'N', 'PHT', NULL, 'Y', 'WEDHA', '202303211455', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsPositionStatusMandatory', 'Apakah Position Status mandatory? [Y = Ya; N = Tidak]', 'N', 'PHT', NULL, 'Y', 'WEDHA', '202303211455', NULL, NULL); ");

            SQL.AppendLine("ALTER TABLE `tbllevelhdr` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `PositionStatusCode` VARCHAR(16) NULL DEFAULT NULL AFTER `Remark`, ");
            SQL.AppendLine("    DROP INDEX IF EXISTS `LevelCode`, ");
            SQL.AppendLine("    ADD INDEX IF NOT EXISTS `LevelCode` (`LevelCode`, `LevelName`, `PositionStatusCode`) USING BTREE; ");

            Sm.ExecQuery(SQL.ToString());
        }

        private void SetLueGrdLevelCode(ref DXE.LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select GrdLvlCode Col1, GrdLvlName Col2 ");
            SQL.AppendLine("From TblGradeLevelHdr ");
            if (Code.Length > 0)
                SQL.AppendLine("Where GrdLvlCode = @Code ");
            else
                SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By GrdLvlName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }


        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtLevelCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLevelCode);
        }

        private void TxtLevelName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLevelName);
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueGrdLvlCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueGrdLvlCode, new Sm.RefreshLue2(SetLueGrdLevelCode), string.Empty);
            }
        }

        private void LueGrdLvlCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.LueKeyDown(Grd2, ref fAccept, e);
            }
        }

        private void LueGrdLvlCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueGrdLvlCode.Visible && fAccept && fCell.ColIndex == 6)
                {
                    if (Sm.GetLue(LueGrdLvlCode).Length == 0)
                        Grd2.Cells[fCell.RowIndex, 5].Value =
                        Grd2.Cells[fCell.RowIndex, 6].Value = null;
                    else
                    {
                        Grd2.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueGrdLvlCode);
                        Grd2.Cells[fCell.RowIndex, 6].Value = LueGrdLvlCode.GetColumnValue("Col2");
                    }
                    LueGrdLvlCode.Visible = false;
                }
            }
        }

        private void LueLocationCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueLocationCode, new Sm.RefreshLue2(Sl.SetLueOption), "LevelLocation");
            }
        }

        private void LueLocationCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.LueKeyDown(Grd2, ref fAccept, e);
            }
        }

        private void LueLocationCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueLocationCode.Visible && fAccept && fCell.ColIndex == 8)
                {
                    if (Sm.GetLue(LueLocationCode).Length == 0)
                        Grd2.Cells[fCell.RowIndex, 7].Value =
                        Grd2.Cells[fCell.RowIndex, 8].Value = null;
                    else
                    {
                        Grd2.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LueLocationCode);
                        Grd2.Cells[fCell.RowIndex, 8].Value = LueLocationCode.GetColumnValue("Col2");
                    }
                    LueLocationCode.Visible = false;
                }
            }
        }

        private void LueWorkingCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWorkingCode, new Sm.RefreshLue2(Sl.SetLueOption), "PositionTeritory");
            }
        }

        private void LueWorkingCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.LueKeyDown(Grd2, ref fAccept, e);
            }
        }

        private void LueWorkingCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueWorkingCode.Visible && fAccept && fCell.ColIndex == 10)
                {
                    if (Sm.GetLue(LueWorkingCode).Length == 0)
                        Grd2.Cells[fCell.RowIndex, 9].Value =
                        Grd2.Cells[fCell.RowIndex, 10].Value = null;
                    else
                    {
                        Grd2.Cells[fCell.RowIndex, 9].Value = Sm.GetLue(LueWorkingCode);
                        Grd2.Cells[fCell.RowIndex, 10].Value = LueWorkingCode.GetColumnValue("Col2");
                    }
                    LueWorkingCode.Visible = false;
                }
            }
        }

        private void LuePositionStatusCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePositionStatusCode, new Sm.RefreshLue1(Sl.SetLuePositionStatusCode));
            }
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
            }
        }

        private void LuePosCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.LueKeyDown(Grd2, ref fAccept, e);
            }
        }

        private void LuePosCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LuePosCode.Visible && fAccept && fCell.ColIndex == 13)
                {
                    if (Sm.GetLue(LuePosCode).Length == 0)
                        Grd2.Cells[fCell.RowIndex, 12].Value =
                        Grd2.Cells[fCell.RowIndex, 13].Value = null;
                    else
                    {
                        Grd2.Cells[fCell.RowIndex, 12].Value = Sm.GetLue(LuePosCode);
                        Grd2.Cells[fCell.RowIndex, 13].Value = LuePosCode.GetColumnValue("Col2");
                    }
                    LuePosCode.Visible = false;
                }
            }
        }

        private void TxtQualityPoint_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtQualityPoint, 0);
            }
        }

        #endregion

        #endregion

    }
}
