﻿namespace RunSystem
{
    partial class FrmProjectImplementation2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProjectImplementation2));
            this.panel3 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtNumber = new DevExpress.XtraEditors.TextEdit();
            this.LueProcessInd = new DevExpress.XtraEditors.LookUpEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtAchievement = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtSOContractDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnSOContractDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label13 = new System.Windows.Forms.Label();
            this.BtnSOContractDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TxtType = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtProjectCode = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtBOQDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnLOPDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnBOQDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtLOPDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LueSPCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.LueCtContactPersonName = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TcProjectImplementation = new System.Windows.Forms.TabControl();
            this.TpgWBS = new System.Windows.Forms.TabPage();
            this.LueItCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DtePlanEndDt = new DevExpress.XtraEditors.DateEdit();
            this.DtePlanStartDt = new DevExpress.XtraEditors.DateEdit();
            this.LueTaskCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueStageCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.TxtTotalPersentase = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.TxtTotalBobot = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.TxtTotalPrice = new DevExpress.XtraEditors.TextEdit();
            this.LblTotalPrice = new System.Windows.Forms.Label();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnImportWBS = new DevExpress.XtraEditors.SimpleButton();
            this.TpgResource = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.LuePPN = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPPhAmt = new DevExpress.XtraEditors.TextEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.TxtContractAmt = new DevExpress.XtraEditors.TextEdit();
            this.TxtNetCash = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtExclPPN = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtPPNAmt = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtTotalResource = new DevExpress.XtraEditors.TextEdit();
            this.LblTotalResource = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.TxtPPNPPhPercentage = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.TxtPPNPercentage = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.TxtTotal = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtDirectCost = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtRemunerationCost = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.BtnImportResource = new DevExpress.XtraEditors.SimpleButton();
            this.TpgIssue = new System.Windows.Forms.TabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgDocument = new System.Windows.Forms.TabPage();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.LuePPh = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcessInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAchievement.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOQDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLOPDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtContactPersonName.Properties)).BeginInit();
            this.TcProjectImplementation.SuspendLayout();
            this.TpgWBS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaskCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStageCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPersentase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalBobot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPrice.Properties)).BeginInit();
            this.TpgResource.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuePPN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPhAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNetCash.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExclPPN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPNAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalResource.Properties)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPNPPhPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPNPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemunerationCost.Properties)).BeginInit();
            this.TpgIssue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpgDocument.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePPh.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(917, 0);
            this.panel1.Size = new System.Drawing.Size(70, 525);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcProjectImplementation);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(917, 525);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.TxtNumber);
            this.panel3.Controls.Add(this.LueProcessInd);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.TxtStatus);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.TxtAchievement);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.TxtSOContractDocNo);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.BtnSOContractDocNo);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.BtnSOContractDocNo2);
            this.panel3.Controls.Add(this.MeeCancelReason);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.MeeRemark);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(917, 202);
            this.panel3.TabIndex = 10;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(105, 176);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 14);
            this.label14.TabIndex = 51;
            this.label14.Text = "Number";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNumber
            // 
            this.TxtNumber.EnterMoveNextControl = true;
            this.TxtNumber.Location = new System.Drawing.Point(160, 174);
            this.TxtNumber.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNumber.Name = "TxtNumber";
            this.TxtNumber.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNumber.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNumber.Properties.Appearance.Options.UseFont = true;
            this.TxtNumber.Properties.MaxLength = 30;
            this.TxtNumber.Properties.ReadOnly = true;
            this.TxtNumber.Size = new System.Drawing.Size(230, 20);
            this.TxtNumber.TabIndex = 50;
            // 
            // LueProcessInd
            // 
            this.LueProcessInd.EnterMoveNextControl = true;
            this.LueProcessInd.Location = new System.Drawing.Point(160, 68);
            this.LueProcessInd.Margin = new System.Windows.Forms.Padding(5);
            this.LueProcessInd.Name = "LueProcessInd";
            this.LueProcessInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.Appearance.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProcessInd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProcessInd.Properties.DropDownRows = 30;
            this.LueProcessInd.Properties.NullText = "[Empty]";
            this.LueProcessInd.Properties.PopupWidth = 300;
            this.LueProcessInd.Size = new System.Drawing.Size(230, 20);
            this.LueProcessInd.TabIndex = 18;
            this.LueProcessInd.ToolTip = "F4 : Show/hide list";
            this.LueProcessInd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProcessInd.EditValueChanged += new System.EventHandler(this.LueProcessInd_EditValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(107, 69);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 14);
            this.label11.TabIndex = 17;
            this.label11.Text = "Process";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(160, 47);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 30;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(230, 20);
            this.TxtStatus.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(113, 49);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 14);
            this.label10.TabIndex = 15;
            this.label10.Text = "Status";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(392, 134);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 14);
            this.label9.TabIndex = 28;
            this.label9.Text = "%";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAchievement
            // 
            this.TxtAchievement.EnterMoveNextControl = true;
            this.TxtAchievement.Location = new System.Drawing.Point(160, 131);
            this.TxtAchievement.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAchievement.Name = "TxtAchievement";
            this.TxtAchievement.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAchievement.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAchievement.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAchievement.Properties.Appearance.Options.UseFont = true;
            this.TxtAchievement.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAchievement.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAchievement.Properties.MaxLength = 30;
            this.TxtAchievement.Properties.ReadOnly = true;
            this.TxtAchievement.Size = new System.Drawing.Size(230, 20);
            this.TxtAchievement.TabIndex = 27;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(76, 133);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 14);
            this.label8.TabIndex = 26;
            this.label8.Text = "Achievement";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSOContractDocNo
            // 
            this.TxtSOContractDocNo.EnterMoveNextControl = true;
            this.TxtSOContractDocNo.Location = new System.Drawing.Point(160, 110);
            this.TxtSOContractDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractDocNo.Name = "TxtSOContractDocNo";
            this.TxtSOContractDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractDocNo.Properties.MaxLength = 30;
            this.TxtSOContractDocNo.Properties.ReadOnly = true;
            this.TxtSOContractDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtSOContractDocNo.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(72, 113);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 14);
            this.label4.TabIndex = 22;
            this.label4.Text = "SO Contract#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSOContractDocNo
            // 
            this.BtnSOContractDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo.Image")));
            this.BtnSOContractDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo.Location = new System.Drawing.Point(391, 112);
            this.BtnSOContractDocNo.Name = "BtnSOContractDocNo";
            this.BtnSOContractDocNo.Size = new System.Drawing.Size(24, 18);
            this.BtnSOContractDocNo.TabIndex = 24;
            this.BtnSOContractDocNo.ToolTip = "Find SO Contract";
            this.BtnSOContractDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo.Click += new System.EventHandler(this.BtnSOContractDocNo_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(20, 91);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(135, 14);
            this.label13.TabIndex = 19;
            this.label13.Text = "Reason For Cancellation";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSOContractDocNo2
            // 
            this.BtnSOContractDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo2.Image")));
            this.BtnSOContractDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo2.Location = new System.Drawing.Point(414, 113);
            this.BtnSOContractDocNo2.Name = "BtnSOContractDocNo2";
            this.BtnSOContractDocNo2.Size = new System.Drawing.Size(24, 18);
            this.BtnSOContractDocNo2.TabIndex = 25;
            this.BtnSOContractDocNo2.ToolTip = "Show SO Contract";
            this.BtnSOContractDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo2.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo2.Click += new System.EventHandler(this.BtnSOContractDocNo2_Click);
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(160, 89);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(230, 20);
            this.MeeCancelReason.TabIndex = 20;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(108, 155);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(47, 14);
            this.label28.TabIndex = 29;
            this.label28.Text = "Remark";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(392, 89);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 21;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(160, 152);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 700;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(230, 20);
            this.MeeRemark.TabIndex = 30;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(160, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(82, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(160, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(117, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(122, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.TxtType);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.TxtProjectCode);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.TxtProjectName);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.TxtBOQDocNo);
            this.panel5.Controls.Add(this.LueCtCode);
            this.panel5.Controls.Add(this.BtnLOPDocNo);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.BtnBOQDocNo);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.TxtLOPDocNo);
            this.panel5.Controls.Add(this.LueSPCode);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.LueCtContactPersonName);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(496, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(417, 198);
            this.panel5.TabIndex = 31;
            // 
            // TxtType
            // 
            this.TxtType.EnterMoveNextControl = true;
            this.TxtType.Location = new System.Drawing.Point(157, 150);
            this.TxtType.Margin = new System.Windows.Forms.Padding(5);
            this.TxtType.Name = "TxtType";
            this.TxtType.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtType.Properties.Appearance.Options.UseBackColor = true;
            this.TxtType.Properties.Appearance.Options.UseFont = true;
            this.TxtType.Properties.MaxLength = 30;
            this.TxtType.Properties.ReadOnly = true;
            this.TxtType.Size = new System.Drawing.Size(230, 20);
            this.TxtType.TabIndex = 49;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(118, 153);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 14);
            this.label17.TabIndex = 48;
            this.label17.Text = "Type";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectCode
            // 
            this.TxtProjectCode.EnterMoveNextControl = true;
            this.TxtProjectCode.Location = new System.Drawing.Point(157, 110);
            this.TxtProjectCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectCode.Name = "TxtProjectCode";
            this.TxtProjectCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectCode.Properties.MaxLength = 30;
            this.TxtProjectCode.Properties.ReadOnly = true;
            this.TxtProjectCode.Size = new System.Drawing.Size(230, 20);
            this.TxtProjectCode.TabIndex = 45;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(75, 113);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 14);
            this.label15.TabIndex = 44;
            this.label15.Text = "Project Code";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectName
            // 
            this.TxtProjectName.EnterMoveNextControl = true;
            this.TxtProjectName.Location = new System.Drawing.Point(157, 130);
            this.TxtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectName.Name = "TxtProjectName";
            this.TxtProjectName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectName.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectName.Properties.MaxLength = 250;
            this.TxtProjectName.Properties.ReadOnly = true;
            this.TxtProjectName.Size = new System.Drawing.Size(230, 20);
            this.TxtProjectName.TabIndex = 47;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(72, 133);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 14);
            this.label12.TabIndex = 46;
            this.label12.Text = "Project Name";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBOQDocNo
            // 
            this.TxtBOQDocNo.EnterMoveNextControl = true;
            this.TxtBOQDocNo.Location = new System.Drawing.Point(157, 5);
            this.TxtBOQDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBOQDocNo.Name = "TxtBOQDocNo";
            this.TxtBOQDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBOQDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBOQDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBOQDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBOQDocNo.Properties.MaxLength = 30;
            this.TxtBOQDocNo.Properties.ReadOnly = true;
            this.TxtBOQDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtBOQDocNo.TabIndex = 33;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(157, 47);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(230, 20);
            this.LueCtCode.TabIndex = 39;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // BtnLOPDocNo
            // 
            this.BtnLOPDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnLOPDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnLOPDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLOPDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnLOPDocNo.Appearance.Options.UseBackColor = true;
            this.BtnLOPDocNo.Appearance.Options.UseFont = true;
            this.BtnLOPDocNo.Appearance.Options.UseForeColor = true;
            this.BtnLOPDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnLOPDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnLOPDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnLOPDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnLOPDocNo.Image")));
            this.BtnLOPDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnLOPDocNo.Location = new System.Drawing.Point(388, 27);
            this.BtnLOPDocNo.Name = "BtnLOPDocNo";
            this.BtnLOPDocNo.Size = new System.Drawing.Size(24, 18);
            this.BtnLOPDocNo.TabIndex = 37;
            this.BtnLOPDocNo.ToolTip = "Show LOP";
            this.BtnLOPDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnLOPDocNo.ToolTipTitle = "Run System";
            this.BtnLOPDocNo.Click += new System.EventHandler(this.BtnLOPDocNo_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(94, 50);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 38;
            this.label3.Text = "Customer";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnBOQDocNo
            // 
            this.BtnBOQDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBOQDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBOQDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBOQDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBOQDocNo.Appearance.Options.UseBackColor = true;
            this.BtnBOQDocNo.Appearance.Options.UseFont = true;
            this.BtnBOQDocNo.Appearance.Options.UseForeColor = true;
            this.BtnBOQDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnBOQDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBOQDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBOQDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnBOQDocNo.Image")));
            this.BtnBOQDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBOQDocNo.Location = new System.Drawing.Point(388, 6);
            this.BtnBOQDocNo.Name = "BtnBOQDocNo";
            this.BtnBOQDocNo.Size = new System.Drawing.Size(24, 18);
            this.BtnBOQDocNo.TabIndex = 34;
            this.BtnBOQDocNo.ToolTip = "Show BOQ";
            this.BtnBOQDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBOQDocNo.ToolTipTitle = "Run System";
            this.BtnBOQDocNo.Click += new System.EventHandler(this.BtnBOQDocNo_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(6, 70);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 14);
            this.label5.TabIndex = 40;
            this.label5.Text = "Customer Contact Person";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLOPDocNo
            // 
            this.TxtLOPDocNo.EnterMoveNextControl = true;
            this.TxtLOPDocNo.Location = new System.Drawing.Point(157, 26);
            this.TxtLOPDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLOPDocNo.Name = "TxtLOPDocNo";
            this.TxtLOPDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtLOPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLOPDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLOPDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLOPDocNo.Properties.MaxLength = 30;
            this.TxtLOPDocNo.Properties.ReadOnly = true;
            this.TxtLOPDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtLOPDocNo.TabIndex = 36;
            // 
            // LueSPCode
            // 
            this.LueSPCode.EnterMoveNextControl = true;
            this.LueSPCode.Location = new System.Drawing.Point(157, 89);
            this.LueSPCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSPCode.Name = "LueSPCode";
            this.LueSPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.Appearance.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSPCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSPCode.Properties.DropDownRows = 30;
            this.LueSPCode.Properties.NullText = "[Empty]";
            this.LueSPCode.Properties.PopupWidth = 300;
            this.LueSPCode.Size = new System.Drawing.Size(230, 20);
            this.LueSPCode.TabIndex = 43;
            this.LueSPCode.ToolTip = "F4 : Show/hide list";
            this.LueSPCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(115, 28);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 14);
            this.label7.TabIndex = 35;
            this.label7.Text = "LOP#";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(78, 92);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 14);
            this.label16.TabIndex = 42;
            this.label16.Text = "Sales Person";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtContactPersonName
            // 
            this.LueCtContactPersonName.EnterMoveNextControl = true;
            this.LueCtContactPersonName.Location = new System.Drawing.Point(157, 68);
            this.LueCtContactPersonName.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtContactPersonName.Name = "LueCtContactPersonName";
            this.LueCtContactPersonName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.Appearance.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtContactPersonName.Properties.DropDownRows = 30;
            this.LueCtContactPersonName.Properties.NullText = "[Empty]";
            this.LueCtContactPersonName.Properties.PopupWidth = 300;
            this.LueCtContactPersonName.Size = new System.Drawing.Size(230, 20);
            this.LueCtContactPersonName.TabIndex = 41;
            this.LueCtContactPersonName.ToolTip = "F4 : Show/hide list";
            this.LueCtContactPersonName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(112, 7);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 14);
            this.label6.TabIndex = 32;
            this.label6.Text = "BOQ#";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TcProjectImplementation
            // 
            this.TcProjectImplementation.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.TcProjectImplementation.Controls.Add(this.TpgWBS);
            this.TcProjectImplementation.Controls.Add(this.TpgResource);
            this.TcProjectImplementation.Controls.Add(this.TpgIssue);
            this.TcProjectImplementation.Controls.Add(this.TpgDocument);
            this.TcProjectImplementation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcProjectImplementation.Location = new System.Drawing.Point(0, 202);
            this.TcProjectImplementation.Multiline = true;
            this.TcProjectImplementation.Name = "TcProjectImplementation";
            this.TcProjectImplementation.SelectedIndex = 0;
            this.TcProjectImplementation.ShowToolTips = true;
            this.TcProjectImplementation.Size = new System.Drawing.Size(917, 323);
            this.TcProjectImplementation.TabIndex = 44;
            // 
            // TpgWBS
            // 
            this.TpgWBS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgWBS.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgWBS.Controls.Add(this.LueItCode);
            this.TpgWBS.Controls.Add(this.DtePlanEndDt);
            this.TpgWBS.Controls.Add(this.DtePlanStartDt);
            this.TpgWBS.Controls.Add(this.LueTaskCode);
            this.TpgWBS.Controls.Add(this.LueStageCode);
            this.TpgWBS.Controls.Add(this.Grd1);
            this.TpgWBS.Controls.Add(this.panel4);
            this.TpgWBS.Location = new System.Drawing.Point(4, 26);
            this.TpgWBS.Name = "TpgWBS";
            this.TpgWBS.Size = new System.Drawing.Size(909, 293);
            this.TpgWBS.TabIndex = 1;
            this.TpgWBS.Text = "WBS";
            this.TpgWBS.UseVisualStyleBackColor = true;
            // 
            // LueItCode
            // 
            this.LueItCode.EnterMoveNextControl = true;
            this.LueItCode.Location = new System.Drawing.Point(340, 146);
            this.LueItCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode.Name = "LueItCode";
            this.LueItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode.Properties.Appearance.Options.UseFont = true;
            this.LueItCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode.Properties.DropDownRows = 30;
            this.LueItCode.Properties.NullText = "[Empty]";
            this.LueItCode.Properties.PopupWidth = 150;
            this.LueItCode.Size = new System.Drawing.Size(224, 20);
            this.LueItCode.TabIndex = 59;
            this.LueItCode.ToolTip = "F4 : Show/hide list";
            this.LueItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode.EditValueChanged += new System.EventHandler(this.LueItCode_EditValueChanged);
            this.LueItCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode_KeyDown);
            this.LueItCode.Leave += new System.EventHandler(this.LueItCode_Leave);
            // 
            // DtePlanEndDt
            // 
            this.DtePlanEndDt.EditValue = null;
            this.DtePlanEndDt.EnterMoveNextControl = true;
            this.DtePlanEndDt.Location = new System.Drawing.Point(449, 82);
            this.DtePlanEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePlanEndDt.Name = "DtePlanEndDt";
            this.DtePlanEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanEndDt.Properties.Appearance.Options.UseFont = true;
            this.DtePlanEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePlanEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePlanEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePlanEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePlanEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePlanEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePlanEndDt.Size = new System.Drawing.Size(138, 20);
            this.DtePlanEndDt.TabIndex = 57;
            this.DtePlanEndDt.Visible = false;
            this.DtePlanEndDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DtePlanEndDt_KeyDown);
            this.DtePlanEndDt.Leave += new System.EventHandler(this.DtePlanEndDt_Leave);
            // 
            // DtePlanStartDt
            // 
            this.DtePlanStartDt.EditValue = null;
            this.DtePlanStartDt.EnterMoveNextControl = true;
            this.DtePlanStartDt.Location = new System.Drawing.Point(607, 81);
            this.DtePlanStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePlanStartDt.Name = "DtePlanStartDt";
            this.DtePlanStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanStartDt.Properties.Appearance.Options.UseFont = true;
            this.DtePlanStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePlanStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePlanStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePlanStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePlanStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePlanStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePlanStartDt.Size = new System.Drawing.Size(138, 20);
            this.DtePlanStartDt.TabIndex = 58;
            this.DtePlanStartDt.Visible = false;
            this.DtePlanStartDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DtePlanStartDt_KeyDown);
            this.DtePlanStartDt.Leave += new System.EventHandler(this.DtePlanStartDt_Leave);
            // 
            // LueTaskCode
            // 
            this.LueTaskCode.EnterMoveNextControl = true;
            this.LueTaskCode.Location = new System.Drawing.Point(276, 82);
            this.LueTaskCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaskCode.Name = "LueTaskCode";
            this.LueTaskCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.Appearance.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaskCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaskCode.Properties.DropDownRows = 30;
            this.LueTaskCode.Properties.NullText = "[Empty]";
            this.LueTaskCode.Properties.PopupWidth = 150;
            this.LueTaskCode.Size = new System.Drawing.Size(152, 20);
            this.LueTaskCode.TabIndex = 56;
            this.LueTaskCode.ToolTip = "F4 : Show/hide list";
            this.LueTaskCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaskCode.EditValueChanged += new System.EventHandler(this.LueTaskCode_EditValueChanged);
            this.LueTaskCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTaskCode_KeyDown);
            this.LueTaskCode.Leave += new System.EventHandler(this.LueTaskCode_Leave);
            // 
            // LueStageCode
            // 
            this.LueStageCode.EnterMoveNextControl = true;
            this.LueStageCode.Location = new System.Drawing.Point(28, 82);
            this.LueStageCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueStageCode.Name = "LueStageCode";
            this.LueStageCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.Appearance.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStageCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStageCode.Properties.DropDownRows = 30;
            this.LueStageCode.Properties.NullText = "[Empty]";
            this.LueStageCode.Properties.PopupWidth = 150;
            this.LueStageCode.Size = new System.Drawing.Size(224, 20);
            this.LueStageCode.TabIndex = 55;
            this.LueStageCode.ToolTip = "F4 : Show/hide list";
            this.LueStageCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStageCode.EditValueChanged += new System.EventHandler(this.LueStageCode_EditValueChanged);
            this.LueStageCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueStageCode_KeyDown);
            this.LueStageCode.Leave += new System.EventHandler(this.LueStageCode_Leave);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 74);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(905, 215);
            this.Grd1.TabIndex = 54;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel9);
            this.panel4.Controls.Add(this.BtnImportWBS);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(905, 74);
            this.panel4.TabIndex = 45;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.TxtTotalPersentase);
            this.panel9.Controls.Add(this.label34);
            this.panel9.Controls.Add(this.TxtTotalBobot);
            this.panel9.Controls.Add(this.label33);
            this.panel9.Controls.Add(this.TxtTotalPrice);
            this.panel9.Controls.Add(this.LblTotalPrice);
            this.panel9.Controls.Add(this.simpleButton1);
            this.panel9.Controls.Add(this.simpleButton2);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel9.Location = new System.Drawing.Point(492, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(413, 74);
            this.panel9.TabIndex = 47;
            // 
            // TxtTotalPersentase
            // 
            this.TxtTotalPersentase.EnterMoveNextControl = true;
            this.TxtTotalPersentase.Location = new System.Drawing.Point(253, 45);
            this.TxtTotalPersentase.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalPersentase.Name = "TxtTotalPersentase";
            this.TxtTotalPersentase.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalPersentase.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalPersentase.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalPersentase.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalPersentase.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalPersentase.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalPersentase.Properties.ReadOnly = true;
            this.TxtTotalPersentase.Size = new System.Drawing.Size(133, 20);
            this.TxtTotalPersentase.TabIndex = 53;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(147, 48);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(102, 14);
            this.label34.TabIndex = 52;
            this.label34.Text = "Total Percentage";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalBobot
            // 
            this.TxtTotalBobot.EnterMoveNextControl = true;
            this.TxtTotalBobot.Location = new System.Drawing.Point(253, 24);
            this.TxtTotalBobot.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalBobot.Name = "TxtTotalBobot";
            this.TxtTotalBobot.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalBobot.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalBobot.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalBobot.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalBobot.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalBobot.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalBobot.Properties.ReadOnly = true;
            this.TxtTotalBobot.Size = new System.Drawing.Size(133, 20);
            this.TxtTotalBobot.TabIndex = 51;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(177, 27);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(72, 14);
            this.label33.TabIndex = 50;
            this.label33.Text = "Total Bobot";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalPrice
            // 
            this.TxtTotalPrice.EnterMoveNextControl = true;
            this.TxtTotalPrice.Location = new System.Drawing.Point(253, 3);
            this.TxtTotalPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalPrice.Name = "TxtTotalPrice";
            this.TxtTotalPrice.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalPrice.Properties.ReadOnly = true;
            this.TxtTotalPrice.Size = new System.Drawing.Size(133, 20);
            this.TxtTotalPrice.TabIndex = 49;
            // 
            // LblTotalPrice
            // 
            this.LblTotalPrice.AutoSize = true;
            this.LblTotalPrice.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotalPrice.ForeColor = System.Drawing.Color.Black;
            this.LblTotalPrice.Location = new System.Drawing.Point(184, 6);
            this.LblTotalPrice.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblTotalPrice.Name = "LblTotalPrice";
            this.LblTotalPrice.Size = new System.Drawing.Size(65, 14);
            this.LblTotalPrice.TabIndex = 48;
            this.LblTotalPrice.Text = "Total Price";
            this.LblTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.simpleButton1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.ForeColor = System.Drawing.Color.White;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Appearance.Options.UseForeColor = true;
            this.simpleButton1.Appearance.Options.UseTextOptions = true;
            this.simpleButton1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(388, 27);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(24, 18);
            this.simpleButton1.TabIndex = 37;
            this.simpleButton1.ToolTip = "Show LOP";
            this.simpleButton1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.simpleButton1.ToolTipTitle = "Run System";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.simpleButton2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.ForeColor = System.Drawing.Color.White;
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Appearance.Options.UseForeColor = true;
            this.simpleButton2.Appearance.Options.UseTextOptions = true;
            this.simpleButton2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.simpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton2.Location = new System.Drawing.Point(388, 6);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(24, 18);
            this.simpleButton2.TabIndex = 34;
            this.simpleButton2.ToolTip = "Show BOQ";
            this.simpleButton2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.simpleButton2.ToolTipTitle = "Run System";
            // 
            // BtnImportWBS
            // 
            this.BtnImportWBS.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnImportWBS.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnImportWBS.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnImportWBS.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnImportWBS.Appearance.Options.UseBackColor = true;
            this.BtnImportWBS.Appearance.Options.UseFont = true;
            this.BtnImportWBS.Appearance.Options.UseForeColor = true;
            this.BtnImportWBS.Appearance.Options.UseTextOptions = true;
            this.BtnImportWBS.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnImportWBS.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnImportWBS.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnImportWBS.Location = new System.Drawing.Point(0, 6);
            this.BtnImportWBS.Name = "BtnImportWBS";
            this.BtnImportWBS.Size = new System.Drawing.Size(86, 21);
            this.BtnImportWBS.TabIndex = 46;
            this.BtnImportWBS.Text = "Import Data";
            this.BtnImportWBS.ToolTip = "Import Data";
            this.BtnImportWBS.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnImportWBS.ToolTipTitle = "Run System";
            this.BtnImportWBS.Click += new System.EventHandler(this.BtnImportWBS_Click);
            // 
            // TpgResource
            // 
            this.TpgResource.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgResource.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgResource.Controls.Add(this.Grd2);
            this.TpgResource.Controls.Add(this.panel6);
            this.TpgResource.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgResource.Location = new System.Drawing.Point(4, 26);
            this.TpgResource.Name = "TpgResource";
            this.TpgResource.Size = new System.Drawing.Size(909, 293);
            this.TpgResource.TabIndex = 0;
            this.TpgResource.Text = "Resource";
            this.TpgResource.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 141);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(905, 148);
            this.Grd2.TabIndex = 77;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.TxtTotalResource);
            this.panel6.Controls.Add(this.LblTotalResource);
            this.panel6.Controls.Add(this.panel8);
            this.panel6.Controls.Add(this.TxtTotal);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.TxtDirectCost);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.TxtRemunerationCost);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Controls.Add(this.BtnImportResource);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(905, 141);
            this.panel6.TabIndex = 51;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.LuePPh);
            this.panel7.Controls.Add(this.LuePPN);
            this.panel7.Controls.Add(this.TxtPPhAmt);
            this.panel7.Controls.Add(this.label27);
            this.panel7.Controls.Add(this.TxtContractAmt);
            this.panel7.Controls.Add(this.TxtNetCash);
            this.panel7.Controls.Add(this.label24);
            this.panel7.Controls.Add(this.TxtExclPPN);
            this.panel7.Controls.Add(this.label23);
            this.panel7.Controls.Add(this.TxtPPNAmt);
            this.panel7.Controls.Add(this.label22);
            this.panel7.Controls.Add(this.label21);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(281, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(326, 141);
            this.panel7.TabIndex = 60;
            // 
            // LuePPN
            // 
            this.LuePPN.EnterMoveNextControl = true;
            this.LuePPN.Location = new System.Drawing.Point(127, 52);
            this.LuePPN.Margin = new System.Windows.Forms.Padding(5);
            this.LuePPN.Name = "LuePPN";
            this.LuePPN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPN.Properties.Appearance.Options.UseFont = true;
            this.LuePPN.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPN.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePPN.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPN.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePPN.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPN.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePPN.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPN.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePPN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePPN.Properties.DropDownRows = 10;
            this.LuePPN.Properties.NullText = "[Empty]";
            this.LuePPN.Properties.PopupWidth = 150;
            this.LuePPN.Size = new System.Drawing.Size(54, 20);
            this.LuePPN.TabIndex = 60;
            this.LuePPN.ToolTip = "F4 : Show/hide list";
            this.LuePPN.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePPN.EditValueChanged += new System.EventHandler(this.LuePPN_EditValueChanged);
            // 
            // TxtPPhAmt
            // 
            this.TxtPPhAmt.EnterMoveNextControl = true;
            this.TxtPPhAmt.Location = new System.Drawing.Point(186, 94);
            this.TxtPPhAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPPhAmt.Name = "TxtPPhAmt";
            this.TxtPPhAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPPhAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPPhAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPPhAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtPPhAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPPhAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPPhAmt.Properties.MaxLength = 30;
            this.TxtPPhAmt.Properties.ReadOnly = true;
            this.TxtPPhAmt.Size = new System.Drawing.Size(131, 20);
            this.TxtPPhAmt.TabIndex = 66;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(79, 34);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(102, 14);
            this.label27.TabIndex = 57;
            this.label27.Text = "Contract Amount";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtContractAmt
            // 
            this.TxtContractAmt.EnterMoveNextControl = true;
            this.TxtContractAmt.Location = new System.Drawing.Point(186, 31);
            this.TxtContractAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtContractAmt.Name = "TxtContractAmt";
            this.TxtContractAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtContractAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtContractAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtContractAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtContractAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtContractAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtContractAmt.Properties.MaxLength = 30;
            this.TxtContractAmt.Properties.ReadOnly = true;
            this.TxtContractAmt.Size = new System.Drawing.Size(131, 20);
            this.TxtContractAmt.TabIndex = 58;
            // 
            // TxtNetCash
            // 
            this.TxtNetCash.EnterMoveNextControl = true;
            this.TxtNetCash.Location = new System.Drawing.Point(186, 115);
            this.TxtNetCash.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNetCash.Name = "TxtNetCash";
            this.TxtNetCash.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtNetCash.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNetCash.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNetCash.Properties.Appearance.Options.UseFont = true;
            this.TxtNetCash.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtNetCash.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtNetCash.Properties.MaxLength = 30;
            this.TxtNetCash.Properties.ReadOnly = true;
            this.TxtNetCash.Size = new System.Drawing.Size(131, 20);
            this.TxtNetCash.TabIndex = 68;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(32, 118);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(149, 14);
            this.label24.TabIndex = 67;
            this.label24.Text = "NET Cash (Excl PPN+PPh)";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtExclPPN
            // 
            this.TxtExclPPN.EnterMoveNextControl = true;
            this.TxtExclPPN.Location = new System.Drawing.Point(186, 73);
            this.TxtExclPPN.Margin = new System.Windows.Forms.Padding(5);
            this.TxtExclPPN.Name = "TxtExclPPN";
            this.TxtExclPPN.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtExclPPN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtExclPPN.Properties.Appearance.Options.UseBackColor = true;
            this.TxtExclPPN.Properties.Appearance.Options.UseFont = true;
            this.TxtExclPPN.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtExclPPN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtExclPPN.Properties.MaxLength = 30;
            this.TxtExclPPN.Properties.ReadOnly = true;
            this.TxtExclPPN.Size = new System.Drawing.Size(131, 20);
            this.TxtExclPPN.TabIndex = 63;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(96, 96);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(28, 14);
            this.label23.TabIndex = 64;
            this.label23.Text = "PPh";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPPNAmt
            // 
            this.TxtPPNAmt.EnterMoveNextControl = true;
            this.TxtPPNAmt.Location = new System.Drawing.Point(186, 52);
            this.TxtPPNAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPPNAmt.Name = "TxtPPNAmt";
            this.TxtPPNAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPPNAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPPNAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPPNAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtPPNAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPPNAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPPNAmt.Properties.MaxLength = 30;
            this.TxtPPNAmt.Properties.ReadOnly = true;
            this.TxtPPNAmt.Size = new System.Drawing.Size(131, 20);
            this.TxtPPNAmt.TabIndex = 61;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(6, 77);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(175, 14);
            this.label22.TabIndex = 62;
            this.label22.Text = "Production Amount(Excl. PPN)";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(96, 55);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 14);
            this.label21.TabIndex = 59;
            this.label21.Text = "PPN";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalResource
            // 
            this.TxtTotalResource.EnterMoveNextControl = true;
            this.TxtTotalResource.Location = new System.Drawing.Point(139, 27);
            this.TxtTotalResource.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalResource.Name = "TxtTotalResource";
            this.TxtTotalResource.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalResource.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalResource.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalResource.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalResource.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalResource.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalResource.Properties.ReadOnly = true;
            this.TxtTotalResource.Size = new System.Drawing.Size(133, 20);
            this.TxtTotalResource.TabIndex = 59;
            // 
            // LblTotalResource
            // 
            this.LblTotalResource.AutoSize = true;
            this.LblTotalResource.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotalResource.ForeColor = System.Drawing.Color.Black;
            this.LblTotalResource.Location = new System.Drawing.Point(37, 30);
            this.LblTotalResource.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblTotalResource.Name = "LblTotalResource";
            this.LblTotalResource.Size = new System.Drawing.Size(89, 14);
            this.LblTotalResource.TabIndex = 58;
            this.LblTotalResource.Text = "Total Resource";
            this.LblTotalResource.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.label31);
            this.panel8.Controls.Add(this.label30);
            this.panel8.Controls.Add(this.label29);
            this.panel8.Controls.Add(this.TxtPPNPPhPercentage);
            this.panel8.Controls.Add(this.label25);
            this.panel8.Controls.Add(this.TxtPPNPercentage);
            this.panel8.Controls.Add(this.label26);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(607, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(298, 141);
            this.panel8.TabIndex = 69;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(270, 73);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(19, 14);
            this.label31.TabIndex = 76;
            this.label31.Text = "%";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(270, 51);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(19, 14);
            this.label30.TabIndex = 73;
            this.label30.Text = "%";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(3, 32);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(116, 14);
            this.label29.TabIndex = 70;
            this.label29.Text = "Cost Percentage :";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TxtPPNPPhPercentage
            // 
            this.TxtPPNPPhPercentage.EnterMoveNextControl = true;
            this.TxtPPNPPhPercentage.Location = new System.Drawing.Point(134, 48);
            this.TxtPPNPPhPercentage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPPNPPhPercentage.Name = "TxtPPNPPhPercentage";
            this.TxtPPNPPhPercentage.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPPNPPhPercentage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPPNPPhPercentage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPPNPPhPercentage.Properties.Appearance.Options.UseFont = true;
            this.TxtPPNPPhPercentage.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPPNPPhPercentage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPPNPPhPercentage.Properties.MaxLength = 30;
            this.TxtPPNPPhPercentage.Properties.ReadOnly = true;
            this.TxtPPNPPhPercentage.Size = new System.Drawing.Size(131, 20);
            this.TxtPPNPPhPercentage.TabIndex = 72;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(44, 73);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(75, 14);
            this.label25.TabIndex = 74;
            this.label25.Text = "Exclude PPN";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPPNPercentage
            // 
            this.TxtPPNPercentage.EnterMoveNextControl = true;
            this.TxtPPNPercentage.Location = new System.Drawing.Point(134, 70);
            this.TxtPPNPercentage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPPNPercentage.Name = "TxtPPNPercentage";
            this.TxtPPNPercentage.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPPNPercentage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPPNPercentage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPPNPercentage.Properties.Appearance.Options.UseFont = true;
            this.TxtPPNPercentage.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPPNPercentage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPPNPercentage.Properties.MaxLength = 30;
            this.TxtPPNPercentage.Properties.ReadOnly = true;
            this.TxtPPNPercentage.Size = new System.Drawing.Size(131, 20);
            this.TxtPPNPercentage.TabIndex = 75;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(15, 53);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(104, 14);
            this.label26.TabIndex = 71;
            this.label26.Text = "Exclude PPN+PPh";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal
            // 
            this.TxtTotal.EnterMoveNextControl = true;
            this.TxtTotal.Location = new System.Drawing.Point(139, 90);
            this.TxtTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal.Name = "TxtTotal";
            this.TxtTotal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal.Properties.MaxLength = 30;
            this.TxtTotal.Properties.ReadOnly = true;
            this.TxtTotal.Size = new System.Drawing.Size(133, 20);
            this.TxtTotal.TabIndex = 57;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(91, 92);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 14);
            this.label20.TabIndex = 56;
            this.label20.Text = "Total";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDirectCost
            // 
            this.TxtDirectCost.EnterMoveNextControl = true;
            this.TxtDirectCost.Location = new System.Drawing.Point(139, 69);
            this.TxtDirectCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDirectCost.Name = "TxtDirectCost";
            this.TxtDirectCost.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDirectCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDirectCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDirectCost.Properties.Appearance.Options.UseFont = true;
            this.TxtDirectCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDirectCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDirectCost.Properties.MaxLength = 30;
            this.TxtDirectCost.Properties.ReadOnly = true;
            this.TxtDirectCost.Size = new System.Drawing.Size(133, 20);
            this.TxtDirectCost.TabIndex = 55;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(59, 70);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(67, 14);
            this.label18.TabIndex = 54;
            this.label18.Text = "Direct Cost";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRemunerationCost
            // 
            this.TxtRemunerationCost.EnterMoveNextControl = true;
            this.TxtRemunerationCost.Location = new System.Drawing.Point(139, 48);
            this.TxtRemunerationCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemunerationCost.Name = "TxtRemunerationCost";
            this.TxtRemunerationCost.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemunerationCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemunerationCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemunerationCost.Properties.Appearance.Options.UseFont = true;
            this.TxtRemunerationCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRemunerationCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRemunerationCost.Properties.MaxLength = 30;
            this.TxtRemunerationCost.Properties.ReadOnly = true;
            this.TxtRemunerationCost.Size = new System.Drawing.Size(133, 20);
            this.TxtRemunerationCost.TabIndex = 53;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(15, 49);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(111, 14);
            this.label19.TabIndex = 52;
            this.label19.Text = "Remuneration Cost";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnImportResource
            // 
            this.BtnImportResource.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnImportResource.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnImportResource.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnImportResource.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnImportResource.Appearance.Options.UseBackColor = true;
            this.BtnImportResource.Appearance.Options.UseFont = true;
            this.BtnImportResource.Appearance.Options.UseForeColor = true;
            this.BtnImportResource.Appearance.Options.UseTextOptions = true;
            this.BtnImportResource.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnImportResource.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnImportResource.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnImportResource.Location = new System.Drawing.Point(3, 5);
            this.BtnImportResource.Name = "BtnImportResource";
            this.BtnImportResource.Size = new System.Drawing.Size(86, 21);
            this.BtnImportResource.TabIndex = 50;
            this.BtnImportResource.Text = "Import Data";
            this.BtnImportResource.ToolTip = "Import Data";
            this.BtnImportResource.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnImportResource.ToolTipTitle = "Run System";
            this.BtnImportResource.Click += new System.EventHandler(this.BtnImportResource_Click);
            // 
            // TpgIssue
            // 
            this.TpgIssue.Controls.Add(this.Grd3);
            this.TpgIssue.Location = new System.Drawing.Point(4, 26);
            this.TpgIssue.Name = "TpgIssue";
            this.TpgIssue.Padding = new System.Windows.Forms.Padding(3);
            this.TpgIssue.Size = new System.Drawing.Size(764, 1);
            this.TpgIssue.TabIndex = 3;
            this.TpgIssue.Text = "Issue";
            this.TpgIssue.UseVisualStyleBackColor = true;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(3, 3);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(758, 0);
            this.Grd3.TabIndex = 45;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // TpgDocument
            // 
            this.TpgDocument.Controls.Add(this.DteDocDt2);
            this.TpgDocument.Controls.Add(this.Grd4);
            this.TpgDocument.Location = new System.Drawing.Point(4, 26);
            this.TpgDocument.Name = "TpgDocument";
            this.TpgDocument.Padding = new System.Windows.Forms.Padding(3);
            this.TpgDocument.Size = new System.Drawing.Size(764, 1);
            this.TpgDocument.TabIndex = 2;
            this.TpgDocument.Text = "Document";
            this.TpgDocument.UseVisualStyleBackColor = true;
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(71, 30);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(138, 20);
            this.DteDocDt2.TabIndex = 42;
            this.DteDocDt2.Visible = false;
            this.DteDocDt2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteDocDt2_KeyDown);
            this.DteDocDt2.Leave += new System.EventHandler(this.DteDocDt2_Leave);
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(3, 3);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(758, 0);
            this.Grd4.TabIndex = 45;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd4_EllipsisButtonClick);
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // LuePPh
            // 
            this.LuePPh.EnterMoveNextControl = true;
            this.LuePPh.Location = new System.Drawing.Point(127, 94);
            this.LuePPh.Margin = new System.Windows.Forms.Padding(5);
            this.LuePPh.Name = "LuePPh";
            this.LuePPh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPh.Properties.Appearance.Options.UseFont = true;
            this.LuePPh.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPh.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePPh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePPh.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPh.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePPh.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPh.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePPh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePPh.Properties.DropDownRows = 10;
            this.LuePPh.Properties.NullText = "[Empty]";
            this.LuePPh.Properties.PopupWidth = 150;
            this.LuePPh.Size = new System.Drawing.Size(54, 20);
            this.LuePPh.TabIndex = 65;
            this.LuePPh.ToolTip = "F4 : Show/hide list";
            this.LuePPh.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePPh.EditValueChanged += new System.EventHandler(this.LuePPh_EditValueChanged);
            // 
            // FrmProjectImplementation2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(987, 525);
            this.Name = "FrmProjectImplementation2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcessInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAchievement.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOQDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLOPDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtContactPersonName.Properties)).EndInit();
            this.TcProjectImplementation.ResumeLayout(false);
            this.TpgWBS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaskCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStageCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPersentase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalBobot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPrice.Properties)).EndInit();
            this.TpgResource.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuePPN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPhAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNetCash.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExclPPN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPNAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalResource.Properties)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPNPPhPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPNPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemunerationCost.Properties)).EndInit();
            this.TpgIssue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpgDocument.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePPh.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.LookUpEdit LueCtContactPersonName;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.LookUpEdit LueSPCode;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.LookUpEdit LueCtCode;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo2;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.TabControl TcProjectImplementation;
        private System.Windows.Forms.TabPage TpgWBS;
        private DevExpress.XtraEditors.LookUpEdit LueStageCode;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.TabPage TpgResource;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.TabPage TpgIssue;
        private DevExpress.XtraEditors.LookUpEdit LueTaskCode;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.TabPage TpgDocument;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        public DevExpress.XtraEditors.SimpleButton BtnLOPDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnBOQDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtLOPDocNo;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtBOQDocNo;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtSOContractDocNo;
        private System.Windows.Forms.Label label4;
        protected System.Windows.Forms.Panel panel5;
        internal DevExpress.XtraEditors.TextEdit TxtAchievement;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.DateEdit DtePlanEndDt;
        internal DevExpress.XtraEditors.DateEdit DtePlanStartDt;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.LookUpEdit LueProcessInd;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        public DevExpress.XtraEditors.SimpleButton BtnImportWBS;
        public DevExpress.XtraEditors.SimpleButton BtnImportResource;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        internal DevExpress.XtraEditors.TextEdit TxtProjectName;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtTotalResource;
        private System.Windows.Forms.Label LblTotalResource;
        protected System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit TxtPPNPercentage;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit TxtPPNPPhPercentage;
        internal DevExpress.XtraEditors.TextEdit TxtTotal;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtDirectCost;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtRemunerationCost;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtType;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtProjectCode;
        private System.Windows.Forms.Label label15;
        protected System.Windows.Forms.Panel panel7;
        internal DevExpress.XtraEditors.TextEdit TxtPPhAmt;
        private System.Windows.Forms.Label label27;
        internal DevExpress.XtraEditors.TextEdit TxtContractAmt;
        internal DevExpress.XtraEditors.TextEdit TxtNetCash;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtExclPPN;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtPPNAmt;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        protected System.Windows.Forms.Panel panel9;
        public DevExpress.XtraEditors.SimpleButton simpleButton1;
        public DevExpress.XtraEditors.SimpleButton simpleButton2;
        internal DevExpress.XtraEditors.TextEdit TxtTotalPersentase;
        private System.Windows.Forms.Label label34;
        internal DevExpress.XtraEditors.TextEdit TxtTotalBobot;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit TxtTotalPrice;
        private System.Windows.Forms.Label LblTotalPrice;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OD;
        private DevExpress.XtraEditors.LookUpEdit LueItCode;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtNumber;
        private DevExpress.XtraEditors.LookUpEdit LuePPN;
        private DevExpress.XtraEditors.LookUpEdit LuePPh;
    }
}