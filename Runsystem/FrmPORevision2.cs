﻿#region Update
/*
    24/01/2022 [TYO/PHT] New Apps, Based on PORevision
    24/01/2022 [TYO/PHT] Rename lable Quantity menjadi New Quantity
    24/01/2022 [TYO/PHT] Filter Item by Item category group user login by parameter IsFilterByItCt
    24/01/2022 [TYO/PHT] Menambahkan field Grand Total PO &  Addendum Availability berdasarkan parameter 
    26/01/2022 [TYO/ PHT] Membuat field New Quantity menjadi editable
    27/01/2022 [TYO/PHT] validasi Uncheck/Check Addendum Indicator dari IEP
    04/02/2022 [RIS/PHT] Feedback merubah source quantity
    08/02/2022 [RIS/PHT] BUG : merubah source IsAddendumAvailabilityValid
    11/02/2022 [RIS/PHT] BUG : Addendum Availability
    03/06/2022 [RIS/PHT] FeedBack : Merubah Source Loop
    15/06/2022 [ICA/PHT] QtyOld dan QtyNew ketika nilai nya 0, ambil dari Qty PO
    27/06/2022 [TYO/PHT] Bug : NewAmt dan OldAmt ketika nilainya 0, ambil dari PO
*/
#endregion

#region Namespace


using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmPORevision2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application;
            DNoPO = string.Empty, DNoPOR = string.Empty, DNoQtOld = string.Empty, DNoQtNew = string.Empty,
            mItCode = string.Empty, mVdCode = "",
            mFormPrintOutPORevision = string.Empty,
            mPOPrintOutCompanyLogo = string.Empty;
        string mType = string.Empty;
        private bool mIsPORevisionApprovalUseDept = false;
        internal FrmPORevision2Find FrmFind;
        internal bool
            mIsFilterBySite = false,
            mIsAutoGeneratePurchaseLocalDocNo = false,
            mIsGroupPaymentTermActived = false,
            mIsPOApprovalActived = false,
            mIsBOMShowSpecifications = false,
            mIsPORevisionInfoOnlyEnabled = false,
            mIsPORevisionShowPOService = false,
            mIsPORevisionUseETA = false,
            mIsFilterByItCt = false,
            mAddendumInd = false;
        private string 
            mMenuCodeForPRService = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty;
        private byte[] downloadedData;

        private List<LocalDocument> mlLocalDocument = null;

        #endregion

        #region Constructor

        public FrmPORevision2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Purchase Order Revision";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                ChkCancelInd.Visible = false;
                
                GetValue();
                mlLocalDocument = new List<LocalDocument>();
                LblNewQty.ForeColor = Color.Red;
                Tc1.SelectedTabPage = Tp2;
                Sl.SetLueTaxCode(ref LueTaxCode);
                MeePORemarkHdrOld.Properties.MaxLength = 10040;
                MeePORemarkHdrNew.Properties.MaxLength = 10040;
                MeePORemarkDtlOld.Properties.MaxLength = 10040;
                MeePORemarkDtlNew.Properties.MaxLength = 10040;
                Tc1.SelectedTabPage = Tp1;
                base.FrmLoad(sender, e);
                SetGrd();
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                if (!mIsPORevisionShowPOService) label31.Visible = TxtPOServiceDocNo.Visible = BtnPOServiceDocNo.Visible = false;
                if (!mIsPORevisionUseETA)
                {
                    label33.Visible = DteEstTimeArrivalOld.Visible = false;
                    label32.Visible = DteEstTimeArrivalNew.Visible = false;
                    label29.Visible = DteEstRecvDt.Visible = true;
                }

                else
                {
                    label33.Visible = DteEstTimeArrivalOld.Visible = true;
                    label32.Visible = DteEstTimeArrivalNew.Visible = true;
                    label29.Visible = DteEstRecvDt.Visible = false;
                }

                


            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "User", 
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[]{ 30, 200, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtLocalDocNo, TxtVdCode, TxtPODocNo, 
                        TxtStatus, TxtQtOld, TxtQtyNew, TxtQtyOld, TxtDiscountAmtOld, 
                        TxtTotalNew, TxtTotalOld, TxtDiscountAmtNew, TxtQtNew, ChkCancelInd, 
                        TxtItCode, TxtPORDocNo, TxtTotalNew, TxtTotalOld, MeeRemark, TxtUnitPriceOld, 
                        TxtUnitPriceNew, TxtDiscNew, TxtDiscOld, TxtRoundingValueNew, TxtRoundingValueOld, 
                        MeePORemarkHdrOld, MeePORemarkDtlOld, MeePORemarkHdrNew, MeePORemarkDtlNew, DteEstTimeArrivalOld, 
                        DteEstTimeArrivalNew, LueTaxCode, DteEstRecvDt, TxtFile, ChkFile
                    }, true);
                    BtnPODocNo.Enabled = false;
                    BtnQtNew.Enabled = false;
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = (TxtFile.Text.Length > 0);
                    ChkCancelInd.Visible = false;
                    Grd1.ReadOnly = true;
                    BtnEdit.Visible = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueTaxCode, DteEstRecvDt, ChkFile
                    }, false);

                    if (!mIsAutoGeneratePurchaseLocalDocNo)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);

                   

                    TxtStatus.Text = "Outstanding";
                    BtnPODocNo.Enabled = true;
                    BtnQtNew.Enabled = true;
                    BtnFile.Enabled = true;
                    BtnDownload.Enabled = false;
                    ChkCancelInd.Checked = false;
                    DteDocDt.Focus();
                    BtnEdit.Visible = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ ChkCancelInd }, false);
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNo, DteDocDt, TxtLocalDocNo, TxtPODocNo, TxtStatus, 
               TxtQtOld, TxtVdCode, TxtQtNew, TxtPORDocNo, TxtItCode,  
               TxtTotalNew, TxtTotalOld, MeeRemark, MeePORemarkHdrNew, MeePORemarkDtlNew, 
               MeePORemarkHdrOld, MeePORemarkDtlOld, DteEstTimeArrivalNew, DteEstTimeArrivalOld, LueTaxCode, DteEstRecvDt, TxtFile, TxtGrandTotal, TxtAddendumAvailability
            });
            ChkCancelInd.Checked = false;
            ChkFile.Checked = false;
            PbUpload.Value = 0;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtDiscountAmtOld, TxtDiscountAmtNew, TxtTotalOld, TxtTotalNew, TxtQtyNew, TxtQtyOld, TxtUnitPriceOld, TxtUnitPriceNew,
                TxtUnitPriceNew, TxtUnitPriceOld,
                TxtDiscNew, TxtDiscOld, TxtRoundingValueNew, TxtRoundingValueOld
            }, 0);
            Grd1.Rows.Clear();
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPORevision2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;
            ParPrint(TxtDocNo.Text);
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            string LocalDocNo = mIsAutoGeneratePurchaseLocalDocNo ? string.Empty : TxtLocalDocNo.Text;
            string
                SeqNo = string.Empty,
                DeptCode = string.Empty,
                ItSCCode = string.Empty,
                Mth = string.Empty,
                Yr = string.Empty,
                Revision = string.Empty;

            if (mIsAutoGeneratePurchaseLocalDocNo)
            {
                SetLocalDocument
                (
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr
                );

                if (mlLocalDocument.Count == 0) return;

                SetRevision(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr,
                    ref Revision
                    );

                if (SeqNo.Length > 0)
                {
                    SetLocalDocNo(
                        "PORevision",
                        ref LocalDocNo,
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr,
                        ref Revision
                        );
                }
                mlLocalDocument.Clear();
            }

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PORevision", "TblPORevision");

            var cml = new List<MySqlCommand>();

            // Aturan PO revision : 1=harga naik harus approve 2=harga naik/turun tidak harus approve 3=harga naik/turun harus approve)

            #region Type 2

            if (mType == "2")
            {
                cml.Add(SavePORevision(
                    DocNo,
                    LocalDocNo,
                    SeqNo,
                    DeptCode,
                    ItSCCode,
                    Mth,
                    Yr,
                    Revision
                    ));
                cml.Add(SavePORevisionApproval(DocNo));
                Sm.ExecCommands(cml);
            }
            #endregion

            #region Type 1
            else if (mType == "1")
            {
                if (Decimal.Parse(TxtTotalNew.Text) > Decimal.Parse(TxtTotalOld.Text))
                {
                    cml.Add(SavePORevision(
                        DocNo,
                        LocalDocNo,
                        SeqNo,
                        DeptCode,
                        ItSCCode,
                        Mth,
                        Yr,
                        Revision
                        ));
                    cml.Add(SavePORevisionApproval(DocNo));
                    Sm.ExecCommands(cml);
                }
                else
                {
                    cml.Add(SavePORevision(
                        DocNo,
                        LocalDocNo,
                        SeqNo,
                        DeptCode,
                        ItSCCode,
                        Mth,
                        Yr,
                        Revision
                    ));
                    cml.Add(UpdatePOR(DocNo));
                    cml.Add(UpdatePO(DocNo));
                    Sm.ExecCommands(cml);
                }
            }
            #endregion

            #region Type 3
            else 
            {
                 cml.Add(SavePORevision(
                        DocNo,
                        LocalDocNo,
                        SeqNo,
                        DeptCode,
                        ItSCCode,
                        Mth,
                        Yr,
                        Revision
                        ));
                    cml.Add(SavePORevisionApproval(DocNo));
                    cml.Add(UpdatePOR(DocNo));
                    cml.Add(UpdatePO(DocNo));
                    Sm.ExecCommands(cml);
            }
            #endregion 

            if (mIsPORevisionUseETA) 
                UpdatePOEstTimeArrival(DocNo);

            if (TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile(DocNo);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtPODocNo, "PO#", false) ||
                Sm.IsTxtEmpty(TxtQtNew, "New Quotation#", false) ||
                Sm.IsTxtEmpty(TxtQtyNew, "New Quantity", false) ||
                IsQtNewBiggerThanOld() ||
                IsPOAlreadyReceived() ||
                IsUploadFileNotValid() ||
                IsOtherPOUsedTheSameQt() ||
                (mAddendumInd == true && IsAddendumAvailabilityValid());
        }

        private bool IsOtherPOUsedTheSameQt()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo And C.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' And D.DocNo=@PODocNo And D.DNo=@PODNo ");
            SQL.AppendLine("Where A.DocNo<>@PODocNo ");
            SQL.AppendLine("And A.Status In ('O', 'A') Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PODocNo", TxtPODocNo.Text);
            Sm.CmParam<String>(ref cm, "@PODNo", DNoPO);

            var PO = Sm.GetValue(cm);
            if (PO.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Another PO ("+PO+") already used this PO request#.");
                return true;
            }
            return false;
        }

        private bool IsQtNewBiggerThanOld()
        {
            decimal 
                PriceOld = Decimal.Parse(TxtUnitPriceOld.Text),
                PriceNew = Decimal.Parse(TxtUnitPriceNew.Text);

            if (mType == "2")
            {
                if (PriceOld > 0 && PriceNew > 0)
                {
                    if (PriceNew >= PriceOld)
                    {
                        Sm.StdMsg(mMsgType.Warning, "New price should be cheaper than previous price.");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsPOAlreadyReceived()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.PODocNo=@PODocNo ");
            SQL.AppendLine("    And B.PODNo=@PODNo ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And B.Status In ('O', 'A') ");
            SQL.AppendLine("Where A.POInd='Y';");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PODocNo", TxtPODocNo.Text);
            Sm.CmParam<String>(ref cm, "@PODNo", DNoPO);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This item already received.");
                return true;
            }
            return false;
        }

        private bool IsUploadFileNotValid()
        {
            return
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted()
             ;
        }

        private bool IsAddendumAvailabilityValid()
        {
            //decimal AddendumAvailability = Convert.ToDecimal(TxtAddendumAvailability.Text),
            //NewAmt = Convert.ToDecimal(TxtTotalNew.Text),
            //GrandTotal = Convert.ToDecimal(TxtGrandTotal.Text),
            //Qty = Convert.ToDecimal(TxtQtyOld.Text),
            //UnitPrice = Convert.ToDecimal(TxtUnitPriceOld.Text),
            //Result = AddendumAvailability - NewAmt +(GrandTotal - (Qty * UnitPrice)),
            //Result2 = AddendumAvailability - (GrandTotal - (Qty * UnitPrice));

            //revisi
            decimal AddendumAvailability = Convert.ToDecimal(TxtAddendumAvailability.Text),
            TotalNew = Convert.ToDecimal(TxtTotalNew.Text),
            TotalOld = Convert.ToDecimal(TxtTotalOld.Text),
            TotalAlert = AddendumAvailability + TotalOld,
            //GrandTotal = Convert.ToDecimal(TxtGrandTotal.Text),
            //TotalOther = GrandTotal - Convert.ToDecimal(TxtTotalOld.Text),
            //TotalAfter = TotalNew + TotalOther,
            AddendumDiff = TotalNew - TotalOld;


            if (AddendumDiff > AddendumAvailability)
            {
              Sm.StdMsg(mMsgType.Warning, "Total amount of PO value Can't more than " + Sm.FormatNum(TotalAlert, 0));
              return true;
            }

                else return false;
            
        }

        private bool IsFTPClientDataNotValid()
        {

            if (TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPORevision ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        #region Generate Local Document

        private void SetLocalDocNo(
            string DocType,
            ref string LocalDocNo,
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            var ShortCode = Sm.GetValue("Select IfNull(ShortCode, DeptCode) From TblDepartment Where DeptCode='" + DeptCode + "'");
            LocalDocNo = SeqNo + "/" + DocAbbr + "/" + ShortCode + "/" + ItSCCode + "/" + Mth + "/" + Yr;
            if (Revision.Length > 0 && Revision != "0")
                LocalDocNo = LocalDocNo + "/R" + Revision;
        }

        private void SetRevision(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select IfNull(Revision, '0') ");
            SQL.AppendLine("From TblPORevision ");
            SQL.AppendLine("Where SeqNo Is Not Null ");
            SQL.AppendLine("And SeqNo=@SeqNo ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And ItSCCode=@ItSCCode ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr ");
            SQL.AppendLine("Order By Revision Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            Revision = Sm.GetValue(cm);
            if (Revision.Length == 0)
                Revision = "0";
            else
                Revision = (int.Parse(Revision) + 1).ToString();
        }


        private void SetLocalDocument(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr)
        {
            mlLocalDocument.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision ");
            SQL.AppendLine("From TblPOHdr Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtPODocNo.Text);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "LocalDocNo", "SeqNo", "DeptCode", "ItSCCode", "Mth", 
                    
                    //6-7
                    "Yr", "Revision" 
                });
                if (dr.HasRows)
                {
                    string
                        DocNoTemp = string.Empty,
                        LocalDocNoTemp = string.Empty,
                        SeqNoTemp = string.Empty,
                        DeptCodeTemp = string.Empty,
                        ItSCCodeTemp = string.Empty,
                        MthTemp = string.Empty,
                        YrTemp = string.Empty,
                        RevisionTemp = string.Empty;

                    while (dr.Read())
                    {
                        DocNoTemp = Sm.DrStr(dr, c[0]);
                        LocalDocNoTemp = Sm.DrStr(dr, c[1]);
                        SeqNoTemp = Sm.DrStr(dr, c[2]);
                        DeptCodeTemp = Sm.DrStr(dr, c[3]);
                        ItSCCodeTemp = Sm.DrStr(dr, c[4]);
                        MthTemp = Sm.DrStr(dr, c[5]);
                        YrTemp = Sm.DrStr(dr, c[6]);
                        RevisionTemp = Sm.DrStr(dr, c[7]);

                        mlLocalDocument.Add(new LocalDocument()
                        {
                            DocNo = DocNoTemp,
                            LocalDocNo = LocalDocNoTemp,
                            SeqNo = SeqNoTemp,
                            DeptCode = DeptCodeTemp,
                            ItSCCode = ItSCCodeTemp,
                            Mth = MthTemp,
                            Yr = YrTemp,
                            Revision = RevisionTemp
                        });

                        if (SeqNoTemp.Length > 0)
                        {
                            SeqNo = SeqNoTemp;
                            DeptCode = DeptCodeTemp;
                            ItSCCode = ItSCCodeTemp;
                            Mth = MthTemp;
                            Yr = YrTemp;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        private MySqlCommand SavePORevision(
            string DocNo,
            string LocalDocNo,
            string SeqNo,
            string DeptCode,
            string ItSCCode,
            string Mth,
            string Yr,
            string Revision
            )
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPORevision");
            SQL.AppendLine("(DocNo, DocDt, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ");
            SQL.AppendLine("CancelInd, Status, PODocNo, PODNo, QtDocNoOld, QtDNoOld, DiscountOld, DisCountAmtOld, RoundingValueOld, AmtOld, PORemarkHdrOld, PORemarkDtlOld, ");
            SQL.AppendLine("QtDocNo, QtDNo, Discount, DiscountAmt, RoundingValue, Amt, PORemarkHdr, PORemarkDtl, ");
            if (mIsPORevisionUseETA) SQL.AppendLine("EstTimeArrivalOld, EstTimeArrival, ");
            if (mIsPORevisionInfoOnlyEnabled) SQL.AppendLine("TaxCode, EstRecvDt, ");
            SQL.AppendLine("QtyOld, Qty, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @LocalDocNo, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, ");
            SQL.AppendLine("'N', @Status, @PODocNo, @PODno, @QtDocNoOld, @QtDNoOld, @DiscountOld, @DisCountAmtOld, @RoundingValueOld, @AmtOld, @PORemarkHdrOld, @PORemarkDtlOld, ");
            SQL.AppendLine("@QtDocNo, @QtDNo, @Discount, @DiscountAmt, @RoundingValue, @Amt, @PORemarkHdr, @PORemarkDtl, ");
            if (mIsPORevisionUseETA) SQL.AppendLine("@EstTimeArrivalOld, @EstTimeArrival, ");
            if (mIsPORevisionInfoOnlyEnabled) SQL.AppendLine("@TaxCode, @EstRecvDt, ");
            SQL.AppendLine("@QtyOld, @Qty, ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()); ");
            if (mType == "2")
            {
                SQL.AppendLine("Update TblPODtl Set ");
                if (mIsPORevisionUseETA) SQL.AppendLine("EstTimeArrival=@EstTimeArrival, ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And DNo=@DNo ");
                SQL.AppendLine("And Exists(Select 1 From TblPORevision Where DocNo=@DocNo And CancelInd='N' And Status='A') ");
                SQL.AppendLine("; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            if (mType == "2")
            {
                Sm.CmParam<String>(ref cm, "@Status", "O");
            }
            if (mType == "3")
            {
                Sm.CmParam<String>(ref cm, "@Status", "O");
            }
            if (mType == "1")
            {
                if (Decimal.Parse(TxtTotalNew.Text) > Decimal.Parse(TxtTotalOld.Text))
                {
                    Sm.CmParam<String>(ref cm, "@Status", "O");
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@Status", "A");
                }
            }

            Sm.CmParam<String>(ref cm, "@PODocNo", TxtPODocNo.Text);
            Sm.CmParam<String>(ref cm, "@PODNo", DNoPO);
            Sm.CmParam<String>(ref cm, "@QtDocNoOld", TxtQtOld.Text);
            Sm.CmParam<String>(ref cm, "@QtDNoOld", DNoQtOld);
            Sm.CmParam<Decimal>(ref cm, "@DiscountOld", Decimal.Parse(TxtDiscOld.Text));
            Sm.CmParam<Decimal>(ref cm, "@DisCountAmtOld", Decimal.Parse(TxtDiscountAmtOld.Text));
            Sm.CmParam<Decimal>(ref cm, "@RoundingValueOld", Decimal.Parse(TxtRoundingValueOld.Text));
            Sm.CmParam<Decimal>(ref cm, "@AmtOld", Decimal.Parse(TxtTotalOld.Text));
            Sm.CmParam<String>(ref cm, "@PORemarkHdrOld", MeePORemarkHdrOld.Text );
            Sm.CmParam<String>(ref cm, "@PORemarkDtlOld", MeePORemarkDtlOld.Text);
            Sm.CmParam<String>(ref cm, "@QtDocNo", TxtQtNew.Text);
            Sm.CmParam<String>(ref cm, "@QtDNo", DNoQtNew);
            Sm.CmParam<Decimal>(ref cm, "@Discount", Decimal.Parse(TxtDiscNew.Text));
            Sm.CmParam<Decimal>(ref cm, "@DiscountAmt", Decimal.Parse(TxtDiscountAmtNew.Text));
            Sm.CmParam<Decimal>(ref cm, "@RoundingValue", Decimal.Parse(TxtRoundingValueNew.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtTotalNew.Text));
            Sm.CmParam<String>(ref cm, "@PORemarkHdr", MeePORemarkHdrNew.Text);
            Sm.CmParam<String>(ref cm, "@PORemarkDtl", MeePORemarkDtlNew.Text);
            if (mIsPORevisionUseETA) 
            {
                Sm.CmParamDt(ref cm, "@EstTimeArrivalOld", Sm.GetDte(DteEstTimeArrivalOld));
                Sm.CmParamDt(ref cm, "@EstTimeArrival", Sm.GetDte(DteEstTimeArrivalNew)); 
            }
            if (mIsPORevisionInfoOnlyEnabled)
            {
                Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LueTaxCode));
                Sm.CmParamDt(ref cm, "@EstRecvDt", Sm.GetDte(DteEstRecvDt));
            }
            
            Sm.CmParam<Decimal>(ref cm, "QtyOld", Decimal.Parse(TxtQtyOld.Text));
            Sm.CmParam<Decimal>(ref cm, "Qty", Decimal.Parse(TxtQtyNew.Text));
            
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private void UpdatePOEstTimeArrival(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPODtl Set ");
            SQL.AppendLine("EstTimeArrival=@EstTimeArrival, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@PODocNo ");
            SQL.AppendLine("And DNo=@PODNo ");
            SQL.AppendLine("And Exists(Select 1 From TblPORevision Where DocNo=@DocNo And CancelInd='N' And Status='A') ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@PODocNo", TxtPODocNo.Text);
            Sm.CmParam<String>(ref cm, "@PODNo", DNoPO);
            Sm.CmParamDt(ref cm, "@EstTimeArrival", Sm.GetDte(DteEstTimeArrivalNew));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);
        }

        private MySqlCommand SavePORevisionApproval(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='PORevision' ");
            if (mIsPORevisionApprovalUseDept)
            {
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select Distinct C.DeptCode ");
                SQL.AppendLine("    From TblPODtl A ");
                SQL.AppendLine("    Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
                SQL.AppendLine("    Inner Join TblMaterialRequestHdr C On B.MaterialRequestDocNo=C.DocNo ");
                SQL.AppendLine("    Where A.DocNo=@PODocNo And A.DNo=@PODNo ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblPORevision Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists(Select 1 From TblDocApproval Where DocType='PORevision' And DocNo=@DocNo); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (mIsPORevisionApprovalUseDept)
            {
                Sm.CmParam<String>(ref cm, "@PODocNo", TxtPODocNo.Text);
                Sm.CmParam<String>(ref cm, "@PODNo", DNoPO);
            }
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdatePO(string DocNo)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Update TblPODtl Set ");
            if (mIsPORevisionInfoOnlyEnabled) SQL.AppendLine("EstRecvDt=@EstRecvDt, ");
            //SQL.AppendLine("Qty = @Qty, ");
            SQL.AppendLine("    Discount=@Discount, DiscountAmt=@DiscountAmt, RoundingValue=@RoundingValue, Remark=@PORemarkDtlNew, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");
            SQL.AppendLine("And Exists(Select 1 From TblPORevision Where DocNo=@PORevisionDocNo And CancelInd='N' And Status='A') ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblPOHdr As T1  ");
            SQL.AppendLine("Inner Join (");
            SQL.AppendLine("    Select A.DocNo, SUM((IFNULL(D.Qty, A.Qty)*C.UPrice)-((IFNULL(D.Qty, A.Qty)*C.UPrice)*A.Discount/100)-A.DiscountAmt+A.RoundingValue) Amt "); // qty rev
            SQL.AppendLine("    From TblPODtl A ");
            SQL.AppendLine("    Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
            SQL.AppendLine("    Inner Join TblQtDtl C On B.QtDocNo=C.DocNo And B.QtDNo=C.DNo ");
            SQL.AppendLine("    Left Join TblPORevision D ON A.DocNo = D.PODocNo AND A.DNo = D.PODNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocNo=@DocNo ");
            SQL.AppendLine("    Group By A.DocNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("Left Join TblTax T3 On T1.TaxCode1=T3.TaxCode ");
            SQL.AppendLine("Left Join TblTax T4 On T1.TaxCode2=T4.TaxCode ");
            SQL.AppendLine("Left Join TblTax T5 On T1.TaxCode3=T5.TaxCode ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("T1.TaxAmt=(IfNull(T2.Amt, 0)*(IfNull(T3.TaxRate, 0)/100))+(IfNull(T2.Amt, 0)*(IfNull(T4.TaxRate, 0)/100))+(IfNull(T2.Amt, 0)*(IfNull(T5.TaxRate, 0)/100)), ");
            SQL.AppendLine("T1.Amt=IfNull(T2.Amt, 0)+((IfNull(T2.Amt, 0)*(IfNull(T3.TaxRate, 0)/100))+(IfNull(T2.Amt, 0)*(IfNull(T4.TaxRate, 0)/100))+(IfNull(T2.Amt, 0)*(IfNull(T5.TaxRate, 0)/100)))+T1.CustomsTaxAmt-T1.DiscountAmt, ");
            SQL.AppendLine("T1.Remark=@PORemarkHdrNew, ");
            SQL.AppendLine("T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where T1.DocNo=@DocNo ");
            SQL.AppendLine("And Exists(Select 1 From TblPORevision Where DocNo=@PORevisionDocNo And CancelInd='N' And Status='A') ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblStockPrice As T1  ");
            SQL.AppendLine("Inner Join TblRecvVdDtl T2 ");
            SQL.AppendLine("    On Substring(T1.Source, 4, length(T1.Source)-7)=T2.DocNo ");
            SQL.AppendLine("    And Right(T1.Source, 3)=T2.DNo ");
            SQL.AppendLine("    And T2.PODocNo=@DocNo ");
            SQL.AppendLine("    And T2.PODNo=@DNo ");
            SQL.AppendLine("    And T2.CancelInd='N' ");
            SQL.AppendLine("    And T2.Status='A' ");
            SQL.AppendLine("Set T1.UPrice=@UPrice, T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Exists(Select 1 From TblPORevision Where DocNo=@PORevisionDocNo And CancelInd='N' And Status='A') ");
            SQL.AppendLine("; ");

            if (mMenuCodeForPRService.Length > 0)
            {
                //VIN/IMS/15/06/2021: Update PO for Service 
                SQL.AppendLine("Update TblMaterialRequestDtl T1 ");
                SQL.AppendLine("Inner Join (");
                SQL.AppendLine("    Select A.DocNo, C.UPrice Amt ");
                SQL.AppendLine("    From TblMaterialRequestDtl A ");
                SQL.AppendLine("    Inner Join TblPORequestDtl B On A.DocNo=B.MaterialRequestDocNo And A.DNo=B.MaterialRequestDNo ");
                SQL.AppendLine("    Inner Join TblQtDtl C On B.QtDocNo=C.DocNo And B.QtDNo=C.DNo ");
                SQL.AppendLine("    Where A.CancelInd='N' ");
                SQL.AppendLine("    And A.DocNo=@MRDocNo ");
                SQL.AppendLine("    And A.DNo In ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        SELECT B.MaterialRequestDNo ");
                SQL.AppendLine("        FROM TblPODtl A ");
                SQL.AppendLine("        INNER JOIN TblPORequestDtl B ON A.PORequestDocNo=B.DocNo AND A.PORequestDNo=B.DNo ");
                SQL.AppendLine("        WHERE A.DocNo = @DocNo AND B.MaterialRequestDocNo=@MRDocNo ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("    Group By A.DocNo ");
                SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("Set ");
                if (mIsPORevisionInfoOnlyEnabled) SQL.AppendLine("EstRecvDt=@EstRecvDt, ");
                SQL.AppendLine("T1.UPrice=IfNull(T2.Amt, 0)-ifnull(T1.DiscountAmt, 0), ");
                SQL.AppendLine("T1.Discount=@Discount, T1.DiscountAmt=@DiscountAmt, T1.RoundingValue=@RoundingValue, ");
                SQL.AppendLine("T1.Remark=@PORemarkDtlNew, T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where T1.DocNo=@MRDocNo ");
                SQL.AppendLine("And T1.DNo In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT B.MaterialRequestDNo ");
                SQL.AppendLine("    FROM TblPODtl A ");
                SQL.AppendLine("    INNER JOIN TblPORequestDtl B ON A.PORequestDocNo=B.DocNo AND A.PORequestDNo=B.DNo ");
                SQL.AppendLine("    WHERE A.DocNo = @DocNo AND B.MaterialRequestDocNo=@MRDocNo ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And T1.MaterialRequestServiceDocNo Is Not Null ");
                SQL.AppendLine("; ");
            }

            SQL.AppendLine("UPDATE TblMaterialRequestHdr As T1 ");  
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.DocNo, Sum((A.Qty*A.UPrice)-((A.Qty*A.UPrice)*A.Discount/100)-A.DiscountAmt+A.RoundingValue) Amt "); 
            SQL.AppendLine("    From TblMaterialRequestDtl A  ");
            SQL.AppendLine("    Where A.CancelInd='N'  ");
            SQL.AppendLine("    And A.DocNo=@MRDocNo  ");
            SQL.AppendLine("    Group By A.DocNo  ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo "); 
            SQL.AppendLine("Left Join TblTax T3 On T1.TaxCode1=T3.TaxCode  ");
            SQL.AppendLine("Left Join TblTax T4 On T1.TaxCode2=T4.TaxCode  ");
            SQL.AppendLine("Left Join TblTax T5 On T1.TaxCode3=T5.TaxCode  ");
            SQL.AppendLine("Set  ");
            SQL.AppendLine("T1.Amt=IfNull(T2.Amt, 0)+((IfNull(T2.Amt, 0)*(IfNull(T3.TaxRate, 0)/100))+(IfNull(T2.Amt, 0)*(IfNull(T4.TaxRate, 0)/100))+(IfNull(T2.Amt, 0)*(IfNull(T5.TaxRate, 0)/100))), ");
            SQL.AppendLine("T1.Remark=@PORemarkHdrNew,  ");
            SQL.AppendLine("T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime()  ");
            SQL.AppendLine("Where T1.DocNo=@MRDocNo  ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PORevisionDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocNo", TxtPODocNo.Text);
            Sm.CmParam<String>(ref cm, "@MRDocNo", TxtPOServiceDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNoPO);
            Sm.CmParam<Decimal>(ref cm, "@Discount", Decimal.Parse(TxtDiscNew.Text));
            Sm.CmParam<Decimal>(ref cm, "@DiscountAmt", Decimal.Parse(TxtDiscountAmtNew.Text));
            Sm.CmParam<Decimal>(ref cm, "@RoundingValue", Decimal.Parse(TxtRoundingValueNew.Text));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Decimal.Parse(TxtUnitPriceNew.Text));
            Sm.CmParam<String>(ref cm, "@PORemarkHdrNew", MeePORemarkHdrNew.Text);
            Sm.CmParam<String>(ref cm, "@PORemarkDtlNew", MeePORemarkDtlNew.Text);
            if (mIsPORevisionInfoOnlyEnabled) Sm.CmParamDt(ref cm, "@EstRecvDt", Sm.GetDte(DteEstRecvDt));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Decimal.Parse(TxtQtyNew.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdatePOR(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPORequestDtl SET QtDocNo=@QtDocNo, QtDNo=@QtDNo, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("And Exists(Select 1 From TblPORevision Where DocNo=@PORevisionDocNo And CancelInd='N' And Status='A') ");
            SQL.AppendLine("; ");
            if (TxtPOServiceDocNo.Text.Length > 0)
            {
                SQL.AppendLine("Update tblMaterialRequestDtl SET QtDocNo=@QtDocNo, QtDNo=@QtDNo, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@MRDocNo And DNo=@DNo ");
                SQL.AppendLine("And Exists(Select 1 From TblPORevision Where DocNo=@PORevisionDocNo And CancelInd='N' And Status='A') ");
                SQL.AppendLine("; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtPORDocNo.Text);
            Sm.CmParam<String>(ref cm, "@MRDocNo", TxtPOServiceDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNoPOR);
            Sm.CmParam<String>(ref cm, "@QtDocNo", TxtQtNew.Text);
            Sm.CmParam<String>(ref cm, "@QtDNo", DNoQtNew);
            Sm.CmParam<String>(ref cm, "@PORevisionDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdatePORevisionFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPORevision Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        #endregion

        #endregion

        #region Cancel Data

        private void EditData()
        {
            try
            {
                if (IsEditedDataNotValid()) return;
            
                Cursor.Current = Cursors.WaitCursor;

                EditPORevision();
                ShowData(TxtDocNo.Text); 
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "PO Revision#", false) ||
                IsDataAlreadyCancelled() ||
                IsDataAlreadyApproved();
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 From TblPORevision Where DocNo=@Param And (CancelInd='Y' Or Status='C');",
                TxtDocNo.Text,
                "This document is already cancelled.");
        }

        private bool IsDataAlreadyApproved()
        {
            return Sm.IsDataExist(
                "Select 1 From TblPORevision Where DocNo=@Param And CancelInd='N' And Status='A';",
                TxtDocNo.Text,
                "This document is already approved.");
        }

        private void EditPORevision()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblPORevision Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo And CancelInd='N' And Status='O'; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowPORevision(DocNo);
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPORevision(string DocNo)
        {
           
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As Status, A.PODocNo, A.PODNo, D.VdName, ");
                SQL.AppendLine("E.Qty, A.QtDocNoOld, A.QtDNoOld, A.DiscountOld, A.DiscountAmtOld, ");
                SQL.AppendLine("A.RoundingValueOld, ");
                SQL.AppendLine("(((100 - A.DiscountOld)/100) * (If(A.QtyOld=0 And A.Qty=0, E.Qty, A.QtyOld) * B.UPrice)) - A.DiscountAmtOld + A.RoundingValueOld As TotalOld, ");
                SQL.AppendLine("A.QtDocNo, A.QtDNo, A.Discount, A.DiscountAmt, ");
                SQL.AppendLine("A.RoundingValue, G2.DocNo POServiceDocNo, ");
                SQL.AppendLine("(((100 - A.Discount)/100) * (If(A.QtyOld=0 And A.Qty=0, E.Qty, A.Qty) * B1.UPrice)) - A.DiscountAmt + A.RoundingValue As TotalNew, "); // qty = po rev ini qty new A.Qty new
                SQL.AppendLine("A.Remark, H.ItName, F.DocNo As DocNoPOR, A.CancelInd, B.UPrice As UPriceOld, B1.UPrice, A.PORemarkHdrOld, A.PORemarkDtlOld, A.PORemarkHdr, A.PORemarkDtl, ");
                SQL.AppendLine("I.Amt, I.AddendumAvailability, If(A.QtyOld=0 And A.Qty=0, E.Qty, A.QtyOld) as QtyOld, If(A.QtyOld=0 And A.Qty=0, E.Qty, A.Qty) as QtyNew, ");


                if (mIsPORevisionInfoOnlyEnabled)
                    SQL.AppendLine("A.TaxCode, A.EstRecvDt, "); 
                else
                    SQL.AppendLine("Null As TaxCode, Null As EstRecvDt, ");
                if (mIsPORevisionUseETA)
                    SQL.AppendLine("A.EstTimeArrivalOld, A.EstTimeArrival, "); 
                else
                    SQL.AppendLine("Null As EstTimeArrivalOld, Null As EstTimeArrival, ");
                
                SQL.AppendLine("A.FileName ");
                SQL.AppendLine("From TblPORevision A ");
                SQL.AppendLine("Inner Join TblQtDtl B On A.QtDocNoOld = B.DocNo And A.QtDNoOld = B.DNo ");
                SQL.AppendLine("Inner Join TblQtDtl B1 On A.QtDocNo = B1.DocNo And A.QtDNo = B1.DNo ");
                SQL.AppendLine("Inner Join TblQtHdr C On A.QtDocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblVendor D On C.VdCode = D.VdCode  ");
                SQL.AppendLine("Inner Join TblPODtl E On A.PODocNo = E.DocNo And A.PODNo = E.Dno ");
                SQL.AppendLine("Inner Join TblPORequestDtl F On E.PORequestDocNo = F.DocNo And E.PORequestDNo = F.DNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl G On F.MaterialRequestDocNo = G.DocNo And F.MaterialRequestDNo= G.DNo ");
                SQL.AppendLine("Left Join TblMaterialRequestDtl G2 On F.MaterialRequestDocNo = G2.DocNo And F.MaterialRequestDNo= G2.DNo And G2.MaterialRequestServiceDocNo Is Not Null ");
                SQL.AppendLine("Inner Join TblItem H On G.ItCode = H.ItCode ");
                SQL.AppendLine("Inner Join TblPOHdr I ON E.DocNo = I.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo;");

                Sm.ShowDataInCtrl(
                   ref cm, SQL.ToString(),
                   new string[] { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "LocalDocNo", "Status", "PODocNo", "PODNo", 
                        
                        //6-10
                        "VdName", "Qty", "QtDocNoOld", "QtDNoOld", "DiscountOld", 
                        
                        //11-15
                        "DiscountAmtOld", "RoundingValueOld", "TotalOld", "QtDocNo", "QtDNo",   
                        
                        //16-20
                        "Discount", "DiscountAmt", "RoundingValue", "TotalNew", "Remark", 
                        
                        //21-25
                        "ItName", "DocNoPOR", "CancelInd", "UPriceOld", "UPrice",

                        //26-30
                        "PORemarkHdrOld", "PORemarkDtlOld", "PORemarkHdr", "PORemarkDtl", "EstTimeArrivalOld",
                        
                        //31-35
                         "EstTimeArrival", "TaxCode", "EstRecvDt", "POServiceDocNo", "FileName",

                         //36-39
                         "Amt", "AddendumAvailability", "QtyOld", "QtyNew"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[3]);
                        TxtPODocNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtVdCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtQtyOld.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[38]),0);
                        TxtQtyNew.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[39]), 0);
                        TxtQtOld.EditValue = Sm.DrStr(dr, c[8]);
                        TxtUnitPriceOld.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[24]), 0);
                        TxtUnitPriceNew.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[25]), 0);
                        TxtDiscOld.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]),0);
                        TxtDiscountAmtOld.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]),0);
                        TxtRoundingValueOld.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]),0);
                        TxtTotalOld.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]),0);
                        TxtQtNew.EditValue = Sm.DrStr(dr, c[14]);
                        TxtDiscNew.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]),0);
                        TxtDiscountAmtNew.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]),0);
                        TxtRoundingValueNew.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]),0);
                        TxtTotalNew.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[19]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[20]);
                        TxtItCode.EditValue = Sm.DrStr(dr, c[21]);
                        TxtPORDocNo.EditValue = Sm.DrStr(dr, c[22]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[23]), "Y");
                        MeePORemarkHdrOld.EditValue = Sm.DrStr(dr, c[26]);
                        MeePORemarkDtlOld.EditValue = Sm.DrStr(dr, c[27]);
                        MeePORemarkHdrNew.EditValue = Sm.DrStr(dr, c[28]);
                        MeePORemarkDtlNew.EditValue = Sm.DrStr(dr, c[29]);
                        Sm.SetDte(DteEstTimeArrivalOld, Sm.DrStr(dr, c[30]));
                        Sm.SetDte(DteEstTimeArrivalNew, Sm.DrStr(dr, c[31]));
                        Sm.SetLue(LueTaxCode, Sm.DrStr(dr, c[32]));
                        Sm.SetDte(DteEstRecvDt, Sm.DrStr(dr, c[33]));
                        TxtPOServiceDocNo.EditValue = Sm.DrStr(dr, c[34]);
                        TxtFile.EditValue = Sm.DrStr(dr, c[35]);
                        TxtGrandTotal.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[36]), 0);
                        TxtAddendumAvailability.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[37]) - Sm.DrDec(dr, c[36]), 0);
                    }, true
            );
        }

       

        private void ShowDocApproval(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.ApprovalDNo, B.UserName, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, " +
                    "Case When A.LastUpDt Is Not Null Then  " +
                    "A.LastUpDt " +
                    "Else Null End As LastUpDt, A.Remark " +
                    "From TblDocApproval A " +
                    "Left Join TblUser B On A.UserCode = B.UserCode " +
                    "Where A.DocType='PORevision' " +
                    "And Status In ('A', 'C') " +
                    "And A.DocNo=@DocNo " +
                    "Order By A.ApprovalDNo ",

                    new string[] 
                    { 
                        "ApprovalDNo",
                        "UserName","StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        


        internal void AddendumValidated()
        {
            if (mAddendumInd && TxtPODocNo.Text.Length > 0)
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                {
                 TxtQtNew, TxtQtyNew, TxtDiscNew, TxtDiscountAmtNew, MeePORemarkHdrNew,
                 MeePORemarkDtlNew, DteEstTimeArrivalNew, MeeRemark
                }, false);
            }

            if (!mAddendumInd && TxtPODocNo.Text.Length > 0)
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                {
                 MeePORemarkHdrNew, MeePORemarkDtlNew, DteEstTimeArrivalNew,
                 MeeRemark
                }, false);

                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                {
                 TxtQtNew, TxtQtyNew, TxtDiscNew, TxtDiscountAmtNew
                }, true);
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetValue()
        {
            mIsPOApprovalActived = Sm.IsDataExist("Select 1 from TblDocApprovalSetting Where DocType='PO' Limit 1;");
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsPORevisionApprovalUseDept = Sm.GetParameterBoo("IsPORevisionApprovalUseDept");
            mIsAutoGeneratePurchaseLocalDocNo = Sm.GetParameterBoo("IsAutoGeneratePurchaseLocalDocNo");
            mType = Sm.GetParameter("PORevisionRule");
            mIsGroupPaymentTermActived = Sm.GetParameterBoo("IsGroupPaymentTermActived");
            mFormPrintOutPORevision = Sm.GetParameter("FormPrintOutPORevision");
            mPOPrintOutCompanyLogo = Sm.GetParameter("POPrintOutCompanyLogo");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsPORevisionInfoOnlyEnabled = Sm.GetParameterBoo("IsPORevisionInfoOnlyEnabled");
            mIsPORevisionShowPOService = Sm.GetParameterBoo("IsPORevisionShowPOService");
            mMenuCodeForPRService = Sm.GetParameter("MenuCodeForPRService");
            mIsPORevisionUseETA = Sm.GetParameterBoo("IsPORevisionUseETA");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            

        }

        internal void ComputeTotalNew()
        {
            decimal Total = 0m, Disc = 0m, UPrice = 0m, Qty = 0m, RoundValue = 0m, DiscAmt = 0m;

            Disc = Decimal.Parse(TxtDiscNew.Text);
            Qty = Decimal.Parse(TxtQtyNew.Text);
            RoundValue = Decimal.Parse(TxtRoundingValueNew.Text);
            DiscAmt = Decimal.Parse(TxtDiscountAmtNew.Text);
            UPrice = Decimal.Parse(TxtUnitPriceNew.Text);

            Total = (((100 - Disc) / 100) * (Qty * UPrice)) - DiscAmt + RoundValue;
            TxtTotalNew.EditValue = Sm.FormatNum(Total, 0);
        }

        private void ParPrint(string DocNo)
        {
            var l = new List<PORevision>();
            var ldtl = new List<PORevDtl>();
            var ldtl2 = new List<PORevDtl2>();
            var ldtl3 = new List<PORevDtl3>();
            var ldtl4 = new List<PORevDtl4>();
            string[] TableName = { "PORevision", "PORevDtl", "PORevDtl2", "PORevDtl3", "PORevDtl4" };
            List<IList> myLists = new List<IList>();
            int Nomor = 1;

            #region Header
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (mIsFilterBySite)
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, I.CompanyName, I.CompanyPhone, I.CompanyFax, I.CompanyAddress, '' As CompanyAddressCity, ");
            }
            else
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            }
            SQL.AppendLine("If((Select parvalue From TblParameter Where ParCode = 'IsFilterLocalDocNo')='Y',A.LocalDocNo,A1.Docno) As DocNo, DATE_FORMAT(A1.DocDt,'%d %M %Y') As DocDt, ");
            SQL.AppendLine("Concat(RIGHT(A1.DocDt, 2), '/', RIGHT(LEFT(A1.DocDt, 6), 2), '/', RIGHT(LEFT(A1.DocDt, 4), 2))As DocDt2, A.VdContactPerson, A.ShipTo, A.BillTo, ");
            SQL.AppendLine("IfNull(A.TaxCode1, null) As TaxCode1,IfNull(A.TaxCode2, null) As TaxCode2, IfNull(A.TaxCode3, null) As TaxCode3, A.TaxAmt, A.CustomsTaxAmt, ");
            SQL.AppendLine("A1.DiscountAmt, A1.Amt, A.DownPayment, A.Remark As ARemark, F.VdName, ");
            SQL.AppendLine(" (Select Z.TaxName From TblTax Z ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode1 ");
            SQL.AppendLine("Inner Join TblPORevision X ON Y.DocNo = X.PODocNo Where X.DocNo= @DocNo) TaxName1, ");
            SQL.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode2 ");
            SQL.AppendLine("Inner Join TblPORevision X ON Y.DocNo = X.PODocNo Where X.DocNo= @DocNo) TaxName2, ");
            SQL.AppendLine("(Select  Z.TaxName From TblTax Z ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode3 ");
            SQL.AppendLine("Inner Join TblPORevision X ON Y.DocNo = X.PODocNo Where X.DocNo= @DocNo) TaxName3, ");
            SQL.AppendLine("Concat(Upper(left(G.UserName,1)),Substring(Lower(G.UserName), 2, Length(G.UserName)))As CreateBy, ");
            SQL.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode1 ");
            SQL.AppendLine("Inner Join TblPORevision X ON Y.DocNo = X.PODocNo Where X.DocNo= @DocNo) TaxRate1, ");
            SQL.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode2 ");
            SQL.AppendLine("Inner Join TblPORevision X ON Y.DocNo = X.PODocNo Where X.DocNo= @DocNo) TaxRate2, ");
            SQL.AppendLine("(Select  Z.TaxRate From TblTax Z ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode3 ");
            SQL.AppendLine("Inner Join TblPORevision X ON Y.DocNo = X.PODocNo Where X.DocNo= @DocNo) TaxRate3, ");
            SQL.AppendLine("F.Address, F.Phone, F.Fax, F.Email, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', H.ContactNumber, E.CurCode, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='IsPOSplitBasedOnTax') As SplitPO, J.SiteName, A.LocalDocNo, K.PtDay, ");
            SQL.AppendLine("D.QtDocNo, Date_Format(E.DocDt,'%d %M %Y')As QtDocDt , M.PosName, H.Position 'VdPOSName',  ");
            SQL.AppendLine("Concat(RIGHT(N.DocDt, 2), '/', RIGHT(LEFT(N.DocDt, 6), 2), '/', RIGHT(LEFT(N.DocDt, 4), 2))As MRDocDt, O.BankAcNo, P.BankName, Q.VdCtName, GROUP_CONCAT(DISTINCT N.DocNo SEPARATOR ', ') MRDocNo, R.ProjectName ");
            SQL.AppendLine("From TblPORevision A1 ");
            SQL.AppendLine("Inner Join TblPOHdr A On A1.PODocNo = A.DocNo");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo And A1.PODNo = B.DNo ");
            SQL.AppendLine("Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblVendor F On E.VdCode=F.VdCode ");
            SQL.AppendLine("Inner Join TblUser G On A.CreateBy=G.UserCode ");
            SQL.AppendLine("Left Join TblVendorContactPerson H On A.VdContactPerson=H.ContactPersonName And A.VdCode = H.VdCode ");
            SQL.AppendLine("Left Join TblSite J On A.SiteCode = J.SiteCode ");
            SQL.AppendLine("Left Join TblPaymentTerm K On E.PtCode=K.PtCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select distinct A1.DocNo, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                SQL.AppendLine("    From TblPORevision A1  ");
                SQL.AppendLine("    Inner Join TblPOhdr A On A1.PODocNo = A.DocNo ");
                SQL.AppendLine("    Inner Join TblSite B On A.SiteCode = B.SiteCode  ");
                SQL.AppendLine("    Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode  ");
                SQL.AppendLine("    Inner Join TblEntity D On C.EntCode = D.EntCode  ");
                SQL.AppendLine("    Where A1.DocNo=@DocNo And A.SiteCode is not null ");
                SQL.AppendLine(") I On A1.DocNo = I.DocNo ");
            }

            SQL.AppendLine("Left Join TblEmployee L On G.UserCode = L.UserCode ");
            SQL.AppendLine("Left Join TblPosition M On L.PosCode = M.PosCode ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr N On D.MaterialRequestDocNo = N.DocNo");
            SQL.AppendLine("Left Join TblVendorBankAccount O On F.VdCode = O.VdCode ");
            SQL.AppendLine("Left Join TblBank P On O.BankCode = P.BankCode ");
            SQL.AppendLine("Left Join TblVendorCategory Q On Q.VdCtCode = F.VdCtCode ");
            SQL.AppendLine("Left JOIN (  ");
            SQL.AppendLine("    SELECT T1.DocNo, GROUP_CONCAT(IFNULL(T2.ProjectName, '') Separator '\r\n') ProjectName ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1  ");
            SQL.AppendLine("    Left Join TblProjectGroup T2 ON T1.PGCode = T2.PGCode  ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") R ON R.DocNo = N.DocNo ");
            SQL.AppendLine("Where A1.DocNo=@DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                if (mIsFilterBySite)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select D.EntLogoName " +
                       "From TblPOhdr A  " +
                       "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                       "Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode " +
                       "Inner Join TblEntity D On C.EntCode = D.EntCode  " +
                       "Where A.Docno='" + TxtDocNo.Text + "' "
                   );
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo(mPOPrintOutCompanyLogo));
                }

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                     //0
                     "CompanyName",
                     //1-5
                     "CompanyAddress",
                     "CompanyAddressCity",
                     "CompanyPhone",
                     "CompanyFax",
                     "DocNo",                        
                     //6-10
                     "DocDt",
                     "VdContactPerson",
                     "ShipTo" ,
                     "BillTo" ,
                     "TaxCode1",
                     //11-15
                     "TaxCode2",
                     "TaxCode3" ,
                     "TaxAmt",
                     "CustomsTaxAmt",
                     "DiscountAmt" ,
                     //16-20
                     "Amt" ,
                     "DownPayment" ,
                     "VdName",
                     "CompanyLogo",
                     "ARemark",
                     //21-25
                     "TaxName1",
                     "TaxName2",
                     "Taxname3" ,
                     "CreateBy",
                     "TaxRate1",
                     //26-30
                     "TaxRate2",
                     "TaxRate3",
                     "Address",
                     "Phone",
                     "Fax",
                     //31-35
                     "Email",
                     "ContactNumber",
                     "SplitPO",
                     "SiteName",
                     "LocalDocNo",

                     //36-39
                     "CurCode",
                     "PtDay",
                     "QtDocNo",
                     "QtDocDt",
                     "PosName",

                     //40-45
                     "VdPosName",
                     "MRDocDt",
                     "BankAcNo",
                     "BankName",
                     "VdCtName",

                     //46-48
                     "DocDt2",
                     "MRDocNo",
                     "ProjectName"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PORevision()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),

                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyLongAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyFax = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),

                            DocDt = Sm.DrStr(dr, c[6]),
                            VdContactPerson = Sm.DrStr(dr, c[7]),
                            ShipTo = Sm.DrStr(dr, c[8]),
                            BillTo = Sm.DrStr(dr, c[9]),
                            TaxCode1 = Sm.DrStr(dr, c[10]),

                            TaxCode2 = Sm.DrStr(dr, c[11]),
                            TaxCode3 = Sm.DrStr(dr, c[12]),
                            TaxAmt = Sm.DrDec(dr, c[13]),
                            CustomsTaxAmt = Sm.DrDec(dr, c[14]),
                            DiscountAmt = Sm.DrDec(dr, c[15]),

                            Amt = Sm.DrDec(dr, c[16]),
                            DownPayment = Sm.DrDec(dr, c[17]),
                            VdName = Sm.DrStr(dr, c[18]),
                            CompanyLogo = Sm.DrStr(dr, c[19]),
                            ARemark = Sm.DrStr(dr, c[20]),

                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            TaxName1 = Sm.DrStr(dr, c[21]),
                            TaxName2 = Sm.DrStr(dr, c[22]),
                            TaxName3 = Sm.DrStr(dr, c[23]),
                            CreateBy = Sm.DrStr(dr, c[24]),
                            TaxRate1 = Sm.DrDec(dr, c[25]),

                            TaxRate2 = Sm.DrDec(dr, c[26]),
                            TaxRate3 = Sm.DrDec(dr, c[27]),
                            Address = Sm.DrStr(dr, c[28]),
                            Phone = Sm.DrStr(dr, c[29]),
                            Fax = Sm.DrStr(dr, c[30]),

                            Email = Sm.DrStr(dr, c[31]),
                            ContactNumber = Sm.DrStr(dr, c[32]),
                            SplitPO = Sm.DrStr(dr, c[33]),
                            SiteName = Sm.DrStr(dr, c[34]),
                            LocalDocNo = Sm.DrStr(dr, c[35]),

                            CurCode = Sm.DrStr(dr, c[36]),
                            PtDay = Sm.DrDec(dr, c[37]),
                            QtDocNo = Sm.DrStr(dr, c[38]),
                            QtDocDt = Sm.DrStr(dr, c[39]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[16])),

                            Terbilang2 = Sm.Terbilang2(Sm.DrDec(dr, c[16])),
                            PosName = Sm.DrStr(dr, c[40]),
                            VdPosName = Sm.DrStr(dr, c[41]),
                            MRDocDt = Sm.DrStr(dr, c[42]),
                            BankAcNo = Sm.DrStr(dr, c[43]),

                            BankName = Sm.DrStr(dr, c[44]),
                            VdCtName = Sm.DrStr(dr, c[45]),
                            DocDt2 = Sm.DrStr(dr, c[46]),
                            MRDocNo = Sm.DrStr(dr, c[47]),
                            ProjectName = Sm.DrStr(dr, c[48]),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region PORevDtl
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select F.ItCode, I.ItName, ");
                SQLDtl.AppendLine("(B.Qty-ifnull(M.Qty, 0)) As Qty, I.PurchaseUomCode, K.PtName, G.CurCode, H.UPrice, ");
                SQLDtl.AppendLine("A.Discount, A.DiscountAmt, A.Roundingvalue, Concat(RIGHT(B.EstRecvDt, 2), '/', RIGHT(LEFT(B.EstRecvDt, 6), 2), '/', RIGHT(LEFT(B.EstRecvDt, 4), 2)) As EstRecvDt, B.Remark As DRemark, ");
                SQLDtl.AppendLine("(H.Uprice * (B.Qty-ifnull(M.Qty, 0))) - (H.Uprice*(B.Qty-ifnull(M.Qty, 0))*A.Discount/100) - A.DiscountAmt + A.Roundingvalue  As Total, L.DtName, I.ForeignName, I.ItCodeInternal, I.Specification ");
                SQLDtl.AppendLine("FROM TblPORevision A ");
                SQLDtl.AppendLine("INNER Join TblPODtl B ON A.PODocNo = B.DocNo AND A.PODNo = B.DNo ");
                SQLDtl.AppendLine("Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
                SQLDtl.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
                SQLDtl.AppendLine("Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
                SQLDtl.AppendLine("Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo ");
                SQLDtl.AppendLine("Inner Join TblQtHdr G On A.QtDocNo=G.DocNo ");
                SQLDtl.AppendLine("Inner Join TblQtDtl H On A.QtDocNo=H.DocNo And A.QtDNo=H.DNo ");
                SQLDtl.AppendLine("Inner Join TblItem I On F.ItCode=I.ItCode ");
                SQLDtl.AppendLine("Inner Join TblPaymentTerm K On G.PtCode=K.PtCode ");
                SQLDtl.AppendLine("Left Join TblDeliveryType L On G.DtCode = L.DtCode ");
                SQLDtl.AppendLine("Left join( ");
                SQLDtl.AppendLine("     Select PODocNo, PODNo, Sum(Qty)Qty ");
                SQLDtl.AppendLine("     From TblPOQtyCancel where cancelind='N' ");
                SQLDtl.AppendLine("     Group by PODocNo, PODNo ");
                SQLDtl.AppendLine(")M On B.DocNo=M.PODocNo And B.DNo=M.PODNo ");
                SQLDtl.AppendLine("Where B.CancelInd='N' AND A.DocNo=@DocNo ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ItCode" ,

                         //1-5
                         "ItName" ,
                         "Qty",
                         "PurchaseUomCode" ,
                         "PtName",
                         "CurCode" ,

                         //6-10
                         "UPrice" ,
                         "Discount" ,
                         "DiscountAmt",
                         "RoundingValue",
                         "EstRecvDt" ,

                         //11-15
                         "DRemark" ,
                         "Total",
                         "DtName",
                         "ForeignName",
                         "ItCodeInternal",

                         //16
                         "Specification"

                        });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new PORevDtl()
                        {
                            nomor = nomor,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),

                            Qty = Sm.DrDec(drDtl, cDtl[2]),
                            PurchaseUomCode = Sm.DrStr(drDtl, cDtl[3]),
                            PtName = Sm.DrStr(drDtl, cDtl[4]),
                            CurCode = Sm.DrStr(drDtl, cDtl[5]),

                            UPrice = Sm.DrDec(drDtl, cDtl[6]),
                            Discount = Sm.DrDec(drDtl, cDtl[7]),
                            DiscountAmt = Sm.DrDec(drDtl, cDtl[8]),
                            RoundingValue = Sm.DrDec(drDtl, cDtl[9]),
                            EstRecvDt = Sm.DrStr(drDtl, cDtl[10]),

                            DRemark = Sm.DrStr(drDtl, cDtl[11]),
                            DTotal = Sm.DrDec(drDtl, cDtl[12]),
                            DtName = Sm.DrStr(drDtl, cDtl[13]),
                            ForeignName = Sm.DrStr(drDtl, cDtl[14]),
                            ItCodeInternal = Sm.DrStr(drDtl, cDtl[15]),

                            Specification = Sm.DrStr(drDtl, cDtl[16]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            #endregion

            #region Detail Signature

            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                if (mIsPOApprovalActived)
                {
                    SQLDtl2.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                    SQLDtl2.AppendLine("T1.UserCode, T1.UserName, T3.PosName, ");
                    SQLDtl2.AppendLine("T1.DNo, T1.Seq As Level, @Space As Space, ");
                    SQLDtl2.AppendLine("T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                    SQLDtl2.AppendLine("From ( ");

                    SQLDtl2.AppendLine("    Select UserCode, UserName, DNo, Seq, Title, LastUpDt From ( ");
                    SQLDtl2.AppendLine("        Select Distinct B.UserCode, C.UserName, ");
                    SQLDtl2.AppendLine("        B.ApprovalDNo As DNo, 200+D.Level As Seq, ");
                    SQLDtl2.AppendLine("        'Approved By' As Title, ");
                    SQLDtl2.AppendLine("        Left(B.LastUpDt, 8) As LastUpDt ");
                    SQLDtl2.AppendLine("        From TblPORevision A1 ");
                    SQLDtl2.AppendLine("        INNER JOIN TblPOHdr A ON A1.PODocNo = A.DocNo ");
                    SQLDtl2.AppendLine("       Inner Join TblDocApproval B On B.DocType='PO' And A.DocNo=B.DocNo ");
                    SQLDtl2.AppendLine("        Inner Join TblUser C On B.UserCode=C.UserCode ");
                    SQLDtl2.AppendLine("        Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType='PO' ");
                    SQLDtl2.AppendLine("        Where A.Status='A' ");
                    SQLDtl2.AppendLine("        AND A1.DocNo=@DocNo ");
                    SQLDtl2.AppendLine("        And B.UserCode Not In (Select CreateBy From TblPOHdr Where DocNo = @DocNo) ");
                    SQLDtl2.AppendLine("        Order By D.Level DESC ");
                    //SQLDtl2.AppendLine("Limit 1 ");
                    SQLDtl2.AppendLine("    ) Tbl ");

                    SQLDtl2.AppendLine("   Union All ");
                    SQLDtl2.AppendLine("    Select Distinct ");
                    SQLDtl2.AppendLine("    A1.CreateBy As UserCode, B.UserName, ");
                    SQLDtl2.AppendLine("    '1' As DNo, 0 As Seq, 'Created By' As Title, LEFT(A1.CreateDt, 8) As LastUpDt ");
                    SQLDtl2.AppendLine("   From TblPORevision A1 ");
                    SQLDtl2.AppendLine("    INNER JOIN TblPOHdr A ON A1.PODocNo = A.DocNo ");
                    SQLDtl2.AppendLine("    Inner Join TblUser B ON A1.CreateBy=B.UserCode ");
                    SQLDtl2.AppendLine("    Where A.Status='A' ");
                    SQLDtl2.AppendLine("    AND A1.DocNo=@DocNo ");

                    SQLDtl2.AppendLine("    Union All ");
                    SQLDtl2.AppendLine("   Select UserCode, UserName, DNo, Seq, Title, LastUpDt From ( ");
                    SQLDtl2.AppendLine("       Select Distinct D.UserCode, E.UserName, ");
                    SQLDtl2.AppendLine("        D.ApprovalDNo As DNo, 100+F.Level As Seq, 'Approved By' As Title, Left(D.LastUpDt, 8) As LastUpDt ");
                    SQLDtl2.AppendLine("        From TblPOHdr A ");
                    SQLDtl2.AppendLine("        Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
                    SQLDtl2.AppendLine("        Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo And C.Status='A' And C.CancelInd='N' ");
                    SQLDtl2.AppendLine("        Inner Join TblDocApproval D On D.DocType='PORequest' And C.DocNo=D.DocNo And C.DNo=D.DNo ");
                    SQLDtl2.AppendLine("        Inner Join TblUser E On D.UserCode=E.UserCode ");
                    SQLDtl2.AppendLine("        Inner Join TblDocApprovalSetting F On D.ApprovalDNo=F.DNo And F.DocType='PORequest' ");
                    SQLDtl2.AppendLine("        Where A.Status='A' ");
                    SQLDtl2.AppendLine("        And A.DocNo=@DocNo ");
                    SQLDtl2.AppendLine("        And D.UserCode Not In (Select CreateBy From TblPOHdr Where DocNo = @DocNo) ");
                    SQLDtl2.AppendLine("        Order By F.Level ");
                    //SQLDtl2.AppendLine("Limit 1 
                    SQLDtl2.AppendLine("    ) Tbl ");
                    SQLDtl2.AppendLine(") T1 ");
                    SQLDtl2.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                    SQLDtl2.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                    SQLDtl2.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                    SQLDtl2.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                    SQLDtl2.AppendLine("Order By T1.Seq; ");
                }
                else
                {
                    SQLDtl2.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                    SQLDtl2.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                    SQLDtl2.AppendLine("From ( ");
                    SQLDtl2.AppendLine("    Select Distinct ");
                    SQLDtl2.AppendLine("    B.UserCode, C.UserName, "); //Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName,
                    SQLDtl2.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                    SQLDtl2.AppendLine("    From TblPORevision A1 ");
                    SQLDtl2.AppendLine("    Inner Join TblPODtl A ON A1.DocNo = A.DocNo And A1.PODNo = A.DNo ");
                    SQLDtl2.AppendLine("    Inner Join TblDocApproval B On B.DocType='PORequest' And A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
                    SQLDtl2.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                    SQLDtl2.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'POrequest' ");
                    SQLDtl2.AppendLine("    Where A.CancelInd='N' AND A1.DocNo=@DocNo ");
                    SQLDtl2.AppendLine("   Union All ");
                    SQLDtl2.AppendLine("   Select Distinct ");
                    SQLDtl2.AppendLine("    A.CreateBy As UserCode, B.UserName, "); //Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName)))
                    SQLDtl2.AppendLine("    '1' As DNo, 0 As Level, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                    SQLDtl2.AppendLine("    FROM TblPORevision A1 ");
                    SQLDtl2.AppendLine("    Inner Join TblPODtl A ON A1.PODocNo = A.DocNo AND A1.PODNo = A.DNo ");
                    SQLDtl2.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                    SQLDtl2.AppendLine("    Where A.CancelInd='N' AND A1.DocNo=@DocNo ");
                    SQLDtl2.AppendLine(") T1 ");
                    SQLDtl2.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                    SQLDtl2.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                    SQLDtl2.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                    SQLDtl2.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Title ");
                    SQLDtl2.AppendLine("Order By T1.Level; ");
                }
                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", DocNo);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {

                        ldtl2.Add(new PORevDtl2()
                        {
                            Signature = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            PosName = Sm.DrStr(drDtl2, cDtl2[2]),
                            Space = Sm.DrStr(drDtl2, cDtl2[3]),
                            DNo = Sm.DrStr(drDtl2, cDtl2[4]),
                            Title = Sm.DrStr(drDtl2, cDtl2[5]),
                            LastUpDt = Sm.DrStr(drDtl2, cDtl2[6])
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail date
            var cmDtl3 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;

                SQLDtl3.AppendLine("Select * From ( ");
                SQLDtl3.AppendLine("Select DATE_FORMAT(SUBSTRING(LastUpDt, 1, 8),'%d %M %Y') As LastUpDt, ApprovalDNo As DNo ");
                SQLDtl3.AppendLine("From TblDocApproval ");
                SQLDtl3.AppendLine("Where DocType = 'PORequest' ");
                SQLDtl3.AppendLine("And DocNo IN ( ");
		        SQLDtl3.AppendLine("        SELECT B.PORequestDocNo ");
		        SQLDtl3.AppendLine("        FROM TblPORevision A ");
		        SQLDtl3.AppendLine("        INNER JOIN TblPODTl B ON A.PODocNo = B.DocNo AND A.PODNo = B.DNo ");
		        SQLDtl3.AppendLine("        WHERE A.DocNo=@DocNo ");
	             SQLDtl3.AppendLine("   ) ");
                SQLDtl3.AppendLine("Group By DocNo, ApprovalDNo ");
                SQLDtl3.AppendLine("UNION ");
                SQLDtl3.AppendLine("Select DATE_FORMAT(SUBSTRING(B.CreateDt, 1, 8),'%d %M %Y') As LastUpDt, '1' As DNo ");
                SQLDtl3.AppendLine("From TblPORevision A ");
                SQLDtl3.AppendLine("INNER JOIN TblPOHdr B ON A.PODocNo = B.DocNo ");
                SQLDtl3.AppendLine("WHERE A.DocNo = @DocNo ");
                SQLDtl3.AppendLine(") G1 ");
                SQLDtl3.AppendLine("Order By G1.DNo; ");


                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", DocNo);
                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                        {
                         //0
                         "LastUpdt", 
                         "DNo"
                        });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        ldtl3.Add(new PORevDtl3()
                        {
                            LastUpDt = Sm.DrStr(drDtl3, cDtl3[0]),
                            DNo = Sm.DrStr(drDtl3, cDtl3[1])
                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(ldtl3);
            #endregion            

            #region Detail note
            var cmDtl4 = new MySqlCommand();

            var SQLDtl4 = new StringBuilder();
            using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl4.Open();
                cmDtl4.Connection = cnDtl4;

                SQLDtl4.AppendLine("Select ParValue From TblParameter Where ParCode='PONotes';");

                cmDtl4.CommandText = SQLDtl4.ToString();
                Sm.CmParam<String>(ref cmDtl4, "@DocNo", DocNo);
                var drDtl4 = cmDtl4.ExecuteReader();
                var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                        {
                         //0
                         "ParValue"
                        });
                if (drDtl4.HasRows)
                {
                    while (drDtl4.Read())
                    {
                        ldtl4.Add(new PORevDtl4()
                        {
                            Note = Sm.DrStr(drDtl4, cDtl4[0])
                        });
                    }
                }
                drDtl4.Close();
            }
            myLists.Add(ldtl4);
            #endregion

            Sm.PrintReport(mFormPrintOutPORevision, myLists, TableName, false);
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }


        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePORevisionFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        #endregion

        #region Event

        private void BtnPODocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPORevision2Dlg(this));
        }

        private void BtnQtNew_Click(object sender, EventArgs e)
        {
            if (TxtItCode.Text == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "PO Number Still Empty");
            }
            else
            {
                Sm.FormShowDialog(new FrmPORevision2Dlg2(this, mItCode, TxtItCode.Text));
            }
        }

        private void TxtDiscNew_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDiscNew, 0);
                ComputeTotalNew();
            }
        }

        private void TxtRoundingValueNew_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtRoundingValueNew, 0);
                ComputeTotalNew();
            }
        }

        private void TxtDiscountAmtNew_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDiscountAmtNew, 0);
                ComputeTotalNew();
            }
        }

        

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        private void TxtQtyNew_Validated(object sender, EventArgs e)
        {
            decimal NewQty = Convert.ToDecimal(TxtQtyNew.Text);
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtQtyNew, 0);
                ComputeTotalNew();
            }

            if( NewQty == 0 )
            {
                Sm.StdMsg(mMsgType.Warning, "New Quantity can't be filled 0");
                TxtQtyNew.Focus();
            }
        }

        private void LueTaxCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueTaxCode, new Sm.RefreshLue1(Sl.SetLueTaxCode));
        }

        private void BtnPODocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPODocNo, "PO#", false))
            {
                var f = new FrmPO2("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPODocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnPORequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPORDocNo, "PORequest#", false))
            {
                var f = new FrmPORequest("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPORDocNo.Text;
                f.ShowDialog();
            }
        }
        private void BtnPOServiceDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPOServiceDocNo, "PO For Service#", false))
            {
                var f = new FrmMaterialRequest4("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPOServiceDocNo.Text;
                f.ShowDialog();
            }
        }
        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }
        #endregion
        

        #region Class

        private class LocalDocument
        {
            public string DocNo { set; get; }
            public string LocalDocNo { set; get; }
            public string SeqNo { get; set; }
            public string DeptCode { get; set; }
            public string ItSCCode { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string Revision { get; set; }
        }

        private class PORevision
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyLongAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdName { get; set; }
            public string VdContactPerson { get; set; }
            public string ShipTo { get; set; }
            public string BillTo { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxCode3 { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal CustomsTaxAmt { get; set; }
            public decimal DiscountAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal DownPayment { get; set; }
            public string ARemark { get; set; }
            public string PrintBy { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public string CreateBy { get; set; }
            public decimal TaxRate1 { get; set; }
            public decimal TaxRate2 { get; set; }
            public decimal TaxRate3 { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Email { get; set; }
            public string ContactNumber { get; set; }
            public string SplitPO { get; set; }
            public string SiteName { get; set; }
            public string LocalDocNo { get; set; }
            public string CurCode { get; set; }
            public decimal PtDay { get; set; }
            public string QtDocNo { get; set; }
            public string QtDocDt { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public string PosName { get; set; }
            public string VdPosName { get; set; }
            public string MRDocDt { get; set; }
            public string BankAcNo { get; set; }
            public string BankName { get; set; }
            public string VdCtName { get; set; }
            public string DocDt2 { get; set; }
            public string ProjectName { get; set; }
            public string MRDocNo { get; set; }
        }

        private class PORevDtl
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUomCode { get; set; }
            public string PtName { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice { get; set; }
            public decimal Discount { get; set; }
            public decimal DiscountAmt { get; set; }
            public decimal RoundingValue { get; set; }
            public string EstRecvDt { get; set; }
            public string DRemark { get; set; }
            public decimal DTotal { get; set; }
            public string DtName { get; set; }
            public string ForeignName { get; set; }
            public string ItCodeInternal { get; set; }
            public string Specification { get; set; }
        }

        private class PORevDtl2
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class PORevDtl3
        {
            public string LastUpDt { get; set; }
            public string DNo { get; set; }
        }

        private class PORevDtl4
        {
            public string Note { get; set; }
        }

        #endregion

        
    }
}
