﻿#region Update
/*
    07/11/2019 [TKG/GSS] New reporting
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptOutstandingSO : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptOutstandingSO(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                SetGrd();
                SetSQL();
                Sl.SetLueCtCode(ref LueCtCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status ");
	        SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'P' Then 'Partial Fulfilled' ");
            SQL.AppendLine("    When 'M' Then 'Manual Fulfilled' ");
            SQL.AppendLine("    When 'F' Then 'Fulfilled' ");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("C.CtName, G.ItCode, G.ItName, A.CurCode, D.UPrice, B.Qty, IfNull(H.Qty, 0.00) AS Qty2, E.PriceUomCode ");
            SQL.AppendLine("From TblSOHdr A ");
            SQL.AppendLine("Inner Join TblSODtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode ");
            SQL.AppendLine("Inner Join TblCtQtDtl D On A.CtQtDocNo=D.DocNo And B.CtQtDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblItemPriceHdr E On D.ItemPriceDocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblItemPriceDtl F On D.ItemPriceDocNo=F.DocNo And D.ItemPriceDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblItem G On F.ItCode=G.ItCode ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T3.SODocNo, T3.SODNo, Sum(T2.Qty) As Qty ");
            SQL.AppendLine("    From TblDOCt2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblDRDtl T3 On T1.DRDocNo=T3.DocNo And T2.DRDNo=T3.DNo ");
            SQL.AppendLine("    Inner Join TblSOHdr T4 "); 
		    SQL.AppendLine("        On T3.SODocNo=T4.DocNo "); 
            SQL.AppendLine("        And T4.CancelInd='N' "); 
		    SQL.AppendLine("        And T4.Status In ('O', 'P')  ");
            SQL.AppendLine("        And T4.DocDt Between @DocDt1 And @DocDt2  ");
            SQL.AppendLine("    Group By T3.SODocNo, T3.SODNo ");
            SQL.AppendLine("    ) H On B.DocNo=H.SODocNo And B.DNo=H.SODNo ");
            SQL.AppendLine("Where A.Status In ('O', 'P') ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",   
                        "Status",
                        "Customer", 
                        "Item's Code",

                        //6-10
                        "Item's Name",
                        "Currency",
                        "Price",
                        "SO's Quantity",
                        "DO's Quantity",

                        //11-14
                        "UoM",
                        "SO's Amount",
                        "DO's Amount",
                        "Balance"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        130, 100, 80, 200, 100,
                        //6-10
                        200, 100, 130, 130, 130,
                        //11-14
                        60, 130, 130, 130
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 12, 13, 14 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By A.DocDt Desc, A.DocNo;",
                new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "StatusDesc", "CtName", "ItCode", "ItName", 

                        //6-10
                        "CurCode", "UPrice", "Qty", "Qty2", "PriceUomCode"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Grd.Cells[Row, 12].Value = Sm.GetGrdDec(Grd, Row, 8) * Sm.GetGrdDec(Grd, Row, 9);
                    Grd.Cells[Row, 13].Value = Sm.GetGrdDec(Grd, Row, 8) * Sm.GetGrdDec(Grd, Row, 10);
                    Grd.Cells[Row, 14].Value = Sm.GetGrdDec(Grd, Row, 12) - Sm.GetGrdDec(Grd, Row, 13);
                }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10, 12, 13, 14 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        #endregion

        #endregion


    }
}
