﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPurchasing5 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private string DocDt = string.Empty;
        private bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptPurchasing5(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt?"Y":"N");

                string CurrentDateTime = Sm.ServerCurrentDateTime();

                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

                iGCellStyle myPercentBarStyle = new iGCellStyle();
                myPercentBarStyle.CustomDrawFlags = TenTec.Windows.iGridLib.iGCustomDrawFlags.Foreground;
                myPercentBarStyle.Flags = ((TenTec.Windows.iGridLib.iGCellFlags)((TenTec.Windows.iGridLib.iGCellFlags.DisplayText | TenTec.Windows.iGridLib.iGCellFlags.DisplayImage)));
                Grd1.Cols[9].CellStyle = myPercentBarStyle;
                Grd1.Cols[7].CellStyle = myPercentBarStyle;

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("select Left(T10.DocDt, 6) As FilterMonth, T5.ItCtCode, T8.ItCtName, ");
            SQL.AppendLine("Round(Avg(T6.PtDay), 2) As AvgPtDay, Avg(T3.UPrice) As AvgPrice, ");
            SQL.AppendLine("Count(T10.DocNo) As FreqOrder, Sum(T1.Qty*T3.UPrice) As TotalOrderAmount ");
            SQL.AppendLine("From TblPoHdr T10 ");
            SQL.AppendLine("Inner Join TblPoDtl T11 On T10.DocNo = T11.DocNo And T11.CancelInd = 'N'");
            SQL.AppendLine("Inner Join tblporequestdtl T1 On T11.POREquestDocNo=T1.DocNo And T11.PORequestDNo=T1.DNo ");
            SQL.AppendLine("inner join tblmaterialrequestdtl T2 on T2.DocNo=T1.MaterialRequestDocNo ");
            SQL.AppendLine("AND T2.DNo=T1.MaterialRequestDNo AND T2.CancelInd='N' AND T2.Status<>'C' ");
            SQL.AppendLine("inner join  tblporequesthdr T9 on T1.DocNo=T9.DocNo ");
            SQL.AppendLine("inner join tblqtdtl T3 on T1.QtDocNo=T3.DocNo AND T1.QtDNo=T3.DNo ");
            SQL.AppendLine("inner join tblqthdr T4 on T4.DocNo=T3.DocNo ");
            SQL.AppendLine("inner join tblitem T5 on T3.ItCode=T5.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=T5.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("inner join tblpaymentterm T6 on T4.PtCode=T6.PtCode ");
            SQL.AppendLine("left join tblitemcategory T8 on T5.ItCtCode=T8.ItCtCode ");
            SQL.AppendLine("where T1.CancelInd='N' AND T1.Status<>'C' ");
            SQL.AppendLine("Group by Left(T10.DocDt, 6), T5.ItCtCode, T8.ItCtName ");
            SQL.AppendLine(" )T ");
            SQL.AppendLine("Where FilterMonth = @FilterMonth ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Filter "+Environment.NewLine+" Month",
                        "Item Category "+Environment.NewLine+" Code", 
                        "Item Category "+Environment.NewLine+" Name",
                        "Average "+Environment.NewLine+" TOP (days)", 
                        "Average "+Environment.NewLine+" Price",

                        //6-9
                        "Total Order "+Environment.NewLine+" Frequency",
                        "Order Frequency"+Environment.NewLine+"Percentage",
                        "Total Order "+Environment.NewLine+" Amount",
                        "Order Amount"+Environment.NewLine+"Percentage"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        50, 100, 150, 100, 200, 
                        
                        //6-9
                        150, 150, 200, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Month = Sm.GetLue(LueMth);
            
            DocDt = string.Concat(Year, Month);

            string TotalFreqOrder = Sm.GetValue("Select Sum(X.FreqOrder) As Total From (" + mSQL.Replace("@FilterMonth", "'" + DocDt + "'") + ") X");
            string TotalOrderAmt = Sm.GetValue("Select Sum(X.TotalOrderAmount) As Total From (" + mSQL.Replace("@FilterMonth", "'" + DocDt + "'") + ") X");

            decimal TotalFreq = ((TotalFreqOrder == string.Empty) || (TotalFreqOrder == "0")) ? 0 : Convert.ToDecimal(TotalFreqOrder);
            decimal TotalAmt = ((TotalOrderAmt == string.Empty) || (TotalOrderAmt == "0")) ? 0 : Convert.ToDecimal(TotalOrderAmt);

            if (Year == string.Empty && Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month And Year is Empty.");
            }
            else if (Year == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
            }
            else if (Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
            }
            else
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = " ";


                    var cm = new MySqlCommand();

                    Sm.CmParam<String>(ref cm, "@FilterMonth", DocDt);
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "ItCtCode", true);

                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter,
                            new string[]
                            { 
                                //0
                                "FilterMonth", 

                                //1-5
                                "ItCtCode", "ItCtName", "AvgPtDay", "AvgPrice", "FreqOrder", 
                                
                                //6
                                "TotalOrderAmount"
                            },

                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd1.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 1, 0);

                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);

                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 6);
                                //TotalFreq = TotalFreq + Sm.DrDec(dr, 5);
                                //TotalAmt = TotalAmt + Sm.DrDec(dr, 6);
                            }, true, false, false, true
                        );
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[]{ 6, 8 });
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Sm.FocusGrd(Grd1, 0, 1);
                    Cursor.Current = Cursors.Default;
                }
                for (int intX = 0; intX < Grd1.Rows.Count; intX++)
                {
                    if (TotalFreq == 0)
                        Grd1.Cells[intX, 7].Value = 0;
                    else
                        Grd1.Cells[intX, 7].Value = Convert.ToDouble(Sm.GetGrdDec(Grd1, intX, 6) / TotalFreq);
                }

                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (TotalAmt == 0)
                        Grd1.Cells[i, 9].Value = 0;
                    else
                        Grd1.Cells[i, 9].Value = Convert.ToDouble(Sm.GetGrdDec(Grd1, i, 8) / TotalAmt);
                }
            }
        }

        #endregion

        #region Grid
        private void Grd1_CustomDrawCellForeground_1(object sender, iGCustomDrawCellEventArgs e)
        {
            if ((e.ColIndex == 7) || (e.ColIndex == 9))
            {
                object myObjValue = Grd1.Cells[e.RowIndex, e.ColIndex].Value;
                if (myObjValue == null) 
                    return;
                Rectangle myBounds = e.Bounds;
                myBounds.Inflate(-2, -2);
                myBounds.Width = myBounds.Width - 1;
                myBounds.Height = myBounds.Height - 1;
                if (myBounds.Width > 0)
                {
                    int myWidth = 0;
                    double myValue = 0;
                    e.Graphics.FillRectangle(Brushes.Bisque, myBounds);
                    try
                    {
                        myValue = (double)myObjValue;
                    }                        
                    catch (Exception Exc)
                    {
                    }

                    if (myValue != 0)
                        myWidth = (int)(myBounds.Width * myValue);
                    e.Graphics.FillRectangle(Brushes.SandyBrown, myBounds.X, myBounds.Y, myWidth, myBounds.Height);

                    e.Graphics.DrawRectangle(Pens.SaddleBrown, myBounds);

                    StringFormat myStringFormat = new StringFormat();
                    myStringFormat.Alignment = StringAlignment.Center;
                    myStringFormat.LineAlignment = StringAlignment.Center;
                    e.Graphics.DrawString(string.Format("{0:F2}%", myValue * 100), Font, SystemBrushes.ControlText, new RectangleF(myBounds.X, myBounds.Y, myBounds.Width, myBounds.Height), myStringFormat);
                }
            }
        }

        private void Grd1_CustomDrawCellGetHeight_1(object sender, iGCustomDrawCellGetHeightEventArgs e)
        {
            e.Height = Font.Height + 4;

        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCodeFilterByItCt), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion

    }
}
