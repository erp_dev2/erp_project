﻿#region Update
/*
    11/05/2017 [TKG] Journal otomatis untuk voucher incoming payment apabila incoming payment untuk management lsg diproses menjadi voucher.
    01/07/2017 [TKG] Generate voucher request CBD untuk keperluan journal.
    18/07/2017 [TKG] Ubah proses journal incoming payment pada saat otomatis menjadi voucher
    18/01/2018 [HAR] tambah inputan project
    31/01/2018 [HAR] tambah parameter buat ngeset urutan bank account
    14/02/2018 [HAR] ambil data dari sales invoice yang baru, invoicetype = 3
    07/08/2018 [TKG] tambah informasi remark DO
    13/08/2018 [WED] tambah print out untuk KIM
    16/08/2018 [WED] feedback print out KIM : margin header 1 cm, tambah kotak di bawah print out (hardcode)
    20/08/2018 [TKG] daftar customer yang invoice amount-nya 0 (karena sudah menggunakan DP) tidak akan dimunculkan.
    23/08/2018 [TKG] Berdasarkan bank account untuk mengisi currency secara otomatis
    07/09/2018 [HAR] Save ambil data dari sales invoice project doctype = 5 (save, show) 
    16/09/2018 [WED] print out KIM tambah tanda tangan
    20/03/2019 [TKG] ubah proses setting account type voucher request untuk additional amount
    25/07/2019 [WED] BUG Printout KIM, nomor nya ambil dari SLIDocNo
    13/09/2019 [TKG] difilter berdasarkan otorisasi site
    25/09/2019 [WED/YK] project implementation ambil dari detail sales invoice
    11/11/2019 [DITA/IMS] print out baru untuk IMS
    12/11/2019 [DITA/IMS] project di printout ambil dari project group
    29/11/2019 [WED/IMS] parameter IsBOMShowSpecifications untuk nampilkan project name, project code
    21/01/2020 [TKG/SIER] berdasarkan parameter IsIncomingPaymentProjectSystemEnabled, incoming payment digunakan untuk modul project system
    26/02/2020 [VIN/KBN] Bank Account masih belum terfilter sesuai group tambah parameter IsVoucherBankAccountFilteredByGrp
    05/03/2020 [IBL/KBN] Penomoran dokumen 6 digit, reset tiap tahun, dan berdasarkan entity
    13/03/2020 [DITA/KBN] berdasarkan parameter rate di incoming payment default nya ngambil dari daily currency rate
    30/03/2020 [WED/ALL] Incoming Payment bisa mengurangi dari Customer Deposit Summary, berdasarkan parameter IsIncomingPaymentUseDeposit
    02/04/2020 [WED/ALL] bug di voucher request detail masih nyimpan nominal sebelum DP. belum insert ke deposit movement
    26/08/2020 [TKG/ALL] bug saat insert vr detail (saat ada deposit amount)
    14/10/2020 [VIN/IMS] tambah parameter mIsIncomingPaymentOnlyShowDataAfterInsert, show data setelah save 
    22/10/2020 [TKG/SIER] Berdasarkan parameter IsIncomingPaymentAmtUseCOAAmt, Incoming payment amount dipengaruhi nilai amount nomor rekening COA piutang customer.
    02/12/2020 [IBL/SIER] Lue customer menamplikan customer category, berdasarkan parameter IsCustomerComboShowCategory
    08/12/2020 [TKG/SIER] bug saat menampilkan SI
    16/12/2020 [ICA/SIER] Menambah format baru untuk menampilkan SetLueBankAcCode based on parameter BankAccountFormat
    29/12/2020 [ICA/SIER] Mengubah format lue bankaccount
    17/01/2021 [TKG/PHT] ubah GenerateDocNo
    26/01/2021 [DITA/SIER] Bank Name langsung ter-input sesuai dengan Bank Account
    02/02/2021 [WED/PHT] item category gak boleh beda berdasarkan parameter IsItemCategoryUseCOAAPAR
    03/02/2021 [WED/SIER] munculin COA customer saat klik lup di COA List, berdasarkan parameter IsIncomingPaymentCOAListShowCustomerAccount
    03/02/2021 [WED/SIER] munculin COA PPH Filnal saat klik lup di COA List, berdasarkan parameter DefaultCOAListIncomingPayment
    05/02/2021 [TKG/SIER] apabila menggunakan coa, dan membuat hanya 1 voucher request, maka isi data coa tetap dimasukkan di voucher request tsb (Parameter : IsIncomingPaymentUseCOA).
    28/02/2021 [ICA/SIER] Lue customer yg muncul berdasarkan customer category nya based on param IsCustomerComboBasedOnCategory
    12/03/2021 [ICA/KSM] menambahkan field customer category based on parameter IsIncomingPaymentUseCustomerCategory
    09/04/2021 [ICA/SIER] menambah kondisi di setluebankaccode yg muncul hanya bank active
    16/04/2021 [BRI/SIER] menambahkan field total amt based on parameter IsIncomingPaymentAmtUseCOAAmt, merubah field coaamt
    21/04/2021 [WED/ALL] COA bisa dipilih lebih dari sekali berdasarkan parameter IsCOACouldBeChosenMoreThanOnce
    29/04/2021 [RDH/SIER] FEEDBACK : menambahkan lup dialog customer 
    05/05/2021 [WED/SRN] adjustment nilai incoming dari tab COA bukan dari COA Piutang Usaha, tapi dari COA Bank berdasarkan parameter IncomingPaymentCOAAmtCalculationMethod & IsIncomingPaymentAmtUseCOAAmt
    06/05/2021 [WED/SRN] IncomingPaymentCOAAmtCalculationMethod dibuat variable internal karna dipakai di dialognya
    18/05/2021 [RDH/SIER] FEEDBACK : penyesuaian customer category berdasarkan grop login
    18/06/2021 [SET/SIER] Kolom item category pada menu Incoming Payment terfilter berdasarkan grup log in.
    05/01/2022 [TRI/YK] BUG ketika show data incoming payment dan SLInya adalah SLI for project belum bisa tampil
    25/01/2022 [WED/GSS] BUG saat generate Voucher Request#, belum menambahkan melihat ke parameter IsVoucherDocSeqNoEnabled
    18/02/2022 [TKG/GSS] Merubah getParameter() dan proses save
    22/02/2022 [VIN/ALL] Tambah union sales return invoice
    25/02/2022 [VIN/IOK] BUG save detail24
    22/03/2022 [VIN/YK] BUG show hdr belum di group by
    04/04/2022 [WED/ALL] UpdateSalesInvoiceProcessInd dan UpdateSalesReturnInvoiceProcessInd query nya masih salah
                         Join ke tbl detail nya 2 kali, tapi kolom2 yang di join kurang, sehingga ProcessInd yang ter update bukannya P atau O tapi malah F
    27/07/2022 [MAU/SIER] Tambah field PaymentDt , ditampilkan di detail. Tambah Parameter IsIncomingPaymentUsePaymentDate
    03/07/2022 [RDA/SIER] tambah field advance charge di tab coa's account list
    09/08/2022 [MAU/SIER] Default unhide kolom Date dan loop
    09/08/2022 [SET/SIER] Parameter baru VRDescForIncomingPaymentDetail
    18/08/2022 [TYO/PRODUCT] inisialisasi parameter IsGroupCOAActived
    18/08/2022 [TYO/PRODUCT] inisialisasi parameter IsFilterByDept
    21/09/2022 [ICA/SKI] bug : muncul warning ketika show incomingpaymentdtl24 karena grid5 tidak ada localname
    23/09/2022 [VIN/ALL] bug : computeamtsi kurang ifnull 
    05/12/2022 [ICA/YK] menambah opsi parameter IncomingPaymentCOAAmtCalculationMethod untuk YK, berdasarkan AccountSet nya (CustomerAcNAR)
    05/02/2023 [MAU/PHT] validasi monthly closing journal VR VAT
    08/02/2022 [BRI/MNET] upload di detail berdasarkan param IsIncomingPaymentAllowToUploadFile
    12/02/2023 [MAU/MNET] penyesuaian source Cost Center journal transaksi IP 
    21/03/2023 [SET/PHT] penyesuain panjang field Account
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Net;

#endregion

namespace RunSystem
{
    public partial class FrmIncomingPayment : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDefaultCOAListIncomingPayment = string.Empty,
            mCustomerAcNoAR = string.Empty,
            mIncomingPaymentCOAAmtCalculationMethod = "2", 
           mDocNo = string.Empty; //if this application is called from other application;
        internal FrmIncomingPaymentFind FrmFind;
        private string 
            mMainCurCode="IDR",
            mEmpCodeIncomingPayment = string.Empty,
            mBankAccountFormat = "0",
            mVoucherCodeFormatType = "1",
            mDocNoFormat = string.Empty,
            mEntCode = string.Empty,
            mDepositMovementDocTypeCreate = "09",
            mDepositMovementDocTypeCancel = "10",
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty,
            mVoucherIncomingPaymentCCJournalFormat = string.Empty;

        internal string mMInd = "N",
            mVRDescForIncomingPaymentDetail = string.Empty;

        internal bool

            mIsFilterBySite = false,
            mIsUseMInd = false,
            mIsUseActivePeriod = false,
            mIsIncomingPaymentShowDORemark = false,
            mIsBOMShowSpecifications = false,
            mIsIncomingPaymentProjectSystemEnabled = false,
            mIsVoucherBankAccountFilteredByGrp = false,
            mIsIncomingPaymentOnlyShowDataAfterInsert = false,
            mIsCustomerComboShowCategory = false,
            mIsIncomingPaymentCOAListShowCustomerAccount = false,
            mIsCustomerComboBasedOnCategory = false,
            mIsIncomingPaymentUseCustomerCategory = false,
            mIsCOACouldBeChosenMoreThanOnce = false,
            mIsShowCustomerCategory = false,
            mIsFilterByCtCt = false,
            mIsItCtFilteredByGroup = false,
            mIsDOCtAmtRounded = false,
            mIsIncomingPaymentUsePaymentDate = false,
            mIsIncomingPaymentUseAdvanceCharge = false,
            mIsGroupCOAActived = false,
            mIsFilterByDept = false
            ;

        private bool mIsIncomingPaymentUseCOA = false, 
            mIsAutoJournalActived = false,
            mIsProjectSystemActive = false,
            mIsUseDailyCurrencyRate = false,
            mIsIncomingPaymentUseDeposit = false,
            mIsIncomingPaymentAmtUseCOAAmt = false,
            mIsItemCategoryUseCOAAPAR = false,
            mIsIncomingPaymentTransactionValidatedbyClosingJournal = false,
            mIsIncomingPaymentAllowToUploadFile = false
            ;
        private decimal mActivePeriod = 0;
        private int mGrd7 = 0;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmIncomingPayment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Incoming Payment";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();

                TpUpload.PageVisible = mIsIncomingPaymentAllowToUploadFile;
                
                SetGrd();
                SetFormControl(mState.View);
                
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                Sl.SetLueCtCtCode(ref LueCtCtCode);
                SetLueCtCode(ref LueCtCode, "");
                if (mBankAccountFormat == "0" || mBankAccountFormat.Length == 0)
                    Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                else
                    SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                Sl.SetLueAcType(ref LueAcType);
                Sl.SetLueVoucherPaymentType(ref LuePaymentType);
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueCurCode(ref LueCurCode2);
                TpCOA.PageVisible = mIsIncomingPaymentUseCOA;
                TpgProject.PageVisible = mIsProjectSystemActive;
                TpDeposit.PageVisible = TxtDepositAmt.Visible = LblDepositAmt.Visible = mIsIncomingPaymentUseDeposit;
                TcIncomingPayment.SelectedTabPage = TpgProject;
                if (TcIncomingPayment.SelectedTabPage == TpgProject)
                {
                    SetLueProjectSystem1(ref LueProjectDocNo1);
                }
                TcIncomingPayment.SelectedTabPage = TpInvoice;

                if (!mIsCustomerComboBasedOnCategory && !mIsIncomingPaymentUseCustomerCategory)
                {
                    label44.Visible = LueCtCtCode.Visible = false;

                    label3.Location = new System.Drawing.Point(54, 49);
                    LueCtCode.Location = new System.Drawing.Point(116, 47);
                    BtnCtCode.Location = new System.Drawing.Point(365, 46);
                    label24.Location = new System.Drawing.Point(65, 70);
                    TxtPaymentUser.Location = new System.Drawing.Point(116, 68);
                    label20.Location = new System.Drawing.Point(45, 91);
                    LuePaidToBankCode.Location = new System.Drawing.Point(116, 89);
                    label21.Location = new System.Drawing.Point(34, 112);
                    TxtPaidToBankBranch.Location = new System.Drawing.Point(116, 110);
                    label22.Location = new System.Drawing.Point(25, 133);
                    TxtPaidToBankAcName.Location = new System.Drawing.Point(116, 131);
                    label23.Location = new System.Drawing.Point(51, 154);
                    TxtPaidToBankAcNo.Location = new System.Drawing.Point(116, 152);
                }

                if (!mIsIncomingPaymentUsePaymentDate)
                {
                    label46.Visible = DtePaymentDt.Visible = false;
                }


                if (mIsIncomingPaymentUseAdvanceCharge)
                {
                    TcIncomingPayment.SelectedTabPage = TpCOA;
                    SetLueAdvanceChargeCode(ref LueAdvanceChargeCode); 
                    SetLueAdvanceChargeCode(ref LueAdvanceChargeCode2); 
                    SetLueAdvanceChargeCode(ref LueAdvanceChargeCode3);
                }

                if (!mIsIncomingPaymentUseAdvanceCharge)
                {
                    label47.Visible = label48.Visible = label49.Visible = false;
                    LueAdvanceChargeCode.Visible = LueAdvanceChargeCode2.Visible = LueAdvanceChargeCode3.Visible = false;
                    panel13.Visible = false;
                    Grd3.Dock = DockStyle.Fill;
                }


                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Invoice#",
                        "Date",
                        "", 
                        "Type Code",

                        //6-10
                        "Type",
                        "Currency",    
                        "Outstanding"+Environment.NewLine+"Amount",
                        "Amount",
                        "Due Date",
                        
                        //11-13
                        "Description for Voucher Request",
                        "CBD",
                        "DO's Remark"
                        
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        20, 150, 100, 20, 0,
                        
                        //6-10
                        130, 60, 130, 130, 80, 
                        
                        //11-13
                        400, 0, 400
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 12 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 10, 12, 13 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 10 });
            Sm.GrdColButton(Grd1, new int[] { 1, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 5, 12 }, false);
            if (!mIsIncomingPaymentShowDORemark)
                Sm.GrdColInvisible(Grd1, new int[] { 13 }, false);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 4;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Checked By", 
                        
                        //1-3
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] 
                    {
                        150, 
                        100, 100, 400
                    }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });

            #endregion

            #region Grid 3

            Grd3.Cols.Count = 9;
            Grd3.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark",

                        //6-7
                        "Duplicated",
                        "Local Name",
                        "LueName"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400,

                        //6-8
                        0, 150, 0
                    }
                );
            Sm.GrdColCheck(Grd3, new int[] { 6 });
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColReadOnly(Grd3, new int[] { 1, 2, 6 });
            Sm.GrdFormatDec(Grd3, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 2, 8 });
            if (!mIsIncomingPaymentUseAdvanceCharge) Sm.GrdColInvisible(Grd3, new int[] { 7 }, false);
            Grd3.Cols[7].Move(3);

            #endregion

            #region Grid 4

            Grd4.Cols.Count = 7;
            Grd4.FrozenArea.ColCount = 0;

            Sm.GrdHdrWithColWidth(
                    Grd4, new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Bank Code", 
                        "Bank Name", 
                        "Giro#",
                        "Due Date",
                        "Currency",

                        //6
                        "Amount"
                    },
                    new int[] 
                    {
                        //0
                        20,

                        //1-5
                        0, 250, 150, 100, 80,

                        //6
                        130
                    }
                );
            Sm.GrdColButton(Grd4, new int[] { 0 });
            Sm.GrdFormatDate(Grd4, new int[] { 4 });
            Sm.GrdFormatDec(Grd4, new int[] { 6 }, 0);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 1, 2, 3, 4, 5, 6 });
            Sm.GrdColInvisible(Grd4, new int[] { 1 });
            Sm.SetGrdProperty(Grd4, false);

            #endregion

            #region Grid 5

            Grd5.Cols.Count = 6;
            Grd5.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400
                    }
                );
            Sm.GrdColButton(Grd5, new int[] { 0 });
            Sm.GrdColReadOnly(Grd5, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd5, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd5, new int[] { 1 }, false);
            Sm.GrdColReadOnly(true, true, Grd5, new int[] { 1, 2 });

            #endregion

            #region Grid 6

            Grd6.Cols.Count = 2;
            Grd6.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd6,
                new string[] { "Currency", "Deposit Amount", }, new int[] { 120, 180 }
            );
            Sm.GrdColReadOnly(Grd6, new int[] { 0, 1 });
            Sm.GrdFormatDec(Grd6, new int[] { 1 }, 0);

            #endregion

            #region Grid 7

            Grd7.Cols.Count = 8;
            Grd7.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd7,
                    new string[]
                    {
                        //0
                        "DNo",
                    
                        //1-5
                        "U",
                        "File Name",
                        "D",
                        "Upload By",
                        "Date",
                    
                        //6-7
                        "Time",
                        "File Name2"
                    },
                    new int[]
                    {
                        //0
                        0,
                        //1-5
                        20, 250, 20, 150, 80,
                        //6-7
                        80, 250
                    }
                );

            Sm.GrdColInvisible(Grd7, new int[] { 0, 7 }, false);
            Sm.GrdColButton(Grd7, new int[] { 1 }, "1");
            Sm.GrdColButton(Grd7, new int[] { 3 }, "2");
            Sm.GrdFormatDate(Grd7, new int[] { 5 });
            Sm.GrdFormatTime(Grd7, new int[] { 6 });
            Sm.GrdColReadOnly(Grd7, new int[] { 0, 2, 4, 5, 6, 7 });

            #endregion
        }

        private void HideInfoInGrd()
        {
            bool IsHide = !ChkHideInfoInGrd.Checked;
            Sm.GrdColInvisible(Grd1, new int[] {  }, IsHide);
            Sm.GrdColInvisible(Grd3, new int[] { 1 }, IsHide);
            Sm.GrdColInvisible(Grd5, new int[] { 1 }, IsHide);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LueCtCode, TxtPaymentUser, LuePaidToBankCode, 
                        TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo, LueAcType, 
                        LueBankAcCode, LuePaymentType, LueBankCode, TxtGiroNo, DteDueDt, 
                        MeeVoucherRequestSummaryDesc, ChkMeeVoucherRequestSummaryInd, MeeRemark, LueCurCode, 
                        LueCurCode2, TxtRateAmt, MeeCancelReason, LueProjectDocNo1, LueProjectDocNo2, 
                        LueProjectDocNo3, TxtDepositAmt, LueCtCtCode, DtePaymentDt, 
                        LueAdvanceChargeCode, LueAdvanceChargeCode2, LueAdvanceChargeCode3
                    }, true);
                    if (!mIsShowCustomerCategory)
                    {
                        BtnCtCode.Visible = false;
                        LueCtCode.Size = new System.Drawing.Size(274, 20);
                    }
                    else {
                        label44.ForeColor = System.Drawing.Color.Black;                    
                    }
                    BtnCtCode.Enabled = false; 
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 9, 11 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 3, 4, 5, 7 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0 });
                    Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 3, 4, 5 });
                    Grd6.ReadOnly = true;
                    Sm.GrdColReadOnly(true, false, Grd7, new int[] { 1 });
                    Sm.GrdColInvisible(Grd1, new int[] { 8 }, false);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, TxtPaymentUser, LuePaymentType, LueBankAcCode, 
                        MeeVoucherRequestSummaryDesc, ChkMeeVoucherRequestSummaryInd, MeeRemark, LueCurCode, 
                        //LueCurCode2, 
                        LueProjectDocNo1, LueProjectDocNo2, LueProjectDocNo3, TxtRateAmt, TxtDepositAmt, DtePaymentDt,
                        LueAdvanceChargeCode, LueAdvanceChargeCode2, LueAdvanceChargeCode3
                    }, false);
                    
                    if(mIsIncomingPaymentUseCustomerCategory || mIsCustomerComboBasedOnCategory)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCtCtCode }, mIsIncomingPaymentUseCustomerCategory);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 9, 11 });
                    Sm.GrdColInvisible(Grd1, new int[] { 8 }, true);
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5, 7 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 0 });
                    Sm.GrdColReadOnly(false, true, Grd5, new int[] { 0, 3, 4, 5 });
                    Sm.GrdColReadOnly(false, false, Grd7, new int[] { 1 });
                    DteDocDt.Focus();

                    if (mIsShowCustomerCategory)
                    {
                        BtnCtCode.Enabled = true;
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCtCode, LueCtCtCode }, true);

                    }
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, LueCtCtCode, LueCtCode, TxtPaymentUser,
                LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName,  TxtPaidToBankAcNo, LueAcType, 
                LueBankAcCode, LuePaymentType, LueBankCode, TxtGiroNo, DteDueDt, 
                TxtVoucherRequestDocNo, MeeVoucherRequestSummaryDesc, ChkMeeVoucherRequestSummaryInd, TxtVoucherDocNo, MeeRemark, 
                LueCurCode, LueCurCode2, TxtVoucherRequestDocNo2, TxtVoucherDocNo2, MeeCancelReason,
                TxtVoucherRequestDocNo3, TxtVoucherDocNo3,LueProjectDocNo1, LueProjectDocNo2, LueProjectDocNo3, DtePaymentDt,
                LueAdvanceChargeCode, LueAdvanceChargeCode2, LueAdvanceChargeCode3
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtCOAAmt, TxtCOAAmt2, TxtGiroAmt, TxtRateAmt, TxtAmt, 
                TxtAmt2, TxtDepositAmt, TxtTotalAmt
            }, 0);
            ChkCancelInd.Checked = ChkMeeVoucherRequestSummaryInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 9 });
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 12 });
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 3, 4 });
            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 6 });
            Grd5.Rows.Clear();
            Grd5.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 3, 4 });
            Sm.ClearGrd(Grd6, false);
            Grd7.Rows.Clear();
            Grd7.Rows.Count = 1;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIncomingPaymentFind(this, mMInd);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                SetLueCtCode(ref LueCtCode, "");
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                //Sm.SetLue(LueCurCode2, Sm.GetParameter("MainCurCode"));
                Sm.SetLue(LueAcType, "D");
                TxtRateAmt.EditValue = Sm.FormatNum(1, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            var x = new StringBuilder();

            string mDocNo = string.Empty;
            string mDNo = string.Empty;

            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                    {
                        if (mDocNo.Length > 0) mDocNo += ",";
                        mDocNo += Sm.GetGrdStr(Grd1, i, 2);
                    }
                }
            }

            x.AppendLine(" Select Distinct T.EntCode ");
            x.AppendLine(" From ");
            x.AppendLine(" ( ");
            x.AppendLine("     Select Distinct E.EntCode ");
            x.AppendLine("     From TblSalesInvoiceDtl A ");
            x.AppendLine("     Inner Join TblDOCtHdr B On A.DOCtDocNo = B.DocNo ");
            x.AppendLine("         And Find_In_Set(Concat(A.DocNo), @Param) ");
            x.AppendLine("     Inner Join TblWarehouse C On B.WhsCode = C.WhsCode ");
            x.AppendLine("     Inner Join TblCostCenter D On C.CCCode = D.CCCode ");
            x.AppendLine("     Inner Join TblProfitCenter E On D.ProfitCenterCode = E.ProfitCenterCode ");
            x.AppendLine("     Union All ");
            x.AppendLine("     Select Distinct E.EntCode ");
            x.AppendLine("     From TblSalesInvoiceDtl A ");
            x.AppendLine("     Inner Join TblDOCt2Hdr B On A.DOCtDocNo = B.DocNo ");
            x.AppendLine("         And Find_In_Set(Concat(A.DocNo), @Param) ");
            x.AppendLine("     Inner Join TblWarehouse C On B.WhsCode = C.WhsCode ");
            x.AppendLine("     Inner Join TblCostCenter D On C.CCCode = D.CCCode ");
            x.AppendLine("     Inner Join TblProfitCenter E On D.ProfitCenterCode = E.ProfitCenterCode ");
            x.AppendLine("     Union All ");
            x.AppendLine("     Select Distinct H.EntCode ");
            x.AppendLine("     From TblSalesInvoice5Dtl A ");
            x.AppendLine("     Inner Join TblProjectImplementationHdr B On A.ProjectImplementationDocNo = B.DocNo ");
            x.AppendLine("         And Find_In_Set(Concat(A.DocNo), @Param) ");
            x.AppendLine("     Inner Join TblSOContractRevisionHdr C ON B.SOContractDocNo = C.DocNo ");
            x.AppendLine("     Inner Join TblSOContractHdr D On C.SOCDocNo = D.DocNo ");
            x.AppendLine("     Inner Join TblBOQHDr E On D.BOQDocNo = E.DocNo ");
            x.AppendLine("     Inner join TblLOPHdr F On E.LOPDocNo = F.DocNo ");
            x.AppendLine("     Inner Join TblCostCenter G On F.CCCode = G.CCCode ");
            x.AppendLine("     Inner Join TblProfitCenter H On G.ProfitCenterCode = H.ProfitCenterCode ");
            x.AppendLine(" ) T ");
            x.AppendLine(" Limit 1; ");

            mEntCode = Sm.GetValue(x.ToString(), mDocNo);

            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();                    
        }

        #endregion

        #region Grid Method

        private void Grd7_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd7, e.RowIndex, 2).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd7, e.RowIndex, 2), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd7, e.RowIndex, 2);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd7, e.RowIndex, 2, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 1)
                {
                    Sm.GrdRequestEdit(Grd7, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd7.Cells[e.RowIndex, 2].Value = OD.FileName;
                }
            }
        }

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4, 5, 6, 7 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
                if (Sm.GetGrdStr(Grd7, e.RowIndex, 2).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd7, e.RowIndex, 2), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd7, e.RowIndex, 2);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd7, e.RowIndex, 2, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd7_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            int SelectedRow = 0;
            for (int Index = Grd7.SelectedRows.Count - 1; Index >= 0; Index--)
                SelectedRow = Grd7.SelectedRows[Index].Index;
            if (SelectedRow >= mGrd7)
            {
                Sm.GrdRemoveRow(Grd7, e, BtnSave);
                Sm.GrdEnter(Grd7, e);
                Sm.GrdTabInLastCell(Grd7, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (mIsUseMInd && mMInd == "Y")
            {
                if (Sm.StdMsgYN("Question",
                        "Do you want to save data ?" + Environment.NewLine +
                        "It will be generate auto-voucher."
                        ) == DialogResult.No) return;
            }
            else
            {
                if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No) return;
            }

            bool IsCBD = IsSalesInvoiceCBD();
            if (mIsIncomingPaymentUseDeposit) GetDepositSummary(); // refresh deposit summary data

            if (IsInsertedDataNotValid(IsCBD)) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            if(mDocNoFormat == "1")
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "IncomingPayment", "TblIncomingPaymentHdr");
            else
                DocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "IncomingPayment", "TblIncomingPaymentHdr", mEntCode, "1");

            string VoucherRequestDocNo = string.Empty;

            if (mVoucherCodeFormatType == "2")
                if(mDocNoFormat == "1")
                    VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");
                else
                    VoucherRequestDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "1");
            else
                if(mDocNoFormat == "1")
                    VoucherRequestDocNo = GenerateVoucherRequestDocNo("1");
                else
                    VoucherRequestDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "1");

            string VoucherDocNo = string.Empty;
            int JournalSeqNo = 0;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveIncomingPaymentHdr(DocNo, VoucherRequestDocNo));
            
            //update deposit summary
            if (mIsIncomingPaymentUseDeposit)
            {
                if (Decimal.Parse(TxtDepositAmt.Text) != 0m)
                {
                    cml.Add(UpdateDepositSummary("I"));
                    cml.Add(SaveDepositMovement(DocNo, "I"));

                    if (mIsAutoJournalActived)
                    {
                        JournalSeqNo += 1;
                        cml.Add(SaveJournalDeposit(DocNo, JournalSeqNo));
                    }
                }
            }

            cml.Add(SaveIncomingPaymentDtl(DocNo));
            //for (int r = 0; r < Grd1.Rows.Count; r++)
            //    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0) 
            //        cml.Add(SaveIncomingPaymentDtl(DocNo, r));

            cml.Add(SaveIncomingPaymentDtl3(DocNo));
            //for (int r = 0; r < Grd4.Rows.Count; r++)
            //    if (Sm.GetGrdStr(Grd4, r, 1).Length > 0) 
            //        cml.Add(SaveIncomingPaymentDtl3(DocNo, r));

            if (mIsIncomingPaymentAllowToUploadFile) cml.Add(SaveIncomingPaymentFile(DocNo));

            cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo, DocNo, IsCBD));

            if (!ChkMeeVoucherRequestSummaryInd.Checked && Decimal.Parse(TxtDepositAmt.Text) == 0m)
            {
                cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo));
                //for (int r = 0; r < Grd1.Rows.Count; r++)
                //    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0) 
                //        cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo, r));
            }

            if (mIsUseMInd && mMInd == "Y")
            {
                VoucherDocNo = string.Empty;

                if (mVoucherCodeFormatType == "2")
                    if(mDocNoFormat == "1")
                        VoucherDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", "1");
                    else
                        VoucherDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", mEntCode, "1");
                else
                    if(mDocNoFormat == "1")
                        VoucherDocNo = GenerateVoucherDocNo("1");
                    else
                        VoucherDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", mEntCode, "1");

                cml.Add(SaveVoucherHdr(VoucherRequestDocNo, VoucherDocNo));
                cml.Add(SaveVoucherDtl(VoucherRequestDocNo, VoucherDocNo));

                if (mIsAutoJournalActived)
                {
                    JournalSeqNo += 1;
                    cml.Add(SaveJournal(VoucherDocNo, "02", JournalSeqNo, VoucherRequestDocNo));
                }
            }

            if (mIsUseActivePeriod && Sm.GetLue(LueCtCode) == Sm.GetParameter("OnlineCtCode"))
                cml.Add(UpdateSalesInvoiceStatusInd());

            cml.Add(UpdateSalesInvoiceProcessInd(DocNo));
            cml.Add(UpdateSalesReturnInvoiceProcessInd(DocNo));

            if (mIsIncomingPaymentUseCOA)
            {
                if (Grd3.Rows.Count > 1)
                {
                    cml.Add(SaveIncomingPaymentDtl24(DocNo, "2", ref Grd3));
                    //for (int r = 0; r < Grd3.Rows.Count; r++)
                    //    if (Sm.GetGrdStr(Grd3, r, 1).Length > 0) 
                    //        cml.Add(SaveIncomingPaymentDtl24(DocNo, "2", ref Grd3, r));

                    if (!mIsIncomingPaymentAmtUseCOAAmt)
                    {
                        var VoucherRequestDocNo2 = string.Empty;
                        if (mVoucherCodeFormatType == "2")
                            VoucherRequestDocNo2 = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "2");
                        else
                            VoucherRequestDocNo2 = GenerateVoucherRequestDocNo("2");

                        cml.Add(SaveVoucherRequest2(VoucherRequestDocNo2, DocNo));
                        if (mIsUseMInd && mMInd == "Y")
                        {
                            var VoucherDocNo2 = string.Empty;
                            if (mVoucherCodeFormatType == "2")
                                if (mDocNoFormat == "1")
                                    VoucherDocNo2 = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", "2");
                                else
                                    VoucherDocNo2 = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", mEntCode, "2");
                            else
                                if (mDocNoFormat == "1")
                                    VoucherDocNo2 = GenerateVoucherDocNo("2");
                                else
                                    VoucherDocNo2 = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", mEntCode, "2");

                            cml.Add(SaveVoucher2(VoucherRequestDocNo2, VoucherDocNo2));

                            if (mIsAutoJournalActived)
                            {
                                JournalSeqNo += 1;
                                cml.Add(SaveJournal(VoucherDocNo2, "18", JournalSeqNo, VoucherRequestDocNo2));
                            }
                        }
                    }
                }

                if (Grd5.Rows.Count > 1)
                {
                    cml.Add(SaveIncomingPaymentDtl24(DocNo, "4", ref Grd5));
                    //for (int r = 0; r < Grd5.Rows.Count; r++)
                    //    if (Sm.GetGrdStr(Grd5, r, 1).Length > 0) 
                    //        cml.Add(SaveIncomingPaymentDtl24(DocNo, "4", ref Grd5, r));

                    var VoucherRequestDocNo3 = string.Empty;

                    if (mVoucherCodeFormatType == "2")
                        if(mDocNoFormat == "1")
                            VoucherRequestDocNo3 = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "3");
                        else
                            VoucherRequestDocNo3 = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "3");
                    else
                        if(mDocNoFormat == "1")
                            VoucherRequestDocNo3 = GenerateVoucherRequestDocNo("3");
                        else
                            VoucherRequestDocNo3 = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "3");

                    cml.Add(SaveVoucherRequest3(VoucherRequestDocNo3, DocNo));
                    if (mIsUseMInd && mMInd == "Y")
                    {
                        var VoucherDocNo3 = string.Empty;
                        if (mVoucherCodeFormatType == "3")
                            if(mDocNoFormat == "1")
                                VoucherDocNo3 = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", "3");
                            else
                                VoucherDocNo3 = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", mEntCode, "3");
                        else
                            if(mDocNoFormat == "1")
                                VoucherDocNo3 = GenerateVoucherDocNo("3");
                            else
                                VoucherDocNo3 = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", mEntCode, "3");

                        cml.Add(SaveVoucher3(VoucherRequestDocNo3, VoucherDocNo3));
                    }
                }

                if (mIsIncomingPaymentUsePaymentDate)
                {

                }

            }

            Sm.ExecCommands(cml);

            for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd7, Row, 2).Length > 0)
                {
                    if (mIsIncomingPaymentAllowToUploadFile && Sm.GetGrdStr(Grd7, Row, 2).Length > 0 && Sm.GetGrdStr(Grd7, Row, 2) != "openFileDialog1")
                    {
                        if (Sm.GetGrdStr(Grd7, Row, 2) != Sm.GetGrdStr(Grd7, Row, 7))
                        {
                            UploadFile(DocNo, Row, Sm.GetGrdStr(Grd7, Row, 2));
                        }
                    }
                }
            }

            if (mIsIncomingPaymentUseDeposit)
            {
                if (Sm.GetLue(LueCurCode) != Sm.GetParameter("MainCurCode") && Decimal.Parse(TxtDepositAmt.Text) != 0m)
                {
                    var l = new List<Rate>();
                    ProcessDepositSummary2(ref l);
                    l.Clear();
                }
            }
            if (mIsIncomingPaymentOnlyShowDataAfterInsert)
                ShowData(DocNo);
            else
                BtnInsertClick(sender, e);

        }

        private string GenerateVoucherRequestDocNo(string Value)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            bool IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string
                Type = Sm.GetValue(
                    "Select " + (Sm.GetLue(LueAcType) == "C" ? "AutoNoCredit" : "AutoNoDebit") + 
                    " From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' "),
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', ");
            SQL.Append("(   Select IfNull( ");
            SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+"+Value+", Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.Append("       Select Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) As DocNo From TblVoucherRequestHdr ");
            //SQL.Append("    (Select Right(Concat('0000', Convert(DocNo+"+Value+", Char)), 4) As Numb ");
            //SQL.Append("        From ( ");
            //SQL.Append("            Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
            SQL.Append("            Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
            if (Type.Length != 0) 
                SQL.Append("            And Right(DocNo, '" + Type.Length + "') = '" + Type + "' ");
            SQL.Append("            Order By Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) Desc Limit 1 "); //Substring(DocNo,7,"+DocSeqNo+")
            SQL.Append("            ) As temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '"+Value+"'), " + DocSeqNo + ") ");
            //SQL.Append("        ), '000"+Value+"' ");
            SQL.Append("    ) As Number), '/', '" + DocAbbr + "'");
            if (Type.Length != 0) 
                SQL.Append(", '/' , '" + Type + "' ");
            SQL.Append(") As DocNo ");
        
            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateVoucherDocNo(string Value)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            bool IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Voucher'"),
                Type= Sm.GetValue(
                    "Select " + 
                    (Sm.GetLue(LueAcType) == "C" ? "AutoNoCredit" : "AutoNoDebit") + 
                    " From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "';"),
                 DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
            SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+"+Value+", Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.Append("       Select Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) As DocNo From TblVoucherHdr ");
            //SQL.Append("(Select Right(Concat('0000', Convert(DocNo+"+Value+", Char)), 4) As Numb From ( ");
            //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherHdr ");
            SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
            if (Type.Length>0) SQL.Append("And Right(DocNo, '" + Type.Length + "') = '" + Type + "' ");
            SQL.Append("Order By SUBSTRING(DocNo,7,"+DocSeqNo+") Desc Limit 1) As temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '"+Value+"'), " + DocSeqNo + ") ");
            //SQL.Append("), '000"+Value+"' ");
            SQL.Append(") As Number), '/', '" + DocAbbr + "', '/' ");
            if (Type.Length > 0) SQL.Append(", '" + Type + "' ");
            SQL.Append(") As DocNo ");

            return Sm.GetValue(SQL.ToString());
        }

        private bool IsInsertedDataNotValid(bool IsCBD)
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                (mIsIncomingPaymentTransactionValidatedbyClosingJournal && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, false, Sm.GetDte(DteDocDt), GetProfitCenterCode())) ||
                (mIsCustomerComboBasedOnCategory && !mIsShowCustomerCategory && Sm.IsLueEmpty(LueCtCtCode, "Customer Category")) ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueAcType, "Account Type") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LueBankAcCode, "Account") ||
                Sm.IsLueEmpty(LueCurCode, "Invoice's Currency") ||
                Sm.IsLueEmpty(LueCurCode2, "Incoming payment's Currency") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                (mIsIncomingPaymentUsePaymentDate && Sm.IsDteEmpty(DtePaymentDt, "Payment Date"))||
                IsPaymentTypeNotValid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsCurrencyNotValid() ||
                IsBankAccountCurrencyNotValid() ||
                IsOutstandingAmtNotValid() ||
                IsJournalAmtNotBalanced() ||
                IsGiroNoInValid("N") ||
                IsCBDSalesInvoiceInvalid(IsCBD) ||
                (mIsIncomingPaymentUseDeposit && IsDepositNotExists()) ||
                (mIsIncomingPaymentUseDeposit && IsDepositAmtInvalid()) ||
                IsItemCategoryInvalid() ||
                IsDuplicateCOANotHaveRemark()
                ;
        }

        private bool IsDuplicateCOANotHaveRemark()
        {
            if (!mIsCOACouldBeChosenMoreThanOnce) return false;
            if (Grd3.Rows.Count <= 1) return false;

            GetDuplicateCOAIndicator();

            for (int i = 0; i < Grd3.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdBool(Grd3, i, 6) && Sm.GetGrdStr(Grd3, i, 5).Length == 0)
                {
                    TcIncomingPayment.SelectedTabPage = TpCOA;
                    Sm.StdMsg(mMsgType.Warning, "You need to fill this remark due to account duplication.");
                    Sm.FocusGrd(Grd3, i, 5);
                    return true;
                }
            }

            return false;
        }

        private bool IsItemCategoryInvalid()
        {
            if (mIsItemCategoryUseCOAAPAR && mIsAutoJournalActived)
            {
                string InvDocNo = string.Empty;
                string ItCtCode = string.Empty;
                string[] ItCtCodes = { };

                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (InvDocNo.Length > 0) InvDocNo += ",";
                    InvDocNo += Sm.GetGrdStr(Grd1, i, 2);
                }

                if (InvDocNo.Length > 0)
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select Group_Concat(Distinct IfNull(B.ItCtCode, '')) ");
                    SQL.AppendLine("From TblSalesInvoiceDtl A ");
                    SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
                    SQL.AppendLine("Where Find_In_Set(A.DocNo, @Param); ");

                    ItCtCode = Sm.GetValue(SQL.ToString(), InvDocNo);

                    ItCtCodes = ItCtCode.Split(',');

                    if (ItCtCodes.Length > 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Different item category detected. This will affect the journal process (COA AR).");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsDepositNotExists()
        {
            if (Decimal.Parse(TxtDepositAmt.Text) == 0m) return false;

            bool IsExists = false;

            for (int i = 0; i < Grd6.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd6, i, 0).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd6, i, 0) == Sm.GetLue(LueCurCode))
                    {
                        IsExists = true;
                        break;
                    }
                }
            }

            if (!IsExists)
            {
                Sm.StdMsg(mMsgType.Warning, "Currency code doesn't exists for the deposit data.");
                LueCurCode.Focus();
                return true;
            }

            return false;
        }

        private bool IsDepositAmtInvalid()
        {
            decimal mAmt1 = 0m, mAmt2 = 0m;

            if (Decimal.Parse(TxtDepositAmt.Text) == 0m) return false;

            mAmt1 = Decimal.Parse(TxtDepositAmt.Text);
            for (int i = 0; i < Grd6.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd6, i, 0).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd6, i, 0) == Sm.GetLue(LueCurCode))
                    {
                        mAmt2 = Sm.GetGrdDec(Grd6, i, 1);
                        break;
                    }
                }
            }

            if (mAmt1 > mAmt2)
            {
                Sm.StdMsg(mMsgType.Warning, "Deposit Amount should not be exceed Customer Deposit Amount.");
                TcIncomingPayment.SelectedTabPage = TpDeposit;
                TxtDepositAmt.Focus();
                return true;
            }

            return false;
        }

        private bool IsBankAccountCurrencyNotValid()
        {
            var CurCode1 = Sm.GetValue("Select CurCode From TblBankAccount Where CurCode Is Not Null And BankAcCode=@Param;", Sm.GetLue(LueBankAcCode));
            var CurCode2 = Sm.GetLue(LueCurCode2);

            if (!Sm.CompareStr(CurCode1, CurCode2))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Bank account's currency : " + CurCode1 + Environment.NewLine +
                    "Incoming payment's currency : " + CurCode2 + Environment.NewLine + Environment.NewLine +
                    "Both should have the same currency.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 document.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Invoice# is empty.") ||
                    IsSalesInvoiceAlreadyCancelled(Sm.GetGrdStr(Grd1, Row, 2)) ||
                    (
                    !ChkMeeVoucherRequestSummaryInd.Checked &&
                    Sm.IsGrdValueEmpty(Grd1, Row, 11, false, "Voucher Request's description is empty.")) ||
                    IsSalesReturnInvoiceAlreadyCancelled(Row) ||
                    IsSalesReturnInvoiceAlreadyFulfilled(Row)
                    )
                    return true;
            }
            if (Grd3.Rows.Count > 1)
            {
                for (int r = 0; r < Grd3.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, r, 1, false, "COA's account is empty.")) return true;
                    if (Sm.GetGrdDec(Grd3, r, 3) == 0m && Sm.GetGrdDec(Grd3, r, 4) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "COA's Account List" + Environment.NewLine +
                            "Account# : " + Sm.GetGrdStr(Grd3, r, 1) + Environment.NewLine +
                            "Description : " + Sm.GetGrdStr(Grd3, r, 2) + Environment.NewLine + Environment.NewLine +
                            "Debit and credit value should not be 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd3, r, 3) != 0m && Sm.GetGrdDec(Grd3, r, 4) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "COA's Account List" + Environment.NewLine +
                            "Account# : " + Sm.GetGrdStr(Grd3, r, 1) + Environment.NewLine +
                            "Description : " + Sm.GetGrdStr(Grd3, r, 2) + Environment.NewLine + Environment.NewLine +
                            "You have to fill in either debit or credit value.");
                        return true;
                    }
                }
            }

            if (Grd5.Rows.Count > 1)
            {
                for (int r = 0; r < Grd5.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd5, r, 1, false, "COA's account is empty.")) return true;
                    if (Sm.GetGrdDec(Grd5, r, 3) == 0m && Sm.GetGrdDec(Grd5, r, 4) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Advance Payment Refund" + Environment.NewLine +
                            "Account# : " + Sm.GetGrdStr(Grd5, r, 1) + Environment.NewLine +
                            "Description : " + Sm.GetGrdStr(Grd5, r, 2) + Environment.NewLine + Environment.NewLine +
                            "Debit and credit value should not be 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd5, r, 3) != 0m && Sm.GetGrdDec(Grd5, r, 4) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Advance Payment Refund" + Environment.NewLine +
                            "Account# : " + Sm.GetGrdStr(Grd5, r, 1) + Environment.NewLine +
                            "Description : " + Sm.GetGrdStr(Grd5, r, 2) + Environment.NewLine + Environment.NewLine +
                            "You have to fill in either debit or credit value.");
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsSalesReturnInvoiceAlreadyCancelled(int Row)
        { 
            if (Sm.GetGrdStr(Grd1, Row, 5)!="2") return false;

            string DocNo = Sm.GetGrdStr(Grd1, Row, 2);
            return Sm.IsDataExist(
                "Select DocNo From TblSalesReturnInvoiceHdr " +
                "Where CancelInd='Y' And DocNo=@Param;",
                DocNo,
                "Sales Return Invoice# : " + DocNo + Environment.NewLine +
                "This document already cancelled."
                );
        }

        private bool IsSalesReturnInvoiceAlreadyFulfilled(int Row)
        {
            if (Sm.GetGrdStr(Grd1, Row, 5) != "2") return false;

            string DocNo = Sm.GetGrdStr(Grd1, Row, 2);
            return Sm.IsDataExist(
                "Select DocNo From TblSalesReturnInvoiceHdr " +
                "Where CancelInd='F' And DocNo=@Param;",
                DocNo,
                "Sales Return Invoice# : " + DocNo + Environment.NewLine +
                "This document already fulfilled."
                );
        }

        private bool IsSalesInvoiceAlreadyCancelled(string DocNo)
        {
            return Sm.IsDataExist(
                "Select DocNo From TblSalesInvoiceHdr " +
                "Where CancelInd='Y' And DocNo=@Param " +
                "Union ALL "+
                "Select DocNo From TblSalesInvoice2Hdr " +
                "Where CancelInd='Y' And DocNo=@Param ",
                DocNo, 
                "Sales Invoice# : " + DocNo + Environment.NewLine +
                "This document already cancelled."
                );       
        }

        private bool IsCurrencyNotValid()
        {
            string CurCode = Sm.GetLue(LueCurCode);
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, Row, 7)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Invoice# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Type : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                            "Currency : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine +
                            "Invalid currency.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }
            return false;
        }

        private bool IsOutstandingAmtNotValid()
        {
            ReComputeOutstandingAmt();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0 &&
                    Sm.GetGrdDec(Grd1, Row, 9) > Sm.GetGrdDec(Grd1, Row, 8))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Invoice# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Type : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                        "Outstanding Amount : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                        "Incoming Payment Amount : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + Environment.NewLine +
                        "Incoming payment amount is bigger than outstanding amount."
                        );
                    return true;
                }
            }
            return false;
        }

        private bool IsJournalAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int r = 0; r < Grd3.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 3).Length > 0) Debit += Sm.GetGrdDec(Grd3, r, 3);
                if (Sm.GetGrdStr(Grd3, r, 4).Length > 0) Credit += Sm.GetGrdDec(Grd3, r, 4);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "COA's Account List" + Environment.NewLine +
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }

            Debit = 0m;
            Credit = 0m;

            for (int r = 0; r < Grd5.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd5, r, 3).Length > 0) Debit += Sm.GetGrdDec(Grd5, r, 3);
                if (Sm.GetGrdStr(Grd5, r, 4).Length > 0) Credit += Sm.GetGrdDec(Grd5, r, 4);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Advance Payment Refund" + Environment.NewLine +
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }
           
            return false;
        }

        private bool IsGiroNoInValid(string ActInd)
        {
            if (Grd4.Rows.Count > 0)
            {
                var CurCode = Sm.GetLue(LueCurCode);
                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                    {
                        if (ActInd == "N" && IsGiroCurrencyInValid(CurCode, Row)) return true;
                        if (IsGiroNoInValid(ActInd, Row)) return true;
                    }
                }
            }
            return false;
        }

        private bool IsGiroNoInValid(string ActInd, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ActInd ");
            SQL.AppendLine("From TblGiroSummary ");
            SQL.AppendLine("Where BusinessPartnerCode=@BusinessPartnerCode ");
            SQL.AppendLine("And BusinessPartnerType='2' ");
            SQL.AppendLine("And BankCode=@BankCode ");
            SQL.AppendLine("And GiroNo=@GiroNo ");
            SQL.AppendLine("And ActInd=@ActInd; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@BusinessPartnerCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParam<String>(ref cm, "@GiroNo", Sm.GetGrdStr(Grd4, Row, 3));
            Sm.CmParam<String>(ref cm, "@ActInd", ActInd);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Bank : " + Sm.GetGrdStr(Grd4, Row, 2) + Environment.NewLine +
                    "Giro# : " + Sm.GetGrdStr(Grd4, Row, 3) + Environment.NewLine +
                    "invalid Giro#."
                    );
                Sm.FocusGrd(Grd4, Row, 2);
                return true;
            }

            return false;
        }

        private bool IsGiroCurrencyInValid(string CurCode, int Row)
        {
            if (CurCode.Length != 0 &&
                !Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, Row, 5)))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Bank : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine +
                    "Giro# : " + Sm.GetGrdStr(Grd3, Row, 3) + Environment.NewLine +
                    "Currency : " + Sm.GetGrdStr(Grd3, Row, 5) + Environment.NewLine +
                    "invalid Giro's currency."
                    );
                Sm.FocusGrd(Grd3, Row, 2);
                return true;
            }
            return false;
        }

        private bool IsSalesInvoiceCBD()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdBool(Grd1, r, 12)) return true;
            return false;
        }

        private bool IsCBDSalesInvoiceInvalid(bool IsCBD)
        {
            if (IsCBD)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 5).Length>0)
                    {
                        if (!Sm.GetGrdBool(Grd1, r, 12))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                            "Invoice# : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                            "Type : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                            "Currency : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine + Environment.NewLine +
                            "Invalid CBD invoice.");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveIncomingPaymentHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Incoming Payment - Hdr */ ");
            SQL.AppendLine("Insert Into TblIncomingPaymentHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, CtCode, ");
            SQL.AppendLine("PaymentUser, PaidToBankCode, PaidToBankBranch, PaidToBankAcName, PaidToBankAcNo, ");
            SQL.AppendLine("AcType, PaymentType,  ");
            if (mIsIncomingPaymentUsePaymentDate)
            {
                SQL.AppendLine("PaymentDt, ");
            }
            SQL.AppendLine("BankAcCode, BankCode, GiroNo, DueDt,");
            SQL.AppendLine("VoucherRequestDocNo, VoucherRequestSummaryInd, VoucherRequestSummaryDesc, ");
            SQL.AppendLine("CurCode, Amt, CurCode2, RateAmt, Amt2, DepositAmt, COAAmt, COAAmt2, ");
            SQL.AppendLine("ProjectDocNo1, ProjectDocNo2, ProjectDocNo3, Remark,  ");
            if (mIsIncomingPaymentUseAdvanceCharge)
            {
                SQL.AppendLine("AdvanceChargeCode, AdvanceChargeCode2, AdvanceChargeCode3, ");
            }
            SQL.AppendLine("CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @MInd, @CtCode, ");
            SQL.AppendLine("@PaymentUser, @PaidToBankCode, @PaidToBankBranch, @PaidToBankAcName, @PaidToBankAcNo, ");
            SQL.AppendLine("@AcType, @PaymentType, ");
            if (mIsIncomingPaymentUsePaymentDate)
            {
                SQL.AppendLine("@PaymentDt, ");
            }
            SQL.AppendLine("@BankAcCode, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("@VoucherRequestDocNo, @VoucherRequestSummaryInd, @VoucherRequestSummaryDesc, ");
            SQL.AppendLine("@CurCode, @Amt, @CurCode2, @RateAmt, @Amt2, @DepositAmt, @COAAmt, @COAAmt2, ");
            SQL.AppendLine("@ProjectDocNo1, @ProjectDocNo2, @ProjectDocNo3, @Remark, ");
            if (mIsIncomingPaymentUseAdvanceCharge)
            {
                SQL.AppendLine("@AdvanceChargeCode, @AdvanceChargeCode2, @AdvanceChargeCode3, ");
            }
            SQL.AppendLine("@UserCode, CurrentDateTime());");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MInd", mMInd);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
            if (Sm.GetLue(LuePaidToBankCode).Length == 0)
                Sm.CmParam<String>(ref cm, "@PaidToBankCode", "");
            else
                Sm.CmParam<String>(ref cm, "@PaidToBankCode", Sm.Left(Sm.GetLue(LuePaidToBankCode), Sm.GetLue(LuePaidToBankCode).Length - 3));
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch", TxtPaidToBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName", TxtPaidToBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", TxtPaidToBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));

            if (mIsIncomingPaymentUsePaymentDate)
            {
                Sm.CmParamDt(ref cm, "@PaymentDt", Sm.GetDte(DtePaymentDt));
            }

            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestSummaryInd", ChkMeeVoucherRequestSummaryInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@VoucherRequestSummaryDesc", MeeVoucherRequestSummaryDesc.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@CurCode2", Sm.GetLue(LueCurCode2));
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<Decimal>(ref cm, "@DepositAmt", Decimal.Parse(TxtDepositAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@COAAmt", Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@COAAmt2", Decimal.Parse(TxtCOAAmt2.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@ProjectDocNo1", Sm.GetLue(LueProjectDocNo1));
            Sm.CmParam<String>(ref cm, "@ProjectDocNo2", Sm.GetLue(LueProjectDocNo2));
            Sm.CmParam<String>(ref cm, "@ProjectDocNo3",Sm.GetLue(LueProjectDocNo3));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@AdvanceChargeCode", Sm.GetLue(LueAdvanceChargeCode));
            Sm.CmParam<String>(ref cm, "@AdvanceChargeCode2", Sm.GetLue(LueAdvanceChargeCode2));
            Sm.CmParam<String>(ref cm, "@AdvanceChargeCode3", Sm.GetLue(LueAdvanceChargeCode3));


            return cm;
        }

        private MySqlCommand SaveIncomingPaymentDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Incoming Payment - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblIncomingPaymentDtl(DocNo, DNo, InvoiceDocno, InvoiceType, Amt, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @InvoiceDocno_" + r.ToString() +
                        ", @InvoiceType_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@InvoiceDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<String>(ref cm, "@InvoiceType_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 9));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 11));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveIncomingPaymentDtl(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblIncomingPaymentDtl(DocNo, DNo, InvoiceDocno, InvoiceType, Amt, Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @InvoiceDocno, @InvoiceType, @Amt, @Remark, @CreateBy, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@InvoiceDocNo", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@InvoiceType", Sm.GetGrdStr(Grd1, Row, 5));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 9));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 11));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
        //    return cm;
        //}

        private MySqlCommand SaveIncomingPaymentDtl24(string DocNo, string Tbl, ref iGrid Grd)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Incoming Payment - DtlX */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblIncomingPaymentDtl"+Tbl+"");
                        //SQL.AppendLine(Tbl);
                        if (mIsIncomingPaymentUseAdvanceCharge)
                            SQL.AppendLine("(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt, LocalName) ");
                        else
                            SQL.AppendLine("(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt ");
                    if (mIsIncomingPaymentUseAdvanceCharge)
                        SQL.AppendLine(", @LocalName_" + r.ToString());
                    SQL.AppendLine(") ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd, r, 4));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd, r, 5));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveIncomingPaymentDtl24(string DocNo, string Tbl, ref iGrid Grd, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblIncomingPaymentDtl"+Tbl+
        //            "(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @Remark, @CreateBy, CurrentDateTime());"
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd, Row, 3));
        //    Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveIncomingPaymentDtl3(string DocNo)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Incoming Payment - Dtl3 */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd4.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd4, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblIncomingPaymentDtl3(DocNo, DNo, BusinessPartnerCode, BusinessPartnerType, BankCode, GiroNo, DueDt, CurCode, Amt, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        SQL3.AppendLine("Insert Into TblGiroMovement(DocType, DocNo, BusinessPartnerCode, BusinessPartnerType, BankCode, GiroNo, CreateBy, CreateDt) ");
                        SQL3.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                    {
                        SQL.AppendLine(", ");
                        SQL3.AppendLine(", ");
                    }
                        

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @BusinessPartnerCode, '2', @BankCode_" + r.ToString() +
                        ", @GiroNo_" + r.ToString() +
                        ", @DueDt_" + r.ToString() +
                        ", @CurCode_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    SQL2.AppendLine("Update TblGiroSummary Set ActInd='N', LastUpBy=@UserCode, LastUpDt=@Dt ");
                    SQL2.AppendLine("Where BusinessPartnerCode=@BusinessPartnerCode ");
                    SQL2.AppendLine("And BusinessPartnerType='2' ");
                    SQL2.AppendLine("And BankCode=@BankCode_" + r.ToString());
                    SQL2.AppendLine("And GiroNo=@GiroNo_" + r.ToString() + "; ");

                    SQL3.AppendLine(
                        " ('07', @DocNo, @BusinessPartnerCode, '2', @BankCode_" + r.ToString() +
                        ", @GiroNo_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@BankCode_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 1));
                    Sm.CmParam<String>(ref cm, "@GiroNo_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 3));
                    Sm.CmParamDt(ref cm, "@DueDt_" + r.ToString(), Sm.GetGrdDate(Grd4, r, 4));
                    Sm.CmParam<String>(ref cm, "@CurCode_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 5));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 6));
                }
            }

            if (!IsFirstOrExisted) 
            {
                SQL.AppendLine("; ");
                SQL3.AppendLine("; ");
            }
            
            cm.CommandText = SQL.ToString()+ SQL2.ToString()+ SQL3.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@BusinessPartnerCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveIncomingPaymentDtl3(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblIncomingPaymentDtl3(DocNo, DNo, BusinessPartnerCode, BusinessPartnerType, BankCode, GiroNo, DueDt, CurCode, Amt, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @BusinessPartnerCode, '2', @BankCode, @GiroNo, @DueDt, @CurCode, @Amt, @UserCode, CurrentDateTime()); ");

        //    SQL.AppendLine("Update TblGiroSummary Set ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
        //    SQL.AppendLine("Where BusinessPartnerCode=@BusinessPartnerCode ");
        //    SQL.AppendLine("And BusinessPartnerType='2' ");
        //    SQL.AppendLine("And BankCode=@BankCode ");
        //    SQL.AppendLine("And GiroNo=@GiroNo; ");

        //    SQL.AppendLine("Insert Into TblGiroMovement(DocType, DocNo, BusinessPartnerCode, BusinessPartnerType, BankCode, GiroNo, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values('07', @DocNo, @BusinessPartnerCode, '2', @BankCode, @GiroNo, @UserCode, CurrentDateTime()); ");

        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@BusinessPartnerCode", Sm.GetLue(LueCtCode));
        //    Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetGrdStr(Grd4, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@GiroNo", Sm.GetGrdStr(Grd4, Row, 3));
        //    Sm.CmParamDt(ref cm, "@DueDt", Sm.GetGrdDate(Grd4, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd4, Row, 5));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd4, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveIncomingPaymentFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;
            if (DocNo == "Edit") DocNo = Sm.GetValue("Select DocNo From TblIncomingPaymentHdr Where DocNo = @Param; ", TxtDocNo.Text);

            SQL.AppendLine("/* IncomingPayment - Upload File */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd7.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd7, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblIncomingPaymentFile(DocNo, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd7, r, 2));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string IncomingPaymentDocNo, bool IsCBD)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            SQL.AppendLine("PIC, CurCode, Amt, PaymentUser, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', @MInd, (Select ParValue From TblParameter Where ParCode='IncomingPaymentDeptCode'), @DocType, Null, ");
            SQL.AppendLine("@AcType, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("(Select UserCode From TblUser Where UserCode=@CreateBy Limit 1), @CurCode, @Amt, @PaymentUser, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            if (ChkMeeVoucherRequestSummaryInd.Checked)
            {
                SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@DocNo, '001', @VoucherRequestSummaryDesc, @Amt, Null, @CreateBy, CurrentDateTime()); ");
            }
            else
            {
                if (mIsIncomingPaymentUseCOA && decimal.Parse(TxtCOAAmt.Text)!=0m)
                {
                    SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values (@DocNo, @VoucherRequestDNo, @VoucherRequestDescription, @COAAmt, Null, @CreateBy, CurrentDateTime()); ");
                }
            }

            if (Decimal.Parse(TxtDepositAmt.Text) != 0m)
            {
                SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@DocNo, '001', @Description, @Amt*@RateAmt, Null, @CreateBy, CurrentDateTime()); ");
            }

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @IncomingPaymentDocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='IncomingPayment' ");
            SQL.AppendLine("And (T.StartAmt=0.00 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1.00) ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate B1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0.00)) ");
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='IncomingPayment' ");
            SQL.AppendLine("    And DocNo=@IncomingPaymentDocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblIncomingPaymentHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@IncomingPaymentDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='IncomingPayment' ");
            SQL.AppendLine("    And DocNo=@IncomingPaymentDocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@MInd", mMInd);
            Sm.CmParam<String>(ref cm, "@IncomingPaymentDocNo", IncomingPaymentDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", IsCBD?"22":"02");
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<String>(ref cm, "@VoucherRequestSummaryDesc", MeeVoucherRequestSummaryDesc.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            //untuk proses tab coa
            if (mIsIncomingPaymentUseCOA && decimal.Parse(TxtCOAAmt.Text) != 0m)
            {
                Sm.CmParam<Decimal>(ref cm, "@COAAmt", decimal.Parse(TxtCOAAmt.Text));
                Sm.CmParam<String>(ref cm, "@VoucherRequestDNo", Sm.Right("000" + (Grd1.Rows.Count).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@VoucherRequestDescription", Sm.GetGrdStr(Grd1, 0, 11));
            }

            if (Decimal.Parse(TxtDepositAmt.Text) != 0m)
            {
                Sm.CmParam<String>(ref cm, "@Description", "Incoming Payment with Deposit #" + VoucherRequestDocNo);
                Sm.CmParam<Decimal>(ref cm, "@RateAmt", Decimal.Parse(TxtRateAmt.Text));
            }

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Incoming Payment - VoucherRequestDtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @Description_" + r.ToString() +
                        ", Case When @InvoiceType_" + r.ToString() +
                        "='2' Then -1 Else 1 End*@Amt_" + r.ToString() +
                        "*@RateAmt, Null, @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@Description_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 11));
                    Sm.CmParam<String>(ref cm, "@InvoiceType_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 9));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveVoucherRequestDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values (@DocNo, @DNo, @Description, Case When @InvoiceType='2' Then -1 Else 1 End*@Amt*@RateAmt, Null, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, Row, 11));
        //    Sm.CmParam<String>(ref cm, "@InvoiceType", Sm.GetGrdStr(Grd1, Row, 5));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 9));
        //    Sm.CmParam<Decimal>(ref cm, "@RateAmt", Decimal.Parse(TxtRateAmt.Text));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveVoucherRequest2(string VoucherRequestDocNo, string IncomingPaymentDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            SQL.AppendLine("PIC, CurCode, Amt, PaymentUser, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', @MInd, (Select ParValue From TblParameter Where ParCode='IncomingPaymentDeptCode'), '18', Null, ");
            SQL.AppendLine("Case When Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblIncomingPaymentHdr A, TblIncomingPaymentDtl2 B, TblBankAccount C ");
            SQL.AppendLine("    Where A.DocNo=@IncomingPaymentDocNo ");
            SQL.AppendLine("    And A.DocNo=B.DocNo ");
            SQL.AppendLine("    And A.BankAcCode=C.BankAcCode ");
            SQL.AppendLine("    And C.COAAcNo Is Not Null ");
            SQL.AppendLine("    And B.AcNo=C.COAAcNo ");
            SQL.AppendLine("    And B.DAmt<>0.00 ");
            SQL.AppendLine("    Limit 1 ");
            SQL.AppendLine("    ) Then 'D' ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select 1 ");
            SQL.AppendLine("        From TblIncomingPaymentHdr A, TblIncomingPaymentDtl2 B, TblBankAccount C ");
            SQL.AppendLine("        Where A.DocNo=@IncomingPaymentDocNo ");
            SQL.AppendLine("        And A.DocNo=B.DocNo ");
            SQL.AppendLine("        And A.BankAcCode=C.BankAcCode ");
            SQL.AppendLine("        And C.COAAcNo Is Not Null ");
            SQL.AppendLine("        And B.AcNo=C.COAAcNo ");
            SQL.AppendLine("        And B.CAmt<>0.00 ");
            SQL.AppendLine("        Limit 1 ");
            SQL.AppendLine("        ) Then 'C' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        @AcType ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("End, ");
            SQL.AppendLine("@PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("(Select UserCode From TblUser Where UserCode=@CreateBy Limit 1), @CurCode, @Amt, @PaymentUser, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, '001', @Description, @Amt, Null, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='VoucherRequest' ");
            SQL.AppendLine("And T.DeptCode In ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='IncomingPaymentDeptCode' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate B1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequest' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblIncomingPaymentHdr Set VoucherRequestDocNo2=@DocNo ");
            SQL.AppendLine("Where DocNo=@IncomingPaymentDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@MInd", mMInd);
            Sm.CmParam<String>(ref cm, "@IncomingPaymentDocNo", IncomingPaymentDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, 0, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequest3(string VoucherRequestDocNo, string IncomingPaymentDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            SQL.AppendLine("PIC, CurCode, Amt, PaymentUser, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', @MInd, (Select ParValue From TblParameter Where ParCode='IncomingPaymentDeptCode'), '01', Null, ");
            SQL.AppendLine("@AcType, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("(Select UserCode From TblUser Where UserCode=@CreateBy Limit 1), @CurCode, @Amt, @PaymentUser, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, '001', @Description, @Amt, Null, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='VoucherRequest' ");
            SQL.AppendLine("And T.DeptCode In ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='IncomingPaymentDeptCode' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate B1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequest' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblIncomingPaymentHdr Set VoucherRequestDocNo3=@DocNo ");
            SQL.AppendLine("Where DocNo=@IncomingPaymentDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@MInd", mMInd);
            Sm.CmParam<String>(ref cm, "@IncomingPaymentDocNo", IncomingPaymentDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtCOAAmt2.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, 0, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherHdr(string VoucherRequestDocNo, string VoucherDocNo)
        {
            var SQL = new StringBuilder();

            SQL.Append("Insert Into TblVoucherHdr (DocNo, DocDt, CancelInd, MInd, VoucherRequestDocNo, DocType, ");
            SQL.Append("AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, Amt, Remark, CreateBy, CreateDt) ");
            SQL.Append("Select @VoucherDocNo, DocDt, 'N', @MInd, @VoucherRequestDocNo, DocType, ");
            SQL.Append("AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, Amt, Remark, CreateBy, CreateDt ");
            SQL.Append("From TblVoucherRequestHdr Where DocNo=@VoucherRequestDocNo; ");

            SQL.Append("Update TblVoucherRequestHdr Set ");
            SQL.Append("    VoucherDocNo=@VoucherDocNo ");
            SQL.Append("Where DocNo=@VoucherRequestDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", VoucherDocNo);
            Sm.CmParam<String>(ref cm, "@MInd", mMInd);

            return cm;
        }

        private MySqlCommand SaveVoucherDtl(string VoucherRequestDocNo, string VoucherDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Select @VoucherDocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt " +
                    "From TblVoucherRequestDtl Where DocNo=@VoucherRequestDocNo;"
            };
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", VoucherDocNo);

            return cm;
        }

        private MySqlCommand SaveVoucher2(string VoucherRequestDocNo, string VoucherDocNo)
        {
            var SQL = new StringBuilder();

            SQL.Append("Insert Into TblVoucherHdr (DocNo, DocDt, CancelInd, MInd, VoucherRequestDocNo, DocType, ");
            SQL.Append("AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, Amt, Remark, CreateBy, CreateDt) ");
            SQL.Append("Select @VoucherDocNo, DocDt, 'N', @MInd, @VoucherRequestDocNo, DocType, ");
            SQL.Append("AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, Amt, Remark, CreateBy, CreateDt ");
            SQL.Append("From TblVoucherRequestHdr Where DocNo=@VoucherRequestDocNo; ");

            SQL.Append("Insert Into TblVoucherDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.Append("Select @VoucherDocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt ");
            SQL.Append("From TblVoucherRequestDtl Where DocNo=@VoucherRequestDocNo;");

            SQL.Append("Update TblVoucherRequestHdr Set ");
            SQL.Append("    VoucherDocNo=@VoucherDocNo ");
            SQL.Append("Where DocNo=@VoucherRequestDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", VoucherDocNo);
            Sm.CmParam<String>(ref cm, "@MInd", mMInd);

            return cm;
        }

        private MySqlCommand SaveVoucher3(string VoucherRequestDocNo, string VoucherDocNo)
        {
            var SQL = new StringBuilder();

            SQL.Append("Insert Into TblVoucherHdr (DocNo, DocDt, CancelInd, MInd, VoucherRequestDocNo, DocType, ");
            SQL.Append("AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, Amt, Remark, CreateBy, CreateDt) ");
            SQL.Append("Select @VoucherDocNo, DocDt, 'N', @MInd, @VoucherRequestDocNo, DocType, ");
            SQL.Append("AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, Amt, Remark, CreateBy, CreateDt ");
            SQL.Append("From TblVoucherRequestHdr Where DocNo=@VoucherRequestDocNo; ");

            SQL.Append("Insert Into TblVoucherDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.Append("Select @VoucherDocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt ");
            SQL.Append("From TblVoucherRequestDtl Where DocNo=@VoucherRequestDocNo;");

            SQL.Append("Update TblVoucherRequestHdr Set ");
            SQL.Append("    VoucherDocNo=@VoucherDocNo ");
            SQL.Append("Where DocNo=@VoucherRequestDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", VoucherDocNo);
            Sm.CmParam<String>(ref cm, "@MInd", mMInd);

            return cm;
        }

        private MySqlCommand UpdateSalesInvoiceProcessInd(string DocNo)
        {
            var SQL = new StringBuilder();
            //update sales invoice
            SQL.AppendLine("Update TblSalesInvoiceHdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.InvoiceDocNo As DocNo, Sum(T3.Amt) As Amt2 ");
            SQL.AppendLine("    From TblIncomingPaymentHdr T1 ");
            //SQL.AppendLine("    Inner Join TblIncomingPaymentDtl T2 ");
            //SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("        And T2.InvoiceType='1' ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl T3 ");
            SQL.AppendLine("        On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("        And T3.InvoiceType='1' ");
            SQL.AppendLine("        And T3.DocNo=@DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("    Group By T3.InvoiceDocNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Set A.ProcessInd = ");
            SQL.AppendLine("    Case When IfNull(B.Amt2, 0) = 0 Then ");
            SQL.AppendLine("        If(A.Amt = 0, 'F', 'O') ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If(IfNull(B.Amt2, 0)>=A.Amt, 'F', 'P') ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select InvoiceDocNo From TblIncomingPaymentDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And InvoiceType='1' ");
            SQL.AppendLine("    ); ");

            //update salesinvoice 2
            SQL.AppendLine("Update TblSalesInvoice2Hdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.InvoiceDocNo As DocNo, Sum(T3.Amt) As Amt2 ");
            SQL.AppendLine("    From TblIncomingPaymentHdr T1 ");
            //SQL.AppendLine("    Inner Join TblIncomingPaymentDtl T2 ");
            //SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("        And T2.InvoiceType='3' ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl T3 ");
            SQL.AppendLine("        On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("        And T3.InvoiceType='3' ");
            SQL.AppendLine("        And T3.DocNo=@DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("    Group By T3.InvoiceDocNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Set A.ProcessInd = ");
            SQL.AppendLine("    Case When IfNull(B.Amt2, 0) = 0 Then ");
            SQL.AppendLine("        If(A.Amt = 0, 'F', 'O') ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If(IfNull(B.Amt2, 0)>=A.Amt, 'F', 'P') ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select InvoiceDocNo From TblIncomingPaymentDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And InvoiceType='3' ");
            SQL.AppendLine("    ); ");

            //Update sales invoice 5
            SQL.AppendLine("Update TblSalesInvoice5Hdr A  ");
            SQL.AppendLine("Left Join (  ");
            SQL.AppendLine("    Select T3.InvoiceDocNo As DocNo, Sum(T3.Amt) As Amt2  ");
            SQL.AppendLine("    From TblIncomingPaymentHdr T1  ");
            //SQL.AppendLine("    Inner Join TblIncomingPaymentDtl T2  ");
            //SQL.AppendLine("        On T1.DocNo=T2.DocNo  ");
            //SQL.AppendLine("        And T2.InvoiceType='5'  ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl T3  ");
            SQL.AppendLine("        On T1.DocNo=T3.DocNo  ");
            SQL.AppendLine("        And T3.InvoiceType='5'  ");
            SQL.AppendLine("        And T3.DocNo=@DocNo  ");
            SQL.AppendLine("    Where T1.CancelInd='N'  ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O')<>'C'  ");
            SQL.AppendLine("    Group By T3.InvoiceDocNo  ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("Set A.ProcessInd =  ");
            SQL.AppendLine("    Case When IfNull(B.Amt2, 0) = 0 Then  ");
            SQL.AppendLine("        If(A.Amt = 0, 'F', 'O')  ");
            SQL.AppendLine("    Else  ");
            SQL.AppendLine("        If(IfNull(B.Amt2, 0)>=A.Amt, 'F', 'P')  ");
            SQL.AppendLine("    End  ");
            SQL.AppendLine("Where A.DocNo In (  ");
            SQL.AppendLine("    Select InvoiceDocNo From TblIncomingPaymentDtl  ");
            SQL.AppendLine("    Where DocNo=@DocNo And InvoiceType='5'  ");
            SQL.AppendLine("    );  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private MySqlCommand UpdateSalesReturnInvoiceProcessInd(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesReturnInvoiceHdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.InvoiceDocNo As DocNo, Sum(T3.Amt) As Amt2 ");
            SQL.AppendLine("    From TblIncomingPaymentHdr T1 ");
            //SQL.AppendLine("    Inner Join TblIncomingPaymentDtl T2 ");
            //SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("        And T2.InvoiceType='2' ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl T3 ");
            SQL.AppendLine("        On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("        And T3.InvoiceType='2' ");
            SQL.AppendLine("        And T3.DocNo=@DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("    Group By T3.InvoiceDocNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Set A.IncomingPaymentInd = ");
            SQL.AppendLine("    Case When IfNull(B.Amt2, 0) = 0 Then ");
            SQL.AppendLine("        If(A.TotalAmt = 0, 'F', 'O') ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If(IfNull(B.Amt2, 0)>=A.TotalAmt, 'F', 'P') ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select InvoiceDocNo From TblIncomingPaymentDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And InvoiceType='2' ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private MySqlCommand UpdateSalesInvoiceStatusInd()
        {
            var SQL = new StringBuilder();

            //sales invoice
            SQL.AppendLine("Update TblSalesInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblSOhdr B On A.SoDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join Msiweb.order_summaries C On B.localDocNo = C.Id ");
            SQL.AppendLine("SET A.cancelInd = 'Y', ");
            SQL.AppendLine("B.cancelInd = 'Y' ");
            SQL.AppendLine("Where A.SoDocNo is not null And A.CtCode = @CtCode ");
            SQL.AppendLine("And (Select Right(Replace(Concat(curdate(), ' ', Curtime()), ':', ''), 6)) >= @ActivePeriod ; ");
          

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetParameter("OnlineCtCode"));
            Sm.CmParam<String>(ref cm, "@Now", Sm.GetValue("Select Concat(curdate(), ' ', Curtime()) "));
            Sm.CmParam<decimal>(ref cm, "@ActivePeriod", mActivePeriod);
                
            return cm;
        }

        private MySqlCommand UpdateDepositSummary(string Stateind)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblCustomerDepositSummary Set ");
            if (Stateind == "I")
                SQL.AppendLine("Amt = Amt - @DepositAmt ");
            else
                SQL.AppendLine("Amt = Amt + @DepositAmt ");
            SQL.AppendLine("Where CtCode = @CtCode ");
            SQL.AppendLine("And CurCode = @CurCode; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<Decimal>(ref cm, "@DepositAmt", Decimal.Parse(TxtDepositAmt.Text));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));

            return cm;
        }

        private MySqlCommand SaveDepositMovement(string DocNo, string Stateind)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblCustomerDepositMovement(DocNo, DocType, DocDt, CtCode, EntCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocType, @DocDt, @CtCode, @EntCode, @CurCode, @DepositAmt, @CreateBy, CurrentDateTime()); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", (Stateind == "I") ? mDepositMovementDocTypeCreate : mDepositMovementDocTypeCancel);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<Decimal>(ref cm, "@DepositAmt", (Stateind == "I") ? Decimal.Parse(TxtDepositAmt.Text) * -1 : Decimal.Parse(TxtDepositAmt.Text));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateDepositSummary2()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblCustomerDepositSummary2 Set ");
            SQL.AppendLine("Amt = Amt + @DepositAmt ");
            SQL.AppendLine("Where CtCode = @CtCode ");
            SQL.AppendLine("And CurCode = @CurCode ");
            SQL.AppendLine("And ExcRate = @ExcRate; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<Decimal>(ref cm, "@DepositAmt", Decimal.Parse(TxtDepositAmt.Text));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", Decimal.Parse(TxtRateAmt.Text));

            return cm;
        }

        #region Journal

        private MySqlCommand SaveJournalDeposit(string DocNo, int JournalSeqNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIncomingPaymentHdr Set ");
            SQL.AppendLine("JournalDepositDocNo = @JournalDocNo ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And DepositAmt != 0.00; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, @DocDt, Concat('Incoming Payment - Deposit : ', @DocNo), @MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode = @MenuCode), @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3), ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAmt, @EntCode, @Remark, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.AcNo, Sum(T1.DAmt) DAmt, Sum(T1.CAmt) CAmt From ( ");
            SQL.AppendLine("        Select Concat(ParValue, @CtCode) As AcNo, 0.00 DAmt, @DepositAmt CAmt ");
            SQL.AppendLine("        From TblParameter ");
            SQL.AppendLine("        Where Parcode = 'CustomerAcNoAR' ");
            SQL.AppendLine("        And ParValue Is Not Null ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Concat(ParValue, @CtCode) As AcNo, @DepositAmt DAmt, 0.00 CAmt ");
            SQL.AppendLine("        From TblParameter ");
            SQL.AppendLine("        Where Parcode = 'CustomerAcNoDownPayment' ");
            SQL.AppendLine("        And ParValue Is Not Null ");
            SQL.AppendLine("   ) T1 ");
            SQL.AppendLine("   Group By T1.AcNo ");
            SQL.AppendLine(") B On 0 = 0 ");
            SQL.AppendLine("Where A.DocNo = @JournalDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            if (mDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), JournalSeqNo));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, JournalSeqNo.ToString()));
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<Decimal>(ref cm, "@DepositAmt", Decimal.Parse(TxtDepositAmt.Text));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournalDeposit2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIncomingPaymentHdr Set ");
            SQL.AppendLine("JournalDepositDocNo2 = @JournalDocNo ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And JournalDepositDocNo2 Is Null ");
            SQL.AppendLine("And DepositAmt != 0.00 ");
            SQL.AppendLine("And CancelInd = 'N'; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, @DocDt, Concat('Cancelling Incoming Payment - Deposit : ', @DocNo), @MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode = @MenuCode), @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, A.DNo, A.AcNo, A.CAmt, A.DAmt, A.EntCode, A.Remark, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr B On A.DocNo = B.JournalDepositDocNo ");
            SQL.AppendLine("Where B.DocNo = @DocNo ");
            SQL.AppendLine("And Exists (Select 1 From TblJournalHdr Where DocNo = @JournalDocNo); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            if (mDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1"));
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, string DocType, int JournalSeqNo, string VoucherRequestDocNo)
        {
            string DocTypeDesc = string.Empty;
            switch (DocType)
            {
                case "02":
                    DocTypeDesc = "Incoming Payment";
                    break;
                case "18":
                    DocTypeDesc = "Incoming Payment Additional Amount";
                    break;
            }

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);

            if(mDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), JournalSeqNo));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, JournalSeqNo.ToString()));

            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@DocType", DocTypeDesc);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode2));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);

            SQL.AppendLine("Update TblVoucherHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt ");
            if (mVoucherIncomingPaymentCCJournalFormat == "2")
                SQL.AppendLine(",CCCode");
            SQL.AppendLine(")");


            SQL.AppendLine("Select A.JournalDocNo, A.DocDt, Concat('Voucher (', IfNull(@DocType, 'None'), ') : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            if (mVoucherIncomingPaymentCCJournalFormat == "2")
                SQL.AppendLine(", F.CCCode");
            SQL.AppendLine("From TblVoucherHdr A ");

            SQL.AppendLine("LEFT JOIN tblincomingpaymenthdr B ON A.VoucherRequestDocNo = B.VoucherRequestDocNo ");
            SQL.AppendLine("LEFT JOIN tblincomingpaymentdtl C ON B.DocNo = C.DocNo ");
            SQL.AppendLine("LEFT JOIN tblsalesinvoicedtl D ON C.InvoiceDocNo = D.DocNo ");
            SQL.AppendLine("LEFT JOIN tbldocthdr E ON D.DOCtDocNo = E.DocNo ");
            SQL.AppendLine("LEFT JOIN tblwarehouse F ON E.WhsCode = F.WhsCode ");
            SQL.AppendLine(" Where A.DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, ");

            if (DocType == "02")
            {
                SQL.AppendLine("B.DAmt, ");
                SQL.AppendLine("B.CAmt, ");
            }
            else
            {
                if (Sm.CompareStr(Sm.GetLue(LueCurCode2), mMainCurCode))
                {
                    SQL.AppendLine("B.DAmt, ");
                    SQL.AppendLine("B.CAmt, ");
                }
                else
                {
                    SQL.AppendLine("B.DAmt*( ");
                    SQL.AppendLine("IfNull(( ");
                    SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("), 0) ");
                    SQL.AppendLine(") As DAmt, ");
                    SQL.AppendLine("B.CAMt*( ");
                    SQL.AppendLine("IfNull(( ");
                    SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("), 0) ");
                    SQL.AppendLine(") As CAmt, ");
                }
            }
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            SQL.AppendLine(GetJournalSQL(ref cm, DocType));

            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            if (DocType == "02")
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select DAmt, CAmt From (");
                SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
                SQL.AppendLine("        From TblJournalDtl Where DocNo=@JournalDocNo ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine(") B On 0=0 ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0 End, ");
                SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0 End ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                SQL.AppendLine("    And ParValue Is Not Null ");
                SQL.AppendLine("    );");

                SQL.AppendLine("Delete From TblJournalDtl ");
                SQL.AppendLine("Where DocNo=@JournalDocNo ");
                SQL.AppendLine("And (DAmt=0 And CAmt=0) ");
                SQL.AppendLine("And AcNo In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                SQL.AppendLine("    And ParValue Is Not Null ");
                SQL.AppendLine("    );");
            }

            cm.CommandText = SQL.ToString();

            return cm;
        }

        private string GetJournalSQL(ref MySqlCommand cm, string DocType)
        {
            var SQL = string.Empty;
            switch (DocType)
            {
                case "02":
                    SQL = GetJournalSQL02(ref cm);
                    break;
                case "18":
                    SQL = GetJournalSQL18(ref cm);
                    break;
            }
            return SQL;
        }

        private string GetJournalSQL02(ref MySqlCommand cm)
        {
            //02 : Incoming Payment
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    Case When C.CurCode<>@MainCurCode Then ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=B.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) ");
            SQL.AppendLine("    Else B.RateAmt End ");
            SQL.AppendLine("End ");
            SQL.AppendLine("*IfNull(A.Amt, 0.00) As DAmt, ");
            SQL.AppendLine("0.00 As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblBankAccount C On C.BankAcCode=@BankAcCode And C.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select T.AcNo, ");
            SQL.AppendLine("0.00 As DAmt, ");
            SQL.AppendLine("Sum(Amt) As CAmt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Concat(E.ParValue, B.CtCode) As AcNo, ");
            SQL.AppendLine("    Case When B.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=if(F.invoiceType = '1', F.Amt, H.Amt) And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0.00) End ");
            SQL.AppendLine("    *if(F.invoiceType = '1', F.Amt, H.Amt) As Amt ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("    Inner Join TblCustomerCategory D On C.CtCtCode=D.CtCtCode ");
            SQL.AppendLine("    Inner Join TblParameter E On E.ParCode='CustomerAcNoAR' And E.ParValue Is Not Null ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl F On B.DocNo=F.DocNo And F.InvoiceType='1' ");
            SQL.AppendLine("    Left Join TblSalesInvoiceHdr G On F.InvoiceDocNo=G.DocNo ");
            SQL.AppendLine("    Left Join TblSalesInvoice2Hdr H On F.InvoiceDocNo=H.DocNo ");
            SQL.AppendLine("    Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Concat(E.ParValue, B.CtCode) As AcNo, ");
            SQL.AppendLine("    Case When B.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=if(F.invoiceType = '1', F.Amt, H.Amt) And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0.00) End ");
            SQL.AppendLine("    *if(F.invoiceType = '1', F.Amt, H.Amt) As Amt ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("    Inner Join TblCustomerCategory D On C.CtCtCode=D.CtCtCode ");
            SQL.AppendLine("    Inner Join TblParameter E On E.ParCode='CustomerAcNoAR' And E.ParValue Is Not Null ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl F On B.DocNo=F.DocNo And F.InvoiceType='2' ");
            SQL.AppendLine("    Left Join TblSalesInvoiceHdr G On F.InvoiceDocNo=G.DocNo ");
            SQL.AppendLine("    Left Join TblSalesInvoice2Hdr H On F.InvoiceDocNo=H.DocNo ");
            SQL.AppendLine("    Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine(") T Group By T.AcNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select ParValue As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblParameter Where ParCode='AcNoForForeignCurrencyExchangeGains' And ParValue Is Not Null ");

            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

            return SQL.ToString();
        }

        private string GetJournalSQL18(ref MySqlCommand cm)
        {
            //Incoming Payment Additional Amount

            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.AcNo, C.DAmt, C.CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr B On B.VoucherRequestDocNo2 Is Not Null And A.VoucherRequestDocNo=B.VoucherRequestDocNo2 ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl2 C On B.DocNo=C.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            return SQL.ToString();
        }


        #endregion

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No||IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelIncomingPaymentHdr());
            cml.Add(UpdateSalesInvoiceProcessInd(TxtDocNo.Text));
            cml.Add(UpdateSalesReturnInvoiceProcessInd(TxtDocNo.Text));
            if (mIsIncomingPaymentUseDeposit)
            {
                if (mIsAutoJournalActived) cml.Add(SaveJournalDeposit2(TxtDocNo.Text));
                cml.Add(UpdateDepositSummary("E"));
                cml.Add(SaveDepositMovement(TxtDocNo.Text, "E"));
                cml.Add(UpdateDepositSummary2());
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                (mIsIncomingPaymentTransactionValidatedbyClosingJournal && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode())) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") ||
                IsDataProcessedAlready() ||
                IsGiroNoInValid("Y");
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblIncomingPaymentHdr " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblVoucherRequestHdr A, TblVoucherHdr B ");
            SQL.AppendLine("Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already processed into voucher.");
                return true;
            }

            return false;
        }

        private MySqlCommand CancelIncomingPaymentHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIncomingPaymentHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status<>'C'; ");

            if (Grd4.Rows.Count > 0)
            {
                SQL.AppendLine("Insert Into TblGiroMovement ");
                SQL.AppendLine("(DocType, DocNo, BusinessPartnerCode, BusinessPartnerType, BankCode, GiroNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select '08', DocNo, BusinessPartnerCode, BusinessPartnerType, BankCode, GiroNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblGiroMovement ");
                SQL.AppendLine("Where DocType='07' And DocNo=@DocNo;");

                SQL.AppendLine("Update TblGiroSummary T Set ");
                SQL.AppendLine("    T.ActInd='Y', T.LastUpBy=@UserCode, T.LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where T.ActInd='N' ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select GiroNo ");
                SQL.AppendLine("    From TblGiroMovement ");
                SQL.AppendLine("    Where DocNo=@DocNo ");
                SQL.AppendLine("    And DocType='08' ");
                SQL.AppendLine("    And BusinessPartnerCode=T.BusinessPartnerCode ");
                SQL.AppendLine("    And BusinessPartnerType=T.BusinessPartnerType ");
                SQL.AppendLine("    And BankCode=T.BankCode ");
                SQL.AppendLine("    And GiroNo=T.GiroNo ");
                SQL.AppendLine(");");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowIncomingPaymentHdr(DocNo);
                ShowIncomingPaymentDtl(DocNo);
                ShowIncomingPaymentDtl24(DocNo, "2", ref Grd3);
                ShowIncomingPaymentDtl24(DocNo, "4", ref Grd5);
                ShowIncomingPaymentDtl3(DocNo);
                ShowIncomingPaymentDtl6(DocNo);
                if (mIsIncomingPaymentAllowToUploadFile) ShowIncomingPaymentFile(DocNo);
                ShowDocApproval(DocNo);
                string testshowprofitcenter = GetProfitCenterCode();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowIncomingPaymentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.*, B.VoucherDocNo, C.VoucherDocNo As VoucherDocNo2, D.VoucherDocNo As VoucherDocNo3, ");
            SQL.AppendLine("(Case A.Status ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("Else '' End) As StatusDesc, A.CancelReason, A.ProjectDocNo1, A.ProjectDocNo2, A.ProjectDocNo3, ");
            SQL.AppendLine("E.CtCtCode, F.TotalAmt, (F.AmtDtl+A.COAAmt)* A.RateAmt AS TotalCOAAmt  ");

            if (mIsIncomingPaymentUsePaymentDate)
            {
                SQL.AppendLine(", A.PaymentDt ");
            }
            else
            {
                SQL.AppendLine(", Null As PaymentDt ");
            }

            if (mIsIncomingPaymentUseAdvanceCharge)
            {
                SQL.AppendLine(", A.AdvanceChargeCode, A.AdvanceChargeCode2, A.AdvanceChargeCode3 ");
            }
            else
            {
                SQL.AppendLine(", Null as AdvanceChargeCode, Null as AdvanceChargeCode2, Null as AdvanceChargeCode3 ");
            }

            SQL.AppendLine("From TblIncomingPaymentHdr A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr C On A.VoucherRequestDocNo2=C.DocNo ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr D On A.VoucherRequestDocNo3=D.DocNo ");
            SQL.AppendLine("Left Join TblCustomer E On A.CtCode = E.CtCode ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("SELECT T1.Docno, SUM(T1.Amt) AS AmtDtl, T2.Amt AS TotalAmt ");
            SQL.AppendLine("FROM tblincomingpaymentdtl T1 ");
            // SQL.AppendLine("INNER JOIN tblsalesinvoicehdr T2 ON T1.InvoiceDocNo=T2.DocNo ");
            SQL.AppendLine("INNER JOIN (select docno,amt from tblsalesinvoicehdr");
            SQL.AppendLine("UNION All ");
            SQL.AppendLine("select DocNo, TotalAmt From TblSalesReturnInvoiceHdr ");
            SQL.AppendLine("UNION All ");
            SQL.AppendLine("select docno,amt from tblsalesinvoice5hdr) T2 ON T1.InvoiceDocNo=T2.DocNo ");

            SQL.AppendLine("WHERE T1.DocNo=@DocNo ");
            SQL.AppendLine("Group BY  T1.DocNo, T2.Amt  ");
            SQL.AppendLine(")F ON A.DocNo = F.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelInd", "StatusDesc", "CtCode", "PaymentUser",
 
                        //6-10
                        "PaidToBankCode", "PaidToBankBranch", "PaidToBankAcName", "PaidToBankAcNo", "AcType",   
                        
                        //11-15
                        "PaymentType", "BankAcCode", "BankCode", "GiroNo", "DueDt", 

                        //16-20
                        "VoucherRequestDocNo", "VoucherRequestSummaryDesc", "VoucherRequestSummaryInd", "VoucherDocNo", "Remark", 
                        
                        //21-25
                        "CurCode", "Amt", "CurCode2", "RateAmt", "Amt2",  

                        //26-30
                        "VoucherRequestDocNo2", "VoucherDocNo2", "COAAmt", "VoucherRequestDocNo3", "VoucherDocNo3", 
                        
                        //31-35
                        "COAAmt2", "CancelReason", "ProjectDocNo1", "ProjectDocNo2", "ProjectDocNo3", 
                        
                        //36-40
                        "DepositAmt", "CtCtCode", "TotalAmt", "TotalCOAAmt", "PaymentDt",

                        //41-43
                        "AdvanceChargeCode", "AdvanceChargeCode2", "AdvanceChargeCode3"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[3]);
                        Sm.SetLue(LueCtCtCode, Sm.DrStr(dr, c[37]));
                        SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[4]));
                        TxtPaymentUser.EditValue = Sm.DrStr(dr, c[5]);
                        SetLuePaidToBankCode(ref LuePaidToBankCode, "", "1");
                        Sm.SetLue(LuePaidToBankCode, Sm.DrStr(dr, c[6]));
                        TxtPaidToBankBranch.EditValue = Sm.DrStr(dr, c[7]);
                        TxtPaidToBankAcName.EditValue = Sm.DrStr(dr, c[8]);
                        TxtPaidToBankAcNo.EditValue = Sm.DrStr(dr, c[9]);
                        Sm.SetLue(LueAcType, Sm.DrStr(dr, c[10]));
                        Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[11]));
                        Sl.SetLueBankAcCode(ref LueBankAcCode, Sm.DrStr(dr, c[12]));
                        Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[13]));
                        TxtGiroNo.EditValue = Sm.DrStr(dr, c[14]);
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[15]));
                        TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[16]);
                        MeeVoucherRequestSummaryDesc.EditValue = Sm.DrStr(dr, c[17]);
                        ChkMeeVoucherRequestSummaryInd.Checked = Sm.DrStr(dr, c[18]) == "Y";
                        TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[19]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[20]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[21]));
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[22]), 0);
                        Sm.SetLue(LueCurCode2, Sm.DrStr(dr, c[23]));
                        TxtRateAmt.EditValue = FormatNum(Sm.DrDec(dr, c[24]));
                        TxtAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[25]), 0);
                        TxtVoucherRequestDocNo2.EditValue = Sm.DrStr(dr, c[26]);
                        TxtVoucherDocNo2.EditValue = Sm.DrStr(dr, c[27]);
                        TxtCOAAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[28]), 0);
                        TxtVoucherRequestDocNo3.EditValue = Sm.DrStr(dr, c[29]);
                        TxtVoucherDocNo3.EditValue = Sm.DrStr(dr, c[30]);
                        TxtCOAAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[31]), 0);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[32]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        if (mIsProjectSystemActive)
                        {
                            Sm.SetLue(LueProjectDocNo1, Sm.DrStr(dr, c[33]));
                            SetLueProjectSystem2(ref LueProjectDocNo2, Sm.DrStr(dr, c[33]));
                            Sm.SetLue(LueProjectDocNo2, Sm.DrStr(dr, c[34]));
                            SetLueProjectSystem3(ref LueProjectDocNo3, Sm.DrStr(dr, c[34]));
                            Sm.SetLue(LueProjectDocNo3, Sm.DrStr(dr, c[35]));
                        }
                        TxtDepositAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[36]), 0);
                        TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[38]), 0);

                        if (mIsIncomingPaymentAmtUseCOAAmt) 
                        {
                            TxtCOAAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[39]), 0);
                        }

                        if (mIsIncomingPaymentUsePaymentDate)
                        {
                            Sm.SetDte(DtePaymentDt, Sm.DrStr(dr, c[40]));
                        }

                        if (mIsIncomingPaymentUseAdvanceCharge)
                        {
                            Sm.SetLue(LueAdvanceChargeCode, Sm.DrStr(dr, c[41]));
                            Sm.SetLue(LueAdvanceChargeCode2, Sm.DrStr(dr, c[42]));
                            Sm.SetLue(LueAdvanceChargeCode3, Sm.DrStr(dr, c[43]));
                        }

                    }, true
                );
        }

        private void ShowIncomingPaymentDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.InvoiceDocNo, ");
            SQL.AppendLine("A.InvoiceType, 'Sales Invoice' As InvoiceTypeDesc, ");
            SQL.AppendLine("A.Amt, A.Remark, ");
            SQL.AppendLine("B.DocDt, B.CurCode, B.DueDt, C.DOCtRemark ");
            SQL.AppendLine("From TblIncomingPaymentDtl A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr B On A.InvoiceDocNo=B.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct T3.Remark Order By T2.DNo Separator ' ') As DOCtRemark ");
            SQL.AppendLine("    From TblSalesInvoiceHdr T1 ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCtHdr T3 On T2.DOCtDocNo=T3.DocNo And T3.Remark Is Not Null ");
            SQL.AppendLine("    Where T1.DocNo In ( ");
            SQL.AppendLine("        Select Distinct X2.DocNo ");
            SQL.AppendLine("        From TblIncomingPaymentDtl X1 ");
            SQL.AppendLine("        Inner Join TblSalesInvoiceHdr X2 On X1.InvoiceDocNo=X2.DocNo ");
            SQL.AppendLine("        Where X1.InvoiceType='1' ");
            SQL.AppendLine("        And X1.DocNo=@DocNo ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine("    ) C On B.DocNo=C.DocNo ");
            SQL.AppendLine("Where A.InvoiceType='1' And A.DocNo=@DocNo ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.DNo, A.InvoiceDocNo, ");
            SQL.AppendLine("A.InvoiceType, 'Sales Return Invoice' As InvoiceTypeDesc, ");
            SQL.AppendLine("A.Amt, A.Remark, ");
            SQL.AppendLine("B.DocDt, B.CurCode, Null As DueDt, Null As DOCtRemark ");
            SQL.AppendLine("From TblIncomingPaymentDtl A ");
            SQL.AppendLine("Left Join TblSalesReturnInvoiceHdr B On A.InvoiceDocNo=B.DocNo ");
            SQL.AppendLine("Where A.InvoiceType='2' And A.DocNo=@DocNo ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.DNo, A.InvoiceDocNo, ");
            SQL.AppendLine("A.InvoiceType, 'Sales Invoice 2' As InvoiceTypeDesc, ");
            SQL.AppendLine("A.Amt, A.Remark, ");
            SQL.AppendLine("B.DocDt, B.CurCode, B.DueDt, Null As DOCtRemark  ");
            SQL.AppendLine("From TblIncomingPaymentDtl A ");
            SQL.AppendLine("Left Join TblSalesInvoice2Hdr B On A.InvoiceDocNo=B.DocNo ");
            SQL.AppendLine("Where A.InvoiceType='3' And A.DocNo=@DocNo ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.DNo, A.InvoiceDocNo,  ");
            SQL.AppendLine("A.InvoiceType, 'Sales Project' As InvoiceTypeDesc,  ");
            SQL.AppendLine("A.Amt, A.Remark,  ");
            SQL.AppendLine("B.DocDt, B.CurCode, Null As DueDt, Null As DOCtRemark  ");
            SQL.AppendLine("From TblIncomingPaymentDtl A  ");
            SQL.AppendLine("Left Join TblSalesReturnInvoiceHdr B On A.InvoiceDocNo=B.DocNo  ");
            SQL.AppendLine("Where A.InvoiceType='5' And A.DocNo=@DocNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "InvoiceDocNo", "DocDt", "InvoiceType", "InvoiceTypeDesc", "CurCode", 
                    
                    //6-9
                    "Amt", "DueDt", "Remark", "DOCtRemark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Grd.Cells[Row, 8].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 9 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowIncomingPaymentDtl24(string DocNo, string Tbl, ref iGrid Grd35)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            if (mIsIncomingPaymentUseAdvanceCharge)
                SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark, A.LocalName ");
            else
                SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark, Null as LocalName ");
            SQL.AppendLine("From TblIncomingPaymentDtl" + Tbl +" A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd35, ref cm, SQL.ToString(),
                new string[] { "AcNo", "AcDesc", "DAmt", "CAmt", "Remark", "LocalName" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    if(Grd.Name == "Grd3") 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd35, Grd35.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd35, 0, 1);
        }

        private void ShowIncomingPaymentDtl3(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.BankCode, B.BankName, A.GiroNo, A.DueDt, A.CurCode, A.Amt ");
            SQL.AppendLine("From TblIncomingPaymentDtl3 A, TblBank B ");
            SQL.AppendLine("Where A.BankCode=B.BankCode And A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd4, ref cm, SQL.ToString(),
                new string[] { "BankCode", "BankName", "GiroNo", "DueDt", "CurCode", "Amt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 6 });
            ComputeGiroAmt();
            Sm.FocusGrd(Grd4, 0, 1);
        }

        private void ShowIncomingPaymentDtl6(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT CurCode, Amt ");
            SQL.AppendLine("FROM TblCustomerDepositSummary ");
            SQL.AppendLine("WHERE CtCode IN (SELECT CtCode FROM TblIncomingPaymentHdr WHERE DocNo = @DocNo); ");

            Sm.ShowDataInGrid(
                ref Grd6, ref cm, SQL.ToString(),
                new string[] { "CurCode", "Amt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 1 });
            Sm.FocusGrd(Grd6, 0, 0);
        }

        private void ShowIncomingPaymentFile(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd7, ref cm,
                   "Select DNo, FileName, CreateBy, CreateDt From TblIncomingPaymentFile Where DocNo=@DocNo Order By DNo",

                    new string[]
                    {
                        // 0
                        "DNo",

                        //1-3
                        "FileName", "CreateBy", "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd7, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("T", Grd7, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 7, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd7, 0, 0);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='IncomingPayment' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[]{ "UserName", "StatusDesc","LastUpDt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        #region FTP

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateIncomingPaymentFile(DocNo, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsIncomingPaymentAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsIncomingPaymentAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsIncomingPaymentAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsIncomingPaymentAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsIncomingPaymentAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsIncomingPaymentAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblRecvVdFile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        private MySqlCommand UpdateIncomingPaymentFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIncomingPaymentFile Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo=@DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        private bool IsGrd7ValueNotValid()
        {
            if (Grd7.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to upload at least 1 file.");
                return true;
            }
            return false;
        }

        #region Deposit Summary

        private void ProcessDepositSummary2(ref List<Rate> l)
        {
            PrepData(ref l);
            if (l.Count > 0)
            {
                ProcessDepositSummary2_1(ref l);
            }
            else
            {
                ProcessDepositSummary2_2();
            }
        }

        private void PrepData(ref List<Rate> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select CtCode, CurCode, ExcRate, Amt ");
            SQL.AppendLine("From TblCustomerDepositSummary2 ");
            SQL.AppendLine("Where CtCode = @CtCode ");
            SQL.AppendLine("And CurCode = @CurCode ");
            SQL.AppendLine("And Amt > 0 Order By CreateDt Asc; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CtCode",

                         //1-3
                         "CurCode",
                         "ExcRate",
                         "Amt"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Rate()
                        {
                            CtCode = Sm.DrStr(dr, c[0]),

                            CurCode = Sm.DrStr(dr, c[1]),
                            ExcRate = Sm.DrDec(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessDepositSummary2_1(ref List<Rate> l)
        {
            var cml = new List<MySqlCommand>();

            decimal DP = decimal.Parse(TxtDepositAmt.Text);
            for (int i = 0; i < l.Count; ++i)
            {
                if (DP > 0)
                {
                    cml.Add(ReduceAmtSummary2(ref l, i, DP));
                    DP = DP - l[i].Amt;
                }
            }

            decimal rate = Decimal.Parse(TxtRateAmt.Text);

            cml.Add(SaveExcRateOtherMainCurCode(rate));

            Sm.ExecCommands(cml);
        }

        private void ProcessDepositSummary2_2()
        {
            var SQL = new StringBuilder();
            var cml = new List<MySqlCommand>();

            SQL.AppendLine("Insert Into TblCustomerDepositSummary2 ");
            SQL.AppendLine("(CtCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@CtCode, @CurCode, @ExcRate, @Amt, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("        Amt=Amt+@Amt, ");
            SQL.AppendLine("        LastUpBy=@UserCode, ");
            SQL.AppendLine("        LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode2));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtDepositAmt.Text) * -1m);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommands(cml);
        }

        private void GetDepositSummary()
        {
            Sm.ClearGrd(Grd6, false);

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));

            SQL.AppendLine("Select CurCode, Amt ");
            SQL.AppendLine("From TblCustomerDepositSummary ");
            SQL.AppendLine("Where CtCode = @CtCode; ");

            Sm.ShowDataInGrid(
                ref Grd6, ref cm, SQL.ToString(),
                new string[] { "CurCode", "Amt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 1 });
            Sm.FocusGrd(Grd6, 0, 0);
        }

        private MySqlCommand ReduceAmtSummary2(ref List<Rate> l, int i, decimal DP)
        {
            var SQL1 = new StringBuilder();

            decimal x = ((DP - l[i].Amt) >= 0) ? l[i].Amt : DP;

            SQL1.AppendLine("Select @DP:=" + x + "; ");

            SQL1.AppendLine("Update TblCustomerDepositSummary2 ");
            SQL1.AppendLine("    Set Amt = Amt-@DP ");
            SQL1.AppendLine("Where CtCode = @CtCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@CtCode", l[i].CtCode);
            Sm.CmParam<String>(ref cm1, "@CurCode", l[i].CurCode);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate", l[i].ExcRate);
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        private MySqlCommand SaveExcRateOtherMainCurCode(decimal rate)
        {
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Insert Into TblCustomerDepositSummary2(CtCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL1.AppendLine("Select @CtCode, @CurCode2, @ExcRate2, @Amt2, @CreateBy, CurrentDateTime() ");
            SQL1.AppendLine("On Duplicate Key ");
            SQL1.AppendLine("   Update Amt=Amt+@Amt2, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm1, "@CurCode2", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm1, "@Amt2", Decimal.Parse(TxtDepositAmt.Text) * -1m);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate2", rate);
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        #endregion

        private void GetDuplicateCOAIndicator()
        {
            if (Grd3.Rows.Count > 1)
            {
                for (int i = 0; i < Grd3.Rows.Count - 1; ++i)
                {
                    string AcNo1 = Sm.GetGrdStr(Grd3, i, 1);
                    for (int j = (i + 1); j < Grd3.Rows.Count - 1; ++j)
                    {
                        string AcNo2 = Sm.GetGrdStr(Grd3, j, 1);

                        if (AcNo1 == AcNo2)
                        {
                            Grd3.Cells[i, 6].Value = true;
                            Grd3.Cells[j, 6].Value = true;
                        }
                    }
                }
            }
        }

        private void ComputeCurrencyRate()
        {
            string mCurCode2 = "IDR";
            string mRate = string.Empty;

            if (Sm.GetDte(DteDocDt).Length > 0 && Sm.GetLue(LueCurCode).Length > 0)
            {
                if (Sm.GetLue(LueCurCode) == mCurCode2) mRate = "1";
                else
                {
                    mRate = Sm.GetValue("Select Amt From TblDailyCurrencyRate Where CurCode2 = '" + mCurCode2 + "' And Curcode1 = '" + Sm.GetLue(LueCurCode) + "' And RateDt = '" + Sm.Left(Sm.GetDte(DteDocDt), 8) + "'");
                }
            }
            if (mRate.Length == 0) mRate = "0";

            TxtRateAmt.Text = Sm.FormatNum(Decimal.Parse(mRate), 0);
        }

        private void GetParameter()
        {
            string 
                MenuCodeForDocWithMInd = string.Empty,
                ActPeriod = string.Empty;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'MenuCodeForDocWithMInd', 'ActivePeriod', 'MainCurCode', 'DefaultCOAListIncomingPayment', 'IncomingPaymentCOAAmtCalculationMethod', ");
            SQL.AppendLine("'CustomerAcNoAR', 'DocNoFormat', 'VoucherCodeFormatType', 'BankAccountFormat', 'EmpCodeIncomingPayment', ");
            SQL.AppendLine("'IsIncomingPaymentCOAListShowCustomerAccount', 'IsShowCustomerCategory', 'IsFilterByCtCt', 'IsItCtFilteredByGroup', 'IsDOCtAmtRounded', ");
            SQL.AppendLine("'IsIncomingPaymentUseDeposit', 'IsIncomingPaymentOnlyShowDataAfterInsert', 'IsIncomingPaymentAmtUseCOAAmt', 'IsCustomerComboShowCategory', 'IsItemCategoryUseCOAAPAR', ");
            SQL.AppendLine("'IsVoucherBankAccountFilteredByGrp', 'IsCustomerComboBasedOnCategory', 'IsIncomingPaymentUseCustomerCategory', 'IsCOACouldBeChosenMoreThanOnce', 'IsUseDailyCurrencyRate', ");
            SQL.AppendLine("'IsIncomingPaymentUseCOA', 'IsIncomingPaymentShowDORemark', 'IsFilterBySite', 'IsBOMShowSpecifications', 'IsIncomingPaymentProjectSystemEnabled', ");
            SQL.AppendLine("'IsUseMInd', 'IsProjectSystemActive', 'IsAutoJournalActived', 'IsUseActivePeriod', 'IsIncomingPaymentUsePaymentDate', ");
            SQL.AppendLine("'IsIncomingPaymentUseAdvanceCharge', 'VRDescForIncomingPaymentDetail', 'IsGroupCOAActived', 'IsFilterByDept', 'IsIncomingPaymentTransactionValidatedbyClosingJournal', ");
            SQL.AppendLine("'IsIncomingPaymentAllowToUploadFile', 'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', 'PortForFTPClient', 'FileSizeMaxUploadFTPClient', 'FormatFTPClient', 'VoucherIncomingPaymentCCJournalFormat' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsUseActivePeriod": mIsUseActivePeriod = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsProjectSystemActive": mIsProjectSystemActive = ParValue == "Y"; break;
                            case "IsUseMInd": mIsUseMInd = ParValue == "Y"; break;
                            case "IsIncomingPaymentProjectSystemEnabled": mIsIncomingPaymentProjectSystemEnabled = ParValue == "Y"; break;
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsIncomingPaymentShowDORemark": mIsIncomingPaymentShowDORemark = ParValue == "Y"; break;
                            case "IsIncomingPaymentUseCOA": mIsIncomingPaymentUseCOA = ParValue == "Y"; break;
                            case "IsUseDailyCurrencyRate": mIsUseDailyCurrencyRate = ParValue == "Y"; break;
                            case "IsCOACouldBeChosenMoreThanOnce": mIsCOACouldBeChosenMoreThanOnce = ParValue == "Y"; break;
                            case "IsIncomingPaymentUseCustomerCategory": mIsIncomingPaymentUseCustomerCategory = ParValue == "Y"; break;
                            case "IsCustomerComboBasedOnCategory": mIsCustomerComboBasedOnCategory = ParValue == "Y"; break;
                            case "IsVoucherBankAccountFilteredByGrp": mIsVoucherBankAccountFilteredByGrp = ParValue == "Y"; break;
                            case "IsItemCategoryUseCOAAPAR": mIsItemCategoryUseCOAAPAR = ParValue == "Y"; break;
                            case "IsCustomerComboShowCategory": mIsCustomerComboShowCategory = ParValue == "Y"; break;
                            case "IsIncomingPaymentAmtUseCOAAmt": mIsIncomingPaymentAmtUseCOAAmt = ParValue == "Y"; break;
                            case "IsIncomingPaymentOnlyShowDataAfterInsert": mIsIncomingPaymentOnlyShowDataAfterInsert = ParValue == "Y"; break;
                            case "IsIncomingPaymentUseDeposit": mIsIncomingPaymentUseDeposit = ParValue == "Y"; break;
                            case "IsDOCtAmtRounded": mIsDOCtAmtRounded = ParValue == "Y"; break;
                            case "IsItCtFilteredByGroup": mIsItCtFilteredByGroup = ParValue == "Y"; break;
                            case "IsFilterByCtCt": mIsFilterByCtCt = ParValue == "Y"; break;
                            case "IsShowCustomerCategory": mIsShowCustomerCategory = ParValue == "Y"; break;
                            case "IsIncomingPaymentCOAListShowCustomerAccount": mIsIncomingPaymentCOAListShowCustomerAccount = ParValue == "Y"; break;
                            case "IsIncomingPaymentUsePaymentDate": mIsIncomingPaymentUsePaymentDate = ParValue == "Y"; break;
                            case "IsIncomingPaymentUseAdvanceCharge": mIsIncomingPaymentUseAdvanceCharge = ParValue == "Y"; break;
                            case "IsGroupCOAActived": mIsGroupCOAActived = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsIncomingPaymentTransactionValidatedbyClosingJournal": mIsIncomingPaymentTransactionValidatedbyClosingJournal = ParValue == "Y"; break;
                            case "IsIncomingPaymentAllowToUploadFile": mIsIncomingPaymentAllowToUploadFile = ParValue == "Y"; break;

                            //string
                            case "EmpCodeIncomingPayment": mEmpCodeIncomingPayment = ParValue; break;
                            case "BankAccountFormat": mBankAccountFormat = ParValue; break;
                            case "VoucherCodeFormatType": mVoucherCodeFormatType = ParValue; break;
                            case "DocNoFormat": mDocNoFormat = ParValue; break;
                            case "CustomerAcNoAR": mCustomerAcNoAR = ParValue; break;
                            case "IncomingPaymentCOAAmtCalculationMethod": mIncomingPaymentCOAAmtCalculationMethod = ParValue; break;
                            case "DefaultCOAListIncomingPayment": mDefaultCOAListIncomingPayment = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "ActivePeriod": ActPeriod = ParValue; break;
                            case "MenuCodeForDocWithMInd": MenuCodeForDocWithMInd = ParValue; break;
                            case "VRDescForIncomingPaymentDetail": mVRDescForIncomingPaymentDetail = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;
                            case "VoucherIncomingPaymentCCJournalFormat": mVoucherIncomingPaymentCCJournalFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
            if (MenuCodeForDocWithMInd.Length > 0)
                mMInd = MenuCodeForDocWithMInd.IndexOf("##" + mMenuCode + "##") != -1 ? "Y" : "N";
            if (ActPeriod.Length != 0) mActivePeriod = Decimal.Parse(ActPeriod);
        }

        internal void AddAdvanceCharge(string AdvanceChargeCode, string Lue, int LastRow, LookUpEdit LueAdvCharge)
        {
            decimal TotalAmt = decimal.Parse(TxtAmt2.Text);
            bool IsDataLueExisted = false;

            int DataLueExistedRow = 0;
            for (int row = 0; row < Grd3.Rows.Count - 1; row++)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd3, row, 8), Lue))
                {
                    IsDataLueExisted = true;
                    DataLueExistedRow = row;
                }
            }
            if (IsDataLueExisted) LastRow = DataLueExistedRow;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;

                SQL.AppendLine("Select A.AdvanceChargeCode, A.LocalName, A.PercentageInd, A.Value, A.AcType, B.AcNo, B.AcDesc ");
                SQL.AppendLine("From TblAdvanceCharge A ");
                SQL.AppendLine("Inner Join TblCoa B On A.AcNo=B.AcNo ");
                SQL.AppendLine("Where A.AdvanceChargeCode=@AdvanceChargeCode; ");

                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@AdvanceChargeCode", AdvanceChargeCode);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AdvanceChargeCode", "LocalName", "PercentageInd", "Value", "AcType", "AcNo", "AcDesc" });
                if (dr.HasRows)
                {
                    int Row = 0;
                    while (dr.Read())
                    {
                        Sm.SetGrdValue("S", Grd3, dr, c, Row + LastRow, 1, 5);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row + LastRow, 2, 6);
                        if (Sm.DrStr(dr, c[2]) == "Y")
                        {
                            Grd3.Cells[Row + LastRow, 3].Value = Sm.DrStr(dr, c[4]) == "D" ? Sm.DrDec(dr, c[3]) * (TotalAmt / 100) : 0m;
                            Grd3.Cells[Row + LastRow, 4].Value = Sm.DrStr(dr, c[4]) == "C" ? Sm.DrDec(dr, c[3]) * (TotalAmt / 100) : 0m;
                        }
                        else
                        {
                            Grd3.Cells[Row + LastRow, 3].Value = Sm.DrStr(dr, c[4]) == "D" ? Sm.DrDec(dr, c[3]) : 0m;
                            Grd3.Cells[Row + LastRow, 4].Value = Sm.DrStr(dr, c[4]) == "C" ? Sm.DrDec(dr, c[3]) : 0m;
                        }
                        Sm.SetGrdValue("S", Grd3, dr, c, Row + LastRow, 7, 1);

                        Grd3.Cells[Row + LastRow, 7].ReadOnly = iGBool.True;
                        Grd3.Cells[Row + LastRow, 7].BackColor = Color.FromArgb(224, 224, 224);
                        Grd3.Cells[Row + LastRow, 8].Value = Lue;

                        if (!IsDataLueExisted)
                        {
                            Grd3.Rows.Add();
                            Sm.SetGrdNumValueZero(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 4 });
                        }

                        Row += 1;
                    }
                }
                dr.Close();
            }

            for (int row = 0; row < Grd3.Rows.Count - 1; row++)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd3, row, 8), Lue) && IsLueEmpty(LueAdvCharge))
                {
                    Grd3.Rows.RemoveAt(Grd3.Rows[row].Index);
                }
            }
        }

        private bool IsLueEmpty(LookUpEdit Lue)
        {
            bool Result = true;
            if (Lue.EditValue == null ||
                Lue.Text.Trim().Length == 0 ||
                Lue.EditValue.ToString().Trim().Length == 0)
            {
                Result = true;
            }
            else
                Result = false;
            return Result;
        }

        internal void AddCustomerAcNoAR(string Lue, int LastRow)
        {
            string CustomerCode = Sm.GetLue(LueCtCode);
            decimal CustomerAcNoARDAmt = 0m;
            decimal CustomerAcNoARCAmt = 0m;
            decimal CustomerAcNoARAmt = 0m;
            bool IsCustomerAcNoARExisted = false;
            int CustomerAcNoARRow = 0;
            for (int row = 0; row < Grd3.Rows.Count - 1; row++)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd3, row, 8), "CustomerAcNoAR"))
                {
                    IsCustomerAcNoARExisted = true;
                    CustomerAcNoARRow = row;
                }
                if (Sm.GetGrdStr(Grd3, row, 8).Length > 0 && !Sm.CompareStr(Sm.GetGrdStr(Grd3, row, 8), "CustomerAcNoAR"))
                {
                    CustomerAcNoARDAmt += Sm.GetGrdDec(Grd3, row, 3);
                    CustomerAcNoARCAmt += Sm.GetGrdDec(Grd3, row, 4);
                }
            }
            CustomerAcNoARAmt = CustomerAcNoARDAmt - CustomerAcNoARCAmt;
            if (IsCustomerAcNoARExisted) LastRow = CustomerAcNoARRow;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;

                SQL.AppendLine("Select Null As AdvanceChargeCode, Null As LocalName, Null As PercentageInd,  ");
                SQL.AppendLine("0 As Value, Null As AcType, Concat(B.Parvalue,@CustomerCode) AS AcNo, A.AcDesc  ");
                SQL.AppendLine("From TblCoa A  ");
                SQL.AppendLine("Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.Parvalue Is Not Null  ");
                SQL.AppendLine("Where A.AcNo=Concat(B.Parvalue,@CustomerCode); ");

                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CustomerCode", CustomerCode);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc" });
                if (dr.HasRows)
                {
                    int Row = 0;
                    while (dr.Read())
                    {
                        Sm.SetGrdNumValueZero(Grd3, Row + LastRow, new int[] { 3, 4 });
                        Sm.SetGrdValue("S", Grd3, dr, c, Row + LastRow, 1, 0);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row + LastRow, 2, 1);
                        if (CustomerAcNoARAmt < 0)
                        {
                            Grd3.Cells[Row + LastRow, 3].Value = CustomerAcNoARAmt * -1;
                            Grd3.Cells[Row + LastRow, 4].Value = 0m;
                        }
                        else if (CustomerAcNoARAmt > 0)
                        {
                            Grd3.Cells[Row + LastRow, 3].Value = 0m;
                            Grd3.Cells[Row + LastRow, 4].Value = CustomerAcNoARAmt;
                        }
                        Grd3.Cells[Row + LastRow, 8].Value = "CustomerAcNoAR";

                        if (!IsCustomerAcNoARExisted)
                        {
                            Grd3.Rows.Add();
                            Sm.SetGrdNumValueZero(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 4 });
                        }
                        Row += 1;
                    }
                }
                dr.Close();
            }
        }

        public static void SetLueAdvanceChargeCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AdvanceChargeCode As Col1, A.AdvanceChargeName As Col2 From TblAdvanceCharge A ");
            SQL.AppendLine("Inner Join TblParameter B ON B.Parcode = 'CustomerAcNoAR' ");

            SQL.AppendLine("Where A.ActInd = 'Y' ");
            SQL.AppendLine("AND (B.Parvalue Is Not Null And A.Acno Not Like Concat(B.Parvalue, '%')); ");
            


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        

        private void SetCurCode(string BankAcCode)
        {
            var CurCode = Sm.GetValue("Select CurCode From TblBankAccount Where bankAcCode=@Param And CurCode Is Not Null;", BankAcCode);
            if (CurCode.Length > 0) Sm.SetLue(LueCurCode2, CurCode);
        }

        private string FormatNum(Decimal NumValue)
        {
            try
            {
                return String.Format("{0:#,###,##0.00##########}", NumValue);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return "0";
        }

        private string FormatNum(string Value)
        {
            decimal NumValue = 0m;
            try
            {
                Value = (Value.Length == 0) ? "0" : Value.Trim();
                if (!decimal.TryParse(Value, out NumValue))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid numeric value.");
                    NumValue = 0m;
                }
                if (NumValue < 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Value should not be less than 0.");
                    NumValue = 0m;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return String.Format("{0:#,###,##0.00##########}", NumValue);
        }

        private void FormatNumTxt(TextEdit Txt)
        {
            Txt.EditValue = FormatNum(Txt.Text);
        }

        private void SetLueCtCode(ref DXE.LookUpEdit Lue, string CtCode)
        {
            try
            {
                var SQL = new StringBuilder();
                string CtCtCode = Sm.GetLue(LueCtCtCode);

                if (CtCode.Length == 0)
                {
                    SQL.AppendLine("Select Distinct T1.CtCode As Col1, ");
                    if (mIsCustomerComboShowCategory)
                        SQL.AppendLine("Concat(T2.CtName, ' [',T3.CtCtName,']') As Col2 ");
                    else
                        SQL.AppendLine("T2.CtName As Col2 ");
                    SQL.AppendLine("From ( ");
                    SQL.AppendLine("        Select Distinct CtCode ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr ");
                    SQL.AppendLine("        Where CancelInd='N' ");
                    SQL.AppendLine("        And IfNull(ProcessInd, 'O') In ('O', 'P') ");
                    SQL.AppendLine("        And Amt<>0.00 ");
                    if (mIsUseMInd) SQL.AppendLine("        And MInd='" + mMInd + "' ");
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select Distinct CtCode ");
                    SQL.AppendLine("        From TblSalesReturnInvoiceHdr ");
                    SQL.AppendLine("        Where CancelInd='N' ");
                    SQL.AppendLine("        And IfNull(IncomingPaymentInd, 'O') In ('O', 'P') ");
                    SQL.AppendLine("        Union ALL ");
                    SQL.AppendLine("        Select Distinct CtCode ");
                    SQL.AppendLine("        From TblSalesInvoice2Hdr ");
                    SQL.AppendLine("        Where CancelInd='N' ");
                    SQL.AppendLine("        And IfNull(ProcessInd, 'O') In ('O', 'P') ");
                    SQL.AppendLine("        And Amt<>0.00 ");
                    SQL.AppendLine("        UNION ALL ");
                    SQL.AppendLine("        Select Distinct A.CtCode  ");
                    SQL.AppendLine("        From ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("            Select Distinct X2.ProjectImplementationDocNo, X1.CtCode ");
                    SQL.AppendLine("            From TblSalesInvoice5Hdr X1 ");
                    SQL.AppendLine("            Inner Join TblSalesInvoice5Dtl X2 On X1.DocNo = X2.DocNo ");
                    SQL.AppendLine("                Where X1.CancelInd = 'N' ");
                    SQL.AppendLine("                And IfNull(X1.ProcessInd, 'O') In ('O', 'P') ");
                    SQL.AppendLine("                And X1.Amt<>0.00 ");
                    SQL.AppendLine("        ) A ");
                    if (mIsFilterBySite)
                    {
                        if (mIsIncomingPaymentProjectSystemEnabled)
                        {
                            SQL.AppendLine("Inner Join TblProjectImplementationHdr B On A.ProjectImplementationDocNo=B.DocNo ");
                            SQL.AppendLine("Inner Join TblSOContractRevisionHdr C On B.SOContractDocNo = C.DocNo ");
                            SQL.AppendLine("Inner Join TblSOContractHdr D On C.SOCDocNo=D.DocNo ");
                            SQL.AppendLine("Inner Join TblBOQHdr E On D.BOQDocNo=E.DocNo ");
                            SQL.AppendLine("Inner Join TblLOPHdr F On E.LOPDocNo=F.DocNo ");
                            SQL.AppendLine("And (F.SiteCode Is Null Or ( ");
                            SQL.AppendLine("    F.SiteCode Is Not Null ");
                            SQL.AppendLine("    And Exists( ");
                            SQL.AppendLine("        Select 1 From TblGroupSite ");
                            SQL.AppendLine("        Where SiteCode=IfNull(F.SiteCode, '') ");
                            SQL.AppendLine("        And GrpCode In ( ");
                            SQL.AppendLine("            Select GrpCode From TblUser ");
                            SQL.AppendLine("            Where UserCode=@UserCode ");
                            SQL.AppendLine("            ) ");
                            SQL.AppendLine("        ) ");
                            SQL.AppendLine(")) ");
                        }
                    }
                    SQL.AppendLine("    ) T1 ");
                    SQL.AppendLine("    Inner Join TblCustomer T2 On T1.CtCode=T2.CtCode ");
                    SQL.AppendLine("    Inner Join TblCustomerCategory T3 On T2.CtCtCode = T3.CtCtCode ");
                    if (mIsCustomerComboBasedOnCategory)
                        SQL.AppendLine("Where T2.CtCtCode = '"+CtCtCode+"' ");
                    SQL.AppendLine("    Order By T2.CtName; ");
                }
                else
                {
                    SQL.AppendLine("Select Distinct T1.CtCode As Col1, ");
                    if (mIsCustomerComboShowCategory)
                        SQL.AppendLine("Concat(T1.CtName, ' [',T2.CtCtName,']') As Col2");
                    else
                        SQL.AppendLine("T1.CtName As Col2");
                    SQL.AppendLine("From TblCustomer T1 ");
                    SQL.AppendLine("Inner Join TblCustomerCategory T2 On T1.CtCtCode = T2.CtCtCode ");
                    SQL.AppendLine("Where T1.CtCode='" + CtCode + "'");
                }

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private decimal GetCOAAmt()
        {
            decimal COAAmt = 0m;
            string AcNo = string.Empty, CtCode = string.Empty, AcType = string.Empty;
            try
            {
                if (mIncomingPaymentCOAAmtCalculationMethod == "3")
                    AcNo = Sm.GetValue("Select COAAcNo From TblBankAccount Where BankAcCode = @Param ", Sm.GetLue(LueBankAcCode));
                else
                    AcNo = Sm.GetParameter("CustomerAcNoAR");
                CtCode = Sm.GetLue(LueCtCode);
                if (AcNo.Length > 0 && CtCode.Length > 0)
                {
                    if (mIncomingPaymentCOAAmtCalculationMethod == "1")
                        AcNo = string.Concat(AcNo, CtCode);
                    AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo=@Param;", AcNo);
                    for (int r = 0; r < Grd3.Rows.Count - 1; r++)
                    {
                        if(mIncomingPaymentCOAAmtCalculationMethod == "5") //YK
                            AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo=@Param;", Sm.GetGrdStr(Grd3, r, 1));
                        if (Sm.CompareStr(AcNo, (mIncomingPaymentCOAAmtCalculationMethod=="5") ? Sm.Left(Sm.GetGrdStr(Grd3, r, 1), AcNo.Length) : Sm.GetGrdStr(Grd3, r, 1)))
                        {
                            if (Sm.GetGrdDec(Grd3, r, 3) != 0)
                            {
                                if (AcType == "D")
                                    COAAmt += Sm.GetGrdDec(Grd3, r, 3);
                                else
                                    COAAmt -= Sm.GetGrdDec(Grd3, r, 3);
                            }
                            if (Sm.GetGrdDec(Grd3, r, 4) != 0)
                            {
                                if (AcType == "C")
                                    COAAmt += Sm.GetGrdDec(Grd3, r, 4);
                                else
                                    COAAmt -= Sm.GetGrdDec(Grd3, r, 4);
                            }
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return COAAmt;
        }

        internal void ComputeAmt()
        {
            decimal Amt = 0m;
            decimal DepositAmt = 0m;
            
            Amt += decimal.Parse(TxtGiroAmt.Text);
            DepositAmt = Decimal.Parse(TxtDepositAmt.Text);

            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && Sm.GetGrdStr(Grd1, Row, 9).Length > 0)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5), "1") || Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5), "3") || Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5), "5"))
                            Amt += Sm.GetGrdDec(Grd1, Row, 9);
                        else
                            Amt -= Sm.GetGrdDec(Grd1, Row, 9);
                    }
                }
            }

            //if (mIsIncomingPaymentAmtUseCOAAmt) Amt += GetCOAAmt();
            //if (mIsIncomingPaymentUseDeposit) Amt -= DepositAmt;
            
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
            ComputeAmt2();
        }

        internal void ComputeAmt2()
        {
            decimal Amt = 0m, RateAmt = 0m;
            
            decimal DepositAmt = 0m;
            DepositAmt = Decimal.Parse(TxtDepositAmt.Text);

            if (TxtAmt.Text.Length != 0) Amt = decimal.Parse(TxtAmt.Text);
            if (TxtRateAmt.Text.Length != 0) RateAmt = decimal.Parse(TxtRateAmt.Text);

            if (mIsIncomingPaymentAmtUseCOAAmt) Amt += GetCOAAmt();
            if (mIsIncomingPaymentUseDeposit) Amt -= DepositAmt;

            TxtAmt2.EditValue = Sm.FormatNum(Amt * RateAmt, 0);
        }

        internal void ComputeAmtSI()
        {
            if (mIsIncomingPaymentAmtUseCOAAmt)
            {
                decimal TotalAmt = 0m;
                if (Grd1.Rows.Count > 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        string DocNo = Sm.GetGrdStr(Grd1, Row, 2);
                        TotalAmt += decimal.Parse(Sm.GetValue("Select ifnull(SUM(Amt), 0.00) from tblsalesinvoicehdr where Find_In_Set(DocNo, @Param)", DocNo));
                        TxtTotalAmt.EditValue = Sm.FormatNum(TotalAmt, 0);
                    }
                }
            }
        }



        private void SetLuePaidToBankCode(ref DXE.LookUpEdit Lue, string CtCode, string Type)
        {
            try
            {
                //Type = 1 -> For View
                //Type = 2 -> For Insert

                var SQL = new StringBuilder();

                if (Type == "1")
                {
                    SQL.AppendLine("Select BankCode As Col1, BankName As Col2, ");
                    SQL.AppendLine("'-' As Col3, 'xxx' As Col4  ");
                    SQL.AppendLine("From TblBank Order By BankName;");
                }
                else
                {
                    SQL.AppendLine("Select Concat(A.BankCode, A.DNo) As Col1, B.BankName As Col2, ");
                    SQL.AppendLine("Trim(Concat(");
                    SQL.AppendLine("    Case When IfNull(A.BankBranch,'')='' Then '' Else Concat('Branch : ', A.BankBranch) End,  ");
                    SQL.AppendLine("    Case When IfNull(A.BankAcName,'')='' Then '' Else Concat(Case When IfNull(A.BankBranch,'')='' Then '' Else ', ' End, 'Name : ', A.BankAcName) End,  ");
                    SQL.AppendLine("    Case When IfNull(A.BankAcNo,'')='' Then '' Else Concat(Case When IfNull(A.BankBranch,'')='' Or IfNull(A.BankAcName,'')='' Then '' Else ', ' End, 'Account Number : ', A.BankAcNo) End ");
                    SQL.AppendLine(")) As Col3, ");
                    SQL.AppendLine("A.DNo As Col4  ");
                    SQL.AppendLine("From TblCustomerBankAccount A ");
                    SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
                    SQL.AppendLine("Where CtCode='" + CtCode + "' ");
                    SQL.AppendLine("Order By B.BankName, A.DNo; ");
                }
                Sm.SetLue4(
                    ref Lue,
                    SQL.ToString(),
                    0, 170, 300, 0, false, true, true, false,
                    "Code", "Bank", "Bank Branch/Account Name/Account No.", "DNo", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowPaidToBankCodeInfo(string CtCode, string DNo)
        {
            var cm = new MySqlCommand();
            try
            {
                Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select BankBranch, BankAcName, BankAcNo " +
                        "From TblCustomerBankAccount " +
                        "Where CtCode=@CtCode And DNo=@DNo; ",
                        new string[] { "BankBranch", "BankAcName", "BankAcNo" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPaidToBankBranch.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPaidToBankAcName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtPaidToBankAcNo.EditValue = Sm.DrStr(dr, c[2]);
                        }, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedInvoice()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            Sm.GetGrdStr(Grd1, Row, 5) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GetProfitCenterCode()
        {
            var Value = Sm.GetLue(LueBankAcCode);
            if (Value.Length == 0) return string.Empty;
            return
                Sm.GetValue(
                    " SELECT B.ProfitCenterCode FROM tblbankaccount A " +
                    " INNER JOIN tblsite B ON A.SiteCode = B.SiteCode " +
                    " INNER JOIN tblprofitcenter C ON B.ProfitCenterCode = C.ProfitCenterCode " +
                    " WHERE A.Bankaccode = @Param ",

            Value);
        }

        private void ReComputeOutstandingAmt()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocNo, T.InvoiceType, T.Amt From (");
            //sales invoice 
            SQL.AppendLine("    Select A.DocNo, '1' As InvoiceType, A.Amt-IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("    From TblSalesInvoiceHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) Amt ");
            SQL.AppendLine("        From TblIncomingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("        Inner Join TblSalesInvoiceHdr T3 ");
            SQL.AppendLine("            On T2.InvoiceDocNo=T3.DocNo ");
            SQL.AppendLine("            And Locate(Concat('##', T3.DocNo, '1', '##'), @Param)>0 ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("        Group By T2.InvoiceDocNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("    And Locate(Concat('##', A.DocNo, '1', '##'), @Param)>0 ");
            //sales return
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select A.DocNo, '2' As InvoiceType, A.TotalAmt-IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("    From TblSalesReturnInvoiceHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) Amt ");
            SQL.AppendLine("        From TblIncomingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
            SQL.AppendLine("        Inner Join TblSalesReturnInvoiceHdr T3 ");
            SQL.AppendLine("            On T2.InvoiceDocNo=T3.DocNo ");
            SQL.AppendLine("            And T3.CtCode=@CtCode ");
            SQL.AppendLine("            And IfNull(T3.IncomingPaymentInd, 'O')<>'F' ");
            SQL.AppendLine("            And Locate(Concat('##', T3.DocNo, '2', '##'), @Param)>0 ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("        And T1.CtCode=@CtCode ");
            SQL.AppendLine("        Group By T2.InvoiceDocNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.IncomingPaymentInd, 'O')<>'F' ");
            SQL.AppendLine("    And Locate(Concat('##', A.DocNo, '2', '##'), @Param)>0 ");
            //sales invoice 2
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select A.DocNo, '3' As InvoiceType, A.Amt-IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("    From TblSalesInvoice2Hdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) Amt ");
            SQL.AppendLine("        From TblIncomingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='3' ");
            SQL.AppendLine("        Inner Join TblSalesInvoice2Hdr T3 ");
            SQL.AppendLine("            On T2.InvoiceDocNo=T3.DocNo ");
            SQL.AppendLine("            And Locate(Concat('##', T3.DocNo, '3', '##'), @Param)>0 ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("        Group By T2.InvoiceDocNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("    And Locate(Concat('##', A.DocNo, '3', '##'), @Param)>0 ");
            //sales invoice 5
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select A.DocNo, '5' As InvoiceType, A.Amt-IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("    From TblSalesInvoice5Hdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) Amt ");
            SQL.AppendLine("        From TblIncomingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='5' ");
            SQL.AppendLine("        Inner Join TblSalesInvoice5Hdr T3 ");
            SQL.AppendLine("            On T2.InvoiceDocNo=T3.DocNo ");
            SQL.AppendLine("            And Locate(Concat('##', T3.DocNo, '5', '##'), @Param)>0 ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("        Group By T2.InvoiceDocNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("    And Locate(Concat('##', A.DocNo, '5', '##'), @Param)>0 ");

            SQL.AppendLine(") T; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@Param", GetSelectedInvoice());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { "DocNo", "InvoiceType", "Amt" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 2), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5), Sm.DrStr(dr, 1)))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 2);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        internal void SetLueBankAcCode(ref LookUpEdit Lue, string BankAcCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.BankAcCode As Col1, ");
            if (mBankAccountFormat == "1")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            else if (mBankAccountFormat == "2")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(IfNull(A.BankAcNm, ''), ' : ', A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(' [', B.BankName, ']') Else '' End ");
                SQL.AppendLine("    )) As Col2 ");
            }
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");

            if (BankAcCode.Length != 0)
                SQL.AppendLine("Where A.BankAcCode=@BankAcCode ");
            else
            {
                if (mIsVoucherBankAccountFilteredByGrp)
                {
                    SQL.AppendLine("Where Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }

            SQL.AppendLine("Order By A.Sequence; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BankAcCode.Length != 0) Sm.SetLue(Lue, BankAcCode);
        }

        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };


        private static string Convert(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang2(cettt);
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }
        #endregion

        #region convertsatuan

        private string ConvertFromDecToWord(Decimal d)
        {
            string[] satuan = new string[10] { "nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan" };
            string[] belasan = new string[10] { "sepuluh", "sebelas", "dua belas", "tiga belas", "empat belas", "lima belas", "enam belas", "tujuh belas", "delapan belas", "sembilan belas" };
            string[] puluhan = new string[10] { "", "", "dua puluh", "tiga puluh", "empat puluh", "lima puluh", "enam puluh", "tujuh puluh", "delapan puluh", "sembilan puluh" };
            string[] ribuan = new string[5] { "", "ribu", "juta", "milyar", "triliyun" };

            string strHasil = "";
            Decimal frac = d - Decimal.Truncate(d);

            if (Decimal.Compare(frac, 0.0m) != 0)
                strHasil = ConvertFromDecToWord(Decimal.Round(frac * 100)) + "";
            else
                strHasil = "";
            int xDigit = 0;
            int xPosisi = 0;

            string strTemp = Decimal.Truncate(d).ToString();
            for (int i = strTemp.Length; i > 0; i--)
            {
                string tmpx = "";
                xDigit = System.Convert.ToInt32(strTemp.Substring(i - 1, 1)); //Convert.ToInt32(strTemp.Substring(i - 1, 1));
                xPosisi = (strTemp.Length - i) + 1;
                switch (xPosisi % 3)
                {
                    case 1:
                        bool allNull = false;
                        if (i == 1)
                            tmpx = satuan[xDigit] + " ";
                        else if (strTemp.Substring(i - 2, 1) == "1")
                            tmpx = belasan[xDigit] + " ";
                        else if (xDigit > 0)
                            tmpx = satuan[xDigit] + " ";
                        else
                        {
                            allNull = true;
                            if (i > 1)
                                if (strTemp.Substring(i - 2, 1) != "0")
                                    allNull = false;
                            if (i > 2)
                                if (strTemp.Substring(i - 3, 1) != "0")
                                    allNull = false;
                            tmpx = "";
                        }

                        if ((!allNull) && (xPosisi > 1))
                            if ((strTemp.Length == 4) && (strTemp.Substring(0, 1) == "1"))
                                tmpx = "se" + ribuan[(int)Decimal.Round(xPosisi / 3m)] + " ";
                            else
                                tmpx = tmpx + ribuan[(int)Decimal.Round(xPosisi / 3)] + " ";
                        strHasil = tmpx + strHasil;
                        break;
                    case 2:
                        if (xDigit > 0)
                            strHasil = puluhan[xDigit] + " " + strHasil;
                        break;
                    case 0:
                        if (xDigit > 0)
                            if (xDigit == 1)
                                strHasil = "seratus " + strHasil;
                            else
                                strHasil = satuan[xDigit] + " ratus " + strHasil;
                        break;
                }
            }
            strHasil = strHasil.Trim().ToLower();
            if (strHasil.Length > 0)
            {
                strHasil = strHasil.Substring(0, 1).ToUpper() +
                  strHasil.Substring(1, strHasil.Length - 1);
            }
            return strHasil;
        }

        private string RemoveChar(Decimal x)
        {
            string numb = x.ToString();
            int indexChar = numb.IndexOf(".");
            numb = numb.Substring(0, indexChar);
            return numb;
        }

        private string dec(Decimal a)
        {
            string numbEnd = " koma ";
            string numbInd = string.Empty;

            string numb = a.ToString();
            int indexChar = numb.IndexOf(".") + 1;

            numb = numb.Substring(indexChar, (numb.Length - indexChar));

            string numbDec = numb;
            for (int i = 0; i < numbDec.Length; i++)
            {
                if (numbDec[i].ToString() == "0")
                {
                    numbInd = "nol ";
                }
                else if (numbDec[i].ToString() == "1")
                {
                    numbInd = "satu ";
                }
                else if (numbDec[i].ToString() == "2")
                {
                    numbInd = "dua ";
                }
                else if (numbDec[i].ToString() == "3")
                {
                    numbInd = "tiga ";
                }
                else if (numbDec[i].ToString() == "4")
                {
                    numbInd = "empat ";
                }
                else if (numbDec[i].ToString() == "5")
                {
                    numbInd = "lima ";
                }
                else if (numbDec[i].ToString() == "6")
                {
                    numbInd = "enam ";
                }
                else if (numbDec[i].ToString() == "7")
                {
                    numbInd = "tujuh ";
                }
                else if (numbDec[i].ToString() == "8")
                {
                    numbInd = "delapan ";
                }
                else
                {
                    numbInd = "sembilan ";
                }

                numbEnd = numbEnd + numbInd;
            }

            numb = numbEnd;
            return numb;
        }

        #endregion

        private void ParPrint()
          {
              string ParValue = Sm.GetValue("Select ParValue From TblParameter Where Parcode='NumberOfInventoryUomCode' ");
              if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
              var l = new List<IpHdr>();
              var ldtl = new List<IpDtl>();
              var ldtl2 = new List<IpDtl2>();
              var l2 = new List<Employee>();
              var lK = new List<IpDtlKIM>();
              var lS = new List<IpSignKIM>();
              var lDtlS = new List<IPSignIMS>();
              var lDtlS2 = new List<IPSignIMS2>();

              string[] TableName = { "IpHdr", "IpDtl", "IpDtl2", "Employee", "IpDtlKIM", "IpSignKIM", "IPSignIMS", "IPSignIMS2" };
              List<IList> myLists = new List<IList>();


              #region Header
              var cm = new MySqlCommand();

              var SQL = new StringBuilder();
              SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
              SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
              SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
              SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
              SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
              SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', "); 
              SQL.AppendLine("C.DocNo As DocNoVC, DATE_FORMAT(C.DocDt,'%d %M %Y')As DocDt, ");
              SQL.AppendLine("Concat(IfNull(F.BankAcNo, ''), ' [', IfNull(F.BankAcNm, ''), ']') As BankAcc,");
              SQL.AppendLine("(Select OptDesc From TblOption Where OptCat='VoucherPaymentType' AND OptCode=B.PaymentType Limit 1) As PaymentType,");
              SQL.AppendLine("E.BankName As GiroBankName, D.CtName, B.DocNo As DocNoVR, DATE_FORMAT(B.DocDt,'%d %M %Y') As DocDtVR, ");
              SQL.AppendLine("A.Remark, A.CurCode, A.Amt, A.DocNo, D.Address, DATE_FORMAT(A.DocDt,'%d %M %Y')As DocDtIP, C.Remark As RemarkVC, A1.InvoiceDocNoKIM, G.ProjectName ");
              SQL.AppendLine("From tblincomingpaymenthdr A ");
              SQL.AppendLine("Inner Join ");
              SQL.AppendLine("( ");
              SQL.AppendLine("    Select DocNo, Group_Concat(Distinct InvoiceDocNo Separator '\n') As InvoiceDocNoKIM ");
              SQL.AppendLine("    From TblIncomingPaymentDtl ");
              SQL.AppendLine("    Where DocNo = @DocNo ");
              SQL.AppendLine("    Group By DocNo ");
              SQL.AppendLine(") A1 On A.DocNo = A1.DocNo ");
              SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
              SQL.AppendLine("Left Join tblvoucherhdr C On A.VoucherRequestDocNo=C.VoucherRequestDocNo ");
              SQL.AppendLine("Left Join tblcustomer D On A.CtCode=D.CtCode ");
              SQL.AppendLine("Left Join tblbank E On B.BankCode=E.BankCode ");
              SQL.AppendLine("Left Join TblBankAccount F On B.BankAcCode=F.BankAcCode ");
              SQL.AppendLine("Left Join  ");
              SQL.AppendLine("  (  ");
	          SQL.AppendLine("      SELECT X1.DocNo, Group_Concat(Distinct IFNULL(X9.ProjectName, X8.ProjectName)) ProjectName  ");
	          SQL.AppendLine("      FROM tblincomingpaymentdtl X1  ");
              SQL.AppendLine("      Inner Join tblsalesinvoicedtl X2 ON X1.InvoiceDocNo = X2.DocNo And X1.DocNo = @DocNo  ");
              SQL.AppendLine("      Inner Join tbldoct2dtl X3 ON X2.DOCtDocNo = X3.DocNo AND X2.DOCtDNo = X3.DNo  ");
              SQL.AppendLine("      Inner Join tbldoct2hdr X4 ON X3.DocNo = X4.DocNo  ");
              SQL.AppendLine("      Inner Join tbldrdtl X5 ON X4.DRDocNo = X5.DocNo ");
              SQL.AppendLine("      Inner Join tblsocontracthdr X6 ON X5.SODocNo = X6.DocNo ");
              SQL.AppendLine("      Inner Join tblboqhdr X7 ON X6.BOQDocNo = X7.DocNo ");
              SQL.AppendLine("      Inner Join tbllophdr X8 ON X7.LOPDocNo = X8.DocNo ");
              SQL.AppendLine("      Left Join tblprojectgroup X9 ON X8.PGCode = X9.PGCode ");
	          SQL.AppendLine("      Group By X1.DocNo ");
              SQL.AppendLine("   ) G ON A.DocNo = G.DocNo ");
              SQL.AppendLine("Where A.DocNo=@DocNo "); 

              using (var cn = new MySqlConnection(Gv.ConnectionString))
              {
                  cn.Open();
                  cm.Connection = cn;
                  cm.CommandText = SQL.ToString();
                  Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                  Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

                  var dr = cm.ExecuteReader();
                  var c = Sm.GetOrdinal(dr, new string[] 
                          {
                           //0
                          "CompanyLogo",

                           //1-5
                           "CompanyName",
                           "CompanyAddress",
                           "CompanyAddressCity",
                           "CompanyPhone",
                           "CompanyFax",
                           
                           //6-10
                           "DocNoVC",
                           "DocDt",
                           "BankAcc",
                           "PaymentType",
                           "GiroBankName",
                          
                           //11-15
                           "CtName",
                           "DocNoVR",
                           "DocDtVR",
                           "Remark",
                           "CurCode",

                           //16-20
                           "Amt",
                           "DocNo",
                           "Address",
                           "DocDtIP",
                           "RemarkVC",

                           //21-22
                           "InvoiceDocNoKIM",
                           "ProjectName"
                          });
                  if (dr.HasRows)
                  {
                      while (dr.Read())
                      {
                          l.Add(new IpHdr()
                          {
                              CompanyLogo = Sm.DrStr(dr, c[0]),

                              CompanyName = Sm.DrStr(dr, c[1]),
                              CompanyAddress = Sm.DrStr(dr, c[2]),
                              CompanyLongAddress = Sm.DrStr(dr, c[3]),
                              CompanyPhone = Sm.DrStr(dr, c[4]),
                              CompanyFax = Sm.DrStr(dr, c[5]),

                              DocNoVC = Sm.DrStr(dr, c[6]),
                              DocDt = Sm.DrStr(dr, c[7]),
                              BankAcc = Sm.DrStr(dr, c[8]),
                              PaymentType = Sm.DrStr(dr, c[9]),
                              GiroBankName = Sm.DrStr(dr, c[10]),

                              CtName = Sm.DrStr(dr, c[11]),
                              DocNoVR = Sm.DrStr(dr, c[12]),
                              DocDtVR= Sm.DrStr(dr, c[13]),
                              Remark = Sm.DrStr(dr, c[14]),
                              CurCode = Sm.DrStr(dr, c[15]),

                              Amt = Sm.DrDec(dr, c[16]),
                              DocNo = Sm.DrStr(dr, c[17]),
                              Address = Sm.DrStr(dr, c[18]),
                              DocDtIP = Sm.DrStr(dr, c[19]),
                              Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[16])),
                              
                              Terbilang2 = Convert(Sm.DrDec(dr, c[16])),
                              RemarkVC = Sm.DrStr(dr, c[20]),
                              InvoiceDocNoKIM = Sm.DrStr(dr, c[21]),
                              Project = Sm.DrStr(dr, c[22]),

                              PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                          });
                      }
                  }
                  dr.Close();
              }
              myLists.Add(l);

              #endregion 

              #region Detail

              var cmDtl = new MySqlCommand();

              var SQLDtl = new StringBuilder();
              using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
              {
                  cnDtl.Open();
                  cmDtl.Connection = cnDtl;

                  SQLDtl.AppendLine("Select Remark As Description, Group_concat(Distinct Remark separator '\n' )As Description2, Group_concat(Distinct invoicedocno separator '\n' )As Invoicedocno, Amt");
                  SQLDtl.AppendLine("From tblincomingpaymentdtl ");
                  SQLDtl.AppendLine("Where DocNo=@DocNo ");

                  cmDtl.CommandText = SQLDtl.ToString();

                  Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                  var drDtl = cmDtl.ExecuteReader();
                  var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                      {
                      //0
                      "Description",

                      //1-3
                      "Description2",
                      "InvoiceDocNo",
                      "Amt",
                      });
                  if (drDtl.HasRows)
                  {
                      while (drDtl.Read())
                      {
                          ldtl.Add(new IpDtl()
                          {
                              Description = Sm.DrStr(drDtl, cDtl[0]),
                              Description2 = Sm.DrStr(drDtl, cDtl[1]),
                              InvoiceDocNo = Sm.DrStr(drDtl, cDtl[2]),
                              Amt = Sm.DrDec(drDtl, cDtl[3]),
                          });
                      }
                  }
                  drDtl.Close();
              }
              myLists.Add(ldtl);
              #endregion

              #region Detail2

              var cmDtl2 = new MySqlCommand();

              var SQLDtl2 = new StringBuilder();
              using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
              {
                  cnDtl2.Open();
                  cmDtl2.Connection = cnDtl2;

                  SQLDtl2.AppendLine("Select Remark As Description, Sum(Amt)As Amt ");
                  SQLDtl2.AppendLine("From tblincomingpaymentdtl ");
                  SQLDtl2.AppendLine("Where DocNo=@DocNo ");
                  SQLDtl2.AppendLine("Group by DocNo ");

                  cmDtl2.CommandText = SQLDtl2.ToString();

                  Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);

                  var drDtl2 = cmDtl2.ExecuteReader();
                  var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                      {
                      //0
                      "Description",

                      //1-2
                      "Amt",
                      });
                  if (drDtl2.HasRows)
                  {
                      while (drDtl2.Read())
                      {
                          ldtl2.Add(new IpDtl2()
                          {
                              Description = Sm.DrStr(drDtl2, cDtl2[0]),
                              Amt = Sm.DrDec(drDtl2, cDtl2[1]),
                              Terbilang = Sm.Terbilang(Sm.DrDec(drDtl2, cDtl2[1])),
                              Terbilang2 = Convert(Sm.DrDec(drDtl2, cDtl2[1])),
                             // Terbilang2 = ConvertFromDecToWord(Decimal.Parse(RemoveChar(Sm.DrDec(drDtl2, cDtl2[2])))) + dec(Sm.DrDec(drDtl2, cDtl2[2])),
                          });
                      }
                  }
                  drDtl2.Close();
              }
              myLists.Add(ldtl2);
              #endregion

              #region Signature KIM
              var cm2 = new MySqlCommand();
              var SQL2 = new StringBuilder();

              SQL2.AppendLine("Select A.EmpCode, A.EmpName, B.PosName From TblEmployee A ");
              SQL2.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
              SQL2.AppendLine("Where A.EmpCode=@EmpCode ");

              using (var cn2 = new MySqlConnection(Gv.ConnectionString))
              {
                  cn2.Open();
                  cm2.Connection = cn2;
                  cm2.CommandText = SQL2.ToString();
                  Sm.CmParam<String>(ref cm2, "@EmpCode", mEmpCodeIncomingPayment);
                  var dr2 = cm2.ExecuteReader();
                  var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0-2
                         "EmpCode",
                         "EmpName",
                         "PosName",
                        
                        });
                  if (dr2.HasRows)
                  {
                      while (dr2.Read())
                      {
                          l2.Add(new Employee()
                          {
                              EmpCode = Sm.DrStr(dr2, c2[0]),

                              EmpName = Sm.DrStr(dr2, c2[1]),
                              Position = Sm.DrStr(dr2, c2[2]),
                          });
                      }
                  }
                  dr2.Close();
              }
              myLists.Add(l2);

              #endregion

              #region Detail KIM

              var SQLK = new StringBuilder();
              var cmK = new MySqlCommand();

              SQLK.AppendLine("Select A.InvoiceDocNo, C.Description, A.Amt ");
              SQLK.AppendLine("From TblIncomingPaymentDtl A ");
              SQLK.AppendLine("Inner Join TblIncomingPaymentHdr B On A.DocNo = B.DocNo And A.DocNo = @DocNo ");
              SQLK.AppendLine("Inner Join TblVoucherRequestDtl C On B.VoucherRequestDocNo = C.DocNo And A.DNo = C.DNo; ");

              using (var cnK = new MySqlConnection(Gv.ConnectionString))
              {
                  cnK.Open();
                  cmK.Connection = cnK;
                  cmK.CommandText = SQLK.ToString();

                  Sm.CmParam<String>(ref cmK, "@DocNo", TxtDocNo.Text);

                  var drK = cmK.ExecuteReader();
                  var cK = Sm.GetOrdinal(drK, new string[] { "InvoiceDocNo", "Description", "Amt" });
                  if (drK.HasRows)
                  {
                      while (drK.Read())
                      {
                          lK.Add(new IpDtlKIM()
                          {
                              InvoiceDocNo = Sm.DrStr(drK, cK[0]),
                              Description = Sm.DrStr(drK, cK[1]),
                              Amt = Sm.DrDec(drK, cK[2]),
                          });
                      }
                  }
                  drK.Close();
              }
              myLists.Add(lK);

              #endregion

              #region Signature KIM 2

              var SQLS = new StringBuilder();
              var cmS = new MySqlCommand();

              SQLS.AppendLine("Select E.AckBy1, E.AckPos1, E.AckSign1, F.AckBy2, F.AckPos2, F.AckSign2, Upper(IfNull(C.EmpName, B.UserName)) As PrepBy, D.PosName As PrepPos, Concat(IfNull(G.ParValue, ''), A.CreateBy, '.jpg') As PrepSign ");
              SQLS.AppendLine("From TblIncomingPaymentHdr A ");
              SQLS.AppendLine("Inner Join TblUser B On A.CreateBy = B.UserCode And A.DocNo = @DocNo ");
              SQLS.AppendLine("Left Join TblEmployee C On A.CreateBy = C.UserCode ");
              SQLS.AppendLine("Left Join TblPosition D On C.PosCode = D.PosCode ");
              SQLS.AppendLine("Left Join  ");
              SQLS.AppendLine("( ");
              SQLS.AppendLine("    Select Upper(T1.EmpName) As AckBy1, T2.PosName As AckPos1, ");
              SQLS.AppendLine("    Concat(IfNull(T3.ParValue, ''), T1.UserCode, '.jpg') As AckSign1 ");
              SQLS.AppendLine("    From TblEmployee T1 ");
              SQLS.AppendLine("    Inner Join TblPosition T2 On T1.PosCode = T2.PosCode And T1.PosCode = '2007' ");
              SQLS.AppendLine("    Left Join TblParameter T3 On T3.ParCode = 'ImgFileSignature' ");
              SQLS.AppendLine(") E On 0 = 0 ");
              SQLS.AppendLine("Left Join ");
              SQLS.AppendLine("( ");
              SQLS.AppendLine("    Select Upper(T1.EmpName) As AckBy2, T2.PosName As AckPos2, ");
              SQLS.AppendLine("    Concat(IfNull(T3.ParValue, ''), T1.UserCode, '.jpg') As AckSign2 ");
              SQLS.AppendLine("    From TblEmployee T1 ");
              SQLS.AppendLine("    Inner Join TblPosition T2 On T1.PosCode = T2.PosCode And T1.PosCode = '2006' ");
              SQLS.AppendLine("    Left Join TblParameter T3 On T3.ParCode = 'ImgFileSignature' ");
              SQLS.AppendLine(") F On 0 = 0 ");
              SQLS.AppendLine("Left Join TblParameter G On G.Parcode = 'ImgFileSignature'; ");

              using (var cnS = new MySqlConnection(Gv.ConnectionString))
              {
                  cnS.Open();
                  cmS.Connection = cnS;
                  cmS.CommandText = SQLS.ToString();

                  Sm.CmParam<String>(ref cmS, "@DocNo", TxtDocNo.Text);

                  var drS = cmS.ExecuteReader();
                  var cS = Sm.GetOrdinal(drS, new string[] 
                      {
                      //0
                      "AckBy1",

                      //1-5
                      "AckPos1",
                      "AckSign1",
                      "AckBy2",
                      "AckPos2",
                      "AckSign2",

                      //6-8
                      "PrepBy",
                      "PrepPos",
                      "PrepSign"
                      });
                  if (drS.HasRows)
                  {
                      while (drS.Read())
                      {
                          lS.Add(new IpSignKIM()
                          {
                              AckBy1 = Sm.DrStr(drS, cS[0]),
                              AckPos1 = Sm.DrStr(drS, cS[1]),
                              AckSign1 = Sm.DrStr(drS, cS[2]),
                              AckBy2 = Sm.DrStr(drS, cS[3]),
                              AckPos2 = Sm.DrStr(drS, cS[4]),
                              AckSign2 = Sm.DrStr(drS, cS[5]),
                              PrepBy = Sm.DrStr(drS, cS[6]),
                              PrepPos = Sm.DrStr(drS, cS[7]),
                              PrepSign = Sm.DrStr(drS, cS[8]),
                          });
                      }
                  }
                  drS.Close();
              }
              myLists.Add(lS);

              #endregion

              #region Detail Signature IMS

              //Dibuat Oleh
              var cmDtlS = new MySqlCommand();

              var SQLDtlS = new StringBuilder();
              using (var cnDtlS = new MySqlConnection(Gv.ConnectionString))
              {
                  cnDtlS.Open();
                  cmDtlS.Connection = cnDtlS;

                  SQLDtlS.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                  SQLDtlS.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                  SQLDtlS.AppendLine("From ( ");
                  SQLDtlS.AppendLine("    Select Distinct ");
                  SQLDtlS.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, '1' As DNo, 0 As Level, 'Prepared By,' As Title, Left(A.CreateDt, 8) As LastUpDt  ");
                  SQLDtlS.AppendLine("    From TblIncomingPaymentHdr A ");
                  SQLDtlS.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                  SQLDtlS.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                  SQLDtlS.AppendLine(") T1 ");
                  SQLDtlS.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                  SQLDtlS.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                  SQLDtlS.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                  SQLDtlS.AppendLine("Group By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Title ");
                  SQLDtlS.AppendLine("Order By T1.Level; ");

                  cmDtlS.CommandText = SQLDtlS.ToString();
                  Sm.CmParam<String>(ref cmDtlS, "@Space", "-------------------------");
                  Sm.CmParam<String>(ref cmDtlS, "@DocNo", TxtDocNo.Text);
                  var drDtlS = cmDtlS.ExecuteReader();
                  var cDtlS = Sm.GetOrdinal(drDtlS, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",

                         //6
                         "LastupDt"
                        });
                  if (drDtlS.HasRows)
                  {
                      while (drDtlS.Read())
                      {
                          lDtlS.Add(new IPSignIMS()
                          {
                              Signature = Sm.DrStr(drDtlS, cDtlS[0]),
                              UserName = Sm.DrStr(drDtlS, cDtlS[1]),
                              PosName = Sm.DrStr(drDtlS, cDtlS[2]),
                              Space = Sm.DrStr(drDtlS, cDtlS[3]),
                              DNo = Sm.DrStr(drDtlS, cDtlS[4]),
                              Title = Sm.DrStr(drDtlS, cDtlS[5]),
                              LastUpDt = Sm.DrStr(drDtlS, cDtlS[6])
                          });
                      }
                  }
                  drDtlS.Close();
              }
              myLists.Add(lDtlS);

              //Disetujui Oleh
              var cmDtlS2 = new MySqlCommand();

              var SQLDtlS2 = new StringBuilder();
              using (var cnDtlS2 = new MySqlConnection(Gv.ConnectionString))
              {
                  cnDtlS2.Open();
                  cmDtlS2.Connection = cnDtlS2;

                  SQLDtlS2.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                  SQLDtlS2.AppendLine(" T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt, T1.Remark  ");
                  SQLDtlS2.AppendLine("From (  ");
                  SQLDtlS2.AppendLine("   Select Distinct  ");
                  SQLDtlS2.AppendLine("   B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName,  ");
                  SQLDtlS2.AppendLine("   B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt, 'Disetujui' As Remark  ");
                  SQLDtlS2.AppendLine("   From tblincomingpaymenthdr A  ");
                  SQLDtlS2.AppendLine("   Inner Join TblDocApproval B On B.DocType='IncomingPayment' And A.DocNo=B.DocNo   ");
                  SQLDtlS2.AppendLine("   Inner Join TblUser C On B.UserCode=C.UserCode  ");
                  SQLDtlS2.AppendLine("   Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'IncomingPayment' ");
                  SQLDtlS2.AppendLine("   Left Join TblGroup E On C.GrpCode=E.GrpCode  ");
                  SQLDtlS2.AppendLine("     Where A.CancelInd='N' And A.DocNo=@DocNo ");
                  SQLDtlS2.AppendLine(") T1  ");
                  SQLDtlS2.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode  ");
                  SQLDtlS2.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode  ");
                  SQLDtlS2.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature'  ");
                  //SQLDtlS2.AppendLine("Where T1.Level >= 2  ");
                  SQLDtlS2.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName  ");
                  SQLDtlS2.AppendLine("Order By T1.Level ");

                  cmDtlS2.CommandText = SQLDtlS2.ToString();
                  Sm.CmParam<String>(ref cmDtlS2, "@Space", "-------------------------");
                  Sm.CmParam<String>(ref cmDtlS2, "@DocNo", TxtDocNo.Text);
                  var drDtlS2 = cmDtlS2.ExecuteReader();
                  var cDtlS2 = Sm.GetOrdinal(drDtlS2, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                  if (drDtlS2.HasRows)
                  {
                      while (drDtlS2.Read())
                      {

                          lDtlS2.Add(new IPSignIMS2()
                          {
                              Signature = Sm.DrStr(drDtlS2, cDtlS2[0]),
                              UserName = Sm.DrStr(drDtlS2, cDtlS2[1]),
                              PosName = Sm.DrStr(drDtlS2, cDtlS2[2]),
                              Space = Sm.DrStr(drDtlS2, cDtlS2[3]),
                              DNo = Sm.DrStr(drDtlS2, cDtlS2[4]),
                              Title = Sm.DrStr(drDtlS2, cDtlS2[5]),
                              LastUpDt = Sm.DrStr(drDtlS2, cDtlS2[6])
                          });
                      }
                  }
                  drDtlS2.Close();
              }
              myLists.Add(lDtlS2);

             
              #endregion

              if (Sm.GetParameter("DocTitle") == "KIM") 
                  Sm.PrintReport("IncomingPaymentKIM", myLists, TableName, false);
              else if (Sm.GetParameter("DocTitle") == "IMS")
                  Sm.PrintReport("IncomingPaymentIMS", myLists, TableName, false);
              else if (Sm.GetParameter("DocTitle") == "KIM")
                  Sm.PrintReport("ReceiptKIM", myLists, TableName, false);
              else 
                  Sm.PrintReport("IncomingPayment", myLists, TableName, false );
              
          }                

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ComputeCOAAmt(ref iGrid Grd, byte Type)
        {            
            // Type=1 -> COA List
            // Type=2 -> Advance Payment Refund
            try
            {
                if (Type == 1)
                {
                    if (mIncomingPaymentCOAAmtCalculationMethod == "1" || mIncomingPaymentCOAAmtCalculationMethod == "3")
                        ComputeCOAAmt1(ref Grd);
                    else if (mIncomingPaymentCOAAmtCalculationMethod == "5")
                        ComputeCOAAmt3(ref Grd);
                    else
                        ComputeCOAAmt2(ref Grd, ref TxtCOAAmt);
                }
                if (Type == 2)
                    ComputeCOAAmt2(ref Grd, ref TxtCOAAmt2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ComputeCOAAmt1(ref iGrid Grd)
        {
            decimal COAAmt = 0m;
            var SQL = new StringBuilder();

            if (mIncomingPaymentCOAAmtCalculationMethod == "1")
                SQL.AppendLine("Select Concat(C.ParValue, A.CtCode) As AcNo ");
            else
                SQL.AppendLine("Select C.COAAcNo As AcNo ");
            SQL.AppendLine("From TblCustomer A ");
            SQL.AppendLine("Inner Join TblCustomerCategory B On A.CtCtCode=B.CtCtCode ");
            if (mIncomingPaymentCOAAmtCalculationMethod == "1")
                SQL.AppendLine("Left Join TblParameter C On C.ParCode='CustomerAcNoAR' ");
            else
                SQL.AppendLine("Left Join TblBankAccount C On C.BankAcCode = @BankAcCode ");
            SQL.AppendLine("Where A.CtCtCode is Not Null ");
            SQL.AppendLine("And A.CtCode=@CtCode Limit 1; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            var AcNo = Sm.GetValue(cm);

            cm.CommandText = "Select AcType From TblCOA Where AcNo=@AcNo;";
            Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
            string AcType = Sm.GetValue(cm);

            for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
            {
                if (AcNo.Length>0 && Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd, Row, 1)))
                {
                    if (Sm.GetGrdDec(Grd, Row, 3) != 0)
                    {
                        if (AcType == "D")
                            COAAmt += Sm.GetGrdDec(Grd, Row, 3);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd, Row, 3);
                    }
                    if (Sm.GetGrdDec(Grd, Row, 4) != 0)
                    {
                        if (AcType == "C")
                            COAAmt += Sm.GetGrdDec(Grd, Row, 4);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd, Row, 4);
                    }
                }
            }
            TxtCOAAmt.EditValue = Sm.FormatNum(COAAmt, 0);
        }

        private void ComputeCOAAmt2(ref iGrid Grd, ref TextEdit Txt)
        {
            decimal COAAmt = 0m;
            for (int r = 0; r < Grd.Rows.Count - 1; r++)
                if (Sm.GetGrdDec(Grd, r, 3) != 0) COAAmt += Sm.GetGrdDec(Grd, r, 3);
            Txt.EditValue = Sm.FormatNum(COAAmt, 0);
        }

        private void ComputeCOAAmt3(ref iGrid Grd)
        {
            decimal COAAmt = 0m;
            string AcType = string.Empty;

            for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
            {
                string AcNo = Sm.GetValue("Select Parvalue From TblParameter Where ParCode = 'CustomerAcNoAR'; ");

                if (AcNo.Length > 0 && Sm.CompareStr(AcNo, Sm.Left(Sm.GetGrdStr(Grd, Row, 1), AcNo.Length)))
                {
                    AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo = @Param;", Sm.GetGrdStr(Grd, Row, 1));
                    if (Sm.GetGrdDec(Grd, Row, 3) != 0)
                    {
                        if (AcType == "D")
                            COAAmt += Sm.GetGrdDec(Grd, Row, 3);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd, Row, 3);
                    }
                    if (Sm.GetGrdDec(Grd, Row, 4) != 0)
                    {
                        if (AcType == "C")
                            COAAmt += Sm.GetGrdDec(Grd, Row, 4);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd, Row, 4);
                    }
                }
            }
            TxtCOAAmt.EditValue = Sm.FormatNum(COAAmt, 0);
        }

        internal void ComputeGiroAmt()
        {
            decimal Amt = 0m;
            for (int r = 0; r < Grd4.Rows.Count - 1; r++)
                Amt += Sm.GetGrdDec(Grd4, r, 6);
            TxtGiroAmt.EditValue = Sm.FormatNum(Amt, 0);
            ComputeAmt();
        }

        private void SetLueProjectSystem1(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("    Select A.DocNo As Col1, Concat(A.AcCode, ' ', B.profitCenterName) As Col2  ");
                SQL.AppendLine("    From TblProject A  ");
                SQL.AppendLine("    Inner Join tblProfitCenter B On A.ProfitCenterCode = B.ProfitCenterCode  ");
                SQL.AppendLine("    Where ParentDocNo is null And Level = 1  ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Description", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueProjectSystem2(ref DXE.LookUpEdit Lue, string ProjectDocNo)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("    Select DocNo As Col1, Concat(AcCode, ' ', AcDesc) As Col2  ");
                SQL.AppendLine("    From TblProject  ");
                SQL.AppendLine("    Where ParentDocNo is not null And Level = 2 And ParentDocNo = '" + ProjectDocNo + "' ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Description", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueProjectSystem3(ref DXE.LookUpEdit Lue, string ProjectDocNo)
        {
            try
            {
                string AcCodeParent = Sm.GetValue("Select AcCode From TblProject Where Docno = '" + ProjectDocNo + "' ");
                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo As Col1, concat(AcCode, ' ', AcDesc) As Col2 From TblProject ");
                SQL.AppendLine("Where DocNo Not In (  ");
                SQL.AppendLine("    Select ParentDocNo From TblProject  ");
                SQL.AppendLine("    Where ParentDocNo Is Not Null  ");
                SQL.AppendLine("    )  ");
                SQL.AppendLine("And AcCode like '" + AcCodeParent + "%'  ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Description", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueAdvanceChargeCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsIncomingPaymentUseAdvanceCharge)
            {
                Sm.RefreshLookUpEdit(LueAdvanceChargeCode, new Sm.RefreshLue1(SetLueAdvanceChargeCode));
                AddAdvanceCharge(Sm.GetLue(LueAdvanceChargeCode), "LueAdvanceChargeCode", Grd3.Rows.Count - 1, LueAdvanceChargeCode);
                AddCustomerAcNoAR("CustomerAcNoAR", Grd3.Rows.Count - 1);
                ComputeCOAAmt(ref Grd3, 1);
                if (mIsIncomingPaymentAmtUseCOAAmt) ComputeAmt();
            }
        }

        private void LueAdvanceChargeCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsIncomingPaymentUseAdvanceCharge)
            {
                Sm.RefreshLookUpEdit(LueAdvanceChargeCode2, new Sm.RefreshLue1(SetLueAdvanceChargeCode));
                AddAdvanceCharge(Sm.GetLue(LueAdvanceChargeCode2), "LueAdvanceChargeCode2", Grd3.Rows.Count - 1, LueAdvanceChargeCode2);
                AddCustomerAcNoAR("CustomerAcNoAR", Grd3.Rows.Count - 1);
                ComputeCOAAmt(ref Grd3, 1);
                if (mIsIncomingPaymentAmtUseCOAAmt) ComputeAmt();
            }
        }

        private void LueAdvanceChargeCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsIncomingPaymentUseAdvanceCharge)
            {
                Sm.RefreshLookUpEdit(LueAdvanceChargeCode3, new Sm.RefreshLue1(SetLueAdvanceChargeCode));
                AddAdvanceCharge(Sm.GetLue(LueAdvanceChargeCode3), "LueAdvanceChargeCode3", Grd3.Rows.Count - 1, LueAdvanceChargeCode3);
                AddCustomerAcNoAR("CustomerAcNoAR", Grd3.Rows.Count - 1);
                ComputeCOAAmt(ref Grd3, 1);
                if (mIsIncomingPaymentAmtUseCOAAmt) ComputeAmt();

            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnCtCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmIncomingPaymentDlg4(this));
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
          HideInfoInGrd();
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
          if (Sm.GetLue(LueFontSize).Length != 0)
          {
              var TheFont = new Font(
                  Grd1.Font.FontFamily.Name.ToString(),
                  int.Parse(Sm.GetLue(LueFontSize))
                  );
              Grd1.Font = TheFont;
              Grd2.Font = TheFont;
          }
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo
                });

                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode }, true);

                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(SetLueCtCode), "");

                if (Sm.GetLue(LueCtCode).Length != 0)
                {
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode }, false);
                    SetLuePaidToBankCode(ref LuePaidToBankCode, Sm.GetLue(LueCtCode), "2");
                }

                TxtCOAAmt.EditValue = Sm.FormatNum(0m, 0);
                ClearGrd();
                ComputeGiroAmt();
                ComputeAmt();
                ComputeAmtSI();
                
                if (mIsIncomingPaymentUseDeposit) GetDepositSummary();
                if (mIsIncomingPaymentUseCustomerCategory && Sm.GetLue(LueCtCode).Length != 0) 
                    Sm.SetLue(LueCtCtCode, Sm.GetValue("Select CtCtCode From TblCustomer Where CtCode=@Param", Sm.GetLue(LueCtCode)));
            }

        }

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(Sl.SetLueAcType));
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDt });

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, true);
                    Sm.SetControlReadOnly(DteDueDt, true);
                    return;
                }

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, false);
                    Sm.SetControlReadOnly(DteDueDt, false);
                    return;
                }

                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
           
        }

        private void TxtGiroNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGiroNo);
        }

        private void TxtRateAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                FormatNumTxt(TxtRateAmt);
                ComputeAmt2();
            }
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                LueCurCode2.EditValue = null;
                try
                {
                    if (mBankAccountFormat == "1" || mBankAccountFormat == "2")
                        Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue2(SetLueBankAcCode), string.Empty);
                    else
                        Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
                    if (Sm.GetLue(LueBankAcCode).Length > 0)
                    {
                        SetCurCode(Sm.GetLue(LueBankAcCode));
                        Sm.SetLue(LueBankCode, Sm.GetValue("Select BankCode From TblBankAccount Where BankAcCode =@Param ", Sm.GetLue(LueBankAcCode)));
                    }
                    if (mIncomingPaymentCOAAmtCalculationMethod == "3" && mIsIncomingPaymentAmtUseCOAAmt)
                    {
                        ComputeCOAAmt(ref Grd3, 1);
                        ComputeAmt();
                    }
                    
                }
                catch (Exception Exc)
                {
                    Sm.StdMsg(mMsgType.Warning, Exc.Message);
                }
            }
        }

        private void TxtPaymentUser_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaymentUser);
        }

        private void LuePaidToBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePaidToBankCode, new Sm.RefreshLue3(SetLuePaidToBankCode), Sm.GetLue(LueCtCode), "2");
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo
                });
                if (Sm.GetLue(LuePaidToBankCode).Length != 0)
                    ShowPaidToBankCodeInfo(
                        Sm.GetLue(LueCtCode),
                        LuePaidToBankCode.GetColumnValue("Col4").ToString()
                        );
            }
        }

        private void TxtPaidToBankBranch_Validated(object sender, EventArgs e)
        {

        }

        private void TxtPaidToBankAcName_Validated(object sender, EventArgs e)
        {

        }

        private void TxtPaidToBankAcNo_Validated(object sender, EventArgs e)
        {

        }

        private void MeeVoucherRequestSummaryDesc_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.TxtTrim(MeeVoucherRequestSummaryDesc);
                ChkMeeVoucherRequestSummaryInd.Checked =
                    (MeeVoucherRequestSummaryDesc.Text.Length == 0) ? false : true;
            }
        }

        private void ChkMeeVoucherRequestSummaryInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!ChkMeeVoucherRequestSummaryInd.Checked) MeeVoucherRequestSummaryDesc.EditValue = null;
            }
        }

        private void LueCurCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode2, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
                if (mIsUseDailyCurrencyRate) ComputeCurrencyRate();
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.MeeTrim(MeeCancelReason);
                ChkCancelInd.Checked = (MeeCancelReason.Text.Length > 0);
            }
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ChkCancelInd.Checked)
                {
                    if (MeeCancelReason.Text.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Reason for cancellation is empty.");
                        ChkCancelInd.Checked = false;
                    }
                }
                else
                    MeeCancelReason.EditValue = null;
            }
        }

        private void LueProjectDocNo1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProjectDocNo1, new Sm.RefreshLue1(SetLueProjectSystem1));
                if (Sm.GetLue(LueProjectDocNo1).Length > 0)
                    SetLueProjectSystem2(ref LueProjectDocNo2, Sm.GetLue(LueProjectDocNo1));
            }
        }

        private void LueProjectDocNo2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (Sm.CompareStr(Sm.GetLue(LueProjectDocNo2), "<Refresh>")) LueProjectDocNo2.EditValue = null;
                if (BtnSave.Enabled && Sm.GetLue(LueProjectDocNo1).Length > 0)
                {
                    Sm.RefreshLookUpEdit(LueProjectDocNo1, new Sm.RefreshLue2(SetLueProjectSystem2), Sm.GetLue(LueProjectDocNo1));
                    SetLueProjectSystem3(ref LueProjectDocNo3, Sm.GetLue(LueProjectDocNo2));
                }
            }
        }

        private void LueProjectDocNo3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && Sm.GetLue(LueProjectDocNo2).Length > 0)
            {
                Sm.RefreshLookUpEdit(LueProjectDocNo3, new Sm.RefreshLue2(SetLueProjectSystem3), Sm.GetLue(LueProjectDocNo2));
            }
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsUseDailyCurrencyRate) ComputeCurrencyRate();
            }
        }

        private void TxtDepositAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDepositAmt, 0);
                ComputeAmt();
            }
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue1(Sl.SetLueCtCtCode));
            if (mIsCustomerComboBasedOnCategory)
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueCtCode });
                SetLueCtCode(ref LueCtCode, "");
            }
        }

        #endregion

        #region Grid Event

        #region Grd1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
          if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer"))
          {
              e.DoDefault = false;
              if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmIncomingPaymentDlg(this, Sm.GetLue(LueCtCode), mMInd, mActivePeriod));
          }

          if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
          {
              e.DoDefault = false;

              var DocTypeSLI = Sm.GetValue("Select DocType From TblSalesInvoiceDtl Where DocNo=@Param Limit 1;", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
              if (DocTypeSLI == "1")
              {
                  var f1 = new FrmSalesInvoice("***");
                  f1.Tag = "***";
                  f1.WindowState = FormWindowState.Normal;
                  f1.StartPosition = FormStartPosition.CenterScreen;
                  f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                  f1.ShowDialog();
              }
              else
              {
                  var f1 = new FrmSalesInvoice3("***");
                  f1.Tag = "***";
                  f1.WindowState = FormWindowState.Normal;
                  f1.StartPosition = FormStartPosition.CenterScreen;
                  f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                  f1.ShowDialog();
              }

              //if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
              //{
              //    var f1 = new FrmSalesInvoice(mMenuCode);
              //    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
              //    f1.Tag = mMenuCode;
              //    f1.WindowState = FormWindowState.Normal;
              //    f1.StartPosition = FormStartPosition.CenterScreen;
              //    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
              //    f1.ShowDialog();
              //}

              //if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "3"))
              //{
              //    //var f1 = new FrmSalesInvoice4(mMenuCode);
              //    //f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
              //    //f1.Tag = mMenuCode;
              //    //f1.WindowState = FormWindowState.Normal;
              //    //f1.StartPosition = FormStartPosition.CenterScreen;
              //    //f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
              //    //f1.ShowDialog();
              //}

              //if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "5"))
              //{
              //    var f1 = new FrmSalesInvoice5(mMenuCode);
              //    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
              //    f1.Tag = mMenuCode;
              //    f1.WindowState = FormWindowState.Normal;
              //    f1.StartPosition = FormStartPosition.CenterScreen;
              //    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
              //    f1.ShowDialog();
              //}
          }

          if (BtnSave.Enabled && 
              TxtDocNo.Text.Length == 0 && 
              Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length>0 && 
              Sm.IsGrdColSelected(new int[] { 9, 11 }, e.ColIndex))
                  Sm.GrdRequestEdit(Grd1, e.RowIndex);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
          if (TxtDocNo.Text.Length == 0)
          {
              Sm.GrdRemoveRow(Grd1, e, BtnSave);
              ComputeGiroAmt();
              ComputeAmt();
              ComputeAmtSI();
          }
          Sm.GrdEnter(Grd1, e);
          Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
          Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 9 }, e);
          Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 11 }, e);
          if (e.ColIndex == 9)
          {
              ComputeGiroAmt();
              ComputeAmt();
              ComputeAmtSI();
          }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
          if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer"))
              Sm.FormShowDialog(new FrmIncomingPaymentDlg(this, Sm.GetLue(LueCtCode), mMInd, mActivePeriod));

          if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
          {
              if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
              {
                  var DocTypeSLI = Sm.GetValue("Select DocType From TblSalesInvoiceDtl Where DocNo=@Param Limit 1;", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
                  if (DocTypeSLI == "1")
                  {
                      var f1 = new FrmSalesInvoice("***");
                      f1.Tag = "***";
                      f1.WindowState = FormWindowState.Normal;
                      f1.StartPosition = FormStartPosition.CenterScreen;
                      f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                      f1.ShowDialog();
                  }
                  else
                  {
                      var f1 = new FrmSalesInvoice3("***");
                      f1.Tag = "***";
                      f1.WindowState = FormWindowState.Normal;
                      f1.StartPosition = FormStartPosition.CenterScreen;
                      f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                      f1.ShowDialog();
                  }
              }

              //if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
              //{
              //    var f1 = new FrmSalesInvoice(mMenuCode);
              //    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
              //    f1.Tag = mMenuCode;
              //    f1.WindowState = FormWindowState.Normal;
              //    f1.StartPosition = FormStartPosition.CenterScreen;
              //    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
              //    f1.ShowDialog();
              //}
              //if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "3"))
              //{
              //    //var f1 = new FrmSalesInvoice4(mMenuCode);
              //    //f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
              //    //f1.Tag = mMenuCode;
              //    //f1.WindowState = FormWindowState.Normal;
              //    //f1.StartPosition = FormStartPosition.CenterScreen;
              //    //f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
              //    //f1.ShowDialog();
              //}
              //if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "5"))
              //{
              //    var f1 = new FrmSalesInvoice5(mMenuCode);
              //    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
              //    f1.Tag = mMenuCode;
              //    f1.WindowState = FormWindowState.Normal;
              //    f1.StartPosition = FormStartPosition.CenterScreen;
              //    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
              //    f1.ShowDialog();
              //}
          }
        }

        #endregion

        #region Grd3

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
            {
                ComputeCOAAmt(ref Grd3, 1);
                if (mIsIncomingPaymentAmtUseCOAAmt) ComputeAmt();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
            {
                ComputeCOAAmt(ref Grd3, 1);
                if (mIsIncomingPaymentAmtUseCOAAmt) ComputeAmt();
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmIncomingPaymentDlg2(this, Grd3, true, Sm.GetLue(LueCtCode), Sm.GetLue(LueBankAcCode)));
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmIncomingPaymentDlg2(this, Grd3, true, Sm.GetLue(LueCtCode), Sm.GetLue(LueBankAcCode)));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeCOAAmt(ref Grd3, 1);
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grd4

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmIncomingPaymentDlg3(this, Sm.GetLue(LueCtCode)));
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeGiroAmt();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer")) 
                Sm.FormShowDialog(new FrmIncomingPaymentDlg3(this, Sm.GetLue(LueCtCode)));
        }

        #endregion 

        #region Grd5

        private void Grd5_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd5, e.RowIndex, 1).Length != 0)
                ComputeCOAAmt(ref Grd5, 2);

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd5, e.RowIndex, 1).Length != 0)
                ComputeCOAAmt(ref Grd5, 2);
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmIncomingPaymentDlg2(this, Grd5, false, string.Empty, string.Empty));
            }
        }

        private void Grd5_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmIncomingPaymentDlg2(this, Grd5, false, string.Empty, string.Empty));
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd5, e, BtnSave);
            ComputeCOAAmt(ref Grd5, 1);
            Sm.GrdEnter(Grd5, e);
            Sm.GrdTabInLastCell(Grd5, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion 

        #endregion 

        #region Reporting Class

        class IpHdr
        {
            public string CompanyLogo { set; get; }

            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyLongAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }

            public string DocNoVC { get; set; }
            public string DocDt { get; set; }
            public string BankAcc { get; set; }
            public string PaymentType { get; set; }
            public string GiroBankName { get; set; }

            public string CtName { get; set; }
            public string DocNoVR { get; set; }
            public string DocDtVR { get; set; }
            public string Remark { get; set; }
            public string CurCode { get; set; }

            public decimal Amt { get; set; }
            public string DocNo { get; set; }
            public string Address { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public string DocDtIP { get; set; }
            public string PrintBy { get; set; }

            public string RemarkVC { get; set; }
            public string InvoiceDocNoKIM { get; set; }
            public string Project { get; set; }
        }

        class IpDtl
        {
            public string Description { get; set; }
            public string Description2 { get; set; }
            public string InvoiceDocNo { get; set; }
            public decimal Amt { get; set; }
        }

        class IpDtl2
        {
            public string Description { get; set; }
            public decimal Amt { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
        }

        private class Employee
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
        }

        private class IpDtlKIM
        {
            public string Description { get; set; }
            public string InvoiceDocNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class IpSignKIM
        {
            public string AckBy1 { get; set; }
            public string AckPos1 { get; set; }
            public string AckSign1 { get; set; }
            public string AckBy2 { get; set; }
            public string AckPos2 { get; set; }
            public string AckSign2 { get; set; }
            public string PrepBy { get; set; }
            public string PrepPos { get; set; }
            public string PrepSign { get; set; }
        }

        private class IPSignIMS
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class IPSignIMS2
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class Rate
        {
            public decimal Downpayment { get; set; }
            public decimal Amt { get; set; }
            public string CurCode { get; set; }
            public decimal ExcRate { get; set; }
            public string CtCode { get; set; }
        }
        
        #endregion

       
    }
}