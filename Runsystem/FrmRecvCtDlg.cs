﻿#region Update
/*
    09/08/2018 [TKG] tambah filter status do
    27/11/2019 [WED/IMS] tambah kolom projectcode, projectname, customer po# berdasarkan parameter IsBOMShowSpecifications
    30/01/2020 [TKG/IMS] munculkan kolom ProjectCode, ProjectName, Customer PO#
    25/02/2020 [WED/SIER] ambil dari DO yg processind = 'F'
    10/06/2020 [VIN/IMS] tambah kolom item local code 
    16/06/2020 [VIN/IMS] tambah kolom customer  
    08/07/2020 [HAR/IMS] remark so contract ambil dari detail item
    13/07/2020 [IBL/KSM] menyambungkan dengan DO to customer based on DRSC
    06/12/2020 [WED/IMS] tambah tarik No
    19/07/2021 [ICA/IMS] hanya menampilkan DOCtDR berdasarkan parameter IsRecvCtBasedOnDOCtDROnly
    17/01/2022 [WED/GSS] data DO yang tarik dari Sales Contract dipagarin parameter IsSalesContractEnabled
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvCtDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRecvCt mFrmParent;
        private string mSQL = string.Empty, mWhsCode = string.Empty, mCtCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRecvCtDlg(FrmRecvCt FrmParent, string WhsCode, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
            mCtCode = CtCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocNo, T.DNo, T.No, T.DocDt, T.ItCode, T.ItName, T.ItCodeInternal, T.ItCtName, T.BatchNo, T.Source, T.Lot, T.Bin, ");
            SQL.AppendLine("T.RemainingQty, T.RemainingQty2, T.RemainingQty3, ");
            SQL.AppendLine("T.InventoryUomCode, T.InventoryUomCode2, T.InventoryUomCode3, T.CurCode, T.UPrice, T.DOType, ");
            SQL.AppendLine("T.ProjectCode, T.ProjectName, T.PONo, T.Remark, ");
            SQL.AppendLine("T.SiteCode ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T5.Remark, T1.DocNo, T1.DNo, T1.No, T1.DocDt, T1.ItCode, T3.ItCodeInternal, T3.ItName, T4.ItCtName, T1.BatchNo, T1.Source, T1.Lot, T1.Bin, ");
            SQL.AppendLine("    T1.Qty-IfNull(T2.Qty, 0) As RemainingQty, T1.Qty2-IfNull(T2.Qty2, 0) As RemainingQty2, T1.Qty3-IfNull(T2.Qty3, 0) As RemainingQty3, ");
            SQL.AppendLine("    T3.InventoryUomCode, T3.InventoryUomCode2, T3.InventoryUomCode3, T1.CurCode, T1.UPrice, T1.DOtype, T1.ProjectCode, T1.ProjectName, T1.PONo, T1.SiteCode ");
            SQL.AppendLine("    From ( ");
            if (!mFrmParent.mIsRecvCtBasedOnDOCtDROnly)
            {
                SQL.AppendLine("        Select A.DocNo, B.DNo, A.DocDt, B.ItCode, B.BatchNo, B.Source, B.Lot, B.Bin, ");
                SQL.AppendLine("        B.Qty, B.Qty2, B.Qty3, A.CurCode, B.UPrice, '1' As DOType, ");
                SQL.AppendLine("        Null As ProjectCode, Null As ProjectName, Null As PONo, Null As SiteCode ");
                if (mFrmParent.mIsDOCtUseSOContract)
                    SQL.AppendLine("        , C.No ");
                else
                    SQL.AppendLine("        , Null As No ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("            And A.Status='A' And B.CancelInd='N' And A.CtCode=@CtCode ");
                SQL.AppendLine("            And A.ProcessInd = 'F' ");
                if (mFrmParent.mIsDOCtUseSOContract)
                {
                    SQL.AppendLine("        Left Join TblSOContractDtl C On A.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
                }

                SQL.AppendLine("        Union All ");
            }

            SQL.AppendLine("        Select A.DocNo, B.DNo, A.DocDt, B.ItCode, B.BatchNo, B.Source, B.Lot, B.Bin, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("        Null As CurCode, 0 As UPrice, '2' As DOType, ");
            SQL.AppendLine("        C.ProjectCode, C.ProjectName, C.PONo, C.SiteCode ");
            if (mFrmParent.mIsDOCtUseSOContract)
                SQL.AppendLine("        , D.No ");
            else
                SQL.AppendLine("        , Null As No ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select X1.DocNo, ");
            SQL.AppendLine("            Group_Concat(Distinct X5.ProjectCode) ProjectCode, ");
            SQL.AppendLine("            Group_Concat(Distinct X5.ProjectName) ProjectName, ");
            SQL.AppendLine("            Group_Concat(Distinct X2.PONo) PONo, Null As SiteCode, Null As No ");
            SQL.AppendLine("            From TblDRDtl X1 ");
            SQL.AppendLine("            Inner Join TblSOContractHdr X2 On X1.SODocNo = X2.DocNo ");
            SQL.AppendLine("            Inner Join TblBOQHdr X3 On X2.BOQDocNo = X3.DocNo ");
            SQL.AppendLine("            Inner Join TblLOPHdr X4 On X3.LOPDocNo = X4.DocNo ");
            SQL.AppendLine("            Inner Join TblProjectGroup X5 On X4.PGCode = X5.PGCode ");
            SQL.AppendLine("            Where X1.DocNo In ( ");
            SQL.AppendLine("                Select DRDocNo ");
            SQL.AppendLine("                From TblDOCt2Hdr ");
            SQL.AppendLine("                Where DRDocNo Is Not Null ");
            SQL.AppendLine("                And CtCode=@CtCode ");
            if (ChkDocDt.Checked)
                SQL.AppendLine("                And DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("            Group By X1.DocNo ");

            if (mFrmParent.mIsSalesContractEnabled)
            {
                SQL.AppendLine("            Union All ");

                SQL.AppendLine("            Select X1.DocNo, Null As ProjectCode, Null As ProjectName, Null As PONo, ");
                SQL.AppendLine("            X3.SiteCode, Null AS No ");
                SQL.AppendLine("            From TblDRDtl X1 ");
                SQL.AppendLine("            Inner Join TblSalesContract X2 on X1.SCDocNo = X2.DocNo ");
                SQL.AppendLine("            Inner Join TblSalesMemoHdr X3 on X2.SalesMemoDocNo = X3.DocNo ");
                SQL.AppendLine("            Where X1.DocNo In ( ");
                SQL.AppendLine("                Select DRDocNO ");
                SQL.AppendLine("                From TblDOCt2Hdr ");
                SQL.AppendLine("                Where DRDocNo Is Not Null ");
                SQL.AppendLine("                And CtCode = @CtCode ");
                if (ChkDocDt.Checked)
                    SQL.AppendLine("                And DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("            Group By X1.DocNo ");
            }

            SQL.AppendLine("        ) C On A.DRDocNo = C.DocNo ");
            if (mFrmParent.mIsDOCtUseSOContract)
            {
                SQL.AppendLine("        Left Join TblSOContractDtl D On B.SOContractDocNo = D.DocNo And B.SOContractDNo = D.DNo ");
            }
            SQL.AppendLine("        Where A.CtCode=@CtCode ");
            SQL.AppendLine("        And A.DrDocNo Is Not Null ");

            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select A.DocNo, B.DNo, A.DocDt, B.ItCode, B.BatchNo, B.Source, B.Lot, B.Bin, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("        Null As CurCode, 0 As UPrice, '2' As DOType, ");
            SQL.AppendLine("        Null As ProjectCode, Null As ProjectName, Null As PONo, Null As SiteCode, Null As No ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("        Where A.CtCode=@CtCode ");
            SQL.AppendLine("        And A.PLDocNo Is Not Null ");

            SQL.AppendLine("    ) T1 ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select B.DOCtDocNo, B.DOCtDNo, Sum(B.Qty) As Qty, Sum(B.Qty2) As Qty2, Sum(B.Qty3) As Qty3 ");
            SQL.AppendLine("        From TblRecvCtHdr A, TblRecvCtDtl B ");
            SQL.AppendLine("        Where A.DocNo=B.DocNo And B.CancelInd='N' And A.CtCode=@CtCode ");
            SQL.AppendLine("        Group By B.DOCtDocNo, B.DOCtDNo ");
            SQL.AppendLine("    ) T2 On T1.DocNo=T2.DOCtDocNo And  T1.DNo=T2.DOCtDNo ");
            SQL.AppendLine("    Inner Join TblItem T3 On T1.ItCode=T3.ItCode ");
            SQL.AppendLine("    Inner Join TblItemCategory T4 On T3.ItCtCode=T4.ItCtCode ");
            //SQL.AppendLine("LEFT JOIN  ");
            //SQL.AppendLine("    (SELECT E.Remark, B.DocNo, F.Dno ");
            //SQL.AppendLine("    from tbldoct2hdr B "); 
            //SQL.AppendLine("    left JOIN tbldrhdr C ON B.DRDocNo= C.DocNo ");
            //SQL.AppendLine("    left JOIN tbldrdtl D ON C.DocNo=D.DocNo ");
            //SQL.AppendLine("    left JOIN tblsocontracthdr E ON D.SODocNo=E.DocNo "); 
            //SQL.AppendLine("    LEFT JOIN tbldoct2dtl F ON B.DocNo=F.DocNo ");
            //SQL.AppendLine(")T5 ON T1.DocNo=T5.DocNo ");

            SQL.AppendLine("    LEFT JOIN ( ");
            SQL.AppendLine("        SELECT A.DocnO, A.ItCode, D.Remark From TblDOCt2Dtl A  ");
            SQL.AppendLine("        Inner Join tblDOCt2hdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblDrhdr C on B.DrDocnO = C.DocNo ");
            SQL.AppendLine("        Inner Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select A.DocNO, B.ItCode, B.Remark  ");
            SQL.AppendLine("            From TblDrDtl A ");
            SQL.AppendLine("            Inner Join tblSoContractDtl B On A.SODocNo = B.DocnO And A.SODno = B.Dno ");
            SQL.AppendLine("            Inner Join TblDrHdr C on A.DocNo = C.DocNo ");
	        SQL.AppendLine("            Where C.CancelInd = 'N' ");
            SQL.AppendLine("        )D On C.DocnO = D.DocnO And A.ItCode = D.ItCode ");
            SQL.AppendLine("    )T5 ON T1.DocNo=T5.DocNo ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Where (T.RemainingQty>0 Or T.RemainingQty2>0 Or T.RemainingQty3>0) ");
            SQL.AppendLine("And Locate(Concat('##', T.DocNo, T.DNo, '##'), @SelectedDOCt)<1 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 29;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Document#",
                        "DNo",
                        "Date",
                        "Item's Code", 

                        //6-10
                        "", 
                        "Item's Name", 
                        "Category",
                        "Batch#",
                        "Source",
                        
                        //11-15
                        "Lot",
                        "Bin", 
                        "Quantity",
                        "UoM",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Currency",
                        "Price",

                        //21-25
                        "DO Type",
                        "Project Code",
                        "Project Name",
                        "Customer PO#",
                        "Item's Local Code",

                        //26-28
                        "Remark",
                        "Site Code",
                        "No"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 6 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15, 17, 20 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 26, 27, 28 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 8, 10, 11, 12, 15, 16, 17, 18, 21, 25, 27 }, false);
            if (!mFrmParent.mIsRecvCtShowPriceInfo)
                Sm.GrdColInvisible(Grd1, new int[] { 19, 20 }, false);
            if (!mFrmParent.mIsSalesTransactionShowSOContractRemark)
                Sm.GrdColInvisible(Grd1, new int[] { 26 }, false);

            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24 });
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 8, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 "; ;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedDOCt", mFrmParent.GetSelectedDOCt());
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "T.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " /*Group By T.DNo*/ Order By T.DocDt, T.DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DNo", "DocDt", "ItCode", "ItName", "ItCtName", 
                        
                        //6-10
                        "BatchNo", "Source", "Lot", "Bin", "RemainingQty", 
                        
                        //11-15
                        "InventoryUomCode", "RemainingQty2", "InventoryUomCode2", "RemainingQty3", "InventoryUomCode3",

                        //16-20
                        "CurCode", "UPrice", "DOType", "ProjectCode", "ProjectName", 

                        //21-25
                        "PONo", "ItCodeInternal", "Remark", "SiteCode", "No"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 12);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 14);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 15);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 16);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 20, 17);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 18);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 19);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 23, 20);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 21);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 25, 22);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 26, 23);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 27, 24);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 28, 25);

                    }, true, false, false, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 22);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 23);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 24);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 32, Grd1, Row2, 25);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 26);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 35, Grd1, Row2, 27);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 36, Grd1, Row2, 28);

                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 15, 18, 21, 25 });
                       
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 24, 25 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 DO's item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 4), Sm.GetGrdStr(Grd1, Row, 2)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 5), Sm.GetGrdStr(Grd1, Row, 3)))
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
