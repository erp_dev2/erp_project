﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentItemDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmInvestmentItem mFrmParent;
        private string mSQL;

        #endregion

        #region Constructor

        public FrmInvestmentItemDlg(FrmInvestmentItem FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Investment Item's Source";
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetSQL();
            SetGrd();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        "No.",

                        //1-5
                        "Item Code", 
                        "Item Name",
                        "Item Local Code",
                        "Active",
                        "Category",

                        //6-8
                        "Sub Category",
                        "Foreign Name",
                        "ItemInd" // dia dari ItemRequest atau bukan. 1 = dari Item Request, 0 = bukan dari Item Request
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  Select A.InvestmentCode, A.InvestmentName, A.InvestmentCodeInternal, A.ForeignName, A.ActInd, B.InvestmentCtName, ");
            SQL.AppendLine("  A.ItScCode, '0' As ItemInd ");
            SQL.AppendLine("  From TblInvestment A ");
            SQL.AppendLine("  Inner Join TblInvestmentCategory B On A.InvestmentCtCode=B.InvestmentCtCode ");
            SQL.AppendLine(")T ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtItem.Text, new string[] { "T.InvestmentCode", "T.InvestmentName", "T.ForeignName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T.InvestmentCode, T.InvestmentName ",
                        new string[]
                        {
                             //0
                            "InvestmentCode", 
                                
                            //1-5
                            "InvestmentName", "InvestmentCodeInternal", "ActInd", "ItCtName", "ItScCode",

                            //6-7
                            "ForeignName", "ItemInd"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        }, true, false, false, true
                        );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 2))
            {
                mFrmParent.ClearData();
                mFrmParent.TxtSource.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.ShowData2(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                if (Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8) == "1")
                {
                    mFrmParent.TxtInvestmentName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    mFrmParent.TxtForeignName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                    mFrmParent.TxtInvestmentCodeInternal.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                }
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmInvestmentItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mInvestmentCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 8), "1"))
                {
                    var f1 = new FrmInvestmentItem(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mInvestmentCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 8), "2"))
                {
                    var f2 = new FrmFormula(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f2.ShowDialog();
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 8), "3"))
                {
                    var f3 = new FrmEmployee(mFrmParent.mMenuCode);
                    f3.Tag = mFrmParent.mMenuCode;
                    f3.WindowState = FormWindowState.Normal;
                    f3.StartPosition = FormStartPosition.CenterScreen;
                    f3.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f3.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmInvestmentItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mInvestmentCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 8), "1"))
                {
                    var f1 = new FrmInvestmentItem(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mInvestmentCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 8), "2"))
                {
                    var f2 = new FrmFormula(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f2.ShowDialog();
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 8), "3"))
                {
                    var f3 = new FrmEmployee(mFrmParent.mMenuCode);
                    f3.Tag = mFrmParent.mMenuCode;
                    f3.WindowState = FormWindowState.Normal;
                    f3.StartPosition = FormStartPosition.CenterScreen;
                    f3.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f3.ShowDialog();
                }
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event
        private void ChkItem_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItem_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
