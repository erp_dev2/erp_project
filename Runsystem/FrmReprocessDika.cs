﻿#region Update
/*
    23/09/2022 [HAR] New Apps
    08/11/2022 [HAR/PHT] bug fixing journal, dan bank account
    14/11/2022 [HAR/PHT] BUG : urutan costcenter RC masih salah
    28/11/2022 [HAR/PHT] Feedback :  tambahan parameter mSITaxRoundingDown untuk pembulatan 
    05/12/2022 [HAR/PHT] BUG :  duplicate saat pembentukan VR
    13/12/2022 [HAR/PHT] proses remark jurnal dan tax disesuaikan
    09/01/2023 [HAR/PHT] penambahan PIC Code dkk
    11/01/2023 [HAR/PHT] ubah primary key dika dtl
    11/01/2023 [WED/PHT] penomoran journal berdasarkan parameter JournalDocNoFormat
    13/01/2023 [WED/PHT] bug fix ketika parameter JournalDocNoFormat value nya default
    16/01/2023 [HAR/PHT] DO to Customer (ship addres, item), SI(type code), VR(PIC)
    19/01/2023 [TKG/PHT] bug journal interoffice harus berdasarkan profit center, dll.
    24/01/2023 [HAR/PHT] tambah validasi pengecekan whs yang double
    31/01/2023 [HAR/PHT] rate di incoming payment di set 1 agar muncul nilai incomingnya, dengan catatan kolom amt di data kiriman harus ke isi
    02/02/2023 [HAR/PHT] tambah pic regional
    10/02/2023 [HAR/PHT] tambah GRID untuk emmilah receipt yang akan di proses
    14/03/2023 [HAR/PHT] nilai tax sebelum dr SLI detail menjadi SLI header 
    16/03/2023 [HAR/PHT] bug fixing : nomor voucher document harusnya ambil dari year
    21/03/2023 [HAR/PHT] Kodefikasi RAK Pajak DIKA, menangkap transaksi BeaMaterai  
    05/04/2023 [HAR/PHT] Reprocess DIKA untuk tax 0 tidak membentuk RAK
    11/04/2023 [HAR/PHT] BUG : sales invoice yang tax amountnya 0 masih terbentuk tax nya (SaveSIPOTP)
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmReprocessDika : RunSystem.FrmBase12
    {
        #region Field, Property

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private string
            mServer1 = "127.0.0.1",
            mDatabase1 = "kim",
            mGenerateCustomerCOAFormat = string.Empty,
            mBankAcCodeForVRIntegrasi = string.Empty,
            mMainCurCode = string.Empty,
            mSLITaxCalculationFormat = string.Empty,
            mDocNoFormat = string.Empty,
            mBankAcCodeForFinet = string.Empty,
            mPHTHostIntegrasi = string.Empty,
            mPHTDbIntegrasi = string.Empty,
            mCCCodePPNPOTP = string.Empty,
            mAcNoForRAK = string.Empty,
            mJournalDocNoFormat = string.Empty
            ;
        private bool mIsDOCtAmtRounded = false,
            mIsAcNoForSaleUseItemCategory,
            mSITaxRoundingDown;
        private int mCountdata = 0;

        #endregion

        #region Constructor

        public FrmReprocessDika(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Method
        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 6;
            
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "ReceiptNo",
                        "Last Process By",
                        "Last Process Date",
                        "Last Process Time"
                    },
                    new int[] { 50, 20, 150, 150, 150, 150}
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatTime(Grd1, new int[] { 5 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5 });
            #endregion

        }


        private void ShowDikaHdr(string DocDt)
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Date") 
                ) return;

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@Docdt", DocDt);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.ReceiptNo, B.createby, B.createdt ");
            SQL.AppendLine("From "+mPHTDbIntegrasi+".TblPayment2 A ");
            SQL.AppendLine("Left Join TblDikaHdr  B On A.DocDt = B.DocDt And A.ReceiptNo = B.Receiptno ");
            SQL.AppendLine("Where A.DocDt=@Docdt Order By A.Receiptno;");
            //untuk list data receipt yang bermasalah
            //SQL.AppendLine("Where A.Receiptno in (Select Receiptno From areceiptno)");


            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { "ReceiptNo", "createby", "createdt",  },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("T", Grd, dr, c, Row, 5, 2);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public static string GetNewJournalDocNoTemp(string DocDt, int Value, string DocNoFirst)
        {
            string
                MthYr = string.Concat(DocDt.Substring(4, 2), "/", DocDt.Substring(2, 2)),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Journal'"),
                JournalDocSeqNo = Sm.GetParameter("JournalDocSeqNo")
                ;
            if (JournalDocSeqNo.Length == 0) JournalDocSeqNo = "4";

            var SQL = new StringBuilder();

            SQL.Append("(Select Concat( ");

            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat( ");
            SQL.Append("   Repeat('0', " + JournalDocSeqNo + "), ");
            SQL.Append("   Convert(DocNoTemp+" + Value + ", Char) ");
            SQL.Append("   ), " + JournalDocSeqNo + ") ");
            SQL.Append("   From ( ");
            SQL.Append("       Select Convert(Left('" + DocNoFirst + "', Locate('/', '" + DocNoFirst + "')-1), Decimal) As DocNoTemp ");
            SQL.Append("    ) As Temp ");
            SQL.Append("   ), ");
            SQL.Append("   Right(Concat( ");
            SQL.Append("        Repeat('0', " + JournalDocSeqNo + "), '" + Value + "'), " + JournalDocSeqNo + ")) ");

            SQL.Append(", '/', '");
            SQL.Append(DocTitle);
            SQL.Append("', '/', '");
            SQL.Append(DocAbbr);
            SQL.Append("', '/', '" + MthYr + "'");
            SQL.Append(")) ");

            return SQL.ToString();
        }

        public static string GetNewJournalDocNo(string DocDt, int Value)
        {
            string
                MthYr = string.Concat(DocDt.Substring(4, 2), "/", DocDt.Substring(2, 2)),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Journal'"),
                JournalDocSeqNo = Sm.GetParameter("JournalDocSeqNo")
                ;
            if (JournalDocSeqNo.Length == 0) JournalDocSeqNo = "4";

            var SQL = new StringBuilder();

            SQL.Append("(Select Concat( ");

            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat( ");
            SQL.Append("   Repeat('0', " + JournalDocSeqNo + "), ");
            SQL.Append("   Convert(DocNoTemp+" + Value + ", Char) ");
            SQL.Append("   ), " + JournalDocSeqNo + ") ");
            SQL.Append("   From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNoTemp ");
            SQL.Append("       From TblJournalHdr ");
            SQL.Append("       Where Right(DocNo, 5)='" + MthYr + "'");
            SQL.Append("       Order By DocNo Desc ");
            SQL.Append("       Limit 1 ");
            SQL.Append("    ) As Temp ");
            SQL.Append("   ), ");
            SQL.Append("   Right(Concat( ");
            SQL.Append("        Repeat('0', " + JournalDocSeqNo + "), '" + Value + "'), " + JournalDocSeqNo + ")) ");

            SQL.Append(", '/', '");
            SQL.Append(DocTitle);
            SQL.Append("', '/', '");
            SQL.Append(DocAbbr);
            SQL.Append("', '/', '" + MthYr + "'");
            SQL.Append(")) ");

            return SQL.ToString();
        }

        private static string GenerateVoucherDocNo(string DteDocDt, string DocType, string JournalDocNoFormat, string code1, string profitCenterCode)
        {
            string
                Yr = DteDocDt.Substring(2, 2),
                Mth = DteDocDt.Substring(4, 2),
                Yr2 = DteDocDt.Substring(0, 4),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'");
            string
                DocAbbr = DocType == "VR" ? Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'")
                : Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Voucher'");

            string DocDt = Yr + Mth + "01";
            string DocDt2 = Yr2 + Mth + "01";


            bool IsDocNoFormatUseFullYear = Sm.GetParameter("IsDocNoFormatUseFullYear") == "Y",
              IsDocNoResetByDocAbbr = Sm.GetParameterBoo("IsDocNoResetByDocAbbr"),
              IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled"),
              IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");

            string DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            var SQL = new StringBuilder();

            //SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
            //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
            //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
            //SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
            //SQL.Append("Order By SUBSTRING(DocNo,7,4) Desc Limit 1) As temp ");
            //SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");

            if (JournalDocNoFormat == "1" || (JournalDocNoFormat == "2" && DocType == "VR"))
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                if (DocType == "VR")
                    SQL.Append("       From TblVoucherRequestHdr ");
                else
                    SQL.Append("       From TblVoucherHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append(Sm.GetNewVoucherDocNoWithAddCodes(DocDt2, 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty));
            }            

            return Sm.GetValue(SQL.ToString());
        }

        public static string GenerateDocNo(string DocDt, string DocType, string Tbl)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            var SQL = new StringBuilder();

            bool IsDocNoFormatUseFullYear = Sm.GetParameter("IsDocNoFormatUseFullYear") == "Y",
                IsDocNoResetByDocAbbr = Sm.GetParameterBoo("IsDocNoResetByDocAbbr"),
                IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");

            string DocSeqNo = "4";

            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            //SQL.Append("Select Concat( ");
            //SQL.Append("IfNull(( ");
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            //SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            //SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
            //SQL.Append("       ) As Temp ");
            //SQL.Append("   ), '0001') ");
            //SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            //SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            //SQL.Append(") As DocNo");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From " + Tbl);
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            if (IsDocNoResetByDocAbbr)
                SQL.Append("       And DocNo Like '%/" + DocAbbr + "/%' ");
            SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
            //SQL.Append("   ), '0001') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'GenerateCustomerCOAFormat', 'BankAcCodeForVRIntegrasi', 'MainCurCode','IsAcNoForSaleUseItemCategory', 'IsDOCtAmtRounded', ");
            SQL.AppendLine("'SLITaxCalculationFormat', 'DocNoFormat', 'BankAcCodeForFinet','PHTHostIntegrasi', 'PHTDbIntegrasi', 'SITaxRoundingDown', ");
            SQL.AppendLine("'CCCodePPNPOTP','AcNoForRAK', 'JournalDocNoFormat' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsDOCtAmtRounded": mIsDOCtAmtRounded = ParValue == "Y"; break;
                            case "IsAcNoForSaleUseItemCategory": mIsAcNoForSaleUseItemCategory = ParValue == "Y"; break;
                            case "SITaxRoundingDown": mSITaxRoundingDown = ParValue == "Y"; break;


                            //string
                            case "GenerateCustomerCOAFormat": mGenerateCustomerCOAFormat = ParValue; break;
                            case "BankAcCodeForVRIntegrasi": mBankAcCodeForVRIntegrasi = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "BankAcCodeForFinet": mBankAcCodeForFinet = ParValue; break;
                            case "PHTHostIntegrasi": mPHTHostIntegrasi = ParValue; break;
                            case "PHTDbIntegrasi": mPHTDbIntegrasi = ParValue; break;
                            case "SLITaxCalculationFormat": mSLITaxCalculationFormat = ParValue; break;
                            case "DocNoFormat": mDocNoFormat = ParValue; break;
                            case "CCCodePPNPOTP": mCCCodePPNPOTP = ParValue; break;
                            case "AcNoForRAK": mAcNoForRAK = ParValue; break;
                            case "JournalDocNoFormat": mJournalDocNoFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }

            if (mJournalDocNoFormat.Length == 0) mJournalDocNoFormat = "1";
        }

        private void ExecuteData()
        {
            string Dt = Sm.Left(Sm.GetDte(DteDocDt1), 8); 
          
            ProcessData(Dt);
        }

        private void ProcessData(string dt)
        {
            //try
            //{
                var l10 = new List<zTemp10>();
                int countdata = 0;
                PrepDataDIKA(ref l10, dt);
            if (IsInsertedDataNotValid(ref l10))
                return;
            else
            {
                if (l10.Count > 0)
                {
                    SavingDataDIKA(ref l10, dt);
                }
                //countdata = Convert.ToInt32(Sm.GetValue("SELECT COUNT(1) FROM tbldikahdr WHERE DocDt = '" + dt + "' "));
                MessageBox.Show(mCountdata-1 + " data already processed ");
            }
            //}
            //catch (Exception Exc)
            //{
            //    //Sm.StdMsg(mMsgType.Info, Exc.Message);
            //    //InsertLog(Exc.Message);
            //}
        }

        private bool IsInsertedDataNotValid(ref List<zTemp10> l10)
        {
            return
                CheckDataDouble(ref l10)||
                CheckDataWhs(ref l10);
        }

        private bool CheckDataDouble(ref List<zTemp10> l10)
        {
            var duplicates = l10.GroupBy(g => new { g.ReceiptNo, g.WhsCodeOld }).Where(c => c.Count() > 1).Select(g => g.Key);
            foreach (var d in duplicates)
            {
                return (Sm.StdMsgYN("Question",
                       "Duplicate warehouse old code found. " + Environment.NewLine + " receipt no : " + d.ReceiptNo + "  " + Environment.NewLine + " Whs Code : " + d.WhsCodeOld + "" +
                       Environment.NewLine +
                       "Do you want to continue ?"
                       ) == DialogResult.No);   
            }
            return false;
        }

        private bool CheckDataWhs(ref List<zTemp10> l10)
        {
            foreach (var d in l10)
            {
                if (d.BankAccount.Length <= 0)
                {
                    return (Sm.StdMsgYN("Question",
                     "Warehouse not connected with  option . " + Environment.NewLine + " receipt no : " + d.ReceiptNo + "  " + Environment.NewLine + " Whs Code : " + d.WhsCodeOld + "" +
                     Environment.NewLine +
                     "Do you want to continue ?"
                     ) == DialogResult.No);
                }
            }
            return false;
        }

        private void PrepDataDIKA(ref List<zTemp10> l10, string dt)
        {
            mCountdata = 0;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string Filter = string.Empty;
            string ReceiptNo = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    ReceiptNo = Sm.GetGrdStr(Grd1, Row, 2);
                    if (Sm.GetGrdBool(Grd1, Row, 1) && ReceiptNo.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(ReceiptNo=@ReceiptNo" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ReceiptNo" + No.ToString(), ReceiptNo);
                        No += 1;
                    }
                }
                mCountdata = No;
            }

            SQL.AppendLine("Select null as DocNo, A.DocDt, ifnull(E.WhsCode, F.Parvalue) As WhsCode, ifnull(B.CtCode, C.ParValue) As CtName, ifnull(B.CtName, 'Perhutani') ShpName, D.ItCode As ItName, A.Batch, A.Qty, ");
            if (mSITaxRoundingDown)
            {
                SQL.AppendLine("Floor(A.TotalAmt) As TotalAmt, Floor(A.Amt) As Amt, FLoor(A.TaxAmt1) As TaxAmt1, Floor(A.price) As price , ");
            } else
            {
                SQL.AppendLine("A.TotalAmt, A.Amt, A.TaxAmt1, A.price, ");
            }
            SQL.AppendLine("A.Uom,  A.Remark, A.DueDt, A.CurCode, if(length(A.TaxAmt1)>0, G.Parvalue, null) As TaxType1, A.ReceiptNo, ");
            SQL.AppendLine("A.InvoiceNo, H.Property1 As BankAccount, A.Pic, A.ChannelName, A.ChannelId, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpby, A.LastUpdt, A.SourceData,  A.CancelInd, A.ProcessInd, ifnull(H.CCCode, I.Parvalue) As CCCode, H.DeptCode, H.property2 As PICCode, B.CntCode, B.Address, A.WhsCode As WhsCodeOld, A.MateraiAmt ");
            SQL.AppendLine("From " + mPHTDbIntegrasi + ".TblPayment2 A  ");
            SQL.AppendLine("Left Join TblCustomer B On A.CtName = B.CtShortCode AND B.CtName LIKE '%DIKA%' ");
            SQL.AppendLine("Inner Join Tblparameter C on 0=0 And C.ParCode = 'OnlineCtCode' ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    SELECT ItCode FROM tblItem A ");
            SQL.AppendLine("    INNER JOIN Tblitemcategory B ON A.ItCtCode = B.ItCtCode  ");
            SQL.AppendLine("    INNER JOIN Tblparameter C ON  B.ItCtCode = C.Parvalue AND C.ParCode = 'ItemCategoryCodeForPOTP' ");
            SQL.AppendLine("    LIMIT 1  ");
            SQL.AppendLine(")D On 0=0 ");
            SQL.AppendLine("Left Join TblWarehouse E On A.WhsCode = E.WhsCodeOld ");
            SQL.AppendLine("Inner Join Tblparameter F on 0=0 And F.ParCode = 'WhsCodePOTP' ");
            SQL.AppendLine("Inner Join Tblparameter G On 0=0 And G.Parcode = 'PPNTaxCode' ");
            SQL.AppendLine("Inner JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT B.WhsCtCode, A.OptDesc as CCCode, A.Property1, ifnull(C.DeptCode, D.ParValue) As DeptCode, A.property2 FROM tbloption A ");
            SQL.AppendLine("    Inner Join tblwarehousecategory B ON A.OptCode = B.WhsCtCode AND A.Optcat = 'WhsCategoryCostCenter' ");
            SQL.AppendLine("    INNER JOIN tblcostcenter C ON A.Optdesc = C.CCCode ");
            SQL.AppendLine("    INNER JOIN tblparameter D ON 0 = 0 AND D.ParCode = 'IncomingPaymentDeptCode' ");
            SQL.AppendLine(")H ON E.WhsCtCode = H.WhsCtCode  ");
            SQL.AppendLine("Inner Join Tblparameter I On 0=0 And I.Parcode = 'CCCodePPNPOTP' ");
            //SQL.AppendLine("LEFT JOIN tbldikahdr J ON A.ReceiptNo = J.ReceiptNo AND A.DocDt = J.DocDt ");
            SQL.AppendLine("Where  A.DocDt = '" + dt + "' And "); // AND E.WhsCtCode IN (SELECT OptCode FROM tbloption WHERE optcat = 'WhsCategoryCostCenter') ");
            //SQL.AppendLine("And A.ProcessInd = 'N' And A.CancelInd = 'N' ");
            if (Filter.Length != 0)
                SQL.AppendLine(Filter);
            else
                SQL.AppendLine(" 0=1");
            SQL.AppendLine("Order By A.DocDt, A.ReceiptNo;  ");
            //SQL.AppendLine("limit 10 ; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 1600000;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                   "DocNo",
                   //1-5
                   "DocDt",
                   "WhsCode",
                   "CtName",
                   "ShpName",
                   "TotalAmt",
                   //6-10
                   "ItName",
                   "Batch",
                   "Qty",
                   "Uom",
                   "Price", 
                   //11-15
                   "Amt",
                   "Remark",
                   "DueDt",
                   "CurCode",
                   "TaxAmt1",
                   //16-20
                   "TaxType1",
                   "ReceiptNo",
                   "InvoiceNo",
                   "BankAccount",
                   "Pic",
                   //21-25
                   "ChannelName",
                   "ChannelId",
                   "CreateBy",
                   "CreateDt",
                   "LastUpby",
                   //25-30
                   "LastUpdt",
                   "SourceData",
                   "CancelInd",
                   "ProcessInd",
                   "CCCode",
                   //31-35
                   "DeptCode",
                   "PICCode",
                   "CntCode",
                   "Address",
                   "WhsCodeOld",
                   //
                   "MateraiAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l10.Add(new zTemp10()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            WhsCode = Sm.DrStr(dr, c[2]),
                            CtName = Sm.DrStr(dr, c[3]),
                            ShpName = Sm.DrStr(dr, c[4]),
                            TotalAmt = Sm.DrDec(dr, c[5]),
                            ItName = Sm.DrStr(dr, c[6]),
                            Batch = Sm.DrStr(dr, c[7]),
                            Qty = Sm.DrDec(dr, c[8]),
                            Uom = Sm.DrStr(dr, c[9]),
                            Price = Sm.DrDec(dr, c[10]),
                            Amt = Sm.DrDec(dr, c[11]),
                            Remark = Sm.DrStr(dr, c[12]),
                            DueDt = Sm.DrStr(dr, c[13]),
                            CurCode = Sm.DrStr(dr, c[14]),
                            TaxAmt1 = Sm.DrDec(dr, c[15]),
                            TaxType1 = Sm.DrStr(dr, c[16]),
                            ReceiptNo = Sm.DrStr(dr, c[17]),
                            InvoiceNo = Sm.DrStr(dr, c[18]),
                            BankAccount = Sm.DrStr(dr, c[19]),
                            PIC = Sm.DrStr(dr, c[20]),
                            ChannelName = Sm.DrStr(dr, c[21]),
                            ChannelId = Sm.DrStr(dr, c[22]),
                            CreateBy = Sm.DrStr(dr, c[23]),
                            CreateDt = Sm.DrStr(dr, c[24]),
                            LastUpBy = Sm.DrStr(dr, c[25]),
                            LastUpDt = Sm.DrStr(dr, c[26]),
                            SourceData = Sm.DrStr(dr, c[27]),
                            CancelInd = Sm.DrStr(dr, c[28]),
                            ProcessInd = Sm.DrStr(dr, c[29]),
                            CCCode = Sm.DrStr(dr, c[30]),
                            DeptCode = Sm.DrStr(dr, c[31]),
                            PICCode = Sm.DrStr(dr, c[32]),
                            CntCode = Sm.DrStr(dr, c[33]),
                            Address = Sm.DrStr(dr, c[34]),
                            WhsCodeOld = Sm.DrStr(dr, c[35]),
                            MateraiAmt = Sm.DrDec(dr, c[36]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SavingDataDIKA(ref List<zTemp10> l10, string dt)
        {
            string DOCtDocNo = string.Empty;
            string SIDocNo = string.Empty;
            string detail = "N";
            string receipttemp = null;
            decimal AmtHdr = 0;
            decimal TaxAmtHdr = 0;
            string JournalDOCtDocNo = string.Empty;
            int row = 0;

            var lDocNo = new List<cDocNo>();
            var l2 = new List<zTemp2>();
            var l3 = new List<zTemp3>();
            var l4 = new List<zTemp4>();
            lDocNo.Clear();
            l2.Clear();
            l3.Clear();
            l4.Clear();

            string IPDocNo = string.Empty;
            string VRDocNo = string.Empty;
            string VCDocNo = string.Empty;
            string JournalVCDocNo = string.Empty;
            string JournalVCDocNoDtl0 = string.Empty;
            string JournalVCDocNoDtl1 = string.Empty;
            string JournalVCDocNoDtl2 = string.Empty;
            string tempCCCode = string.Empty;


            //var cmlbefore = new List<MySqlCommand>();
            
            //cmlbefore.Add(cancelhistoryPOTP(dt));
            //Sm.ExecCommands(cmlbefore);

            foreach (var i in l10.OrderBy(o => o.ReceiptNo))
            {
                #region jika 1 receipt lbh dari 1 item 
                //if (receipttemp != i.ReceiptNo)
                //{
                //    detail = "N";
                //    row = 1;
                //    TaxAmtHdr = i.TaxAmt1;
                //    AmtHdr = i.Amt;
                //    var cml = new List<MySqlCommand>();


                //    DOCtDocNo = GenerateDocNo(i.DocDt, "DOCt", "TblDOCtHdr");
                //    SIDocNo = GenerateDocNo(i.DocDt, "SalesInvoice", "TblSalesInvoiceHdr");
                //    IPDocNo = GenerateDocNo(i.DocDt, "IncomingPayment", "TblIncomingPaymentHdr");
                //    VRDocNo = GenerateVoucherDocNo(i.DocDt, "VR");
                //    VCDocNo = GenerateVoucherDocNo(i.DocDt, "VC");

                //    cml.Add(SaveDOCtPOTP(i, DOCtDocNo, JournalDOCtDocNo, detail, row));
                //    cml.Add(SaveSIPOTP(i, SIDocNo, DOCtDocNo, detail, row, TaxAmtHdr, AmtHdr));
                //    cml.Add(SaveIPPOTP(i, IPDocNo, SIDocNo, VRDocNo, detail, row, AmtHdr));
                //    cml.Add(SaveVoucherRequestPOTP(i, VRDocNo, VCDocNo, i.DocDt, IPDocNo, detail, AmtHdr));
                //    cml.Add(SaveVoucherPOTP(i, VRDocNo, VCDocNo, detail, AmtHdr));
                //    cml.Add(SaveLogDIKA(i.DocDt, i.ReceiptNo, DOCtDocNo, SIDocNo, IPDocNo, VRDocNo, VCDocNo));

                //    Sm.ExecCommands(cml);

                //    receipttemp = i.ReceiptNo;
                //    lDocNo.Add(new  cDocNo()
                //    {
                //        cDocNoIPDocNo = IPDocNo,
                //        cDocNoVRDocNo = VRDocNo,
                //        cDocNoVCDocNo = VCDocNo,
                //        cDocNoReceiptNo = i.ReceiptNo,
                //        cDocNoCCCode = i.CCCode,
                //        cDocNoCurCode = i.CurCode,
                //        cDocNoDocDt = i.DocDt,
                //        cDocNoTotalAmt = AmtHdr,
                //        cDocNoTotalTax = TaxAmtHdr,
                //        cDocNoBankAcCode = i.BankAccount,
                //    });
                //}
                //else
                //{
                //    detail = "Y";
                //    row = row + 1;
                //    TaxAmtHdr = TaxAmtHdr + i.TaxAmt1;
                //    AmtHdr = AmtHdr + i.Amt;

                //    var cml = new List<MySqlCommand>();

                //    cml.Add(SaveDOCtPOTP(i, DOCtDocNo, JournalDOCtDocNo, detail, row));
                //    cml.Add(SaveSIPOTP(i, SIDocNo, DOCtDocNo, detail, row, TaxAmtHdr, AmtHdr));
                //    cml.Add(SaveIPPOTP(i, IPDocNo, SIDocNo, VRDocNo, detail, row, AmtHdr));
                //    cml.Add(SaveVoucherRequestPOTP(i, VRDocNo, VCDocNo, i.DocDt, IPDocNo, detail, AmtHdr));
                //    cml.Add(SaveVoucherPOTP(i, VRDocNo, VCDocNo, detail, AmtHdr));
                //    cml.Add(SaveLogDIKA(i.DocDt, i.ReceiptNo, DOCtDocNo, SIDocNo, IPDocNo, VRDocNo, VCDocNo));
                //    Sm.ExecCommands(cml);
                //}
                #endregion

                #region 1 data receipt 1 item 
                
                detail = "N";
                row = 1;
                TaxAmtHdr = i.TaxAmt1;
                AmtHdr = i.Amt;
                var cml = new List<MySqlCommand>();

                var bankAcTp = Sm.GetValue("Select BankAcTp From TblBankAccount Where BankAcCode = @Param ", i.BankAccount);
                var code1 = Sm.GetCode1ForJournalDocNo("FrmVoucher", bankAcTp, "D", mJournalDocNoFormat);
                var code2 = mJournalDocNoFormat == "1" ? string.Empty : Sm.Left(code1, 1) + Sm.Right(code1, 1) == "K" ? "M" : "K";
                var profitCenterCode = Sm.GetValue("Select ProfitCenterCode From TblCostCenter Where CCCode = @Param", i.CCCode);

                DOCtDocNo = GenerateDocNo(i.DocDt, "DOCt", "TblDOCtHdr");
                SIDocNo = GenerateDocNo(i.DocDt, "SalesInvoice", "TblSalesInvoiceHdr");
                IPDocNo = GenerateDocNo(i.DocDt, "IncomingPayment", "TblIncomingPaymentHdr");
                VRDocNo = GenerateVoucherDocNo(i.DocDt, "VR", mJournalDocNoFormat, string.Empty, string.Empty);
                VCDocNo = GenerateVoucherDocNo(i.DocDt, "VC", mJournalDocNoFormat, code1, profitCenterCode);

                cml.Add(SaveDOCtPOTP(i, DOCtDocNo, JournalDOCtDocNo, detail, row));
                cml.Add(SaveSIPOTP(i, SIDocNo, DOCtDocNo, detail, row, TaxAmtHdr, AmtHdr));
                cml.Add(SaveIPPOTP(i, IPDocNo, SIDocNo, VRDocNo, detail, row, AmtHdr));
                cml.Add(SaveVoucherRequestPOTP(i, VRDocNo, VCDocNo, i.DocDt, IPDocNo, detail, AmtHdr));
                cml.Add(SaveVoucherPOTP(i, VRDocNo, VCDocNo, detail, AmtHdr));
                cml.Add(SaveLogDIKA(i.DocDt, i.ReceiptNo, DOCtDocNo, SIDocNo, IPDocNo, VRDocNo, VCDocNo, i.ChannelId, i.ChannelName));
                cml.Add(deletehistoryPOTP(i.DocDt, i.ReceiptNo));

                Sm.ExecCommands(cml);

                receipttemp = i.ReceiptNo;
                lDocNo.Add(new cDocNo()
                {
                    cDocNoIPDocNo = IPDocNo,
                    cDocNoVRDocNo = VRDocNo,
                    cDocNoVCDocNo = VCDocNo,
                    cDocNoReceiptNo = i.ReceiptNo,
                    cDocNoCCCode = i.CCCode,
                    cDocNoCurCode = i.CurCode,
                    cDocNoDocDt = i.DocDt,
                    cDocNoTotalAmt = AmtHdr,
                    cDocNoTotalTax = TaxAmtHdr,
                    cDocNoBankAcCode = i.BankAccount,
                    cDocNoMeteraiAmt = i.MateraiAmt
                });

                #endregion 

                foreach (var x in lDocNo.OrderBy(o => o.cDocNoCCCode))
                {
                    var cml2 = new List<MySqlCommand>();
                    
                    if (tempCCCode != x.cDocNoCCCode)
                    {
                        l2.Clear();
                        l3.Clear();
                        l4.Clear();
                        PrepDataCC1(ref l2, x.cDocNoCCCode);
                        PrepDataCC2(ref l3, mCCCodePPNPOTP);
                        ProcessDataCC(ref l2, ref l3, ref l4);
                    }

                    if (mJournalDocNoFormat == "1")
                        JournalVCDocNo = Sm.GetValue(GetNewJournalDocNo(dt, 1));
                    else
                    {
                        // TKG 1
                        // JournalVCDocNo = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(dt, 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty));
                        JournalVCDocNo = Sm.GetNewJournalDocNoWithAddCodes(dt, 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty);
                    }

                    cml2.Add(SaveJournalIPPOTP(JournalVCDocNo, x.cDocNoVRDocNo, x.cDocNoDocDt, x.cDocNoCurCode, x.cDocNoVCDocNo, x.cDocNoCCCode, x.cDocNoBankAcCode, x.cDocNoReceiptNo, x.cDocNoMeteraiAmt));
                    //cml2.Add(SaveLogDIKADtl(x.cDocNoDocDt, x.cDocNoReceiptNo, JournalVCDocNo, 1));

                    int tipe = 0, seqno = 1, dno = 2, vals = 1;

                    string type = string.Empty;
                    var lastItem = l4.OrderBy(z => z.seqno).LastOrDefault();

                    foreach (var s in l4.OrderBy(o => o.seqno))
                    {
                        vals++;
                        var profitCenterCode1 = Sm.GetValue("Select ProfitCenterCode From TblCostCenter Where CCCode = @Param", s.cccode);
                        if (s.seqno == 0) //journal pertama
                        {
                            if (mJournalDocNoFormat == "1")
                                JournalVCDocNoDtl0 = Sm.GetValue(GetNewJournalDocNoTemp(dt, seqno, JournalVCDocNo));
                            else
                            {
                                // TKG 2
                                //JournalVCDocNoDtl0 = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(dt, vals, code1, profitCenterCode1, string.Empty, string.Empty, string.Empty));
                                JournalVCDocNoDtl0 = Sm.GetNewJournalDocNoWithAddCodes(dt, 1, "M", profitCenterCode1, string.Empty, string.Empty, string.Empty);
                            }
                            if(x.cDocNoTotalTax>0)
                                cml2.Add(SaveJournalIIPPOTP(JournalVCDocNoDtl0, x.cDocNoDocDt, x.cDocNoCurCode, x.cDocNoVCDocNo, "2", s.cccode, x.cDocNoTotalTax, dno, x.cDocNoReceiptNo));
                            // cml2.Add(SaveLogDIKADtl(x.cDocNoDocDt, x.cDocNoReceiptNo, JournalVCDocNoDtl0, dno));
                            tipe++; dno++; seqno++;
                        }
                        else if (s.seqno.ToString() == lastItem.seqno.ToString()) //journal terakhir
                        {
                            type = Convert.ToString(tipe % 2);
                            if (mJournalDocNoFormat == "1")
                                JournalVCDocNoDtl0 = Sm.GetValue(GetNewJournalDocNoTemp(dt, seqno, JournalVCDocNo));
                            else
                            {
                                // TKG 3
                                //JournalVCDocNoDtl0 = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(dt, vals, code1, profitCenterCode1, string.Empty, string.Empty, string.Empty));
                                JournalVCDocNoDtl0 = Sm.GetNewJournalDocNoWithAddCodes(dt, 1, "M", profitCenterCode1, string.Empty, string.Empty, string.Empty);
                            }
                            if (x.cDocNoTotalTax > 0)
                                cml2.Add(SaveJournalIIPPOTP(JournalVCDocNoDtl0, x.cDocNoDocDt, x.cDocNoCurCode, x.cDocNoVCDocNo, "1", s.cccode, x.cDocNoTotalTax, dno, x.cDocNoReceiptNo));
                            // cml2.Add(SaveLogDIKADtl(x.cDocNoDocDt, x.cDocNoReceiptNo, JournalVCDocNoDtl0, dno));
                            tipe++; dno++; seqno++;
                        }
                        else //jurnal RAK
                        {
                            type = Convert.ToString(tipe % 2);
                            if (mJournalDocNoFormat == "1")
                                JournalVCDocNoDtl1 = Sm.GetValue(GetNewJournalDocNoTemp(dt, seqno, JournalVCDocNo));
                            else
                            {
                                // TKG 4
                                //JournalVCDocNoDtl1 = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(dt, vals, code1, profitCenterCode1, string.Empty, string.Empty, string.Empty));
                                JournalVCDocNoDtl1 = Sm.GetNewJournalDocNoWithAddCodes(dt, 1, "M", profitCenterCode1, string.Empty, string.Empty, string.Empty);
                            }
                            if(x.cDocNoTotalTax > 0)
                                cml2.Add(SaveJournalIIPPOTP(JournalVCDocNoDtl1, x.cDocNoDocDt, x.cDocNoCurCode, x.cDocNoVCDocNo, type, s.cccode, x.cDocNoTotalTax, dno, x.cDocNoReceiptNo));
                            // cml2.Add(SaveLogDIKADtl(x.cDocNoDocDt, x.cDocNoReceiptNo, JournalVCDocNoDtl1, dno));
                            tipe++; dno++; seqno++; vals++;
                            type = Convert.ToString(tipe % 2);
                            if (mJournalDocNoFormat == "1")
                                JournalVCDocNoDtl2 = Sm.GetValue(GetNewJournalDocNoTemp(dt, seqno, JournalVCDocNo));
                            else
                            {
                                // TKG 5
                                //JournalVCDocNoDtl2 = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(dt, vals, code1, profitCenterCode1, string.Empty, string.Empty, string.Empty));
                                JournalVCDocNoDtl2 = Sm.GetNewJournalDocNoWithAddCodes(dt, 1, "M", profitCenterCode1, string.Empty, string.Empty, string.Empty);
                            }
                            if (x.cDocNoTotalTax > 0)
                                cml2.Add(SaveJournalIIPPOTP(JournalVCDocNoDtl2, x.cDocNoDocDt, x.cDocNoCurCode, x.cDocNoVCDocNo, type, s.cccode, x.cDocNoTotalTax, dno, x.cDocNoReceiptNo));
                            // cml2.Add(SaveLogDIKADtl(x.cDocNoDocDt, x.cDocNoReceiptNo, JournalVCDocNoDtl2, dno));
                            tipe++; dno++; seqno++;
                        }
                    }
                    Sm.ExecCommands(cml2);
                    tempCCCode = x.cDocNoCCCode;
                }
                lDocNo.Clear();
            }
        }

        private MySqlCommand cancelhistoryPOTP(string Dt)
        {
            var SQL = new StringBuilder();
            //DOct
            //SQL.AppendLine("delete FROM tbldoctdtl ");
            //SQL.AppendLine("WHERE DocNo IN( ");
            //SQL.AppendLine("   SELECT DocNo FROM tbldocthdr ");
            //SQL.AppendLine("   WHERE docdt = @Dt AND (CreateBy = 'POTP' OR Createby = 'DIKA') ");
            //SQL.AppendLine("); ");
            //SQL.AppendLine("delete FROM tbldocthdr ");
            //SQL.AppendLine("WHERE  docdt = @Dt AND (CreateBy = 'POTP' OR Createby = 'DIKA'); ");
            ////SI
            //SQL.AppendLine("delete FROM tblsalesInvoicedtl ");
            //SQL.AppendLine("WHERE DocNo IN( ");
            //SQL.AppendLine("   SELECT DocNo FROM tblsalesInvoiceHdr ");
            //SQL.AppendLine("   WHERE docdt = @Dt AND (CreateBy = 'POTP' OR Createby = 'DIKA') ");
            //SQL.AppendLine("); ");
            //SQL.AppendLine("delete FROM tblsalesInvoiceHdr ");
            //SQL.AppendLine("WHERE  docdt = @Dt AND (CreateBy = 'POTP' OR Createby = 'DIKA'); ");
            ////IP
            //SQL.AppendLine("delete FROM tblincomingpaymentdtl ");
            //SQL.AppendLine("WHERE DocNo IN( ");
            //SQL.AppendLine("   SELECT DocNo FROM tblincomingpaymenthdr ");
            //SQL.AppendLine("   WHERE docdt = @Dt AND (CreateBy = 'POTP' OR Createby = 'DIKA') ");
            //SQL.AppendLine("); ");
            //SQL.AppendLine("delete FROM tblincomingpaymenthdr ");
            //SQL.AppendLine("WHERE  docdt = @Dt AND(CreateBy = 'POTP' OR Createby = 'DIKA'); ");
            ////VR
            //SQL.AppendLine("delete FROM tblvoucherRequestdtl ");
            //SQL.AppendLine("WHERE DocNo IN( ");
            //SQL.AppendLine("   SELECT DocNo FROM tblVoucherRequesthdr ");
            //SQL.AppendLine("   WHERE docdt = @Dt AND (CreateBy = 'POTP' OR Createby = 'DIKA') ");
            //SQL.AppendLine("); ");
            //SQL.AppendLine("delete FROM tblVoucherRequesthdr ");
            //SQL.AppendLine("WHERE  docdt = @Dt AND(CreateBy = 'POTP' OR Createby = 'DIKA'); ");
            ////VC
            //SQL.AppendLine("delete FROM tblvoucherdtl ");
            //SQL.AppendLine("WHERE DocNo IN( ");
            //SQL.AppendLine("   SELECT DocNo FROM tblVoucherhdr ");
            //SQL.AppendLine("   WHERE docdt = @Dt AND (CreateBy = 'POTP' OR Createby = 'DIKA') ");
            //SQL.AppendLine("); "); 
            //SQL.AppendLine("delete FROM TblVoucherDtl3 ");
            //SQL.AppendLine("WHERE DocNo IN( ");
            //SQL.AppendLine("   SELECT DocNo FROM tblVoucherhdr ");
            //SQL.AppendLine("   WHERE docdt = @Dt AND (CreateBy = 'POTP' OR Createby = 'DIKA') ");
            //SQL.AppendLine("); ");
            //SQL.AppendLine("delete FROM tblVoucherhdr ");
            //SQL.AppendLine("WHERE  docdt = @Dt AND (CreateBy = 'POTP' OR Createby = 'DIKA'); ");
            //Journal
            SQL.AppendLine("delete FROM tblJournaldtl ");
            SQL.AppendLine("WHERE DocNo IN( ");
            SQL.AppendLine("   SELECT DocNo FROM tblJournalhdr ");
            SQL.AppendLine("   WHERE docdt = @Dt AND (CreateBy = 'POTP' OR Createby = 'DIKA') ");
            SQL.AppendLine("); ");
            SQL.AppendLine("delete FROM tblJournalhdr ");
            SQL.AppendLine("WHERE  docdt = @Dt AND (CreateBy = 'POTP' OR Createby = 'DIKA'); ");

            //SQL.AppendLine("Update TblSalesInvoicehdr Set CancelInd = 'Y' Where DocNo in (Select SIDocNo From TblDikahdr Where DocDt = @Dt); ");
            //SQL.AppendLine("Update TblIncomingpaymenthdr Set CancelInd = 'Y' Where DocNo in (Select IPDocNo From TblDikahdr Where DocDt = @Dt); ");
            //SQL.AppendLine("Update TblVoucherRequesthdr Set CancelInd = 'Y' Where DocNo in (Select VRDocNo From TblDikahdr Where DocDt = @Dt); ");
            //SQL.AppendLine("Update TblVoucherhdr Set CancelInd = 'Y' Where DocNo in (Select VCDocNo From TblDikahdr Where DocDt = @Dt); ");
            //SQL.AppendLine("Delete From TblJournalhdr  Where DocNo in (Select JournalDocNo From TblDikaDtl Where DocDt = @Dt); ");
            //SQL.AppendLine("Delete From TblJournalDtl  Where DocNo in (Select JournalDocNo From TblDikaDtl Where DocDt = @Dt); ");
            SQL.AppendLine("Delete From TblDikaDtl Where DocDt = @Dt; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            return cm;
        }

        private MySqlCommand deletehistoryPOTP(string Dt, string Receiptno)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("delete FROM tblJournaldtl ");
            SQL.AppendLine("Where DocNo in ( ");
            SQL.AppendLine("Select journalDocNo From TblDikaDtl  ");
            SQL.AppendLine("WHERE docdt = @Dt AND Receiptno=@ReceiptNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("delete FROM tblJournalhdr ");
            SQL.AppendLine("Where DocNo in ( ");
            SQL.AppendLine("Select journalDocNo From TblDikaDtl  ");
            SQL.AppendLine("WHERE docdt = @Dt AND Receiptno=@ReceiptNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Delete From TblDikaDtl Where DocDt =@Dt AND Receiptno=@ReceiptNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            Sm.CmParam<String>(ref cm, "@ReceiptNo", Receiptno);

            return cm;
        }


        private MySqlCommand SaveDOCtPOTP(zTemp10 x, string DOCtDocNo, string JournalDocNo, string detail, int Row)
        {
            var SQL = new StringBuilder();

           
            if (detail == "N")
            {
                SQL.AppendLine("delete FROM tbldoctdtl ");
                SQL.AppendLine("WHERE DocNo IN( ");
                SQL.AppendLine("   SELECT DocNo FROM tbldocthdr ");
                SQL.AppendLine("   WHERE docdt = @DocDt AND DocNoInternal=@ReceiptNo  AND (CreateBy = 'POTP' OR Createby = 'DIKA') ");
                SQL.AppendLine("); ");
                SQL.AppendLine("delete FROM tbldocthdr ");
                SQL.AppendLine("WHERE  docdt = @DocDt AND DocNoInternal=@ReceiptNo And (CreateBy = 'POTP' OR Createby = 'DIKA'); ");

                SQL.AppendLine("Insert Into TblDOCtHdr ");
                SQL.AppendLine("(DocNo, DocDt, Status, ProcessInd, WhsCode, CtCode, DocNoInternal, ResiNo, CurCode, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, SAPhone, SAFax, SAEmail, SAMobile, ");
                SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, KBSubmissionNo, CustomsDocCode, ");
                SQL.AppendLine("Driver, Expedition, VehicleRegNo, ProductionWorkGroup, JournalDocNo, ");
                SQL.AppendLine("Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DOCtDocNo, @DocDt, @Status, @ProcessInd, @WhsCode, @CtCode, @DocNoInternal, @ResiNo, @CurCode, @SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCd, @SAPhone, @SAFax, @SAEmail, @SAMobile, ");
                SQL.AppendLine("null, null, null, null, null, null, null, null, ");
                SQL.AppendLine("null, null, null, null, @JournalDocNo, ");
                SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()); ");
            }

            SQL.AppendLine("Insert Into TblDOCtDtl(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, ProcessInd, Source, Lot, Bin, QtyPackagingUnit, PackagingUnitUomCode, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt)  ");
            SQL.AppendLine("Values(@DOCtDocNo, @DNo, 'N', @ItCode, @PropCode, @BatchNo, @ProcessInd, @Source, @Lot, @Bin, @QtyPackagingUnit, @PackagingUnitUomCode, @Qty, @Qty2, @Qty3, @UPrice, @Remark, @CreateBy, CurrentDateTime()) ; ");

            //SQL.AppendLine("Update " + mDatabase2 + ".TblPayment2 SET ProcessInd = 'Y' ");
            //SQL.AppendLine("Where ReceiptNo=@ReceiptNo And ProcessInd = 'N' And cancelInd = 'N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DOCtDocNo", DOCtDocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);
            Sm.CmParam<String>(ref cm, "@ReceiptNo", x.ReceiptNo);
            Sm.CmParam<String>(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@Status", "A");
            Sm.CmParam<String>(ref cm, "@ProcessInd", "F");
            Sm.CmParam<String>(ref cm, "@WhsCode", x.WhsCode);
            Sm.CmParam<String>(ref cm, "@CtCode", x.CtName);
            Sm.CmParam<String>(ref cm, "@DocNoInternal", x.ReceiptNo);
            Sm.CmParam<String>(ref cm, "@CurCode", x.CurCode);
            Sm.CmParam<String>(ref cm, "@SAName", x.ShpName);
            Sm.CmParam<String>(ref cm, "@SAAddress", x.Address);
            Sm.CmParam<String>(ref cm, "@SACntCode", x.CntCode);
            Sm.CmParam<String>(ref cm, "@SACityCode", x.CtName);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@UserCode", x.CreateBy);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("000", Row), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", x.ItName);
            Sm.CmParam<String>(ref cm, "@PropCode", "-");
            Sm.CmParam<String>(ref cm, "@BatchNo", x.Batch.Length > 0 ? x.Batch : "-");
            Sm.CmParam<String>(ref cm, "@Source", "-");
            Sm.CmParam<String>(ref cm, "@Lot", "-");
            Sm.CmParam<String>(ref cm, "@Bin", "-");
            Sm.CmParam<Decimal>(ref cm, "@Qty", x.Qty);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", x.Qty);
            Sm.CmParam<Decimal>(ref cm, "@Qty3", x.Qty);
            Sm.CmParam<Decimal>(ref cm, "@UPrice", x.Price);
            Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", x.Qty);
            Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", x.Uom);
            Sm.CmParam<String>(ref cm, "@CreateBy", x.SourceData);

            return cm;
        }

    

        private MySqlCommand SaveSIPOTP(zTemp10 x, string SIDocNo, string DOCtDocNo, string detail, int Row, decimal TaxAmtHdr, decimal AmtHdr)
        {
            var SQL = new StringBuilder();
            
            if (detail == "N")
            {
                SQL.AppendLine("delete FROM tblsalesInvoicedtl ");
                SQL.AppendLine("WHERE DocNo IN( ");
                SQL.AppendLine("   SELECT DocNo FROM tblsalesInvoiceHdr ");
                SQL.AppendLine("   WHERE docdt = @DocDt And LocalDocNo = @ReceiptNo AND (CreateBy = 'POTP' OR Createby = 'DIKA') ");
                SQL.AppendLine("); ");
                SQL.AppendLine("delete FROM tblsalesInvoiceHdr ");
                SQL.AppendLine("WHERE  docdt = @DocDt And LocalDocNo = @ReceiptNo AND (CreateBy = 'POTP' OR Createby = 'DIKA'); ");

                SQL.AppendLine("Insert Into TblSalesInvoiceHdr(DocNo, ReceiptNo, DocDt, CancelInd, ProcessInd, CBDInd, CtCode, DueDt, LocalDocNo, TaxInvDocument, TaxInvDt, ");
                SQL.AppendLine("TaxInvoiceNo, TaxInvoiceNo2, TaxInvoiceNo3, ");
                SQL.AppendLine("TaxInvoiceDt, TaxInvoiceDt2, TaxInvoiceDt3, ");
                SQL.AppendLine("TaxCode1, TaxCode2, TaxCode3, ");
                SQL.AppendLine("TaxAlias1, TaxAlias2, TaxAlias3, ");
                SQL.AppendLine("TaxAmt1, TaxAmt2, TaxAmt3, ");
                SQL.AppendLine("CurCode, TotalAmt, TotalTax, Downpayment, Amt, AdditionalCostDiscAmt, MInd, BankAcCode, SalesName, SignName, JournalDocNo, JournalDocNo2, DeptCode, TypeCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@SIDocNo, @ReceiptNo, @DocDt, 'N', 'F', 'N', @CtCode, @DueDt, @LocalDocNo, @TaxInvDocument, @TaxInvDt, ");
                SQL.AppendLine("@TaxInvoiceNo, @TaxInvoiceNo2, @TaxInvoiceNo3, ");
                SQL.AppendLine("@TaxInvoiceDt, @TaxInvoiceDt2, @TaxInvoiceDt3, ");
                SQL.AppendLine("@TaxCode1, @TaxCode2, @TaxCode3, ");
                SQL.AppendLine("@TaxAlias1, @TaxAlias2, @TaxAlias3, ");
                SQL.AppendLine("@TaxAmt1, @TaxAmt2, @TaxAmt3, ");
                SQL.AppendLine("@CurCode, @TotalAmt, @TotalTax, @Downpayment, @Amt, @AdditionalCostDiscAmt, 'N', @BankAcCode, @SalesName, @SignName, Null, Null, @DeptCode, @TypeCode, @Remark, @CreateBy, CurrentDateTime());");
            }

            SQL.AppendLine("Insert Into TblSalesInvoiceDtl(DocNo, DNo, DocType, DOCtDocNo, DOCtDNo, ProcessInd, ItCode, QtyPackagingUnit, Qty, UPriceBeforeTax, TaxRate, TaxAmt, UPriceAfterTax, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SIDocNo, @DNo, @DocType, @DOCtDocNo, @DOCtDNo, 'F', @ItCode, @QtyPackagingUnit, @Qty, @UPriceBeforeTax, @TaxRate, @TaxAmt, @UPriceAfterTax, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblsalesInvoiceHdr SET TotalAmt =@TotalAmt, TotalTax=@TotalTax,  Amt =@Amt, TaxAmt1 = @TaxAmt1  Where DocNo=@SIDocNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SIDocNo", SIDocNo);
            Sm.CmParam<String>(ref cm, "@ReceiptNo", x.ReceiptNo);
            Sm.CmParam<String>(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@CtCode", x.CtName);
            Sm.CmParam<String>(ref cm, "@DueDt", x.DueDt);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", x.ReceiptNo);
            Sm.CmParam<String>(ref cm, "@TaxInvDocument", "");
            Sm.CmParam<String>(ref cm, "@TaxInvDt", "");
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", "");
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", "");
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", "");
            Sm.CmParam<String>(ref cm, "@TaxInvoiceDt", "");
            Sm.CmParam<String>(ref cm, "@TaxInvoiceDt2", "");
            Sm.CmParam<String>(ref cm, "@TaxInvoiceDt3", "");
            Sm.CmParam<String>(ref cm, "@TaxCode1", x.TaxAmt1>0?  x.TaxType1 : "");
            Sm.CmParam<String>(ref cm, "@TaxCode2", "");
            Sm.CmParam<String>(ref cm, "@TaxCode3", "");
            Sm.CmParam<String>(ref cm, "@TaxAlias1", "");
            Sm.CmParam<String>(ref cm, "@TaxAlias2", "");
            Sm.CmParam<String>(ref cm, "@TaxAlias3", "");
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", TaxAmtHdr);
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", 0);
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", 0);
            Sm.CmParam<String>(ref cm, "@CurCode", x.CurCode);
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", AmtHdr);
            Sm.CmParam<Decimal>(ref cm, "@TotalTax", TaxAmtHdr);
            Sm.CmParam<Decimal>(ref cm, "@Downpayment", 0);
            Sm.CmParam<Decimal>(ref cm, "@Amt", (AmtHdr + TaxAmtHdr));
            Sm.CmParam<Decimal>(ref cm, "@AdditionalCostDiscAmt", 0);
            Sm.CmParam<String>(ref cm, "@SignName", "");
            Sm.CmParam<String>(ref cm, "@SalesName", "");
            Sm.CmParam<String>(ref cm, "@BankAcCode", x.BankAccount);
            Sm.CmParam<String>(ref cm, "@DeptCode", x.DeptCode);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", x.SourceData);

            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("000", Row), 3));
            Sm.CmParam<String>(ref cm, "@DocType", "3");
            Sm.CmParam<String>(ref cm, "@DOCtDocNo", DOCtDocNo);
            Sm.CmParam<String>(ref cm, "@DOCtDNo", Sm.Right(string.Concat("000", Row), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", x.ItName);
            Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", 0);
            Sm.CmParam<Decimal>(ref cm, "@Qty", x.Qty);
            Sm.CmParam<Decimal>(ref cm, "@UPriceBeforeTax", x.Price);
            Sm.CmParam<Decimal>(ref cm, "@TaxRate", 0);
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", 0);
            Sm.CmParam<Decimal>(ref cm, "@UPriceAfterTax", x.Price);
            Sm.CmParam<String>(ref cm, "@TypeCode", "5");


            return cm;
        }

        private  MySqlCommand SaveIPPOTP(zTemp10 x, string IPDocNo, string SIDocNo, string VRDocNo, string detail, int row, decimal AmtTotal)
        {
            var SQL = new StringBuilder();

            if (detail == "N")
            {
                SQL.AppendLine("delete FROM tblincomingpaymentdtl ");
                SQL.AppendLine("WHERE DocNo IN( ");
                SQL.AppendLine("   SELECT DocNo FROM tblincomingpaymenthdr ");
                SQL.AppendLine("   WHERE docdt = @DocDt  And Remark = @InvoiceNo AND (CreateBy = 'POTP' OR Createby = 'DIKA') ");
                SQL.AppendLine("); ");
                SQL.AppendLine("delete FROM tblincomingpaymenthdr ");
                SQL.AppendLine("WHERE  docdt = @DocDt  And Remark = @InvoiceNo AND (CreateBy = 'POTP' OR Createby = 'DIKA'); ");

                SQL.AppendLine("Insert Into TblIncomingPaymentHdr ");
                SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, CtCode, ");
                SQL.AppendLine("PaymentUser, PaidToBankCode, PaidToBankBranch, PaidToBankAcName, PaidToBankAcNo, ");
                SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
                SQL.AppendLine("VoucherRequestDocNo, VoucherRequestSummaryInd, VoucherRequestSummaryDesc, ");
                SQL.AppendLine("CurCode, Amt, CurCode2, RateAmt, Amt2, DepositAmt, COAAmt, COAAmt2, ");
                SQL.AppendLine("ProjectDocNo1, ProjectDocNo2, ProjectDocNo3, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@IPDocNo, @DocDt, 'N', 'F', @MInd, @CtCode, ");
                SQL.AppendLine("@PaymentUser, @PaidToBankCode, @PaidToBankBranch, @PaidToBankAcName, @PaidToBankAcNo, ");
                SQL.AppendLine("@AcType, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, ");
                SQL.AppendLine("@VoucherRequestDocNo, @VoucherRequestSummaryInd, @VoucherRequestSummaryDesc, ");
                SQL.AppendLine("@CurCode, @AmtTotal, @CurCode2, @RateAmt, @Amt2, @DepositAmt, @COAAmt, @COAAmt2, ");
                SQL.AppendLine("@ProjectDocNo1, @ProjectDocNo2, @ProjectDocNo3, @Remark, @CreateBy, CurrentDateTime()); ");

                SQL.AppendLine("Insert Into TblIncomingPaymentDtl(DocNo, DNo, InvoiceDocno, InvoiceType, Amt, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@IPDocNo, @DNo, @InvoiceDocno, @InvoiceType, @AmtTotal, @Remark, @CreateBy, CurrentDateTime()); ");
            }
            else
            {
                SQL.AppendLine("SELECT null;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@IPDocNo", IPDocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@MInd", "N");
            Sm.CmParam<String>(ref cm, "@CtCode", x.CtName);
            Sm.CmParam<String>(ref cm, "@PaymentUser", "");
            Sm.CmParam<String>(ref cm, "@PaidToBankCode", "");
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch", "");
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName", "");
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", x.BankAccount);
            Sm.CmParam<String>(ref cm, "@AcType", "D");
            Sm.CmParam<String>(ref cm, "@PaymentType", "B");
            Sm.CmParam<String>(ref cm, "@BankAcCode", x.BankAccount);
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetValue("SELECT BankCode FROM TblBankAccount WHERE BankAcCode = '" + x.BankAccount + "'"));
            Sm.CmParam<String>(ref cm, "@GiroNo", "");
            Sm.CmParam<String>(ref cm, "@DueDt", x.DueDt);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VRDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestSummaryInd", "N");
            Sm.CmParam<String>(ref cm, "@VoucherRequestSummaryDesc", "");
            Sm.CmParam<String>(ref cm, "@CurCode", x.CurCode);
            Sm.CmParam<String>(ref cm, "@CurCode2", x.CurCode);
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", 1);
            Sm.CmParam<Decimal>(ref cm, "@Amt2", (x.Amt + x.TaxAmt1));
            Sm.CmParam<Decimal>(ref cm, "@DepositAmt", 0);
            Sm.CmParam<Decimal>(ref cm, "@COAAmt", 0);
            Sm.CmParam<Decimal>(ref cm, "@COAAmt2", 0);
            Sm.CmParam<String>(ref cm, "@ProjectDocNo1", "");
            Sm.CmParam<String>(ref cm, "@ProjectDocNo2", "");
            Sm.CmParam<String>(ref cm, "@ProjectDocNo3", "");
            Sm.CmParam<String>(ref cm, "@CreateBy", x.CreateBy);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("000", row), 3));
            Sm.CmParam<String>(ref cm, "@InvoiceDocNo", SIDocNo);
            Sm.CmParam<String>(ref cm, "@InvoiceType", "1");
            Sm.CmParam<Decimal>(ref cm, "@Amt", (x.Amt + x.TaxAmt1));
            Sm.CmParam<String>(ref cm, "@Remark", string.Concat(x.InvoiceNo));
            Sm.CmParam<Decimal>(ref cm, "@AmtTotal", x.TotalAmt);

            return cm;
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsProcessedDataNotValid()) return;
                    
                ExecuteData();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsProcessedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt1, "date") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            bool CheckInd = false;
            if (e.ColIndex == 1)
            {
                if (Sm.GetGrdBool(Grd1, 0, 1) == true)
                    CheckInd = false;
                else
                    CheckInd = true;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        Grd1.Cells[Row, 1].Value = CheckInd;
                }
            }
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsExisted = false;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1))
                {
                    IsExisted = true;
                    break;
                }
            }
            if (!IsExisted)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 receiptno.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveVoucherRequestPOTP(zTemp10 x, string VRDocNo, string VCDocNo, string DocDt, string IPDocNo, string detail, decimal AmtTotal)
        {
            var SQL = new StringBuilder();
            if (detail == "N")
            {
                SQL.AppendLine("delete FROM tblvoucherRequestdtl ");
                SQL.AppendLine("WHERE DocNo IN( ");
                SQL.AppendLine("   SELECT DocNo FROM tblVoucherRequesthdr ");
                SQL.AppendLine("   WHERE docdt = @DocDt AND Remark=@Remark And (CreateBy = 'POTP' OR Createby = 'DIKA') ");
                SQL.AppendLine("); ");
                SQL.AppendLine("delete FROM tblVoucherRequesthdr ");
                SQL.AppendLine("WHERE  docdt = @DocDt AND Remark=@Remark AND (CreateBy = 'POTP' OR Createby = 'DIKA'); ");

                SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
                SQL.AppendLine("(DocNo, LocalDocNo,  DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
                SQL.AppendLine("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, GiroNo, ");
                SQL.AppendLine("BankCode, OpeningDt, DueDt, PIC, CurCode, Amt, CurCode2, ExcRate, PaymentUser, ");
                SQL.AppendLine("PaidToBankCode, PaidToBankBranch, PaidToBankAcNo, PaidToBankAcName, DocEnclosure, EntCode, RefundDt, Remark, ");
                SQL.AppendLine("ProjectDocNo1, ProjectDocNo2, ProjectDocNo3, SiteCode, ReqType, BCCode, Amt2, BIOPProjectInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values ");
                SQL.AppendLine("(@DocNo, null, @DocDt, 'N', @Status, 'N', @DeptCode, @DocType, @VoucherDocNo, ");
                SQL.AppendLine("@AcType, @BankAcCode, @AcType2, @BankAcCode2, @PaymentType, @GiroNo, ");
                SQL.AppendLine("@BankCode, @OpeningDt, @DueDt, @PIC, @CurCode, @AmtTotal, @CurCode2, @ExcRate, @PaymentUser,");
                SQL.AppendLine("@PaidToBankCode, @PaidToBankBranch, @PaidToBankAcNo, @PaidToBankAcName, @DocEnclosure, @EntCode, @RefundDt, @Remark, ");
                SQL.AppendLine("@ProjectDocNo1, @ProjectDocNo2, @ProjectDocNo3, @SiteCode, @ReqType, @BCCode, @Amt2, @BIOPProjectInd, @CreateBy, CurrentDateTime()); ");

                SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@DocNo, @DNo, @Description, @AmtTotal, @Remark, @CreateBy, CurrentDateTime()); ");
            }
            else
            {
                SQL.AppendLine("SELECT null;");
                //SQL.AppendLine("Update TblVoucherRequestHdr SET Amt=@Amt Where DocNo=@DocNo; ");
                //SQL.AppendLine("Update TblVoucherRequestDtl SET Amt=@Amt Where DocNo=@DocNo; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VRDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@LocalDocNo", "");
            Sm.CmParam<String>(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@Status", "A");
            Sm.CmParam<String>(ref cm, "@Description", string.Concat(x.InvoiceNo, x.SourceData));
            Sm.CmParam<String>(ref cm, "@MInd", "N");
            Sm.CmParam<String>(ref cm, "@DeptCode", x.DeptCode);
            Sm.CmParam<String>(ref cm, "@DocType", "02");
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", VCDocNo);
            Sm.CmParam<String>(ref cm, "@OpeningDt", "");
            Sm.CmParam<String>(ref cm, "@DueDt", "");
            Sm.CmParam<String>(ref cm, "@PIC", x.PICCode);
            Sm.CmParam<String>(ref cm, "@CurCode", x.CurCode);
            Sm.CmParam<String>(ref cm, "@CurCode2", x.CurCode);
            //CmParam<Decimal>(ref cm, "@Amt", AmtTotal);
            Sm.CmParam<Decimal>(ref cm, "@Amt2", 0);
            Sm.CmParam<String>(ref cm, "@AcType", "D");
            Sm.CmParam<String>(ref cm, "@BankAcCode", x.BankAccount);
            Sm.CmParam<String>(ref cm, "@AcType2", "");
            Sm.CmParam<String>(ref cm, "@BankAcCode2", "");
            Sm.CmParam<String>(ref cm, "@PaymentType", "B");
            Sm.CmParam<String>(ref cm, "@GiroNo", "");
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetValue("SELECT BankCode FROM TblBankAccount WHERE BankAcCode = '" + x.BankAccount + "'"));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", 0);
            Sm.CmParam<String>(ref cm, "@PaymentUser", "");
            Sm.CmParam<String>(ref cm, "@PaidToBankCode", "");
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch", "");
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", "");
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName", "");
            Sm.CmParam<Decimal>(ref cm, "@DocEnclosure", 0);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetValue("SELECT EntCode FROM TblBankAccount WHERE BankAcCode = '" + x.BankAccount + "'"));
            Sm.CmParam<String>(ref cm, "@RefundDt", "");
            Sm.CmParam<String>(ref cm, "@Remark", x.InvoiceNo);
            Sm.CmParam<String>(ref cm, "@ProjectDocNo1", "");
            Sm.CmParam<String>(ref cm, "@ProjectDocNo2", "");
            Sm.CmParam<String>(ref cm, "@ProjectDocNo3", "");
            Sm.CmParam<String>(ref cm, "@SiteCode", "");
            Sm.CmParam<String>(ref cm, "@BCCode", "");
            Sm.CmParam<String>(ref cm, "@ReqType", "");
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", "");
            Sm.CmParam<String>(ref cm, "@ItCode", "");
            Sm.CmParam<String>(ref cm, "@AcNo", "");
            Sm.CmParam<String>(ref cm, "@BIOPProjectInd", "");

            Sm.CmParam<String>(ref cm, "@ReceiptNo", x.InvoiceNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", x.CreateBy);
            Sm.CmParam<String>(ref cm, "@CreateDt", x.CreateDt);
            Sm.CmParam<String>(ref cm, "@LastUpBy", x.LastUpBy);
            Sm.CmParam<String>(ref cm, "@LastUpDt", x.LastUpDt);
            Sm.CmParam<String>(ref cm, "@CancelInd", x.CancelInd);
            Sm.CmParam<String>(ref cm, "@ProcessInd", x.ProcessInd);
            Sm.CmParam<Decimal>(ref cm, "@AmtTotal", x.TotalAmt);

            return cm;
        }

       
        private MySqlCommand SaveVoucherPOTP(zTemp10 x, string VRDocNo, string VCDocNo, string detail, decimal AmtTotal)
        {
            decimal Amt = AmtTotal;
            var SQL = new StringBuilder();

            if (detail == "N")
            {
                SQL.AppendLine("delete FROM tblvoucherdtl ");
                SQL.AppendLine("WHERE DocNo IN( ");
                SQL.AppendLine("   SELECT DocNo FROM tblVoucherhdr ");
                SQL.AppendLine("   WHERE docdt = @DocDt And Remark=@Remark AND (CreateBy = 'POTP' OR Createby = 'DIKA') ");
                SQL.AppendLine("); ");
                SQL.AppendLine("delete FROM TblVoucherDtl3 ");
                SQL.AppendLine("WHERE DocNo IN( ");
                SQL.AppendLine("   SELECT DocNo FROM tblVoucherhdr ");
                SQL.AppendLine("   WHERE docdt = @DocDt And Remark=@Remark AND (CreateBy = 'POTP' OR Createby = 'DIKA') ");
                SQL.AppendLine("); ");
                SQL.AppendLine("delete FROM tblVoucherhdr ");
                SQL.AppendLine("WHERE  docdt = @DocDt And Remark=@Remark AND (CreateBy = 'POTP' OR Createby = 'DIKA'); ");

                SQL.AppendLine("Insert Into TblVoucherHdr ");
                SQL.AppendLine("(DocNo, DocDt, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ");
                SQL.AppendLine("CancelInd, MInd, VoucherRequestDocNo, DocType, AcType, BankAcCode, ");
                SQL.AppendLine("AcType2, BankAcCode2, PaymentType, GiroNo, BankCode, OpeningDt, DueDt, PIC, ");
                SQL.AppendLine("CurCode, Amt, CurCode2, ExcRate, AcNo, EntCode, interOfficeInd, Remark, CashTypeGrpCode, CashTypeCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values ");
                SQL.AppendLine("(@DocNo, @DocDt, @LocalDocNo, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, ");
                SQL.AppendLine("'N', @MInd, @VoucherRequestDocNo, @DocType, @AcType, @BankAcCode,  ");
                SQL.AppendLine("@AcType2, @BankAcCode2, @PaymentType, @GiroNo, @BankCode, @OpeningDt, @DueDt, ");
                SQL.AppendLine("@PIC, @CurCode, (@AmtTotal+@MateraiAmt), @CurCode2, @ExcRate, @AcNo, @EntCode, 'Y', @Remark, '1', '1.1.1.01', @CreateBy, CurrentDateTime()); ");

                SQL.AppendLine("Insert Into TblVoucherDtl(DocNo,DNo,Description,Amt,Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@DocNo, @DNo, @Description, @AmtTotal, @Remark, @CreateBy, CurrentDateTime()); ");
                SQL.AppendLine("Insert Into TblVoucherDtl(DocNo,DNo,Description,Amt,Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@DocNo, '002', 'Biaya Materai', @MateraiAmt, @Remark, @CreateBy, CurrentDateTime()); ");

            }
            else
            {
                SQL.AppendLine("SELECT null;");
                //SQL.AppendLine("Update TblVoucherHdr SET Amt=@Amt Where DocNo=@DocNo; ");
                //SQL.AppendLine("Update TblVoucherDtl SET Amt=@Amt Where DocNo=@DocNo; ");
            }

            //SQL.AppendLine("Update " + mDatabase2 + ".TblPayment2 SET ProcessInd = 'Y' ");
            //SQL.AppendLine("Where InvoiceNo=@InvoiceNo And CancelInd = 'N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VCDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@InvoiceNo", x.InvoiceNo);
            Sm.CmParam<String>(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", "");
            Sm.CmParam<String>(ref cm, "@SeqNo", "");
            Sm.CmParam<String>(ref cm, "@DeptCode", x.DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", "");
            Sm.CmParam<String>(ref cm, "@Mth", x.DocDt.Substring(4, 2));
            Sm.CmParam<String>(ref cm, "@Yr", x.DocDt.Substring(0, 4));
            Sm.CmParam<String>(ref cm, "@Revision", "");
            Sm.CmParam<String>(ref cm, "@MInd", "N");
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VRDocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "02");
            Sm.CmParam<String>(ref cm, "@AcType", "D");
            Sm.CmParam<String>(ref cm, "@Description", string.Concat(x.InvoiceNo, " ", x.DocDt));
            Sm.CmParam<String>(ref cm, "@BankAcCode", x.BankAccount);
            //CmParam<String>(ref cm, "@AcType2", "D");
            //CmParam<String>(ref cm, "@BankAcCode2", GetValue("SELECT BankAcCode FROM TblBankAccount WHERE BankAcNo = '" + x.BankAcCodeCredit + "'"));
            Sm.CmParam<String>(ref cm, "@PaymentType", "B");
            Sm.CmParam<String>(ref cm, "@GiroNo", "");
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetValue("SELECT BankCode FROM TblBankAccount WHERE BankAcCode = '" + x.BankAccount + "'"));
            Sm.CmParam<String>(ref cm, "@OpeningDt", "");
            Sm.CmParam<String>(ref cm, "@DueDt", "");
            Sm.CmParam<String>(ref cm, "@PIC", x.PICCode);
            Sm.CmParam<String>(ref cm, "@CurCode", mMainCurCode);
            //CmParam<Decimal>(ref cm, "@Amt", Amt);
            Sm.CmParam<String>(ref cm, "@CurCode2", "");
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", 0);
            Sm.CmParam<String>(ref cm, "@AcNo", "");
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetValue("SELECT EntCode FROM TblBankAccount WHERE BankAcCode = '" + x.BankAccount + "'"));
            Sm.CmParam<String>(ref cm, "@Remark", string.Concat(x.InvoiceNo));
            Sm.CmParam<String>(ref cm, "@CreateBy", x.CreateBy);
            Sm.CmParam<String>(ref cm, "@CreateDt", x.CreateDt);
            Sm.CmParam<Decimal>(ref cm, "@AmtTotal", x.TotalAmt);
            Sm.CmParam<Decimal>(ref cm, "@MateraiAmt", x.MateraiAmt);


            return cm;
        }

        private  MySqlCommand SaveJournalIPPOTP(string JournalDocNo, string VRDocNo, string DocDt, string CurCode, string VCDocNo, string CCCode, string BankAcCode, string ReceiptNo, decimal MateraiAmt)
        {
            var DocTitle = Sm.GetParameter("DocTitle");
            string DocTypeDesc = "Incoming Payment";


            var SQL = new StringBuilder();

            //SQL.AppendLine("Insert Into TblVoucherJournalProcess(DocDt, VoucherDocNo, JournalDocNo, Source) ");
            //SQL.AppendLine("Select @DocDt, @VCDocNo, @JournalDocNo, 'POTP'; ");

            JournalDocNo = Sm.GetValue(JournalDocNo);
            SQL.AppendLine("Set @JournalDocNo:=('" + JournalDocNo + "'); ");

            SQL.AppendLine("Update TblVoucherHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@VCDocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, REMARK, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DocDt, Concat('Voucher (', IfNull(@DocType, 'None'), ') : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, @CCCode, concat('Penerimaan Pendapatan POTP (Integrasi DIKA) INV No : ', Remark), ");
            SQL.AppendLine("@CreateBy,  CurrentDateTime() ");
            SQL.AppendLine("From TblVoucherHdr Where DocNo=@VCDocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, ");
            SQL.AppendLine("B.DAmt, ");
            SQL.AppendLine("B.CAmt, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            //coa bank
            SQL.AppendLine("Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    Case When C.CurCode<>@MainCurCode Then ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=B.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) ");
            SQL.AppendLine("    Else B.RateAmt End ");
            SQL.AppendLine("End ");
            SQL.AppendLine("*IfNull(A.Amt, 0.00) As DAmt, ");
            SQL.AppendLine("0.00 As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblBankAccount C On C.BankAcCode=@BankAcCode And C.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@VCDocNo ");
            SQL.AppendLine("Union All ");
            //pendapatan  parameter
            SQL.AppendLine("Select T.AcNo, ");
            SQL.AppendLine("0.00 As DAmt, ");
            SQL.AppendLine("Sum(Amt) As CAmt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select E.ParValue As AcNo, ");
            SQL.AppendLine("    Case When B.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=if(F.invoiceType = '1', B.DocDt, G.DocDt) And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0.00) End ");
            SQL.AppendLine("    *if(F.invoiceType = '1', G.TotalAmt, H.TotalAmt) As Amt ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("    Inner Join TblCustomerCategory D On C.CtCtCode=D.CtCtCode ");
            SQL.AppendLine("    Inner Join TblParameter E On E.ParCode='COAAccountPOTPRevenue' And E.ParValue Is Not Null ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl F On B.DocNo=F.DocNo And F.InvoiceType='1' ");
            SQL.AppendLine("    Left Join TblSalesInvoiceHdr G On F.InvoiceDocNo=G.DocNo ");
            SQL.AppendLine("    Left Join TblSalesInvoice2Hdr H On F.InvoiceDocNo=H.DocNo ");
            SQL.AppendLine("    Where A.DocNo=@VRDocNo ");
            //SQL.AppendLine("    Union ALL ");
            //SQL.AppendLine("    Select E.ParValue As AcNo, ");
            //SQL.AppendLine("    Case When B.CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("    IfNull(( ");
            //SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("        Where RateDt<=if(F.invoiceType = '1', B.DocDt, G.DocDt) And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("    ), 0.00) End ");
            //SQL.AppendLine("    *if(F.invoiceType = '1', G.TotalTax, H.TotalTax) As Amt ");
            //SQL.AppendLine("    From TblVoucherRequestHdr A ");
            //SQL.AppendLine("    Inner Join TblIncomingPaymentHdr B On A.DocNo=B.VoucherRequestDocNo ");
            //SQL.AppendLine("    Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            //SQL.AppendLine("    Inner Join TblCustomerCategory D On C.CtCtCode=D.CtCtCode ");
            //SQL.AppendLine("    Inner Join TblParameter E On E.ParCode='PPNTaxCode' And E.ParValue Is Not Null ");
            //SQL.AppendLine("    Inner Join TblIncomingPaymentDtl F On B.DocNo=F.DocNo And F.InvoiceType='1' ");
            //SQL.AppendLine("    Left Join TblSalesInvoiceHdr G On F.InvoiceDocNo=G.DocNo ");
            //SQL.AppendLine("    Left Join TblSalesInvoice2Hdr H On F.InvoiceDocNo=H.DocNo ");
            //SQL.AppendLine("    Where A.DocNo=@VRDocNo ");



            SQL.AppendLine(") T Group By T.AcNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblParameter A Inner Join Tbltax B ON A.Parvalue = B.TaxCode ");
            SQL.AppendLine("        Where A.parCode ='PPNTaxCode' And A.ParValue Is Not Null ");

            if(MateraiAmt>0)
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select A.Parvalue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        @MateraiAmt As CAmt ");
                SQL.AppendLine("        From TblParameter A  ");
                SQL.AppendLine("        Where A.parCode ='AcNoforAccruedMeterai' And A.ParValue Is Not Null ");
            }


            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            SQL.AppendLine("Update TblJournalDtl A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select DAmt, CAmt From (");
            SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
            SQL.AppendLine("        From TblJournalDtl Where DocNo=@JournalDocNo ");
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0 End, ");
            SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0 End ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
            SQL.AppendLine("And A.AcNo In ( ");
            SQL.AppendLine("    Select B.AcNo2 FROM Tblparameter A ");
            SQL.AppendLine("    INNER JOIN Tbltax B ON A.Parvalue = B.TaxCode ");
            SQL.AppendLine("    WHERE A.parCode ='PPNTaxCode' And A.ParValue Is Not Null ");
            SQL.AppendLine("    );");

            //SELECT B.AcNo2 FROM Tblparameter A
            //INNER JOIN Tbltax B ON A.Parvalue = B.TaxCode
            //WHERE A.parCode ='PPNTaxCode'

            SQL.AppendLine("Delete From TblJournalDtl ");
            SQL.AppendLine("Where DocNo=@JournalDocNo ");
            SQL.AppendLine("And (DAmt=0 And CAmt=0) ");
            SQL.AppendLine("And AcNo In ( ");
            SQL.AppendLine("    Select B.AcNo2 FROM Tblparameter A ");
            SQL.AppendLine("    INNER JOIN Tbltax B ON A.Parvalue = B.TaxCode ");
            SQL.AppendLine("    WHERE A.parCode ='PPNTaxCode' And A.ParValue Is Not Null ");
            SQL.AppendLine("    );");

            SQL.AppendLine("Insert Into TblDIKADtl(DocDt, ReceiptNo, Dno, JournalDocNo) ");
            SQL.AppendLine("Select @DocDt, @ReceiptNo, @Dno, @JournalDocNo  ");
            SQL.AppendLine("    On duplicate Key Update JournalDocNo=@JournalDocNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            // Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);
            Sm.CmParam<String>(ref cm, "@VRDocNo", VRDocNo);
            Sm.CmParam<String>(ref cm, "@VCDocNo", VCDocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "02");
            Sm.CmParam<String>(ref cm, "@MenuCode", "0102025004");
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@CurCode", CurCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode.Length>0 ? BankAcCode : mBankAcCodeForVRIntegrasi);
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", "DIKA");
            Sm.CmParam<String>(ref cm, "@Dno", "001");
            Sm.CmParam<String>(ref cm, "@ReceiptNo", ReceiptNo);
            Sm.CmParam<decimal>(ref cm, "@MateraiAmt", MateraiAmt);

            //CmParam<String>(ref cm, "@EntCode", GetValue("Select C.EntCode " +
            //    "From TblWarehouse A, TblCostCenter B, TblProfitCenter C " +
            //    "Where A.CCCode=B.CCCode And B.ProfitCenterCode=C.ProfitCenterCode And A.WhsCode='" + WhsCode + "';"));


            return cm;
        }

        private  MySqlCommand SaveJournalIIPPOTP(string JournalDocNo, string DocDt, string CurCode, string VCDocNo, string Tipe, string CCCode, Decimal totalTax, int Dno, string ReceiptNo)
        {
            var DocTitle = Sm.GetParameter("DocTitle");

            var SQL = new StringBuilder();

            SQL.AppendLine("Set @JournalDocNo:=(" + JournalDocNo + "); ");

            //SQL.AppendLine("Insert Into TblVoucherDtl3(DocDt, VoucherDocNo, JournalDocNo, Source) ");
            //SQL.AppendLine("Select @DocDt, @VCDocNo, @JournalDocNo, 'POTP' ; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DocDt, Concat('Voucher (', IfNull(@DocType, 'None'), ') : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, @CCCode, concat('Penerimaan Pendapatan POTP (Integrasi DIKA) INV No : ', Remark), ");
            SQL.AppendLine("@CreateBy,  CurrentDateTime() ");
            SQL.AppendLine("From TblVoucherHdr Where DocNo=@VCDocNo;");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");

            SQL.AppendLine("Select @JournalDocNo, '001' DNo, ");
            if (Tipe == "1")
            {
                SQL.AppendLine("C.ACNo2, ");
                SQL.AppendLine("0 As DAmt, ");
                SQL.AppendLine("G.TaxAmt as CAmt, ");
            }
            else
            {
                SQL.AppendLine("C.AcNo2, ");
                SQL.AppendLine("G.TaxAmt As DAmt, ");
                SQL.AppendLine("0 As CAmt, ");
            }
            SQL.AppendLine("@CreateBy,  CurrentDateTime() ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblParameter B On 0=0 And B.ParCode='PPNTaxCode' And B.ParValue Is Not Null ");
            SQL.AppendLine("Inner Join Tbltax C On B.Parvalue = C.TaxCode ");
            SQL.AppendLine("INNER JOIN tblvoucherrequesthdr D ON A.VoucherRequestDocNo = D.DocNo ");
            SQL.AppendLine("INNER JOIN tblincomingpaymenthdr E ON D.DocNo = E.VoucherRequestDocNo ");
            SQL.AppendLine("INNER JOIN tblincomingpaymentDtl F ON E.DocNo = F.Docno ");
            SQL.AppendLine("Inner JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT DocNo, SUM(Totaltax) AS taxAmt FROM tblsalesinvoicehdr ");
            SQL.AppendLine("    GROUP BY DocNo ");
            SQL.AppendLine(")G ON F.InvoiceDocNo = G.DocNo ");
            SQL.AppendLine("Where A.DocNo = @VCDocNo ");
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("Select @JournalDocNo, '002' DNo, ");
            if (Tipe == "1")
            {
                SQL.AppendLine("@AcNoRAK, ");
                SQL.AppendLine("G.TaxAmt As DAmt, ");
                SQL.AppendLine("0 As CAmt, ");
            }
            else
            {
                SQL.AppendLine("@AcNoRAK, ");
                SQL.AppendLine("0 as DAmt, ");
                SQL.AppendLine("G.TaxAmt As CAMt, ");
            }
            SQL.AppendLine("@CreateBy,  CurrentDateTime() ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblParameter B On 0=0 And B.ParCode='PPNTaxCode' And B.ParValue Is Not Null ");
            SQL.AppendLine("Inner Join Tbltax C On B.Parvalue = C.TaxCode ");
            SQL.AppendLine("INNER JOIN tblvoucherrequesthdr D ON A.VoucherRequestDocNo = D.DocNo ");
            SQL.AppendLine("INNER JOIN tblincomingpaymenthdr E ON D.DocNo = E.VoucherRequestDocNo ");
            SQL.AppendLine("INNER JOIN tblincomingpaymentDtl F ON E.DocNo = F.Docno ");
            SQL.AppendLine("Inner JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT DocNo, SUM(Totaltax) AS taxAmt FROM tblsalesinvoiceHdr ");
            SQL.AppendLine("    GROUP BY DocNo ");
            SQL.AppendLine(")G ON F.InvoiceDocNo = G.DocNo ");
            SQL.AppendLine("Where A.DocNo = @VCDocNo; ");

            //insert ke voucher dtl 3
            SQL.AppendLine("Insert into TblVoucherDtl3(DocNo, Dno, JournalDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @VCDocNo, @DNo, @JournalDocNo, @CreateBy, CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblDIKADtl(DocDt, ReceiptNo, Dno, JournalDocNo) ");
            SQL.AppendLine("Select @DocDt, @ReceiptNo, @Dno, @JournalDocNo  ");
            SQL.AppendLine("    On duplicate Key Update JournalDocNo=@JournalDocNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            //Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);
            Sm.CmParam<String>(ref cm, "@VCDocNo", VCDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("000", Dno), 3));
            Sm.CmParam<String>(ref cm, "@DocType", "02");
            Sm.CmParam<String>(ref cm, "@MenuCode", "0102025004");
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@CurCode", CurCode);
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@Remark", "Penerimaan Pendapatan POTP (Integrasi DIKA) INV No ");
            Sm.CmParam<String>(ref cm, "@CreateBy", "DIKA");
            Sm.CmParam<String>(ref cm, "@AcNoRAK", mAcNoForRAK);
            Sm.CmParam<String>(ref cm, "@ReceiptNo", ReceiptNo);

            return cm;
        }

        private  MySqlCommand SaveLogDIKA(string DocDt, string ReceiptNo, string DOCtDocNo, string SIDocNo, string IPDocNo, string VRDocno, string VCDocno, string ChannelId, string ChannelName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDIKAHdr(DocDt, ReceiptNo, DOCtDocNo, SIDocNo, IPDocNo, VRDocno, VCDocno, ChannelId, ChannelName, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocDt, @ReceiptNo, @DOCtDocNo, @SIDocNo, @IPDocNo, @VRDocno, @VCDocno, @ChannelId, @ChannelName, @CreateBy, CurrentDateTime()  ");
            SQL.AppendLine("    On duplicate Key Update DOCtDocNo=@DOCtDocNo, SIDocNo = @SIDocNo, IPDocNo = @IPDocNo, VRDocno = @VRDocno, VCDocno =@VCDocno, ChannelId=@ChannelId, ChannelName=@ChannelName, CreateBy=@CreateBy, CreateDt=CurrentDateTime() ; ");
            SQL.AppendLine("Update " + mPHTDbIntegrasi + ".TblPayment2 SET ProcessInd = 'Y' ");
            SQL.AppendLine("Where ReceiptNo=@ReceiptNo And ProcessInd = 'N' And cancelInd = 'N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@ReceiptNo", ReceiptNo);
            Sm.CmParam<String>(ref cm, "@DOCtDocNo", DOCtDocNo);
            Sm.CmParam<String>(ref cm, "@SIDocNo", SIDocNo);
            Sm.CmParam<String>(ref cm, "@IPDocNo", IPDocNo);
            Sm.CmParam<String>(ref cm, "@VRDocno", VRDocno);
            Sm.CmParam<String>(ref cm, "@VCDocNo", VCDocno);
            Sm.CmParam<String>(ref cm, "@ChannelId", ChannelId);
            Sm.CmParam<String>(ref cm, "@ChannelName", ChannelName);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);


            return cm;
        }

        //private MySqlCommand SaveLogDIKADtl(string DocDt, string ReceiptNo, string JournalDocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblDIKADtl(DocDt, ReceiptNo, Dno, JournalDocNo) ");
        //    SQL.AppendLine("Select @DocDt, @ReceiptNo, @Dno, @JournalDocNo  ");
        //    SQL.AppendLine("    On duplicate Key Update JournalDocNo=@JournalDocNo ; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocDt", DocDt);
        //    Sm.CmParam<String>(ref cm, "@ReceiptNo", ReceiptNo);
        //    Sm.CmParam<String>(ref cm, "@Dno", Sm.Right(string.Concat("000000", Row), 6));
        //    Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);


        //    return cm;
        //}


        #region  costcenter 

        private static void PrepDataCC1(ref List<zTemp2> l2, string mCCCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string parentCCCode = Sm.GetValue("Select parent From tblCostcenter Where CCCode=@Param", mCCCode);

            for (int i = 0; i < parentCCCode.Length; i++)
            {
                if (parentCCCode.Length > 0)
                {
                    if(i==0)
                    {
                        l2.Add(new zTemp2()
                        {
                            parent = mCCCode
                        }) ;
                    }
                    parentCCCode = Sm.GetValue("Select parent From tblCostcenter Where CCCode=@Param", mCCCode);
                    l2.Add(new zTemp2()
                    {
                        parent = parentCCCode
                    });
                    mCCCode = parentCCCode;
                }
            }
        }

        private static void PrepDataCC2(ref List<zTemp3> l3, string mCCCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();


            string parentCCCode = Sm.GetValue("Select parent From tblCostcenter Where CCCode=@Param", mCCCode);

            for (int i = 0; i < parentCCCode.Length; i++)
            {
                if (parentCCCode.Length > 0)
                {
                    if (i == 0)
                    {
                        l3.Add(new zTemp3()
                        {
                            parent = mCCCode
                        });
                    }
                    parentCCCode = Sm.GetValue("Select parent From tblCostcenter Where CCCode=@Param", mCCCode);
                    l3.Add(new zTemp3()
                    {
                        parent = parentCCCode
                    });
                    mCCCode = parentCCCode;
                }
            }

        }

        private static void ProcessDataCC(ref List<zTemp2> l2, ref List<zTemp3> l3, ref List<zTemp4> l4)
        {
            l4.Clear();

            int ccind1 = 0;
            int ccind2 = 0;
            int ind1 = 0;
            int ind2 = 0;
            bool it = true;
            int sequenceno = 0;


            if (l2.Count >= l3.Count)
            {
                foreach (var i in l2)
                {
                    if (it)
                    {
                        ccind1++;
                        ccind2 = 0;
                        foreach (var y in l3)
                        {
                            ccind2++;
                            if (i.parent == y.parent)
                            {
                                ind1 = ccind1;
                                ind2 = ccind2;
                                it = false;
                                //break;
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (var i in l3)
                {
                    if (it)
                    {

                        ccind2++;
                        ccind1 = 0;
                        foreach (var y in l2)
                        {
                            ccind1++;
                            if (i.parent == y.parent)
                            {
                                ind1 = ccind1;
                                ind2 = ccind2;
                                it = false;
                                //break;
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < l2.Count; i++)
            {
                if (i < ccind1 - 1)
                {
                    l4.Add(new zTemp4()
                    {
                        cccode = l2[i].parent,
                        seqno = sequenceno,
                    });
                    sequenceno++;
                }
                //else
                //    break;
            }

            sequenceno = sequenceno + l3.Count;
            for (int i = 0; i < l3.Count; i++)
            {
                if (i < ccind2 - 1)
                {
                    l4.Add(new zTemp4()
                    {
                        cccode = l3[i].parent,
                        seqno = sequenceno,
                    });
                    sequenceno--;
                }
                //else
                //    break;
            }
        }

        #endregion

        #endregion

        #region Additional Class

        //invoicing POTP
        public class zTemp10
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string CtName { get; set; }
            public string ShpName { get; set; }
            public Decimal TotalAmt { get; set; }
            public string ItName { get; set; }
            public string Batch { get; set; }
            public Decimal Qty { get; set; }
            public string Uom { get; set; }
            public Decimal Price { get; set; }
            public Decimal Amt { get; set; }
            public string Remark { get; set; }
            public string DueDt { get; set; }
            public string CurCode { get; set; }
            public Decimal TaxAmt1 { get; set; }
            public string TaxType1 { get; set; }
            public string ReceiptNo { get; set; }
            public string InvoiceNo { get; set; }
            public string BankAccount { get; set; }
            public string ChannelName { get; set; }
            public string ChannelId { get; set; }
            public string PIC { get; set; }
            public string CreateBy { get; set; }
            public string CreateDt { get; set; }
            public string LastUpBy { get; set; }
            public string LastUpDt { get; set; }
            public string SourceData { get; set; }
            public string CancelInd { get; set; }
            public string ProcessInd { get; set; }
            public string SIDocNo { get; set; }
            public string CCCode { get; set; }
            public string DeptCode { get; set; }
            public string PICCode { get; set; }

            public string CntCode  { get; set; }
            public string Address { get; set; }
            public string WhsCodeOld { get; set; }

            public decimal MateraiAmt { get; set; }
        }


        public class zTemp2
        {
            public string cccode { get; set; }
            public string parent { get; set; }

        }

        public class zTemp3
        {
            public string cccode { get; set; }
            public string parent { get; set; }

        }

        public class zTemp4
        {
            public string cccode { get; set; }
            public int seqno { get; set; }

        }

        public class cDocNo
        {
            public string cDocNoIPDocNo { get; set; }
            public string cDocNoVRDocNo { get; set; }
            public string cDocNoVCDocNo { get; set; }

            public string cDocNoReceiptNo { get; set; }
            public string cDocNoCCCode { get; set; }
            public string cDocNoCurCode { get; set; }
            public string cDocNoDocDt { get; set; }
            public decimal cDocNoTotalAmt { get; set; }
            public decimal cDocNoTotalTax { get; set; }

            public string cDocNoBankAcCode { get; set; }
            public decimal cDocNoMeteraiAmt { get; set; }
        }
        #endregion

        #region button
        private void BtnShow_Click(object sender, EventArgs e)
        {
            string Dt = Sm.Left(Sm.GetDte(DteDocDt1), 8);
            ShowDikaHdr(Dt);
        }
        #endregion
    }
}
