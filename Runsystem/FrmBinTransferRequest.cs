﻿#region Update
    // 15/06/2017 [TKG] tambah informasi lot pada saat pencarian bin
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBinTransferRequest : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        private bool mIsInventoryShowTotalQty = false;
        internal int mNumberOfInventoryUomCode = 1;
        internal FrmBinTransferRequestFind FrmFind;

        #endregion

        #region Constructor

        public FrmBinTransferRequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Bin Transfer Request";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                SetNumberOfInventoryUomCode();
                mIsInventoryShowTotalQty = (Sm.GetParameter("IsInventoryShowTotalQty") == "Y");
                SetGrd();
                SetFormControl(mState.View);

                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueWhsCode(ref LueWhsCode2);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Sequence#",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "Bin",
                        "Item's Code",
                        "",
                        
                        //6-10
                        "Item's"+Environment.NewLine+"Local Code",
                        "Item's Name",
                        "Batch#",
                        "Source",
                        "Lot",
                        
                        //11-15
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Stock",
                        "Quantity",

                        //16-20
                        "UoM",
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        50, 50, 100, 100, 20,
                        
                        //6-10
                        100, 300, 150, 150, 60,
                        
                        //11-15
                        80, 80, 80, 80, 80,
                        
                        //16-20
                        80, 80, 80, 80, 400
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColButton(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 14, 15, 17, 18 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 9, 14, 15, 16, 17, 18, 19 }, false);
            Grd1.Cols[10].Move(4);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 11;
            Sm.GrdHdrWithColWidth(Grd2, 
                new string[]{ 
                    //0
                    "Sequence#",
                        
                    //1-5
                    "Cancel",
                    "Old Cancel",
                    "",
                    "Bin",
                    "Quantity",

                    //6-10
                    "UoM",
                    "Quantity 2",
                    "Uom 2",
                    "Quantity 3",
                    "Uom 3"
                }, 
                new int[] { 
                    //0
                    50,
 
                    //1-5
                    50, 50, 20, 200, 80,

                    //6-10
                    80, 80, 80, 80, 80
                } );

            Sm.GrdColCheck(Grd2, new int[] { 1, 2 });
            Sm.GrdColButton(Grd2, new int[] { 3 });
            Sm.GrdFormatDec(Grd2, new int[] { 5, 7, 9 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2, 7, 8, 9, 10 }, false);

            #endregion

            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 9 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 7, 8 }, true);
                PanelTotal2.Visible = true;
            }
            
            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 7, 8, 9, 10 }, true);
                PanelTotal2.Visible = true;
                PanelTotal3.Visible = true;
            }
            
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 12, 15, 18, 20 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 3 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, LueWhsCode2,
                MeeRemark, TxtInventoryUomCode, TxtInventoryUomCode2, TxtInventoryUomCode3
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtTotal, TxtTotal2, TxtTotal3 
            }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 11, 12, 14, 15, 17, 18 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdBoolValueFalse(ref Grd2, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 5, 7, 9 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBinTransferRequestFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            Sm.SetDteCurrentDate(DteDocDt);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (Sm.IsGrdColSelected(new int[] { 12, 15, 18, 20 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 12, 14, 15, 17, 18 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 11, 12, 14, 15, 17, 18 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 20 }, e);

            if (e.ColIndex == 12 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 13), Sm.GetGrdStr(Grd1, e.RowIndex, 16)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 15, Grd1, e.RowIndex, 12);

            if (e.ColIndex == 12 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 13), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 12);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);

            if (Sm.IsGrdColSelected(new int[] { 12, 15, 18 }, e.ColIndex)) ShowBinInfo();
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 11, 12, 14, 15, 17, 18 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += 
                        Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Additional Method

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal string GetSelectedBin()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 4).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 4) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 3).Length != 0)
                        SQL += 
                            "##" + 
                            Sm.GetGrdStr(Grd1, Row, 3) +
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 9) +
                            Sm.GetGrdStr(Grd1, Row, 10) + 
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GetSelectedItem2()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 3).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 9) +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    int No=1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 8);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 9), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 10), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 3), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 11, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 14, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 17, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        internal void InsertItem(string Bin)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Bin, A.ItCode, B.ItCodeInternal, B.ItName, A.BatchNo, A.Source, A.Lot, ");
            SQL.AppendLine("A.Qty, A.Qty2, A.Qty3, B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3 ");
            SQL.AppendLine("From TblStockSummary A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where IfNull(A.Bin, '')=@Bin ");
            SQL.AppendLine("And WhsCode=@WhsCode ");
            SQL.AppendLine("And (A.Qty>0) ");
            SQL.AppendLine("Order By B.ItName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Bin", Bin);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                        new string[] 
                        { 
                            //0
                            "Bin", 

                            //1-5
                            "ItCode", "ItCodeInternal", "ItName", "BatchNo", "Source",  
                            
                            //6-10
                            "Lot", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2",   
                            
                            //11-12
                            "Qty3", "InventoryUomCode3" 
                        }
                        );
                    if (!dr.HasRows)
                    {
                        Sm.StdMsg(mMsgType.NoData, "");
                    }
                    else
                    {
                        Grd1.BeginUpdate();
                        if (Grd1.Rows.Count >= 1)
                        {
                            if (Sm.GetGrdStr(Grd1, Grd1.Rows.Count - 1, 3).Length != 0)
                                Grd1.Rows.Add();
                        }
                        else
                            Grd1.Rows.Add();
                        int Row = Grd1.Rows.Count-1;
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Grd1.Cells[Row, 1].Value = false;
                            Grd1.Cells[Row, 2].Value = false;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 10);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 11);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 12);
                            Row++;
                        }
                        dr.Dispose();
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 12, 14, 15, 17, 18 });

                        ShowBinInfo();
                        Sm.FocusGrd(Grd1, 0, 1);
                        Grd1.EndUpdate();
                    }
                }
                cm.Dispose();
            }
        }

        private void ShowBinInfo()
        {
            decimal Total = 0m, Total2 = 0m, Total3 = 0m;
            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd2, row, 4).Length != 0)
                {
                    ShowBinInfo(Sm.GetGrdStr(Grd2, row, 4), row);
                    if (!Sm.GetGrdBool(Grd2, row, 1))
                    {
                        Total += Sm.GetGrdDec(Grd2, row, 5);
                        Total2 += Sm.GetGrdDec(Grd2, row, 7);
                        Total3 += Sm.GetGrdDec(Grd2, row, 9);
                    }
                }
            }
            TxtTotal.EditValue = Sm.FormatNum(Total, 0);
            TxtTotal2.EditValue = Sm.FormatNum(Total2, 0);
            TxtTotal3.EditValue = Sm.FormatNum(Total3, 0);
        }   

        private void ShowBinInfo(string Bin, int Row)
        {
            bool IsFirst = true;
            decimal Qty = 0m, Qty2 = 0m, Qty3 = 0m;
            for (int Index = 0; Index < Grd1.Rows.Count; Index++)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Index, 3), Bin) && !Sm.GetGrdBool(Grd1, Index, 1))
                {
                    if (IsFirst)
                    {
                        Grd2.Cells[Row, 6].Value = Sm.GetGrdStr(Grd1, Index, 13);
                        Grd2.Cells[Row, 8].Value = Sm.GetGrdStr(Grd1, Index, 16);
                        Grd2.Cells[Row, 10].Value = Sm.GetGrdStr(Grd1, Index, 19);
                        TxtInventoryUomCode.Text = Sm.GetGrdStr(Grd1, Index, 13);
                        TxtInventoryUomCode2.Text = Sm.GetGrdStr(Grd1, Index, 16);
                        TxtInventoryUomCode3.Text = Sm.GetGrdStr(Grd1, Index, 19);
                        IsFirst = false;
                    }
                    Qty += Sm.GetGrdDec(Grd1, Index, 12);
                    Qty2 += Sm.GetGrdDec(Grd1, Index, 15);
                    Qty3 += Sm.GetGrdDec(Grd1, Index, 18);
                }
            }
            Grd2.Cells[Row, 5].Value = Qty;
            Grd2.Cells[Row, 7].Value = Qty2;
            Grd2.Cells[Row, 9].Value = Qty3;
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BinTransferRequest", "TblBinTransferRequestHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveBinTransferRequestHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0) cml.Add(SaveBinTransferRequestDtl2(DocNo, Row));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 4).Length > 0) cml.Add(SaveBinTransferRequestDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            ReComputeStock();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse (From)") ||
                Sm.IsLueEmpty(LueWhsCode2, "Warehouse (To)") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Requested data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Bin is empty.")) return true;

                Msg =
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                    "Item Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item Name : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Item Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Batch Number : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 12) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 12) > Sm.GetGrdDec(Grd1, Row, 11))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should not be bigger than available stock.");
                    return true;
                }

                if (Grd1.Cols[15].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 15) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 15) > Sm.GetGrdDec(Grd1, Row, 14))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should not be bigger than available stock (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[18].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 18) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 18) > Sm.GetGrdDec(Grd1, Row, 17))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should not be bigger than available stock (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveBinTransferRequestHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblBinTransferRequestHdr(DocNo, ProcessInd, DocDt, WhsCode, WhsCode2, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, 'O', @DocDt, @WhsCode, @WhsCode2, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveBinTransferRequestDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblBinTransferRequestDtl(DocNo, DNo, Bin, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @Bin, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBinTransferRequestDtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblBinTransferRequestDtl2(DocNo, DNo, CancelInd, ItCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, 'N', @ItCode, @BatchNo, @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            UpdateCancelledItem();

            string Bin = "'XXX'";

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd2, Row, 1) && !Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdStr(Grd2, Row, 4).Length > 0)
                    Bin = Bin + ",'" + Sm.GetGrdStr(Grd2, Row, 4) + "'";

            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid(Bin)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelBinTransferRequest(Bin));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblBinTransferRequestDtl2 " +
                        "Where DocNo=@DocNo Order By DNo"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                {
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                }
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string Bin)
        {
            return
                IsCancelledBinNotExisted(Bin) ||
                IsBinTransferRequestAlreadyProcessed(Bin);
        }

        private bool IsCancelledBinNotExisted(string Bin)
        {
            if (Sm.CompareStr(Bin, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel minimum 1 bin.");
                return true;
            }
            return false;
        }

        private bool IsBinTransferRequestAlreadyProcessed(string Bin)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblBinTransferRequestDtl " +
                    "Where IfNull(ProcessInd, 'O')<>'O' " +
                    "And CancelInd='N' " + 
                    "And DocNo=@DocNo " +
                    "And Bin In (" + Bin + "); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This requested data already processed.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelBinTransferRequest(string Bin)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBinTransferRequestDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Bin In (" + Bin + "); ");

            SQL.AppendLine("Update TblBinTransferRequestDtl2 Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Bin In (" + Bin + "); ");

            SQL.AppendLine("Update TblBinTransferRequestHdr Set ");
            SQL.AppendLine("    ProcessInd='F', LastUpBy=@UserCode, LastUpDt=CurrentDateTime()  ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblBinTransferRequestDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And ProcessInd='O'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowBinTransferRequestHdr(DocNo);
                ShowBinTransferRequestDtl(DocNo);
                ShowBinTransferRequestDtl2(DocNo);
                ShowBinInfo();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBinTransferRequestHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    //"Select DocNo, DocDt, WhsCode, WhsCode2, Remark, " +
                    //"(Select DocNo From TblBinTransferHdr Where BinTransferRequestDocNo=T.DocNo) As BinTransferDocNo " +
                    //"From TblBinTransferRequestHdr T Where T.DocNo=@DocNo;",

                    "Select DocNo, DocDt, WhsCode, WhsCode2, Remark " +
                    "From TblBinTransferRequestHdr T Where T.DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        //"DocDt", "WhsCode", "WhsCode2", "BinTransferDocNo", "Remark"
                        "DocDt", "WhsCode", "WhsCode2", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueWhsCode2, Sm.DrStr(dr, c[3]));
                        //TxtBinTransferDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        //MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowBinTransferRequestDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, CancelInd, Bin ");
            SQL.AppendLine("From TblBinTransferRequestDtl ");
            SQL.AppendLine("Where DocNo=@DocNo Order By Bin;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-2
                    "CancelInd", "Bin"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd2, Grd2.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 5, 7, 9 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowBinTransferRequestDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.CancelInd, B.Bin, B.ItCode, C.ItCodeInternal, C.ItName, B.BatchNo, B.Source, B.Lot, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, B.Remark ");
            SQL.AppendLine("From TblBinTransferRequestHdr A ");
            SQL.AppendLine("Inner Join TblBinTransferRequestDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.Bin, C.ItName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "Bin", "ItCode", "ItCodeInternal", "ItName",  
                    
                    //6-10
                    "BatchNo", "Source", "Lot", "Qty", "InventoryUomCode", 
                    
                    //11-15
                    "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", "Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 15);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 12, 14, 15, 17, 18 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtInventoryUomCode, TxtInventoryUomCode2, TxtInventoryUomCode3
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtTotal, TxtTotal2, TxtTotal3 
            }, 0);
            ClearGrd();
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        #endregion

        #region Grid Event

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmBinTransferRequestDlg(this, Sm.GetLue(LueWhsCode)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd2, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd2, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd2, e.RowIndex, 2) || Sm.GetGrdStr(Grd2, e.RowIndex, 4).Length == 0))
                    {
                        e.DoDefault = false;
                    }
                }
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
            {
                Sm.FormShowDialog(new FrmBinTransferRequestDlg(this, Sm.GetLue(LueWhsCode)));
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                if (Grd2.SelectedRows.Count > 0)
                {
                    if (Grd2.Rows[Grd2.Rows[Grd2.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd2.SelectedRows.Count - 1; Index >= 0; Index--)
                            {
                                int Row = Grd2.SelectedRows[Index].Index;
                                for (int Index2 = Grd1.Rows.Count - 1; Index2 >= 0; Index2--)
                                {
                                    if (Sm.CompareStr(
                                            Sm.GetGrdStr(Grd1, Index2, 3), 
                                            Sm.GetGrdStr(Grd2, Row, 4)
                                            ))
                                        Grd1.Rows.RemoveAt(Index2);
                                }
                                if (Grd1.Rows.Count <= 0) Grd1.Rows.Add();
                                Grd2.Rows.RemoveAt(Row);
                            }
                            if (Grd2.Rows.Count <= 0) Grd2.Rows.Add();
                            ShowBinInfo();
                        }
                    }
                }
            }
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd2, e.RowIndex, 4).Length != 0)
            {
                bool CancelInd = Sm.GetGrdBool(Grd2, e.RowIndex, 1);
                for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 3).Length != 0 &&
                        Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 3), Sm.GetGrdStr(Grd2, e.RowIndex, 4)))
                        Grd1.Cells[Row, 1].Value = CancelInd;
                }
                ShowBinInfo();
            }
        }

        #endregion

        #endregion
    }
}
