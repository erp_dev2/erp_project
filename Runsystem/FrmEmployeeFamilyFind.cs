﻿#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmployeeFamilyFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmEmployeeFamily mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmEmployeeFamilyFind(FrmEmployeeFamily FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode);
                SetLueStatus(ref LueStatus);
                Sm.SetLue(LueStatus, "1");
                SetLueSite(ref LueSiteCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code", 
                        "Employee's Name",
                        "Old Code", 
                        "Department",
                        "Position",
 
                        //6-10
                        "Site",
                        "Join Date",
                        "Family Name",
                        "Status",
                        "Gender",

                        //11-15
                        "Birth Date",
                        "Id Card#",
                        "Created"+Environment.NewLine+"By",  
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time", 
                        
                        //16-18
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        80, 170, 80, 150, 180, 

                        //6-10
                        150, 80, 170, 80, 70, 

                        //11-15
                        80, 150, 100, 100, 100,

                        //16-18
                        100, 100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 7, 11, 14, 17 });
            Sm.GrdFormatTime(Grd1, new int[] { 15, 18 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 13, 14, 15, 16, 17, 18 }, false);
            Sm.SetGrdProperty(Grd1, false);
           }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.DeptName, C.PosName, D.SiteName, A.JoinDt, ");
            SQL.AppendLine("E.FamilyName, F.OptDesc As Status, G.OptDesc As Gender, E.BirthDt, E.IDNo, ");
            SQL.AppendLine("E.CreateBy, E.CreateDt, E.LastUpBy, E.LastUpDt");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblSite D On A.SiteCode=D.SiteCode ");
            SQL.AppendLine("Left Join TblEmployeeFamily E On A.EmpCode=E.EmpCode ");
            SQL.AppendLine("Left Join TblOption F On F.OptCat='FamilyStatus' And E.Status=F.OptCode ");
            SQL.AppendLine("Left Join TblOption G On G.OptCat='Gender' And E.Gender=G.OptCode ");
            
            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 13, 14, 15, 16, 17, 18 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                switch (Sm.GetLue(LueStatus))
                {
                    case "1":
                        Filter = " Where A.ResignDt Is Null ";
                        break;
                    case "2":
                        Filter = " Where A.ResignDt Is Not Null ";
                        break;
                }

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpCodeOld", "A.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By DeptName, EmpName;",
                        new string[]
                        {
                            //0
                            "EmpCode",
 
                            //1-5
                            "EmpName","EmpCodeOld","DeptName","PosName", "SiteName", 
                            
                            //6-10
                            "JoinDt", "FamilyName", "Status", "Gender", "BirthDt",
                            
                            //11-15
                            "IdNo", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 18, 15);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Additional Method

        private void SetLueSite(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T Where T.ActInd='Y' ");
                SQL.AppendLine("Order By SiteName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Active Employee' As Col2 Union " +
                "Select '2' As Col1, 'Resignee' As Col2; ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
           Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this,sender,"Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employee's status");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(SetLueSite));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

    }
}
