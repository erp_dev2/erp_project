﻿#region Update
/*
    28/01/2018 [TKG] tambah info dan filter payrun period  
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRLPFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmRLP mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRLPFind(FrmRLP FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.VoucherRequestDocNo, C.VoucherDocNo, D.OptDesc As RLPDesc, A.Yr, ");
            SQL.AppendLine("B.EmpCode, E.EmpName, E.EmpCodeOld, F.PosName, G.DeptName, H.SiteName, I.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("B.NoOfDay, A.CurCode, B.Amt, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblRLPHdr A ");
            SQL.AppendLine("Inner Join TblRLPDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr C On A.VoucherRequestDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblOption D On A.RLPCode=D.OptCode And D.OptCat='ResidualLeave' ");
            SQL.AppendLine("Inner Join TblEmployee E On B.EmpCode=E.EmpCode ");
            if (!mFrmParent.mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And E.GrdLvlCode In ( ");
                SQL.AppendLine("    Select T2.GrdLvlCode ");
                SQL.AppendLine("    From TblPPAHdr T1 ");
                SQL.AppendLine("    Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Left Join TblPosition F On E.PosCode=F.PosCode ");
            SQL.AppendLine("Inner Join TblDepartment G On A.DeptCode=G.DeptCode ");
            SQL.AppendLine("Left Join TblSite H On A.SiteCode=H.SiteCode ");
            SQL.AppendLine("Left Join TblOption I On E.PayrunPeriod=I.OptCode And I.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "Status",
                    "Voucher"+Environment.NewLine+"Request#",
                    
                    //6-10
                    "Voucher#",
                    "Residual"+Environment.NewLine+"Leave",
                    "Year",
                    "Employee's"+Environment.NewLine+"Code",
                    "Employee's Name",

                    //11-15
                    "Old Code",
                    "Position",
                    "Department",
                    "Site",
                    "Payrun"+Environment.NewLine+"Period",
                    
                    //16-20
                    "Number"+Environment.NewLine+"of Days",
                    "Currency",
                    "Amount",
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date", 
                    
                    
                    //21-24
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date",
                    "Last"+Environment.NewLine+"Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 80, 80, 150, 
                    
                    //6-10
                    150, 150, 80, 80, 200,  
                    
                    //11-15
                    100, 150, 150, 150, 150, 

                    //16-20
                    100, 80, 130, 100, 100, 

                    //21-24
                    100, 100, 100, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 16, 18 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 20, 23 });
            Sm.GrdFormatTime(Grd1, new int[] { 21, 24 });
            Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21, 22, 23, 24 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21, 22, 23, 24 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "E.EmpCode", "E.EmpName", "E.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePayrunPeriod), "E.PayrunPeriod", true);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "StatusDesc", "VoucherRequestDocNo", "VoucherDocNo", 
                            
                            //6-10
                            "RLPDesc", "Yr", "EmpCode", "EmpName", "EmpCodeOld", 
                            
                            //11-15
                            "PosName", "DeptName", "SiteName", "PayrunPeriodDesc", "NoOfDay", 
                            
                            //16-20
                            "CurCode", "Amt", "CreateBy", "CreateDt", "LastUpBy", 
                            
                            //21
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 21);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPayrunPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Payrun period");
        }

        #endregion

        #endregion
    }
}
