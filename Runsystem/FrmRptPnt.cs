﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using System.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptPnt : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptPnt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -14);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private string GetSQL(string Filter, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocDt, T2.DocName, T1.EmpCode, T3.EmpName, T3.EmpCodeOld, T4.DeptName, ");
            SQL.AppendLine("T1.ItCode, T5.ItName, T1.BatchNo, T1.Qty, T5.PlanningUomCode, T1.Qty2, T5.PlanningUomCode2 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T.DocDt, T.WorkCenterDocNo, T.ItCode, T.BatchNo, T.EmpCode, Sum(T.Qty) Qty, Sum(T.Qty2) Qty2 From ( ");
            SQL.AppendLine("        Select A.DocDt, A.WorkCenterDocNo, C.ItCode, C.BatchNo, B.EmpCode, B.Qty, B.Qty2 ");
            SQL.AppendLine("        From TblPntHdr A ");
            SQL.AppendLine("        Inner Join TblPntDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblShopFloorControlDtl C On B.ShopFloorControlDocNo=C.DocNo And B.ShopFloorControlDNo=C.DNo ");
            SQL.AppendLine("        Where A.CancelInd='N' " + Filter2);
            //SQL.AppendLine(" Union All ");
            //SQL.AppendLine("Select A.DocDt, A.WorkCenterDocNo, C.ItCode, C.BatchNo, D.EmpCode, B.Qty, B.Qty2 ");
            //SQL.AppendLine("From TblPnt2Hdr A ");
            //SQL.AppendLine("Inner Join TblPnt2Dtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("Inner Join TblShopFloorControlDtl C On B.ShopFloorControlDocNo=C.DocNo And B.ShopFloorControlDNo=C.DNo ");
            //SQL.AppendLine("Inner Join TblPnt2Dtl3 D On A.DocNo = D.DocNo ");
            //SQL.AppendLine("Where A.CancelInd='N' " + Filter2);
            SQL.AppendLine("    ) T Group By T.DocDt, T.WorkCenterDocNo, T.ItCode, T.BatchNo, T.EmpCode ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblWorkCenterHdr T2 On T1.WorkCenterDocNo=T2.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee T3 On T1.EmpCode=T3.EmpCode ");
            SQL.AppendLine("Inner Join TblDepartment T4 On T3.DeptCode=T4.DeptCode ");
            SQL.AppendLine("Inner Join TblItem T5 On T1.ItCode=T5.ItCode ");
            SQL.AppendLine(Filter);

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Date",
                        "Work Center",
                        "Employee's Code",
                        "Employee's Name",
                        "Old Code",

                        //6-10
                        "Department",
                        "Item's Code",
                        "Item's Name",
                        "Batch#",
                        "Quantity",
                        
                        //11-13
                        "Uom", 
                        "Quantity",
                        "Uom"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 250, 100, 200, 100, 
                        
                        //6-10
                        200, 100, 200, 150, 100, 
                        
                        //11-13
                        80, 100, 80
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 7 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty, Filter2 = " ";
                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter2, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenterDocNo.Text, new string[] { "T1.WorkCenterDocNo", "T2.DocName" });
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T3.EmpCode", "T3.EmpCodeOld", "T3.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T3.DeptCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, 
                    GetSQL(Filter, Filter2) + " Order By T1.DocDt, T2.DocName, T3.EmpName;",
                    new string[] 
                        { 
                            //0
                            "DocDt",  

                            //1-5
                            "DocName", "EmpCode", "EmpName", "EmpCodeOld", "DeptName",  
                            
                            //6-10
                            "ItCode", "ItName", "BatchNo", "Qty", "PlanningUomCode",    
                            
                            //11-12
                            "Qty2", "PlanningUomCode2"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row+1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 12 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion 

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtWorkCenterDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWorkCenterDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work Center#");
        }

                private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #endregion
    }
}
