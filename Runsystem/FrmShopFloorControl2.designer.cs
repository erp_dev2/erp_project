﻿namespace RunSystem
{
    partial class FrmShopFloorControl2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmShopFloorControl2));
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.LblBatchCategoryCode = new System.Windows.Forms.Label();
            this.LueBatchCategoryCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MeePPRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnPPDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.LblProductionWorkGroup = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LueProductionWorkGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.LueMachineCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtWhsCode2 = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtWorkCenterDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnWorkCenterDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtWorkCenterDocName = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueProductionShiftCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPPDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnPPDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnBOM = new System.Windows.Forms.Button();
            this.LueBatchNo = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.LueItCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnProductionResult = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueBatchCategoryCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeePPRemark.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionWorkGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMachineCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWhsCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkCenterDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkCenterDocName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionShiftCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBatchNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Location = new System.Drawing.Point(805, 0);
            this.panel1.Size = new System.Drawing.Size(70, 473);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.splitContainer1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(805, 473);
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 431);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 8;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            this.LueFontSize.EditValueChanged += new System.EventHandler(this.LueFontSize_EditValueChanged);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 451);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 9;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.LblBatchCategoryCode);
            this.panel3.Controls.Add(this.LueBatchCategoryCode);
            this.panel3.Controls.Add(this.MeePPRemark);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.BtnPPDocNo2);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.LueProductionShiftCode);
            this.panel3.Controls.Add(this.TxtPPDocNo);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.BtnPPDocNo);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(805, 151);
            this.panel3.TabIndex = 10;
            // 
            // LblBatchCategoryCode
            // 
            this.LblBatchCategoryCode.AutoSize = true;
            this.LblBatchCategoryCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBatchCategoryCode.ForeColor = System.Drawing.Color.Black;
            this.LblBatchCategoryCode.Location = new System.Drawing.Point(41, 112);
            this.LblBatchCategoryCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBatchCategoryCode.Name = "LblBatchCategoryCode";
            this.LblBatchCategoryCode.Size = new System.Drawing.Size(38, 14);
            this.LblBatchCategoryCode.TabIndex = 36;
            this.LblBatchCategoryCode.Text = "Batch";
            this.LblBatchCategoryCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBatchCategoryCode
            // 
            this.LueBatchCategoryCode.EnterMoveNextControl = true;
            this.LueBatchCategoryCode.Location = new System.Drawing.Point(83, 109);
            this.LueBatchCategoryCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBatchCategoryCode.Name = "LueBatchCategoryCode";
            this.LueBatchCategoryCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchCategoryCode.Properties.Appearance.Options.UseFont = true;
            this.LueBatchCategoryCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchCategoryCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBatchCategoryCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchCategoryCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBatchCategoryCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchCategoryCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBatchCategoryCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchCategoryCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBatchCategoryCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBatchCategoryCode.Properties.DropDownRows = 30;
            this.LueBatchCategoryCode.Properties.NullText = "[Empty]";
            this.LueBatchCategoryCode.Properties.PopupWidth = 300;
            this.LueBatchCategoryCode.Size = new System.Drawing.Size(231, 20);
            this.LueBatchCategoryCode.TabIndex = 37;
            this.LueBatchCategoryCode.ToolTip = "F4 : Show/hide list";
            this.LueBatchCategoryCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBatchCategoryCode.EditValueChanged += new System.EventHandler(this.LueBatchCategoryCode_EditValueChanged);
            // 
            // MeePPRemark
            // 
            this.MeePPRemark.EnterMoveNextControl = true;
            this.MeePPRemark.Location = new System.Drawing.Point(83, 87);
            this.MeePPRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeePPRemark.Name = "MeePPRemark";
            this.MeePPRemark.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeePPRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePPRemark.Properties.Appearance.Options.UseBackColor = true;
            this.MeePPRemark.Properties.Appearance.Options.UseFont = true;
            this.MeePPRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePPRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeePPRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePPRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeePPRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePPRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeePPRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePPRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeePPRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeePPRemark.Properties.MaxLength = 400;
            this.MeePPRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeePPRemark.Properties.ReadOnly = true;
            this.MeePPRemark.Properties.ShowIcon = false;
            this.MeePPRemark.Size = new System.Drawing.Size(231, 22);
            this.MeePPRemark.TabIndex = 35;
            this.MeePPRemark.ToolTip = "F4 : Show/hide text";
            this.MeePPRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeePPRemark.ToolTipTitle = "Run System";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(32, 91);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 14);
            this.label4.TabIndex = 34;
            this.label4.Text = "Remark";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPPDocNo2
            // 
            this.BtnPPDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPPDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPPDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPPDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPPDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnPPDocNo2.Appearance.Options.UseFont = true;
            this.BtnPPDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnPPDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnPPDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPPDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPPDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnPPDocNo2.Image")));
            this.BtnPPDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPPDocNo2.Location = new System.Drawing.Point(284, 66);
            this.BtnPPDocNo2.Name = "BtnPPDocNo2";
            this.BtnPPDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnPPDocNo2.TabIndex = 21;
            this.BtnPPDocNo2.ToolTip = "Show Production Planning Information";
            this.BtnPPDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPPDocNo2.ToolTipTitle = "Run System";
            this.BtnPPDocNo2.Click += new System.EventHandler(this.BtnPPDocNo2_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.LblProductionWorkGroup);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.LueProductionWorkGroup);
            this.panel5.Controls.Add(this.LueMachineCode);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.TxtWhsCode2);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.TxtWorkCenterDocNo);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.LueWhsCode);
            this.panel5.Controls.Add(this.BtnWorkCenterDocNo);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.TxtWorkCenterDocName);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(336, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(469, 151);
            this.panel5.TabIndex = 22;
            // 
            // LblProductionWorkGroup
            // 
            this.LblProductionWorkGroup.AutoSize = true;
            this.LblProductionWorkGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblProductionWorkGroup.ForeColor = System.Drawing.Color.Red;
            this.LblProductionWorkGroup.Location = new System.Drawing.Point(84, 132);
            this.LblProductionWorkGroup.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblProductionWorkGroup.Name = "LblProductionWorkGroup";
            this.LblProductionWorkGroup.Size = new System.Drawing.Size(40, 14);
            this.LblProductionWorkGroup.TabIndex = 34;
            this.LblProductionWorkGroup.Text = "Group";
            this.LblProductionWorkGroup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(71, 48);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 14);
            this.label5.TabIndex = 34;
            this.label5.Text = "Machine";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProductionWorkGroup
            // 
            this.LueProductionWorkGroup.EnterMoveNextControl = true;
            this.LueProductionWorkGroup.Location = new System.Drawing.Point(126, 129);
            this.LueProductionWorkGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueProductionWorkGroup.Name = "LueProductionWorkGroup";
            this.LueProductionWorkGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.Appearance.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProductionWorkGroup.Properties.DropDownRows = 30;
            this.LueProductionWorkGroup.Properties.NullText = "[Empty]";
            this.LueProductionWorkGroup.Properties.PopupWidth = 300;
            this.LueProductionWorkGroup.Size = new System.Drawing.Size(318, 20);
            this.LueProductionWorkGroup.TabIndex = 35;
            this.LueProductionWorkGroup.ToolTip = "F4 : Show/hide list";
            this.LueProductionWorkGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProductionWorkGroup.EditValueChanged += new System.EventHandler(this.LueProductionWorkGroup_EditValueChanged);
            // 
            // LueMachineCode
            // 
            this.LueMachineCode.EnterMoveNextControl = true;
            this.LueMachineCode.Location = new System.Drawing.Point(126, 45);
            this.LueMachineCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueMachineCode.Name = "LueMachineCode";
            this.LueMachineCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMachineCode.Properties.Appearance.Options.UseFont = true;
            this.LueMachineCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMachineCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMachineCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMachineCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMachineCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMachineCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMachineCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMachineCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMachineCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMachineCode.Properties.DropDownRows = 30;
            this.LueMachineCode.Properties.NullText = "[Empty]";
            this.LueMachineCode.Properties.PopupWidth = 350;
            this.LueMachineCode.Size = new System.Drawing.Size(318, 20);
            this.LueMachineCode.TabIndex = 35;
            this.LueMachineCode.ToolTip = "F4 : Show/hide list";
            this.LueMachineCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueMachineCode.EditValueChanged += new System.EventHandler(this.LueMachineCode_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(10, 69);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 14);
            this.label3.TabIndex = 28;
            this.label3.Text = "Transfer BOM From";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWhsCode2
            // 
            this.TxtWhsCode2.EnterMoveNextControl = true;
            this.TxtWhsCode2.Location = new System.Drawing.Point(126, 66);
            this.TxtWhsCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWhsCode2.Name = "TxtWhsCode2";
            this.TxtWhsCode2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtWhsCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWhsCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWhsCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtWhsCode2.Properties.MaxLength = 16;
            this.TxtWhsCode2.Properties.ReadOnly = true;
            this.TxtWhsCode2.Size = new System.Drawing.Size(318, 20);
            this.TxtWhsCode2.TabIndex = 29;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(9, 90);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(113, 14);
            this.label21.TabIndex = 30;
            this.label21.Text = "Transfer Results To";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWorkCenterDocNo
            // 
            this.TxtWorkCenterDocNo.EnterMoveNextControl = true;
            this.TxtWorkCenterDocNo.Location = new System.Drawing.Point(126, 3);
            this.TxtWorkCenterDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWorkCenterDocNo.Name = "TxtWorkCenterDocNo";
            this.TxtWorkCenterDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtWorkCenterDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWorkCenterDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWorkCenterDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtWorkCenterDocNo.Properties.MaxLength = 16;
            this.TxtWorkCenterDocNo.Properties.ReadOnly = true;
            this.TxtWorkCenterDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtWorkCenterDocNo.TabIndex = 24;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(10, 27);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(112, 14);
            this.label14.TabIndex = 26;
            this.label14.Text = "Work Center Name";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(126, 87);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 30;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 350;
            this.LueWhsCode.Size = new System.Drawing.Size(318, 20);
            this.LueWhsCode.TabIndex = 31;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // BtnWorkCenterDocNo
            // 
            this.BtnWorkCenterDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnWorkCenterDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnWorkCenterDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnWorkCenterDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnWorkCenterDocNo.Appearance.Options.UseBackColor = true;
            this.BtnWorkCenterDocNo.Appearance.Options.UseFont = true;
            this.BtnWorkCenterDocNo.Appearance.Options.UseForeColor = true;
            this.BtnWorkCenterDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnWorkCenterDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnWorkCenterDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnWorkCenterDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnWorkCenterDocNo.Image")));
            this.BtnWorkCenterDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnWorkCenterDocNo.Location = new System.Drawing.Point(293, 2);
            this.BtnWorkCenterDocNo.Name = "BtnWorkCenterDocNo";
            this.BtnWorkCenterDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnWorkCenterDocNo.TabIndex = 25;
            this.BtnWorkCenterDocNo.ToolTip = "Show Work Center Information";
            this.BtnWorkCenterDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnWorkCenterDocNo.ToolTipTitle = "Run System";
            this.BtnWorkCenterDocNo.Click += new System.EventHandler(this.BtnWorkCenterDocNo_Click);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(126, 108);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(318, 20);
            this.MeeRemark.TabIndex = 33;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(36, 6);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 14);
            this.label13.TabIndex = 23;
            this.label13.Text = "Work Center#";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWorkCenterDocName
            // 
            this.TxtWorkCenterDocName.EnterMoveNextControl = true;
            this.TxtWorkCenterDocName.Location = new System.Drawing.Point(126, 24);
            this.TxtWorkCenterDocName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWorkCenterDocName.Name = "TxtWorkCenterDocName";
            this.TxtWorkCenterDocName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtWorkCenterDocName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWorkCenterDocName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWorkCenterDocName.Properties.Appearance.Options.UseFont = true;
            this.TxtWorkCenterDocName.Properties.MaxLength = 16;
            this.TxtWorkCenterDocName.Properties.ReadOnly = true;
            this.TxtWorkCenterDocName.Size = new System.Drawing.Size(318, 20);
            this.TxtWorkCenterDocName.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(75, 111);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 14);
            this.label10.TabIndex = 32;
            this.label10.Text = "Remark";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(251, 3);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 13;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(47, 48);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 14);
            this.label12.TabIndex = 16;
            this.label12.Text = "Shift";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProductionShiftCode
            // 
            this.LueProductionShiftCode.EnterMoveNextControl = true;
            this.LueProductionShiftCode.Location = new System.Drawing.Point(83, 45);
            this.LueProductionShiftCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueProductionShiftCode.Name = "LueProductionShiftCode";
            this.LueProductionShiftCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.Appearance.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProductionShiftCode.Properties.DropDownRows = 30;
            this.LueProductionShiftCode.Properties.NullText = "[Empty]";
            this.LueProductionShiftCode.Properties.PopupWidth = 300;
            this.LueProductionShiftCode.Size = new System.Drawing.Size(231, 20);
            this.LueProductionShiftCode.TabIndex = 17;
            this.LueProductionShiftCode.ToolTip = "F4 : Show/hide list";
            this.LueProductionShiftCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProductionShiftCode.EditValueChanged += new System.EventHandler(this.LueProductionShiftCode_EditValueChanged);
            // 
            // TxtPPDocNo
            // 
            this.TxtPPDocNo.EnterMoveNextControl = true;
            this.TxtPPDocNo.Location = new System.Drawing.Point(83, 66);
            this.TxtPPDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPPDocNo.Name = "TxtPPDocNo";
            this.TxtPPDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPPDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPPDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPPDocNo.Properties.MaxLength = 16;
            this.TxtPPDocNo.Properties.ReadOnly = true;
            this.TxtPPDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtPPDocNo.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(18, 69);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 14);
            this.label8.TabIndex = 18;
            this.label8.Text = "Planning#";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPPDocNo
            // 
            this.BtnPPDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPPDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPPDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPPDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPPDocNo.Appearance.Options.UseBackColor = true;
            this.BtnPPDocNo.Appearance.Options.UseFont = true;
            this.BtnPPDocNo.Appearance.Options.UseForeColor = true;
            this.BtnPPDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnPPDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPPDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPPDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnPPDocNo.Image")));
            this.BtnPPDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPPDocNo.Location = new System.Drawing.Point(253, 66);
            this.BtnPPDocNo.Name = "BtnPPDocNo";
            this.BtnPPDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnPPDocNo.TabIndex = 20;
            this.BtnPPDocNo.ToolTip = "Find Production Planning With Work Center";
            this.BtnPPDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPPDocNo.ToolTipTitle = "Run System";
            this.BtnPPDocNo.Click += new System.EventHandler(this.BtnPPDocNo_Click);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(83, 24);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(116, 20);
            this.DteDocDt.TabIndex = 15;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(46, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(83, 3);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 151);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.LueItCode);
            this.splitContainer1.Panel2.Controls.Add(this.Grd1);
            this.splitContainer1.Panel2.Controls.Add(this.BtnProductionResult);
            this.splitContainer1.Size = new System.Drawing.Size(805, 322);
            this.splitContainer1.SplitterDistance = 220;
            this.splitContainer1.TabIndex = 34;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.Grd2);
            this.splitContainer2.Panel1.Controls.Add(this.BtnBOM);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.LueBatchNo);
            this.splitContainer2.Panel2.Controls.Add(this.Grd3);
            this.splitContainer2.Size = new System.Drawing.Size(805, 220);
            this.splitContainer2.SplitterDistance = 84;
            this.splitContainer2.TabIndex = 38;
            // 
            // Grd2
            // 
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 23);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(801, 57);
            this.Grd2.TabIndex = 37;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            this.Grd2.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd2_ColHdrDoubleClick);
            this.Grd2.AfterContentsSorted += new System.EventHandler(this.Grd2_AfterContentsSorted);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.BeforeContentsSorted += new System.EventHandler(this.Grd2_BeforeContentsSorted);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // BtnBOM
            // 
            this.BtnBOM.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnBOM.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnBOM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnBOM.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBOM.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnBOM.Location = new System.Drawing.Point(0, 0);
            this.BtnBOM.Name = "BtnBOM";
            this.BtnBOM.Size = new System.Drawing.Size(801, 23);
            this.BtnBOM.TabIndex = 36;
            this.BtnBOM.Text = "Bill Of Material";
            this.BtnBOM.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnBOM.UseVisualStyleBackColor = false;
            // 
            // LueBatchNo
            // 
            this.LueBatchNo.EnterMoveNextControl = true;
            this.LueBatchNo.Location = new System.Drawing.Point(129, 22);
            this.LueBatchNo.Margin = new System.Windows.Forms.Padding(5);
            this.LueBatchNo.Name = "LueBatchNo";
            this.LueBatchNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchNo.Properties.Appearance.Options.UseFont = true;
            this.LueBatchNo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchNo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBatchNo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchNo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBatchNo.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchNo.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBatchNo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchNo.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBatchNo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBatchNo.Properties.DropDownRows = 30;
            this.LueBatchNo.Properties.NullText = "[Empty]";
            this.LueBatchNo.Properties.PopupWidth = 500;
            this.LueBatchNo.Size = new System.Drawing.Size(180, 20);
            this.LueBatchNo.TabIndex = 38;
            this.LueBatchNo.ToolTip = "F4 : Show/hide list";
            this.LueBatchNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBatchNo.EditValueChanged += new System.EventHandler(this.LueBatchNo_EditValueChanged);
            this.LueBatchNo.Enter += new System.EventHandler(this.LueBatchNo_Enter);
            this.LueBatchNo.Leave += new System.EventHandler(this.LueBatchNo_Leave);
            this.LueBatchNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueBatchNo_KeyDown);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(801, 128);
            this.Grd3.TabIndex = 39;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            this.Grd3.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd3_ColHdrDoubleClick);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            // 
            // LueItCode
            // 
            this.LueItCode.EnterMoveNextControl = true;
            this.LueItCode.Location = new System.Drawing.Point(129, 42);
            this.LueItCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode.Name = "LueItCode";
            this.LueItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode.Properties.Appearance.Options.UseFont = true;
            this.LueItCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode.Properties.DropDownRows = 30;
            this.LueItCode.Properties.NullText = "[Empty]";
            this.LueItCode.Properties.PopupWidth = 500;
            this.LueItCode.Size = new System.Drawing.Size(180, 20);
            this.LueItCode.TabIndex = 41;
            this.LueItCode.ToolTip = "F4 : Show/hide list";
            this.LueItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode.EditValueChanged += new System.EventHandler(this.LueItCode_EditValueChanged);
            this.LueItCode.Enter += new System.EventHandler(this.LueItCode_Enter);
            this.LueItCode.Leave += new System.EventHandler(this.LueItCode_Leave);
            this.LueItCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode_KeyDown);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 20);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(801, 74);
            this.Grd1.TabIndex = 42;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd1_ColHdrDoubleClick);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // BtnProductionResult
            // 
            this.BtnProductionResult.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnProductionResult.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnProductionResult.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnProductionResult.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProductionResult.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnProductionResult.Location = new System.Drawing.Point(0, 0);
            this.BtnProductionResult.Name = "BtnProductionResult";
            this.BtnProductionResult.Size = new System.Drawing.Size(801, 20);
            this.BtnProductionResult.TabIndex = 40;
            this.BtnProductionResult.Text = "Production Results";
            this.BtnProductionResult.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnProductionResult.UseVisualStyleBackColor = false;
            // 
            // FrmShopFloorControl2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 473);
            this.Name = "FrmShopFloorControl2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueBatchCategoryCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeePPRemark.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionWorkGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMachineCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWhsCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkCenterDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkCenterDocName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionShiftCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBatchNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        protected System.Windows.Forms.Panel panel3;
        public DevExpress.XtraEditors.SimpleButton BtnPPDocNo2;
        protected System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtWhsCode2;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtWorkCenterDocNo;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        public DevExpress.XtraEditors.SimpleButton BtnWorkCenterDocNo;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtWorkCenterDocName;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.LookUpEdit LueProductionShiftCode;
        internal DevExpress.XtraEditors.TextEdit TxtPPDocNo;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.SimpleButton BtnPPDocNo;
        private DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Button BtnBOM;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Button BtnProductionResult;
        internal DevExpress.XtraEditors.LookUpEdit LueBatchNo;
        internal DevExpress.XtraEditors.LookUpEdit LueItCode;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.MemoExEdit MeePPRemark;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.LookUpEdit LueMachineCode;
        private System.Windows.Forms.Label LblBatchCategoryCode;
        private DevExpress.XtraEditors.LookUpEdit LueBatchCategoryCode;
        private System.Windows.Forms.Label LblProductionWorkGroup;
        private DevExpress.XtraEditors.LookUpEdit LueProductionWorkGroup;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label10;

    }
}