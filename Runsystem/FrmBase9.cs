﻿#region Update
/*
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBase9 : Form
    {
        public FrmBase9()
        {
            InitializeComponent();
        }

        #region Method

        #region Form Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {

        }

        #endregion

        #region Grid Method

        virtual protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {

        }

        virtual protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {

        }

        virtual protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {

        }

        virtual protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {

        }

        virtual protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        virtual protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #endregion

        #region Event

        #region Form Event

        private void FrmBase9_Load(object sender, EventArgs e)
        {
            FrmLoad(sender, e);
        }

        #endregion

        #region  Grid Event

        private void Grd1_AfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
            GrdAfterRowStateChanged(sender, e);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            GrdEllipsisButtonClick(sender, e);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdRequestEdit(sender, e);
        }

        #endregion

        #endregion
    }
}
