﻿namespace RunSystem
{
    partial class FrmPropertyInventoryCost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPropertyInventoryCost));
            this.tabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp4 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp5 = new DevExpress.XtraTab.XtraTabPage();
            this.TxtUPriceAfter = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtUPrice = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtFairValueAmt = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtUPriceBefore = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtAmtBefore = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtUoM = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtInventoryQty = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.LblPaymentType = new System.Windows.Forms.Label();
            this.LueFairValueType = new DevExpress.XtraEditors.LookUpEdit();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.ChkFile2 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload2 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload2 = new System.Windows.Forms.ProgressBar();
            this.TxtFile2 = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.BtnFile2 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TxtCostCenter = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtProfitCenter = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtSiteName = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtPropertyName = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtPropertyCode = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtPropertyCategory = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.MeeCostComponenReason = new DevExpress.XtraEditors.MemoExEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtPropertyInventoryAfter = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtCostComponentValue = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtPropertyInventoryBefore = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.BtnPropertyCode = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.Tp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.Tp4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.Tp5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceAfter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFairValueAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceBefore.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmtBefore.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUoM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFairValueType.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCostCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProfitCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCostComponenReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyInventoryAfter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCostComponentValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyInventoryBefore.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(879, 0);
            this.panel1.Size = new System.Drawing.Size(70, 517);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.MeeCancelReason);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.BtnPropertyCode);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.TxtCostCenter);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.TxtProfitCenter);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.TxtSiteName);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtPropertyName);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtPropertyCode);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.TxtPropertyCategory);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtStatus);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Size = new System.Drawing.Size(879, 223);
            // 
            // tabControl1
            // 
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.tabControl1.Location = new System.Drawing.Point(0, 223);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedTabPage = this.Tp1;
            this.tabControl1.Size = new System.Drawing.Size(879, 294);
            this.tabControl1.TabIndex = 39;
            this.tabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp4,
            this.Tp5,
            this.Tp2,
            this.Tp3});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.Grd1);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(873, 266);
            this.Tp1.Text = "DO To Department";
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(873, 266);
            this.Grd1.TabIndex = 55;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllispsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // Tp4
            // 
            this.Tp4.Controls.Add(this.Grd3);
            this.Tp4.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp4.Name = "Tp4";
            this.Tp4.Size = new System.Drawing.Size(873, 266);
            this.Tp4.Text = "Cash Advance Settlement";
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(873, 266);
            this.Grd3.TabIndex = 56;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllispsisButtonClick);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // Tp5
            // 
            this.Tp5.Appearance.PageClient.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Tp5.Appearance.PageClient.Options.UseBackColor = true;
            this.Tp5.Controls.Add(this.TxtUPriceAfter);
            this.Tp5.Controls.Add(this.label18);
            this.Tp5.Controls.Add(this.TxtUPrice);
            this.Tp5.Controls.Add(this.label19);
            this.Tp5.Controls.Add(this.TxtFairValueAmt);
            this.Tp5.Controls.Add(this.label15);
            this.Tp5.Controls.Add(this.TxtUPriceBefore);
            this.Tp5.Controls.Add(this.label2);
            this.Tp5.Controls.Add(this.TxtAmtBefore);
            this.Tp5.Controls.Add(this.label4);
            this.Tp5.Controls.Add(this.TxtUoM);
            this.Tp5.Controls.Add(this.label12);
            this.Tp5.Controls.Add(this.TxtInventoryQty);
            this.Tp5.Controls.Add(this.label13);
            this.Tp5.Controls.Add(this.LblPaymentType);
            this.Tp5.Controls.Add(this.LueFairValueType);
            this.Tp5.Name = "Tp5";
            this.Tp5.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp5.Size = new System.Drawing.Size(873, 266);
            this.Tp5.Text = "Fair Value Adjustment";
            // 
            // TxtUPriceAfter
            // 
            this.TxtUPriceAfter.EnterMoveNextControl = true;
            this.TxtUPriceAfter.Location = new System.Drawing.Point(149, 155);
            this.TxtUPriceAfter.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUPriceAfter.Name = "TxtUPriceAfter";
            this.TxtUPriceAfter.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUPriceAfter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUPriceAfter.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPriceAfter.Properties.Appearance.Options.UseFont = true;
            this.TxtUPriceAfter.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUPriceAfter.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUPriceAfter.Properties.MaxLength = 16;
            this.TxtUPriceAfter.Properties.ReadOnly = true;
            this.TxtUPriceAfter.Size = new System.Drawing.Size(272, 20);
            this.TxtUPriceAfter.TabIndex = 48;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(34, 157);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(113, 14);
            this.label18.TabIndex = 47;
            this.label18.Text = "Unit Price After Adj";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUPrice
            // 
            this.TxtUPrice.EnterMoveNextControl = true;
            this.TxtUPrice.Location = new System.Drawing.Point(149, 134);
            this.TxtUPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUPrice.Name = "TxtUPrice";
            this.TxtUPrice.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtUPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUPrice.Properties.MaxLength = 16;
            this.TxtUPrice.Properties.ReadOnly = true;
            this.TxtUPrice.Size = new System.Drawing.Size(272, 20);
            this.TxtUPrice.TabIndex = 46;
            this.TxtUPrice.Validated += new System.EventHandler(this.TxtUPrice_Validated);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(66, 136);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(81, 14);
            this.label19.TabIndex = 45;
            this.label19.Text = "Unit Price Adj";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFairValueAmt
            // 
            this.TxtFairValueAmt.EnterMoveNextControl = true;
            this.TxtFairValueAmt.Location = new System.Drawing.Point(149, 112);
            this.TxtFairValueAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFairValueAmt.Name = "TxtFairValueAmt";
            this.TxtFairValueAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtFairValueAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFairValueAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFairValueAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtFairValueAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtFairValueAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtFairValueAmt.Properties.MaxLength = 16;
            this.TxtFairValueAmt.Properties.ReadOnly = true;
            this.TxtFairValueAmt.Size = new System.Drawing.Size(272, 20);
            this.TxtFairValueAmt.TabIndex = 44;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(18, 114);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(129, 14);
            this.label15.TabIndex = 43;
            this.label15.Text = "Fair Value Adj Amount";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUPriceBefore
            // 
            this.TxtUPriceBefore.EnterMoveNextControl = true;
            this.TxtUPriceBefore.Location = new System.Drawing.Point(149, 90);
            this.TxtUPriceBefore.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUPriceBefore.Name = "TxtUPriceBefore";
            this.TxtUPriceBefore.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUPriceBefore.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUPriceBefore.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPriceBefore.Properties.Appearance.Options.UseFont = true;
            this.TxtUPriceBefore.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUPriceBefore.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUPriceBefore.Properties.MaxLength = 16;
            this.TxtUPriceBefore.Properties.ReadOnly = true;
            this.TxtUPriceBefore.Size = new System.Drawing.Size(272, 20);
            this.TxtUPriceBefore.TabIndex = 42;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(26, 92);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 14);
            this.label2.TabIndex = 41;
            this.label2.Text = "Unit Price Before Adj";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmtBefore
            // 
            this.TxtAmtBefore.EnterMoveNextControl = true;
            this.TxtAmtBefore.Location = new System.Drawing.Point(149, 69);
            this.TxtAmtBefore.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmtBefore.Name = "TxtAmtBefore";
            this.TxtAmtBefore.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmtBefore.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmtBefore.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmtBefore.Properties.Appearance.Options.UseFont = true;
            this.TxtAmtBefore.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmtBefore.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmtBefore.Properties.MaxLength = 16;
            this.TxtAmtBefore.Properties.ReadOnly = true;
            this.TxtAmtBefore.Size = new System.Drawing.Size(272, 20);
            this.TxtAmtBefore.TabIndex = 40;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(34, 71);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 14);
            this.label4.TabIndex = 39;
            this.label4.Text = "Amount Before Adj";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUoM
            // 
            this.TxtUoM.EnterMoveNextControl = true;
            this.TxtUoM.Location = new System.Drawing.Point(149, 48);
            this.TxtUoM.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUoM.Name = "TxtUoM";
            this.TxtUoM.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUoM.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUoM.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUoM.Properties.Appearance.Options.UseFont = true;
            this.TxtUoM.Properties.MaxLength = 16;
            this.TxtUoM.Properties.ReadOnly = true;
            this.TxtUoM.Size = new System.Drawing.Size(272, 20);
            this.TxtUoM.TabIndex = 38;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(117, 50);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 14);
            this.label12.TabIndex = 37;
            this.label12.Text = "UoM";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInventoryQty
            // 
            this.TxtInventoryQty.EnterMoveNextControl = true;
            this.TxtInventoryQty.Location = new System.Drawing.Point(149, 27);
            this.TxtInventoryQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInventoryQty.Name = "TxtInventoryQty";
            this.TxtInventoryQty.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInventoryQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInventoryQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInventoryQty.Properties.Appearance.Options.UseFont = true;
            this.TxtInventoryQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInventoryQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInventoryQty.Properties.MaxLength = 16;
            this.TxtInventoryQty.Properties.ReadOnly = true;
            this.TxtInventoryQty.Size = new System.Drawing.Size(272, 20);
            this.TxtInventoryQty.TabIndex = 36;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(36, 29);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 14);
            this.label13.TabIndex = 35;
            this.label13.Text = "Inventory Quantity";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblPaymentType
            // 
            this.LblPaymentType.AutoSize = true;
            this.LblPaymentType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPaymentType.ForeColor = System.Drawing.Color.Black;
            this.LblPaymentType.Location = new System.Drawing.Point(56, 8);
            this.LblPaymentType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPaymentType.Name = "LblPaymentType";
            this.LblPaymentType.Size = new System.Drawing.Size(91, 14);
            this.LblPaymentType.TabIndex = 29;
            this.LblPaymentType.Text = "Fair Value Type";
            this.LblPaymentType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueFairValueType
            // 
            this.LueFairValueType.EnterMoveNextControl = true;
            this.LueFairValueType.Location = new System.Drawing.Point(149, 5);
            this.LueFairValueType.Margin = new System.Windows.Forms.Padding(5);
            this.LueFairValueType.Name = "LueFairValueType";
            this.LueFairValueType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFairValueType.Properties.Appearance.Options.UseFont = true;
            this.LueFairValueType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFairValueType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFairValueType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFairValueType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFairValueType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFairValueType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFairValueType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFairValueType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFairValueType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFairValueType.Properties.DropDownRows = 30;
            this.LueFairValueType.Properties.NullText = "[Empty]";
            this.LueFairValueType.Properties.PopupWidth = 300;
            this.LueFairValueType.Size = new System.Drawing.Size(273, 20);
            this.LueFairValueType.TabIndex = 30;
            this.LueFairValueType.ToolTip = "F4 : Show/hide list";
            this.LueFairValueType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueFairValueType.EditValueChanged += new System.EventHandler(this.LueFairValueType_EditValueChanged);
            this.LueFairValueType.Validated += new System.EventHandler(this.LueFairValueType_Validated);
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.ChkFile2);
            this.Tp2.Controls.Add(this.BtnDownload2);
            this.Tp2.Controls.Add(this.PbUpload2);
            this.Tp2.Controls.Add(this.TxtFile2);
            this.Tp2.Controls.Add(this.label23);
            this.Tp2.Controls.Add(this.BtnFile2);
            this.Tp2.Controls.Add(this.ChkFile);
            this.Tp2.Controls.Add(this.BtnDownload);
            this.Tp2.Controls.Add(this.PbUpload);
            this.Tp2.Controls.Add(this.TxtFile);
            this.Tp2.Controls.Add(this.label22);
            this.Tp2.Controls.Add(this.BtnFile);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(873, 266);
            this.Tp2.Text = "Upload File";
            // 
            // ChkFile2
            // 
            this.ChkFile2.Location = new System.Drawing.Point(261, 50);
            this.ChkFile2.Name = "ChkFile2";
            this.ChkFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile2.Properties.Appearance.Options.UseFont = true;
            this.ChkFile2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile2.Properties.Caption = " ";
            this.ChkFile2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile2.Size = new System.Drawing.Size(20, 22);
            this.ChkFile2.TabIndex = 63;
            this.ChkFile2.ToolTip = "Remove filter";
            this.ChkFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile2.ToolTipTitle = "Run System";
            this.ChkFile2.CheckedChanged += new System.EventHandler(this.ChkFile2_CheckedChanged);
            // 
            // BtnDownload2
            // 
            this.BtnDownload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload2.Appearance.Options.UseBackColor = true;
            this.BtnDownload2.Appearance.Options.UseFont = true;
            this.BtnDownload2.Appearance.Options.UseForeColor = true;
            this.BtnDownload2.Appearance.Options.UseTextOptions = true;
            this.BtnDownload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload2.Image")));
            this.BtnDownload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload2.Location = new System.Drawing.Point(306, 50);
            this.BtnDownload2.Name = "BtnDownload2";
            this.BtnDownload2.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload2.TabIndex = 65;
            this.BtnDownload2.ToolTip = "DownloadFile";
            this.BtnDownload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload2.ToolTipTitle = "Run System";
            this.BtnDownload2.Click += new System.EventHandler(this.BtnDownload2_Click);
            // 
            // PbUpload2
            // 
            this.PbUpload2.Location = new System.Drawing.Point(36, 74);
            this.PbUpload2.Name = "PbUpload2";
            this.PbUpload2.Size = new System.Drawing.Size(294, 17);
            this.PbUpload2.TabIndex = 66;
            // 
            // TxtFile2
            // 
            this.TxtFile2.EnterMoveNextControl = true;
            this.TxtFile2.Location = new System.Drawing.Point(36, 53);
            this.TxtFile2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile2.Name = "TxtFile2";
            this.TxtFile2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile2.Properties.Appearance.Options.UseFont = true;
            this.TxtFile2.Properties.MaxLength = 16;
            this.TxtFile2.Size = new System.Drawing.Size(221, 20);
            this.TxtFile2.TabIndex = 62;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(8, 55);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(24, 14);
            this.label23.TabIndex = 61;
            this.label23.Text = "File";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnFile2
            // 
            this.BtnFile2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile2.Appearance.Options.UseBackColor = true;
            this.BtnFile2.Appearance.Options.UseFont = true;
            this.BtnFile2.Appearance.Options.UseForeColor = true;
            this.BtnFile2.Appearance.Options.UseTextOptions = true;
            this.BtnFile2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile2.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile2.Image")));
            this.BtnFile2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile2.Location = new System.Drawing.Point(279, 50);
            this.BtnFile2.Name = "BtnFile2";
            this.BtnFile2.Size = new System.Drawing.Size(24, 21);
            this.BtnFile2.TabIndex = 64;
            this.BtnFile2.ToolTip = "BrowseFile";
            this.BtnFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile2.ToolTipTitle = "Run System";
            this.BtnFile2.Click += new System.EventHandler(this.BtnFile2_Click);
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(261, 4);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 57;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(306, 4);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 59;
            this.BtnDownload.ToolTip = "DownloadFile";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(36, 28);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(294, 17);
            this.PbUpload.TabIndex = 60;
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(36, 7);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(221, 20);
            this.TxtFile.TabIndex = 56;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(8, 9);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(24, 14);
            this.label22.TabIndex = 55;
            this.label22.Text = "File";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(279, 4);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(24, 21);
            this.BtnFile.TabIndex = 58;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.Grd2);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(873, 266);
            this.Tp3.Text = "Approval Information";
            // 
            // Grd2
            // 
            this.Grd2.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd2.BackColorOddRows = System.Drawing.Color.White;
            this.Grd2.DefaultAutoGroupRow.Height = 21;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.FrozenArea.ColCount = 3;
            this.Grd2.FrozenArea.SortFrozenRows = true;
            this.Grd2.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd2.Name = "Grd2";
            this.Grd2.ProcessTab = false;
            this.Grd2.ReadOnly = true;
            this.Grd2.RowTextStartColNear = 3;
            this.Grd2.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd2.SearchAsType.SearchCol = null;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(873, 266);
            this.Grd2.TabIndex = 55;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TxtCostCenter
            // 
            this.TxtCostCenter.EnterMoveNextControl = true;
            this.TxtCostCenter.Location = new System.Drawing.Point(124, 197);
            this.TxtCostCenter.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCostCenter.Name = "TxtCostCenter";
            this.TxtCostCenter.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCostCenter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCostCenter.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCostCenter.Properties.Appearance.Options.UseFont = true;
            this.TxtCostCenter.Properties.MaxLength = 16;
            this.TxtCostCenter.Properties.ReadOnly = true;
            this.TxtCostCenter.Size = new System.Drawing.Size(272, 20);
            this.TxtCostCenter.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(48, 199);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 14);
            this.label10.TabIndex = 29;
            this.label10.Text = "Cost Center";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProfitCenter
            // 
            this.TxtProfitCenter.EnterMoveNextControl = true;
            this.TxtProfitCenter.Location = new System.Drawing.Point(124, 176);
            this.TxtProfitCenter.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProfitCenter.Name = "TxtProfitCenter";
            this.TxtProfitCenter.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProfitCenter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProfitCenter.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProfitCenter.Properties.Appearance.Options.UseFont = true;
            this.TxtProfitCenter.Properties.MaxLength = 16;
            this.TxtProfitCenter.Properties.ReadOnly = true;
            this.TxtProfitCenter.Size = new System.Drawing.Size(272, 20);
            this.TxtProfitCenter.TabIndex = 28;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(43, 178);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 14);
            this.label11.TabIndex = 27;
            this.label11.Text = "Profit Center";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSiteName
            // 
            this.TxtSiteName.EnterMoveNextControl = true;
            this.TxtSiteName.Location = new System.Drawing.Point(124, 155);
            this.TxtSiteName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSiteName.Name = "TxtSiteName";
            this.TxtSiteName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSiteName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSiteName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSiteName.Properties.Appearance.Options.UseFont = true;
            this.TxtSiteName.Properties.MaxLength = 16;
            this.TxtSiteName.Properties.ReadOnly = true;
            this.TxtSiteName.Size = new System.Drawing.Size(272, 20);
            this.TxtSiteName.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(92, 157);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 14);
            this.label9.TabIndex = 25;
            this.label9.Text = "Site";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPropertyName
            // 
            this.TxtPropertyName.EnterMoveNextControl = true;
            this.TxtPropertyName.Location = new System.Drawing.Point(124, 134);
            this.TxtPropertyName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPropertyName.Name = "TxtPropertyName";
            this.TxtPropertyName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPropertyName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPropertyName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPropertyName.Properties.Appearance.Options.UseFont = true;
            this.TxtPropertyName.Properties.MaxLength = 16;
            this.TxtPropertyName.Properties.ReadOnly = true;
            this.TxtPropertyName.Size = new System.Drawing.Size(272, 20);
            this.TxtPropertyName.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(32, 136);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 14);
            this.label8.TabIndex = 23;
            this.label8.Text = "Property Name";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPropertyCode
            // 
            this.TxtPropertyCode.EnterMoveNextControl = true;
            this.TxtPropertyCode.Location = new System.Drawing.Point(124, 113);
            this.TxtPropertyCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPropertyCode.Name = "TxtPropertyCode";
            this.TxtPropertyCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPropertyCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPropertyCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPropertyCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPropertyCode.Properties.MaxLength = 16;
            this.TxtPropertyCode.Properties.ReadOnly = true;
            this.TxtPropertyCode.Size = new System.Drawing.Size(249, 20);
            this.TxtPropertyCode.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(35, 115);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 14);
            this.label7.TabIndex = 20;
            this.label7.Text = "Property Code";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPropertyCategory
            // 
            this.TxtPropertyCategory.EnterMoveNextControl = true;
            this.TxtPropertyCategory.Location = new System.Drawing.Point(124, 92);
            this.TxtPropertyCategory.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPropertyCategory.Name = "TxtPropertyCategory";
            this.TxtPropertyCategory.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPropertyCategory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPropertyCategory.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPropertyCategory.Properties.Appearance.Options.UseFont = true;
            this.TxtPropertyCategory.Properties.MaxLength = 16;
            this.TxtPropertyCategory.Properties.ReadOnly = true;
            this.TxtPropertyCategory.Size = new System.Drawing.Size(272, 20);
            this.TxtPropertyCategory.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(14, 94);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 14);
            this.label6.TabIndex = 18;
            this.label6.Text = "Property Category";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(124, 71);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 16;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(120, 20);
            this.TxtStatus.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(78, 74);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 14);
            this.label5.TabIndex = 16;
            this.label5.Text = "Status";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(87, 52);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 14);
            this.label3.TabIndex = 14;
            this.label3.Text = "Date";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(124, 49);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(120, 20);
            this.DteDocDt.TabIndex = 15;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(335, 27);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 13;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(124, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(272, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(47, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.MeeCostComponenReason);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.TxtPropertyInventoryAfter);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.TxtCostComponentValue);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.TxtPropertyInventoryBefore);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(404, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(475, 223);
            this.panel4.TabIndex = 35;
            // 
            // MeeCostComponenReason
            // 
            this.MeeCostComponenReason.EditValue = "";
            this.MeeCostComponenReason.EnterMoveNextControl = true;
            this.MeeCostComponenReason.Location = new System.Drawing.Point(252, 71);
            this.MeeCostComponenReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCostComponenReason.Name = "MeeCostComponenReason";
            this.MeeCostComponenReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCostComponenReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCostComponenReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCostComponenReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCostComponenReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCostComponenReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCostComponenReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCostComponenReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCostComponenReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCostComponenReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCostComponenReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCostComponenReason.Properties.MaxLength = 1000;
            this.MeeCostComponenReason.Properties.PopupFormSize = new System.Drawing.Size(600, 70);
            this.MeeCostComponenReason.Properties.ShowIcon = false;
            this.MeeCostComponenReason.Size = new System.Drawing.Size(216, 20);
            this.MeeCostComponenReason.TabIndex = 38;
            this.MeeCostComponenReason.ToolTip = "F4 : Show/hide text";
            this.MeeCostComponenReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCostComponenReason.ToolTipTitle = "Run System";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(81, 74);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(168, 14);
            this.label20.TabIndex = 37;
            this.label20.Text = "Add Cost Component Reason";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPropertyInventoryAfter
            // 
            this.TxtPropertyInventoryAfter.EnterMoveNextControl = true;
            this.TxtPropertyInventoryAfter.Location = new System.Drawing.Point(252, 50);
            this.TxtPropertyInventoryAfter.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPropertyInventoryAfter.Name = "TxtPropertyInventoryAfter";
            this.TxtPropertyInventoryAfter.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtPropertyInventoryAfter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPropertyInventoryAfter.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPropertyInventoryAfter.Properties.Appearance.Options.UseFont = true;
            this.TxtPropertyInventoryAfter.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPropertyInventoryAfter.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPropertyInventoryAfter.Properties.MaxLength = 16;
            this.TxtPropertyInventoryAfter.Properties.ReadOnly = true;
            this.TxtPropertyInventoryAfter.Size = new System.Drawing.Size(216, 20);
            this.TxtPropertyInventoryAfter.TabIndex = 36;
            this.TxtPropertyInventoryAfter.TabStop = false;
            this.TxtPropertyInventoryAfter.Validated += new System.EventHandler(this.TxtEcoLife3_Validated);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(23, 53);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(226, 14);
            this.label21.TabIndex = 35;
            this.label21.Text = "Property Inventory Value After Addition";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCostComponentValue
            // 
            this.TxtCostComponentValue.EnterMoveNextControl = true;
            this.TxtCostComponentValue.Location = new System.Drawing.Point(252, 28);
            this.TxtCostComponentValue.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCostComponentValue.Name = "TxtCostComponentValue";
            this.TxtCostComponentValue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCostComponentValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCostComponentValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCostComponentValue.Properties.Appearance.Options.UseFont = true;
            this.TxtCostComponentValue.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCostComponentValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCostComponentValue.Properties.MaxLength = 16;
            this.TxtCostComponentValue.Properties.ReadOnly = true;
            this.TxtCostComponentValue.Size = new System.Drawing.Size(216, 20);
            this.TxtCostComponentValue.TabIndex = 34;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(116, 31);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(133, 14);
            this.label17.TabIndex = 33;
            this.label17.Text = "Cost Component Value";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPropertyInventoryBefore
            // 
            this.TxtPropertyInventoryBefore.EnterMoveNextControl = true;
            this.TxtPropertyInventoryBefore.Location = new System.Drawing.Point(252, 6);
            this.TxtPropertyInventoryBefore.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPropertyInventoryBefore.Name = "TxtPropertyInventoryBefore";
            this.TxtPropertyInventoryBefore.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPropertyInventoryBefore.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPropertyInventoryBefore.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPropertyInventoryBefore.Properties.Appearance.Options.UseFont = true;
            this.TxtPropertyInventoryBefore.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPropertyInventoryBefore.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPropertyInventoryBefore.Properties.MaxLength = 16;
            this.TxtPropertyInventoryBefore.Properties.ReadOnly = true;
            this.TxtPropertyInventoryBefore.Size = new System.Drawing.Size(216, 20);
            this.TxtPropertyInventoryBefore.TabIndex = 32;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(15, 9);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(234, 14);
            this.label14.TabIndex = 31;
            this.label14.Text = "Property Inventory Value Before Addition";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EditValue = "";
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(124, 28);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 1000;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(600, 70);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(202, 20);
            this.MeeCancelReason.TabIndex = 12;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(35, 31);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 14);
            this.label16.TabIndex = 11;
            this.label16.Text = "Cancel Reason";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // BtnPropertyCode
            // 
            this.BtnPropertyCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPropertyCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPropertyCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPropertyCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPropertyCode.Appearance.Options.UseBackColor = true;
            this.BtnPropertyCode.Appearance.Options.UseFont = true;
            this.BtnPropertyCode.Appearance.Options.UseForeColor = true;
            this.BtnPropertyCode.Appearance.Options.UseTextOptions = true;
            this.BtnPropertyCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPropertyCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPropertyCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnPropertyCode.Image")));
            this.BtnPropertyCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPropertyCode.Location = new System.Drawing.Point(374, 112);
            this.BtnPropertyCode.Name = "BtnPropertyCode";
            this.BtnPropertyCode.Size = new System.Drawing.Size(24, 21);
            this.BtnPropertyCode.TabIndex = 22;
            this.BtnPropertyCode.ToolTip = "Show VR";
            this.BtnPropertyCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPropertyCode.ToolTipTitle = "Run System";
            this.BtnPropertyCode.Click += new System.EventHandler(this.BtnPropertyCode_Click);
            // 
            // FrmPropertyInventoryCost
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 517);
            this.Controls.Add(this.tabControl1);
            this.Name = "FrmPropertyInventoryCost";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.tabControl1, 0);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.Tp4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.Tp5.ResumeLayout(false);
            this.Tp5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceAfter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFairValueAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceBefore.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmtBefore.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUoM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFairValueType.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.Tp2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCostCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProfitCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCostComponenReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyInventoryAfter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCostComponentValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyInventoryBefore.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl tabControl1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        internal DevExpress.XtraEditors.TextEdit TxtCostCenter;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtProfitCenter;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtSiteName;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtPropertyName;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtPropertyCode;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtPropertyCategory;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtPropertyInventoryAfter;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtCostComponentValue;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtPropertyInventoryBefore;
        private System.Windows.Forms.Label label14;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraEditors.MemoExEdit MeeCostComponenReason;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.SaveFileDialog SFD;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.ProgressBar PbUpload;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.Label label22;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        public DevExpress.XtraEditors.SimpleButton BtnPropertyCode;
        private DevExpress.XtraTab.XtraTabPage Tp4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private DevExpress.XtraTab.XtraTabPage Tp5;
        internal DevExpress.XtraEditors.TextEdit TxtUPriceAfter;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtUPrice;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtFairValueAmt;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtUPriceBefore;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtAmtBefore;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtUoM;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtInventoryQty;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label LblPaymentType;
        internal DevExpress.XtraEditors.LookUpEdit LueFairValueType;
        private DevExpress.XtraEditors.CheckEdit ChkFile2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload2;
        private System.Windows.Forms.ProgressBar PbUpload2;
        internal DevExpress.XtraEditors.TextEdit TxtFile2;
        private System.Windows.Forms.Label label23;
        public DevExpress.XtraEditors.SimpleButton BtnFile2;
    }
}