﻿#region Update

/*
 * 21/06/2022 [SET/PRODUCT] Menu Reporting baru
 */ 

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptDebtSecurities : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptDebtSecurities(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                //GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string SetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT C.DocNo, C.DocDt, D.OptDesc MenuDesc, C.InvestmentName, C.InvestmentCode, C.BankAcCode, C.BankAcNm, C.Issuer, C.InvestmentCtCode, ");
            SQL.AppendLine("C.InvestmentCtName, E.OptDesc InvestmentType, C.Status, C.NominalAmt, C.Price, C.InvestmentCost, C.Expenses, C.TotalAcquisition, C.SalesPrice, ");
            SQL.AppendLine("C.SalesAmt, C.TotalSalesAmt, C.RealizedGainLoss, C.MaturityDt, C.InterestRateAmt ");
            SQL.AppendLine("FROM tblinvestmentstockmovement A ");
            SQL.AppendLine("INNER JOIN tblinvestmentstocksummary B ON A.BankAcCode = B.BankAcCode AND A.Source = B.Source ");
            SQL.AppendLine("INNER JOIN ( ");
            SQL.AppendLine("	SELECT A.DocNo, A.DocDt, D.PortofolioName InvestmentName, C.PortofolioId InvestmentCode, A.BankAcCode, E.BankAcNm, D.Issuer, ");
            SQL.AppendLine("	D.InvestmentCtCode, F.InvestmentCtName, A.InvestmentType, A.Status, A.Amt NominalAmt, A.AcquisitionPrice Price, ");
            SQL.AppendLine("	IFNULL(A.Amt, 0.00)*IFNULL(A.AcquisitionPrice, 0.00) AS InvestmentCost, SUM(IFNULL(B.Amt, 0.00)) Expenses, ");
            SQL.AppendLine("	(IFNULL(A.Amt, 0.00)*IFNULL(A.AcquisitionPrice, 0.00))+SUM(IFNULL(B.Amt, 0.00)) TotalAcquisition, 0.00 SalesPrice, 0.00 SalesAmt, 0.00 TotalSalesAmt, ");
            SQL.AppendLine("	0.00 RealizedGainLoss, C.MaturityDt, C.InterestRateAmt, A.Source ");
            SQL.AppendLine("	FROM tblacquisitiondebtsecuritieshdr A ");
            SQL.AppendLine("	INNER JOIN tblacquisitiondebtsecuritiesdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("	INNER JOIN tblinvestmentitemdebt C ON A.InvestmentdebtCode = C.InvestmentdebtCode ");
            SQL.AppendLine("	INNER JOIN tblinvestmentportofolio D ON C.PortofolioId = D.PortofolioId ");
            SQL.AppendLine("	INNER JOIN tblbankaccount E ON A.BankAcCode = E.BankAcCode ");
            SQL.AppendLine("	INNER JOIN tblinvestmentcategory F ON D.InvestmentCtCode = F.InvestmentCtCode ");
            SQL.AppendLine("	GROUP BY A.DocNo ");
            SQL.AppendLine("	UNION ALL ");
            SQL.AppendLine("	SELECT A.DocNo, A.DocDt, D.PortofolioName InvestmentName, C.PortofolioId InvestmentCode, A.BankAcCode, E.BankAcNm, D.Issuer, ");
            SQL.AppendLine("	D.InvestmentCtCode, F.InvestmentCtName, A.InvestmentType, A.Status, A.NominalAmt, A.MovingAvgPrice Price, A.NominalAmt*A.MovingAvgPrice AS InvestmentCost, ");
            SQL.AppendLine("	SUM(B.Amt) Expenses, 0.00 TotalAcquisition, A.SalesPrice, IFNULL(A.NominalAmt, 0.00)*IFNULL(A.SalesPrice, 0.00) SalesAmt, ");
            SQL.AppendLine("	SUM(IFNULL(B.Amt, 0.00))+(IFNULL(A.NominalAmt, 0.00)*IFNULL(A.SalesPrice, 0.00)) TotalSalesAmt, 0.00 RealizedGainLoss, C.MaturityDt, C.InterestRateAmt, A.Source ");
            SQL.AppendLine("	FROM tblsalesdebtsecuritieshdr A ");
            SQL.AppendLine("	INNER JOIN tblsalesdebtsecuritiesdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("	INNER JOIN tblinvestmentitemdebt C ON A.InvestmentdebtCode = C.InvestmentdebtCode ");
            SQL.AppendLine("	INNER JOIN tblinvestmentportofolio D ON C.PortofolioId = D.PortofolioId ");
            SQL.AppendLine("	INNER JOIN tblbankaccount E ON A.BankAcCode = E.BankAcCode ");
            SQL.AppendLine("	INNER JOIN tblinvestmentcategory F ON D.InvestmentCtCode = F.InvestmentCtCode ");
            SQL.AppendLine("	GROUP BY A.DocNo ");
            SQL.AppendLine("	) C ON A.DocNo = C.DocNo AND A.Source = C.Source ");
            SQL.AppendLine("INNER JOIN tbloption D ON A.DocType = D.OptCode AND D.OptCat = 'InvestmentTransType' ");
            SQL.AppendLine("INNER JOIN tbloption E ON C.InvestmentType = E.OptCode AND E.OptCat = 'InvestmentType' ");
            SQL.AppendLine("INNER JOIN tblinvestmentstockprice F ON A.Source = F.Source ");
            SQL.AppendLine("WHERE C.DocDt BETWEEN @DocDt1 AND @DocDt2 ");
            SQL.AppendLine(Filter);

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5  
                        "Document#",
                        "Document Date",
                        "Menu Desc",
                        "Investment's Code",
                        "Investment's" + Environment.NewLine + "Name",

                        //6-10
                        "Investment's" + Environment.NewLine + "Bank Account (RDN)#",
                        "Investment's" + Environment.NewLine + "Bank Account Name",
                        "Issuer",
                        "Category Code",
                        "Category",

                        //11-15
                        "Type",
                        "Status",
                        "Nominal Amount",
                        "Acquisition /" + Environment.NewLine + "Moving Average Price",
                        "Investment Cost",

                        //16-20
                        "Expenses &" + Environment.NewLine + "Other Detail",
                        "Total Acquisition",
                        "Sales Price",
                        "Sales Amount",
                        "Total Sales",

                        //21-25
                        "Realized (Gain)/Loss",
                        "Maturity Date",
                        "Interest Rate",
                        "Create Date",

                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 180, 150, 250, 
                        
                        //6-10
                        180, 200, 250, 0, 130,
                        
                        //11-15
                        150, 100, 130, 130, 130,
  
                        //16-20
                        150, 150, 150, 150, 150,

                        //21-22
                        150, 100, 100, 100,
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 24 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 22 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 14, 15, 16, 17, 18, 19, 20, 21 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, true);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var cm = new MySqlCommand();
                string Filter = " ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                //Sm.CmParam<String>(ref cm, "@EquityInvestmentCtCode", mEquityInvestmentCtCode);
                Sm.FilterStr(ref Filter, ref cm, TxtItInvestmentCode.Text, new string[] { "C.InvestmentCode", "C.InvestmentName" });

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SetSQL(Filter) + " Order By A.InvestmentCode, E.OptDesc; ",
                new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DocDt", "MenuDesc", "InvestmentCode", "InvestmentName", "BankAcCode",
                        //6-10
                        "BankAcNm", "Issuer", "InvestmentCtCode", "InvestmentCtName", "InvestmentType",
                        //11-15
                        "Status", "NominalAmt", "Price", "InvestmentCost", "Expenses", 
                        //16-20
                        "TotalAcquisition", "SalesPrice", "SalesAmt", "TotalSalesAmt", "RealizedGainLoss", 
                        //21-22
                        "MaturityDt", "InterestRateAmt"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                        //Grd.Cells[Row, 21].Value = Sm.GetGrdec(Grd1, Row, 12)* Sm.GetGrdDec(Grd1, Row, 20);
                        //Grd.Cells[Row, 19].Value = 0m;
                        //Grd.Cells[Row, 20].Value = 0m;
                        //Grd.Cells[Row, 21].Value = 0m;
                    }, true, false, false, false
                );

                //Sm.ComputeStockAccumulaton(Sm.GetDte(DteDocDt2), mEquityInvestmentCtCode, TxtItInvestmentCode.Text, ref Grd1, 22, 4, 9, 11, 18, 19, 20, 21);
                ComputeRealizedGainLoss();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] {  });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region

        private void ComputeRealizedGainLoss()
        {
            decimal TotalSalesAmt = 0m, InvestmentCost = 0m;

            for(int r = 0; r < Grd1.Rows.Count; r++)
            {
                InvestmentCost = Sm.GetGrdDec(Grd1, r, 15);
                TotalSalesAmt = Sm.GetGrdDec(Grd1, r, 20);

                if(TotalSalesAmt == 0)
                    Grd1.Cells[r, 21].Value = 0m;
                else
                    Grd1.Cells[r, 21].Value = TotalSalesAmt - InvestmentCost;
            }
        }

        #endregion

        #region Event

        private void TxtItInvestmentCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItInvestmentCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment");
        }

        #endregion
    }
}
