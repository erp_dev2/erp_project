﻿#region Update
/*
    10/09/2018 [HAR] jumlah kolomnya jadi in 7
    19/09/2018 [TKG] difilter otorisasi berdasarkan site di grup, tambah filter site.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmployeeTraining : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterBySiteHR = false;

        #endregion

        #region Constructor

        public FrmRptEmployeeTraining(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocNo, T1.TrainingCode, T3.TrainingName, T1.Remark As Objective, T4.TargetAudience, ");
            SQL.AppendLine("T4.TrainerCode, T4.TrainerName, T1.DocDt, T5.SiteName, ");
            SQL.AppendLine("IfNull(Group_Concat(Distinct Concat(Left(T2.Tm, 2), ':', Right(T2.Tm, 2)) Separator ', '), '') Tm, ");
            SQL.AppendLine("T4.NoOfHr, Count(T2.EmpCode) EstimatedPax, (T4.NoOfHr * Count(T2.EmpCode)) TotalTrainingHr ");
            //SQL.AppendLine("Case T1.Status When 'A' Then 'Approved' When 'C' Then  ");
            SQL.AppendLine("From TblTrainingRequestHdr T1 ");
            SQL.AppendLine("Inner Join TblTrainingRequestDtl2 T2 On T1.DocNo = T2.DocNo  ");
            SQL.AppendLine("    And T2.Status = 'A' And T2.CancelInd = 'N' ");
            SQL.AppendLine("    And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("Inner Join TblTraining T3 On T1.TrainingCode = T3.TrainingCode ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And T3.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(T3.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select X1.DocNo, ");
	        SQL.AppendLine("    Group_Concat(Distinct X2.TrainerCode Separator ', ') As TrainerCode, ");
	        SQL.AppendLine("    Group_Concat(Distinct X3.TrainerName Separator ', ') As TrainerName, ");
	        SQL.AppendLine("    Sum(IfNull(X3.NoOfHr, 0)) NoOfHr, X21.Remark As TargetAudience ");
	        SQL.AppendLine("    From TblTrainingRequestHdr X1 ");
	        SQL.AppendLine("    Inner Join TblTrainingRequestDtl X2 On X1.DocNo = X2.DocNo ");
	        SQL.AppendLine("        And (X1.DocDt Between @DocDt1 And @DocDt2) ");
	        SQL.AppendLine("    Inner Join TblTrainingRequestDtl X21 On X1.DocNo = X21.DocNo And X21.DNo = '001' ");
	        SQL.AppendLine("    Inner Join TblTrainer X3 On X2.TrainerCode = X3.TrainerCode ");
	        SQL.AppendLine("    Group By X1.DocNo, X21.Remark ");
            SQL.AppendLine(") T4 On T1.DocNo = T4.DocNo ");
            SQL.AppendLine("Left Join TblSite T5 On T3.SiteCode=T5.SiteCode ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Training Code",
                    "Learning & Development" + Environment.NewLine + "Program",
                    "Objectives",
                    "Target Audience",

                    //6-10
                    "Trainer Code",
                    "Trainer(s)",
                    "Date of" + Environment.NewLine + "Training Request",
                    "Time",
                    "Duration of Session" + Environment.NewLine + "(hour)",

                    //11-14
                    "Estimated" + Environment.NewLine + "# of Pax",
                    "Estimated Total" + Environment.NewLine + "Training Hours",
                    "Site",
                    "Remarks"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 100, 180, 180, 150, 
                    
                    //6-10
                    100, 200, 130, 130, 120, 

                    //11-14
                    120, 120, 200, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T1.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T3.SiteCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + 
                    " Group By T1.DocNo, T1.TrainingCode, T3.TrainingName, T1.Remark, T4.TargetAudience, T4.TrainerCode, T4.TrainerName, T1.DocDt, T4.NoOfHr, T5.SiteName; ",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "TrainingCode", "TrainingName", "Objective", "TargetAudience", "TrainerCode", 
                        
                        //6-10
                        "TrainerName", "DocDt", "Tm", "NoOfHr", "EstimatedPax", 
                        
                        //11-12
                        "TotalTrainingHr", "SiteName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Grd1.Cells[Row, 14].Value = "Undone";
                    }, true, false, false, false
                );
                ProcessStatus();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void ProcessStatus()
        {
            var l = new List<TrainingAssignment>();
            string mDocNo = string.Empty;
            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    {
                        if (mDocNo.Length > 0) mDocNo += ",";
                        mDocNo += Sm.GetGrdStr(Grd1, i, 1);
                    }
                }
            }

            if (mDocNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select Distinct A.DocNo ");
                SQL.AppendLine("From TblTrainingRequestHdr A ");
                SQL.AppendLine("Inner Join TblTrainingRequestDtl2 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    And Find_In_Set(A.DocNo, @DocNo) ");
                SQL.AppendLine("Inner Join TblTrainingAssignmentDtl C On B.EmpCode = C.EmpCode And A.TrainingCode = C.TrainingCode ");
                SQL.AppendLine("    And C.TrainingSchDt Is Not Null ");
                SQL.AppendLine("Inner Join TblTrainingAssignmentHdr D On C.DocNo = D.DocNo And D.CancelInd = 'N'; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DocNo" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new TrainingAssignment()
                            {
                                TrainingRequestDocNo = Sm.DrStr(dr, c[0])
                            });
                        }
                    }
                    dr.Close();
                }

                if(l.Count > 0)
                {
                    for (int Row1 = 0; Row1 < l.Count; Row1++)
                    {
                        for (int Row2 = 0; Row2 < Grd1.Rows.Count; Row2++)
                        {
                            if (l[Row1].TrainingRequestDocNo == Sm.GetGrdStr(Grd1, Row2, 1))
                            {
                                Grd1.Cells[Row2, 14].Value = "Done";
                            }
                        }
                    }
                }
            }

            l.Clear();
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion

        #region Class

        private class TrainingAssignment
        {
            public string TrainingRequestDocNo { get; set; }
        }

        #endregion
    }
}
