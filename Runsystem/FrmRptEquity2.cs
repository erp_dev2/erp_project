﻿#region Update
/*
    24/04/2020 [WED/YK] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEquity2 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mThisYear = string.Empty,
            mOneYearAgo = string.Empty,
            mTwoYearsAgo = string.Empty,
            mSpecialEquity2Code = string.Empty;

        private int[] mGrdColDec = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };

        #endregion

        #region Constructor

        public FrmRptEquity2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetTimeStampVariable();
                SetGrd();
                GetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #region Standard Method

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("SELECT *FROM TblRptDescription where doctype = 'Equity' order by description1 ");
            SQL.AppendLine("Select * ");
            SQL.AppendLine("From TblFicoSettingJournalToCBPHdr ");
            SQL.AppendLine("Where Doctype = 'Equity' ");
            SQL.AppendLine("And ActInd = 'Y' ");
            SQL.AppendLine("Order By Right(Concat('00000000', Sequence ), 8), Code ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Keterangan",
                    "Setoran Modal", 
                    "Tambahan modal disetor",
                    "Saham treasuri",
                    "Bantuan Pemerintah yang Belum"+Environment.NewLine+"Ditetapkan Statusnya (BPYBDS)",

                    //6-10
                    "Modal Lainnya",
                    "Selisih transaksi dengan"+Environment.NewLine+"pihak nonpengendali", 
                    "Saldo laba (akumulasi rugi)"+Environment.NewLine+"Yang telah ditentukan penggunaannya", 
                    "Saldo laba (akumulasi rugi)"+Environment.NewLine+"Yang belum ditentukan penggunaannya", 
                    "Penghasilan Komprehensif Lain (setelah pajak)"+Environment.NewLine+"Selisih kurs",
                    
                    //11-15
                    "Penghasilan Komprehensif Lain (setelah pajak)"+Environment.NewLine+"Cadangan lindung nilai arus kas",
                    "Penghasilan Komprehensif Lain (setelah pajak)"+Environment.NewLine+"Surplus revaluasi aset tetap",
                    "Penghasilan Komprehensif Lain (setelah pajak)"+Environment.NewLine+"Perubahan nilai wajar aset keuangan",
                    "Penghasilan Komprehensif Lain (setelah pajak)"+Environment.NewLine+"Keuntungan (kerugian) pengukuran",
                    "Penghasilan Komprehensif Lain (setelah pajak)"+Environment.NewLine+"Bagian penghasilan",
                    
                    //16-20
                    "Komponen ekuitas lainnya", 
                    "Total Ekuitas"+Environment.NewLine+"yang dapat diatribusi",
                    "Kepentingan nonpengendali",
                    "Total Ekuitas",
                    "DocType",

                    //21
                    "Code"
                },
                new int[] 
                {
                    //0
                    30, 

                    //1-5
                    300, 200, 200, 200, 200, 
                    
                    //6-10
                    200, 200, 200, 200, 200, 

                    //11-15
                    200, 200, 200, 200, 200, 

                    //16-20
                    200, 200, 200, 200, 0, 

                    //21
                    0
                }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 20, 21 });
            Sm.GrdFormatDec(Grd1, mGrdColDec, 0);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            try
            {
                var cm = new MySqlCommand();
                Cursor.Current = Cursors.WaitCursor;
                Sm.ClearGrd(Grd1, false);
                SetTimeStampVariable();
                SetGrd();
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(),
                    new string[]
                    {
                        //0
                        "Description1",
                        //1-5
                        "Description2", "Description3", "Description4", "DocType", "Code"                       
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        for (int i = 0; i < mGrdColDec.Count(); ++i)
                        {
                            Grd.Cells[Row, mGrdColDec[i]].Value = 0m;
                        }
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 5);
                    }, true, false, false, false
                );
                ProcessData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mSpecialEquity2Code = Sm.GetParameter("SpecialEquity2Code");
        }

        private void SetTimeStampVariable()
        {
            mThisYear = Sm.GetLue(LueYr);
            mOneYearAgo = (Int32.Parse(mThisYear) - 1).ToString();
            mTwoYearsAgo = (Int32.Parse(mThisYear) - 2).ToString();
        }

        private void ProcessData()
        {
            var l2 = new List<EquityCOA>();
            var l3 = new List<Equity2RowCols>();

            GetRowColsCombination(ref l3);
            GetEquityCOA(ref l2);
            if (l2.Count > 0) ProcessEquityCOA(ref l2, ref l3);
            ProcessSpecialCode();
        }

        private void GetRowColsCombination(ref List<Equity2RowCols> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.Code, IfNull(B.OptDesc, '2') Cols, IFNULL(C.OptDesc, '0') Yr ");
            SQL.AppendLine("From TblFicoSettingJournalToCBPHdr A ");
            SQL.AppendLine("Left Join TblOption B On A.Code = B.OptCode And B.OptCat = 'Equity2RowCols' ");
            SQL.AppendLine("LEFT JOIN TblOption C ON C.optCat = 'Equity2ColYr' AND IFNULL(B.OptDesc, '2') = C.OptCode ");
            SQL.AppendLine("Where A.Doctype = 'Equity' ");
            SQL.AppendLine("And A.ActInd = 'Y' ");
            SQL.AppendLine("And Not Find_In_Set(A.Code, @SpecialEquity2Code) ");
            SQL.AppendLine("Order By Right(Concat('00000000', A.Sequence ), 8), A.Code; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SpecialEquity2Code", mSpecialEquity2Code);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Code", "Cols", "Yr" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Equity2RowCols()
                        {
                            Code = Sm.DrStr(dr, c[0]),
                            Cols = Sm.DrInt(dr, c[1]),
                            Yr = Sm.DrStr(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetEquityCOA(ref List<EquityCOA> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT T.Doctype, T.Code, SUM(T.Amt) Amt, SUM(T.AmtOld) AmtOld ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT A.DocType, A.Code, IFNULL(D.Amt, 0.00) Amt, 0.00 AmtOld ");
            SQL.AppendLine("    FROM TblFicoSettingJournalToCBPHdr A ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl B ON A.Doctype = B.Doctype ");
            SQL.AppendLine("        AND A.Code = B.Code ");
            SQL.AppendLine("        AND A.DocType = 'Equity' ");
            SQL.AppendLine("        AND A.ActInd = 'Y' ");
            SQL.AppendLine("        AND Not Find_In_Set(A.Code, @SpecialEquity2Code) ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON C.AcNo Like CONCAT(B.AcNo, '%') ");
            SQL.AppendLine("    LEFT JOIN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T2.AcNo, If(T3.AcType = 'D', (T2.DAmt - T2.CAmt), (T2.CAmt - T2.DAmt)) Amt ");
            SQL.AppendLine("        FROM TblJournalHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblJournalDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            AND LEFT(T1.DocDt, 4) = @Yr ");
            SQL.AppendLine("            AND T1.Cancelind = 'N' ");
            SQL.AppendLine("            AND T2.AcNo IN ");
            SQL.AppendLine("            ( ");
            SQL.AppendLine("                SELECT DISTINCT X3.AcNo ");
            SQL.AppendLine("                FROM TblFicoSettingJournalToCBPHdr X1 ");
            SQL.AppendLine("                INNER JOIN TblFicoSettingJournalToCBPDtl X2 ON X1.DocType = 'Equity' ");
            SQL.AppendLine("                    AND X1.DocType = X2.DocType ");
            SQL.AppendLine("                    AND X1.Code = X2.Code ");
            SQL.AppendLine("                    AND X1.Actind = 'Y' ");
            SQL.AppendLine("                    AND Not Find_In_Set(X1.Code, @SpecialEquity2Code) ");
            SQL.AppendLine("                INNER JOIN TblCOA X3 ON X3.AcNo LIKE CONCAT(X2.AcNo, '%') ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        INNER JOIN TblCOA T3 ON T2.AcNo = T3.AcNo ");
            SQL.AppendLine("    ) D ON C.AcNo = D.AcNo ");
                
            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT A.DocType, A.Code, 0.00 Amt, IFNULL(D.Amt, 0.00) AmtOld ");
            SQL.AppendLine("    FROM TblFicoSettingJournalToCBPHdr A ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl B ON A.Doctype = B.Doctype ");
            SQL.AppendLine("        AND A.Code = B.Code ");
            SQL.AppendLine("        AND A.DocType = 'Equity' ");
            SQL.AppendLine("        AND A.ActInd = 'Y' ");
            SQL.AppendLine("        AND Not Find_In_set(A.Code, @SpecialEquity2Code) ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON C.AcNo Like CONCAT(B.AcNo, '%') ");
            SQL.AppendLine("    LEFT JOIN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T2.AcNo, If(T3.AcType = 'D', (T2.DAmt - T2.CAmt), (T2.CAmt - T2.DAmt)) Amt ");
            SQL.AppendLine("        FROM TblJournalHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblJournalDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            AND LEFT(T1.DocDt, 4) = (@Yr - 1) ");
            SQL.AppendLine("            AND T1.Cancelind = 'N' ");
            SQL.AppendLine("            AND T2.AcNo IN ");
            SQL.AppendLine("            ( ");
            SQL.AppendLine("                SELECT DISTINCT X3.AcNo ");
            SQL.AppendLine("                FROM TblFicoSettingJournalToCBPHdr X1 ");
            SQL.AppendLine("                INNER JOIN TblFicoSettingJournalToCBPDtl X2 ON X1.DocType = 'Equity' ");
            SQL.AppendLine("                    AND X1.DocType = X2.DocType ");
            SQL.AppendLine("                    AND X1.Code = X2.Code ");
            SQL.AppendLine("                    AND X1.Actind = 'Y' ");
            SQL.AppendLine("                    AND Not Find_In_Set(X1.Code, @SpecialEquity2Code) ");
            SQL.AppendLine("                INNER JOIN TblCOA X3 ON X3.AcNo LIKE CONCAT(X2.AcNo, '%') ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        INNER JOIN TblCOA T3 ON T2.AcNo = T3.AcNo ");
            SQL.AppendLine("    ) D ON C.AcNo = D.AcNo ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("GROUP BY T.Doctype, T.Code ");
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@SpecialEquity2Code", mSpecialEquity2Code);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Doctype", "Code", "Amt", "AmtOld" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EquityCOA()
                        {
                            DocType = Sm.DrStr(dr, c[0]),
                            Code = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            AmtOld = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEquityCOA(ref List<EquityCOA> l, ref List<Equity2RowCols> l2)
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                foreach(var x in l
                    .Where(a =>
                        a.DocType == Sm.GetGrdStr(Grd1, i, 20) &&
                        a.Code == Sm.GetGrdStr(Grd1, i, 21)
                    ))
                {
                    foreach(var y in l2
                        .Where(a =>
                            a.Code == Sm.GetGrdStr(Grd1, i, 21)
                        ))
                    {
                        Grd1.Cells[i, y.Cols].Value = y.Yr == "0" ? x.Amt : x.AmtOld;
                        break;
                    }
                }
            }
        }

        private void ProcessSpecialCode()
        {
            Process027();
            Process028();
            CalculateEquity();
            Process029();
        }

        private void Process027()
        {
            var lx = new List<EquitySpecialCodeCol>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T1.DocType, T1.Code, T1.AcNo, T1.Amt, T1.AmtOld, IfNull(T2.OptDesc, '2') Cols, IfNull(T3.OptDesc, '0') Yr From ( ");
            SQL.AppendLine("SELECT T.Doctype, T.Code, T.AcNo, SUM(T.Amt) Amt, SUM(T.AmtOld) AmtOld ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT A.DocType, A.Code, B.AcNo, IFNULL(D.Amt, 0.00) Amt, 0.00 AmtOld ");
            SQL.AppendLine("    FROM TblFicoSettingJournalToCBPHdr A ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl B ON A.Doctype = B.Doctype ");
            SQL.AppendLine("        AND A.Code = B.Code ");
            SQL.AppendLine("        AND A.DocType = 'Equity' ");
            SQL.AppendLine("        AND A.ActInd = 'Y' ");
            SQL.AppendLine("        AND Find_In_Set(A.Code, @SpecialEquity2Code) ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON C.AcNo Like CONCAT(B.AcNo, '%') ");
            SQL.AppendLine("    LEFT JOIN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T2.AcNo, If(T3.AcType = 'D', (T2.DAmt - T2.CAmt), (T2.CAmt - T2.DAmt)) Amt ");
            SQL.AppendLine("        FROM TblJournalHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblJournalDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            AND LEFT(T1.DocDt, 4) = @Yr ");
            SQL.AppendLine("            AND T1.Cancelind = 'N' ");
            SQL.AppendLine("            AND T2.AcNo IN ");
            SQL.AppendLine("            ( ");
            SQL.AppendLine("                SELECT DISTINCT X3.AcNo ");
            SQL.AppendLine("                FROM TblFicoSettingJournalToCBPHdr X1 ");
            SQL.AppendLine("                INNER JOIN TblFicoSettingJournalToCBPDtl X2 ON X1.DocType = 'Equity' ");
            SQL.AppendLine("                    AND X1.DocType = X2.DocType ");
            SQL.AppendLine("                    AND X1.Code = X2.Code ");
            SQL.AppendLine("                    AND X1.Actind = 'Y' ");
            SQL.AppendLine("                    AND Find_In_Set(X1.Code, @SpecialEquity2Code) ");
            SQL.AppendLine("                INNER JOIN TblCOA X3 ON X3.AcNo LIKE CONCAT(X2.AcNo, '%') ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        INNER JOIN TblCOA T3 ON T2.AcNo = T3.AcNo ");
            SQL.AppendLine("    ) D ON C.AcNo = D.AcNo ");

            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT A.DocType, A.Code, B.AcNo, 0.00 Amt, IFNULL(D.Amt, 0.00) AmtOld ");
            SQL.AppendLine("    FROM TblFicoSettingJournalToCBPHdr A ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl B ON A.Doctype = B.Doctype ");
            SQL.AppendLine("        AND A.Code = B.Code ");
            SQL.AppendLine("        AND A.DocType = 'Equity' ");
            SQL.AppendLine("        AND A.ActInd = 'Y' ");
            SQL.AppendLine("        AND Find_In_set(A.Code, @SpecialEquity2Code) ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON C.AcNo Like CONCAT(B.AcNo, '%') ");
            SQL.AppendLine("    LEFT JOIN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T2.AcNo, If(T3.AcType = 'D', (T2.DAmt - T2.CAmt), (T2.CAmt - T2.DAmt)) Amt ");
            SQL.AppendLine("        FROM TblJournalHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblJournalDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            AND LEFT(T1.DocDt, 4) = (@Yr - 1) ");
            SQL.AppendLine("            AND T1.Cancelind = 'N' ");
            SQL.AppendLine("            AND T2.AcNo IN ");
            SQL.AppendLine("            ( ");
            SQL.AppendLine("                SELECT DISTINCT X3.AcNo ");
            SQL.AppendLine("                FROM TblFicoSettingJournalToCBPHdr X1 ");
            SQL.AppendLine("                INNER JOIN TblFicoSettingJournalToCBPDtl X2 ON X1.DocType = 'Equity' ");
            SQL.AppendLine("                    AND X1.DocType = X2.DocType ");
            SQL.AppendLine("                    AND X1.Code = X2.Code ");
            SQL.AppendLine("                    AND X1.Actind = 'Y' ");
            SQL.AppendLine("                    AND Find_In_Set(X1.Code, @SpecialEquity2Code) ");
            SQL.AppendLine("                INNER JOIN TblCOA X3 ON X3.AcNo LIKE CONCAT(X2.AcNo, '%') ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        INNER JOIN TblCOA T3 ON T2.AcNo = T3.AcNo ");
            SQL.AppendLine("    ) D ON C.AcNo = D.AcNo ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("GROUP BY T.Doctype, T.Code, T.AcNo ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Left Join TblOption T2 On T2.OptCat = 'Equity2SpecialCodeCol' ");
            SQL.AppendLine("    And T2.OptCode = T1.AcNo ");
            SQL.AppendLine("LEFT JOIN TblOption T3 ON T3.optCat = 'Equity2ColYr' AND IFNULL(T2.OptDesc, '2') = T3.OptCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@SpecialEquity2Code", mSpecialEquity2Code);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Doctype", "Code", "AcNo", "Amt", "AmtOld", "Cols", "Yr" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lx.Add(new EquitySpecialCodeCol()
                        {
                            DocType = Sm.DrStr(dr, c[0]),
                            Code = Sm.DrStr(dr, c[1]),
                            AcNo = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            AmtOld = Sm.DrDec(dr, c[4]),
                            Cols = Sm.DrInt(dr, c[5]),
                            Yr = Sm.DrStr(dr, c[6])
                        });
                    }
                }
                dr.Close();
            }

            if (lx.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    foreach (var x in lx)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 20) == x.DocType &&
                            Sm.GetGrdStr(Grd1, i, 21) == x.Code)
                        {
                            Grd1.Cells[i, x.Cols].Value = Sm.GetGrdDec(Grd1, i, x.Cols) + (x.Yr == "0" ? x.Amt : x.AmtOld); 
                        }
                    }
                }
            }

            lx.Clear();
        }

        private void Process028()
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 21) == "028")
                {
                    for (int j = 2; j < 16; ++j) // kolom ke
                    {
                        Grd1.Cells[i, j].Value = Sm.GetGrdDec(Grd1, (i - 3), j) + Sm.GetGrdDec(Grd1, (i - 2), j) + Sm.GetGrdDec(Grd1, (i - 1), j);
                    }

                    break;
                }
            }
        }

        private void Process029()
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if(Sm.GetGrdStr(Grd1, i, 21) == "029")
                {
                    for (int j = 2; j < Grd1.Cols.Count - 2; ++j) // kolom ke
                    {
                        for(int k = 4; k < Grd1.Rows.Count - 1; ++k) // baris mulai summary
                        {
                            Grd1.Cells[i, j].Value = Sm.GetGrdDec(Grd1, i, j) + Sm.GetGrdDec(Grd1, k, j);
                        }
                    }
                    break;
                }
            }
        }

        private void CalculateEquity()
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 21) != "029")
                {
                    for (int j = 2; j < 17; ++j)
                    {
                        Grd1.Cells[i, 17].Value = Sm.GetGrdDec(Grd1, i, 17) + Sm.GetGrdDec(Grd1, i, j);
                        Grd1.Cells[i, 19].Value = Sm.GetGrdDec(Grd1, i, 17) + Sm.GetGrdDec(Grd1, i, 18);
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Class

        private class Equity2RowCols
        {
            public string Code { get; set; }
            public int Cols { get; set; }
            public string Yr { get; set; }
        }

        private class Equity
        {
            public string DocType { get; set; }
            public string Code { get; set; }
            public decimal Amt { get; set; }
            public decimal AmtOld { get; set; }
        }

        private class EquityCOA
        {
            public string DocType { get; set; }
            public string Code { get; set; }
            public decimal Amt { get; set; }
            public decimal AmtOld { get; set; }
        }

        private class EquitySpecialCodeCol
        {
            public int Cols { get; set; }
            public string AcNo { get; set; }
            public string DocType { get; set; }
            public string Code { get; set; }
            public decimal Amt { get; set; }
            public decimal AmtOld { get; set; }
            public string Yr { get; set; }
        }

        #endregion
    }
}
