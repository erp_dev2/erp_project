﻿namespace RunSystem
{
    partial class FrmAP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.TxtSSID = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtAPName = new DevExpress.XtraEditors.TextEdit();
            this.TxtAPCode = new DevExpress.XtraEditors.TextEdit();
            this.LueAntennaCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.LueChannelWidthCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueWLanCardCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtFrequency = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSSID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAPName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAntennaCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueChannelWidthCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWLanCardCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFrequency.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtFrequency);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.LueWLanCardCode);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.LueChannelWidthCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueAntennaCode);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtSSID);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.TxtAPName);
            this.panel2.Controls.Add(this.TxtAPCode);
            this.panel2.Size = new System.Drawing.Size(772, 164);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 164);
            this.panel3.Size = new System.Drawing.Size(772, 309);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(772, 309);
            this.Grd1.TabIndex = 25;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(95, 53);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "SSID";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSSID
            // 
            this.TxtSSID.EnterMoveNextControl = true;
            this.TxtSSID.Location = new System.Drawing.Point(133, 50);
            this.TxtSSID.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSSID.Name = "TxtSSID";
            this.TxtSSID.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSSID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSSID.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSSID.Properties.Appearance.Options.UseFont = true;
            this.TxtSSID.Properties.MaxLength = 80;
            this.TxtSSID.Size = new System.Drawing.Size(319, 20);
            this.TxtSSID.TabIndex = 16;
            this.TxtSSID.Validated += new System.EventHandler(this.TxtSSID_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(17, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Access Point Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(20, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Access Point Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(376, 5);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(65, 22);
            this.ChkActInd.TabIndex = 12;
            // 
            // TxtAPName
            // 
            this.TxtAPName.EnterMoveNextControl = true;
            this.TxtAPName.Location = new System.Drawing.Point(133, 28);
            this.TxtAPName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAPName.Name = "TxtAPName";
            this.TxtAPName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAPName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAPName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAPName.Properties.Appearance.Options.UseFont = true;
            this.TxtAPName.Properties.MaxLength = 80;
            this.TxtAPName.Size = new System.Drawing.Size(319, 20);
            this.TxtAPName.TabIndex = 14;
            this.TxtAPName.EditValueChanged += new System.EventHandler(this.TxtAPName_EditValueChanged);
            this.TxtAPName.Validated += new System.EventHandler(this.TxtAPName_Validated);
            // 
            // TxtAPCode
            // 
            this.TxtAPCode.EnterMoveNextControl = true;
            this.TxtAPCode.Location = new System.Drawing.Point(133, 6);
            this.TxtAPCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAPCode.Name = "TxtAPCode";
            this.TxtAPCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAPCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAPCode.Properties.Appearance.Options.UseFont = true;
            this.TxtAPCode.Properties.MaxLength = 20;
            this.TxtAPCode.Size = new System.Drawing.Size(234, 20);
            this.TxtAPCode.TabIndex = 11;
            this.TxtAPCode.Validated += new System.EventHandler(this.TxtAPCode_Validated);
            // 
            // LueAntennaCode
            // 
            this.LueAntennaCode.EnterMoveNextControl = true;
            this.LueAntennaCode.Location = new System.Drawing.Point(133, 72);
            this.LueAntennaCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueAntennaCode.Name = "LueAntennaCode";
            this.LueAntennaCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAntennaCode.Properties.Appearance.Options.UseFont = true;
            this.LueAntennaCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAntennaCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAntennaCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAntennaCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAntennaCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAntennaCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAntennaCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAntennaCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAntennaCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAntennaCode.Properties.DropDownRows = 25;
            this.LueAntennaCode.Properties.NullText = "[Empty]";
            this.LueAntennaCode.Properties.PopupWidth = 350;
            this.LueAntennaCode.Size = new System.Drawing.Size(319, 20);
            this.LueAntennaCode.TabIndex = 18;
            this.LueAntennaCode.ToolTip = "F4 : Show/hide list";
            this.LueAntennaCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAntennaCode.EditValueChanged += new System.EventHandler(this.LueAntennaCode_EditValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(74, 75);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 14);
            this.label10.TabIndex = 17;
            this.label10.Text = "Antenna";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueChannelWidthCode
            // 
            this.LueChannelWidthCode.EnterMoveNextControl = true;
            this.LueChannelWidthCode.Location = new System.Drawing.Point(133, 116);
            this.LueChannelWidthCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueChannelWidthCode.Name = "LueChannelWidthCode";
            this.LueChannelWidthCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueChannelWidthCode.Properties.Appearance.Options.UseFont = true;
            this.LueChannelWidthCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueChannelWidthCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueChannelWidthCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueChannelWidthCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueChannelWidthCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueChannelWidthCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueChannelWidthCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueChannelWidthCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueChannelWidthCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueChannelWidthCode.Properties.DropDownRows = 25;
            this.LueChannelWidthCode.Properties.NullText = "[Empty]";
            this.LueChannelWidthCode.Properties.PopupWidth = 350;
            this.LueChannelWidthCode.Size = new System.Drawing.Size(319, 20);
            this.LueChannelWidthCode.TabIndex = 22;
            this.LueChannelWidthCode.ToolTip = "F4 : Show/hide list";
            this.LueChannelWidthCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueChannelWidthCode.EditValueChanged += new System.EventHandler(this.LueChannelWidthCode_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(41, 119);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 14);
            this.label4.TabIndex = 21;
            this.label4.Text = "Channel Width";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWLanCardCode
            // 
            this.LueWLanCardCode.EnterMoveNextControl = true;
            this.LueWLanCardCode.Location = new System.Drawing.Point(133, 138);
            this.LueWLanCardCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWLanCardCode.Name = "LueWLanCardCode";
            this.LueWLanCardCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLanCardCode.Properties.Appearance.Options.UseFont = true;
            this.LueWLanCardCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLanCardCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWLanCardCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLanCardCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWLanCardCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLanCardCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWLanCardCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLanCardCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWLanCardCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWLanCardCode.Properties.DropDownRows = 25;
            this.LueWLanCardCode.Properties.NullText = "[Empty]";
            this.LueWLanCardCode.Properties.PopupWidth = 350;
            this.LueWLanCardCode.Size = new System.Drawing.Size(319, 20);
            this.LueWLanCardCode.TabIndex = 24;
            this.LueWLanCardCode.ToolTip = "F4 : Show/hide list";
            this.LueWLanCardCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWLanCardCode.EditValueChanged += new System.EventHandler(this.LueWLanCardCode_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(26, 141);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 14);
            this.label5.TabIndex = 23;
            this.label5.Text = "Wireless Lan Card";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFrequency
            // 
            this.TxtFrequency.EnterMoveNextControl = true;
            this.TxtFrequency.Location = new System.Drawing.Point(133, 94);
            this.TxtFrequency.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFrequency.Name = "TxtFrequency";
            this.TxtFrequency.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFrequency.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFrequency.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFrequency.Properties.Appearance.Options.UseFont = true;
            this.TxtFrequency.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtFrequency.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtFrequency.Size = new System.Drawing.Size(109, 20);
            this.TxtFrequency.TabIndex = 20;
            this.TxtFrequency.Validated += new System.EventHandler(this.TxtFrequency_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(64, 97);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 14);
            this.label16.TabIndex = 19;
            this.label16.Text = "Frequency";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmAP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmAP";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSSID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAPName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAntennaCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueChannelWidthCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWLanCardCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFrequency.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtSSID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private DevExpress.XtraEditors.TextEdit TxtAPName;
        internal DevExpress.XtraEditors.TextEdit TxtAPCode;
        internal DevExpress.XtraEditors.LookUpEdit LueAntennaCode;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.LookUpEdit LueWLanCardCode;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.LookUpEdit LueChannelWidthCode;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtFrequency;
        private System.Windows.Forms.Label label16;
    }
}