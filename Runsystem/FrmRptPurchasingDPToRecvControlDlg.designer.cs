﻿namespace RunSystem
{
    partial class FrmRptPurchasingDPToRecvControlDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtPODocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtCurCode = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPODocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 55);
            this.panel3.Size = new System.Drawing.Size(842, 418);
            // 
            // Grd1
            // 
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(842, 418);
            this.Grd1.TabIndex = 5;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtCurCode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtPODocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(842, 55);
            this.panel2.TabIndex = 0;
            // 
            // TxtPODocNo
            // 
            this.TxtPODocNo.EnterMoveNextControl = true;
            this.TxtPODocNo.Location = new System.Drawing.Point(71, 7);
            this.TxtPODocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPODocNo.Name = "TxtPODocNo";
            this.TxtPODocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPODocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPODocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPODocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPODocNo.Properties.MaxLength = 30;
            this.TxtPODocNo.Properties.ReadOnly = true;
            this.TxtPODocNo.Size = new System.Drawing.Size(273, 20);
            this.TxtPODocNo.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(33, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "PO#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCurCode
            // 
            this.TxtCurCode.EnterMoveNextControl = true;
            this.TxtCurCode.Location = new System.Drawing.Point(71, 29);
            this.TxtCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode.Name = "TxtCurCode";
            this.TxtCurCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode.Properties.MaxLength = 30;
            this.TxtCurCode.Properties.ReadOnly = true;
            this.TxtCurCode.Size = new System.Drawing.Size(273, 20);
            this.TxtCurCode.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(10, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 14);
            this.label2.TabIndex = 3;
            this.label2.Text = "Currency";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmRptPurchasingDPToRecvControlDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmRptPurchasingDPToRecvControlDlg";
            this.Text = "PO\'s Item Information";
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPODocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtCurCode;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtPODocNo;
        private System.Windows.Forms.Label label1;
    }
}